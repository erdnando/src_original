package com.netro.cadenas;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.sql.ResultSet;
import java.util.HashMap;
import netropology.utilerias.*;
import org.apache.commons.logging.Log;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import netropology.utilerias.ServiceLocator;

public class ReporteNafinsaMovilDetalle implements IQueryGeneratorRegExtJS{

	private final static Log log = ServiceLocator.getInstance().getLog(ReporteNafinsaMovilDetalle.class);
	private List   conditions;
	private List   gridTotales;
	private String tipoDocto;
	private String anioDetalle;
	private String numMes;

	/**
	 * Obtiene las llaves primarias
	 * @return sentencia sql
	 */
	public String getDocumentQuery() {

		log.info("getDocumentQuery(E)");
		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();

		String fechaNotificacion = "01/" + this.numMes+ "/" + this.anioDetalle;

		qrySentencia.append("  SELECT DISTINCT CRN.IC_NAFIN_ELECTRONICO, \n"                                       );
		qrySentencia.append("         BDN.CG_ESTATUS, \n"                                                          );
		qrySentencia.append("         '" + fechaNotificacion + "' AS FECHA \n"                                     );
		qrySentencia.append("    FROM COM_PROMO_PYME CPP, \n"                                                      );
		qrySentencia.append("         COMREL_NAFIN CRN, \n"                                                        );
		qrySentencia.append("         COMCAT_PYME CCP, \n"                                                         );
		qrySentencia.append("         COM_CONTACTO CC, \n"                                                         );
		qrySentencia.append("         BIT_DOCTOS_NOTIFICADOS BDN \n"                                               );
		qrySentencia.append("   WHERE CPP.IC_PYME = CCP.IC_PYME \n"                                                );
		qrySentencia.append("     AND CPP.IC_PYME = CRN.IC_EPO_PYME_IF \n"                                         );
		qrySentencia.append("     AND CPP.IC_PYME = CC.IC_PYME \n"                                                 );
		qrySentencia.append("     AND CPP.IC_PYME = BDN.IC_PYME \n"                                                );
		qrySentencia.append("     AND CRN.CG_TIPO = ? \n"                                                          );
		qrySentencia.append("     AND CC.CS_PRIMER_CONTACTO = ? \n"                                                );
		qrySentencia.append("     AND BDN.CG_ESTATUS = ? \n"                                                       );
		qrySentencia.append("     AND BDN.DF_FECHA_NOTIFICACION >= TO_DATE (?, 'DD/MM/YYYY') \n"                   );
		qrySentencia.append("     AND BDN.DF_FECHA_NOTIFICACION < ADD_MONTHS (TO_DATE (?, 'DD/MM/YYYY'), 1) \n"    );
		qrySentencia.append("ORDER BY CRN.IC_NAFIN_ELECTRONICO ASC"                                                );

		conditions.add("P");
		conditions.add("S");
		conditions.add(this.tipoDocto);
		conditions.add(fechaNotificacion);
		conditions.add(fechaNotificacion);

		log.info("Sentencia:\n"   + qrySentencia.toString());
		log.info("Ccondiciones: " + conditions.toString());
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();

	}

	/**
	 * Obtiene la lista de los Ids en turno para realizar la paginacion
	 * @return sentencia sql
	 * @param ids
	 */
	public String getDocumentSummaryQueryForIds(List ids) {

		log.info("getDocumentSummaryQueryForIds(E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();
		String fechaNotificacion = "";
		String estatus = "";

		conditions.add("P");
		conditions.add("S");

		condicion.append("     AND CRN.IC_NAFIN_ELECTRONICO IN (");
		for (int i = 0; i < ids.size(); i++) {
			List lItem = (ArrayList)ids.get(i);
			if(i == 0){
				estatus =           lItem.get(1).toString();
				fechaNotificacion = lItem.get(2).toString();
				conditions.add(estatus);
				conditions.add(fechaNotificacion);
				conditions.add(fechaNotificacion);
			}
			if(i > 0){
				condicion.append(",");
			}
			condicion.append("?" );
			conditions.add(new Long(lItem.get(0).toString()));
		}
		condicion.append(") \n");

		qrySentencia.append("  SELECT CRN.IC_NAFIN_ELECTRONICO NAFIN_ELECTRONICO, CCP.IC_PYME IC_PYME, \n"         );
		qrySentencia.append("         CCP.CG_RFC RFC, CCP.CG_RAZON_SOCIAL NOMBRE_PYME, CC.CG_CELULAR NUM_CEL, \n"  );
		qrySentencia.append("         BDN.IG_NUMERO_DOCTOS DOCTOS_NOTIFICADOS, \n"                                 );
		qrySentencia.append("         BDN.FN_MONTO_NOTIFICADO MONTO_NOTIFICADO, \n"                                );
		qrySentencia.append("         BDN.IG_NUM_DOCTOS_OPERADOS DOCTOS_OPERADOS, \n"                              );
		qrySentencia.append("         BDN.FG_MONTO_OPERADO MONTO_OPERADO, \n"                                      );
		qrySentencia.append("         BDN.IG_NUM_DOCTOS_VENCIDOS DOCTOS_VENCIDOS, \n"                              );
		qrySentencia.append("         BDN.FG_MONTO_VENCIDO MONTO_VENCIDO \n"                                       );
		qrySentencia.append("    FROM COM_PROMO_PYME CPP, \n"                                                      );
		qrySentencia.append("         COMREL_NAFIN CRN, \n"                                                        );
		qrySentencia.append("         COMCAT_PYME CCP, \n"                                                         );
		qrySentencia.append("         COM_CONTACTO CC, \n"                                                         );
		qrySentencia.append("         BIT_DOCTOS_NOTIFICADOS BDN \n"                                               );
		qrySentencia.append("   WHERE CPP.IC_PYME = CCP.IC_PYME \n"                                                );
		qrySentencia.append("     AND CPP.IC_PYME = CRN.IC_EPO_PYME_IF \n"                                         );
		qrySentencia.append("     AND CPP.IC_PYME = CC.IC_PYME \n"                                                 );
		qrySentencia.append("     AND CPP.IC_PYME = BDN.IC_PYME \n"                                                );
		qrySentencia.append("     AND CRN.CG_TIPO = ? \n"                                                          );
		qrySentencia.append("     AND CC.CS_PRIMER_CONTACTO = ? \n"                                                );
		qrySentencia.append("     AND BDN.CG_ESTATUS = ? \n"                                                       );
		qrySentencia.append("     AND BDN.DF_FECHA_NOTIFICACION >= TO_DATE (?, 'DD/MM/YYYY') \n"                   );
		qrySentencia.append("     AND BDN.DF_FECHA_NOTIFICACION < ADD_MONTHS (TO_DATE (?, 'DD/MM/YYYY'), 1) \n"    );
		qrySentencia.append(condicion.toString()                                                                   );
		qrySentencia.append("ORDER BY CRN.IC_NAFIN_ELECTRONICO ASC"                                                );

		log.info("Sentencia:\n"  + qrySentencia.toString());
		log.info("Condiciones: " + conditions.toString());
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();

	}

	/**
	 * Obtiene los totales
	 * @return sentencia sql
	 */
	public String getAggregateCalculationQuery() {

		log.info("getAggregateCalculationQuery (E)");
		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();

		String fechaNotificacion = "01/" + this.numMes+ "/" + this.anioDetalle;

		qrySentencia.append("SELECT COUNT (IC_PYME) NO_PYMES, SUM (IG_NUMERO_DOCTOS) DOCTOS_NOTIFICADOS, \n");
		qrySentencia.append("       SUM (FN_MONTO_NOTIFICADO) MONTO_NOTIFICADO, \n"                         );
		qrySentencia.append("       SUM (IG_NUM_DOCTOS_OPERADOS) DOCTOS_OPERADOS, \n"                       );
		qrySentencia.append("       SUM (FG_MONTO_OPERADO) MONTO_OPERADO, \n"                               );
		qrySentencia.append("       SUM (IG_NUM_DOCTOS_VENCIDOS) DOCTOS_VENCIDOS, \n"                       );
		qrySentencia.append("       SUM (FG_MONTO_VENCIDO) MONTO_VENCIDO \n"                                );
		qrySentencia.append("  FROM BIT_DOCTOS_NOTIFICADOS \n"                                              );
		qrySentencia.append(" WHERE CG_ESTATUS = ? \n"                                                      );
		qrySentencia.append("   AND DF_FECHA_NOTIFICACION >= TO_DATE (?, 'DD/MM/YYYY') \n"                  );
		qrySentencia.append("   AND DF_FECHA_NOTIFICACION < ADD_MONTHS (TO_DATE (?, 'DD/MM/YYYY'), 1)"      );

		conditions.add(this.tipoDocto);
		conditions.add(fechaNotificacion);
		conditions.add(fechaNotificacion);

		log.info("Sentencia:\n"  + qrySentencia.toString());
		log.info("Condiciones: " + conditions.toString());
		log.info("getAggregateCalculationQuery (S)");
		return qrySentencia.toString();

	}

	/**
	 * Realiza la consulta para generar el archivo csv
	 * @return sentencia sql
	 */
	public String getDocumentQueryFile() {

		log.info(" getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();

		String fechaNotificacion = "01/" + this.numMes+ "/" + this.anioDetalle;

		qrySentencia.append("  SELECT DISTINCT CRN.IC_NAFIN_ELECTRONICO NAFIN_ELECTRONICO, CCP.IC_PYME IC_PYME, \n");
		qrySentencia.append("         CCP.CG_RFC RFC, CCP.CG_RAZON_SOCIAL NOMBRE_PYME, CC.CG_CELULAR NUM_CEL, \n"  );
		qrySentencia.append("         BDN.IG_NUMERO_DOCTOS DOCTOS_NOTIFICADOS, \n"                                 );
		qrySentencia.append("         BDN.FN_MONTO_NOTIFICADO MONTO_NOTIFICADO, \n"                                );
		qrySentencia.append("         BDN.IG_NUM_DOCTOS_OPERADOS DOCTOS_OPERADOS, \n"                              );
		qrySentencia.append("         BDN.FG_MONTO_OPERADO MONTO_OPERADO, \n"                                      );
		qrySentencia.append("         BDN.IG_NUM_DOCTOS_VENCIDOS DOCTOS_VENCIDOS, \n"                              );
		qrySentencia.append("         BDN.FG_MONTO_VENCIDO MONTO_VENCIDO \n"                                       );
		qrySentencia.append("    FROM COM_PROMO_PYME CPP, \n"                                                      );
		qrySentencia.append("         COMREL_NAFIN CRN, \n"                                                        );
		qrySentencia.append("         COMCAT_PYME CCP, \n"                                                         );
		qrySentencia.append("         COM_CONTACTO CC, \n"                                                         );
		qrySentencia.append("         BIT_DOCTOS_NOTIFICADOS BDN \n"                                               );
		qrySentencia.append("   WHERE CPP.IC_PYME = CCP.IC_PYME \n"                                                );
		qrySentencia.append("     AND CPP.IC_PYME = CRN.IC_EPO_PYME_IF \n"                                         );
		qrySentencia.append("     AND CPP.IC_PYME = CC.IC_PYME \n"                                                 );
		qrySentencia.append("     AND CPP.IC_PYME = BDN.IC_PYME \n"                                                );
		qrySentencia.append("     AND CRN.CG_TIPO = ? \n"                                                          );
		qrySentencia.append("     AND CC.CS_PRIMER_CONTACTO = ? \n"                                                );
		qrySentencia.append("     AND BDN.CG_ESTATUS = ? \n"                                                       );
		qrySentencia.append("     AND BDN.DF_FECHA_NOTIFICACION >= TO_DATE (?, 'DD/MM/YYYY') \n"                   );
		qrySentencia.append("     AND BDN.DF_FECHA_NOTIFICACION < ADD_MONTHS (TO_DATE (?, 'DD/MM/YYYY'), 1) \n"    );
		qrySentencia.append("ORDER BY CRN.IC_NAFIN_ELECTRONICO ASC"                                                );

		conditions.add("P");
		conditions.add("S");
		conditions.add(this.tipoDocto);
		conditions.add(fechaNotificacion);
		conditions.add(fechaNotificacion);

		log.info("Sentencia:\n" + qrySentencia.toString());
		log.info("Condiciones: "  + conditions.toString());
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();

	}

	/**
	 * Metodo para generar un archivo PDF, utilizando paginaci�n
	 * @return nombreArchivo
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearPageCustomFile(HttpServletRequest request, Registros rs, String path, String tipo) {
		return null;
	}

	/**
	 * Metodo para generar un archivo CSV, de todos los registros
	 * @return nombre del archivo
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo) {

		log.info("crearCustomFile (E)");

		String             nombreArchivo    = "";
		StringBuffer       contenidoArchivo = new StringBuffer();
		OutputStreamWriter writer           = null;
		BufferedWriter     buffer           = null;
		int                total            = 0;

		try {

			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer        = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer        = new BufferedWriter(writer);

			contenidoArchivo = new StringBuffer();
			contenidoArchivo.append("No. N@E, No. PyME, RFC, Raz�n Social, N�m. Celular, "+
											"No. de Documentos Notificados, Monto Notificado," +
											"No. de Documentos Operados, Monto Operado, No. de Documentos Vencidos, Monto Vencido \n");

			while (rs.next()){
				String ne                  = (rs.getString("NAFIN_ELECTRONICO")  == null) ? "" : rs.getString("NAFIN_ELECTRONICO");
				String noPyme              = (rs.getString("IC_PYME")            == null) ? "" : rs.getString("IC_PYME");
				String rfc                 = (rs.getString("RFC")                == null) ? "" : rs.getString("RFC");
				String razonSocial         = (rs.getString("NOMBRE_PYME")        == null) ? "" : rs.getString("NOMBRE_PYME");
				String numCelular          = (rs.getString("NUM_CEL")            == null) ? "" : rs.getString("NUM_CEL");
				String noDoctosNotificados = (rs.getString("DOCTOS_NOTIFICADOS") == null) ? "0": rs.getString("DOCTOS_NOTIFICADOS");
				String montoNotificado     = (rs.getString("MONTO_NOTIFICADO")   == null) ? "0": rs.getString("MONTO_NOTIFICADO");
				String noDoctosOperados    = (rs.getString("DOCTOS_OPERADOS")    == null) ? "0": rs.getString("DOCTOS_OPERADOS");
				String montoOperado        = (rs.getString("MONTO_OPERADO")      == null) ? "0": rs.getString("MONTO_OPERADO");
				String noDoctosVencidos    = (rs.getString("DOCTOS_VENCIDOS")    == null) ? "0": rs.getString("DOCTOS_VENCIDOS");
				String montoVencido        = (rs.getString("MONTO_VENCIDO")      == null) ? "0": rs.getString("MONTO_VENCIDO");

				contenidoArchivo.append(ne.replace(',',' ')                                                  +", "+
												noPyme.replace(',',' ')                                              +", "+
												rfc.replace(',',' ')                                                 +", "+
												razonSocial.replace(',',' ')                                         +", "+
												numCelular.replace(',',' ')                                          +", "+
												noDoctosNotificados.replace(',',' ')                                 +", "+
												"$" + Comunes.formatoDecimal(montoNotificado,2).replace(',',' ')     +", "+
												noDoctosOperados.replace(',',' ')                                    +", "+
												"$" + Comunes.formatoDecimal(montoOperado,2).replace(',',' ')        +", "+
												noDoctosVencidos.replace(',',' ')                                    +", "+
												"$" + Comunes.formatoDecimal(montoVencido,2).replace(',',' ')        +"\n");

				total++;
				if(total==1000){
					total=0;
					buffer.write(contenidoArchivo.toString());
					contenidoArchivo = new StringBuffer();//Limpio
				}
			}

			if(!this.gridTotales.isEmpty()){
				HashMap datosGrid = new HashMap();
				contenidoArchivo.append("Totales\n");
				contenidoArchivo.append("No. PYMES, No. de Documentos Notificados, Monto Notificado, No. de Documentos Operados, Monto Operado, No. de Documentos Vencidos, Monto Vencido \n");
				for(int i = 0; i < this.gridTotales.size(); i++){
					datosGrid = (HashMap) this.gridTotales.get(i);
					contenidoArchivo.append(
					datosGrid.get("NO_PYMES")                                                            + ", " +
					datosGrid.get("DOCTOS_NOTIFICADOS")                                                  + ", " +
					"$" + Comunes.formatoDecimal(datosGrid.get("MONTO_NOTIFICADO"),2).replace(',',' ')   + ", " +
					datosGrid.get("DOCTOS_OPERADOS")                                                     + ", " +
					"$" + Comunes.formatoDecimal(datosGrid.get("MONTO_OPERADO"),2).replace(',',' ')      + ", " +
					datosGrid.get("DOCTOS_VENCIDOS")                                                     + ", " +
					"$" + Comunes.formatoDecimal(datosGrid.get("MONTO_VENCIDO"),2).replace(',',' ')      + "\n"
					);
				}
			}

			buffer.write(contenidoArchivo.toString());
			buffer.close();	
			contenidoArchivo = new StringBuffer();//Limpio

		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo csv ", e);
		} finally {
			try {
			} catch(Exception e) {}
		}

		log.info(" crearCustomFile (S)");
		return nombreArchivo;
	}

/*****************************************
 *          GETTERS AND SETTERS          *
 *****************************************/
	public List getConditions() {
		return conditions;
	}

	public List getGridTotales() {
		return gridTotales;
	}

	public void setGridTotales(List gridTotales) {
		this.gridTotales = gridTotales;
	}

	public String getTipoDocto() {
		return tipoDocto;
	}

	public void setTipoDocto(String tipoDocto) {
		this.tipoDocto = tipoDocto;
	}

	public String getAnioDetalle() {
		return anioDetalle;
	}

	public void setAnioDetalle(String anioDetalle) {
		this.anioDetalle = anioDetalle;
	}

	public String getNumMes() {
		return numMes;
	}

	public void setNumMes(String numMes) {
		this.numMes = numMes;
	}

}