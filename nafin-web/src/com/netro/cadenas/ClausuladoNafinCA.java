package com.netro.cadenas;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.IQueryGeneratorPS;

public class ClausuladoNafinCA implements IQueryGeneratorPS {	

	private int numList = 1;
	
	public ClausuladoNafinCA() {}
	
	public int getNumList(HttpServletRequest request){
		return this.numList;

	}
  
	public ArrayList getConditions(HttpServletRequest request){
	System.out.println("*************getConditions=" + variablesBind);
   return this.variablesBind;
  	} // Fin del m�todo getConditions


	public String getAggregateCalculationQuery(HttpServletRequest request) {
		return "";
  	} // Fin del m�todo getAggregateCalculationQuery

	public String getDocumentQuery(HttpServletRequest request){
		
		this.variablesBind = new ArrayList();
		StringBuffer strQuery = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		
		String ic_epo				= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String ic_pyme				= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String txt_usuario			= (request.getParameter("txt_usuario")==null)?"":request.getParameter("txt_usuario");
		String txt_fecha_acep_de	= (request.getParameter("txt_fecha_acep_de")==null)?"":request.getParameter("txt_fecha_acep_de");
		String txt_fecha_acep_a		= (request.getParameter("txt_fecha_acep_a")==null)?"":request.getParameter("txt_fecha_acep_a");
		String ic_if				= (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String noBancoFondeo			= (request.getParameter("noBancoFondeo")==null)?"":request.getParameter("noBancoFondeo");
		//System.out.println("ic_if:::getDocumentQuery[[[[[[ "+ic_if);
				
		if(!"".equals(ic_epo)) {
			condicion.append(" AND con.ic_epo = ? ");
			this.variablesBind.add(new Integer(ic_epo));
		}
		if(!"".equals(ic_pyme)) {
			condicion.append(" AND con.ic_pyme = ? ");
			this.variablesBind.add(new Integer(ic_pyme));
		}
		if(!"".equals(txt_usuario)) {
			condicion.append(" AND con.ic_usuario = ? ");
			this.variablesBind.add(txt_usuario);
		}
		if(!"".equals(txt_fecha_acep_de)) {
			condicion.append(" AND con.df_aceptacion >= to_date(?, 'dd/mm/yyyy') ");
			this.variablesBind.add(txt_fecha_acep_de);
		}
		if(!"".equals(txt_fecha_acep_a)) {
			condicion.append(" AND con.df_aceptacion < to_date(?, 'dd/mm/yyyy') + 1 ");
			this.variablesBind.add(txt_fecha_acep_a);
		}
		if(!"".equals(ic_if)) {
			condicion.append(" AND con.ic_epo in (select ic_epo from comrel_if_epo where ic_if = ? and CS_VOBO_NAFIN = 'S') ");
			this.variablesBind.add(new Integer(ic_if));
		}
		
		if(!"".equals(noBancoFondeo) ){
			condicion.append(" AND epo.ic_banco_fondeo = ? ");
			this.variablesBind.add(new Integer(noBancoFondeo));
		}

		strQuery.append(
				" SELECT con.ic_epo||'|'||con.ic_consecutivo||'|'||con.ic_pyme, 'ClausuladoNafinCA::getDocumentQuery()' origen" +
				" FROM com_aceptacion_contrato con, comcat_epo epo "   +
				" WHERE con.ic_epo IS NOT NULL "   +
				" 	AND con.ic_pyme IS NOT NULL "   +
				"  AND con.ic_epo = epo.ic_epo" +
				condicion);

		System.out.println("strQuery.toString(): "+strQuery.toString());
		return strQuery.toString();
  	} // Fin del metodo getDocumentQuery

	public String getDocumentSummaryQueryForIds(HttpServletRequest request, Collection ids){

		StringBuffer strQuery = new StringBuffer();
		StringBuffer clavesCombinaciones = new StringBuffer("");
		
		for (Iterator it = ids.iterator(); it.hasNext();){
			it.next();
			clavesCombinaciones.append("?,");
		}
		clavesCombinaciones.deleteCharAt(clavesCombinaciones.length()-1);


		strQuery.append(
			" SELECT " +
			" 	      con.ic_epo, con.ic_consecutivo, pym.cg_razon_social nompyme,"   +
			" 	      cpe.cg_pyme_epo_interno proveedor, pym.cg_razon_social nompyme,"   +
			"        epo.cg_razon_social nomepo, con.ic_usuario,"   +
			"        TO_CHAR (con.df_aceptacion, 'dd/mm/yyyy hh24:mi') || ' Hrs.' df_aceptacion, "   +
			"        'ClausuladoNafinCA::getDocumentSummaryQueryForIds()' origen "   +
			"   FROM com_aceptacion_contrato con,"   +
			"        comrel_pyme_epo cpe,"   +
			"        comcat_pyme pym,"   +
			"        comcat_epo epo"   +
			"  WHERE con.ic_epo = cpe.ic_epo"   +
			"    AND con.ic_pyme = cpe.ic_pyme"   +
			"    AND cpe.ic_pyme = pym.ic_pyme"   +
			"    AND cpe.ic_epo = epo.ic_epo"  +
			"    AND con.ic_epo||'|'||con.ic_consecutivo||'|'||con.ic_pyme in ( " + clavesCombinaciones + " )");
		System.out.println("strQuery.toString(): "+strQuery.toString());
		
		return strQuery.toString();		
  } //Fin del metodo getDocumentSummaryQueryForIds
  
 	public String getDocumentQueryFile(HttpServletRequest request){

		StringBuffer strQuery = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		this.variablesBind = new ArrayList();
		
		String ic_epo				= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String ic_pyme				= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String txt_usuario			= (request.getParameter("txt_usuario")==null)?"":request.getParameter("txt_usuario");
		String txt_fecha_acep_de	= (request.getParameter("txt_fecha_acep_de")==null)?"":request.getParameter("txt_fecha_acep_de");
		String txt_fecha_acep_a		= (request.getParameter("txt_fecha_acep_a")==null)?"":request.getParameter("txt_fecha_acep_a");
		String ic_if				= (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String noBancoFondeo			= (request.getParameter("noBancoFondeo")==null)?"":request.getParameter("noBancoFondeo");
		
		//System.out.println("ic_if:::getDocumentQueryFile[[[[[[ "+ic_if);
				
		if(!"".equals(ic_epo)) {
			condicion.append(" AND con.ic_epo = ? ");
			this.variablesBind.add(new Integer(ic_epo));
		}
		if(!"".equals(ic_pyme)) {
			condicion.append(" AND con.ic_pyme = ? ");
			this.variablesBind.add(new Integer(ic_pyme));
		}
		if(!"".equals(txt_usuario)) {
			condicion.append(" AND con.ic_usuario = ? ");
			this.variablesBind.add(txt_usuario);
		}
		if(!"".equals(txt_fecha_acep_de)) {
			condicion.append(" AND con.df_aceptacion >= to_date(?, 'dd/mm/yyyy') ");
			this.variablesBind.add(txt_fecha_acep_de);
		}
		if(!"".equals(txt_fecha_acep_a)) {
			condicion.append(" AND con.df_aceptacion < to_date(?, 'dd/mm/yyyy') + 1 ");
			this.variablesBind.add(txt_fecha_acep_a);
		}
		if(!"".equals(ic_if)) {
			condicion.append(" AND con.ic_epo in (select ic_epo from comrel_if_epo where ic_if = ? and CS_VOBO_NAFIN = 'S') ");
			this.variablesBind.add(new Integer(ic_if));
		}
		
		System.out.println("Banco de Fondeo>>>>>>>>>>" + noBancoFondeo);
		if(!"".equals(noBancoFondeo) ){
			condicion.append( "    AND con.ic_epo = epo.ic_epo " +
									"    AND epo.ic_banco_fondeo = ? "); 
			this.variablesBind.add(new Integer(noBancoFondeo));
		}
		strQuery.append(
			" SELECT cpe.cg_pyme_epo_interno proveedor, pym.cg_razon_social nompyme,"   +
			"        epo.cg_razon_social nomepo, con.ic_usuario,"   +
			"        TO_CHAR (con.df_aceptacion, 'dd/mm/yyyy hh24:mi') || ' Hrs.' df_aceptacion, "   +
			"        'ClausuladoNafinCA::getDocumentQueryFile()' origen "   +
			"   FROM com_aceptacion_contrato con,"   +
			"        comrel_pyme_epo cpe,"   +
			"        comcat_pyme pym,"   +
			"        comcat_epo epo"   +
			"  WHERE con.ic_epo = cpe.ic_epo"   +
			"    AND con.ic_pyme = cpe.ic_pyme"   +
			"    AND cpe.ic_pyme = pym.ic_pyme"   +
			"    AND cpe.ic_epo = epo.ic_epo" + condicion) ;
			//"    AND con.ic_epo = epo.ic_epo " +
			//"	  AND epo.ic_banco_fondeo = 2 " +
			//condicion);
			
						
			
		System.out.println("strQuery.toString(): "+strQuery.toString());
		return strQuery.toString();
		
  	} // Fin del metodo getDocumentQueryFile


	private ArrayList variablesBind = null;
  	
} // Fin de la clase ConsProvDistIfCA