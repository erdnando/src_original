package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class BitacoraPagos implements IQueryGeneratorRegExtJS {

  private final static Log log = ServiceLocator.getInstance().getLog(BitacoraPagos.class);
  private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
  StringBuffer qrySentencia = new StringBuffer("");
  
  private String radio;
  private String intermediario;
  private String numInter;
  private String fechaInicial;
  private String fechaFinal;
  private String tipo;
  private String moneda;
  private String numSirac;
  private String cliExterno;
  private String tipoLinea;
  
  private String totales;
    
  public BitacoraPagos()  {  }
  
  /**
   * 
   * 
   */
   public Registros catalogoIF(){
    log.info("CatalogoIF(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 
		Registros registros  = new Registros();
		try{
			con.conexionDB();
      
      if("C".equals(tipoLinea))
      {
        if("NB".equals(intermediario))
        {
          qrySentencia.append(  " SELECT i.IN_NUMERO_SIRAC as clave, i.IN_NUMERO_SIRAC || ' ' || i.cg_razon_social	as descripcion "+
            " FROM comcat_if i, ope_datos_if od	"+
            " WHERE i.ic_if = od.ic_if " + 
            " AND i.cs_habilitado = 'S' AND i.cs_tipo = '"+ intermediario +"'	"+
            " AND i.IN_NUMERO_SIRAC is not null " +
            " ORDER BY i.IN_NUMERO_SIRAC ");
        }else{
        qrySentencia.append(  " SELECT i.IN_NUMERO_SIRAC as clave, i.IN_NUMERO_SIRAC || ' ' || i.cg_razon_social	as descripcion "+
            " FROM comcat_if i	"+
            " WHERE i.cs_habilitado = 'S' AND i.cs_tipo = '"+ intermediario +"'	"+
            " AND i.IN_NUMERO_SIRAC is not null " +
            " ORDER BY i.IN_NUMERO_SIRAC ");
        }
      }else{
			qrySentencia.append( " \n \n"+
      " SELECT i.ic_if as clave, i.ic_financiera || ' ' || i.cg_razon_social	as descripcion \n"+
			" FROM comcat_if i		\n"+
			" WHERE i.cs_habilitado = 'S' AND i.cs_tipo = '"+ intermediario +"'		\n"+
			" ORDER BY i.ic_financiera "+
      " \n \n");				
      }
			registros = con.consultarDB(qrySentencia.toString());
			
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("CatalogoIF  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 		
		log.info("qrySentencia CatalogoIF : "+qrySentencia.toString());
		log.info("CatalogoIF (S) ");
		return registros;															
	}
  
   public Registros catalogoClienteExterno(){
    log.info("catalogoClienteExterno(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 
		Registros registros  = new Registros();
		try{
			con.conexionDB();
			qrySentencia.append(	"SELECT   i.ic_nafin_electronico AS clave, " +
                "            '(' " +
                "         || i.ic_nafin_electronico " +
                "         || ') ' " +
                "         || ' ' " +
                "         || i.cg_razon_social AS descripcion " +
                "    FROM comcat_cli_externo i where i.cs_habilitado = 'S' " +
                "ORDER BY i.ic_nafin_electronico ");
			registros = con.consultarDB(qrySentencia.toString());

			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("catalogoClienteExterno  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 		
		log.info("qrySentencia catalogoClienteExterno : "+qrySentencia.toString());
		log.info("catalogoClienteExterno (S) ");
		return registros;															
	}
   
  	/**
	 * Este m�todo debe regresar un query con el que se obtienen los 
	 * identificadores unicos de los registros a mostrar en la consulta
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public  String getDocumentQuery(){
  	qrySentencia 	= new StringBuffer();
    	String			condicion		= "";
      conditions 		= new ArrayList();
    	
		try {
			if(!"".equals(intermediario)){
				condicion += "    AND bcp.ic_if = ? ";
        conditions.add(intermediario);
      }
			if(!"".equals(numInter)){
				condicion += "    AND ci.ic_financiera = ?";
        conditions.add(numInter);
      }
			if(!"".equals(fechaInicial) && !"".equals(fechaFinal)){
				condicion += " and trunc(bcp.df_consulta_pago) >= trunc(to_date(?,'dd/mm/yyyy')) and trunc(bcp.df_consulta_pago) <= trunc(to_date(?,'dd/mm/yyyy'))";
        conditions.add(fechaInicial);
        conditions.add(fechaFinal);
      }
		if(!"".equals(radio) && !"CE".equals(radio)){
    			condicion += "    AND ci.cs_tipo = ? ";
          conditions.add(radio);
      }
    		if(!"".equals(tipo)){
    			if(tipo.equals("A")){
    				condicion += "    AND bcp.cg_pantalla_consulta is null ";
           // conditions.add(tipo);
          }
    			else{
    				condicion += "    AND bcp.cg_pantalla_consulta = ? ";
            conditions.add(tipo); 
          }
    		}    			
    		if(!"".equals(moneda)){
    			condicion += "    AND mon.ic_moneda = ? ";
          conditions.add(moneda); 
        }

        if(!"".equals(cliExterno)){
          condicion += "    AND bcp.ic_cliente_externo = ? ";
          conditions.add(cliExterno);
        }
        
        if(!"".equals(numSirac)){
          condicion += "    AND bcp.ig_cliente = ? ";
          conditions.add(numSirac);
        }
        
        if(!"".equals(tipoLinea)){
          condicion += "    AND bcp.cg_tipo_linea = ? ";
          conditions.add(tipoLinea);
        }

		if(!"".equals(radio) && !"CE".equals(radio)){
			qrySentencia.append(" SELECT bcp.ic_if||'-'||TO_CHAR (bcp.df_consulta_pago, 'dd/mm/yyyy hh24:mi:ss') as ids " );
		}else
		{
			qrySentencia.append(" SELECT bcp.ic_cliente_externo||'-'||TO_CHAR (bcp.df_consulta_pago, 'dd/mm/yyyy hh24:mi:ss') as ids " );
		}
		
		qrySentencia.append("   FROM bit_consulta_pago bcp, comcat_if ci, comcat_moneda mon" );
		qrySentencia.append("  WHERE bcp.ic_if = ci.ic_if(+) ");

			if(tipo.equals("A")){
				qrySentencia.append(" AND bcp.ic_moneda = mon.ic_moneda");
      }
			else {
				qrySentencia.append(" AND bcp.ic_moneda = mon.ic_moneda(+)");
      }
		if("C".equals(tipoLinea))
		{
			qrySentencia.append(" AND bcp.IG_CLIENTE is not null ");
		}
	  
	  if("CE".equals(radio)){
			qrySentencia.append("    AND bcp.IC_CLIENTE_EXTERNO is not null ");
			
      }
	  
			qrySentencia.append(condicion);
			qrySentencia.append(" order by bcp.ic_if, bcp.df_consulta_pago ");
    	}catch(Exception e) {
      		System.err.println("BitacoraPagos::getDocumentQueryException "+e);
    	}
  log.debug("Query : \n \n"+qrySentencia +" \n \n");
  log.debug("Varibles Bind: "+conditions+"\n \n");
  return qrySentencia.toString();
  }
  
	
  
	/**
	 * Este m�todo debe regresar un query con el que se obtienen totales de la
	 * consulta.
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public  String getAggregateCalculationQuery(){
    
      StringBuffer	qrySentencia	= new StringBuffer();
    	String			condicion		= "";
      conditions = new ArrayList();
      try {
    		if(!"".equals(intermediario)){
    			condicion += "    AND bcp.ic_if = ? ";
          conditions.add(intermediario);
        }
    		if(!"".equals(numInter)){
    			condicion += "    AND ci.ic_financiera = ?";
          conditions.add(numInter); 
        }
    		if(!"".equals(fechaInicial) && !"".equals(fechaFinal)){
    			condicion += " and trunc(bcp.df_consulta_pago) >= trunc(to_date(?,'dd/mm/yyyy')) and trunc(bcp.df_consulta_pago) <= trunc(to_date(?,'dd/mm/yyyy'))";
          conditions.add(fechaInicial);
          conditions.add(fechaFinal);
        }
    		if(!"".equals(radio) && !"CE".equals(radio)){
    			condicion += "    AND ci.cs_tipo = ? ";
          conditions.add(radio);
        }
    		if(!"".equals(tipo)){
    			if(tipo.equals("A")){
    				condicion += "    AND bcp.cg_pantalla_consulta is null ";
          } else{
    				condicion += "    AND bcp.cg_pantalla_consulta = ? ";
            conditions.add(tipo);
          }
    		}    			
    		if(!"".equals(moneda)){
    			condicion += "    AND mon.ic_moneda = ? ";
          conditions.add(moneda); 
        }

         if(!"".equals(cliExterno)){
          condicion += "    AND bcp.ic_cliente_externo = ? ";
          conditions.add(cliExterno);
        }
        
        if(!"".equals(numSirac)){
          condicion += "    AND bcp.ig_cliente = ? ";
          conditions.add(numSirac);
        }
        
        if(!"".equals(tipoLinea)){
          condicion += "    AND bcp.cg_tipo_linea = ? ";
          conditions.add(tipoLinea);
        }

		if("C".equals(tipoLinea))
		{
			condicion += " AND bcp.IG_CLIENTE is not null ";
		}
		
		if("CE".equals(radio)){
			condicion += "    AND bcp.IC_CLIENTE_EXTERNO is not null ";
			
		}

			qrySentencia.append(
				" SELECT bcp.ic_moneda, mon.cd_nombre, COUNT (1) as total, SUM (bcp.fn_total_pagar), 'BitPagosNafinCA::getAggregateCalculationQuery'"   +
				"   FROM bit_consulta_pago bcp, comcat_if ci, comcat_moneda mon"   +
				"  WHERE bcp.ic_if = ci.ic_if(+)"   +
				"    AND bcp.ic_moneda = mon.ic_moneda"   +
				condicion +
				"  GROUP BY bcp.ic_moneda,  mon.cd_nombre"   +
				"  ORDER BY bcp.ic_moneda");
      
      }catch(Exception e){
        System.out.println("BitacoraPagos::getAggregateCalculationQuery "+e);
      }
    
    
  log.info("qrySentencia  "+qrySentencia);
	log.info("conditions "+conditions);
	log.info("getAggregateCalculationQuery(S)");  
  return qrySentencia.toString();	
  }
  
  /**
	 * Este m�todo debe regresar un query con el que se obtienen los 
	 * datos a mostrar en la consulta basandose en los identificadores unicos 
	 * especificados en las lista de ids
	 * @param ids Lista de los identificadoes unicos. El tama�o de la lista 
	 * 	recibida ser� de acuerdo a la configuraci�n de registros x p�gina
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public  String getDocumentSummaryQueryForIds(List ids){
  
  qrySentencia = new StringBuffer("");
  conditions = new ArrayList();
  log.info("getDocumentSummaryQueryForIds(E)");
  log.info("tipo "+tipo);
  if (tipo.equals("C")){
		qrySentencia.append("SELECT 'Cedula de conciliaci�n' AS tipo");
  } else if (tipo.equals("V")){
		qrySentencia.append("SELECT 'Vencimiento' AS tipo ");
  } else if (tipo.equals("O")){
		qrySentencia.append("SELECT 'Operados' AS tipo ");
  } else if (tipo.equals("E")){
		qrySentencia.append("SELECT 'Estado de cuenta' AS tipo ");
  } else if (tipo.equals("A")){
		qrySentencia.append("SELECT 'Archivos de pago' AS tipo ");
  } else {qrySentencia.append("SELECT '' AS tipo ");}
  qrySentencia.append(
        " , ci.ic_financiera AS intermediario, "+
        " ci.cg_razon_social AS nombreintermediario, bcp.ig_cliente AS cliente, bcp.ic_cliente_externo as cliexterno, "+
        " bcp.ic_usuario || ' ' || bcp.cg_nombre_usuario AS usuario, "+
        " TO_CHAR (bcp.df_consulta_pago, 'dd/mm/yyyy') AS fechaacceso, "+
        " cm.cd_nombre AS moneda, cm.ic_moneda AS ic_moneda ");
		  if (tipo.equals("C") | tipo.equals("O")){
			if (tipo.equals("C")){
				qrySentencia.append(", TO_CHAR (cc.df_fecha_corte, 'dd/mm/yyyy') AS fechacorte, ''  AS fechavencimiento");		  
			} else if (tipo.equals("O")){
					qrySentencia.append(", TO_CHAR (bcp.df_fecha_corte, 'dd/mm/yyyy') AS fechacorte, ''  AS fechavencimiento");		  
			}
		  } else {
				qrySentencia.append(", TO_CHAR (bcp.df_fecha_corte, 'dd/mm/yyyy') AS fechacorte , TO_CHAR (bcp.df_vencimiento, 'dd/mm/yyyy')  AS fechavencimiento ");		    
		  }
qrySentencia.append(		  
        " , ig_numero_prestamo AS prestamo, '' AS fechaoperacion, "+
        " '' AS proximopago, cli.cg_razon_social AS nombrecliexterno " +	  		  
        " FROM bit_consulta_pago bcp, comcat_if ci, comcat_moneda cm, comcat_cli_externo cli ");
		  if (tipo.equals("C")){
			qrySentencia.append(", com_cedula_concilia cc "); 
			}
	qrySentencia.append(		  		
		" WHERE bcp.ic_if = ci.ic_if(+) "+
		" AND bcp.ic_moneda = cm.ic_moneda(+)  " +
		" AND bcp.ic_cliente_externo = cli.ic_nafin_electronico(+)");
		  if (tipo.equals("C")){
		if(!"".equals(radio) && !"CE".equals(radio)){
			qrySentencia.append(" AND cc.ic_if = bcp.ic_if " );
		}else
		{
			qrySentencia.append(" AND bcp.ic_cliente_externo = cc.ic_cliente_externo " );
		}
		qrySentencia.append(" AND cc.cg_nombre_usuario = bcp.cg_nombre_usuario AND cc.ig_clave_certificado = bcp.ig_clave_certificado "); 
	}
	
	if("CE".equals(radio)){
		qrySentencia.append(" AND    bcp.ic_cliente_externo || '-' || TO_CHAR (bcp.df_consulta_pago, 'dd/mm/yyyy hh24:mi:ss') IN ("	);
	}else
	{
		qrySentencia.append(" AND    bcp.ic_if || '-' || TO_CHAR (bcp.df_consulta_pago, 'dd/mm/yyyy hh24:mi:ss') IN ("	);
			}
     List   pKeys = new ArrayList();
		for(int i = 0; i < ids.size(); i++){
			List lItem = (ArrayList)ids.get(i);
			if(i>0){
        qrySentencia.append(",?");
      } else{
        qrySentencia.append("?");
      }
			conditions.add(lItem.get(0));
			pKeys.add(ids.get(i));			
      }
      qrySentencia.append(")");
  log.info("getDocumentSummaryQueryForIds_qrySentencia  "+qrySentencia);
	log.info("conditions "+conditions);
    
  log.info("getDocumentSummaryQueryForIds(S)");
	return qrySentencia.toString();	
  }
  
	/**
	 * Este m�todo debe regresar una Lista de parametros que ser�n usados
	 * como valor de las variables BIND de los queries generados.
	 * @return Lista con los valores a usar en las variables bind
	 */
	public  List getConditions(){
  return conditions;
  }
  
	/**
	 * Este m�todo debe regresar un query que obtendr� todos los registros
	 * resultantes de la b�squeda, con la finalidad de generar un archivo.
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public  String getDocumentQueryFile(){
    log.info("getDocumentQueryFile(E)");
    StringBuffer 	qrySentencia 	= new StringBuffer();
    String			condicion		= "";
    conditions 		= new ArrayList();
		try {
			if(!"".equals(intermediario)){
				condicion += "    AND bcp.ic_if = ? ";
        conditions.add(intermediario);
      }
			if(!"".equals(numInter)){
				condicion += "    AND ci.ic_financiera = ?";
        conditions.add(numInter); 
      }
			if(!"".equals(fechaInicial) && !"".equals(fechaFinal)){
				condicion += " and trunc(bcp.df_consulta_pago) >= trunc(to_date(?,'dd/mm/yyyy')) and trunc(bcp.df_consulta_pago) <= trunc(to_date(?,'dd/mm/yyyy'))";
        conditions.add(fechaInicial);
        conditions.add(fechaFinal);
      }
			if(!"".equals(radio) && !"CE".equals(radio)){
    			condicion += "    AND ci.cs_tipo = ? ";
          conditions.add(radio);   
      }
    		if(!"".equals(tipo)){
    			if(tipo.equals("A")) {
    				condicion += "    AND bcp.cg_pantalla_consulta is null ";
    			} else {
    				condicion += "    AND bcp.cg_pantalla_consulta = ? ";
            conditions.add(tipo);
          }
    		}    			
    		if(!"".equals(moneda)){
    			condicion += "    AND cm.ic_moneda = ? ";
				conditions.add(moneda);
			}

       if(!"".equals(cliExterno)){
          condicion += "    AND bcp.ic_cliente_externo = ? ";
          conditions.add(cliExterno);
        }
        
        if(!"".equals(numSirac)){
          condicion += "    AND bcp.ig_cliente = ? ";
          conditions.add(numSirac);
        }
        
        if(!"".equals(tipoLinea)){
          condicion += "    AND bcp.cg_tipo_linea = ? ";
          conditions.add(tipoLinea);
        }
		
		if("C".equals(tipoLinea))
		{
			condicion +=  " AND bcp.IG_CLIENTE is not null ";
		}
		
		if("CE".equals(radio)){
			condicion += "    AND bcp.IC_CLIENTE_EXTERNO is not null ";
		
		}

			if(!tipo.equals("A")){				
				if(tipo.equals("E"))
					qrySentencia.append("select 'Estado de cuenta' as tipo, ");
				if(tipo.equals("V"))
					qrySentencia.append("select 'Vencimiento' as tipo, ");
				if(tipo.equals("O"))
					qrySentencia.append("select 'Operados' as tipo, ");
				if(tipo.equals("C"))
					qrySentencia.append("select DISTINCT 'Cedula de conciliaci�n' as tipo, ");
					
				qrySentencia.append("ci.ic_financiera as intermediario, ci.cg_razon_social as nombreintermediario, ");
				qrySentencia.append("bcp.ig_cliente as cliente, bcp.ic_usuario||' '||bcp.cg_nombre_usuario as usuario, ");
				qrySentencia.append("to_char(bcp.df_consulta_pago,'dd/mm/yyyy') as fechaAcceso, cm.cd_nombre as moneda, cm.ic_moneda AS ic_moneda, ");
				if (tipo.equals("C")){
					qrySentencia.append(" ig_numero_prestamo AS PRESTAMO, TO_CHAR (cc.df_fecha_corte, 'dd/mm/yyyy') AS fechacorte, ''  AS fechavencimiento, ");		  
				} else if (tipo.equals("O")){
					qrySentencia.append(" ig_numero_prestamo AS PRESTAMO, TO_CHAR (bcp.df_fecha_corte, 'dd/mm/yyyy') AS fechacorte, ''  AS fechavencimiento, ");		  
				} else{
					qrySentencia.append("to_char(bcp.df_fecha_corte,'dd/mm/yyyy') as fechaCorte, ig_numero_prestamo as prestamo, TO_CHAR (bcp.df_vencimiento, 'dd/mm/yyyy')  AS fechavencimiento,  ");
				}
				qrySentencia.append(" '' as fechaOperacion, '' as proximoPago, cli.cg_razon_social AS nombrecliexterno ");				
			}else{
				qrySentencia.append("select 'Archivos de pago' as tipo, ci.ic_financiera as intermediario, ci.cg_razon_social as nombreintermediario, ");
				qrySentencia.append("bcp.ig_cliente as cliente, bcp.ic_usuario||' '||bcp.cg_nombre_usuario as usuario, ");
				qrySentencia.append("to_char(bcp.df_consulta_pago,'dd/mm/yyyy') as fechaAcceso, cm.cd_nombre as moneda, cm.ic_moneda AS ic_moneda,");
				qrySentencia.append("'' as fechaCorte, '' as prestamo, '' as fechaOperacion, '' as proximoPago, ");
				qrySentencia.append("to_char(bcp.df_vencimiento,'dd/mm/yyyy') as fechaVencimiento, cli.cg_razon_social AS nombrecliexterno ");										
			}
				
			qrySentencia.append("from bit_consulta_pago bcp, comcat_if ci, comcat_moneda cm, comcat_cli_externo cli ");
			if (tipo.equals("C")){
					qrySentencia.append(", com_cedula_concilia cc "); 
			}
			if(!tipo.equals("A")){
				qrySentencia.append("where bcp.ic_if = ci.ic_if(+) and cm.ic_moneda(+) = bcp.ic_moneda ");
				 if (tipo.equals("C")){
					if(!"".equals(radio) && !"CE".equals(radio)){
						qrySentencia.append(" AND cc.ic_if = bcp.ic_if " );
					}else
					{
						qrySentencia.append(" AND bcp.ic_cliente_externo = cc.ic_cliente_externo " );
					}
					qrySentencia.append(" AND cc.cg_nombre_usuario = bcp.cg_nombre_usuario AND cc.ig_clave_certificado = bcp.ig_clave_certificado "); 
				}
			}else{
				qrySentencia.append("where bcp.ic_if = ci.ic_if(+) and cm.ic_moneda(+) = bcp.ic_moneda ");
			}
      
      qrySentencia.append(" AND bcp.ic_cliente_externo = cli.ic_nafin_electronico(+)");
  			qrySentencia.append(condicion);
  			if(tipo.equals("A")){
				qrySentencia.append("GROUP BY ci.ic_financiera, ci.cg_razon_social, bcp.ig_cliente, bcp.ic_usuario||' '||bcp.cg_nombre_usuario, ");
				qrySentencia.append("bcp.df_consulta_pago, cm.cd_nombre, bcp.df_vencimiento ");
			}

    	}catch(Exception e){
      		System.out.println("BitacoraPagos::getDocumentQueryFileException "+e);
    	}
	log.info("getDocumentQueryFile_qrySentencia  "+qrySentencia);
	log.info("conditions "+conditions);
	log.info("getDocumentQueryFile(S)");
	return qrySentencia.toString();
  }
  
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public  String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
    log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		//ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
    
    OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		int total = 0;
    
    try {
      nombreArchivo = Comunes.cadenaAleatoria(16) + ".txt";
    	writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
      buffer = new BufferedWriter(writer);
		
			contenidoArchivo = new StringBuffer();
      if(!"CE".equals(radio))
      {
      contenidoArchivo.append("Tipo,Intermediario,Nombre del Intermediario,Cliente,Usuario,Fecha de Acceso,Moneda,Fecha de Corte,Pr�stamo,Fecha de Operaci�n,Fecha de Vencimiento\n");
      }else{
        contenidoArchivo.append("Tipo,Intermediario,Nombre del Intermediario,No. Cliente SIRAC,Cliente Externo,Usuario,Fecha de Acceso,Moneda,Fecha de Corte,Pr�stamo,Fecha de Operaci�n,Fecha de Vencimiento\n");
      }
      
      while (rs.next())	{	
      String rsTipo = (rs.getString("TIPO") == null) ? "" : rs.getString("TIPO");  
      String rsIntermediario = (rs.getString("INTERMEDIARIO") == null) ? "" : rs.getString("INTERMEDIARIO");  
      String rsNombreInterme = (rs.getString("NOMBREINTERMEDIARIO") == null) ? "" : rs.getString("NOMBREINTERMEDIARIO");  
      String rsCliente = (rs.getString("CLIENTE") == null) ? "" : rs.getString("CLIENTE");  
      String rsUsuario = (rs.getString("USUARIO") == null) ? "" : rs.getString("USUARIO");  
      String rsFechaAc = (rs.getString("FECHAACCESO") == null) ? "" : rs.getString("FECHAACCESO");  
      String rsMoneda  = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");  
      String rsFechaCo = (rs.getString("FECHACORTE") == null) ? "" : rs.getString("FECHACORTE");  
      String rsPrestam = (rs.getString("PRESTAMO") == null) ? "" : rs.getString("PRESTAMO");  
      String rsFechaOp = (rs.getString("FECHAOPERACION") == null) ? "" : rs.getString("FECHAOPERACION");  
      String rsFechaVe = (rs.getString("FECHAVENCIMIENTO") == null) ? "" : rs.getString("FECHAVENCIMIENTO");  
      String rsCliExterno = (rs.getString("NOMBRECLIEXTERNO") == null) ? "" : rs.getString("NOMBRECLIEXTERNO");
		if(rsTipo.equals("Operados")){
			rsFechaOp = rsFechaCo;
			rsFechaCo="";
		} else if(rsTipo.equals("Vencimiento")){
			rsFechaVe = rsFechaCo;
			rsFechaCo="";		
		}
      contenidoArchivo.append(
        rsTipo.replace(',',' ')+",	"+        rsIntermediario.replace(',',' ')+",	"+
        rsNombreInterme.replace(',',' ')+","+ rsCliente.replace(',',' ')+","+
        (!"CE".equals(radio)?"":rsCliExterno+",")+
        rsUsuario.replace(',',' ')+","+       rsFechaAc.replace(',',' ')+","+
        rsMoneda.replace(',',' ')+","+        rsFechaCo.replace(',',' ')+","+
        rsPrestam.replace(',',' ')+","+       rsFechaOp.replace(',',' ')+","+
        rsFechaVe.replace(',',' ')+","+"\n");	
      }
      creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
      nombreArchivo = creaArchivo.getNombre();
  } catch(Throwable e) {
    throw new AppException("Error al generar el archivo ", e);
  } finally {
    try {
      //rs.close();
    } catch(Exception e) {}
    log.debug("crearCustomFile (S)");
  }
  return nombreArchivo;
  }
  
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public  String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo){
    String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		int contMn=0;
		int contDA=0;
    try{
      nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
      if(!"CE".equals(radio))
      pdfDoc.setTable(11,100); //encabezado 
      else
        pdfDoc.setTable(12,100); //encabezado 
      pdfDoc.setCell("Tipo","celda01",ComunesPDF.CENTER);                     pdfDoc.setCell("Intermediario","celda01",ComunesPDF.CENTER);
      pdfDoc.setCell("Nombre del Intermediario","celda01",ComunesPDF.CENTER); 
      if(!"CE".equals(radio)){
        pdfDoc.setCell("Cliente","celda01",ComunesPDF.CENTER);
      }else
      {
        pdfDoc.setCell("No. Cliente SIRAC","celda01",ComunesPDF.CENTER);
        pdfDoc.setCell("Cliente Externo","celda01",ComunesPDF.CENTER);
      }
      pdfDoc.setCell("Usuario","celda01",ComunesPDF.CENTER);                  pdfDoc.setCell("Fecha de Acceso","celda01",ComunesPDF.CENTER);
      pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);                   pdfDoc.setCell("Fecha de Corte","celda01",ComunesPDF.CENTER);
      pdfDoc.setCell("Pr�stamo","celda01",ComunesPDF.CENTER);                 pdfDoc.setCell("Fecha de Operaci�n","celda01",ComunesPDF.CENTER);
      pdfDoc.setCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
      
      while (reg.next())	{
        String regTipo = (reg.getString("TIPO") == null) ? "" : reg.getString("TIPO");  
        String regIntermediario = (reg.getString("INTERMEDIARIO") == null) ? "" : reg.getString("INTERMEDIARIO");  
        String regNombreInterme = (reg.getString("NOMBREINTERMEDIARIO") == null) ? "" : reg.getString("NOMBREINTERMEDIARIO");  
        String regCliente = (reg.getString("CLIENTE") == null) ? "" : reg.getString("CLIENTE");  
        String regUsuario = (reg.getString("USUARIO") == null) ? "" : reg.getString("USUARIO");  
        String regFechaAc = (reg.getString("FECHAACCESO") == null) ? "" : reg.getString("FECHAACCESO");  
        String regMoneda  = (reg.getString("MONEDA") == null) ? "" : reg.getString("MONEDA");  
        String regIcMoneda  = (reg.getString("IC_MONEDA") == null) ? "" : reg.getString("IC_MONEDA");  
        String regFechaCo = (reg.getString("FECHACORTE") == null) ? "" : reg.getString("FECHACORTE");  
        String regPrestam = (reg.getString("PRESTAMO") == null) ? "" : reg.getString("PRESTAMO");  
        String regFechaOp = (reg.getString("FECHAOPERACION") == null) ? "" : reg.getString("FECHAOPERACION");  
        String regFechaVe = (reg.getString("FECHAVENCIMIENTO") == null) ? "" : reg.getString("FECHAVENCIMIENTO");  
        String rsCliExterno = (reg.getString("NOMBRECLIEXTERNO") == null) ? "" : reg.getString("NOMBRECLIEXTERNO");
        
		  if(regCliente.equals("0")){
				regCliente = "";
		  }
		  if(regIcMoneda.equals("1")){
			contMn++;
		  }else if(regIcMoneda.equals("54")){
				contDA++;
		  }
		  
			pdfDoc.setCell(regTipo,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(regIntermediario,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(regNombreInterme,"formas",ComunesPDF.LEFT);
			pdfDoc.setCell(regCliente,"formas",ComunesPDF.LEFT);
      if("CE".equals(radio))
      {
        pdfDoc.setCell(rsCliExterno,"formas",ComunesPDF.LEFT);
      }
			pdfDoc.setCell(regUsuario,"formas",ComunesPDF.LEFT);
			pdfDoc.setCell(regFechaAc,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(regMoneda,"formas",ComunesPDF.CENTER);
			if(regTipo.equals("Operados")){
				pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			} else if(regTipo.equals("Vencimiento")){
				pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			} else {
				pdfDoc.setCell(regFechaCo,"formas",ComunesPDF.LEFT);
			}
			if(regPrestam.equals("0")){
						pdfDoc.setCell("","formas",ComunesPDF.LEFT);
			} else {
						pdfDoc.setCell(regPrestam,"formas",ComunesPDF.LEFT);
			}
			if(regTipo.equals("Operados")){
				pdfDoc.setCell(regFechaCo,"formas",ComunesPDF.CENTER);
			} else {
				pdfDoc.setCell(regFechaOp,"formas",ComunesPDF.CENTER);
			}
			if(regTipo.equals("Vencimiento")){
				pdfDoc.setCell(regFechaCo,"formas",ComunesPDF.CENTER);
			} else {
				pdfDoc.setCell(regFechaVe,"formas",ComunesPDF.CENTER);
			}
      }
		if(contMn>0||contDA>0){ 
			pdfDoc.setCell("","formasmen",ComunesPDF.CENTER,11);
		}
		if(contMn>0){  
			pdfDoc.setCell("TOTAL DE REGISTROS MONEDA NACIONAL:","formasmenB",ComunesPDF.RIGHT,5);
			pdfDoc.setCell(String.valueOf(contMn),"formasmen",ComunesPDF.CENTER,2);
			pdfDoc.setCell("","formasmen",ComunesPDF.LEFT,4);
		
		}
		if(contDA>0){
			pdfDoc.setCell("TOTAL DE REGISTROS D�LAR AMERICANO:","formasmenB",ComunesPDF.RIGHT,5);
			pdfDoc.setCell(String.valueOf(contDA),"formasmen",ComunesPDF.CENTER,2);
			pdfDoc.setCell("","formasmen",ComunesPDF.LEFT,4);
		
		}
      
      pdfDoc.addTable();
      pdfDoc.endDocument();	
      
    } catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
	return nombreArchivo;
  }
 
  
  
  


  public void setIntermediario(String intermediario)
  {
    this.intermediario = intermediario;
  }


  public String getIntermediario()
  {
    return intermediario;
  }


  public void setNumInter(String numInter)
  {
    this.numInter = numInter;
  }


  public String getNumInter()
  {
    return numInter;
  }


  public void setFechaInicial(String fechaInicial)
  {
    this.fechaInicial = fechaInicial;
  }


  public String getFechaInicial()
  {
    return fechaInicial;
  }


  public void setTipo(String tipo)
  {
    this.tipo = tipo;
  }


  public String getTipo()
  {
    return tipo;
  }


  public void setMoneda(String moneda)
  {
    this.moneda = moneda;
  }


  public String getMoneda()
  {
    return moneda;
  }


  public void setRadio(String radio)
  {
    this.radio = radio;
  }


  public String getRadio()
  {
    return radio;
  }


  public void setFechaFinal(String fechaFinal)
  {
    this.fechaFinal = fechaFinal;
  }


  public String getFechaFinal()
  {
    return fechaFinal;
  }


  public void setTotales(String totales)
  {
    this.totales = totales;
  }


  public String getTotales()
  {
    return totales;
  }


  public void setNumSirac(String numSirac)
  {
    this.numSirac = numSirac;
  }


  public String getNumSirac()
  {
    return numSirac;
  }


  public void setCliExterno(String cliExterno)
  {
    this.cliExterno = cliExterno;
  }


  public String getCliExterno()
  {
    return cliExterno;
  }


  public void setTipoLinea(String tipoLinea)
  {
    this.tipoLinea = tipoLinea;
  }


  public String getTipoLinea()
  {
    return tipoLinea;
  }
}