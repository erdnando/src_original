package com.netro.cadenas;
import netropology.utilerias.*;
import org.apache.commons.logging.Log;
import java.util.Collection;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.sql.ResultSet;
import java.util.List;
import java.util.HashMap;
import com.netro.parametrosgrales.*;

public class ConsMatrizApis implements IQueryGeneratorRegExtJS  {
	
		private final static Log log = ServiceLocator.getInstance().getLog(ConsMatrizApis.class);
		String sTipoInterfaz;
		String sNombreProducto; 
		private List 		conditions;
		StringBuffer qrySentencia = new StringBuffer("");
	
		public ConsMatrizApis() {}
		
		
		public String getDocumentQuery() {
			log.info("getDocumentQuery(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			
			log.info("qrySentencia  "+qrySentencia);
			log.info("conditions "+conditions);
			log.info("getDocumentQuery(S)");
			return qrySentencia.toString();
		}
		
		
		public String getDocumentSummaryQueryForIds(List ids){
			log.info("getDocumentSummaryQueryForIds(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			
			
			log.info("qrySentencia  "+qrySentencia);
			log.info("conditions "+conditions);
			log.info("getDocumentSummaryQueryForIds(S)");
			return qrySentencia.toString();
		}
	
		
		public String getAggregateCalculationQuery(){
			log.info("getAggregateCalculationQuery(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			
			
			log.info("qrySentencia  "+qrySentencia);
			log.info("conditions "+conditions);
			log.info("getAggregateCalculationQuery(S)");
			return qrySentencia.toString();	
		}
		
		public List getConditions(){
			return conditions;
		}
		
	
		public String getDocumentQueryFile(){
			log.info("getDocumentQueryFile(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			
			qrySentencia.append(  " select CA.cg_nombre as nombreAPI, RPA.ic_parametro as numeroParametro, "+
					" RPA.cg_nombre as nombreParametro, RPA.cg_descripcion as descParametro" +
					
					((sNombreProducto.equals("1C")||sNombreProducto.equals("T"))?",  MI.ig_param_campo  ":"")+//F001-2016
					
					((sNombreProducto.equals("1A")||sNombreProducto.equals("T"))?", MI.cg_descprod1 ":"")+
					((sNombreProducto.equals("1B")||sNombreProducto.equals("T"))?", MI.cg_descprod2 ":"")+
					((sNombreProducto.equals("1C")||sNombreProducto.equals("T"))?", MI.cg_descprod3 ":"")+
					((sNombreProducto.equals("1D")||sNombreProducto.equals("T"))?", MI.cg_descprod4 ":"")+
					((sNombreProducto.equals("2A")||sNombreProducto.equals("T"))?", MI.cg_descprod5 ":"")+
					((sNombreProducto.equals("0")||sNombreProducto.equals("T"))?", MI.cg_descprod6 ":"")+
					((sNombreProducto.equals("5A")||sNombreProducto.equals("T"))?", MI.cg_descprod7 ":"")+
					((sNombreProducto.equals("4A")||sNombreProducto.equals("T"))?", MI.cg_descprod8 " :"")+
					((sNombreProducto.equals("4B")||sNombreProducto.equals("T"))?", MI.cg_descprod9 " :"")+
					((sNombreProducto.equals("8")||sNombreProducto.equals("T"))?", MI.cg_descprod10 " :"")+
					" from " +
					" (" +
					"     select ic_parametro, cc_api,IG_PARAM_CAMPO," +
					"     max(decode(cc_producto, '1A', cg_descripcion, null)) as cg_descprod1," +
					"     max(decode(cc_producto, '1B', cg_descripcion, null)) as cg_descprod2," +
					"     max(decode(cc_producto, '1C', cg_descripcion, null)) as cg_descprod3," +
					"     max(decode(cc_producto, '1D', cg_descripcion, null)) as cg_descprod4," +
					"     max(decode(cc_producto, '2A', cg_descripcion, null)) as cg_descprod5," +
					"     max(decode(cc_producto, '0', cg_descripcion, null)) as cg_descprod6," +
					"     max(decode(cc_producto, '5A', cg_descripcion, null)) as cg_descprod7," +
					"     max(decode(cc_producto, '4A', cg_descripcion, null)) as cg_descprod8," +
					"     max(decode(cc_producto, '4B', cg_descripcion, null)) as cg_descprod9, " +
					"     max(decode(cc_producto, '8',  cg_descripcion, null)) as cg_descprod10 " +
					"     from com_matriz_interface" +
					"     where ic_parametro is not null" +
					((!sTipoInterfaz.equals("T"))?"     and cc_api = '"+sTipoInterfaz+"'":"") +
					((!sNombreProducto.equals("T"))?"     and cc_producto = '"+sNombreProducto+"'":"") +
					"     group by ic_parametro, cc_api, ig_param_campo " +
					"     order by cc_api, ic_parametro" +
					" ) MI, comrel_parametro_api RPA, comcat_api CA" +
					" where RPA.ic_parametro = MI.ic_parametro" +
					" and RPA.cc_api = MI.cc_api" +
					" and RPA.cc_api = CA.cc_api" +
					//" and MI.ic_parametro in (8) "+
					" order by CA.ig_orden, RPA.ic_parametro"	);
			
			log.info("qrySentencia  "+qrySentencia);
			log.info("conditions "+conditions);
			log.info("getDocumentQueryFile(S)");
			return qrySentencia.toString();
		}
		
		
		public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rsAPI, String path, String tipo){
			log.debug("crearCustomFile (E)");
			String nombreArchivo = "";
			HttpSession session = request.getSession();	
			CreaArchivo creaArchivo = new CreaArchivo();
			StringBuffer contenidoArchivo = new StringBuffer();
		
			try {
			
				HashMap hmProductos = new HashMap();

			    ParametrosGrales BeanParametrosGrales = ServiceLocator.getInstance().lookup("ParametrosGralesEJB",ParametrosGrales.class);
				
				hmProductos =(HashMap)BeanParametrosGrales.getEncabezadosMatrizApis(); 
		
				StringBuffer sbArchivo = new StringBuffer();
				String nombreAPI = "";
				int iNoReg = 0;
				while(rsAPI.next()) {
					if(!nombreAPI.equals(rsAPI.getString("nombreAPI"))) {
						nombreAPI = rsAPI.getString("nombreAPI");
						
						sbArchivo.append(nombreAPI.toUpperCase()+"\r\n");
						sbArchivo.append("N�mero de Par�metro,Nombre,Nombre del Dato en la Forma");
						if(sNombreProducto.equals("1A") || sNombreProducto.equals("T"))
							sbArchivo.append(","+hmProductos.get("1A").toString());
						if(sNombreProducto.equals("1B") || sNombreProducto.equals("T"))
							sbArchivo.append(","+hmProductos.get("1B").toString());
						if(sNombreProducto.equals("1C") || sNombreProducto.equals("T")){
							sbArchivo.append(","+hmProductos.get("1C").toString());
							sbArchivo.append(","+"Campos Parametrizables por Default");
						}
						if(sNombreProducto.equals("1D") || sNombreProducto.equals("T"))
							sbArchivo.append(","+hmProductos.get("1D").toString());
						if(sNombreProducto.equals("2A") || sNombreProducto.equals("T"))
							sbArchivo.append(","+hmProductos.get("2A").toString());
						if(sNombreProducto.equals("0") || sNombreProducto.equals("T"))
							sbArchivo.append(","+hmProductos.get("0").toString());
						if(sNombreProducto.equals("5A") || sNombreProducto.equals("T"))
							sbArchivo.append(","+hmProductos.get("5A").toString());
						if(sNombreProducto.equals("4A") || sNombreProducto.equals("T"))
							sbArchivo.append(","+hmProductos.get("4A").toString());
						if(sNombreProducto.equals("4B") || sNombreProducto.equals("T"))
							sbArchivo.append(","+hmProductos.get("4B").toString());
						if(sNombreProducto.equals("8") || sNombreProducto.equals("T"))
							sbArchivo.append(","+hmProductos.get("8").toString());
						sbArchivo.append("\r\n");
					}
					sbArchivo.append((rsAPI.getString("numeroParametro")==null?"":rsAPI.getString("numeroParametro"))+",");
					sbArchivo.append((rsAPI.getString("nombreParametro")==null?"":rsAPI.getString("nombreParametro").replace(',',' '))+",");
					sbArchivo.append((rsAPI.getString("descParametro")==null?"":rsAPI.getString("descParametro").replace(',',' ')));
					if(sNombreProducto.equals("1A") || sNombreProducto.equals("T"))
						sbArchivo.append(","+(rsAPI.getString("cg_descprod1")==null?"":getQuitarEtiquetas(rsAPI.getString("cg_descprod1"),"<br>")));
					if(sNombreProducto.equals("1B") || sNombreProducto.equals("T"))
						sbArchivo.append(","+(rsAPI.getString("cg_descprod2")==null?"":getQuitarEtiquetas(rsAPI.getString("cg_descprod2"),"<br>")));
					if(sNombreProducto.equals("1C") || sNombreProducto.equals("T")){
						sbArchivo.append(","+(rsAPI.getString("cg_descprod3")==null?"":getQuitarEtiquetas(rsAPI.getString("cg_descprod3"),"<br>")));
						sbArchivo.append(","+(rsAPI.getString("IG_PARAM_CAMPO")==null?"N/A":getQuitarEtiquetas(rsAPI.getString("IG_PARAM_CAMPO"),"<br>")));
					}
					if(sNombreProducto.equals("1D") || sNombreProducto.equals("T"))
						sbArchivo.append(","+(rsAPI.getString("cg_descprod4")==null?"":getQuitarEtiquetas(rsAPI.getString("cg_descprod4"),"<br>")));
					if(sNombreProducto.equals("2A") || sNombreProducto.equals("T"))
						sbArchivo.append(","+(rsAPI.getString("cg_descprod5")==null?"":getQuitarEtiquetas(rsAPI.getString("cg_descprod5"),"<br>")));
					if(sNombreProducto.equals("0") || sNombreProducto.equals("T"))
						sbArchivo.append(","+(rsAPI.getString("cg_descprod6")==null?"":getQuitarEtiquetas(rsAPI.getString("cg_descprod6"),"<br>")));
					if(sNombreProducto.equals("5A") || sNombreProducto.equals("T"))
						sbArchivo.append(","+(rsAPI.getString("cg_descprod7")==null?"":getQuitarEtiquetas(rsAPI.getString("cg_descprod7"),"<br>")));
					if(sNombreProducto.equals("4A") || sNombreProducto.equals("T"))
						sbArchivo.append(","+(rsAPI.getString("cg_descprod8")==null?"":getQuitarEtiquetas(rsAPI.getString("cg_descprod8"),"<br>")));
					if(sNombreProducto.equals("4B") || sNombreProducto.equals("T"))
						sbArchivo.append(","+(rsAPI.getString("cg_descprod9")==null?"":getQuitarEtiquetas(rsAPI.getString("cg_descprod9"),"<br>")));
					if(sNombreProducto.equals("8") || sNombreProducto.equals("T"))
						sbArchivo.append(","+(rsAPI.getString("cg_descprod10")==null?"":getQuitarEtiquetas(rsAPI.getString("cg_descprod10"),"<br>")));
					sbArchivo.append("\r\n");
				} 
				CreaArchivo archivo = new CreaArchivo();
				if (archivo.make(sbArchivo.toString(), path, ".csv"))
					nombreArchivo = archivo.getNombre();
			} catch(Exception e) { 
				log.error("Error en la generacion del Archivo CSV: " + e);
			}	
			return nombreArchivo;		 
		}
			
		
		private String getQuitarEtiquetas(String sCadenaConFormato, String sEtiqueta) {
			String sCadena = sCadenaConFormato.replace(',',' ');
			int iIndice = sCadena.indexOf(sEtiqueta);
			while(iIndice!=-1) {
				sCadena = sCadena.substring(0, iIndice).concat(" ").concat(sCadena.substring(iIndice+sEtiqueta.length(), sCadena.length()));
				iIndice = sCadena.indexOf(sEtiqueta);
			}
			return sCadena;
	}
		 
			
		
		public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo){
			String nombreArchivo = "";
			HttpSession session = request.getSession();	
		
			CreaArchivo creaArchivo = new CreaArchivo();
			StringBuffer contenidoArchivo = new StringBuffer();
		
			try {
		
			}catch(Throwable e) {
				throw new AppException("Error al generar el archivo ", e);
			} finally {
			try {
				
			} catch(Exception e) {}
		  
			}
				return nombreArchivo;
		}
		

		/************ SETTERS ***************/

		public void setsTipoInterfaz(String sTipoInterfaz){
			this.sTipoInterfaz = sTipoInterfaz;
		}
	
		public void setsNombreProducto(String sNombreProducto){
			this.sNombreProducto = sNombreProducto;
		}	
	
}