package com.netro.cadenas;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

public class BusquedaPymeGral  {

	private String ic_epo;
	private String ic_pyme;
	private String cg_pyme;
	private String nombre_pyme;
	private String rfc_pyme;
	private String num_pyme;
	private String num_sirac_pyme;
	
	private String producto;
	private String login;
	private String facultad;
	private String cveFacultad;

	public BusquedaPymeGral() {}
	
	public List getProveedores(String ic_epo, String num_pyme, String rfc_pyme, String nombre_pyme,
					  String ic_pyme,String producto,String num_sirac_pyme, String login) throws AppException {
		
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String strSQL = "";
		List lstPymesGral = new ArrayList();
		
		try{
			con.conexionDB();
			String hint = "";
			StringBuffer condicion = new StringBuffer();
			
			
			if("4".equals(producto)){
				if(!"".equals(num_pyme)) {
					condicion.append("	AND crn.ic_nafin_electronico = ? ");
					hint = " /*+ leading (crn) use_nl(crn p pe pep d) */ ";
				}
				if(!"".equals(rfc_pyme)) {
					
					if (rfc_pyme.indexOf('*')==0) {	//Se elimina el primer * para permitir que se sue el indice sobre cg_rfc
						rfc_pyme = rfc_pyme.substring(1);
					}
					condicion.append(" AND cg_rfc LIKE nvl(replace(upper(?), '*', '%'), '%') ");
					hint = " /*+ leading(p) use_nl(p pep pe d crn) index(p IN_COMCAT_PYME_11_NUK) */ ";
				}
				if(!"".equals(nombre_pyme)) {
					condicion.append("	AND cg_razon_social LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')");
					hint = " /*+ leading (p) index (p in_comcat_pyme_13_ctxcat) use_nl(p pe pep d crn) */ ";
				}
				
				if(!"".equals(num_sirac_pyme)) {
					condicion.append(" AND p.in_numero_sirac = ? ");
				}
	
				
				strSQL =
					" SELECT   " +  hint +
					"           p.ic_pyme,"   +
					"           pe.cg_pyme_epo_interno, p.cg_razon_social,"   +
					"           crn.ic_nafin_electronico || ' ' || p.cg_razon_social despliega,"   +
					"           crn.ic_nafin_electronico"   +
					"     FROM comrel_pyme_epo pe"   +
					" 		 , dis_documento d"   +
					" 		 , comrel_nafin crn"   +
					" 		 , comrel_pyme_epo_x_producto pep"   +
					" 		 , comcat_pyme p		 "   +
					"    WHERE p.ic_pyme = pe.ic_pyme"   +
					"    AND d.ic_pyme = pe.ic_pyme"   +
					"    AND d.ic_epo = pe.ic_epo"   +
					"    AND p.ic_pyme = crn.ic_epo_pyme_if"   +
					" 	 AND pe.ic_epo = pep.ic_epo"   +
					" 	 AND pe.ic_pyme = pep.ic_pyme"   +
					" 	 AND pep.ic_producto_nafin = 4"   +
					"    AND crn.cg_tipo = 'P'"   +
					"    AND pe.cs_habilitado = 'S'"   +
					"    AND pe.cg_num_distribuidor IS NOT NULL "   +
					"    AND p.cs_habilitado = 'S'"   +
						condicion+" "+
					" GROUP BY p.ic_pyme,"   +
					"          pe.cg_pyme_epo_interno,"   +
					"          p.cg_razon_social,"   +
					"           RPAD (pe.cg_pyme_epo_interno, 10, ' ') || p.cg_razon_social,"   +
					"          crn.ic_nafin_electronico"   +
					" ORDER BY p.cg_razon_social";
				System.out.println("\n qrySentencia: "+strSQL);
				ps = con.queryPrecompilado(strSQL);
				int cont = 0;
				if(!"".equals(num_pyme)) {
					cont++;ps.setInt(cont,Integer.parseInt(num_pyme));
				}
				if(!"".equals(rfc_pyme)) {
					cont++;ps.setString(cont,rfc_pyme);
				}
				if(!"".equals(nombre_pyme)) {
					cont++;ps.setString(cont,nombre_pyme);
				}
				if(!"".equals(num_sirac_pyme)){
					cont++;ps.setInt(cont,Integer.parseInt(num_sirac_pyme));
				}

			}
			
			
			if("1".equals(producto)){	//Descuento Electrónico

				boolean consideraUsuario = false;
				String claveAfiliado = null;
				String tipoAfiliado = null;
	
				if(login != null && !login.equals("")) {
	
					UtilUsr utilUsr = new UtilUsr();
					Usuario usr = utilUsr.getUsuario(login);
					claveAfiliado = usr.getClaveAfiliado();
					tipoAfiliado = usr.getTipoAfiliado();
	
					if (tipoAfiliado != null && tipoAfiliado.equals("P") &&
							claveAfiliado != null && !claveAfiliado.equals("")) {
						condicion.append("	AND p.ic_pyme = ? ");
						consideraUsuario = true;
					} else {
						return lstPymesGral;
					}
				}
	
	
				if(!"".equals(num_pyme))
					condicion.append("	AND crn.ic_nafin_electronico = ? ");
				if(!"".equals(rfc_pyme)) {
					//condicion += "	AND UPPER(cg_rfc) LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')";
					if (rfc_pyme.indexOf('*')==0) {	//Se elimina el primer * para permitir que se use el indice sobre cg_rfc
						rfc_pyme = rfc_pyme.substring(1);
					}
					condicion.append(" AND cg_rfc LIKE nvl(replace(upper(?), '*', '%'), '%') ");
					hint =  "/*+  use_nl(p pe crn) index(p IN_COMCAT_PYME_11_NUK) */";
				}
				
				if(!"".equals(nombre_pyme)) {
					condicion.append("	AND cg_razon_social LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')");
					//condicion.append(" AND CATSEARCH(cg_razon_social, ?, NULL) > 0 ");
					hint = " /*+ index (p in_comcat_pyme_13_ctxcat) */ ";
				}
					
				if(!"".equals(num_sirac_pyme))
					condicion.append(" AND p.in_numero_sirac = ? ");
	
				//Cuando la busqueda sea por N@E o RFC se omite la tabla com_documento
				boolean consideraComDocumento;
				if (!"".equals(num_pyme) || !"".equals(rfc_pyme)) {
					consideraComDocumento = false;
				} else {
					consideraComDocumento = true;
				}
				strSQL =
					" SELECT   " +
					//((consideraComDocumento)?" /*+ use_nl(p pe crn d)*/":hint) +
					((consideraComDocumento)?" /*+ leading(crn) */ ":hint) +
					"           distinct p.ic_pyme"   +
					"           ,'' as cg_pyme_epo_interno"+
					"           , p.cg_razon_social"   +
					"           ,crn.ic_nafin_electronico || ' ' || p.cg_razon_social despliega "   +
					"           ,crn.ic_nafin_electronico"   +
					"			,'BusquedaPymeGral::getProveedores()' "+
					"     FROM comcat_pyme p "   +
					((consideraComDocumento)?", com_documento d ":"") +
					"          , comrel_pyme_epo pe "   +
					"          , comrel_nafin crn "   +
					"    WHERE p.ic_pyme = pe.ic_pyme"   +
					((consideraComDocumento)?" AND d.ic_pyme = pe.ic_pyme AND d.ic_epo = pe.ic_epo ":"") +
					"    AND pe.ic_pyme = crn.ic_epo_pyme_if"   +
					"    AND crn.cg_tipo = 'P'"   +
					"    AND pe.cs_habilitado = 'S'"   +
					"    AND pe.cg_pyme_epo_interno IS NOT NULL "   +
					"    AND p.cs_habilitado = 'S'"   +
					condicion+" "+
					" ORDER BY p.cg_razon_social";
				System.out.println("\n qrySentencia: "+strSQL);
				ps = con.queryPrecompilado(strSQL);
				int cont = 0;
	
				if(consideraUsuario) {
					cont++;ps.setString(cont, claveAfiliado);
				}
				if(!"".equals(num_pyme)) {
					cont++;ps.setInt(cont,Integer.parseInt(num_pyme));
				}
				if(!"".equals(rfc_pyme)) {
					cont++;ps.setString(cont,rfc_pyme);
				}
				if(!"".equals(nombre_pyme)) {
					cont++;ps.setString(cont,nombre_pyme);
				}
				if(!"".equals(num_sirac_pyme)){
					cont++;ps.setInt(cont,Integer.parseInt(num_sirac_pyme));
				}
			}
			
			rs = ps.executeQuery();
			int cont = 0;
			while(rs.next() && cont < 10000) {
				HashMap hm = new HashMap();
				hm.put("clave",       rs.getString(1));
				hm.put("cveEpoPyme",  rs.getString(2));
				hm.put("nombrePyme",  rs.getString(3));
				hm.put("descripcion", rs.getString(4));
				hm.put("nafinElec",   rs.getString(5));
				lstPymesGral.add(hm);
				cont++;
			}

			rs.close();
			if(ps!=null) ps.close();
			
			return lstPymesGral;
		}catch(Throwable t){
			
			throw new AppException("Error al buscar pymes...", t);
			
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	
	}
	
	
	public List getDomicilioPyme(String cvePyme) throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String strSQL = "";
		List lstDomicilio = new ArrayList();
		try{
			con.conexionDB();
			
			strSQL = " SELECT P.cg_rfc as rfcPyme, D.cg_calle||' '||D.cg_numero_ext||' '||D.cg_numero_int as domicilio, "+
						" D.cg_colonia as colonia, D.cn_cp as codigoPostal, D.cg_telefono1 as telefono1 "+
						" FROM comcat_pyme P, com_domicilio D "+
						" WHERE P.ic_pyme = D.ic_pyme "+
						" AND P.ic_pyme = ? ";
			
			ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, Integer.parseInt(cvePyme));
			rs = ps.executeQuery();
			
			if(rs.next()) {
				HashMap hm = new HashMap();
				hm.put("RFC",(rs.getString("rfcPyme")==null?"":rs.getString("rfcPyme")));
				hm.put("CALLE",(rs.getString("domicilio")==null?"":rs.getString("domicilio")));
				hm.put("COLONIA",(rs.getString("colonia")==null?"":rs.getString("colonia")));
				hm.put("CP",(rs.getString("codigoPostal")==null?"":rs.getString("codigoPostal")));
				hm.put("TELEFONO",(rs.getString("telefono1")==null?"":rs.getString("telefono1")));
				lstDomicilio.add(hm);
			}
			rs.close();
			if(ps!=null) ps.close();
		
			return lstDomicilio;
			
		}catch(Throwable t){
			throw new AppException("Error al consultar domicilio pyme. ", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		
	}
	
	public List getResumenDoctos(String cvePyme, String sTipoDscto) throws AppException{
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String strSQL = "";
		List lstDoctos = new ArrayList();
		List lstResDocto = new ArrayList();
		try{
			con.conexionDB();
			
			strSQL = " select E.cg_razon_social as nombreCadena, "+
							" TF.CG_NOMBRE as tipoFactoraje, "+
							" COUNT(CASE WHEN (D.ic_moneda = 1) THEN D.ig_numero_docto END) AS numDoctosMN, "+
							" SUM(CASE WHEN (D.ic_moneda = 1) THEN D.fn_monto ELSE 0 END) AS totDoctosMN, "+
							" COUNT(CASE WHEN (D.ic_moneda = 54) THEN D.ig_numero_docto END) AS numDoctosDA, "+
							" SUM(CASE WHEN (D.ic_moneda = 54) THEN D.fn_monto ELSE 0 END) AS totDoctosDA, "+
							" D.ic_epo as noEPO, "+
							" D.CS_DSCTO_ESPECIAL  "+							
							" from comcat_epo E, com_documento D, comcat_tipo_factoraje TF "+
							" where D.ic_epo = E.ic_epo "+
							" and D.cs_dscto_especial = TF.CC_TIPO_FACTORAJE " +
							" and D.cs_dscto_especial in ("+sTipoDscto+") "+
							" and D.ic_estatus_docto = 2 "+
							" and D.ic_pyme = ? "+
							" group by E.cg_razon_social, TF.CG_NOMBRE, D.ic_epo, D.CS_DSCTO_ESPECIAL "+
							" order by E.cg_razon_social ";
		
		System.out.println("  strSQL  "+strSQL);	
		System.out.println("  cvePyme  "+cvePyme);	
		
			ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, Integer.parseInt(cvePyme));
			rs = ps.executeQuery();
			
			while(rs.next()) {
				HashMap hm = new HashMap();
				
				hm.put("NOMBREEPO",(rs.getString("nombreCadena")==null?"":rs.getString("nombreCadena")));
				hm.put("TIPOFACTORAJE",(rs.getString("tipoFactoraje")==null?"":rs.getString("tipoFactoraje")));
				hm.put("NUMDOCTOMN",(rs.getString("numDoctosMN")==null?"0":rs.getString("numDoctosMN")));
				hm.put("MONTOMN",(rs.getString("totDoctosMN")==null?"0":rs.getString("totDoctosMN")));
				hm.put("NUMDOCTODA",(rs.getString("numDoctosDA")==null?"0":rs.getString("numDoctosDA")));
				hm.put("MONTODA",(rs.getString("totDoctosDA")==null?"0":rs.getString("totDoctosDA")));
				hm.put("CVEEPO",(rs.getString("noEPO")==null?"":rs.getString("noEPO")));
				hm.put("DESCESPECIAL",(rs.getString("CS_DSCTO_ESPECIAL")==null?"":rs.getString("CS_DSCTO_ESPECIAL")));				
				lstDoctos.add(hm);
			}
			rs.close();
			if(ps!=null) ps.close();
			
			Hashtable hMonedas = getMonedas();
			String sNombreEpo = "", sTipoFactoraje = "",sTipoFactorajeDesc = "", sNoDoctosMN = "", 
									  sTotalDoctosMN = "", sNoDoctosDL = "", sTotalDoctosDL = "", sNoEPO = "",
									  linkMn = "", linkDl = "";
			for(int i=0; i<lstDoctos.size(); i++) {
				HashMap hmDocto = (HashMap)lstDoctos.get(i);
				sNombreEpo = (String)hmDocto.get("NOMBREEPO");
				sTipoFactorajeDesc = (String)hmDocto.get("TIPOFACTORAJE");
				sTipoFactoraje = (String)hmDocto.get("DESCESPECIAL");
				sNoDoctosMN = (String)hmDocto.get("NUMDOCTOMN");
				sTotalDoctosMN = (String)hmDocto.get("MONTOMN");
				sNoDoctosDL = (String)hmDocto.get("NUMDOCTODA");
				sTotalDoctosDL = (String)hmDocto.get("MONTODA");
				sNoEPO = (String)hmDocto.get("CVEEPO");
				
				String sDestinoSeleccionDoctos = "";
				if(sTipoFactoraje.equals("N")) sDestinoSeleccionDoctos = "13forma1ext.jsp?tipoFact="+sTipoFactoraje+"&cvePyme="+cvePyme+"&envia=1&cboEpo="+java.net.URLEncoder.encode(sNoEPO+"|"+sNombreEpo, "UTF-8")+"&Vez=1";
				if(sTipoFactoraje.equals("V")) sDestinoSeleccionDoctos = "13forma2ext.jsp?tipoFact="+sTipoFactoraje+"&cvePyme="+cvePyme+"&envia=1&cboEpo="+java.net.URLEncoder.encode(sNoEPO+"|"+sNombreEpo, "UTF-8");
				if(sTipoFactoraje.equals("D")) sDestinoSeleccionDoctos = "13formapd4ext.jsp?tipoFact="+sTipoFactoraje+"&cvePyme="+cvePyme+"&envia=1&cboEpo="+java.net.URLEncoder.encode(sNoEPO+"|"+sNombreEpo, "UTF-8");
				if(sTipoFactoraje.equals("M")) sDestinoSeleccionDoctos = "13forma9ext.jsp?tipoFact="+sTipoFactoraje+"&cvePyme="+cvePyme+"&envia=1&cboEpo="+java.net.URLEncoder.encode(sNoEPO+"|"+sNombreEpo, "UTF-8");
				if(sTipoFactoraje.equals("I")) sDestinoSeleccionDoctos = "13forma10ext.jsp?envia=1&cvePyme="+cvePyme+"&cboEpo="+java.net.URLEncoder.encode(sNoEPO+"|"+sNombreEpo, "UTF-8");
				
				if(!sNoDoctosMN.equals("0")) {
					linkMn = sDestinoSeleccionDoctos+"&cboMoneda=" + java.net.URLEncoder.encode("1|"+hMonedas.get("1"), "UTF-8");
				}else{ linkMn = ""; }
				
				if(!sNoDoctosDL.equals("0")) {
					linkDl = sDestinoSeleccionDoctos+"&cboMoneda=" + java.net.URLEncoder.encode("54|"+hMonedas.get("54"), "UTF-8");
				}else{ linkDl=""; }
				
				HashMap hmFinal = new HashMap();
				
				hmFinal.put("NOMBREEPO", sNombreEpo);
				hmFinal.put("TIPOFACTORAJE",sTipoFactorajeDesc);
				hmFinal.put("NUMDOCTOMN",sNoDoctosMN);
				hmFinal.put("MONTOMN",sTotalDoctosMN);
				hmFinal.put("NUMDOCTODA",sNoDoctosDL);
				hmFinal.put("MONTODA",sTotalDoctosDL);
				hmFinal.put("CVEEPO",sNoEPO);
				hmFinal.put("DESCESPECIAL",sTipoFactoraje);
				hmFinal.put("LINKMN",linkMn);
				hmFinal.put("LINKDL",linkDl);
				
				lstResDocto.add(hmFinal);
			}	
			
			return lstResDocto;
				
		}catch(Throwable t){
			throw new AppException("Error al obtener documentos. ", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}
	
	public Hashtable getMonedas(){
		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String strSQL = "";
		Hashtable hMonedas = new Hashtable();
		try{
			con.conexionDB();
			
			strSQL = " select ic_moneda, cd_nombre from comcat_moneda "+
							" where ic_moneda in (1, 54) "+
							" order by ic_moneda ";
							
			ps = con.queryPrecompilado(strSQL);
			rs = ps.executeQuery();
			
			while(rs.next()) {
				hMonedas.put(rs.getString("ic_moneda"), rs.getString("cd_nombre"));
			}
			rs.close();
			if(ps!=null) ps.close();
		
			return hMonedas;
			
		}catch(Throwable t){
			throw new AppException("Error al obtener monedas. ", t);
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
	}


	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}


	public String getIc_epo() {
		return ic_epo;
	}


	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}


	public String getIc_pyme() {
		return ic_pyme;
	}


	public void setCg_pyme(String cg_pyme) {
		this.cg_pyme = cg_pyme;
	}


	public String getCg_pyme() {
		return cg_pyme;
	}


	public void setNombre_pyme(String nombre_pyme) {
		this.nombre_pyme = nombre_pyme;
	}


	public String getNombre_pyme() {
		return nombre_pyme;
	}


	public void setRfc_pyme(String rfc_pyme) {
		this.rfc_pyme = rfc_pyme;
	}


	public String getRfc_pyme() {
		return rfc_pyme;
	}


	public void setNum_pyme(String num_pyme) {
		this.num_pyme = num_pyme;
	}


	public String getNum_pyme() {
		return num_pyme;
	}


	public void setNum_sirac_pyme(String num_sirac_pyme) {
		this.num_sirac_pyme = num_sirac_pyme;
	}


	public String getNum_sirac_pyme() {
		return num_sirac_pyme;
	}


	public void setProducto(String producto) {
		this.producto = producto;
	}


	public String getProducto() {
		return producto;
	}


	public void setLogin(String login) {
		this.login = login;
	}


	public String getLogin() {
		return login;
	}

}
