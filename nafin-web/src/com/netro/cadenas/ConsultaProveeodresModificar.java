package com.netro.cadenas;

import java.sql.ResultSet;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/******************************************************************************************
 * Fodea 11-2014																									*
 * Descripci�n: Migraci�n de la pantalla Administraci�n-Afiliados-Consultas-Proveedores	*
 * 				 a ExtJS. Los m�todos tienen consultas para validar el proveedor y 			*
 *              no_sirac antes de actualizar los datos.												*
 * Elabor�:     Agust�n Bautista Ruiz																		*
 * Fecha:       08/07/2014																						*
 ******************************************************************************************/
public class ConsultaProveeodresModificar  {

	private static final Log log = ServiceLocator.getInstance().getLog(ConsultaProveeodresModificar.class);
	private int icEpo;
	private int icPyme;
	private int numeroConsulta;	
	private int icProductoNafin;
	private String numeroCliente;
	private String numeroSirac;
	// Las siguientes variables son para obtener el nombre del Municipio
	private int icEstado;
	private int icPais;
	private int icMunicipio;
	
	
	

	public ConsultaProveeodresModificar() {}

	/**
	 * Obtiene el nombre de la EPO seleccionada en el combo
	 * @return 
	 */
	public String consultaNombreEpo(){
	
		String cadena = "";
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		StringBuffer sentencia = new StringBuffer();
		log.info("consultaNombreEpo (E)");
		try{
			con.conexionDB();  
			sentencia.append( 
					"SELECT CG_RAZON_SOCIAL FROM COMCAT_EPO WHERE IC_EPO = " + this.icEpo
				);			
			
			rs = con.queryDB(sentencia.toString());
			if(rs.next()) {
				cadena = rs.getString("CG_RAZON_SOCIAL");
			}
			rs.close();
			con.cierraStatement();
	
		} catch (Exception e) {
			log.warn("Error en consultaNombreEpo: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("Sentencia: " + sentencia.toString() );
		log.info("consultaNombreEpo (S)");
		return cadena;
	}
	
	/**
	 * Obtiene el nombre del municipio seleccionado en el combo
	 * @return 
	 */
	public String consultaNombreMunicipio(){
	
		String cadena = "";
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		StringBuffer sentencia = new StringBuffer();
		log.info("consultaNombreMunicipio (E)");
		try{
			con.conexionDB();  
			sentencia.append( 
					"SELECT CD_NOMBRE " +
					"FROM   COMCAT_MUNICIPIO " + 
					"WHERE  IC_PAIS = "+ this.icPais + 
					"       AND IC_ESTADO = " + this.icEstado +
					"       AND IC_MUNICIPIO =  " + this.icMunicipio
				);			
			
			rs = con.queryDB(sentencia.toString());
			if(rs.next()) {
				cadena = rs.getString("CD_NOMBRE");
			}
			rs.close();
			con.cierraStatement();
	
		} catch (Exception e) {
			log.warn("Error en consultaNombreMunicipio: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("Sentencia: " + sentencia.toString() );
		log.info("consultaNombreMunicipio (S)");
		return cadena;
	}
	
	/**
	 * Obtiene la consulta para los campos susceptible a anticipo y susceptible a  credicadenas.
	 * Si IC_PRODUCTO_NAFIN = 2 => anticipo
	 * Si IC_PRODUCTO_NAFIN = 5 => credicadenas
	 * @return strHab
	 */
	public String consultaStrHab(){
	
		String strHab = "";
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		StringBuffer sentencia = new StringBuffer();
		log.info("consultaStrHab (E)");
		try{
			con.conexionDB();  
			sentencia.append( 
					"SELECT CS_HABILITADO " +
					"FROM   COMREL_PYME_EPO_X_PRODUCTO " + 
					"WHERE  IC_PRODUCTO_NAFIN = "+ this.icProductoNafin + 
					"       AND IC_PYME = " + this.icPyme +
					"       AND IC_EPO =  " + this.icEpo
				);			
			
			rs = con.queryDB(sentencia.toString());
			if(rs.next()) {
				strHab = rs.getString("CS_HABILITADO");
			}
			rs.close();
			con.cierraStatement();
	
		} catch (Exception e) {
			log.warn("Error en consultaStrHab: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("Sentencia: " + sentencia.toString() );
		log.info("consultaStrHab (S)");
		return strHab;
	}	
	
	/**
	 * Busca si existe un n�mero de proveedor
	 * @return existe
	 */
	public int consultaNumeroProveedor(){
	
		int existe = 0;
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		StringBuffer sentencia = new StringBuffer();
		log.info("consultaNumeroProveedor (E)");
		try{
			con.conexionDB();  
			if( this.numeroConsulta == 1 ){
				sentencia.append( 
					"SELECT COUNT(*) AS TOTAL " +
					"FROM COMREL_PYME_EPO " +
					"WHERE IC_EPO = " + this.icEpo +
					" AND CG_PYME_EPO_INTERNO = '" + this.numeroCliente + "' " +
					" AND IC_PYME = " + this.icPyme  
				);			
			} else if( this.numeroConsulta == 2 ){
				sentencia.append( 
					"SELECT COUNT(*) AS TOTAL " +
					"FROM COMREL_PYME_EPO " + 
					"WHERE IC_EPO = " + this.icEpo +
					" AND CG_PYME_EPO_INTERNO = '" + this.numeroCliente + "' " +
					" AND IC_PYME <> " + this.icPyme  
				);			
			}
			
			rs = con.queryDB(sentencia.toString());
			if(rs.next()) {
				existe = rs.getInt("TOTAL");
			}
			rs.close();
			con.cierraStatement();
	
		} catch (Exception e) {
			log.warn("Error en consultaNumeroProveedor: " + e);
			log.warn("numeroConsulta: " + this.numeroConsulta );
			log.warn("icEpo: " + this.icEpo );
			log.warn("numeroCliente: " + this.numeroCliente );
			log.warn("icPyme: " + this.icPyme );
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 	
		log.info("numeroConsulta: " + this.numeroConsulta );
		log.info("Sentencia: " + sentencia.toString() );
		log.info("consultaNumeroProveedor (S)");
		return existe;
	}

	/**
	 * Obtiene el numero de proveedor a partir del numero_sirac
	 * @return existe
	 */
	public int consultaNumeroSirac(){
	
		int existe = 0;
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		StringBuffer sentencia = new StringBuffer();
		log.info("consultaNumeroSirac (E)");
		try{
			con.conexionDB();  
			sentencia.append( 
				"SELECT COUNT(*) AS TOTAL " +
				"FROM COMCAT_PYME " + 
				"WHERE IN_NUMERO_SIRAC = '" + this.numeroSirac + "' " +
				"AND IC_PYME <> " + this.icPyme 
			);
			rs = con.queryDB(sentencia.toString());
			if(rs.next()) {
				existe = rs.getInt("TOTAL");
			}
			rs.close();
			con.cierraStatement();
	
		} catch (Exception e) {
			log.warn("Error en consultaNumeroSirac: " + e);
			log.warn("numeroSirac: " + this.numeroSirac );
			log.warn("icPyme: " + this.icPyme );
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("Sentencia: " + sentencia.toString() );
		log.info("consultaNumeroSirac (S)");
		return existe;
	}
	
	/**
	 * Obtiene el ic_nafin_electronico
	 * @return 
	 */
	public String consultaNafinElectronico(){
	
		String nafinElectronico = "";
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		StringBuffer sentencia = new StringBuffer();
		log.info("consultaNafinElectronico (E)");
		try{
			con.conexionDB();  
			sentencia.append(	"SELECT IC_NAFIN_ELECTRONICO FROM COMREL_NAFIN WHERE IC_EPO_PYME_IF = " + this.icPyme + " AND CG_TIPO = 'P' "	);
			rs = con.queryDB(sentencia.toString());
			if(rs.next()) {
				nafinElectronico = rs.getString("IC_NAFIN_ELECTRONICO");
			}
			rs.close();
			con.cierraStatement();
	
		} catch (Exception e) {
			log.warn("Error en consultaNafinElectronico: " + e);
			log.warn("icPyme: " + this.icPyme );
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		log.info("Sentencia: " + sentencia.toString() );
		log.info("consultaNafinElectronico (S)");
		return nafinElectronico;
	}
		
	/**
	 * Obtiene el email registrado
	 * @return 
	 */
	public String consultaEmail(){
	
		String email = "";
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		StringBuffer sentencia = new StringBuffer();
		log.info("consultaEmail (E)");
		try{
			con.conexionDB();  
			sentencia.append(	"SELECT CG_EMAIL FROM COM_CONTACTO WHERE IC_PYME = " + this.icPyme + " AND CS_PRIMER_CONTACTO = 'S' "	);
			rs = con.queryDB(sentencia.toString());
			if(rs.next()) {
				email = rs.getString("CG_EMAIL");
			}
			rs.close();
			con.cierraStatement();
	
		} catch (Exception e) {
			log.warn("Error en consultaEmail: " + e);
			log.warn("icPyme: " + this.icPyme );
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		log.info("Sentencia: " + sentencia.toString() );
		log.info("consultaEmail (S)");
		return email;
	}
	
	/**
	 *Datos que regresa el metodoto
	 * H  = Habilitado
	 * R = Afiliado
	 * N  = No Suceptible
	 * S = Suceptible
	 * @return 
	 * @param ic_epo
	 * @param ic_pyme
	 */
	public String  getstrTAfilia(String ic_pyme, String ic_epo ){
	
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		StringBuffer sentencia = new StringBuffer();
		log.info("getstrTAfilia (E)");
		String strTAfilia ="";
		
		try{
			con.conexionDB();  
			sentencia.append(" SELECT cs_aceptacion  "+
				"FROM comrel_pyme_epo " +
				" WHERE ic_pyme =    "+ic_pyme +
				"AND ic_epo = "+ic_epo);
			
			log.error("sentencia =====================" + sentencia);
				
			rs = con.queryDB(sentencia.toString());
			if(rs.next()) {
				strTAfilia = rs.getString("cs_aceptacion");
			}
			rs.close();
			
			con.cierraStatement();
	
		} catch (Exception e) {
			log.error("Error en getstrTAfilia: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		
		log.info("getstrTAfilia (S)");
		return strTAfilia;
	}
	
/************************************************************************
 *									GETTERS AND SETTERS									*
 ************************************************************************/
	public int getIcEpo() {
		return icEpo;
	}

	public void setIcEpo(int icEpo) {
		this.icEpo = icEpo;
	}

	public int getIcPyme() {
		return icPyme;
	}

	public void setIcPyme(int icPyme) {
		this.icPyme = icPyme;
	}

	public String getNumeroCliente() {
		return numeroCliente;
	}

	public void setNumeroCliente(String numeroCliente) {
		this.numeroCliente = numeroCliente;
	}

	public int getNumeroConsulta() {
		return numeroConsulta;
	}

	public void setNumeroConsulta(int numeroConsulta) {
		this.numeroConsulta = numeroConsulta;
	}

	public String getNumeroSirac() {
		return numeroSirac;
	}

	public void setNumeroSirac(String numeroSirac) {
		this.numeroSirac = numeroSirac;
	}

	public int getIcProductoNafin() {
		return icProductoNafin;
	}

	public void setIcProductoNafin(int icProductoNafin) {
		this.icProductoNafin = icProductoNafin;
	}

	public int getIcEstado() {
		return icEstado;
	}

	public void setIcEstado(int icEstado) {
		this.icEstado = icEstado;
	}

	public int getIcPais() {
		return icPais;
	}

	public void setIcPais(int icPais) {
		this.icPais = icPais;
	}

	public int getIcMunicipio() {
		return icMunicipio;
	}

	public void setIcMunicipio(int icMunicipio) {
		this.icMunicipio = icMunicipio;
	}
	
}