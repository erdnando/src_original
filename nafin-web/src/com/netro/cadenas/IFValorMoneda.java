package com.netro.cadenas;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class IFValorMoneda implements IQueryGeneratorRegExtJS {
	//Variables
	StringBuffer 		qrySentencia;
	private List 		conditions;
	private String moneda;
	private String fechaIni;
	private String fechaFin;


	public IFValorMoneda() {
	}
	
	public  String ObtenerMonedas(){
			String sentencia = "SELECT TC.fn_valor_compra as VALOR"+
			" , TO_CHAR(TC.dc_fecha,'DD/MM/YYYY') as FECHA "+
			" FROM comcat_moneda M, com_tipo_cambio TC "+
			" WHERE M.IC_MONEDA = TC.IC_MONEDA AND TC.dc_fecha "+
			" BETWEEN TO_Date('"+getFechaIni()+"','DD/MM/YYYY')"+
			" AND TO_Date('"+getFechaFin()+"','DD/MM/YYYY') "+
			" AND TC.ic_moneda = "+getMoneda()+" ORDER BY TC.dc_fecha";
			return sentencia;
	}
	
	
	public Registros executeQuery(){
		AccesoDB conn = new AccesoDB();
		Registros registros = new Registros();
		conditions = new ArrayList();
		try{
			conn.conexionDB();
			registros = conn.consultarDB(this.ObtenerMonedas());
			return registros;
		} catch(Exception e) {
			throw new AppException("IFValorMoneda::ExecuteQuery(Exception) ", e);
		} finally {
			if (conn.hayConexionAbierta()) {
				conn.terminaTransaccion(true); //Para consultas que hagan uso de tablas remotas via dblink
				conn.cierraConexionDB();
			}
		}
	}
	
	
	
	
	private static final Log log = ServiceLocator.getInstance().getLog(IFValorMoneda.class);
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		return "";
	}
	public String getDocumentQuery(){
	return "";
	}
	public String getDocumentQueryFile(){
		
	return "";
	}
	public String getAggregateCalculationQuery() {
	return "";
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds){
	return "";
	}
	
	public List getConditions() {  return conditions;  }  


	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}


	public String getMoneda() {
		return moneda;
	}


	public void setFechaIni(String fechaIni) {
		this.fechaIni = fechaIni;
	}


	public String getFechaIni() {
		return fechaIni;
	}


	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}


	public String getFechaFin() {
		return fechaFin;
	}
	
}