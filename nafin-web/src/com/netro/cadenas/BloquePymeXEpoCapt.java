package com.netro.cadenas;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class BloquePymeXEpoCapt implements IQueryGeneratorRegExtJS {

	private final static Log log = ServiceLocator.getInstance().getLog(BloquePymeXEpoCapt.class);

	private List conditions;
	String num_pyme;
	String rfc_pyme;
	String nombre_pyme;
	String ic_producto_nafin;
	private String ic_epo;
	private String idUsuario;
	
	StringBuffer qrySentencia = new StringBuffer("");
	
		
	public BloquePymeXEpoCapt() {
	}
	public String getDocumentQuery() {
			log.info("getDocumentQuery(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			
			log.info("qrySentencia  "+qrySentencia);
			log.info("conditions "+conditions);
			log.info("getDocumentQuery(S)");
			return qrySentencia.toString();
		}
		
		
		public String getDocumentSummaryQueryForIds(List ids){
			log.info("getDocumentSummaryQueryForIds(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			
			
			log.info("qrySentencia  "+qrySentencia);
			log.info("conditions "+conditions);
			log.info("getDocumentSummaryQueryForIds(S)");
			return qrySentencia.toString();
		}
	
		
		public String getAggregateCalculationQuery(){
			log.info("getAggregateCalculationQuery(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			
			
			log.info("qrySentencia  "+qrySentencia);
			log.info("conditions "+conditions);
			log.info("getAggregateCalculationQuery(S)");
			return qrySentencia.toString();	
		}
		
		public String getDocumentQueryFile(){
			log.info("getDocumentQueryFile(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			
			StringBuffer condicion = new StringBuffer();
			if(!"".equals(num_pyme)) {
				condicion.append("	AND n.ic_nafin_electronico = ? ");
				
			}
			if(!"".equals(rfc_pyme)) {
				condicion.append("	AND UPPER(p.cg_rfc) LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')");
			}
			if(!"".equals(nombre_pyme)) {
				condicion.append("	AND UPPER(p.cg_razon_social) LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')");
			}
			
			if(!"".equals(ic_epo)) {
				condicion.append("	AND ce.ic_epo = ? ");
			}

			if (ic_producto_nafin.equals("1")) { //Descuento
				qrySentencia.append( 
						" SELECT /*+ use_nl ( pe p) */ " +
						" 	DISTINCT p.ic_pyme,  1 as ic_producto_nafin, " +
						" 	'Descuento Electronico' as ic_nombre, " +
						" 	n.ic_nafin_electronico,  " +
						" 	p.cg_razon_social,  " +
						" 	p.cg_rfc,  " +
						" 	c.cg_email,  " +
						" 	bpxp.cs_bloqueo, " +
						" 	TO_CHAR(bpxp.df_modificacion,'dd/mm/yyyy hh24:mi') as df_modificacion,  " +
						" 	bpxp.cg_causa_bloqueo, " +
						"  CASE bpxp.cs_bloqueo WHEN 'B' THEN 'true' " +
						"  WHEN 'D' THEN 'false' ELSE 'false' END AS SELECCIONAR," +
						" 	ce.ic_epo,  " +
						" 	ce.cg_razon_social nombre_epo " +
						" FROM  " +
						" 	comrel_pyme_epo pe, comcat_pyme p, " +
						"	comcat_epo ce, com_contacto c, " +
						"  comrel_nafin n, " +
						" 	comrel_bloqueo_pyme_x_epo bpxp " +
						" WHERE  " +
						" 	pe.ic_pyme = p.ic_pyme " +
						"	AND pe.ic_epo = ce.ic_epo " +
						" 	AND pe.cs_habilitado = 'S' " +
						" 	AND pe.cg_pyme_epo_interno IS NOT NULL " +
						" 	AND pe.ic_pyme = c.ic_pyme " +
						" 	AND c.cs_primer_contacto = 'S' " +
						" 	AND pe.ic_pyme = n.ic_epo_pyme_if " +
						" 	AND p.cs_habilitado = 'S' " +
						" 	AND n.cg_tipo = 'P' " +
						" " + condicion + " " +
						" 	AND pe.ic_pyme  =  bpxp.ic_pyme(+) " +
						"	AND pe.ic_epo = bpxp.ic_epo(+) " +
						" 	AND bpxp.ic_producto_nafin(+) = 1 " +
						" ORDER BY p.cg_razon_social");
			} else {
				qrySentencia.append( 
						" SELECT /*+ use_nl ( pe p) */  " +
						" 	DISTINCT p.ic_pyme,  pe.ic_producto_nafin, " +
						" 	pn.ic_nombre,  " +
						" 	n.ic_nafin_electronico,  " +
						" 	p.cg_razon_social,  " +
						" 	p.cg_rfc,  " +
						" 	c.cg_email,  " +
						" 	bpxp.cs_bloqueo, " +
						" 	TO_CHAR(bpxp.df_modificacion,'dd/mm/yyyy hh24:mi') as df_modificacion,  " +
						" 	bpxp.cg_causa_bloqueo, " +
						"  CASE bpxp.cs_bloqueo WHEN 'B' THEN 'true' " +
						"  WHEN 'D' THEN 'false' ELSE 'false' END AS SELECCIONAR, " +
						" 	ce.ic_epo,  " +
						" 	ce.cg_razon_social nombre_epo " +
						" FROM   " +
						" 	comrel_pyme_epo_x_producto pe, comcat_pyme p, " +
						" 	comcat_epo ce, com_contacto c, comrel_nafin n, " +
						" 	comcat_producto_nafin pn, " +
						" 	comrel_bloqueo_pyme_x_epo bpxp  " +
						" WHERE   " +
						" 	pe.ic_pyme = p.ic_pyme  " +
						"	AND pe.ic_epo = ce.ic_epo " +
						" 	AND pe.ic_producto_nafin = ? " +
						" 	AND p.cs_habilitado = 'S'  " +
						" 	AND n.cg_tipo = 'P'  " +
						" 	AND pe.ic_pyme = c.ic_pyme  " +
						" 	AND c.cs_primer_contacto = 'S' " +
						" 	AND pe.ic_pyme = n.ic_epo_pyme_if  " +
						" 	AND pe.ic_producto_nafin = pn.ic_producto_nafin " +
						" 	AND pe.ic_pyme = bpxp.ic_pyme(+)  " +
						"	AND pe.ic_epo = bpxp.ic_epo(+) " +
						" 	AND bpxp.ic_producto_nafin(+) = ? " +
						" " + condicion + " " +
						" ORDER BY p.cg_razon_social ");
			}
		
		
			if (!ic_producto_nafin.equals("1")) { //Si no es descuento establece la clave del producto
				conditions.add(new Integer(ic_producto_nafin)); 
				conditions.add(new Integer(ic_producto_nafin)); 				
			}
			
			if(!"".equals(num_pyme)) {
				conditions.add(new Long(num_pyme)); 
			}
			if(!"".equals(rfc_pyme)) {
				conditions.add(rfc_pyme); 
			}
			if(!"".equals(nombre_pyme)) {
				conditions.add(nombre_pyme); 
			}
			
			if(!"".equals(ic_epo)) {
				conditions.add(new Integer(ic_epo)); 
			}
			
			log.info("qrySentencia  "+qrySentencia);
			log.info("conditions "+conditions);
			log.info("getDocumentQueryFile(S)");
			return qrySentencia.toString();
		}
		
		
		public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rsAPI, String path, String tipo){
			log.debug("crearCustomFile (E)");
			String nombreArchivo = "";
			HttpSession session = request.getSession();	
			CreaArchivo creaArchivo = new CreaArchivo();
			StringBuffer contenidoArchivo = new StringBuffer();
			return nombreArchivo;		 
		}
	
			
		
		public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo){
			String nombreArchivo = "";
			HttpSession session = request.getSession();	
		
			CreaArchivo creaArchivo = new CreaArchivo();
			StringBuffer contenidoArchivo = new StringBuffer();
		
			try {
		
			}catch(Throwable e) {
				throw new AppException("Error al generar el archivo ", e);
			} finally {
			try {
				
			} catch(Exception e) {}
		  
			}
				return nombreArchivo;
		}
 
	public void setNum_pyme(String num_pyme) {
		this.num_pyme = num_pyme;
	}


	public String getNum_pyme() {
		return num_pyme;
	}


	public void setRfc_pyme(String rfc_pyme) {
		this.rfc_pyme = rfc_pyme;
	}


	public String getRfc_pyme() {
		return rfc_pyme;
	}


	public void setNombre_pyme(String nombre_pyme) {
		this.nombre_pyme = nombre_pyme;
	}


	public String getNombre_pyme() {
		return nombre_pyme;
	}


	public void setIc_producto_nafin(String ic_producto_nafin) {
		this.ic_producto_nafin = ic_producto_nafin;
	}


	public String getIc_producto_nafin() {
		return ic_producto_nafin;
	}
		
	public List getConditions(){
			return conditions;
	}

	public String getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}


	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}


	public String getIc_epo() {
		return ic_epo;
	}
	
}