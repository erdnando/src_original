package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.sql.ResultSet;
import netropology.utilerias.*;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.logging.Log;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import netropology.utilerias.ServiceLocator;

public class OperacionPymesCadenas implements IQueryGeneratorRegExtJS{

	private final static Log log = ServiceLocator.getInstance().getLog(OperacionPymesCadenas.class);
	private List conditions;
	private JSONArray arrayRegistros;
	private String tipoReporte;

	public String getDocumentQuery() {
		return null;
	}

	public String getDocumentSummaryQueryForIds(List ids) {
		return null;
	}

	public String getAggregateCalculationQuery() {
		return null;
	}

	public String getDocumentQueryFile() {
		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();
		qrySentencia.append("SELECT TO_CHAR(SYSDATE,'dd/mm/rrrr') FROM DUAL");
		return qrySentencia.toString();
	}

	public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo) {
		return null;
	}

	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo) {

		log.info("crearCustomFile(E)");
		String nombreArchivo = "";

		if("POR_EPO".equals(this.tipoReporte)){
			nombreArchivo = generaReportePorEpo(request, path, tipo);
		} else if("POR_MES".equals(this.tipoReporte)){
			nombreArchivo = generaReportePorMes(request, path, tipo);
		}

		log.info("crearCustomFile(S)");
		return nombreArchivo;

	}

	public String generaReportePorEpo(HttpServletRequest request, String path, String tipo){

		log.info("generaReportePorEpo(E)");
		String nombreArchivo = "";
		HttpSession session  = request.getSession();

		try{

			if("PDF".equals(tipo)){

				ComunesPDF pdfDoc = new ComunesPDF();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				String meses[]     = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

				pdfDoc.addText("\n", "formas",ComunesPDF.RIGHT);

				pdfDoc.setLTable(12,100);
				pdfDoc.setLCell("PYMES",                                       "celda01", ComunesPDF.CENTER   );
				pdfDoc.setLCell("EPO",                                         "celda01", ComunesPDF.CENTER   );
				pdfDoc.setLCell("Total de PYMES \n Afiliadas Habilitadas",     "celda01", ComunesPDF.CENTER   );
				pdfDoc.setLCell("PYMES �nicas Con Publicaci�n",                "celda01", ComunesPDF.CENTER, 2);
				pdfDoc.setLCell("PYMES �nicas \n Sin Publicaci�n",             "celda01", ComunesPDF.CENTER   );
				pdfDoc.setLCell("PYMES �nicas Con Operaci�n",                  "celda01", ComunesPDF.CENTER, 2);
				pdfDoc.setLCell("Total de PYMES \n Afiliadas No Habilitadas",  "celda01", ComunesPDF.CENTER   );
				pdfDoc.setLCell("PYMES �nicas Con Publicaci�n",                "celda01", ComunesPDF.CENTER, 2);
				pdfDoc.setLCell("PYMES �nicas \n Sin Publicaci�n",             "celda01", ComunesPDF.CENTER   );

				pdfDoc.setLCell("",                                            "celda01", ComunesPDF.CENTER   );
				pdfDoc.setLCell("",                                            "celda01", ComunesPDF.CENTER   );
				pdfDoc.setLCell("",                                            "celda01", ComunesPDF.CENTER   );
				pdfDoc.setLCell("Cantidad",                                    "celda01", ComunesPDF.CENTER   );
				pdfDoc.setLCell("Monto",                                       "celda01", ComunesPDF.CENTER   );
				pdfDoc.setLCell("Cantidad",                                    "celda01", ComunesPDF.CENTER   );
				pdfDoc.setLCell("Cantidad",                                    "celda01", ComunesPDF.CENTER   );
				pdfDoc.setLCell("Monto",                                       "celda01", ComunesPDF.CENTER   );
				pdfDoc.setLCell("Cantidad",                                    "celda01", ComunesPDF.CENTER   );
				pdfDoc.setLCell("Cantidad",                                    "celda01", ComunesPDF.CENTER   );
				pdfDoc.setLCell("Monto",                                       "celda01", ComunesPDF.CENTER   );
				pdfDoc.setLCell("Cantidad",                                    "celda01", ComunesPDF.CENTER   );

				if(this.arrayRegistros.size() > 0){
					for(int i = 0; i < this.arrayRegistros.size(); i++){
						JSONObject auxiliar = arrayRegistros.getJSONObject(i);
						pdfDoc.setLCell(auxiliar.getString("PYMES"),                                "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(auxiliar.getString("EPO"),                                  "formas", ComunesPDF.LEFT);
						pdfDoc.setLCell(auxiliar.getString("TOTAL_PYMES_AFILIADAS_HABILITADAS"),    "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(auxiliar.getString("CANTIDAD_PUCP"),                        "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(auxiliar.getString("MONTO_PUCP"),                           "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(auxiliar.getString("PYMES_UNICAS_SIN_OPERACION"),           "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(auxiliar.getString("CANTIDAD_PUCO"),                        "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(auxiliar.getString("MONTO_PUCO"),                           "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(auxiliar.getString("TOTAL_PYMES_AFILIADAS_NO_HABILITADAS"), "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(auxiliar.getString("CANTIDAD_PUCP_1"),                      "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(auxiliar.getString("MONTO_PUCP_1"),                         "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(auxiliar.getString("PYMES_UNICAS_SIN_PUBLICACION"),         "formas", ComunesPDF.CENTER);
					}
				}
				pdfDoc.addLTable();
				pdfDoc.endDocument();

			} else if("CSV".equals(tipo)){

				StringBuffer contenidoArchivo = new StringBuffer();
				CreaArchivo creaArchivo = new CreaArchivo();

				contenidoArchivo.append(" ,"                                      );
				contenidoArchivo.append(" ,"                                      );
				contenidoArchivo.append(" ,"                                      );
				contenidoArchivo.append("PYMES �nicas Con Publicaci�n,"           );
				contenidoArchivo.append(","                                       );
				contenidoArchivo.append("PYMES �nicas Sin Publicaci�n,"           );
				contenidoArchivo.append("PYMES �nicas Con Operaci�n,"             );
				contenidoArchivo.append(","                                       );
				contenidoArchivo.append("Total de PYMES Afiliadas No Habilitadas,");
				contenidoArchivo.append("PYMES �nicas Con Publicaci�n,"           );
				contenidoArchivo.append(","                                       );
				contenidoArchivo.append("PYMES �nicas Sin Publicaci�n,"           );
				contenidoArchivo.append("\n"                                      );

				contenidoArchivo.append("PYMES,"                                  );
				contenidoArchivo.append("EPO,"                                    );
				contenidoArchivo.append("Total de PYMES Afiliadas Habilitadas,"   );
				contenidoArchivo.append("Cantidad,"                               );
				contenidoArchivo.append("Monto,"                                  );
				contenidoArchivo.append("Cantidad,"                               );
				contenidoArchivo.append("Cantidad,"                               );
				contenidoArchivo.append("Monto,"                                  );
				contenidoArchivo.append("Cantidad,"                               );
				contenidoArchivo.append("Cantidad,"                               );
				contenidoArchivo.append("Monto,"                                  );
				contenidoArchivo.append("Cantidad,"                               );
				contenidoArchivo.append("\n"                                      );

				if(this.arrayRegistros.size() > 0){
					for(int i = 0; i < this.arrayRegistros.size(); i++){
						JSONObject auxiliar = arrayRegistros.getJSONObject(i);
						contenidoArchivo.append(auxiliar.getString("PYMES").replace(',',' ')                                +",");
						contenidoArchivo.append(auxiliar.getString("EPO").replace(',',' ')                                  +",");
						contenidoArchivo.append(auxiliar.getString("TOTAL_PYMES_AFILIADAS_HABILITADAS").replace(',',' ')    +",");
						contenidoArchivo.append(auxiliar.getString("CANTIDAD_PUCP").replace(',',' ')                        +",");
						contenidoArchivo.append(auxiliar.getString("MONTO_PUCP").replace(',',' ')                           +",");
						contenidoArchivo.append(auxiliar.getString("PYMES_UNICAS_SIN_OPERACION").replace(',',' ')           +",");
						contenidoArchivo.append(auxiliar.getString("CANTIDAD_PUCO").replace(',',' ')                        +",");
						contenidoArchivo.append(auxiliar.getString("MONTO_PUCO").replace(',',' ')                           +",");
						contenidoArchivo.append(auxiliar.getString("TOTAL_PYMES_AFILIADAS_NO_HABILITADAS").replace(',',' ') +",");
						contenidoArchivo.append(auxiliar.getString("CANTIDAD_PUCP_1").replace(',',' ')                      +",");
						contenidoArchivo.append(auxiliar.getString("MONTO_PUCP_1").replace(',',' ')                         +",");
						contenidoArchivo.append(auxiliar.getString("PYMES_UNICAS_SIN_PUBLICACION").replace(',',' ')         +",");
						contenidoArchivo.append("\n"                                                                            );
					}
				}
				if(!creaArchivo.make(contenidoArchivo.toString(),path, ".csv")){
					nombreArchivo="ERROR";
				}else{
					nombreArchivo = creaArchivo.nombre;
				}

			}

		} catch(Exception e){
			throw new AppException("Error al generar el archivo "+ tipo +". ", e);
		}

		log.info("generaReportePorEpo(S)");
		return nombreArchivo;

	}

	public String generaReportePorMes(HttpServletRequest request, String path, String tipo){

		log.info("generaReportePorMes(E)");
		String nombreArchivo = "";
		HttpSession session  = request.getSession();

		try{

			if("PDF".equals(tipo)){

				ComunesPDF pdfDoc = new ComunesPDF();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				String meses[]     = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

				pdfDoc.addText("\n", "formas",ComunesPDF.RIGHT);

				pdfDoc.setLTable(8,100);

				pdfDoc.setLCell("Mes / A�o",                           "celda01", ComunesPDF.CENTER   );
				pdfDoc.setLCell("PYMES �nicas Afiliadas",              "celda01", ComunesPDF.CENTER   );
				pdfDoc.setLCell("PYMES con \n Publicaci�n",            "celda01", ComunesPDF.CENTER   );
				pdfDoc.setLCell("Porcentaje PYMES \n con Publicaci�n", "celda01", ComunesPDF.CENTER   );
				pdfDoc.setLCell("Monto Publicado",                     "celda01", ComunesPDF.CENTER   );
				pdfDoc.setLCell("No. de Documentos \n Publicados",     "celda01", ComunesPDF.CENTER   );
				pdfDoc.setLCell("Monto Operado",                       "celda01", ComunesPDF.CENTER   );
				pdfDoc.setLCell("No. de Documentos \n Operados",       "celda01", ComunesPDF.CENTER   );

				if(this.arrayRegistros.size() > 0){
					for(int i = 0; i < this.arrayRegistros.size(); i++){
						JSONObject auxiliar = arrayRegistros.getJSONObject(i);
						pdfDoc.setLCell(auxiliar.getString("MES_ANIO"),                   "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(auxiliar.getString("PYMES_UNICAS_AFILIADAS"),     "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(auxiliar.getString("PYMES_CON_PUBLICACION"),      "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(auxiliar.getString("PORC_PYMES_CON_PUBLICACION"), "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(auxiliar.getString("MONTO_PUBLICADO"),            "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(auxiliar.getString("DOCUMENTOS_PUBLICADOS"),      "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(auxiliar.getString("MONTO_OPERADO"),              "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(auxiliar.getString("DOCUMENTOS_OPERADOS"),        "formas", ComunesPDF.CENTER);
					}
				}
				pdfDoc.addLTable();
				pdfDoc.endDocument();

			} else if("CSV".equals(tipo)){

				StringBuffer contenidoArchivo = new StringBuffer();
				CreaArchivo creaArchivo = new CreaArchivo();

				contenidoArchivo.append("Mes / A�o,"                              );
				contenidoArchivo.append("PYMES �nicas Afiliadas,"                 );
				contenidoArchivo.append("PYMES con Publicaci�n,"                  );
				contenidoArchivo.append("Porcentaje PYMES con Publicaci�n,"       );
				contenidoArchivo.append("Monto Publicado,"                        );
				contenidoArchivo.append("No. de Documentos Publicados,"           );
				contenidoArchivo.append("Monto Operado,"                          );
				contenidoArchivo.append("No. de Documentos Operados,"             );
				contenidoArchivo.append("\n"                                      );

				if(this.arrayRegistros.size() > 0){
					for(int i = 0; i < this.arrayRegistros.size(); i++){
						JSONObject auxiliar = arrayRegistros.getJSONObject(i);
						contenidoArchivo.append(auxiliar.getString("MES_ANIO").replace(',',' ')                   +",");
						contenidoArchivo.append(auxiliar.getString("PYMES_UNICAS_AFILIADAS").replace(',',' ')     +",");
						contenidoArchivo.append(auxiliar.getString("PYMES_CON_PUBLICACION").replace(',',' ')      +",");
						contenidoArchivo.append(auxiliar.getString("PORC_PYMES_CON_PUBLICACION").replace(',',' ') +",");
						contenidoArchivo.append(auxiliar.getString("MONTO_PUBLICADO").replace(',',' ')            +",");
						contenidoArchivo.append(auxiliar.getString("DOCUMENTOS_PUBLICADOS").replace(',',' ')      +",");
						contenidoArchivo.append(auxiliar.getString("MONTO_OPERADO").replace(',',' ')              +",");
						contenidoArchivo.append(auxiliar.getString("DOCUMENTOS_OPERADOS").replace(',',' ')        +",");
						contenidoArchivo.append("\n"                                                                  );
					}
				}
				if(!creaArchivo.make(contenidoArchivo.toString(),path, ".csv")){
					nombreArchivo="ERROR";
				}else{
					nombreArchivo = creaArchivo.nombre;
				}

			}

		} catch(Exception e){
			throw new AppException("Error al generar el archivo "+ tipo +". ", e);
		}

		log.info("generaReportePorMes(S)");
		return nombreArchivo;

	}

/*****************************************
 *          GETTERS AND SETTERS          *
 *****************************************/
	public List getConditions() {
		return conditions;
	}

	public void setArrayRegistros(JSONArray arrayRegistros){
		this.arrayRegistros = arrayRegistros;
	}

	public JSONArray getArrayRegistros(){
		return this.arrayRegistros;	
	}

	public void setTipoReporte(String tipoReporte){
		this.tipoReporte = tipoReporte;
	}

	public String getTipoReporte(){
		return this.tipoReporte;
	}

}
