package com.netro.cadenas;

import com.netro.xls.ComunesXLS;
import java.math.BigDecimal;
import netropology.utilerias.*;
import java.util.Collection;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import org.apache.commons.logging.Log;
import java.util.*;     
import com.netro.pdf.ComunesPDF;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;  
import com.netro.exception.*;
import java.io.FileOutputStream;
import javax.naming.*;
import java.io.*;
 
 
public class RepCreditoElectronico  {

  
//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(RepCreditoElectronico.class);
	private String informacion;
	private String periodo;
	private String reporte_credito;
	private String path;
	private String tipo;
	private String titulo;
		 
	public RepCreditoElectronico() { }
		
	/**
	 * 
	 * @return 
	 * @param informacion
	 * @param registros
	 * @param tipo
	 * @param path
	 * @param request
	 */
	
	public String getArchivosConst(HttpServletRequest request, List registros   ) {
		
		log.debug("crearCustomFile (E)");
		 
		 HttpSession session = request.getSession();	
		 CreaArchivo archivo = new CreaArchivo();
		 ComunesXLS xls = new ComunesXLS();;		
		 ComunesPDF pdfDoc = new ComunesPDF();		 
		 String nombreArchivo = "";  
		
				
		try {	
		
		
			int total_registros = 0;
			BigDecimal total_monto_solic = new BigDecimal("0.00");
			BigDecimal total_monto_desc = new BigDecimal("0.00");
			int total_personas_f = 0;
			int total_personas_m = 0;
			
			
			if(tipo.equals("PDF") ){
			
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
			
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
							
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
				
			}else  if(tipo.equals("XLS")) {
				
				 nombreArchivo = archivo.nombreArchivo()+".xls";
				 xls = new ComunesXLS(path+nombreArchivo);
			
			}
			  
			if(tipo.equals("XLS") && informacion.equals("Generar_Arch")  ) {
									
				xls.setTabla(9);
				xls.setCelda( periodo,  "celda01", ComunesXLS.CENTER, 9);
				xls.setCelda( "",  "celda01", ComunesXLS.CENTER, 7);
				xls.setCelda( " Personas F�sicas ",  "celda01", ComunesXLS.CENTER, 1);
				xls.setCelda( " Personas Morales",  "celda01", ComunesXLS.CENTER, 1);
				
				if(reporte_credito.equals("BASE_OPER") ){
					xls.setCelda(" Base Operaci�n (SIRAC)  ", "celda01", ComunesXLS.CENTER, 1);
					xls.setCelda(" Intermediario Financiero  ", "celda01", ComunesXLS.CENTER, 1);
				}else  if(reporte_credito.equals("INT_FIN") ){
					xls.setCelda(" Intermediario Financiero  ", "celda01", ComunesXLS.CENTER, 1);
					xls.setCelda(" Base Operaci�n (SIRAC)  ", "celda01", ComunesXLS.CENTER, 1);					
				}
				
				xls.setCelda(" Tipo de Cr�dito (Destino del Cr�dito)", "celda01", ComunesXLS.CENTER, 1);
				xls.setCelda(" Total de Registros ", "celda01", ComunesXLS.CENTER, 1);
				xls.setCelda(" Monto Solicitado ", "celda01", ComunesXLS.CENTER, 1);
				xls.setCelda(" Monto del Descuento ", "celda01", ComunesXLS.CENTER, 1);
				xls.setCelda(" Estado del Acreditado ", "celda01", ComunesXLS.CENTER, 1);
				xls.setCelda(" Total", "celda01", ComunesXLS.CENTER, 1);
				xls.setCelda(" Total  ", "celda01", ComunesXLS.CENTER, 1);
			 				
				for(int i= 0; i < registros.size(); i++){
					List registroReporteCredito = (List)registros.get(i);
					total_registros += Integer.parseInt((String)registroReporteCredito.get(8));
					total_monto_solic = total_monto_solic.add(new BigDecimal((String)registroReporteCredito.get(3)));
					total_monto_desc = total_monto_desc.add(new BigDecimal((String)registroReporteCredito.get(4)));
					total_personas_f += Integer.parseInt((String)registroReporteCredito.get(6));
					total_personas_m += Integer.parseInt((String)registroReporteCredito.get(7));
					
				   xls.setCelda( registroReporteCredito.get(0).toString(), "formas", ComunesXLS.CENTER, 1);
					xls.setCelda( registroReporteCredito.get(1).toString(), "formas", ComunesXLS.CENTER, 1);
					xls.setCelda( registroReporteCredito.get(2).toString(), "formas", ComunesXLS.CENTER, 1);
					 xls.setCelda(registroReporteCredito.get(8).toString(), "formas", ComunesXLS.CENTER, 1);
					 xls.setCelda("$"+ Comunes.formatoDecimal(registroReporteCredito.get(3).toString(),2),"formas", ComunesXLS.CENTER, 1);
					 xls.setCelda("$"+ Comunes.formatoDecimal(registroReporteCredito.get(4).toString(),2), "formas", ComunesXLS.CENTER, 1);
					 xls.setCelda(registroReporteCredito.get(5).toString(), "formas", ComunesXLS.CENTER, 1);
					 xls.setCelda(registroReporteCredito.get(6).toString(), "formas", ComunesXLS.CENTER, 1);
					 xls.setCelda(registroReporteCredito.get(7).toString(), "formas", ComunesXLS.CENTER, 1);       
					
				}
				xls.setCelda( "Total General", "celda01", ComunesXLS.CENTER, 1);       
				xls.setCelda("", "formas", ComunesXLS.CENTER, 2);       
				xls.setCelda( String.valueOf(total_registros)  ,"formas", ComunesXLS.CENTER, 1);       
				xls.setCelda( "$"+ Comunes.formatoDecimal(String.valueOf(total_monto_solic), 2) , "formas", ComunesXLS.CENTER, 1);       
				xls.setCelda("$"+ Comunes.formatoDecimal( String.valueOf(total_monto_desc), 2)  , "formas", ComunesXLS.CENTER, 1);       
				xls.setCelda("", "formas", ComunesXLS.CENTER, 1);       
				xls.setCelda(String.valueOf(total_personas_f), "formas", ComunesXLS.CENTER, 1);       
				xls.setCelda(String.valueOf(total_personas_m), "formas", ComunesXLS.CENTER, 1);       
				
				xls.cierraTabla();
				xls.cierraXLS();
				  			
					
			}else  if(tipo.equals("PDF") && informacion.equals("Generar_Arch")  ) {
			
				pdfDoc.setTable(9, 100 );   
				pdfDoc.setCell(periodo, "celda01",ComunesPDF.CENTER, 9);	
				pdfDoc.setCell("", "celda01",ComunesPDF.CENTER, 7);	
				pdfDoc.setCell("Personas F�sicas", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Personas Morales ", "celda01",ComunesPDF.CENTER);
				
				if(reporte_credito.equals("BASE_OPER") ){
					pdfDoc.setCell(" Base Operaci�n (SIRAC)  ", "celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell(" Intermediario Financiero ", "celda01",ComunesPDF.CENTER);	
				}else  if(reporte_credito.equals("INT_FIN") ){
					pdfDoc.setCell(" Intermediario Financiero ", "celda01",ComunesPDF.CENTER);			
					pdfDoc.setCell(" Base Operaci�n (SIRAC)  ", "celda01",ComunesPDF.CENTER);							
				}				
				
				pdfDoc.setCell(" Tipo de Cr�dito (Destino del Cr�dito)", "celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell(" Total de Registros ", "celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell(" Monto Solicitado ", "celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell(" Monto del Descuento ", "celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell(" Estado del Acreditado ", "celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell(" Total ", "celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell(" Total ", "celda01",ComunesPDF.CENTER);	
			
			
				for(int i= 0; i < registros.size(); i++){
					List registroReporteCredito = (List)registros.get(i);
					total_registros += Integer.parseInt((String)registroReporteCredito.get(8));
					total_monto_solic = total_monto_solic.add(new BigDecimal((String)registroReporteCredito.get(3)));
					total_monto_desc = total_monto_desc.add(new BigDecimal((String)registroReporteCredito.get(4)));
					total_personas_f += Integer.parseInt((String)registroReporteCredito.get(6));
					total_personas_m += Integer.parseInt((String)registroReporteCredito.get(7));
					
				   pdfDoc.setCell( registroReporteCredito.get(0).toString(), "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell( registroReporteCredito.get(1).toString(), "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell( registroReporteCredito.get(2).toString(), "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(registroReporteCredito.get(8).toString(), "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell("$"+ Comunes.formatoDecimal(registroReporteCredito.get(3).toString(),2), "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell("$"+ Comunes.formatoDecimal(registroReporteCredito.get(4).toString(),2), "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(registroReporteCredito.get(5).toString(), "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(registroReporteCredito.get(6).toString(), "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(registroReporteCredito.get(7).toString(), "formas",ComunesPDF.CENTER);	         
					
				}
					pdfDoc.setCell( "Total General", "celda01",ComunesPDF.CENTER);	
					pdfDoc.setCell("", "formas",ComunesPDF.CENTER,2);	
					pdfDoc.setCell( String.valueOf(total_registros)  , "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell( "$"+ Comunes.formatoDecimal(String.valueOf(total_monto_solic), 2) , "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell("$"+ Comunes.formatoDecimal( String.valueOf(total_monto_desc), 2)  , "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell("", "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(String.valueOf(total_personas_f), "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(String.valueOf(total_personas_m), "formas",ComunesPDF.CENTER);	
								
				pdfDoc.addTable();
				pdfDoc.endDocument();	
			
			}else if(tipo.equals("XLS") && informacion.equals("Generar_Arch_Det")  ) {
		
				xls.setTabla(5);
				xls.setCelda(titulo,  "celda01", ComunesXLS.CENTER, 5);
				xls.setCelda(" N�mero SIRAC",  "celda01", ComunesXLS.CENTER, 1);
				xls.setCelda(" RFC ",  "celda01", ComunesXLS.CENTER, 1);
				xls.setCelda(" Nombre o Raz�n Social",  "celda01", ComunesXLS.CENTER, 1);				
				xls.setCelda(" Estado  ", "celda01", ComunesXLS.CENTER, 1);
				xls.setCelda(" Ciudad  ", "celda01", ComunesXLS.CENTER, 1);
							
				for(int i= 0; i < registros.size(); i++){
					List registroReporteCredito = (List)registros.get(i);
					
					xls.setCelda(registroReporteCredito.get(0), "formas", ComunesXLS.CENTER, 1);
					xls.setCelda(registroReporteCredito.get(1), "formas", ComunesXLS.CENTER, 1);
					xls.setCelda(registroReporteCredito.get(2), "formas", ComunesXLS.CENTER, 1);
					xls.setCelda(registroReporteCredito.get(3), "formas", ComunesXLS.CENTER, 1);
					xls.setCelda(registroReporteCredito.get(4), "formas", ComunesXLS.CENTER, 1);				
				}
					
				xls.cierraTabla();
				xls.cierraXLS();
				
				
			}else  if(tipo.equals("PDF") && informacion.equals("Generar_Arch_Det")  ) {
			
				pdfDoc.setTable(5, 100 );   
				pdfDoc.setCell(titulo, "celda01",ComunesPDF.CENTER, 5);	
				pdfDoc.setCell(" N�mero SIRAC", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" RFC ",  "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Nombre o Raz�n Social",  "celda01",ComunesPDF.CENTER);		
				pdfDoc.setCell(" Estado  ", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Ciudad  ", "celda01",ComunesPDF.CENTER);
							
				for(int i= 0; i < registros.size(); i++){
					List registroReporteCredito = (List)registros.get(i);
					
					pdfDoc.setCell(registroReporteCredito.get(0).toString(),  "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(registroReporteCredito.get(1).toString(),  "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(registroReporteCredito.get(2).toString(),  "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(registroReporteCredito.get(3).toString(),  "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(registroReporteCredito.get(4).toString(),  "formas",ComunesPDF.CENTER);			
				}
					
				pdfDoc.addTable();
				pdfDoc.endDocument();
				
			
			
			}
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}

	public String getInformacion() {
		return informacion;
	}

	public void setInformacion(String informacion) {
		this.informacion = informacion;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public String getReporte_credito() {
		return reporte_credito;
	}

	public void setReporte_credito(String reporte_credito) {
		this.reporte_credito = reporte_credito;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}	

}