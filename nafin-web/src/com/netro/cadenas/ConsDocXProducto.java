package com.netro.cadenas;

import java.io.BufferedWriter;
import org.apache.commons.logging.Log;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.IQueryGeneratorRegExtJS;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import netropology.utilerias.AppException;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.Comunes;

public class ConsDocXProducto implements IQueryGeneratorRegExtJS {
	
	private final static Log log = ServiceLocator.getInstance().getLog(ConsDocXProducto.class);
	String noBancoFondeo; 
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");

	public ConsDocXProducto() {}
	
	public String getDocumentQuery() {
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();
	}
	
	public String getDocumentSummaryQueryForIds(List ids){
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();
	}


	public String getAggregateCalculationQuery(){
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}	
		
		
	public String getDocumentQueryFile(){
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();		
	
		qrySentencia.append("SELECT  decode (EPO.cs_habilitado,'N','<font color=\"Blue\">*</font>','S',' ')||' '||EPO.CG_NOMBRE_COMERCIAL as EPO, "+
		  "  DECODE (epo.cs_habilitado,'N', '*','S', ' ' ) || ' ' || epo.cg_nombre_comercial AS nombreEpo,  "+
		 " PROD.IC_NOMBRE, DOCTO.CD_DESCRIPCION ");
		qrySentencia.append(" FROM COMCAT_CLASE_DOCTO DOCTO, COMCAT_EPO EPO, COMCAT_PRODUCTO_NAFIN PROD, COMREL_PRODUCTO_DOCTO RELPRODOCTO ");
		qrySentencia.append(" WHERE DOCTO.IC_CLASE_DOCTO = RELPRODOCTO.IC_CLASE_DOCTO AND PROD.IC_PRODUCTO_NAFIN = RELPRODOCTO.IC_PRODUCTO_NAFIN AND EPO.IC_EPO = RELPRODOCTO.IC_EPO ");
	
		if(!"".equals(noBancoFondeo) ){ 
			qrySentencia.append("    AND ic_banco_fondeo = ?");
			conditions.add(noBancoFondeo);
		}
		qrySentencia.append("ORDER BY 1");
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();
	}		
	
	public List getConditions(){
		return conditions;
	}	
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
			log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		StringBuffer contenidoArchivo = new StringBuffer();
		
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		int total = 0;
		
		try {
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);
		
			contenidoArchivo = new StringBuffer();		
			contenidoArchivo.append("Cadena Productiva, Producto, Clase de Documento \n");			
		
			while (rs.next())	{	
			
				String nombreEPO = (rs.getString("nombreEpo") == null) ? "" : rs.getString("nombreEpo");	
				String ic_nombre = (rs.getString("IC_NOMBRE") == null) ? "" : rs.getString("IC_NOMBRE");				
				String descripcion = (rs.getString("CD_DESCRIPCION") == null) ? "" : rs.getString("CD_DESCRIPCION");
			
				contenidoArchivo.append(nombreEPO.replace(',',' ')+", "+
											ic_nombre.replace(',',' ')+", "+
											descripcion.replace(',',' ')+"\n");					
				total++;
				if(total==1000){					
					total=0;	
					buffer.write(contenidoArchivo.toString());
					contenidoArchivo = new StringBuffer();//Limpio  
				}
				
			}//while(rs.next()){
				
			buffer.write(contenidoArchivo.toString());
			buffer.close();	
			contenidoArchivo = new StringBuffer();//Limpio    
			
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo){
		String nombreArchivo = "";
		return nombreArchivo;
	}
	
/************ SETTERS ***************/

		public void setnoBancoFondeo(String noBancoFondeo){
			this.noBancoFondeo = noBancoFondeo;
		}	

}