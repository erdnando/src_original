package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsCredito implements IQueryGeneratorRegExtJS  {
	
	String noIf;
	String tipoTasa;
	String baseOperacionDinamica;
	String altaAutomaticaGarantia;
	String tipoCred;
	String tipoAmort;
	String tipoCartera;
	String tipo_plazo;
	String tipoOperacion; 
	//String noEmisor; 
	String periodicidad;
	String tipoInteres;
	String tipoRenta;
	
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");	
	
	private final static Log log = ServiceLocator.getInstance().getLog(ConsCredito.class);
	
	public ConsCredito() {}
	
	public String getDocumentQuery() {
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();
	}
	
	public String getDocumentSummaryQueryForIds(List ids){
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();
	}
	
	public String getAggregateCalculationQuery(){
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
		
	public List getConditions(){
		return conditions;
	}
		
		public String getDocumentQueryFile(){
			log.info("getDocumentQueryFile(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			
		qrySentencia.append(  " select I.cg_razon_social as nomIF, T.cd_nombre as nomTasa, "+
						" TC.cd_descripcion as nomTipCred, TA.cd_descripcion as nomAmortiza, "+
						" BC.ig_plazo_minimo as plzMin, BC.ig_plazo_maximo as plzMax, "+
						" BC.ic_base_op_credito as icBaseOpeCred, BC.cs_tipo_plazo as tipoplazo, BC.ig_tipo_cartera as cartera, "+
						" BO.cg_descripcion as nomBaseOper, BO.ig_codigo_base as CodigoBase "+
						" , E.cd_descripcion as nomEmisor, BC.cs_alta_automatica_garantia as alta"+
						" , CP.cd_descripcion as nomPeriodicidad "+//SE AGREGA POR F004-2010 BASES DE OPERACION - FVR
						" , to_char(BC.ig_producto_banco) as productoBanco "+//SE AGREGA POR F004-2010 BASES DE OPERACION - FVR
						" , DECODE(BC.cg_tipo_interes,'S','Simple','C','Compuesto',BC.cg_tipo_interes ) as tipoInteres "+//SE AGREGA POR F004-2010 BASES DE OPERACION - FVR
						" , BC.cs_tipo_renta as tipoRenta "+//SE AGREGA POR F020-2011 BASES DE OPERACION - FVR
						" , bc.cs_base_op_dinamica as basedinamica "  + // F014 - 2012. BASES DE OPERACION DINAMICAS. BY JSHD
						//" , '/15nafin/15basesoperce2.jsp' as pantalla " +
						" from com_base_op_credito BC, comcat_if I, comcat_tasa T, "+
						" comcat_base_operacion BO, comcat_tipo_credito TC, comcat_tabla_amort TA"+
						" , comcat_emisor E "+
						" , comcat_periodicidad CP "+// se agrega tabla para mostrar periodicidad F004-2010 FVR
						" where BC.ic_if = I.ic_if "+
						" and BC.ic_tasa = T.ic_tasa "+
						" and BC.ig_codigo_base = BO.ig_codigo_base(+) "+
						" and BC.ic_tipo_credito = TC.ic_tipo_credito "+
						" and BC.ic_tabla_amort = TA.ic_tabla_amort "+
						" and BC.ic_emisor = E.ic_emisor(+) "+
						" and BC.ic_periodicidad = CP.ic_periodicidad(+) "+//se agrega join para mostrar la periodicidad F004-2010 FVR
						" and BC.ic_if = "+noIf+
						((!tipoTasa.equals(""))?" and BC.ic_tasa = "+tipoTasa:"")+
						((!tipoCred.equals(""))?" and BC.ic_tipo_credito = "+tipoCred:"")+
						((!tipoAmort.equals(""))?" and BC.ic_tabla_amort = "+tipoAmort:"")+
						((!tipoCartera.equals(""))?" and BC.ig_tipo_cartera = "+tipoCartera:"")+
						((!tipo_plazo.equals(""))?" and BC.cs_tipo_plazo = '"+tipo_plazo+"'":"")+
						((!periodicidad.equals(""))?"  and BC.ic_periodicidad ="+periodicidad:"")+
						((!tipoInteres.equals(""))?"  and cg_tipo_interes = '"+tipoInteres+"'":"")+
						((!baseOperacionDinamica.equals(""))?" and bc.cs_base_op_dinamica = '" + baseOperacionDinamica + "' ":"") + // F014 - 2012. BASES DE OPERACION DINAMICAS. BY JSHD
						((!altaAutomaticaGarantia.equals(""))?" AND bc.cs_alta_automatica_garantia = '" + altaAutomaticaGarantia + "' ":"") +
						((tipoOperacion.equals("credito"))?" and T.cs_creditoelec = 'S' and TC.cs_creditoelec = 'S' ":"")+
						((tipoOperacion.equals("equipamiento"))?"  and BC.ic_producto_nafin = 7 ":" and BC.ic_producto_nafin is null")+
						((!tipoRenta.equals(""))?" and bc.cs_tipo_renta = '"+tipoRenta+"' ":" and bc.cs_tipo_renta is null ")+ //MODIFICACION POR F020-2011 FVR
						" order by nomTasa "	);
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();
	}

	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
			log.debug("crearCustomFile (E)");
			String nombreArchivo = "";
			HttpSession session = request.getSession();	
			StringBuffer contenidoArchivo = new StringBuffer("");

					
		try {
		if("PDF".equals(tipo)){

			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				
			ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
			session.getAttribute("iNoNafinElectronico") == null ? "" : session.getAttribute("iNoNafinElectronico").toString(),
			(String)session.getAttribute("sesExterno"),
			(String) session.getAttribute("strNombre"),
			(String) session.getAttribute("strNombreUsuario"),
			(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);		

			pdfDoc.setTable(16,90);	
			pdfDoc.setCell("Nombre IF","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tasa","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo Cr�dito","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo Amortizaci�n","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo de Cartera","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo Plazo","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Plazo M�nimo","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Plazo M�ximo","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Emisor","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("C�digo Operaci�n","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Base Operaci�n Din�mica","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Alta Aut. Garant.","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("C�digo Producto Banco","celda01",ComunesPDF.CENTER);			
			pdfDoc.setCell("Periodicidad de Pago a Capital","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo de Inter�s","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo Renta","celda01",ComunesPDF.CENTER);			
		
			while (rs.next()) {
								
					String no_if 	= rs.getString("NOMIF")==null?"":rs.getString("NOMIF");
					String tipoTasa 		= rs.getString("NOMTASA")==null?"":rs.getString("NOMTASA");
					String credito 		= rs.getString("NOMTIPCRED")==null?"":rs.getString("NOMTIPCRED");
					String amortizacion 		= rs.getString("NOMAMORTIZA")==null?"":rs.getString("NOMAMORTIZA");
					String cartera = rs.getString("CARTERA")==null?"":rs.getString("CARTERA");
					String tPlazo	=  rs.getString("TIPOPLAZO")==null?"":rs.getString("TIPOPLAZO"); 				
					String plazomin 	= rs.getString("PLZMIN")==null?"":rs.getString("PLZMIN");
					String plazomax 		= rs.getString("PLZMAX")==null?"":rs.getString("PLZMAX");
					String emisor 		= rs.getString("NOMEMISOR")==null?"":rs.getString("NOMEMISOR");
					String codOperacion 		= rs.getString("CODIGOBASE")==null?"":rs.getString("CODIGOBASE");
					String descripcion = rs.getString("NOMBASEOPER")==null?"":rs.getString("NOMBASEOPER");
					
					String codigo = "";
					if(!codOperacion.equals("")){
						codigo = codOperacion + " - "+ descripcion;
					}
					
					String dinamica 		= rs.getString("BASEDINAMICA")==null?"":rs.getString("BASEDINAMICA");
					
					String baseDinamica = "";
					if(dinamica.equals("S"))
						baseDinamica = "Si";
					else	
						baseDinamica = "No";	
						
					String alta 	= rs.getString("ALTA")==null?"":rs.getString("ALTA");
					
					String altaAut = "";
					if(alta.equals("S"))
						altaAut = "Si";
					else
						altaAut = "No";
					
					String renCartera= "";
					if (cartera.equals("1")){
						renCartera = "1er Piso";
					}else{
						renCartera = "2do Piso";
					}
					
					String banco = rs.getString("PRODUCTOBANCO")==null?"":rs.getString("PRODUCTOBANCO");
					String periodicidad 	= rs.getString("NOMPERIODICIDAD")==null?"":rs.getString("NOMPERIODICIDAD");
					String interes = rs.getString("TIPOINTERES")==null?"":rs.getString("TIPOINTERES");
					String renta = rs.getString("TIPORENTA")==null?"":rs.getString("TIPORENTA");

					String Plaz = "";
					if(tPlazo.equals("F")){
						Plaz = "Factoraje";
					}else if(tPlazo.equals("C")){
						Plaz = "Cr�dito";
					}else if(tPlazo.equals("E")){
						Plaz = "Cotizaciones Espec�ficas";
					}
					
					
					
					String tipoRenta = "";
					if(renta.equals("")){
						tipoRenta = "N/A";
					}else if(renta.equals("S")){
						tipoRenta = "Si";
					}else{
						tipoRenta = "No";
					}	
			
					pdfDoc.setCell(no_if,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(tipoTasa,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(credito,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(amortizacion,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(renCartera,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(Plaz,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(plazomin,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(plazomax,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(emisor,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(codigo,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(baseDinamica,"formas",ComunesPDF.CENTER);

					pdfDoc.setCell(altaAut,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(banco,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(periodicidad,"formas",ComunesPDF.CENTER);					
					pdfDoc.setCell(interes,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(tipoRenta,"formas",ComunesPDF.CENTER);			
						
			}	

				pdfDoc.addTable();
				pdfDoc.endDocument();			
		
		}else if("CSV".equals(tipo)){
			contenidoArchivo.append("Nombre IF,Tasa,Tipo Cr�dito,Tipo Amortizaci�n,Tipo de Cartera,Tipo Plazo,Plazo M�nimo,Plazo M�ximo,Emisor,C�digo Operaci�n"); 
			contenidoArchivo.append(",Base de Operaci�n Din�mica,Alta Automatica de Garantia,Codigo Producto Banco,Periodicidad de Pago a Capital,Tipo de Interes,Tipo Renta\n");
		
		int posReg = 0;
		while(rs.next()) {
			posReg++;	
			//i++;
			contenidoArchivo.append(rs.getString("nomIF").replace(',',' ')+","+
									rs.getString("nomTasa").replace(',',' ')+","+
									rs.getString("nomTipCred").replace(',',' ')+","+
									rs.getString("nomAmortiza").replace(',',' ')+","+
									(rs.getString("cartera").equals("1")?"1er Piso":"2do Piso")+",");
									
			if(rs.getString("tipoplazo").equals("F"))
			{
			 contenidoArchivo.append("Factoraje,");
			} else if (rs.getString("tipoplazo").equals("C"))
			{
			  contenidoArchivo.append("Credito,");
			} else {
			  contenidoArchivo.append("Cotizaciones Especificas,");
			}
									
			contenidoArchivo.append(rs.getString("plzMin")+","+
									rs.getInt("plzMax")+","+
									(rs.getString("nomEmisor")==null?"":rs.getString("nomEmisor").replace(',',' '))+","+
									(rs.getString("CodigoBase")==null?"":rs.getString("CodigoBase")+" - " )+
									(rs.getString("nomBaseOper")==null?"":rs.getString("nomBaseOper").replace(',',' '))    );
			//if(tipoOperacion.equals("credito")){
				
				// Agregar campo Base de Operacion Din�mica. F014 - 2012. BASES DE OPERACION DINAMICAS. BY JSHD 
				contenidoArchivo.append(",");
				
				String base = "";
				if(rs.getString("basedinamica").equals("S"))
					base = "Si";
				else 
					base = "No";
				
				//contenidoArchivo.append(rs.getString("basedinamica"));
				contenidoArchivo.append(base);
				
				String automatica = "";
				if(rs.getString("alta").equals("S"))
					automatica = "Si";
				else 
					automatica = "No";
				// Agregar demas campos	
				contenidoArchivo.append(("," + automatica) + 
												(rs.getString("productoBanco")==null?",":","+rs.getString("productoBanco").replace(',',' ')) +
												(rs.getString("nomPeriodicidad")==null?",":","+rs.getString("nomPeriodicidad").replace(',',' ')) +
												(rs.getString("tipoInteres")==null?",":","+rs.getString("tipoInteres").replace(',',' ')) +												
												((rs.getString("tipoRenta"))==null?",N/A":("".equals(rs.getString("tipoRenta"))?",N/A":("S".equals(rs.getString("tipoRenta"))?",Si":",No")))+"\n");
			//}
		}// while 		
		
		
			CreaArchivo archivo = new CreaArchivo();
			if (archivo.make(contenidoArchivo.toString(), path, ".csv"))
				nombreArchivo = archivo.getNombre();
				
		
		}
		
		
		}catch(Exception e) { 
				log.error("Error en la generacion del Archivo CSV: " + e);
		}	


		return nombreArchivo;
	}

	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo){
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
	
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		
		return nombreArchivo;
	}




/************************SETTERS**********************/
		public void setIntermediario(String noIf){
			this.noIf = noIf;
		}
	
		public void setTasa(String tipoTasa){
			this.tipoTasa = tipoTasa;
		}	

		public void setBaseOperacion(String baseOperacionDinamica){
			this.baseOperacionDinamica = baseOperacionDinamica;
		}		

		public void setAltaAutomatica(String altaAutomaticaGarantia){
			this.altaAutomaticaGarantia = altaAutomaticaGarantia;
		}

		public void setCredito(String tipoCred){
			this.tipoCred = tipoCred;
		}

		public void setAmortizacion(String tipoAmort){
			this.tipoAmort = tipoAmort;
		}
		
		public void setCartera (String tipoCartera){
			this.tipoCartera = tipoCartera;
		}	
		
		public void setPlazo(String tipo_plazo){
			this.tipo_plazo = tipo_plazo;
		}

		public void setOperacion (String tipoOperacion){
			this.tipoOperacion = tipoOperacion;
		}
		
		public void setPeriodicidad (String periodicidad){
			this.periodicidad = periodicidad;
		}	

		public void setInteres (String tipoInteres){
			this.tipoInteres = tipoInteres;
		}
		
		public void setRenta (String tipoRenta){
			this.tipoRenta = tipoRenta;
		}		

	
	
	}// fin clase
	