package com.netro.cadenas;

import com.netro.anticipos.Parametro;
import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ReporteIf implements IQueryGeneratorReg, IQueryGeneratorRegExtJS {

	public ReporteIf(){}	

	//Variable para enviar mensajes al log.
	private static final Log log = ServiceLocator.getInstance().getLog(ReporteIf.class);
	
	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer 		qrySentencia;
	private List 		conditions;
	private String 	paginaOffset;
	private String 	paginaNo;	
	public 	String 	paginar;  
	private String intermediario;
	private String noEpo;
	private String moneda;

	/* M�todos �para nueva versi�n */
	
/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
*/	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		log.info("crearCustomFile(E)");
		CreaArchivo 	archivo 				= new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		String NoCajones = "10";
		String nombreEPO = "";
		String nombreArchivo = "";
		try{
		
			Parametro parametro = ServiceLocator.getInstance().lookup("ParametroEJB", Parametro.class);
			
			List representates = parametro.IntermeyEpoRepresentante( noEpo, intermediario,  moneda);
			if(representates.size()>0) {
				NoCajones = (String)representates.get(2)==null?"0":(String)representates.get(2);
				nombreEPO = (String)representates.get(1)==null?"":(String)representates.get(1);
			}
		
			
			String montoPublicacionVigente = parametro.getMontoPublicacionVigente(intermediario,noEpo,moneda);
			
			contenidoArchivo.append("EPO," +nombreEPO+"\n");
			contenidoArchivo.append("PUBLICACION VIGENTE,\"" + montoPublicacionVigente + "\"\n");
		
			contenidoArchivo.append("Ranking,");
			contenidoArchivo.append("No Proveedor,");
			contenidoArchivo.append("No Nafin Electr�nico,");
			contenidoArchivo.append("N�mero de SIRAC,");
			contenidoArchivo.append("RFC,");
			contenidoArchivo.append("Tel�fono del Contacto,");
			contenidoArchivo.append("Nombre o Raz�n Social,");
			contenidoArchivo.append("Moneda,");
			contenidoArchivo.append("Rango de Publicaci�n \n");
			
			String rango ="" , rangopublicIni ="";
			while(rs.next()){
				rango =(rs.getString("rangopublic"));
				rango			= rango			== null?"0.00"	:rango.trim();
				if(rango.equals(".01")){
					rangopublicIni = "1";
				}else{
					rangopublicIni = rango;
				}
				
				String razonsocialproveedor = rs.getString("razonsocialproveedor");
				String nombreMoneda			 = rs.getString("moneda");
				String numeroSirac			 = rs.getString("in_numero_sirac");
				String telefono				 = rs.getString("telefono");
				String rangopublicFin		 = rs.getString("rangopublicFin"); 
			
				razonsocialproveedor = razonsocialproveedor 	== null?    ""	:razonsocialproveedor.trim();
				nombreMoneda			= nombreMoneda				== null?    ""	:nombreMoneda.trim(); 
				numeroSirac				= numeroSirac				== null?    "" :numeroSirac.trim(); 
				telefono					= telefono					== null?	   ""	:telefono.trim(); 
				rangopublicIni			= rangopublicIni			== null?"0.00" :rangopublicIni.trim();			
				rangopublicFin			= rangopublicFin			== null?"0.00"	:rangopublicFin.trim();	
			
				String ranking		 = rs.getString("ranking"); 
				ranking			= ranking			== null?"0" :ranking.trim();	
				
				//contenidoArchivo.append(rs.getString("ranking")+",");
				contenidoArchivo.append(ranking+",");
				contenidoArchivo.append(rs.getString("pyme")+",");	
				contenidoArchivo.append(rs.getString("nafinelectronico")+",");	
				contenidoArchivo.append(numeroSirac+",");
				contenidoArchivo.append(rs.getString("rfc")+",");
				contenidoArchivo.append("\""+" "+telefono.replaceAll("\"","\"\"")					+"\",");
				contenidoArchivo.append("\""+razonsocialproveedor.replaceAll("\"","\"\"")	+"\",");
				contenidoArchivo.append("\""+nombreMoneda.replaceAll("\"","\"\"")				+"\",");
				contenidoArchivo.append("\"De $"+Comunes.formatoDecimal(rangopublicIni,2)+" hasta  $"+ Comunes.formatoDecimal(rangopublicFin,2)+"\",");
				contenidoArchivo.append("\n");
			}//fin while
			
			if(archivo.make(contenidoArchivo.toString(), path, ".csv")){
				nombreArchivo = archivo.getNombre();
			}
		
		}catch(Exception e){
			log.error("Error en la generaci�n de archivo Excel",e);
		}
		return nombreArchivo;
	}
	
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		log.info("crearPageCustomFile(E)");
		String nombreArchivo="";
		String NoCajones = "10";
		String nombreEPO = "";
		
		if ("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
				Parametro parametro = ServiceLocator.getInstance().lookup("ParametroEJB", Parametro.class);
		
				String nombreIF = parametro.getNombreIF(intermediario);
				List representates = parametro.IntermeyEpoRepresentante( noEpo, intermediario,  moneda);
				if(representates.size()>0) {
					NoCajones = (String)representates.get(2)==null?"0":(String)representates.get(2);
					nombreEPO = (String)representates.get(1)==null?"":(String)representates.get(1);
				}
				
				String montoPublicacionVigente = parametro.getMontoPublicacionVigente(intermediario,noEpo,moneda);				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico") == null ? "" : session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
					
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				
				//pdfDoc.addText("INTERMEDIARIO FINANCIERO: "+ ,"formas",ComunesPDF.LEFT);
				pdfDoc.addText("              " ,"formas",ComunesPDF.LEFT);
				pdfDoc.addText("INTERMEDIARIO FINANCIERO: "+ nombreIF ,"formas",ComunesPDF.LEFT);
				pdfDoc.addText("EPO: "+ nombreEPO ,"formas",ComunesPDF.LEFT);
				pdfDoc.addText("PUBLICACION VIGENTE: "+montoPublicacionVigente ,"formas",ComunesPDF.LEFT);

				pdfDoc.setTable(9,100);
				pdfDoc.setCell("Ranking","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No Proveedor","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No Nafin Electronico","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de SIRAC","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("RFC","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tel�fono del Contacto","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre o Raz�n Social","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Rango de Publicaci�n","celda01",ComunesPDF.CENTER);				
							
				while (reg.next()) {
					String ranking 		= reg.getString("RANKING")==null?"":reg.getString("RANKING");
					String pyme 		= reg.getString("PYME")==null?"":reg.getString("PYME");
					String nafine 		= reg.getString("NAFINELECTRONICO")==null?"":reg.getString("NAFINELECTRONICO");
					String sirac 		= reg.getString("IN_NUMERO_SIRAC")==null?"":reg.getString("IN_NUMERO_SIRAC");
					String rfc 	= reg.getString("RFC")==null?"":reg.getString("RFC");
					String telefono 		= reg.getString("TELEFONO")==null?"":reg.getString("TELEFONO");
					String razonsocial 		= reg.getString("RAZONSOCIALPROVEEDOR")==null?"":reg.getString("RAZONSOCIALPROVEEDOR");
					String moneda 		= reg.getString("MONEDA")==null?"":reg.getString("MONEDA");
					String rango 	= reg.getString("RANGOPUBLIC")==null?"":reg.getString("RANGOPUBLIC");
					String rangopublicFin 	= reg.getString("RANGOPUBLICFIN")==null?"":reg.getString("RANGOPUBLICFIN");
					
					String rangopublicIni ="";

					if(rango.equals("0.01")){
						rangopublicIni = "1";
					}else{
						rangopublicIni =rango;
					}
			
					String Ran = "De $"+Comunes.formatoDecimal(rangopublicIni,2)+" hasta  $"+ Comunes.formatoDecimal(rangopublicFin,2); 

					pdfDoc.setCell(ranking,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(pyme,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nafine,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(sirac,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(rfc,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(telefono,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(razonsocial,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(moneda,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(Ran,"formas",ComunesPDF.LEFT);					
				}
				
				pdfDoc.addTable();
				pdfDoc.endDocument();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo ", e);
			} finally {
				log.info("crearPageCustomFile(S)");
			}
		}
		return nombreArchivo;
	}
	
	
	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */

	public String getAggregateCalculationQuery() {
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
	 
	  		
		qrySentencia.append(" SELECT COUNT(*) total_registros   /*+  use_nl(v rn p m c ) */ ");		 
		qrySentencia.append(" from vm_prov_publicacion v,");
		qrySentencia.append(" comrel_nafin rn,");
		qrySentencia.append(" comcat_pyme p, ");
		qrySentencia.append(" comcat_moneda m,  ");
		qrySentencia.append(" com_contacto c ");
		qrySentencia.append(" where v.ic_pyme = rn.ic_epo_pyme_if ");
		qrySentencia.append(" and p.ic_pyme = rn.ic_epo_pyme_if  ");
		qrySentencia.append(" and v.ic_moneda = m.ic_moneda  ");
		qrySentencia.append(" and c.ic_pyme = v.ic_pyme ");
		qrySentencia.append(" and c.cs_primer_contacto = 'S' ");
		qrySentencia.append(" and rn.cg_tipo = 'P'   ");
		
		if (!intermediario.equals("")){
				qrySentencia.append(" AND v.ic_if = ? ");
				conditions.add(intermediario);
			}
			
			if (!noEpo.equals("")){
				qrySentencia.append(" AND v.ic_epo = ? ");
				conditions.add(noEpo);
			}
			
			if (!moneda.equals("")){
				qrySentencia.append(" AND v.ic_moneda = ? ");
				conditions.add(moneda);
			}
		
		log.debug("getAggregateCalculationQuery "+conditions.toString());
		log.debug("getAggregateCalculationQuery "+qrySentencia.toString());

		return qrySentencia.toString();

	}
		
	 /**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery(){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
    
		qrySentencia.append("select   /*+  use_nl(v rn p m c ) */ ");		
		qrySentencia.append(" v.ic_pyme pyme,  ");
		qrySentencia.append("	v.CG_RANKING as ranking , ");
		qrySentencia.append(" rn.ic_nafin_electronico as nafinelectronico, ");
		qrySentencia.append(" p.cg_rfc as rfc, ");
		qrySentencia.append(" c.cg_tel as telefono,  ");
		qrySentencia.append(" p.cg_razon_social as razonsocialproveedor,  ");
		qrySentencia.append(" m.cd_nombre moneda, ");
		qrySentencia.append(" v.fn_monto_publicacion as montopublic ,  ");
		qrySentencia.append(" v.CG_RANGO_PUB as rangopublic , "); 
		qrySentencia.append(" v.CG_RANGO_PUBFIN as rangopublicFin   "); 
		
		qrySentencia.append(" from vm_prov_publicacion v,");
		qrySentencia.append(" comrel_nafin rn,");
		qrySentencia.append(" comcat_pyme p, ");
		qrySentencia.append(" comcat_moneda m,  ");
		qrySentencia.append(" com_contacto c ");
		qrySentencia.append(" where v.ic_pyme = rn.ic_epo_pyme_if ");
		qrySentencia.append(" and p.ic_pyme = rn.ic_epo_pyme_if  ");
		qrySentencia.append(" and v.ic_moneda = m.ic_moneda  ");
		qrySentencia.append(" and c.ic_pyme = v.ic_pyme ");
		qrySentencia.append(" and c.cs_primer_contacto = 'S' ");
		qrySentencia.append(" and rn.cg_tipo = 'P'   ");
  
			  
		if (!intermediario.equals("")){
				qrySentencia.append(" AND v.ic_if = ? ");
				conditions.add(intermediario);
			}
			
			if (!noEpo.equals("")){
				qrySentencia.append(" AND v.ic_epo = ? ");
				conditions.add(noEpo);
			}
			
			if (!moneda.equals("")){
				qrySentencia.append(" AND v.ic_moneda = ? ");
				conditions.add(moneda);
			}
			
			qrySentencia.append(" ORDER BY montopublic DESC ");

		
		log.debug("getDocumentQuery "+conditions.toString());
		log.debug("getDocumentQuery "+qrySentencia.toString());

		return qrySentencia.toString();
	}

	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
  
    qrySentencia.append("select   /*+  use_nl(v rn p m c ) */ ");		
		qrySentencia.append(" v.ic_pyme as  pyme,  ");
		qrySentencia.append("	v.CG_RANKING as ranking , ");
		qrySentencia.append(" rn.ic_nafin_electronico as nafinelectronico, ");
		qrySentencia.append(" p.cg_rfc as rfc, ");
		qrySentencia.append(" c.cg_tel as telefono,  ");
		qrySentencia.append(" p.cg_razon_social as razonsocialproveedor,  ");
		qrySentencia.append(" m.cd_nombre moneda, ");
		qrySentencia.append(" v.fn_monto_publicacion as montopublic ,  ");
		qrySentencia.append(" v.CG_RANGO_PUB as rangopublic , "); 
		qrySentencia.append(" v.CG_RANGO_PUBFIN as rangopublicFin,   ");
		qrySentencia.append(" to_char(p.in_numero_sirac) as in_numero_sirac ");		
		qrySentencia.append(" from vm_prov_publicacion v,");
		qrySentencia.append(" comrel_nafin rn,");
		qrySentencia.append(" comcat_pyme p, ");
		qrySentencia.append(" comcat_moneda m,  ");
		qrySentencia.append(" com_contacto c ");
		qrySentencia.append(" where v.ic_pyme = rn.ic_epo_pyme_if ");
		qrySentencia.append(" and p.ic_pyme = rn.ic_epo_pyme_if  ");
		qrySentencia.append(" and v.ic_moneda = m.ic_moneda  ");
		qrySentencia.append(" and c.ic_pyme = v.ic_pyme ");
		qrySentencia.append(" and c.cs_primer_contacto = 'S' ");
		qrySentencia.append(" and rn.cg_tipo = 'P'   ");

		 
	if (!intermediario.equals("")){
				qrySentencia.append(" AND v.ic_if = ? ");
				conditions.add(intermediario);
			}
			
			if (!noEpo.equals("")){
				qrySentencia.append(" AND v.ic_epo = ? ");
				conditions.add(noEpo);
			}
			
			if (!moneda.equals("")){
				qrySentencia.append(" AND v.ic_moneda = ? ");
				conditions.add(moneda);
			}
			
		qrySentencia.append("   AND ( ");
		
		for(int i = 0; i < pageIds.size(); i++){
			List lItem = (ArrayList)pageIds.get(i);
			if(i > 0){qrySentencia.append("  OR  ");}
				qrySentencia.append(" v.ic_pyme  = ?");
				conditions.add(new Long(lItem.get(0).toString()));      
			}
		qrySentencia.append(" ) ");
		
			
				qrySentencia.append(" ORDER BY montopublic DESC ");
                    
                    
		log.debug("getDocumentSummaryQueryForIds "+conditions.toString());
		log.debug(" getDocumentSummaryQueryForIds "+qrySentencia.toString());
	
		
		return qrySentencia.toString();
	}
	
/**
	 *genera el archivo 
	 * @return 
	 */
	public String getDocumentQueryFile(){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
    qrySentencia.append("select   /*+  use_nl(v rn p m c ) */ ");
	  qrySentencia.append(" v.ic_pyme pyme,  ");
		qrySentencia.append("	v.CG_RANKING as ranking , ");
		qrySentencia.append(" rn.ic_nafin_electronico as nafinelectronico, ");
		qrySentencia.append(" p.cg_rfc as rfc, ");
		qrySentencia.append(" c.cg_tel as telefono,  ");
		qrySentencia.append(" p.cg_razon_social as razonsocialproveedor,  ");
		qrySentencia.append(" m.cd_nombre moneda, ");
		qrySentencia.append(" v.fn_monto_publicacion as montopublic ,  ");
		qrySentencia.append(" v.CG_RANGO_PUB as rangopublic , "); 
		qrySentencia.append(" v.CG_RANGO_PUBFIN as rangopublicFin,  "); 
		qrySentencia.append(" p.in_numero_sirac ");		
		qrySentencia.append(" from vm_prov_publicacion v,");
		qrySentencia.append(" comrel_nafin rn,");
		qrySentencia.append(" comcat_pyme p, ");
		qrySentencia.append(" comcat_moneda m,  ");
		qrySentencia.append(" com_contacto c ");
		qrySentencia.append(" where v.ic_pyme = rn.ic_epo_pyme_if ");
		qrySentencia.append(" and p.ic_pyme = rn.ic_epo_pyme_if  ");
		qrySentencia.append(" and v.ic_moneda = m.ic_moneda  ");
		qrySentencia.append(" and c.ic_pyme = v.ic_pyme ");
		qrySentencia.append(" and c.cs_primer_contacto = 'S' ");
		qrySentencia.append(" and rn.cg_tipo = 'P'   ");
		
		
    if (!intermediario.equals("")){
				qrySentencia.append(" AND v.ic_if = ? ");
				conditions.add(intermediario);
			}
			
			if (!noEpo.equals("")){
				qrySentencia.append(" AND v.ic_epo = ? ");
				conditions.add(noEpo);
			}
			
			if (!moneda.equals("")){
				qrySentencia.append(" AND v.ic_moneda = ? ");
				conditions.add(moneda);
			}
			
		qrySentencia.append(" ORDER BY montopublic DESC ");
 
		log.debug("getDocumentQueryFile "+conditions.toString());
		log.debug("getDocumentQueryFile "+qrySentencia.toString());
		
		return qrySentencia.toString();
	}//getDocumentQueryFile
	

	/**
	 * Regresar el query para el calculo del monto de la publicacion vigente 
	 * @return <tt>String</tt> query para el calculo del monto de la publicacion vigente
	 */
	public String getMontoPublicacionVigente(){
		
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		qrySentencia.append(
			" select /*+  use_nl(v rn p m c ) */                    "  +
			"		sum(v.fn_monto_publicacion) publicacion_vigente   "  +		 
			" from                        "  + 
			"    vm_prov_publicacion 	v,	"  +
			"    comrel_nafin 			rn,"  +
			"    comcat_pyme 				p, "  +
			"    comcat_moneda 			m, "  +
			"    com_contacto 			c 	"  +
			" where                       "  +
			"    v.ic_pyme                = rn.ic_epo_pyme_if "  +
			"    and p.ic_pyme            = rn.ic_epo_pyme_if "  +
			"    and v.ic_moneda          = m.ic_moneda       "  +
			"    and c.ic_pyme            = v.ic_pyme         "  +
			"    and c.cs_primer_contacto = 'S'               "  +
			"    and rn.cg_tipo           = 'P'               "
		);
 
		qrySentencia.append(" and v.ic_if = ? ");
		conditions.add(intermediario);
 
		qrySentencia.append(" and v.ic_epo = ? ");
		conditions.add(noEpo);
 
		qrySentencia.append(" and v.ic_moneda = ? ");
		conditions.add(moneda);
  
		log.debug("getMontoPublicacionVigente "+conditions.toString());
		log.debug("getMontoPublicacionVigente "+qrySentencia.toString());
		
		return qrySentencia.toString();
		
	}//getDocumentQueryFile
	
/*****************************************************
	 GETTERS
*******************************************************/
 
	/**
	  Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
 	
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	
 
 

  public String getPaginar() {
    return paginar;
  }

  public void setPaginar(String paginar) {
    this.paginar = paginar;
  }

	public String getIntermediario() {
		return intermediario;
	}

	public void setIntermediario(String intermediario) {
		this.intermediario = intermediario;
	}

	public String getNoEpo() {
		return noEpo;
	}

	public void setNoEpo(String noEpo) {
		this.noEpo = noEpo;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

 
		
}