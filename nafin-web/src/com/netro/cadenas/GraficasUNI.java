package com.netro.cadenas;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class GraficasUNI  {

	private final static Log log = ServiceLocator.getInstance().getLog(GraficasUNI.class);
	
	public GraficasUNI() {
	}
	
	
	/**
	 * Obtiene la información especifica para armar la grafica de la UNI.
	 * @param claveUni Clave del UNI
	 * @param claveGrafica Clave de la grafica
	 */
	public List getInfoGraficaUni(String claveUni, String grafica) {
		log.info("getInfoGraficaUni(E)");
		AccesoDB con = new AccesoDB();
		int iClaveUni = 0;		
		List lstData = new ArrayList();		
		PreparedStatement ps = null;
		ResultSet rs = null;  
		String query = "";
		List listasGraficas = new ArrayList();

		//**********************************Validación de parametros:*****************************
		try {
			if (claveUni == null || claveUni.equals("")) {
				throw new Exception("Los parametros son requeridos");
			}
			iClaveUni = Integer.parseInt(claveUni);
			
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos",e);
		}
		//****************************************************************************************
		
		try{
		
			con.conexionDB();
		
			query = "SELECT b.ic_grafica_cache_det, b.cg_grafica, b.cg_atributo, b.cg_valor,b.cg_valorce " +
					"  FROM com_graficas_cache a, com_graficas_cache_det b " +
					" WHERE a.ic_afiliado = b.ic_afiliado " +
					"   AND a.ic_tipo_afiliado = b.ic_tipo_afiliado " +
					"	 AND a.cs_estatus <> 'ERR' " +
					"   AND b.cg_grafica in( "+grafica+" ) " +
					"   AND b.ic_afiliado = ? " +
					"   AND b.ic_tipo_afiliado = ? "+
					"   ORDER BY b.cg_atributo ";
			
			ps = con.queryPrecompilado(query);
			
			ps.setInt(1, iClaveUni);
			ps.setString(2,"U");
			rs = ps.executeQuery();
			
			
			while(rs.next()){
				HashMap mp = new HashMap();
				mp.put("CG_ATRIBUTO",rs.getString("CG_ATRIBUTO"));
				mp.put("CG_VALOR",rs.getString("CG_VALOR"));
				mp.put("CG_VALORCE",rs.getString("CG_VALORCE"));
				mp.put("CG_GRAFICA",rs.getString("CG_GRAFICA"));
				lstData.add(mp);				
			}
			rs.close();
			ps.close();
								
			return lstData;
			
		}catch(Throwable t){
			throw new AppException("Error al obtener informacion de Grafica", t);	
		}finally{
			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			
			log.info("getInfoGraficaUni(S)");
		}
		
	}



}