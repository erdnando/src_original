package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ContactosEntidad implements IQueryGeneratorRegExtJS {


//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ContactosEntidad.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String ic_pyme;
		
		 
	public ContactosEntidad() { }
	
	  
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();  
	
	
	qrySentencia.append(" select IC_CONTACTO AS IC_CONTACTO, "+
							  " CG_APPAT AS APELLIDO_PATERNO ,"+
							  " CG_APMAT AS APELLIDO_MATERNO , "+
							  " CG_NOMBRE AS NOMBRES, "+
							  " CG_TEL AS TELEFONO, "+
							  " CG_FAX AS FAX,  "+
							  " CG_EMAIL AS CORREO_ELECTRONICO,  "+	
							  " INHABILITADO  as INHABILITADO "+
							  " from  com_contacto  "+
							  " where CS_PRIMER_CONTACTO = ?   "+							  
							  " AND IC_PYME = ? "+
							  " ORDER BY   IC_CONTACTO ASC ");
				conditions.add("N");		  
				conditions.add(ic_pyme);		  
					
	
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
			
			
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		int total = 0;
		
		try {
			
		
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
	
	/**
	 * Metodo para guardar o modificar los contactos 
	 * @throws netropology.utilerias.AppException
	 * @param email
	 * @param fax
	 * @param telefono
	 * @param nombre
	 * @param ap_materno
	 * @param ap_paterno
	 * @param ic_contacto 
	 */
	public void guardaContactos( String ic_contacto[],  String ap_paterno[],String ap_materno[], String nombre[], String telefono[],String fax[],String email[], String ic_pyme  ) throws AppException {
		log.info(" guardaContactos(E)");
		
		boolean				lbSinError 		= true;
		AccesoDB 			con 		= new AccesoDB();
		PreparedStatement	ps;
		ResultSet			rs;				
		StringBuffer 	SQL = new StringBuffer();   
		List varBind = new ArrayList();
		try{
			con.conexionDB();
			
			SQL = new StringBuffer();  
					
			
			for(int i=0;i<ap_paterno.length;i++){  
				
				SQL = new StringBuffer();  
				varBind = new ArrayList();	
				
				System.out.println("ic_contacto[i] "+ic_contacto[i]);
				
				if(!ic_contacto[i].equals("")){  
					
					SQL.append(" UPDATE  COM_CONTACTO "+
					" SET  CG_APPAT  = ? ,  "+
					" CG_APMAT  = ?  , "+
					" CG_NOMBRE  = ?  , "+
					" CG_TEL  = ?  ,  "+
					" CG_FAX   = ?  ,  "+
					" CG_EMAIL   = ?    "+
					" WHERE 	IC_CONTACTO  = ?  ");	
					
					varBind.add(ap_paterno[i]);
					varBind.add(ap_materno[i]);
					varBind.add(nombre[i]);
					varBind.add(telefono[i]);
					varBind.add(fax[i]);
					varBind.add(email[i]);
					varBind.add(ic_contacto[i]);
									
				}else {
						
					String no_contacto =  getIc_Contacto();
						
					SQL.append(" INSERT INTO  COM_CONTACTO "+
					" 			( IC_CONTACTO , CG_APPAT ,  CG_APMAT ,  CG_NOMBRE ,  CG_TEL ,  CG_FAX  ,   CG_EMAIL , IC_PYME  )    "+
					" VALUES ( ? ,  ?,   ?,  ?,  ?,  ?,  ? , ?  )");		 			
					
					varBind.add(no_contacto);
					varBind.add(ap_paterno[i]);
					varBind.add(ap_materno[i]);
					varBind.add(nombre[i]);
					varBind.add(telefono[i]);
					varBind.add(fax[i]);
					varBind.add(email[i]);	
					varBind.add(ic_pyme);	
				}
				
				System.out.println("SQL "+SQL);
				System.out.println("varBind "+varBind);
				
				ps = con.queryPrecompilado(SQL.toString(), varBind);
				ps.executeUpdate();
				ps.close();	
				con.terminaTransaccion(true);
			}
			
					
		
		}catch(Throwable t ){
			lbSinError = false;
			throw new AppException("Error al guardar Contactos de Entidad  ", t);
		}finally{
			con.terminaTransaccion(lbSinError);
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}			
			log.info(" guardaContactos (S)");
		}
		
	}
	
	/**
	 * Metodo para Inhabilitar Contacto de la Pyme 
	 * @throws netropology.utilerias.AppException
	 * @param ic_contacto  
	 */
	public void inhabilitarContacto( String ic_contacto) throws AppException {
		log.info(" inhabilitarContacto(E)");
		
		boolean				lbSinError 		= true;
		AccesoDB 			con 		= new AccesoDB();
		PreparedStatement	ps;
		ResultSet			rs;				
		StringBuffer 	SQL = new StringBuffer();   
		List varBind = new ArrayList();
		try{
			con.conexionDB();
			
				SQL = new StringBuffer();  
				varBind = new ArrayList();	
				
				SQL.append(" UPDATE  COM_CONTACTO "+
					" SET INHABILITADO  = ?  "+
					" WHERE 	IC_CONTACTO  = ?  ");	
				
				varBind.add("S");
				varBind.add(ic_contacto);
				
				System.out.println("SQL "+SQL);
				System.out.println("varBind "+varBind);
				
				ps = con.queryPrecompilado(SQL.toString(), varBind);
				ps.executeUpdate();
				ps.close();					
					
		
		}catch(Throwable t ){
			lbSinError = false;
			throw new AppException("Error al inhabilitar Contacto de Entidad  ", t);
		}finally{
			con.terminaTransaccion(lbSinError);
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}			
			log.info(" inhabilitarContacto (S)");
		}
		
	}
	
	
	/**
	 *  Metodo que valida si la pyme opera Entidad Gobierno 
	 * @return 
	 */
	public String getValidaPymeEntidad(String ic_pyme  ){
	
      AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;      
      String   entidad  =  "N";
		StringBuffer 	SQL = new StringBuffer();   
		List varBind = new ArrayList();
		log.info("getValidaPymeEntidad (E)");		
		
		 try{
         con.conexionDB();
			
         SQL.append("SELECT  CS_ENTIDAD_GOBIERNO   "+
								" FROM  COMCAT_PYME  "+ 
								" WHERE IC_PYME =  ?   ");
			
			varBind.add(ic_pyme);
			
			log.debug("SQL " +SQL);
			log.debug("varBind " +varBind);
					
			ps = con.queryPrecompilado(SQL.toString(),varBind );	
         rs = ps.executeQuery();
         
         if(rs.next() && rs!=null){           
				entidad= (rs.getString("CS_ENTIDAD_GOBIERNO")==null?"N":rs.getString("CS_ENTIDAD_GOBIERNO"));
         }
			rs.close();
       	ps.close();      
			
			
			
      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  getValidaPymeEntidad (S)" +e);
         throw new RuntimeException(e);      
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}		
      } 
      log.info("getValidaPymeEntidad (S)");
      return entidad;
    }
	 
	 



	public String getIc_Contacto(){
	
      AccesoDB 	      con   =  new AccesoDB();
      ResultSet 	      rs	   =  null;
      PreparedStatement ps    =  null;      
      String   sContacto  =  "";
		StringBuffer 	SQL = new StringBuffer();   
		List varBind = new ArrayList();
		log.info("getIc_Contacto (E)");		
		
		 try{
         con.conexionDB();
			
         SQL.append("select nvl(max(ic_contacto),0)+1 from com_contacto  ");			
			
			log.debug("SQL " +SQL);
					
			ps = con.queryPrecompilado(SQL.toString() );	
         rs = ps.executeQuery();
         
         if(rs.next() && rs!=null){           
				sContacto = rs.getString(1);
         }
			rs.close();
       	ps.close();      
			
			
			
      }catch(Exception e){
         e.printStackTrace();
			con.terminaTransaccion(false);
			log.error("Error en  getIc_Contacto (S)" +e);
         throw new RuntimeException(e);      
      }finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}		
      } 
      log.info("getIc_Contacto (S)");
      return sContacto;
    }
	 
	 
	
	/**
	 Obtiene la lista de parámetros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getIc_pyme() {
		return ic_pyme;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}

	

		

}