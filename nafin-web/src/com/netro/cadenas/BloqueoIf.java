package com.netro.cadenas;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class BloqueoIf implements IQueryGeneratorRegExtJS {

	//Constructor
	public BloqueoIf() {
	}

		//Variable para enviar mensajes al log.
	private static final Log log = ServiceLocator.getInstance().getLog(BloqueoIf.class);

	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer 		qrySentencia;
	StringBuffer 		condicion;
	public List 		conditions;
	private String 	paginaOffset="";
	private String 	paginaNo="";
	private String directorio="";
  	public String cveProducto;
  	public String cveIf;
	public String acuses="";

	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */

	public String getAggregateCalculationQuery() {
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		condicion 		= new StringBuffer();
		String bandera;
		bandera=acuses.equals("")?" AND sp.ic_estatus_solic = 12 ": " AND sp.cc_acuse in (" + acuses + ") ";

		log.debug("-------------------------:: ");
		log.debug("cveProducto:: "+ cveProducto);
		log.debug("cveIf:: "+cveIf);
		log.debug("-------------------------:: ");

	  	qrySentencia.append(" SELECT COUNT(1) " +
									" FROM com_solic_portal sp, " +
									" 	comcat_if i, " +
									" 	comcat_moneda m " +
									" WHERE " +
									" 	sp.ic_if = i.ic_if " +
									" 	AND sp.ic_moneda = m.ic_moneda " +bandera
									/*" 	AND sp.ic_estatus_solic = 12 "*/);
									
        	try{

						if(!"".equals(cveIf))	{
							condicion.append("	AND sp.ic_if = ? ");
							conditions.add(cveIf);
						}
					
						qrySentencia.append(condicion);

						System.out.println("BloqueoIf::getAggregateCalculationQuery::qrySentencia:"+qrySentencia.toString());
					} catch(Exception e) {
						System.out.println("BloqueoIf::getAggregateCalculationQuery "+e);
			    	}
					log.debug(" :::: BloqueoIf:: getAggregateCalculationQuery (S) :::: ");
					log.debug(getConditions().toString());
		return qrySentencia.toString();

	}


	/**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery()	{ //genera todos los id's
	    	log.debug(" :::: BloqueoIf :: getDocumentQuery (E) :::: ");
			StringBuffer qrySentencia = new StringBuffer();
			conditions 		= new ArrayList();
	    	condicion = new StringBuffer();
			String bandera=acuses.equals("")?" AND sp.ic_estatus_solic = 12 ": " AND sp.cc_acuse in (" + acuses + ") ";

			log.debug("-------------------------:: ");
			log.debug("cveProducto:: "+ cveProducto);
			log.debug("cveIf:: "+cveIf);
			log.debug("-------------------------:: ");


			qrySentencia.append(" SELECT i.ic_if " +
									" FROM com_solic_portal sp, " +
									" 	comcat_if i, " +
									" 	comcat_moneda m " +
									" WHERE " +
									" 	sp.ic_if = i.ic_if " +
									" 	AND sp.ic_moneda = m.ic_moneda " +bandera
									/*" 	AND sp.ic_estatus_solic = 12 "*/);


			try{

				if(!"".equals(cveIf))	{
							condicion.append("	AND sp.ic_if = ? ");
							conditions.add(cveIf);
					}

				qrySentencia.append(condicion +" GROUP BY i.ic_if, i.cg_razon_social, sp.cc_acuse, m.cd_nombre " +
						" ORDER BY i.cg_razon_social ");

				log.debug("EL query de la llave primaria: "+qrySentencia.toString());
			} catch(Exception e) {
				log.debug("BloqueoIf::getDocumentQueryException "+e);
	    	}
			log.debug(" :::: BloqueoIf :: getDocumentQuery (S) :::: ");
			log.debug(getConditions());
			return qrySentencia.toString();
	}//getDocumentQuery


	public String getDocumentQueryFile() {

			System.out.println(" :::: BloqueoIf :: getDocumentQueryFile (E) :::: ");
			StringBuffer 	qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
	    	condicion 		= new StringBuffer();
			String 			camposNuevos 	= "";
			String bandera=acuses.equals("")?" AND sp.ic_estatus_solic = 12 ": " AND sp.cc_acuse in (" + acuses + ") ";

			log.debug("-------------------------:: ");
			log.debug("cveProducto:: "+ cveProducto);
			log.debug("cveIf:: "+cveIf);
			log.debug("-------------------------:: ");

			String tablaTipoAfiliado = "";
			String campoTipoAfiliado = "";
		
			qrySentencia.append(" SELECT " +
						" 	i.ic_if as ic_if, i.cg_razon_social as razon_social, sp.cc_acuse as acuse, " +
						" 	TO_CHAR(MIN(sp.df_carga), 'dd/mm/yyyy hh:Mi:ss') as df_carga, " +
						" 	m.cd_nombre as nombreMoneda, SUM(sp.fn_importe_dscto) as importeTotalDscto, " +
						" 	count(*) as numOperaciones " +
						" FROM com_solic_portal sp, " +
						" 	comcat_if i, " +
						" 	comcat_moneda m " +
						" WHERE " +
						" 	sp.ic_if = i.ic_if " +
						" 	AND sp.ic_moneda = m.ic_moneda " +bandera
						/*" 	AND sp.ic_estatus_solic = 12 "*/);

			try {

				if(!"".equals(cveIf))	{
							condicion.append("	AND sp.ic_if = ? ");
							conditions.add(cveIf);
					}

				qrySentencia.append(condicion +" GROUP BY i.ic_if, i.cg_razon_social, sp.cc_acuse, m.cd_nombre " +
						" ORDER BY i.cg_razon_social ");

				log.debug("BloqueoIf::getDocumentQueryFile::qrySentencia:"+qrySentencia.toString());
		    }catch(Exception e){
				log.debug("BloqueoIf::getDocumentQueryFileException "+e);
			}
			log.debug(" :::: BloqueoIf :: getDocumentQueryFile (S) :::: ");
			return qrySentencia.toString();
	}//getDocumentQueryFile

	/**
		 * Obtiene el query necesario para mostrar la información completa de
		 * una página a partir de las llaves primarias enviadas como parámetro
		 * @return Cadena con la consulta de SQL, para obtener la información
		 * 	completa de los registros con las llaves especificadas
	 */
	 //Checar la implementacion de este metodo
	public String getDocumentSummaryQueryForIds(List ids)  {
	  		log.debug(" :::: BloqueoIf :: getDocumentSummaryQueryForIds (E) :::: ");
			int i=0;
			StringBuffer qrySentencia = new StringBuffer();
			condicion 		= new StringBuffer();
			conditions 		= new ArrayList();
			String camposNuevos = "";
			String bandera=acuses.equals("")?" AND sp.ic_estatus_solic = 12 ": " AND sp.cc_acuse in (" + acuses + ") ";

			log.debug("-------------------------:: ");
			log.debug("cveProducto:: "+ cveProducto);
			log.debug("cveIf:: "+cveIf);
			log.debug("-------------------------:: ");

			StringBuffer clavesCombinaciones = new StringBuffer("");
			
		log.debug("*************************La lista de ids es: "+ids);
			for (Iterator it = ids.iterator(); it.hasNext();) {
				List lItem = (List)it.next();
				clavesCombinaciones.append("?,");
				//this.conditions.add(lItem.get(0));
			}
			clavesCombinaciones = clavesCombinaciones.delete(clavesCombinaciones.length()-1,clavesCombinaciones.length());

			qrySentencia.append(" SELECT " +
										" 	i.ic_if as ic_if, i.cg_razon_social as razon_social, sp.cc_acuse as acuse, " +
										" 	TO_CHAR(MIN(sp.df_carga), 'dd/mm/yyyy hh:Mi:ss') as df_carga, " +
										" 	m.cd_nombre as nombreMoneda, SUM(sp.fn_importe_dscto) as importeTotalDscto, " +
										" 	count(*) as numOperaciones " +
										" FROM com_solic_portal sp, " +
										" 	comcat_if i, " +
										" 	comcat_moneda m " +
										" WHERE " +
										" 	sp.ic_if = i.ic_if " +
										" 	AND sp.ic_moneda = m.ic_moneda " +bandera
										/*" 	AND sp.ic_estatus_solic = 12 "*/);

			//qrySentencia.append("   AND i.ic_if IN("+clavesCombinaciones+")");
			if(!cveIf.equals(""))	{
							condicion.append("	AND sp.ic_if = ? ");
							conditions.add(cveIf);
					}

			qrySentencia.append(condicion +" GROUP BY i.ic_if, i.cg_razon_social, sp.cc_acuse, m.cd_nombre " +
						" ORDER BY i.cg_razon_social ");

			log.debug("el query queda de la siguiente manera "+qrySentencia.toString());
			log.debug(this.getConditions().toString());
			log.debug(" :::: BloqueoIf :: getDocumentSummaryQueryForIds (S) :::: ");
			return qrySentencia.toString();
	}//getDocumentSummaryQueryForIds

	/**
	 * En este método se debe realizar la implementación de la generación de archivo
	 * con base en el resultset que se recibe como parámetro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generará el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){
		String nombreArchivo = "";
		log.debug("crearCustomFile (E)");
	
		return nombreArchivo;
	}

	/**
	 * En este método se debe realizar la implementación de la generación de archivo
	 * con base en el resultset que se recibe como parámetro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generará el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
		String nombreArchivo = "";
	
		return nombreArchivo;
	}


	/*****************************************************
		 GETTERS
	*******************************************************/

		/**
		  Obtiene la lista de parámetros a usar como variables bind en las condiciones de la consulta
		  @return Lista con los parametros de las condiciones
		 */
		public List getConditions() {  return conditions;  }
	  /**
		 * Obtiene el numero de pagina.
		 * @return Cadena con el numero de pagina
		 */
		public String getPaginaNo() { return paginaNo; 	}
	  /**
		 * Obtiene el offset de la pagina.
		 * @return Cadena con el offset de pagina
		 */
	public String getPaginaOffset() { return paginaOffset; 	}


	/*****************************************************
						 SETTERS
	*******************************************************/
		/**
		 * Establece el numero de pagina.
		 * @param  newPaginaNo Cadena con el numero de pagina
		 */
		public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }

	  /**
		 * Establece el offset de la pagina.
		 * @param newPaginaOffset Cadena con el offset de pagina
		 */
		public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}


		public String getDirectorio() {
			return directorio;
		}

		public void setDirectorio(String directorio) {
			this.directorio = directorio;
		}

	  public String getCve_producto() {
	      return cveProducto;
	    }

	    public void setCve_producto(String cve_producto) {
	      this.cveProducto = cve_producto;
	  }
	  
	  public String getCve_if() {
	      return cveIf;
	    }

	    public void setCve_if(String cve_if) {
	      this.cveIf = cve_if;
	  }
	  
	  public String getAcuses() {
	      return acuses;
	    }

	    public void setAcuses(String acuses) {
	      this.acuses = acuses;
	  }

}
