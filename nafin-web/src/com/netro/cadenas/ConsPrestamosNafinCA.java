package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsPrestamosNafinCA implements IQueryGeneratorReg , IQueryGeneratorRegExtJS{
 
private final static Log log = ServiceLocator.getInstance().getLog(ConsPrestamosNafinCA.class);


	StringBuffer 	qrySentencia;
	private List 	conditions;
	private String 	operacion;
	private String 	paginaOffset;
	private String 	paginaNo;
	
	private String 	rad_prestamos;
	private String 	txt_cliente;
	private String 	txt_num_prestamo;
	
	private String 	txt_ultimo_dia_mes_ultimo;
	private String 	txt_ultimo_dia_mes_penultimo;
	private String 	txt_ultimo_dia_mes_antepenultimo;
	
	//Constructor
	public ConsPrestamosNafinCA(){}
	
	/**
	 * metodo para calcular los totales 
	 * @return 
	 */
	public String getAggregateCalculationQuery() {
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		qrySentencia.append("SELECT /*+index(est ");
			if(txt_num_prestamo!=null&&!"".equals(txt_num_prestamo)) {
				qrySentencia.append("in_com_estado_cuenta_diario_02 ");
			}
			else if(txt_cliente!=null&&!"".equals(txt_cliente)){
				qrySentencia.append("in_com_estado_cuenta_diario_03 ");
			}
		qrySentencia.append(" ) use_nl(est tip)*/ " +
			"           est.ic_moneda, mon.cd_nombre nombre_moneda, COUNT (1) registros,"   +
			"           SUM (est.fn_saldo_insoluto+est.fn_interes_vigente+est.fn_interes_vencido+est.fn_mora100) saldo_insoluto,"   +
			"           'ConsPrestamosNafinCA::getAggregateCalculationQuery' origen"   +
			"     FROM com_estado_cuenta_diario est,"   +
			"          comcat_tipo_credito tip,"   +
			"          comcat_moneda mon"   +
			"    WHERE est.ig_codigo_sub_aplicacion = tip.ic_tipo_credito"   +
			"      AND est.ic_moneda = mon.ic_moneda"   +
			"      AND tip.cs_info_pago = ?");
		conditions.add("S");
		if(txt_cliente!=null&&!"".equals(txt_cliente)) {
			qrySentencia.append(" AND est.ig_codigo_cliente = ? ");
			conditions.add(txt_cliente);
		}
		if(txt_num_prestamo!=null&&!"".equals(txt_num_prestamo)) {
			qrySentencia.append(" AND est.ig_numero_prestamo = ? ");
			conditions.add(txt_num_prestamo);
		}
		qrySentencia.append(" GROUP BY est.ic_moneda, mon.cd_nombre ");
		
		log.debug("qrySentencia:  getAggregateCalculationQuery  "+qrySentencia);
		log.debug("conditions: "+conditions);
		
		return qrySentencia.toString();
	}//getAggregateCalculationQuery
	
	/**
	 * metodo para obtener las llaves primarias
	 * @return 
	 */
	public String getDocumentQuery(){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		qrySentencia.append("SELECT /*+index(est ");
			if(txt_num_prestamo!=null&&!"".equals(txt_num_prestamo)) {
				qrySentencia.append("in_com_estado_cuenta_diario_02 ");
			}
			else if(txt_cliente!=null&&!"".equals(txt_cliente)){
				qrySentencia.append("in_com_estado_cuenta_diario_03 ");
			}
		qrySentencia.append(" ) use_nl(est tip)*/ " +
			"       est.ig_codigo_empresa, est.ic_codigo_agencia, est.ig_codigo_sub_aplicacion, est.ig_numero_prestamo, " +
			"       'ConsPrestamosNafinCA::getDocumentQuery' origen " +
			"  FROM com_estado_cuenta_diario est, comcat_tipo_credito tip " +
			" WHERE est.ig_codigo_sub_aplicacion = tip.ic_tipo_credito AND tip.cs_info_pago = ? ");
		conditions.add("S");
		if(txt_cliente!=null&&!"".equals(txt_cliente)) {
			qrySentencia.append(" AND est.ig_codigo_cliente = ? ");
			conditions.add(txt_cliente);
		}
		if(txt_num_prestamo!=null&&!"".equals(txt_num_prestamo)) {
			qrySentencia.append(" AND est.ig_numero_prestamo = ? ");
			conditions.add(txt_num_prestamo);
		}
		qrySentencia.append(" ORDER BY est.ig_codigo_cliente, est.df_fecha_vencimiento ");
		
		log.debug("qrySentencia: getDocumentQuery  llaves ------------>  "+qrySentencia);
		log.debug("conditions: "+conditions);
		
		return qrySentencia.toString();
	}//getDocumentQuery
	
	/**
	 * metodo para obtener el los registros por paginacion  
	 * @return 
	 * @param pageIds
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		log.debug("pageIds: "+pageIds);
			
			
		qrySentencia.append("SELECT /*+index(est ");
		
		if(txt_num_prestamo!=null&&!"".equals(txt_num_prestamo)) {
				qrySentencia.append("in_com_estado_cuenta_diario_02 ");
			}
			else if(txt_cliente!=null&&!"".equals(txt_cliente)){
				qrySentencia.append("in_com_estado_cuenta_diario_03 ");
			}
		qrySentencia.append(" ) use_nl(est tip)*/ " +
			"       est.ig_codigo_cliente, est.ic_estado_cuenta, est.ig_codigo_cliente, est.cg_desc_cliente, est.ig_numero_prestamo, " +
			"       est.cg_desc_producto_banco, TO_CHAR (est.df_fecha_vencimiento, 'dd/mm/yyyy') df_fecha_vencimiento, " +
			"       est.fn_monto_inicial, (est.fn_saldo_insoluto + est.fn_interes_vigente + est.fn_interes_vencido + est.fn_mora100 ) as fn_saldo_insoluto, " +
			"       CASE " +
			"          WHEN NVL (est.fn_capital_vencido, 0) > 0 " +
			"             THEN est.ic_estado_cuenta " +
			"          ELSE NULL " +
			"       END vencido, 'ConsPrestamosNafinCA::getDocumentSummaryQueryForIds' origen, " +
			"       fn_hay_avisos_de_pago (est.ig_numero_prestamo) hay_avisos, " +
			"       fn_hay_avisos_de_pago_vencidos (est.ig_numero_prestamo) hay_avisos_vencidos, " +
			"       fn_hay_datos_estado_cuenta (est.ig_codigo_cliente, est.ig_numero_prestamo, TO_DATE (?, 'dd/mm/yyyy')) estado_ultimo_mes, " +
			"       fn_hay_datos_estado_cuenta (est.ig_codigo_cliente, est.ig_numero_prestamo, TO_DATE (?, 'dd/mm/yyyy')) estado_penultimo_mes, " +
			"       fn_hay_datos_estado_cuenta (est.ig_codigo_cliente, est.ig_numero_prestamo, TO_DATE (?, 'dd/mm/yyyy')) estado_antepenultimo_mes, " +
			"		  fn_hay_tabla_amortizacion (est.ig_numero_prestamo) hay_tabla_amortizacion, " +			
			" 		  '' as  MESULTIMO,  "+
			" 		  '' as  MESPENULTIMO,  "+
			" 		  '' as  MESANTEPENULTIMO "+		
			
			"  FROM com_estado_cuenta_diario est "+
			"  WHERE ");
		conditions.add(txt_ultimo_dia_mes_ultimo);
		conditions.add(txt_ultimo_dia_mes_penultimo);
		conditions.add(txt_ultimo_dia_mes_antepenultimo);
		
		for(int i=0;i<pageIds.size();i++) {
			List lItem = (ArrayList)pageIds.get(i);
				if(i>0) {
					qrySentencia.append("  OR  "); 
				}
				qrySentencia.append("  (est.ig_codigo_empresa = ? AND est.ic_codigo_agencia = ? "); 
				qrySentencia.append("   AND est.ig_codigo_sub_aplicacion = ? AND est.ig_numero_prestamo = ?) "); 
				conditions.add(new Long(lItem.get(0).toString()));
				conditions.add(new Long(lItem.get(1).toString()));
				conditions.add(new Long(lItem.get(2).toString()));
				conditions.add(new Long(lItem.get(3).toString()));
		}//for(int i=0;i<ids.size();i++)
		
		qrySentencia.append(" ORDER BY est.ig_codigo_cliente, est.df_fecha_vencimiento ");
		
		log.debug("qrySentencia: getDocumentSummaryQueryForIds   "+qrySentencia);
		log.debug("conditions: "+conditions);
		
		return qrySentencia.toString();
	}//getDocumentSummaryQueryForIds
	
	/**
	 * metodo para generar el Archivo  PDF o CSV con todos los registos generados por la consulta * 
	 * @return 
	 */
	public String getDocumentQueryFile(){
		
		conditions = new ArrayList();
		qrySentencia = new StringBuffer();
		
	qrySentencia.append("SELECT /*+index(est ");
		
		if(txt_num_prestamo!=null&&!"".equals(txt_num_prestamo)) {
				qrySentencia.append("in_com_estado_cuenta_diario_02 ");
			}
			else if(txt_cliente!=null&&!"".equals(txt_cliente)){
				qrySentencia.append("in_com_estado_cuenta_diario_03 ");
			}
		qrySentencia.append(" ) use_nl(est tip)*/ " +
			"       est.ic_estado_cuenta, est.ig_codigo_cliente, est.cg_desc_cliente, est.ig_numero_prestamo, " +
			"       est.cg_desc_producto_banco, TO_CHAR (est.df_fecha_vencimiento, 'dd/mm/yyyy') df_fecha_vencimiento, " +
			"       est.fn_monto_inicial, (est.fn_saldo_insoluto + est.fn_interes_vigente + est.fn_interes_vencido + est.fn_mora100 ) as fn_saldo_insoluto, " +
			"       CASE " +
			"          WHEN NVL (est.fn_capital_vencido, 0) > 0 " +
			"             THEN est.ic_estado_cuenta " +
			"          ELSE NULL " +
			"       END vencido, 'ConsPrestamosNafinCA::getDocumentSummaryQueryForIds' origen, " +
			"       fn_hay_avisos_de_pago (est.ig_numero_prestamo) hay_avisos, " +
			"       fn_hay_avisos_de_pago_vencidos (est.ig_numero_prestamo) hay_avisos_vencidos, " +
			"       fn_hay_datos_estado_cuenta (est.ig_codigo_cliente, est.ig_numero_prestamo, TO_DATE (?, 'dd/mm/yyyy')) estado_ultimo_mes, " +
			"       fn_hay_datos_estado_cuenta (est.ig_codigo_cliente, est.ig_numero_prestamo, TO_DATE (?, 'dd/mm/yyyy')) estado_penultimo_mes, " +
			"       fn_hay_datos_estado_cuenta (est.ig_codigo_cliente, est.ig_numero_prestamo, TO_DATE (?, 'dd/mm/yyyy')) estado_antepenultimo_mes, " +
			"		  fn_hay_tabla_amortizacion (est.ig_numero_prestamo) hay_tabla_amortizacion " +
			"  FROM com_estado_cuenta_diario est,  "+
			"  comcat_tipo_credito tip, "+
			"  comcat_moneda mon "+
			"  WHERE est.ig_codigo_sub_aplicacion = tip.ic_tipo_credito "+
      " AND est.ic_moneda = mon.ic_moneda "+
			"  AND tip.cs_info_pago = ? ");
		conditions.add(txt_ultimo_dia_mes_ultimo);
		conditions.add(txt_ultimo_dia_mes_penultimo);
		conditions.add(txt_ultimo_dia_mes_antepenultimo);
		conditions.add("S");
			if(txt_cliente!=null&&!"".equals(txt_cliente)) {
			qrySentencia.append(" AND est.ig_codigo_cliente = ? ");
			conditions.add(txt_cliente);
		}
		if(txt_num_prestamo!=null&&!"".equals(txt_num_prestamo)) {
			qrySentencia.append(" AND est.ig_numero_prestamo = ? ");
			conditions.add(txt_num_prestamo);
		}
		qrySentencia.append(" ORDER BY est.ig_codigo_cliente, est.df_fecha_vencimiento ");
		
		
		
		log.debug("qrySentencia: getDocumentQueryFile   "+qrySentencia);
		log.debug("conditions: "+conditions);
		
		
		return qrySentencia.toString();
	}//getDocumentQueryFile
	
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	
								
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		StringBuffer linea = new StringBuffer(1024);
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		
		
		
		
		if ("PDF".equals(tipo)) {
			try {
				
				EstadoCuentaPyme BeanEstadoCuenta = ServiceLocator.getInstance().lookup("EstadoCuentaPymeEJB", EstadoCuentaPyme.class);
				String 	fechaActual2 = "", mesUltimo= "" , mesPenultimo ="", mesAntePenultimo ="" ;
	
	
				List meses2 = BeanEstadoCuenta.getFechasAviso();
				fechaActual2 = ((List)meses2.get(0)).get(0).toString();						
							
		
				HttpSession session = request.getSession();

				String tipoUsuario = (String)session.getAttribute("strTipoUsuario");

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				
				System.out.println("nombreArchivo "+nombreArchivo);
				
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
		 if(!tipoUsuario.equals("NAFIN") ){
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
		 }else{
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),	" ",
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
		 
		 }
								
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				pdfDoc.addText("Resumen de Pr�stamos ","formas",ComunesPDF.CENTER);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				
				
				
				pdfDoc.setTable(9, 100);
				pdfDoc.setCell("Cliente","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Num. Pr�stamo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Programa ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto del Cr�dito Solicitado","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Total del Adeudo al  "+fechaActual,"celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estado de Cuenta por Pr�stamo","celda01",ComunesPDF.CENTER,3);
					
							
				double totalInsoluto = 0;
				
				while (rs.next()) {
					String cliente = (rs.getString("CG_DESC_CLIENTE") == null) ? "" : rs.getString("CG_DESC_CLIENTE");
					String prestamo  = (rs.getString("IG_NUMERO_PRESTAMO") == null) ? "" : rs.getString("IG_NUMERO_PRESTAMO");
					String programa = (rs.getString("CG_DESC_PRODUCTO_BANCO") == null) ? "" : rs.getString("CG_DESC_PRODUCTO_BANCO");
					String fechaVencimiento  = (rs.getString("DF_FECHA_VENCIMIENTO") == null) ? "" : rs.getString("DF_FECHA_VENCIMIENTO");
					String monto = (rs.getString("FN_MONTO_INICIAL") == null) ? "" : rs.getString("FN_MONTO_INICIAL");
					String saldoInsoluto = (rs.getString("FN_SALDO_INSOLUTO") == null) ? "" : rs.getString("FN_SALDO_INSOLUTO");
					totalInsoluto +=  (rs.getDouble("FN_SALDO_INSOLUTO"));
					
						for(int i=1;i<=3;i++) {
						List saldosFinMes = (List)meses2.get(i);
						boolean haySaldo = false;
						if(i == 1)      {  
							haySaldo=(rs.getString("ESTADO_ULTIMO_MES").equals("true")) ?true:false; 
							if(haySaldo){
								mesUltimo= saldosFinMes.get(1).toString();
							}
						} 	else if(i == 2) {  
							haySaldo=(rs.getString("ESTADO_PENULTIMO_MES").equals("true")) ?true:false; 
							if(haySaldo){
								mesPenultimo  = saldosFinMes.get(1).toString();
							}						
						} else if(i == 3) {  
							haySaldo=(rs.getString("ESTADO_ANTEPENULTIMO_MES").equals("true"))?true:false; 
							if(haySaldo){
								mesAntePenultimo = saldosFinMes.get(1).toString();
							}
						}
					}
					
					
							
					pdfDoc.setCell(cliente,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(prestamo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(programa,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(saldoInsoluto,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(mesUltimo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(mesPenultimo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(mesAntePenultimo,"formas",ComunesPDF.CENTER);
				}
				
				pdfDoc.setCell("TOTAL A PAGAR:","celda01",ComunesPDF.RIGHT,5);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(totalInsoluto,2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell(" ","formas",ComunesPDF.RIGHT,3);
				pdfDoc.addTable();
				pdfDoc.endDocument();
				 
				

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		}
		
		return nombreArchivo;
	}
	
	//GETERS
	public List getConditions() {
		return conditions;
	}
	public String getOperacion() {
		return operacion;
	}
	public String getPaginaNo() {
		return paginaNo;
	}
	public String getPaginaOffset() {
		return paginaOffset;
	}
	public String getRad_prestamos() {
		return rad_prestamos;
	}
	public String getTxt_cliente() {
		return txt_cliente;
	}
	public String getTxt_num_prestamo() {
		return txt_num_prestamo;
	}
	
	public String 	getTxt_ultimo_dia_mes_ultimo(){
		return txt_ultimo_dia_mes_ultimo ;
	}
	public String 	getTxt_ultimo_dia_mes_penultimo(){
		return txt_ultimo_dia_mes_penultimo ;
	}
	public String 	getTxt_ultimo_dia_mes_antepenultimo(){
		return txt_ultimo_dia_mes_antepenultimo ;
	}
	
	//SETERS
	public void setOperacion(String newOperacion) {
		operacion = newOperacion;
	}
	public void setPaginaNo(String newPaginaNo) {
		paginaNo = newPaginaNo;
	}
	public void setPaginaOffset(String newPaginaOffset) {
		paginaOffset = newPaginaOffset;
	}
	public void setRad_prestamos(String newRad_prestamos){
		rad_prestamos = newRad_prestamos;
	}
	public void setTxt_cliente(String newTxt_cliente) {
		txt_cliente = newTxt_cliente;
	}
	public void setTxt_num_prestamo(String newTxt_num_prestamo) {
		txt_num_prestamo = newTxt_num_prestamo;
	}
	public void setTxt_ultimo_dia_mes_ultimo(String newTxt_ultimo_dia_mes_ultimo ){
		txt_ultimo_dia_mes_ultimo = newTxt_ultimo_dia_mes_ultimo;
	}
	public void setTxt_ultimo_dia_mes_penultimo(String newTxt_ultimo_dia_mes_penultimo ){
		txt_ultimo_dia_mes_penultimo = newTxt_ultimo_dia_mes_penultimo;
	}
	public void setTxt_ultimo_dia_mes_antepenultimo(String newTxt_ultimo_dia_mes_antepenultimo){
		txt_ultimo_dia_mes_antepenultimo = newTxt_ultimo_dia_mes_antepenultimo;
	}
		
}//ConsPrestamosNafinCA