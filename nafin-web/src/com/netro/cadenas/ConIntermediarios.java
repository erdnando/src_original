package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConIntermediarios implements IQueryGeneratorRegExtJS {


//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConIntermediarios.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String txtNumElectronico;
	private String txtNombre;
	private String sel_edo;
	private String convenio_unico;	
	private String interFinan;
	private String operaFactDistribuido;
		 
	public ConIntermediarios() { }
	
	  
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		qrySentencia.append( " SELECT DISTINCT  t1.ic_if AS IC_IF  "+
		   " FROM comcat_if t1,  "+
         "    com_domicilio t2,  "+
         "    comcat_estado t3,  "+
         "    comrel_nafin t4  "+
         " WHERE t1.ic_if = t2.ic_if   "+
			" AND t1.ic_if = t4.ic_epo_pyme_if  "+
         " AND t2.ic_estado = t3.ic_estado  "+           
         " AND t4.cg_tipo = 'I'  "+
         " AND t2.cs_fiscal = 'S'  ");
		
			if(interFinan.equals("FB")){
				qrySentencia.append("and t1.cs_tipo <>  ? ");	
				conditions.add("CO");			
			}else if(!interFinan.equals("")){
				qrySentencia.append("   AND t1.cs_tipo = ?   ");
				conditions.add(interFinan);
			}
			
			if ( !txtNumElectronico.equals("") ) {
				qrySentencia.append(" AND t4.ic_nafin_electronico = ? ");
				conditions.add(txtNumElectronico);
			}
		
			if(!txtNombre.equals("")){
				qrySentencia.append(" AND upper(trim(t1.cg_razon_social)) LIKE upper(?) ");
				conditions.add(txtNombre+"%");
			}
		
			if(!sel_edo.equals("")){
				qrySentencia.append(" AND t2.ic_estado = ?  ");
				conditions.add(sel_edo);
			}
			
			if(convenio_unico.equals("S")){
				qrySentencia.append(" AND t1.cs_convenio_unico = ? ") ;
				conditions.add(convenio_unico);
			}
						
			if ("S".equals(operaFactDistribuido)  ) {
				qrySentencia.append(" AND t1.CS_FACT_DISTRIBUIDO = ?"); 
				conditions.add(operaFactDistribuido);
			}
	 		
			qrySentencia.append("  ORDER BY t1.ic_if   ");
		
				
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		qrySentencia.append( " SELECT DISTINCT t1.ic_if AS IC_IF, "+
			" t4.ic_nafin_electronico AS NUM_NAFIN_ELEC, "+
         " decode (t1.cs_habilitado,'N','<font color=\"Blue\">*</font>','S',' ')||' '||t1.cg_razon_social   AS NOMBRE_IF,  "+
         " t2.cg_calle  || ' '  ||  t2.cg_numero_ext  || ' '  ||   t2.cg_numero_int || ' '  || t2.cg_colonia as DOMICILIO,   "+
         " t3.cd_nombre AS ESTADO, t2.cg_telefono1 AS TELEFONO,          "+       
         " decode(t1.cs_convenio_unico, 'S', 'SI', 'N', 'NO')  AS convenio_unico,   "+
			" t1.cs_habilitado " +
         " FROM comcat_if t1,  "+
         "    com_domicilio t2,  "+
         "    comcat_estado t3,  "+
         "    comrel_nafin t4  "+
         " WHERE t1.ic_if = t2.ic_if   "+
			" AND t1.ic_if = t4.ic_epo_pyme_if  "+
         " AND t2.ic_estado = t3.ic_estado  "+           
         " AND t4.cg_tipo = 'I'  "+
         " AND t2.cs_fiscal = 'S'  ");
		
			if(interFinan.equals("FB")){
				qrySentencia.append("and t1.cs_tipo <>  ? ");	
				conditions.add("CO");			
			}else if(!interFinan.equals("")){
				qrySentencia.append("   AND t1.cs_tipo = ?   ");
				conditions.add(interFinan);
			}
			
			if ( !txtNumElectronico.equals("") ) {
				qrySentencia.append(" AND t4.ic_nafin_electronico = ? ");
				conditions.add(txtNumElectronico);
			}
		
			if(!txtNombre.equals("")){
				qrySentencia.append(" AND upper(trim(t1.cg_razon_social)) LIKE upper(?) ");
				conditions.add(txtNombre+"%");
			}
			
			if(!sel_edo.equals("")){
				qrySentencia.append(" AND t2.ic_estado = ?  ");
				conditions.add(sel_edo);
			}
			
			if(convenio_unico.equals("S")){
				qrySentencia.append(" AND t1.cs_convenio_unico = ? ") ;
				conditions.add(convenio_unico);
			}
	 		
			if ("S".equals(operaFactDistribuido)  ) {
				qrySentencia.append(" AND t1.CS_FACT_DISTRIBUIDO = ?"); 
				conditions.add(operaFactDistribuido);
			}
			
			qrySentencia.append(" AND (");
			
			for (int i = 0; i < pageIds.size(); i++) { 
				List lItem = (ArrayList)pageIds.get(i);
				
				if(i > 0){qrySentencia.append("  OR  ");}
				
				qrySentencia.append(" t1.ic_if  =  ? " );
				conditions.add(new Long(lItem.get(0).toString()));
			}
			
			qrySentencia.append(" ) ");
			
			
			qrySentencia.append("  ORDER BY t1.ic_if   ");
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();  
	
		
		qrySentencia.append( " SELECT DISTINCT t1.ic_if AS IC_IF, "+
			" t4.ic_nafin_electronico AS NUM_NAFIN_ELEC, "+
         " decode (t1.cs_habilitado,'N','<font color=\"Blue\">*</font>','S',' ')||' '||t1.cg_razon_social  AS NOMBRE_IF,  "+
         " t2.cg_calle  || ' '  ||  t2.cg_numero_ext  || ' '  ||   t2.cg_numero_int || ' '  || t2.cg_colonia as DOMICILIO,   "+
         " t3.cd_nombre AS ESTADO, t2.cg_telefono1 AS TELEFONO,          "+       
         " decode(t1.cs_convenio_unico, 'S', 'SI', 'N', 'NO')  AS convenio_unico,   "+
			" t1.cs_habilitado " +
         " FROM comcat_if t1,  "+
         "    com_domicilio t2,  "+
         "    comcat_estado t3,  "+
         "    comrel_nafin t4  "+
         " WHERE t1.ic_if = t2.ic_if   "+
			" AND t1.ic_if = t4.ic_epo_pyme_if  "+
         " AND t2.ic_estado = t3.ic_estado  "+           
         " AND t4.cg_tipo = 'I'  "+
         " AND t2.cs_fiscal = 'S'  ");
		
			if(interFinan.equals("FB")){
				qrySentencia.append("and t1.cs_tipo <>  ? ");	
				conditions.add("CO");			
			}else if(!interFinan.equals("")){
				qrySentencia.append("   AND t1.cs_tipo = ?   ");
				conditions.add(interFinan);
			}
			
			if ( !txtNumElectronico.equals("") ) {
				qrySentencia.append(" AND t4.ic_nafin_electronico = ? ");
				conditions.add(txtNumElectronico);
			}
		
			if(!txtNombre.equals("")){
				qrySentencia.append(" AND upper(trim(t1.cg_razon_social)) LIKE upper(?) ");
				conditions.add(txtNombre+"%");
			}
		
			if(!sel_edo.equals("")){
				qrySentencia.append(" AND t2.ic_estado = ?  ");
				conditions.add(sel_edo);
			}
			
			if(convenio_unico.equals("S")){
				qrySentencia.append(" AND t1.cs_convenio_unico = ? ") ;
				conditions.add(convenio_unico);
			}
		
		if ("S".equals(operaFactDistribuido)  ) {
				qrySentencia.append(" AND t1.CS_FACT_DISTRIBUIDO = ?"); 
				conditions.add(operaFactDistribuido);
			}
		qrySentencia.append("  ORDER BY t1.ic_if   ");
	
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
			
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
			
			pdfDoc.setTable(7,100);
			pdfDoc.setCell("No. de intermediario financiero ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("N�mero de Nafin Electr�nico ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Nombre o Raz�n Social ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Domicilio ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Estado ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tel�fono ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Convenio �nico ","celda01",ComunesPDF.CENTER); 
			
			
			while (rs.next())	{		
				String ic_if = (rs.getString("IC_IF") == null) ? "" : rs.getString("IC_IF");	
				String num_nafin = (rs.getString("NUM_NAFIN_ELEC") == null) ? "" : rs.getString("NUM_NAFIN_ELEC");				
				String NOMBRE_IF = (rs.getString("NOMBRE_IF") == null) ? "" : rs.getString("NOMBRE_IF");
				String domicilio = (rs.getString("DOMICILIO") == null) ? "" : rs.getString("DOMICILIO");
				String estado = (rs.getString("ESTADO") == null) ? "" : rs.getString("ESTADO");
				String telefono = (rs.getString("TELEFONO") == null) ? "" : rs.getString("TELEFONO");
				String convenioUnico = (rs.getString("CONVENIO_UNICO") == null) ? "" : rs.getString("CONVENIO_UNICO");
				
				pdfDoc.setCell(ic_if,"formas",ComunesPDF.CENTER);			
				pdfDoc.setCell(num_nafin,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(NOMBRE_IF,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(domicilio,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(estado,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(telefono,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(convenioUnico,"formas",ComunesPDF.CENTER);
			}
		
			pdfDoc.addTable();
			pdfDoc.endDocument();	
				
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}  
	  
	  
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		int total = 0;
		
		try {
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);
		
			contenidoArchivo = new StringBuffer();		
			contenidoArchivo.append("No. de intermediario financiero,  N�mero de Nafin Electr�nico,  Nombre o Raz�n Social, Domicilio , Estado,  "+
											" Tel�fono, Convenio �nico  \n");			
		
			while (rs.next())	{	
			
				String ic_if = (rs.getString("IC_IF") == null) ? "" : rs.getString("IC_IF");	
				String num_nafin = (rs.getString("NUM_NAFIN_ELEC") == null) ? "" : rs.getString("NUM_NAFIN_ELEC");				
				String NOMBRE_IF = (rs.getString("NOMBRE_IF") == null) ? "" : rs.getString("NOMBRE_IF");
				String domicilio = (rs.getString("DOMICILIO") == null) ? "" : rs.getString("DOMICILIO");
				String estado = (rs.getString("ESTADO") == null) ? "" : rs.getString("ESTADO");
				String telefono = (rs.getString("TELEFONO") == null) ? "" : rs.getString("TELEFONO");
				String convenioUnico = (rs.getString("CONVENIO_UNICO") == null) ? "" : rs.getString("CONVENIO_UNICO");
				
			
				contenidoArchivo.append(ic_if.replace(',',' ')+", "+
											num_nafin.replace(',',' ')+", "+
											NOMBRE_IF.replace(',',' ')+", "+
											domicilio.replace(',',' ')+", "+
											estado.replace(',',' ')+", "+
											telefono.replace(',',' ')+", "+											
											convenioUnico.replace(',',' ')+"\n");		
			
				total++;
				if(total==1000){					
					total=0;	
					buffer.write(contenidoArchivo.toString());
					contenidoArchivo = new StringBuffer();//Limpio  
				}
				
			}//while(rs.next()){
				
			buffer.write(contenidoArchivo.toString());
			buffer.close();	
			contenidoArchivo = new StringBuffer();//Limpio    
			
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
	
	
	public String imprimirCuentas(HttpServletRequest request, List cuentas, String tituloF,  String tituloC, String path ) {  
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		try {
				
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				
			pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);
			pdfDoc.addText(tituloF,"formas",ComunesPDF.CENTER);
			pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);
			
			pdfDoc.setTable(1,30);			
			pdfDoc.setCell(tituloC, "formas",ComunesPDF.LEFT);  
			pdfDoc.setCell("Login del Usuario: ", "celda02",ComunesPDF.CENTER);	
			  
			
			Iterator itCuentas = cuentas.iterator();	
			while (itCuentas.hasNext()) {
				String cuenta = (String) itCuentas.next();		
				pdfDoc.setCell(cuenta, "formas",ComunesPDF.CENTER);	
			}	
			
			pdfDoc.addTable();
			pdfDoc.endDocument();
			
		 
			
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
	
	
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getTxtNumElectronico() {
		return txtNumElectronico;
	}

	public void setTxtNumElectronico(String txtNumElectronico) {
		this.txtNumElectronico = txtNumElectronico;
	}

	public String getTxtNombre() {
		return txtNombre;
	}

	public void setTxtNombre(String txtNombre) {
		this.txtNombre = txtNombre;
	}

	public String getSel_edo() {
		return sel_edo;
	}

	public void setSel_edo(String sel_edo) {
		this.sel_edo = sel_edo;
	}

	public String getConvenio_unico() {
		return convenio_unico;
	}

	public void setConvenio_unico(String convenio_unico) {
		this.convenio_unico = convenio_unico;
	}
	

	public String getInterFinan() {
		return interFinan;
	}

	public void setInterFinan(String interFinan) {
		this.interFinan = interFinan;
	}

	public void setOperaFactDistribuido(String operaFactDistribuido) {
		this.operaFactDistribuido = operaFactDistribuido;
	}

	public String getOperaFactDistribuido() {
		return operaFactDistribuido;
	}


}
