package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class RepDispersion implements IQueryGeneratorRegExtJS  {	
	
	private int numList = 1;
	private String 	ic_if;
	private String 	ic_epo;
	private String 	desde;
	public static final boolean SIN_COMAS = false;
		
	public RepDispersion() {}

	// Variable para enviar mensajes al log.
	private static final Log log = ServiceLocator.getInstance().getLog(ConsComision.class);
  
	public  StringBuffer strQuery;
	private List conditions = new ArrayList(); 
	
	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */
	public String getAggregateCalculationQuery() { // Obtiene los totales 
		conditions = new ArrayList();   
		StringBuffer strQuery = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		
		strQuery.append( 
		 "  SELECT   /*+index(doc IN_COM_DOCUMENTO_09_NUK) use_nl(doc sol crn c mon)*/ "+
      "	mon.ic_moneda, mon.cd_nombre, COUNT (1) total, SUM (nvl(doc.fn_monto_ant,doc.fn_monto)) suma "+
		 "  FROM com_documento doc, " +
		 "		 com_solicitud sol, " +
		 "		 comrel_nafin crn, " +
		 "		 com_cuentas c, " +
		 "		 comcat_moneda mon " +
		 "  WHERE doc.ic_documento = sol.ic_documento (+) " +
		 "	 AND crn.ic_epo_pyme_if = doc.ic_if " +
		 "	 AND crn.ic_nafin_electronico = c.ic_nafin_electronico " +
		 "	 AND doc.ic_epo = c.ic_epo " +
		 "	 AND c.cg_tipo_afiliado = crn.cg_tipo " +
		 "	 AND doc.ic_moneda = mon.ic_moneda " +
		 "	 AND doc.ic_estatus_docto IN (4, 11) " +
		 "	 AND c.ic_producto_nafin = 1 " +
		 "	 AND c.ic_tipo_cuenta = 40 " +
		 "	 AND c.ic_estatus_cecoban = 99 " +
		 "	 AND crn.cg_tipo = 'I' " +
		 "	 AND doc.ic_if = ? ");		 
     conditions.add(new Integer(ic_if));
		if(ic_epo!=null&&!"".equals(ic_epo)) {
			strQuery.append(" AND doc.ic_epo = ? " );
			conditions.add(new Integer(ic_epo));
      
		}
		if(desde!=null&&!"".equals(desde)) {
			strQuery.append(" AND doc.df_fecha_venc >= TO_DATE (?, 'dd/mm/yyyy')  ");
			conditions.add(desde);
		}
		if(desde!=null&&!"".equals(desde)) { 
			strQuery.append(" AND doc.df_fecha_venc < (TO_DATE (?, 'dd/mm/yyyy') + 1)  ");
			conditions.add(desde);
		}
		strQuery.append(" GROUP BY mon.ic_moneda, mon.cd_nombre  ");

		return strQuery.toString();
 	}  
	/**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */	 
	public String getDocumentQuery(){  // para las llaves primarias		
		conditions = new ArrayList();   
		StringBuffer strQuery = new StringBuffer();
		
    System.out.println("*****************getDocumentQuery**********************");
		strQuery.append(
				"  SELECT /*+index(doc IN_COM_DOCUMENTO_09_NUK) use_nl(doc sol crn c mon)*/ " + 
				"	   doc.ic_documento " + 
				"  FROM com_documento doc, " + 
				"	   com_solicitud sol, " + 
				"	   comrel_nafin crn, " + 
				"	   com_cuentas c, " + 
				"	   comcat_moneda mon " + 
				" WHERE doc.ic_documento = sol.ic_documento (+) " + 
				"   AND crn.ic_epo_pyme_if = doc.ic_if " + 
				"   AND crn.ic_nafin_electronico = c.ic_nafin_electronico " + 
				"   AND doc.ic_epo = c.ic_epo " + 
				"   AND c.cg_tipo_afiliado = crn.cg_tipo " + 
				"   AND doc.ic_moneda = mon.ic_moneda " + 
				"   AND doc.ic_estatus_docto IN (4, 11) " + 
				"   AND c.ic_producto_nafin = 1 " + 
				"   AND c.ic_tipo_cuenta = 40 " + 
				"   AND c.ic_estatus_cecoban = 99 " + 
				"   AND crn.cg_tipo = 'I' " + 
				"   AND doc.ic_if = ?" );		
      conditions.add(new Integer(ic_if));   
		if(ic_epo!=null&&!"".equals(ic_epo)) {
			strQuery.append(" AND doc.ic_epo = ? ");
			conditions.add(new Integer(ic_epo));
      
		}
		if(desde!=null&&!"".equals(desde)) {
			strQuery.append(" AND doc.df_fecha_venc >= TO_DATE (?, 'dd/mm/yyyy')  ");
			conditions.add(desde);
		}
		if(desde!=null&&!"".equals(desde)) {
			strQuery.append(" AND doc.df_fecha_venc < (TO_DATE (?, 'dd/mm/yyyy') + 1)  ");
			conditions.add(desde);
		}
		System.out.println("desde " + desde);
		System.out.println("ic_if "+ ic_if);
		System.out.println("ic_epo + " + ic_epo);
		System.out.println("strQuery.toString() :::::::::: " + strQuery.toString());
		return strQuery.toString();		
 	}  
	
	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */	  		
	public String getDocumentSummaryQueryForIds(List pageIds){ // paginacion 			
		conditions = new ArrayList();   
		StringBuffer strQuery = new StringBuffer();
		
		strQuery.append(
			"					SELECT pym.cg_razon_social nomproveedor, cpe.cg_pyme_epo_interno numproveedor, "+
			" 		doc.ig_numero_docto numtrans,"   +
			" 		TO_CHAR (doc.df_fecha_venc, 'dd/mm/yyyy') fechapago,"   +
			" 		nvl(doc.FN_MONTO_ANT,doc.FN_MONTO) montotrans, "   +
			" 		mon.cd_nombre moneda,"   +
			" 				TO_CHAR (doc.DF_FECHA_DOCTO, 'dd/mm/yyyy') fechfac,"   +
			" 		doc.fn_monto montofac,"   +
			" 		TO_CHAR (sol.df_fecha_solicitud, 'dd/mm/yyyy') fechaoperacion"   +
			"					  FROM com_documento doc, "+
			"						   com_solicitud sol, "+
			"						   comrel_pyme_epo cpe, "+
			"						   comcat_pyme pym, "+
			"					   comcat_moneda mon "+
			"					 WHERE doc.ic_documento = sol.ic_documento (+) "+
			"					   AND doc.ic_epo = cpe.ic_epo "+
			"					   AND doc.ic_pyme = cpe.ic_pyme "+
			"					   AND doc.ic_pyme = pym.ic_pyme "+
			"					   AND doc.ic_moneda = mon.ic_moneda "+
			"					   AND doc.ic_documento IN (");

		for(int i=0;i<pageIds.size();i++) {
			List lItem = (ArrayList)pageIds.get(i);
			strQuery.append("?,");
			conditions.add(new Long(lItem.get(0).toString()));
		}//for(int i=0;i<ids.size();i++)
		strQuery.setLength(strQuery.length()-1);
		strQuery.append(")");
		return strQuery.toString();
 	}  
	
	public String getDocumentQueryFile(){ // para todos los registros				
		conditions = new ArrayList();   
		StringBuffer strQuery = new StringBuffer();
		
		strQuery.append(
			"SELECT /*+index(doc) use_nl(doc sol cpe pym)*/ " +
/*01*/		"       pym.cg_razon_social nomproveedor,  " +
/*02*/		"       cpe.cg_pyme_epo_interno numproveedor,  " +
/*03*/		"       'FIDEICOMISO' site, " +
/*04*/		"       doc.ig_numero_docto numtrans,  " +
/*05*/		"       TO_CHAR (doc.df_fecha_venc, 'dd/mm/yyyy') fechapago,  " +
/*06*/		"       doc.fn_monto montocheque, " +
/*07*/		"       DECODE (doc.ic_moneda, 1, 'Moneda Nacional', 'D�lares') moneda,  " +
/*08*/		"       '' nulo, " +
/*09*/		"       doc.IG_NUMERO_DOCTO numfactura,  " +
/*10*/		"       TO_CHAR (doc.df_fecha_docto, 'dd/mm/yyyy') fechafactura, " +
/*11*/		"       doc.fn_monto montofactura,  " +
/*12*/		"       DECODE (doc.ic_moneda, 1, 'MXP', 'USD') moneda,  " +
/*13*/		"       doc.fn_monto montofactura, " +
/*14*/		"       '40001' ctapagadora,  " +
/*15*/		"       '012180001487577209' ctafideicomiso,  " +
/*16*/		"       doc.ig_numero_docto numtrans, " +
/*17*/		"       doc.ic_epo numorganismo,  " +
/*18*/		"       sol.cc_acuse numacuse, " +
/*19*/		"       TO_CHAR (sol.df_fecha_solicitud, 'dd/mm/yyyy') fechaoperacion " +
			"  FROM com_documento doc, com_solicitud sol, comrel_pyme_epo cpe, comcat_pyme pym " +
			" WHERE doc.ic_documento = sol.ic_documento(+) " +
			"   AND doc.ic_epo = cpe.ic_epo " +
			"   AND doc.ic_pyme = cpe.ic_pyme " +
			"   AND doc.ic_pyme = pym.ic_pyme " +
			"   AND doc.ic_if = ? ");
	conditions.add(new Integer(ic_if));

		if(ic_epo!=null&&!"".equals(ic_epo)) {
			strQuery.append(" AND doc.ic_epo = ? ");
			conditions.add(new Integer(ic_epo));      
		}
		if(desde!=null&&!"".equals(desde)) {
			strQuery.append(" AND doc.df_fecha_venc >= TO_DATE (?, 'dd/mm/yyyy')  ");
			conditions.add(desde);
		}
		if(desde!=null&&!"".equals(desde)) {
			strQuery.append(" AND doc.df_fecha_venc < (TO_DATE (?, 'dd/mm/yyyy') + 1)  ");
			conditions.add(desde);
		}
		return strQuery.toString();
 	} 
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	///para formar el csv o pdf de los todos los registros
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";  
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		CreaArchivo 	archivo 			= new CreaArchivo();

			if ("XLS".equals(tipo)) {
				try {
					int registros = 0;	
					while(rs.next()){
						if(registros==0) { 
							contenidoArchivo.append("Nombre del proveedor, Numero del proveedor, Numero de Transferencia, Fecha de pago, Montonto total de la transferencia, Moneda, Fecha de la factura, Monto de la factura, Fecha de operacion");
						}
						registros++;
						String nomproveedor = rs.getString("nomproveedor")==null?"":rs.getString("nomproveedor");
						String numproveedor = rs.getString("numproveedor")==null?"":rs.getString("numproveedor");
						String numtrans = rs.getString("numtrans")==null?"":rs.getString("numtrans");
						String fechapago = rs.getString("fechapago")==null?"":rs.getString("fechapago");
						String montocheque = rs.getString("montocheque")==null?"":rs.getString("montocheque");
						String moneda = rs.getString("moneda")==null?"":rs.getString("moneda");
						String fechafactura = rs.getString("fechafactura")==null?"":rs.getString("fechafactura");
						String montofactura = rs.getString("montofactura")==null?"":rs.getString("montofactura");
						String fechaoperacion = rs.getString("fechaoperacion")==null?"":rs.getString("fechaoperacion");
							
						contenidoArchivo.append("\n"+
							nomproveedor.replaceAll(",","") + "," +
							numproveedor + "," +
							numtrans + "," +
							fechapago.replaceAll("-","/") + "," +
							"$ " +  Comunes.formatoDecimal(montocheque, 2, false) + "," +
							moneda + "," +
							fechafactura.replaceAll("-","/") + "," +
							"$ " + Comunes.formatoDecimal(montofactura, 2, false) + "," +
							fechaoperacion.replaceAll("-","/") + ",");
					}
					
					if(registros >= 1){
						if(!archivo.make(contenidoArchivo.toString(),path, ".csv")){
							nombreArchivo="ERROR";
						}else{
							nombreArchivo = archivo.nombre;
						}
					}	 
				} catch (Throwable e) {
						throw new AppException("Error al generar el archivo", e);
				} finally {
					try {
						rs.close();
					} catch(Exception e) {}
				}
			}else if ("PDF".equals(tipo)) {
				try {
					HttpSession session = request.getSession();
					nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
					ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					
					String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
					String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
					String diaActual    = fechaActual.substring(0,2);
					String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
					String anioActual   = fechaActual.substring(6,10);
					String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
							
					pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					session.getAttribute("iNoNafinElectronico").toString(),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
							
					pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
					pdfDoc.setTable(9, 80);
					pdfDoc.setCell("Nombre del Proveedor","celda01",ComunesPDF.LEFT);
					pdfDoc.setCell("Numero del Proveedor","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Numero de Transferencia","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Pagado","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Total de la Transferencia","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de la Factura","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto de la Factura","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Operacion","celda01",ComunesPDF.CENTER);
													
					while (rs.next()) {
						String nombreproveedor = rs.getString("nomproveedor")==null?"":rs.getString("nomproveedor");
						String numeroproveedor = rs.getString("numproveedor")==null?"":rs.getString("numproveedor");
						String numtrans = rs.getString("numtrans")==null?"":rs.getString("numtrans");
						String fechapago = rs.getString("fechapago")==null?"":rs.getString("fechapago");
						String montocheque = rs.getString("montocheque")==null?"":rs.getString("montocheque");
						String moneda = rs.getString("moneda")==null?"":rs.getString("moneda");
						String fechafactura = rs.getString("fechafactura")==null?"":rs.getString("fechafactura");
						String montofactura = rs.getString("montofactura")==null?"":rs.getString("montofactura");
						String fechaoperacion = rs.getString("fechaoperacion")==null?"":rs.getString("fechaoperacion");
						 
						pdfDoc.setCell(nombreproveedor,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(numeroproveedor,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(numtrans,"formas",ComunesPDF.CENTER);		
						pdfDoc.setCell(fechapago,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(montocheque, 2, false),"formas",ComunesPDF.CENTER);		
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechafactura,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(montocheque, 2, false),"formas",ComunesPDF.CENTER);	
						pdfDoc.setCell(fechaoperacion,"formas",ComunesPDF.CENTER);				
					}
					pdfDoc.addTable();
					pdfDoc.endDocument();
				} catch(Throwable e) {
					throw new AppException("Error al generar el archivo", e);
				} finally {
					try {
						rs.close();
					} catch(Exception e) {}
				}
			}
		return nombreArchivo;	
	}	
	
	//para formar  csv o pdf por pagina  15 registros	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	
	public List getConditions() {
		return conditions;
	}


	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getIc_if() {
		return ic_if;
	}


	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}


	public String getIc_epo() {
		return ic_epo;
	}


	public void setDesde(String desde) {
		this.desde = desde;
	}


	public String getDesde() {
		return desde;
	}


}