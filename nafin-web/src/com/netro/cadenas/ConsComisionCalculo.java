package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Iva;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsComisionCalculo implements IQueryGeneratorRegExtJS {


//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsComisionCalculo.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String ic_if;
	private String ic_producto_nafin;
	private String ic_mes_calculo_de;
	private String ic_mes_calculo_a;
	private String anio_calculo;
	private String ic_moneda;
	private String cg_estatus_pagado;
	double  valorIva =0;
	private String porcentajeIva;
	private String condIF;
	private String bindIF;
	
	public ConsComisionCalculo() { }
	
	  
	/**
	 * Metodo que obtiene los montos 
	 * @return 
	 */
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
			
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	/**
	 * Metodo que saca las llavez primarias 
	 * @return 
	 */
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
				
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();  

	
		  String fecha   = "01/"+ ic_mes_calculo_a + "/" + anio_calculo; // formato fecha: DD/MM/YYYY
		  String porcentajeIva   = Iva.getPorcentaje(fecha);
		  double valorIva = ((BigDecimal)Iva.getValor(fecha)).doubleValue();
	

		qrySentencia.append(	" SELECT i.ic_if, cfd.ic_producto_nafin as  IC_PRODUCTO, "+
				"  pn.ic_nombre as PRODUCTO  ,  " +
				" 	cfd.ic_mes_calculo as DESCRIPCION_MES , "+ 
				"  pcf.cg_estatus_pagado as ESTATUS  , " +
				" 	i.cg_razon_social as NOMBRE_IF , " +
				" 	SUM(cfd.fg_saldo_promedio) as SALDO_PROMEDIO,  " +
				" 	SUM(cfd.fg_contraprestacion) as CONTRAPRESTACION, " +
				" '' AS PORCENTAJE_IVA , "+
				" '' AS ANIO, "+				
			  " 	SUM(cfd.fg_contraprestacion) * " + String.valueOf(valorIva) +" AS MONTO_IVA, " +//Fodea 01 tasa Iva
			  " 	SUM(cfd.fg_contraprestacion) * " + String.valueOf(1 + valorIva) + " AS MONTO_TOTAL " +//Fodea 01 tasa iva
				" FROM com_comision_fondeo_det cfd, comcat_producto_nafin  pn, " +
				" 	com_pago_comision_fondeo pcf, comcat_if i " +
				" WHERE cfd.ic_producto_nafin = pn.ic_producto_nafin " +
				" 	AND cfd.ic_if = i.ic_if " +
				" 	AND cfd.ic_producto_nafin = pcf.ic_producto_nafin " +
				" 	AND cfd.ic_if = pcf.ic_if " +
				" 	AND cfd.ic_mes_calculo = pcf.ic_mes_calculo " +
				" 	AND cfd.ic_anio_calculo = pcf.ic_anio_calculo " +													
				"	AND cfd.ic_moneda = pcf.ic_moneda " +
            "	AND cfd.ic_banco_fondeo = pcf.ic_banco_fondeo "  );
				
				if (!ic_if.equals("")) {
					qrySentencia.append(" and 	cfd.ic_if = ?    ");
					conditions.add(ic_if);
				}
				
				
				if (!anio_calculo.equals("")) {
					qrySentencia.append(" 	AND cfd.ic_anio_calculo = ?    ");
					conditions.add(anio_calculo);
				}
				
				if (!ic_moneda.equals("")) {
					qrySentencia.append(" AND cfd.ic_moneda = ?   ");
					conditions.add(ic_moneda);
				}
				
				if (!ic_mes_calculo_de.equals("") && !ic_mes_calculo_de.equals("")) {
					qrySentencia.append(" AND cfd.ic_mes_calculo BETWEEN 	?   AND  ?   ");
					conditions.add(ic_mes_calculo_de);
					conditions.add(ic_mes_calculo_a);
				}
				if (!ic_producto_nafin.equals("")) {
					qrySentencia.append(" AND cfd.ic_producto_nafin = ?  ");
					conditions.add(ic_producto_nafin);
				}
				if (!cg_estatus_pagado.equals("")) {
					qrySentencia.append(" AND pcf.cg_estatus_pagado = ?  ");
					conditions.add(cg_estatus_pagado);
				}
		
		
		qrySentencia.append(" GROUP BY cfd.ic_producto_nafin, pn.ic_nombre, i.cg_razon_social, " +
				" 	cfd.ic_mes_calculo, pcf.cg_estatus_pagado, i.ic_if " +
				" ORDER BY cfd.ic_producto_nafin, 	i.cg_razon_social ");
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		
		if(condIF == null){
			log.debug(" ");
		}else if(condIF.equals("1")){
		 qrySentencia = new StringBuffer("");
		 conditions 		= new ArrayList();
		 qrySentencia.append(
			"select ccf.IC_EPO,ce.cg_razon_social as epo, ci.cg_razon_social as nombre_if ,cpn.IC_NOMBRE AS producto,  ccf.IC_IF, ccf.IC_MES_CALCULO descripcion_mes, ccf.IC_ANIO_CALCULO as anio, ccf.FG_SALDO_PROMEDIO AS saldo_promedio, ccf.FG_CONTRI_COMISION AS contraprestacion, '' AS PORCENTAJE_IVA, ( ccf.fg_contri_comision * 0."+porcentajeIva+") AS monto_iva,  ( ccf.fg_contri_comision * 1."+porcentajeIva+") as MONTO_TOTAL, cpcf.CG_ESTATUS_PAGADO AS estatus "+
			" from com_comision_fondeo ccf, com_pago_comision_fondeo cpcf, com_comision_fondeo_det ccfd, comcat_if ci, comcat_producto_nafin cpn,comcat_epo ce "+
			
			" where ccf.IC_IF = cpcf.IC_IF "+
			" and ccf.IC_PRODUCTO_NAFIN = cpcf.IC_PRODUCTO_NAFIN "+
			" and ccf.IC_MES_CALCULO = cpcf.IC_MES_CALCULO "+
			" and ccf.IC_ANIO_CALCULO = cpcf.IC_ANIO_CALCULO "+
			" and ccf.IC_MONEDA = cpcf.IC_MONEDA "+
			" and ccf.IC_BANCO_FONDEO = cpcf.IC_BANCO_FONDEO "+
			
			" and ccf.IC_IF = ccfd.IC_IF "+
			" and ccf.IC_PRODUCTO_NAFIN = ccfd.IC_PRODUCTO_NAFIN "+
			" and ccf.IC_MES_CALCULO = ccfd.IC_MES_CALCULO "+
			" and ccf.IC_ANIO_CALCULO = ccfd.IC_ANIO_CALCULO "+
			" and ccf.IC_MONEDA = ccfd.IC_MONEDA "+
			" and ccf.IC_BANCO_FONDEO = ccfd.IC_BANCO_FONDEO "+
			
			" and ccf.IC_IF = ci.IC_IF "+
			" and ccf.IC_PRODUCTO_NAFIN = cpn.IC_PRODUCTO_NAFIN "+
			
			" and ccf.ic_epo = ce.ic_epo "+
			
			
			" and ccf.ic_anio_calculo=? "+
			" and ccf.ic_mes_calculo between ? and ? "+
			" and ccf.ic_moneda=? "+
			" and ccf.ic_banco_fondeo=1 "+
			" and ccf.ic_producto_nafin in (1,2,4,5) "+
			" and ccf.ic_if in ("+bindIF+")"); 
			conditions.add(anio_calculo);
			conditions.add(ic_mes_calculo_de);
			conditions.add(ic_mes_calculo_a);
			conditions.add(ic_moneda);
			if (!ic_producto_nafin.equals("")) {
					qrySentencia.append(" AND ccf.ic_producto_nafin = ?  ");
					conditions.add(ic_producto_nafin);
				}
			if (!cg_estatus_pagado.equals("")) {
				qrySentencia.append(" AND cpcf.cg_estatus_pagado = ?  ");
				conditions.add(cg_estatus_pagado);
			}
			log.info("qrySentencia  "+qrySentencia);
			log.info("conditions "+conditions);
		}
		
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
		
		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}  
	  
	  
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		int total = 0;
		
		List meses = new ArrayList();
		meses.add("Enero"); meses.add("Febrero"); meses.add("Marzo");
		meses.add("Abril"); meses.add("Mayo"); meses.add("Junio");
		meses.add("Julio"); meses.add("Agosto"); meses.add("Septiembre");
		meses.add("Octubre"); meses.add("Noviembre"); meses.add("Diciembre");
		String productoAnterior ="", nombreIFAnterior= "";
		double dblTotal2SaldoPromedioMN = 0;
		double dblTotal2ContraprestacionMN = 0;
		double dblTotal2ContraprestacionTotalMN = 0;
	
		double dblTotalSaldoPromedioMN = 0;
		double dblTotalContraprestacionMN = 0;
		double dblTotalContraprestacionTotalMN = 0;
		String despMoneda =  ic_moneda.equals("1")?"M.N.":"DOLARES";
		
		
		try {
		
			if(tipo.equals("CSV"))  {
			
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
				buffer = new BufferedWriter(writer);
				contenidoArchivo = new StringBuffer();						
				
				while (rs.next()){
						
					String  producto  =  rs.getString("PRODUCTO")==null?"":rs.getString("PRODUCTO");		
					String  nombreIF  =  rs.getString("NOMBRE_IF")==null?"":rs.getString("NOMBRE_IF");							
					String  saldo_promedio  =  rs.getString("SALDO_PROMEDIO")==null?"":rs.getString("SALDO_PROMEDIO");		
					String  contraprestacion  =  rs.getString("CONTRAPRESTACION")==null?"":rs.getString("CONTRAPRESTACION");		
					String  porcentajeIVa  =  rs.getString("PORCENTAJE_IVA")==null?"":rs.getString("PORCENTAJE_IVA");		
					String  montoIva  =  rs.getString("MONTO_IVA")==null?"":rs.getString("MONTO_IVA");		
					String  montoTotal  =  rs.getString("MONTO_TOTAL")==null?"":rs.getString("MONTO_TOTAL");		
					String  estatus  =  rs.getString("ESTATUS")==null?"":rs.getString("ESTATUS");	
					String  strMes  =  rs.getString("DESCRIPCION_MES")==null?"":rs.getString("DESCRIPCION_MES");	
					String  epo		="";
					boolean bandera=false;
					try{
						epo	= rs.getString("EPO")==null?"":rs.getString("EPO");
						bandera=true;
					} catch(Exception e){
						
					}
					if(estatus.equals("N")){
						estatus="No Pagado";
					} else {
						estatus="Pagado";
					}
				
					if (!productoAnterior.equals(producto)){
					
						productoAnterior= producto;
						contenidoArchivo.append("Producto: " + producto + "\n");
						if(bandera){
							contenidoArchivo.append(",Epo,Mes,A�o,Saldo Promedio,Contraprestaci�n,% IVA,Monto de IVA,TOTAL,Estatus\n");
						} else {
							contenidoArchivo.append(",Mes,A�o,Saldo Promedio,Contraprestaci�n,% IVA,Monto de IVA,TOTAL,Estatus\n");
						}
					}
					if(!nombreIFAnterior.equals(nombreIF)){
						nombreIFAnterior= nombreIF;
						contenidoArchivo.append("IF: " + nombreIF.replace(',', ' ') + "\n");								
					}
					if(bandera){
						contenidoArchivo.append(","+epo.replace(',',' ')+",");
					}else{
						contenidoArchivo.append(",");
					}
					contenidoArchivo.append(meses.get(Integer.parseInt(strMes) - 1) +
						"," + anio_calculo + 
						"," + Comunes.formatoDecimal(saldo_promedio,2,false) + 
						"," + Comunes.formatoDecimal(contraprestacion,2,false) +				
						 "," + porcentajeIva + "," + Comunes.formatoDecimal(montoIva, 2,false) +//ENERO 2010 ACF
						"," + Comunes.formatoDecimal(montoTotal, 2,false) + 
						"," + estatus + "\n");
				
				
				
					total++;
					if(total==1000){					
						total=0;	
						buffer.write(contenidoArchivo.toString());
						contenidoArchivo = new StringBuffer();//Limpio  
					}
				
				}//while(rs.next()){				
				
				buffer.write(contenidoArchivo.toString());
				buffer.close();	
				contenidoArchivo = new StringBuffer();//Limpio   
				
			}else  if(tipo.equals("PDF"))  {
				
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String mese[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = mese[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
							
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				
				if(condIF == null){
					pdfDoc.setTable(8,100);
				} else {
					pdfDoc.setTable(9,100);
				}
				while (rs.next()){
						total++;
					String  producto  =  rs.getString("PRODUCTO")==null?"":rs.getString("PRODUCTO");		
					String  nombreIF  =  rs.getString("NOMBRE_IF")==null?"":rs.getString("NOMBRE_IF");		
					String  anio  =  rs.getString("ANIO")==null?"":rs.getString("ANIO");		
					String  saldo_promedio  =  rs.getString("SALDO_PROMEDIO")==null?"":rs.getString("SALDO_PROMEDIO");		
					String  contraprestacion  =  rs.getString("CONTRAPRESTACION")==null?"":rs.getString("CONTRAPRESTACION");		
					String  porcentajeIVa  =  rs.getString("PORCENTAJE_IVA")==null?"":rs.getString("PORCENTAJE_IVA");		
					String  montoIva  =  rs.getString("MONTO_IVA")==null?"":rs.getString("MONTO_IVA");		
					String  montoTotal  =  rs.getString("MONTO_TOTAL")==null?"":rs.getString("MONTO_TOTAL");		
					String  estatus  =  rs.getString("ESTATUS")==null?"":rs.getString("ESTATUS");	
					String  strMes  =  rs.getString("DESCRIPCION_MES")==null?"":rs.getString("DESCRIPCION_MES");	
					String  mes =  meses.get(Integer.parseInt(strMes) - 1)+"";//+anio_calculo;		
					
					if(estatus.equals("N")){
						estatus="No Pagado";
					} else {
						estatus="Pagado"; 
					}
					String  epo="";
					try {
						epo=rs.getString("EPO")==null?"":rs.getString("EPO");	
					} catch(Exception e){ 
						
					}
					
					if (total > 0) {
						if(total == 1) {
							pdfDoc.setCell(" Producto ","celda01",ComunesPDF.CENTER);		
							if(condIF == null){
								pdfDoc.setCell(producto,"celda01",ComunesPDF.LEFT, 7);									
							} else {
								pdfDoc.setCell(producto,"celda01",ComunesPDF.LEFT, 8);									
							}
							
						}					
					}
					boolean nvl=false;
					if (!productoAnterior.equals(producto)){
						if(condIF == null){ 
						
						} else {
							nvl=true;
						}	
						productoAnterior= producto;
					}
					
					
					if(!nombreIFAnterior.equals(nombreIF)){									
						
						if(!nombreIFAnterior.equals(nombreIF) && total>1){	 										
							if(condIF == null){
								pdfDoc.setCell("Total "+ despMoneda , "celda01",ComunesPDF.RIGHT, 2);	
							}else{
								pdfDoc.setCell("Total "+ despMoneda , "celda01",ComunesPDF.RIGHT, 3);	
							}
							
							pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalSaldoPromedioMN, 2) , "formas",ComunesPDF.RIGHT);							
							pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalContraprestacionMN, 2) , "formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("" , "formas",ComunesPDF.CENTER);	
							pdfDoc.setCell("" , "formas",ComunesPDF.CENTER);							
							pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalContraprestacionTotalMN, 2) , "formas",ComunesPDF.RIGHT);	
							pdfDoc.setCell(" " , "formas",ComunesPDF.CENTER);
							
							dblTotalSaldoPromedioMN = 0;
							dblTotalContraprestacionMN = 0;
							dblTotalContraprestacionTotalMN = 0;
						}
						
						nombreIFAnterior= nombreIF;		
						
						
						if(condIF == null){
							pdfDoc.setCell(nombreIF,"celda01",ComunesPDF.CENTER, 8);		
						} else {
							pdfDoc.setCell(nombreIF,"celda01",ComunesPDF.CENTER, 9);	
							pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
						}
						pdfDoc.setCell(" Mes","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(" A�o","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(" Saldo Promedio","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(" Contraprestaci�n","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(" % IVA","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(" Monto de IVA","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(" TOTAL","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(" Estatus","celda01",ComunesPDF.CENTER);
						 
					} else if(condIF != null){
							pdfDoc.setCell("Total "+ despMoneda , "celda01",ComunesPDF.RIGHT, 3);	
							pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalSaldoPromedioMN, 2) , "formas",ComunesPDF.RIGHT);							
							pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalContraprestacionMN, 2) , "formas",ComunesPDF.RIGHT);
							pdfDoc.setCell("" , "formas",ComunesPDF.CENTER);	
							pdfDoc.setCell("" , "formas",ComunesPDF.CENTER);							
							pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalContraprestacionTotalMN, 2) , "formas",ComunesPDF.RIGHT);	
							pdfDoc.setCell(" " , "formas",ComunesPDF.CENTER);
							
							if (nvl){
								pdfDoc.setCell(" Producto ","celda01",ComunesPDF.CENTER);						
								pdfDoc.setCell(producto,"celda01",ComunesPDF.LEFT, 8);
							}
							dblTotalSaldoPromedioMN = 0;
							dblTotalContraprestacionMN = 0;
							dblTotalContraprestacionTotalMN = 0;	
							pdfDoc.setCell(nombreIF,"celda01",ComunesPDF.CENTER, 9);	
							pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);						
							pdfDoc.setCell(" Mes","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell(" A�o","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell(" Saldo Promedio","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell(" Contraprestaci�n","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell(" % IVA","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell(" Monto de IVA","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell(" TOTAL","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell(" Estatus","celda01",ComunesPDF.CENTER);
					}
						if(condIF == null){
							
						} else {
							pdfDoc.setCell(epo, "formas",ComunesPDF.CENTER);
						}
						
						pdfDoc.setCell(mes, "formas",ComunesPDF.CENTER);		
						pdfDoc.setCell(anio_calculo , "formas",ComunesPDF.CENTER);		
						pdfDoc.setCell("$"+ Comunes.formatoDecimal(saldo_promedio,2), "formas",ComunesPDF.RIGHT);		
						pdfDoc.setCell("$"+Comunes.formatoDecimal(contraprestacion,2) , "formas",ComunesPDF.RIGHT);					
						pdfDoc.setCell( porcentajeIva+"%"  , "formas",ComunesPDF.CENTER);	
						pdfDoc.setCell("$" + Comunes.formatoDecimal(montoIva, 2) , "formas",ComunesPDF.RIGHT);		
						pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTotal, 2) , "formas",ComunesPDF.RIGHT);	
						pdfDoc.setCell(estatus , "formas",ComunesPDF.CENTER);	
	
					
					dblTotal2SaldoPromedioMN += Double.parseDouble((String)saldo_promedio);
					dblTotal2ContraprestacionMN += Double.parseDouble((String)contraprestacion);
					dblTotal2ContraprestacionTotalMN += Double.parseDouble((String)montoTotal);
					
					dblTotalSaldoPromedioMN += Double.parseDouble((String)saldo_promedio);
					dblTotalContraprestacionMN += Double.parseDouble((String)contraprestacion);
					dblTotalContraprestacionTotalMN += Double.parseDouble((String)montoTotal);
																		
				}//while(rs.next()){					
				if( total>0){	 	
					if(condIF == null){
						pdfDoc.setCell("Total "+ despMoneda , "celda01",ComunesPDF.RIGHT, 2); 
					} else {
						pdfDoc.setCell("Total "+ despMoneda , "celda01",ComunesPDF.RIGHT, 3);
					}				
					pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalSaldoPromedioMN, 2) , "formas",ComunesPDF.RIGHT);							
					pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalContraprestacionMN, 2) , "formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("" , "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell("" , "formas",ComunesPDF.CENTER);							
					pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotalContraprestacionTotalMN, 2) , "formas",ComunesPDF.RIGHT);	
					pdfDoc.setCell(" " , "formas",ComunesPDF.CENTER);
				
					if(condIF == null){
						pdfDoc.setCell(" Gran Total  "+ despMoneda , "celda01",ComunesPDF.RIGHT, 2); 
					} else {
						pdfDoc.setCell(" Gran Total  "+ despMoneda , "celda01",ComunesPDF.RIGHT, 3);
					}				
					pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotal2SaldoPromedioMN, 2) , "formas",ComunesPDF.RIGHT);							
					pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotal2ContraprestacionMN, 2) , "formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("" , "formas",ComunesPDF.CENTER);	
					pdfDoc.setCell("" , "formas",ComunesPDF.CENTER);							
					pdfDoc.setCell("$"+Comunes.formatoDecimal(dblTotal2ContraprestacionTotalMN, 2) , "formas",ComunesPDF.RIGHT);	
					pdfDoc.setCell(" " , "formas",ComunesPDF.CENTER);
				}
					
				pdfDoc.addTable();
				pdfDoc.endDocument();	
			}
					 
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}

	
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getIc_producto_nafin() {
		return ic_producto_nafin;
	}

	public void setIc_producto_nafin(String ic_producto_nafin) {
		this.ic_producto_nafin = ic_producto_nafin;
	}

	public String getIc_mes_calculo_de() {
		return ic_mes_calculo_de;
	}

	public void setIc_mes_calculo_de(String ic_mes_calculo_de) {
		this.ic_mes_calculo_de = ic_mes_calculo_de;
	}

	public String getIc_mes_calculo_a() {
		return ic_mes_calculo_a;
	}

	public void setIc_mes_calculo_a(String ic_mes_calculo_a) {
		this.ic_mes_calculo_a = ic_mes_calculo_a;
	}

	public String getAnio_calculo() {
		return anio_calculo;
	}

	public void setAnio_calculo(String anio_calculo) {
		this.anio_calculo = anio_calculo;
	}

	public String getIc_moneda() {
		return ic_moneda;
	}

	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}

	public String getCg_estatus_pagado() {
		return cg_estatus_pagado;
	}

	public void setCg_estatus_pagado(String cg_estatus_pagado) {
		this.cg_estatus_pagado = cg_estatus_pagado;
	}

	public String getPorcentajeIva() {
		return porcentajeIva;
	}

	public void setPorcentajeIva(String porcentajeIva) {
		this.porcentajeIva = porcentajeIva;
	}


	public void setCondIF(String condIF) {
		this.condIF = condIF;
	}


	public String getCondIF() {
		return condIF;
	}


	public void setBindIF(String bindIF) {
		this.bindIF = bindIF;
	}


	public String getBindIF() {
		return bindIF;
	}



	
  


}