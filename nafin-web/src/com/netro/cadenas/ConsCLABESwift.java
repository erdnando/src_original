package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsCLABESwift implements IQueryGeneratorRegExtJS  {
	

	private String cmb_tipo_afiliado;
	private String no_producto;
	private String no_epo ;
	private String no_nafin ;
	private String clave_afiliado;
	private String rfc;
	private String fecha_registro_de;
	private String fecha_registro_a;
	private String cuentas_sin_validar;
	
	private String moneda;
	
	private List 		conditions;
	String qrySentencia = new String("");	
	
	private final static Log log = ServiceLocator.getInstance().getLog(ConsCLABESwift.class);
	
	public ConsCLABESwift() {}
	
	public String getDocumentQuery() {
		log.info("getDocumentQuery(E)");
		qrySentencia 	= "";
		conditions 		= new ArrayList();
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();
	}
	
	public String getDocumentSummaryQueryForIds(List ids){
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= "";
		conditions 		= new ArrayList();
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();
	}
	
	public String getAggregateCalculationQuery(){
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= "";
		conditions 		= new ArrayList();
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
		
	public List getConditions(){
		return conditions;
	}
		
		public String getDocumentQueryFile(){
			log.info("getDocumentQueryFile(E)");
			qrySentencia 	= "";
			conditions 		= new ArrayList();
			

		String condicion = "";
		if(!"".equals(no_producto)) {
			condicion +=	"    AND c.ic_producto_nafin = "+no_producto+" ";
		}
		if(!"".equals(no_nafin)) {
			condicion += "    AND c.ic_nafin_electronico = "+no_nafin+" ";
		}
		if(!"".equals(fecha_registro_de)) {
			condicion += "    AND c.df_registro >= TO_DATE('"+fecha_registro_de+"', 'dd/mm/yyyy')";
		}
		if(!"".equals(fecha_registro_a)) {
			condicion += "    AND c.df_registro < (TO_DATE('"+fecha_registro_a+"', 'dd/mm/yyyy')+1)";
		}
		if("S".equals(cuentas_sin_validar)) {
			condicion += "    AND c.ic_estatus_cecoban IS NULL ";
		}
		if(!"".equals(moneda)){
			condicion += "    AND cm.ic_moneda = "+moneda+" ";
		}
		String qrySentenciaEPO =
			" SELECT c.ic_cuenta, c.cg_tipo_afiliado AS tipoafiliado,"   +
			"        epo.cg_razon_social AS nombre, bt.cd_descripcion banco,"   +
			"        DECODE (c.ic_tipo_cuenta, 40, c.cg_cuenta) AS cuentaclabe,"   +
			"        DECODE (c.ic_tipo_cuenta, 1, c.cg_cuenta) AS cuentaspeua,"   +
			"        tef.cd_descripcion AS estatus_tef,"   +
			"        DECODE(c.ic_moneda,54,'',cec.cd_descripcion) AS estatus_cecoban,"   +
			"        TO_CHAR (c.df_ultima_mod, 'dd-MM-yyyy hh:mi') AS fecha_ult_mod,"   +
			"        c.ic_usuario AS usuario,"   +
			"        '' AS eporel, cpn.ic_nombre as producto, c.ic_estatus_cecoban, to_number(null) AS ic_epo_rel, "   +
			"			DECODE (c.ic_tipo_cuenta, 50, c.cg_cuenta) AS cuentaswift, 		"  +
			"			c.ic_moneda                                AS tipo_moneda, 		"  +
			"			DECODE(c.ic_moneda,54,cec.cd_descripcion,'') AS estatus_dolares, "  +
			"			c.ic_estatus_cecoban as estatus_cuenta, "  +
			"        DECODE (c.ic_tipo_cuenta,1,'true','false') AS es_cuenta_speua,   "   +
			"        DECODE (c.ic_moneda,54,'true','false') 	 AS es_cuenta_dolares, "   +
			"			c.cg_plaza_swift    as plaza_swift,   "  +
			"			c.cg_sucursal_swift as sucursal_swift, "  +
			"			cm.CD_NOMBRE as nombre_moneda,"+
			"			to_char(c.ic_aba) as ABA"+			
			"   FROM com_cuentas c,"   +
			"        comrel_nafin crn,"   +
			"        comcat_epo epo,"   +
			"        comcat_bancos_tef bt,"   +
			"        comcat_estatus_cecoban cec,"   +
			"        comcat_estatus_tef tef"   +
			"        ,comcat_producto_nafin cpn"   +
			"			,comcat_moneda cm"+
			"  WHERE c.ic_nafin_electronico = crn.ic_nafin_electronico"   +
			"    AND c.ic_bancos_tef = bt.ic_bancos_tef"   +
			"	  AND c.ic_moneda = cm.ic_moneda"+
			"    AND c.ic_estatus_tef = tef.ic_estatus_tef (+)"   +
			"    AND c.ic_estatus_cecoban = cec.ic_estatus_cecoban (+)"   +
			"    AND c.ic_producto_nafin = cpn.ic_producto_nafin"   +
			"    AND crn.ic_epo_pyme_if = epo.ic_epo"   +
			"	  AND c.ic_tipo_cuenta != 1 "  + // excluir Cuentas Speua
			"    AND crn.cg_tipo = 'E'"+condicion;

		if(!"".equals(rfc)) {
			qrySentenciaEPO += "    AND epo.cg_rfc = '"+rfc+"' ";
		}
		
		String qrySentenciaPYME =
			" SELECT c.ic_cuenta, c.cg_tipo_afiliado AS tipoafiliado,"   +
			"        pym.cg_razon_social AS nombre, bt.cd_descripcion banco,"   +
			"        DECODE (c.ic_tipo_cuenta, 40, c.cg_cuenta) AS cuentaclabe,"   +
			"        DECODE (c.ic_tipo_cuenta, 1, c.cg_cuenta) AS cuentaspeua,"   +
			"        tef.cd_descripcion AS estatus_tef,"   +
			"        DECODE(c.ic_moneda,54,'',cec.cd_descripcion) AS estatus_cecoban,"   +
			"        TO_CHAR (c.df_ultima_mod, 'dd-MM-yyyy hh:mi') AS fecha_ult_mod,"   +
			"        c.ic_usuario AS usuario,"   +
			"        epo.cg_razon_social AS eporel, cpn.ic_nombre as producto, c.ic_estatus_cecoban, c.ic_epo AS ic_epo_rel, "   +
			"			DECODE (c.ic_tipo_cuenta, 50, c.cg_cuenta)   AS cuentaswift, 		"  +
			"			c.ic_moneda                                  AS tipo_moneda, 		"  +
			"			DECODE(c.ic_moneda,54,cec.cd_descripcion,'') AS estatus_dolares, 	"  +
			"			c.ic_estatus_cecoban as estatus_cuenta, "  +
			"        DECODE (c.ic_tipo_cuenta,1,'true','false') AS es_cuenta_speua,   "   +
			"        DECODE (c.ic_moneda,54,'true','false') 	 AS es_cuenta_dolares, "   +
			"			c.cg_plaza_swift    as plaza_swift,   "  +
			"			c.cg_sucursal_swift as sucursal_swift, "  +
			"			cm.CD_NOMBRE as nombre_moneda,"+
			"			to_char(c.ic_aba) as ABA"+			
			"   FROM com_cuentas c,"   +
			"        comrel_nafin crn,"   +
			"        comcat_pyme pym,"   +
			"        comcat_epo epo,"   +
			"        comcat_bancos_tef bt,"   +
			"        comcat_estatus_cecoban cec,"   +
			"        comcat_estatus_tef tef"   +
			"        ,comcat_producto_nafin cpn"   +
			"			,comcat_moneda cm"+
			"  WHERE c.ic_nafin_electronico = crn.ic_nafin_electronico"   +
			"    AND c.ic_bancos_tef = bt.ic_bancos_tef"   +
			"    AND c.ic_estatus_tef = tef.ic_estatus_tef (+)"   +
			"	  AND c.ic_moneda = cm.ic_moneda"+
			"    AND c.ic_estatus_cecoban = cec.ic_estatus_cecoban (+)"   +
			"    AND c.ic_producto_nafin = cpn.ic_producto_nafin"   +
			"    AND c.ic_epo = epo.ic_epo "   +
			"    AND crn.ic_epo_pyme_if = pym.ic_pyme"   +
			"	  AND c.ic_tipo_cuenta != 1 "  + // excluir Cuentas Speua
			"    AND crn.cg_tipo = 'P'"+condicion;
		
		if("P".equals(cmb_tipo_afiliado) || "D".equals(cmb_tipo_afiliado)) {
			qrySentenciaPYME +=	"    AND c.cg_tipo_afiliado = '"+cmb_tipo_afiliado+"' ";
		}
		if(!"".equals(no_epo)) {
			qrySentenciaPYME += "    AND c.ic_epo = "+no_epo+" ";
		}
		if(!"".equals(rfc)) {
			qrySentenciaPYME += "    AND pym.cg_rfc = '"+rfc+"' ";
		}
		
		String qrySentenciaIF =
			" SELECT c.ic_cuenta, c.cg_tipo_afiliado AS tipoafiliado,"   +
			"        cif.cg_razon_social AS nombre, bt.cd_descripcion banco,"   +
			"        DECODE (c.ic_tipo_cuenta, 40, c.cg_cuenta) AS cuentaclabe,"   +
			"        DECODE (c.ic_tipo_cuenta, 1, c.cg_cuenta) AS cuentaspeua,"   +
			"        tef.cd_descripcion AS estatus_tef,"   +
			"        DECODE(c.ic_moneda,54,'',cec.cd_descripcion) AS estatus_cecoban,"   +
			"        TO_CHAR (c.df_ultima_mod, 'dd-MM-yyyy hh:mi') AS fecha_ult_mod,"   +
			"        c.ic_usuario AS usuario,"   +
			"        epo.cg_razon_social AS eporel, cpn.ic_nombre as producto, c.ic_estatus_cecoban, c.ic_epo AS ic_epo_rel, "   +
			"			DECODE (c.ic_tipo_cuenta, 50, c.cg_cuenta) AS cuentaswift, 		"  +
			"			c.ic_moneda                                AS tipo_moneda, 		"  +
			"			DECODE(c.ic_moneda,54,cec.cd_descripcion,'') AS estatus_dolares, 	"  +
			"			c.ic_estatus_cecoban as estatus_cuenta, "  +
			"        DECODE (c.ic_tipo_cuenta,1,'true','false') AS es_cuenta_speua, "   +
			"        DECODE (c.ic_moneda,54,'true','false') 	 AS es_cuenta_dolares, "   +
			"			c.cg_plaza_swift    as plaza_swift,   "  +
			"			c.cg_sucursal_swift as sucursal_swift, "  +
			"			cm.CD_NOMBRE as nombre_moneda,"+
			"			to_char(c.ic_aba) as ABA"+			
			"   FROM com_cuentas c,"   +
			"        comrel_nafin crn,"   +
			"        comcat_if cif,"   +
			"        comcat_epo epo,"   +
			"        comcat_bancos_tef bt,"   +
			"        comcat_estatus_cecoban cec,"   +
			"        comcat_estatus_tef tef"   +
			"        ,comcat_producto_nafin cpn"   +
			"			,comcat_moneda cm"+
			"  WHERE c.ic_nafin_electronico = crn.ic_nafin_electronico"   +
			"    AND c.ic_bancos_tef = bt.ic_bancos_tef"   +
			"    AND c.ic_estatus_tef = tef.ic_estatus_tef (+)"   +
			"	  AND c.ic_moneda = cm.ic_moneda"+
			"    AND c.ic_estatus_cecoban = cec.ic_estatus_cecoban (+)"   +
			"    AND c.ic_epo = epo.ic_epo "   +
			"    AND c.ic_producto_nafin = cpn.ic_producto_nafin"   +
			"    AND crn.ic_epo_pyme_if = cif.ic_if"   +
			"	  AND c.ic_tipo_cuenta != 1 "  + // excluir Cuentas Speua
			"    AND crn.cg_tipo = 'I'"+condicion;

		if("I".equals(cmb_tipo_afiliado) || "B".equals(cmb_tipo_afiliado)) {
			qrySentenciaIF +=	"    AND c.cg_tipo_afiliado = '"+cmb_tipo_afiliado+"' ";
		}
		if(!"".equals(no_epo)) {
			qrySentenciaIF +=	"    AND c.ic_epo = "+no_epo+" ";
		}
		if(!"".equals(rfc)) {
			qrySentenciaIF += "    AND cif.cg_rfc = '"+rfc+"' ";
		}
		qrySentencia = "";
		if("".equals(cmb_tipo_afiliado) || "E".equals(cmb_tipo_afiliado))
			qrySentencia = qrySentenciaEPO;
		if("".equals(cmb_tipo_afiliado) || "P".equals(cmb_tipo_afiliado) || "D".equals(cmb_tipo_afiliado)) {
			if(!"".equals(qrySentencia))
				qrySentencia += " UNION ";
			qrySentencia += qrySentenciaPYME;
		}
		if("".equals(cmb_tipo_afiliado) || "I".equals(cmb_tipo_afiliado) || "B".equals(cmb_tipo_afiliado)) {
			if(!"".equals(qrySentencia))
				qrySentencia += " UNION ";
			qrySentencia += qrySentenciaIF;
		}
		qrySentencia += "  ORDER BY nombre"  ;
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();
	}

	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
			log.debug("crearCustomFile (E)");
			String nombreArchivo = "";
			HttpSession session = request.getSession();	
			StringBuffer contenidoArchivo = new StringBuffer("");

					
		try {
		if("PDF".equals(tipo)){

			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				
			ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
			session.getAttribute("iNoNafinElectronico") == null ? "" : session.getAttribute("iNoNafinElectronico").toString(),
			(String)session.getAttribute("sesExterno"),
			(String) session.getAttribute("strNombre"),
			(String) session.getAttribute("strNombreUsuario"),
			(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				
			pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);		
			
			int columnas =12;
			if("".equals(cmb_tipo_afiliado)) {
						columnas++;
			}
			if("".equals(no_producto)) {
						columnas++;
					}
			if("".equals(cmb_tipo_afiliado) || "I".equals(cmb_tipo_afiliado) || "B".equals(cmb_tipo_afiliado) || "P".equals(cmb_tipo_afiliado)) {
						columnas++;
			}
			
			
			pdfDoc.setTable(columnas,90);	
			
			if("".equals(cmb_tipo_afiliado)) {
						pdfDoc.setCell("Tipo de Afiliado","celda01",ComunesPDF.CENTER);
			}
			if("".equals(no_producto)) {
						pdfDoc.setCell("Producto","celda01",ComunesPDF.CENTER);
					}
			
			
			pdfDoc.setCell("Nombre","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Banco","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Cuenta Clabe","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Cuenta Swift","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Plaza Swift","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Sucursal Swift","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo de Moneda","celda01",ComunesPDF.CENTER);
			if("".equals(cmb_tipo_afiliado) || "I".equals(cmb_tipo_afiliado) || "B".equals(cmb_tipo_afiliado) || "P".equals(cmb_tipo_afiliado)) {
						pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
						
			}

			pdfDoc.setCell("Descripción TEF","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Descripción CECOBAN","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Descripción DOLARES","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha y hora de última actualización","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Usuario","celda01",ComunesPDF.CENTER);				
		
			while (rs.next()) {
								
			String auxTipoAfiliado="";
			String rs_cuenta 				= rs.getString(1)==null?"":rs.getString(1);
			String rs_tipoAfiliado 		= rs.getString(2)==null?"":rs.getString(2);
			String rs_nombre 				= rs.getString(3)==null?"":rs.getString(3);
			String rs_banco 				= rs.getString(4)==null?"":rs.getString(4);
			String rs_cuentaClabe		= rs.getString(5)==null?"":rs.getString(5);
			String rs_cuentaSpeua		= rs.getString(6)==null?"":rs.getString(6);
			String rs_estatTef			= rs.getString(7)==null?"":rs.getString(7);
			String rs_estatCec			= rs.getString(8)==null?"":rs.getString(8);
			String rs_fechaUltima		= rs.getString(9)==null?"":rs.getString(9);
			String rs_Usuario				= rs.getString(10)==null?"":rs.getString(10);
			String rs_epoRelacionada	= rs.getString(11)==null?"":rs.getString(11);
			String rs_producto			= rs.getString(12)==null?"":rs.getString(12);
			String rs_estatus_cecoban	= rs.getString("ic_estatus_cecoban")== null?"":rs.getString("ic_estatus_cecoban");
			String rs_clave_epo_rel		= rs.getString("ic_epo_rel")			== null?"":rs.getString("ic_epo_rel");
			String rs_cuentaSwift		= rs.getString("cuentaswift")			== null?"":rs.getString("cuentaswift");
			String rs_plazaSwift			= rs.getString("plaza_swift")			== null?"":rs.getString("plaza_swift");
			String rs_sucursalSwift		= rs.getString("sucursal_swift")		== null?"":rs.getString("sucursal_swift");
			//String rs_tipoMoneda			= rs.getString("nombre_moneda")			== null?"":rs.getString("nombre_moneda");
      String rs_tipoMoneda			= rs.getString("tipo_moneda")			== null?"":rs.getString("tipo_moneda");
			String rs_estatusDolares	= rs.getString("estatus_dolares")	== null?"":rs.getString("estatus_dolares");
			String rs_estatus_cuentas	= (rs.getString("estatus_cuenta")	== null || rs.getString("estatus_cuenta").trim().equals(""))	?""		:rs.getString("estatus_cuenta");
			String rs_esCuentaSpeua		= (rs.getString("es_cuenta_speua")	== null || rs.getString("es_cuenta_speua").trim().equals(""))	?"false"	:rs.getString("es_cuenta_speua");
			String rs_esCuentaDolares	= (rs.getString("es_cuenta_dolares")== null || rs.getString("es_cuenta_dolares").trim().equals(""))?"false"	:rs.getString("es_cuenta_dolares");
			
			boolean 	esCuentaEnDolaresAmericanos 	= rs_esCuentaDolares.equals("true")?true:false;
			
			if("".equals(cmb_tipo_afiliado)) {
				if("E".equals(rs_tipoAfiliado))
					auxTipoAfiliado = "EPO";
				else if("P".equals(rs_tipoAfiliado))
					auxTipoAfiliado = "Proveedor";
				else if("D".equals(rs_tipoAfiliado))
					auxTipoAfiliado = "Distribuidor";
				else if("I".equals(rs_tipoAfiliado))
					auxTipoAfiliado = "IF Bancario/No Bancario";
				else if("B".equals(rs_tipoAfiliado))
					auxTipoAfiliado = "IF Beneficiario";
			}
			
			if("".equals(cmb_tipo_afiliado)) {
						pdfDoc.setCell(auxTipoAfiliado,"formas",ComunesPDF.CENTER);
						
			}
			if("".equals(no_producto)) {
						pdfDoc.setCell(rs_producto,"formas",ComunesPDF.CENTER);
						
					}
			
			pdfDoc.setCell(rs_nombre,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(rs_banco,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell((esCuentaEnDolaresAmericanos?"N/A":"'"+rs_cuentaClabe),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell((!esCuentaEnDolaresAmericanos?"N/A":"'"+rs_cuentaSwift),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell((!esCuentaEnDolaresAmericanos?"N/A":rs_plazaSwift),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell((!esCuentaEnDolaresAmericanos?"N/A":rs_sucursalSwift),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(rs_tipoMoneda,"formas",ComunesPDF.CENTER);
			
			if("".equals(cmb_tipo_afiliado) || "I".equals(cmb_tipo_afiliado) || "B".equals(cmb_tipo_afiliado) || "P".equals(cmb_tipo_afiliado)) {
						pdfDoc.setCell(rs_epoRelacionada,"formas",ComunesPDF.CENTER);
			}
			pdfDoc.setCell((esCuentaEnDolaresAmericanos?"N/A":rs_estatTef),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell((esCuentaEnDolaresAmericanos?"N/A":rs_estatCec),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell((!esCuentaEnDolaresAmericanos?"N/A":(rs_estatusDolares.equals("")?"Sin Validar":rs_estatusDolares)),"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(rs_fechaUltima,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(rs_Usuario,"formas",ComunesPDF.CENTER);
			}	

				pdfDoc.addTable();
				pdfDoc.endDocument();			
		
		}else if("CSV".equals(tipo)){
			OutputStreamWriter writer = null;
			BufferedWriter buffer = null;
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);
			if("".equals(cmb_tipo_afiliado)) {
						contenidoArchivo.append( "Tipo de Afiliado,");
			}
			if("".equals(no_producto)) {
						contenidoArchivo.append( "Producto,");
					}
			
			contenidoArchivo.append("Nombre,Banco,Cuenta Clabe,Cuenta Swift,Plaza Swift,Sucursal Swift,Tipo de Moneda,"); 
			if("".equals(cmb_tipo_afiliado) || "I".equals(cmb_tipo_afiliado) || "B".equals(cmb_tipo_afiliado) || "P".equals(cmb_tipo_afiliado)) {
						contenidoArchivo.append( "EPO,");
			}
			
			contenidoArchivo.append("Descripción TEF, Descripción CECOBAN,Descripción DOLARES,Fecha y hora de última actualización,Usuario\n");
		
		int posReg = 0;
		while(rs.next()) {
			String auxTipoAfiliado="";
			String rs_cuenta 				= rs.getString(1)==null?"":rs.getString(1);
			String rs_tipoAfiliado 		= rs.getString(2)==null?"":rs.getString(2);
			String rs_nombre 				= rs.getString(3)==null?"":rs.getString(3);
			String rs_banco 				= rs.getString(4)==null?"":rs.getString(4);
			String rs_cuentaClabe		= rs.getString(5)==null?"":rs.getString(5);
			String rs_cuentaSpeua		= rs.getString(6)==null?"":rs.getString(6);
			String rs_estatTef			= rs.getString(7)==null?"":rs.getString(7);
			String rs_estatCec			= rs.getString(8)==null?"":rs.getString(8);
			String rs_fechaUltima		= rs.getString(9)==null?"":rs.getString(9);
			String rs_Usuario				= rs.getString(10)==null?"":rs.getString(10);
			String rs_epoRelacionada	= rs.getString(11)==null?"":rs.getString(11);
			String rs_producto			= rs.getString(12)==null?"":rs.getString(12);
			String rs_estatus_cecoban	= rs.getString("ic_estatus_cecoban")== null?"":rs.getString("ic_estatus_cecoban");
			String rs_clave_epo_rel		= rs.getString("ic_epo_rel")			== null?"":rs.getString("ic_epo_rel");
			String rs_cuentaSwift		= rs.getString("cuentaswift")			== null?"":rs.getString("cuentaswift");
			String rs_plazaSwift			= rs.getString("plaza_swift")			== null?"":rs.getString("plaza_swift");
			String rs_sucursalSwift		= rs.getString("sucursal_swift")		== null?"":rs.getString("sucursal_swift");
			//String rs_tipoMoneda			= rs.getString("nombre_moneda")			== null?"":rs.getString("nombre_moneda");
      String rs_tipoMoneda			= rs.getString("tipo_moneda")			== null?"":rs.getString("tipo_moneda");
			String rs_estatusDolares	= rs.getString("estatus_dolares")	== null?"":rs.getString("estatus_dolares");
			String rs_estatus_cuentas	= (rs.getString("estatus_cuenta")	== null || rs.getString("estatus_cuenta").trim().equals(""))	?""		:rs.getString("estatus_cuenta");
			String rs_esCuentaSpeua		= (rs.getString("es_cuenta_speua")	== null || rs.getString("es_cuenta_speua").trim().equals(""))	?"false"	:rs.getString("es_cuenta_speua");
			String rs_esCuentaDolares	= (rs.getString("es_cuenta_dolares")== null || rs.getString("es_cuenta_dolares").trim().equals(""))?"false"	:rs.getString("es_cuenta_dolares");
			
			boolean 	esCuentaEnDolaresAmericanos 	= rs_esCuentaDolares.equals("true")?true:false;
			
			if("".equals(cmb_tipo_afiliado)) {
				if("E".equals(rs_tipoAfiliado))
					auxTipoAfiliado = "EPO";
				else if("P".equals(rs_tipoAfiliado))
					auxTipoAfiliado = "Proveedor";
				else if("D".equals(rs_tipoAfiliado))
					auxTipoAfiliado = "Distribuidor";
				else if("I".equals(rs_tipoAfiliado))
					auxTipoAfiliado = "IF Bancario/No Bancario";
				else if("B".equals(rs_tipoAfiliado))
					auxTipoAfiliado = "IF Beneficiario";
			}
			
			if("".equals(cmb_tipo_afiliado)) {
						contenidoArchivo.append( auxTipoAfiliado+",");
			}
			if("".equals(no_producto)) {
						contenidoArchivo.append(rs_producto+ ",");
					}
			contenidoArchivo.append(rs_nombre.replace(',',' ')+","+rs_banco.replace(',',' ')+","+(esCuentaEnDolaresAmericanos?"N/A":"=\""+rs_cuentaClabe+"\"")+","+(!esCuentaEnDolaresAmericanos?"N/A":"=\""+rs_cuentaSwift+"\"")+","+(!esCuentaEnDolaresAmericanos?"N/A":"=\""+rs_plazaSwift+"\"")+","+(!esCuentaEnDolaresAmericanos?"N/A":"=\""+rs_sucursalSwift+"\"")+","+rs_tipoMoneda+",");
			if("".equals(cmb_tipo_afiliado) || "I".equals(cmb_tipo_afiliado) || "B".equals(cmb_tipo_afiliado) || "P".equals(cmb_tipo_afiliado)) {
						contenidoArchivo.append( rs_epoRelacionada.replace(',',' ')+",");
			}
			contenidoArchivo.append((esCuentaEnDolaresAmericanos?"N/A":rs_estatTef)+","+(esCuentaEnDolaresAmericanos?"N/A":rs_estatCec)+","+(!esCuentaEnDolaresAmericanos?"N/A":(rs_estatusDolares.equals("")?"Sin Validar":rs_estatusDolares))+","+rs_fechaUltima+","+rs_Usuario+"\n");
			buffer.write(contenidoArchivo.toString());	
			contenidoArchivo = new StringBuffer();//Limpio
			
		}// while 		
		
			buffer.close();	
		}else if("CSVGRAL".equals(tipo)){
		
			OutputStreamWriter writer = null;
			BufferedWriter buffer = null;
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);
		
		
			contenidoArchivo.append( " Nombre,Banco, Cuenta Clabe, Cuenta Swift, Plaza Swift, Sucursal Swift, Tipo Moneda, "+
											 " EPO, Descripción TEF, Descripción CECOBAN, Descripción Dólares , "+
											 " Fecha y Hora de Última Actualización, Usuario \n ");
		      
		
		 /*
			while(rs.next()) {
			String auxTipoAfiliado="";
			String rs_cuenta 				= rs.getString(1)==null?"":rs.getString(1);
			String rs_tipoAfiliado 		= rs.getString(2)==null?"":rs.getString(2);
			String rs_nombre 				= rs.getString(3)==null?"":rs.getString(3);
			String rs_banco 				= rs.getString(4)==null?"":rs.getString(4);
			String rs_cuentaClabe		= rs.getString(5)==null?"":rs.getString(5);
			String rs_cuentaSpeua		= rs.getString(6)==null?"":rs.getString(6);
			String rs_estatTef			= rs.getString(7)==null?"":rs.getString(7);
			String rs_estatCec			= rs.getString(8)==null?"":rs.getString(8);
			String rs_fechaUltima		= rs.getString(9)==null?"":rs.getString(9);
			String rs_Usuario				= rs.getString(10)==null?"":rs.getString(10);
			
			String rs_cuentaSwift		= rs.getString("cuentaswift")			== null?"":rs.getString("cuentaswift");
			String rs_plazaSwift			= rs.getString("plaza_swift")			== null?"":rs.getString("plaza_swift");
			String rs_sucursalSwift		= rs.getString("sucursal_swift")		== null?"":rs.getString("sucursal_swift");			
			String rs_estatusDolares	= rs.getString("estatus_dolares")	== null?"":rs.getString("estatus_dolares");
			String rs_estatus_cuentas	= (rs.getString("estatus_cuenta")	== null || rs.getString("estatus_cuenta").trim().equals(""))	?""		:rs.getString("estatus_cuenta");
			String rs_esCuentaSpeua		= (rs.getString("es_cuenta_speua")	== null || rs.getString("es_cuenta_speua").trim().equals(""))	?"false"	:rs.getString("es_cuenta_speua");
			String rs_esCuentaDolares	= (rs.getString("es_cuenta_dolares")== null || rs.getString("es_cuenta_dolares").trim().equals(""))?"false"	:rs.getString("es_cuenta_dolares");
			String moneda	= rs.getString("moneda")== null?"":rs.getString("moneda");
			
			boolean 	esCuentaEnDolaresAmericanos 	= rs_esCuentaDolares.equals("true")?true:false;
			
			contenidoArchivo.append(rs_nombre.replace(',',' ')+","+
											rs_banco.replace(',',' ')+","+
											(esCuentaEnDolaresAmericanos?"N/A":"=\""+rs_cuentaClabe+"\"")+","+
											(!esCuentaEnDolaresAmericanos?"N/A":"=\""+rs_cuentaSwift+"\"")+","+
											(!esCuentaEnDolaresAmericanos?"N/A":"=\""+rs_plazaSwift+"\"")+","+
											(!esCuentaEnDolaresAmericanos?"N/A":"=\""+rs_sucursalSwift+"\"")+","+
											moneda+","+
											(esCuentaEnDolaresAmericanos?"N/A":rs_estatTef)+","+
											(esCuentaEnDolaresAmericanos?"N/A":rs_estatCec)+","+
											(!esCuentaEnDolaresAmericanos?"N/A":(rs_estatusDolares.equals("")?"Sin Validar":rs_estatusDolares))+","+
											rs_fechaUltima+","+
											rs_Usuario+"\n");
											
											
			buffer.write(contenidoArchivo.toString());	
			contenidoArchivo = new StringBuffer();//Limpio
			
		}// while 
		buffer.close();
		*/
			
			
				while(rs.next()) {
			String auxTipoAfiliado="";
			String rs_cuenta 				= rs.getString(1)==null?"":rs.getString(1);
			String rs_tipoAfiliado 		= rs.getString(2)==null?"":rs.getString(2);
			String rs_nombre 				= rs.getString(3)==null?"":rs.getString(3);
			String rs_banco 				= rs.getString(4)==null?"":rs.getString(4);
			String rs_cuentaClabe		= rs.getString(5)==null?"":rs.getString(5);
			String rs_cuentaSpeua		= rs.getString(6)==null?"":rs.getString(6);
			String rs_estatTef			= rs.getString(7)==null?"":rs.getString(7);
			String rs_estatCec			= rs.getString(8)==null?"":rs.getString(8);
			String rs_fechaUltima		= rs.getString(9)==null?"":rs.getString(9);
			String rs_Usuario				= rs.getString(10)==null?"":rs.getString(10);
			String rs_epoRelacionada	= rs.getString(11)==null?"":rs.getString(11);
			String rs_producto			= rs.getString(12)==null?"":rs.getString(12);
			String rs_estatus_cecoban	= rs.getString("ic_estatus_cecoban")== null?"":rs.getString("ic_estatus_cecoban");
			String rs_clave_epo_rel		= rs.getString("ic_epo_rel")			== null?"":rs.getString("ic_epo_rel");
			String rs_cuentaSwift		= rs.getString("cuentaswift")			== null?"":rs.getString("cuentaswift");
			String rs_plazaSwift			= rs.getString("plaza_swift")			== null?"":rs.getString("plaza_swift");
			String rs_sucursalSwift		= rs.getString("sucursal_swift")		== null?"":rs.getString("sucursal_swift");
			//String rs_tipoMoneda			= rs.getString("nombre_moneda")			== null?"":rs.getString("nombre_moneda");
      String rs_tipoMoneda			= rs.getString("tipo_moneda")			== null?"":rs.getString("tipo_moneda");
			String rs_estatusDolares	= rs.getString("estatus_dolares")	== null?"":rs.getString("estatus_dolares");
			String rs_estatus_cuentas	= (rs.getString("estatus_cuenta")	== null || rs.getString("estatus_cuenta").trim().equals(""))	?""		:rs.getString("estatus_cuenta");
			String rs_esCuentaSpeua		= (rs.getString("es_cuenta_speua")	== null || rs.getString("es_cuenta_speua").trim().equals(""))	?"false"	:rs.getString("es_cuenta_speua");
			String rs_esCuentaDolares	= (rs.getString("es_cuenta_dolares")== null || rs.getString("es_cuenta_dolares").trim().equals(""))?"false"	:rs.getString("es_cuenta_dolares");
			
			boolean 	esCuentaEnDolaresAmericanos 	= rs_esCuentaDolares.equals("true")?true:false;
			
			if("".equals(cmb_tipo_afiliado)) {
				if("E".equals(rs_tipoAfiliado))
					auxTipoAfiliado = "EPO";
				else if("P".equals(rs_tipoAfiliado))
					auxTipoAfiliado = "Proveedor";
				else if("D".equals(rs_tipoAfiliado))
					auxTipoAfiliado = "Distribuidor";
				else if("I".equals(rs_tipoAfiliado))
					auxTipoAfiliado = "IF Bancario/No Bancario";
				else if("B".equals(rs_tipoAfiliado))
					auxTipoAfiliado = "IF Beneficiario"; 
			}
			
			contenidoArchivo.append(rs_nombre.replace(',',' ')+","+
			rs_banco.replace(',',' ')+","+
			(esCuentaEnDolaresAmericanos?"N/A":"=\""+rs_cuentaClabe+"\"")+","+
			(!esCuentaEnDolaresAmericanos?"N/A":"=\""+rs_cuentaSwift+"\"")+","+
			(!esCuentaEnDolaresAmericanos?"N/A":"=\""+rs_plazaSwift+"\"")+","+
			(!esCuentaEnDolaresAmericanos?"N/A":"=\""+
			rs_sucursalSwift+"\"")+","+ 
			rs_tipoMoneda+",");
			if("".equals(cmb_tipo_afiliado) || "I".equals(cmb_tipo_afiliado) || "B".equals(cmb_tipo_afiliado) || "P".equals(cmb_tipo_afiliado)) {
						contenidoArchivo.append( rs_epoRelacionada.replace(',',' ')+",");
			}
			contenidoArchivo.append((esCuentaEnDolaresAmericanos?"N/A":rs_estatTef)+","+(esCuentaEnDolaresAmericanos?"N/A":rs_estatCec)+","+(!esCuentaEnDolaresAmericanos?"N/A":(rs_estatusDolares.equals("")?"Sin Validar":rs_estatusDolares))+","+rs_fechaUltima+","+rs_Usuario+"\n");
			buffer.write(contenidoArchivo.toString());	
			contenidoArchivo = new StringBuffer();//Limpio
			
		}// while 		
		
			buffer.close();
		
		}
		
		
		}catch(Exception e) { 
				log.error("Error en la generacion del Archivo CSV: " + e);
		}	


		return nombreArchivo;
	}

	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo){
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
	
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		
		return nombreArchivo;
	}




/************************SETTERS**********************/
	


	public void setCmb_tipo_afiliado(String cmb_tipo_afiliado) {
		this.cmb_tipo_afiliado = cmb_tipo_afiliado;
	}


	public String getCmb_tipo_afiliado() {
		return cmb_tipo_afiliado;
	}


	public void setNo_producto(String no_producto) {
		this.no_producto = no_producto;
	}


	public String getNo_producto() {
		return no_producto;
	}


	public void setNo_epo(String no_epo) {
		this.no_epo = no_epo;
	}


	public String getNo_epo() {
		return no_epo;
	}


	public void setNo_nafin(String no_nafin) {
		this.no_nafin = no_nafin;
	}


	public String getNo_nafin() {
		return no_nafin;
	}




	public void setClave_afiliado(String clave_afiliado) {
		this.clave_afiliado = clave_afiliado;
	}


	public String getClave_afiliado() {
		return clave_afiliado;
	}


	public void setRfc(String rfc) {
		this.rfc = rfc;
	}


	public String getRfc() {
		return rfc;
	}


	public void setFecha_registro_de(String fecha_registro_de) {
		this.fecha_registro_de = fecha_registro_de;
	}


	public String getFecha_registro_de() {
		return fecha_registro_de;
	}


	public void setFecha_registro_a(String fecha_registro_a) {
		this.fecha_registro_a = fecha_registro_a;
	}


	public String getFecha_registro_a() {
		return fecha_registro_a;
	}


	public void setCuentas_sin_validar(String cuentas_sin_validar) {
		this.cuentas_sin_validar = cuentas_sin_validar;
	}


	public String getCuentas_sin_validar() {
		return cuentas_sin_validar;
	}


	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}


	public String getMoneda() {
		return moneda;
	}

	
	
	}// fin clase
	