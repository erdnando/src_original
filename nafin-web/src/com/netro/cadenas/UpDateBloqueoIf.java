package com.netro.cadenas;

import java.sql.SQLException;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class UpDateBloqueoIf{
	//Variable para enviar mensajes al log.
	private static final Log log = ServiceLocator.getInstance().getLog(UpDateBloqueoIf.class);
	
	public static void upDateBI(String acuses)throws SQLException{
		log.debug("***ENTRADA AL UPDATE");
		boolean exito = true;
		String qrySentencia = 
			" UPDATE com_solic_portal " +
			" SET ic_estatus_solic = 1 " +
			" WHERE ic_estatus_solic = 12 " +
			" AND cc_acuse in (" + acuses + ")";

		AccesoDB con = null;
		try{
			con = new AccesoDB();
			con.conexionDB();
			con.ejecutaSQL(qrySentencia);
		}catch(Exception e) {
			exito = false;
			throw new AppException("Error en la actualización de los datos", e);
			//throw e;
		} finally{
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}

		}
		log.debug("***SALIDA DEL UPDATE");
	}
}