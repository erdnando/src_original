package com.netro.cadenas;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class CedulaConciliacionObservaciones  {
	
	private final static Log log = ServiceLocator.getInstance().getLog(CedulaConciliacionObservaciones.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	
	private String tipo;
		//PARAMETROS 
	private String icIf;
	private String moneda;
	private String numIntermediario;
	private String fechaCorte;
	private String tipoLinea;
	private String numSirac;
	
	public CedulaConciliacionObservaciones() {
	}
	
	public Registros getDataCatalogoIntermediario(){
	log.info("getDataCatalogoIntermediario(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 
		Registros registros  = new Registros();
		try{
			con.conexionDB();

			
			if(!"CE".equals(tipo)){
				if(!"C".equals(tipoLinea)){
					qrySentencia.append("SELECT   ");
					qrySentencia.append("	 i.ic_if AS clave,   ");
					qrySentencia.append("	 '(' || i.ic_financiera || ') ' || ' ' || i.cg_razon_social AS descripcion   ");
					qrySentencia.append("FROM    ");
					qrySentencia.append("	 comcat_if i   ");
					qrySentencia.append("WHERE    ");
					qrySentencia.append("	 i.cs_habilitado = 'S'    ");
					qrySentencia.append("	 AND i.cs_tipo = ?   ");
					qrySentencia.append("ORDER BY    ");
					qrySentencia.append("	 i.ic_financiera	");
			conditions.add(tipo);
				}else
				{
					qrySentencia.append(	"SELECT   ");
					qrySentencia.append("	 i.ic_if AS clave,   ");
					qrySentencia.append("	 '(' || i.IN_NUMERO_SIRAC || ') ' || ' ' || i.cg_razon_social AS descripcion   ");
					qrySentencia.append("FROM    ");
					qrySentencia.append("	 comcat_if i   ");
					qrySentencia.append("WHERE    ");
					qrySentencia.append("	 i.cs_habilitado = 'S'    ");
					qrySentencia.append("	 AND i.cs_tipo = ?   ");
					qrySentencia.append("	 AND i.IN_NUMERO_SIRAC is not null   ");
					qrySentencia.append("ORDER BY    ");
					qrySentencia.append("	 i.ic_financiera	");
					conditions.add(tipo);
				}
			}else
			{
				qrySentencia.append(	"SELECT   i.IC_NAFIN_ELECTRONICO AS clave, " );
				qrySentencia.append("            '(' " );
				qrySentencia.append("         || i.IN_NUMERO_SIRAC " );
				qrySentencia.append("         || ') ' " );
				qrySentencia.append("         || ' ' " );
				qrySentencia.append("         || i.cg_razon_social AS descripcion " );
				qrySentencia.append("    FROM comcat_cli_externo i " );
				qrySentencia.append("ORDER BY i.ic_nafin_electronico ");
			}
			
			
			registros = con.consultarDB(qrySentencia.toString(),conditions);
			
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("getDataCatalogoIntermediario  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 	
	
		log.info("qrySentencia CatalogoIntermediario: "+qrySentencia.toString());
		log.info("conditions "+conditions);
		
		log.info("getDataCatalogoIntermediario (S) ");
		return registros;
															
	}
	public Registros catalogoMoneda(){
	log.info("catalogoMoneda(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 
		Registros registros  = new Registros();
		try{
			con.conexionDB();
			qrySentencia.append(	"SELECT DISTINCT	");
			qrySentencia.append("	 M.IC_MONEDA as clave,		");
			qrySentencia.append("	 M.CD_NOMBRE as descripcion		");
			qrySentencia.append("FROM 		");
			qrySentencia.append("	 COMCAT_MONEDA M, 		");
			qrySentencia.append("	 COMCAT_TASA T		");
			qrySentencia.append("WHERE 		");
			qrySentencia.append("	 M.IC_MONEDA = T.IC_MONEDA AND 		");
			qrySentencia.append("	 T.CS_DISPONIBLE = 'S' AND		");
			qrySentencia.append("	 M.IC_MONEDA = 1		");
			qrySentencia.append("UNION		");
			qrySentencia.append("SELECT DISTINCT 		");
			qrySentencia.append("	 M.IC_MONEDA as clave, 		");
			qrySentencia.append("	 M.CD_NOMBRE as descripcion		");
			qrySentencia.append("FROM 		");
			qrySentencia.append("	 COMCAT_MONEDA M , 		");
			qrySentencia.append("	 COMCAT_TASA T ,		");
			qrySentencia.append("	 COM_TIPO_CAMBIO TC    	");
			qrySentencia.append("WHERE     	");
			qrySentencia.append("	  M.IC_MONEDA = T.IC_MONEDA AND     	");
			qrySentencia.append("	  T.CS_DISPONIBLE = 'S' AND    	");
			qrySentencia.append("	  M.IC_MONEDA = TC.IC_MONEDA    	");
			qrySentencia.append("ORDER BY 2    ");

			
			
			registros = con.consultarDB(qrySentencia.toString());
			
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("catalogoMoneda  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 	
	
		log.info("qrySentencia catalogoMoneda: "+qrySentencia.toString());
		log.info("conditions "+conditions);
		
		log.info("catalogoMoneda (S) ");
		return registros;
															
	}
	public Registros getDatosFechaCorte(){
		log.info("getDatosFechaCorte(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 
		Registros registros  = new Registros();
		try{
			con.conexionDB();
			qrySentencia.append(	"	SELECT DISTINCT  ");
			qrySentencia.append("	 TO_CHAR (a.df_fechafinmes, 'dd/mm/yyyy') as clave, ");
			qrySentencia.append("	 TO_CHAR (a.df_fechafinmes, 'dd/mm/yyyy') as descripcion  ");
			//qrySentencia.append("	 --a.df_fechafinmes  ");
			qrySentencia.append("FROM  ");
			qrySentencia.append("	 com_estado_cuenta a, comcat_if b ");
			qrySentencia.append("WHERE  ");
			qrySentencia.append("	 a.ic_if = b.ic_if ");
			qrySentencia.append("	 and a.ic_moneda =  ? ");
										conditions.add(moneda);
			
			if(!"C".equals(tipoLinea)){
			if(!"".equals(icIf)) {
					qrySentencia.append( "   and a.ic_if = ? " );
				conditions.add(icIf);
			}
			}else
			{
				qrySentencia.append( "   and a.ic_if = ? " );
				conditions.add("12");
			}
    		if(!"".equals(numIntermediario)){
    			if(!"C".equals(tipoLinea)){
					qrySentencia.append("   and b.ic_financiera = ?  ");
				conditions.add(numIntermediario);
				}else{
					qrySentencia.append("   and a.ig_cliente = ?  ");
					conditions.add(numSirac);
				}
				
			}
			
			qrySentencia.append("GROUP BY TO_CHAR (a.df_fechafinmes, 'dd/mm/yyyy') 	");
			//qrySentencia.append("ORDER BY clave DESC	");
			
			registros = con.consultarDB(qrySentencia.toString(),conditions);
			
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("getDatosFechaCorte  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 	
	
		log.info("qrySentencia Fecha de corte: "+qrySentencia.toString());
		log.info("conditions "+conditions);
		
		log.info("getDatosFechaCorte (S) ");
		return registros;
															
	}
	public Registros getObservaciones(){
	log.info("getObservaciones(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 
		Registros registros  = new Registros();
		try{
			con.conexionDB();
			qrySentencia.append(	"	SELECT ");
			qrySentencia.append("		 CG_OBSERVACIONES  ");
			qrySentencia.append("	FROM  ");
			qrySentencia.append("		 COM_OBSERVACIONES_CEDULA  ");
			qrySentencia.append("	WHERE  ");
			qrySentencia.append("		 trunc(DF_FECHA_CORTE) = trunc(to_date(?,'dd/mm/yyyy'))  ");
			qrySentencia.append("		 AND IC_MONEDA = ? ");
			qrySentencia.append("		 AND IC_IF= ? ");
			
			if(!"".equals(fechaCorte)){
				conditions.add(fechaCorte);
			}	
			if(!"".equals(moneda)) {
				conditions.add(moneda);
			}
    		if(!"".equals(icIf)){
				if(!"C".equals(tipoLinea))
				conditions.add(icIf);
				else
					conditions.add("12");
			}
			if(!"".equals(numSirac)){
				qrySentencia.append("		 AND IG_CLIENTE = ? ");
				conditions.add(numSirac);
			}
			registros = con.consultarDB(qrySentencia.toString(),conditions);
			
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("getObservaciones  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 	
	
		log.info("qrySentencia Observaciones: "+qrySentencia.toString());
		log.info("conditions "+conditions);
		
		log.info("getObservaciones (S) ");
		return registros;
	
	}
	public Registros getClaveIF(){
		log.info("getClaveIF(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 
		Registros  registros  = new Registros();
		try{
			con.conexionDB();
			qrySentencia.append("	select b.ic_if from com_estado_cuenta a, comcat_if b where a.IC_IF = b.IC_IF ");
			if(!"".equals(icIf)) {
				qrySentencia.append( "   and a.ic_if = ? " );
				conditions.add(icIf);
			}
    		if(!"".equals(numIntermediario)){
    			qrySentencia.append("   and b.ic_financiera = ?  ");
				conditions.add(numIntermediario);
			}
			qrySentencia.append("   group by b.ic_if  ");
			registros = con.consultarDB(qrySentencia.toString(),conditions);
			con.cierraConexionDB();
			
		}catch(Exception e){
			e.printStackTrace();
			log.error("getClaveIF  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		log.info("qrySentencia getClaveIF: "+qrySentencia.toString());
		log.info("conditions "+conditions);
		
		log.info("getClaveIF (S) ");
		return registros;
	
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


	public String getTipo() {
		return tipo;
	}


	public void setIcIf(String icIf) {
		this.icIf = icIf;
	}


	public String getIcIf() {
		return icIf;
	}


	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}


	public String getMoneda() {
		return moneda;
	}


	public void setNumIntermediario(String numIntermediario) {
		this.numIntermediario = numIntermediario;
	}


	public String getNumIntermediario() {
		return numIntermediario;
	}


	public void setFechaCorte(String fechaCorte) {
		this.fechaCorte = fechaCorte;
	}


	public String getFechaCorte() {
		return fechaCorte;
	}


	public void setTipoLinea(String tipoLinea)
	{
		this.tipoLinea = tipoLinea;
	}


	public String getTipoLinea()
	{
		return tipoLinea;
	}


	public void setNumSirac(String numSirac)
	{
		this.numSirac = numSirac;
	}


	public String getNumSirac()
	{
		return numSirac;
	}
}