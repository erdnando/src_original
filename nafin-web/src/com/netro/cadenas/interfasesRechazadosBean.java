package com.netro.cadenas; 

import com.netro.pdf.ComunesPDF;
import com.netro.xlsx.ComunesXLSX;

import java.io.Writer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.Correo;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;

import org.apache.commons.logging.Log;

public class interfasesRechazadosBean { 
	private String 		icproducto 				="";
	private String 		codigoaplicacion 		="";
	private String 		cmbestatus 				="";
	private String 		qrysentencia 			="";
	private String 		icif 					="";
	private String 		icnafinelectronico 		="";
	private String 		nombrejsp 				="";
	private String 		chckFueraFacultades 	="";
	private boolean 	divCod 					= false;

	////////////////L�neas de prueba////////////////
	////////////////////////////////////////////////
	private int    		numFolios 				= 0;
	private String 		foliosIds 				= "";
	private String 		pedidosIds 				= "";
	private String 		creditosIds 			= "";
	private String 		solicsIds 				= "";
	private String 		documentosIds 			= "";

	String strFoliosConDescripcionD 			= "";
	String strFoliosSinDescripcionD 			= "";
	String strFoliosConDescripcionA 			= "";
	String strFoliosSinDescripcionA 			= "";
	String strFoliosConDescripcionC 			= "";
	String strFoliosSinDescripcionC 			= "";
	String strFoliosConDescripcionI 			= "";
	String strFoliosSinDescripcionI 			= "";
	String strFoliosConDescripcionDis 			= "";
	String strFoliosSinDescripcionDis 			= "";

	private String 		condicionFolios2a 		="";
	private String 		condicionPedidos2a 		="";
	private String 		condicionCreditos2a 	="";
	private String 		condicionSolics2a 		="";
	private String 		condicionDocumentos2a 	="";

	private String 		foliosb 				="";
	private String 		pedidosb 				="";
	private String 		creditosb 				="";
	private String 		solicsb 				="";
	private String 		documentosb 			="";
	private String 		condicionFoliosb 		="";
	private String 		condicionPedidosb 		="";
	private String 		condicionCreditosb 		="";
	private String 		condicionSolicsb 		="";
	private String 		condicionDocumentosb 	="";
	private String 		strFacultad 			="";

	Vector idsFolio		  = new Vector();
	Vector idsPedido	  = new Vector();
	Vector idsSolic		  = new Vector();
	Vector idsCredito	  = new Vector();
	Vector idsDocumento	  = new Vector();

	private String tipoProceso;
	private String directorioPlantilla;
	private String ic_solicitudes ="";         
	

	////////////////L�neas de prueba////////////////
	////////////////////////////////////////////////

	public void setIcproducto(String icproducto){
		this.icproducto = icproducto;
	}
	public void setChckFueraFacultades(String chckFueraFacultades){
		this.chckFueraFacultades = chckFueraFacultades;
	}
	public void setCodigoaplicacion(String codigoaplicacion){
		this.codigoaplicacion = codigoaplicacion;
	}
	public void setCmbestatus(String cmbestatus){
		this.cmbestatus=cmbestatus;
	}
	public void setIcif(String icif){
		this.icif=icif;
	}
	
	////////////////////////////////
	////////////////////////////////
	public void setNombrejsp(String nombrejsp) {
		String 		dir 		= nombrejsp;
		String 		let 		= "";
		int 		first 		= 0, 
					posicion 	= 0, 
					diagonal 	= 0;
		boolean 	mas 		= false;
		for(int i=dir.length(); i>0; i--) {
			let=dir.substring(i-1,i);
			if(let.equals("/")){
				first=posicion;
				break;
			}
		posicion++;
		}//for(int i=dir.length(); i>0; i--)
		
		strFacultad = dir.substring(dir.length()-first,dir.length()-4).toUpperCase();

		if(strFacultad.length() > 20)
			strFacultad = strFacultad.substring(strFacultad.length() - 20);
		
		this.nombrejsp = strFacultad;
	}//setNombrejsp
	////////////////////////////////
	////////////////////////////////
	
	public void setIcnafinelectronico(String icnafinelectronico){
		this.icnafinelectronico=icnafinelectronico;
		this.qrysentencia = "";
		setQrysentencia("");
	}//setIcnafinelectronico
	
	public void setQrysentencia(String noUsado) {
		int 		comcontrol 	= 0;
		String 		sprodGen 	= (this.icproducto.equals("") || this.icproducto == null)? "": this.icproducto.substring(0,1);
		
		System.out.println("*********sprodGen*********: " + sprodGen);
		System.out.println("*********chckFueraFacultades*********: " + this.chckFueraFacultades	);
		System.out.println("*********chckFueraFacultades*********: " + this.chckFueraFacultades	);
		 System.out.println("*********  ic_solicitudes*********   "+this.ic_solicitudes);    
		 
		
		
		String 		cgestatus 				= "", 
					condicionExtra			= "", 
					campos 					= "", 
					condicionEstatus 		= "", 
					condicion 				= "", 
					condicion1 				= "", 
					condicion2 				= "", 
					condicion3 				= "", 
					cmbestatusvalor1 		= "", 
					cmbestatusvalor2 		= "",
					queryDE 				= "", 
					qrysentenciaDiversos	= "";
					
		this.qrysentencia 					= "";
		
		//this.cmbestatus puede llegar con un valor como: 7|NE
		VectorTokenizer valores_cmbestatus = new VectorTokenizer(this.cmbestatus,"|");
		Vector vc = valores_cmbestatus.getValuesVector();
		if(vc.size()>0) {
			cmbestatusvalor1=vc.get(0).toString();
			cmbestatusvalor2=vc.get(1).toString();
		}
		
		this.cmbestatus = cmbestatusvalor1;
		
		//codigoaplicacion
		if(this.codigoaplicacion.equals("PY")) { //PROYECTOS
			comcontrol 	= 1;
			cgestatus 	= " c1.cg_estatus = 'E' ";
			campos 		= 
				(sprodGen.equals("1") /*|| sprodGen.equals("2")*/ )?" c5.ic_error_proceso, ":
				(sprodGen.equals("0") || sprodGen.equals("4") /*|| sprodGen.equals("5")*/)?" inter.ic_error_proceso, ":"";
		} else if(this.codigoaplicacion.equals("CN")) {//CONTRATOS
			comcontrol 	= 2;
			cgestatus 	= " c2.cs_estatus = 'N' ";
			campos 		= 
				(sprodGen.equals("1") /*|| sprodGen.equals("2")*/)?" c2.ig_numero_linea_credito, ":
				(sprodGen.equals("0") || sprodGen.equals("4") /*|| sprodGen.equals("5")*/)?" inter.ig_numero_linea_credito, ":"";
		} else if(this.codigoaplicacion.equals("PR")) {//PRESTAMOS
			comcontrol 	= 3;
			cgestatus 	= " c3.cs_estatus = 'N' ";
			campos 		= 
				(sprodGen.equals("1") /* || sprodGen.equals("2")*/)?" c3.ig_numero_linea_credito,c3.ig_numero_contrato, ":
				(sprodGen.equals("0") || sprodGen.equals("4") /*|| sprodGen.equals("5")*/)?" inter.ig_numero_linea_credito,inter.ig_numero_contrato, ":"";
		} else if(this.codigoaplicacion.equals("DS")) {//DISPOSICIONES
			comcontrol 		= 4;
			cgestatus 		= " c4.cs_estatus = 'N' ";
			campos 			= 
				(sprodGen.equals("1") /*|| sprodGen.equals("2")*/)?" c4.ig_numero_linea_credito, c4.ig_numero_contrato, c4.ig_numero_prestamo, ":
				(sprodGen.equals("0") || sprodGen.equals("4") /*|| sprodGen.equals("5")*/)?" inter.ig_numero_linea_credito, inter.ig_numero_contrato, inter.ig_numero_prestamo, ":"";
			condicionExtra 	= 
				(sprodGen.equals("0") /*|| sprodGen.equals("5")*/ || sprodGen.equals("4"))?" AND inter.ic_error_proceso IS NOT NULL AND inter.cc_codigo_aplicacion IS NOT NULL ":"";
		} else if(this.codigoaplicacion.equals("DV")) {//DIVERSOS
			comcontrol 		= 5;
			cgestatus 		= " c1.cs_estatus = 'N' ";
			//ya esta 0,4,5 falta 1,2
			campos 			= 
				(sprodGen.equals("1"))?" 'control05' AS tablaorigen, ":
				//(sprodGen.equals("2"))?" 'controlant05' AS tablaorigen, ":
				(sprodGen.equals("0"))?" decode(inter.ic_error_proceso, 7, 'interfase_error7', 'interfase') AS tablaorigen, ":
				(sprodGen.equals("4"))?" decode(inter.ic_error_proceso, 7, 'dis_interfase_error7', 'dis_interfase') AS tablaorigen, ":"";
				//(sprodGen.equals("5"))?" decode(inter.ic_error_proceso, 7, 'inv_interfase_error7', 'inv_interfase') AS tablaorigen, ":"";
			condicionExtra 	= 
				(/*sprodGen.equals("5") ||*/ sprodGen.equals("4"))?" AND inter.ig_proceso_numero IS NULL AND inter.cc_codigo_aplicacion = 'NE' AND ep.cc_codigo_aplicacion = 'NE' ":
				(sprodGen.equals("0"))?" AND inter.ig_proceso_numero IS NULL AND ep.cc_codigo_aplicacion = 'NE' ":"";
		}
		
		if (!this.icif.equals("")) {
			condicion += " and d.ic_if = " + this.icif;
			condicion1 += " and de.ic_if = " + this.icif;
			condicion2 += " and lc.ic_if = " + this.icif;
			condicion3 += " and i.ic_if = "+ this.icif +" and sp.ic_if= "+ this.icif;
		}
		
		if (!this.icnafinelectronico.equals("")) {
			String condicional = " and ic_nafin_electronico = " + this.icnafinelectronico;
			condicion += condicional;
			condicion1 += condicional;
			condicion2 += condicional;
			condicion3 += " and n.ic_nafin_electronico = "+ this.icnafinelectronico + " and cg_tipo='P' ";
		}
		
		////////////////L�neas de prueba////////////////
		////////////////////////////////////////////////

		if(!foliosb.equals("")){
				condicionFoliosb = " AND s.ic_folio in ("+foliosb+")";
		}
		if(!pedidosb.equals("")){
				condicionPedidosb = " AND a.ic_pedido in("+pedidosb+")";
		}
		if(!creditosb.equals("") ){
			condicionCreditosb = " AND sp.ic_solic_portal in ("+creditosb+") AND inter.ic_solic_portal in ("+creditosb+")";
		}
		if(!solicsb.equals("") ){
				condicionSolicsb = " AND inter.cc_disposicion in ("+solicsb+")";
		}
		if(!documentosb.equals("") ){
			if(nombrejsp.equals("15MONPROC1A")){
				condicionDocumentosb = " AND doc.ic_documento in ("+documentosb+")";
			}
			if(nombrejsp.equals("15MONPROC2A") || nombrejsp.equals("15MONPROC3A") || nombrejsp.equals("15MONPROC4A") || nombrejsp.equals("15MONPROC5A")){
				condicionDocumentosb = " AND inter.ic_documento in ("+documentosb+")";
			}
		}

		////////////////L�neas de prueba////////////////
		////////////////////////////////////////////////

		//
		if( 
			nombrejsp.equals("15MONPROC1A") || 
			nombrejsp.equals("15MONPROC2A") || 
			nombrejsp.equals("15MONPROC3A") || 
			nombrejsp.equals("15MONPROC4A") || 
			nombrejsp.equals("15MONPROC5A")) {
			if(sprodGen.equals("")) {
				sprodGen = "TD";
			}
		}
		
		if((sprodGen.equals("1") || sprodGen.equals("")) || !idsFolio.isEmpty()) {
			// Diversos-Descuento 1er Piso, (supongo)
			if(this.codigoaplicacion.equals("DV")) {
				
				if(!"S".equals(chckFueraFacultades)) {
					queryDE = 
						" SELECT '1' AS producto, i.cg_razon_social AS nombreif, "+
						"       p.cg_razon_social AS nombrepyme, n.ic_nafin_electronico, "+
						"       p.in_numero_sirac AS numclisirac, c1.df_hora_proceso, c1.ic_folio, "+
						"       c1.cg_descripcion, c1.ig_codigo_agencia, c1.ig_codigo_sub_aplicacion, "+
						"       c1.ig_numero_linea_fondeo, c1.ig_aprobado_por,nvl(ds.in_importe_recibir_fondeo ,  ds.in_importe_recibir) as monto_descontar, "+
						"       'control01' AS tablaorigen, "+
						"       '' modalidad, "+
						"       '1-15monitoreo/15monproc.jsp-interfasesRechazadosBean.java' as pantalla" +
						"  FROM com_control01 c1, "+
						"       com_solicitud s, "+
						"       com_docto_seleccionado ds, "+
						"       comcat_if i, "+
						"       com_documento d, "+
						"       comcat_pyme p, "+
						"       comrel_nafin n "+
						" WHERE c1.ic_folio = s.ic_folio "+
						"   AND s.ic_documento = ds.ic_documento "+
						"   AND ds.ic_linea_credito_emp is null "+
						"   AND d.ic_if = i.ic_if "+
						"   AND ds.ic_documento = d.ic_documento "+
						"   AND d.ic_pyme = p.ic_pyme "+
						"   AND n.ic_epo_pyme_if = d.ic_pyme "+
						"   AND n.cg_tipo = 'P' "+
						"   AND c1.cg_estatus = 'N' " + condicion + this.getCondicionProducto("c1")+ condicionFoliosb;
						 
						     
						if( !ic_solicitudes.equals("")) {
							queryDE += " and  c1.ic_folio in ("+ic_solicitudes+")";	//F017-2015  						
						}
						  
						queryDE += " UNION "+
						" SELECT '1----' AS producto, i.cg_razon_social AS nombreif, "+
						"       p.cg_razon_social AS nombrepyme, n.ic_nafin_electronico, "+
						"       p.in_numero_sirac AS numclisirac, c1.df_hora_proceso, c1.ic_folio, "+
						"       c1.cg_descripcion, c1.ig_codigo_agencia, c1.ig_codigo_sub_aplicacion, "+
						"       c1.ig_numero_linea_fondeo, c1.ig_aprobado_por,nvl(ds.in_importe_recibir_fondeo ,  ds.in_importe_recibir) as monto_descontar, "+
						"       'control01' AS tablaorigen, "+
						"       'Factoraje con Recurso' modalidad, "+
						"       '1-15monitoreo/15monproc.jsp-interfasesRechazadosBean.java' as pantalla" +
						"  FROM com_control01 c1, "+
						"       com_solicitud s, "+
						"       com_docto_seleccionado ds, "+
						"       emp_linea_credito lc, "+
						"       comcat_if i, "+
						"       com_documento d, "+
						"       comcat_pyme p, "+
						"       comrel_nafin n "+
						" WHERE c1.ic_folio = s.ic_folio "+
						"   AND s.ic_documento = ds.ic_documento "+
						"   AND d.ic_if = i.ic_if "+
						"   AND ds.ic_documento = d.ic_documento "+
						"   AND ds.ic_linea_credito_emp = lc.ic_linea_credito "+
						"   AND d.ic_pyme = p.ic_pyme "+
						"   AND n.ic_epo_pyme_if = d.ic_pyme "+
						"   AND n.cg_tipo = 'P' "+
						"   AND c1.cg_estatus = 'N' " + condicion + this.getCondicionProducto("c1")+ condicionFoliosb;
				}//if(!"S".equals(chckFueraFacultades))
				
				if(!"".equals(this.cmbestatus)) {
					if ("7".equals(this.cmbestatus)){
						this.qrysentencia += queryDE;
					} else {
						this.divCod = true;
						if("S".equals(chckFueraFacultades)) {
							condicionEstatus =  "    AND c5.IC_ERROR_PROCESO = 14 ";
						} else {
							condicionEstatus =  "    AND c5.IC_ERROR_PROCESO = "+this.cmbestatus;
						}
					}
				} else {
					this.qrysentencia += queryDE;
					System.out.println("this.qrysentencia 1:: "+this.qrysentencia);
				}
				
				System.out.println("this.qrysentencia 2:: "+this.qrysentencia);
				if (!"7".equals(this.cmbestatus)) {
					this.qrysentencia += (!"".equals(this.qrysentencia))?" UNION ":"";
					System.out.println("this.qrysentencia union :: "+this.qrysentencia);    
					// Diversos-Descuento 2do Piso, (supongo)
					this.qrysentencia +=
						" SELECT /*+ use_nl(c5 s ds d p n i ep) */ "+
						"       '1' AS producto, i.cg_razon_social AS nombreif, "+
						"       p.cg_razon_social AS nombrepyme, n.ic_nafin_electronico, "+
						"       p.in_numero_sirac AS numclisirac, c5.df_hora_proceso, c5.ic_folio, "+
						"       ep.cg_descripcion, c5.ig_codigo_agencia, c5.ig_codigo_sub_aplicacion, "+
						"       c5.ig_numero_linea_fondeo, c5.ig_aprobado_por, nvl(ds.in_importe_recibir_fondeo ,  ds.in_importe_recibir) as monto_descontar, "+
						"       'control05' AS tablaorigen, "+
						"       '' modalidad, "+
						"       '2-15monitoreo/15monproc.jsp-interfasesRechazadosBean.java' as pantalla " +
						"  FROM com_control05 c5, "+
						"       comcat_error_procesos ep, "+
						"       com_solicitud s, "+
						"       com_docto_seleccionado ds, "+
						"       comcat_if i, "+
						"       com_documento d, "+
						"       comcat_pyme p, "+
						"       comrel_nafin n "+
						" WHERE c5.ic_error_proceso = ep.ic_error_proceso "+
						"   AND c5.cc_codigo_aplicacion = ep.cc_codigo_aplicacion "+
						"   AND c5.ic_folio = s.ic_folio "+
						"   AND s.ic_documento = ds.ic_documento "+
						"   AND d.ic_if = i.ic_if "+
						"   AND ds.ic_documento = d.ic_documento "+
						"   AND ds.ic_linea_credito_emp is null "+
						"   AND d.ic_pyme = p.ic_pyme "+
						"   AND n.ic_epo_pyme_if = d.ic_pyme "+
						"   AND n.cg_tipo = 'P' ";
						
					if("S".equals(chckFueraFacultades)) {
						System.out.println("Dentro del if chckFueraFacultades :: "+chckFueraFacultades);
						this.qrysentencia += (!"".equals(this.qrysentencia))?"   AND c5.ic_error_proceso = 14 ":"";
					} else {
						System.out.println("Dentro del else chckFueraFacultades :: "+chckFueraFacultades);
						this.qrysentencia += (!"".equals(this.qrysentencia))?"   AND c5.ic_error_proceso IN (0, 1, 2, 3, 4, 5, 10, 11) ":"";
					}
					//"   AND c5.ic_error_proceso IN (0, 1, 2, 3, 4, 5, 10, 11) "+
					this.qrysentencia += "   AND c5.cc_codigo_aplicacion = 'NE' "+
					condicionEstatus + condicion + condicionFoliosb;
						
					if( !ic_solicitudes.equals("")) {
						this.qrysentencia += " and  c5.ic_folio in ("+ic_solicitudes+")";	//F017-2015  						
					}
						
					this.qrysentencia += 
						" UNION "+
						" SELECT /*+ use_nl(c5 s ds d p n i ep) */ "+
						"       '1' AS producto, i.cg_razon_social AS nombreif, "+
						"       p.cg_razon_social AS nombrepyme, n.ic_nafin_electronico, "+
						"       p.in_numero_sirac AS numclisirac, c5.df_hora_proceso, c5.ic_folio, "+
						"       ep.cg_descripcion, c5.ig_codigo_agencia, c5.ig_codigo_sub_aplicacion, "+
						"       c5.ig_numero_linea_fondeo, c5.ig_aprobado_por, nvl(ds.in_importe_recibir_fondeo ,  ds.in_importe_recibir) as monto_descontar, "+
						"       'control05' AS tablaorigen, "+
						"       'Factoraje con Recurso' modalidad, "+
						"       '2-15monitoreo/15monproc.jsp-interfasesRechazadosBean.java' as pantalla " +
						"  FROM com_control05 c5, "+
						"       comcat_error_procesos ep, "+
						"       com_solicitud s, "+
						"       com_docto_seleccionado ds, "+
						"       emp_linea_credito lc, "+
						"       comcat_if i, "+
						"       com_documento d, "+
						"       comcat_pyme p, "+
						"       comrel_nafin n "+
						" WHERE c5.ic_error_proceso = ep.ic_error_proceso "+
						"   AND c5.cc_codigo_aplicacion = ep.cc_codigo_aplicacion "+
						"   AND c5.ic_folio = s.ic_folio "+
						"   AND s.ic_documento = ds.ic_documento "+
						"   AND d.ic_if = i.ic_if "+
						"   AND ds.ic_documento = d.ic_documento "+
						"   AND ds.ic_linea_credito_emp = lc.ic_linea_credito "+
						"   AND d.ic_pyme = p.ic_pyme "+
						"   AND n.ic_epo_pyme_if = d.ic_pyme "+
						"   AND n.cg_tipo = 'P' ";
						
						if( !ic_solicitudes.equals("")) { 
							this.qrysentencia += " and  c5.ic_folio in ("+ic_solicitudes+")";	//F017-2015  						
						}
					
					System.out.println("Fuera del if chckFueraFacultades :: "+chckFueraFacultades);
					if("S".equals(chckFueraFacultades)) {
						System.out.println("Dentro del if chckFueraFacultades :: "+chckFueraFacultades);
						this.qrysentencia += (!"".equals(this.qrysentencia))?"   AND c5.ic_error_proceso = 14 ":"";
					} else {
						System.out.println("Dentro del else chckFueraFacultades :: "+chckFueraFacultades);
						this.qrysentencia += (!"".equals(this.qrysentencia))?"   AND c5.ic_error_proceso IN (0, 1, 2, 3, 4, 5, 10, 11) ":"";
					}
					//"   AND c5.ic_error_proceso IN (0, 1, 2, 3, 4, 5, 10, 11) "+
					this.qrysentencia += "   AND c5.cc_codigo_aplicacion = 'NE' "+
					condicionEstatus + condicion + condicionFoliosb;
				}//if (!"7".equals(this.cmbestatus))
			} else {
				if(sprodGen.equals("") || sprodGen.equals("TD")) {//cuando la consulta es para todos los productos
					campos = (this.codigoaplicacion.equals("PY") )?" c5.ic_error_proceso, ":campos;
					campos = (this.codigoaplicacion.equals("CN") )?" c2.ig_numero_linea_credito, ":campos;
					campos = (this.codigoaplicacion.equals("PR") )?" c3.ig_numero_linea_credito,c3.ig_numero_contrato, ":campos;
					campos = (this.codigoaplicacion.equals("DS") )?" c4.ig_numero_linea_credito, c4.ig_numero_contrato, c4.ig_numero_prestamo, ":campos;
				}//if(sprodGen.equals("") || sprodGen.equals("TD"))
				//1er. Query Rechazos - A partir de Proyectos 	dinamico por el comcontrol	Descuento
				
				System.out.println("comcontrol======>   "+comcontrol);     
				
				this.qrysentencia =
					 " SELECT /*+ use_nl(c5 c"+comcontrol+" s ds d p n i ep) */ " +
					 " 		'1' AS producto, i.cg_razon_social AS nombreif, " +
					 "      p.cg_razon_social AS nombrepyme, n.ic_nafin_electronico, " +
					 "      p.in_numero_sirac AS numclisirac, c5.df_hora_proceso, c"+comcontrol+".ic_folio, " +
					 "      ep.cg_descripcion, c"+comcontrol+".ig_codigo_agencia, c"+comcontrol+".ig_codigo_sub_aplicacion, " +
					 "      c"+comcontrol+".ig_numero_linea_fondeo, c"+comcontrol+".ig_aprobado_por, c"+comcontrol+".fn_monto_operacion as monto_descontar, " +
					 campos +
					"       '' modalidad, "+
					 "      '3-15monitoreo/15monproc"+(comcontrol+1)+".jsp-interfasesRechazadosBean.java' as pantalla" +
					 " FROM com_control0"+comcontrol+" c"+comcontrol+", "+
					 "      com_solicitud s, " +
					 "      com_docto_seleccionado ds, "+
					 "      comcat_if i, "+
					 "      com_documento d, "+
					 "      comcat_pyme p, "+
					 "      comrel_nafin n, "+
					 " 		com_control05 c5, "+
					 "      comcat_error_procesos ep "+
					 " WHERE c"+comcontrol+".ic_folio = s.ic_folio "+
					 "  AND c"+comcontrol+".ic_folio = c5.ic_folio "+
					 "  AND c5.ic_error_proceso = ep.ic_error_proceso "+
					 "  AND c5.cc_codigo_aplicacion = ep.cc_codigo_aplicacion "+
					 "  AND s.ic_documento = ds.ic_documento "+
					 "  AND d.ic_if = i.ic_if "+
					 "  AND ds.ic_documento = d.ic_documento "+
					 "  AND ds.ic_linea_credito_emp is null "+
					 "  AND d.ic_pyme = p.ic_pyme " +
					 "  AND n.ic_epo_pyme_if = d.ic_pyme " +
					 "  AND n.cg_tipo = 'P' "+ condicionFoliosb +
					 "  AND "+ cgestatus+condicion;
//************************************************************************************************************************************************************
						if(!(this.codigoaplicacion.equals("DV") && ( sprodGen.equals("1") /*|| sprodGen.equals("2")*/ ))) {
							if(!"".equals(this.cmbestatus)) {
								if("S".equals(chckFueraFacultades)) { 
									condicionEstatus = 
										(sprodGen.equals("1") /*|| sprodGen.equals("2")*/ )?"  AND c5.ic_error_proceso = 14 AND c5.cc_codigo_aplicacion = '"+cmbestatusvalor2+"'  ":
										(sprodGen.equals("0") || sprodGen.equals("4") /*|| sprodGen.equals("5")*/ )?"  AND inter.ic_error_proceso = 14 AND inter.cc_codigo_aplicacion = '"+cmbestatusvalor2+"'  ":"";
								} else { 
									condicionEstatus = 
										(sprodGen.equals("1") /*|| sprodGen.equals("2")*/ )?"  AND c5.ic_error_proceso = "+this.cmbestatus+" AND c5.cc_codigo_aplicacion = '"+cmbestatusvalor2+"'  ":
										(sprodGen.equals("0") || sprodGen.equals("4") /*|| sprodGen.equals("5")*/ )?"  AND inter.ic_error_proceso = "+this.cmbestatus+" AND inter.cc_codigo_aplicacion = '"+cmbestatusvalor2+"'  ":"";
								}
							}
						}
						
						if(!nombrejsp.equals("15MONPROC1A") && !nombrejsp.equals("15MONPROC2A") && !nombrejsp.equals("15MONPROC3A") && !nombrejsp.equals("15MONPROC4A") && !nombrejsp.equals("15MONPROC5A")){
							this.qrysentencia += condicionEstatus + ((divCod==true)?this.getCondicionProducto():"");
						}
						
						if( !ic_solicitudes.equals("")) {
							this.qrysentencia += " and  c"+comcontrol+".ic_folio in ("+ic_solicitudes+")";	//F017-2015  						
						}
						
//************************************************************************************************************************************************************
				this.qrysentencia +=
					 " UNION " +
					 " SELECT /*+ use_nl(c5 c"+comcontrol+" s ds d p n i ep) */ " +
					 " 		'1' AS producto, i.cg_razon_social AS nombreif, " +
					 "      p.cg_razon_social AS nombrepyme, n.ic_nafin_electronico, " +
					 "      p.in_numero_sirac AS numclisirac, c5.df_hora_proceso, c"+comcontrol+".ic_folio, " +
					 "      ep.cg_descripcion, c"+comcontrol+".ig_codigo_agencia, c"+comcontrol+".ig_codigo_sub_aplicacion, " +
					 "      c"+comcontrol+".ig_numero_linea_fondeo, c"+comcontrol+".ig_aprobado_por, c"+comcontrol+".fn_monto_operacion as monto_descontar, " +
					 campos +
					"       'Factoraje con Recurso' modalidad, "+
					 "      '3-15monitoreo/15monproc"+(comcontrol+1)+".jsp-interfasesRechazadosBean.java' as pantalla" +
					 " FROM com_control0"+comcontrol+" c"+comcontrol+", "+
					 "      com_solicitud s, " +
					 "      com_docto_seleccionado ds, "+
					 "      emp_linea_credito lc, "+
					 "      comcat_if i, "+
					 "      com_documento d, "+
					 "      comcat_pyme p, "+
					 "      comrel_nafin n, "+
					 " 		com_control05 c5, "+
					 "      comcat_error_procesos ep "+
					 " WHERE c"+comcontrol+".ic_folio = s.ic_folio "+
					 "  AND c"+comcontrol+".ic_folio = c5.ic_folio "+
					 "  AND c5.ic_error_proceso = ep.ic_error_proceso "+
					 "  AND c5.cc_codigo_aplicacion = ep.cc_codigo_aplicacion "+
					 "  AND s.ic_documento = ds.ic_documento "+
					 "  AND d.ic_if = i.ic_if "+
					 "  AND ds.ic_documento = d.ic_documento "+
					 "  AND ds.ic_linea_credito_emp = lc.ic_linea_credito "+
					 "  AND d.ic_pyme = p.ic_pyme " +
					 "  AND n.ic_epo_pyme_if = d.ic_pyme " +
					 "  AND n.cg_tipo = 'P' "+ condicionFoliosb +
					 "  AND "+ cgestatus+condicion;
					 
					 if( !ic_solicitudes.equals("")) { 
							this.qrysentencia += " and  c"+comcontrol+".ic_folio in ("+ic_solicitudes+")";	//F017-2015  						
						}
						
			}//NO codigoaplicacion.equals("DV")
		}//if((sprodGen.equals("1") || sprodGen.equals("")) || !idsFolio.isEmpty())
		
		
		/*
		if((sprodGen.equals("2") || sprodGen.equals("")) || !idsPedido.isEmpty()) {
			//Diversos-Pedidos 2
			if(this.codigoaplicacion.equals("DV")) {
				queryDE =
					" SELECT TO_CHAR (p.ic_producto_nafin) AS producto, "+
					"       i.cg_razon_social AS nombreif, py.cg_razon_social AS nombrepyme, "+
					"       n.ic_nafin_electronico, py.in_numero_sirac AS numclisirac, "+
					"       c1.df_hora_proceso, TO_CHAR (c1.ic_pedido) AS ic_folio, "+
					"       c1.cg_descripcion, c1.ig_codigo_agencia, c1.ig_codigo_sub_aplicacion, "+
					"       c1.ig_numero_linea_fondeo, c1.ig_aprobado_por, ps.fn_credito_recibir as monto_descontar, "+
					"       'controlant01' AS tablaorigen, "+
					"       '' modalidad, "+
					"      '4-15monitoreo/15monproc.jsp-interfasesRechazadosBean.java' as pantalla " +
					"  FROM com_controlant01 c1, "+
					"       com_anticipo a, "+
					"       com_pedido_seleccionado ps, "+
					"       comcat_if i, "+
					"       com_pedido p, "+
					"       comcat_pyme py, "+
					"       comrel_nafin n, "+
					"       com_linea_credito lc "+
					" WHERE c1.ic_pedido = a.ic_pedido "+
					"   AND a.ic_pedido = ps.ic_pedido "+
					"   AND ps.ic_linea_credito = lc.ic_linea_credito "+
					"   AND lc.ic_if = i.ic_if "+
					"   AND ps.ic_pedido = p.ic_pedido "+
					"   AND p.ic_pyme = py.ic_pyme "+
					"   AND n.ic_epo_pyme_if = p.ic_pyme "+
					"   AND n.cg_tipo = 'P' "+
					"   AND c1.cg_estatus = 'N' "+condicion2 + this.getCondicionProducto("c1") + condicionPedidosb+
					" UNION "+
					" SELECT TO_CHAR (p.ic_producto_nafin) AS producto, "+
					"       i.cg_razon_social AS nombreif, py.cg_razon_social AS nombrepyme, "+
					"       n.ic_nafin_electronico, py.in_numero_sirac AS numclisirac, "+
					"       c1.df_hora_proceso, TO_CHAR (c1.ic_pedido) AS ic_folio, "+
					"       c1.cg_descripcion, c1.ig_codigo_agencia, c1.ig_codigo_sub_aplicacion, "+
					"       c1.ig_numero_linea_fondeo, c1.ig_aprobado_por, ps.fn_credito_recibir as monto_descontar, "+
					"       'controlant01' AS tablaorigen, "+
					"       'Financiamiento a Contratos' modalidad, "+
					"      '4-15monitoreo/15monproc.jsp-interfasesRechazadosBean.java' as pantalla " +
					"  FROM com_controlant01 c1, "+
					"       com_anticipo a, "+
					"       com_pedido_seleccionado ps, "+
					"       comcat_if i, "+
					"       com_pedido p, "+
					"       comcat_pyme py, "+
					"       comrel_nafin n, "+
					"       emp_linea_credito lc "+
					" WHERE c1.ic_pedido = a.ic_pedido "+
					"   AND a.ic_pedido = ps.ic_pedido "+
					"   AND ps.ic_linea_credito_emp = lc.ic_linea_credito "+
					"   AND lc.ic_if = i.ic_if "+
					"   AND ps.ic_pedido = p.ic_pedido "+
					"   AND p.ic_pyme = py.ic_pyme "+
					"   AND n.ic_epo_pyme_if = p.ic_pyme "+
					"   AND n.cg_tipo = 'P' "+
					"   AND c1.cg_estatus = 'N' "+condicion2 + this.getCondicionProducto("c1") + condicionPedidosb;
					
				if(!"".equals(this.cmbestatus)) {
					if ("7".equals(this.cmbestatus)) {
						this.qrysentencia += queryDE;
					} else {
						condicionEstatus =  "    AND c5.IC_ERROR_PROCESO = "+this.cmbestatus;
					}
				} else {
					this.qrysentencia += (!"".equals(this.qrysentencia))?" UNION ":"";
					this.qrysentencia += queryDE;
				}

				//Diversos-Pedidos 1
				if (!"7".equals(this.cmbestatus)) {
					this.qrysentencia += (!"".equals(this.qrysentencia))?" UNION ":"";
					this.qrysentencia +=
						" SELECT TO_CHAR (p.ic_producto_nafin) AS producto, "+
						"       i.cg_razon_social AS nombreif, py.cg_razon_social AS nombrepyme, "+
						"       n.ic_nafin_electronico, py.in_numero_sirac AS numclisirac, "+
						"       c5.df_hora_proceso, TO_CHAR (c5.ic_pedido) AS ic_folio, "+
						"       ep.cg_descripcion, c5.ig_codigo_agencia, c5.ig_codigo_sub_aplicacion, "+
						"       c5.ig_numero_linea_fondeo, c5.ig_aprobado_por, ps.fn_credito_recibir as monto_descontar, "+
						"       'controlant05' AS tablaorigen, "+
						"       '' modalidad, "+
						"      '5-15monitoreo/15monproc.jsp-interfasesRechazadosBean.java' as pantalla " +
						"  FROM com_controlant05 c5, "+
						"       comcat_error_procesos ep, "+
						"       com_anticipo a, "+
						"       com_pedido_seleccionado ps, "+
						"       comcat_if i, "+
						"       com_pedido p, "+
						"       comcat_pyme py, "+
						"       comrel_nafin n, "+
						"       com_linea_credito lc "+
						" WHERE c5.ic_error_proceso = ep.ic_error_proceso "+
						"   AND c5.cc_codigo_aplicacion = ep.cc_codigo_aplicacion "+
						"   AND c5.ic_pedido = a.ic_pedido "+
						"   AND a.ic_pedido = ps.ic_pedido "+
						"   AND ps.ic_linea_credito = lc.ic_linea_credito "+
						"   AND lc.ic_if = i.ic_if "+
						"   AND ps.ic_pedido = p.ic_pedido "+
						"   AND p.ic_pyme = py.ic_pyme "+
						"   AND p.ic_pyme = n.ic_epo_pyme_if "+
						"   AND n.cg_tipo = 'P' "+
						"   AND c5.ic_error_proceso IN (0, 1, 2, 3, 4, 5) "+
						"   AND c5.cc_codigo_aplicacion = 'NE' "+
						condicionEstatus + condicion2 + condicionPedidosb+
						" UNION "+
						" SELECT TO_CHAR (p.ic_producto_nafin) AS producto, "+
						"       i.cg_razon_social AS nombreif, py.cg_razon_social AS nombrepyme, "+
						"       n.ic_nafin_electronico, py.in_numero_sirac AS numclisirac, "+
						"       c5.df_hora_proceso, TO_CHAR (c5.ic_pedido) AS ic_folio, "+
						"       ep.cg_descripcion, c5.ig_codigo_agencia, c5.ig_codigo_sub_aplicacion, "+
						"       c5.ig_numero_linea_fondeo, c5.ig_aprobado_por, ps.fn_credito_recibir as monto_descontar, "+
						"       'controlant05' AS tablaorigen, "+
						"       'Financiamiento a Contratos' modalidad, "+
						"      '5-15monitoreo/15monproc.jsp-interfasesRechazadosBean.java' as pantalla " +
						"  FROM com_controlant05 c5, "+
						"       comcat_error_procesos ep, "+
						"       com_anticipo a, "+
						"       com_pedido_seleccionado ps, "+
						"       comcat_if i, "+
						"       com_pedido p, "+
						"       comcat_pyme py, "+
						"       comrel_nafin n, "+
						"       emp_linea_credito lc "+
						" WHERE c5.ic_error_proceso = ep.ic_error_proceso "+
						"   AND c5.cc_codigo_aplicacion = ep.cc_codigo_aplicacion "+
						"   AND c5.ic_pedido = a.ic_pedido "+
						"   AND a.ic_pedido = ps.ic_pedido "+
						"   AND ps.ic_linea_credito_emp = lc.ic_linea_credito "+
						"   AND lc.ic_if = i.ic_if "+
						"   AND ps.ic_pedido = p.ic_pedido "+
						"   AND p.ic_pyme = py.ic_pyme "+
						"   AND p.ic_pyme = n.ic_epo_pyme_if "+
						"   AND n.cg_tipo = 'P' "+
						"   AND c5.ic_error_proceso IN (0, 1, 2, 3, 4, 5) "+
						"   AND c5.cc_codigo_aplicacion = 'NE' "+
						condicionEstatus + condicion2 + condicionPedidosb;
				}//if (!"7".equals(this.cmbestatus))
			} else {
				if(sprodGen.equals("") || sprodGen.equals("TD")) {//cuando la consulta es para todos los productos
					campos = (this.codigoaplicacion.equals("PY"))?" c5.ic_error_proceso, ":campos;
					campos = (this.codigoaplicacion.equals("CN"))?" c2.ig_numero_linea_credito, ":campos;
					campos = (this.codigoaplicacion.equals("PR"))?" c3.ig_numero_linea_credito,c3.ig_numero_contrato, ":campos;
					campos = (this.codigoaplicacion.equals("DS"))?" c4.ig_numero_linea_credito, c4.ig_numero_contrato, c4.ig_numero_prestamo, ":campos;
				}//if(sprodGen.equals("") || sprodGen.equals("TD"))
				
				//
				this.qrysentencia += (sprodGen.equals("") || (sprodGen.equals("TD") && !idsFolio.isEmpty()))?" UNION ":"";
				
				//2do. Query Rechazos - A partir de Proyectos  Pedidos
				this.qrysentencia +=
					" SELECT TO_CHAR (p.ic_producto_nafin) AS producto, "+
					"      i.cg_razon_social AS nombreif, py.cg_razon_social AS nombrepyme, "+
					"      n.ic_nafin_electronico, py.in_numero_sirac AS numclisirac, "+
					"      c5.df_hora_proceso, TO_CHAR (c"+comcontrol+".ic_pedido) AS ic_folio, "+
					"      ep.cg_descripcion, c"+comcontrol+".ig_codigo_agencia, c"+comcontrol+".ig_codigo_sub_aplicacion, "+
					"      c"+comcontrol+".ig_numero_linea_fondeo, c"+comcontrol+".ig_aprobado_por, c"+comcontrol+".fn_monto_operacion as monto_descontar, "+
					campos +
					"       '' modalidad, "+
					"      '6-15monitoreo/15monproc"+(comcontrol+1)+".jsp-interfasesRechazadosBean.java' as pantalla "+
					" FROM com_controlant0"+comcontrol+" c"+comcontrol+", "+
					"      com_anticipo a, "+
					"      com_pedido_seleccionado ps, "+
					"      comcat_if i, "+
					"      com_pedido p, "+
					"      comcat_pyme py, "+
					"      comrel_nafin n, "+
					"      com_controlant05 c5, "+
					"      comcat_error_procesos ep, "+
					"      com_linea_credito lc "+
					" WHERE c"+comcontrol+".ic_pedido = a.ic_pedido "+
					"  AND c"+comcontrol+".ic_pedido = c5.ic_pedido "+
					"  AND c5.ic_error_proceso = ep.ic_error_proceso "+
					"  AND c5.cc_codigo_aplicacion = ep.cc_codigo_aplicacion "+
					"  AND a.ic_pedido = ps.ic_pedido "+
					"  AND ps.ic_linea_credito = lc.ic_linea_credito "+
					"  AND lc.ic_if = i.ic_if "+
					"  AND ps.ic_pedido = p.ic_pedido "+
					"  AND p.ic_pyme = py.ic_pyme "+
					"  AND n.ic_epo_pyme_if = p.ic_pyme "+
					"  AND n.cg_tipo = 'P' "+ condicionPedidosb +
					"  AND "+ cgestatus;
				this.qrysentencia += condicion2;
				this.qrysentencia +=
					" UNION ALL "+
					" SELECT TO_CHAR (p.ic_producto_nafin) AS producto, "+
					"      i.cg_razon_social AS nombreif, py.cg_razon_social AS nombrepyme, "+
					"      n.ic_nafin_electronico, py.in_numero_sirac AS numclisirac, "+
					"      c5.df_hora_proceso, TO_CHAR (c"+comcontrol+".ic_pedido) AS ic_folio, "+
					"      ep.cg_descripcion, c"+comcontrol+".ig_codigo_agencia, c"+comcontrol+".ig_codigo_sub_aplicacion, "+
					"      c"+comcontrol+".ig_numero_linea_fondeo, c"+comcontrol+".ig_aprobado_por, c"+comcontrol+".fn_monto_operacion as monto_descontar, "+
					campos +
					"      'Financiamiento a Contratos' modalidad, "+
					"      '6-15monitoreo/15monproc"+(comcontrol+1)+".jsp-interfasesRechazadosBean.java' as pantalla "+
					" FROM com_controlant0"+comcontrol+" c"+comcontrol+", "+
					"      com_anticipo a, "+
					"      com_pedido_seleccionado ps, "+
					"      comcat_if i, "+
					"      com_pedido p, "+
					"      comcat_pyme py, "+
					"      comrel_nafin n, "+
					"      com_controlant05 c5, "+
					"      comcat_error_procesos ep, "+
					"      emp_linea_credito lc "+
					" WHERE c"+comcontrol+".ic_pedido = a.ic_pedido "+
					"  AND c"+comcontrol+".ic_pedido = c5.ic_pedido "+
					"  AND c5.ic_error_proceso = ep.ic_error_proceso "+
					"  AND c5.cc_codigo_aplicacion = ep.cc_codigo_aplicacion "+
					"  AND a.ic_pedido = ps.ic_pedido "+
					"  AND ps.ic_linea_credito_emp = lc.ic_linea_credito "+
					"  AND lc.ic_if = i.ic_if "+
					"  AND ps.ic_pedido = p.ic_pedido "+
					"  AND p.ic_pyme = py.ic_pyme "+
					"  AND n.ic_epo_pyme_if = p.ic_pyme "+
					"  AND n.cg_tipo = 'P' "+ condicionPedidosb +
					"  AND "+ cgestatus;
				this.qrysentencia += condicion2;
			}
		}//if((sprodGen.equals("2") || sprodGen.equals("")) || !idsPedido.isEmpty())
		
		*/

		//
		if((sprodGen.equals("0") || sprodGen.equals("")) || !idsCredito.isEmpty()) {
			if(sprodGen.equals("") || sprodGen.equals("TD")) { //cuando la consulta es para todos los productos
				campos = (this.codigoaplicacion.equals("PY"))?" inter.ic_error_proceso, ":campos;
				campos = (this.codigoaplicacion.equals("CN"))?" inter.ig_numero_linea_credito, ":campos;
				campos = (this.codigoaplicacion.equals("PR"))?" inter.ig_numero_linea_credito,inter.ig_numero_contrato, ":campos;
				campos = (this.codigoaplicacion.equals("DS"))?" inter.ig_numero_linea_credito, inter.ig_numero_contrato, inter.ig_numero_prestamo, ":campos;
				campos = (this.codigoaplicacion.equals("DV"))?" 'interfase_error7' AS tablaorigen, ":campos;
				condicionExtra = (this.codigoaplicacion.equals("DS"))?" AND inter.ic_error_proceso IS NOT NULL AND inter.cc_codigo_aplicacion IS NOT NULL ":condicionExtra;
				if("S".equals(chckFueraFacultades)) {
					condicionExtra= (this.codigoaplicacion.equals("DV"))?" AND inter.ig_proceso_numero IS NULL AND ep.cc_codigo_aplicacion = 'NE'  AND inter.ic_error_proceso = 14 ":condicionExtra;
					System.out.println("Entre condicion condicionExtra"+condicionExtra);
				} else { 
					System.out.println("Dentro del eles : de codigo 0"+condicionExtra);
					condicionExtra= (this.codigoaplicacion.equals("DV"))?" AND inter.ig_proceso_numero IS NULL AND ep.cc_codigo_aplicacion = 'NE'  AND inter.ic_error_proceso != 14 ":condicionExtra;
				}
			}
			if("S".equals(chckFueraFacultades)) {
				condicionExtra= (this.codigoaplicacion.equals("DV"))?" AND inter.ig_proceso_numero IS NULL AND ep.cc_codigo_aplicacion = 'NE'  AND inter.ic_error_proceso = 14 ":condicionExtra;
				System.out.println("Entre condicion condicionExtra"+condicionExtra);
			} else {
				condicionExtra= (this.codigoaplicacion.equals("DV"))?" AND inter.ig_proceso_numero IS NULL AND ep.cc_codigo_aplicacion = 'NE' AND inter.ic_error_proceso != 14 ":condicionExtra;
			}
			
			//
			this.qrysentencia += (sprodGen.equals("") || (sprodGen.equals("TD") && (!idsPedido.isEmpty() || !idsFolio.isEmpty())))?" UNION ":"";
			
			//3er. Query Rechazos - A partir de Proyectos Credito Electronico
			String condicionMonto = "";
			if(this.codigoaplicacion.equals("DV")) {
				condicionMonto = "sp.fn_importe_dscto as monto_descontar";
			} else {
				condicionMonto = "inter.fn_monto_operacion as monto_descontar";
			}
			this.qrysentencia +=
				" SELECT /*+ index(inter in_com_interfase_01_nuk) use_nl(inter, sp, ep, p, n, i)*/   '0' AS producto, i.cg_razon_social AS nombreif, "+
				"       p.cg_razon_social AS nombrepyme, n.ic_nafin_electronico, "+
				"       sp.ig_clave_sirac AS numclisirac, inter.df_hora_proceso, "+
				"       TO_CHAR (inter.ic_solic_portal) AS ic_folio, ep.cg_descripcion, "+
				"       inter.ig_codigo_agencia, inter.ig_codigo_sub_aplicacion, "+
				"       inter.ig_numero_linea_fondeo, inter.ig_aprobado_por, "+condicionMonto+", " +
				campos +
				"       '' modalidad, "+
				"       '7-15monitoreo/15monproc"+(((this.codigoaplicacion.equals("DV"))?0:comcontrol)+1)+".jsp-interfasesRechazadosBean.java'  as pantalla "+
				"  FROM  com_interfase inter,"   +
				"  com_solic_portal sp,  "   +
				"  comcat_error_procesos ep,   "   +
				"  comcat_pyme p,"   +
				"  comrel_nafin n,"   +
				"  comcat_if i"   +
				" WHERE sp.ic_solic_portal = inter.ic_solic_portal "+
				"   AND inter.ic_error_proceso = ep.ic_error_proceso "+
				"   AND inter.cc_codigo_aplicacion = ep.cc_codigo_aplicacion "+
				"   AND sp.ic_if = i.ic_if "+
				"   AND sp.ig_clave_sirac = p.in_numero_sirac(+) "+
				"   AND n.ic_epo_pyme_if(+) = p.ic_pyme "+
				"   AND n.cg_tipo(+) = 'P' "+ condicionCreditosb +
//					condicionExtra ;
				condicionExtra + this.getCondicionProducto();
				if(!this.codigoaplicacion.equals("DV"))//DIVERSOS
					this.qrysentencia +="   AND inter.ig_proceso_numero = "+comcontrol;
				this.qrysentencia +=condicion3;
				
				if( !ic_solicitudes.equals("")) {
					this.qrysentencia += " and  inter.ic_solic_portal  in ("+ic_solicitudes+")";	//F017-2015  						
				}
						
				
		}//if((sprodGen.equals("0") || sprodGen.equals("")) || !idsCredito.isEmpty())
		//
		
		/*
		if((sprodGen.equals("5") || sprodGen.equals("")) || !idsSolic.isEmpty()) {
			if(sprodGen.equals("") || sprodGen.equals("TD")) {//cuando la consulta es para todos los productos
				campos = (this.codigoaplicacion.equals("PY"))?" inter.ic_error_proceso, ":campos;
				campos = (this.codigoaplicacion.equals("CN"))?" inter.ig_numero_linea_credito, ":campos;
				campos = (this.codigoaplicacion.equals("PR"))?" inter.ig_numero_linea_credito,inter.ig_numero_contrato, ":campos;
				campos = (this.codigoaplicacion.equals("DS"))?" inter.ig_numero_linea_credito, inter.ig_numero_contrato, inter.ig_numero_prestamo, ":campos;
				campos = (this.codigoaplicacion.equals("DV"))?" 'inv_interfase_error7' AS tablaorigen, ":campos;
				condicionExtra= (this.codigoaplicacion.equals("DS"))?" AND inter.ic_error_proceso IS NOT NULL AND inter.cc_codigo_aplicacion IS NOT NULL ":condicionExtra;
				condicionExtra= (this.codigoaplicacion.equals("DV"))?" AND inter.ig_proceso_numero IS NULL AND inter.cc_codigo_aplicacion = 'NE' AND ep.cc_codigo_aplicacion = 'NE' ":condicionExtra;
			}
			//
			this.qrysentencia += (sprodGen.equals("") || (sprodGen.equals("TD") && (!idsFolio.isEmpty() || !idsPedido.isEmpty() || !idsCredito.isEmpty())))?" UNION ":"";
			
			//4to. Query Rechazos - A partir de Proyectos Credicadenas
			String condicionMontoC = "";
			if(this.codigoaplicacion.equals("DV"))
				condicionMontoC = "disp.fn_monto_credito as monto_descontar";
			else
				condicionMontoC = "inter.fn_monto_operacion as monto_descontar";
				
			this.qrysentencia +=
				" SELECT TO_CHAR (disp.ic_producto_nafin) AS producto, "+
				"       i.cg_razon_social AS nombreif, p.cg_razon_social AS nombrepyme, "+
				"       n.ic_nafin_electronico, p.in_numero_sirac AS numclisirac, "+
				"       inter.df_hora_proceso, TO_CHAR (inter.cc_disposicion) AS ic_folio, "+
				"       ep.cg_descripcion, inter.ig_codigo_agencia, "+
				"       inter.ig_codigo_sub_aplicacion, inter.ig_numero_linea_fondeo, " +
				"       inter.ig_aprobado_por, " +condicionMontoC+  ","+
				campos+
				"       '' modalidad, "+
				"       '8-15monitoreo/15monproc"+(((this.codigoaplicacion.equals("DV"))?0:comcontrol)+1)+".jsp-interfasesRechazadosBean.java ' as pantalla "+
				"  FROM comcat_pyme p, "+
				"       comrel_nafin n, "+
				"       comcat_if i, "+
				"       comcat_error_procesos ep, "+
				"       com_linea_credito lc, "+
				"       inv_interfase inter, "+
				"       inv_disposicion disp, "+
				"       inv_solicitud sol "+
				" WHERE sol.cc_disposicion = inter.cc_disposicion "+
				"   AND inter.cc_disposicion = disp.cc_disposicion "+
				"   AND inter.ic_error_proceso = ep.ic_error_proceso "+
				"   AND inter.cc_codigo_aplicacion = ep.cc_codigo_aplicacion "+
				"   AND disp.ic_linea_credito = lc.ic_linea_credito "+
				"   AND lc.ic_if = i.ic_if "+
				"   AND disp.ic_pyme = p.ic_pyme "+
				"   AND n.ic_epo_pyme_if = p.ic_pyme "+
				"   AND n.cg_tipo = 'P' "+ condicionSolicsb +
				condicionExtra +
				" UNION "+
				" SELECT TO_CHAR (disp.ic_producto_nafin) AS producto, "+
				"       i.cg_razon_social AS nombreif, p.cg_razon_social AS nombrepyme, "+
				"       n.ic_nafin_electronico, p.in_numero_sirac AS numclisirac, "+
				"       inter.df_hora_proceso, TO_CHAR (inter.cc_disposicion) AS ic_folio, "+
				"       ep.cg_descripcion, inter.ig_codigo_agencia, "+
				"       inter.ig_codigo_sub_aplicacion, inter.ig_numero_linea_fondeo, " +
				"       inter.ig_aprobado_por, " +condicionMontoC+  ","+
				campos+
				"       decode(disp.cg_exp_emp, 'S', 'Financiamiento de Exportacion', 'Libre Disposicion') modalidad, "+
				"       '8-15monitoreo/15monproc"+(((this.codigoaplicacion.equals("DV"))?0:comcontrol)+1)+".jsp-interfasesRechazadosBean.java ' as pantalla "+
				"  FROM comcat_pyme p, "+
				"       comrel_nafin n, "+
				"       comcat_if i, "+
				"       comcat_error_procesos ep, "+
				"       emp_linea_credito lc, "+
				"       inv_interfase inter, "+
				"       inv_disposicion disp, "+
				"       inv_solicitud sol "+
				" WHERE sol.cc_disposicion = inter.cc_disposicion "+
				"   AND inter.cc_disposicion = disp.cc_disposicion "+
				"   AND inter.ic_error_proceso = ep.ic_error_proceso "+
				"   AND inter.cc_codigo_aplicacion = ep.cc_codigo_aplicacion "+
				"   AND disp.ic_linea_credito_emp = lc.ic_linea_credito "+
				"   AND lc.ic_if = i.ic_if "+
				"   AND disp.ic_pyme = p.ic_pyme "+
				"   AND n.ic_epo_pyme_if = p.ic_pyme "+
				"   AND n.cg_tipo = 'P' "+ condicionSolicsb +
				condicionExtra ;
				if(!this.codigoaplicacion.equals("DV"))//DIVERSOS
					this.qrysentencia +="   AND inter.ig_proceso_numero = "+ comcontrol;
				this.qrysentencia +=condicion2;
		}//if((sprodGen.equals("5") || sprodGen.equals("")) || !idsSolic.isEmpty())
		
		*/
		//
		if((sprodGen.equals("4")||sprodGen.equals("")) || !idsDocumento.isEmpty()) {
			
			if(sprodGen.equals("") || sprodGen.equals("TD")) {//cuando la consulta es para todos los productos
				campos = (this.codigoaplicacion.equals("PY"))?" inter.ic_error_proceso, ":campos;
				campos = (this.codigoaplicacion.equals("CN"))?" inter.ig_numero_linea_credito, ":campos;
				campos = (this.codigoaplicacion.equals("PR"))?" inter.ig_numero_linea_credito,inter.ig_numero_contrato, ":campos;
				campos = (this.codigoaplicacion.equals("DS"))?" inter.ig_numero_linea_credito, inter.ig_numero_contrato, inter.ig_numero_prestamo, ":campos;
				campos = (this.codigoaplicacion.equals("DV"))?" 'dis_interfase_error7' AS tablaorigen, ":campos;
				condicionExtra= (this.codigoaplicacion.equals("DS"))?" AND inter.ic_error_proceso IS NOT NULL AND inter.cc_codigo_aplicacion IS NOT NULL ":condicionExtra;
				condicionExtra= (this.codigoaplicacion.equals("DV"))?" AND inter.ig_proceso_numero IS NULL AND inter.cc_codigo_aplicacion = 'NE' AND ep.cc_codigo_aplicacion = 'NE' ":condicionExtra;
			}//if(sprodGen.equals("") || sprodGen.equals("TD"))
			
			//5to. Query Rechazos - A partir de Proyectos Distribuidores
			this.qrysentencia += (sprodGen.equals("") || (sprodGen.equals("TD") && (!idsFolio.isEmpty() || !idsPedido.isEmpty() || !idsCredito.isEmpty() || !idsSolic.isEmpty())))?" UNION ":"";

			String condicionMontoD = "";
			if(this.codigoaplicacion.equals("DV"))
				condicionMontoD = "ds.fn_importe_recibir as monto_descontar";
			else
				condicionMontoD = "inter.fn_monto_operacion as monto_descontar";

			this.qrysentencia +=
				" SELECT TO_CHAR (doc.ic_producto_nafin) AS producto, "+
				"       i.cg_razon_social AS nombreif, p.cg_razon_social AS nombrepyme, "+
				"       n.ic_nafin_electronico, p.in_numero_sirac AS numclisirac, "+
				"       inter.df_hora_proceso, ";

			this.qrysentencia +=(this.codigoaplicacion.equals("DV"))?" TO_CHAR (doc.ic_documento) AS ic_folio, ":" TO_CHAR (inter.ic_documento) AS ic_folio, ";

			this.qrysentencia +=
				"       ep.cg_descripcion, inter.ig_codigo_agencia, "+
				"       inter.ig_codigo_sub_aplicacion, inter.ig_numero_linea_fondeo, "+
				"       inter.ig_aprobado_por, "+condicionMontoD+ "," +
				campos+
				"       '' modalidad, "+
				"       '9-15monitoreo/15monproc"+(((this.codigoaplicacion.equals("DV"))?0:comcontrol)+1)+".jsp-interfasesRechazadosBean.java' as pantalla "+
				"  FROM comcat_pyme p, "+
				"       comrel_nafin n, "+
				"       comcat_if i, "+
				"       comcat_error_procesos ep, "+
				"       dis_linea_credito_dm lc, "+
				"       dis_interfase inter, "+
				"       dis_documento doc, "+
				"       dis_solicitud sol, "+
				"       dis_docto_seleccionado ds "+
				" WHERE sol.ic_documento = inter.ic_documento "+
				"   AND inter.ic_documento = doc.ic_documento "+
				"   AND sol.ic_documento = ds.ic_documento "+
				"   AND inter.ic_documento = ds.ic_documento "+
				"   AND inter.ic_error_proceso = ep.ic_error_proceso "+
				"   AND inter.cc_codigo_aplicacion = ep.cc_codigo_aplicacion "+
				"   AND doc.ic_linea_credito_dm = lc.ic_linea_credito_dm "+
				"   AND lc.ic_if = i.ic_if "+
				"   AND doc.ic_pyme = p.ic_pyme "+
				"   AND n.ic_epo_pyme_if = p.ic_pyme "+
				"   AND n.cg_tipo = 'P' "+ condicionDocumentosb +
				condicionExtra + this.getCondicionProducto();

			if(!"".equals(this.cmbestatus)){
				condicionEstatus = 
					(sprodGen.equals("1") /*|| sprodGen.equals("2")*/)?"  AND c5.ic_error_proceso = "+this.cmbestatus+" AND c5.cc_codigo_aplicacion = '"+cmbestatusvalor2+"'  ":
					(sprodGen.equals("0") || sprodGen.equals("4") /*|| sprodGen.equals("5")*/)?"  AND inter.ic_error_proceso = "+this.cmbestatus+" AND inter.cc_codigo_aplicacion = '"+cmbestatusvalor2+"'  ":"";
	    	}
	    	
			this.qrysentencia +=condicionEstatus;

			if(!this.codigoaplicacion.equals("DV"))//DIVERSOS
				this.qrysentencia +="   AND inter.ig_proceso_numero = "+ comcontrol;

			String condicionMontoD2 = "";
				if(this.codigoaplicacion.equals("DV"))
					condicionMontoD2 = "ds.fn_importe_recibir as monto_descontar";
				else
					condicionMontoD2 = "inter.fn_monto_operacion as monto_descontar";


				if( !ic_solicitudes.equals("")) {
					this.qrysentencia += " and  doc.ic_documento  in ("+ic_solicitudes+")";	//F017-2015  						
				}
				
				//6to. Query Rechazos - A partir de Proyectos - Distribuidores Dolares?
					this.qrysentencia += condicion2 +
						" UNION "+
						" SELECT TO_CHAR (doc.ic_producto_nafin) AS producto, "+
						"       i.cg_razon_social AS nombreif, p.cg_razon_social AS nombrepyme, "+
						"       n.ic_nafin_electronico, p.in_numero_sirac AS numclisirac, "+
						"       inter.df_hora_proceso, TO_CHAR (inter.ic_documento) AS ic_folio, "+
						"       ep.cg_descripcion, inter.ig_codigo_agencia, "+
						"       inter.ig_codigo_sub_aplicacion, inter.ig_numero_linea_fondeo, "+
						"       inter.ig_aprobado_por, "+condicionMontoD2+ "," +
						campos+
						"       '' modalidad, "+
						"       '10-15monitoreo/15monproc"+(((this.codigoaplicacion.equals("DV"))?0:comcontrol)+1)+".jsp-interfasesRechazadosBean.java'  as pantalla  "+
						"  FROM comcat_pyme p, "+
						"       comrel_nafin n, "+
						"       comcat_if i, "+
						"       comcat_error_procesos ep, "+
						"       com_linea_credito lc, "+
						"       dis_interfase inter, "+
						"       dis_documento doc, "+
						"       dis_solicitud sol, "+
						"       dis_docto_seleccionado ds "+
						" WHERE sol.ic_documento = inter.ic_documento "+
						"   AND inter.ic_documento = doc.ic_documento "+
						"   AND sol.ic_documento = ds.ic_documento "+
						"   AND inter.ic_documento = ds.ic_documento "+
						"   AND inter.ic_error_proceso = ep.ic_error_proceso "+
						"   AND inter.cc_codigo_aplicacion = ep.cc_codigo_aplicacion "+
						"   AND doc.ic_linea_credito = lc.ic_linea_credito "+
						"   AND lc.ic_if = i.ic_if "+
						"   AND doc.ic_pyme = p.ic_pyme "+
						"   AND n.ic_epo_pyme_if = p.ic_pyme "+
						"   AND n.cg_tipo = 'P' "+ condicionDocumentosb +
						condicionExtra;
			if(!this.codigoaplicacion.equals("DV"))//DIVERSOS
				this.qrysentencia +="   AND inter.ig_proceso_numero = " + comcontrol;
			this.qrysentencia += condicion2;
			
			if( !ic_solicitudes.equals("")) {
				this.qrysentencia += " and  inter.ic_documento  in ("+ic_solicitudes+")";	//F017-2015    						
			}
				
		}//if((sprodGen.equals("4")||sprodGen.equals("")) || !idsDocumento.isEmpty())


		if(!(this.codigoaplicacion.equals("DV") && ( sprodGen.equals("1") /*|| sprodGen.equals("2") */ ))) {
			if(!"".equals(this.cmbestatus)) {
				if("S".equals(chckFueraFacultades)) { 
					condicionEstatus = 
						(sprodGen.equals("1") /* || sprodGen.equals("2") */ )?"  AND c5.ic_error_proceso = 14 AND c5.cc_codigo_aplicacion = '"+cmbestatusvalor2+"'  ":
						(sprodGen.equals("0") || sprodGen.equals("4") /*|| sprodGen.equals("5")*/ )?"  AND inter.ic_error_proceso = 14 AND inter.cc_codigo_aplicacion = '"+cmbestatusvalor2+"'  ":"";
				} else { 
					condicionEstatus = 
						(sprodGen.equals("1") /*|| sprodGen.equals("2")*/ )?"  AND c5.ic_error_proceso = "+this.cmbestatus+" AND c5.cc_codigo_aplicacion = '"+cmbestatusvalor2+"'  ":
						(sprodGen.equals("0") || sprodGen.equals("4") /*|| sprodGen.equals("5")*/ )?"  AND inter.ic_error_proceso = "+this.cmbestatus+" AND inter.cc_codigo_aplicacion = '"+cmbestatusvalor2+"'  ":"";
				}
			}
		}
		
		if(!nombrejsp.equals("15MONPROC1A") && !nombrejsp.equals("15MONPROC2A") && !nombrejsp.equals("15MONPROC3A") && !nombrejsp.equals("15MONPROC4A") && !nombrejsp.equals("15MONPROC5A")){
			this.qrysentencia += condicionEstatus + ((divCod==true)?this.getCondicionProducto():"");
			this.qrysentencia += " order by producto, nombreIf, monto_descontar desc, df_hora_proceso, nombrePyme ";
		} else {
			this.qrysentencia += " order by producto, nombreIf, df_hora_proceso, nombrePyme";
		}
		
		System.out.println("this.qrysentencia ::::::.>>>>> "+this.qrysentencia); 
		/*
		if ("7".equals(this.cmbestatus) && "S".equals(chckFueraFacultades)){
			this.qrysentencia = "";      
		}*/
	}//setQrysentencia

	public String getIcproducto(){
		return this.icproducto;
	}
	public String getChckFueraFacultades(){
		return this.chckFueraFacultades;
	}
	public String getCodigoaplicacion(){
		return this.codigoaplicacion;
	}
	public String getCmbestatus(){
		return this.cmbestatus;
	}
	public String getIcif(){
		return this.icif=icif;
	}
	public String getIcnafinelectronico(){
		return this.icnafinelectronico=icnafinelectronico;
	}
	public String getQrysentencia(){
		return this.qrysentencia;
	}
	private String getCondicionProducto(){
		return getCondicionProducto("");
	}
	
	private String getCondicionProducto(String sTabla) {
		String condicionProducto = "";
		if (this.icproducto.equals("1") /*|| this.icproducto.equals("2")*/ ) {
			if ("".equals(sTabla)) {
				condicionProducto = " AND c5.cc_producto like '" + this.icproducto + "%' ";
			} else {
				condicionProducto = " AND "+sTabla+".cc_producto like '" + this.icproducto + "%' ";
			}
		}
		if (this.icproducto.equals("4") /*|| this.icproducto.equals("5")*/ )
			condicionProducto = " AND cc_producto like '" + this.icproducto + "%' ";
		if (this.icproducto.equals("1A") || this.icproducto.equals("1B") || this.icproducto.equals("1C") || this.icproducto.equals("1D")  /*||
			this.icproducto.equals("2A") || this.icproducto.equals("2B") */ ) {
			if ("".equals(sTabla)) {
				condicionProducto = " AND c5.cc_producto = '" + this.icproducto + "' ";
			} else {
				condicionProducto = " AND "+sTabla+".cc_producto = '" + this.icproducto + "' ";
			}
		}
		if (this.icproducto.equals("4A") || this.icproducto.equals("4B") ||
			//this.icproducto.equals("5A") || this.icproducto.equals("5B") ||
			this.icproducto.equals("0F") || this.icproducto.equals("0C") || this.icproducto.equals("0E") )
			condicionProducto = " AND cc_producto = '" + this.icproducto + "' ";

		return condicionProducto;
	}//getCondicionProducto

	public Vector getIds() {
		Vector ids = new Vector();
		ids.addElement(idsFolio);
		ids.addElement(idsPedido);
		ids.addElement(idsSolic);
		ids.addElement(idsCredito);
		ids.addElement(idsDocumento);
		
		ids.addElement(foliosIds);
		ids.addElement(pedidosIds);
		ids.addElement(solicsIds);
		ids.addElement(creditosIds);
		ids.addElement(documentosIds);
		ids.addElement(strFoliosConDescripcionD);
		ids.addElement(strFoliosSinDescripcionD);
		ids.addElement(strFoliosConDescripcionA);
		ids.addElement(strFoliosSinDescripcionA);
		ids.addElement(strFoliosConDescripcionC);
		ids.addElement(strFoliosSinDescripcionC);
		ids.addElement(strFoliosConDescripcionDis);
		ids.addElement(strFoliosSinDescripcionDis);
		ids.addElement(strFoliosConDescripcionI);
		ids.addElement(strFoliosSinDescripcionI);
		ids.addElement(String.valueOf(numFolios));
		
		return ids;
	}

	public void setIds(HttpServletRequest request) {
		int numRegistros= Integer.parseInt(request.getParameter("numRegistros"));
		numFolios = 0;
		for(int i=1; i<=numRegistros; i++) {
			String folio = request.getParameter("folio"+i);
			String producto = request.getParameter("producto"+i);
			String strError = (request.getParameter("errorProceso"+i)==null)?"":request.getParameter("errorProceso"+i);
System.out.println("\n folio: "+folio);
System.out.println("\n producto: "+producto);
System.out.println("\n strError: "+strError);
			if (folio != null && producto!=null) {
				switch(Integer.parseInt(producto)) {
					case 1:
						idsFolio.addElement(folio);
						if(foliosb.equals("")) {
							foliosb = "?";
							foliosIds = "'" + folio + "'";
						} else {
							foliosb += ",?";
							foliosIds += ",'" + folio +"'";
						}
						if (strError.equals("43")) {
							if (strFoliosConDescripcionD.equals(""))
								strFoliosConDescripcionD = "'" + folio + "'";
							else
								strFoliosConDescripcionD += ",'" + folio +"'";
						}
						else {
							if (strFoliosSinDescripcionD.equals(""))
								strFoliosSinDescripcionD = "'" + folio + "'";
							else
								strFoliosSinDescripcionD += ",'" + folio +"'";
						}
						numFolios++;
					break;
					
					case 2:
						idsPedido.addElement(folio);
						if(pedidosb.equals("")) {
							pedidosb = "?";
							pedidosIds = "'" + folio + "'";
						} else {
							pedidosb += ",?";
							pedidosIds += ",'" + folio + "'";
						}
						if (strError.equals("43")) {
							if (strFoliosConDescripcionA.equals(""))
								strFoliosConDescripcionA = "'" + folio + "'";
							else
								strFoliosConDescripcionA += ",'" + folio +"'";
						} else {
							if (strFoliosSinDescripcionA.equals(""))
								strFoliosSinDescripcionA = "'" + folio + "'";
							else
								strFoliosSinDescripcionA += ",'" + folio +"'";
						}
						numFolios++;
System.out.println("\n idsPedido: "+idsPedido);
					break;
					
					case 0:
						idsCredito.addElement(folio);
						if(creditosb.equals("")) {
							creditosb = "?";
							creditosIds = "'" + folio + "'";
						} else {
							creditosb += ",?";
							creditosIds += ",'" + folio + "'";
						}
						if (strError.equals("43")) {
							if (strFoliosConDescripcionC.equals(""))
								strFoliosConDescripcionC = "'" + folio + "'";
							else
								strFoliosConDescripcionC += ",'" + folio +"'";
						} else {
							if (strFoliosSinDescripcionC.equals(""))
								strFoliosSinDescripcionC = "'" + folio + "'";
							else
								strFoliosSinDescripcionC += ",'" + folio +"'";
						}
						numFolios++;
					break;
					
					case 4:
						idsDocumento.addElement(folio);
						if(documentosb.equals("")) {
							documentosb = "?";
							documentosIds = "'" + folio + "'";
						} else {
							documentosb += ",?";
							documentosIds += ",'" + folio + "'";
						}
						if (strError.equals("43")) {
							if (strFoliosConDescripcionDis.equals(""))
								strFoliosConDescripcionDis = "'" + folio + "'";
							else
								strFoliosConDescripcionDis += ",'" + folio +"'";
						} else {
							if (strFoliosSinDescripcionDis.equals(""))
								strFoliosSinDescripcionDis = "'" + folio + "'";
							else
								strFoliosSinDescripcionDis += ",'" + folio +"'";
						}
						numFolios++;
					break;
					
					case 5:
						idsSolic.addElement(folio);
						if(solicsb.equals("")) {
							solicsb = "?";
							solicsIds = "'" + folio + "'";
						} else {
							solicsb += ",?";
							solicsIds += ",'" + folio + "'";
						}
						if (strError.equals("43")) {
							if (strFoliosConDescripcionI.equals(""))
								strFoliosConDescripcionI = "'" + folio + "'";
							else
								strFoliosConDescripcionI += ",'" + folio +"'";
						} else {
							if (strFoliosSinDescripcionI.equals(""))
								strFoliosSinDescripcionI = "'" + folio + "'";
							else
								strFoliosSinDescripcionI += ",'" + folio +"'";
						}
						numFolios++;
					break;
				}//switch(Integer.parseInt(producto))
				
			}//if (folio != null && producto!=null)
		}//for(int i=1; i<=numRegistros; i++)
	}//setIds
	

private final static Log log = ServiceLocator.getInstance().getLog(interfasesRechazadosBean.class);



	public String impDescargaArchivo(  HttpServletRequest request, List registros, String path , String tipo ) {
		
		log.debug("impDescargaArchivo (E)");
				
		String nombreArchivo = "";
		
		try {
		
			if(tipo.equals("PDF")) {
			
				CreaArchivo creaArchivo = new CreaArchivo();
				HttpSession session = request.getSession();
				ComunesPDF pdfDoc = new ComunesPDF();
				
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
					pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
					String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
					String diaActual    = fechaActual.substring(0,2);
					String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
					String anioActual   = fechaActual.substring(6,10);
					String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
						((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
						(String)session.getAttribute("sesExterno"),
						(String) session.getAttribute("strNombre"),
						(String) session.getAttribute("strNombreUsuario"),
						(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
						
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		
				if(tipoProceso.equals("Diversos") ||  tipoProceso.equals("Contratos") )  {   
					pdfDoc.setTable(14, 100);
				}else if(tipoProceso.equals("Proyectos")  )  { 
					pdfDoc.setTable(13, 100);
				}else if(tipoProceso.equals("Disposiciones") || tipoProceso.equals("Prestamos")   )  {   
			pdfDoc.setTable(17, 100);
		}	
		pdfDoc.setCell("Producto","celda01",ComunesPDF.CENTER);
				if(tipoProceso.equals("Diversos") || tipoProceso.equals("Disposiciones") || tipoProceso.equals("Prestamos") )  { 
			pdfDoc.setCell("Modalidad ","celda01",ComunesPDF.CENTER);
		}
		pdfDoc.setCell("Intermediario ","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("PYME ","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Num. PYME","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Monto a descontar","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Num. de PYME Sirac ","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Hora ","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Solicitud ","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Motivo ","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Agencia ","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Subaplicaci�n ","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("L�nea fondeo ","celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Aprobador por ","celda01",ComunesPDF.CENTER);
				if(tipoProceso.equals("Contratos") || tipoProceso.equals("Disposiciones")  || tipoProceso.equals("Prestamos")) {
			pdfDoc.setCell("N�mero Proyecto","celda01",ComunesPDF.CENTER);
		}
				if(tipoProceso.equals("Disposiciones")   || tipoProceso.equals("Prestamos")  )  { 
					pdfDoc.setCell("N�mero Contrato","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero Prestamo","celda01",ComunesPDF.CENTER);
				}
				
				int total= registros.size();
				
				for (int i = 0; i <registros.size(); i++) {
		
					HashMap datos = (HashMap)registros.get(i);
					 String producto= datos.get("PRODUCTO").toString();
					 String modalidad= datos.get("MODALIDAD").toString();
					 String intermediario= datos.get("INTERMEDIARIO").toString();
					 String nombre_pyme= datos.get("NOMBRE_PYME").toString();
					 String num_pyme= datos.get("NUM_PYME").toString();
					 String monto_desc= datos.get("MONTO_DESC").toString();
					 String num_sirac= datos.get("NUM_PYME_SIRAC").toString();
					 String hora= datos.get("HORA").toString();
					 String solicitud= datos.get("SOLICITUD").toString();
					 String motivo= datos.get("MOTIVO").toString();
					 String agencia= datos.get("AGENCIA").toString();
					 String subaplicacion= datos.get("SUBAPLICACION").toString();
					 String linea_fondeo= datos.get("LINEA_FONDEO").toString();
					 String aprobador= datos.get("APROBADOR").toString();
					 String igNumeroLineaCredito = datos.get("NUM_PROYECTO").toString();
					 String igNumeroContrato = datos.get("NUM_CONTRATO").toString();
					 String igNumeroPrestamo = datos.get("NUM_PRESTAMO").toString();
					
					 
					 pdfDoc.setCell(producto,"formas",ComunesPDF.CENTER);
					 if(tipoProceso.equals("Diversos") || tipoProceso.equals("Disposiciones") || tipoProceso.equals("Prestamos") )  { 
				pdfDoc.setCell(modalidad,"formas",ComunesPDF.CENTER);
			 }
			 pdfDoc.setCell(intermediario,"formas",ComunesPDF.LEFT);
			 pdfDoc.setCell(nombre_pyme,"formas",ComunesPDF.LEFT);
			 pdfDoc.setCell(num_pyme,"formas",ComunesPDF.CENTER);
			 pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_desc,2),"formas",ComunesPDF.RIGHT);
			pdfDoc.setCell(num_sirac,"formas",ComunesPDF.CENTER);
			 pdfDoc.setCell(hora,"formas",ComunesPDF.CENTER);
			 pdfDoc.setCell(solicitud,"formas",ComunesPDF.CENTER);
			 pdfDoc.setCell(motivo,"formas",ComunesPDF.LEFT);
			 pdfDoc.setCell(agencia,"formas",ComunesPDF.CENTER);
			 pdfDoc.setCell(subaplicacion,"formas",ComunesPDF.CENTER);
			 pdfDoc.setCell(linea_fondeo,"formas",ComunesPDF.CENTER);
			 pdfDoc.setCell(aprobador,"formas",ComunesPDF.CENTER);
					 if(tipoProceso.equals("Contratos") || tipoProceso.equals("Disposiciones") || tipoProceso.equals("Prestamos") ) {
				 pdfDoc.setCell(igNumeroLineaCredito,"formas",ComunesPDF.CENTER);				 
			 }
					 if(tipoProceso.equals("Disposiciones") || tipoProceso.equals("Prestamos") )  { 
				pdfDoc.setCell(igNumeroContrato,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(igNumeroPrestamo,"formas",ComunesPDF.CENTER);
			 }
			 
		}
		
		pdfDoc.setCell("Total Rechazadas","celda01",ComunesPDF.RIGHT,2);
		
		if(tipoProceso.equals("Diversos")  ||  tipoProceso.equals("Contratos") )  { 
			pdfDoc.setCell(String.valueOf(total),"celda01",ComunesPDF.LEFT, 12);
		}else if(tipoProceso.equals("Proyectos"))  { 
			pdfDoc.setCell(String.valueOf(total),"celda01",ComunesPDF.LEFT, 11);
				}else  if(tipoProceso.equals("Disposiciones")  || tipoProceso.equals("Prestamos") )  {  
					pdfDoc.setCell(String.valueOf(total),"celda01",ComunesPDF.LEFT, 15);
				}		
				
			
			pdfDoc.addTable();
			pdfDoc.endDocument();
				
			}else if( tipo.equals("XLSX") ) {
			
				ComunesXLSX				documentoXLSX 	= null;
				Writer					writer			= null;
		
				// Estilos en la plantilla xlsx
				HashMap 					estilos			= new HashMap();
				estilos.put("PERCENT",						"1");
				estilos.put("COEFF",							"2");
				estilos.put("CURRENCY",						"3");
				estilos.put("DATE",							"4");
				estilos.put("HEADER",						"5");
				estilos.put("CSTRING",						"6");
				estilos.put("TITLE",							"7");
				estilos.put("SUBTITLE",						"8");
				estilos.put("RIGHTCSTRING",				"9");
				estilos.put("CENTERCSTRING",				"10");
				estilos.put("REDCURRENCY",					"11");
				estilos.put("CENTERCSTRING_ESMERALDA",	"12");
				estilos.put("BOLD_CSTRING",				"13");
				estilos.put("BOLD_CENTERCSTRING",		"14");
				estilos.put("BOLD_CURRENCY",				"15");
		
		
				// Crear archivo XLSX
				documentoXLSX 	= new ComunesXLSX( path, estilos );
		
				// Crear archivo
				writer 			= documentoXLSX.creaHoja();
				documentoXLSX.setLongitudesColumna(new String[]{"63.42578125","25.7109375","35.140625","23.7109375","15.7109375","23.7109375","23.7109375","23.7109375","22.7109375","23.7109375"});
					
				short	indiceRenglon 	= 0;
				short	indiceCelda		= 0;
		
				// Definir el Encabezado PROGRAMA
				documentoXLSX.agregaRenglon(indiceRenglon++);
				indiceCelda		= 0;
									
				documentoXLSX.agregaCelda(indiceCelda++, (String)" Producto","HEADER" );
				if(tipoProceso.equals("Diversos") || tipoProceso.equals("Disposiciones") || tipoProceso.equals("Prestamos") )  { 
					documentoXLSX.agregaCelda(indiceCelda++, (String)" Modalidad ","HEADER" );
				}
				documentoXLSX.agregaCelda(indiceCelda++, (String)" Intermediario ","HEADER" );
				documentoXLSX.agregaCelda(indiceCelda++, (String)" PYME ","HEADER" );
				documentoXLSX.agregaCelda(indiceCelda++, (String)"Num. PYME","HEADER" );
				documentoXLSX.agregaCelda(indiceCelda++, (String)"Monto a descontar","HEADER" );
				documentoXLSX.agregaCelda(indiceCelda++, (String)"Num. de PYME Sirac ","HEADER" );
				documentoXLSX.agregaCelda(indiceCelda++, (String)"Hora ","HEADER" );
				documentoXLSX.agregaCelda(indiceCelda++, (String)"Solicitud ","HEADER" );
				documentoXLSX.agregaCelda(indiceCelda++, (String)"Motivo ","HEADER" );
				documentoXLSX.agregaCelda(indiceCelda++, (String)"Agencia ","HEADER" );
				documentoXLSX.agregaCelda(indiceCelda++, (String)"Subaplicaci�n ","HEADER" );
				documentoXLSX.agregaCelda(indiceCelda++, (String)"L�nea fondeo ","HEADER" );
				documentoXLSX.agregaCelda(indiceCelda++, (String)"Aprobador por ","HEADER" );
				if(tipoProceso.equals("Contratos") || tipoProceso.equals("Disposiciones")  || tipoProceso.equals("Prestamos")) {
					documentoXLSX.agregaCelda(indiceCelda++, (String)"N�mero Proyecto","HEADER" );
				}
				if(tipoProceso.equals("Disposiciones")   || tipoProceso.equals("Prestamos")  )  { 
					documentoXLSX.agregaCelda(indiceCelda++, (String)"N�mero Contrato","HEADER" );
					documentoXLSX.agregaCelda(indiceCelda++, (String)"N�mero Prestamo","HEADER" );
				}
				documentoXLSX.finalizaRenglon();
				
				int total= registros.size();			 
				
				for (int i = 0; i <registros.size(); i++) {
		
					HashMap datos = (HashMap)registros.get(i);
					 String producto= datos.get("PRODUCTO").toString();
					 String modalidad= datos.get("MODALIDAD").toString();
					 String intermediario= datos.get("INTERMEDIARIO").toString();
					 String nombre_pyme= datos.get("NOMBRE_PYME").toString();
					 String num_pyme= datos.get("NUM_PYME").toString();
					 String monto_desc= datos.get("MONTO_DESC").toString();
					 String num_sirac= datos.get("NUM_PYME_SIRAC").toString();
					 String hora= datos.get("HORA").toString();
					 String solicitud= datos.get("SOLICITUD").toString();
					 String motivo= datos.get("MOTIVO").toString();
					 String agencia= datos.get("AGENCIA").toString();
					 String subaplicacion= datos.get("SUBAPLICACION").toString();
					 String linea_fondeo= datos.get("LINEA_FONDEO").toString();
					 String aprobador= datos.get("APROBADOR").toString();
					 String igNumeroLineaCredito = datos.get("NUM_PROYECTO").toString();
					 String igNumeroContrato = datos.get("NUM_CONTRATO").toString();
					 String igNumeroPrestamo = datos.get("NUM_PRESTAMO").toString();
					
					documentoXLSX.agregaRenglon(indiceRenglon++);
					indiceCelda					= 0;					  
					documentoXLSX.agregaCelda(indiceCelda++,producto,"CSTRING" );
					 if(tipoProceso.equals("Diversos") || tipoProceso.equals("Disposiciones") || tipoProceso.equals("Prestamos") )  { 
						documentoXLSX.agregaCelda(indiceCelda++,modalidad,"CSTRING" );
					 }
					 documentoXLSX.agregaCelda(indiceCelda++,intermediario,"CSTRING" );
					 documentoXLSX.agregaCelda(indiceCelda++,nombre_pyme,"CSTRING" );
					 documentoXLSX.agregaCelda(indiceCelda++,num_pyme,"CSTRING" );
					 documentoXLSX.agregaCelda(indiceCelda++,"$"+Comunes.formatoDecimal(monto_desc,2),"CSTRING" );
					documentoXLSX.agregaCelda(indiceCelda++,num_sirac,"CSTRING" );
					 documentoXLSX.agregaCelda(indiceCelda++,hora,"CSTRING" );
					 documentoXLSX.agregaCelda(indiceCelda++,solicitud,"CSTRING" );
					 documentoXLSX.agregaCelda(indiceCelda++,motivo,"CSTRING" );
					 documentoXLSX.agregaCelda(indiceCelda++,agencia,"CSTRING" );
					 documentoXLSX.agregaCelda(indiceCelda++,subaplicacion,"CSTRING" );
					 documentoXLSX.agregaCelda(indiceCelda++,linea_fondeo,"CSTRING" );
					 documentoXLSX.agregaCelda(indiceCelda++,aprobador,"CSTRING" );
					 if(tipoProceso.equals("Contratos") || tipoProceso.equals("Disposiciones") || tipoProceso.equals("Prestamos") ) {
						documentoXLSX.agregaCelda(indiceCelda++,igNumeroLineaCredito,"CSTRING" );			 
					 }
					 if(tipoProceso.equals("Disposiciones") || tipoProceso.equals("Prestamos") )  { 
						documentoXLSX.agregaCelda(indiceCelda++,igNumeroContrato,"CSTRING" );
						documentoXLSX.agregaCelda(indiceCelda++,igNumeroPrestamo,"CSTRING" );
					 }
					documentoXLSX.finalizaRenglon(); 
				
				}//for
				
				documentoXLSX.finalizaHoja();   

				nombreArchivo = documentoXLSX.finalizaDocumento( directorioPlantilla, "Rechazados_"+tipoProceso); 

	 
			}
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("impDescargaArchivo (S)");
		}
		return nombreArchivo;
				
	}
	
	/**
	 * Metodo para los registros de Procesos- Diversos 
	 * @return 
	 * @param operacion
	 * @param descripcion
	 * @param tabla_Origen
	 * @param producto
	 * @param noSolicitud
	 * @param noRegistros
	 */
	public String  procRegDiversos(int noRegistros,   String [] noSolicitud ,  String [] producto , String [] tabla_Origen , String descripcion, String operacion  ){
		
		log.info("procRegDiversos (E)");
	
		AccesoDB con =new AccesoDB();
		PreparedStatement ps	= null;
		ResultSet			rs	= null;	
		PreparedStatement ps2	= null;
		ResultSet			rs1	= null;	
		StringBuffer qrySentencia = new StringBuffer("");		
		String   mensaje ="";
		int numRegistrosAfectar =0;
		boolean errorConcurrencia=true;
		
		try{	
			con.conexionDB();
			
			for (int i=0;i<noRegistros;i++)  {
				
				String solicitud = noSolicitud[i];
				String tablaOrigen = tabla_Origen[i];
				String prod = producto[i];
				
				log.debug("solicitud  "+solicitud);
				log.debug("tablaOrigen  "+tablaOrigen);
				log.debug("prod  "+prod);
				
			
			if(prod.equals("1")) {
					qrySentencia = new StringBuffer("");	
					qrySentencia.append(
						" SELECT COUNT ( * ) "   +
						"     FROM com_control01 "   +
						"     WHERE ic_folio = ? "   +
						"     AND cg_estatus = 'N' "   +
						" UNION "   +
						" SELECT COUNT ( * ) "   +
						"     FROM com_control05 "   +
						"     WHERE ic_folio = ? ") ;
						
				}/* else if(prod.equals("2")) {
					qrySentencia = new StringBuffer("");	
					qrySentencia.append(
						" SELECT COUNT( * ) "   +
						"     FROM com_controlant01 "   +
						"     WHERE ic_pedido = ? "   +
						"     AND cg_estatus = 'N' "   +
						" UNION "   +
						" SELECT COUNT( * ) "   +
						"     FROM com_controlant05 "   +
						"     WHERE ic_pedido = ? ")  ;
				}
				*/
				if(prod.equals("1") /*|| prod.equals("2") */) {
					ps2 = con.queryPrecompilado(qrySentencia.toString());
					ps2.setString(1,solicitud);
					ps2.setString(2,solicitud);
					rs1 = ps2.executeQuery();
					numRegistrosAfectar=0;
					while (rs1.next()) {
						numRegistrosAfectar = numRegistrosAfectar + rs1.getInt(1);
					}
					rs1.close();
					if(ps2 != null) ps2.close();
					
				} else	if (prod.equals("0") || prod.equals("4") /*|| prod.equals("5")*/ )   { // No se requiere esta validacion para Credito Electronico
					  	numRegistrosAfectar = 1;
				}
				if (numRegistrosAfectar==1)  {	//Control de concurrencia (Debe existir el registro en la tabla para poder ser reprocesado o cancelado)
					errorConcurrencia=false;
					if (operacion.equals("REPROCESAR")) {
					// Descuento Electronico.
						if (tablaOrigen.equals("control01")) {
							qrySentencia = new StringBuffer("");	
							qrySentencia.append( "update com_control01 set cg_estatus='S'"+
														" , cg_descripcion='"+descripcion+"'"+
														" where ic_folio ='"+solicitud+"'");
							con.ejecutaSQL(qrySentencia.toString());
						
						}else if (tablaOrigen.equals("control05")) {
							qrySentencia = new StringBuffer("");	
							qrySentencia.append(" update com_solicitud set ic_estatus_solic=1, ic_bloqueo = null"+
													  " where ic_folio ='"+solicitud+"'");
							con.ejecutaSQL(qrySentencia.toString());

							qrySentencia = new StringBuffer("");	
							qrySentencia.append("delete from com_control05"+
								" where ic_folio ='"+solicitud+"'");							
							con.ejecutaSQL(qrySentencia.toString());
						}//tabla control05
						
						// Anticipo Electronico.
						else if (tablaOrigen.equals("controlant01")) {
							qrySentencia = new StringBuffer("");	
							qrySentencia.append( "update com_controlant01 set cg_estatus='S' "+
											" , cg_descripcion='"+descripcion+"'"+
											" where ic_pedido ='"+solicitud+"'");
							//out.println(qrySentencia+"<br>");
							con.ejecutaSQL(qrySentencia.toString());
						}//tabla controlant01
						else if (tablaOrigen.equals("controlant05")) {
							
							qrySentencia = new StringBuffer("");	
							qrySentencia.append("update com_anticipo set ic_estatus_antic=1, ic_bloqueo = null"+
											" where ic_pedido ='"+solicitud+"'");							
							con.ejecutaSQL(qrySentencia.toString());
							
							qrySentencia = new StringBuffer("");	
							qrySentencia.append("delete from com_controlant05"+
											" where ic_pedido ='"+solicitud+"'");							
							con.ejecutaSQL(qrySentencia.toString());
						}//tabla controlant05
						// Credito Electronico que requiere aprobacion de comite
						else if (tablaOrigen.equals("interfase_error7")) {
							qrySentencia = new StringBuffer("");	
							qrySentencia.append(" update com_interfase set "+
											" cg_descripcion = '"+descripcion+"',"+
											" cc_codigo_aplicacion = null, "+
											" DF_DIVERSOS = SYSDATE, "+
											" ic_error_proceso = null "+
											" where ic_solic_portal = "+solicitud);
							con.ejecutaSQL(qrySentencia.toString());
						}
						// Credito Electronico General
						else if (tablaOrigen.equals("interfase")) 	{	
						
							qrySentencia = new StringBuffer("");	
							qrySentencia.append(" delete from com_interfase "+
										   " where ic_solic_portal = "+solicitud);
							con.ejecutaSQL(qrySentencia.toString());
							
							qrySentencia = new StringBuffer("");	
							qrySentencia.append(" update com_solic_portal "+
											" set ic_estatus_solic=1, ic_bloqueo=null "+
											" where ic_solic_portal = "+solicitud);
							con.ejecutaSQL(qrySentencia.toString());
							
								System.out.println("Paso  ");
						}
						// Credicadenas que requiere aprobacion de comite
						else if (tablaOrigen.equals("inv_interfase_error7")) {
						
							qrySentencia = new StringBuffer("");	
							qrySentencia.append(" update inv_interfase set "+
											" cg_descripcion = '"+descripcion+"',"+
											" cc_codigo_aplicacion = null, "+
											" DF_DIVERSOS = SYSDATE, "+
											" ic_error_proceso = null "+
											" where cc_disposicion = "+solicitud);
							con.ejecutaSQL(qrySentencia.toString());
						}
						// Credicadenas General
						else if (tablaOrigen.equals("inv_interfase"))  {
							
							qrySentencia = new StringBuffer("");	
							qrySentencia.append(" delete from inv_interfase "+
										   " where cc_disposicion = "+solicitud);
							con.ejecutaSQL(qrySentencia.toString());
							
							qrySentencia = new StringBuffer("");	
							qrySentencia.append(" update inv_solicitud "+
											" set ic_estatus_solic=1, ic_bloqueo=null "+
											" where cc_disposicion = "+solicitud);
							con.ejecutaSQL(qrySentencia.toString());
						}
						// Financiamiento a Distribuidores que requiere aprobacion de comite
						else if (tablaOrigen.equals("dis_interfase_error7"))  {
							
							qrySentencia = new StringBuffer("");	
							qrySentencia.append(" update dis_interfase set "+
											" cg_descripcion = '"+descripcion+"',"+
											" cc_codigo_aplicacion = null, "+
											" DF_DIVERSOS = SYSDATE, "+
											" ic_error_proceso = null "+
											" where ic_documento = "+solicitud);
							con.ejecutaSQL(qrySentencia.toString());
						}
						// Financiamiento a Distribuidores General
						else if (tablaOrigen.equals("dis_interfase"))  {
							
							qrySentencia = new StringBuffer("");	
							qrySentencia.append(" delete from dis_interfase "+
										   " where ic_documento = "+solicitud);
							con.ejecutaSQL(qrySentencia.toString());
							
							qrySentencia = new StringBuffer("");	
							qrySentencia.append(" update dis_solicitud "+
											" set ic_estatus_solic=1, ic_bloqueo=null "+
											" where ic_documento = "+solicitud);
							con.ejecutaSQL(qrySentencia.toString());
						}

					}	else if (operacion.equals("CANCELAR"))		{
					
						if(prod.equals("1")) {
							if("S".equals(chckFueraFacultades)){
							
							qrySentencia = new StringBuffer("");	
							qrySentencia.append("delete from com_control05"+
												" where ic_folio = '"+solicitud+"'");								
							con.ejecutaSQL(qrySentencia.toString());
							
							qrySentencia = new StringBuffer("");	
							qrySentencia.append("update com_solicitud"+
												" set ic_estatus_solic=4, ic_bloqueo=1, df_operacion = sysdate "+
												" where ic_folio = '"+solicitud+"'");								
							con.ejecutaSQL(qrySentencia.toString());
							}else{
								
								qrySentencia = new StringBuffer("");	
								qrySentencia.append("update com_solicitud"+
												" set ic_estatus_solic=1, ic_bloqueo=1"+
												" where ic_folio = '"+solicitud+"'");								
								con.ejecutaSQL(qrySentencia.toString());
								
								qrySentencia = new StringBuffer("");	
								qrySentencia.append("delete from com_control05"+
												" where ic_folio = '"+solicitud+"'");								
								con.ejecutaSQL(qrySentencia.toString());
								
								qrySentencia = new StringBuffer("");	
								qrySentencia.append("delete from com_control01"+
												" where ic_folio = '"+solicitud+"'");
								con.ejecutaSQL(qrySentencia.toString());
							}
						}
						/*else	if(prod.equals("2")) {
						
							qrySentencia = new StringBuffer("");	
							qrySentencia.append("update com_anticipo"+
											" set ic_estatus_antic=1, ic_bloqueo=1"+
											" where ic_pedido = '"+solicitud+"'");											
							con.ejecutaSQL(qrySentencia.toString());
							
							qrySentencia = new StringBuffer("");	
							qrySentencia.append("delete from com_controlant05"+
											" where ic_pedido = '"+solicitud+"'");							
							con.ejecutaSQL(qrySentencia.toString());
							
							qrySentencia = new StringBuffer("");	
							qrySentencia.append("delete from com_controlant01"+
											" where ic_pedido = '"+solicitud+"'");							
							con.ejecutaSQL(qrySentencia.toString());
							
						}*/ else if (prod.equals("0")){
								
								qrySentencia = new StringBuffer("");	
								qrySentencia.append( "delete from com_interfase "+
											"where ic_solic_portal= "+solicitud);
							con.ejecutaSQL(qrySentencia.toString());
							
							qrySentencia = new StringBuffer("");	
							qrySentencia.append( "update com_solic_portal "+
											"set ic_estatus_solic=4, ic_bloqueo=1, df_operacion = sysdate "+
											"where ic_solic_portal= "+solicitud);
							con.ejecutaSQL(qrySentencia.toString());
							
						} else if (prod.equals("4")){
							
							qrySentencia = new StringBuffer("");	
							qrySentencia.append("delete from dis_interfase "+
											"where ic_documento= "+solicitud);
							con.ejecutaSQL(qrySentencia.toString());
							
							qrySentencia = new StringBuffer("");	
							qrySentencia.append("update dis_solicitud "+
											"set ic_estatus_solic=1, ic_bloqueo=1 "+
											"where ic_documento= "+solicitud);
							con.ejecutaSQL(qrySentencia.toString());
							
						}/* else if (prod.equals("5"))	{
						
							qrySentencia = new StringBuffer("");	
							qrySentencia.append("delete from inv_interfase "+
											"where cc_disposicion= "+solicitud );
							con.ejecutaSQL(qrySentencia.toString());
							
							qrySentencia = new StringBuffer("");	
							qrySentencia.append("update inv_solicitud "+
											"set ic_estatus_solic=1, ic_bloqueo=1 "+
											"where cc_disposicion= "+solicitud);
							con.ejecutaSQL(qrySentencia.toString());
						}
						*/
						
					}	else if (operacion.equals("CANCEL_FONDEO"))		{
					
						if(prod.equals("1")) {						
							qrySentencia = new StringBuffer("");	
							qrySentencia.append("update com_solicitud"+
												" set ic_estatus_solic=10, ic_bloqueo=1"+
												" where ic_folio = '"+solicitud+"'");
								
							con.ejecutaSQL(qrySentencia.toString());
							
							qrySentencia = new StringBuffer("");	
							qrySentencia.append("delete from com_control05"+
													" where ic_folio = '"+solicitud+"'");
							con.ejecutaSQL(qrySentencia.toString());
								
							qrySentencia = new StringBuffer("");	
							qrySentencia.append("delete from com_control01"+
													" where ic_folio = '"+solicitud+"'");								
							con.ejecutaSQL(qrySentencia.toString());

						}

					}//fin de operacion=CANCEL_FONDEO
				}	//fin de validacion de concurrencia
					else	 {//Problema de concurrencia
						errorConcurrencia=true;
						break;	//sale de while
				}
			}	//for
			
			
			if (errorConcurrencia) 	{
				con.terminaTransaccion(false);
				mensaje ="<span style='color:red;'>Existen inconsistencias en la informaci�n, debido a la concurrencia de usuarios. LOS CAMBIOS NO TOMARON EFECTO. Intente nuevamente el proceso.</span>";
					
			}else 	{ //fin de mensaje de error de concurrencia
				con.terminaTransaccion(true); 
				if (operacion.equals("REPROCESAR"))	{
					mensaje ="Los registros mostrados fueron REPROCESADOS";
					
				}else if (operacion.equals("CANCELAR"))	{ 
					mensaje ="registro mostrado fue CANCELADO";				
					
				} else if (operacion.equals("CANCEL_FONDEO"))	{
					mensaje="	Las operaciones siguientes cambiaron de estatus a Operada con Fondeo Propio";
				}
			}//fin de else --- mensaje de operacion realizada
			
					
		} catch(Exception e) {
				
			e.printStackTrace();
			log.error("error al procRegDiversos " +e); 
		} finally {	
			if(con.hayConexionAbierta()) {				
				con.cierraConexionDB();	
			}
		}  
		log.info("procRegDiversos (S)");  
		return mensaje;
	}	   

	/**
		 * 
		 * @return 
		 * @param operacion
		 * @param errorPro
		 * @param productos
		 * @param folios
		 * @param numRegistros
		 */
	public String  procRegREINICIAR_INTERFAZ(int numRegistros,   String [] folios ,  String [] productos, String [] errorPro, String operacion, String tipoProceso ){
	      
	      log.info("procRegREINICIAR_INTERFAZ (E)");
	   
	      AccesoDB con =new AccesoDB();
	      PreparedStatement ps = null;
	      ResultSet         rs = null;  
	      PreparedStatement ps2   = null;
	      ResultSet         rs1   = null;  
	      StringBuffer qrySentencia = new StringBuffer("");     
	      int numRegistrosAfectar =0;
	      int nFolios = 0;
	       
	      String mensaje ="";
	      String foliosId ="",  pedidosId="", creditosId="", solicsId="", documentosId ="", descripcion="", 
	      strFoliosConDescripcionD_ ="",  strFoliosSinDescripcionD_ ="", strFoliosConDescripcionA_ ="", 
	      strFoliosSinDescripcionA_ ="", strFoliosConDescripcionC_="", strFoliosSinDescripcionC_="",
	      strFoliosConDescripcionI_="", strFoliosSinDescripcionI_="", strFoliosConDescripcionDis_="",
	      strFoliosSinDescripcionDis_ ="";
	    
	    log.debug("************************** ");
	    log.debug("operacion: "+operacion);
	     log.debug("tipoProceso: "+tipoProceso);
	    log.debug("numRegistros: "+numRegistros); 
	    log.debug("************************** ");
	    
	    
	      try{  
	         con.conexionDB();
	         
	      for(int i=0; i<numRegistros; i++) {
	         String folio =   folios[i];
	         String producto = productos[i];
	         String strError = errorPro[i];
	         log.debug("folio:---- "+folio);
	         log.debug(" producto:--- "+producto);        
	         log.debug("strError:  --- "+strError);
	         
	         if (folio != null && producto!=null) {
	            switch(Integer.parseInt(producto)) {
	               case 1:              
	                  if(foliosId.equals("")) {                    
	                     foliosId = "'" + folio + "'";
	                  } else {                
	                     foliosId += ",'" + folio +"'";
	                  }
	                  if (strError.equals("43")) {
	                     if (strFoliosConDescripcionD_.equals(""))
	                        strFoliosConDescripcionD_ = "'" + folio + "'";
	                     else
	                        strFoliosConDescripcionD_ += ",'" + folio +"'";
	                  }
	                  else {
	                     if (strFoliosSinDescripcionD_.equals(""))
	                        strFoliosSinDescripcionD_ = "'" + folio + "'";
	                     else
	                        strFoliosSinDescripcionD_ += ",'" + folio +"'";
	                  }
	                  nFolios++;
	               break;
	               
	               case 2:                 
	                  if(pedidosId.equals("")) {                   
	                     pedidosId = "'" + folio + "'";
	                  } else {                   
	                     pedidosId += ",'" + folio + "'";
	                  }
	                  if (strError.equals("43")) {
	                     if (strFoliosConDescripcionA_.equals(""))
	                        strFoliosConDescripcionA_ = "'" + folio + "'";
	                     else
	                        strFoliosConDescripcionA_ += ",'" + folio +"'";
	                  } else {
	                     if (strFoliosSinDescripcionA_.equals(""))
	                        strFoliosSinDescripcionA_ = "'" + folio + "'";
	                     else
	                        strFoliosSinDescripcionA_ += ",'" + folio +"'";
	                  }
	                  nFolios++;        
	               break;               
	               case 0:
	                  
	                  if(creditosId.equals("")) {                     
	                     creditosId = "'" + folio + "'";
	                  } else {                   
	                     creditosId += ",'" + folio + "'";
	                  }
	                  if (strError.equals("43")) {
	                     if (strFoliosConDescripcionC_.equals(""))
	                        strFoliosConDescripcionC_ = "'" + folio + "'";
	                     else
	                        strFoliosConDescripcionC_ += ",'" + folio +"'";
	                  } else {
	                     if (strFoliosSinDescripcionC_.equals(""))
	                        strFoliosSinDescripcionC_ = "'" + folio + "'";
	                     else
	                        strFoliosSinDescripcionC_ += ",'" + folio +"'";
	                  }
	                  nFolios++;
	               break;
	               
	               case 4:
	                  
	                  if(documentosId.equals("")) {                   
	                     documentosId = "'" + folio + "'";
	                  } else {                   
	                     documentosId += ",'" + folio + "'";
	                  }
	                  if (strError.equals("43")) {
	                     if (strFoliosConDescripcionDis_.equals(""))
	                        strFoliosConDescripcionDis_ = "'" + folio + "'";
	                     else
	                        strFoliosConDescripcionDis_ += ",'" + folio +"'";
	                  } else {
	                     if (strFoliosSinDescripcionDis_.equals(""))
	                        strFoliosSinDescripcionDis_ = "'" + folio + "'";
	                     else
	                        strFoliosSinDescripcionDis_ += ",'" + folio +"'";
	                  }
	                  nFolios++;
	               break;
	               
	               case 5:
	               
	                  if(solicsId.equals("")) {                    
	                     solicsId = "'" + folio + "'";
	                  } else {                
	                     solicsId += ",'" + folio + "'";
	                  }
	                  if (strError.equals("43")) {
	                     if (strFoliosConDescripcionI_.equals(""))
	                        strFoliosConDescripcionI_ = "'" + folio + "'";
	                     else
	                        strFoliosConDescripcionI_ += ",'" + folio +"'";
	                  } else {
	                     if (strFoliosSinDescripcionI_.equals(""))
	                        strFoliosSinDescripcionI_ = "'" + folio + "'";
	                     else
	                        strFoliosSinDescripcionI_ += ",'" + folio +"'";
	                  }
	                  nFolios++;
	               break;
	            }//switch(Integer.parseInt(producto))
	            
	         }//if (folio != null && producto!=null)
	      }//for(int i=1; i<=numRegistros; i++)
	            
	      log.debug("foliosId:  --- "+foliosId);
	      log.debug("pedidosId:  --- "+pedidosId);
	      log.debug("creditosId:  --- "+creditosId);
	      log.debug("documentosId:  --- "+documentosId);
	      
	      if(!foliosId.equals("")) {
	            qrySentencia = new StringBuffer("");   
	            qrySentencia.append("select count(*) from com_control05"+
	                        " where ic_folio in ("+foliosId+")");
	         }
	         qrySentencia.append((!foliosId.equals("") && !pedidosId.equals(""))?" UNION ALL ":"");
	         
	         if(!pedidosId.equals("")) {
	            qrySentencia = new StringBuffer("");   
	            qrySentencia.append( "select count(*) from com_controlant01"+
	                        " where ic_pedido in ("+pedidosId+")");
	         }
	         qrySentencia.append((!creditosId.equals("") && (!foliosId.equals("") || !pedidosId.equals("")))?" UNION ALL ":"");
	         
	         if (!creditosId.equals(""))   {  
	            qrySentencia = new StringBuffer("");   
	            qrySentencia.append(" select count(*) from com_interfase "+
	                        " where ic_solic_portal in ("+creditosId+") "+
	                        "   and ic_error_proceso is not null ");
	                        //"   and ig_proceso_numero = 1 "); //ya no es necesario por eso se quito del query
	         }
	         qrySentencia.append((!solicsId.equals("") && (!foliosId.equals("") || !pedidosId.equals("") || !creditosId.equals("")))?" UNION ALL ":"");
	         
	         if (!solicsId.equals("")) {
	            qrySentencia = new StringBuffer("");   
	            qrySentencia.append(" select count(*) from inv_interfase "+
	                        " where cc_disposicion in ("+solicsId+") "+
	                        "   and ic_error_proceso is not null ");
	                        //"   and ig_proceso_numero = 1 ");//ya no es necesario por eso se quito del query
	         }
	         qrySentencia.append((!documentosId.equals("") && (!foliosId.equals("") || !pedidosId.equals("") || !creditosId.equals("") || !solicsId.equals("")))?" UNION ALL ":"");
	         
	         if (!documentosId.equals("")) {
	            qrySentencia = new StringBuffer("");   
	            qrySentencia.append( " select count(*) from dis_interfase "+
	                        " where ic_documento in ("+documentosId+") "+
	                        "   and ic_error_proceso is not null ");
	                     // "   and ig_proceso_numero = 1 ");//ya no es necesario por eso se quito del query
	         }
	         
	         log.debug("qrySentencia   "+qrySentencia);
	         
	         rs1 = con.queryDB(qrySentencia.toString());
	         while (rs1.next()) {
	            numRegistrosAfectar += rs1.getInt(1);
	         }
	         
	         log.debug("numRegistrosAfectar   "+numRegistrosAfectar +"    nFolios   "+nFolios);
	         
	         String  tablaDescuento ="com_control01";
	         if(tipoProceso.equals("Contratos")) {  tablaDescuento ="com_control02";  }
	         else  if( tipoProceso.equals("Disposiciones") )  { tablaDescuento ="com_control04";  }
	         else if ( tipoProceso.equals("Prestamos") ||  tipoProceso.equals("Pr�stamos"))  { tablaDescuento ="com_control03";     }
	         else  if( tipoProceso.equals("Diversos") )  { tablaDescuento ="com_control05";  }
	                  
	         if (numRegistrosAfectar==nFolios) { //Control de concurrencia
	            if (operacion.equals("REINICIAR INTERFAZ")) {

	               if(!foliosId.equals("")) { // Descuento Electronico.
	                  qrySentencia = new StringBuffer("");
	                  qrySentencia.append("update com_solicitud"+
	                              " set ic_estatus_solic=1, ic_bloqueo=null"+
	                              " where ic_folio in ("+foliosId+")");                 
	                  con.ejecutaSQL(qrySentencia.toString());
	                  
	                  qrySentencia = new StringBuffer("");
	                  qrySentencia.append("delete from com_control05"+
	                              " where ic_folio in ("+foliosId+")");                 
	                  con.ejecutaSQL(qrySentencia.toString());
	                  
	                  qrySentencia = new StringBuffer("");
	                  qrySentencia.append("delete from "+tablaDescuento+
	                              " where ic_folio in ("+foliosId+")");                 
	                  con.ejecutaSQL(qrySentencia.toString());
	               }
	               if(!pedidosId.equals("")) { // Anticipo Electronico
	                  
	                  qrySentencia = new StringBuffer("");
	                  qrySentencia.append("update com_anticipo"+
	                              " set ic_estatus_antic=1, ic_bloqueo=null"+
	                              " where ic_pedido in ("+pedidosId+")");                  
	                  con.ejecutaSQL(qrySentencia.toString());
	                  
	                  qrySentencia = new StringBuffer("");
	                  qrySentencia.append("delete from com_controlant05"+
	                              " where ic_pedido in ("+pedidosId+")");                  
	                  con.ejecutaSQL(qrySentencia.toString());
	                  
	                  qrySentencia = new StringBuffer("");
	                  qrySentencia.append( "delete from com_controlant01"+
	                              " where ic_pedido in ("+pedidosId+")");                  
	                  con.ejecutaSQL(qrySentencia.toString());
	               }
	               if(!creditosId.equals("")) { // Credito Electronico.
	                  
	                  qrySentencia = new StringBuffer("");
	                  qrySentencia.append("delete from com_interfase"+
	                              "   where ic_solic_portal in ("+creditosId+")");                  
	                  con.ejecutaSQL(qrySentencia.toString());
	                  
	                  qrySentencia = new StringBuffer("");
	                  qrySentencia.append("update com_solic_portal"+
	                              " set ic_estatus_solic=1, ic_bloqueo=null"+
	                              " where ic_solic_portal in ("+creditosId+")");                 
	                  con.ejecutaSQL(qrySentencia.toString());
	               }
	               if(!solicsId.equals("")) { // Credicadenas.
	                  
	                  qrySentencia = new StringBuffer("");
	                  qrySentencia.append( "delete from inv_interfase"+
	                              "   where cc_disposicion in ("+solicsId+")");                  
	                  con.ejecutaSQL(qrySentencia.toString());
	                  
	                  qrySentencia = new StringBuffer("");
	                  qrySentencia.append("update inv_solicitud "+
	                              " set ic_estatus_solic=1, ic_bloqueo=null"+
	                              " where cc_disposicion in ("+solicsId+")");                 
	                  con.ejecutaSQL(qrySentencia.toString());
	               }
	               if(!documentosId.equals("")) { // Financiamiento a Distribuidores.
	                  
	                  qrySentencia = new StringBuffer("");
	                  qrySentencia.append("delete from dis_interfase"+
	                              "   where ic_documento in ("+documentosId+")");                
	                  con.ejecutaSQL(qrySentencia.toString());
	                  
	                  qrySentencia = new StringBuffer("");
	                  qrySentencia.append("update dis_solicitud "+
	                              " set ic_estatus_solic=1, ic_bloqueo=null"+
	                              " where ic_documento in ("+documentosId+")");                  
	                  con.ejecutaSQL(qrySentencia.toString());
	               }
	               mensaje= "El o los registro(s) mostrado(s) fueron REINICIADO(S).";
	            
	            }  
	            
	            con.terminaTransaccion(true);
	            
	         }else  {//Problema de concurrencia
	            con.terminaTransaccion(false);
	            mensaje= "<span style='color:red;'> Existen inconsistencias en la informaci�n, debido a la "+
	                     "concurrencia de usuarios. LOS CAMBIOS NO TOMARON EFECTO. "+
	                     "Intente nuevamente el proceso. </span>";
	         }
	               
	      } catch(Exception e) {        
	         e.printStackTrace();       
	         log.error("error al procRegREINICIAR_INTERFAZ  " +e);          
	      } finally { 
	         if(con.hayConexionAbierta()) {            
	            con.cierraConexionDB(); 
	         }
	      }  
	      log.info("procRegREINICIAR_INTERFAZ (S)");  
	      return mensaje;
	   }
	/**
	 * Ver Archivo(Log)
	 * @return 
	 * @param strDirectorioTemp
	 * @param logAdicional
	 * @param interfase
	 * @param claveSolicitud
	 * @param producto
	 */
	public String  generarArchivoLog(String producto, String  claveSolicitud , String interfase, String logAdicional  , String strDirectorioTemp  ){
		
		log.info("generarArchivoLog (E)");
	
		AccesoDB con =new AccesoDB();
		PreparedStatement ps	= null;
		ResultSet			rs	= null;	
				
		String nombreArchivo = "", contenidoArchivo ="";
		CreaArchivo creaArchivo = new CreaArchivo();
		
		try{	
			con.conexionDB();
				
			String query = "", srtSelect = "SELECT ", strFrom = " FROM ", strWhere = " WHERE ";
			
			if ("todos".equals(interfase)) {
				srtSelect += "cg_proyectos_log, cg_contratos_log, cg_prestamos_log, cg_disposiciones_log ";
			} else {
				if ("".equals(logAdicional)) {
					srtSelect += "cg_"+ interfase +"_log";
				} else {
					srtSelect += "cg_"+ logAdicional +"_log";
				}
			}
			if ("0".equals(producto)){
				strFrom += "com_interfase ";
				strWhere += "ic_solic_portal = ?";
			} else if("1".equals(producto)) {
				strFrom += ("todos".equals(interfase) || "Disposiciones".equals(interfase))? "com_control04 " : ("Proyectos".equals(interfase))?"com_control01 ":("Contratos".equals(interfase))?"com_control02 ": "com_control03 ";
				strWhere += "ic_folio = ?";
			}
			/*else if("2".equals(producto)) {
				strFrom += ("todos".equals(interfase) || "Disposiciones".equals(interfase))? "com_controlant04 " : ("Proyectos".equals(interfase))?"com_controlant01 ":("Contratos".equals(interfase))?"com_controlant02 ": "com_controlant03 ";
				strWhere += "ic_pedido = ?";
			} */
			else if("4".equals(producto)) {
				strFrom += "dis_interfase ";
				strWhere += "ic_documento = ?";
			} 
			/*
			 else if("5".equals(producto)) {
				strFrom += "inv_interfase ";
				strWhere += "cc_disposicion = ?";
			} else if("8".equals(producto)) {
				strFrom += "com_control04 ";
				strWhere += "ic_folio = ?";
			}
			*/
			query = srtSelect + strFrom + strWhere;
				
			ps = con.queryPrecompilado(query);
			ps.setString(1, claveSolicitud);
			rs = ps.executeQuery();
			if (rs.next()) {	
				if ("T".equals(interfase)) {
					contenidoArchivo += (rs.getString("cg_proyectos_log")==null)?"****NO EXISTE LOG DE PROYECTOS****\n":"****PROYECTOS****\n" + rs.getString("cg_proyectos_log") + "\n";
					contenidoArchivo += (rs.getString("cg_contratos_log")==null)?"****NO EXISTE LOG DE CONTRATOS****\n":"****CONTRATOS****\n" + rs.getString("cg_contratos_log") + "\n";
					contenidoArchivo += (rs.getString("cg_prestamos_log")==null)?"****NO EXISTE LOG DE PRESTAMOS****\n":"****PRESTAMOS****\n" + rs.getString("cg_prestamos_log") + "\n";
					contenidoArchivo += (rs.getString("cg_disposiciones_log")==null)?"****NO EXISTE LOG DE DISPOSICIONES****\n":"****DISPOSICIONES****\n" + rs.getString("cg_disposiciones_log");
		
				} else {
					if ("".equals(logAdicional)) {
						contenidoArchivo += (rs.getString("cg_"+ interfase +"_log")==null)?"****NO EXISTE LOG DE " + interfase.toUpperCase() + "****\n":rs.getString("cg_"+ interfase +"_log");
					} else {
						contenidoArchivo += (rs.getString("cg_"+ logAdicional +"_log")==null)?"****NO EXISTE LOG DE " + logAdicional.toUpperCase() + "****\n":rs.getString("cg_"+ logAdicional +"_log");
					}
				}
			}
			
			creaArchivo.make(contenidoArchivo, strDirectorioTemp, ".txt");
			nombreArchivo = creaArchivo.getNombre();
	
		
	
		} catch(Exception e) {
				
			e.printStackTrace();
			log.error("error al Procesar registros  " +e); 
		} finally {	
			if(con.hayConexionAbierta()) {				
				con.cierraConexionDB();	
			}
		}  
		log.info("generarArchivoLog (S)");  
		return nombreArchivo;
	}	   
	
	/**
	 * Metodo para los registros de Procesos- Proyectos 
	 * @return 
	 * @param errorPro
	 * @param productos
	 * @param folios
	 * @param numRegistros
	 * @param operacion
	 */
		public String  procRegProyectos(int numRegistros,   String [] folios ,  String [] productos, String [] errorPro, String operacion){
		
		log.info("procRegProyectos (E)");
	
		AccesoDB con =new AccesoDB();
		PreparedStatement ps	= null;
		ResultSet			rs	= null;	
		PreparedStatement ps2	= null;
		ResultSet			rs1	= null;	
		StringBuffer qrySentencia = new StringBuffer("");		
		int numRegistrosAfectar =0;
		int nFolios = 0;
		 
		String mensaje ="";
		String foliosId ="",  pedidosId="", creditosId="", solicsId="", documentosId ="", descripcion="", 
		strFoliosConDescripcionD_ ="",  strFoliosSinDescripcionD_ ="", strFoliosConDescripcionA_ ="", 
	   strFoliosSinDescripcionA_ ="", strFoliosConDescripcionC_="", strFoliosSinDescripcionC_="",
		strFoliosConDescripcionI_="", strFoliosSinDescripcionI_="", strFoliosConDescripcionDis_="",
		strFoliosSinDescripcionDis_ ="";
	 
	 log.debug("************************** ");
	 log.debug("operacion: "+operacion);
	 log.debug("numRegistros: "+numRegistros);
	 log.debug("************************** ");
	 
	 
		try{	
			con.conexionDB();
			
		for(int i=0; i<numRegistros; i++) {
			String folio =   folios[i];
			String producto = productos[i];
			String strError = errorPro[i];
			log.debug("folio:---- "+folio);
			log.debug(" producto:--- "+producto);			
			log.debug("strError:  --- "+strError);
			
			if (folio != null && producto!=null) {
				switch(Integer.parseInt(producto)) {
					case 1:					
						if(foliosId.equals("")) {							
							foliosId = "'" + folio + "'";
						} else {						
							foliosId += ",'" + folio +"'";
						}
						if (strError.equals("43")) {
							if (strFoliosConDescripcionD_.equals(""))
								strFoliosConDescripcionD_ = "'" + folio + "'";
							else
								strFoliosConDescripcionD_ += ",'" + folio +"'";
						}
						else {
							if (strFoliosSinDescripcionD_.equals(""))
								strFoliosSinDescripcionD_ = "'" + folio + "'";
							else
								strFoliosSinDescripcionD_ += ",'" + folio +"'";
						}
						nFolios++;
					break;
					
					case 2:						
						if(pedidosId.equals("")) {							
							pedidosId = "'" + folio + "'";
						} else {							
							pedidosId += ",'" + folio + "'";
						}
						if (strError.equals("43")) {
							if (strFoliosConDescripcionA_.equals(""))
								strFoliosConDescripcionA_ = "'" + folio + "'";
							else
								strFoliosConDescripcionA_ += ",'" + folio +"'";
						} else {
							if (strFoliosSinDescripcionA_.equals(""))
								strFoliosSinDescripcionA_ = "'" + folio + "'";
							else
								strFoliosSinDescripcionA_ += ",'" + folio +"'";
						}
						nFolios++;			
					break;					
					case 0:
						
						if(creditosId.equals("")) {							
							creditosId = "'" + folio + "'";
						} else {							
							creditosId += ",'" + folio + "'";
						}
						if (strError.equals("43")) {
							if (strFoliosConDescripcionC_.equals(""))
								strFoliosConDescripcionC_ = "'" + folio + "'";
							else
								strFoliosConDescripcionC_ += ",'" + folio +"'";
						} else {
							if (strFoliosSinDescripcionC_.equals(""))
								strFoliosSinDescripcionC_ = "'" + folio + "'";
							else
								strFoliosSinDescripcionC_ += ",'" + folio +"'";
						}
						nFolios++;
					break;
					
					case 4:
						
						if(documentosId.equals("")) {							
							documentosId = "'" + folio + "'";
						} else {							
							documentosId += ",'" + folio + "'";
						}
						if (strError.equals("43")) {
							if (strFoliosConDescripcionDis_.equals(""))
								strFoliosConDescripcionDis_ = "'" + folio + "'";
							else
								strFoliosConDescripcionDis_ += ",'" + folio +"'";
						} else {
							if (strFoliosSinDescripcionDis_.equals(""))
								strFoliosSinDescripcionDis_ = "'" + folio + "'";
							else
								strFoliosSinDescripcionDis_ += ",'" + folio +"'";
						}
						nFolios++;
					break;
					
					case 5:
					
						if(solicsId.equals("")) {							
							solicsId = "'" + folio + "'";
						} else {						
							solicsId += ",'" + folio + "'";
						}
						if (strError.equals("43")) {
							if (strFoliosConDescripcionI_.equals(""))
								strFoliosConDescripcionI_ = "'" + folio + "'";
							else
								strFoliosConDescripcionI_ += ",'" + folio +"'";
						} else {
							if (strFoliosSinDescripcionI_.equals(""))
								strFoliosSinDescripcionI_ = "'" + folio + "'";
							else
								strFoliosSinDescripcionI_ += ",'" + folio +"'";
						}
						nFolios++;
					break;
				}//switch(Integer.parseInt(producto))
				
			}//if (folio != null && producto!=null)
		}//for(int i=1; i<=numRegistros; i++)
				
		log.debug("foliosId:  --- "+foliosId);
		log.debug("pedidosId:  --- "+pedidosId);
		log.debug("creditosId:  --- "+creditosId);
		log.debug("documentosId:  --- "+documentosId);
				
		
		if(!foliosId.equals("")) {
				qrySentencia = new StringBuffer("");	
				qrySentencia.append("select count(*) from com_control01"+
								" where ic_folio in ("+foliosId+")");
			}
			qrySentencia.append((!foliosId.equals("") && !pedidosId.equals(""))?" UNION ALL ":"");
			
			if(!pedidosId.equals("")) {
				qrySentencia = new StringBuffer("");	
				qrySentencia.append( "select count(*) from com_controlant01"+
								" where ic_pedido in ("+pedidosId+")");
			}
			qrySentencia.append((!creditosId.equals("") && (!foliosId.equals("") || !pedidosId.equals("")))?" UNION ALL ":"");
			
			if (!creditosId.equals(""))	{	
				qrySentencia = new StringBuffer("");	
				qrySentencia.append(" select count(*) from com_interfase "+
								" where ic_solic_portal in ("+creditosId+") "+
								"   and ic_error_proceso is not null "+
								"   and ig_proceso_numero = 1 ");
			}
			qrySentencia.append((!solicsId.equals("") && (!foliosId.equals("") || !pedidosId.equals("") || !creditosId.equals("")))?" UNION ALL ":"");
			
			if (!solicsId.equals("")) {
				qrySentencia = new StringBuffer("");	
				qrySentencia.append(" select count(*) from inv_interfase "+
								" where cc_disposicion in ("+solicsId+") "+
								"   and ic_error_proceso is not null "+
								"   and ig_proceso_numero = 1 ");
			}
			qrySentencia.append((!documentosId.equals("") && (!foliosId.equals("") || !pedidosId.equals("") || !creditosId.equals("") || !solicsId.equals("")))?" UNION ALL ":"");
			
			if (!documentosId.equals("")) {
				qrySentencia = new StringBuffer("");	
				qrySentencia.append( " select count(*) from dis_interfase "+
								" where ic_documento in ("+documentosId+") "+
								"   and ic_error_proceso is not null "+
								"   and ig_proceso_numero = 1 ");
			}
				
			rs1 = con.queryDB(qrySentencia.toString());
			while (rs1.next()) {
				numRegistrosAfectar += rs1.getInt(1);
			}
			if (numRegistrosAfectar==nFolios) {	//Control de concurrencia
			
				if (operacion.equals("REPROCESAR"))	{
					//condicion cuasa 43.
					String condicion = (!descripcion.equals(""))?" ,cg_descripcion='"+descripcion+"'":"";

					if(!foliosId.equals("")) { // Descuento Electronico.
						// Actualiza los registos con el error 43.
						if (!strFoliosConDescripcionD_.equals("")) {
							qrySentencia = new StringBuffer("");	
							qrySentencia.append("Update com_control01 " +
										   "   Set cg_estatus='S'" + condicion +
										   " Where ic_folio in (" + strFoliosConDescripcionD_ + ")");
							con.ejecutaSQL(qrySentencia.toString());							
						}

						if (!strFoliosSinDescripcionD_.equals("")) {
							// Actualiza los registos que son de otro error.
							qrySentencia = new StringBuffer("");	
							qrySentencia.append("Update com_control01 " +
										   "   Set cg_estatus='S'" +
										   " Where ic_folio in (" + strFoliosSinDescripcionD_ + ")");							
							con.ejecutaSQL(qrySentencia.toString());
						}
						qrySentencia = new StringBuffer("");	
						qrySentencia.append( "Delete " +
									   "  From com_control05 " +
									   " Where ic_folio in (" + foliosId + ")");						
						con.ejecutaSQL(qrySentencia.toString());
					}
					if(!pedidosId.equals("")) { // Anticipo Electronico
						if (!strFoliosConDescripcionA_.equals("")) {
							// Actualiza los registos con el error 43.
							qrySentencia = new StringBuffer("");	
							qrySentencia.append("Update com_controlant01 " +
										   "   Set cg_estatus='S'" + condicion +
										   " Where ic_pedido in (" + strFoliosConDescripcionA_ + ")");							
							con.ejecutaSQL(qrySentencia.toString());
						}

						if (!strFoliosSinDescripcionA_.equals("")) {
							// Actualiza los registos que son de otro error.
							qrySentencia = new StringBuffer("");	
							qrySentencia.append("Update com_controlant01 " +
										   "   Set cg_estatus='S'" +
										   " Where ic_pedido in (" + strFoliosSinDescripcionA_ + ")");							
							con.ejecutaSQL(qrySentencia.toString());
						}
						qrySentencia = new StringBuffer("");	
						qrySentencia.append("Delete " +
									   "  From com_controlant05 " +
									   " Where ic_pedido in (" + pedidosId + ")");						
						con.ejecutaSQL(qrySentencia.toString());
					}
					if(!creditosId.equals("")) { // Reprocesar Credito Electronico.
						// Actualiza los registos con el error 43.
						if (!strFoliosConDescripcionC_.equals("")) {
							qrySentencia = new StringBuffer("");	
							qrySentencia.append("Update com_interfase  " +
										   "   Set cc_codigo_aplicacion = null, "+
										   "       ic_error_proceso = null " + condicion +
										   " Where ic_solic_portal in (" + strFoliosConDescripcionC_ + ")");							
							con.ejecutaSQL(qrySentencia.toString());
						}

						if (!strFoliosSinDescripcionC_.equals("")) {
							// Actualiza los registos que son de otro error.
							qrySentencia = new StringBuffer("");	
							qrySentencia.append( "update com_interfase "+
											"set ic_error_proceso=null, cc_codigo_aplicacion=null "+
											"where ic_solic_portal in ("+strFoliosSinDescripcionC_+")");							
							con.ejecutaSQL(qrySentencia.toString());
						}
					}
					if(!solicsId.equals("")) { // Reprocesar Credicadenas.
						// Actualiza los registos con el error 43.
						if (!strFoliosConDescripcionI_.equals("")) {
							qrySentencia = new StringBuffer("");	
							qrySentencia.append("Update inv_interfase  " +
										   "   Set cc_codigo_aplicacion = null, "+
										   "       ic_error_proceso = null " + condicion +
										   " Where cc_disposicion in (" + strFoliosConDescripcionI_ + ")");							
							con.ejecutaSQL(qrySentencia.toString());
						}

						if (!strFoliosSinDescripcionI_.equals("")) {
							// Actualiza los registos que son de otro error.
							qrySentencia = new StringBuffer("");	
							qrySentencia.append("update inv_interfase "+
											"set ic_error_proceso=null, cc_codigo_aplicacion=null "+
											"where cc_disposicion in ("+strFoliosSinDescripcionI_+")");							
							con.ejecutaSQL(qrySentencia.toString());
						}
					}
					if(!documentosId.equals("")) { // Reprocesar Financiamiento a Distribuidores
						// Actualiza los registos con el error 43.
						if (!strFoliosConDescripcionDis_.equals("")) {
							qrySentencia = new StringBuffer("");	
							qrySentencia.append("Update dis_interfase  " +
										   "   Set cc_codigo_aplicacion = null, "+
										   "       ic_error_proceso = null " + condicion +
										   " Where ic_documento in (" + strFoliosConDescripcionDis_ + ")");							
							con.ejecutaSQL(qrySentencia.toString());
						}

						if (!strFoliosSinDescripcionDis_.equals("")) {
							// Actualiza los registos que son de otro error.
							qrySentencia = new StringBuffer("");	
							qrySentencia.append("update dis_interfase "+
											"set ic_error_proceso=null, cc_codigo_aplicacion=null "+
											"where ic_documento in ("+strFoliosSinDescripcionDis_+")");							
							con.ejecutaSQL(qrySentencia.toString());
						}
					}
					
					mensaje = "El o los registro(s) mostrado(s) fueron REPROCESADO(S)";
					
				} else if (operacion.equals("CANCELAR")) {
					if(!foliosId.equals("")) { // Descuento Electronico.
						
						qrySentencia = new StringBuffer("");
						qrySentencia.append("update com_solicitud"+
										" set ic_estatus_solic=1, ic_bloqueo=1"+
										" where ic_folio in ("+foliosId+")");
						con.ejecutaSQL(qrySentencia.toString());
						
						qrySentencia = new StringBuffer("");
						qrySentencia.append( "delete from com_control05"+
										" where ic_folio in ("+foliosId+")");						
						con.ejecutaSQL(qrySentencia.toString());
						
						qrySentencia = new StringBuffer("");
						qrySentencia.append("delete from com_control01"+
										" where ic_folio in ("+foliosId+")");						
						con.ejecutaSQL(qrySentencia.toString());
					}
					if(!pedidosId.equals("")) { // Anticipo Electronico
						
						qrySentencia = new StringBuffer("");
						qrySentencia.append( "update com_anticipo"+
										" set ic_estatus_antic=1, ic_bloqueo=1"+
										" where ic_pedido in ("+pedidosId+")");						
						con.ejecutaSQL(qrySentencia.toString());
						
						qrySentencia = new StringBuffer("");
						qrySentencia.append("delete from com_controlant05"+
										" where ic_pedido in ("+pedidosId+")");						
						con.ejecutaSQL(qrySentencia.toString());
						
						qrySentencia = new StringBuffer("");qrySentencia.append("delete from com_controlant01"+
										" where ic_pedido in ("+pedidosId+")");						
						con.ejecutaSQL(qrySentencia.toString());
					}
					if(!creditosId.equals("")) { // Credito Electronico.
						
						qrySentencia = new StringBuffer("");
						qrySentencia.append("delete from com_interfase "+
										"where ic_solic_portal in ("+creditosId+")");						
						con.ejecutaSQL(qrySentencia.toString());
						
						qrySentencia = new StringBuffer("");
						qrySentencia.append("update com_solic_portal "+
										"set ic_estatus_solic=4, ic_bloqueo=1, df_operacion = sysdate "+
										"where ic_solic_portal in ("+creditosId+")");						
						con.ejecutaSQL(qrySentencia.toString());
					}
					if(!solicsId.equals("")) { // Credicadenas.
						
						qrySentencia = new StringBuffer("");
						qrySentencia.append("delete from inv_interfase "+
										"where cc_disposicion in ("+solicsId+")");
						con.ejecutaSQL(qrySentencia.toString());
						
						qrySentencia = new StringBuffer("");
						qrySentencia.append("update inv_solicitud "+
										"set ic_estatus_solic=1, ic_bloqueo=1 "+
										"where cc_disposicion in ("+solicsId+")");
						con.ejecutaSQL(qrySentencia.toString());
					}
					if(!documentosId.equals("")) { // Financiamiento a Distribuidores.
						
						qrySentencia = new StringBuffer("");
						qrySentencia.append("delete from dis_interfase "+
										"where ic_documento in ("+documentosId+")");
						con.ejecutaSQL(qrySentencia.toString());
						
						qrySentencia = new StringBuffer("");
						qrySentencia.append("update dis_solicitud "+
										"set ic_estatus_solic=1, ic_bloqueo=1 "+
										"where ic_documento in ("+documentosId+")");
						con.ejecutaSQL(qrySentencia.toString());
					}
					mensaje="El o los registro(s) mostrado(s) fueron CANCELADO(S)";				
				
				}	else if (operacion.equals("CANCEL_FONDEO")) {
				
					if(!foliosId.equals("")) { // Descuento Electronico.
							
							qrySentencia = new StringBuffer("");
							qrySentencia.append("update com_solicitud"+
											" set ic_estatus_solic=10, ic_bloqueo=1"+
											" where ic_folio in ("+foliosId+")");
							con.ejecutaSQL(qrySentencia.toString());
							
							qrySentencia = new StringBuffer("");
							qrySentencia.append("delete from com_control05"+
											" where ic_folio in ("+foliosId+")");							
							con.ejecutaSQL(qrySentencia.toString());
							
							qrySentencia = new StringBuffer("");
							qrySentencia.append("delete from com_control01"+
											" where ic_folio in ("+foliosId+")");							
							con.ejecutaSQL(qrySentencia.toString());
					}
					mensaje = "Las operaciones siguientes cambiaron de estatus a Operada con Fondeo Propio";				

				}//fin de operacion=CANCEL_FONDEO
				con.terminaTransaccion(true);
				
			}else	 {//Problema de concurrencia
				con.terminaTransaccion(false);
				mensaje= "<span style='color:red;'> Existen inconsistencias en la informaci�n, debido a la "+
							"concurrencia de usuarios. LOS CAMBIOS NO TOMARON EFECTO. "+
							"Intente nuevamente el proceso. </span>";
			}
					
		} catch(Exception e) {			
			e.printStackTrace();			
			log.error("error al procRegProyectos  " +e); 			
		} finally {	
			if(con.hayConexionAbierta()) {				
				con.cierraConexionDB();	
			}
		}  
		log.info("procRegProyectos (S)");  
		return mensaje;
	}	
	
	/**
	 * Metodo para los registros de Procesos- Contratos 
	 * @return 
	 * @param operacion
	 * @param errorPro
	 * @param productos
	 * @param folios
	 * @param numRegistros
	 */
	public String  procRegContratos(int numRegistros,   String [] folios ,  String [] productos, String [] errorPro, String operacion){
		
		log.info("procRegContratos (E)");
	
		AccesoDB con =new AccesoDB();
		PreparedStatement ps	= null;
		ResultSet			rs	= null;	
		PreparedStatement ps2	= null;
		ResultSet			rs1	= null;				
		int numRegistrosAfectar =0;
		int nFolios = 0;
		 
		String mensaje ="", qrySentencia ="",  foliosId ="",  pedidosId="", creditosId="", solicsId="", documentosId ="", descripcion="", 
		strFoliosConDescripcionD_ ="",  strFoliosSinDescripcionD_ ="", strFoliosConDescripcionA_ ="", 
	   strFoliosSinDescripcionA_ ="", strFoliosConDescripcionC_="", strFoliosSinDescripcionC_="",
		strFoliosConDescripcionI_="", strFoliosSinDescripcionI_="", strFoliosConDescripcionDis_="",
		strFoliosSinDescripcionDis_ ="";
	 
	 log.debug("************************** ");
	 log.debug("operacion: "+operacion);
	 log.debug("numRegistros: "+numRegistros);
	 log.debug("************************** ");
	 
	 
		try{	
			con.conexionDB();
			
		for(int i=0; i<numRegistros; i++) {
			String folio =   folios[i];
			String producto = productos[i];
			String strError = errorPro[i];
			log.debug("folio:---- "+folio);
			log.debug(" producto:--- "+producto);			
			log.debug("strError:  --- "+strError);
			
			if (folio != null && producto!=null) {
				switch(Integer.parseInt(producto)) {
					case 1:					
						if(foliosId.equals("")) {							
							foliosId = "'" + folio + "'";
						} else {						
							foliosId += ",'" + folio +"'";
						}
						if (strError.equals("43")) {
							if (strFoliosConDescripcionD_.equals(""))
								strFoliosConDescripcionD_ = "'" + folio + "'";
							else
								strFoliosConDescripcionD_ += ",'" + folio +"'";
						}
						else {
							if (strFoliosSinDescripcionD_.equals(""))
								strFoliosSinDescripcionD_ = "'" + folio + "'";
							else
								strFoliosSinDescripcionD_ += ",'" + folio +"'";
						}
						nFolios++;
					break;
					
					case 2:						
						if(pedidosId.equals("")) {							
							pedidosId = "'" + folio + "'";
						} else {							
							pedidosId += ",'" + folio + "'";
						}
						if (strError.equals("43")) {
							if (strFoliosConDescripcionA_.equals(""))
								strFoliosConDescripcionA_ = "'" + folio + "'";
							else
								strFoliosConDescripcionA_ += ",'" + folio +"'";
						} else {
							if (strFoliosSinDescripcionA_.equals(""))
								strFoliosSinDescripcionA_ = "'" + folio + "'";
							else
								strFoliosSinDescripcionA_ += ",'" + folio +"'";
						}
						nFolios++;			
					break;					
					case 0:
						
						if(creditosId.equals("")) {							
							creditosId = "'" + folio + "'";
						} else {							
							creditosId += ",'" + folio + "'";
						}
						if (strError.equals("43")) {
							if (strFoliosConDescripcionC_.equals(""))
								strFoliosConDescripcionC_ = "'" + folio + "'";
							else
								strFoliosConDescripcionC_ += ",'" + folio +"'";
						} else {
							if (strFoliosSinDescripcionC_.equals(""))
								strFoliosSinDescripcionC_ = "'" + folio + "'";
							else
								strFoliosSinDescripcionC_ += ",'" + folio +"'";
						}
						nFolios++;
					break;
					
					case 4:
						
						if(documentosId.equals("")) {							
							documentosId = "'" + folio + "'";
						} else {							
							documentosId += ",'" + folio + "'";
						}
						if (strError.equals("43")) {
							if (strFoliosConDescripcionDis_.equals(""))
								strFoliosConDescripcionDis_ = "'" + folio + "'";
							else
								strFoliosConDescripcionDis_ += ",'" + folio +"'";
						} else {
							if (strFoliosSinDescripcionDis_.equals(""))
								strFoliosSinDescripcionDis_ = "'" + folio + "'";
							else
								strFoliosSinDescripcionDis_ += ",'" + folio +"'";
						}
						nFolios++;
					break;
					
					case 5:
					
						if(solicsId.equals("")) {							
							solicsId = "'" + folio + "'";
						} else {						
							solicsId += ",'" + folio + "'";
						}
						if (strError.equals("43")) {
							if (strFoliosConDescripcionI_.equals(""))
								strFoliosConDescripcionI_ = "'" + folio + "'";
							else
								strFoliosConDescripcionI_ += ",'" + folio +"'";
						} else {
							if (strFoliosSinDescripcionI_.equals(""))
								strFoliosSinDescripcionI_ = "'" + folio + "'";
							else
								strFoliosSinDescripcionI_ += ",'" + folio +"'";
						}
						nFolios++;
					break;
				}//switch(Integer.parseInt(producto))
				
			}//if (folio != null && producto!=null)
		}//for(int i=1; i<=numRegistros; i++)
				
		log.debug("foliosId:  --- "+foliosId);
		log.debug("pedidosId:  --- "+pedidosId);
		log.debug("creditosId:  --- "+creditosId);
		log.debug("documentosId:  --- "+documentosId);

		qrySentencia ="";
		if(!foliosId.equals("")) {
			qrySentencia = "select count(*) from com_control02"+
								" where ic_folio in ("+foliosId+")";
		}
		qrySentencia += (!foliosId.equals("") && !pedidosId.equals(""))?" UNION ALL ":"";
		if(!pedidosId.equals("")) {
			qrySentencia += "select count(*) from com_controlant02"+
								" where ic_pedido in ("+pedidosId+")";
		}
		qrySentencia += (!creditosId.equals("") && (!foliosId.equals("") || !pedidosId.equals("")))?" UNION ALL ":"";
		if (!creditosId.equals(""))	{	
			qrySentencia += " select count(*) from com_interfase "+
								" where ic_solic_portal in ("+creditosId+") "+
								"   and ic_error_proceso is not null "+
								"   and ig_proceso_numero = 2 ";
		}
		qrySentencia += (!solicsId.equals("") && (!foliosId.equals("") || !pedidosId.equals("")||!creditosId.equals("")))?" UNION ALL ":"";
		if (!solicsId.equals(""))	{	
			qrySentencia += " select count(*) from inv_interfase "+
								" where CC_DISPOSICION in ("+solicsId+") "+
								"   and ic_error_proceso is not null "+
								"   and ig_proceso_numero = 2 ";
		}
		qrySentencia += (!documentosId.equals("") && (!foliosId.equals("") || !pedidosId.equals("")||!creditosId.equals("")||!solicsId.equals("")))?" UNION ALL ":"";
		if (!documentosId.equals("")) 	{
			qrySentencia += " select count(*) from dis_interfase "+
								" where ic_documento in ("+documentosId+") "+
								"   and ic_error_proceso is not null "+
								"   and ig_proceso_numero = 2 ";
			}
			rs1 = con.queryDB(qrySentencia);
			while (rs1.next()) {
				numRegistrosAfectar += rs1.getInt(1);
			}
			
			if (numRegistrosAfectar==nFolios)	 { //Control de concurrencia
				if (operacion.equals("REPROCESAR")) {
					if(!foliosId.equals("")) {  // Descuento Electronico.
						
						qrySentencia = "update com_control02 set cs_estatus='S'"+
										" where ic_folio in ("+foliosId+")";						
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "delete from com_control05"+
										" where ic_folio in ("+foliosId+")";						
						con.ejecutaSQL(qrySentencia);
					}
					
					if(!pedidosId.equals("")) { // Financiamiento a Pedidos
						
						qrySentencia = "update com_controlant02 set cs_estatus='S'"+
										" where ic_pedido in ("+pedidosId+")";						
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "delete from com_controlant05"+
										" where ic_pedido in ("+pedidosId+")";						
						con.ejecutaSQL(qrySentencia);
					}
					
					if(!creditosId.equals("")) { // Reprocesar Credito Electronico.
					
						qrySentencia = "update com_interfase "+
										"set ic_error_proceso=null, cc_codigo_aplicacion=null "+
										"where ic_solic_portal in ("+creditosId+")";							
							con.ejecutaSQL(qrySentencia);
					}
					
					if(!solicsId.equals("")) { // Reprocesar Credito Electronico.
						qrySentencia = "update inv_interfase "+
										"set ic_error_proceso=null, cc_codigo_aplicacion=null "+
										"where CC_DISPOSICION in ("+solicsId+")";							
							con.ejecutaSQL(qrySentencia);
					}
					if(!documentosId.equals("")) { // Reprocesar Credito Electronico.
						
						qrySentencia = "update dis_interfase "+
										"set ic_error_proceso=null, cc_codigo_aplicacion=null "+
										"where ic_documento in ("+documentosId+")";							
							con.ejecutaSQL(qrySentencia);
					}
					mensaje = "Los registros mostrados fueron REPROCESADOS";
				
				}	else if (operacion.equals("CANCELAR")) {
				
					if(!foliosId.equals("")) { // Descuento Electronico.
						qrySentencia = "update com_solicitud"+
										" set ic_estatus_solic=1, ic_bloqueo=1"+
										" where ic_folio in ("+foliosId+")";						
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "delete from com_control05"+
										" where ic_folio in ("+foliosId+")";						
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "delete from com_control02"+
										" where ic_folio in ("+foliosId+")";						
						con.ejecutaSQL(qrySentencia);
					}
					if(!pedidosId.equals("")) { // Financiamiento a Pedidos
						qrySentencia = "update com_anticipo"+
										" set ic_estatus_antic=1, ic_bloqueo=1"+
										" where ic_pedido in ("+pedidosId+")";						
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "delete from com_controlant05"+
										" where ic_pedido in ("+pedidosId+")";
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "delete from com_controlant02"+
										" where ic_pedido in ("+pedidosId+")";						
						con.ejecutaSQL(qrySentencia);
					}
					
					if(!creditosId.equals("")) { // Credito Electronico.
						
						qrySentencia = "delete from com_interfase "+
										"where ic_solic_portal in ("+creditosId+")";						
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "update com_solic_portal "+
										"set ic_estatus_solic=4, ic_bloqueo=1, df_operacion = sysdate "+
										"where ic_solic_portal in ("+creditosId+")";						
						con.ejecutaSQL(qrySentencia);
					}
					if(!solicsId.equals("")) { // Credicadenas.
						
						qrySentencia = "delete from inv_interfase "+
										"where CC_DISPOSICION in ("+solicsId+")";						
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "update inv_solicitud "+
										"set ic_estatus_solic=1, ic_bloqueo=1 "+
										"where CC_DISPOSICION in ("+solicsId+")";						
						con.ejecutaSQL(qrySentencia);
					}
					if(!documentosId.equals("")) { // Financiamiento a Distribuidores
						qrySentencia = "delete from dis_interfase "+
										"where ic_documento in ("+documentosId+")";						
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "update dis_solicitud "+
										"set ic_estatus_solic=1, ic_bloqueo=1 "+
										"where ic_documento in ("+documentosId+")";						
						con.ejecutaSQL(qrySentencia);
					}
					mensaje = " El registro mostrado fue CANCELADO";
				
				}	else if (operacion.equals("CANCEL_FONDEO")){
				
					if(!foliosId.equals("")) { // Descuento Electronico.
						
						qrySentencia = "update com_solicitud"+
										" set ic_estatus_solic=10, ic_bloqueo=1"+
										" where ic_folio in ("+foliosId+")";						
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "delete from com_control05"+
										" where ic_folio in ("+foliosId+")";
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "delete from com_control02"+
										" where ic_folio in ("+foliosId+")";
						con.ejecutaSQL(qrySentencia);
						
					}
					mensaje = "	Las operaciones siguientes cambiaron de estatus a Operada con Fondeo Propio";
				}//fin de operacion=CANCEL_FONDEO
				con.terminaTransaccion(true);
			}	else	 { //Problema de concurrencia
				con.terminaTransaccion(false);
				mensaje = "<span style='color:red;'> 	Existen inconsistencias en la informaci�n, debido a la "+
							" concurrencia de usuarios. LOS CAMBIOS NO TOMARON EFECTO."+
							" Intente nuevamente el proceso.</span>";					
			}
					
		} catch(Exception e) {			
			e.printStackTrace();			
			log.error("error al procRegContratos  " +e); 			
		} finally {	
			if(con.hayConexionAbierta()) {				
				con.cierraConexionDB();	
			}
		}  
		log.info("procRegContratos (S)");  
		return mensaje;
	}	
	
	/**
	 * Metodo para los registros de Procesos- Disposiciones 
	 * @return 
	 * @param operacion
	 * @param errorPro
	 * @param productos
	 * @param folios
	 * @param numRegistros
	 */
	public String  procRegDisposiciones(int numRegistros,   String [] folios ,  String [] productos, String [] errorPro, String operacion){
		
		log.info("procRegDisposiciones (E)");
	
		AccesoDB con =new AccesoDB();
		PreparedStatement ps	= null;
		ResultSet			rs	= null;	
		PreparedStatement ps2	= null;
		ResultSet			rs1	= null;				
		int numRegistrosAfectar =0;
		int nFolios = 0;
		 
		String mensaje ="", qrySentencia ="",  foliosId ="",  pedidosId="", creditosId="", solicsId="", documentosId ="", descripcion="", 
		strFoliosConDescripcionD_ ="",  strFoliosSinDescripcionD_ ="", strFoliosConDescripcionA_ ="", 
	   strFoliosSinDescripcionA_ ="", strFoliosConDescripcionC_="", strFoliosSinDescripcionC_="",
		strFoliosConDescripcionI_="", strFoliosSinDescripcionI_="", strFoliosConDescripcionDis_="",
		strFoliosSinDescripcionDis_ ="";
	 
	 log.debug("************************** ");
	 log.debug("operacion: "+operacion);
	 log.debug("numRegistros: "+numRegistros);
	 log.debug("************************** ");
	 
	 
		try{	
			con.conexionDB();
			
			for(int i=0; i<numRegistros; i++) {
				String folio =   folios[i];
				String producto = productos[i];
				String strError = errorPro[i];
				log.debug("folio:---- "+folio);
				log.debug(" producto:--- "+producto);			
				log.debug("strError:  --- "+strError);
				
				if (folio != null && producto!=null) {
					switch(Integer.parseInt(producto)) {
						case 1:					
							if(foliosId.equals("")) {							
								foliosId = "'" + folio + "'";
							} else {						
								foliosId += ",'" + folio +"'";
							}
							if (strError.equals("43")) {
								if (strFoliosConDescripcionD_.equals(""))
									strFoliosConDescripcionD_ = "'" + folio + "'";
								else
									strFoliosConDescripcionD_ += ",'" + folio +"'";
							}
							else {
								if (strFoliosSinDescripcionD_.equals(""))
									strFoliosSinDescripcionD_ = "'" + folio + "'";
								else
									strFoliosSinDescripcionD_ += ",'" + folio +"'";
							}
							nFolios++;
						break;
						
						case 2:						
							if(pedidosId.equals("")) {							
								pedidosId = "'" + folio + "'";
							} else {							
								pedidosId += ",'" + folio + "'";
							}
							if (strError.equals("43")) {
								if (strFoliosConDescripcionA_.equals(""))
									strFoliosConDescripcionA_ = "'" + folio + "'";
								else
									strFoliosConDescripcionA_ += ",'" + folio +"'";
							} else {
								if (strFoliosSinDescripcionA_.equals(""))
									strFoliosSinDescripcionA_ = "'" + folio + "'";
								else
									strFoliosSinDescripcionA_ += ",'" + folio +"'";
							}
							nFolios++;			
						break;					
						case 0:
							
							if(creditosId.equals("")) {							
								creditosId = "'" + folio + "'";
							} else {							
								creditosId += ",'" + folio + "'";
							}
							if (strError.equals("43")) {
								if (strFoliosConDescripcionC_.equals(""))
									strFoliosConDescripcionC_ = "'" + folio + "'";
								else
									strFoliosConDescripcionC_ += ",'" + folio +"'";
							} else {
								if (strFoliosSinDescripcionC_.equals(""))
									strFoliosSinDescripcionC_ = "'" + folio + "'";
								else
									strFoliosSinDescripcionC_ += ",'" + folio +"'";
							}
							nFolios++;
						break;
						
						case 4:
							
							if(documentosId.equals("")) {							
								documentosId = "'" + folio + "'";
							} else {							
								documentosId += ",'" + folio + "'";
							}
							if (strError.equals("43")) {
								if (strFoliosConDescripcionDis_.equals(""))
									strFoliosConDescripcionDis_ = "'" + folio + "'";
								else
									strFoliosConDescripcionDis_ += ",'" + folio +"'";
							} else {
								if (strFoliosSinDescripcionDis_.equals(""))
									strFoliosSinDescripcionDis_ = "'" + folio + "'";
								else
									strFoliosSinDescripcionDis_ += ",'" + folio +"'";
							}
							nFolios++;
						break;
						
						case 5:
						
							if(solicsId.equals("")) {							
								solicsId = "'" + folio + "'";
							} else {						
								solicsId += ",'" + folio + "'";
							}
							if (strError.equals("43")) {
								if (strFoliosConDescripcionI_.equals(""))
									strFoliosConDescripcionI_ = "'" + folio + "'";
								else
									strFoliosConDescripcionI_ += ",'" + folio +"'";
							} else {
								if (strFoliosSinDescripcionI_.equals(""))
									strFoliosSinDescripcionI_ = "'" + folio + "'";
								else
									strFoliosSinDescripcionI_ += ",'" + folio +"'";
							}
							nFolios++;
						break;
					}//switch(Integer.parseInt(producto))
					
				}//if (folio != null && producto!=null)
			}//for(int i=1; i<=numRegistros; i++)
					
			log.debug("foliosId:  --- "+foliosId);
			log.debug("pedidosId:  --- "+pedidosId);
			log.debug("creditosId:  --- "+creditosId);
			log.debug("documentosId:  --- "+documentosId);

			qrySentencia ="";
			if(!foliosId.equals("")) {
				qrySentencia = "select count(1) from com_control04"+
								" where ic_folio in ("+foliosId+")";
			}
			qrySentencia += (!foliosId.equals("") && !pedidosId.equals(""))?" UNION ALL ":"";
			
			if(!pedidosId.equals("")) {
				qrySentencia += "select count(1) from com_controlant04"+
								" where ic_pedido in ("+pedidosId+")";
			}
			qrySentencia += (!creditosId.equals("")&&(!foliosId.equals("") || !pedidosId.equals("")))?" UNION ALL ":"";
			
			if(!creditosId.equals("")) {
				qrySentencia += "select count(1) from com_interfase"+
								" where ig_proceso_numero=4 and "+
								"   ic_error_proceso is not null and "+
								"	ic_solic_portal in ("+creditosId+")";
			}
			qrySentencia += (!solicsId.equals("")&&(!foliosId.equals("") || !pedidosId.equals("")||!creditosId.equals("")))?" UNION ALL ":"";
			
			if(!solicsId.equals("")) {
				qrySentencia += "select count(1) from inv_interfase"+
								" where ig_proceso_numero=4 and "+
								"   ic_error_proceso is not null and "+
								"	CC_DISPOSICION in ("+solicsId+")";
			}
			qrySentencia += (!documentosId.equals("")&&(!foliosId.equals("") || !pedidosId.equals("")||!creditosId.equals("")||!solicsId.equals("")))?" UNION ALL ":"";
			
			if(!documentosId.equals("")) {
				qrySentencia += "select count(1) from dis_interfase"+
								" where ig_proceso_numero=4 and "+
								"   ic_error_proceso is not null and "+
								"	ic_documento in ("+documentosId+")";
			}
			rs1 = con.queryDB(qrySentencia);
			while (rs1.next()) {
				numRegistrosAfectar += rs1.getInt(1);
			}

			if (numRegistrosAfectar==nFolios)	 { //Control de concurrencia
				if (operacion.equals("REPROCESAR"))	{
					if(!foliosId.equals("")) { // Descuento Electronico.
						
						qrySentencia = "update com_control04 set cs_estatus='S'"+
										" where ic_folio in ("+foliosId+")";						
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "delete from com_control05"+
										" where ic_folio in ("+foliosId+")";						
						con.ejecutaSQL(qrySentencia);
					}
					if(!pedidosId.equals("")) { // Finaciamiento a Pedidos
						qrySentencia = "update com_controlant04 set cs_estatus='S'"+
										" where ic_pedido in ("+pedidosId+")";						
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "delete from com_controlant05"+
										" where ic_pedido in ("+pedidosId+")";						
						con.ejecutaSQL(qrySentencia);
					}
					if(!creditosId.equals("")) { // Reprocesar Credito Electronico.
					
						qrySentencia = "update com_interfase "+
										"set ic_error_proceso=null, cc_codigo_aplicacion=null "+
										"where ic_solic_portal in ("+creditosId+")";							
							con.ejecutaSQL(qrySentencia);
					}
					if(!solicsId.equals("")) { // Reprocesar Credicadenas.
					
						qrySentencia = "update inv_interfase "+
										"set ic_error_proceso=null, cc_codigo_aplicacion=null "+
										"where CC_DISPOSICION in ("+solicsId+")";							
							con.ejecutaSQL(qrySentencia);
					}
					if(!documentosId.equals("")) { // Reprocesar Financiamiento a Distribuidores.
						qrySentencia = "update dis_interfase "+
										"set ic_error_proceso=null, cc_codigo_aplicacion=null "+
										"where ic_documento in ("+documentosId+")";							
							con.ejecutaSQL(qrySentencia);
					}
					mensaje = "Los registros mostrados fueron REPROCESADOS";
					
				}	else if (operacion.equals("CANCELAR")) 	{
					
					if(!foliosId.equals("")) { // Descuento Electronico.
						qrySentencia = "update com_solicitud"+
										" set ic_estatus_solic=1, ic_bloqueo=1"+
										" where ic_folio in ("+foliosId+")";						
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "delete from com_control05"+
										" where ic_folio in ("+foliosId+")";						
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "delete from com_control04"+
										" where ic_folio in ("+foliosId+")";						
						con.ejecutaSQL(qrySentencia);
					}
					if(!pedidosId.equals("")) { // Finaciamiento a Pedidos
					
						qrySentencia = "update com_anticipo"+
										" set ic_estatus_antic=1, ic_bloqueo=1"+
										" where ic_pedido in ("+pedidosId+")";												
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "delete from com_controlant05"+
										" where ic_pedido in ("+pedidosId+")";
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "delete from com_controlant04"+
										" where ic_pedido in ("+pedidosId+")";						
						con.ejecutaSQL(qrySentencia);
					}
					if(!creditosId.equals("")) { // Cancelacion Credito Electronico.
						qrySentencia = "delete from com_interfase "+
										"where ic_solic_portal in ("+creditosId+")";						
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "update com_solic_portal "+
										"set ic_estatus_solic=4, ic_bloqueo=1, df_operacion = sysdate "+
										"where ic_solic_portal in ("+creditosId+")";						
						con.ejecutaSQL(qrySentencia);
						
					}
					if(!solicsId.equals("")) { // Cancelacion Credicadenas.
						
						qrySentencia = "delete from inv_interfase "+
										"where CC_DISPOSICION in ("+solicsId+")";						
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "update inv_solicitud "+
										"set ic_estatus_solic=1, ic_bloqueo=1 "+
										"where CC_DISPOSICION in ("+solicsId+")";						
						con.ejecutaSQL(qrySentencia);
						
					}
					if(!documentosId.equals("")) { // Cancelacion Financiamiento a Distribuidores.
						
						qrySentencia = "delete from dis_interfase "+
										"where ic_documento in ("+documentosId+")";						
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "update dis_solicitud "+
										"set ic_estatus_solic=1, ic_bloqueo=1 "+
										"where ic_documento in ("+documentosId+")";						
						con.ejecutaSQL(qrySentencia);						
					}
					mensaje="El registro mostrado fue CANCELADO ";				
				
				}	else if (operacion.equals("CANCEL_FONDEO")){
				
					if(!foliosId.equals("")) { // Descuento Electronico.
						
						qrySentencia = "update com_solicitud"+
										" set ic_estatus_solic=10, ic_bloqueo=1"+
										" where ic_folio in ("+foliosId+")";						
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "delete from com_control05"+
										" where ic_folio in ("+foliosId+")";												
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "delete from com_control04"+
										" where ic_folio in ("+foliosId+")";						
						con.ejecutaSQL(qrySentencia);
					}
						mensaje = "	Las operaciones siguientes cambiaron de estatus a Operada con Fondeo Propio";					

				}//fin de operacion=CANCEL_FONDEO

				con.terminaTransaccion(true);
			}	else	 { //Problema de concurrencia
				con.terminaTransaccion(false);
			mensaje = "<span style='color:red;'> 	Existen inconsistencias en la informaci�n, debido a la"+
						 " concurrencia de usuarios. LOS CAMBIOS NO TOMARON EFECTO."+
						 "	Intente nuevamente el proceso.</span>";						 
			}
					
		} catch(Exception e) {			
			e.printStackTrace();			
			log.error("error al procRegDisposiciones  " +e); 			
		} finally {	
			if(con.hayConexionAbierta()) {				
				con.cierraConexionDB();	
			}
		}  
		log.info("procRegDisposiciones (S)");  
		return mensaje;
	}	
	
	
	
	/**
	 * Metodo para los registros de Procesos- Pr�stamos 
	 * @return 
	 * @param operacion
	 * @param errorPro
	 * @param productos
	 * @param folios
	 * @param numRegistros
	 */
	public String  procRegPrestamos(int numRegistros,   String [] folios ,  String [] productos, String [] errorPro, String operacion){
		
		log.info("procRegPrestamos (E)");
	
		AccesoDB con =new AccesoDB();
		PreparedStatement ps	= null;
		ResultSet			rs	= null;	
		PreparedStatement ps2	= null;
		ResultSet			rs1	= null;				
		int numRegistrosAfectar =0;
		int nFolios = 0;
		 
		String mensaje ="", qrySentencia ="",  foliosId ="",  pedidosId="", creditosId="", solicsId="", documentosId ="", descripcion="", 
		strFoliosConDescripcionD_ ="",  strFoliosSinDescripcionD_ ="", strFoliosConDescripcionA_ ="", 
	   strFoliosSinDescripcionA_ ="", strFoliosConDescripcionC_="", strFoliosSinDescripcionC_="",
		strFoliosConDescripcionI_="", strFoliosSinDescripcionI_="", strFoliosConDescripcionDis_="",
		strFoliosSinDescripcionDis_ ="";
	 
	 log.debug("************************** ");
	 log.debug("operacion: "+operacion);
	 log.debug("numRegistros: "+numRegistros);
	 log.debug("************************** ");
	 
	 
		try{	
			con.conexionDB();
			
			for(int i=0; i<numRegistros; i++) {
				String folio =   folios[i];
				String producto = productos[i];
				String strError = errorPro[i];
				log.debug("folio:---- "+folio);
				log.debug(" producto:--- "+producto);			
				log.debug("strError:  --- "+strError);
				
				if (folio != null && producto!=null) {
					switch(Integer.parseInt(producto)) {
						case 1:					
							if(foliosId.equals("")) {							
								foliosId = "'" + folio + "'";
							} else {						
								foliosId += ",'" + folio +"'";
							}
							if (strError.equals("43")) {
								if (strFoliosConDescripcionD_.equals(""))
									strFoliosConDescripcionD_ = "'" + folio + "'";
								else
									strFoliosConDescripcionD_ += ",'" + folio +"'";
							}
							else {
								if (strFoliosSinDescripcionD_.equals(""))
									strFoliosSinDescripcionD_ = "'" + folio + "'";
								else
									strFoliosSinDescripcionD_ += ",'" + folio +"'";
							}
							nFolios++;
						break;
						
						case 2:						
							if(pedidosId.equals("")) {							
								pedidosId = "'" + folio + "'";
							} else {							
								pedidosId += ",'" + folio + "'";
							}
							if (strError.equals("43")) {
								if (strFoliosConDescripcionA_.equals(""))
									strFoliosConDescripcionA_ = "'" + folio + "'";
								else
									strFoliosConDescripcionA_ += ",'" + folio +"'";
							} else {
								if (strFoliosSinDescripcionA_.equals(""))
									strFoliosSinDescripcionA_ = "'" + folio + "'";
								else
									strFoliosSinDescripcionA_ += ",'" + folio +"'";
							}
							nFolios++;			
						break;					
						case 0:
							
							if(creditosId.equals("")) {							
								creditosId = "'" + folio + "'";
							} else {							
								creditosId += ",'" + folio + "'";
							}
							if (strError.equals("43")) {
								if (strFoliosConDescripcionC_.equals(""))
									strFoliosConDescripcionC_ = "'" + folio + "'";
								else
									strFoliosConDescripcionC_ += ",'" + folio +"'";
							} else {
								if (strFoliosSinDescripcionC_.equals(""))
									strFoliosSinDescripcionC_ = "'" + folio + "'";
								else
									strFoliosSinDescripcionC_ += ",'" + folio +"'";
							}
							nFolios++;
						break;
						
						case 4:
							
							if(documentosId.equals("")) {							
								documentosId = "'" + folio + "'";
							} else {							
								documentosId += ",'" + folio + "'";
							}
							if (strError.equals("43")) {
								if (strFoliosConDescripcionDis_.equals(""))
									strFoliosConDescripcionDis_ = "'" + folio + "'";
								else
									strFoliosConDescripcionDis_ += ",'" + folio +"'";
							} else {
								if (strFoliosSinDescripcionDis_.equals(""))
									strFoliosSinDescripcionDis_ = "'" + folio + "'";
								else
									strFoliosSinDescripcionDis_ += ",'" + folio +"'";
							}
							nFolios++;
						break;
						
						case 5:
						
							if(solicsId.equals("")) {							
								solicsId = "'" + folio + "'";
							} else {						
								solicsId += ",'" + folio + "'";
							}
							if (strError.equals("43")) {
								if (strFoliosConDescripcionI_.equals(""))
									strFoliosConDescripcionI_ = "'" + folio + "'";
								else
									strFoliosConDescripcionI_ += ",'" + folio +"'";
							} else {
								if (strFoliosSinDescripcionI_.equals(""))
									strFoliosSinDescripcionI_ = "'" + folio + "'";
								else
									strFoliosSinDescripcionI_ += ",'" + folio +"'";
							}
							nFolios++;
						break;
					}//switch(Integer.parseInt(producto))
					
				}//if (folio != null && producto!=null)
			}//for(int i=1; i<=numRegistros; i++)
					
			log.debug("foliosId:  --- "+foliosId);
			log.debug("pedidosId:  --- "+pedidosId);
			log.debug("creditosId:  --- "+creditosId);
			log.debug("documentosId:  --- "+documentosId);

			qrySentencia ="";
			if(!foliosId.equals("")) {
				qrySentencia = "select count(*) from com_control03"+
								" where ic_folio in ("+foliosId+")";
			}
			qrySentencia += (!foliosId.equals("") && !pedidosId.equals(""))?" UNION ALL ":"";
			
			if(!pedidosId.equals("")) {
				qrySentencia += "select count(*) from com_controlant03"+
								" where ic_pedido in ("+pedidosId+")";
			}
			qrySentencia += (!creditosId.equals("")&&(!foliosId.equals("") || !pedidosId.equals("")))?" UNION ALL ":"";
			
			if(!creditosId.equals("")) {
				qrySentencia += "select count(*) from com_interfase"+
								" where ig_proceso_numero=3 and "+
								"   ic_error_proceso is not null and "+
								"	ic_solic_portal in ("+creditosId+")";
			}
			qrySentencia += (!solicsId.equals("")&&(!foliosId.equals("") || !pedidosId.equals("")||!creditosId.equals("")))?" UNION ALL ":"";
			
			if(!solicsId.equals("")) {
				qrySentencia += "select count(*) from inv_interfase"+
								" where ig_proceso_numero=3 and "+
								"   ic_error_proceso is not null and "+
								"	CC_DISPOSICION in ("+solicsId+")";
			}
			qrySentencia += (!documentosId.equals("")&&(!foliosId.equals("") || !pedidosId.equals("")||!creditosId.equals("")||!solicsId.equals("")))?" UNION ALL ":"";
			
			if(!documentosId.equals("")) {
				qrySentencia += "select count(*) from dis_interfase"+
								" where ig_proceso_numero=3 and "+
								"   ic_error_proceso is not null and "+
								"	ic_documento in ("+documentosId+")";
			}
			rs1 = con.queryDB(qrySentencia);
			while (rs1.next()) {
				numRegistrosAfectar += rs1.getInt(1);
			}
			
			if (numRegistrosAfectar==nFolios){ //Control de concurrencia
				if (operacion.equals("REPROCESAR"))	{
					if(!foliosId.equals("")) { // Descuento Electronico.
						qrySentencia = "update com_control03 set cs_estatus='S'"+
										" where ic_folio in ("+foliosId+")";						
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "delete from com_control05"+
										" where ic_folio in ("+foliosId+")";						
						con.ejecutaSQL(qrySentencia);
					}
					if(!pedidosId.equals("")) { // Finaciamiento a Pedidos
						qrySentencia = "update com_controlant03 set cs_estatus='S'"+
										" where ic_pedido in ("+pedidosId+")";						
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "delete from com_controlant05"+
										" where ic_pedido in ("+pedidosId+")";						
						con.ejecutaSQL(qrySentencia);
						
					}
					if(!creditosId.equals("")) { // Reprocesar Credito Electronico.
						qrySentencia = "update com_interfase "+
										"set ic_error_proceso=null, cc_codigo_aplicacion=null "+
										"where ic_solic_portal in ("+creditosId+")";
							con.ejecutaSQL(qrySentencia);
					}
					if(!solicsId.equals("")) { // Reprocesar Credicadenas.
						qrySentencia = "update inv_interfase "+
										"set ic_error_proceso=null, cc_codigo_aplicacion=null "+
										"where CC_DISPOSICION in ("+solicsId+")";							
							con.ejecutaSQL(qrySentencia);
					}
					if(!documentosId.equals("")) { // Reprocesar Financiamiento a Distribuidores.
						qrySentencia = "update dis_interfase "+
										"set ic_error_proceso=null, cc_codigo_aplicacion=null "+
										"where ic_documento in ("+documentosId+")";							
							con.ejecutaSQL(qrySentencia);
					}
					mensaje = "Los registros mostrados fueron REPROCESADOS";				
				
				}	else if (operacion.equals("CANCELAR"))	{
				
					if(!foliosId.equals("")) { // Descuento Electronico.
						qrySentencia = "update com_solicitud"+
										" set ic_estatus_solic=1, ic_bloqueo=1"+
										" where ic_folio in ("+foliosId+")";						
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "delete from com_control05"+
										" where ic_folio in ("+foliosId+")";																
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "delete from com_control03"+
										" where ic_folio in ("+foliosId+")";
						con.ejecutaSQL(qrySentencia);						
					}
					if(!pedidosId.equals("")) { // Finaciamiento a Pedidos
						qrySentencia = "update com_anticipo"+
										" set ic_estatus_antic=1, ic_bloqueo=1"+
										" where ic_pedido in ("+pedidosId+")";						
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "delete from com_controlant05"+
										" where ic_pedido in ("+pedidosId+")";						
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "delete from com_controlant03"+
										" where ic_pedido in ("+pedidosId+")";						
						con.ejecutaSQL(qrySentencia);
						
					}
					if(!creditosId.equals("")) { // Credito Electronico.
						qrySentencia = "delete from com_interfase "+
										"where ic_solic_portal in ("+creditosId+")";						
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "update com_solic_portal "+
										"set ic_estatus_solic=4, ic_bloqueo=1, df_operacion = sysdate "+
										"where ic_solic_portal in ("+creditosId+")";						
						con.ejecutaSQL(qrySentencia);
						
					}
					if(!solicsId.equals("")) { // Credicadenas
						qrySentencia = "delete from inv_interfase "+
										"where CC_DISPOSICION in ("+solicsId+")";						
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "update inv_solicitud "+
										"set ic_estatus_solic=1, ic_bloqueo=1 "+
										"where CC_DISPOSICION in ("+solicsId+")";						
						con.ejecutaSQL(qrySentencia);
					}
					if(!documentosId.equals("")) { // Financiamiento a Distribuidores
						qrySentencia = "delete from dis_interfase "+
										"where ic_documento in ("+documentosId+")";						
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "update dis_solicitud "+
										"set ic_estatus_solic=1, ic_bloqueo=1 "+
										"where ic_documento in ("+documentosId+")";						
						con.ejecutaSQL(qrySentencia);
					}
					mensaje ="El registro mostrado fue CANCELADO";
					
				}	else if (operacion.equals("CANCEL_FONDEO")){
					if(!foliosId.equals("")) { // Descuento Electronico.
						qrySentencia = "update com_solicitud"+
										" set ic_estatus_solic=10, ic_bloqueo=1"+
										" where ic_folio in ("+foliosId+")";						
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "delete from com_control05"+
										" where ic_folio in ("+foliosId+")";
						con.ejecutaSQL(qrySentencia);
						
						qrySentencia = "delete from com_control03"+
										" where ic_folio in ("+foliosId+")";						
						con.ejecutaSQL(qrySentencia);
					}
						mensaje = "	Las operaciones siguientes cambiaron de estatus a Operada con Fondeo Propio";
					
				}//fin de operacion=CANCEL_FONDEO
				con.terminaTransaccion(true);
			}  else	 {//Problema de concurrencia
				con.terminaTransaccion(false);
				mensaje =" <span style='color:red;'>Existen inconsistencias en la informaci�n, debido a la "+
							" concurrencia de usuarios. LOS CAMBIOS NO TOMARON EFECTO."+
							" Intente nuevamente el proceso. ";
			}
		} catch(Exception e) {			
			e.printStackTrace();			
			log.error("error al procRegPrestamos  " +e); 			
		} finally {	
			if(con.hayConexionAbierta()) {				
				con.cierraConexionDB();	
			}
		}  
		log.info("procRegPrestamos (S)");  
		return mensaje;
	}	
	
	public String getTipoProceso() {
		return tipoProceso;
	}

	public void setTipoProceso(String tipoProceso) {
		this.tipoProceso = tipoProceso;
	}

	/**
	 *  Fodea 017-2015 
	 * @return 
	 * @param ic_operacion
	 */
	public HashMap getDatosCorreo(String ic_operacion)  {
	 
		HashMap alParametros = new HashMap();
		log.info("getDatosCorreo(E)");
		AccesoDB con = new AccesoDB();
		try {
			
			con.conexionDB();
			StringBuffer qrySentencia = new StringBuffer("");
			PreparedStatement ps = null;
			ResultSet rs = null;
			List 	conditions = null;
	
			qrySentencia.append("  SELECT  IC_OPERACIONES,  CG_DESTINATARIO, CG_CCDESTINATARIO , CG_ASUNTO, CG_CUERPO_CORREO   "+
					" FROM COM_PLANTILLA_CORREO  "+
					" WHERE IC_OPERACIONES =  ? ");
			
			log.debug(":::Consulta :"+ qrySentencia);
			log.debug(":::ic_operacion::"+ ic_operacion);
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1, Integer.parseInt(ic_operacion));
	
			rs = ps.executeQuery();
			while(rs.next()) {
				alParametros = new HashMap();
				alParametros.put("CG_DESTINATARIO", rs.getString("CG_DESTINATARIO")==null?"":rs.getString("CG_DESTINATARIO"));
				alParametros.put("CG_CCDESTINATARIO", rs.getString("CG_CCDESTINATARIO")==null?"":rs.getString("CG_CCDESTINATARIO"));
				alParametros.put("CG_ASUNTO", rs.getString("CG_ASUNTO")==null?"":rs.getString("CG_ASUNTO"));
				alParametros.put("CG_CUERPO_CORREO", rs.getString("CG_CUERPO_CORREO")==null?"":rs.getString("CG_CUERPO_CORREO"));
			}
			rs.close();
			ps.close();		
			
		} catch(SQLException sqle) {
			sqle.printStackTrace();		
		} catch (Exception e) {
			e.printStackTrace();		
		} finally {
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
		}
		log.info("getDatosCorreo(S)");
		return alParametros;
	}

	/**
	 * Fodea 017-2015
	 * @return 
	 * @param parametros
	 */
	public String  procesoEnviodeCorreo (List parametros)		{
			log.info("procesoEnviodeCorreo (E)");

		   StringBuffer mensajeCorreo = new StringBuffer();
			HashMap 	archivoAdjunto 		 = new HashMap();
			ArrayList listaDeArchivos = new ArrayList();
			ArrayList listaDeImagenes = new ArrayList();

			String remitente=  parametros.get(0).toString();
			String destinatario=  parametros.get(1).toString();
			String ccdestinatario=  parametros.get(2).toString();
			String asunto=  parametros.get(3).toString();
			String cuerpoCorreo=  parametros.get(4).toString();
			String nombreArchivo=  parametros.get(5).toString();
			String respuesta ="Fallo";
			

			try {

				mensajeCorreo = new StringBuffer();
				
				mensajeCorreo.append("<html> "+
					" <head> "+
					 " <meta http-equiv='content-type' content='text/html; charset=UTF-8'> "+
					" </head>"+
					 " <body> "+cuerpoCorreo+
				  "  </body>  </html> "); 

				// para adjuntar el archivo
				archivoAdjunto = new HashMap();
				archivoAdjunto.put("FILE_FULL_PATH", nombreArchivo );
				listaDeArchivos.add(archivoAdjunto);


				try { //Si el env�o del correo falla se ignora.
					Correo correo = new Correo();
					correo.setCodificacion("charset=UTF-8"); 
					correo.enviaCorreoConDatosAdjuntos(remitente,destinatario,ccdestinatario,asunto,mensajeCorreo.toString(),listaDeImagenes,listaDeArchivos);
				} catch(Throwable t) {
					log.error("Error al enviar correo " + t.getMessage());
				}

		}catch(Exception e){
				log.error("procesoEnviodeCorreo (Exception) " + e);
				throw new AppException("procesoEnviodeCorreo(Exception) ", e);
			}finally{
				respuesta= "Correo enviado con �xito";
			}

			log.info("procesoEnviodeCorreo (S)");

			return respuesta;

		}
		
	public String getDirectorioPlantilla() {
		return directorioPlantilla;
	} 

	public void setDirectorioPlantilla(String directorioPlantilla) {
		this.directorioPlantilla = directorioPlantilla;
	}

	public String getIc_solicitudes() { 
		return ic_solicitudes;
	}

	public void setIc_solicitudes(String ic_solicitudes) { 
		this.ic_solicitudes = ic_solicitudes;
	}
	
	
	
	



	
}