package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class BitCambiosNafinCA2 implements IQueryGeneratorRegExtJS {
	public BitCambiosNafinCA2()  {}

	private String txt_ne;
	private String cmb_pantalla;
	private String pantalla;
	private String txt_usuario;
	private String txt_fecha_act_de;
	private String txt_fecha_act_a;
	private String rfc_prov;
	private String numeroProveedor;
	private String claveEpo;
	
	private String cmb_afiliado;
	private String cmb_grupo;
	private String noBancoFondeo;

	private static final Log log = ServiceLocator.getInstance().getLog(BitCambiosNafinCA2.class);//Variable para enviar mensajes al log.  

	public String getAggregateCalculationQuery()	{
		return "";
	}
	/*
	public String getAggregateCalculationQuery()	{ 

		StringBuffer 	qrySentencia	= new StringBuffer();
    	StringBuffer	condicion		= new StringBuffer();;
		
		String tablaTipoAfiliado = "";
		String campoTipoAfiliado = "";
		
		if (cmb_afiliado.equals("E") || cmb_afiliado.equals("EI")) {
			tablaTipoAfiliado = "comcat_epo";
			campoTipoAfiliado = "ic_epo";
		} else if (cmb_afiliado.equals("P") || cmb_afiliado.equals("PI") ||
				cmb_afiliado.equals("D") || cmb_afiliado.equals("C")) {
			tablaTipoAfiliado = "comcat_pyme";
			campoTipoAfiliado = "ic_pyme";
		} else if (cmb_afiliado.equals("DF")) {
			tablaTipoAfiliado = "foncat_distribuidor";
			campoTipoAfiliado = "ic_distribuidor";
		} else if (cmb_afiliado.equals("I") || cmb_afiliado.equals("CON")) {
			tablaTipoAfiliado = "comcat_if";
			campoTipoAfiliado = "ic_if";
		} else if (cmb_afiliado.equals("N")) {  //truco para usuarios Nafin
			tablaTipoAfiliado = "(select 0 as ic_nafin, 'Nacional Financiera' as cg_razon_social from dual)";
			campoTipoAfiliado = "ic_nafin";
		}else if (cmb_afiliado.equals("GA")) {  //truco para usuarios Nafin
			tablaTipoAfiliado = "(select ic_grupo_epo as ic_grupo, cg_descripcion as cg_razon_social from comcat_grupo_epo)";
			campoTipoAfiliado = "ic_grupo";
		}

		qrySentencia.append(
				" 	SELECT COUNT(1) "   +
				"  FROM bit_cambios_gral bit, comcat_pantalla_bitacora pan, "   +
				" 		comrel_nafin n, " + tablaTipoAfiliado + " x " +
				(cmb_afiliado.equals("P") && !claveEpo.equals("") ?", comrel_pyme_epo rel_pyme ":"")+// Fodea 057 - 2010
				"  WHERE bit.cc_pantalla_bitacora = pan.cc_pantalla_bitacora"   +
				" 		AND bit.cg_clave_afiliado = pan.cg_clave_afiliado" + 
				" 		AND bit.cg_clave_afiliado = ? " +
				" 		AND n.ic_nafin_electronico(+) = bit.ic_nafin_electronico "+
				(cmb_afiliado.equals("P") && !claveEpo.equals("")?" AND n.ic_epo_pyme_if = rel_pyme.ic_pyme ":""));// Fodea 057 - 2010
				/*if(cmb_afiliado.equals("GA"))	{
					qrySentencia.append(" 	AND bit.ic_grupo_epo = x." + campoTipoAfiliado + "(+) ");
				} else {
					qrySentencia.append(" 	AND n.ic_epo_pyme_if = x." + campoTipoAfiliado + "(+) ");
				}
				
				if(cmb_afiliado.equals("GA"))	{
					qrySentencia.append(" 	AND bit.ic_grupo_epo = x." + campoTipoAfiliado + "(+) ");
				} else if( (  cmb_afiliado.equals("E") ||  cmb_afiliado.equals("I") ) && cmb_pantalla.equals("CONSUSUARIO") ) {
					qrySentencia.append(" AND bit.ic_epo = x." + campoTipoAfiliado + "(+) ");
				}else {
			   qrySentencia.append(" 	AND n.ic_epo_pyme_if = x." + campoTipoAfiliado + "(+) ");				 
				}
			
			
		try{
			
			if (cmb_afiliado.equals("P"))
				if(!"".equals(rfc_prov)){
					condicion.append("    AND x.cg_rfc = ? ");
				}

    	
			if(!"".equals(txt_ne)){
				condicion.append("    AND bit.ic_nafin_electronico = ?");
			}
				
			if(!"".equals(cmb_pantalla)){
				condicion.append("    AND bit.cc_pantalla_bitacora = ? ");
				
			}else{
				if("B".equals(pantalla)){
					condicion.append("    AND bit.cc_pantalla_bitacora in (?,?,?) ");
				}
			}
			
			if(!"".equals(txt_usuario)){
				condicion.append("    AND bit.ic_usuario = ?");
			}
				
			if(!"".equals(txt_fecha_act_de)&&!"".equals(txt_fecha_act_a)) {
				condicion.append("    AND bit.df_cambio >= TO_DATE (?, 'dd/mm/yyyy')");
				condicion.append("    AND bit.df_cambio < TRUNC (TO_DATE (?, 'dd/mm/yyyy') + 1)");
			}
			if("".equals(condicion.toString())) {
				condicion.append("    AND bit.df_cambio >= TRUNC (SYSDATE)");
				condicion.append("    AND bit.df_cambio < TRUNC (SYSDATE + 1)");
			}
			
			if(!"".equals(cmb_grupo)){
				condicion.append("    AND bit.ic_grupo_epo = ?");
			}

			/*Modificacion FODEA 000 - 2009//**			
			if(!"".equals(noBancoFondeo) ){
				if (cmb_afiliado.equals("E")) { //Cadenas Productivas
					condicion.append("    AND x.ic_banco_fondeo = ? ");
					
				} else if (cmb_afiliado.equals("I")) { //Intermediario Financiero
					condicion.append(
						" AND x.ic_if IN (" +
						" select distinct ie.ic_if " +
						" from comrel_if_epo ie, comcat_epo e " +
						" where ie.ic_epo = e.ic_epo and ie.cs_aceptacion=? " +
						" AND ie.cs_vobo_nafin=? AND ie.cs_bloqueo=? AND e.ic_banco_fondeo=? ) ");
				} else if (cmb_afiliado.equals("P")) {	//Provedores
					condicion.append(
						" AND x.ic_pyme IN (" +
						" select distinct pe.ic_pyme " +
						" from comrel_pyme_epo pe, comcat_epo e " +
						" where pe.ic_epo = e.ic_epo AND e.ic_banco_fondeo=? ) ");
				}
			}

			if(cmb_afiliado.equals("P") && !claveEpo.equals("")){ // Fodea 057 - 2010
				condicion.append(" AND rel_pyme.ic_epo = ? ");
			}
			
			if(cmb_afiliado.equals("P") && !claveEpo.equals("") && !numeroProveedor.equals("")){ // Fodea 057 - 2010
				condicion.append(" AND rel_pyme.cg_pyme_epo_interno = ? ");
			}
			
			qrySentencia.append(condicion.toString());

		} catch(Exception e) {
			log.debug("BitCambiosNafinCA2::getAggregateCalculationQuery "+e);
    	}

		log.debug("getAggregateCalculationQuery)"+qrySentencia.toString());
		log.debug("getAggregateCalculationQuery)"+condicion.toString());
		return qrySentencia.toString();
	}*/

	public String getDocumentSummaryQueryForIds(List pageIds)  {
		int i=0;
		this.variablesBind = new ArrayList();
		StringBuffer qrySentencia = new StringBuffer();
		String camposNuevos = "";
		
		StringBuffer clavesCombinaciones = new StringBuffer("");
		String tablaTipoAfiliado = "";
		String campoTipoAfiliado = "";
		if (cmb_afiliado.equals("E") || cmb_afiliado.equals("EI")) {
			tablaTipoAfiliado = "comcat_epo";
			campoTipoAfiliado = "ic_epo";
		} else if (cmb_afiliado.equals("P") || cmb_afiliado.equals("PI") ||
				cmb_afiliado.equals("D") || cmb_afiliado.equals("C")) {
			tablaTipoAfiliado = "comcat_pyme";
			campoTipoAfiliado = "ic_pyme";
			camposNuevos = ", x.cg_rfc ";
		} else if (cmb_afiliado.equals("DF")) {
			tablaTipoAfiliado = "foncat_distribuidor";
			campoTipoAfiliado = "ic_distribuidor";
		} else if (cmb_afiliado.equals("I") || cmb_afiliado.equals("CON")) {
			tablaTipoAfiliado = "comcat_if";
			campoTipoAfiliado = "ic_if";
		} else if (cmb_afiliado.equals("N")) {  //truco para usuarios Nafin
			tablaTipoAfiliado = "(select 0 as ic_nafin, 'Nacional Financiera' as cg_razon_social from dual)";
			campoTipoAfiliado = "ic_nafin";
		}else if (cmb_afiliado.equals("GA")) {  //truco para usuarios Nafin
			tablaTipoAfiliado = "(select ic_grupo_epo as ic_grupo, cg_descripcion as cg_razon_social from comcat_grupo_epo)";
			campoTipoAfiliado = "ic_grupo";
		}

		/*for (Iterator it = ids.iterator(); it.hasNext();){
			clavesCombinaciones.append("?,");
			this.variablesBind.add(it.next());
			//it.next();
		}*/
		for(int ii=0;ii<pageIds.size();ii++) {
			List lItem = (ArrayList)pageIds.get(ii);
				//if(ii>0){
					clavesCombinaciones.append("?,");
				//}
				this.variablesBind.add(new Long(lItem.get(0).toString()));
		}//for(int i=0;i<ids.size();i++)
		
		clavesCombinaciones = clavesCombinaciones.delete(clavesCombinaciones.length()-1,clavesCombinaciones.length());
		
		qrySentencia.append(
			" SELECT bit.ic_cambio, bit.ic_nafin_electronico, pan.cg_descripcion AS pantalla, x.cg_razon_social, bit.ic_usuario,"   +
			"   bit.cg_nombre_usuario,TO_CHAR (df_cambio, 'dd/mm/yyyy hh24:mi')||'hrs' AS fecha_cambio, bit.cg_anterior, bit.cg_actual "+camposNuevos+
			" FROM bit_cambios_gral bit, comcat_pantalla_bitacora pan,"   +
			" 	comrel_nafin n, " + tablaTipoAfiliado + " x "+
			" WHERE bit.cc_pantalla_bitacora = pan.cc_pantalla_bitacora"   +
			"   AND bit.cg_clave_afiliado = pan.cg_clave_afiliado"   +
			" 	AND n.ic_nafin_electronico(+) = bit.ic_nafin_electronico ");
			
			/*if(cmb_afiliado.equals("GA"))
			{
				qrySentencia.append(" 	AND bit.ic_grupo_epo = x." + campoTipoAfiliado + "(+) " +
				"   AND bit.ic_cambio IN("+clavesCombinaciones+")");
			}else
			{
				qrySentencia.append(" 	AND n.ic_epo_pyme_if = x." + campoTipoAfiliado + "(+) " +
				"   AND bit.ic_cambio IN("+clavesCombinaciones+")");
			}
			*/
			
			if(cmb_afiliado.equals("GA"))	{
					qrySentencia.append(" 	AND bit.ic_grupo_epo = x." + campoTipoAfiliado + "(+) ");
				} else if( (  cmb_afiliado.equals("E") ||  cmb_afiliado.equals("I") ) && cmb_pantalla.equals("CONSUSUARIO") ) {
					qrySentencia.append(" AND bit.ic_epo = x." + campoTipoAfiliado + "(+) ");
				}else {
			   qrySentencia.append(" 	AND n.ic_epo_pyme_if = x." + campoTipoAfiliado + "(+) ");				 
				}
				
				 qrySentencia.append("   AND bit.ic_cambio IN("+clavesCombinaciones+")");
				
			
			
			
			
		qrySentencia.append("   ORDER BY bit.df_cambio DESC");

		log.debug("getDocumentSummaryQueryForIds)"+qrySentencia.toString());
		return qrySentencia.toString();
	}//getDocumentSummaryQueryForIds

	public String getDocumentQuery()	{ //genera todos los id's

    	this.variablesBind = new ArrayList();
		StringBuffer qrySentencia = new StringBuffer();
    	StringBuffer condicion	= new StringBuffer();;

		String tablaTipoAfiliado = "";
		String campoTipoAfiliado = "";
		if (cmb_afiliado.equals("E") || cmb_afiliado.equals("EI")) {
			tablaTipoAfiliado = "comcat_epo";
			campoTipoAfiliado = "ic_epo";
		} else if (cmb_afiliado.equals("P") || cmb_afiliado.equals("PI") ||
				cmb_afiliado.equals("D") || cmb_afiliado.equals("C")) {
			tablaTipoAfiliado = "comcat_pyme";
			campoTipoAfiliado = "ic_pyme";
		} else if (cmb_afiliado.equals("DF")) {
			tablaTipoAfiliado = "foncat_distribuidor";
			campoTipoAfiliado = "ic_distribuidor";
		} else if (cmb_afiliado.equals("I") || cmb_afiliado.equals("CON")) {
			tablaTipoAfiliado = "comcat_if";
			campoTipoAfiliado = "ic_if";
		} else if (cmb_afiliado.equals("N")) {  //truco para usuarios Nafin
			tablaTipoAfiliado = "(select 0 as ic_nafin, 'Nacional Financiera' as cg_razon_social from dual)";
			campoTipoAfiliado = "ic_nafin";
		}else if (cmb_afiliado.equals("GA")) {  //truco para usuarios Nafin
			tablaTipoAfiliado = "(select ic_grupo_epo as ic_grupo, cg_descripcion as cg_razon_social from comcat_grupo_epo)";
			campoTipoAfiliado = "ic_grupo";
		}

		qrySentencia.append(
				" 	SELECT bit.ic_cambio"   +
				"  FROM bit_cambios_gral bit, comcat_pantalla_bitacora pan, "   +
				" 		comrel_nafin n, " + tablaTipoAfiliado + " x " +
				(cmb_afiliado.equals("P") && !claveEpo.equals("")?", comrel_pyme_epo rel_pyme ":"")+// Fodea 057 - 2010
				"  WHERE bit.cc_pantalla_bitacora = pan.cc_pantalla_bitacora"   +
				" 		AND bit.cg_clave_afiliado = pan.cg_clave_afiliado" + 
				" 		AND bit.cg_clave_afiliado = ? " +
				" 		AND n.ic_nafin_electronico(+) = bit.ic_nafin_electronico "+
				(cmb_afiliado.equals("P") && !claveEpo.equals("")?" AND n.ic_epo_pyme_if = rel_pyme.ic_pyme ":""));// Fodea 057 - 2010
			
				/*if(cmb_afiliado.equals("GA"))	{
					qrySentencia.append(" 	AND bit.ic_grupo_epo = x." + campoTipoAfiliado + "(+) ");
				} else {
					qrySentencia.append(" 	AND n.ic_epo_pyme_if = x." + campoTipoAfiliado + "(+) ");
				}*/
				
				if(cmb_afiliado.equals("GA"))	{
					qrySentencia.append(" 	AND bit.ic_grupo_epo = x." + campoTipoAfiliado + "(+) ");
				} else if( (  cmb_afiliado.equals("E") ||  cmb_afiliado.equals("I") ) && cmb_pantalla.equals("CONSUSUARIO") ) {			
					qrySentencia.append(" AND bit.ic_epo = x." + campoTipoAfiliado + "(+) ");				
				}else {
			   qrySentencia.append(" 	AND n.ic_epo_pyme_if = x." + campoTipoAfiliado + "(+) ");	
				}
				
			this.variablesBind.add(cmb_afiliado);
			
		try{
			
			if (cmb_afiliado.equals("P"))
				if(!"".equals(rfc_prov)){
					condicion.append("    AND x.cg_rfc = ? ");
					this.variablesBind.add(rfc_prov);
				}
 
			if(!"".equals(txt_ne)){
				condicion.append("    AND bit.ic_nafin_electronico = ?");
				this.variablesBind.add(new Integer(txt_ne));
			}
				
			if(!"".equals(cmb_pantalla)){
				condicion.append("    AND bit.cc_pantalla_bitacora = ? ");
				this.variablesBind.add(cmb_pantalla);
			}else{
				if("B".equals(pantalla)){
					condicion.append("    AND bit.cc_pantalla_bitacora in (?,?,?) ");
					this.variablesBind.add("REGAFILMAS");
					this.variablesBind.add("REGAFIL");
					this.variablesBind.add("CONSAFIL");
				}
			}
			
			if(!"".equals(txt_usuario)){
				condicion.append("    AND bit.ic_usuario = ?");
				this.variablesBind.add(txt_usuario);
			}
				
			if(!"".equals(txt_fecha_act_de)&&!"".equals(txt_fecha_act_a)) {
				condicion.append("    AND bit.df_cambio >= TO_DATE (?, 'dd/mm/yyyy')");
				condicion.append("    AND bit.df_cambio < TRUNC (TO_DATE (?, 'dd/mm/yyyy') + 1)");
				this.variablesBind.add(txt_fecha_act_de);
				this.variablesBind.add(txt_fecha_act_a);
			}
			if("".equals(condicion.toString())) {
				condicion.append("    AND bit.df_cambio >= TRUNC (SYSDATE)");
				condicion.append("    AND bit.df_cambio < TRUNC (SYSDATE + 1)");
			}
			
			if(!"".equals(cmb_grupo)){
				condicion.append("    AND bit.ic_grupo_epo = ?");
				this.variablesBind.add(new Integer(cmb_grupo));
			}

			/*Modificacion FODEA 000 - 2009*/			
			if(!"".equals(noBancoFondeo) ){
				if (cmb_afiliado.equals("E")) { //Cadenas Productivas
					condicion.append("    AND x.ic_banco_fondeo = ? ");
					this.variablesBind.add(new Integer(noBancoFondeo));
				} else if (cmb_afiliado.equals("I")) { //Intermediario Financiero
					condicion.append(
						" AND x.ic_if IN (" +
						" select distinct ie.ic_if " +
						" from comrel_if_epo ie, comcat_epo e " +
						" where ie.ic_epo = e.ic_epo and ie.cs_aceptacion=? " +
						" AND ie.cs_vobo_nafin=? AND ie.cs_bloqueo=? AND e.ic_banco_fondeo=? ) ");
						this.variablesBind.add("S");
						this.variablesBind.add("S");
						this.variablesBind.add("N");
						this.variablesBind.add(new Integer(noBancoFondeo));
				} else if (cmb_afiliado.equals("P")) {	//Provedores
					condicion.append(
						" AND x.ic_pyme IN (" +
						" select distinct pe.ic_pyme " +
						" from comrel_pyme_epo pe, comcat_epo e " +
						" where pe.ic_epo = e.ic_epo AND e.ic_banco_fondeo=? ) ");
						this.variablesBind.add(new Integer(noBancoFondeo));
				}
			}

			if(cmb_afiliado.equals("P") && !claveEpo.equals("")){ // Fodea 057 - 2010
				condicion.append(" AND rel_pyme.ic_epo = ? ");
				this.variablesBind.add(new Integer(claveEpo));
			}
			
			if(cmb_afiliado.equals("P") && !claveEpo.equals("") && !numeroProveedor.equals("")){ // Fodea 057 - 2010
				condicion.append(" AND rel_pyme.cg_pyme_epo_interno = ? ");
				this.variablesBind.add(numeroProveedor);
			}
			
			qrySentencia.append(condicion.toString() +
						"   ORDER BY bit.df_cambio DESC");

		} catch(Exception e) {
			log.debug("BitCambiosNafinCA2::getDocumentQueryException "+e);
    	}

		log.debug("getDocumentQuery)"+qrySentencia.toString());
		return qrySentencia.toString();
	}//getDocumentQuery

	public String getDocumentQueryFile() {
    	this.variablesBind = new ArrayList();
		
		StringBuffer 	qrySentencia 	= new StringBuffer();
    	StringBuffer	condicion 		= new StringBuffer();
		String 			camposNuevos 	= "";

		String tablaTipoAfiliado = "";
		String campoTipoAfiliado = "";
		if (cmb_afiliado.equals("E") || cmb_afiliado.equals("EI")) {
			tablaTipoAfiliado = "comcat_epo";
			campoTipoAfiliado = "ic_epo";
		} else if (cmb_afiliado.equals("P") || cmb_afiliado.equals("PI") ||
				cmb_afiliado.equals("D") || cmb_afiliado.equals("C")) {
			tablaTipoAfiliado = "comcat_pyme";
			campoTipoAfiliado = "ic_pyme";
			camposNuevos = ", x.cg_rfc ";
		} else if (cmb_afiliado.equals("DF")) {
			tablaTipoAfiliado = "foncat_distribuidor";
			campoTipoAfiliado = "ic_distribuidor";
		} else if (cmb_afiliado.equals("I") || cmb_afiliado.equals("CON")) {
			tablaTipoAfiliado = "comcat_if";
			campoTipoAfiliado = "ic_if";
		} else if (cmb_afiliado.equals("N")) {  //truco para usuarios Nafin
			tablaTipoAfiliado = "(select 0 as ic_nafin, 'Nacional Financiera' as cg_razon_social from dual)";
			campoTipoAfiliado = "ic_nafin";
		}else if (cmb_afiliado.equals("GA")) {  //truco para usuarios Nafin
			tablaTipoAfiliado = "(select ic_grupo_epo as ic_grupo, cg_descripcion as cg_razon_social from comcat_grupo_epo)";
			campoTipoAfiliado = "ic_grupo";
		}

		qrySentencia.append(
				" SELECT bit.ic_cambio, bit.ic_nafin_electronico, pan.cg_descripcion AS pantalla, x.cg_razon_social, bit.ic_usuario,"   +
				"        bit.cg_nombre_usuario,TO_CHAR (df_cambio, 'dd/mm/yyyy hh24:mi')||'hrs' AS fecha_cambio, bit.cg_anterior, bit.cg_actual "+camposNuevos+
				"   FROM bit_cambios_gral bit, comcat_pantalla_bitacora pan, "   +
				" 	comrel_nafin n, " + tablaTipoAfiliado + " x "+
				(cmb_afiliado.equals("P") && !claveEpo.equals("") ?", comrel_pyme_epo rel_pyme ":"")+// Fodea 057 - 2010
				"  WHERE bit.cc_pantalla_bitacora = pan.cc_pantalla_bitacora"   +
				"    AND bit.cg_clave_afiliado = pan.cg_clave_afiliado" +
				" 	 AND n.ic_nafin_electronico(+) = bit.ic_nafin_electronico "+
				(cmb_afiliado.equals("P") && !claveEpo.equals("")?" AND n.ic_epo_pyme_if = rel_pyme.ic_pyme ":""));// Fodea 057 - 2010
			
			/*	if(cmb_afiliado.equals("GA"))
				{
					qrySentencia.append(" 	AND bit.ic_grupo_epo = x." + campoTipoAfiliado + "(+) ");
				}else
				{
					qrySentencia.append(" 	 AND n.ic_epo_pyme_if = x." + campoTipoAfiliado + "(+) ");
				}
				*/
				
				if(cmb_afiliado.equals("GA"))	{
					qrySentencia.append(" 	AND bit.ic_grupo_epo = x." + campoTipoAfiliado + "(+) ");
				} else if( (  cmb_afiliado.equals("E") ||  cmb_afiliado.equals("I") ) && cmb_pantalla.equals("CONSUSUARIO") ) {			
					qrySentencia.append(" AND bit.ic_epo = x." + campoTipoAfiliado + "(+) ");
				}else {
			   qrySentencia.append(" 	AND n.ic_epo_pyme_if = x." + campoTipoAfiliado + "(+) ");				 
				}
				
		this.variablesBind.add(cmb_afiliado);

		try {
			 
			 if (cmb_afiliado.equals("P"))
			 	if(!"".equals(rfc_prov)){
					condicion.append("    AND x.cg_rfc = ? ");
					this.variablesBind.add(rfc_prov);
				}
			
	  
			if(!"".equals(txt_ne)){
				condicion.append("    AND bit.ic_nafin_electronico = ?");
				this.variablesBind.add(txt_ne);
			}
				
			if(!"".equals(cmb_pantalla)){
				condicion.append("    AND bit.cc_pantalla_bitacora = ?");
				this.variablesBind.add(cmb_pantalla);
			}else{
				if("B".equals(pantalla)){
					condicion.append("    AND bit.cc_pantalla_bitacora in (?,?,?) ");
					this.variablesBind.add("REGAFILMAS");
					this.variablesBind.add("REGAFIL");
					this.variablesBind.add("CONSAFIL");
				}
			}
				
			if(!"".equals(txt_usuario)){
				condicion.append("    AND bit.ic_usuario = ?");
				this.variablesBind.add(txt_usuario);
				
			}
				
			if(!"".equals(txt_fecha_act_de)&&!"".equals(txt_fecha_act_a)) {
				condicion.append("    AND bit.df_cambio >= TO_DATE (?, 'dd/mm/yyyy')");
				condicion.append("    AND bit.df_cambio < TRUNC (TO_DATE (?, 'dd/mm/yyyy') + 1)");
				this.variablesBind.add(txt_fecha_act_de);
				this.variablesBind.add(txt_fecha_act_a);
			}
			if("".equals(condicion.toString())) {
				condicion.append("    AND bit.df_cambio >= TRUNC (SYSDATE)");
				condicion.append("    AND bit.df_cambio < TRUNC (SYSDATE + 1)");
			}
			if(!"".equals(cmb_grupo)){
					condicion.append("    AND bit.ic_grupo_epo = ?");
					this.variablesBind.add(cmb_grupo);
			}
			
			/*Modificacion FODEA 000 - 2009*/			
			if(!"".equals(noBancoFondeo) ){
				if (cmb_afiliado.equals("E")) { //Cadena Productiva
					condicion.append("    AND x.ic_banco_fondeo = ? ");
					this.variablesBind.add(new Integer(noBancoFondeo));
				} else if (cmb_afiliado.equals("I")) { //Intermediario Financiero
					condicion.append(
						" AND x.ic_if IN (" +
						" select distinct ie.ic_if " +
						" from comrel_if_epo ie, comcat_epo e " +
						" where ie.ic_epo = e.ic_epo and ie.cs_aceptacion=? " +
						" AND ie.cs_vobo_nafin=? AND ie.cs_bloqueo=? AND e.ic_banco_fondeo=? ) ");
						this.variablesBind.add("S");
						this.variablesBind.add("S");
						this.variablesBind.add("N");
						this.variablesBind.add(new Integer(noBancoFondeo));
				} else if (cmb_afiliado.equals("P")) { //Proveedor
				//caso para Pyme
					condicion.append(
						" AND x.ic_pyme IN (" +
						" select distinct pe.ic_pyme " +
						" from comrel_pyme_epo pe, comcat_epo e " +
						" where pe.ic_epo = e.ic_epo AND e.ic_banco_fondeo=? ) ");
						this.variablesBind.add(new Integer(noBancoFondeo));
				}
			}
				
			if(cmb_afiliado.equals("P") && !claveEpo.equals("")){ // Fodea 057 - 2010
				condicion.append(" AND rel_pyme.ic_epo = ? ");
				this.variablesBind.add(new Integer(claveEpo));
			}
			
			if(cmb_afiliado.equals("P") && !claveEpo.equals("") && !numeroProveedor.equals("")){ // Fodea 057 - 2010
				condicion.append(" AND rel_pyme.cg_pyme_epo_interno = ? ");
				this.variablesBind.add(numeroProveedor);
			}
			
			qrySentencia.append(
				"    AND bit.cg_clave_afiliado = ? "+ condicion.toString() +
				"   ORDER BY bit.df_cambio DESC");

	    }catch(Exception e){
			log.debug("BitCambiosNafinCA2::getDocumentQueryFileException "+e);
		}
		log.debug("getDocumentQueryFile)"+qrySentencia.toString());
		return qrySentencia.toString();
	}//getDocumentQueryFile

  	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String nombreArchivo	=	"";
		String numnafinelect,razonsocial,rfc,pantalla,ic_usuario,usuario,fechaCambio,rs_anterior,rs_actual;
		if ("CSV".equals(tipo)) {
			HttpSession session = request.getSession();
			try{
				StringBuffer linea = new StringBuffer();
				OutputStreamWriter writer = null;
				BufferedWriter buffer = null;
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
				buffer = new BufferedWriter(writer);
				linea.append(
					"Num. Nafin Electronico,"+
					"Pantalla,");
					if(this.cmb_afiliado.equals("GA"))
					{
						linea.append("Grupo,");
					}else
					{
						linea.append("Raz�n Social,");
					}
					if ("EPO".equals((String)session.getAttribute("strTipoUsuario"))){
						linea.append("R.F.C.,");
					}
					linea.append(	
					(
						!"EPO".equals((String)session.getAttribute("strTipoUsuario"))
						?
							"Usuario - Modifico,"+
							"Nombre de Usuario,"
						:
							""
					) +
					"Fecha de Actualizaci�n,"+
					"Datos Anteriores,"+
					"Datos Nuevos");
				//linea = "Num Nafin Electr�nico,Pantalla,"+razonsocial+",RFC,Domicilio,Estado,Tel�fono,\n";
				buffer.write(linea.toString());
				while (rs.next()) {
					numnafinelect	=	(rs.getString("IC_NAFIN_ELECTRONICO")==null)?"":rs.getString("IC_NAFIN_ELECTRONICO");
					pantalla			=	(rs.getString("PANTALLA")==null)?"":rs.getString("PANTALLA");
					razonsocial		=	(rs.getString("CG_RAZON_SOCIAL")==null)?"":rs.getString("CG_RAZON_SOCIAL");
					rfc				=	(rs.getString("CG_RFC")==null)?"":rs.getString("CG_RFC");
					ic_usuario		=	(rs.getString("IC_USUARIO")==null)?"":rs.getString("IC_USUARIO");
					usuario			=	(rs.getString("CG_NOMBRE_USUARIO")==null)?"":rs.getString("CG_NOMBRE_USUARIO");
					fechaCambio		=	(rs.getString("FECHA_CAMBIO")==null)?"":rs.getString("FECHA_CAMBIO");
					rs_anterior			=	(rs.getString("CG_ANTERIOR")==null)?"":rs.getString("CG_ANTERIOR");
					rs_actual			=	(rs.getString("CG_ACTUAL")==null)?"":rs.getString("CG_ACTUAL");
					String anterior= rs_anterior.replace(',',' ');
					String actual = rs_actual.replace(',',' ');
					linea = new StringBuffer();
				 linea.append("\n"+
							numnafinelect+","+
							pantalla+","+
							razonsocial.replace(',',' ')+","+
							(
								"EPO".equals((String)session.getAttribute("strTipoUsuario"))
								?
									rfc + ","
								:
									""
							)+
							( 	!"EPO".equals((String)session.getAttribute("strTipoUsuario"))
								?
									ic_usuario+","+
									usuario.replace(',',' ')+","
								: 
									""
							)+
							fechaCambio+","+
							anterior.replace('\n', '|')+","+
							actual.replace('\n', '|'));
					//linea = numnafinelect.replace(',',' ') + "," + razonsocial.replace(',',' ') + "," + rfc.replace(',',' ') + "," + calle.replace(',',' ') + "," + estado.replace(',',' ') + "," + telefono.replace(',',' ') + ",\n";
					buffer.write(linea.toString());
				}
				buffer.close();
			}catch (Exception e) {
					throw new AppException("Error al generar el archivo", e);
			}	finally {
				}
		}
		return nombreArchivo;
	}

	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		String nombreArchivo	=	"";
		String numnafinelect,razonsocial,rfc,pantalla,ic_usuario,usuario,fechaCambio,anterior,actual;
		if ("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				int nRow = 0;
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				while (reg.next()) {
					numnafinelect	=	(reg.getString("IC_NAFIN_ELECTRONICO")==null)?"":reg.getString("IC_NAFIN_ELECTRONICO");
					pantalla			=	(reg.getString("PANTALLA")==null)?"":reg.getString("PANTALLA");
					razonsocial		=	(reg.getString("CG_RAZON_SOCIAL")==null)?"":reg.getString("CG_RAZON_SOCIAL");
					rfc				=	(reg.getString("CG_RFC")==null)?"":reg.getString("CG_RFC");
					ic_usuario		=	(reg.getString("IC_USUARIO")==null)?"":reg.getString("IC_USUARIO");
					usuario			=	(reg.getString("CG_NOMBRE_USUARIO")==null)?"":reg.getString("CG_NOMBRE_USUARIO");
					fechaCambio		=	(reg.getString("FECHA_CAMBIO")==null)?"":reg.getString("FECHA_CAMBIO");
					anterior			=	(reg.getString("CG_ANTERIOR")==null)?"":reg.getString("CG_ANTERIOR");
					actual			=	(reg.getString("CG_ACTUAL")==null)?"":reg.getString("CG_ACTUAL");
					if (nRow == 0){
						String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
						String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
						String diaActual    = fechaActual.substring(0,2);
						String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
						String anioActual   = fechaActual.substring(6,10);
						String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

						if ( !"NAFIN".equals( (String)session.getAttribute("strPerfil") ) ) {
							pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
							session.getAttribute("iNoNafinElectronico").toString(),
							(String)session.getAttribute("sesExterno"),
							(String) session.getAttribute("strNombre"),
							(String) session.getAttribute("strNombreUsuario"),
							(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
						}
						pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
						pdfDoc.addText(" ","formas", ComunesPDF.CENTER);
						pdfDoc.addText("Bit�cora de Movimientos de Proveedores","formas",ComunesPDF.CENTER);
						List lEncabezados = new ArrayList();
						lEncabezados.add("Num. Naf�n Electr�nico");
						lEncabezados.add("Pantalla");
						
						if (this.cmb_afiliado.equals("GA")){
							lEncabezados.add("Grupo");
						}else{
							lEncabezados.add("Raz�n Social");
						}
						if("EPO".equals((String)session.getAttribute("strTipoUsuario"))) {
							lEncabezados.add("RFC");
						}
						if(!"EPO".equals((String)session.getAttribute("strTipoUsuario"))) {
							lEncabezados.add("Usuario - Modifico");
							lEncabezados.add("Nombre de Usuario");
						}
						lEncabezados.add("Fecha de Actualizaci�n");
						lEncabezados.add("Datos Anteriores");
						lEncabezados.add("Datos Nuevos");

						pdfDoc.setTable(lEncabezados,"celda01rep",100);
						
					}
					pdfDoc.setCell(numnafinelect, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(pantalla, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(razonsocial, "formasmen", ComunesPDF.CENTER);
					
					if("EPO".equals((String)session.getAttribute("strTipoUsuario"))) {
						pdfDoc.setCell(rfc, "formasmen", ComunesPDF.CENTER);
					}

					if(!"EPO".equals((String)session.getAttribute("strTipoUsuario"))) {
						pdfDoc.setCell(ic_usuario, "formasmen", ComunesPDF.CENTER);
						pdfDoc.setCell(usuario, "formasmen", ComunesPDF.CENTER);
					}
					pdfDoc.setCell(fechaCambio, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(anterior, "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell(actual, "formasmen", ComunesPDF.CENTER);
					nRow++;
				}
				if (nRow == 0)	{
					pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
					pdfDoc.addText("No se Encontro Ning�n Registro","formas",ComunesPDF.LEFT);
				}else	{
					pdfDoc.addTable();
				}
				pdfDoc.endDocument();
			}catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			}finally {
			}
		}
		return nombreArchivo;
	}

	public List getConditions() {
		log.debug("*************getConditions=" + variablesBind);
		return variablesBind;
	}
	private ArrayList variablesBind = null;

	public void setNoNafin(String txt_ne) {this.txt_ne = txt_ne;}
	public void setCmbPantalla(String cmb_pantalla) {this.cmb_pantalla = cmb_pantalla;}
	public void setPantalla(String pantalla) {this.pantalla = pantalla;}
	public void setUsuario(String txt_usuario) {this.txt_usuario = txt_usuario;}
	public void setFechaInicial(String txt_fecha_act_de) {this.txt_fecha_act_de = txt_fecha_act_de;}
	public void setFechaFinal(String txt_fecha_act_a) {this.txt_fecha_act_a = txt_fecha_act_a;}
	public void setRfc(String rfc_prov) {this.rfc_prov = rfc_prov;}
	public void setNoProveedor(String noProv){this.numeroProveedor = noProv;}
	public void setClaveEpo(String cve_epo){this.claveEpo = cve_epo;}
	
	public void setClaveAfiliado(String cmb_afiliado){this.cmb_afiliado = cmb_afiliado;}
	public void setGrupo(String cmb_grupo){this.cmb_grupo = cmb_grupo;}
	public void setBancoFondeo(String noBancoFondeo){this.noBancoFondeo = noBancoFondeo;}

}//BitCambiosNafinCA2