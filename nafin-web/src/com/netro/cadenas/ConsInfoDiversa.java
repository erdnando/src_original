package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsInfoDiversa implements IQueryGeneratorRegExtJS{
	public ConsInfoDiversa() {	 }
	
	private List conditions;
	StringBuffer strQuery;
	
	private static final Log log = ServiceLocator.getInstance().getLog(ConsInfoDiversa.class);
	
	private String claveEPO;
	private String clavePyme;
	private String tipoUsuario;
	
	public String getAggregateCalculationQuery() {
		return "";
	}
	
	public String getDocumentQuery() {
		conditions	=	new ArrayList();
		strQuery		=	new StringBuffer();
		return strQuery.toString();
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
		conditions	=	new ArrayList();
		strQuery		=	new StringBuffer();
		return strQuery.toString();
	}
	
	public String getDocumentQueryFile() {
		conditions	=	new ArrayList();
		strQuery		=	new StringBuffer();
		
		log.debug("ic_epo"+this.claveEPO);
		
		if(this.tipoUsuario.equals("NAFIN")) {
			strQuery.append(
				"SELECT DISTINCT(ic_publicacion) AS ic_publicacion," +
					" ic_tipo_publicacion,cg_nombre," +
					" TO_CHAR(df_ini_pub, 'dd/mm/yyyy') AS fch_ini," +
					" TO_CHAR(df_fin_pub, 'dd/mm/yyyy') AS fch_fin" +
				" FROM com_publicacion WHERE ic_epo = ?" +
            " ORDER BY ic_publicacion");	
			 conditions.add(this.claveEPO);
		}
		else{
			strQuery.append(
				"SELECT pub.ic_publicacion" +
					", pub.ic_tipo_publicacion" +
					", pub.cg_nombre" +
					", TO_CHAR(pub.df_ini_pub, 'dd/mm/yyyy') AS fch_ini" +
					", TO_CHAR(pub.df_fin_pub, 'dd/mm/yyyy') AS fch_fin" +
				" FROM com_publicacion pub" +
					", comrel_publicacion_usuario usr_pub" +
					", comrel_nafin cn" +
				" WHERE usr_pub.ic_publicacion = pub.ic_publicacion" +
					" AND usr_pub.ic_nafin_electronico = cn.ic_nafin_electronico" +
					" AND cn.ic_epo_pyme_if = ?" +
					" AND cn.cg_tipo = 'P'" +
					" AND SYSDATE BETWEEN df_ini_pub AND df_fin_pub" +
				" ORDER BY pub.ic_publicacion");	
			conditions.add(this.clavePyme);		
		}
		
		log.debug("getDocumentQueryFile "+strQuery.toString());
		log.debug("getDocumentQueryFile "+conditions);
		
		return strQuery.toString();
	}
	
	/**
	 * En este m�todo de debe realizar la implementaci�n de la generaci�n del archivo
	 * con base en el resulset que se recibe como par�metro. El ResulSet es el resultado
	 * de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResulSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta f�sica donde se generar� el archivo 
	 * @param tipo cadena que identifica el tipo o variante de archivo que va a generar.
	 * @return Cadena con la ruta del archivo generado
	*/
	public String crearCustomFile (HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String nombreArchivo = "";
		
		if("PDF".equals(tipo)){
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[]	=	{"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	=	new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual	=	fechaActual.substring(0,2);
				String mesActual	=	meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual	=	fechaActual.substring(6,10);
				String horaActual	=	new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),
				(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + "------------------------------" +
				horaActual, "formas", ComunesPDF.RIGHT);
				pdfDoc.setTable(3, 100);
				pdfDoc.setCell("Nombre","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Vigencia de", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Vigencia hasta","celda01",ComunesPDF.CENTER);
				
				while (rs.next()){
					String CG_NOMBRE = (rs.getString("CG_NOMBRE") == null) ? "" : rs.getString("CG_NOMBRE");
					String FCH_INI = (rs.getString("FCH_INI") == null) ? "" : rs.getString("FCH_INI");
					String FCH_FIN = (rs.getString("FCH_FIN") == null) ? "" : rs.getString("FCH_FIN");
					
					pdfDoc.setCell(CG_NOMBRE,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(FCH_INI,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(FCH_FIN,"formas",ComunesPDF.CENTER);
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();
				
			}catch(Throwable e) {
				throw new AppException("Error al generar el archivo");
			}finally {
				try{
					rs.close();
				}catch(Exception e){}
			}
		}
		return nombreArchivo;
	}
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	public List getConditions() {
		return conditions;
		}
	public String getClaveEPO() {
		return claveEPO;
		}
	public void setClaveEPO(String claveEPO){
		this.claveEPO	=	claveEPO;
	}
	public String getClavePyme() {
		return clavePyme;
	}
	public void setClavePyme(String clavePyme) {
		this.clavePyme	=	clavePyme;
	}
	public String getTipoUsuario() {
		return tipoUsuario;
	}
	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}
}