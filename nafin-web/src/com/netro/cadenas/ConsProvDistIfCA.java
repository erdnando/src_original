package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorPS;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsProvDistIfCA implements IQueryGeneratorPS, IQueryGeneratorRegExtJS {	
	private int numList = 1;
	private String suceptibleFloating;
	
	public ConsProvDistIfCA() {}
  
	public ArrayList getConditions(HttpServletRequest request){
		/* Establece el orden en que son pasados las variables bind */
		ArrayList conditions = new ArrayList();    
		String 	sTipoAfiliado   = ( request.getParameter("sTipoAfiliado") == null ) ? "1" : request.getParameter("sTipoAfiliado");
		String 	sNombre   		= ( request.getParameter("sNombre") == null ) ? "" : request.getParameter("sNombre");
		String 	sPYME   		= ( request.getParameter("sPYME") == null ) ? "" : request.getParameter("sPYME");
		String 	sIcEdo   		= ( request.getParameter("sIcEdo") == null ) ? "" : request.getParameter("sIcEdo");
		String 	sRFC   			= ( request.getParameter("sRFC") == null ) ? "" : request.getParameter("sRFC");
		String 	sIcEpo   		= ( request.getParameter("sIcEpo") == null ) ? "" : request.getParameter("sIcEpo");
		String 	ses_ic_if 		= (String)request.getSession().getAttribute("iNoCliente");
		if (sTipoAfiliado.equals("1"))
		{ // Proveedores
			conditions.add(ses_ic_if);
			conditions.add(sTipoAfiliado);
			conditions.add(sTipoAfiliado);
			if(!"".equals(sPYME))
				conditions.add(new Integer(sPYME));
			if(!"".equals(sRFC))
				conditions.add(sRFC);
			if(!"".equals(sIcEdo))
				conditions.add(new Integer(sIcEdo));
			if(!"".equals(sIcEpo))
				conditions.add(new Integer(sIcEpo));
			if(!"".equals(sNombre))
				conditions.add("%"+sNombre+"%");
		} else {
		  // Distribuidores
			conditions.add(ses_ic_if);
			conditions.add(ses_ic_if);
			conditions.add(sTipoAfiliado);
			conditions.add(sTipoAfiliado);
			if(!"".equals(sPYME))
				conditions.add(new Integer(sPYME));
			if(!"".equals(sRFC))
				conditions.add(sRFC);
			if(!"".equals(sIcEdo))
				conditions.add(new Integer(sIcEdo));
			if(!"".equals(sIcEpo))
				conditions.add(new Integer(sIcEpo));	
			if(!"".equals(sNombre))
				conditions.add("%"+sNombre+"%");        
		}
    	return conditions;
  	} // Fin del m�todo getConditions


	public String getAggregateCalculationQuery(HttpServletRequest request){
	  // Obtiene la informaci�n de la zona de totales (En este caso no se utiliza)	
		String 	sTipoAfiliado   = ( request.getParameter("sTipoAfiliado") == null ) ? "" : request.getParameter("sTipoAfiliado");
		String 	sNombre   		= ( request.getParameter("sNombre") == null ) ? "" : request.getParameter("sNombre");
		String 	sPYME   		= ( request.getParameter("sPYME") == null ) ? "" : request.getParameter("sPYME");
		String 	sIcEdo   		= ( request.getParameter("sIcEdo") == null ) ? "" : request.getParameter("sIcEdo");
		String 	sRFC   			= ( request.getParameter("sRFC") == null ) ? "" : request.getParameter("sRFC");
		String 	sIcEpo   		= ( request.getParameter("sIcEpo") == null ) ? "" : request.getParameter("sIcEpo");
		StringBuffer strQuery = new StringBuffer();	
		/*if (!sTipoAfiliado.equals("1"))
		{ // Distribuidores
			strQuery.append(",? ");
		}	
		if(!"".equals(sPYME))
			strQuery.append(",? ");
		if(!"".equals(sRFC))
			strQuery.append(",? ");
		if(!"".equals(sIcEdo))
			strQuery.append(",? ");
		if(!"".equals(sIcEpo))
			strQuery.append(",? ");
		if(!"".equals(sNombre))
			strQuery.append(",? ");
		strQuery.append(" from dual ");*/
		
		if (sTipoAfiliado.equals("1"))
		{ strQuery.append(" SELECT count(1) from( "   +
				" SELECT 1"+
				" FROM 	"   +
				" 	comrel_pyme_if pi,"   +
				" 	comrel_cuenta_bancaria cb,"   +
				" 	comcat_pyme p,"   +
				" 	com_contacto c,"   +
				" 	com_domicilio d,"   +
				" 	comcat_epo e,"   +
				" 	comcat_estado edo"   +
				" WHERE "   +
				" 	 p.ic_pyme = cb.ic_pyme and"   +
				"   	 p.ic_pyme = d.ic_pyme  and"   +
				" 	 p.ic_pyme = c.ic_pyme(+)  and"   +
				" 	 c.cs_primer_contacto(+) = 'S' and"   +
				" 	 pi.ic_cuenta_bancaria = cb.ic_cuenta_bancaria and"   +
				" 	 pi.ic_epo = e.ic_epo  	and"   +
				" 	 d.ic_estado = edo.ic_estado and"   +
				" 	 d.cs_fiscal = 'S'      and  "   +
				"   e.cs_habilitado = 'S' and "  +
				" 	 pi.cs_borrado = 'N'	and"   +
				" 	 cb.cs_borrado = 'N'    and"   +
				" 	 pi.ic_if = ?		and"   +
				" 	 decode(p.ic_tipo_cliente,4,?,p.ic_tipo_cliente) = ?  ");
		  if(!"".equals(sPYME))
			strQuery.append(" 	 and p.ic_pyme = ? ");
		  if(!"".equals(sRFC))
			strQuery.append(" 	 and p.cg_rfc = ? ");
		  if(!"".equals(sIcEdo))
			strQuery.append(" 	 and d.ic_estado = ? ");
		  if(!"".equals(sIcEpo))
			strQuery.append(" 	 and pi.ic_epo = ? ");
		  if(!"".equals(sNombre))
			strQuery.append(" 	 and p.cg_razon_social like ? ");
		  strQuery.append(" GROUP BY"   +
				"   p.ic_pyme,e.ic_epo,"   +
				" 	e.cg_razon_social,"   +
				" 	p.cg_razon_social,"   +
				" 	p.cg_rfc,"   +
				" 	d.cg_calle||' '||d.cg_numero_ext||' '||d.cg_numero_int||' COL. '||d.cg_colonia,"   +
				" 	edo.cd_nombre,"   +
				" 	d.cg_telefono1,"   +
				" 	d.cg_email,"   +
				" 	c.cg_appat||' '||c.cg_apmat||' '||c.cg_nombre, 'ConsProvDistIfCA:getDocumentQueryFile'"   +
				" order by e.cg_razon_social,	p.cg_razon_social)	");
		} else {
		  strQuery.append("SELECT count(1) from( " +
				" SELECT 1"+   
				"  FROM "   +
				"  	  comrel_cuenta_bancaria_x_prod cbp, "   +
				" 	  comcat_pyme p,	  "   +
				" 	  com_cuenta_aut_x_prod aut, "   +
				" 	  com_contacto c,"   +
				" 	  com_domicilio d,"   +
				" 	  comcat_epo e,"   +
				" 	  comcat_estado edo"   +
				"  WHERE "   +
				" 	  cbp.ic_pyme = p.ic_pyme AND "   +
				"  	  cbp.ic_cuenta_bancaria = aut.ic_cuenta_bancaria(+) AND "   +
				" 	  cbp.ic_epo = e.ic_epo AND"   +
				"   	  p.ic_pyme = d.ic_pyme  and"   +
				" 	 p.ic_pyme = c.ic_pyme(+)  and"   +
				" 	 c.cs_primer_contacto(+) = 'S' and"   +
				" 	 d.ic_estado = edo.ic_estado and"   +
				" 	  cbp.ic_if=? AND"   +
				" 	 d.cs_fiscal = 'S'      and  "   +
				"   e.cs_habilitado = 'S' and " +
				" 	 aut.cs_borrado(+) = 'N'	and"   +
				" 	 aut.IC_EPO(+) = cbp.ic_epo and"   +
				" 	 aut.IC_IF(+) = ? and "   +
				" 	 cbp.cs_borrado = 'N'    and"   +
				" 	 decode(p.ic_tipo_cliente,4,?,p.ic_tipo_cliente) = ?  and"   +
				"      cbp.ic_producto_nafin = 4 ");
		  if(!"".equals(sPYME))
			strQuery.append(" 	 and p.ic_pyme = ? ");
		  if(!"".equals(sRFC))
			strQuery.append(" 	 and p.cg_rfc = ? ");
		  if(!"".equals(sIcEdo))
			strQuery.append(" 	 and d.ic_estado = ? ");
		  if(!"".equals(sIcEpo))
			strQuery.append(" 	 and cbp.ic_epo = ? ");
		  if(!"".equals(sNombre))
			strQuery.append(" 	 and p.cg_razon_social like ? ");
		  strQuery.append(" group by"   +
					"     p.ic_pyme,"   +
					" 	e.ic_epo,"   +
					" 	e.cg_razon_social,"   +
					" 	p.cg_razon_social,"   +
					" 	p.cg_rfc,"   +
					" 	d.cg_calle||' '||d.cg_numero_ext||' '||d.cg_numero_int||' COL. '||d.cg_colonia,"   +
					" 	edo.cd_nombre,"   +
					" 	d.cg_telefono1,"   +
					" 	d.cg_email,"   +
					" 	c.cg_appat||' '||c.cg_apmat||' '||c.cg_nombre, 'ConsProvDistIfCA:getDocumentQueryFile'"   +
					" order by e.cg_razon_social,	p.cg_razon_social)");
		}//fin-else
		return strQuery.toString();
  	} // Fin del m�todo getAggregateCalculationQuery

	public String getDocumentQuery(HttpServletRequest request){
	  // Obtiene las claves que son utilizadas para ejecutar la consulta por pagina	
		StringBuffer strQuery = new StringBuffer("");
		String 	sTipoAfiliado   = ( request.getParameter("sTipoAfiliado") == null ) ? "1" : request.getParameter("sTipoAfiliado");
		String 	sNombre   		= ( request.getParameter("sNombre") == null ) ? "" : request.getParameter("sNombre");
		String 	sPYME   		= ( request.getParameter("sPYME") == null ) ? "" : request.getParameter("sPYME");
		String 	sIcEdo   		= ( request.getParameter("sIcEdo") == null ) ? "" : request.getParameter("sIcEdo");
		String 	sRFC   			= ( request.getParameter("sRFC") == null ) ? "" : request.getParameter("sRFC");
		String 	sIcEpo   		= ( request.getParameter("sIcEpo") == null ) ? "" : request.getParameter("sIcEpo");
		if (sTipoAfiliado.equals("1"))
		{  strQuery.append(" SELECT /*+ index(pi IN_COMREL_PYME_IF_01_NUK) use_nl(pi,cb,p,d,e) */"   +
					"     (p.ic_pyme||' '||pi.ic_epo) CLAVE, 'ConsProvDistIfCA:getDocumentQuery' PANTALLA "   +
					" FROM 	"   +
					" 	comrel_pyme_if pi,"   +
					" 	comrel_cuenta_bancaria cb,"   +
					" 	comcat_pyme p,"   +
					" 	com_domicilio d,"   +
					" 	comcat_epo e"   +
					" WHERE "   +
					" 	 p.ic_pyme = cb.ic_pyme and"   +
					"   	 p.ic_pyme = d.ic_pyme  and"   +
					" 	 pi.ic_cuenta_bancaria = cb.ic_cuenta_bancaria and"   +
					" 	 pi.ic_epo = e.ic_epo  	and"   +
					" 	 d.cs_fiscal = 'S'      and  "   +
					"   e.cs_habilitado = 'S' and " +
					" 	 pi.cs_borrado = 'N'	and"   +
					" 	 cb.cs_borrado = 'N'    and"   +
					" 	 pi.ic_if = ?		and"   +
					" 	 decode(p.ic_tipo_cliente,4,?,p.ic_tipo_cliente) = ?  ");
		  if(!"".equals(sPYME))
			strQuery.append(" 	 and p.ic_pyme = ? ");
		  if(!"".equals(sRFC))
			strQuery.append(" 	 and p.cg_rfc = ? ");
		  if(!"".equals(sIcEdo))
			strQuery.append(" 	 and d.ic_estado = ? ");
		  if(!"".equals(sIcEpo))
			strQuery.append(" 	 and pi.ic_epo = ? ");
		  if(!"".equals(sNombre))
			strQuery.append(" 	 and p.cg_razon_social like ? ");
		  strQuery.append(" GROUP BY (p.ic_pyme||' '||pi.ic_epo),'ConsProvDistIfCA:getDocumentQuery',e.cg_razon_social,	p.cg_razon_social	order by e.cg_razon_social,	p.cg_razon_social	");
		} else {
		  strQuery.append(" SELECT /*+ index(aut CP_COM_CUENTA_AUT_X_PROD_PK) use_nl(cbp,p,aut,d,e) */"   +
						" 	   (p.ic_pyme||' '||e.ic_epo) CLAVE, 'ConsProvDistIfCA:getDocumentQuery'  PANTALLA"   +
						" FROM "   +
						"  	  comrel_cuenta_bancaria_x_prod cbp, "   +
						" 	  comcat_pyme p,	  "   +
						" 	  com_cuenta_aut_x_prod aut, "   +
						" 	  com_domicilio d,"   +
						" 	  comcat_epo e"   +
						"  WHERE "   +
						" 	  cbp.ic_pyme = p.ic_pyme AND "   +
						"  	  cbp.ic_cuenta_bancaria = aut.ic_cuenta_bancaria(+) AND "   +
						" 	  cbp.ic_epo = e.ic_epo AND"   +
						"   	  p.ic_pyme = d.ic_pyme  and"   +
						" 	  cbp.ic_if=? AND"   +
						" 	 d.cs_fiscal = 'S'      and  "   +
						"   e.cs_habilitado = 'S' and " +
						" 	 aut.cs_borrado(+) = 'N'	and"   +
						" 	 aut.IC_EPO(+) = cbp.ic_epo and"   +
						" 	 aut.IC_IF(+) = ? and "   +
						" 	 cbp.cs_borrado = 'N'    and"   +
						" 	 decode(p.ic_tipo_cliente,4,?,p.ic_tipo_cliente) = ?  and"   +
						"    cbp.ic_producto_nafin = 4 ");	
		  if(!"".equals(sPYME))
			strQuery.append(" 	 and p.ic_pyme = ? ");
		  if(!"".equals(sRFC))
			strQuery.append(" 	 and p.cg_rfc = ? ");
		  if(!"".equals(sIcEdo))
			strQuery.append(" 	 and d.ic_estado = ? ");
		  if(!"".equals(sIcEpo))
			strQuery.append(" 	 and cbp.ic_epo = ? ");
		  if(!"".equals(sNombre))
			strQuery.append(" 	 and p.cg_razon_social like ? ");
		  strQuery.append("GROUP BY (p.ic_pyme||' '||e.ic_epo),'ConsProvDistIfCA:getDocumentQuery',e.cg_razon_social,	p.cg_razon_social	order by e.cg_razon_social,	p.cg_razon_social	");
		}			
		return strQuery.toString();
  	} // Fin del metodo getDocumentQuery

 	public String getDocumentQueryFile(HttpServletRequest request){
 	  // Obtiene la informacion completa de la consulta	
	  StringBuffer strQuery = new StringBuffer("");

		String 	sTipoAfiliado   = ( request.getParameter("sTipoAfiliado") == null ) ? "1" : request.getParameter("sTipoAfiliado");
		String 	sNombre   		= ( request.getParameter("sNombre") == null ) ? "" : request.getParameter("sNombre");
		String 	sPYME   		= ( request.getParameter("sPYME") == null ) ? "" : request.getParameter("sPYME");
		String 	sIcEdo   		= ( request.getParameter("sIcEdo") == null ) ? "" : request.getParameter("sIcEdo");
		String 	sRFC   			= ( request.getParameter("sRFC") == null ) ? "" : request.getParameter("sRFC");
		String 	sIcEpo   		= ( request.getParameter("sIcEpo") == null ) ? "" : request.getParameter("sIcEpo");

		if (sTipoAfiliado.equals("1"))
		{ strQuery.append(" SELECT /*+ index(pi IN_COMREL_PYME_IF_01_NUK) use_nl(pi,cb,p,c,d,e,edo) */"   +
				"     p.ic_pyme CLAVE,e.ic_epo,"   +
				" 	e.cg_razon_social EPO,"   +
				" 	p.cg_razon_social NOMBRE,"   +
				" 	p.cg_rfc RFC,"   +
				" 	d.cg_calle||' '||d.cg_numero_ext||' '||d.cg_numero_int||' COL. '||d.cg_colonia DOMICILIO,"   +
				" 	edo.cd_nombre ESTADO,"   +
				" 	d.cg_telefono1 TEL,"   +
				" 	d.cg_email MAIL,"   +
				" 	c.cg_appat||' '||c.cg_apmat||' '||c.cg_nombre CONTACTO,"   +
				" 	sum(decode(pi.cs_vobo_if,'S',1,0)) AUTORIZADAS, 'ConsProvDistIfCA:getDocumentQueryFile'  PANTALLA "   +
				" FROM 	"   +
				" 	comrel_pyme_if pi,"   +
				" 	comrel_cuenta_bancaria cb,"   +
				" 	comcat_pyme p,"   +
				" 	com_contacto c,"   +
				" 	com_domicilio d,"   +
				" 	comcat_epo e,"   +
				" 	comcat_estado edo"   +
				" WHERE "   +
				" 	 p.ic_pyme = cb.ic_pyme and"   +
				"   	 p.ic_pyme = d.ic_pyme  and"   +
				" 	 p.ic_pyme = c.ic_pyme(+)  and"   +
				" 	 c.cs_primer_contacto(+) = 'S' and"   +
				" 	 pi.ic_cuenta_bancaria = cb.ic_cuenta_bancaria and"   +
				" 	 pi.ic_epo = e.ic_epo  	and"   +
				" 	 d.ic_estado = edo.ic_estado and"   +
				" 	 d.cs_fiscal = 'S'      and  "   +
				"   e.cs_habilitado = 'S' and " +
				" 	 pi.cs_borrado = 'N'	and"   +
				" 	 cb.cs_borrado = 'N'    and"   +
				" 	 pi.ic_if = ?		and"   +
				" 	 decode(p.ic_tipo_cliente,4,?,p.ic_tipo_cliente) = ?  ");
		  if(!"".equals(sPYME))
			strQuery.append(" 	 and p.ic_pyme = ? ");
		  if(!"".equals(sRFC))
			strQuery.append(" 	 and p.cg_rfc = ? ");
		  if(!"".equals(sIcEdo))
			strQuery.append(" 	 and d.ic_estado = ? ");
		  if(!"".equals(sIcEpo))
			strQuery.append(" 	 and pi.ic_epo = ? ");
		  if(!"".equals(sNombre))
			strQuery.append(" 	 and p.cg_razon_social like ? ");
		  strQuery.append(" GROUP BY"   +
				"   p.ic_pyme,e.ic_epo,"   +
				" 	e.cg_razon_social,"   +
				" 	p.cg_razon_social,"   +
				" 	p.cg_rfc,"   +
				" 	d.cg_calle||' '||d.cg_numero_ext||' '||d.cg_numero_int||' COL. '||d.cg_colonia,"   +
				" 	edo.cd_nombre,"   +
				" 	d.cg_telefono1,"   +
				" 	d.cg_email,"   +
				" 	c.cg_appat||' '||c.cg_apmat||' '||c.cg_nombre, 'ConsProvDistIfCA:getDocumentQueryFile'"   +
				" order by e.cg_razon_social,	p.cg_razon_social	");
		} else {
		  strQuery.append(" SELECT /*+ index(aut CP_COM_CUENTA_AUT_X_PROD_PK) use_nl(cbp,p,aut,c,d,e,edo) */"   +
				"     p.ic_pyme CLAVE,"   +
				" 	e.ic_epo,"   +
				" 	e.cg_razon_social EPO,"   +
				" 	p.cg_razon_social NOMBRE,"   +
				" 	p.cg_rfc RFC,"   +
				" 	d.cg_calle||' '||d.cg_numero_ext||' '||d.cg_numero_int||' COL. '||d.cg_colonia DOMICILIO,"   +
				" 	edo.cd_nombre ESTADO,"   +
				" 	d.cg_telefono1 TEL,"   +
				" 	d.cg_email MAIL,"   +
				" 	c.cg_appat||' '||c.cg_apmat||' '||c.cg_nombre CONTACTO,"   +
				" 	sum(decode(aut.cs_vobo_if,'S',1,0)) AUTORIZADAS, 'ConsProvDistIfCA:getDocumentQueryFile'  PANTALLA "   +
				"  FROM "   +
				"  	  comrel_cuenta_bancaria_x_prod cbp, "   +
				" 	  comcat_pyme p,	  "   +
				" 	  com_cuenta_aut_x_prod aut, "   +
				" 	  com_contacto c,"   +
				" 	  com_domicilio d,"   +
				" 	  comcat_epo e,"   +
				" 	  comcat_estado edo"   +
				"  WHERE "   +
				" 	  cbp.ic_pyme = p.ic_pyme AND "   +
				"  	  cbp.ic_cuenta_bancaria = aut.ic_cuenta_bancaria(+) AND "   +
				" 	  cbp.ic_epo = e.ic_epo AND"   +
				"   	  p.ic_pyme = d.ic_pyme  and"   +
				" 	 p.ic_pyme = c.ic_pyme(+)  and"   +
				" 	 c.cs_primer_contacto(+) = 'S' and"   +
				" 	 d.ic_estado = edo.ic_estado and"   +
				" 	  cbp.ic_if=? AND"   +
				" 	 d.cs_fiscal = 'S'      and  "   +
				"   e.cs_habilitado = 'S' and " +
				" 	 aut.cs_borrado(+) = 'N'	and"   +
				" 	 aut.IC_EPO(+) = cbp.ic_epo and"   +
				" 	 aut.IC_IF(+) = ? and "   +
				" 	 cbp.cs_borrado = 'N'    and"   +
				" 	 decode(p.ic_tipo_cliente,4,?,p.ic_tipo_cliente) = ?  and"   +
				"      cbp.ic_producto_nafin = 4 ");
		  if(!"".equals(sPYME))
			strQuery.append(" 	 and p.ic_pyme = ? ");
		  if(!"".equals(sRFC))
			strQuery.append(" 	 and p.cg_rfc = ? ");
		  if(!"".equals(sIcEdo))
			strQuery.append(" 	 and d.ic_estado = ? ");
		  if(!"".equals(sIcEpo))
			strQuery.append(" 	 and cbp.ic_epo = ? ");
		  if(!"".equals(sNombre))
			strQuery.append(" 	 and p.cg_razon_social like ? ");
		  strQuery.append(" group by"   +
					"     p.ic_pyme,"   +
					" 	e.ic_epo,"   +
					" 	e.cg_razon_social,"   +
					" 	p.cg_razon_social,"   +
					" 	p.cg_rfc,"   +
					" 	d.cg_calle||' '||d.cg_numero_ext||' '||d.cg_numero_int||' COL. '||d.cg_colonia,"   +
					" 	edo.cd_nombre,"   +
					" 	d.cg_telefono1,"   +
					" 	d.cg_email,"   +
					" 	c.cg_appat||' '||c.cg_apmat||' '||c.cg_nombre, 'ConsProvDistIfCA:getDocumentQueryFile'"   +
					" order by e.cg_razon_social,	p.cg_razon_social");
		}
		return strQuery.toString();
  	} // Fin del metodo getDocumentQueryFile


	public int getNumList(HttpServletRequest request){
		return this.numList;

	}

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids){

	  StringBuffer strQuery = new StringBuffer("");
	  StringBuffer clavesCombinaciones = new StringBuffer("");
	  String 	ses_ic_if 		= (String)request.getSession().getAttribute("iNoCliente");
	  String 	sTipoAfiliado   = ( request.getParameter("sTipoAfiliado") == null ) ? "1" : request.getParameter("sTipoAfiliado");

		for (Iterator it = ids.iterator(); it.hasNext();){
			it.next();
			clavesCombinaciones.append("?,");
		}
		clavesCombinaciones = clavesCombinaciones.delete(clavesCombinaciones.length()-1,clavesCombinaciones.length());
		if (sTipoAfiliado.equals("1"))
		{ strQuery.append(" SELECT /*+ index(pi IN_COMREL_PYME_IF_01_NUK) use_nl(pi,cb,p,c,d,e,edo) */"   +
						"     p.ic_pyme CLAVE,e.ic_epo,"   +
						" 	e.cg_razon_social EPO,"   +
						" 	p.cg_razon_social NOMBRE,"   +
						" 	p.cg_rfc RFC,"   +
						" 	d.cg_calle||' '||d.cg_numero_ext||' '||d.cg_numero_int||' COL. '||d.cg_colonia DOMICILIO,"   +
						" 	edo.cd_nombre ESTADO,"   +
						" 	d.cg_telefono1 TEL,"   +
						" 	d.cg_email MAIL,"   +
						" 	c.cg_appat||' '||c.cg_apmat||' '||c.cg_nombre CONTACTO,"   +
						" 	sum(decode(pi.cs_vobo_if,'S',1,0)) AUTORIZADAS,'ConsProvDistIfCA:getDocumentSummaryQueryForIds'  PANTALLA  "   +
						" FROM 	"   +
						" 	comrel_pyme_if pi,"   +
						" 	comrel_cuenta_bancaria cb,"   +
						" 	comcat_pyme p,"   +
						" 	com_contacto c,"   +
						" 	com_domicilio d,"   +
						" 	comcat_epo e,"   +
						" 	comcat_estado edo"   +
						" WHERE "   +
						" 	 p.ic_pyme = cb.ic_pyme and"   +
						"   	 p.ic_pyme = d.ic_pyme  and"   +
						" 	 p.ic_pyme = c.ic_pyme(+)  and"   +
						" 	 c.cs_primer_contacto(+) = 'S' and"   +
						" 	 pi.ic_cuenta_bancaria = cb.ic_cuenta_bancaria and"   +
						" 	 pi.ic_epo = e.ic_epo  	and"   +
						" 	 d.ic_estado = edo.ic_estado and"   +
						" 	 d.cs_fiscal = 'S'      and  "   +
						"   e.cs_habilitado = 'S' and " +
						" 	 pi.cs_borrado = 'N'	and"   +
						" 	 cb.cs_borrado = 'N'    and"  +
						" 	 pi.ic_if = "+ses_ic_if+"		and"   +
						" 	 (p.ic_pyme||' '||pi.ic_epo)  in ("+clavesCombinaciones.toString()+") "   +
						" GROUP BY"   +
						"     p.ic_pyme,e.ic_epo,"   +
						" 	e.cg_razon_social,"   +
						" 	p.cg_razon_social,"   +
						" 	p.cg_rfc,"   +
						" 	d.cg_calle||' '||d.cg_numero_ext||' '||d.cg_numero_int||' COL. '||d.cg_colonia,"   +
						" 	edo.cd_nombre,"   +
						" 	d.cg_telefono1,"   +
						" 	d.cg_email,"   +
						" 	c.cg_appat||' '||c.cg_apmat||' '||c.cg_nombre, 'ConsProvDistIfCA:getDocumentSummaryQueryForIds'"   +
						" order by e.cg_razon_social,	p.cg_razon_social	");
		} else {
			strQuery.append(" SELECT /*+ index(aut CP_COM_CUENTA_AUT_X_PROD_PK) use_nl(cbp,p,aut,c,d,e,edo) */"   +
						"     p.ic_pyme CLAVE,"   +
						" 	e.ic_epo,"   +
						" 	e.cg_razon_social EPO,"   +
						" 	p.cg_razon_social NOMBRE,"   +
						" 	p.cg_rfc RFC,"   +
						" 	d.cg_calle||' '||d.cg_numero_ext||' '||d.cg_numero_int||' COL. '||d.cg_colonia DOMICILIO,"   +
						" 	edo.cd_nombre ESTADO,"   +
						" 	d.cg_telefono1 TEL,"   +
						" 	d.cg_email MAIL,"   +
						" 	c.cg_appat||' '||c.cg_apmat||' '||c.cg_nombre CONTACTO,"   +
						" 	sum(decode(aut.cs_vobo_if,'S',1,0)) AUTORIZADAS, 'ConsProvDistIfCA:getDocumentSummaryQueryForIds'  PANTALLA "   +
						"  FROM "   +
						"  	  comrel_cuenta_bancaria_x_prod cbp, "   +
						" 	  comcat_pyme p,	  "   +
						" 	  com_cuenta_aut_x_prod aut, "   +
						" 	  com_contacto c,"   +
						" 	  com_domicilio d,"   +
						" 	  comcat_epo e,"   +
						" 	  comcat_estado edo"   +
						"  WHERE "   +
						" 	  cbp.ic_pyme = p.ic_pyme AND "   +
						"  	  cbp.ic_cuenta_bancaria = aut.ic_cuenta_bancaria(+) AND "   +
						" 	  cbp.ic_epo = e.ic_epo AND"   +
						"   	  p.ic_pyme = d.ic_pyme  and"   +
						" 	 p.ic_pyme = c.ic_pyme(+)  and"   +
						" 	 c.cs_primer_contacto(+) = 'S' and"   +
						" 	 d.ic_estado = edo.ic_estado and"   +
						" 	  cbp.ic_if="+ses_ic_if+" AND"   +
						" 	 d.cs_fiscal = 'S'      and  "   +
						"   e.cs_habilitado = 'S' and " +
						" 	 aut.cs_borrado(+) = 'N'	and"   +
						" 	 aut.IC_EPO(+) = cbp.ic_epo and"   +
						" 	 aut.IC_IF(+) = "+ses_ic_if+" and "   +
						" 	 cbp.cs_borrado = 'N'    and"   +
						"      cbp.ic_producto_nafin = 4 and"   +
						" 	 (p.ic_pyme||' '||e.ic_epo)  in ("+clavesCombinaciones.toString()+")"   +
						" group by"   +
						"     p.ic_pyme,"   +
						" 	e.ic_epo,"   +
						" 	e.cg_razon_social,"   +
						" 	p.cg_razon_social,"   +
						" 	p.cg_rfc,"   +
						" 	d.cg_calle||' '||d.cg_numero_ext||' '||d.cg_numero_int||' COL. '||d.cg_colonia,"   +
						" 	edo.cd_nombre,"   +
						" 	d.cg_telefono1,"   +
						" 	d.cg_email,"   +
						" 	c.cg_appat||' '||c.cg_apmat||' '||c.cg_nombre, 'ConsProvDistIfCA:getDocumentSummaryQueryForIds'"   +
						" order by e.cg_razon_social,	p.cg_razon_social");			
		}				
		return strQuery.toString();
		
  } //Fin del metodo getDocumentSummaryQueryForIds
  
  /****************************************************************************
	* 																									*
	*     				COMIENZA MIGRACION 													*
	*     																							*
	****************************************************************************/
	// Variable para enviar mensajes al log.
	private static final Log log = ServiceLocator.getInstance().getLog(ClausuladoNafinIF.class);
	
	public  StringBuffer strQuery;
	private List conditions = new ArrayList();
	private String sTipoAfiliado;
	private String sNombre;
	private String sPYME;
	private String sIcEdo;
	private String sRFC;
	private String sIcEpo;
	private String ses_ic_if;	
	
	
	
	public String getAggregateCalculationQuery() { // Obtiene los totales 
		return "";
 	}  
		 
	public String getDocumentQuery(){  // para las llaves primarias
		//conditions = new ArrayList();	
		strQuery   = new StringBuffer(); 
		StringBuffer condicion = new StringBuffer();
		
		if (sTipoAfiliado.equals("1")){
			conditions.add(ses_ic_if);
			conditions.add(sTipoAfiliado);
			conditions.add(sTipoAfiliado);
			
		if(!"".equals(sPYME)){
			condicion.append(" and p.ic_pyme = ? ");
			conditions.add(new Integer(sPYME));
		  }
		  if(!"".equals(sRFC)){
			condicion.append (" and p.cg_rfc = ? ");
			conditions.add(sRFC);
		  }
		  if(!"".equals(sIcEdo)){
			condicion.append(" and d.ic_estado = ? ");
			conditions.add(new Integer(sIcEdo));
		  }
		  if(!"".equals(sIcEpo)){
			condicion.append(" and pi.ic_epo = ? ");
			conditions.add(new Integer(sIcEpo));
		  }
		  if(!"".equals(sNombre)){
			condicion.append(" and p.cg_razon_social like ? ");
			conditions.add("%"+sNombre+"%");
		  }
		  
		if (!"".equals(suceptibleFloating)  ) {
			condicion.append(" AND p.CS_TASA_FLOATING = ? "); 
			conditions.add(suceptibleFloating);
		}
		
		
			strQuery.append(" SELECT /*+ index(pi IN_COMREL_PYME_IF_01_NUK) use_nl(pi,cb,p,d,e) */"   +
					"     (p.ic_pyme||' '||pi.ic_epo) CLAVE, 'ConsProvDistIfCA:getDocumentQuery' PANTALLA "   +
					" FROM 	"   +
					" 	comrel_pyme_if pi,"   +
					" 	comrel_cuenta_bancaria cb,"   +
					" 	comcat_pyme p,"   +
					" 	com_domicilio d,"   +
					" 	comcat_epo e"   +
					" WHERE "   +
					" 	 p.ic_pyme = cb.ic_pyme and"   +
					"   	 p.ic_pyme = d.ic_pyme  and"   +
					" 	 pi.ic_cuenta_bancaria = cb.ic_cuenta_bancaria and"   +
					" 	 pi.ic_epo = e.ic_epo  	and"   +
					" 	 d.cs_fiscal = 'S'      and  "   +
					"   e.cs_habilitado = 'S' and " +
					" 	 pi.cs_borrado = 'N'	and"   +
					" 	 cb.cs_borrado = 'N'    and"   +
					" 	 pi.ic_if = ?		and"   +
					" 	 decode(p.ic_tipo_cliente,4,?,p.ic_tipo_cliente) = ?  " + condicion);
					
					strQuery.append("GROUP BY (p.ic_pyme||' '||pi.ic_epo),'ConsProvDistIfCA:getDocumentQuery',e.cg_razon_social," +
					" 	p.cg_razon_social	order by e.cg_razon_social,	p.cg_razon_social	");
		} else {
			
			conditions.add(ses_ic_if);
			conditions.add(ses_ic_if);
			conditions.add(sTipoAfiliado);
			conditions.add(sTipoAfiliado);
			
			 if(!"".equals(sPYME)){
			condicion.append(" 	 and p.ic_pyme = ? ");
			conditions.add(new Integer(sPYME));
		  }
		  if(!"".equals(sRFC)){
			condicion.append (" 	 and p.cg_rfc = ? ");
			conditions.add(sRFC);
		  }
		  if(!"".equals(sIcEdo)){
			condicion.append(" 	 and d.ic_estado = ? ");
			conditions.add(new Integer(sIcEdo));
		  }
		  if(!"".equals(sIcEpo)){
			condicion.append(" 	 and cbp.ic_epo = ? ");
			conditions.add(new Integer(sIcEpo));
		  }
		  if(!"".equals(sNombre)){
			condicion.append(" 	 and p.cg_razon_social like ? ");
			conditions.add("%"+sNombre+"%");
		  }
		  strQuery.append(" SELECT /*+ index(aut CP_COM_CUENTA_AUT_X_PROD_PK) use_nl(cbp,p,aut,d,e) */"   +
						" 	   (p.ic_pyme||' '||e.ic_epo) CLAVE, 'ConsProvDistIfCA:getDocumentQuery'  PANTALLA"   +
						" FROM "   +
						"  	  comrel_cuenta_bancaria_x_prod cbp, "   +
						" 	  comcat_pyme p,	  "   +
						" 	  com_cuenta_aut_x_prod aut, "   +
						" 	  com_domicilio d,"   +
						" 	  comcat_epo e"   +
						"  WHERE "   +
						" 	  cbp.ic_pyme = p.ic_pyme AND "   +
						"  	  cbp.ic_cuenta_bancaria = aut.ic_cuenta_bancaria(+) AND "   +
						" 	  cbp.ic_epo = e.ic_epo AND"   +
						"   	  p.ic_pyme = d.ic_pyme  and"   +
						" 	  cbp.ic_if=? AND"   +
						" 	 d.cs_fiscal = 'S'      and  "   +
						"   e.cs_habilitado = 'S' and " +
						" 	 aut.cs_borrado(+) = 'N'	and"   +
						" 	 aut.IC_EPO(+) = cbp.ic_epo and"   +
						" 	 aut.IC_IF(+) = ? and "   +
						" 	 cbp.cs_borrado = 'N'    and"   +
						" 	 decode(p.ic_tipo_cliente,4,?,p.ic_tipo_cliente) = ?  and"   +
						"    cbp.ic_producto_nafin = 4 " + condicion);	
		  strQuery.append("GROUP BY (p.ic_pyme||' '||e.ic_epo),'ConsProvDistIfCA:getDocumentQuery',e.cg_razon_social,	p.cg_razon_social	order by e.cg_razon_social,	p.cg_razon_social	");
		}
	
		return strQuery.toString();
 	}  
	  
	
	public String getDocumentSummaryQueryForIds(List pageIds){ // paginacion 
		StringBuffer strQuery = new StringBuffer();
		StringBuffer clavesCombinaciones = new StringBuffer("");
		StringBuffer condicion = new StringBuffer();
		conditions  = new ArrayList();
				
		for (int i = 0; i < pageIds.size(); i++) {
			List lItem = (ArrayList)pageIds.get(i);
			clavesCombinaciones.append("'"+lItem.get(0).toString()+"'"+",");
		}
		clavesCombinaciones.deleteCharAt(clavesCombinaciones.length()-1);
		
		if (sTipoAfiliado.equals("1"))
		{ strQuery.append(" SELECT /*+ index(pi IN_COMREL_PYME_IF_01_NUK) use_nl(pi,cb,p,c,d,e,edo) */"   +
						"  p.ic_pyme CLAVE,e.ic_epo,"   +
						" 	e.cg_razon_social EPO,"   +
						" 	p.cg_razon_social NOMBRE,"   +
						" 	p.cg_rfc RFC,"   +
						" 	d.cg_calle||' '||d.cg_numero_ext||' '||d.cg_numero_int||' COL. '||d.cg_colonia DOMICILIO,"   +
						" 	edo.cd_nombre ESTADO,"   +
						" 	d.cg_telefono1 TEL,"   +
						" 	d.cg_email MAIL,"   +
						" 	c.cg_appat||' '||c.cg_apmat||' '||c.cg_nombre CONTACTO,"   +
						" 	sum(decode(pi.cs_vobo_if,'S',1,0)) AUTORIZADAS,'ConsProvDistIfCA:getDocumentSummaryQueryForIds'  PANTALLA  "   +
						" ,decode( p.CS_TASA_FLOATING, 'S', 'Si', 'N', 'No') as TASA_FLOATING  " +	
						" ,(SELECT  TO_CHAR (c.DF_OPERACION, 'DD/MM/YYYY')   FROM   COM_ACUSE_FLOATING c  WHERE  c.CC_ACUSE  =  p.CC_ACUSE )    as FECHA_PARAM_FLOATING "+
								
						" FROM 	"   +
						" 	comrel_pyme_if pi,"   +
						" 	comrel_cuenta_bancaria cb,"   +
						" 	comcat_pyme p,"   +
						" 	com_contacto c,"   +
						" 	com_domicilio d,"   +
						" 	comcat_epo e,"   +
						" 	comcat_estado edo"   +
						" WHERE "   +
						" 	 p.ic_pyme = cb.ic_pyme and"   +
						"   	 p.ic_pyme = d.ic_pyme  and"   +
						" 	 p.ic_pyme = c.ic_pyme(+)  and"   +
						" 	 c.cs_primer_contacto(+) = 'S' and"   +
						" 	 pi.ic_cuenta_bancaria = cb.ic_cuenta_bancaria and"   +
						" 	 pi.ic_epo = e.ic_epo  	and"   +
						" 	 d.ic_estado = edo.ic_estado and"   +
						" 	 d.cs_fiscal = 'S'      and  "   +
						"   e.cs_habilitado = 'S' and " +
						" 	 pi.cs_borrado = 'N'	and"   +
						" 	 cb.cs_borrado = 'N'    and"  +
						" 	 pi.ic_if = '"+ses_ic_if+"'	and"   +
						" 	 (p.ic_pyme||' '||pi.ic_epo)  in ("+clavesCombinaciones+") "   +
						" GROUP BY"   +
						"     p.ic_pyme,e.ic_epo,"   +
						" 	e.cg_razon_social,"   +
						" 	p.cg_razon_social,"   +
						" 	p.cg_rfc,"   +
						" 	d.cg_calle||' '||d.cg_numero_ext||' '||d.cg_numero_int||' COL. '||d.cg_colonia,"   +
						" 	edo.cd_nombre,"   +
						" 	d.cg_telefono1,"   +
						" 	d.cg_email,"   +
						" 	c.cg_appat||' '||c.cg_apmat||' '||c.cg_nombre, 'ConsProvDistIfCA:getDocumentSummaryQueryForIds'"   +
						",  p.cs_tasa_floating, p.cc_acuse "+
						" order by e.cg_razon_social,	p.cg_razon_social	");
		} else {
			strQuery.append(" SELECT /*+ index(aut CP_COM_CUENTA_AUT_X_PROD_PK) use_nl(cbp,p,aut,c,d,e,edo) */"   +
						"  p.ic_pyme CLAVE,"   +
						" 	e.ic_epo,"   +
						" 	e.cg_razon_social EPO,"   +
						" 	p.cg_razon_social NOMBRE,"   +
						" 	p.cg_rfc RFC,"   +
						" 	d.cg_calle||' '||d.cg_numero_ext||' '||d.cg_numero_int||' COL. '||d.cg_colonia DOMICILIO,"   +
						" 	edo.cd_nombre ESTADO,"   +
						" 	d.cg_telefono1 TEL,"   +
						" 	d.cg_email MAIL,"   +
						" 	c.cg_appat||' '||c.cg_apmat||' '||c.cg_nombre CONTACTO,"   +
						" 	sum(decode(aut.cs_vobo_if,'S',1,0)) AUTORIZADAS, 'ConsProvDistIfCA:getDocumentSummaryQueryForIds'  PANTALLA "   +
						"  FROM "   +
						"  	  comrel_cuenta_bancaria_x_prod cbp, "   +
						" 	  comcat_pyme p,	  "   +
						" 	  com_cuenta_aut_x_prod aut, "   +
						" 	  com_contacto c,"   +
						" 	  com_domicilio d,"   +
						" 	  comcat_epo e,"   +
						" 	  comcat_estado edo"   +
						"  WHERE "   +
						" 	  cbp.ic_pyme = p.ic_pyme AND "   +
						"  	  cbp.ic_cuenta_bancaria = aut.ic_cuenta_bancaria(+) AND "   +
						" 	  cbp.ic_epo = e.ic_epo AND"   +
						"   	  p.ic_pyme = d.ic_pyme  and"   +
						" 	 p.ic_pyme = c.ic_pyme(+)  and"   +
						" 	 c.cs_primer_contacto(+) = 'S' and"   +
						" 	 d.ic_estado = edo.ic_estado and"   +
						" 	  cbp.ic_if= '"+ses_ic_if+"' AND"   +
						" 	 d.cs_fiscal = 'S'      and  "   +
						"   e.cs_habilitado = 'S' and " +
						" 	 aut.cs_borrado(+) = 'N'	and"   +
						" 	 aut.IC_EPO(+) = cbp.ic_epo and"   +
						" 	 aut.IC_IF(+) = '"+ses_ic_if+"' and "   +
						" 	 cbp.cs_borrado = 'N'    and"   +
						"      cbp.ic_producto_nafin = 4 and"   +
						" 	 (p.ic_pyme||' '||e.ic_epo)  in ("+clavesCombinaciones.toString()+")"   +
						" group by"   +
						"     p.ic_pyme,"   +
						" 	e.ic_epo,"   +
						" 	e.cg_razon_social,"   +
						" 	p.cg_razon_social,"   +
						" 	p.cg_rfc,"   +
						" 	d.cg_calle||' '||d.cg_numero_ext||' '||d.cg_numero_int||' COL. '||d.cg_colonia,"   +
						" 	edo.cd_nombre,"   +
						" 	d.cg_telefono1,"   +
						" 	d.cg_email,"   +
						" 	c.cg_appat||' '||c.cg_apmat||' '||c.cg_nombre, 'ConsProvDistIfCA:getDocumentSummaryQueryForIds'"   +
						" order by e.cg_razon_social,	p.cg_razon_social");			
		}	
		System.out.println(strQuery.toString());
		return strQuery.toString();
 	}  
	
	
	public String getDocumentQueryFile(){ // para todos los registros
		conditions = new ArrayList();   
		StringBuffer strQuery = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
				
		if (sTipoAfiliado.equals("1")){ 
			conditions.add(ses_ic_if);
			conditions.add(sTipoAfiliado);
			conditions.add(sTipoAfiliado);
			
		  if(!"".equals(sPYME)){
			condicion.append(" 	 and p.ic_pyme = ? ");
			conditions.add(new Integer(sPYME));
		  }
		  if(!"".equals(sRFC)){
			condicion.append (" 	 and p.cg_rfc = ? ");
			conditions.add(sRFC);
		  }
		  if(!"".equals(sIcEdo)){
			condicion.append(" 	 and d.ic_estado = ? ");
			conditions.add(new Integer(sIcEdo));
		  }
		  if(!"".equals(sIcEpo)){
			condicion.append(" 	 and pi.ic_epo = ? ");
			conditions.add(new Integer(sIcEpo));
		  }
		  if(!"".equals(sNombre)){
			condicion.append(" 	 and p.cg_razon_social like ? ");
			conditions.add("%"+sNombre+"%");
		  }
		  
			if (!"".equals(suceptibleFloating)  ) {
				condicion.append(" AND p.CS_TASA_FLOATING = ?"); 
				conditions.add(suceptibleFloating);
			}		  
		  
		strQuery.append(" SELECT /*+ index(pi IN_COMREL_PYME_IF_01_NUK) use_nl(pi,cb,p,c,d,e,edo) */"   +
				"  p.ic_pyme CLAVE,e.ic_epo,"   +
				" 	e.cg_razon_social EPO,"   +
				" 	p.cg_razon_social NOMBRE,"   +
				" 	p.cg_rfc RFC,"   +
				" 	d.cg_calle||' '||d.cg_numero_ext||' '||d.cg_numero_int||' COL. '||d.cg_colonia DOMICILIO,"   +
				" 	edo.cd_nombre ESTADO,"   +
				" 	d.cg_telefono1 TEL,"   +
				" 	d.cg_email MAIL,"   +
				" 	c.cg_appat||' '||c.cg_apmat||' '||c.cg_nombre CONTACTO,"   +
				" 	sum(decode(pi.cs_vobo_if,'S',1,0)) AUTORIZADAS, 'ConsProvDistIfCA:getDocumentQueryFile'  PANTALLA "   +
				" ,decode( p.CS_TASA_FLOATING, 'S', 'Si', 'N', 'No') as TASA_FLOATING  " +	
				" ,(SELECT  TO_CHAR (c.DF_OPERACION, 'DD/MM/YYYY')   FROM   COM_ACUSE_FLOATING c  WHERE  c.CC_ACUSE  =  p.CC_ACUSE )    as FECHA_PARAM_FLOATING "+
							
				" FROM 	"   +
				" 	comrel_pyme_if pi,"   +
				" 	comrel_cuenta_bancaria cb,"   +
				" 	comcat_pyme p,"   +
				" 	com_contacto c,"   +
				" 	com_domicilio d,"   +
				" 	comcat_epo e,"   +
				" 	comcat_estado edo"   +
				" WHERE "   +
				" 	 p.ic_pyme = cb.ic_pyme and"   +
				"   p.ic_pyme = d.ic_pyme  and"   +
				" 	 p.ic_pyme = c.ic_pyme(+)  and"   +
				" 	 c.cs_primer_contacto(+) = 'S' and"   +
				" 	 pi.ic_cuenta_bancaria = cb.ic_cuenta_bancaria and"   +
				" 	 pi.ic_epo = e.ic_epo  	and"   +
				" 	 d.ic_estado = edo.ic_estado and"   +
				" 	 d.cs_fiscal = 'S'      and  "   +
				"   e.cs_habilitado = 'S' and " +
				" 	 pi.cs_borrado = 'N'	and"   +
				" 	 cb.cs_borrado = 'N'    and"   +
				" 	 pi.ic_if = ? and"   +
				" 	 decode(p.ic_tipo_cliente,4,?,p.ic_tipo_cliente) = ?  " + condicion);
		  strQuery.append(" GROUP BY"   +
				"   p.ic_pyme,e.ic_epo,"   +
				" 	e.cg_razon_social,"   +
				" 	p.cg_razon_social,"   +
				" 	p.cg_rfc,"   +
				" 	d.cg_calle||' '||d.cg_numero_ext||' '||d.cg_numero_int||' COL. '||d.cg_colonia,"   +
				" 	edo.cd_nombre,"   +
				" 	d.cg_telefono1,"   +
				" 	d.cg_email,"   +
				" 	c.cg_appat||' '||c.cg_apmat||' '||c.cg_nombre, 'ConsProvDistIfCA:getDocumentQueryFile'"   +
				",  p.cs_tasa_floating, p.cc_acuse "+
				" order by e.cg_razon_social,	p.cg_razon_social	");
		} else {
			
				conditions.add(ses_ic_if);
				conditions.add(ses_ic_if);
				conditions.add(sTipoAfiliado);
				conditions.add(sTipoAfiliado);
				
		 if(!"".equals(sPYME)){
			strQuery.append(" 	 and p.ic_pyme = ? ");
			conditions.add(new Integer(sPYME));
		  }
		  if(!"".equals(sRFC)){
			condicion.append (" 	 and p.cg_rfc = ? ");
			conditions.add(sRFC);
		  }
		  if(!"".equals(sIcEdo)){
			condicion.append(" 	 and d.ic_estado = ? ");
			conditions.add(new Integer(sIcEdo));
		  }
		  if(!"".equals(sIcEpo)){
			condicion.append(" 	 and cbp.ic_epo = ? ");
			conditions.add(new Integer(sIcEpo));
		  }
		  if(!"".equals(sNombre)){
			condicion.append(" 	 and p.cg_razon_social like ? ");
			conditions.add("%"+sNombre+"%");
		  }
		  
		  strQuery.append(" SELECT /*+ index(aut CP_COM_CUENTA_AUT_X_PROD_PK) use_nl(cbp,p,aut,c,d,e,edo) */"   +
				"  p.ic_pyme CLAVE,"   +
				" 	e.ic_epo,"   +
				" 	e.cg_razon_social EPO,"   +
				" 	p.cg_razon_social NOMBRE,"   +
				" 	p.cg_rfc RFC,"   +
				" 	d.cg_calle||' '||d.cg_numero_ext||' '||d.cg_numero_int||' COL. '||d.cg_colonia DOMICILIO,"   +
				" 	edo.cd_nombre ESTADO,"   +
				" 	d.cg_telefono1 TEL,"   +
				" 	d.cg_email MAIL,"   +
				" 	c.cg_appat||' '||c.cg_apmat||' '||c.cg_nombre CONTACTO,"   +
				" 	sum(decode(aut.cs_vobo_if,'S',1,0)) AUTORIZADAS, 'ConsProvDistIfCA:getDocumentQueryFile'  PANTALLA "   +
				"  FROM "   +
				"  	  comrel_cuenta_bancaria_x_prod cbp, "   +
				" 	  comcat_pyme p,	  "   +
				" 	  com_cuenta_aut_x_prod aut, "   +
				" 	  com_contacto c,"   +
				" 	  com_domicilio d,"   +
				" 	  comcat_epo e,"   +
				" 	  comcat_estado edo"   +
				"  WHERE "   +
				" 	  cbp.ic_pyme = p.ic_pyme AND "   +
				"  	  cbp.ic_cuenta_bancaria = aut.ic_cuenta_bancaria(+) AND "   +
				" 	  cbp.ic_epo = e.ic_epo AND"   +
				"   	  p.ic_pyme = d.ic_pyme  and"   +
				" 	 p.ic_pyme = c.ic_pyme(+)  and"   +
				" 	 c.cs_primer_contacto(+) = 'S' and"   +
				" 	 d.ic_estado = edo.ic_estado and"   +
				" 	  cbp.ic_if=? AND"   +
				" 	 d.cs_fiscal = 'S'      and  "   +
				"   e.cs_habilitado = 'S' and " +
				" 	 aut.cs_borrado(+) = 'N'	and"   +
				" 	 aut.IC_EPO(+) = cbp.ic_epo and"   +
				" 	 aut.IC_IF(+) = ? and "   +
				" 	 cbp.cs_borrado = 'N'    and"   +
				" 	 decode(p.ic_tipo_cliente,4,?,p.ic_tipo_cliente) = ?  and"   +
				"      cbp.ic_producto_nafin = 4 " + condicion);
		 
		  strQuery.append(" group by"   +
					"     p.ic_pyme,"   +
					" 	e.ic_epo,"   +
					" 	e.cg_razon_social,"   +
					" 	p.cg_razon_social,"   +
					" 	p.cg_rfc,"   +
					" 	d.cg_calle||' '||d.cg_numero_ext||' '||d.cg_numero_int||' COL. '||d.cg_colonia,"   +
					" 	edo.cd_nombre,"   +
					" 	d.cg_telefono1,"   +
					" 	d.cg_email,"   +
					" 	c.cg_appat||' '||c.cg_apmat||' '||c.cg_nombre, 'ConsProvDistIfCA:getDocumentQueryFile'"   +
					" order by e.cg_razon_social,	p.cg_razon_social");
		}
	
		return strQuery.toString();		
 	} 
	
	///para formar el csv o pdf de los todos los registros
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		CreaArchivo 	archivo 			= new CreaArchivo();
	   String floating ="";
		String fechaParamFloating = "";
	
		if ("XLS".equals(tipo)) {
			try {
				
				int registros = 0, total =0;
				
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
				buffer = new BufferedWriter(writer);
		
				
				while(rs.next()){
					if(registros==0) {
						contenidoArchivo.append("Cadena Productiva,Nombre o Razon Social,RFC,Domicilio,Estado,Telefono,Email,Contacto,Autorizada ");
						
						if ("1".equals(sTipoAfiliado)  ) {
							contenidoArchivo.append(", Proveedores susceptibles a Floating,Fecha Parametrizaci�n Floating ");
						}
					}
					registros++;
					String 	operacion   = ( request.getParameter("operacion") == null ) ? "1" : request.getParameter("operacion");
					String sEpo	= (rs.getString("EPO")==null)?"":rs.getString("EPO");
					String sNomPyme	= (rs.getString("NOMBRE")==null)?"":rs.getString("NOMBRE");
					String sRfcPyme	= (rs.getString("RFC")==null)?"":rs.getString("RFC");
					String sDomicilio	= (rs.getString("DOMICILIO")==null)?"":rs.getString("DOMICILIO");
					String sEstado	= (rs.getString("ESTADO")==null)?"":rs.getString("ESTADO");
					String sTel	= (rs.getString("TEL")==null)?"":rs.getString("TEL");
					String sMail	= (rs.getString("MAIL")==null)?"":rs.getString("MAIL");
					String sContacto	= (rs.getString("CONTACTO")==null)?"":rs.getString("CONTACTO");
					String sNumCtas	= (rs.getString("AUTORIZADAS")==null)?"":rs.getString("AUTORIZADAS");	
				
					
					if ("1".equals(sTipoAfiliado)  ) {
						floating = (rs.getString("TASA_FLOATING") == null) ? "" : rs.getString("TASA_FLOATING");
						fechaParamFloating ="";
						if ("Si".equals(floating)  ) {
							fechaParamFloating = (rs.getString("FECHA_PARAM_FLOATING") == null) ? "" : rs.getString("FECHA_PARAM_FLOATING");
						}					
					}
					
               sNumCtas = Integer.parseInt(sNumCtas)>0?"SI":"NO";
					
					contenidoArchivo.append("\n"+sEpo.replace(',',' ')+","+sNomPyme.replace(',',' ')+","+sRfcPyme.replace(',',' ')+","+sDomicilio.replace(',',' ')+
													","+sEstado.replace(',',' ')+","+sTel.replace(',',' ')+","+sMail.replace(',',' ')+","+sContacto.replace(',',' ')+
													","+sNumCtas.replace(',',' ') );
					
					if ("1".equals(sTipoAfiliado)  ) {
						contenidoArchivo.append(","+floating.replace(',',' ') +","+fechaParamFloating.replace(',',' ') 		);
					}
					
					total++;
					if(total==1000){					
						total=0;										
						buffer.write(contenidoArchivo.toString());
						contenidoArchivo = new StringBuffer();//Limpio 
					}
				
				}//while(rs.next())		
				buffer.write(contenidoArchivo.toString());
				buffer.close();	
				
			} catch (Throwable e) {
					throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		}else if ("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
						
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText((("1".equals(sTipoAfiliado))?"Consulta de Proveedores\n":"Consulta de Distribuidores\n"),"celda01",ComunesPDF.CENTER);
				int columnas =9;
				if ("1".equals(sTipoAfiliado)  ) {
					columnas = columnas+2;
				}
				pdfDoc.setTable(columnas, 100);
				pdfDoc.setCell("Cadena Productiva", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre o Raz�n Social", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("RFC", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Domicilio", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Estado", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Tel�fono", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("E-mail", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Contacto", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Autorizada", "formasmenB", ComunesPDF.CENTER);
				if ("1".equals(sTipoAfiliado)  ) {
					pdfDoc.setCell("Proveedores susceptibles a Floating ","celda01",ComunesPDF.CENTER);		
					pdfDoc.setCell("Fecha Parametrizaci�n Floating ","celda01",ComunesPDF.CENTER);	
				}
			 	
				while (rs.next()) {
					String rs_epo 			= rs.getString("epo")==null?"":rs.getString("epo");
					String rs_nombre 		= rs.getString("nombre")==null?"":rs.getString("nombre");
					String rs_rfc 			= rs.getString("rfc")==null?"":rs.getString("rfc");
					String rs_domicilio 	= rs.getString("domicilio")==null?"":rs.getString("domicilio");
					String rs_estado 		= rs.getString("estado")==null?"":rs.getString("estado");
					String rs_tel 			= rs.getString("tel")==null?"":rs.getString("tel");
					String rs_mail 		= rs.getString("mail")==null?"":rs.getString("mail");
					String rs_contacto 	= rs.getString("contacto")==null?"":rs.getString("contacto");
					String rs_autorizadas= rs.getString("autorizadas")==null?"":rs.getString("autorizadas");
					rs_autorizadas = Integer.parseInt(rs_autorizadas)>0?"SI":"NO";
					
					if ("1".equals(sTipoAfiliado)  ) {
						floating = (rs.getString("TASA_FLOATING") == null) ? "" : rs.getString("TASA_FLOATING");
						fechaParamFloating ="";
						if ("Si".equals(floating)  ) {
							fechaParamFloating = (rs.getString("FECHA_PARAM_FLOATING") == null) ? "" : rs.getString("FECHA_PARAM_FLOATING");
						}
					}
					
					pdfDoc.setCell(rs_epo,"formas",ComunesPDF.LEFT);				
					pdfDoc.setCell(rs_nombre,"formas",ComunesPDF.LEFT);				
					pdfDoc.setCell(rs_rfc,"formas",ComunesPDF.LEFT);				
					pdfDoc.setCell(rs_domicilio,"formas",ComunesPDF.LEFT);				
					pdfDoc.setCell(rs_estado,"formas",ComunesPDF.LEFT);				
					pdfDoc.setCell(rs_tel,"formas",ComunesPDF.LEFT);				
					pdfDoc.setCell(rs_mail,"formas",ComunesPDF.LEFT);	
					pdfDoc.setCell(rs_contacto,"formas",ComunesPDF.LEFT);				
					pdfDoc.setCell(rs_autorizadas,"formas",ComunesPDF.LEFT);	
					if ("1".equals(sTipoAfiliado)  ) {
						pdfDoc.setCell(floating,"formas",ComunesPDF.LEFT);			
						pdfDoc.setCell(fechaParamFloating,"formas",ComunesPDF.LEFT);	
					}
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		}
		return nombreArchivo;	
	}
	
	//para formar  csv o pdf por pagina  15 registros	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		StringBuffer contenidoArchivo = new StringBuffer();
		CreaArchivo archivo 	= new CreaArchivo();
		String floating = "";
		String fechaParamFloating = "";
		
		if ("PDFPrint".equals(tipo)) {
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
						
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText((("1".equals(sTipoAfiliado))?"Consulta de Proveedores\n":"Consulta de Distribuidores\n"),"celda01",ComunesPDF.CENTER);
				
				int columnas =9;
				if ("1".equals(sTipoAfiliado)  ) {
					columnas = columnas+2;
				}
				pdfDoc.setTable(columnas, 100);
				pdfDoc.setCell("Cadena Productiva", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre o Raz�n Social", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("RFC", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Domicilio", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Estado", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Tel�fono", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("E-mail", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Contacto", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setCell("Autorizada", "formasmenB", ComunesPDF.CENTER);
				if ("1".equals(sTipoAfiliado)  ) {
					pdfDoc.setCell("Proveedores susceptibles a Floating ","celda01",ComunesPDF.CENTER);		
					pdfDoc.setCell("Fecha Parametrizaci�n Floating ","celda01",ComunesPDF.CENTER);	
				}
				
				while (rs.next()) {
					String sEpo	      = (rs.getString("EPO")==null)?"":rs.getString("EPO");
               String sNomPyme	= (rs.getString("NOMBRE")==null)?"":rs.getString("NOMBRE");
               String sRfcPyme	= (rs.getString("RFC")==null)?"":rs.getString("RFC");
               String sDomicilio	= (rs.getString("DOMICILIO")==null)?"":rs.getString("DOMICILIO");
               String sEstado	   = (rs.getString("ESTADO")==null)?"":rs.getString("ESTADO");
               String sTel	      = (rs.getString("TEL")==null)?"":rs.getString("TEL");
               String sMail	   = (rs.getString("MAIL")==null)?"":rs.getString("MAIL");
               String sContacto	= (rs.getString("CONTACTO")==null)?"":rs.getString("CONTACTO");
               String sNumCtas	= (rs.getString("AUTORIZADAS")==null)?"":rs.getString("AUTORIZADAS");
               if (sNumCtas!=null&&!sNumCtas.equals(""))
                  sNumCtas = ((Integer.parseInt(sNumCtas))>0)?"SI":"NO";
               else
                  sNumCtas = "";
               
               //rs_autorizadas = Integer.parseInt(rs_autorizadas)>0?"SI":"NO";
					if ("1".equals(sTipoAfiliado)  ) {
						floating = (rs.getString("TASA_FLOATING") == null) ? "" : rs.getString("TASA_FLOATING");
						fechaParamFloating ="";
						if ("Si".equals(floating)  ) {
							fechaParamFloating = (rs.getString("FECHA_PARAM_FLOATING") == null) ? "" : rs.getString("FECHA_PARAM_FLOATING");
						}	
					}
					
					pdfDoc.setCell(sEpo,       "formas",ComunesPDF.LEFT);				
					pdfDoc.setCell(sNomPyme,   "formas",ComunesPDF.LEFT);				
					pdfDoc.setCell(sRfcPyme,   "formas",ComunesPDF.LEFT);				
					pdfDoc.setCell(sDomicilio, "formas",ComunesPDF.LEFT);				
					pdfDoc.setCell(sEstado,    "formas",ComunesPDF.LEFT);				
					pdfDoc.setCell(sTel,       "formas",ComunesPDF.LEFT);				
					pdfDoc.setCell(sMail,      "formas",ComunesPDF.LEFT);	
					pdfDoc.setCell(sContacto,  "formas",ComunesPDF.LEFT);				
					pdfDoc.setCell(sNumCtas,   "formas",ComunesPDF.LEFT);		
					if ("1".equals(sTipoAfiliado)  ) {
						pdfDoc.setCell(floating,   "formas",ComunesPDF.CENTER);			
						pdfDoc.setCell(fechaParamFloating,   "formas",ComunesPDF.CENTER);	
					}
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					//rs.close();
				} catch(Exception e) {}
			}
		}		
		return nombreArchivo;
	}
	
	
	public List getConditions() {
		return conditions;
	} 

	public void setSTipoAfiliado(String sTipoAfiliado) {
		this.sTipoAfiliado = sTipoAfiliado;
	}
 

	public String getSTipoAfiliado() {
		return sTipoAfiliado;
	}


	public void setSNombre(String sNombre) {
		this.sNombre = sNombre;
	}


	public String getSNombre() {
		return sNombre;
	}


	public void setSPYME(String sPYME) {
		this.sPYME = sPYME;
	}


	public String getSPYME() {
		return sPYME;
	}


	public void setSIcEdo(String sIcEdo) {
		this.sIcEdo = sIcEdo;
	}


	public String getSIcEdo() {
		return sIcEdo;
	}


	public void setSRFC(String sRFC) {
		this.sRFC = sRFC;
	}


	public String getSRFC() {
		return sRFC;
	}


	public void setSIcEpo(String sIcEpo) {
		this.sIcEpo = sIcEpo;
	}


	public String getSIcEpo() {
		return sIcEpo;
	}


	public void setSes_ic_if(String ses_ic_if) {
		this.ses_ic_if = ses_ic_if;
	}


	public String getSes_ic_if() {
		return ses_ic_if;
	}
	
	public void setSuceptibleFloating(String suceptibleFloating) {
		this.suceptibleFloating = suceptibleFloating;
	}

	public String getSuceptibleFloating() {
		return suceptibleFloating;
	}
	  
} // Fin de la clase ConsProvDistIfCA