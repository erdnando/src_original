/**
 * @class		:Consultas
 * 				Esta clase obtiene informacion de la Base de Datos.
 *             y da como resultado una cadena con los datos obtenidos.
 * @Pantalla :	*.
 * @PERFIL	 : *.
 * @date		 :	31-10-2014.
 * @by		 :	rpastrana.
 */

package com.netro.cadenas;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class Consultas  {

  /**
   * Declaracion de variables
	* @log			: muestra informacion de los eventos que suceden en el servidor.
	* @orden			: recibe el orden para mostrar la informacion.
	* @tabla			: recibe la tabla.
	* @campos		: recibe los campos a mostrar.
	* @strQuery		: almacena el query que sera ejecutado. 
	* @condicion	: recibe la condicion WHERE.
	* @conditions	: almacena la lista de variables que seran usadas como Bind(?).
   */
	  
  private String tabla  	="";
  private String orden 		="";
  private String campos 	="";
  private String condicion ="";
  private List   conditions;
  private StringBuffer strQuery = new StringBuffer();
  private final static Log log  = ServiceLocator.getInstance().getLog(Consultas.class); 

 /**
  * Constructor by Default
  */
	public Consultas() {
	}
	
	/**
	 * Este metodo obtiene informacion por medio de un query especificando
	 * Los campos, tabla, condiciones u orden, mediante los metodos de acceso:
	 * @setTabla
	 * @setCampos
	 * @setCondicion
	 * @setOrden
	 * @return información obtenida en la consulta.
	 */
	public String getInfoByQuery(){		
		log.info("getInfoByQuery(E)");
		AccesoDB  			conexionDB			= new AccesoDB(); 
		ResultSet 			resultSet			= null;
		ResultSetMetaData resultSetMetaData	= null;
		List 					listaAuxiliar		= new ArrayList();
		List  				listaRegistros		= new ArrayList();	
		LinkedHashMap 		linkedHashMap		= new LinkedHashMap();
		StringBuffer		strQuery 			= new StringBuffer("");
		String 				respuesta			= new String("");
		int 					numeroColumnas		= 0;
		int 					numeroFilas 		= 0;
		StringBuffer renglon = new StringBuffer("");
		StringBuffer info		= new StringBuffer(""); 
		
		try{
			conexionDB.conexionDB();				
			if(!campos.equals("")){
				strQuery.append( " SELECT "+ campos );
			}
			if(!tabla.equals("")){
				strQuery.append( " FROM " + tabla);	
			}
			if(!condicion.equals("")){
				strQuery.append(" WHERE " + condicion );
			}
			if(!orden.equals("")){
				strQuery.append( " ORDER BY " + orden );
			}			
			resultSet=conexionDB.queryDB(strQuery.toString());			
			resultSetMetaData = resultSet.getMetaData();
			numeroColumnas		= resultSetMetaData.getColumnCount();
			
			while (resultSet.next() && numeroFilas<1000){
				listaAuxiliar = new ArrayList();
				renglon = new StringBuffer("");
				
				for (int i = 1 ; i <= numeroColumnas; i++) {
					 if(i==1){
						renglon.append(resultSetMetaData.getColumnName(i)+","+resultSetMetaData.getColumnTypeName(i)+","+resultSet.getString(i));
					 } else {
						renglon.append(","+resultSetMetaData.getColumnName(i)+","+resultSetMetaData.getColumnTypeName(i)+","+resultSet.getString(i));
					 }
					linkedHashMap = new LinkedHashMap();
					linkedHashMap.put("COLUMNA",	resultSetMetaData.getColumnName(i));
					linkedHashMap.put("TIPO",		resultSetMetaData.getColumnTypeName(i));
					linkedHashMap.put("VALOR",		resultSet.getString(i));
					listaAuxiliar.add(linkedHashMap);
				}
				if (numeroFilas==0){
					renglon.append(",F"+numeroFilas);
					info.append(renglon);
				} else {
					renglon.append(",F"+numeroFilas);
					info.append(","+renglon);
				}
				listaRegistros.add(listaAuxiliar);
				numeroFilas++;
			}	
			
			resultSet.close();
		} catch (Exception e){ 
			System.err.println("Error: "+e);
		} finally {
			if (conexionDB.hayConexionAbierta()) {
			conexionDB.cierraConexionDB();
			}
		}
		log.debug("getInfoByQuery: "+strQuery.toString());
		//log.info("registros : "+listaRegistros);
		//log.info("info : "+info.toString());
		log.info("columnas : "+numeroColumnas);
		log.info("filas : "+numeroFilas);
		log.info("getInfoByQuery(S)");
		
		return info.toString();
	}

/**
 * @Setters_&_Getters_Methods_
 */

	public void setTabla(String tabla) {
		this.tabla = tabla;
	}


	public String getTabla() {
		return tabla;
	}


	public void setOrden(String orden) {
		this.orden = orden;
	}


	public String getOrden() {
		return orden;
	}


	public void setCampos(String campos) {
		this.campos = campos;
	}


	public String getCampos() {
		return campos;
	}


	public void setCondicion(String condicion) {
		this.condicion = condicion;
	}


	public String getCondicion() {
		return condicion;
	}
}