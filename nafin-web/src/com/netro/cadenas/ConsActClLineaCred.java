package com.netro.cadenas;

import netropology.utilerias.*;
import java.util.Collection;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import org.apache.commons.logging.Log;
import java.util.*;     
import com.netro.pdf.ComunesPDF;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;  
import com.netro.exception.*;
import java.io.FileOutputStream;
import javax.naming.*;
import java.io.*;

public class ConsActClLineaCred implements IQueryGeneratorRegExtJS {

	private final static Log log = ServiceLocator.getInstance().getLog(ConsActClLineaCred.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	
	private String numElectronico; 
	private String lineaCredito;
	private String nombre;
	
	

	public ConsActClLineaCred() {
	}

	/**
	 * 
	 * @return 
	 */
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	/**
	 * 
	 * @return 
	 */
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		qrySentencia.append("  SELECT 	"+		
			"  c.IC_NAFIN_ELECTRONICO AS  NUM_ELECTRONICO  "+		
			
			"   FROM COMCAT_CLI_EXTERNO  c,   "+
			"   com_domicilio d  ,  "+
			"   comcat_estado e , "+
			"   comcat_municipio m, "+
			"   comcat_pais p "+
			"  WHERE  c.IC_NAFIN_ELECTRONICO  = d.IC_NAFIN_ELECTRONICO  "+
			" AND d.ic_pais = p.ic_pais "+
			" AND d.ic_estado = e.ic_estado "+
			" AND d.ic_pais = e.ic_pais "+
			" AND d.ic_municipio = m.ic_municipio "+
			" AND d.ic_estado = m.ic_estado "+
			" AND d.ic_pais = m.ic_pais ");
			
			if(!lineaCredito.equals("")){
				qrySentencia.append(" AND c.IC_NAFIN_ELECTRONICO  =?  ");
				conditions.add(lineaCredito);
			}	
			
			
			

		log.info("getDocumentQuery_qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}

	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		
				
		qrySentencia.append("  SELECT 	"+		
		"  c.IC_NAFIN_ELECTRONICO AS  NUM_ELECTRONICO,   "+		
		"	DECODE (c.CS_TIPO_PERSONA,  'F', c.CG_NOMBRE || ' ' || c.CG_APPAT || ' ' || c.CG_APMAT ,	c.CG_RAZON_SOCIAL  ) AS NOMBRE_RAZON_SOCIAL,	"+
		"  c.CG_RFC AS  RFC ,   "+		
		"  d.CG_CALLE || ' ' || d.CG_COLONIA  as  DOMICILIO,  "+
		"  e.CD_NOMBRE as  ESTADO ,  "+
	   "  d.cg_telefono1 AS TELEFONO , "+
		"  C.IN_NUMERO_SIRAC AS NUM_SIRAC,  "+
		
		"  C.CS_HABILITADO,  "+
		"  C.CS_OBSERVACIONES   "+
		"   FROM COMCAT_CLI_EXTERNO  c,   "+
		"   com_domicilio d  ,  "+
		"   comcat_estado e , "+
		"   comcat_municipio m, "+
		"   comcat_pais p "+
		"  WHERE  c.IC_NAFIN_ELECTRONICO  = d.IC_NAFIN_ELECTRONICO  "+
		" AND d.ic_pais = p.ic_pais "+
		" AND d.ic_estado = e.ic_estado "+
		" AND d.ic_pais = e.ic_pais "+
		" AND d.ic_municipio = m.ic_municipio "+
		" AND d.ic_estado = m.ic_estado "+
		" AND d.ic_pais = m.ic_pais ");
		
		qrySentencia.append(" AND (");
			
			for (int i = 0; i < pageIds.size(); i++) { 
				List lItem = (ArrayList)pageIds.get(i);
				
				if(i > 0){qrySentencia.append("  OR  ");}
				qrySentencia.append(" c.IC_NAFIN_ELECTRONICO = ? " );
				conditions.add(new Long(lItem.get(0).toString()));				
			}
			
			qrySentencia.append(" ) ");
			
			qrySentencia.append(" order by  c.IC_NAFIN_ELECTRONICO  asc  "); 
			
		
		log.info("getDocumentSummaryQueryForIds_qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	
	public String getDocumentQueryFile() {
		return "";	
	}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		
		return "";
					
	}
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
	
		return "";
	}


	public String modificaEstatus(String cveCliente, String estatus, String observaciones)throws Exception{
		log.info("modificaEstatus (E)");
		AccesoDB 	con = new AccesoDB();
		conditions = new ArrayList();
		PreparedStatement ps = null;
		StringBuffer 	strQuery;
		ResultSet rs = null;
		boolean		commit = true;
		strQuery 		= new StringBuffer();
		String mansaje	= "";
		try{
			con.conexionDB();
			strQuery.append(
					"update COMCAT_CLI_EXTERNO"+
					" set CS_HABILITADO = '"+estatus+"'"+
					" ,CS_OBSERVACIONES = '"+observaciones+"'"+
					" where IC_NAFIN_ELECTRONICO = "+cveCliente
					);
			ps = con.queryPrecompilado(strQuery.toString());
			ps.executeUpdate();
			mansaje = "Los cambios fueron realizados exitosamente.";	
		}catch(Exception e){
			mansaje = "";	
			commit = false;
			e.printStackTrace();
			log.error("Error modificaEstatus "+e);
		}finally{
			if(ps!=null) ps.close();
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("modificaEstatus (S)");
		}	
		return mansaje;
 	}
	
	/**
	 Obtiene la lista de parámetros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina 
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/	

	public void setLineaCredito(String lineaCredito) {
		this.lineaCredito = lineaCredito;
	}


	public String getLineaCredito() {
		return lineaCredito;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}









	


}