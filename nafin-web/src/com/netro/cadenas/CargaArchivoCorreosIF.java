package com.netro.cadenas;

import com.netro.exception.NafinException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.VectorTokenizer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * clase para dar de alta los  correos parametrizados  alos IF�s
 */
public class CargaArchivoCorreosIF {
   
    private static final Log LOG = LogFactory.getLog(CargaArchivoCorreosIF.class);
    /**
     *  metodo para leer el arhivo txt
     * @param ruta
     * @param nombreArch
     * @return
     * @throws IOException
     */
    public Map<String, String> leerArchivo(String ruta, String nombreArch) throws IOException {

        LOG.info("leerArchivo(E)");

        int ilinea = 1;
        StringBuilder resultado = new StringBuilder();      
        int erroresTotal =0;
        boolean registroOk  = true;
        
        Map<String, String> mResultado = new HashMap<String, String>();
        
        try {

            java.io.File lofArchivo = new java.io.File(ruta + nombreArch);
            BufferedReader br =
                new BufferedReader(new InputStreamReader(new FileInputStream(lofArchivo), "ISO-8859-1"));
            String linea = "";

            List<String> lineaTitulos = new ArrayList();

            List ifs = new ArrayList();

            while ((linea = br.readLine()) != null) {

                registroOk = true;
                
                lineaTitulos = this.formatealinea(linea, "|");

                System.out.println("Tama�o del " + lineaTitulos.size() + " lineaTitulos  " + lineaTitulos);

                String claveIF = "";
                String listaCorreos = "";

                if (lineaTitulos.size() == 1) {
                    claveIF = lineaTitulos.get(0).toString();

                } else if (lineaTitulos.size() == 2) {
                    claveIF = lineaTitulos.get(0).toString();
                    listaCorreos = lineaTitulos.get(1).toString();
                }                

                if (!"".equals(claveIF)) {
                    if (claveIF.length() > 3) {
                        resultado.append("El archivo no se puedo cargar. Error en la l�nea " + ilinea +
                                         ", el tama�o m�ximo para el campo [clave del intermediario] es de 3  \n");
                        erroresTotal++;
                        registroOk = false;
                        
                    } else if (Comunes.esNumero(claveIF) == false) {
                        resultado.append("El archivo no se puedo cargar. Error en la l�nea " + ilinea +
                                         ", el valor en el campo [clave del intermediario] es invalido. \n");
                        erroresTotal++;
                        registroOk = false;

                    } else if (!existeIF(claveIF)) {
                        resultado.append("El archivo no se puedo cargar. Error en la l�nea " + ilinea +
                                         ", el valor en el campo [clave del intermediario] no existe en el sistema. \n");
                        erroresTotal++;
                        registroOk = false;
                    }

                    if (registroOk) {
                        ifs.add(claveIF);
                    }

                } else {
                    resultado.append("El archivo no se puedo cargar. Error en la l�nea " + ilinea +
                                     ", el campo [clave del intermediario] es obligatorio \n");
                     erroresTotal++;
                }

                if (registroOk) {
                    
                    if ("".equals(listaCorreos)) {
                        resultado.append("El archivo no se puedo cargar. Error en la l�nea " + ilinea +
                                         ", el valor en el campo [lista de correos electr�nicos] es obligatorio \n");
                         erroresTotal++;

                    } else if (!"".equals(listaCorreos)) {

                        if (listaCorreos.length() > 600) {
                                                        
                            resultado.append("El archivo no se puedo cargar. Error en la l�nea " + ilinea +
                                             ", el valor en el campo [lista de correos electr�nicos] no debe ser mayor a 600 caracteres \n");
                            erroresTotal++;
                            
                        } else {

                            VectorTokenizer correos = new VectorTokenizer(listaCorreos, ";");
                            Vector correosdat = correos.getValuesVector();
                            int errores = 0;
                            for (int k = 0; k < correosdat.size(); k++) {
                                String email = correosdat.get(k).toString().trim();

                                if (!isEmail(email)) {
                                    errores++;
                                }
                            }
                            if (errores > 0) {
                                resultado.append("El archivo no se puedo cargar. Error en la l�nea " + ilinea +
                                                 ", el valor en el campo [lista de correos electr�nicos] es invalido \n");
                                erroresTotal++;
                            }
                        }
                    }
                }
                ilinea++;

            } //while ((linea = br.readLine()) != null)


            System.out.println (" erroresTotal ===> "+erroresTotal);
            
            // Verificar  si hay algun If repedito
            ilinea = 1;
            Map<String, String> ifsRepeditos = new HashMap<String, String>();
            int linea2 = 1;
            for (Object o : ifs) {
                //Si no coincide el primer y �ltimo index => est�n repetidas
                if (ifs.indexOf(o) != ifs.lastIndexOf(o)) {
                    ifsRepeditos.put(String.valueOf(linea2), o.toString());
                }
                linea2++;
            }
            Iterator it = ifsRepeditos.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry e = (Map.Entry) it.next();
                resultado.append(" El archivo no se puedo cargar. Error en la l�nea " + e.getKey() +
                                 ", el valor en el campo [clave del intermediario] esta repetido en el archivo. \n");
                erroresTotal++;
            }

            br.close();           
            
            mResultado.put("resultado", resultado.toString());
            if (erroresTotal>0) {
                mResultado.put("respuesta", "ConError");
            } else if (erroresTotal==0) {
                mResultado.put("respuesta", "Exitoso");
            }


        } catch (IOException ioe) {
            LOG.error("IOException:..pos:: " + ioe);
        } catch (Exception e) {
            LOG.error("Exception:..pos:: " + e);
        } finally {
            LOG.info("leerArchivo(S)");
        }

        return mResultado;
    }

    /**
     * metodo que valida si el correo electronico es valddo 
     * @param correo
     * @return
     */
    public boolean isEmail(String correo) {
        Pattern pat = null;
        Matcher mat = null;
        pat = Pattern.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        mat = pat.matcher(correo);

        if (mat.find()) {
            LOG.debug("[" + mat.group() + "]");
            return true;
        } else {
            return false;
        }
    }

    /**
     *metodo para validar si existe el intermediario financiero
     * @param claveIF
     * @return
     * @throws AppException
     */
    private boolean existeIF(String claveIF ) throws AppException {
        LOG.info("existeIF  (e) ");
       
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean existe = false;
        List varBind = new ArrayList();
        
        try{                
            con.conexionDB();
                       
            StringBuilder qrySentencia = new StringBuilder();             
            qrySentencia.append(" select distinct B.ic_if "+
                                " from comcat_epo A, comcat_if B, comrel_cuenta_bancaria C,  comrel_pyme_if E  "+
                                " where A.ic_epo = E.ic_epo  "+
                                " and B.ic_if = E.ic_if  "+
                                " and C.ic_cuenta_bancaria = E.ic_cuenta_bancaria  "+
                                " and E.cs_borrado = ?  "+
                                " and B.ic_if = ? ");
                    
            varBind = new ArrayList();
            varBind.add("N"); 
            varBind.add(claveIF);     
            
            LOG.debug ("qrySentencia "+qrySentencia.toString()  +"varBind   "+  varBind);
            
            ps = con.queryPrecompilado(qrySentencia.toString(), varBind);
            rs = ps.executeQuery();
            if (rs.next()){                
                existe=(rs.getInt(1)>0)?true:false;
            }
            rs.close();
            ps.close();
           
                    
        }catch(Exception e) {
            LOG.error("Exception en existeIF. "+e);
            throw new AppException("Error consultar la tabla comcat_if   ");
        } finally {
            if(con.hayConexionAbierta()){
                con.cierraConexionDB();
            }
        }     
        LOG.info("existeIF  (s) ");
        return existe;
    }
    /**
     * metodo para guardar la parametrizaci�n de correos a los If�s 
     * @param ruta
     * @param nombreArch
     * @param strLogin
     * @return
     * @throws AppException
     */
    public int guardaArchivo(String ruta, String nombreArch, String strLogin) throws NafinException {
        LOG.info("guardaArchivo  (e) "); 
        AccesoDB con = new AccesoDB();
        int totalLineas = 0;
        StringBuilder qrySentencia = new StringBuilder();    
        PreparedStatement ps = null;
        List varBind = new ArrayList();
        try {
            con.conexionDB();
              
            String icCarga   = getIcCarga();
            java.io.File lofArchivo = new java.io.File(ruta + nombreArch);
            BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(lofArchivo)));

            VectorTokenizer vtd = null;
            String linea = "";
            Vector vecdat = null;
            
            while ((linea = br.readLine()) != null) {

                vtd = new VectorTokenizer(linea, "|");
                vecdat = vtd.getValuesVector();
            
                String claveIF = (vecdat.size() >= 1) ? (String) vecdat.get(0) : "";
                String listaCorreos = (vecdat.size() >= 2) ? (String) vecdat.get(1) : ""; 
                
                qrySentencia = new StringBuilder();
                qrySentencia.append(" INSERT INTO  COM_EMAIL_ACTNOT_IF "+
                                    " ( IC_CARGA,  IC_IF,  CG_EMAILS, DF_ALTA, CG_USUARIO  ) "+
                                    "  VALUES (  ?, ?, ?, Sysdate , ?  ) ");
                
                
                varBind = new ArrayList();  
                varBind.add(icCarga ); 
                varBind.add(claveIF );   
                varBind.add(listaCorreos );  
                varBind.add(strLogin );  
                
                LOG.debug ("qrySentencia  "+qrySentencia.toString() +" varBind    \n "+varBind);
                ps = con.queryPrecompilado(qrySentencia.toString(), varBind);
                ps.executeUpdate();
                ps.close();
                
                totalLineas++;
                
            }
            

        } catch (Exception e) {
            LOG.error("Exception en guardaArchivo. " + e);
            throw new AppException("Error al guardar los correos parametrizados para los Intermediarios  ");
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        LOG.info("guardaArchivo  (s) "); 
        return totalLineas;
    }
    
    
    /**
     * @return
     * @throws NafinException
     */
    public String getIcCarga() throws NafinException {
        LOG.info("getIcCarga  (e) "); 
        AccesoDB con = new AccesoDB();
        String icCarga=null;
        StringBuilder query = new StringBuilder();   

        try{
        
            con.conexionDB();
            
            query.append("select COM_EMAIL_ACTNOT_IF_SEQ.NEXTVAL from dual");

            PreparedStatement  ps = con.queryPrecompilado(query.toString());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                icCarga = rs.getString(1);  
            }
            rs.close();
            ps.close();
                    
        } catch (Exception e) {
            LOG.error("Exception " + e + "  Message" + e.getMessage());
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        LOG.info("getIcCarga  (s) "); 
        return icCarga;
    }
        
    /**
     * metodo para descargar el ultimo archivo cargado 
     * @param path
     * @return
     * @throws NafinException
     */
    public String getUltimaCarga(String path ) throws NafinException {
        LOG.info("getUltimaCarga  (e) "); 
        
        AccesoDB con = new AccesoDB();        
        StringBuilder query = new StringBuilder();   

        String nombreArchivo = "";
        StringBuilder contenidoArchivo = new StringBuilder();
        OutputStreamWriter writer = null;
        BufferedWriter buffer = null;
                
        try{
            con.conexionDB();
            
            nombreArchivo = Comunes.cadenaAleatoria(16) + ".txt";
            writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
            buffer = new BufferedWriter(writer);
            contenidoArchivo = new StringBuilder();
                        
            query.append(" SELECT  IC_IF , CG_EMAILS  "+
                         " FROM COM_EMAIL_ACTNOT_IF " +
                         " WHERE IC_CARGA = (SELECT MAX(IC_CARGA) FROM COM_EMAIL_ACTNOT_IF )  ");

            PreparedStatement  ps = con.queryPrecompilado(query.toString());
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String icIF = (rs.getString("IC_IF") == null) ? "" : rs.getString("IC_IF");  
                String emails = (rs.getString("CG_EMAILS") == null) ? "" : rs.getString("CG_EMAILS");
                
                contenidoArchivo.append(icIF+ "|"+emails+"\r\n");
            }
            rs.close();
            ps.close();
            
            buffer.write(contenidoArchivo.toString());
            buffer.close();
            
        } catch (Exception e) {
            LOG.error("Exception " + e + "  Message" + e.getMessage());
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        LOG.info("getUltimaCarga  (s) "); 
        return nombreArchivo;
    }
    
    /**
     * metodo para saber si existe una carga y poder mostrar activo el bot�n de descarga 
     * @return
     * @throws NafinException
     */
    public String getExisteCarga( ) throws NafinException {
        LOG.info("getExisteCarga  (e) "); 
        
        AccesoDB con = new AccesoDB();        
        StringBuilder query = new StringBuilder();  
        String existe = "N";
                
        try{
            con.conexionDB();
                                 
            query.append(" SELECT  count(*) AS TOTAL  FROM COM_EMAIL_ACTNOT_IF " );
            
            PreparedStatement  ps = con.queryPrecompilado(query.toString());
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                existe = (rs.getInt(1)>0)?"S":"N";                
            }
            rs.close();
            ps.close();
           
            
        } catch (Exception e) {
            LOG.error("Exception " + e + "  Message" + e.getMessage());
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        
        LOG.info("getExisteCarga  (s) "); 
        return existe;
    }

    /**
     * @param linea
     * @param separador
     * @return
     */
    public List<String> formatealinea(String linea, String separador) {

        List<String> lista = new ArrayList();
        String elem = "", cad = "";
        int possIni = 0, possFin = 0, countokens = 0;

        for (int i = 0; i < linea.length(); i++) {
            elem = linea.substring(i, i + 1);
            if (elem.equals(separador)) {
                possIni = (countokens == 0) ? 0 : possFin;
                possFin = i + 1;
                countokens++;
                cad = linea.substring(possIni, possFin - 1);
                lista.add(cad);
            }
        } // for
        String Delimit1 = linea.substring(possFin - 1, possFin);
        String Delimit2 = linea.substring(linea.length() - 1, linea.length());
        if ((Delimit1.equals(separador)) && (!Delimit2.equals(separador))) {
            if (possFin < linea.length())
                lista.add(linea.substring(possFin, linea.length()));
        }
        return lista;
    }


}
