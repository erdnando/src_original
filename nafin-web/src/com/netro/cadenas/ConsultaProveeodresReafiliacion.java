package com.netro.cadenas;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/******************************************************************************************
 * Fodea 11-2014																									*
 * Descripci�n: Migraci�n de la pantalla Administraci�n-Afiliados-Consultas-Proveedores	*
 * 				 a ExtJS. La clase tiene m�todos que generan las consultas para obtener		*
 *              los datos de reafiliaci�n.																*
 * Elabor�:     Agust�n Bautista Ruiz																		*
 * Fecha:       09/06/2014																						*
 ******************************************************************************************/
public class ConsultaProveeodresReafiliacion  implements IQueryGeneratorRegExtJS {

	private static final Log log = ServiceLocator.getInstance().getLog(ConsultaProveeodresReafiliacion.class);
	private String icPyme;
	private String icGrupoEpo;
	private String numProveedor;
	private String icEpo;
	private String tipoReafiliacion;
	private String condicionEpo;
	private List conditions;
	

	public ConsultaProveeodresReafiliacion() {}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){ return null; }
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){ return null; }
	
	public String getAggregateCalculationQuery(){ return null; }
	
	public String getDocumentSummaryQueryForIds(List ids){ return null; }
	
	public String getDocumentQuery(){ return null; }
	
	public String getDocumentQueryFile(){
			
		StringBuffer sentencia = new StringBuffer();
		conditions = new ArrayList();
		log.info("getDocumentQueryFile (E)");	
		try{
			
			if(this.tipoReafiliacion.equals("EPO")){
			
				sentencia.append(
					"SELECT P.IC_PYME, " +
					"       E.IC_EPO, " +
					"       E.CG_RAZON_SOCIAL AS CADENA_PRODUCTIVA, " +
					"       N.IC_NAFIN_ELECTRONICO AS NAFIN_ELECTRONICO, " +
					"       P.CG_RAZON_SOCIAL AS RAZON_SOCIAL, " +
					"       P.CG_RFC AS RFC " +
					"FROM   COMCAT_PYME P, COMREL_NAFIN N, COMCAT_EPO E " +
					"WHERE  P.IC_PYME = N.IC_EPO_PYME_IF " +
					"       AND N.CG_TIPO = 'P' " +
					"       AND P.IC_PYME = ? " + //994392
					"       AND E.IC_EPO IN ("+ this.icEpo +") " //411,1158,1079
				);			
				conditions.add(this.icPyme);
				//conditions.add(new String(this.icEpo));
				
			} else if(this.tipoReafiliacion.equals("GRUPO")){
				sentencia.append(
					"SELECT   REL.IC_EPO, " +
					"         P.IC_PYME, " +
					"         EPO.CG_RAZON_SOCIAL AS CADENA_PRODUCTIVA, " +
					"         N.IC_NAFIN_ELECTRONICO AS NAFIN_ELECTRONICO, " +
					"         P.CG_RAZON_SOCIAL AS RAZON_SOCIAL, " +
					"         P.CG_RFC AS RFC, " +
					"         '" + this.numProveedor + "' AS NO_PROVEEDOR " +
					"FROM     COMREL_GRUPO_X_EPO REL, COMCAT_EPO EPO, COMCAT_PYME P, COMREL_NAFIN N " +
					"WHERE    REL.IC_GRUPO_EPO = ? " +//19
					"         AND REL.IC_EPO = EPO.IC_EPO " +
					"         AND P.IC_PYME = N.IC_EPO_PYME_IF " +
					"         AND N.CG_TIPO = 'P' " +
					"         AND P.IC_PYME = ? " +     //994392
					"         AND EPO.IC_EPO NOT IN (SELECT IC_EPO " +
					"                                FROM   COMREL_PYME_EPO " +
					"                                WHERE  IC_PYME = ?) " +    
					"         AND EPO.CS_HABILITADO = 'S' " +
					"ORDER BY EPO.CG_RAZON_SOCIAL "
				);
				conditions.add(this.icGrupoEpo);
				conditions.add(this.icPyme);
				conditions.add(this.icPyme);
				
			}
					
		}catch(Exception e){
			log.warn("ConsultaProveeodresReafiliacion::getDocumentQueryFileException "+e);
		}
		log.debug("Sentencia: " + sentencia.toString() );
		log.debug("Condiciones: " + conditions.toString() );
		log.info("getDocumentQueryFile(S)");
		return sentencia.toString();
	}

	
	public HashMap consultaReafilacion(){
		
		log.info("consultaReafilacion (E)");
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		StringBuffer sentencia = new StringBuffer();
		HashMap datos = new HashMap();
		try{
			con.conexionDB();     			
			sentencia.append( "SELECT CG_RAZON_SOCIAL, CG_RFC " +
									"FROM   COMCAT_PYME " +
									"WHERE  IC_PYME = " + this.icPyme
								 );
				
			rs = con.queryDB(sentencia.toString());

			if(rs.next()) {
				datos.put("CG_RAZON_SOCIAL", (rs.getString("CG_RAZON_SOCIAL") == null) ? "" : rs.getString("CG_RAZON_SOCIAL"));
				datos.put("CG_RFC",          (rs.getString("CG_RFC")          == null) ? "" : rs.getString("CG_RFC"));
			}
			rs.close();
			con.cierraStatement();
	
		} catch (Exception e) {
			log.warn("Error en consultaReafilacion: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 	
		log.info("consultaReafilacion (S)");
		return datos;
	}

	/**
	 * Cuando no se puede agregar la pyme, se muestran los datos de las pymes con 
	 * errores
	 * @return 
	 */
	public JSONArray consultaDatosReafilacion(){
		
		log.info("consultaDatosReafilacion (E)");
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		StringBuffer sentencia = new StringBuffer();
		HashMap datos = new HashMap();
		List listDatos = new ArrayList();
		JSONArray registros = new JSONArray();
		try{
			con.conexionDB();     			
			sentencia.append( 
				"SELECT P.IC_PYME, " +
				"		  P.CG_RAZON_SOCIAL, " +
				"		  N.IC_NAFIN_ELECTRONICO, " + 
				"		  P.CG_RFC, " +
				"		  E.CG_RAZON_SOCIAL  AS ACREDITADA, " +	
				"		  E.IC_EPO AS NO_PROVEEDOR " +	
				"FROM	  COMCAT_PYME P, COMREL_NAFIN N, COMCAT_EPO E " +
				"WHERE  P.IC_PYME = N.IC_EPO_PYME_IF " +
				"		  AND N.CG_TIPO = 'P' " +
				"		  AND P.IC_PYME = " + this.icPyme +
				"		  AND E.IC_EPO IN (" + this.condicionEpo +")"
			);
				
			rs = con.queryDB(sentencia.toString());
			
			while(rs.next()) {
				datos = new HashMap();
				datos.put("IC_PYME",           (rs.getString("IC_PYME")              == null) ? "" : rs.getString("IC_PYME"));
				datos.put("ACREDITADA",        (rs.getString("ACREDITADA")           == null) ? "" : rs.getString("ACREDITADA"));
				datos.put("CG_RAZON_SOCIAL",   (rs.getString("CG_RAZON_SOCIAL")      == null) ? "" : rs.getString("CG_RAZON_SOCIAL"));
				datos.put("NAFIN_ELECTRONICO", (rs.getString("IC_NAFIN_ELECTRONICO") == null) ? "" : rs.getString("IC_NAFIN_ELECTRONICO"));
				datos.put("CG_RFC",            (rs.getString("CG_RFC")               == null) ? "" : rs.getString("CG_RFC"));
				datos.put("NO_PROVEEDOR",      (rs.getString("NO_PROVEEDOR")         == null) ? "" : rs.getString("NO_PROVEEDOR"));
				registros.add(datos);
			}
			rs.close();
			con.cierraStatement();
			
		} catch (Exception e) {
			log.warn("Error en consultaDatosReafilacion: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 	
		log.debug("Sentencia: " + sentencia.toString());
		log.info("consultaDatosReafilacion (S)");
		return registros;
	}
	
/************************************************************************
 *									GETTERS AND SETTERS									*
 ************************************************************************/
	public String getIcEpo() {
		return icEpo;
	}

	public void setIcEpo(String icEpo) {
		this.icEpo = icEpo;
	}

	public String getIcPyme() {
		return icPyme;
	}

	public void setIcPyme(String icPyme) {
		this.icPyme = icPyme;
	}

	public List getConditions() {
		return conditions;
	}

	public String getTipoReafiliacion() {
		return tipoReafiliacion;
	}

	public void setTipoReafiliacion(String tipoReafiliacion) {
		this.tipoReafiliacion = tipoReafiliacion;
	}

	public String getIcGrupoEpo() {
		return icGrupoEpo;
	}

	public void setIcGrupoEpo(String icGrupoEpo) {
		this.icGrupoEpo = icGrupoEpo;
	}

	public String getCondicionEpo() {
		return condicionEpo;
	}

	public void setCondicionEpo(String condicionEpo) {
		this.condicionEpo = condicionEpo;
	}

	public String getNumProveedor() {
		return numProveedor;
	}

	public void setNumProveedor(String numProveedor) {
		this.numProveedor = numProveedor;
	}
}