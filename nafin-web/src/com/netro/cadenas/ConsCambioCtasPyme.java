package com.netro.cadenas;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsCambioCtasPyme implements IQueryGeneratorReg,IQueryGeneratorRegExtJS {
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ConsCambioCtasPyme.class);
	
	/**
	 * Constructor de la clase.
	 */
	public ConsCambioCtasPyme(){}
	
	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer 	strSQL;
	private List 	conditions;
	private List 	pageIdsClon;
	private String paginaOffset;
	private String paginaNo;
	private String strDirectorioPublicacion;
	private String cveEpo;
	private String fecCambioCtaIni;
	
	private String fecCambioCtaFin;
	private String cmbAutorizacion;
	private String operacion ;


	//METODOS NUEVA VERSION
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo)
	{
		String linea = "";
		String nombreArchivo = "";
		String espacio = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
      
		try {
		 if(tipo.equals("CSV")) {
		  linea = "Nombre o Raz�n Social,RFC,Numero de Nafin Electr�nico,Intermediario,EPO,Cuenta Anterior,Banco,Cuenta Nueva,Banco,Usuario que Autoriz�"+
		  "ultima actualizaci�n,Fecha de Cambio Cta,Autorizaci�n\n";
		  nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
		  writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
		  buffer = new BufferedWriter(writer);
		
		  buffer.write(linea);
			 			
		  while (rs.next()) {
					String pyme = (rs.getString("Nombre_o_Razon_Social") == null) ? "" : rs.getString("Nombre_o_Razon_Social");
					String pyme_rfc = (rs.getString("RFC") == null) ? "" : rs.getString("RFC");
					String numNaf_elec = (rs.getString("Numero_Nafin_Electronico") == null) ? "" : rs.getString("Numero_Nafin_Electronico");
					String intermediario = (rs.getString("Intermediario") == null) ? "" : rs.getString("Intermediario");
					String epo = (rs.getString("EPO") == null) ? "" : rs.getString("EPO");
					String cuenta_anterior = (rs.getString("Cuenta_anterior") == null) ? "" : rs.getString("Cuenta_anterior");
					String banco_anterior = (rs.getString("Banco_anterior") == null) ? "" : rs.getString("Banco_anterior");
					String cuenta_nueva = (rs.getString("Cuenta_nueva") == null) ? "" : rs.getString("Cuenta_nueva");
					String banco_nuevo = (rs.getString("Banco_anterior") == null) ? "" : rs.getString("Banco_anterior");
					String fecha_cambio = (rs.getString("Fecha_Cambio_Cta") == null) ? "" : rs.getString("Fecha_Cambio_Cta");
					String ic_usuario_camb_cuen = (rs.getString("Usuario_Autorizo_Actualizacion") == null) ? "" : rs.getString("Usuario_Autorizo_Actualizacion");
					String csAutorizaIf = (rs.getString("Autorizacion") == null) ? "" : rs.getString("Autorizacion");
		
						
					linea = pyme.replace(',',' ') + ", " + 
								pyme_rfc.replace(',',' ') + ", " +
								numNaf_elec.replace(',',' ') + ", " +
								intermediario.replace(',',' ') + ", " +
								epo.replace(',',' ') + ", " +
								cuenta_anterior.replace(',',' ') + ", " +
								banco_anterior.replace(',',' ') +	","+	
								cuenta_nueva.replace(',',' ') + ", " + 
								banco_nuevo.replace(',',' ') + ", " +
								fecha_cambio.replace(',',' ') + ", " +
								ic_usuario_camb_cuen.replace(',',' ') +"," +
								csAutorizaIf.replace(',',' ') + "\n " ;
					buffer.write(linea);
		  }
		  buffer.close();
		 }else if(tipo.equals("PDF")){
		 
		 }
		 
		 }
		catch (Throwable e) {
			throw new AppException("Error al generar el archivo",e);
		}
		finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
		
		return nombreArchivo;
	}
	
	//METODOS VERSION ANTERIOR
	
	/**
	* Obtiene el query para obtener los totales de la consulta
	* @return Cadena con la consulta de SQL, para obtener los totales
	*/
	public String getAggregateCalculationQuery(){
		log.info("getAggregateCalculationQuery(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		log.info("getAggregateCalculationQuery(S) ::..");
		return strSQL.toString();
	}//getAggregateCalculationQuery
	
	/**
	* Obtiene el query para obtener las llaves primarias de la consulta
	* @return Cadena con la consulta de SQL, para obtener llaves primarias
	*/
	public String getDocumentQuery(){
		log.info("getDocumentQuery(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();		
		log.info("getDocumentQuery(E) ::..");
		

			strSQL.append(" SELECT  /*+ USE_NL (epo cif cta cn) INDEX(cta) */ ");
			strSQL.append("	cta.ic_epo, cta.ic_if , cta.ic_cuenta_bancaria,");				
      strSQL.append(" pyme.cg_razon_social AS pyme, " );
      strSQL.append(" pyme.cg_rfc AS pyme_rfc,    " );
      strSQL.append(" cn.ic_nafin_electronico AS numNaf_elec, ");
      strSQL.append(" cif.cg_razon_social AS intermediario, " );
      strSQL.append(" epo.cg_razon_social AS epo,  " );
      strSQL.append(" cta.cg_cuenta_ant AS cuenta_anterior, " );
      strSQL.append(" cta.cg_bancos_ant AS banco_anterior,  " );
      strSQL.append(" cta.cg_cuenta AS cuenta_nueva, " );
      strSQL.append(" cta.cg_bancos AS banco_nuevo, " );
      strSQL.append(" TO_CHAR (cta.df_cambio_cta, 'DD/MM/YYYY') AS fecha_cambio, " );
      strSQL.append(" cta.ic_usuario AS ic_usuario_camb_cuen, " );
      strSQL.append(" cta.cg_nombre_usuario AS nom_usuario_camb_cuen, " );
		  strSQL.append(" nvl(cta.cs_autoriza_if,'N') as csAutorizaIf " );
      strSQL.append(" FROM comrel_nafin cn, " );
      strSQL.append(" comcat_if cif,  " );
      strSQL.append(" comcat_epo epo,  " );
      strSQL.append(" comcat_pyme pyme, " );
      strSQL.append(" comhis_cambios_cta cta " );
      strSQL.append(" WHERE cta.ic_if = cif.ic_if " );
      strSQL.append(" AND cta.ic_epo = epo.ic_epo " );
      strSQL.append(" AND cta.ic_pyme = pyme.ic_pyme " );
      strSQL.append(" AND cta.ic_pyme = cn.ic_epo_pyme_if " );
      strSQL.append(" AND cn.cg_tipo = 'P' " );
			
		if(fecCambioCtaIni!=null && !fecCambioCtaIni.equals("") && fecCambioCtaFin!=null && !fecCambioCtaFin.equals("")){
			strSQL.append(" AND cta.DF_CAMBIO_CTA >= TO_DATE(?,'DD/MM/YYYY') " );
			strSQL.append(" AND cta.DF_CAMBIO_CTA < TO_DATE(?,'DD/MM/YYYY')+1 ");
			conditions.add(fecCambioCtaIni);
			conditions.add(fecCambioCtaFin);
		}
		
		if(cveEpo!=null && !cveEpo.equals("")){
			strSQL.append(" AND epo.ic_epo = ? " );
			conditions.add(new Long(cveEpo));
		}	
		
		if(cmbAutorizacion!=null && !cmbAutorizacion.equals("")){
			strSQL.append(" AND cta.cs_autoriza_if = ? " );
			conditions.add(cmbAutorizacion);
		}	
		
		strSQL.append(" ORDER BY cta.df_cambio_cta DESC ");
		
		log.debug("..:: strSQL: " + strSQL.toString());
		log.debug("..:: conditions: " + conditions);
		
		log.info("getDocumentQuery(S) ::..");
		return strSQL.toString();
	}//getDocumentQuery
	
	/**
	* Obtiene el query necesario para mostrar la informaci�n completa de 
	* una p�gina a partir de las llaves primarias enviadas como par�metro
	* @return Cadena con la consulta de SQL, para obtener la informaci�n 
	* completa de los registros con las llaves especificadas
	*/
	public String getDocumentSummaryQueryForIds(List pageIds){
		log.info("getDocumentSummaryQueryForIds(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();

	
  	strSQL.append(" SELECT  /*+ USE_NL (epo cif cta cn) INDEX(cta) */ ");
    strSQL.append(" pyme.cg_razon_social AS pyme, " );
    strSQL.append(" pyme.cg_rfc AS pyme_rfc,    " );
    strSQL.append(" cn.ic_nafin_electronico AS numNaf_elec, ");
    strSQL.append(" cif.cg_razon_social AS intermediario, " );
    strSQL.append(" epo.cg_razon_social AS epo,  " );
    strSQL.append(" cta.cg_cuenta_ant AS cuenta_anterior, " );
    strSQL.append(" cta.cg_bancos_ant AS banco_anterior,  " );
    strSQL.append(" cta.cg_cuenta AS cuenta_nueva, " );
    strSQL.append(" cta.cg_bancos AS banco_nuevo, " );
    strSQL.append(" TO_CHAR (cta.df_cambio_cta, 'DD/MM/YYYY') AS fecha_cambio, " );
    strSQL.append(" cta.ic_usuario AS ic_usuario_camb_cuen, " );
    strSQL.append(" cta.cg_nombre_usuario AS nom_usuario_camb_cuen, " );
		strSQL.append(" nvl(cta.cs_autoriza_if,'N') as csAutorizaIf " );
    strSQL.append(" FROM comrel_nafin cn, " );
    strSQL.append(" comcat_if cif,  " );
    strSQL.append(" comcat_epo epo,  " );
    strSQL.append(" comcat_pyme pyme, " );
    strSQL.append(" comhis_cambios_cta cta " );
    strSQL.append(" WHERE cta.ic_if = cif.ic_if " );
    strSQL.append(" AND cta.ic_epo = epo.ic_epo " );
    strSQL.append(" AND cta.ic_pyme = pyme.ic_pyme " );
    strSQL.append(" AND cta.ic_pyme = cn.ic_epo_pyme_if " );
    strSQL.append(" AND cn.cg_tipo = 'P' " );
			
		if(fecCambioCtaIni!=null && !fecCambioCtaIni.equals("") && fecCambioCtaFin!=null && !fecCambioCtaFin.equals("")){
			strSQL.append(" AND cta.DF_CAMBIO_CTA >= TO_DATE(?,'DD/MM/YYYY') " );
			strSQL.append(" AND cta.DF_CAMBIO_CTA < TO_DATE(?,'DD/MM/YYYY')+1 ");
			conditions.add(fecCambioCtaIni);
			conditions.add(fecCambioCtaFin);
		}
    		
		if(cveEpo!=null && !cveEpo.equals("")){
			strSQL.append(" AND epo.ic_epo = ? " );
			conditions.add(new Long(cveEpo));
		}
		
		if(cmbAutorizacion!=null && !cmbAutorizacion.equals("")){
			strSQL.append(" AND cta.cs_autoriza_if = ? " );
			conditions.add(cmbAutorizacion);
		}
	
		strSQL.append(" AND (");
		
			for(int i = 0; i < pageIds.size(); i++){
      List lItem = (ArrayList)pageIds.get(i);
			
      if(i > 0){strSQL.append("  OR  ");}
      
			strSQL.append(" (cta.ic_epo = ? and cta.ic_if = ? and cta.ic_cuenta_bancaria = ? ) ");
		
			conditions.add(new Long(lItem.get(0).toString()));
			conditions.add(new Long(lItem.get(1).toString()));
			conditions.add(new Long(lItem.get(2).toString()));
			
		}
		strSQL.append(" ) ");
		
		
		strSQL.append(" ORDER BY cta.df_cambio_cta DESC ");
		
	
		log.debug("..:: strSQL: " + strSQL.toString());
		log.debug("..:: conditions: " + conditions);
		
		log.info("getDocumentSummaryQueryForIds(S)");
		return strSQL.toString();
	}//getDocumentSummaryQueryForIds	
	
	public String getDocumentQueryFile(){
		log.info("getDocumentQueryFile(E)");
		strSQL = new StringBuffer();
		conditions = new ArrayList();
	
		
		strSQL.append(" SELECT  /*+ USE_NL (epo cif cta cn) INDEX(cta) */ ");
    strSQL.append(" pyme.cg_razon_social AS Nombre_o_Razon_Social, " );
    strSQL.append(" pyme.cg_rfc AS RFC,    " );
    strSQL.append(" cn.ic_nafin_electronico AS Numero_Nafin_Electronico, ");
    strSQL.append(" cif.cg_razon_social AS Intermediario, " );
    strSQL.append(" epo.cg_razon_social AS EPO,  " );
    strSQL.append(" cta.cg_cuenta_ant AS Cuenta_anterior, " );
    strSQL.append(" cta.cg_bancos_ant AS Banco_anterior,  " );
    strSQL.append(" cta.cg_cuenta AS Cuenta_nueva, " );
    strSQL.append(" cta.cg_bancos AS Banco_nuevo, " );      
    strSQL.append(" (cta.ic_usuario ||' '|| " );
    strSQL.append(" cta.cg_nombre_usuario) AS Usuario_Autorizo_Actualizacion, " );
		strSQL.append(" TO_CHAR (cta.df_cambio_cta, 'DD/MM/YYYY') AS Fecha_Cambio_Cta, " );
		strSQL.append(" nvl(cta.cs_autoriza_if,'N') as Autorizacion " );
    strSQL.append(" FROM comrel_nafin cn, " );
    strSQL.append(" comcat_if cif,  " );
    strSQL.append(" comcat_epo epo,  " );
    strSQL.append(" comcat_pyme pyme, " );
    strSQL.append(" comhis_cambios_cta cta " );
    strSQL.append(" WHERE cta.ic_if = cif.ic_if " );
    strSQL.append(" AND cta.ic_epo = epo.ic_epo " );
    strSQL.append(" AND cta.ic_pyme = pyme.ic_pyme " );
    strSQL.append(" AND cta.ic_pyme = cn.ic_epo_pyme_if " );
    strSQL.append(" AND cn.cg_tipo = 'P' " );
		
		if(fecCambioCtaIni!=null && !fecCambioCtaIni.equals("") && fecCambioCtaFin!=null && !fecCambioCtaFin.equals("")){
			strSQL.append(" AND cta.DF_CAMBIO_CTA >= TO_DATE(?,'DD/MM/YYYY') " );
			strSQL.append(" AND cta.DF_CAMBIO_CTA < TO_DATE(?,'DD/MM/YYYY')+1 ");
			conditions.add(fecCambioCtaIni);
			conditions.add(fecCambioCtaFin);
		}
      
		if(cveEpo!=null && !cveEpo.equals("")){
			strSQL.append(" AND epo.ic_epo = ? " );
			conditions.add(new Long(cveEpo));
		}
	
	 if(cmbAutorizacion!=null && !cmbAutorizacion.equals("")){
			strSQL.append(" AND cta.cs_autoriza_if = ? " );
			conditions.add(cmbAutorizacion);
		}
	
		strSQL.append(" ORDER BY cta.df_cambio_cta DESC ");


		log.debug("..:: strSQL: " + strSQL.toString());
		log.debug("..:: conditions: " + conditions);
		log.info("getDocumentQueryFile(S)");
		return strSQL.toString();
	}//getDocumentQueryFile
	
	//GETTERS
  /**
	 * Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 * @return Lista con los parametros de las condiciones
	 */
	public List getConditions(){return conditions;}


	public void setPaginaOffset(String paginaOffset) {
		this.paginaOffset = paginaOffset;
	}


	public String getPaginaOffset() {
		return paginaOffset;
	}


	public void setPaginaNo(String paginaNo) {
		this.paginaNo = paginaNo;
	}


	public String getPaginaNo() {
		return paginaNo;
	}


	public void setStrDirectorioPublicacion(String strDirectorioPublicacion) {
		this.strDirectorioPublicacion = strDirectorioPublicacion;
	}


	public String getStrDirectorioPublicacion() {
		return strDirectorioPublicacion;
	}


	public void setCveEpo(String cveEpo) {
		this.cveEpo = cveEpo;
	}


	public String getCveEpo() {
		return cveEpo;
	}


	public void setFecCambioCtaIni(String fecCambioCtaIni) {
		this.fecCambioCtaIni = fecCambioCtaIni;
	}


	public String getFecCambioCtaIni() {
		return fecCambioCtaIni;
	}


	public void setFecCambioCtaFin(String fecCambioCtaFin) {
		this.fecCambioCtaFin = fecCambioCtaFin;
	}


	public String getFecCambioCtaFin() {
		return fecCambioCtaFin;
	}


	public void setCmbAutorizacion(String cmbAutorizacion) {
		this.cmbAutorizacion = cmbAutorizacion;
	}


	public String getCmbAutorizacion() {
		return cmbAutorizacion;
	}


	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}


	public String getOperacion() {
		return operacion;
	}
  
  
	
	
}