package com.netro.cadenas;

import netropology.utilerias.*;
import java.util.Collection;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import org.apache.commons.logging.Log;
import java.util.*;    
import java.io.*;
import com.netro.pdf.ComunesPDF;
import java.sql.PreparedStatement;
import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ConsultaCanceXConti2 implements IQueryGeneratorRegExtJS {

//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaCanceXConti2.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	StringBuffer qryCondicion 	= new StringBuffer("");
	private String ic_producto;
	private String ic_if;
	private String ig_numero_prestamo;
	private String df_operacion;
	private String df_cancelacion;
	private String solicitudes;
	private String pantalla;
		
		 
	public ConsultaCanceXConti2() { }
	
	  
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		qryCondicion 	= new StringBuffer();
		conditions 		= new ArrayList();
		String condicion = "";
		String hint ="";
		
		
			if (ic_producto.equals("0"))  { // Credito Electronico 
				if (!ic_if.equals("") && !ic_if.equals("0") ){
					condicion += " AND sp.ic_if= "+ ic_if;
				}
				if (!ig_numero_prestamo.equals("")){
					condicion += " AND sp.ig_numero_prestamo IN(" + ig_numero_prestamo + ")";
				}
				if (!df_operacion.equals("")){										
					condicion += " AND sp.df_operacion >= to_date('" + df_operacion + "', 'dd/mm/yyyy' )"
										+  " AND sp.df_operacion < to_date('" + df_operacion + "', 'dd/mm/yyyy') + 1";
				}
				if (!df_cancelacion.equals("")){													
					condicion += " AND sp.df_cancelacion >= to_date('" + df_cancelacion + "', 'dd/mm/yyyy' )"
										+  " AND sp.df_cancelacion < to_date('" + df_cancelacion + "', 'dd/mm/yyyy') + 1";
				}	
				
					qrySentencia.append(
					"SELECT  " +
					" sp.ic_solic_portal AS nosolicitud " +
					" FROM comcat_moneda m, " +
					"		 comcat_pyme p, " +
					"		 comcat_estatus_solic es, "+					
					"		 comcat_if i, " +					
					"		 com_solic_portal sp " +
				 "WHERE es.ic_estatus_solic = sp.ic_estatus_solic " +
					" AND m.ic_moneda = sp.ic_moneda " +
					" AND sp.ic_if = i.ic_if " +
					" AND sp.ig_clave_sirac = p.in_numero_sirac(+) " +										
					" AND sp.ic_estatus_solic = 4 " +
					" AND sp.ic_bloqueo = 5 " +
				    condicion
				);
											
						
						
				} else if (ic_producto.equals("1"))	 { //Descuento //FOEA 047-2010 Rebos
									
					if (!ic_if.equals("") && !ic_if.equals("0") ){
					condicion += " AND ds.ic_if= "+ ic_if;
				}
				if (!ig_numero_prestamo.equals("")){
					condicion += " AND s.ig_numero_prestamo IN(" + ig_numero_prestamo + ")";
				}
				if (!df_operacion.equals("")){										
					condicion += " AND s.df_operacion >= to_date('" + df_operacion + "', 'dd/mm/yyyy' )"
										+  " AND s.df_operacion < to_date('" + df_operacion + "', 'dd/mm/yyyy') + 1";
				}
				if (!df_cancelacion.equals("")){										
					condicion += " AND s.df_cancelacion >= to_date('" + df_cancelacion + "', 'dd/mm/yyyy' )"
										+  " AND s.df_cancelacion < to_date('" + df_cancelacion + "', 'dd/mm/yyyy') + 1";
				}					
				qrySentencia.append(
					"SELECT /*+ index(s IN_COM_SOLICITUD_10_NUK) use_nl(s ds d i p e m es) */ " +
					"		  s.ic_folio AS nosolicitud, " +
					"		 m.cd_nombre AS moneda, " +
					"		 TO_CHAR (s.df_operacion, 'DD/MM/YYYY') AS dfoperacion, " +
					"		 DECODE (d.cs_dscto_especial, " +
					"						 'N', 'Normal', " +
					"						 'V', 'Vencido' " +
					"						) AS tipofactoraje, " +						
					"		 s.ig_numero_prestamo AS numeroprestamo, " +
					"		 ds.in_importe_recibir AS montooperar, " +
					"		 es.cd_descripcion AS estatusactual, " +
					"		 TO_CHAR (s.df_cancelacion, 'DD/MM/YYYY') AS fechacancelacion " +
					"FROM com_solicitud s, " +
					"		 com_docto_seleccionado ds, " +
					"		 com_documento d, " +
					"		 comcat_if i, " +
					"		 comcat_pyme p, " +
					"		 comcat_epo e, " +
					"		 comcat_moneda m, " +
					"		 comcat_estatus_solic es " +
				 "WHERE s.ic_estatus_solic = 1 " +
					" AND s.ic_bloqueo = 5 " +
					" AND s.ic_documento = ds.ic_documento " +
					" AND ds.ic_documento = d.ic_documento " +
					" AND ds.ic_linea_credito_emp IS NULL " +
					" AND ds.ic_if = i.ic_if " +
					" AND d.ic_pyme = p.ic_pyme " +
					" AND d.ic_epo = e.ic_epo " +										      
					" AND s.ic_estatus_solic = es.ic_estatus_solic " +
					" AND m.ic_moneda = d.ic_moneda " +
					condicion +
					" GROUP BY s.ic_folio, " +
					" i.cg_razon_social, " +
					" e.cg_razon_social, " +
					" p.cg_razon_social, " +
					" m.cd_nombre, " +
					" s.df_operacion, " +
					" d.cs_dscto_especial, " +
					" s.df_cancelacion, " +
					" s.ig_numero_prestamo, " +
					" ds.in_importe_recibir, " +
					" es.cd_descripcion "
				);
							
				} else if (ic_producto.equals("2"))  { // Anticipos
			
					if (!ic_if.equals("") && !ic_if.equals("0") ){
					condicion += " AND i.ic_if= "+ ic_if;
				}
				if (!ig_numero_prestamo.equals("")){
					condicion += " AND a.ig_numero_prestamo IN(" + ig_numero_prestamo + ")";
				}
				if (!df_operacion.equals("")){										
					condicion += " AND a.df_operacion >= to_date('" + df_operacion + "', 'dd/mm/yyyy' )"
										+  " AND a.df_operacion < to_date('" + df_operacion + "', 'dd/mm/yyyy') + 1";
				}
				if (!df_cancelacion.equals("")){															
					condicion += " AND a.df_cancelacion >= to_date('" + df_cancelacion + "', 'dd/mm/yyyy' )"
										+  " AND a.df_cancelacion < to_date('" + df_cancelacion + "', 'dd/mm/yyyy') + 1";
				}					
				qrySentencia.append(
					"SELECT '2' AS claveproducto, 'Financiamiento a Pedidos' AS nombreproducto, " +
					"	i.cg_razon_social AS intermediario, e.cg_razon_social AS epo, " + 
					"	p.cg_razon_social AS pyme, a.ic_pedido AS nosolicitud, " +
					"	m.cd_nombre AS moneda, 'N/A' AS tipofactoraje, " +
					"	TO_CHAR (a.df_operacion, 'DD/MM/YYYY') AS dfoperacion, " +
					"	a.ig_numero_prestamo AS numeroprestamo, " +
					"	ps.fn_credito_recibir AS montooperar, " +
					"	es.cd_descripcion AS estatusactual, " +
					"	TO_CHAR (a.df_cancelacion, 'DD/MM/YYYY') AS fechacancelacion " +
					"FROM com_anticipo a, " +					
					"	com_pedido_seleccionado ps, " +
					"	com_linea_credito lc, " +
					"	com_pedido ped, " +
					"	comcat_if i, " +
					"	comcat_pyme p, " +
					"	comcat_epo e, " +
					"	comcat_moneda m, " +
					"	comcat_estatus_solic es " +
					"WHERE a.ic_pedido = ps.ic_pedido " +
					"	AND ps.ic_pedido = ped.ic_pedido " +
					"	AND lc.ic_linea_credito = ps.ic_linea_credito " +
					"	AND lc.ic_if = i.ic_if " +
					"	AND ped.ic_moneda = m.ic_moneda " +
					"	AND ped.ic_pyme = p.ic_pyme " +
					"	AND ped.ic_epo = e.ic_epo " +
					"	AND a.ic_estatus_antic = es.ic_estatus_solic " +
					"	AND a.ic_bloqueo = 5 " +					
					"	AND a.ic_estatus_antic = 1 " +
				    condicion											                                                              
				);
					
				} else if (ic_producto.equals("4")) { // Financiamiento a Distribuidores
					
					if (!ic_if.equals("") && !ic_if.equals("0") ){
					condicion += " AND lc.ic_if= "+ ic_if;
				}
				if (!ig_numero_prestamo.equals("")){
					condicion += " AND sol.ig_numero_prestamo IN(" + ig_numero_prestamo + ")";
				}
				if (!df_operacion.equals("")){										
					condicion += " AND sol.df_operacion >= to_date('" + df_operacion + "', 'dd/mm/yyyy' )"
										+  " AND sol.df_operacion < to_date('" + df_operacion + "', 'dd/mm/yyyy') + 1";
				}
				if (!df_cancelacion.equals("")){										
					condicion += " AND sol.df_cancelacion >= to_date('" + df_cancelacion + "', 'dd/mm/yyyy' )"
										+  " AND sol.df_cancelacion < to_date('" + df_cancelacion + "', 'dd/mm/yyyy') + 1";
				}					
				qrySentencia.append(
					"	SELECT  " +
					"	TO_CHAR (doc.ic_documento) AS nosolicitud" +
					"	FROM comcat_moneda m, " +
					"	comcat_epo epo, " +
					"	comcat_pyme p, " +
					"	comcat_if i, " +
					" 	comcat_estatus_solic es, " +
					"	comcat_tasa tas, " +
					"	dis_linea_credito_dm lc, " +
					" 	dis_documento doc, " +
					"	dis_docto_seleccionado sel, " +
					"	dis_solicitud sol " +
					"	WHERE es.ic_estatus_solic = sol.ic_estatus_solic " +
					" 	AND sol.ic_documento = doc.ic_documento " +
					"	AND doc.ic_documento = sel.ic_documento " +
					"	AND sel.ic_tasa = tas.ic_tasa " +
					"	AND m.ic_moneda = tas.ic_moneda " +
					" 	AND doc.ic_epo = epo.ic_epo " +
					" 	AND doc.ic_linea_credito_dm = lc.ic_linea_credito_dm " +
					" 	AND lc.ic_if = i.ic_if " +
					" 	AND doc.ic_pyme = p.ic_pyme " +
					"	AND sol.ic_estatus_solic = 1 " +
					" 	AND sol.ic_bloqueo = 5 " +
						condicion +																								                                                                                                                  																																														
					" 	UNION " +
					"	SELECT  " +
					"	 TO_CHAR (doc.ic_documento) AS nosolicitud " +
					"	FROM comcat_moneda m, " +
					" 	comcat_epo epo, " +
					"	comcat_pyme p, " +
					" 	comcat_if i, " +
					" 	comcat_estatus_solic es, " +
					" 	comcat_tasa tas, " +
					"	com_linea_credito lc, " +
					" 	dis_documento doc, " +
					" 	dis_docto_seleccionado sel, " +
					" 	dis_solicitud sol " +
					"	WHERE es.ic_estatus_solic = sol.ic_estatus_solic " +
					" 	AND sol.ic_documento = doc.ic_documento " +
					"	AND doc.ic_documento = sel.ic_documento " +
					"	AND sel.ic_tasa = tas.ic_tasa " +
					"	AND m.ic_moneda = tas.ic_moneda " +
					"	AND doc.ic_epo = epo.ic_epo " +
					"	AND doc.ic_linea_credito = lc.ic_linea_credito " +
					"	AND lc.ic_if = i.ic_if " +
					"	AND doc.ic_pyme = p.ic_pyme " +
					" 	AND sol.ic_estatus_solic = 1 " +
					"  AND sol.ic_bloqueo = 5 " + 				    
						condicion 
				);
						
				} else if (ic_producto.equals("5")) { // Credicadenas
					
					if (!ic_if.equals("") && !ic_if.equals("0") ){
					condicion += " AND lc.ic_if= "+ ic_if;
				}
				if (!ig_numero_prestamo.equals("")){
					condicion += " AND sol.ig_numero_prestamo IN(" + ig_numero_prestamo + ")";
				}
				if (!df_operacion.equals("")){										
					condicion += " AND sol.df_operacion >= to_date('" + df_operacion + "', 'dd/mm/yyyy' )"
										+  " AND sol.df_operacion < to_date('" + df_operacion + "', 'dd/mm/yyyy') + 1";
				}
				if (!df_cancelacion.equals("")){
					condicion += " AND sol.df_cancelacion >= to_date('" + df_cancelacion + "', 'dd/mm/yyyy' )"
										+  " AND sol.df_cancelacion < to_date('" + df_cancelacion + "', 'dd/mm/yyyy') + 1";					
				}					
				qrySentencia.append(
					"SELECT TO_CHAR (disp.ic_producto_nafin) AS claveproducto, " +
					"	'Credicadenas' AS nombreproducto, i.cg_razon_social AS intermediario, " +
					"	epo.cg_razon_social AS epo, p.cg_razon_social AS pyme, " +
					"	TO_CHAR (disp.cc_disposicion) AS nosolicitud, m.cd_nombre AS moneda, " +
					"	'N/A' AS tipofactoraje, " +
					"	TO_CHAR (sol.df_operacion, 'DD/MM/YYYY') AS dfoperacion, " +
					"	sol.ig_numero_prestamo AS numeroprestamo, " +
					"	disp.fn_monto_credito AS montooperar, " +
					"	es.cd_descripcion AS estatusactual, " +
					"	TO_CHAR (sol.df_cancelacion, 'DD/MM/YYYY') AS fechacancelacion " +
					"FROM comcat_moneda m, " +
					"	comcat_epo epo, " +
					"	comcat_pyme p, " +
					"	comcat_if i, " +
					"	comcat_estatus_solic es, " +
					"	comcat_tasa tas, " +
					"	com_linea_credito lc, " +
					"	inv_disposicion disp, " +
					"	inv_solicitud sol " +
					"WHERE es.ic_estatus_solic = sol.ic_estatus_solic " +
					"AND sol.cc_disposicion = disp.cc_disposicion " +
					"AND disp.ic_tasa = tas.ic_tasa " +
					"AND m.ic_moneda = tas.ic_moneda " +
					"AND disp.ic_epo = epo.ic_epo " +
					"AND disp.ic_linea_credito = lc.ic_linea_credito " +
					"AND lc.ic_if = i.ic_if " +
					"AND disp.ic_pyme = p.ic_pyme " +
					"AND sol.ic_estatus_solic = 1 " +
					"AND sol.ic_bloqueo = 5 " +
					condicion +
					 " ORDER BY claveProducto ASC "
				);
						
				} 
				
			
			
			
		log.debug("ic_producto  ---->"+ic_producto);		
		log.debug("qrySentencia  "+qrySentencia);
		log.debug("conditions "+conditions);
		
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		qryCondicion 	= new StringBuffer();
		conditions 		= new ArrayList();
		String condicion = "";
		String hint ="";
		
		
			if (ic_producto.equals("0"))  { // Credito Electronico 
				if (!ic_if.equals("") && !ic_if.equals("0") ){
					condicion += " AND sp.ic_if= "+ ic_if;
				}
				if (!ig_numero_prestamo.equals("")){
					condicion += " AND sp.ig_numero_prestamo IN(" + ig_numero_prestamo + ")";
				}
				if (!df_operacion.equals("")){										
					condicion += " AND sp.df_operacion >= to_date('" + df_operacion + "', 'dd/mm/yyyy' )"
										+  " AND sp.df_operacion < to_date('" + df_operacion + "', 'dd/mm/yyyy') + 1";
				}
				if (!df_cancelacion.equals("")){													
					condicion += " AND sp.df_cancelacion >= to_date('" + df_cancelacion + "', 'dd/mm/yyyy' )"
										+  " AND sp.df_cancelacion < to_date('" + df_cancelacion + "', 'dd/mm/yyyy') + 1";
				}	
				
					qrySentencia.append(
					"SELECT /*+ index(sp IN_COM_SOLIC_PORTAL_01_NUK) use_nl(sp es p i m) */ " +
					"'0' AS claveproducto, 'Credito Electronico' AS nombreproducto, " +
					"		 i.cg_razon_social AS intermediario, 'N/A' AS epo, " +
					"		 p.cg_razon_social AS pyme, sp.ic_solic_portal AS nosolicitud, " +
					"		 m.cd_nombre AS moneda, 'N/A' AS tipofactoraje, " +
					"		 TO_CHAR (sp.df_operacion, 'DD/MM/YYYY') AS dfoperacion, " +
					"		 sp.ig_numero_prestamo AS numeroprestamo, " +
					"		 sp.fn_importe_dscto AS montooperar, " +
					"		 es.cd_descripcion AS estatusactual, " +
					"		 TO_CHAR (sp.df_cancelacion, 'DD/MM/YYYY') AS fechacancelacion " +
					" FROM comcat_moneda m, " +
					"		 comcat_pyme p, " +
					"		 comcat_estatus_solic es, "+					
					"		 comcat_if i, " +					
					"		 com_solic_portal sp " +
				 "WHERE es.ic_estatus_solic = sp.ic_estatus_solic " +
					" AND m.ic_moneda = sp.ic_moneda " +
					" AND sp.ic_if = i.ic_if " +
					" AND sp.ig_clave_sirac = p.in_numero_sirac(+) " +										
					" AND sp.ic_estatus_solic = 4 " +
					" AND sp.ic_bloqueo = 5 " +
				    condicion
				);
				qrySentencia.append(" AND (");
				for (int i = 0; i < pageIds.size(); i++) { 
				List lItem = (ArrayList)pageIds.get(i);
				
				if(i > 0){qrySentencia.append("  OR  ");}
				
					qrySentencia.append(" sp.ic_solic_portal = ? " );
					conditions.add(new Long(lItem.get(0).toString()));
				}							
			qrySentencia.append(" ) ");		
			qrySentencia.append(" ORDER BY claveProducto ASC   ");		
	} else if (ic_producto.equals("1"))	 { 
									
				if (!ic_if.equals("") && !ic_if.equals("0") ){
					condicion += " AND ds.ic_if= "+ ic_if;
				}
				if (!ig_numero_prestamo.equals("")){
					condicion += " AND s.ig_numero_prestamo IN(" + ig_numero_prestamo + ")";
				}
				if (!df_operacion.equals("")){										
					condicion += " AND s.df_operacion >= to_date('" + df_operacion + "', 'dd/mm/yyyy' )"
										+  " AND s.df_operacion < to_date('" + df_operacion + "', 'dd/mm/yyyy') + 1";
				}
				if (!df_cancelacion.equals("")){										
					condicion += " AND s.df_cancelacion >= to_date('" + df_cancelacion + "', 'dd/mm/yyyy' )"
										+  " AND s.df_cancelacion < to_date('" + df_cancelacion + "', 'dd/mm/yyyy') + 1";
				}					
				qrySentencia.append(
					"SELECT /*+ index(s IN_COM_SOLICITUD_10_NUK) use_nl(s ds d i p e m es) */ " +
					"'0' AS claveproducto, 'Descuento Electronico' AS nombreproducto, " +
					"		 i.cg_razon_social AS intermediario, e.cg_razon_social AS epo, " +
					"		 p.cg_razon_social AS pyme, s.ic_folio AS nosolicitud, " +
					"		 m.cd_nombre AS moneda, " +
					"		 TO_CHAR (s.df_operacion, 'DD/MM/YYYY') AS dfoperacion, " +
					"		 DECODE (d.cs_dscto_especial, " +
					"						 'N', 'Normal', " +
					"						 'V', 'Vencido' " +
					"						) AS tipofactoraje, " +						
					"		 s.ig_numero_prestamo AS numeroprestamo, " +
					"		 ds.in_importe_recibir AS montooperar, " +
					"		 es.cd_descripcion AS estatusactual, " +
					"		 TO_CHAR (s.df_cancelacion, 'DD/MM/YYYY') AS fechacancelacion " +
					"FROM com_solicitud s, " +
					"		 com_docto_seleccionado ds, " +
					"		 com_documento d, " +
					"		 comcat_if i, " +
					"		 comcat_pyme p, " +
					"		 comcat_epo e, " +
					"		 comcat_moneda m, " +
					"		 comcat_estatus_solic es " +
				 "WHERE s.ic_estatus_solic = 1 " +
					" AND s.ic_bloqueo = 5 " +
					" AND s.ic_documento = ds.ic_documento " +
					" AND ds.ic_documento = d.ic_documento " +
					" AND ds.ic_linea_credito_emp IS NULL " +
					" AND ds.ic_if = i.ic_if " +
					" AND d.ic_pyme = p.ic_pyme " +
					" AND d.ic_epo = e.ic_epo " +										      
					" AND s.ic_estatus_solic = es.ic_estatus_solic " +
					" AND m.ic_moneda = d.ic_moneda " +
					condicion );
					qrySentencia.append(" AND (");
						for (int i = 0; i < pageIds.size(); i++) { 
						List lItem = (ArrayList)pageIds.get(i);
						
						if(i > 0){qrySentencia.append("  OR  ");}
						
							qrySentencia.append("s.ic_folio = ? " );
							conditions.add(new Long(lItem.get(0).toString()));
						}							
					qrySentencia.append(" ) ");	
					qrySentencia.append(" GROUP BY s.ic_folio, " +
					" i.cg_razon_social, " +
					" e.cg_razon_social, " +
					" p.cg_razon_social, " +
					" m.cd_nombre, " +
					" s.df_operacion, " +
					" d.cs_dscto_especial, " +
					" s.df_cancelacion, " +
					" s.ig_numero_prestamo, " +
					" ds.in_importe_recibir, " +
					" es.cd_descripcion "
				);
							
				} else if (ic_producto.equals("2"))  { // Anticipos
			
					if (!ic_if.equals("") && !ic_if.equals("0") ){
					condicion += " AND i.ic_if= "+ ic_if;
				}
				if (!ig_numero_prestamo.equals("")){
					condicion += " AND a.ig_numero_prestamo IN(" + ig_numero_prestamo + ")";
				}
				if (!df_operacion.equals("")){										
					condicion += " AND a.df_operacion >= to_date('" + df_operacion + "', 'dd/mm/yyyy' )"
										+  " AND a.df_operacion < to_date('" + df_operacion + "', 'dd/mm/yyyy') + 1";
				}
				if (!df_cancelacion.equals("")){															
					condicion += " AND a.df_cancelacion >= to_date('" + df_cancelacion + "', 'dd/mm/yyyy' )"
										+  " AND a.df_cancelacion < to_date('" + df_cancelacion + "', 'dd/mm/yyyy') + 1";
				}					
				qrySentencia.append(
					"SELECT '2' AS claveproducto, 'Financiamiento a Pedidos' AS nombreproducto, " +
					"	i.cg_razon_social AS intermediario, e.cg_razon_social AS epo, " + 
					"	p.cg_razon_social AS pyme, a.ic_pedido AS nosolicitud, " +
					"	m.cd_nombre AS moneda, 'N/A' AS tipofactoraje, " +
					"	TO_CHAR (a.df_operacion, 'DD/MM/YYYY') AS dfoperacion, " +
					"	a.ig_numero_prestamo AS numeroprestamo, " +
					"	ps.fn_credito_recibir AS montooperar, " +
					"	es.cd_descripcion AS estatusactual, " +
					"	TO_CHAR (a.df_cancelacion, 'DD/MM/YYYY') AS fechacancelacion " +
					"FROM com_anticipo a, " +					
					"	com_pedido_seleccionado ps, " +
					"	com_linea_credito lc, " +
					"	com_pedido ped, " +
					"	comcat_if i, " +
					"	comcat_pyme p, " +
					"	comcat_epo e, " +
					"	comcat_moneda m, " +
					"	comcat_estatus_solic es " +
					"WHERE a.ic_pedido = ps.ic_pedido " +
					"	AND ps.ic_pedido = ped.ic_pedido " +
					"	AND lc.ic_linea_credito = ps.ic_linea_credito " +
					"	AND lc.ic_if = i.ic_if " +
					"	AND ped.ic_moneda = m.ic_moneda " +
					"	AND ped.ic_pyme = p.ic_pyme " +
					"	AND ped.ic_epo = e.ic_epo " +
					"	AND a.ic_estatus_antic = es.ic_estatus_solic " +
					"	AND a.ic_bloqueo = 5 " +					
					"	AND a.ic_estatus_antic = 1 " +
				    condicion											                                                              
				);
					
				} else if (ic_producto.equals("4")) { // Financiamiento a Distribuidores
					
					if (!ic_if.equals("") && !ic_if.equals("0") ){
					condicion += " AND lc.ic_if= "+ ic_if;
				}
				if (!ig_numero_prestamo.equals("")){
					condicion += " AND sol.ig_numero_prestamo IN(" + ig_numero_prestamo + ")";
				}
				if (!df_operacion.equals("")){										
					condicion += " AND sol.df_operacion >= to_date('" + df_operacion + "', 'dd/mm/yyyy' )"
										+  " AND sol.df_operacion < to_date('" + df_operacion + "', 'dd/mm/yyyy') + 1";
				}
				if (!df_cancelacion.equals("")){										
					condicion += " AND sol.df_cancelacion >= to_date('" + df_cancelacion + "', 'dd/mm/yyyy' )"
										+  " AND sol.df_cancelacion < to_date('" + df_cancelacion + "', 'dd/mm/yyyy') + 1";
				}					
				qrySentencia.append(
					"SELECT TO_CHAR (doc.ic_producto_nafin) AS claveproducto, " +
					"	'Financiamiento a Distribuidores' AS nombreproducto, " +
					"	i.cg_razon_social AS intermediario, epo.cg_razon_social AS epo, " +
					"	p.cg_razon_social AS pyme, TO_CHAR (doc.ic_documento) AS nosolicitud, " +
					"	m.cd_nombre AS moneda, 'N/A' AS tipofactoraje, " +
					"	TO_CHAR (sol.df_operacion, 'DD/MM/YYYY') AS dfoperacion, " +
					"	sol.ig_numero_prestamo AS numeroprestamo, " +
					"	sel.fn_importe_recibir AS montooperar, " +
					"	es.cd_descripcion AS estatusactual, " +
					"	TO_CHAR (sol.df_cancelacion, 'DD/MM/YYYY') AS fechacancelacion " +
					"FROM comcat_moneda m, " +
					"	comcat_epo epo, " +
					"	comcat_pyme p, " +
					"	comcat_if i, " +
					" comcat_estatus_solic es, " +
					"	comcat_tasa tas, " +
					"	dis_linea_credito_dm lc, " +
					" dis_documento doc, " +
					"	dis_docto_seleccionado sel, " +
					"	dis_solicitud sol " +
					"WHERE es.ic_estatus_solic = sol.ic_estatus_solic " +
					" AND sol.ic_documento = doc.ic_documento " +
					"	AND doc.ic_documento = sel.ic_documento " +
					"	AND sel.ic_tasa = tas.ic_tasa " +
					"	AND m.ic_moneda = tas.ic_moneda " +
					" AND doc.ic_epo = epo.ic_epo " +
					" AND doc.ic_linea_credito_dm = lc.ic_linea_credito_dm " +
					" AND lc.ic_if = i.ic_if " +
					" AND doc.ic_pyme = p.ic_pyme " +
					" AND sol.ic_estatus_solic = 1 " +
					" AND sol.ic_bloqueo = 5 " +
						condicion );
					qrySentencia.append(" AND (");
						for (int i = 0; i < pageIds.size(); i++) { 
						List lItem = (ArrayList)pageIds.get(i);
						
						if(i > 0){qrySentencia.append("  OR  ");}
						
							qrySentencia.append("doc.ic_documento = ? " );
							conditions.add(new Long(lItem.get(0).toString()));
						}							
					qrySentencia.append(" ) ");	
						
					qrySentencia.append( " UNION " +
					"	SELECT TO_CHAR (doc.ic_producto_nafin) AS claveproducto, " +
					"	'Financiamiento a Distribuidores' AS nombreproducto, " +
					"	i.cg_razon_social AS intermediario, epo.cg_razon_social AS epo, " +
					" 	p.cg_razon_social AS pyme, TO_CHAR (doc.ic_documento) AS nosolicitud, " +
					" 	m.cd_nombre AS moneda, 'N/A' AS tipofactoraje, " +
					" 	TO_CHAR (sol.df_operacion, 'DD/MM/YYYY') AS dfoperacion, " +
					" 	sol.ig_numero_prestamo AS numeroprestamo, " +
					"	 sel.fn_importe_recibir AS montooperar, " +
					"	 es.cd_descripcion AS estatusactual, " +
					"	 TO_CHAR (sol.df_cancelacion, 'DD/MM/YYYY') AS fechacancelacion " +															                     										
					"	FROM comcat_moneda m, " +
					" comcat_epo epo, " +
					"	comcat_pyme p, " +
					" comcat_if i, " +
					" comcat_estatus_solic es, " +
					" comcat_tasa tas, " +
					" com_linea_credito lc, " +
					" dis_documento doc, " +
					" dis_docto_seleccionado sel, " +
					" dis_solicitud sol " +
					"	WHERE es.ic_estatus_solic = sol.ic_estatus_solic " +
					"	 AND sol.ic_documento = doc.ic_documento " +
					"	AND doc.ic_documento = sel.ic_documento " +
					"	AND sel.ic_tasa = tas.ic_tasa " +
					"	AND m.ic_moneda = tas.ic_moneda " +
					"	AND doc.ic_epo = epo.ic_epo " +
					"	AND doc.ic_linea_credito = lc.ic_linea_credito " +
					"	AND lc.ic_if = i.ic_if " +
					"	AND doc.ic_pyme = p.ic_pyme " +
					" 	AND sol.ic_estatus_solic = 1 " +
					"	AND sol.ic_bloqueo = 5 " + 				    
						condicion );
					qrySentencia.append(" AND (");
						for (int i = 0; i < pageIds.size(); i++) { 
						List lItem = (ArrayList)pageIds.get(i);
						
						if(i > 0){qrySentencia.append("  OR  ");}
						
							qrySentencia.append("doc.ic_documento = ? " );
							conditions.add(new Long(lItem.get(0).toString()));
						}							
					qrySentencia.append(" ) ");
					qrySentencia.append( " ORDER BY claveProducto ASC "
					);
						
				} else if (ic_producto.equals("5")) { // Credicadenas
					
					if (!ic_if.equals("") && !ic_if.equals("0") ){
					condicion += " AND lc.ic_if= "+ ic_if;
				}
				if (!ig_numero_prestamo.equals("")){
					condicion += " AND sol.ig_numero_prestamo IN(" + ig_numero_prestamo + ")";
				}
				if (!df_operacion.equals("")){										
					condicion += " AND sol.df_operacion >= to_date('" + df_operacion + "', 'dd/mm/yyyy' )"
										+  " AND sol.df_operacion < to_date('" + df_operacion + "', 'dd/mm/yyyy') + 1";
				}
				if (!df_cancelacion.equals("")){
					condicion += " AND sol.df_cancelacion >= to_date('" + df_cancelacion + "', 'dd/mm/yyyy' )"
										+  " AND sol.df_cancelacion < to_date('" + df_cancelacion + "', 'dd/mm/yyyy') + 1";					
				}					
				qrySentencia.append(
					"SELECT TO_CHAR (disp.ic_producto_nafin) AS claveproducto, " +
					"	'Credicadenas' AS nombreproducto, i.cg_razon_social AS intermediario, " +
					"	epo.cg_razon_social AS epo, p.cg_razon_social AS pyme, " +
					"	TO_CHAR (disp.cc_disposicion) AS nosolicitud, m.cd_nombre AS moneda, " +
					"	'N/A' AS tipofactoraje, " +
					"	TO_CHAR (sol.df_operacion, 'DD/MM/YYYY') AS dfoperacion, " +
					"	sol.ig_numero_prestamo AS numeroprestamo, " +
					"	disp.fn_monto_credito AS montooperar, " +
					"	es.cd_descripcion AS estatusactual, " +
					"	TO_CHAR (sol.df_cancelacion, 'DD/MM/YYYY') AS fechacancelacion " +
					"FROM comcat_moneda m, " +
					"	comcat_epo epo, " +
					"	comcat_pyme p, " +
					"	comcat_if i, " +
					"	comcat_estatus_solic es, " +
					"	comcat_tasa tas, " +
					"	com_linea_credito lc, " +
					"	inv_disposicion disp, " +
					"	inv_solicitud sol " +
					"WHERE es.ic_estatus_solic = sol.ic_estatus_solic " +
					"AND sol.cc_disposicion = disp.cc_disposicion " +
					"AND disp.ic_tasa = tas.ic_tasa " +
					"AND m.ic_moneda = tas.ic_moneda " +
					"AND disp.ic_epo = epo.ic_epo " +
					"AND disp.ic_linea_credito = lc.ic_linea_credito " +
					"AND lc.ic_if = i.ic_if " +
					"AND disp.ic_pyme = p.ic_pyme " +
					"AND sol.ic_estatus_solic = 1 " +
					"AND sol.ic_bloqueo = 5 " +
					condicion +
					 " ORDER BY claveProducto ASC "
				);
						
				} 
				
			
			
			
		log.debug("ic_producto  ---->"+ic_producto);		
		log.debug("qrySentencia  "+qrySentencia);
		log.debug("conditions "+conditions);
		
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info(" getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		qryCondicion 	= new StringBuffer();
		conditions 		= new ArrayList();
		String condicion = "";
		String hint ="";
		
		
			if (ic_producto.equals("0"))  { // Credito Electronico 
				if (!ic_if.equals("") && !ic_if.equals("0") ){
					condicion += " AND sp.ic_if= "+ ic_if;
				}
				if (!ig_numero_prestamo.equals("")){
					condicion += " AND sp.ig_numero_prestamo IN(" + ig_numero_prestamo + ")";
				}
				if (!df_operacion.equals("")){										
					condicion += " AND sp.df_operacion >= to_date('" + df_operacion + "', 'dd/mm/yyyy' )"
										+  " AND sp.df_operacion < to_date('" + df_operacion + "', 'dd/mm/yyyy') + 1";
				}
				if (!df_cancelacion.equals("")){													
					condicion += " AND sp.df_cancelacion >= to_date('" + df_cancelacion + "', 'dd/mm/yyyy' )"
										+  " AND sp.df_cancelacion < to_date('" + df_cancelacion + "', 'dd/mm/yyyy') + 1";
				}	
				
					qrySentencia.append(
					"SELECT /*+ index(sp IN_COM_SOLIC_PORTAL_01_NUK) use_nl(sp es p i m) */ " +
					"'0' AS claveproducto, 'Credito Electronico' AS nombreproducto, " +
					"		 i.cg_razon_social AS intermediario, 'N/A' AS epo, " +
					"		 p.cg_razon_social AS pyme, sp.ic_solic_portal AS nosolicitud, " +
					"		 m.cd_nombre AS moneda, 'N/A' AS tipofactoraje, " +
					"		 TO_CHAR (sp.df_operacion, 'DD/MM/YYYY') AS dfoperacion, " +
					"		 sp.ig_numero_prestamo AS numeroprestamo, " +
					"		 sp.fn_importe_dscto AS montooperar, " +
					"		 es.cd_descripcion AS estatusactual, " +
					"		 TO_CHAR (sp.df_cancelacion, 'DD/MM/YYYY') AS fechacancelacion " +
					" FROM comcat_moneda m, " +
					"		 comcat_pyme p, " +
					"		 comcat_estatus_solic es, "+					
					"		 comcat_if i, " +					
					"		 com_solic_portal sp " +
				 "WHERE es.ic_estatus_solic = sp.ic_estatus_solic " +
					" AND m.ic_moneda = sp.ic_moneda " +
					" AND sp.ic_if = i.ic_if " +
					" AND sp.ig_clave_sirac = p.in_numero_sirac(+) " +										
					" AND sp.ic_estatus_solic = 4 " +
					" AND sp.ic_bloqueo = 5 " +
				    condicion+
					 " ORDER BY claveProducto ASC "
				);
											
						
						
				} else if (ic_producto.equals("1"))	 { //Descuento //FOEA 047-2010 Rebos
									
					if (!ic_if.equals("") && !ic_if.equals("0") ){
					condicion += " AND ds.ic_if= "+ ic_if;
				}
				if (!ig_numero_prestamo.equals("")){
					condicion += " AND s.ig_numero_prestamo IN(" + ig_numero_prestamo + ")";
				}
				if (!df_operacion.equals("")){										
					condicion += " AND s.df_operacion >= to_date('" + df_operacion + "', 'dd/mm/yyyy' )"
										+  " AND s.df_operacion < to_date('" + df_operacion + "', 'dd/mm/yyyy') + 1";
				}
				if (!df_cancelacion.equals("")){										
					condicion += " AND s.df_cancelacion >= to_date('" + df_cancelacion + "', 'dd/mm/yyyy' )"
										+  " AND s.df_cancelacion < to_date('" + df_cancelacion + "', 'dd/mm/yyyy') + 1";
				}					
				qrySentencia.append(
					"SELECT /*+ index(s IN_COM_SOLICITUD_10_NUK) use_nl(s ds d i p e m es) */ " +
					"'0' AS claveproducto, 'Descuento Electronico' AS nombreproducto, " +
					"		 i.cg_razon_social AS intermediario, e.cg_razon_social AS epo, " +
					"		 p.cg_razon_social AS pyme, s.ic_folio AS nosolicitud, " +
					"		 m.cd_nombre AS moneda, " +
					"		 TO_CHAR (s.df_operacion, 'DD/MM/YYYY') AS dfoperacion, " +
					"		 DECODE (d.cs_dscto_especial, " +
					"						 'N', 'Normal', " +
					"						 'V', 'Vencido' " +
					"						) AS tipofactoraje, " +						
					"		 s.ig_numero_prestamo AS numeroprestamo, " +
					"		 ds.in_importe_recibir AS montooperar, " +
					"		 es.cd_descripcion AS estatusactual, " +
					"		 TO_CHAR (s.df_cancelacion, 'DD/MM/YYYY') AS fechacancelacion " +
					"FROM com_solicitud s, " +
					"		 com_docto_seleccionado ds, " +
					"		 com_documento d, " +
					"		 comcat_if i, " +
					"		 comcat_pyme p, " +
					"		 comcat_epo e, " +
					"		 comcat_moneda m, " +
					"		 comcat_estatus_solic es " +
				 "WHERE s.ic_estatus_solic = 1 " +
					" AND s.ic_bloqueo = 5 " +
					" AND s.ic_documento = ds.ic_documento " +
					" AND ds.ic_documento = d.ic_documento " +
					" AND ds.ic_linea_credito_emp IS NULL " +
					" AND ds.ic_if = i.ic_if " +
					" AND d.ic_pyme = p.ic_pyme " +
					" AND d.ic_epo = e.ic_epo " +										      
					" AND s.ic_estatus_solic = es.ic_estatus_solic " +
					" AND m.ic_moneda = d.ic_moneda " +
					condicion +
					" GROUP BY s.ic_folio, " +
					" i.cg_razon_social, " +
					" e.cg_razon_social, " +
					" p.cg_razon_social, " +
					" m.cd_nombre, " +
					" s.df_operacion, " +
					" d.cs_dscto_especial, " +
					" s.df_cancelacion, " +
					" s.ig_numero_prestamo, " +
					" ds.in_importe_recibir, " +
					" es.cd_descripcion "+
					 " ORDER BY claveProducto ASC "
				);
							
				} else if (ic_producto.equals("2"))  { // Anticipos
			
					if (!ic_if.equals("") && !ic_if.equals("0") ){
					condicion += " AND i.ic_if= "+ ic_if;
				}
				if (!ig_numero_prestamo.equals("")){
					condicion += " AND a.ig_numero_prestamo IN(" + ig_numero_prestamo + ")";
				}
				if (!df_operacion.equals("")){										
					condicion += " AND a.df_operacion >= to_date('" + df_operacion + "', 'dd/mm/yyyy' )"
										+  " AND a.df_operacion < to_date('" + df_operacion + "', 'dd/mm/yyyy') + 1";
				}
				if (!df_cancelacion.equals("")){															
					condicion += " AND a.df_cancelacion >= to_date('" + df_cancelacion + "', 'dd/mm/yyyy' )"
										+  " AND a.df_cancelacion < to_date('" + df_cancelacion + "', 'dd/mm/yyyy') + 1";
				}					
				qrySentencia.append(
					"SELECT '2' AS claveproducto, 'Financiamiento a Pedidos' AS nombreproducto, " +
					"	i.cg_razon_social AS intermediario, e.cg_razon_social AS epo, " + 
					"	p.cg_razon_social AS pyme, a.ic_pedido AS nosolicitud, " +
					"	m.cd_nombre AS moneda, 'N/A' AS tipofactoraje, " +
					"	TO_CHAR (a.df_operacion, 'DD/MM/YYYY') AS dfoperacion, " +
					"	a.ig_numero_prestamo AS numeroprestamo, " +
					"	ps.fn_credito_recibir AS montooperar, " +
					"	es.cd_descripcion AS estatusactual, " +
					"	TO_CHAR (a.df_cancelacion, 'DD/MM/YYYY') AS fechacancelacion " +
					"FROM com_anticipo a, " +					
					"	com_pedido_seleccionado ps, " +
					"	com_linea_credito lc, " +
					"	com_pedido ped, " +
					"	comcat_if i, " +
					"	comcat_pyme p, " +
					"	comcat_epo e, " +
					"	comcat_moneda m, " +
					"	comcat_estatus_solic es " +
					"WHERE a.ic_pedido = ps.ic_pedido " +
					"	AND ps.ic_pedido = ped.ic_pedido " +
					"	AND lc.ic_linea_credito = ps.ic_linea_credito " +
					"	AND lc.ic_if = i.ic_if " +
					"	AND ped.ic_moneda = m.ic_moneda " +
					"	AND ped.ic_pyme = p.ic_pyme " +
					"	AND ped.ic_epo = e.ic_epo " +
					"	AND a.ic_estatus_antic = es.ic_estatus_solic " +
					"	AND a.ic_bloqueo = 5 " +					
					"	AND a.ic_estatus_antic = 1 " +
				    condicion											                                                              
				);
					
				} else if (ic_producto.equals("4")) { // Financiamiento a Distribuidores
					
					if (!ic_if.equals("") && !ic_if.equals("0") ){
					condicion += " AND lc.ic_if= "+ ic_if;
				}
				if (!ig_numero_prestamo.equals("")){
					condicion += " AND sol.ig_numero_prestamo IN(" + ig_numero_prestamo + ")";
				}
				if (!df_operacion.equals("")){										
					condicion += " AND sol.df_operacion >= to_date('" + df_operacion + "', 'dd/mm/yyyy' )"
										+  " AND sol.df_operacion < to_date('" + df_operacion + "', 'dd/mm/yyyy') + 1";
				}
				if (!df_cancelacion.equals("")){										
					condicion += " AND sol.df_cancelacion >= to_date('" + df_cancelacion + "', 'dd/mm/yyyy' )"
										+  " AND sol.df_cancelacion < to_date('" + df_cancelacion + "', 'dd/mm/yyyy') + 1";
				}					
				qrySentencia.append(
					"SELECT TO_CHAR (doc.ic_producto_nafin) AS claveproducto, " +
					"	'Financiamiento a Distribuidores' AS nombreproducto, " +
					"	i.cg_razon_social AS intermediario, epo.cg_razon_social AS epo, " +
					"	p.cg_razon_social AS pyme, TO_CHAR (doc.ic_documento) AS nosolicitud, " +
					"	m.cd_nombre AS moneda, 'N/A' AS tipofactoraje, " +
					"	TO_CHAR (sol.df_operacion, 'DD/MM/YYYY') AS dfoperacion, " +
					"	sol.ig_numero_prestamo AS numeroprestamo, " +
					"	sel.fn_importe_recibir AS montooperar, " +
					"	es.cd_descripcion AS estatusactual, " +
					"	TO_CHAR (sol.df_cancelacion, 'DD/MM/YYYY') AS fechacancelacion " +
					"FROM comcat_moneda m, " +
					"	comcat_epo epo, " +
					"	comcat_pyme p, " +
					"	comcat_if i, " +
					" comcat_estatus_solic es, " +
					"	comcat_tasa tas, " +
					"	dis_linea_credito_dm lc, " +
					" dis_documento doc, " +
					"	dis_docto_seleccionado sel, " +
					"	dis_solicitud sol " +
					"WHERE es.ic_estatus_solic = sol.ic_estatus_solic " +
					" AND sol.ic_documento = doc.ic_documento " +
					"	AND doc.ic_documento = sel.ic_documento " +
					"	AND sel.ic_tasa = tas.ic_tasa " +
					"	AND m.ic_moneda = tas.ic_moneda " +
					" AND doc.ic_epo = epo.ic_epo " +
					" AND doc.ic_linea_credito_dm = lc.ic_linea_credito_dm " +
					" AND lc.ic_if = i.ic_if " +
					" AND doc.ic_pyme = p.ic_pyme " +
					" AND sol.ic_estatus_solic = 1 " +
					" AND sol.ic_bloqueo = 5 " +
						condicion +																								                                                                                                                  																																														
					" UNION " +
					"SELECT TO_CHAR (doc.ic_producto_nafin) AS claveproducto, " +
					"	'Financiamiento a Distribuidores' AS nombreproducto, " +
					"	i.cg_razon_social AS intermediario, epo.cg_razon_social AS epo, " +
					" p.cg_razon_social AS pyme, TO_CHAR (doc.ic_documento) AS nosolicitud, " +
					" m.cd_nombre AS moneda, 'N/A' AS tipofactoraje, " +
					" TO_CHAR (sol.df_operacion, 'DD/MM/YYYY') AS dfoperacion, " +
					" sol.ig_numero_prestamo AS numeroprestamo, " +
					" sel.fn_importe_recibir AS montooperar, " +
					" es.cd_descripcion AS estatusactual, " +
					" TO_CHAR (sol.df_cancelacion, 'DD/MM/YYYY') AS fechacancelacion " +															                     										
					"FROM comcat_moneda m, " +
					" comcat_epo epo, " +
					"	comcat_pyme p, " +
					" comcat_if i, " +
					" comcat_estatus_solic es, " +
					" comcat_tasa tas, " +
					" com_linea_credito lc, " +
					" dis_documento doc, " +
					" dis_docto_seleccionado sel, " +
					" dis_solicitud sol " +
					"WHERE es.ic_estatus_solic = sol.ic_estatus_solic " +
					" AND sol.ic_documento = doc.ic_documento " +
					"	AND doc.ic_documento = sel.ic_documento " +
					"	AND sel.ic_tasa = tas.ic_tasa " +
					"	AND m.ic_moneda = tas.ic_moneda " +
					"	AND doc.ic_epo = epo.ic_epo " +
					"	AND doc.ic_linea_credito = lc.ic_linea_credito " +
					"	AND lc.ic_if = i.ic_if " +
					"	AND doc.ic_pyme = p.ic_pyme " +
					" AND sol.ic_estatus_solic = 1 " +
					"	AND sol.ic_bloqueo = 5 " + 				    
						condicion +
					 " ORDER BY claveProducto ASC "
				);
						
				} else if (ic_producto.equals("5")) { // Credicadenas
					
					if (!ic_if.equals("") && !ic_if.equals("0") ){
					condicion += " AND lc.ic_if= "+ ic_if;
				}
				if (!ig_numero_prestamo.equals("")){
					condicion += " AND sol.ig_numero_prestamo IN(" + ig_numero_prestamo + ")";
				}
				if (!df_operacion.equals("")){										
					condicion += " AND sol.df_operacion >= to_date('" + df_operacion + "', 'dd/mm/yyyy' )"
										+  " AND sol.df_operacion < to_date('" + df_operacion + "', 'dd/mm/yyyy') + 1";
				}
				if (!df_cancelacion.equals("")){
					condicion += " AND sol.df_cancelacion >= to_date('" + df_cancelacion + "', 'dd/mm/yyyy' )"
										+  " AND sol.df_cancelacion < to_date('" + df_cancelacion + "', 'dd/mm/yyyy') + 1";					
				}					
				qrySentencia.append(
					"SELECT TO_CHAR (disp.ic_producto_nafin) AS claveproducto, " +
					"	'Credicadenas' AS nombreproducto, i.cg_razon_social AS intermediario, " +
					"	epo.cg_razon_social AS epo, p.cg_razon_social AS pyme, " +
					"	TO_CHAR (disp.cc_disposicion) AS nosolicitud, m.cd_nombre AS moneda, " +
					"	'N/A' AS tipofactoraje, " +
					"	TO_CHAR (sol.df_operacion, 'DD/MM/YYYY') AS dfoperacion, " +
					"	sol.ig_numero_prestamo AS numeroprestamo, " +
					"	disp.fn_monto_credito AS montooperar, " +
					"	es.cd_descripcion AS estatusactual, " +
					"	TO_CHAR (sol.df_cancelacion, 'DD/MM/YYYY') AS fechacancelacion " +
					"FROM comcat_moneda m, " +
					"	comcat_epo epo, " +
					"	comcat_pyme p, " +
					"	comcat_if i, " +
					"	comcat_estatus_solic es, " +
					"	comcat_tasa tas, " +
					"	com_linea_credito lc, " +
					"	inv_disposicion disp, " +
					"	inv_solicitud sol " +
					"WHERE es.ic_estatus_solic = sol.ic_estatus_solic " +
					"AND sol.cc_disposicion = disp.cc_disposicion " +
					"AND disp.ic_tasa = tas.ic_tasa " +
					"AND m.ic_moneda = tas.ic_moneda " +
					"AND disp.ic_epo = epo.ic_epo " +
					"AND disp.ic_linea_credito = lc.ic_linea_credito " +
					"AND lc.ic_if = i.ic_if " +
					"AND disp.ic_pyme = p.ic_pyme " +
					"AND sol.ic_estatus_solic = 1 " +
					"AND sol.ic_bloqueo = 5 " +
					condicion +
					 " ORDER BY claveProducto ASC "
				);
						
				} 
				
			
			
			
		log.debug("ic_producto  ---->"+ic_producto);		
		log.debug("qrySentencia  "+qrySentencia);
		log.debug("conditions "+conditions);
		
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
			
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		int total =0;
		try {
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);
			contenidoArchivo = new StringBuffer();	
			contenidoArchivo.append("Producto,Intermediario,EPO,PYME,No. Solicitud, Moneda, Tipo de Factoraje, Fecha de Operacion, No. de Prestamo, Monto a Operar, Estatus Actual, Fecha Cancelacion\r\n");		

			while(rs != null && rs.next()){
					
					String claveProduc  = rs.getString("claveProducto");
					String nombreProd = rs.getString("nombreProducto");
					String interm = rs.getString("intermediario");
					String epo = rs.getString("epo");
					String pyme = rs.getString("pyme");
					String nosolic = rs.getString("nosolicitud");
					String moneda = rs.getString("moneda");
					String tipofactor = rs.getString("tipofactoraje");
					String dfopera = rs.getString("dfoperacion");		
					String numprestamo = rs.getString("numeroprestamo");
					String montooperar = rs.getString("montooperar");
					String estatusactual = rs.getString("estatusactual");
					String dfcancelacion = rs.getString("fechacancelacion");										
									
					claveProduc = claveProduc == null ? "" : claveProduc;
					nombreProd = nombreProd == null ? "" : nombreProd;
					interm = interm == null ? "" : interm;
					epo = epo == null ? "" : epo;
					pyme = pyme == null ? "" : pyme;
					nosolic = nosolic == null ? "" : nosolic;
					moneda = moneda == null ? "" : moneda;
					tipofactor = tipofactor == null ? "" : tipofactor;
					dfopera = dfopera == null ? "" : dfopera;			
					numprestamo = numprestamo == null ? "" : numprestamo;
					montooperar = montooperar == null ? "" : montooperar;
					estatusactual = estatusactual == null ? "" : estatusactual;
					dfcancelacion = dfcancelacion == null ? "" : dfcancelacion;
										
					contenidoArchivo.append("\""+nombreProd+"\",\""+interm+"\",\""+epo+"\",\""+pyme+"\",\""+nosolic+"\",\""+moneda
																	+"\",\""+tipofactor+"\",\""+dfopera+"\",\""+numprestamo+"\",\""+montooperar+"\",\""+estatusactual+"\",\""+dfcancelacion+"\"\r\n");
					total++;
					if(total==1000){					
						total=0;	
						buffer.write(contenidoArchivo.toString());
						contenidoArchivo = new StringBuffer();//Limpio  
					}
			}
			//creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
			//nombreArchivo = creaArchivo.getNombre();
			buffer.write(contenidoArchivo.toString());
			buffer.close();	
			contenidoArchivo = new StringBuffer();//Limpio    
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
	


	public String  procesarCancela(int numRegistros, String []  folios,  String  ic_producto,  String []  productos ){
		
		log.info("procesarCancela (E)");
	
		AccesoDB con =new AccesoDB();
		PreparedStatement ps	= null;
		ResultSet			rs1	= null;	
		String mensaje ="", qrySentencia ="";
		int numRegistrosAfectar =0;
		boolean errorConcurrencia=true;

		try{
		
			con.conexionDB();
			
			for(int i=0; i<numRegistros; i++) {
		
				String solicitud =   folios[i];				
				String producto =   productos[i];
									
				if (ic_producto.equals("1")|| producto.equals("FR"))	{	
					qrySentencia = " SELECT count(*) "+
						 " FROM com_solicitud "+
						 " WHERE ic_folio ='"+solicitud+"'"+
						 "	AND ic_estatus_solic = 3";
						 
					} else if (ic_producto.equals("2")||producto.equals("FC"))	{	
						qrySentencia = " SELECT count(*) "+
						 " FROM com_anticipo "+
						 " WHERE ic_pedido ='"+solicitud+"'"+
						 "	AND ic_estatus_antic = 3";
						 
					} else if (ic_producto.equals("0"))	{	
						qrySentencia = " SELECT count(*) "+
						 " FROM com_solic_portal "+
						 " WHERE ic_solic_portal ='"+solicitud+"'"+
						 "	AND ic_estatus_solic = 3";
						 
					} else if (ic_producto.equals("4"))	{	
						qrySentencia = " SELECT count(*) "+
						 " FROM dis_solicitud "+
						 " WHERE ic_documento ='"+solicitud+"'"+
						 "	AND ic_estatus_solic = 3";
						 
					} else if (ic_producto.equals("5")||producto.equals("DS"))	{
						qrySentencia = " SELECT count(*) "+
						 " FROM inv_solicitud "+
						 " WHERE CC_DISPOSICION ='"+solicitud+"'"+
						 "	AND ic_estatus_solic = 3";
					}
					rs1 = con.queryDB(qrySentencia);
					numRegistrosAfectar=0;
					while (rs1.next()) {
						numRegistrosAfectar = numRegistrosAfectar + rs1.getInt(1);
					}
					rs1.close();
					con.cierraStatement();
					
					if (numRegistrosAfectar==1)	{ //Control de concurrencia (Debe existir el registro en la tabla para poder ser reprocesado o cancelado)
						errorConcurrencia=false;					
						if (ic_producto.equals("1")||producto.equals("FR"))		{
							qrySentencia = "UPDATE com_solicitud " + 
								" SET ic_estatus_solic=1, "+
								"	ic_bloqueo = 5, "+
								"	df_operacion = null,"+
								"	ig_numero_prestamo = null, "+
								"	df_cancelacion = SYSDATE "+ //FODEA 047-2010 Rebos
								" WHERE ic_folio ='"+solicitud+"'";														
							con.ejecutaSQL(qrySentencia);
							
							qrySentencia = "delete from com_control04"+
								" where ic_folio ='"+solicitud+"'";						
							con.ejecutaSQL(qrySentencia);
							
						} else if (ic_producto.equals("2")||producto.equals("FC")) 	{
							qrySentencia = "UPDATE com_anticipo " + 
								" SET ic_estatus_antic=1, "+
								"	ic_bloqueo = 5, "+
								"	df_operacion = null, "+
								"	ig_numero_prestamo = null, "+
								"	df_cancelacion = SYSDATE "+ //FODEA 047-2010 Rebos
								" WHERE ic_pedido ='"+solicitud+"'";							
							con.ejecutaSQL(qrySentencia);
							
							qrySentencia = "delete from com_controlant04"+
								" where ic_pedido ='"+solicitud+"'";
							con.ejecutaSQL(qrySentencia);						
							
						} else if (ic_producto.equals("0"))	{
							qrySentencia = " UPDATE com_solic_portal "+
								" SET ic_bloqueo=5, ic_estatus_solic=4, "+
								"	df_cancelacion = SYSDATE "+ //FODEA 047-2010 Rebos
								" WHERE ic_solic_portal = '"+solicitud+"'";
							con.ejecutaSQL(qrySentencia);
							
							qrySentencia = "delete com_interfase "+
								" where ic_solic_portal = '"+solicitud+"'";
							con.ejecutaSQL(qrySentencia);		
							
						} else if (ic_producto.equals("4"))	{ 
						
							qrySentencia = " UPDATE dis_solicitud "+
								" SET ic_bloqueo=5, ic_estatus_solic=1, "+
								" df_operacion=null, ig_numero_prestamo=null, "+	
								"	df_cancelacion = SYSDATE "+ //FODEA 047-2010 Rebos
								" WHERE ic_documento = '"+solicitud+"'";
							con.ejecutaSQL(qrySentencia);
						
							qrySentencia = "delete dis_interfase "+
								" where ic_documento = '"+solicitud+"'";
							con.ejecutaSQL(qrySentencia);		
						
							qrySentencia = "delete dishis_interfase "+
								" where ic_documento = '"+solicitud+"'";
							con.ejecutaSQL(qrySentencia);		
						
						} else if (ic_producto.equals("5")||producto.equals("DS"))	{	
							
							qrySentencia = " UPDATE inv_solicitud "+
								" SET ic_bloqueo=5, ic_estatus_solic=1, "+
								" df_operacion=null, ig_numero_prestamo=null, "+							
								"	df_cancelacion = SYSDATE "+ //FODEA 047-2010 Rebos
								" WHERE CC_DISPOSICION = '"+solicitud+"'";
							con.ejecutaSQL(qrySentencia);
							
							qrySentencia = "delete inv_interfase "+
								" where CC_DISPOSICION = '"+solicitud+"'";
							con.ejecutaSQL(qrySentencia);		
							
							qrySentencia = "delete invhis_interfase "+
								" where CC_DISPOSICION = '"+solicitud+"'";
							con.ejecutaSQL(qrySentencia);		
							
						}
					} else {	//Problema de concurrencia
						errorConcurrencia=true;
						//break;	//sale de while
					}
			}
			con.cierraStatement();
			
			if (errorConcurrencia) {
				con.terminaTransaccion(false);
				mensaje = "<span style='color:red;'> 	Existen inconsistencias en la informaci&oacute;n, debido a la "+
							" concurrencia de usuarios. El registro mostrado ya fue seleccionado"+
							" previamente. Intente nuevamente el proceso.</span>";						
			//fin de mensaje de error de concurrencia
			} else {
				con.terminaTransaccion(true);
			}			
		
		
		} catch(Exception e) {			
			e.printStackTrace();			
			log.error("error al procesarCancela  " +e); 			
		} finally {	
			if(con.hayConexionAbierta()) {				
				con.cierraConexionDB();	
			}
		}  
		log.info("procesarCancela (S)");  
		return mensaje;
	}
	
	
		
	/**
	 Obtiene la lista de parámetros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getIc_producto() {
		return ic_producto;
	}
 
	public void setIc_producto(String ic_producto) {
		this.ic_producto = ic_producto;
	}

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getIg_numero_prestamo() {
		return ig_numero_prestamo;
	}

	public void setIg_numero_prestamo(String ig_numero_prestamo) {
		this.ig_numero_prestamo = ig_numero_prestamo;
	}

	public String getDf_operacion() {
		return df_operacion;
	}

	public void setDf_operacion(String df_operacion) {
		this.df_operacion = df_operacion;
	}
	public String getDf_cancelacion() {
		return df_cancelacion;
	}

	public void setDf_cancelacion(String df_cancelacion) {
		this.df_cancelacion = df_cancelacion;
	}

	public String getSolicitudes() {
		return solicitudes;
	}

	public void setSolicitudes(String solicitudes) {
		this.solicitudes = solicitudes;
	}

	public String getPantalla() {
		return pantalla;
	}

	public void setPantalla(String pantalla) {
		this.pantalla = pantalla;
	}

	

}