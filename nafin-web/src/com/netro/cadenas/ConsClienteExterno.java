package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsClienteExterno implements IQueryGeneratorRegExtJS {

	private final static Log log = ServiceLocator.getInstance().getLog(ConsClienteExterno.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	
	private String numElectronico; 
	private String nombre;
	
	

	public ConsClienteExterno() {
	}

	/**
	 * 
	 * @return 
	 */
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	/**
	 * 
	 * @return 
	 */
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		qrySentencia.append("  SELECT 	"+		
			"  c.IC_NAFIN_ELECTRONICO AS  NUM_ELECTRONICO  "+		
			
			"   FROM COMCAT_CLI_EXTERNO  c,   "+
			"   com_domicilio d  ,  "+
			"   comcat_estado e , "+
			"   comcat_municipio m, "+
			"   comcat_pais p "+
			"  WHERE  c.IC_NAFIN_ELECTRONICO  = d.IC_NAFIN_ELECTRONICO  "+
			" AND d.ic_pais = p.ic_pais "+
			" AND d.ic_estado = e.ic_estado "+
			" AND d.ic_pais = e.ic_pais "+
			" AND d.ic_municipio = m.ic_municipio "+
			" AND d.ic_estado = m.ic_estado "+
			" AND d.ic_pais = m.ic_pais ");
			
			if(!numElectronico.equals("")){
				qrySentencia.append(" AND c.IC_NAFIN_ELECTRONICO  =?  ");
				conditions.add(numElectronico);
			}	
			
			if(!nombre.equals("")){
				qrySentencia.append("	AND (   (UPPER (TRIM (c.cg_razon_social)) LIKE UPPER (?))	"+
										"			OR (UPPER (TRIM (c.cg_nombre || ' ' || c.cg_appat || ' '	|| c.cg_apmat	)	) LIKE UPPER (?))	"+
										"		)"
										) ;
				conditions.add(nombre+"%");
				conditions.add(nombre+"%");
			}
			

		log.info("getDocumentQuery_qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}

	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		//StringBuffer condicion  = new StringBuffer("");
		
				
		qrySentencia.append("  SELECT 	"+		
		"  c.IC_NAFIN_ELECTRONICO AS  NUM_ELECTRONICO,   "+		
		"	DECODE (c.CS_TIPO_PERSONA,  'F', c.CG_NOMBRE || ' ' || c.CG_APPAT || ' ' || c.CG_APMAT ,	c.CG_RAZON_SOCIAL  ) AS NOMBRE_RAZON_SOCIAL,	"+
		"  c.CG_RFC AS  RFC ,   "+		
		"  d.CG_CALLE || ' ' || d.CG_COLONIA  as  DOMICILIO,  "+
		"  e.CD_NOMBRE as  ESTADO ,  "+
	   "  d.cg_telefono1 AS TELEFONO , "+
		"  C.IN_NUMERO_SIRAC AS NUM_SIRAC   "+
		
		"   FROM COMCAT_CLI_EXTERNO  c,   "+
		"   com_domicilio d  ,  "+
		"   comcat_estado e , "+
		"   comcat_municipio m, "+
		"   comcat_pais p "+
		"  WHERE  c.IC_NAFIN_ELECTRONICO  = d.IC_NAFIN_ELECTRONICO  "+
		" AND d.ic_pais = p.ic_pais "+
		" AND d.ic_estado = e.ic_estado "+
		" AND d.ic_pais = e.ic_pais "+
		" AND d.ic_municipio = m.ic_municipio "+
		" AND d.ic_estado = m.ic_estado "+
		" AND d.ic_pais = m.ic_pais ");
		
		qrySentencia.append(" AND (");
			
			for (int i = 0; i < pageIds.size(); i++) { 
				List lItem = (ArrayList)pageIds.get(i);
				
				if(i > 0){qrySentencia.append("  OR  ");}
				qrySentencia.append(" c.IC_NAFIN_ELECTRONICO = ? " );
				conditions.add(new Long(lItem.get(0).toString()));				
			}
			
			qrySentencia.append(" ) ");
			
			qrySentencia.append(" order by  c.IC_NAFIN_ELECTRONICO  asc  "); 
			
		
		log.info("getDocumentSummaryQueryForIds_qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();  
		
	qrySentencia.append("  SELECT 	"+		
		"  c.IC_NAFIN_ELECTRONICO AS  NUM_ELECTRONICO,   "+		
		"	DECODE (c.CS_TIPO_PERSONA,  'F', c.CG_NOMBRE || ' ' || c.CG_APPAT || ' ' || c.CG_APMAT ,	c.CG_RAZON_SOCIAL  ) AS NOMBRE_RAZON_SOCIAL,	"+
		"  c.CG_RFC AS  RFC ,   "+		
		"  d.CG_CALLE || ' ' || d.CG_COLONIA  as  DOMICILIO,  "+
		"  e.CD_NOMBRE as  ESTADO ,  "+
	   "  d.cg_telefono1 AS TELEFONO , "+
		"  C.IN_NUMERO_SIRAC AS NUM_SIRAC   "+
		
		"   FROM COMCAT_CLI_EXTERNO  c,   "+
		"   com_domicilio d  ,  "+
		"   comcat_estado e , "+
		"   comcat_municipio m, "+
		"   comcat_pais p "+
		"  WHERE  c.IC_NAFIN_ELECTRONICO  = d.IC_NAFIN_ELECTRONICO  "+
		" AND d.ic_pais = p.ic_pais "+
		" AND d.ic_estado = e.ic_estado "+
		" AND d.ic_pais = e.ic_pais "+
		" AND d.ic_municipio = m.ic_municipio "+
		" AND d.ic_estado = m.ic_estado "+
		" AND d.ic_pais = m.ic_pais "); 
			
			if(!numElectronico.equals("")){
				qrySentencia.append(" AND c.IC_NAFIN_ELECTRONICO  =?  ");  
				conditions.add(numElectronico);
			}	
			
			if(!nombre.equals("")){
				qrySentencia.append("	AND (   (UPPER (TRIM (c.cg_razon_social)) LIKE UPPER (?))	"+
										"			OR (UPPER (TRIM (c.cg_nombre || ' ' || c.cg_appat || ' '	|| c.cg_apmat	)	) LIKE UPPER (?))	"+
										"		)"
										) ;
				conditions.add(nombre+"%");
				conditions.add(nombre+"%");
			}
			
			qrySentencia.append(" order by  c.IC_NAFIN_ELECTRONICO  asc  "); 
			
		log.info("getDocumentQueryFile_qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		//CreaArchivo creaArchivo = new CreaArchivo();
		//StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
			
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
			
			pdfDoc.setTable(7,100);
			pdfDoc.setCell("N�mero de Nafin Electr�nico' ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Nombre O Raz�n Social ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("RFC ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("N�m. Sirac	","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Domicilio","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Estado ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tel�fono ","celda01",ComunesPDF.CENTER);
			
			while (rs.next())	{					
				String num_electronico = (rs.getString("NUM_ELECTRONICO") == null) ? "" : rs.getString("NUM_ELECTRONICO");
				String numSIRAC = (rs.getString("NUM_SIRAC") == null) ? "" : rs.getString("NUM_SIRAC");
				String razon_social = (rs.getString("NOMBRE_RAZON_SOCIAL").equals("")) ? "--" : rs.getString("NOMBRE_RAZON_SOCIAL");
				String rfc = (rs.getString("RFC") == null) ? "" : rs.getString("RFC");
				String domicilio = (rs.getString("DOMICILIO") == null) ? "" : rs.getString("DOMICILIO");
				String estado = (rs.getString("ESTADO") == null) ? "" : rs.getString("ESTADO");
				String telefono = (rs.getString("TELEFONO") == null) ? "" : rs.getString("TELEFONO");
					
			
				pdfDoc.setCell(num_electronico,"formas",ComunesPDF.CENTER);			
				pdfDoc.setCell(razon_social,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(rfc,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(numSIRAC,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(domicilio,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(estado,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(telefono,"formas",ComunesPDF.CENTER);
				
			}
		
			pdfDoc.addTable();
			pdfDoc.endDocument();	
			
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		//HttpSession session = request.getSession();
		//ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		//int total = 0;
		
		try {
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);
		
			contenidoArchivo = new StringBuffer();	
			contenidoArchivo.append(" N�mero de Nafin Electr�nico,  Nombre O Raz�n Social , RFC,  N�m. Sirac,  Domicilio,  Estado ,  Tel�fono \n " );
			
			
		while (rs.next())	{					
				String num_electronico = (rs.getString("NUM_ELECTRONICO") == null) ? "" : rs.getString("NUM_ELECTRONICO");
				String numSIRAC = (rs.getString("NUM_SIRAC") == null) ? "" : rs.getString("NUM_SIRAC");
				String razon_social = (rs.getString("NOMBRE_RAZON_SOCIAL").equals("")) ? "" : rs.getString("NOMBRE_RAZON_SOCIAL");
				String rfc = (rs.getString("RFC") == null) ? "" : rs.getString("RFC");
				String domicilio = (rs.getString("DOMICILIO") == null) ? "" : rs.getString("DOMICILIO");
				String estado = (rs.getString("ESTADO") == null) ? "" : rs.getString("ESTADO");
				String telefono = (rs.getString("TELEFONO") == null) ? "" : rs.getString("TELEFONO");
				 log.debug("estado "+estado);
				 log.debug("telefono "+telefono);  
				 
			
				contenidoArchivo.append(num_electronico.replace(',',' ')+", "+												
												razon_social.replace(',',' ')+", "+
												rfc.replace(',',' ')+", "+
												numSIRAC.replace(',',' ')+", "+												
												domicilio.replace(',',' ')+", "+
												estado.replace(',',' ')+", "+
												telefono.replace(',',' ')+"\n");	 	 										
			}
				
		creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
		nombreArchivo = creaArchivo.getNombre();
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}


	/**
	 * Imprimir Cuentas del Cliente Externo 
	 * @return 
	 * @param path
	 * @param tituloC
	 * @param cuentas
	 * @param request
	 */
	public String imprimirCuentas(HttpServletRequest request, List cuentas, String tituloC, String path ) {  
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		//CreaArchivo creaArchivo = new CreaArchivo();
		try {
				
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				
			
			pdfDoc.setTable(1,30);			
			pdfDoc.setCell(tituloC, "formas",ComunesPDF.LEFT);  
			pdfDoc.setCell("Login del Usuario: ", "celda02",ComunesPDF.CENTER);	
			  
			System.out.println("cuentas ----> "+cuentas); 
			
			Iterator itCuentas = cuentas.iterator();	
			while (itCuentas.hasNext()) {
				String cuenta = (String) itCuentas.next();		
				pdfDoc.setCell(cuenta, "formas",ComunesPDF.CENTER);	
			}	
			
			pdfDoc.addTable();
			pdfDoc.endDocument();
			
		 
			
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
	
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina 
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/	

	public void setNumElectronico(String numElectronico) {
		this.numElectronico = numElectronico;
	}


	public String getNumElectronico() {
		return numElectronico;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}









	


}