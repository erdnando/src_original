package com.netro.cadenas;

import com.netro.appinit.InicializacionContextListener;
import com.netro.exception.NafinException;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class DisenoIF implements Serializable {
    
    //Variable para enviar mensajes al log.
    private final static Log log = ServiceLocator.getInstance().getLog(DisenoIF.class);
	
	public DisenoIF(){};
		
	public void setClaveIF(String ic_if){
		this.claveIF = (ic_if == null)?"0":ic_if;
	}
	
	/**
	 * Obtiene la clave del IF(ic_if) 
	 */
	public String getClaveIF() {
		return (this.claveIF == null)?"0":this.claveIF;
	}
	
	public void setClaveEPO(String ic_epo){
		this.claveEPO = (ic_epo == null)?"0":ic_epo;
	}
	
	/**
	 * Obtiene la clave de la EPO(ic_epo) 
	 */
	public String getClaveEPO() {
		return (this.claveEPO == null)?"0":this.claveEPO;
	}

	/**
	 * Establece la fecha de alta o modificacion del dise�o
	 * @param fechaModificacion fecha de modificacion.
	 */
	public void setFechaModificacion(java.util.Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	
	/**
	 * Obtiene la fecha de alta o modificacion del dise�o
	 * @return (objeto Date) fecha de modificacion.
	 */
	public java.util.Date getFechaModificacion() {
		return this.fechaModificacion;
	}	
	
	public void guardar(String ic_if,String rutaArchivo) throws NafinException {
		AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		byte abyte0[] = new byte[4096];
		java.io.OutputStream outstream = null;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (ic_if == null || rutaArchivo == null) {
				throw new Exception("La clave de la epo y ruta de archivo no estan establecidos");
			}
		} catch(Exception e) {
			System.out.println("Error en los parametros establecidos. " + e.getMessage() +
					" claveIF=" + ic_if +
					" rutaArchivoZip=" + rutaArchivo);
			throw new NafinException("SIST0001");
		}
		//***************************************************************************************

		try {	
			
			File archivo = new File(rutaArchivo);
			FileInputStream fileinputstream = new FileInputStream(archivo);
	
			con.conexionDB();
			String strSQL = "";			
			strSQL = "SELECT count(*) as numRegistros " +
					" from com_diseno_if " +
					" WHERE ic_if = ? ";
			
			ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, Integer.parseInt(ic_if));
			rs = ps.executeQuery();			
			rs.next();
			boolean existeRegistro = (rs.getInt("numRegistros") == 0)?false:true;
			rs.close();
			ps.close();

			if(!existeRegistro){
				//Se mete un valor temporal '0'
				strSQL = "INSERT INTO com_diseno_if (ic_if, bi_imagen) " +
						" VALUES (?, empty_blob()) ";
				ps = con.queryPrecompilado(strSQL);
				ps.setInt(1, Integer.parseInt(ic_if));
				ps.executeUpdate();				
				ps.close();
			}

			strSQL = " UPDATE com_diseno_if " +
					" SET df_modificacion = sysdate," +
					" bi_imagen = ?" +
					" WHERE ic_if = ? ";
			
			ps = con.queryPrecompilado(strSQL);
			ps.setBinaryStream(1, fileinputstream, (int)archivo.length());
			ps.setInt(2, Integer.parseInt(ic_if));
			ps.executeUpdate();
			ps.close();

			/*strSQL = "SELECT bi_imagen " +
					" FROM com_diseno_if " +
					" WHERE ic_if = ? FOR UPDATE NOWAIT ";
			ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, Integer.parseInt(ic_if));
			rs = ps.executeQuery();			
			rs.next();
			
			java.sql.Blob blob = (java.sql.Blob)rs.getObject("bi_imagen");			
			outstream = ((oracle.sql.BLOB)blob).getBinaryOutputStream();			
			int i;
			while((i = fileinputstream.read(abyte0)) != -1){
				outstream.write(abyte0, 0, i);
			}
			outstream.close();
			rs.close();
			ps.close();*/
		}catch(Exception e){
			exito = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally{
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
	}//FIN GUARDA ARCHIVO

	public void Clonar (String archivo1, String archivo2)throws NafinException {
		try{
			InputStream is = new FileInputStream(archivo1);
			OutputStream os = new FileOutputStream(archivo2);
			// Transfer bytes from in to out
			byte[] buf = new byte[1024];
			int len;
			while ((len = is.read(buf)) > 0) {
				os.write(buf, 0, len);
			}
			is.close();
			os.close();
		}catch(Exception e){
			String strErr = e.toString();
			System.out.println("strErr"+strErr);
		}
	}//fin clonar

	public void guardarConvenio(String ic_if,String ic_epo,String rutaArchivo,String tipoArchivo) throws NafinException {
		AccesoDB con = new AccesoDB();
		boolean exito = true;
		PreparedStatement ps = null;
		ResultSet rs = null;
		byte abyte0[] = new byte[4096];
		java.io.OutputStream outstream = null;

		//**********************************Validaci�n de parametros:*****************************
		try {
			if (ic_if == null || rutaArchivo == null || ic_epo == null) {
				throw new Exception("La clave de la epo y ruta de archivo no estan establecidos");
			}
		} catch(Exception e) {
			System.out.println("Error en los parametros establecidos. " + e.getMessage() +
					" claveIF=" + ic_if +
					" claveEPO=" + ic_epo +
					" rutaArchivo=" + rutaArchivo);
			throw new NafinException("SIST0001");
		} 
		//***************************************************************************************
		try {	
			File archivo = new File(rutaArchivo);
			FileInputStream fileinputstream = new FileInputStream(archivo);	
			con.conexionDB();
			String strSQL = "";			
			strSQL = "SELECT count(*) as numRegistros " +
					" from com_convenio_epo_if " +
					" WHERE ic_if = ? "+
					" AND ic_epo=? ";
			
			ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, Integer.parseInt(ic_if));
			ps.setInt(2, Integer.parseInt(ic_epo));
			rs = ps.executeQuery();			
			rs.next();
			boolean existeRegistro = (rs.getInt("numRegistros") == 0)?false:true;
			rs.close();
			ps.close();

			if (!existeRegistro) {
				//Se mete un valor temporal '0'
				strSQL = "INSERT INTO com_convenio_epo_if (ic_producto_nafin,ic_if,ic_epo,bi_archivo, ic_tipo_documento) " +
						" VALUES (1,?,?, '0',?) ";
				ps = con.queryPrecompilado(strSQL);				
				ps.setInt(1, Integer.parseInt(ic_if));
				ps.setInt(2, Integer.parseInt(ic_epo));
				ps.setString(3, tipoArchivo);
				ps.executeUpdate();				
				ps.close();
			}
			strSQL = " UPDATE com_convenio_epo_if " +
					" SET df_modificacion = sysdate," +
					" bi_archivo = ?" +
					", ic_tipo_documento =?"+
					" WHERE ic_if = ? "+
					" AND ic_epo=? ";
			
			ps = con.queryPrecompilado(strSQL);
			ps.setBinaryStream(1, fileinputstream, (int)archivo.length());
			ps.setString(2, tipoArchivo);
			ps.setInt(3, Integer.parseInt(ic_if));
			ps.setInt(4, Integer.parseInt(ic_epo));
			ps.executeUpdate();
			ps.close();
			
			/*strSQL = "SELECT bi_archivo " +
					" FROM com_convenio_epo_if  " +
					" WHERE ic_if = ? and ic_epo=? FOR UPDATE NOWAIT ";
			ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, Integer.parseInt(ic_if));
			ps.setInt(2, Integer.parseInt(ic_epo));
			rs = ps.executeQuery();
			rs.next();
			java.sql.Blob blob = (java.sql.Blob)rs.getObject("bi_archivo");
			outstream = ((oracle.sql.BLOB)blob).getBinaryOutputStream();
			int i;
			while((i = fileinputstream.read(abyte0)) != -1)  {
				outstream.write(abyte0, 0, i);
			}
			outstream.close();
			rs.close();
			ps.close();*/
		} catch(Exception e) {
			exito = false;
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
	}
	
	/**
	 * Determina si existe o no dise�o para la IF en la BD.
	 * Si el dise�o existe aunque est� marcado como borrado se regresa
	 * "true" de cualquier modo
	 * Debe estar establecido el la claveEpo antes de llamar este m�todo
	 * 
	 */
	public boolean existeDisenoIF() throws NafinException {
			AccesoDB con = new AccesoDB();
			boolean existe = false;
			PreparedStatement ps = null;
			ResultSet rs = null;
			//**********************************Validaci�n de parametros:*****************************
			try {
				if (this.claveIF == null) {
					throw new Exception("La clave del if");
				}
			} catch(Exception e) {
				System.out.println("Error en los parametros establecidos. " 
						+ e.getMessage() + "\n" +
						" claveIF=" + this.claveIF);
				throw new NafinException("SIST0001");
			}
			try {
				con.conexionDB();
				String strSQL = "SELECT count(*) as numRegistros " +
						" from com_diseno_if " +
						" WHERE ic_if = ? ";
				
				ps = con.queryPrecompilado(strSQL);
				ps.setInt(1, Integer.parseInt(this.claveIF));
				rs = ps.executeQuery();
				rs.next();
				existe = (rs.getInt("numRegistros") == 0)?false:true;
				rs.close();
				ps.close();
				return existe;
			} catch(Exception e) {
				e.printStackTrace();
				throw new NafinException("SIST0001");
			} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
			}
		}	
	
	public String descomprimirArchivosDeBD(String rutaBase, String tipo) throws NafinException {
		//la ruta del archivo no aplica porque la informaci�n s eobtiene de BD
		//**********************************Validaci�n de parametros:*****************************
		try {
			if (rutaBase == null) {
				throw new Exception("Los parametros no pueden ser nulos");
			}
			if (this.claveIF == null) {
				throw new Exception("La clave de la if no esta establecida");
			}			
		} catch(Exception e) {
			System.out.println("Error en los parametros recibidos/establecidos. " + 
					e.getMessage() + "\n" +
					"rutaBase=" + rutaBase + "\n" +
					"claveID=" + this.claveIF);
			throw new NafinException("SIST0001");
		}
		return descomprimirArchivos(tipo, rutaBase);
	}	
	
	/**
	 * Descomprime los archivos de dise�o de la IF en ruta especificada
	 * Antes de llamar este m�todo es necesario establecer la clave de la epo
	 *
	 * @param fuente Origen de donde se obtiene el contenido comprimido del dise�o.
	 * 		BASE_DATOS
	 * @param rutaBase Ruta (Fisica) destino donde se descompacta el dise�o de la IF
	 * 
	 * @return String que especifica el nombre del logo para mostrar
	 */
	private String descomprimirArchivos(String fuente, String rutaBase) throws NafinException {
		AccesoDB con = new AccesoDB();
		InputStream inStream = null;
		OutputStream outstream = null;
		String logo = "";
		String strSQL = "";
		String rutaBasetotal = "";
		PreparedStatement ps = null;
		ResultSet rs = null;
		java.util.Date fechaArchivo = null;		
		try {	
			con.conexionDB();					
			if (fuente.equals("GIF")) {	
				log.trace(" +++++ Entro a GIF +++++ ");			
				strSQL = "SELECT bi_imagen, " +
					" df_modificacion " +
					" FROM com_diseno_if " +
					" WHERE ic_if = ? ";				
				ps = con.queryPrecompilado(strSQL);
				ps.setInt(1, Integer.parseInt(this.claveIF));
				rs = ps.executeQuery();			
				rs.next();			
				fechaArchivo = rs.getTimestamp("df_modificacion");
				//java.sql.Blob blob = (java.sql.Blob)rs.getObject("bi_imagen");
				//inStream = ((oracle.sql.BLOB)blob).getBinaryStream();				
				inStream = rs.getBinaryStream("bi_imagen");
				byte arrByte[] = new byte[4096];			
				rutaBasetotal = rutaBase+"00archivos/if/logos/"+this.claveIF+".gif";		
				outstream = new BufferedOutputStream(new FileOutputStream(rutaBasetotal));
				int i = 0;
				while((i = inStream.read(arrByte, 0, arrByte.length)) != -1){
				     outstream.write(arrByte, 0, i);
				}
				outstream.close();
				logo =this.claveIF+ ".gif";
			}
				
			if(fuente.equals("PDF")){
				log.trace(" +++++ Entro a PDF +++++ ");
				strSQL = "SELECT bi_archivo, " +
					" df_modificacion " +
					" FROM com_convenio_epo_if " +
					" WHERE ic_if = ? " +
					" AND ic_epo = ? ";				
				ps = con.queryPrecompilado(strSQL);
				ps.setInt(1, Integer.parseInt(this.claveIF));
				ps.setInt(2, Integer.parseInt(this.claveEPO));
				rs = ps.executeQuery();			
				rs.next();			
				fechaArchivo = rs.getTimestamp("df_modificacion");
				//java.sql.Blob blob = (java.sql.Blob)rs.getObject("bi_archivo");
				//inStream = ((oracle.sql.BLOB)blob).getBinaryStream();	
				inStream = rs.getBinaryStream("bi_archivo");			
				byte arrBytec[] = new byte[4096];			
				rutaBasetotal = rutaBase+"00archivos/if/conv/"+this.claveIF+"_"+this.claveEPO+".pdf";		
				outstream = new BufferedOutputStream(new FileOutputStream(rutaBasetotal));
				int i = 0;
				while((i = inStream.read(arrBytec, 0, arrBytec.length)) != -1){
				     outstream.write(arrBytec, 0, i);
				}
				outstream.close();
			}		
					
			//Cuando hay fechaArchivo esa es la que le pone al archivo creado
			if (fechaArchivo != null) {
				File archivo = new File(rutaBasetotal);
				if (archivo.exists()) {
					archivo.setLastModified(fechaArchivo.getTime());
				}
			}
			return logo;
		} catch(Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				try {
					if (rs != null) {
						rs.close();
					}
					if (ps !=null) {
						ps.close();
					}
				} catch(Exception e){}
				con.cierraConexionDB();
			}
		}
	}	
	
	/**
	 * Obtiene la informaci�n del dise�o todas los if.
	 * 
	 * @return Lista con la informaci�n de los dise�os (objetos DisenoIF).
	 * 		Si no hay dise�os, regresa un vector vacio, nunca null.
	 */
	public static List getInfoDisenosIfs() throws NafinException {
		List disenosIfs = new ArrayList();
		AccesoDB con = new AccesoDB();
		//String logo = "";
				
		try {
			con.conexionDB();
			String strSQL = " SELECT ic_if, " +
					" df_modificacion " +
					" FROM com_diseno_if " +
					" ORDER BY ic_if";

			ResultSet rs = con.queryDB(strSQL);
			while (rs.next()) {
				DisenoIF diseno = new DisenoIF();
				diseno.setClaveIF(rs.getString("ic_if"));
				diseno.setFechaModificacion(rs.getTimestamp("df_modificacion"));
				disenosIfs.add(diseno);				
			}

			rs.close();
			con.cierraStatement();
			return disenosIfs;
		} catch(Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}	
		
	public static List getInfoconveniosIfs() throws NafinException {
		List conveniosIfs = new ArrayList();
		AccesoDB con = new AccesoDB();
				
		try {
			con.conexionDB();
			String strSQL = " SELECT ic_if, " +
					" ic_epo, " +
					" df_modificacion " +
					" FROM com_convenio_epo_if " +
					" ORDER BY ic_if";

			ResultSet rs = con.queryDB(strSQL);
			while (rs.next()) {
				DisenoIF convenio = new DisenoIF();
				convenio.setClaveIF(rs.getString("ic_if"));
				convenio.setClaveEPO(rs.getString("ic_epo"));
				convenio.setFechaModificacion(rs.getTimestamp("df_modificacion"));
				conveniosIfs.add(convenio);				
			}

			rs.close();
			con.cierraStatement();
			return conveniosIfs;
		} catch(Exception e) {
			e.printStackTrace();
			throw new NafinException("SIST0001");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}
	
	private String claveIF;
	private String claveEPO;
	private java.util.Date fechaModificacion;
	
}//FIN 	class DisenoIF