package com.netro.cadenas;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import java.util.HashMap;
import java.io.Writer;
import netropology.utilerias.Fecha;
import netropology.utilerias.usuarios.UtilUsr;
import netropology.utilerias.usuarios.Usuario;
import com.netro.xlsx.ComunesXLSX;

import org.apache.commons.logging.Log;

public class ConsClieRegMovil  implements IQueryGeneratorRegExtJS {

	private static final Log log = ServiceLocator.getInstance().getLog(ConsClieRegMovil.class);//Variable para enviar mensajes al log.
	//ATRIBUTOS
	private String    paginaOffset;
	private String    paginaNo;
	private List 		conditions;
	
	
	private String 	strDirectorioTemp;
	
	private String num_nae="", claveAnioI="", claveAnioF="", claveMesI="", claveMesF="", claveAnioC="", claveMesC="", tipoConsulta="";
	
	private String numMes="", anioDetalle="";
	
	
	
	

	private String Totalreg;
	boolean bandera = true;
	
	StringBuffer strSQL = new StringBuffer();	
	
	public ConsClieRegMovil() {
	}
	/**
	 * Obtiene las llaves primarias
	 * @return sentencia sql
	 */
	public String getDocumentQuery() {
		log.info("getDocumentQuery(E)");
		strSQL 	= new StringBuffer();
		conditions 		= new ArrayList();
		strSQL 	= new StringBuffer();
		conditions 		= new ArrayList();
		if(tipoConsulta.equals("CONSULTA_GENERAL")){
			SimpleDateFormat sdfFecha = new SimpleDateFormat("dd/MM/yyyy");
			Calendar calI = Calendar.getInstance();
			Calendar calF = Calendar.getInstance();
			calI.set(Integer.parseInt(claveAnioC), Integer.parseInt(claveMesC), 1);
			calF.set(Integer.parseInt(claveAnioF), Integer.parseInt(claveMesF), 1);
			calF.set(Integer.parseInt(claveAnioF), Integer.parseInt(claveMesF), calF.getActualMaximum(calF.DAY_OF_MONTH));
	
			String fecha_registro_ini = sdfFecha.format(calI.getTime());
			String fecha_registro_fin = sdfFecha.format(calF.getTime());
			
			strSQL.append(" SELECT   afiliadas.anio, afiliadas.mes ");
			strSQL.append("     FROM (SELECT   TO_CHAR (df_carta_bajo, 'yyyy') anio, ");
			strSQL.append("                    CASE ");
			strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 1 THEN 'ENERO' ");
			strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 2 THEN 'FEBRERO' ");
			strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 3 THEN 'MARZO' ");
			strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 4 THEN 'ABRIL' ");
			strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 5 THEN 'MAYO' ");
			strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 6 THEN 'JUNIO' ");
			strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 7 THEN 'JULIO' ");
			strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 8 THEN 'AGOSTO' ");
			strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 9 THEN 'SEPTIEMBRE' ");
			strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 10 THEN 'OCTUBRE' ");
			strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 11 THEN 'NOVIEMBRE' ");
			strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 12 THEN 'DICIEMBRE' ");
			strSQL.append("                    END AS mes, ");
			strSQL.append("                    TO_CHAR (df_carta_bajo, 'mm') nummes, ");
			strSQL.append("                    COUNT (DISTINCT ic_pyme) afiliadas ");
			strSQL.append("               FROM comrel_pyme_epo ");
			strSQL.append("              WHERE cs_aceptacion != ? ");
			strSQL.append("                AND df_carta_bajo >= TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("                AND df_carta_bajo < TO_DATE (?, 'dd/mm/yyyy') + 1 ");
			strSQL.append("           GROUP BY TO_CHAR (df_carta_bajo, 'yyyy'), ");
			strSQL.append("                    TO_CHAR (df_carta_bajo, 'MONTH'), ");
			strSQL.append("                    TO_CHAR (df_carta_bajo, 'mm')) afiliadas, ");
			strSQL.append("          (SELECT   TO_CHAR (df_aceptacion, 'yyyy') anio, ");
			strSQL.append("                    TO_CHAR (df_aceptacion, 'mm') nummes, ");
			strSQL.append("                    COUNT (DISTINCT ic_pyme) habilitadas ");
			strSQL.append("               FROM comrel_pyme_epo ");
			strSQL.append("              WHERE cs_aceptacion = ? ");
			strSQL.append("                AND df_aceptacion >= TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("                AND df_aceptacion < TO_DATE (?, 'dd/mm/yyyy') + 1 ");
			strSQL.append("           GROUP BY TO_CHAR (df_aceptacion, 'yyyy'), ");
			strSQL.append("                    TO_CHAR (df_aceptacion, 'MONTH'), ");
			strSQL.append("                    TO_CHAR (df_aceptacion, 'mm')) habilitadas, ");
			strSQL.append("          (SELECT   TO_CHAR (df_activacion, 'yyyy') anio, ");
			strSQL.append("                    TO_CHAR (df_activacion, 'mm') nummes, ");
			strSQL.append("                    COUNT (df_activacion) altas ");
			strSQL.append("               FROM com_promo_pyme ");
			strSQL.append("              WHERE df_activacion >= TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("                AND df_activacion < TO_DATE (?, 'dd/mm/yyyy') + 1 ");
			strSQL.append("                AND (   df_activacion > df_desactivacion ");
			strSQL.append("                     OR df_desactivacion IS NULL ");
			strSQL.append("                    ) ");
			strSQL.append("           GROUP BY TO_CHAR (df_activacion, 'yyyy'), ");
			strSQL.append("                    TO_CHAR (df_activacion, 'mm')) altas, ");
			strSQL.append("          (SELECT   TO_CHAR (df_desactivacion, 'yyyy') anio, ");
			strSQL.append("                    TO_CHAR (df_desactivacion, 'mm') nummes, ");
			strSQL.append("                    COUNT (df_desactivacion) inactivas ");
			strSQL.append("               FROM com_promo_pyme ");
			strSQL.append("              WHERE df_desactivacion >= TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("                AND df_desactivacion < TO_DATE (?, 'dd/mm/yyyy') + 1 ");
			strSQL.append("                AND df_desactivacion > df_activacion ");
			strSQL.append("           GROUP BY TO_CHAR (df_desactivacion, 'yyyy'), ");
			strSQL.append("                    TO_CHAR (df_desactivacion, 'mm')) inactivas ");
			strSQL.append("    WHERE afiliadas.anio = habilitadas.anio(+) ");
			strSQL.append("      AND afiliadas.nummes = habilitadas.nummes(+) ");
			strSQL.append("      AND afiliadas.anio = altas.anio(+) ");
			strSQL.append("      AND afiliadas.nummes = altas.nummes(+) ");
			strSQL.append("      AND afiliadas.anio = inactivas.anio(+) ");
			strSQL.append("      AND afiliadas.nummes = inactivas.nummes(+) ");
			strSQL.append(" ORDER BY afiliadas.anio, afiliadas.nummes ");
			
			conditions.add("H");
			conditions.add(fecha_registro_ini);
			conditions.add(fecha_registro_fin);
			conditions.add("H");
			conditions.add(fecha_registro_ini);
			conditions.add(fecha_registro_fin);
			conditions.add(fecha_registro_ini);
			conditions.add(fecha_registro_fin);
			conditions.add(fecha_registro_ini);
			conditions.add(fecha_registro_fin);
		}else{
			String fecha_ini_act = "01/01/2008";
			String fecha_ini_mes = "01/" + numMes + "/" + anioDetalle;
			
			strSQL.append("SELECT rel.ic_nafin_electronico no_nafin, ");
			strSQL.append("			  rel.ic_epo_pyme_if no_pyme ");
			strSQL.append("  FROM com_promo_pyme promo, ");
			strSQL.append("       comrel_nafin rel, ");
			strSQL.append("       comcat_pyme pym, ");
			strSQL.append("       com_contacto con, ");
			strSQL.append("       com_domicilio cd, ");
			strSQL.append("       comcat_estado ce, ");
			strSQL.append("       (SELECT   COUNT (1) total, relpe.ic_pyme ");
			strSQL.append("            FROM comrel_pyme_epo relpe, comrel_nafin relna ");
			strSQL.append("           WHERE relpe.cs_habilitado = ? ");
			strSQL.append("             AND relpe.cs_aceptacion = ? ");
			strSQL.append("             AND relpe.ic_pyme = relna.ic_epo_pyme_if ");
			strSQL.append("        GROUP BY relpe.ic_pyme) afiliadas, ");
			strSQL.append("       (SELECT   COUNT (1) total, relpe.ic_pyme ");
			strSQL.append("            FROM comrel_pyme_epo relpe, comrel_nafin relna ");
			strSQL.append("           WHERE relpe.cs_habilitado = ? ");
			strSQL.append("             AND relpe.cs_aceptacion != ? ");
			strSQL.append("             AND relpe.ic_pyme = relna.ic_epo_pyme_if ");
			strSQL.append("        GROUP BY relpe.ic_pyme) noafiliadas ");
			strSQL.append(" WHERE promo.ic_pyme = rel.ic_epo_pyme_if ");
			strSQL.append("   AND rel.cg_tipo = ? ");
			strSQL.append("   AND promo.ic_pyme = pym.ic_pyme ");
			strSQL.append("   AND promo.ic_pyme = con.ic_pyme ");
			strSQL.append("   AND con.cs_primer_contacto = ? ");
			strSQL.append("   AND pym.ic_pyme = afiliadas.ic_pyme(+) ");
			strSQL.append("   AND pym.ic_pyme = noafiliadas.ic_pyme(+) ");
			strSQL.append("   AND promo.ic_pyme = cd.ic_pyme ");
			strSQL.append("   AND ce.ic_estado = cd.ic_estado ");
			strSQL.append("   AND ce.ic_pais = cd.ic_pais ");
			strSQL.append("   AND cd.cs_fiscal = ? ");
			strSQL.append("   AND (   (    promo.df_desactivacion >= TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("            AND promo.df_desactivacion < TO_DATE (?, 'dd/mm/yyyy') + 1 ");
			strSQL.append("            AND promo.df_desactivacion > promo.df_activacion ");
			strSQL.append("              ) ");
			strSQL.append("           OR (    promo.df_activacion >= TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("               AND promo.df_activacion < TO_DATE (?, 'dd/mm/yyyy') + 1 ");
			strSQL.append("        			  AND (promo.df_activacion > promo.df_desactivacion OR promo.df_desactivacion IS NULL) ");
			strSQL.append("              ) ");
			strSQL.append("           OR (    promo.df_activacion >= TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("               AND promo.df_activacion < TO_DATE (?, 'dd/mm/yyyy') + 1 ");
			strSQL.append("               AND (   promo.df_activacion > promo.df_desactivacion ");
			strSQL.append("                    OR promo.df_desactivacion IS NULL ");
			strSQL.append("                   ) ");
			strSQL.append("              ) ");
			strSQL.append("          ) ");


			/*conditions.add(fecha_ini_mes);
			conditions.add(fecha_ini_mes);
			conditions.add(fecha_ini_act);
			conditions.add(fecha_ini_mes);
			conditions.add(fecha_ini_mes);
			conditions.add(fecha_ini_mes);*/
			conditions.add("S");
			conditions.add("H");
			conditions.add("S");
			conditions.add("H");
			conditions.add("P");
			conditions.add("S");
			conditions.add("S");
			conditions.add(fecha_ini_mes);
			conditions.add(fecha_ini_mes);
			conditions.add(fecha_ini_act);
			conditions.add(fecha_ini_mes);
			conditions.add(fecha_ini_mes);
			conditions.add(fecha_ini_mes);

		}
			
		log.info("qrySentencia  "+strSQL);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return strSQL.toString();
	}
	/**
	 * Obtiene la lista de los Ids en turno para realizar la paginacion
	 * @return sentencia sql
	 * @param ids
	 */
	public String getDocumentSummaryQueryForIds(List ids){
		log.info("getDocumentSummaryQueryForIds(E)");
		strSQL 	= new StringBuffer();
		conditions 		= new ArrayList();
		if(tipoConsulta.equals("CONSULTA_GENERAL")){
			SimpleDateFormat sdfFecha = new SimpleDateFormat("dd/MM/yyyy");
			Calendar calI = Calendar.getInstance();
			Calendar calF = Calendar.getInstance();
			calI.set(Integer.parseInt(claveAnioC), Integer.parseInt(claveMesC), 1);
			calF.set(Integer.parseInt(claveAnioF), Integer.parseInt(claveMesF), 1);
			calF.set(Integer.parseInt(claveAnioF), Integer.parseInt(claveMesF), calF.getActualMaximum(calF.DAY_OF_MONTH));
	
			String fecha_registro_ini = sdfFecha.format(calI.getTime());
			String fecha_registro_fin = sdfFecha.format(calF.getTime());
			
			strSQL.append(" SELECT   afiliadas.anio, afiliadas.mes, afiliadas.afiliadas, ");
			strSQL.append("          habilitadas.habilitadas, altas.altas, inactivas.inactivas, afiliadas.nummes,  '' AS ACTIVAS, '' AS BAJAS");
			strSQL.append("     FROM (SELECT   TO_CHAR (df_carta_bajo, 'yyyy') anio, ");
			strSQL.append("                    CASE ");
			strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 1 THEN 'ENERO' ");
			strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 2 THEN 'FEBRERO' ");
			strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 3 THEN 'MARZO' ");
			strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 4 THEN 'ABRIL' ");
			strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 5 THEN 'MAYO' ");
			strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 6 THEN 'JUNIO' ");
			strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 7 THEN 'JULIO' ");
			strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 8 THEN 'AGOSTO' ");
			strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 9 THEN 'SEPTIEMBRE' ");
			strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 10 THEN 'OCTUBRE' ");
			strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 11 THEN 'NOVIEMBRE' ");
			strSQL.append("                       WHEN TO_CHAR (df_carta_bajo, 'mm') = 12 THEN 'DICIEMBRE' ");
			strSQL.append("                    END AS mes, ");
			strSQL.append("                    TO_CHAR (df_carta_bajo, 'mm') nummes, ");
			strSQL.append("                    COUNT (DISTINCT ic_pyme) afiliadas ");
			strSQL.append("               FROM comrel_pyme_epo ");
			strSQL.append("              WHERE cs_aceptacion != ? ");
			strSQL.append("                AND df_carta_bajo >= TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("                AND df_carta_bajo < TO_DATE (?, 'dd/mm/yyyy') + 1 ");
			strSQL.append("           GROUP BY TO_CHAR (df_carta_bajo, 'yyyy'), ");
			strSQL.append("                    TO_CHAR (df_carta_bajo, 'MONTH'), ");
			strSQL.append("                    TO_CHAR (df_carta_bajo, 'mm')) afiliadas, ");
			strSQL.append("          (SELECT   TO_CHAR (df_aceptacion, 'yyyy') anio, ");
			strSQL.append("                    TO_CHAR (df_aceptacion, 'mm') nummes, ");
			strSQL.append("                    COUNT (DISTINCT ic_pyme) habilitadas ");
			strSQL.append("               FROM comrel_pyme_epo ");
			strSQL.append("              WHERE cs_aceptacion = ? ");
			strSQL.append("                AND df_aceptacion >= TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("                AND df_aceptacion < TO_DATE (?, 'dd/mm/yyyy') + 1 ");
			strSQL.append("           GROUP BY TO_CHAR (df_aceptacion, 'yyyy'), ");
			strSQL.append("                    TO_CHAR (df_aceptacion, 'MONTH'), ");
			strSQL.append("                    TO_CHAR (df_aceptacion, 'mm')) habilitadas, ");
			strSQL.append("          (SELECT   TO_CHAR (df_activacion, 'yyyy') anio, ");
			strSQL.append("                    TO_CHAR (df_activacion, 'mm') nummes, ");
			strSQL.append("                    COUNT (df_activacion) altas ");
			strSQL.append("               FROM com_promo_pyme ");
			strSQL.append("              WHERE df_activacion >= TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("                AND df_activacion < TO_DATE (?, 'dd/mm/yyyy') + 1 ");
			strSQL.append("                AND (   df_activacion > df_desactivacion ");
			strSQL.append("                     OR df_desactivacion IS NULL ");
			strSQL.append("                    ) ");
			strSQL.append("           GROUP BY TO_CHAR (df_activacion, 'yyyy'), ");
			strSQL.append("                    TO_CHAR (df_activacion, 'mm')) altas, ");
			strSQL.append("          (SELECT   TO_CHAR (df_desactivacion, 'yyyy') anio, ");
			strSQL.append("                    TO_CHAR (df_desactivacion, 'mm') nummes, ");
			strSQL.append("                    COUNT (df_desactivacion) inactivas ");
			strSQL.append("               FROM com_promo_pyme ");
			strSQL.append("              WHERE df_desactivacion >= TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("                AND df_desactivacion < TO_DATE (?, 'dd/mm/yyyy') + 1 ");
			strSQL.append("                AND df_desactivacion > df_activacion ");
			strSQL.append("           GROUP BY TO_CHAR (df_desactivacion, 'yyyy'), ");
			strSQL.append("                    TO_CHAR (df_desactivacion, 'mm')) inactivas ");
			strSQL.append("    WHERE afiliadas.anio = habilitadas.anio(+) ");
			strSQL.append("      AND afiliadas.nummes = habilitadas.nummes(+) ");
			strSQL.append("      AND afiliadas.anio = altas.anio(+) ");
			strSQL.append("      AND afiliadas.nummes = altas.nummes(+) ");
			strSQL.append("      AND afiliadas.anio = inactivas.anio(+) ");
			strSQL.append("      AND afiliadas.nummes = inactivas.nummes(+) ");
															
			conditions.add("H");
			conditions.add(fecha_registro_ini);
			conditions.add(fecha_registro_fin);
			conditions.add("H");
			conditions.add(fecha_registro_ini);
			conditions.add(fecha_registro_fin);
			conditions.add(fecha_registro_ini);
			conditions.add(fecha_registro_fin);
			conditions.add(fecha_registro_ini);
			conditions.add(fecha_registro_fin);
			
			strSQL.append(" AND (");
			for(int i=0;i<ids.size();i++) {
				List lItem = (ArrayList)ids.get(i);
					if(i>0) {
							strSQL.append("  OR  "); 
					}
					strSQL.append(" afiliadas.anio IN ( ? ");
						conditions.add(lItem.get(0).toString());			
					strSQL.append(" ) ");
					
					strSQL.append("  AND  "); 
					
					strSQL.append(" afiliadas.mes IN ( ?");
					conditions.add(lItem.get(1).toString());			
					strSQL.append(" ) ");
			}
			strSQL.append(") " );
			
			strSQL.append(" ORDER BY afiliadas.anio, afiliadas.nummes ");
		}else{
			String fecha_ini_act = "01/01/2008";
			String fecha_ini_mes = "01/" + numMes + "/" + anioDetalle;
			
			strSQL.append("SELECT rel.ic_nafin_electronico no_nafin, ");
			strSQL.append("			  rel.ic_epo_pyme_if no_pyme, ");
			strSQL.append("       pym.cg_razon_social razon_social, ");
			strSQL.append("       con.cg_email email, ");
			strSQL.append("       con.cg_celular no_celular, ");
			strSQL.append("       TO_CHAR (promo.df_promocion, 'dd/mm/yyyy') fecha_registro, ");
			strSQL.append("       TO_CHAR (promo.df_desactivacion, 'dd/mm/yyyy') fecha_desactivacion, ");
			strSQL.append("       TO_CHAR (promo.df_activacion, 'dd/mm/yyyy') fecha_activacion, ");
			strSQL.append("       NVL (afiliadas.total, 0) epos_afiliadas, ");
			strSQL.append("       NVL (noafiliadas.total, 0) epos_no_afiliadas, ");
			strSQL.append("       ce.cd_nombre estado, ");
			strSQL.append("       CASE ");
			strSQL.append("              WHEN promo.df_desactivacion >= TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("              AND promo.df_desactivacion < ADD_MONTHS( TO_DATE (?, 'dd/mm/yyyy'), 1) ");
			strSQL.append("              AND promo.df_desactivacion > promo.df_activacion ");
			strSQL.append("                 THEN 'BAJA' ");
			strSQL.append("              WHEN promo.df_activacion >= TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("              AND promo.df_activacion < TO_DATE (?, 'dd/mm/yyyy') + 1 ");
      strSQL.append("       			 AND (promo.df_activacion > promo.df_desactivacion OR promo.df_desactivacion IS NULL) ");
			strSQL.append("              		THEN 'ACTIVA' ");
			strSQL.append("              WHEN promo.df_activacion >= TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("              AND promo.df_activacion < ADD_MONTHS( TO_DATE (?, 'dd/mm/yyyy'), 1) ");
      strSQL.append("       			 AND (promo.df_activacion > promo.df_desactivacion OR promo.df_desactivacion IS NULL) ");
			strSQL.append("                 THEN 'ALTA' ");
			strSQL.append("       END AS estatus, '' as TIPOEPO ");
			strSQL.append("  FROM com_promo_pyme promo, ");
			strSQL.append("       comrel_nafin rel, ");
			strSQL.append("       comcat_pyme pym, ");
			strSQL.append("       com_contacto con, ");
			strSQL.append("       com_domicilio cd, ");
			strSQL.append("       comcat_estado ce, ");
			strSQL.append("       (SELECT   COUNT (1) total, relpe.ic_pyme ");
			strSQL.append("            FROM comrel_pyme_epo relpe, comrel_nafin relna ");
			strSQL.append("           WHERE relpe.cs_habilitado = ? ");
			strSQL.append("             AND relpe.cs_aceptacion = ? ");
			strSQL.append("             AND relpe.ic_pyme = relna.ic_epo_pyme_if ");
			strSQL.append("        GROUP BY relpe.ic_pyme) afiliadas, ");
			strSQL.append("       (SELECT   COUNT (1) total, relpe.ic_pyme ");
			strSQL.append("            FROM comrel_pyme_epo relpe, comrel_nafin relna ");
			strSQL.append("           WHERE relpe.cs_habilitado = ? ");
			strSQL.append("             AND relpe.cs_aceptacion != ? ");
			strSQL.append("             AND relpe.ic_pyme = relna.ic_epo_pyme_if ");
			strSQL.append("        GROUP BY relpe.ic_pyme) noafiliadas ");
			strSQL.append(" WHERE promo.ic_pyme = rel.ic_epo_pyme_if ");
			strSQL.append("   AND rel.cg_tipo = ? ");
			strSQL.append("   AND promo.ic_pyme = pym.ic_pyme ");
			strSQL.append("   AND promo.ic_pyme = con.ic_pyme ");
			strSQL.append("   AND con.cs_primer_contacto = ? ");
			strSQL.append("   AND pym.ic_pyme = afiliadas.ic_pyme(+) ");
			strSQL.append("   AND pym.ic_pyme = noafiliadas.ic_pyme(+) ");
			strSQL.append("   AND promo.ic_pyme = cd.ic_pyme ");
			strSQL.append("   AND ce.ic_estado = cd.ic_estado ");
			strSQL.append("   AND ce.ic_pais = cd.ic_pais ");
			strSQL.append("   AND cd.cs_fiscal = ? ");
			strSQL.append("   AND (   (    promo.df_desactivacion >= TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("            AND promo.df_desactivacion < TO_DATE (?, 'dd/mm/yyyy') + 1 ");
			strSQL.append("            AND promo.df_desactivacion > promo.df_activacion ");
			strSQL.append("              ) ");
			strSQL.append("           OR (    promo.df_activacion >= TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("               AND promo.df_activacion < TO_DATE (?, 'dd/mm/yyyy') + 1 ");
			strSQL.append("        			  AND (promo.df_activacion > promo.df_desactivacion OR promo.df_desactivacion IS NULL) ");
			strSQL.append("              ) ");
			strSQL.append("           OR (    promo.df_activacion >= TO_DATE (?, 'dd/mm/yyyy') ");
			strSQL.append("               AND promo.df_activacion < TO_DATE (?, 'dd/mm/yyyy') + 1 ");
			strSQL.append("               AND (   promo.df_activacion > promo.df_desactivacion ");
			strSQL.append("                    OR promo.df_desactivacion IS NULL ");
			strSQL.append("                   ) ");
			strSQL.append("              ) ");
			strSQL.append("          ) ");


			conditions.add(fecha_ini_mes);
			conditions.add(fecha_ini_mes);
			conditions.add(fecha_ini_act);
			conditions.add(fecha_ini_mes);
			conditions.add(fecha_ini_mes);
			conditions.add(fecha_ini_mes);
			conditions.add("S");
			conditions.add("H");
			conditions.add("S");
			conditions.add("H");
			conditions.add("P");
			conditions.add("S");
			conditions.add("S");
			conditions.add(fecha_ini_mes);
			conditions.add(fecha_ini_mes);
			conditions.add(fecha_ini_act);
			conditions.add(fecha_ini_mes);
			conditions.add(fecha_ini_mes);
			conditions.add(fecha_ini_mes);
			
			strSQL.append(" AND (");
			for(int i=0;i<ids.size();i++) {
				List lItem = (ArrayList)ids.get(i);
					if(i>0) {
							strSQL.append("  OR  "); 
					}
					strSQL.append(" rel.ic_nafin_electronico IN ( ? ");
						conditions.add(lItem.get(0).toString());			
					strSQL.append(" ) ");
					
					strSQL.append("  AND  "); 
					
					strSQL.append(" rel.ic_epo_pyme_if IN ( ?");
					conditions.add(lItem.get(1).toString());			
					strSQL.append(" ) ");
			}
			strSQL.append(") " );
			
		}	
	
		log.info("qrySentencia  "+strSQL);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return strSQL.toString();
	}
	/**
	 * Obtiene los totales
	 * @return sentencia sql
	 */
	public String getAggregateCalculationQuery(){
		log.info("getAggregateCalculationQuery(E)");
		strSQL 	= new StringBuffer();
		conditions 		= new ArrayList();
			
			
		log.info("qrySentencia  "+strSQL);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return strSQL.toString();	
	}
	/**
	 * Obtiene la consulta para generar el archivo PDF o CSV sin utilizar paginaci�n
	 * @return sentencia sql
	 */
	public String getDocumentQueryFile(){

		log.info("getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();
		
		log.info("qrySentencia: \n" + qrySentencia);
		log.info("conditions: " + conditions);
		log.info("getDocumentQueryFile(S)");

		return qrySentencia.toString();

	}
	/**
	 * Crea el archivo PDF o CSV seg�n el tipo que se le pasa como par�metro.
	 * Obtiene los datos de la consulta generada en el m�todo getDocumentQueryFile.
	 * @param request
	 * @param rs
	 * @param path
	 * @param tipo
	 * @return nombreArchivo
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String directorioTemporal, String tipo){

		log.debug("crearCustomFile(E)");

		String      nombreArchivo = "";
		ComunesXLSX documentoXLSX = null;
		Writer      writer        = null;
		
		
		
		return nombreArchivo;

	}
	/**
	 * Crea el archivo PDF utilizando paginaci�n
	 * @param request
	 * @param reg
	 * @param path
	 * @param tipo
	 * @return nombreArchivo
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo){
		String nombreArchivo = "";
		HttpSession session = request.getSession();

		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try{

		} catch(Throwable e){
			throw new AppException("Error al generar el archivo ", e);
		} finally{
			try{

			} catch(Exception e){}

		}
		return nombreArchivo;
	}
	

	
	
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  

	public String getNum_nae() {
		return num_nae;
	}

	public void setNum_nae(String num_nae) {
		this.num_nae = num_nae;
	}

	public String getClaveAnioI() {
		return claveAnioI;
	}

	public void setClaveAnioI(String claveAnioI) {
		this.claveAnioI = claveAnioI;
	}

	public String getClaveAnioF() {
		return claveAnioF;
	}

	public void setClaveAnioF(String claveAnioF) {
		this.claveAnioF = claveAnioF;
	}

	public String getClaveMesI() {
		return claveMesI;
	}

	public void setClaveMesI(String claveMesI) {
		this.claveMesI = claveMesI;
	}

	public String getClaveMesF() {
		return claveMesF;
	}

	public void setClaveMesF(String claveMesF) {
		this.claveMesF = claveMesF;
	}

	public String getClaveAnioC() {
		return claveAnioC;
	}

	public void setClaveAnioC(String claveAnioC) {
		this.claveAnioC = claveAnioC;
	}

	public String getClaveMesC() {
		return claveMesC;
	}

	public void setClaveMesC(String claveMesC) {
		this.claveMesC = claveMesC;
	}

	public String getTipoConsulta() {
		return tipoConsulta;
	}

	public void setTipoConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}

	public String getNumMes() {
		return numMes;
	}

	public void setNumMes(String numMes) {
		this.numMes = numMes;
	}

	public String getAnioDetalle() {
		return anioDetalle;
	}

	public void setAnioDetalle(String anioDetalle) {
		this.anioDetalle = anioDetalle;
	}

	

}