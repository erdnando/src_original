package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class BiSolicAfilia implements IQueryGeneratorRegExtJS   {

	private List conditions;
	StringBuffer strQuery;
	String ic_pyme,ic_epo,clave_if,dfSol_ini,dfSol_fin;
	int maximoNumeroRegistros= 1000;
	private static final Log log = ServiceLocator.getInstance().getLog(ConsSeleccionIF.class);
	
	public BiSolicAfilia() {
	}
	
	
	
	public String getAggregateCalculationQuery() {
		return "";
   }
  
   public String getDocumentQuery() {
		
		
			return "";
  }
  
  public String getDocumentSummaryQueryForIds(List pageIds) {
		
		
		return "";
  }
  
  public String getDocumentQueryFile() {
		conditions =new ArrayList();
		log.info("getBiSolicAfilia(E)");
		AccesoDB	con							= null;
		StringBuffer	qrySentencia  =new StringBuffer();
		StringBuffer	condicion  =new StringBuffer(); 		

		int 			cont						= 0;

	

			if (!"".equals(ic_epo) && ic_epo != null){
			
				condicion.append(" AND s.ic_epo =? ");	
				conditions.add(ic_epo);
			}
			if (!"".equals(ic_pyme) && ic_pyme != null){
				condicion.append(" AND s.ic_pyme =? ");

				conditions.add(ic_pyme);
			}

			if (!"".equals(clave_if) && clave_if != null){
				condicion.append(" AND s.ic_if =? ");		
				conditions.add(clave_if);
			}

			if ((!"".equals(dfSol_ini) && dfSol_ini != null) && (!"".equals(dfSol_fin) && dfSol_fin != null) ){
				condicion.append(" AND s.DF_SOLICITUD >= TO_DATE (?, 'dd/mm/yyyy')" );
				condicion.append(" AND s.DF_SOLICITUD <  TO_DATE (?, 'dd/mm/yyyy')+1 ");
				System.out.println("\n\n\n\n\n");
				conditions.add(dfSol_ini);
				conditions.add(dfSol_fin);
			}
					
			qrySentencia.append(" SELECT  ");
			qrySentencia.append(" 	epo.cg_razon_social AS razon_social_epo,  ");
			qrySentencia.append(" 	pyme.cg_razon_social AS razon_social_pyme,  ");
			qrySentencia.append("  	pyme.cg_rfc AS rfc,  ");
			qrySentencia.append(" 	dom.cg_calle || ' '|| dom.cg_numero_ext || ' '|| dom.cg_numero_int || ' '|| dom.cg_colonia || ' '|| dom.cn_cp || ' ' || mun.cd_nombre || ' '|| edo.cd_nombre AS domicilio,  ");
			qrySentencia.append(" 	c.cg_nombre || ' '||c.cg_appat || ' ' || c.cg_apmat as contacto, ");
			qrySentencia.append(" 	c.cg_tel as telefono, ");
			qrySentencia.append("  	TO_CHAR (s.df_solicitud, 'DD/MM/YYYY') AS fecha ");
			qrySentencia.append(" From COM_SOLIC_REAFILIACION s,  ");
			qrySentencia.append(" 		 comcat_if cif,  ");
			qrySentencia.append("      comcat_pyme pyme,  ");
			qrySentencia.append("			 comcat_epo epo,  ");
			qrySentencia.append("			 comrel_nafin cn,  ");
			qrySentencia.append("      com_domicilio dom,  ");
			qrySentencia.append("      comcat_estado edo,  ");
			qrySentencia.append("      comcat_municipio mun, ");
			qrySentencia.append("      com_contacto c ");
			qrySentencia.append(" WHERE pyme.ic_pyme = s.ic_pyme  ");
			qrySentencia.append(" and cif.ic_if = s.ic_if  ");
			qrySentencia.append(" and epo.ic_epo = s.ic_epo ");
			qrySentencia.append(" AND cn.ic_epo_pyme_if = pyme.ic_pyme ");
			qrySentencia.append(" AND pyme.ic_pyme = dom.ic_pyme  ");
			qrySentencia.append(" AND dom.ic_estado = edo.ic_estado ");
			qrySentencia.append(" AND edo.ic_estado = mun.ic_estado  ");
			qrySentencia.append(" AND dom.ic_municipio = mun.ic_municipio ");
			qrySentencia.append(" AND dom.ic_estado = mun.ic_estado  ");
			qrySentencia.append(" AND dom.ic_pais = mun.ic_pais  ");
			qrySentencia.append(" and s.ic_pyme = c.ic_pyme "+condicion.toString());
   		
			log.debug(" qrySentencia "+qrySentencia.toString());
			strQuery=qrySentencia;
			return qrySentencia.toString();
  }
  
  
  /**
		* En este m�todo se debe realizar la implementaci�n de la generaci�n del archivo
		* con base en el resulset que se recibe como par�metro. El ResulSet es el resultado
		* de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
		* @param request HttpRequest empleado principalmente para obtener el objeto session
		* @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
		* @param path Ruta f�sica donde se generar� el archivo
		* @param tipo cadena que identifica el tipo o variante de archivo que va a generar.
		* @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo)
	{
		String linea = "";
		String nombreArchivo = "";
		String espacio = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
      
		try {
		 if(tipo.equals("CSV")) {
		  linea = "Nombre EPO,Nombre o Razon Social,RFC,Domicilio,Nombre del Contacto,Tel�fono,Fecha de Solicitud\n";
		  nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
		  writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
		  buffer = new BufferedWriter(writer);
		
		  buffer.write("Datos de la PyME\n"+linea);
			 
		  while (rs.next()) {
					String razonEpo = (rs.getString("razon_social_epo") == null) ? "" : rs.getString("razon_social_epo");
					String razonPyme = (rs.getString("razon_social_pyme") == null) ? "" : rs.getString("razon_social_pyme");
					String rfc = (rs.getString("rfc") == null) ? "" : rs.getString("rfc");
					String domicilio = (rs.getString("domicilio") == null) ? "" : rs.getString("domicilio");
					String contacto = (rs.getString("contacto") == null) ? "" : rs.getString("contacto");
					String telefono = (rs.getString("telefono") == null) ? "" : rs.getString("telefono");
					String fecha = (rs.getString("fecha") == null) ? "" : rs.getString("fecha");
			
						
					linea = razonEpo.replace(',',' ') + ", " + 
									razonPyme.replace(',',' ') + ", " +
								rfc.replace(',',' ') + ", " +
								domicilio.replace(',',' ') + ", " +
								contacto.replace(',',' ') + ", " +
								telefono.replace(',',' ') + ", " +
								fecha.replace(',',' ') + "\n";				 
					buffer.write(linea);
		  }
		  buffer.close();
		 }
		 else if(tipo.equals("PDF")){
		  HttpSession session = request.getSession();
		  nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
		  ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
		  String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		  String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		  String diaActual = fechaActual.substring(0,2);
		  String mesActual = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		  String anioActual = fechaActual.substring(6,10);
		  String horaActual = new SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
		  pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		  session.getAttribute("iNoNafinElectronico").toString(),
		  (String)session.getAttribute("sesExterno"),
		  (String)session.getAttribute("strNombre"),
		  (String)session.getAttribute("strNombreUsuario"),
		  (String)session.getAttribute("strLogo"),
		  (String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
		  pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual, "formas",ComunesPDF.RIGHT);
		  pdfDoc.setTable(7,100);
		  pdfDoc.setCell("Nombre EPO", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setCell("Nombre o Razon Social", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setCell("RFC", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setCell("Domicilio", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setCell("Nombre del Contacto", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setCell("Tel�fono", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setCell("Fecha de Solicitud", "celda01",ComunesPDF.CENTER);
		  
		  while (rs.next()) {
				String razonEpo = (rs.getString("razon_social_epo") == null) ? "" : rs.getString("razon_social_epo");
				String razonPyme = (rs.getString("razon_social_pyme") == null) ? "" : rs.getString("razon_social_pyme");
				String rfc = (rs.getString("rfc") == null) ? "" : rs.getString("rfc");
				String domicilio = (rs.getString("domicilio") == null) ? "" : rs.getString("domicilio");
				String contacto = (rs.getString("contacto") == null) ? "" : rs.getString("contacto");
				String telefono = (rs.getString("telefono") == null) ? "" : rs.getString("telefono");
				String fecha = (rs.getString("fecha") == null) ? "" : rs.getString("fecha");
				
				pdfDoc.setCell(razonEpo,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(razonPyme,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(rfc,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(domicilio,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(contacto,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(telefono,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fecha,"formas",ComunesPDF.CENTER);
			}
		  pdfDoc.addTable();
		  pdfDoc.endDocument();
		 
		 }
		}
		catch (Throwable e) {
			throw new AppException("Error al generar el archivo",e);
		}
		finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
		
		return nombreArchivo;
   }
	
	
	/** En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va a generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	
	public List getConditions() {
		return conditions;
	}


	public void setStrQuery(StringBuffer strQuery) {
		this.strQuery = strQuery;
	}


	public StringBuffer getStrQuery() {
		return strQuery;
	}


	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}


	public String getIc_pyme() {
		return ic_pyme;
	}


	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}


	public String getIc_epo() {
		return ic_epo;
	}


	public void setClave_if(String clave_if) {
		this.clave_if = clave_if;
	}


	public String getClave_if() {
		return clave_if;
	}


	public void setDfSol_ini(String dfSol_ini) {
		this.dfSol_ini = dfSol_ini;
	}


	public String getDfSol_ini() {
		return dfSol_ini;
	}


	public void setDfSol_fin(String dfSol_fin) {
		this.dfSol_fin = dfSol_fin;
	}


	public String getDfSol_fin() {
		return dfSol_fin;
	}


	public void setMaximoNumeroRegistros(int maximoNumeroRegistros) {
		this.maximoNumeroRegistros = maximoNumeroRegistros;
	}


	public int getMaximoNumeroRegistros() {
		return maximoNumeroRegistros;
	}

	
	
	
}