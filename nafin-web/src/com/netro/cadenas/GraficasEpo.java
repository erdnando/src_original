package com.netro.cadenas;


import com.netro.promoepo.webservice.edocuenta.EdoCuentaWSClient;

import com.netro.promoepo.webservice.edocuenta.EdoCuentaWSInfo;

import java.math.BigDecimal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class GraficasEpo  {

	private final static Log log = ServiceLocator.getInstance().getLog(GraficasEpo.class);
	
	public GraficasEpo() {
	}
	
	/**
	 * Obtiene la información necesaria para la generación de gráficas para la
	 * EPO de acuerdo a los parametros especificados
	 * @param claveEpo Clave de la EPO (comcat_epo.ic_epo)
	 * @param tipoMoneda Tipo de moneda. MN.-Moneda Nacional, DL.-Dolares
	 * @return Cadena con el estatus de la ejecución del proceso:
	 * 		OK.- Hay información para graficas
	 *       NO.- No hay información para graficas
	 *       ERR.- Se presentó un error al obtener la información de las graficas.
	 */
	public String obtenerInformacion(String claveEpo, String tipoMoneda) {
		log.info("obtenerInformacion(E)");
		AccesoDB con = new AccesoDB();
		boolean bOk = true;
		int iClaveEpo = 0;
		boolean existeInformacionParaGraficas = false;
		List params = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String mensaje = "";
		String estatusProc = "";
		String txtMoneda = "";
		String  tipoGraficas ="";
			
   	//**********************************Validación de parametros:*****************************
		try {
			if (claveEpo == null || claveEpo.equals("") || 
					tipoMoneda == null  || tipoMoneda.equals("") ) {
				throw new Exception("Los parametros son requeridos");
			}
			iClaveEpo = Integer.parseInt(claveEpo);
			
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos",e);
		}
		//****************************************************************************************
				
		if(tipoMoneda.equals("DL"))  {
			txtMoneda = "_DL";
		}
				
		try {
			con.conexionDB();
			String strSQL = 
					" SELECT COUNT(*) " +
					" FROM com_graficas_cache " +
					" WHERE ic_afiliado = ? " +
					" AND ic_tipo_afiliado = ? " +
					" AND df_cache >= TRUNC(sysdate) " +
					" AND df_cache < TRUNC(sysdate) + 1 " +
					" AND cs_estatus <> 'ERR' "+
					" AND cg_moneda =  ? ";
			ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, iClaveEpo);
			ps.setString(2, "E");
			ps.setString(3, tipoMoneda);
			rs = ps.executeQuery();
			rs.next();
			boolean existeRegGrafCacheActual = (rs.getInt(1) == 0)?false:true;
			rs.close();
			ps.close();
				
			if (existeRegGrafCacheActual) {
				return "OK"; //No continua porque ya hay información en el cache
			}
				
			try {
				EdoCuentaWSClient  	edoCuentaWSCliente 	= new EdoCuentaWSClient();
				EdoCuentaWSInfo info = edoCuentaWSCliente.consultarEstadoCuenta(claveEpo, new java.text.SimpleDateFormat("MM/yyyy").format(new java.util.Date()));
				
				String codigoEjecucion = info.getCodigoEjecucion();
				if( codigoEjecucion.equals(EdoCuentaWSInfo.CON_ERROR) ){
					log.error("Error al generar la informacion necesaria para el estado de cuenta en sistema PromoEpo");
				}
			} catch(Exception e) {
				log.error("Error al generar la informacion necesaria para el estado de cuenta en sistema PromoEpo", e);
			}
			
			strSQL = 
					" DELETE FROM com_graficas_cache_det " +
					" WHERE ic_afiliado = ? " +
					" AND ic_tipo_afiliado = ? "+
					" AND CG_MONEDA =  ?  ";
			params = new ArrayList();
			params.add(new Integer(iClaveEpo));
			params.add("E");
			params.add(tipoMoneda);
			con.ejecutaUpdateDB(strSQL, params);
			
			
			
			strSQL = 
						" DELETE FROM  com_graficas_cache " +
						" WHERE ic_afiliado = ? " +
						" AND ic_tipo_afiliado = ? "+
						" AND CG_MONEDA   = ? " ; 
				params = new ArrayList();
				params.add(new Integer(iClaveEpo));
				params.add("E");
				params.add(tipoMoneda);
				con.ejecutaUpdateDB(strSQL, params);   
				

			params = new ArrayList();
			strSQL = 
					" INSERT INTO com_graficas_cache(ic_afiliado, ic_tipo_afiliado, cs_estatus, CG_MONEDA  ) " +
					" VALUES(?,?,?, ? ) ";
			params.add(new Integer(iClaveEpo));
			params.add("E");
			params.add("PR");	//En Proceso
			params.add(tipoMoneda);	//moneda

			con.ejecutaUpdateDB(strSQL, params);
			con.terminaTransaccion(true);	//Para registrar que comenzo el proceso de obtención de informacion
			
			
			
			//-----------------------------------------------------------------
			//-----------------------------------------------------------------
			//-----------------------------------------------------------------
			
			strSQL = 
					" SELECT oper.ig_prov_reg prov_reg, " +
					"       oper.ig_prov_con_pub prov_con_pub, " +
					"       oper.fn_monto_susceptible monto_sucep, " +
					"       oper.fn_monto_publicado monto_total, " +
					"       oper.fn_monto_operado monto_operado, " +
					"       oper.fn_monto_neto fn_monto_neto, " +
					"       CASE WHEN oper.ic_periodo = 'C' THEN 'AC' " +
					"       ELSE oper.ic_periodo END as periodo," +
					"       CASE WHEN oper.cc_columna = 'MXN' THEN 'MN' " +
					"       ELSE 'DL' END as moneda " +
					"  FROM r_prm_bit_oper_epos oper " +
					" WHERE oper.ic_epo = ? " +
					"   AND oper.df_mes_reporte = TRUNC(SYSDATE,'mm') " +
					"   AND oper.cc_columna IN ('MXN', 'DL')  ";
			params = new ArrayList();
			params.add(new Integer(iClaveEpo));
			Registros reg = con.consultarDB(strSQL, params);

			String strInsert = 
				" INSERT INTO com_graficas_cache_det (ic_grafica_cache_det, " +
				" ic_afiliado, ic_tipo_afiliado, " +
				" cg_grafica, cg_atributo, cg_valor, cg_color , CG_MONEDA  ) " +
				" VALUES(seq_com_graficas_cache_det.nextval,?,?,?,?,?,?, ? )";
			ps = con.queryPrecompilado(strInsert);
			existeInformacionParaGraficas = false;
			while(reg.next()) {
				existeInformacionParaGraficas = true;
				String moneda = reg.getString("moneda");
				String periodo = reg.getString("periodo");
				
				ps.clearParameters();
				ps.setInt(1, iClaveEpo);
				ps.setString(2, "E");
				ps.setString(3, "PROVEEDOR" + moneda);
				ps.setString(4, "CONCUENTAXPAGAR" + periodo);
				ps.setString(5, reg.getString("prov_con_pub"));
				ps.setString(6, "");
				ps.setString(7, tipoMoneda);
				ps.executeUpdate();

				ps.clearParameters();
				ps.setInt(1, iClaveEpo);
				ps.setString(2, "E");
				ps.setString(3, "PROVEEDOR" + moneda);
				ps.setString(4, "REGISTRADO" + periodo);
				ps.setString(5, reg.getString("prov_reg"));
				ps.setString(6, "");
				ps.setString(7, tipoMoneda);
				ps.executeUpdate();
				
				ps.clearParameters();
				ps.setInt(1, iClaveEpo);
				ps.setString(2, "E");
				ps.setString(3, "DOCREG" + moneda);
				ps.setString(4, "MONTOSUSCFACT" + periodo);
				ps.setString(5, reg.getString("monto_sucep"));
				ps.setString(6, "");
				ps.setString(7, tipoMoneda);
				ps.executeUpdate();

				ps.clearParameters();
				ps.setInt(1, iClaveEpo);
				ps.setString(2, "E");
				ps.setString(3, "DOCREG" + moneda);
				ps.setString(4, "MONTOTOTAL" + periodo);
				ps.setString(5, reg.getString("monto_total"));
				ps.setString(6, "");
				ps.setString(7, tipoMoneda);
				ps.executeUpdate();

				ps.clearParameters();
				ps.setInt(1, iClaveEpo);
				ps.setString(2, "E");
				ps.setString(3, "FACTORAJE" + moneda);
				ps.setString(4, "OPERADO" + periodo);
				ps.setString(5, reg.getString("monto_operado"));
				ps.setString(6, "");
				ps.setString(7, tipoMoneda);
				ps.executeUpdate();
				
				ps.clearParameters();
				ps.setInt(1, iClaveEpo);
				ps.setString(2, "E");
				ps.setString(3, "FACTORAJE" + moneda);
				ps.setString(4, "PUBLICADO" + periodo);
				ps.setString(5, reg.getString("fn_monto_neto"));
				ps.setString(6, "");
				ps.setString(7, tipoMoneda);
				ps.executeUpdate();
			
			}
			ps.close();
			
			strSQL = "SELECT SUM (in_limite_epo"+txtMoneda +") as sumalimite "+
						"  FROM comrel_if_epo " +
						" WHERE cs_vobo_nafin = 'S' " +
						"   AND cs_aceptacion = 'S' " +
						"   AND cs_activa_limite = 'S' " +
						"   AND cs_bloqueo = 'N' " +
						"   AND ic_epo = ? " +
						"   AND in_limite_epo"+txtMoneda+"  >0 ";
			
			params = new ArrayList();
			params.add(new Integer(iClaveEpo));
			Registros reg2 = con.consultarDB(strSQL, params);
			
			
			BigDecimal sumLimEpo = new BigDecimal("0.0");
			String sumLimEpo_V ="";
			if(reg2.next()){
				sumLimEpo_V = (reg2.getString("sumalimite")==null)?"":reg2.getString("sumalimite");								
			}
			//log.debug(" sumLimEpo_V---------------->  "+sumLimEpo_V);         
			
			if(!sumLimEpo_V.equals(""))  {
				sumLimEpo = new BigDecimal(sumLimEpo_V);
		
				strSQL = "SELECT i.ic_if, ie.in_limite_epo"+txtMoneda+" , i.cg_nombre_comercial cg_nombre_comercial , " +
							"       ROUND (ie.in_limite_epo"+txtMoneda+" * 100 / ? , 2) pocentaje " +
							"  FROM comrel_if_epo ie, comcat_if i " +
							" WHERE ie.ic_if = i.ic_if " +
							"   AND ie.cs_vobo_nafin = 'S' " +
							"   AND ie.cs_aceptacion = 'S' " +
							"   AND ie.cs_activa_limite = 'S' " +
							"   AND ie.cs_bloqueo = 'N' " +
							"   AND ie.ic_epo = ? " +
							"   AND ie.in_limite_epo"+txtMoneda+" > 0 ";
	
	
				params = new ArrayList();
				params.add(sumLimEpo.toPlainString());
				params.add(new Integer(iClaveEpo));
				Registros reg3 = con.consultarDB(strSQL, params);
				
				ps = con.queryPrecompilado(strInsert);
				//existeInformacionParaGraficas = false;
				while(reg3.next()) {
					existeInformacionParaGraficas = true;
					String razonSocial = reg3.getString("cg_nombre_comercial");
					String pocentaje = reg3.getString("pocentaje");
					String   moneda = "MN"; 
					if(tipoMoneda.equals("DL"))  { moneda = "DL";   }
					ps.clearParameters();
					ps.setInt(1, iClaveEpo);
					ps.setString(2, "E");
					ps.setString(3, "DISTLINEASAUT"+moneda);
					ps.setString(4, razonSocial);
					ps.setString(5, pocentaje);
					ps.setString(6, "");
					ps.setString(7, tipoMoneda);
					ps.executeUpdate();
				
				}
				ps.close();
				
			}
			
			strSQL = "SELECT   TO_CHAR (df_fecha_venc, 'dd/mm/yyyy') AS fechavenc, " +
						"         SUM (fn_monto) AS montototal, " +
						"         CASE " +
						"            WHEN ic_moneda = 1 " +
						"               THEN 'MN' " +
						"            ELSE 'DL' " +
						"         END AS moneda " +
						"    FROM com_documento " +
						"   WHERE ic_epo = ? " +
						"     AND df_fecha_venc >= TRUNC (SYSDATE, 'mm') " +
						"     AND df_fecha_venc < ADD_MONTHS (TRUNC (SYSDATE, 'mm'), 1) " +
						"     AND ic_moneda = ? " +
						"  and ic_estatus_docto not in (1,5,6,7)  "+
						"GROUP BY df_fecha_venc, ic_moneda " +
						"ORDER BY df_fecha_venc ";

			
			params = new ArrayList();
			params.add(new Integer(iClaveEpo));
			params.add(new Integer("1"));
			Registros reg4 = con.consultarDB(strSQL, params);

			ps = con.queryPrecompilado(strInsert);
			//existeInformacionParaGraficas = false;
			while(reg4.next()) {
				existeInformacionParaGraficas = true;
				String fechavenc = reg4.getString("fechavenc");
				String montototal = reg4.getString("montototal");
				String moneda = reg4.getString("moneda");
				
				ps.clearParameters();
				ps.setInt(1, iClaveEpo);
				ps.setString(2, "E");
				ps.setString(3, "VENCDELMES"+moneda);
				ps.setString(4, fechavenc);
				ps.setString(5, montototal);
				ps.setString(6, "");
				ps.setString(7, tipoMoneda);
				ps.executeUpdate();
			
			}
			ps.close();
			
			strSQL = "SELECT   TO_CHAR (df_fecha_venc, 'dd/mm/yyyy') AS fechavenc, " +
						"         SUM (fn_monto) AS montototal, " +
						"         CASE " +
						"            WHEN ic_moneda = 1 " +
						"               THEN 'MN' " +
						"            ELSE 'DL' " +
						"         END AS moneda " +
						"    FROM com_documento " +
						"   WHERE ic_epo = ? " +
						"     AND df_fecha_venc >= TRUNC (SYSDATE, 'mm') " +
						"     AND df_fecha_venc < ADD_MONTHS (TRUNC (SYSDATE, 'mm'), 1) " +
						"     AND ic_moneda = ? " +
						"  and ic_estatus_docto not in (1,5,6,7)  "+  
						"GROUP BY df_fecha_venc, ic_moneda " +
						"ORDER BY df_fecha_venc "; 

			
			params = new ArrayList();
			params.add(new Integer(iClaveEpo));
			params.add(new Integer("54"));
			Registros reg5 = con.consultarDB(strSQL, params);

			ps = con.queryPrecompilado(strInsert);
			//existeInformacionParaGraficas = false;
			while(reg5.next()) {
				existeInformacionParaGraficas = true;
				String fechavenc = reg5.getString("fechavenc");
				String montototal = reg5.getString("montototal");
				String moneda = reg5.getString("moneda");
				
				ps.clearParameters();
				ps.setInt(1, iClaveEpo);
				ps.setString(2, "E");
				ps.setString(3, "VENCDELMES"+moneda);
				ps.setString(4, fechavenc);
				ps.setString(5, montototal);
				ps.setString(6, "");
				ps.setString(7, tipoMoneda);
				ps.executeUpdate();
			
			}
			ps.close();
			

			bOk = true;
		} catch(Throwable e) {
			bOk = false;
			SimpleDateFormat formatoHora = new SimpleDateFormat ("yyyyMMdd_HHmmss");
			String fechaError = formatoHora.format(new java.util.Date());
			mensaje = fechaError + ": ERROR INESPERADO. " + e.getMessage();
			log.error(mensaje, e);
		} finally {
			if (con.hayConexionAbierta()) {
				try {
					if (rs != null) {
						rs.close();
					}
					if (ps != null) {
						ps.close();
					}
				} catch(Exception e) {
					log.error("Error al cerrar recursos",e);
				}
				con.terminaTransaccion(bOk);
				con.cierraConexionDB();
			}
		}
		
	    AccesoDB conF = new AccesoDB();
	    try {
	        conF.conexionDB();
	        String estatus = "";
	        if (bOk == false) {
	            estatus = "ERR";
	        } else {
	            estatus = existeInformacionParaGraficas?"OK":"NO";
	        }
	        estatusProc = estatus;
	        params = new ArrayList();
	        String strSQLUpdate = 
	                " UPDATE com_graficas_cache " +
	                " SET cs_estatus = ?, "+
	                " cg_mensaje = ?, " +
	                " df_cache = SYSDATE " +
	                " WHERE ic_afiliado = ? " +
	                " AND ic_tipo_afiliado = ? "+
	                " AND CG_MONEDA =  ? ";
	        params.add(estatus);
	        params.add(mensaje);
	        params.add(new Integer(iClaveEpo));
	        params.add("E");
	        params.add(tipoMoneda);
	        conF.ejecutaUpdateDB(strSQLUpdate, params);
	        bOk = true;
	        return estatusProc ;
	    } catch(Throwable t) {
	        bOk = false;
	        throw new AppException("Error al guardar el estatus del proceso", t);               
	    } finally {
	        if(conF.hayConexionAbierta()) {
	            conF.terminaTransaccion(bOk);
	            conF.cierraConexionDB();
	        }
	        log.info("obtenerInformacion(S)");
	    }
		
	}

	
	
	/**
	 * Obtiene la información especifica para armar una grafica determinada
	 * de la EPO.
	 * @param claveEpo Clave de la epo
	 * @param claveGrafica Clave de la grafica
	 * @return Lista con la información necesaria para generar la gráfica
	 */
	public List getInfoGraficaEpo(String claveEpo, String grafica) {
		log.info("getInfoGraficaEpo(E)");
		AccesoDB con = new AccesoDB();
		int iClaveEpo = 0;
		List lstData = new ArrayList();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";

		//**********************************Validación de parametros:*****************************
		try {
			if (claveEpo == null || claveEpo.equals("")) {
				throw new Exception("Los parametros son requeridos");
			}
			iClaveEpo = Integer.parseInt(claveEpo);
			
		} catch(Exception e) {
			throw new AppException("Error en los parametros recibidos",e);
		}
		//****************************************************************************************
		
		try{
		
			con.conexionDB();

			
			query = "SELECT b.ic_grafica_cache_det, b.cg_grafica, b.cg_atributo, b.cg_valor " +
					"  FROM com_graficas_cache a, com_graficas_cache_det b " +
					" WHERE a.ic_afiliado = b.ic_afiliado " +
					"   AND a.ic_tipo_afiliado = b.ic_tipo_afiliado " +
					"	 AND a.cs_estatus <> 'ERR' " +
					"   AND b.cg_grafica = ? " +
					"   AND b.ic_afiliado = ? " +
					"   AND b.ic_tipo_afiliado = ? ";					
			
			ps = con.queryPrecompilado(query);
			ps.setString(1, grafica);
			ps.setInt(2, iClaveEpo);
			ps.setString(3,"E");
			rs = ps.executeQuery();
			
			while(rs.next()){
				HashMap mp = new HashMap();
				mp.put(rs.getString("CG_ATRIBUTO"),rs.getString("CG_VALOR"));
				
				lstData.add(mp);
			}
			rs.close();
			ps.close();
			
			System.out.println("lstData==="+lstData.size());
			return lstData;
		}catch(Throwable t){
			throw new AppException("Error al obtener informacion de Grafica", t);	
		}finally{
			if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			
			log.info("getInfoGraficaEpo(S)");
		}
		
	}


}