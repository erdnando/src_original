package com.netro.cadenas;

import com.netro.exception.NafinException;
import com.netro.pdf.ComunesPDF;
import com.netro.xls.ComunesXLS;
import com.netro.zip.ComunesZIP;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsDepositoCred implements IQueryGeneratorRegExtJS {
	//Variables
	StringBuffer qrySentencia = new StringBuffer();
	private List 		conditions;
		   

	private static final Log log = ServiceLocator.getInstance().getLog(ConsDepositoCred.class);
	private String ic_if;
	private String txtfolio;
	private String txtfechaIni;
	private String txtfechaFin;
	private String lsTipoOperacion;	
	private String accionBoton;
	private String ic_moneda;
	private String lsFechaOperacion;
	private String lsNomMoneda;
	private String lsImporteTotalO;
	private String lsNombreIF;

	
	
	public String getAggregateCalculationQuery() {
	return "";
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds){
	return "";
	}
	
	public String getDocumentQuery(){
	return "";
	}
	
	public String getDocumentQueryFile(){
		
		log.debug("getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();		
		this.conditions = new ArrayList();
		
		if(accionBoton.equals("Consultar") ) {
		
			qrySentencia.append( "  SELECT   /*+index(sp) index(p) use_nl(sp pym ce t)*/  "+
				"   sp.ic_solic_portal AS NUM_FOLIO,   "+
				"		sp.ig_numero_prestamo AS NUM_PRESTAMO ,  "+
				" NVL (sp.fn_importe_dscto, 0) AS IMPORTE_TOTAL,   "+
				" NVL (ce.cd_descripcion, ' ') AS EMISOR, "+
				" NVL (sp.cg_lugar_firma, ' ') AS LUGAR_EMISION,   "+
				" NVL (TO_CHAR (sp.df_etc, 'dd/mm/yyyy'), ' ') AS FECHA_EMISION,   "+
				" NVL (sp.cg_domicilio_pago, ' ') AS DOMICILIO_PAGO,  "+
				" NVL (TO_CHAR (sp.df_v_descuento, 'dd/mm/yyyy'), ' ') AS FECHA_VENC,  "+
				" NVL (sp.ig_numero_docto, 0) AS NUM_DOCTO,  "+
				" TO_CHAR (sp.df_operacion, 'dd') || ' de ' || DECODE ( TO_CHAR (sp.df_operacion, 'mm'),  "+
				"     '01', 'Enero', '02', 'Febrero', '03', 'Marzo','04', 'Abril',  "+
				"     '05', 'Mayo', '06', 'Junio','07', 'Julio', '08', 'Agosto',  "+
				"     '09', 'Septiembre', '10', 'Octubre', '11', 'Noviembre', '12', 'Diciembre'  "+
				"  ) || ' de ' || TO_CHAR (sp.df_operacion, 'YYYY') AS FECHA_OPERACION, "+
				" NVL (sp.in_numero_amort, 0) AS NUM_AORTIZACION,   "+
				" t.cd_nombre AS TASA_IF,  "+
				" pym.cg_razon_social AS   NOMBRE_PYME,  "+
				" cs_tipo_plazo AS TIPO_PLAZO,  "+
				" m.cd_nombre as MONEDA,  "+
                " cd.cd_descripcion as CLASE_DOCTO "  +
				" FROM com_solic_portal sp  "+
				" , comcat_pyme pym  "+
				" , comcat_emisor ce  "+
				" , comcat_tasa t  "+
				" , comcat_moneda m  "+
                " , comcat_clase_docto cd "+
				" WHERE sp.ic_emisor = ce.ic_emisor(+)  "+
				" AND sp.ic_tasaif = t.ic_tasa   "+
				" AND sp.ic_clase_docto = cd.Ic_Clase_Docto " +
				" AND sp.ig_clave_sirac = pym.in_numero_sirac(+)  "+
				" AND sp.ic_moneda = m.ic_moneda(+)  "+
				" AND ic_estatus_solic IN (3, 5, 6)  ");
				
			if(!ic_if.equals(""))  {
				qrySentencia.append(" AND sp.ic_if = ?   ");
				conditions.add(ic_if);				
			}
			if(!"".equals(txtfechaIni)  &&  !"".equals(txtfechaFin) ) {
				qrySentencia.append("  AND sp.df_operacion >= TO_DATE (?, 'dd/mm/yyyy')  AND sp.df_operacion < (TO_DATE (?, 'dd/mm/yyyy') + 1) ");
				conditions.add(txtfechaIni);
				conditions.add(txtfechaFin);
			}
			if (!txtfolio.equals("")) {
				qrySentencia.append(" AND sp.ic_solic_portal = ?" );
				conditions.add(txtfolio);
			}
			
			qrySentencia.append(" order by 1");
			
		}else if(accionBoton.equals("Operaciones") ) {
		
		
			qrySentencia.append( " SELECT   /*+index(sp IN_COM_SOLIC_PORTAL_05_NU) use_nl(sp pym ce t m)*/"   +
				"           sp.ic_solic_portal,"   +
				"           sp.ig_numero_prestamo, sp.fn_importe_dscto importetotal,"   +
				"           ce.cd_descripcion emisor, sp.cg_lugar_firma lugaremision,"   +
				"           TO_CHAR (sp.df_etc, 'dd/mm/yyyy') fechaemision, sp.cg_domicilio_pago,"   +
				"           TO_CHAR (sp.df_v_descuento, 'dd/mm/yyyy') fechavenc,"   +
				"           sp.ig_numero_docto,"   +
				"           TO_CHAR (sp.df_operacion, 'dd/mm/yyyy') AS fechaoper,"   +
				"           sp.in_numero_amort, t.cd_nombre tasaif, pym.cg_razon_social nompyme,"   +
				"           cs_tipo_plazo, m.cd_nombre moneda"   +
				"     FROM com_solic_portal sp,"   +
				"          comcat_pyme pym,"   +
				"          comcat_emisor ce,"   +
				"          comcat_tasa t,"   +
				"          comcat_moneda m"   +
				"    WHERE sp.ic_emisor = ce.ic_emisor(+)"   +
				"      AND sp.ic_tasaif = t.ic_tasa"   +
				"      AND sp.ig_clave_sirac = pym.in_numero_sirac(+)"   +
				"      AND sp.ic_moneda = m.ic_moneda(+)"   +
				"      AND sp.ic_estatus_solic IN (?, ?, ?)"  );
				
				conditions.add(new Integer(3));
				conditions.add(new Integer(5));
				conditions.add(new Integer(6));
				
				if(!ic_if.equals("")) {
					qrySentencia.append(" AND sp.ic_if = ? "  );
					conditions.add(ic_if);					
				}
				if(!ic_moneda.equals("")) {
					qrySentencia.append(" AND sp.ic_moneda = ? ");
					conditions.add(ic_moneda);	
				}
				
				if(!lsFechaOperacion.equals("")) { 
					qrySentencia.append( " AND sp.df_operacion >= TO_DATE (?, 'dd/mm/yyyy')"   +
											" AND sp.df_operacion < (TO_DATE (?, 'dd/mm/yyyy') + 1)" );
					conditions.add(lsFechaOperacion);	
					conditions.add(lsFechaOperacion);	
				}
								
				qrySentencia.append(" ORDER BY 1"  );	
		
		}
		
		log.debug("strQuery.toString(): "+qrySentencia.toString());
		log.debug("conditions.toString(): "+conditions);
		log.debug("getDocumentQueryFile(S)");
		
		return qrySentencia.toString();
	
	}
	
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String nombreArchivo = "";
		CreaArchivo creaArchivo = new CreaArchivo();
		ComunesXLS xlsDoc = new ComunesXLS();
		StringBuffer contenidoArchivo = new StringBuffer();		
		ComunesPDF pdfDoc = new ComunesPDF();
		HttpSession session = request.getSession();	
		
		List datos = this.getEncabezado(ic_if, accionBoton );
		System.out.println("datos---->"+datos);
		String lsFechaContrato = datos.get(0).toString();  
		String lsPuesto = datos.get(1).toString();
		String lsDatos = datos.get(2).toString();
		String lsNombre = datos.get(3).toString();
	    String lsClaseDocto = null; 
		int numReg =0;
		String mensaje = "", lsImporte ="0", lsTipoOperacion ="", lsFolio ="", lsNumPrestamo ="", lsImporteTotal ="", lsEmisor ="", lsLugarEmision ="",
					 lsFechaEmision ="", lsDomicilioPago ="", lsFechaVenc ="", lsNumDocto  ="", lsFechaOper ="",  lsNoAmort ="", lsTasaIF ="",  lsMoneda ="";
						
						
		try {
			
			if(tipo.equals("XLS")) {
			
				nombreArchivo = creaArchivo.nombreArchivo()+".xls";
				xlsDoc = new ComunesXLS(path+nombreArchivo);
			}
			
			if(tipo.equals("PDF"))  {
				
					nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
					pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
					String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
					String diaActual    = fechaActual.substring(0,2);
					String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
					String anioActual   = fechaActual.substring(6,10);
					String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
					pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
						((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
						(String)session.getAttribute("sesExterno"),
						(String) session.getAttribute("strNombre"),
						(String) session.getAttribute("strNombreUsuario"),
						(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
						pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
					pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);
			}
			
			if(accionBoton.equals("Consultar") ) {
			
				while(rs.next()){			
					lsTipoOperacion = (rs.getString("TIPO_PLAZO") == null) ? "" : rs.getString("TIPO_PLAZO");
					lsFolio = (rs.getString("NUM_FOLIO") == null) ? "" : rs.getString("NUM_FOLIO");
					lsNumPrestamo = (rs.getString("NUM_PRESTAMO") == null) ? "" : rs.getString("NUM_PRESTAMO");
					lsImporteTotal = (rs.getString("IMPORTE_TOTAL") == null) ? "" : rs.getString("IMPORTE_TOTAL");
					lsEmisor = (rs.getString("EMISOR") == null) ? "" : rs.getString("EMISOR");
					lsLugarEmision = (rs.getString("LUGAR_EMISION") == null) ? "" : rs.getString("LUGAR_EMISION");
					lsFechaEmision = (rs.getString("FECHA_EMISION") == null) ? "" : rs.getString("FECHA_EMISION");
					lsDomicilioPago = (rs.getString("DOMICILIO_PAGO") == null) ? "" : rs.getString("DOMICILIO_PAGO");
					lsFechaVenc = (rs.getString("FECHA_VENC") == null) ? "" : rs.getString("FECHA_VENC");
					lsNumDocto = (rs.getString("NUM_DOCTO") == null) ? "" : rs.getString("NUM_DOCTO");					
					lsFechaOper = (rs.getString("FECHA_OPERACION") == null) ? "" : rs.getString("FECHA_OPERACION");
					lsNoAmort = (rs.getString("NUM_AORTIZACION") == null) ? "" : rs.getString("NUM_AORTIZACION");
					lsTasaIF = (rs.getString("TASA_IF") == null) ? "" : rs.getString("TASA_IF");
					lsMoneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
                    lsClaseDocto = (rs.getString("CLASE_DOCTO") == null) ? "" : rs.getString("CLASE_DOCTO");
					
					
					if(tipo.equals("XLS") && accionBoton.equals("Consultar") ) {
					
						if ("F".equals(lsTipoOperacion)) {
							xlsDoc.setCelda("FORMATO DE CONSTANCIA DE DEPOSITO DE DOCUMENTOS DESCONTADOS PARA OPERACIONES DE FACTORAJE ELECTRONICO", "titulo", ComunesXLS.CENTER, 12);
							xlsDoc.setCelda("De conformidad con el convenio que " +lsNombre + " y en la fecha que posteriormente se se�ala. celebr� con Nacional Financiera S.N.C. bajo protesta de decir verdad, se hace constar que para todos los efectos legales, nos asumimos como depositarios de los ", "formas", ComunesXLS.LEFT, 12);
							xlsDoc.setCelda("t�tulo(s) de cr�dito y/o documento(s) electr�nico(s) sin derecho a honorarios y asumiendo la responsabilidad civil y penal correspondiente al car�cter de depositario judicial, el(los) titulo(s) de cr�dito y/o documento(s) electr�nico(s) en que se consignan ", "formas", ComunesXLS.LEFT, 12);
							xlsDoc.setCelda("los derechos de cr�dito derivados de las operaciones de factoraje financiero electr�nicas, celebradas con los clientes que se describen en el cuadro siguiente, en donde se especifica la fecha de presentaci�n, importe total y nombre de la empresa acreditada.", "formas", ComunesXLS.LEFT, 12);
				
							xlsDoc.setCelda("La informaci�n del documento en "+lsMoneda+" por un importe de $"+ Comunes.formatoDecimal(new Double(lsImporteTotal),2) +" citado, est�n registrados en el sistema denominado Nafin Electr�nico a disposici�n de Nacional Financiera S.N.C. cuando esta los requiera.", "formas", ComunesXLS.LEFT, 12);
							
							xlsDoc.setCelda("Trat�ndose del t�tulo de cr�dito electr�nico, este se encuentra debidamente transmitido en propiedad y con nuestra responsabilidad a favor de Nacional Financiera S.N.C..", "formas", ComunesXLS.LEFT, 12);
							xlsDoc.setCelda("En el caso del documento electr�nico, en este acto cedemos a dicha instituci�n de Banca de Desarrollo los derechos  de cr�dito que se derivan de los mismos.", "formas", ComunesXLS.LEFT, 12);
				
							xlsDoc.setCelda("En terminos del art�culo 2044 del C�digo Civil para el Distrito Federal, la cesi�n a la que se refiere el p�rrafo anterior, la realizamos con nuestra responsabilidad, ", "formas", ComunesXLS.LEFT, 12);
							xlsDoc.setCelda("respecto de la solvencia de los emisores, por el tiempo en el que permanezcan vigentes los adeudos y hasta su liquidaci�n final.", "formas", ComunesXLS.LEFT, 12);
				
							xlsDoc.setCelda("As� mismo y en t�rminos de lo estipulado en el Convenio antes mencionado, nos obligamos a efectuar el cobro del t�tulo de cr�dito electr�nico y/o ejercer los derechos de cobro consignados en el documento objeto de descuento, ", "formas", ComunesXLS.LEFT, 12);
							xlsDoc.setCelda("as� como ejecutar todos los actos que sean necesarios para que el documento se�alado conserve su valor y dem�s derechos que les correspondan.", "formas", ComunesXLS.LEFT, 12);
							
						
						} else {
							xlsDoc.setCelda("FORMATO DE CERTIFICADO DE DEPOSITO DE TITULOS DE CREDITO EN ADMINISTRACION", "titulo", ComunesXLS.CENTER, 12);
							xlsDoc.setCelda("Obran en nuestro poder y nos obligamos a conservar en dep�sito en administraci�n a disposici�n de Nacional Financiera S.N.C. el (los) t�tulo (s) de cr�dito por los importes que abajo se detalla (n) suscrito (s) conforme a la Ley General de T�tulos y Operaciones de Cr�dito el (los) que descontamos con Nacional Financiera S.N.C. consider�ndose para todos los efectos legales que somos depositarios de los mismos.", "formas", ComunesXLS.LEFT, 12);
							xlsDoc.setCelda("Sin cargo alguno para Nacional Financiera S.N.C. quedamos obligados a efectuar el cobro del (de los) referido(s) t�tulo(s) a su vencimiento y a efectuar en su caso los dem�s actos a que se refiere el Art�culo 278 de la Ley General de T�tulos y Operaciones de Cr�dito.", "formas", ComunesXLS.LEFT, 12);
							xlsDoc.setCelda("El (los) citado (s) t�tulo(s) ha (n) sido endosado (s) en propiedad con nuestra responsabilidad a Nacional Financiera S.N.C. en virtud del descuento del (de los) mismo (s) en los t�rminos del contrato de apertura de cr�dito que con la fecha que a continuaci�n se describe celebramos con dicha Instituci�n.", "formas", ComunesXLS.LEFT, 12);
						}
						xlsDoc.creaFila();
						xlsDoc.setTabla(2, 4);
						xlsDoc.setBorde(true);
						xlsDoc.setCelda("Importe Total", "celda01");
						xlsDoc.setCelda(Comunes.formatoDecimal(new Double(lsImporteTotal),2), "formas", ComunesXLS.RIGHT);
						xlsDoc.setCelda("Fecha de Contrato", "celda01");
						xlsDoc.setCelda(lsFechaContrato);			
						xlsDoc.cierraTabla();
						
						xlsDoc.setTabla(12);
						xlsDoc.setBorde(true);
						xlsDoc.setCelda("Nombre del emisor", "celda01", ComunesXLS.CENTER);
						xlsDoc.setCelda("Beneficiario", "celda01", ComunesXLS.CENTER);
						xlsDoc.setCelda("Clase de Documento", "celda01", ComunesXLS.CENTER);
						xlsDoc.setCelda("Lugar de Emision", "celda01", ComunesXLS.CENTER);
						xlsDoc.setCelda("Fecha de Emision", "celda01", ComunesXLS.CENTER);
						xlsDoc.setCelda("Domicilio de Pago", "celda01", ComunesXLS.CENTER);
						xlsDoc.setCelda("Tasa de Interes", "celda01", ComunesXLS.CENTER);
						xlsDoc.setCelda("Numero de Documento", "celda01", ComunesXLS.CENTER);
						xlsDoc.setCelda("Fecha Vencimiento", "celda01", ComunesXLS.CENTER);
						xlsDoc.setCelda("Valor Nominal", "celda01", ComunesXLS.CENTER);
						xlsDoc.setCelda("Numero de Prestamo", "celda01", ComunesXLS.CENTER);
						xlsDoc.setCelda("Tipo de Documento", "celda01", ComunesXLS.CENTER);
								
						xlsDoc.setCelda(lsEmisor);
						xlsDoc.setCelda(lsNombre);
					    xlsDoc.setCelda(lsClaseDocto);
						xlsDoc.setCelda(lsLugarEmision);
						xlsDoc.setCelda(lsFechaEmision);
						xlsDoc.setCelda(lsDomicilioPago);
						xlsDoc.setCelda(lsTasaIF);
						xlsDoc.setCelda(lsNumDocto);
						xlsDoc.setCelda(lsFechaVenc);
						xlsDoc.setCelda(Comunes.formatoDecimal(new Double(lsImporteTotal),2));
						xlsDoc.setCelda(lsNumPrestamo);
						xlsDoc.setCelda("Certificado");
						
						xlsDoc.setCelda("Total", "celda01", ComunesXLS.CENTER, 9);
						xlsDoc.setCelda(Comunes.formatoDecimal(new Double(lsImporteTotal),2));
						xlsDoc.setCelda("", "celda01", ComunesXLS.LEFT, 2);
									 
						xlsDoc.cierraTabla();
						
						xlsDoc.creaFila();
						xlsDoc.setTabla(3, 5);
						xlsDoc.setBorde(true);
						xlsDoc.setCelda("Amortizacion", "celda01");
						xlsDoc.setCelda("Monto", "celda01");
						xlsDoc.setCelda("Fecha Vencimiento", "celda01");
						
						xlsDoc.setCelda(lsNoAmort);
						xlsDoc.setCelda(Comunes.formatoDecimal(new Double(lsImporteTotal),2));
						xlsDoc.setCelda(lsFechaVenc);
						xlsDoc.cierraTabla();
						
						xlsDoc.creaFila();
						xlsDoc.setTabla(1, 5);			
						xlsDoc.setCelda(lsDomicilioPago, "formast", ComunesXLS.CENTER, 3);
						xlsDoc.setCelda(lsFechaOper, "formast", ComunesXLS.CENTER, 3);
						xlsDoc.setCelda(lsNombre, "formast", ComunesXLS.CENTER, 3);			
						xlsDoc.setCelda(lsPuesto+" "+lsDatos, "formast", ComunesXLS.CENTER, 3);
						
						xlsDoc.cierraTabla();
						
						xlsDoc.creaFila();
						xlsDoc.creaFila();
						xlsDoc.creaFila();
					
						
						
					}else if(tipo.equals("PDF") && accionBoton.equals("Consultar"))  {				
						
						if(numReg==0){
							if ("F".equals(lsTipoOperacion)) {
								pdfDoc.addText("FORMATO DE CONSTANCIA DE DEPOSITO DE DOCUMENTOS DESCONTADOS PARA OPERACIONES DE FACTORAJE ELECTRONICO ","formasrepB",ComunesPDF.CENTER);
							} else {
								pdfDoc.addText("FORMATO DE CERTIFICADO DE DEPOSITO DE \n TITULOS DE CREDITO EN ADMINISTRACI�N  ","formasrepB",ComunesPDF.CENTER);
							} 
						
							pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);
				
							if ("F".equals(lsTipoOperacion)) {
								mensaje = " De conformidad con el convenio que "+lsNombre+" y en la fecha que posteriormente se se�ala. celebr� con Nacional Financiera S.N.C. bajo protesta de decir verdad, "+
											 " se hace constar que para todos los efectos legales, nos asumimos como depositarios de los t�tulo(s) de cr�dito y/o documento(s) electr�nico(s) sin derecho a honorarios y asumiendo "+
											 " la responsabilidad civil y penal correspondiente al car�cter de depositario judicial, el(los) titulo(s) de cr�dito y/o documento(s) electr�nico(s) en que se consignan los derechos de "+
											 " cr�dito derivados de las operaciones de factoraje financiero electr�nicas, celebradas con los clientes que se describen en el cuadro siguiente, en donde se especifica la fecha de presentaci�n, "+
											 " importe total y nombre de la empresa acreditada.\n\n"+
											 " La informaci�n del documento en   por un importe de $"+Comunes.formatoDecimal(new Double(lsImporte),2) +" citado, est�n registrados en el sistema denominado Nafin Electr�nico a disposici�n de Nacional Financiera S.N.C.  cuando esta los requiera.\n\n "+
											 " Trat�ndose del t�tulo de cr�dito electr�nico, este se encuentra debidamente transmitido en propiedad y con nuestra responsabilidad a favor de Nacional Financiera S.N.C.. En el caso del documento electr�nico, en este acto cedemos a dicha instituci�n de Banca de Desarrollo los derechos  de cr�dito que se derivan de los mismos\n\n"+
											 " En terminos del art�culo 2044 del C�digo Civil para el Distrito Federal, la cesi�n a la que se refiere el p�rrafo anterior, la realizamos con nuestra responsabilidad, respecto de la solvencia de los emisores, por el tiempo en el que permanezcan vigentes los adeudos y hasta su liquidaci�n final\n\n  "+
											 " As� mismo y en t�rminos de lo estipulado en el Convenio antes mencionado, nos obligamos a efectuar el cobro del t�tulo de cr�dito electr�nico y/o ejercer los derechos de cobro consignados en el documento objeto de descuento, as� como ejecutar todos los actos que sean necesarios para que el documento se�alado conserve su valor y dem�s derechos que les correspondan. ";		
							} else {
								mensaje = " Obran en nuestro poder y nos obligamos a conservar en dep�sito en administraci�n a disposici�n de Nacional Financiera, S.N.C. el (los) t�tulo (s) de cr�dito por los importes que abajo se detalla (n), suscrito (s) conforme a la Ley General de T�tulos y Operaciones de Cr�dito, el (los) que descontamos con Nacional Financiera, S.N.C. consider�ndose para todos los efectos legales que somos depositarios de los mismos.\n\n"+
								"	Sin cargo alguno para Nacional Financiera, S.N.C. quedamos obligados a efectuar el cobro del (de los) referido(s) t�tulo(s) a su vencimiento, y a efectuar, en su caso, los dem�s actos a que se refiere el Art�culo 278 de la Ley General de T�tulos y Operaciones de Cr�dito.\n\n "+
								"	El (los) citado (s) t�tulo(s) ha (n) sido endosado (s) en propiedad con nuestra responsabilidad a Nacional Financiera, S.N.C., en virtud del descuento del (de los) mismo (s) en los t�rminos del contrato de apertura de cr�dito que con la fecha que a continuaci�n se describe celebramos con dicha Instituci�n. ";
							} 
							
							pdfDoc.addText(mensaje,"formasrep",ComunesPDF.JUSTIFIED);
												
							pdfDoc.setTable(1, 50);
							pdfDoc.setCell("Importe Total              $"+Comunes.formatoDecimal(lsImporteTotal,2,true),"formasB", ComunesPDF.CENTER, 1, 1, 0);
							pdfDoc.setCell("Fecha de Contrato          "+lsFechaContrato,"formasB", ComunesPDF.CENTER, 1, 1, 0);
							pdfDoc.addTable();
							
							pdfDoc.setTable(11, 100);
							pdfDoc.setCell("Nombre del Emisor","celda01rep",ComunesPDF.CENTER); 
							pdfDoc.setCell("Beneficiario","celda01rep",ComunesPDF.CENTER); 
							pdfDoc.setCell("Clase de Documento","celda01rep",ComunesPDF.CENTER); 
							pdfDoc.setCell("Lugar de Emisi�n","celda01rep",ComunesPDF.CENTER); 
							pdfDoc.setCell("Fecha de Emisi�n","celda01rep",ComunesPDF.CENTER); 
							pdfDoc.setCell("Domicilio de Pago","celda01rep",ComunesPDF.CENTER); 
							pdfDoc.setCell("Tasa de Interes","celda01rep",ComunesPDF.CENTER); 
							pdfDoc.setCell("N�mero de Documento","celda01rep",ComunesPDF.CENTER); 
							pdfDoc.setCell("Fecha de Vencimiento","celda01rep",ComunesPDF.CENTER); 
							pdfDoc.setCell("Valor Nominal","celda01rep",ComunesPDF.CENTER); 
							pdfDoc.setCell("N�mero de Pr�stamo","celda01rep",ComunesPDF.CENTER); 											
						}	
						
						pdfDoc.setCell(lsEmisor,"formasrep",ComunesPDF.LEFT);
						pdfDoc.setCell(lsNombre,"formasrep",ComunesPDF.LEFT);
					    pdfDoc.setCell(lsClaseDocto,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(lsLugarEmision,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(lsFechaEmision,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(lsDomicilioPago,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(lsTasaIF,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(lsNumDocto,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(lsFechaVenc,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(lsImporteTotal,2,true),"formasrep",ComunesPDF.RIGHT);
						pdfDoc.setCell(lsNumPrestamo,"formasrep",ComunesPDF.CENTER);				
						
					}
					
					numReg++;
				}//while
			}
			if(tipo.equals("XLS") && accionBoton.equals("Consultar") )  {
				xlsDoc.cierraXLS();
			}
			if(tipo.equals("PDF") && accionBoton.equals("Consultar"))  {
			
				pdfDoc.setCell("Total","celda01rep",ComunesPDF.RIGHT,9);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(lsImporteTotal,2,true),"formasrep",ComunesPDF.RIGHT);				
				pdfDoc.setCell(" ","formasrep",ComunesPDF.RIGHT);
				pdfDoc.addTable();
				
				pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
				
				pdfDoc.setTable(3, 30);
				pdfDoc.setCell("Amortizaci�n","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Montor","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Vencimiento","celda01rep",ComunesPDF.CENTER);				
				pdfDoc.setCell(lsNoAmort,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(lsImporteTotal,2,true),"formasrep",ComunesPDF.RIGHT);
				pdfDoc.setCell(lsFechaVenc,"formasrep",ComunesPDF.CENTER);
				pdfDoc.addTable();
				
				pdfDoc.addText(" ","formas",ComunesPDF.CENTER);
				pdfDoc.addText(lsDomicilioPago +"  "+lsFechaOper,"formas",ComunesPDF.CENTER);
				
				pdfDoc.endDocument();
			}
			
			/****************ESTA PARTE CORRESPONDE AL BOTON DE Operaciones Confirmadas por IF  **********************/
			if(accionBoton.equals("Operaciones") ) {
			
				double 		mtoTotal 		= 0;
				int registros = 0;
			
				pdfDoc.addText("\n ");
				if ("F".equals(lsTipoOperacion)) {
					pdfDoc.addText("FORMATO DE CONSTANCIA DE DEPOSITO DE DOCUMENTOS DESCONTADOS PARA OPERACIONES DE FACTORAJE ELECTRONICO","formasrepB",ComunesPDF.CENTER);
				} else {
					pdfDoc.addText("FORMATO DE CERTIFICADO DE DEPOSITO DE\nTITULOS DE CREDITO EN ADMINISTRACI�N","formasrepB",ComunesPDF.CENTER);
				}
				pdfDoc.addText("\n","formasb",ComunesPDF.CENTER);
			
		
				if ("F".equals(lsTipoOperacion)) {
					pdfDoc.addText("De conformidad con el convenio que "+lsNombre+" y en la fecha que posteriormente se se�ala. celebr� con Nacional Financiera S.N.C. bajo protesta de decir verdad, se hace constar que para todos los efectos legales, nos asumimos como depositarios de los t�tulo(s) de cr�dito y/o documento(s) electr�nico(s) sin derecho a honorarios y asumiendo la responsabilidad civil y penal correspondiente al car�cter de depositario judicial, el(los) titulo(s) de cr�dito y/o documento(s) electr�nico(s) en que se consignan los derechos de cr�dito derivados de las operaciones de factoraje financiero electr�nicas, celebradas con los clientes que se describen en el cuadro siguiente, en donde se especifica la fecha de presentaci�n, importe total y nombre de la empresa acreditada.\n \n", "formasrep");
					pdfDoc.addText("La informaci�n del documento en "+lsNomMoneda+" por un importe de $ "+Comunes.formatoDecimal(new Double(lsImporte),2)+" citado, est�n registrados en el sistema denominado Nafin Electr�nico a disposici�n de Nacional Financiera S.N.C. cuando esta los requiera.\n \n", "formasrep");
					pdfDoc.addText("Trat�ndose del t�tulo de cr�dito electr�nico, este se encuentra debidamente transmitido en propiedad y con nuestra responsabilidad a favor de Nacional Financiera S.N.C.. En el caso del documento electr�nico, en este acto cedemos a dicha instituci�n de Banca de Desarrollo los derechos  de cr�dito que se derivan de los mismos.\n \n", "formasrep");
					pdfDoc.addText("En terminos del art�culo 2044 del C�digo Civil para el Distrito Federal, la cesi�n a la que se refiere el p�rrafo anterior, la realizamos con nuestra responsabilidad, respecto de la solvencia de los emisores, por el tiempo en el que permanezcan vigentes los adeudos y hasta su liquidaci�n final.\n \n", "formasrep");
					pdfDoc.addText("As� mismo y en t�rminos de lo estipulado en el Convenio antes mencionado, nos obligamos a efectuar el cobro del t�tulo de cr�dito electr�nico y/o ejercer los derechos de cobro consignados en el documento objeto de descuento, as� como ejecutar todos los actos que sean necesarios para que el documento se�alado conserve su valor y dem�s derechos que les correspondan.\n \n", "formasrep");
				} else {
					pdfDoc.addText("Obran en nuestro poder y nos obligamos a conservar en dep�sito en administraci�n a disposici�n de Nacional Financiera, S.N.C. el (los) t�tulo (s) de cr�dito por los importes que abajo se detalla (n), suscrito (s) conforme a la Ley General de T�tulos y Operaciones de Cr�dito, el (los) que descontamos con Nacional Financiera, S.N.C. consider�ndose para todos los efectos legales que somos depositarios de los mismos.\n \n", "formasrep");
					pdfDoc.addText("Sin cargo alguno para Nacional Financiera, S.N.C. quedamos obligados a efectuar el cobro del (de los) referido(s) t�tulo(s) a su vencimiento, y a efectuar, en su caso, los dem�s actos a que se refiere el Art�culo 278 de la Ley General de T�tulos y Operaciones de Cr�dito.\n \n", "formasrep");
					pdfDoc.addText("El (los) citado (s) t�tulo(s) ha (n) sido endosado (s) en propiedad con nuestra responsabilidad a Nacional Financiera, S.N.C., en virtud del descuento del (de los) mismo (s) en los t�rminos del contrato de apertura de cr�dito que con la fecha que a continuaci�n se describe celebramos con dicha Instituci�n.\n \n", "formasrep");
				}
				
				pdfDoc.setTable(2, 50);
				pdfDoc.setCell("Importe Total:","formasrepB", ComunesPDF.RIGHT, 1, 1, 0);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(lsImporteTotalO,2),"formasrep", ComunesPDF.LEFT, 1, 1, 0);
				pdfDoc.setCell("Fecha de Contrato:","formasrepB", ComunesPDF.RIGHT, 1, 1, 0);
				pdfDoc.setCell(lsFechaContrato,"formasrep", ComunesPDF.LEFT, 1, 1, 0);
				if ("F".equals(lsTipoOperacion)) {
					pdfDoc.setCell("Fecha del Descuento:","formasrepB", ComunesPDF.RIGHT, 1, 1, 0);
					pdfDoc.setCell(lsFechaOperacion,"formasrep", ComunesPDF.LEFT, 1, 1, 0);
				}
				pdfDoc.addTable();
				
				pdfDoc.setLTable(14);
				pdfDoc.setLCell("Nombre del Emisor","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Beneficiario","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Clase de Documento","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Lugar de Emisi�n","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Emisi�n","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Domicilio de Pago","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tasa de Interes","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("N�mero de Documento","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Operaci�n","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Vencimiento","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Valor Nominal","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("N�mero de Pr�stamo","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Amortizaci�n","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Domicilio Pago","celda01rep",ComunesPDF.CENTER);
				
		
				while(rs.next()){			
					String rs_solic_portal		= (rs.getString(1) == null) ? "" : rs.getString(1);
					String rs_numero_prestamo	= (rs.getString(2) == null) ? "" : rs.getString(2);
					String rs_importe_total		= (rs.getString(3) == null) ? "" : rs.getString(3);
					String rs_emisor			   =(rs.getString(4) == null) ? "" : rs.getString(4);
					String rs_lugar_emision		=(rs.getString(5) == null) ? "" : rs.getString(5);
					String rs_fecha_emision		= (rs.getString(6) == null) ? "" : rs.getString(6);
					String rs_domicilio_pago	= (rs.getString(7) == null) ? "" : rs.getString(7);
					String rs_fecha_venc		= (rs.getString(8) == null) ? "" : rs.getString(8);
					String rs_numero_docto		= (rs.getString(9) == null) ? "" : rs.getString(9);
					String rs_fecha_oper		= (rs.getString(10) == null) ? "" : rs.getString(10);
					String rs_numero_amort		= (rs.getString(11) == null) ? "" : rs.getString(11);
					String rs_tasa_if			= (rs.getString(12) == null) ? "" : rs.getString(12);
					String rs_nom_pyme			= (rs.getString(13) == null) ? "" : rs.getString(13);
					String rs_tipo_plazo		= (rs.getString(14) == null) ? "" : rs.getString(14);
					String rs_moneda			= (rs.getString(15) == null) ? "" : rs.getString(15);
					mtoTotal += Double.parseDouble(rs_importe_total);
		
					pdfDoc.setLCell(rs_emisor,"formasrep",ComunesPDF.LEFT);
					pdfDoc.setLCell(lsNombreIF,"formasrep",ComunesPDF.LEFT);
				    pdfDoc.setLCell(lsClaseDocto,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell(rs_lugar_emision,"formasrep",ComunesPDF.LEFT);
					pdfDoc.setLCell(rs_fecha_emision,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell(rs_domicilio_pago,"formasrep",ComunesPDF.LEFT);
					pdfDoc.setLCell(rs_tasa_if,"formasrep",ComunesPDF.LEFT);
					pdfDoc.setLCell(rs_numero_docto,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell(rs_fecha_oper,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell(rs_fecha_venc,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell("$ "+Comunes.formatoDecimal(rs_importe_total,2),"formasrep",ComunesPDF.RIGHT);
					pdfDoc.setLCell(rs_numero_prestamo,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell(rs_numero_amort,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell(rs_domicilio_pago,"formasrep",ComunesPDF.LEFT);
					registros++;	
				}
					//if(registros>0) {
						pdfDoc.setLCell("Total:","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setLCell(String.valueOf(registros),"formasrep",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","celda01rep",ComunesPDF.CENTER,7);
						pdfDoc.setLCell("$ "+Comunes.formatoDecimal(mtoTotal,2),"formasrep",ComunesPDF.RIGHT);
						pdfDoc.setLCell(" ","celda01rep",ComunesPDF.CENTER,3);						
					//}
					pdfDoc.addLTable();					
					pdfDoc.addText(" ", "formasrepB", ComunesPDF.CENTER);
					pdfDoc.addText(" ", "formasrepB", ComunesPDF.CENTER);
					pdfDoc.addText(lsNombreIF, "formasrepB", ComunesPDF.CENTER);
					pdfDoc.addText(" ", "formasrepB", ComunesPDF.CENTER);
					pdfDoc.addText(lsPuesto, "formasrepB", ComunesPDF.CENTER);
					pdfDoc.addText(lsNombre, "formasrepB", ComunesPDF.CENTER);
					
					pdfDoc.endDocument();
			}
			
		
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo", e);
		} finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
		return nombreArchivo;
	}
	
	/**
	 *  metodo para Formar el combo de IF
	 * @return 
	 */
	public List  getCatalogoIF( ){
		log.info("getCatalogoIF (E) ");
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;	
		HashMap datos =  new HashMap();
		List registros  = new ArrayList();
		try{
			con.conexionDB();
			
			String SQL =" SELECT ic_if, cg_razon_social,  ig_tipo_piso" +
					"   FROM comcat_if"   +
					"  WHERE cs_habilitado = 'S' order by 2"  ;
	
			rs = con.queryDB(SQL);			
			while(rs.next()) {			
				datos = new HashMap();			
				datos.put("clave", rs.getString(1) );
				datos.put("descripcion", rs.getString(2));
				registros.add(datos);
			}
			rs.close();
			con.cierraStatement();
		
		} catch (Exception e) {
			log.error("Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("getCatalogoIF (S) ");
		return registros;
	}
	

	/**
	 * metodo para el encabezado del Boton Consulta 
	 * @return 
	 * @param ic_if
	 */
	public List  getEncabezado( String ic_if, String accionBoton  ){
		log.info("getEncabezado (E) ");
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;	
		PreparedStatement ps = null;
		StringBuffer lsSQL = new StringBuffer();
		List datos = new ArrayList();
		String lsFechaContrato ="", lsDatos ="", lsPuesto="", lsNombre="";
		
		try{
			con.conexionDB();
			
			lsSQL = new StringBuffer();
			lsSQL.append(
				" SELECT TO_CHAR (df_convenio, 'dd') dia,"   +
				"        DECODE (TO_CHAR (df_convenio, 'mm'),"   +
				"                '01', 'Enero',"   +
				"                '02', 'Febrero',"   +
				"                '03', 'Marzo',"   +
				"                '04', 'Abril',"   +
				"                '05', 'Mayo',"   +
				"                '06', 'Junio',"   +
				"                '07', 'Julio',"   +
				"                '08', 'Agosto',"   +
				"                '09', 'Septiembre',"   +
				"                '10', 'Octubre',"   +
				"                '11', 'Noviembre',"   +
				"                '12', 'Diciembre'"   +
				"               ) mes,"   +
				"        TO_CHAR (df_convenio, 'YYYY') anyo"   +
				"   FROM comrel_producto_if"   +
				"  WHERE ic_if = ? "+
				"  AND ic_producto_nafin = 0");
				ps = con.queryPrecompilado(lsSQL.toString());
				ps.setString(1, ic_if);
				rs = ps.executeQuery();
				if (rs.next()){
					lsFechaContrato = rs.getString(1)+" de "+rs.getString(2)+" de "+rs.getString(3);
				}
				rs.close();
				ps.close();
				
				if(accionBoton.equals("Consultar")){
					lsSQL = new StringBuffer();
					lsSQL.append(" SELECT per.cg_nombre || ' ' || per.cg_ap_paterno || ' ' || per.cg_ap_materno, per.cg_puesto, cif.cg_razon_social nomIF"   +
									 "  FROM com_personal_facultado per, comcat_if cif "+
									  "  WHERE per.ic_if = ? AND per.ic_producto_nafin = 0 AND per.cs_habilitado = 'S' AND cif.ic_if = ?");
			
					ps = con.queryPrecompilado(lsSQL.toString());
					ps.setString(1, ic_if);
					ps.setString(2, ic_if);		
					rs = ps.executeQuery();
					if (rs.next()) {
						lsDatos = rs.getString(1);
						lsPuesto = rs.getString(2);
						lsNombre = rs.getString(3);
					}
					rs.close();
					ps.close();
				}
				if(accionBoton.equals("Operaciones")){
				
					lsSQL = new StringBuffer();
					lsSQL.append(" SELECT "+
									" per.cg_nombre || ' ' || per.cg_ap_paterno || ' ' || per.cg_ap_materno, " +
									" per.cg_puesto "+									
									"  FROM com_personal_facultado per "+
									"  WHERE per.ic_if = ? "+
									" AND per.ic_producto_nafin = 0 "+
									" AND per.cs_habilitado = 'S' ");
			
					ps = con.queryPrecompilado(lsSQL.toString());
					ps.setString(1, ic_if);						
					rs = ps.executeQuery();
					lsNombre 	= "ERROR: Falta Parametrizar Personal Facultado";
					lsPuesto 	= "";
					if (rs.next()) {
						lsNombre = rs.getString(1);
						lsPuesto = rs.getString(2);						
					}
					rs.close();
					ps.close();
				}
				
				
				
				datos.add(lsFechaContrato);
				datos.add(lsDatos);
				datos.add(lsPuesto);
				datos.add(lsNombre);
				
		
		} catch (Exception e) {
			log.error("Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("getEncabezado (S) ");
		
		return datos;
	}
	
	/**
	 * Consulta para 
	 * Operaciones Confirmadas por IF	
	 * @return 
	 * @param txtfechaFin
	 * @param txtfechaIni
	 * @param txtfolio
	 * @param ic_if
	 */  
	public String   getConsOperaciones( String ic_if, String txtfolio, String txtfechaIni, String txtfechaFin ){
		log.info("getConsOperaciones (E) ");
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;	
		PreparedStatement ps = null;
		
		List lVarBind = new ArrayList();
		List		renglones		= new ArrayList();
		HashMap datos = new HashMap();					
		JSONArray  registros  = new JSONArray();
		String consulta ="";
		
		try{
			con.conexionDB();
			
			String 	qrySentencia = 
				" SELECT TO_CHAR (ccd.df_confirma_if, 'dd/mm/yyyy hh24:mi') df_confirma_if,"   +
				"        TO_CHAR (ccd.df_operacion, 'dd/mm/yyyy') df_operacion_,"   +
				"        ccd.ic_usuario || ' ' || ccd.cg_nombre_usuario usuario, ccd.ic_moneda, ccd.df_operacion,"   +
				"        cif.cg_razon_social nombreif "+
				"   FROM comrel_constancia_deposito ccd, comcat_if cif"   +
				"  WHERE ccd.ic_if = cif.ic_if "+
				"    AND ccd.ic_if = ?"   +
				"    AND ccd.df_operacion >= TO_DATE (?, 'dd/mm/yyyy')"   +
				"    AND ccd.df_operacion < TO_DATE (?, 'dd/mm/yyyy') + 1"  +
				"    AND ccd.ic_usuario IS NOT NULL"  +
				" ORDER BY ccd.df_operacion ";
			lVarBind = new ArrayList();
			lVarBind.add(new Integer(ic_if));
			lVarBind.add(txtfechaIni);
			lVarBind.add(txtfechaFin);

			renglones = con.consultaDB(qrySentencia, lVarBind, false);
			
			for(int i=0;i<renglones.size();i++) {
				List lDatos = (ArrayList)renglones.get(i);
				String rs_df_confirma_if	= lDatos.get(0).toString();
				String rs_df_operacion		= lDatos.get(1).toString();
				String rs_usuario			= lDatos.get(2).toString();
				String rs_moneda			= lDatos.get(3).toString();
				String rs_nombre_if			= lDatos.get(5).toString();
				
				qrySentencia = 
					" SELECT   /*+index(sp IN_COM_SOLIC_PORTAL_05_NU) use_nl(sp)*/"   +
					"           COUNT (1) operaciones, SUM (sp.fn_importe_dscto) monto,"   +
					"           mon.cd_nombre moneda, "   +
					"           DECODE (sp.cs_tipo_plazo, 'C', 'Cr�dito', 'Factoraje'), sp.cs_tipo_plazo"   +
					"     FROM com_solic_portal sp, comcat_moneda mon"   +
					"    WHERE sp.ic_moneda = mon.ic_moneda"   +
					"      AND sp.ic_if = ?"   +
					"      AND sp.ic_moneda = ?"   +
					"      AND sp.ic_estatus_solic IN (?, ?, ?)"   +
					"      AND sp.df_operacion >= TO_DATE (?, 'dd/mm/yyyy')"   +
					"      AND sp.df_operacion < (TO_DATE (?, 'dd/mm/yyyy') + 1)"  ;
					if(!"".equals(txtfolio)) {
						qrySentencia += " AND sp.ic_solic_portal = ? ";
					}					
					qrySentencia +=" GROUP BY mon.cd_nombre, DECODE (cs_tipo_plazo, 'C', 'Cr�dito', 'Factoraje'), cs_tipo_plazo";
					
					lVarBind = new ArrayList();
					lVarBind.add(new Integer(ic_if));
					lVarBind.add(new Integer(rs_moneda));
					lVarBind.add(new Integer(3));
					lVarBind.add(new Integer(5));
					lVarBind.add(new Integer(6));
					lVarBind.add(rs_df_operacion);
					lVarBind.add(rs_df_operacion);
					if(!"".equals(txtfolio)) {
						lVarBind.add(new Long(txtfolio.trim()));
					}
					List lRenglones = con.consultaDB(qrySentencia, lVarBind, false);
					
					for(int j=0;j<lRenglones.size();j++) {
						List lDatosOper = (ArrayList)lRenglones.get(j);
						String rs_num_oper 			= lDatosOper.get(0).toString();
						String rs_monto 			= lDatosOper.get(1).toString();
						String rs_nom_moneda 		= lDatosOper.get(2).toString();
						String rs_tipo_plazo 		= lDatosOper.get(3).toString();
						String rs_cve_tipo_plazo 	= lDatosOper.get(4).toString();
					
					
					
					datos = new HashMap();			
					datos.put("FECHA_ENVIO", rs_df_operacion );
					datos.put("NUM_OPERACIONES", rs_num_oper);
					datos.put("MONTO", rs_monto);
					datos.put("IC_MONEDA", rs_moneda);
					datos.put("MONEDA", rs_nom_moneda);
					datos.put("TIPO", rs_tipo_plazo);
					datos.put("USUARIO_REVISO", rs_usuario);
					datos.put("FECHA_REVISION", rs_df_confirma_if);
					datos.put("NOMBRE_IF", rs_nombre_if);
					datos.put("TIPO_PLAZO", rs_cve_tipo_plazo);
					registros.add(datos);
				}
			}
				
			consulta =  "{\"success\": true, \"total\": \"" + registros.size() + "\", \"registros\": " + registros.toString()+"}";
			
		
		} catch (Exception e) {
			log.error("Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("getConsOperaciones (S) ");
		
		return consulta;
	}
	
	/**
	 * Consulta de Confirmaci�n de Operaciones del d�a
	 * @return 
	 * @param ic_moneda
	 * @param fecha_a
	 * @param fecha_de
	 */
	public List  consConfirmacion( String fecha_de , String fecha_a, String ic_moneda   ){
		log.info("consConfirmacion (E)");
		
		AccesoDB con =new AccesoDB();
		PreparedStatement ps	= null;
		ResultSet			rs	= null;
		boolean transactionOk = true;		
		StringBuffer qrySentencia = new StringBuffer("");		
		List varBind = new ArrayList();	
		List respuesta = new ArrayList();
		int numRegistros  =0, numRegistrosDL =0;
							
		try{	
		
			con.conexionDB();	
	
			qrySentencia = new StringBuffer("");			
			qrySentencia.append( 
				" SELECT sp.ic_if, cif.cg_razon_social, COUNT (1), SUM (sp.fn_importe_dscto), t.ic_moneda "   +
				"   FROM com_solic_portal sp, comcat_if cif, comcat_tasa t"   +
				"  WHERE sp.ic_tasaif = t.ic_tasa"   +
				"    AND sp.ic_if = cif.ic_if"   +
				"    AND ic_estatus_solic IN (3,  5,  6)"   +
				"    AND sp.df_operacion >= TO_DATE (?, 'dd/mm/yyyy')"   +
				"    AND sp.df_operacion < TO_DATE (?, 'dd/mm/yyyy')+1"   +
				"  GROUP BY sp.ic_if,  cif.cg_razon_social,  t.ic_moneda"  );
				
			varBind = new ArrayList();
			varBind.add(fecha_de);
			varBind.add(fecha_a);  				
			ps = con.queryPrecompilado(qrySentencia.toString(), varBind);
			rs = ps.executeQuery();
			
			log.debug("qrySentencia  "+qrySentencia.toString());
			log.debug("varBind "+varBind );
			
			Vector doctoMN = new Vector();
			Vector doctoDL = new Vector();
			String confirmaMN = "N";
			String confirmaDL = "N";
			while(rs.next()) {
				Vector cols = new Vector();
				String rs_ic_if 	= rs.getString(1)==null?"":rs.getString(1);
				String rs_if 		= rs.getString(2)==null?"":rs.getString(2);
				String rs_numOper 	= rs.getString(3)==null?"":rs.getString(3);
				String rs_monto 	= rs.getString(4)==null?"":rs.getString(4);
				String rs_moneda 	= rs.getString(5)==null?"":rs.getString(5);
				cols.addElement(rs_ic_if);
				cols.addElement(rs_if);
				cols.addElement(rs_numOper);
				cols.addElement(rs_monto);
				if("1".equals(rs_moneda)){
					doctoMN.addElement(cols);
				}	else if("54".equals(rs_moneda)) {
					doctoDL.addElement(cols);
				}
			}//while(rs.next())
			rs.close();
			ps.close();
						
			qrySentencia = new StringBuffer("");			
			qrySentencia.append(
				" SELECT COUNT (1)"   +
				"   FROM comrel_constancia_deposito"   +
				"  WHERE ic_moneda = ? "   +
				"    AND cs_confirmacion = 'S'"   +
				"    AND df_operacion >= TO_DATE (?, 'dd/mm/yyyy')"   +
				"    AND df_operacion < TO_DATE (?, 'dd/mm/yyyy')+1 ")   ;
			varBind = new ArrayList();
			varBind.add("1");
			varBind.add(fecha_de);
			varBind.add(fecha_a);  				
			ps = con.queryPrecompilado(qrySentencia.toString(), varBind);
			rs = ps.executeQuery();
			if(rs.next()) {
				if(rs.getInt(1)>0)
					confirmaMN = "S";
			}
			rs.close();
			ps.close();
		
			qrySentencia = new StringBuffer("");			
			qrySentencia.append(
				" SELECT COUNT (1)"   +
				"   FROM comrel_constancia_deposito"   +
				"  WHERE ic_moneda = ? "   +
				"    AND cs_confirmacion = 'S'"   +
				"    AND df_operacion >= TO_DATE (?, 'dd/mm/yyyy')"   +
				"    AND df_operacion < TO_DATE (?, 'dd/mm/yyyy')+1" )   ;
			varBind = new ArrayList();
			varBind.add("54");
			varBind.add(fecha_de);
			varBind.add(fecha_a); 
			ps = con.queryPrecompilado(qrySentencia.toString(), varBind);
			rs = ps.executeQuery();
			if(rs.next()) {
				if(rs.getInt(1)>0)
					confirmaDL = "S";
			}
			rs.close();
			ps.close();
			Vector registros;
			double totalImporte = 0;
			double totalImporteDL = 0;
		
			HashMap datos =  new HashMap();
			JSONArray  regi  = new JSONArray();
			if(ic_moneda.equals("1")){
				if(doctoMN.size()>0) {
					for(int i=0;i<doctoMN.size();i++) {	
						registros = (Vector)doctoMN.get(i);
						numRegistros += Integer.parseInt(registros.get(2).toString());
						totalImporte += Double.parseDouble(registros.get(3).toString());
				
						datos =  new HashMap();
						datos.put("IC_IF",registros.get(0).toString());
						datos.put("NOMBRE_IF",registros.get(1).toString());
						datos.put("NUM_OPERACIONES",registros.get(2).toString());
						datos.put("MONTO",Comunes.formatoMN(registros.get(3).toString()));
						datos.put("CHECKBOX","");
						datos.put("CONFIRMA","");
						regi.add(datos);				
					}
					
					datos =  new HashMap();
					datos.put("IC_IF","");
					datos.put("NOMBRE_IF","");
					datos.put("NUM_OPERACIONES","Total de Registros");
					datos.put("MONTO",String.valueOf(numRegistros));
					datos.put("CHECKBOX","");
					datos.put("CONFIRMA","");
					regi.add(datos);
					
					datos =  new HashMap();
					datos.put("IC_IF","");
					datos.put("NOMBRE_IF","");
					datos.put("NUM_OPERACIONES","Total Importe en M.N");
					datos.put("MONTO",Comunes.formatoMN(totalImporte+""));
					datos.put("CHECKBOX","");
					datos.put("CONFIRMA","");
					regi.add(datos);
					
					datos =  new HashMap();
					datos.put("IC_IF","");
					datos.put("NOMBRE_IF","");
					datos.put("NUM_OPERACIONES","Confirmaci�n de Operaciones al D�a ");
					datos.put("MONTO","");
					datos.put("CHECKBOX","SI");
					datos.put("CONFIRMA",confirmaMN);
					regi.add(datos);	
				}
			}
			
			if(ic_moneda.equals("54")){
			
				if(doctoDL.size()>0) {
				
				for(int i=0;i<doctoDL.size();i++) {
					registros = (Vector)doctoDL.get(i);
					numRegistrosDL += Integer.parseInt(registros.get(2).toString());
					totalImporteDL += Double.parseDouble(registros.get(3).toString());
					
					datos =  new HashMap();
					datos.put("IC_IF",registros.get(0).toString());
					datos.put("NOMBRE_IF",registros.get(1).toString());
					datos.put("NUM_OPERACIONES",registros.get(2).toString());
					datos.put("MONTO",Comunes.formatoMN(registros.get(3).toString()));
					datos.put("CHECKBOX","");
					datos.put("CONFIRMA","");
					regi.add(datos);				
				}
					datos =  new HashMap();
					datos.put("IC_IF","");
					datos.put("NOMBRE_IF","");
					datos.put("NUM_OPERACIONES","Total de Registros");
					datos.put("MONTO",String.valueOf(numRegistrosDL));
					datos.put("CHECKBOX","");
					datos.put("CONFIRMA","");
					regi.add(datos);
					
					datos =  new HashMap();
					datos.put("IC_IF","");
					datos.put("NOMBRE_IF","");
					datos.put("NUM_OPERACIONES","Total Importe en M.N");
					datos.put("MONTO",Comunes.formatoMN(totalImporteDL+""));
					datos.put("CHECKBOX","");
					datos.put("CONFIRMA","");
					regi.add(datos);
					
					datos =  new HashMap();
					datos.put("IC_IF","");
					datos.put("NOMBRE_IF","");
					datos.put("NUM_OPERACIONES","Confirmaci�n de Operaciones al D�a ");
					datos.put("MONTO","");
					datos.put("CHECKBOX","SI");
					datos.put("CONFIRMA",confirmaDL);
					regi.add(datos);	
							
				}
			}
		
			String consulta =  "{\"success\": true, \"total\": \"" + regi.size() + "\", \"registros\": " + regi.toString()+"}";
			respuesta.add(consulta);
			respuesta.add(String.valueOf(numRegistros));
			respuesta.add(String.valueOf(numRegistrosDL));
			
					
	} catch(Exception e) {
		transactionOk=false;	
		e.printStackTrace();
		log.error("error al consConfirmacion  " +e); 
	} finally {	
		if(con.hayConexionAbierta()) {
			con.terminaTransaccion(transactionOk);
			con.cierraConexionDB();	
		}
	}    
	log.info("consConfirmacion (S) ");
		return respuesta;
	}
	
	/**
	 * Confirmaci�n de Operaciones del d�a
	 * @return 
	 * @param confirmaOperacionDL
	 * @param confirmaOperacionMN
	 * @param ic_if_dl
	 * @param ic_if_mn
	 * @param fecha
	 */
	public String  AceptaConfirmacion( String fecha , String ic_if_mn [],  String ic_if_dl [], String numRegistrosMN , String numRegistrosDL, String confirmaOperacionMN, String confirmaOperacionDL ){
		log.info("AceptaConfirmacion (E)");
		
		AccesoDB con =new AccesoDB();
		PreparedStatement ps	= null;
		ResultSet			rs	= null;
		boolean transactionOk = true;		
		StringBuffer qrySentencia = new StringBuffer("");		
		List varBind = new ArrayList();	
		String mensaje ="";
		
		try{	
			con.conexionDB();	
			String ic_if = "";
			
			if(Integer.parseInt(numRegistrosMN) >0) {
				for(int i=0;i<ic_if_mn.length;i++) {
			
					ic_if = ic_if_mn[i];
					
					qrySentencia = new StringBuffer("");
					qrySentencia.append(
						" SELECT COUNT (1)"   +
						"   FROM comrel_constancia_deposito"   +
						"  WHERE ic_if = ?"   +
						"    AND ic_moneda = 1"   +
						"    AND df_operacion >= TO_DATE (?, 'dd/mm/yyyy')"   +
						"    AND df_operacion < (TO_DATE (?, 'dd/mm/yyyy') + 1)" ) ;
						
					varBind = new ArrayList();	
					varBind.add(ic_if);
					varBind.add(fecha);
					varBind.add(fecha);
					ps = con.queryPrecompilado(qrySentencia.toString(),varBind );			
					rs = ps.executeQuery();
					int existe = 0;
					if(rs.next()){
						existe = rs.getInt(1);
					}
					rs.close();
					ps.close();
					
					if(existe==0) {
						qrySentencia = new StringBuffer("");
						qrySentencia.append(" INSERT INTO comrel_constancia_deposito"   +
						"             (ic_if, df_operacion, ic_moneda, cs_confirmacion)"   +
						"      VALUES(?, TO_DATE (?, 'dd/mm/yyyy'), 1, ?)");
						
						varBind = new ArrayList();	
						varBind.add(ic_if);
						varBind.add(fecha);	
						varBind.add(confirmaOperacionMN);	
						ps = con.queryPrecompilado(qrySentencia.toString(), varBind);
						
					} else {
						qrySentencia = new StringBuffer("");
						qrySentencia.append(
						" UPDATE comrel_constancia_deposito"   +
						"    SET cs_confirmacion = ?"   +
						"  WHERE ic_if = ?"   +
						"    AND ic_moneda = ? "   +
						"    AND df_operacion >= TO_DATE (?, 'dd/mm/yyyy')"   +
						"    AND df_operacion < (TO_DATE (?, 'dd/mm/yyyy') + 1)" ) ;
						varBind = new ArrayList();	
						varBind.add(confirmaOperacionMN);
						varBind.add(ic_if);
						varBind.add("1");
						varBind.add(fecha);
						varBind.add(fecha);						
						ps = con.queryPrecompilado(qrySentencia.toString(), varBind);
						
					}
					ps.executeUpdate();
					ps.close();
				}//for(int i=0;i<ic_if_mn.length;i++)
			}
			
			if(Integer.parseInt(numRegistrosDL) >0) {
				for(int i=0;i<ic_if_dl.length;i++) {
					ic_if = ic_if_dl[i];
					qrySentencia = new StringBuffer("");
					qrySentencia.append(
						" SELECT COUNT (1)"   +
						"   FROM comrel_constancia_deposito"   +
						"  WHERE ic_if = ?"   +
						"    AND ic_moneda = ? "   +
						"    AND df_operacion >= TO_DATE (?, 'dd/mm/yyyy')"   +
						"    AND df_operacion < (TO_DATE (?, 'dd/mm/yyyy') + 1)" ) ;
			
					varBind = new ArrayList();	
					varBind.add(ic_if);
					varBind.add("54");
					varBind.add(fecha);
					varBind.add(fecha);
					ps = con.queryPrecompilado(qrySentencia.toString(), varBind);
					rs = ps.executeQuery();
					int existe = 0;
					if(rs.next()){
						existe = rs.getInt(1);
					}	
					rs.close();
					ps.close();
					if(existe==0) {
						qrySentencia = new StringBuffer("");
						qrySentencia.append(
						" INSERT INTO comrel_constancia_deposito"   +
						"             (ic_if, df_operacion, ic_moneda, cs_confirmacion)"   +
						"      VALUES(?, TO_DATE (?, 'dd/mm/yyyy'), ? , ?)")  ;
				
						varBind = new ArrayList();	
						varBind.add(ic_if);
						varBind.add(fecha);
						varBind.add("54");
						varBind.add(confirmaOperacionDL);
						ps = con.queryPrecompilado(qrySentencia.toString(), varBind);
				
					} else {
						qrySentencia = new StringBuffer("");
						qrySentencia.append(
							" UPDATE comrel_constancia_deposito"   +
							"    SET cs_confirmacion = ?"   +
							"  WHERE ic_if = ?"   +
							"    AND ic_moneda = ? "   +
							"    AND df_operacion >= TO_DATE (?, 'dd/mm/yyyy')"   +
							"    AND df_operacion < (TO_DATE (?, 'dd/mm/yyyy') + 1)")  ;
						
						varBind = new ArrayList();							
						varBind.add(confirmaOperacionDL);
						varBind.add(ic_if);
						varBind.add("54");
						varBind.add(fecha);
						varBind.add(fecha);
						ps = con.queryPrecompilado(qrySentencia.toString(), varBind);
				
					}
					ps.executeUpdate();
					if(ps!=null) ps.close();
				}//for(int i=0;i<ic_if_mn.length;i++)
			}
			mensaje = "Los cambios se almacenaron satisfactoriamente";
			con.terminaTransaccion(true);
			
		} catch(Exception e) {
			transactionOk=false;	
			e.printStackTrace();
			log.error("error al AceptaConfirmacion  " +e); 
		} finally {	
			if(con.hayConexionAbierta()) {
				con.terminaTransaccion(transactionOk);
				con.cierraConexionDB();	
			}
		}    
		log.info("AceptaConfirmacion (S) ");
		return mensaje;
	}
	
//*********************Concentrado del d�a **********************************
/**
 * Genera el archivo de constancias de credito
 * 
 * @param	fecha	Fecha en formato dd/mm/aaaa
 * @param	strDirectorioTemp	Ruta del directorio de almacenamiento
 * @return	String	Nombre del archivo generado. Regresa una
 * 			cadena vacia si no se encontr� informaci�n.
 */
public String generarArchivo(String fecha, 	String strDirectorioTemp) throws Exception {

	AccesoDB con = new AccesoDB();
	StringBuffer contenidoArch = new StringBuffer(1200);	//Tma�o inicial de 1200
	
	ResultSet rs = null;
	ResultSet rsIF = null;
	ResultSet rsPersonal = null;	
	PreparedStatement	ps = null;
	
	String nombreMes[] = {"enero", "febrero", "marzo", "abril", "mayo",	"junio","julio","agosto","septiembre","octubre","noviembre","diciembre"};
	
	Calendar cal = Calendar.getInstance();
	java.util.Date fechaConvenio = null;
	String fechaContrato = null;
	String dia = null;	//Dia del convenio
	String mes = null;	//Nombre del mes del convenio
	String anio = null;	//A�o del convenio
	String puesto = null;
	String datos = null;
	String textFirma = null;
	List lArchivos = new ArrayList();
	String nombreArchivoZIP = "";	
	int numTotalRegistros = 0;
	double granTotal = 0;
	
	String lsTextTitulo =
			"\nFORMATO DE CERTIFICADO DE DEPOSITO DE \n" +
			"TITULOS DE CREDITO EN ADMINISTRACION" + 
			"\nObran en nuestro poder y nos obligamos a conservar en dep�sito " +
			"en administraci�n a disposici�n de Nacional Financiera S.N.C. " +
			"el (los) t�tulo (s) de cr�dito por los importes que abajo se " +
			"detalla (n) suscrito (s) conforme a la Ley General de T�tulos y " +
			"Operaciones de Cr�dito el (los) que descontamos con Nacional " +
			"Financiera S.N.C. consider�ndose para todos los efectos legales " +
			"que somos depositarios de los mismos.\n" +
			"Sin cargo alguno para Nacional Financiera S.N.C. quedamos " +
			"obligados a efectuar el cobro del (de los) referido(s) t�tulo(s) " +
			"a su vencimiento y a efectuar en su caso los dem�s actos a que se " +
			"refiere el Art�culo 278 de la Ley General de T�tulos y Operaciones " +
			"de Cr�dito.\n" +
			"El (los) citado (s) t�tulo(s) ha (n) sido endosado (s) en " +
			"propiedad con nuestra responsabilidad a Nacional Financiera " +
			"S.N.C. en virtud del descuento del (de los) mismo (s) en los " +
			"t�rminos del contrato de apertura de cr�dito que con la fecha que " +
			"a continuaci�n se describe celebramos con dicha Instituci�n.\n" +
			"\n";

	String lsTitulosTabla = 
			"\n\n Nombre del emisor,Beneficiario,Clase de Documento," +
			"Lugar de Emision,Fecha de Emision,Domicilio de Pago,"+
			"Tasa de Interes,Numero de Documento,Fecha Vencimiento,"+
			"Valor Nominal,Numero de Prestamo,Tipo de Documento\n";
		

	String sql = 
			" SELECT /*+ index (sp in_com_solic_portal_05_nuk) index (py IN_COMCAT_PYME_12_NUK)*/ " +
			"   sp.ic_solic_portal as Folio, " +
			" 	sp.ig_numero_prestamo as NumPrestamo, " +
			" 	nvl(sp.fn_importe_dscto, 0) as ImporteTotal, " +
			" 	nvl(ce.cd_descripcion,' ') as Emisor, " +
			" 	nvl(sp.cg_lugar_firma, ' ') as LugarEmision, " +
			" 	nvl(to_char(sp.df_etc, 'dd/mm/yyyy'), ' ') as FechaEmision, " +
			" 	nvl(sp.cg_domicilio_pago, ' ') as DomicilioPago, " +
			" 	nvl(to_char(sp.df_v_descuento, 'dd/mm/yyyy'), ' ') as FechaVenc, " +
			" 	nvl(sp.ig_numero_docto, 0) as NumDocto, " +
			" 	TO_CHAR(sp.df_operacion, 'dd/mm/yyyy') as FechaOper, " +
			" 	nvl(sp.in_numero_amort, 0) as NoAmort, " +
			" 	t.cd_nombre as TasaIF, sp.ic_if " +
			"	,py.cg_razon_social as pyme" +
			"  , sp.ic_moneda " +
			" FROM com_solic_portal sp, comcat_pyme py, comcat_emisor ce, comcat_tasa t " +
			" WHERE sp.ic_emisor = ce.ic_emisor(+) " +
			" 	AND sp.ic_tasaif = t.ic_tasa " +
			" 	AND ic_estatus_solic in (?, ?, ?) " +
			"  AND sp.ig_clave_sirac = py.in_numero_sirac (+)" +
			" 	And sp.df_operacion >= to_date(?, 'dd/mm/yyyy') " +
			" 	And sp.df_operacion < to_date(?,'dd/mm/yyyy') + 1 " +
			" ORDER BY sp.ic_if, sp.ic_moneda, sp.ic_solic_portal ";
	
	String sqlNE = 
			" SELECT ic_nafin_electronico " +
			" FROM comrel_nafin " +
			" WHERE ic_epo_pyme_if = ? " +
			" AND cg_tipo = ? ";
	
	try {
		con.conexionDB();
		ps = con.queryPrecompilado(sql);
		ps.setInt(1,3); //3=Operada
		ps.setInt(2,5); //5=Operada Pagada
		ps.setInt(3,6); //6=Operada Pendiente de pago
		ps.setString(4, fecha);
		ps.setString(5, fecha);
		rs = ps.executeQuery();
		String claveIFPrevia = "";
		int monedaPrevia = -1;
		String monedaNombre = "";

		PreparedStatement psNE = con.queryPrecompilado(sqlNE);

		boolean hayRegistros = false;
		String nombreArchivo = null;	
		while (rs.next()) {
			hayRegistros = true;
			String folio = rs.getString("FOLIO");
			String numPrestamo = rs.getString("NUMPRESTAMO");
			double importeTotal = rs.getDouble("IMPORTETOTAL");
			String emisor = rs.getString("EMISOR");
			String lugarEmision = rs.getString("LUGAREMISION");
			String fechaEmision = rs.getString("FECHAEMISION");
			String domicilioPago = rs.getString("DOMICILIOPAGO");
			String fechaVenc = rs.getString("FECHAVENC");
			String numDocto = rs.getString("NUMDOCTO");
			String fechaOper = rs.getString("FECHAOPER");
			String noAmort = rs.getString("NOAMORT");
			String tasaIF = rs.getString("TASAIF");
			String claveIFActual = rs.getString("IC_IF");
			String nombre = (rs.getString("PYME")== null)?"":rs.getString("PYME");
			int moneda = (rs.getString("IC_MONEDA")== null)?1:rs.getInt("IC_MONEDA");
			if ( !claveIFActual.equals(claveIFPrevia) || moneda != monedaPrevia ) { //Si hay cambio de IF �  moneda

				nombreArchivo = null;
				if (numTotalRegistros > 0) {
					//Generar archivo
					contenidoArch.append(
							" \n \n N�mero Total de Registros:," + numTotalRegistros + "\n" +
							"Importe Total:," +
							Comunes.formatoDecimal(granTotal, 2, false)
					);
					nombreArchivo = guardarArchivo(contenidoArch, strDirectorioTemp, psNE, claveIFPrevia, monedaNombre, fecha );
					lArchivos.add(nombreArchivo);
					
				}
				
				//Resetea valores
				numTotalRegistros = 0;
				granTotal = 0;
				if (contenidoArch.length() > 0) {
					contenidoArch.delete(0, contenidoArch.length());  //Limpia el contenido del StringBuffer
				}

			}
			numTotalRegistros++;
			granTotal += importeTotal;
					
			if (monedaPrevia != moneda) {
				monedaPrevia = moneda;
				//MN = 1, USD = 54
				monedaNombre = (moneda == 1)?"MN":"USD";
			}
			
			/*
				Solo si cambia la clave del IF se obtiene la fecha de convenio,
				y la firma.
			*/
			if (!claveIFActual.equals(claveIFPrevia)) {
				
				String sqlIF = 
						" SELECT df_convenio " +
						" FROM comrel_producto_if " +
						" WHERE ic_if = ? " +
						" AND ic_producto_nafin = ? ";
				
				PreparedStatement psIF = con.queryPrecompilado(sqlIF);
				psIF.setInt(1, Integer.parseInt(claveIFActual));
				psIF.setInt(2, 1); //1= Descuento Electronico
				
				rsIF = psIF.executeQuery();
				
				fechaContrato = "";
				dia = mes = anio = "";
				if (rsIF.next()) {
					fechaConvenio = rsIF.getDate("DF_CONVENIO");
					if (fechaConvenio != null) {
						cal.setTime(fechaConvenio);
						dia = String.valueOf(cal.get(Calendar.DATE));
						mes = nombreMes[cal.get(Calendar.MONTH)];
						anio = String.valueOf(cal.get(Calendar.YEAR));
						fechaContrato = dia + " de " + mes +
								" de " + anio;
					}
				}
				rsIF.close();
				psIF.close();
				
				String sqlPersonal = 
						" Select cg_nombre||' '||cg_ap_paterno||' '||cg_ap_materno, " +
	  					" 	cg_puesto " +
						" From com_personal_facultado " +
						" Where ic_if = ? " +
						" 	And ic_producto_nafin = ? ";  
				PreparedStatement psPersonal = con.queryPrecompilado(sqlPersonal);
				psPersonal.setInt(1, Integer.parseInt(claveIFActual));
				psPersonal.setInt(2, 1); //1= Descuento Electronico
				
				rsPersonal = psPersonal.executeQuery();
				datos =  " ";
				puesto = " ";
	
				if (rsPersonal.next()){
					datos = (rsPersonal.getString(1)==null)?"":rsPersonal.getString(1);
					puesto =(rsPersonal.getString(2)==null)?"":rsPersonal.getString(2);
				}
				rsPersonal.close();
				psPersonal.close();
				
				textFirma = puesto + " " + datos;
				claveIFPrevia = claveIFActual;
			} //fin !claveIFActual.equals(claveIFPrevia)
			
			contenidoArch.append(lsTextTitulo + "Importe Total " + importeTotal + "\n" +	"Fecha de Contrato " + fechaContrato);	
			contenidoArch.append(lsTitulosTabla);
			
			contenidoArch.append(
					emisor +
					"," + nombre +
					"," + "PAGARE" +
					"," + lugarEmision.replace(',',' ') +
					"," + fechaEmision +
					"," + domicilioPago.replace(',',' ') +
					"," + tasaIF +
					"," + numDocto +
					"," + fechaVenc +
					"," + importeTotal +
					"," + numPrestamo +
					"," + "Certificado"
			);
			String fechaOperTexto = "";
			if (fechaOper != null && !fechaOper.equals("")) {
				StringTokenizer st=new StringTokenizer(fechaOper,"/");
				dia = st.nextToken();
				mes = nombreMes[Integer.parseInt(st.nextToken()) - 1];
				anio = st.nextToken();

				fechaOperTexto = dia + " de " + mes +
						" de " + anio;
			}

			contenidoArch.append(
					"\n,,,,,,,,Total," + importeTotal +
					"\n Amortizacion,Monto,FechaVencimiento " +
					"\n "+noAmort+","+importeTotal+","+fechaVenc +
					"\n" + domicilioPago + " a " + fechaOperTexto);
	
			contenidoArch.append(" " + textFirma + "\n \n \n");
		
		}	//fin del while
		if (numTotalRegistros > 0) {  //Guarda los registros del ultimo bloque
			//Generar archivo
			contenidoArch.append(
					" \n \n N�mero Total de Registros:," + numTotalRegistros + "\n" +
					"Importe Total:," +
					Comunes.formatoDecimal(granTotal, 2, false)
			);
			nombreArchivo = guardarArchivo(contenidoArch, strDirectorioTemp, psNE, claveIFPrevia, monedaNombre, fecha );
			//System.out.println("nombreArchivo" + nombreArchivo);
			lArchivos.add(nombreArchivo);
		}
		psNE.close();		
		
		rs.close();
		ps.close();
		
		if (hayRegistros) {
			String fechaArchivo = new SimpleDateFormat("yyyyMMdd").format(Comunes.parseDate(fecha));
			nombreArchivoZIP = fechaArchivo + "_Constancias_Credele.zip";
			ComunesZIP.comprimir(lArchivos,  strDirectorioTemp, 
					strDirectorioTemp + nombreArchivoZIP);
			return nombreArchivoZIP;
		} else {
			return "";
		}
	} catch(Exception e) {
		e.printStackTrace();
		throw e;
	} finally {
		if (con.hayConexionAbierta()) con.cierraConexionDB();
	}
}	//fin del metodo


/**
 * Guarda el archivo
 * @param contenidoArch Contenido del archivo
 * @param strDirectorioTemp Directorio Temporal
 * @param psNE PreparedStatement correspondiente al query con el cual 
 * 		se obtiene el N@E del IF
 * @param claveIF Clave del IF
 * @param monedaNombre nombre de la moneda en turno
 * @param fecha Fecha en formato dd/mm/aaaa
 * @return Nombre del archivo guardado
 */

private String guardarArchivo(StringBuffer contenidoArch, 
		String strDirectorioTemp, PreparedStatement psNE, String claveIF, 
		String monedaNombre, String fecha) throws Exception{

	String neIF = "";
	psNE.clearParameters();
	psNE.setInt(1, Integer.parseInt(claveIF));
	psNE.setString(2, "I");
	ResultSet rsNE = psNE.executeQuery();
	if(rsNE.next()) {
		neIF = rsNE.getString("ic_nafin_electronico");
	}
	rsNE.close();
	
	String fechaArchivo = new SimpleDateFormat("yyyyMMdd").format(Comunes.parseDate(fecha));
	
	String nombreArchivo = fechaArchivo + "_" + "IF" + neIF + "_credele_" + monedaNombre;
			
	CreaArchivo archivo = new CreaArchivo();
				
	if (!archivo.make(contenidoArch.toString(), strDirectorioTemp, nombreArchivo, ".csv")) {
		System.out.println("Error en la generacion del archivo");
		throw new NafinException("SIST0001");
	}
	
	return nombreArchivo + ".csv";
}



//*********************Total del d�a **********************************

/**
 * Genera el archivo de constancias de credito, para cualquier IF
 * 
 * @param	fechaInicial	Fecha inicial
 * @param	fechaFinal	Fecha final
 * @param	strDirectorio	Ruta del directorio de almacenamiento
 * @return	String	Nombre del archivo generado. Regresa una
 * 			cadena vacia si no se encontr� informaci�n.  
 */
public String generarArchivo(String fechaInicial, String fechaFinal,	String strDirectorioTemp) throws Exception {

	AccesoDB con = new AccesoDB();

	StringBuffer contenidoArch = new StringBuffer(1200);	//Tma�o inicial de 1200
	
	ResultSet rs = null;
	ResultSet rsIF = null;
	ResultSet rsPersonal = null;
	
	PreparedStatement	ps = null;
	
	String nombreMes[] = {"enero", "febrero", "marzo", "abril", "mayo",
			"junio","julio","agosto","septiembre","octubre",
			"noviembre","diciembre"};
	
	Calendar cal = Calendar.getInstance();
	java.util.Date fechaConvenio = null;

	String fechaContrato = null;
	String dia = null;	//Dia del convenio
	String mes = null;	//Nombre del mes del convenio
	String anio = null;	//A�o del convenio
	String puesto = null;
	String datos = null;
	String textFirma = null;
	
	int numTotalRegistrosMN = 0;
	double granTotalMN = 0;
	int numTotalRegistrosDL = 0;
	double granTotalDL = 0;
	
	String lsTextTitulo =
			"\nFORMATO DE CERTIFICADO DE DEPOSITO DE \n" +
			"TITULOS DE CREDITO EN ADMINISTRACION" + 
			"\nObran en nuestro poder y nos obligamos a conservar en dep�sito " +
			"en administraci�n a disposici�n de Nacional Financiera S.N.C. " +
			"el (los) t�tulo (s) de cr�dito por los importes que abajo se " +
			"detalla (n) suscrito (s) conforme a la Ley General de T�tulos y " +
			"Operaciones de Cr�dito el (los) que descontamos con Nacional " +
			"Financiera S.N.C. consider�ndose para todos los efectos legales " +
			"que somos depositarios de los mismos.\n" +
			"Sin cargo alguno para Nacional Financiera S.N.C. quedamos " +
			"obligados a efectuar el cobro del (de los) referido(s) t�tulo(s) " +
			"a su vencimiento y a efectuar en su caso los dem�s actos a que se " +
			"refiere el Art�culo 278 de la Ley General de T�tulos y Operaciones " +
			"de Cr�dito.\n" +
			"El (los) citado (s) t�tulo(s) ha (n) sido endosado (s) en " +
			"propiedad con nuestra responsabilidad a Nacional Financiera " +
			"S.N.C. en virtud del descuento del (de los) mismo (s) en los " +
			"t�rminos del contrato de apertura de cr�dito que con la fecha que " +
			"a continuaci�n se describe celebramos con dicha Instituci�n.\n" +
			"\n";

	String lsTitulosTabla = 
			"\n\n Nombre del emisor,Beneficiario,Clase de Documento," +
			"Lugar de Emision,Fecha de Emision,Domicilio de Pago,"+
			"Tasa de Interes,Numero de Documento,Fecha Vencimiento,"+
			"Valor Nominal,Numero de Prestamo,Tipo de Documento\n";
		

	String sql = 
			" SELECT /*+ index (sp in_com_solic_portal_05_nuk) index (py IN_COMCAT_PYME_12_NUK)*/ " +
			"   sp.ic_solic_portal as Folio, " +
			" 	sp.ig_numero_prestamo as NumPrestamo, " +
			" 	nvl(sp.fn_importe_dscto, 0) as ImporteTotal, " +
			" 	nvl(ce.cd_descripcion,' ') as Emisor, " +
			" 	nvl(sp.cg_lugar_firma, ' ') as LugarEmision, " +
			" 	nvl(to_char(sp.df_etc, 'dd/mm/yyyy'), ' ') as FechaEmision, " +
			" 	nvl(sp.cg_domicilio_pago, ' ') as DomicilioPago, " +
			" 	nvl(to_char(sp.df_v_descuento, 'dd/mm/yyyy'), ' ') as FechaVenc, " +
			" 	nvl(sp.ig_numero_docto, 0) as NumDocto, " +
			" 	TO_CHAR(sp.df_operacion, 'dd/mm/yyyy') as FechaOper, " +
			" 	nvl(sp.in_numero_amort, 0) as NoAmort, " +
			" 	t.cd_nombre as TasaIF, sp.ic_if " +
			"	,py.cg_razon_social as pyme" +
			"  , t.ic_moneda " +
			" FROM com_solic_portal sp, comcat_pyme py, comcat_emisor ce, comcat_tasa t " +
			" WHERE sp.ic_emisor = ce.ic_emisor(+) " +
			" 	AND sp.ic_tasaif = t.ic_tasa " +
			" 	AND ic_estatus_solic in (?, ?, ?) " +
			"  AND sp.ig_clave_sirac = py.in_numero_sirac (+)" +
			" 	And sp.df_operacion >= to_date(?, 'dd/mm/yyyy') " +
			" 	And sp.df_operacion < to_date(?,'dd/mm/yyyy') + 1 " +
			" ORDER BY ic_if, sp.ic_solic_portal ";
	
	try {
		con.conexionDB();
		ps = con.queryPrecompilado(sql);
		ps.setInt(1,3); //3=Operada
		ps.setInt(2,5); //5=Operada Pagada
		ps.setInt(3,6); //6=Operada Pendiente de pago
		ps.setString(4, fechaInicial);
		ps.setString(5, fechaInicial);
		rs = ps.executeQuery();
		String claveIFPrevia = "";
		while (rs.next()) {
			String folio = rs.getString("FOLIO");
			String numPrestamo = rs.getString("NUMPRESTAMO");
			double importeTotal = rs.getDouble("IMPORTETOTAL");
			String emisor = rs.getString("EMISOR");
			String lugarEmision = rs.getString("LUGAREMISION");
			String fechaEmision = rs.getString("FECHAEMISION");
			String domicilioPago = rs.getString("DOMICILIOPAGO");
			String fechaVenc = rs.getString("FECHAVENC");
			String numDocto = rs.getString("NUMDOCTO");
			String fechaOper = rs.getString("FECHAOPER");
			//java.util.Date fechaOperacion = rs.getDate("FECHAOPER");
			String noAmort = rs.getString("NOAMORT");
			String tasaIF = rs.getString("TASAIF");
			String claveIFActual = rs.getString("IC_IF");
			String nombre = (rs.getString("PYME")== null)?"":rs.getString("PYME");
			int moneda = (rs.getString("IC_MONEDA")== null)?1:rs.getInt("IC_MONEDA");
			
			if (moneda == 1) {
				numTotalRegistrosMN++;
				granTotalMN += importeTotal;
			} else {
				numTotalRegistrosDL++;
				granTotalDL += importeTotal;
			}
			
			/*
				Solo si cambia la clave del IF se obtiene la fecha de convenio,
				y la firma.
			*/
			if (!claveIFActual.equals(claveIFPrevia)) {
				
				String sqlIF = 
						" SELECT df_convenio " +
						" FROM comrel_producto_if " +
						" WHERE ic_if = ? " +
						" AND ic_producto_nafin = ? ";
				
				PreparedStatement psIF = con.queryPrecompilado(sqlIF);
				psIF.setInt(1, Integer.parseInt(claveIFActual));
				psIF.setInt(2, 1); //1= Descuento Electronico
				
				rsIF = psIF.executeQuery();
				
				fechaContrato = "";
				dia = mes = anio = "";
				if (rsIF.next()) {
					fechaConvenio = rsIF.getDate("DF_CONVENIO");
					if (fechaConvenio != null) {
						cal.setTime(fechaConvenio);
						dia = String.valueOf(cal.get(Calendar.DATE));
						mes = nombreMes[cal.get(Calendar.MONTH)];
						anio = String.valueOf(cal.get(Calendar.YEAR));
						fechaContrato = dia + " de " + mes +
								" de " + anio;
					}
				}
				rsIF.close();
				psIF.close();
				
				String sqlPersonal = 
						" Select cg_nombre||' '||cg_ap_paterno||' '||cg_ap_materno, " +
	  					" 	cg_puesto " +
						" From com_personal_facultado " +
						" Where ic_if = ? " +
						" 	And ic_producto_nafin = ? ";  
				PreparedStatement psPersonal = con.queryPrecompilado(sqlPersonal);
				psPersonal.setInt(1, Integer.parseInt(claveIFActual));
				psPersonal.setInt(2, 1); //1= Descuento Electronico
				
				rsPersonal = psPersonal.executeQuery();
				datos =  " ";
				puesto = " ";
	
				if (rsPersonal.next()){
					datos = (rsPersonal.getString(1)==null)?"":rsPersonal.getString(1);
					puesto =(rsPersonal.getString(2)==null)?"":rsPersonal.getString(2);
				}
				rsPersonal.close();
				psPersonal.close();
				
				textFirma = puesto + " " + datos;
				claveIFPrevia = claveIFActual;
			}
			
			contenidoArch.append(
					lsTextTitulo + "Importe Total " + importeTotal + "\n" +
					"Fecha de Contrato " + fechaContrato);
			
			contenidoArch.append(lsTitulosTabla);
			
			contenidoArch.append(
					emisor +
					"," + nombre +
					"," + "PAGARE" +
					"," + lugarEmision.replace(',',' ') +
					"," + fechaEmision +
					"," + domicilioPago.replace(',',' ') +
					"," + tasaIF +
					"," + numDocto +
					"," + fechaVenc +
					"," + importeTotal +
					"," + numPrestamo +
					"," + "Certificado"
			);
			String fechaOperTexto = "";
			if (fechaOper != null && !fechaOper.equals("")) {
				StringTokenizer st=new StringTokenizer(fechaOper,"/");
				dia = st.nextToken();
				mes = nombreMes[Integer.parseInt(st.nextToken()) - 1];
				anio = st.nextToken();

				fechaOperTexto = dia + " de " + mes +
						" de " + anio;
			}

			contenidoArch.append(
					"\n,,,,,,,,Total," + importeTotal +
					"\n Amortizacion,Monto,FechaVencimiento " +
					"\n "+noAmort+","+importeTotal+","+fechaVenc +
					"\n" + domicilioPago + " a " + fechaOperTexto);
	
			contenidoArch.append(" " + textFirma + "\n \n \n");
		}	//fin del while
		rs.close();
		ps.close();
		
		
		String nombreArchivo = "";
		if ((numTotalRegistrosMN + numTotalRegistrosDL) > 0) {

			contenidoArch.append(
					" \n \n N�mero Total de Registros Moneda Nacional:," + numTotalRegistrosMN + "\n" +
					"Importe Total Moneda Nacional :," +
					Comunes.formatoDecimal(granTotalMN, 2, false)
			);
			contenidoArch.append(
					" \n \n N�mero Total de Registros Dolares:," + numTotalRegistrosDL + "\n" +
					"Importe Total Dolares :," +
					Comunes.formatoDecimal(granTotalDL, 2, false)
			);

			CreaArchivo archivo = new CreaArchivo();
			if (!archivo.make(contenidoArch.toString(), strDirectorioTemp, ".csv")) {
				System.out.println("Error en la generacion del archivo");
			} else {
				nombreArchivo = archivo.nombre;
			}
		}

		return nombreArchivo;
		
	} catch(Exception e) {
		e.printStackTrace();
		throw e;
	} finally {
		if (con.hayConexionAbierta()) con.cierraConexionDB();
	}
}	//fin del metodo





	public List getConditions() {  return conditions;  }  

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getTxtfolio() {
		return txtfolio;
	}

	public void setTxtfolio(String txtfolio) {
		this.txtfolio = txtfolio;
	}

	public String getTxtfechaIni() {
		return txtfechaIni;
	}

	public void setTxtfechaIni(String txtfechaIni) {
		this.txtfechaIni = txtfechaIni;
	}

	public String getTxtfechaFin() {
		return txtfechaFin;
	}

	public void setTxtfechaFin(String txtfechaFin) {
		this.txtfechaFin = txtfechaFin;
	}

	public String getLsTipoOperacion() {
		return lsTipoOperacion;
	}

	public void setLsTipoOperacion(String lsTipoOperacion) {
		this.lsTipoOperacion = lsTipoOperacion;
	}

	public String getAccionBoton() {
		return accionBoton;
	}

	public void setAccionBoton(String accionBoton) {
		this.accionBoton = accionBoton;
	}

	public String getIc_moneda() {
		return ic_moneda;
	}

	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}

	public String getLsFechaOperacion() {
		return lsFechaOperacion;
	}

	public void setLsFechaOperacion(String lsFechaOperacion) {
		this.lsFechaOperacion = lsFechaOperacion;
	}

	public String getLsNomMoneda() {
		return lsNomMoneda;
	}

	public void setLsNomMoneda(String lsNomMoneda) {
		this.lsNomMoneda = lsNomMoneda;
	}

	public String getLsImporteTotalO() {
		return lsImporteTotalO;
	}

	public void setLsImporteTotalO(String lsImporteTotalO) {
		this.lsImporteTotalO = lsImporteTotalO;
	}   

	public String getLsNombreIF() {
		return lsNombreIF;
	}

	public void setLsNombreIF(String lsNombreIF) {
		this.lsNombreIF = lsNombreIF;
	}

	


}