package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsuLineaFondeoSIAC implements IQueryGeneratorReg,IQueryGeneratorRegExtJS {
	public ConsuLineaFondeoSIAC()  { 	}
//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsuLineaFondeoSIAC.class);
			
	StringBuffer 		qrySentencia;
	private List 		conditions;
	private String 	paginaOffset = "0";
	private String 	paginaNo;
	private String noIntermediario;		
	private String parametrizacion;
	private String nombreIntermediario;
		 
	/**
	 *Metodos nueva versi�n
	 * GPEREZ 
	 */
	 public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo)
	{
		StringBuffer linea = new StringBuffer("");
		String nombreArchivo = "";
		String espacio = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
      
		try {
		 if(tipo.equals("CSV")) {
		 linea.append(nombreIntermediario.replace(',',' ')+"\n");
		 if(parametrizacion.equals("D")){
					linea.append("N�mero L�nea Fondeo, Monto Asignando, Monto Utilizado, Monto Comprometido ,Disponible , Adicionado por, Fecha Adici�n , Modificado por, Fecha Modificaci�n ");
					linea.append("\r\n");
				}else if(parametrizacion.equals("G")){ 
					linea.append("N�mero L�nea Fondeo, Monto Asignando, Monto Utilizado ");
					linea.append("\r\n");
					
				}
		  nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
		  writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
		  buffer = new BufferedWriter(writer);
		
		  buffer.write(linea.toString());
			 			
		  while (rs.next()) {
					linea= new StringBuffer("");
						String numero_linea_fondeo = rs.getString("numero_linea_fondeo")==null?"":rs.getString("numero_linea_fondeo");
						String monto_asignado = rs.getString("monto_asignado")==null?"":rs.getString("monto_asignado");
						String monto_utilizado = rs.getString("monto_utilizado")==null?"":rs.getString("monto_utilizado");
						String monto_comprometido = rs.getString("monto_comprometido")==null?"":rs.getString("monto_comprometido");
						String disponible = rs.getString("disponible")==null?"":rs.getString("disponible");
						String adicionado_por = rs.getString("adicionado_por")==null?"":rs.getString("adicionado_por");
						String fecha_adicion = rs.getString("fecha_adicion")==null?"":rs.getString("fecha_adicion");
						String modificado_por = rs.getString("modificado_por")==null?"":rs.getString("modificado_por");
						String fecha_modificacion = rs.getString("fecha_modificacion")==null?"":rs.getString("fecha_modificacion");
					if(parametrizacion.equals("D")){ //detallada
					  linea.append( numero_linea_fondeo.replace(',',' ') +",");	
						linea.append( Comunes.formatoDecimal(monto_asignado, 2, false)+",");	
						linea.append( Comunes.formatoDecimal(monto_utilizado, 2, false) +",");	
						linea.append( Comunes.formatoDecimal(monto_comprometido, 2, false) +",");	
						linea.append( Comunes.formatoDecimal(disponible, 2, false) +",");
						linea.append( adicionado_por.replace(',',' ') +",");	
						linea.append( fecha_adicion.replace(',',' ') +",");	
						linea.append( modificado_por.replace(',',' ') +",");	
						linea.append( fecha_modificacion.replace(',',' ') +",");	
						linea.append("");					
						linea.append("\r\n");	
						
					}else if(parametrizacion.equals("G")){ //Global
						linea.append(numero_linea_fondeo.replace(',',' ') +",");	
						linea.append( Comunes.formatoDecimal(monto_asignado, 2, false)+",");	
						linea.append( Comunes.formatoDecimal(monto_utilizado, 2, false)+",");	
						linea.append("");					
						linea.append("\r\n");	
					}
					
					buffer.write(linea.toString());
		  }
		  buffer.close();
		 }else if(tipo.equals("PDF")){
			HttpSession session = request.getSession();
					nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
					ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					
					String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
					String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
					String diaActual    = fechaActual.substring(0,2);
					String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
					String anioActual   = fechaActual.substring(6,10);
					String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
							
					pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					session.getAttribute("iNoNafinElectronico").toString(),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
							
					pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
					if(parametrizacion.equals("D")){
						pdfDoc.setTable(9, 80);
						pdfDoc.setCell("N�mero L�nea Fondeo","celda01",ComunesPDF.LEFT);
						pdfDoc.setCell("Monto Asignando","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto Utilizado","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto Comprometido","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Disponible","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Adicionado por","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha Adici�n","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Modificado por","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha Modificaci�n","celda01",ComunesPDF.CENTER);
					}
					else if(parametrizacion.equals("G")){
						pdfDoc.setTable(3, 80);
						pdfDoc.setCell("N�mero L�nea Fondeo","celda01",ComunesPDF.LEFT);
						pdfDoc.setCell("Monto Asignando","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto Utilizado","celda01",ComunesPDF.CENTER);
					
					}			
					while (rs.next()) {
					
						
						String numero_linea_fondeo = rs.getString("numero_linea_fondeo")==null?"":rs.getString("numero_linea_fondeo");
						String monto_asignado = rs.getString("monto_asignado")==null?"":rs.getString("monto_asignado");
						String monto_utilizado = rs.getString("monto_utilizado")==null?"":rs.getString("monto_utilizado");
						String monto_comprometido = rs.getString("monto_comprometido")==null?"":rs.getString("monto_comprometido");
						String disponible = rs.getString("disponible")==null?"":rs.getString("disponible");
						String adicionado_por = rs.getString("adicionado_por")==null?"":rs.getString("adicionado_por");
						String fecha_adicion = rs.getString("fecha_adicion")==null?"":rs.getString("fecha_adicion");
						String modificado_por = rs.getString("modificado_por")==null?"":rs.getString("modificado_por");
						String fecha_modificacion = rs.getString("fecha_modificacion")==null?"":rs.getString("fecha_modificacion");
						
					if(parametrizacion.equals("D")){ //detallada
					   
						pdfDoc.setCell(numero_linea_fondeo,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(monto_asignado, 2, true),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(monto_utilizado, 2, true),"formas",ComunesPDF.CENTER);		
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(monto_comprometido, 2, true),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(disponible, 2, true),"formas",ComunesPDF.CENTER);		
						pdfDoc.setCell(adicionado_por,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fecha_adicion,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(modificado_por,"formas",ComunesPDF.CENTER);	
						pdfDoc.setCell(fecha_modificacion,"formas",ComunesPDF.CENTER);	
					}else if(parametrizacion.equals("G")){ //Global
						pdfDoc.setCell(numero_linea_fondeo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(monto_asignado, 2, true),"formas",ComunesPDF.CENTER);	
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(monto_utilizado, 2, true),"formas",ComunesPDF.CENTER);	
						
					}
						
						 
									
						
						
					}
					pdfDoc.addTable();
					pdfDoc.endDocument();
		 
		 }
		 
		 }
		catch (Throwable e) {
			throw new AppException("Error al generar el archivo",e);
		}
		finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
		
		return nombreArchivo;
	}
	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */

	public String getAggregateCalculationQuery() {
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();


		log.debug("noIntermediario  "+noIntermediario);

		qrySentencia.append(" SELECT  count(*)  ");
		qrySentencia.append(" FROM lc_lineas_fondeo_inter lc, ");
		qrySentencia.append(" 		 mg_financieras fin, ");
		qrySentencia.append(" 		 comcat_if i, ");
		qrySentencia.append(" 			comcat_financiera f ");
		qrySentencia.append(" WHERE lc.fecha_de_vcmto > = TO_DATE (TO_CHAR (SYSDATE, 'ddmmyyyy'), 'ddmmyyyy') ");
		qrySentencia.append("  and  lc.codigo_financiera = fin.codigo_financiera ");
		qrySentencia.append(" AND lc.tipo_financiera = fin.tipo_financiera ");
	  qrySentencia.append(" AND lc.codigo_estado = 1 ");
		qrySentencia.append("  AND fin.codigo_financiera = i.ic_financiera ");
		qrySentencia.append(" AND fin.tipo_financiera = f.ic_tipo_financiera ");
		qrySentencia.append(" AND i.ic_financiera = f.ic_financiera ");
		qrySentencia.append("  AND i.ic_if = "+  noIntermediario+"");	 
	
		log.debug("getAggregateCalculationQuery "+conditions.toString());
		log.debug("getAggregateCalculationQuery "+qrySentencia.toString());
		return qrySentencia.toString();
				
	}
	
	
	 /**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery(){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
	 log.debug("noIntermediario  "+noIntermediario);	  	
			
	  qrySentencia.append(" SELECT   numero_linea_fondeo as numero_linea_fondeo,  monto_asignado as monto_asignado, ");
		qrySentencia.append(" 				 monto_utilizado as monto_utilizado ,  ");
		qrySentencia.append(" 				 monto_comprometido as  monto_comprometido , ");
		qrySentencia.append(" 				 (monto_asignado - (NVL (monto_utilizado, 0) + NVL (monto_comprometido, 0))) AS disponible, ");
		qrySentencia.append(" 				 lc.adicionado_por AS adicionado_por, ");
		qrySentencia.append("          TO_CHAR (lc.fecha_adicion, 'dd/mm/yyyy hh24:mm') AS fecha_adicion, ");
		qrySentencia.append(" 				 lc.modificado_por AS modificado_por , ");
		qrySentencia.append("  				 TO_CHAR (lc.fecha_modificacion, 'dd/mm/yyyy hh24:mm' ) AS fecha_modificacion ");
		qrySentencia.append(" FROM lc_lineas_fondeo_inter lc, ");
		qrySentencia.append(" 		 mg_financieras fin, ");
		qrySentencia.append(" 		 comcat_if i, ");
		qrySentencia.append(" 			comcat_financiera f ");
		qrySentencia.append(" WHERE lc.fecha_de_vcmto > = TO_DATE (TO_CHAR (SYSDATE, 'ddmmyyyy'), 'ddmmyyyy') ");
		qrySentencia.append("  and  lc.codigo_financiera = fin.codigo_financiera ");
		qrySentencia.append(" AND lc.tipo_financiera = fin.tipo_financiera ");
	  qrySentencia.append(" AND lc.codigo_estado = 1 ");
		qrySentencia.append("  AND fin.codigo_financiera = i.ic_financiera ");
		qrySentencia.append(" AND fin.tipo_financiera = f.ic_tipo_financiera ");
		qrySentencia.append(" AND i.ic_financiera = f.ic_financiera ");
		qrySentencia.append("  AND i.ic_if = "+  noIntermediario+"");
	
	
		log.debug("getDocumentQuery "+conditions.toString());
		log.debug("getDocumentQuery "+qrySentencia.toString());
		return qrySentencia.toString();
	}

	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();		
		
		log.debug("noIntermediario  "+noIntermediario);		
		
		qrySentencia.append(" SELECT   numero_linea_fondeo as numero_linea_fondeo,  monto_asignado as monto_asignado, ");
	  qrySentencia.append(" 				 monto_utilizado as monto_utilizado ,  ");
		qrySentencia.append(" 				 monto_comprometido as  monto_comprometido , ");
		qrySentencia.append(" 				 (monto_asignado - (NVL (monto_utilizado, 0) + NVL (monto_comprometido, 0))) AS disponible, ");
		qrySentencia.append(" 				 lc.adicionado_por AS adicionado_por, ");
		qrySentencia.append("          TO_CHAR (lc.fecha_adicion, 'dd/mm/yyyy hh24:mm') AS fecha_adicion, ");
		qrySentencia.append(" 				 lc.modificado_por AS modificado_por , ");
		qrySentencia.append("  				 TO_CHAR (lc.fecha_modificacion, 'dd/mm/yyyy hh24:mm' ) AS fecha_modificacion ");
		qrySentencia.append(" FROM lc_lineas_fondeo_inter lc, ");
		qrySentencia.append(" 		 mg_financieras fin, ");
		qrySentencia.append(" 		 comcat_if i, ");
		qrySentencia.append(" 			comcat_financiera f ");
		qrySentencia.append(" WHERE lc.fecha_de_vcmto > = TO_DATE (TO_CHAR (SYSDATE, 'ddmmyyyy'), 'ddmmyyyy') ");
		qrySentencia.append("  and  lc.codigo_financiera = fin.codigo_financiera ");
		qrySentencia.append(" AND lc.tipo_financiera = fin.tipo_financiera ");
	  qrySentencia.append(" AND lc.codigo_estado = 1 ");
		qrySentencia.append("  AND fin.codigo_financiera = i.ic_financiera ");
		qrySentencia.append(" AND fin.tipo_financiera = f.ic_tipo_financiera ");
		qrySentencia.append(" AND i.ic_financiera = f.ic_financiera ");
		qrySentencia.append("  AND i.ic_if = "+  noIntermediario+"");
			
			
		log.debug("getDocumentSummaryQueryForIds "+conditions.toString());
		log.debug("getDocumentSummaryQueryForIds "+qrySentencia.toString());
		return qrySentencia.toString();
	}
	
		

	public String getDocumentQueryFile(){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();

		
	  qrySentencia.append(" SELECT   numero_linea_fondeo as numero_linea_fondeo,  monto_asignado as monto_asignado, ");
		qrySentencia.append(" 				 monto_utilizado as monto_utilizado ,  ");
		qrySentencia.append(" 				 monto_comprometido as  monto_comprometido , ");
		qrySentencia.append(" 				 (monto_asignado - (NVL (monto_utilizado, 0) + NVL (monto_comprometido, 0))) AS disponible, ");
		qrySentencia.append(" 				 lc.adicionado_por AS adicionado_por, ");
		qrySentencia.append("          TO_CHAR (lc.fecha_adicion, 'dd/mm/yyyy hh24:mm') AS fecha_adicion, ");
		qrySentencia.append(" 				 lc.modificado_por AS modificado_por , ");
		qrySentencia.append("  				 TO_CHAR (lc.fecha_modificacion, 'dd/mm/yyyy hh24:mm' ) AS fecha_modificacion ");
		qrySentencia.append(" FROM lc_lineas_fondeo_inter lc, ");
		qrySentencia.append(" 		 mg_financieras fin, ");
		qrySentencia.append(" 		 comcat_if i, ");
		qrySentencia.append(" 			comcat_financiera f ");
		qrySentencia.append(" WHERE lc.fecha_de_vcmto > = TO_DATE (TO_CHAR (SYSDATE, 'ddmmyyyy'), 'ddmmyyyy') ");
		qrySentencia.append("  and  lc.codigo_financiera = fin.codigo_financiera ");
		qrySentencia.append(" AND lc.tipo_financiera = fin.tipo_financiera ");
	  qrySentencia.append(" AND lc.codigo_estado = 1 ");
		qrySentencia.append("  AND fin.codigo_financiera = i.ic_financiera ");
		qrySentencia.append(" AND fin.tipo_financiera = f.ic_tipo_financiera ");
		qrySentencia.append(" AND i.ic_financiera = f.ic_financiera ");
		qrySentencia.append("  AND i.ic_if = "+  noIntermediario+"");
		 
	
	
	
		log.debug("getDocumentQueryFile "+conditions.toString());
		log.debug("getDocumentQueryFile "+qrySentencia.toString());
		return qrySentencia.toString();
	}//getDocumentQueryFile
	


/*****************************************************
	 GETTERS
*******************************************************/
 
	/**
	  Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
		
	
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getNoIntermediario() {
		return noIntermediario;
	}

	public void setNoIntermediario(String noIntermediario) {
		this.noIntermediario = noIntermediario;
	}


	public void setParametrizacion(String parametrizacion) {
		this.parametrizacion = parametrizacion;
	}


	public String getParametrizacion() {
		return parametrizacion;
	}


	public void setNombreIntermediario(String nombreIntermediario) {
		this.nombreIntermediario = nombreIntermediario;
	}





	
}