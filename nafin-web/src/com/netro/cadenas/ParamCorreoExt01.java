package com.netro.cadenas;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ParamCorreoExt01 {
  /**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ParamCorreoExt01.class);
  
  private String claveTransaccion;
  public ParamCorreoExt01() { }
  
  /**
   * 
   * @return 
   */
  public List getListaEstatus(){
  log.info("getListaEstatus (E) - > ");
  List catalogoEstatus = new ArrayList();
  
  if (claveTransaccion.equals("2")) {
			ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
			elementoCatalogo.setClave("1");
			elementoCatalogo.setDescripcion("RECHAZADA");
			catalogoEstatus.add(elementoCatalogo);
			
			elementoCatalogo = new ElementoCatalogo();
			elementoCatalogo.setClave("2");
			elementoCatalogo.setDescripcion("AUTORIZADA IF");
			catalogoEstatus.add(elementoCatalogo);
			
			elementoCatalogo = new ElementoCatalogo();
			elementoCatalogo.setClave("3");
			elementoCatalogo.setDescripcion("AMBOS");
			catalogoEstatus.add(elementoCatalogo);
		} else {
			ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
			elementoCatalogo.setClave("1");
			elementoCatalogo.setDescripcion("RECHAZADA");
			catalogoEstatus.add(elementoCatalogo);
		}
  log.info("getListaEstatus (S) < - ");
  return catalogoEstatus;
  }


  public void setClaveTransaccion(String claveTransaccion)
  {
    this.claveTransaccion = claveTransaccion;
  }


  public String getClaveTransaccion()
  {
    return claveTransaccion;
  }
}