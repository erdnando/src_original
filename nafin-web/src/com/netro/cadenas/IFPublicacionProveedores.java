package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class IFPublicacionProveedores implements IQueryGeneratorReg, IQueryGeneratorRegExtJS {

	public IFPublicacionProveedores()
	{
	}	
		//Variable para enviar mensajes al log.
	private static final Log log = ServiceLocator.getInstance().getLog(IFPublicacionProveedores.class);
	
	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer 		qrySentencia;
	private List 		conditions;
	private String 	paginaOffset;
	private String 	paginaNo;	
	
  public 	String 	ic_epo;  
  public 	String 	paginar;  
  public 	String 	usuario;
	private String ic_moneda;
	private String nomEpo;
	private String montoPublicacion;

	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */

	public String getAggregateCalculationQuery() {
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
	 
   		
    qrySentencia.append("select   COUNT(*)  /*+  use_nl(v rn p m c ) */ ");
		qrySentencia.append(" from vm_prov_publicacion v,");
		qrySentencia.append(" comrel_nafin rn,");
		qrySentencia.append(" comcat_pyme p, ");
		qrySentencia.append(" comcat_moneda m,  ");
		qrySentencia.append(" com_contacto c ");
		qrySentencia.append(" where v.ic_pyme = rn.ic_epo_pyme_if ");
		qrySentencia.append(" and p.ic_pyme = rn.ic_epo_pyme_if  ");
		qrySentencia.append(" and v.ic_moneda = m.ic_moneda  ");
		qrySentencia.append(" and c.ic_pyme = v.ic_pyme ");
		qrySentencia.append(" and c.cs_primer_contacto = 'S' ");
		qrySentencia.append(" and rn.cg_tipo = 'P'   ");
    qrySentencia.append(" AND v.ic_if = ? ");
		
		conditions.add(usuario);
		
		if(!ic_epo.equals("0") ){
			qrySentencia.append("  AND v.ic_epo = ? ");
			conditions.add(ic_epo);
	  }  
		if(!ic_moneda.equals("0") ){
			qrySentencia.append("  AND v.ic_moneda = ? ");
			conditions.add(ic_moneda);
		}  
		
		log.debug("getAggregateCalculationQuery "+conditions.toString());
		log.debug("getAggregateCalculationQuery "+qrySentencia.toString());

		return qrySentencia.toString();

	}
		
	 /**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery(){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
    
	  qrySentencia.append("select   /*+  use_nl(v rn p m c ) */ ");
		qrySentencia.append(" v.ic_pyme pyme,  ");
		qrySentencia.append("	v.CG_RANKING as ranking , ");
		qrySentencia.append(" rn.ic_nafin_electronico as nafinelectronico, ");
		qrySentencia.append(" p.cg_rfc as rfc, ");
		qrySentencia.append(" c.cg_tel as telefono,  ");
		qrySentencia.append(" p.cg_razon_social as razonsocialproveedor,  ");
		qrySentencia.append(" m.cd_nombre moneda, ");
		qrySentencia.append(" v.fn_monto_publicacion as montopublic ,  ");
		qrySentencia.append(" v.CG_RANGO_PUB as rangopublic , "); 
		qrySentencia.append(" v.CG_RANGO_PUBFIN as rangopublicFin   "); 		
		qrySentencia.append(" from vm_prov_publicacion v,");
		qrySentencia.append(" comrel_nafin rn,");
		qrySentencia.append(" comcat_pyme p, ");
		qrySentencia.append(" comcat_moneda m,  ");
		qrySentencia.append(" com_contacto c ");
		qrySentencia.append(" where v.ic_pyme = rn.ic_epo_pyme_if ");
		qrySentencia.append(" and p.ic_pyme = rn.ic_epo_pyme_if  ");
		qrySentencia.append(" and v.ic_moneda = m.ic_moneda  ");
		qrySentencia.append(" and c.ic_pyme = v.ic_pyme ");
		qrySentencia.append(" and c.cs_primer_contacto = 'S' ");
		qrySentencia.append(" and rn.cg_tipo = 'P'   ");
    qrySentencia.append(" AND v.ic_if  = ? ");
			  
		conditions.add(usuario);
		if(!ic_epo.equals("0") ){
			qrySentencia.append(" AND v.ic_epo = ? ");
			conditions.add(ic_epo);
		}  
		if(!ic_moneda.equals("0") ){
			qrySentencia.append(" AND v.ic_moneda = ? ");
			conditions.add(ic_moneda);
		}  
			
		qrySentencia.append(" ORDER BY montopublic DESC ");

		
		log.debug("getDocumentQuery "+conditions.toString());
		log.debug("getDocumentQuery "+qrySentencia.toString());

		return qrySentencia.toString();
	}

	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
  
    qrySentencia.append("select   /*+  use_nl(v rn p m c ) */ ");
		qrySentencia.append(" v.ic_pyme pyme,  ");
		qrySentencia.append("	v.CG_RANKING as ranking , ");
		qrySentencia.append(" rn.ic_nafin_electronico as nafinelectronico, ");
		qrySentencia.append(" p.cg_rfc as rfc, ");
		qrySentencia.append(" c.cg_tel as telefono,  ");
		qrySentencia.append(" p.cg_razon_social as razonsocialproveedor,  ");
		qrySentencia.append(" m.cd_nombre moneda, ");
		qrySentencia.append(" v.fn_monto_publicacion as montopublic ,  ");
		qrySentencia.append(" v.CG_RANGO_PUB as rangopublic , "); 
		qrySentencia.append(" v.CG_RANGO_PUBFIN as rangopublicFin,   ");
		qrySentencia.append(" to_char(p.in_numero_sirac) as in_numero_sirac  "); 		
		qrySentencia.append(" from vm_prov_publicacion v,");
		qrySentencia.append(" comrel_nafin rn,");
		qrySentencia.append(" comcat_pyme p, ");
		qrySentencia.append(" comcat_moneda m,  ");
		qrySentencia.append(" com_contacto c ");
		qrySentencia.append(" where v.ic_pyme = rn.ic_epo_pyme_if ");
		qrySentencia.append(" and p.ic_pyme = rn.ic_epo_pyme_if  ");
		qrySentencia.append(" and v.ic_moneda = m.ic_moneda  ");
		qrySentencia.append(" and c.ic_pyme = v.ic_pyme ");
		qrySentencia.append(" and c.cs_primer_contacto = 'S' ");
		qrySentencia.append(" and rn.cg_tipo = 'P'   ");
    qrySentencia.append(" AND v.ic_if  = ? ");		
		 
		conditions.add(usuario);
		
		if(!ic_epo.equals("0") ){
			qrySentencia.append("  AND v.ic_epo = ? ");
			conditions.add(ic_epo);
		}  
		if(!ic_moneda.equals("0") ){
			qrySentencia.append("  AND v.ic_moneda = ? ");
			conditions.add(ic_moneda);
		}  
		
		qrySentencia.append("   AND ( ");
		
		for(int i = 0; i < pageIds.size(); i++){
			List lItem = (ArrayList)pageIds.get(i);
			if(i > 0){qrySentencia.append("  OR  ");}
				qrySentencia.append(" v.ic_pyme  = ?");
				conditions.add(new Long(lItem.get(0).toString()));      
			}
		qrySentencia.append(" ) ");
		
			
		qrySentencia.append(" ORDER BY montopublic DESC ");
                    
                    
		log.debug("getDocumentSummaryQueryForIds "+conditions.toString());
		log.debug(" getDocumentSummaryQueryForIds "+qrySentencia.toString());
	
		
		return qrySentencia.toString();
	}
	
/**
	 *genera el archivo 
	 * @return 
	 */
	public String getDocumentQueryFile(){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
    qrySentencia.append("select   /*+  use_nl(v rn p m c ) */ ");
		qrySentencia.append(" v.ic_pyme pyme,  ");
		qrySentencia.append("	v.CG_RANKING as ranking , ");
		qrySentencia.append(" rn.ic_nafin_electronico as nafinelectronico, ");
		qrySentencia.append(" p.cg_rfc as rfc, ");
		qrySentencia.append(" c.cg_tel as telefono,  ");
		qrySentencia.append(" p.cg_razon_social as razonsocialproveedor,  ");
		qrySentencia.append(" m.cd_nombre moneda, ");	
		qrySentencia.append(" v.fn_monto_publicacion as montopublic ,  ");
		qrySentencia.append(" v.CG_RANGO_PUB as rangopublic , "); 
		qrySentencia.append(" v.CG_RANGO_PUBFIN as rangopublicFin,   "); 
		qrySentencia.append(" p.in_numero_sirac ");
		qrySentencia.append(" from vm_prov_publicacion v,");
		qrySentencia.append(" comrel_nafin rn,");
		qrySentencia.append(" comcat_pyme p, ");
		qrySentencia.append(" comcat_moneda m,  ");
		qrySentencia.append(" com_contacto c ");
		qrySentencia.append(" where v.ic_pyme = rn.ic_epo_pyme_if ");
		qrySentencia.append(" and p.ic_pyme = rn.ic_epo_pyme_if  ");
		qrySentencia.append(" and v.ic_moneda = m.ic_moneda  ");
		qrySentencia.append(" and c.ic_pyme = v.ic_pyme ");
		qrySentencia.append(" and c.cs_primer_contacto = 'S' ");
		qrySentencia.append(" and rn.cg_tipo = 'P'   ");
    qrySentencia.append(" AND v.ic_if  = ? ");
			  
		conditions.add(usuario);
		
		if(!ic_epo.equals("0") ){
			qrySentencia.append("  AND v.ic_epo = ? ");
			conditions.add(ic_epo);
		}  
		if(!ic_moneda.equals("0") ){
			qrySentencia.append("  AND v.ic_moneda = ? ");
			conditions.add(ic_moneda);
		}  
			
		qrySentencia.append(" ORDER BY montopublic DESC ");
		
		
		log.debug("getDocumentQueryFile "+conditions.toString());
		log.debug("getDocumentQueryFile "+qrySentencia.toString());
		
		return qrySentencia.toString();
	}//getDocumentQueryFile
	
	/**
	 * Regresar el query para el calculo del monto de la publicacion vigente 
	 * @return <tt>String</tt> query para el calculo del monto de la publicacion vigente
	 */
	public String getMontoPublicacionVigente(){
		
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		qrySentencia.append(
			" select /*+  use_nl(v rn p m c ) */                    "  +
			"		sum(v.fn_monto_publicacion) publicacion_vigente   "  +		 
			" from                        "  + 
			"    vm_prov_publicacion 	v,	"  +
			"    comrel_nafin 			rn,"  +
			"    comcat_pyme 				p, "  +
			"    comcat_moneda 			m, "  +
			"    com_contacto 			c 	"  +
			" where                       "  +
			"    v.ic_pyme                = rn.ic_epo_pyme_if "  +
			"    and p.ic_pyme            = rn.ic_epo_pyme_if "  +
			"    and v.ic_moneda          = m.ic_moneda       "  +
			"    and c.ic_pyme            = v.ic_pyme         "  +
			"    and c.cs_primer_contacto = 'S'               "  +
			"    and rn.cg_tipo           = 'P'               "
		);
 
		qrySentencia.append(" and v.ic_if = ? ");
		conditions.add(usuario);
 
		qrySentencia.append(" and v.ic_epo = ? ");
		conditions.add(ic_epo);
 
		qrySentencia.append(" and v.ic_moneda = ? ");
		conditions.add(ic_moneda);
  
		log.debug("getMontoPublicacionVigente "+conditions.toString());
		log.debug("getMontoPublicacionVigente "+qrySentencia.toString());
		
		return qrySentencia.toString();
		
	}//getDocumentQueryFile
		public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		String nombreArchivo = "";
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		
		
		if("PDF".equals(tipo)){
			try{
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
				
				String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual		= fechaActual.substring(0,2);
				String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual		= fechaActual.substring(6,10);
				String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText("EPO,"+getNomEpo()+"\nPUBLICACION VIGENTE,"+getMontoPublicacion(),"formas",ComunesPDF.CENTER);
				//Ranking,No Proveedor,No Nafin Electr�nico,N�mero de SIRAC,RFC,Tel�fono  del Contacto,Nombre o Raz�n Social,Moneda"+",Rango de Electr�nico\
				pdfDoc.setTable(9, 100);
				pdfDoc.setCell("Ranking","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No Proveedor","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No Nafin Electr�nico","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de SIRAC","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("RFC","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tel�fono  del Contacto","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre o Raz�n Social","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Rango de Publicaci�n","celda01",ComunesPDF.CENTER);
				
				
				while (reg.next()){
					String ran= (reg.getString("RANKING") == null) ? "" : reg.getString("RANKING");
					String pyme=(reg.getString("PYME") == null) ? "" : reg.getString("PYME");
					String nafin=(reg.getString("NAFINELECTRONICO") == null) ? "" : reg.getString("NAFINELECTRONICO");
					String sirac=(reg.getString("IN_NUMERO_SIRAC") == null) ? "" : reg.getString("IN_NUMERO_SIRAC");
					String rfc=(reg.getString("RFC") == null) ? "" : reg.getString("RFC");
					String telefono=(reg.getString("TELEFONO") == null) ? "" : reg.getString("TELEFONO");
					String razon=(reg.getString("RAZONSOCIALPROVEEDOR") == null) ? "" : reg.getString("RAZONSOCIALPROVEEDOR");
					String moneda=(reg.getString("MONEDA") == null) ? "" : reg.getString("MONEDA");
					String rangoIni=(reg.getString("RANGOPUBLIC") == null) ? "" : reg.getString("RANGOPUBLIC");
					String rangoFin=(reg.getString("RANGOPUBLICFIN") == null) ? "" : reg.getString("RANGOPUBLICFIN");				

					
					pdfDoc.setCell(ran,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(pyme,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nafin,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(sirac,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(rfc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(telefono,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(razon,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("De $"+Comunes.formatoDecimal(rangoIni,2)+" hasta $"+Comunes.formatoDecimal(rangoFin,2),"formas",ComunesPDF.CENTER);
					
					
				}
				
					pdfDoc.addTable();
					pdfDoc.endDocument();
							}catch(Throwable e){
				throw new AppException("Error al generar el archivo",e);
			}finally{
				try{
				}catch(Exception e){}
			}
		}	
		return nombreArchivo;
	}
	///para formar el csv o pdf de los todos los registros
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		CreaArchivo 	archivo 			= new CreaArchivo();
		try {
		nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
				buffer = new BufferedWriter(writer);
				buffer.write(linea);
				
				//Encabezado
				linea="EPO,"+getNomEpo()+"\nPUBLICACION VIGENTE,"+getMontoPublicacion().replace(',',' ')+"\nRanking,No Proveedor,No Nafin Electr�nico,N�mero de SIRAC,RFC,Tel�fono  del Contacto,Nombre o Raz�n Social,Moneda"+
				",Rango de Electr�nico\n";
				buffer.write(linea);
				while (rs.next()) {
					
					String ran= (rs.getString("RANKING") == null) ? "" : rs.getString("RANKING");
					String pyme=(rs.getString("PYME") == null) ? "" : rs.getString("PYME");
					String nafin=(rs.getString("NAFINELECTRONICO") == null) ? "" : rs.getString("NAFINELECTRONICO");
					String sirac=(rs.getString("IN_NUMERO_SIRAC") == null) ? "" : rs.getString("IN_NUMERO_SIRAC");
					String rfc=(rs.getString("RFC") == null) ? "" : rs.getString("RFC");
					String telefono=(rs.getString("TELEFONO") == null) ? "" : rs.getString("TELEFONO");
					String razon=(rs.getString("RAZONSOCIALPROVEEDOR") == null) ? "" : rs.getString("RAZONSOCIALPROVEEDOR");
					String moneda=(rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
					String rangoIni=(rs.getString("RANGOPUBLIC") == null) ? "" : rs.getString("RANGOPUBLIC");
					String rangoFin=(rs.getString("RANGOPUBLICFIN") == null) ? "" : rs.getString("RANGOPUBLICFIN");
					

					
					linea =ran+","+pyme+","+nafin+","+sirac+","+rfc+","+telefono+","+razon.replace(',',' ')+","+moneda+",De $"+rangoIni+" hasta $"+rangoFin+"\n";
					
					buffer.write(linea);
				}
				buffer.close();
			} catch (Throwable e) {
					throw new AppException("Error al generar el archivo", e);
				} finally {
					try {
						rs.close();
					} catch(Exception e) {}
				}
	
		return nombreArchivo;
	}
	
/*****************************************************
	 GETTERS
*******************************************************/
 
	/**
	  Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
 	
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	
  public String getIc_epo() {
    return ic_epo;
  }

  public void setIc_epo(String ic_epo) {
    this.ic_epo = ic_epo;
  }

 

  public String getPaginar() {
    return paginar;
  }

  public void setPaginar(String paginar) {
    this.paginar = paginar;
  }

  public String getUsuario() {
    return usuario;
  }

  public void setUsuario(String usuario) {
    this.usuario = usuario;
  }

	public String getIc_moneda() {
		return ic_moneda;
	}

	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}


	public void setNomEpo(String nomEpo) {
		this.nomEpo = nomEpo;
	}


	public String getNomEpo() {
		return nomEpo;
	}


	public void setMontoPublicacion(String montoPublicacion) {
		this.montoPublicacion = montoPublicacion;
	}


	public String getMontoPublicacion() {
		return montoPublicacion;
	}
		
}