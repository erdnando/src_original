package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsClientesSiracEXT implements IQueryGeneratorRegExtJS {

private final static Log log = ServiceLocator.getInstance().getLog(ConsClientesSiracEXT.class);
private List 		conditions;
private List pKeys;
StringBuffer qrySentencia = new StringBuffer("");
StringBuffer condicion = new StringBuffer();

	private String noSirac;
	private String tipoPersona;
	private String numeroIdentificacion;
	private String razonSocial;
	private String pApellido;
	private String sApellido;
	private String nombre;
	
	public ConsClientesSiracEXT() {
	}
	
	/**
	 * Este m�todo debe regresar una Lista de parametros que ser�n usados
	 * como valor de las variables BIND de los queries generados.
	 * @return Lista con los valores a usar en las variables bind
	 */
	public  List getConditions(){
		return conditions;
	}
	
	/**
	 * Este m�todo debe regresar un query con el que se obtienen los 
	 * identificadores unicos de los registros a mostrar en la consulta
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public String getDocumentQuery(){
	log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		condicion 		= new StringBuffer();	

if(tipoPersona.equals("F")){
	if(!noSirac.equals("")){
		if(condicion.toString().equals("")){
			condicion.append(" WHERE codigo_cliente = ? \n");
			conditions.add(noSirac);
		} 
	}
	if(!numeroIdentificacion.equals("")){
		numeroIdentificacion=numeroIdentificacion.replace('*','%');
		if(condicion.toString().equals("")){
			condicion.append(" WHERE numero_identificacion LIKE ? \n");
		} else {
			condicion.append(" AND numero_identificacion LIKE ? \n");
		}
		conditions.add(numeroIdentificacion);
	}
	if(!pApellido.equals("")){
		pApellido=pApellido.replace('*','%');
		if(condicion.toString().equals("")){
			condicion.append(" WHERE primer_apellido LIKE ? \n");
		} else {
			condicion.append(" AND primer_apellido LIKE ? \n");
		}
		conditions.add(pApellido);
	}
	if(!sApellido.equals("")){
		sApellido=sApellido.replace('*','%');
		if(condicion.toString().equals("")){
			condicion.append(" WHERE segundo_apellido LIKE ? \n");
		} else {
			condicion.append(" AND segundo_apellido LIKE ? \n");
		}
		conditions.add(sApellido);
	}
	if(!nombre.equals("")){
		nombre=nombre.replace('*','%');
		if(condicion.toString().equals("")){
			condicion.append(" WHERE nombres LIKE ? \n");
		} else {
			condicion.append(" AND nombres LIKE ? \n");
		}
		conditions.add(nombre);
	}
}//fin f
				
if(tipoPersona.equals("M")){
	if(!noSirac.equals("")){
		if(condicion.toString().equals("")){
			condicion.append(" WHERE codigo_cliente = ? \n");
			conditions.add(noSirac);
		} 
	}
	if(!numeroIdentificacion.equals("")){
		numeroIdentificacion=numeroIdentificacion.replace('*','%');
		if(condicion.toString().equals("")){
			condicion.append(" WHERE numero_identificacion LIKE ? \n");
		} else {
			condicion.append(" AND numero_identificacion LIKE ? \n");
		}
		conditions.add(numeroIdentificacion);
	}
	if(!razonSocial.equals("")){
		razonSocial=razonSocial.replace('*','%');
		if(condicion.toString().equals("")){
			condicion.append(" WHERE razon_social LIKE ? \n");
		} else {
			condicion.append(" AND razon_social LIKE ? \n");
		}
		conditions.add(razonSocial);
	}	
}//fin m
				
	qrySentencia.append(
		" SELECT codigo_cliente"  +
		" FROM mg_clientes " + condicion );
									
	qrySentencia.append("ORDER BY codigo_cliente ");
			

		log.info("getDocumentQuery_qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();
	}
	
		/**
	 * Este m�todo debe regresar un query con el que se obtienen totales de la
	 * consulta.
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public  String getAggregateCalculationQuery(){
	log.info("getAggregateCalculationQuery(E)");
		qrySentencia = new StringBuffer("");
		conditions = new ArrayList();
		condicion 		= new StringBuffer();	

if(tipoPersona.equals("F")){
	if(!noSirac.equals("")){
		if(condicion.toString().equals("")){
			condicion.append(" WHERE codigo_cliente = ? \n");
			conditions.add(noSirac);
		} 
	}
	if(!numeroIdentificacion.equals("")){
		numeroIdentificacion=numeroIdentificacion.replace('*','%');
		if(condicion.toString().equals("")){
			condicion.append(" WHERE numero_identificacion LIKE ? \n");
		} else {
			condicion.append(" AND numero_identificacion LIKE ? \n");
		}
		conditions.add(numeroIdentificacion);
	}
	if(!pApellido.equals("")){
		pApellido=pApellido.replace('*','%');
		if(condicion.toString().equals("")){
			condicion.append(" WHERE primer_apellido LIKE ? \n");
		} else {
			condicion.append(" AND primer_apellido LIKE ? \n");
		}
		conditions.add(pApellido);
	}
	if(!sApellido.equals("")){
		sApellido=sApellido.replace('*','%');
		if(condicion.toString().equals("")){
			condicion.append(" WHERE segundo_apellido LIKE ? \n");
		} else {
			condicion.append(" AND segundo_apellido LIKE ? \n");
		}
		conditions.add(sApellido);
	}
	if(!nombre.equals("")){
		nombre=nombre.replace('*','%');
		if(condicion.toString().equals("")){
			condicion.append(" WHERE nombres LIKE ? \n");
		} else {
			condicion.append(" AND nombres LIKE ? \n");
		}
		conditions.add(nombre);
	}
}//fin f
				
if(tipoPersona.equals("M")){
	if(!noSirac.equals("")){
		if(condicion.toString().equals("")){
			condicion.append(" WHERE codigo_cliente = ? \n");
			conditions.add(noSirac);
		} 
	}
	if(!numeroIdentificacion.equals("")){
		numeroIdentificacion=numeroIdentificacion.replace('*','%');
		if(condicion.toString().equals("")){
			condicion.append(" WHERE numero_identificacion LIKE ? \n");
		} else {
			condicion.append(" AND numero_identificacion LIKE ? \n");
		}
		conditions.add(numeroIdentificacion);
	}
	if(!razonSocial.equals("")){
		razonSocial=razonSocial.replace('*','%');
		if(condicion.toString().equals("")){
			condicion.append(" WHERE razon_social LIKE ? \n");
		} else {
			condicion.append(" AND razon_social LIKE ? \n");
		}
		conditions.add(razonSocial);
	}	
}//fin m
				
	qrySentencia.append(
		" SELECT codigo_cliente, numero_identificacion, nombres, "   +
		 	 " codigo_tipo_identificacion, primer_apellido, segundo_apellido, "   +
			 " razon_social,codigo_bloqueo, numero_de_nit, codigo_tipo_cliente, "   +
			 " codigo_categoria, codigo_agencia,tipo_de_persona "   +
		" FROM mg_clientes " + condicion );
									
	qrySentencia.append("ORDER BY codigo_cliente ");
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	/**
	 * Este m�todo debe regresar un query con el que se obtienen los 
	 * datos a mostrar en la consulta basandose en los identificadores unicos 
	 * especificados en las lista de ids
	 * @param ids Lista de los identificadoes unicos. El tama�o de la lista 
	 * 	recibida ser� de acuerdo a la configuraci�n de registros x p�gina
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public  String getDocumentSummaryQueryForIds(List ids){
	log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		condicion 		= new StringBuffer();	

if(tipoPersona.equals("F")){
	if(!noSirac.equals("")){
		if(condicion.toString().equals("")){
			condicion.append(" WHERE codigo_cliente = ? \n");
			conditions.add(noSirac);
		} 
	}
	if(!numeroIdentificacion.equals("")){
		numeroIdentificacion=numeroIdentificacion.replace('*','%');
		if(condicion.toString().equals("")){
			condicion.append(" WHERE numero_identificacion LIKE ? \n");
		} else {
			condicion.append(" AND numero_identificacion LIKE ? \n");
		}
		conditions.add(numeroIdentificacion);
	}
	if(!pApellido.equals("")){
		pApellido=pApellido.replace('*','%');
		if(condicion.toString().equals("")){
			condicion.append(" WHERE primer_apellido LIKE ? \n");
		} else {
			condicion.append(" AND primer_apellido LIKE ? \n");
		}
		conditions.add(pApellido);
	}
	if(!sApellido.equals("")){
		sApellido=sApellido.replace('*','%');
		if(condicion.toString().equals("")){
			condicion.append(" WHERE segundo_apellido LIKE ? \n");
		} else {
			condicion.append(" AND segundo_apellido LIKE ? \n");
		}
		conditions.add(sApellido);
	}
	if(!nombre.equals("")){
		nombre=nombre.replace('*','%');
		if(condicion.toString().equals("")){
			condicion.append(" WHERE nombres LIKE ? \n");
		} else {
			condicion.append(" AND nombres LIKE ? \n");
		}
		conditions.add(nombre);
	}
}//fin f
				
if(tipoPersona.equals("M")){
	if(!noSirac.equals("")){
		if(condicion.toString().equals("")){
			condicion.append(" WHERE codigo_cliente = ? \n");
			conditions.add(noSirac);
		} 
	}
	if(!numeroIdentificacion.equals("")){
		numeroIdentificacion=numeroIdentificacion.replace('*','%');
		if(condicion.toString().equals("")){
			condicion.append(" WHERE numero_identificacion LIKE ? \n");
		} else {
			condicion.append(" AND numero_identificacion LIKE ? \n");
		}
		conditions.add(numeroIdentificacion);
	}
	if(!razonSocial.equals("")){
		razonSocial=razonSocial.replace('*','%');
		if(condicion.toString().equals("")){
			condicion.append(" WHERE razon_social LIKE ? \n");
		} else {
			condicion.append(" AND razon_social LIKE ? \n");
		}
		conditions.add(razonSocial);
	}	
}//fin m
				
	qrySentencia.append(
		" SELECT codigo_cliente, numero_identificacion, nombres, "   +
		 	 " codigo_tipo_identificacion, primer_apellido, segundo_apellido, "   +
			 " razon_social,codigo_bloqueo, numero_de_nit, codigo_tipo_cliente, "   +
			 " codigo_categoria, codigo_agencia,tipo_de_persona "   +
		" FROM mg_clientes " + condicion );
									
	qrySentencia.append("ORDER BY codigo_cliente ");
		
		log.info("getDocumentSummaryQueryForIds_qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();
	}
	
	
	/**
	 * Este m�todo debe regresar un query que obtendr� todos los registros
	 * resultantes de la b�squeda, con la finalidad de generar un archivo.
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public  String getDocumentQueryFile(){
	log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();  
				condicion 		= new StringBuffer();	

if(tipoPersona.equals("F")){
	if(!noSirac.equals("")){
		if(condicion.toString().equals("")){
			condicion.append(" WHERE codigo_cliente = ? \n");
			conditions.add(noSirac);
		} 
	}
	if(!numeroIdentificacion.equals("")){
		numeroIdentificacion=numeroIdentificacion.replace('*','%');
		if(condicion.toString().equals("")){
			condicion.append(" WHERE numero_identificacion LIKE ? \n");
		} else {
			condicion.append(" AND numero_identificacion LIKE ? \n");
		}
		conditions.add(numeroIdentificacion);
	}
	if(!pApellido.equals("")){
		pApellido=pApellido.replace('*','%');
		if(condicion.toString().equals("")){
			condicion.append(" WHERE primer_apellido LIKE ? \n");
		} else {
			condicion.append(" AND primer_apellido LIKE ? \n");
		}
		conditions.add(pApellido);
	}
	if(!sApellido.equals("")){
		sApellido=sApellido.replace('*','%');
		if(condicion.toString().equals("")){
			condicion.append(" WHERE segundo_apellido LIKE ? \n");
		} else {
			condicion.append(" AND segundo_apellido LIKE ? \n");
		}
		conditions.add(sApellido);
	}
	if(!nombre.equals("")){
		nombre=nombre.replace('*','%');
		if(condicion.toString().equals("")){
			condicion.append(" WHERE nombres LIKE ? \n");
		} else {
			condicion.append(" AND nombres LIKE ? \n");
		}
		conditions.add(nombre);
	}
}//fin f
				
if(tipoPersona.equals("M")){
	if(!noSirac.equals("")){
		if(condicion.toString().equals("")){
			condicion.append(" WHERE codigo_cliente = ? \n");
			conditions.add(noSirac);
		} 
	}
	if(!numeroIdentificacion.equals("")){
		numeroIdentificacion=numeroIdentificacion.replace('*','%');
		if(condicion.toString().equals("")){
			condicion.append(" WHERE numero_identificacion LIKE ? \n");
		} else {
			condicion.append(" AND numero_identificacion LIKE ? \n");
		}
		conditions.add(numeroIdentificacion);
	}
	if(!razonSocial.equals("")){
		razonSocial=razonSocial.replace('*','%');
		if(condicion.toString().equals("")){
			condicion.append(" WHERE razon_social LIKE ? \n");
		} else {
			condicion.append(" AND razon_social LIKE ? \n");
		}
		conditions.add(razonSocial);
	}	
}//fin m
				
	qrySentencia.append(
		" SELECT codigo_cliente, numero_identificacion, nombres, "   +
		 	 " codigo_tipo_identificacion, primer_apellido, segundo_apellido, "   +
			 " razon_social,codigo_bloqueo, numero_de_nit, codigo_tipo_cliente, "   +
			 " codigo_categoria, codigo_agencia,tipo_de_persona "   +
		" FROM mg_clientes " + condicion );
									
	qrySentencia.append("ORDER BY codigo_cliente ");
							
		log.info("getDocumentQueryFile_qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
	
		/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public  String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
	log.debug("crearCustomFile (E)");
	String nombreArchivo ="";
	HttpSession session = request.getSession();
	CreaArchivo creaArchivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer();
		
	OutputStreamWriter writer = null;
	BufferedWriter buffer = null;
	int total = 0;
		
	try {
		nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
		writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
		buffer = new BufferedWriter(writer);
			
		contenidoArchivo = new StringBuffer();	
		contenidoArchivo.append( "C�digo Cliente ,RFC,Nombre,Apellido Paterno,Apellido Materno,Raz�n social,C�digo de Bloqueo\n");
			
		while (rs.next())	{
			String codcliente		= (rs.getString("CODIGO_CLIENTE") 				== null) ? "" : rs.getString("CODIGO_CLIENTE");
			String rfc 				= (rs.getString("NUMERO_IDENTIFICACION") 		== null) ? "" : rs.getString("NUMERO_IDENTIFICACION");
			String nombre 			= (rs.getString("NOMBRES") 						== null) ? "" : rs.getString("NOMBRES");				
			String pApellido		= (rs.getString("PRIMER_APELLIDO") 				== null) ? "" : rs.getString("PRIMER_APELLIDO");
			String sApellido	 	= (rs.getString("SEGUNDO_APELLIDO") 			== null) ? "" : rs.getString("SEGUNDO_APELLIDO");				
			String razonSocial	= (rs.getString("RAZON_SOCIAL")					== null) ? "" : rs.getString("RAZON_SOCIAL");
			String codBloqueo	 	= (rs.getString("CODIGO_BLOQUEO") 				== null) ? "" : rs.getString("CODIGO_BLOQUEO");
			
			contenidoArchivo.append(
			codcliente.replace	(',',' ')+","+
			rfc.replace	(',',' ')+","+		
			nombre.replace		(',',' ')+","+
			pApellido.replace	(',',' ')+","+
			sApellido.replace		(',',' ')+","+
			razonSocial.replace	(',',' ')+","+
			codBloqueo.replace		(',',' ')+","+ "\n");
		}
			
		creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
		nombreArchivo = creaArchivo.getNombre();
		
		}catch (Exception e){
			throw new AppException("Error al generar el archivo ", e);
		}
	log.debug("crearCustomFile (S)");
	return nombreArchivo;
	}
	

	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public  String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){
	String nombreArchivo = "";
	HttpSession session = request.getSession();	
	ComunesPDF pdfDoc = new ComunesPDF();
	CreaArchivo creaArchivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer();
		
	try {
		nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
		pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
		pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
		pdfDoc.setTable(7,90);
			
		pdfDoc.setCell("C�digo Cliente"		,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("RFC"						,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Nombre"					,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Apellido Paterno"		,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Apellido Materno"	,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Raz�n social"			,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("C�digo de Bloqueo"	,"celda01",ComunesPDF.CENTER);

			
		while(rs.next()){
			String codcliente		= (rs.getString("CODIGO_CLIENTE") 				== null) ? "" : rs.getString("CODIGO_CLIENTE");
			String rfc 				= (rs.getString("NUMERO_IDENTIFICACION") 		== null) ? "" : rs.getString("NUMERO_IDENTIFICACION");
			String nombre 			= (rs.getString("NOMBRES") 						== null) ? "" : rs.getString("NOMBRES");				
			String pApellido		= (rs.getString("PRIMER_APELLIDO") 				== null) ? "" : rs.getString("PRIMER_APELLIDO");
			String sApellido	 	= (rs.getString("SEGUNDO_APELLIDO") 			== null) ? "" : rs.getString("SEGUNDO_APELLIDO");				
			String razonSocial	= (rs.getString("RAZON_SOCIAL")					== null) ? "" : rs.getString("RAZON_SOCIAL");
			String codBloqueo	 	= (rs.getString("CODIGO_BLOQUEO") 				== null) ? "" : rs.getString("CODIGO_BLOQUEO");
			
			pdfDoc.setCell(codcliente	,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(rfc			,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(nombre		,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(pApellido	,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(sApellido	,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(razonSocial	,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(codBloqueo	,"formas",ComunesPDF.CENTER);
		}
		pdfDoc.addTable();
		pdfDoc.endDocument();
	} catch(Exception e){
		throw new AppException("Error al generar el archivo PDF ", e);
	}
	return nombreArchivo;
	}


	public void setNoSirac(String noSirac) {
		this.noSirac = noSirac;
	}


	public String getNoSirac() {
		return noSirac;
	}


	public void setNumeroIdentificacion(String numeroIdentificacion) {
		this.numeroIdentificacion = numeroIdentificacion;
	}


	public String getNumeroIdentificacion() {
		return numeroIdentificacion;
	}


	public void setRazonSocial(String razonSocial) {
		this.razonSocial = razonSocial;
	}


	public String getRazonSocial() {
		return razonSocial;
	}


	public void setPApellido(String pApellido) {
		this.pApellido = pApellido;
	}


	public String getPApellido() {
		return pApellido;
	}


	public void setSApellido(String sApellido) {
		this.sApellido = sApellido;
	}


	public String getSApellido() {
		return sApellido;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getNombre() {
		return nombre;
	}


	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}


	public String getTipoPersona() {
		return tipoPersona;
	}






}