package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class BloqueoPymeXEpoCons implements IQueryGeneratorRegExtJS {

	private final static Log log = ServiceLocator.getInstance().getLog(BloqueoPymeXEpoCons.class);
	private List conditions;
	String num_pyme;
	String rfc_pyme;
	String nombre_pyme;
	String ic_producto_nafin;
	private String ic_epo;
	String noBancoFondeo="";
	
	
	StringBuffer qrySentencia = new StringBuffer("");
	
	public BloqueoPymeXEpoCons() {}
	
	/**
	 * Obtiene las llaves primarias, 
	 * estoy asumiendo que numero_sirac es la llave primaria y unica
	 * @return sentencia sql
	 */
	public String getDocumentQuery(){ 

		log.info(" getDocumentQuery(E)");	
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();
		
		if (!"".equals(ic_producto_nafin)){
			condicion.append(" AND bpxp.ic_producto_nafin = ? ");
			conditions.add(ic_producto_nafin);	
		}	
		if (!"".equals(rfc_pyme)){
			condicion.append(" AND UPPER(p.cg_rfc) LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') "); 
			conditions.add(rfc_pyme);
		}
		if (!"".equals(num_pyme)){
			condicion.append(" AND n.ic_nafin_electronico = ? "); 
			conditions.add(num_pyme);
		}
		if (!"".equals(ic_epo)){
			condicion.append(" AND ce.ic_epo = ? "); 
			conditions.add(ic_epo);
		}
		if (!"".equals( nombre_pyme)){
			condicion.append(" AND UPPER(p.cg_razon_social) LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') "); 
			conditions.add( nombre_pyme);
		}		
		if("2".equals(noBancoFondeo)) {
			condicion.append(	" AND bpxp.ic_pyme IN " +
									"( select cpe.ic_pyme from comrel_pyme_epo cpe, comcat_epo epo " +
									" where cpe.ic_epo = epo.ic_epo " +
									" AND ic_banco_fondeo = "+noBancoFondeo+" )" );
		}
		try {	
			qrySentencia.append(
				" SELECT bpxp.ic_pyme, 'BloqueoPymeConsulta::getDocumentQuery'"+
				" FROM 	comrel_bloqueo_pyme_x_epo bpxp,"+
				"    		comcat_producto_nafin pn,"+
				"    		com_contacto c, comrel_nafin n, comcat_pyme p, comcat_epo ce "+
				" WHERE  bpxp.ic_pyme = p.ic_pyme"+
				"			AND bpxp.ic_epo = ce.ic_epo " +
				"    		AND bpxp.ic_producto_nafin = pn.ic_producto_nafin"+
				"    		AND bpxp.ic_pyme = c.ic_pyme"+
				"    		AND bpxp.ic_pyme = n.ic_epo_pyme_if "+
				"    		AND c.cs_primer_contacto = 'S'"+
				"    		AND n.cg_tipo = 'P'"+
				condicion +
				" ORDER BY p.cg_razon_social");
				
		}catch(Exception e){
			log.warn("BloqueoPymeConsulta::getDocumentQueryException "+e);
		}
		log.info(" getDocumentQuery(S)");
		log.debug("Sentencia(getDocumentQuery): " + qrySentencia.toString());
		return qrySentencia.toString();
	}
		
	/**
	 * Obtiene la lista de los Ids en turno para realizar la paginacion
	 * @return sentencia sql
	 * @param ids
	 */	
	public String getDocumentSummaryQueryForIds(List ids){
		log.info(" getDocumentSummaryQueryForIds(E)");		
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();
		
		condicion.append(" AND bpxp.ic_pyme in (");
		for (int i = 0; i < ids.size(); i++) { 
			List lItem = (ArrayList)ids.get(i);
			if(i > 0){
				condicion.append(",");
			}
			condicion.append(" ? " );
			conditions.add(new Long(lItem.get(0).toString()));
		}
		condicion.append(" ) ");		
		
		qrySentencia.append(
				"SELECT 	bpxp.ic_pyme,"+ 
				"    		bpxp.ic_producto_nafin, "+
				"    		pn.ic_nombre, "+
				"    		n.ic_nafin_electronico, "+
				"    		p.cg_razon_social, "+
				"    		p.cg_rfc, "+
				"    		c.cg_email, "+
				"    		TO_CHAR(bpxp.df_modificacion,'dd/mm/yyyy hh24:mi') as df_modificacion,  "+
				"    		bpxp.cg_causa_bloqueo, "+
				"    		bpxp.ic_usuario,  "+
				"    		bpxp.cs_bloqueo, "+
				"    		ce.ic_epo, "+
				"    		ce.cg_razon_social nombre_epo "+
				"FROM 	comrel_bloqueo_pyme_x_epo bpxp, "+
				"    		comcat_producto_nafin pn, "+
				"    		com_contacto c, comrel_nafin n, comcat_pyme p, comcat_epo ce "+
				"WHERE 	bpxp.ic_pyme = p.ic_pyme "+
				"			AND bpxp.ic_epo = ce.ic_epo " +
				"    		AND bpxp.ic_producto_nafin = pn.ic_producto_nafin "+
				"   	 	AND bpxp.ic_pyme = c.ic_pyme "+
				"    		AND bpxp.ic_pyme = n.ic_epo_pyme_if  "+
				"    		AND c.cs_primer_contacto = 'S' "+
				"    		AND n.cg_tipo = 'P' "+
				condicion +
				" ORDER BY p.cg_razon_social");
		
		log.info(" getDocumentSummaryQueryForIds(S)");
		log.debug("Sentencia(getDocumentSummaryQueryForIds): " + qrySentencia.toString());
		log.debug("condicion(getDocumentSummaryQueryForIds): " + condicion.toString());
		return qrySentencia.toString();
	}
	
	/**
	 * Obtiene los totales
	 * @return sentencia sql
	 */		
	public String getAggregateCalculationQuery(){

		log.info(" getAggregateCalculationQuery(E)");		
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();
		
		if (!"".equals(ic_producto_nafin)){
			condicion.append(" AND bpxp.ic_producto_nafin = ? ");
			conditions.add(ic_producto_nafin);	
		}	
		if (!"".equals(rfc_pyme)){
			condicion.append(" AND UPPER(p.cg_rfc) LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')"); 
			conditions.add(rfc_pyme);
		}
		if (!"".equals(num_pyme)){
			condicion.append(" AND n.ic_nafin_electronico = ?"); 
			conditions.add(num_pyme);
		}
		if (!"".equals(ic_epo)){
			condicion.append(" AND ce.ic_epo = ? "); 
			conditions.add(ic_epo);
		}
		if (!"".equals( nombre_pyme)){
			condicion.append(" AND UPPER(p.cg_razon_social) LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')"); 
			conditions.add( nombre_pyme);
		}		
		if("2".equals(noBancoFondeo)) {
			condicion.append(	" AND bpxp.ic_pyme IN " +
									"( select cpe.ic_pyme from comrel_pyme_epo cpe, comcat_epo epo " +
									" where cpe.ic_epo = epo.ic_epo " +
									" AND ic_banco_fondeo = "+noBancoFondeo+" )" );
		}
		try {	
			qrySentencia.append(
				" SELECT count(bpxp.ic_pyme) as total"+
				" FROM 	comrel_bloqueo_pyme_x_epo bpxp,"+
				"    		comcat_producto_nafin pn,"+
				"    		com_contacto c, comrel_nafin n, comcat_pyme p, comcat_epo ce "+
				" WHERE  bpxp.ic_pyme = p.ic_pyme"+
				"			AND bpxp.ic_epo = ce.ic_epo " +
				"    		AND bpxp.ic_producto_nafin = pn.ic_producto_nafin"+
				"    		AND bpxp.ic_pyme = c.ic_pyme"+
				"    		AND bpxp.ic_pyme = n.ic_epo_pyme_if "+
				"    		AND c.cs_primer_contacto = 'S'"+
				"    		AND n.cg_tipo = 'P'"+
				condicion +
				" ORDER BY p.cg_razon_social");
				
		}catch(Exception e){
			log.warn("BloqueoPymeConsulta::getAggregateCalculationQuery "+e);
		}
		log.info(" getAggregateCalculationQuery(S)");
		log.debug("Sentencia(getAggregateCalculationQuery): " + qrySentencia.toString());
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile(){
			log.info("getDocumentQueryFile(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			
		StringBuffer condicion = new StringBuffer();
		
		if(!"".equals(num_pyme)) {
			condicion.append("	AND n.ic_nafin_electronico = ? ");
		}
		if(!"".equals(rfc_pyme)) {
			condicion.append("	AND UPPER(p.cg_rfc) LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')");
		}
		if(!"".equals(nombre_pyme)) {
			condicion.append("	AND UPPER(p.cg_razon_social) LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')");
		}
		if(!"".equals(ic_producto_nafin)) {
			condicion.append("	AND bpxp.ic_producto_nafin = ? ");
		}
		if (!"".equals(ic_epo)){
			condicion.append(" AND ce.ic_epo = ? "); 
			conditions.add(ic_epo);
		}
//--i- fin MODIFICACION FODEA 000 -2009 --
		if("2".equals(noBancoFondeo)) {
			condicion.append(	" AND bpxp.ic_pyme IN " +
									"( select cpe.ic_pyme from comrel_pyme_epo cpe, comcat_epo epo " +
									" where cpe.ic_epo = epo.ic_epo " +
									" AND ic_banco_fondeo = "+noBancoFondeo+" )" );
		}

/**		if (!"".equals(ic_producto_nafin)){
			condicion.append(" AND bpxp.ic_producto_nafin = ?");
			if(ic_producto_nafin.equals("Financiamiento a Distribuidores")){
				 conditions.add("4");
			} else{
				conditions.add("1");
			}	
		}	
		if (!"".equals(rfc_pyme)){
			condicion.append(" AND UPPER(p.cg_rfc) LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')"); 
			conditions.add(rfc_pyme);
		}
		if (!"".equals(num_pyme)){
			condicion.append(" AND n.ic_nafin_electronico = ?"); 
			conditions.add(num_pyme);
		}
		if (!"".equals( nombre_pyme)){
			condicion.append(" AND UPPER(p.cg_razon_social) LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')"); 
			conditions.add( nombre_pyme);
		}		
		if("2".equals(noBancoFondeo)) {
			condicion.append(	" AND bpxp.ic_pyme IN " +
									"( select cpe.ic_pyme from comrel_pyme_epo cpe, comcat_epo epo " +
									" where cpe.ic_epo = epo.ic_epo " +
									" AND ic_banco_fondeo = "+noBancoFondeo+" )" );
		}
*/		
//--f- fin MODIFICACION FODEA 000 -2009 --
		qrySentencia.append(
				" SELECT " +
				" 	bpxp.ic_pyme, " +
				" 	bpxp.ic_producto_nafin, " +
				" 	pn.ic_nombre, " +
				" 	n.ic_nafin_electronico, " +
				" 	p.cg_razon_social, " +
				" 	p.cg_rfc, " +
				" 	c.cg_email, " +
				" 	TO_CHAR(bpxp.df_modificacion,'dd/mm/yyyy hh24:mi') as df_modificacion,  " +
				" 	bpxp.cg_causa_bloqueo, " +
				" 	bpxp.ic_usuario,  " +
				" 	bpxp.cs_bloqueo, " +
				"  ce.ic_epo, "+
				"  ce.cg_razon_social nombre_epo "+
				" FROM " +
				" 	comrel_bloqueo_pyme_x_epo bpxp, " +
				" 	comcat_producto_nafin pn, " +
				" 	com_contacto c, comrel_nafin n, comcat_pyme p, comcat_epo ce " +
				" WHERE " +
				" 	bpxp.ic_pyme = p.ic_pyme " +
				"	AND bpxp.ic_epo = ce.ic_epo " +
				" 	AND bpxp.ic_producto_nafin = pn.ic_producto_nafin " +
				" 	AND bpxp.ic_pyme = c.ic_pyme " +
				" 	AND bpxp.ic_pyme = n.ic_epo_pyme_if  " +
				" 	AND c.cs_primer_contacto = 'S' " +
				" 	AND n.cg_tipo = 'P' " +
				condicion.toString() +
				" ORDER BY p.cg_razon_social ");
				
			if(!"".equals(num_pyme)) {
				//cont++;ps.setString(cont,num_pyme);
				conditions.add(new Integer(num_pyme)); 
			}
			if(!"".equals(rfc_pyme)) {
				//cont++;ps.setString(cont,rfc_pyme);
				conditions.add(rfc_pyme); 
			}
			if(!"".equals(nombre_pyme)) {
				//cont++;ps.setString(cont,nombre_pyme);
				conditions.add(nombre_pyme); 
			}
			
			if(!"".equals(ic_producto_nafin)) {
				//cont++;ps.setString(cont, ic_producto_nafin);
				conditions.add(ic_producto_nafin);
			}

		
			log.info("qrySentencia  "+qrySentencia);
			log.info("conditions "+conditions);
			log.info("getDocumentQueryFile(S)");
			return qrySentencia.toString();
		}
		
		
		public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rsAPI, String path, String tipo){
			log.debug("crearCustomFile (E)");
			String nombreArchivo = "";
			HttpSession session = request.getSession();	
			CreaArchivo creaArchivo = new CreaArchivo();
			StringBuffer contenidoArchivo = new StringBuffer();
		
			/*try {
			
				HashMap hmProductos = new HashMap();

				ParametrosGralesHome parametrosGralesHome =(ParametrosGralesHome)ServiceLocator.getInstance().getEJBHome("ParametrosGralesEJB",ParametrosGralesHome.class);
				ParametrosGrales BeanParametrosGrales = parametrosGralesHome.create();
				
				hmProductos =(HashMap)BeanParametrosGrales.getEncabezadosMatrizApis(); 
		
				StringBuffer sbArchivo = new StringBuffer();
				String nombreAPI = "";
				int iNoReg = 0;
				while(rsAPI.next()) {
					if(!nombreAPI.equals(rsAPI.getString("nombreAPI"))) {
						nombreAPI = rsAPI.getString("nombreAPI");
						
						sbArchivo.append(nombreAPI.toUpperCase()+"\r\n");
						sbArchivo.append("N�mero de Par�metro,Nombre,Nombre del Dato en la Forma");
						if(sNombreProducto.equals("1A") || sNombreProducto.equals("T"))
							sbArchivo.append(","+hmProductos.get("1A").toString());
						if(sNombreProducto.equals("1B") || sNombreProducto.equals("T"))
							sbArchivo.append(","+hmProductos.get("1B").toString());
						if(sNombreProducto.equals("1C") || sNombreProducto.equals("T"))
							sbArchivo.append(","+hmProductos.get("1C").toString());
						if(sNombreProducto.equals("1D") || sNombreProducto.equals("T"))
							sbArchivo.append(","+hmProductos.get("1D").toString());
						if(sNombreProducto.equals("2A") || sNombreProducto.equals("T"))
							sbArchivo.append(","+hmProductos.get("2A").toString());
						if(sNombreProducto.equals("0") || sNombreProducto.equals("T"))
							sbArchivo.append(","+hmProductos.get("0").toString());
						if(sNombreProducto.equals("5A") || sNombreProducto.equals("T"))
							sbArchivo.append(","+hmProductos.get("5A").toString());
						if(sNombreProducto.equals("4A") || sNombreProducto.equals("T"))
							sbArchivo.append(","+hmProductos.get("4A").toString());
						if(sNombreProducto.equals("4B") || sNombreProducto.equals("T"))
							sbArchivo.append(","+hmProductos.get("4B").toString());
						if(sNombreProducto.equals("8") || sNombreProducto.equals("T"))
							sbArchivo.append(","+hmProductos.get("8").toString());
						sbArchivo.append("\r\n");
					}
					sbArchivo.append((rsAPI.getString("numeroParametro")==null?"":rsAPI.getString("numeroParametro"))+",");
					sbArchivo.append((rsAPI.getString("nombreParametro")==null?"":rsAPI.getString("nombreParametro").replace(',',' '))+",");
					sbArchivo.append((rsAPI.getString("descParametro")==null?"":rsAPI.getString("descParametro").replace(',',' ')));
					if(sNombreProducto.equals("1A") || sNombreProducto.equals("T"))
						sbArchivo.append(","+(rsAPI.getString("cg_descprod1")==null?"":getQuitarEtiquetas(rsAPI.getString("cg_descprod1"),"<br>")));
					if(sNombreProducto.equals("1B") || sNombreProducto.equals("T"))
						sbArchivo.append(","+(rsAPI.getString("cg_descprod2")==null?"":getQuitarEtiquetas(rsAPI.getString("cg_descprod2"),"<br>")));
					if(sNombreProducto.equals("1C") || sNombreProducto.equals("T"))
						sbArchivo.append(","+(rsAPI.getString("cg_descprod3")==null?"":getQuitarEtiquetas(rsAPI.getString("cg_descprod3"),"<br>")));
					if(sNombreProducto.equals("1D") || sNombreProducto.equals("T"))
						sbArchivo.append(","+(rsAPI.getString("cg_descprod4")==null?"":getQuitarEtiquetas(rsAPI.getString("cg_descprod4"),"<br>")));
					if(sNombreProducto.equals("2A") || sNombreProducto.equals("T"))
						sbArchivo.append(","+(rsAPI.getString("cg_descprod5")==null?"":getQuitarEtiquetas(rsAPI.getString("cg_descprod5"),"<br>")));
					if(sNombreProducto.equals("0") || sNombreProducto.equals("T"))
						sbArchivo.append(","+(rsAPI.getString("cg_descprod6")==null?"":getQuitarEtiquetas(rsAPI.getString("cg_descprod6"),"<br>")));
					if(sNombreProducto.equals("5A") || sNombreProducto.equals("T"))
						sbArchivo.append(","+(rsAPI.getString("cg_descprod7")==null?"":getQuitarEtiquetas(rsAPI.getString("cg_descprod7"),"<br>")));
					if(sNombreProducto.equals("4A") || sNombreProducto.equals("T"))
						sbArchivo.append(","+(rsAPI.getString("cg_descprod8")==null?"":getQuitarEtiquetas(rsAPI.getString("cg_descprod8"),"<br>")));
					if(sNombreProducto.equals("4B") || sNombreProducto.equals("T"))
						sbArchivo.append(","+(rsAPI.getString("cg_descprod9")==null?"":getQuitarEtiquetas(rsAPI.getString("cg_descprod9"),"<br>")));
					if(sNombreProducto.equals("8") || sNombreProducto.equals("T"))
						sbArchivo.append(","+(rsAPI.getString("cg_descprod10")==null?"":getQuitarEtiquetas(rsAPI.getString("cg_descprod10"),"<br>")));
					sbArchivo.append("\r\n");
				} 
				CreaArchivo archivo = new CreaArchivo();
				if (archivo.make(sbArchivo.toString(), path, ".csv"))
					nombreArchivo = archivo.getNombre();
			} catch(Exception e) { 
				log.error("Error en la generacion del Archivo CSV: " + e);
			}	*/
			return nombreArchivo;		 
		}
	
			
	/**
	 * Metodo para generar un archivo PDF, utilizando paginaci�n
	 * @return 
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){	 
		
		log.info("crearPageCustomFile (E)");
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();		
		try {
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
			pdfDoc.setTable(10,100);
			pdfDoc.setCell("Producto ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("EPO ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("N@E ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Pyme ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("RFC ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("E-mail ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Bloqueo/Hist�rico ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha y Hora Bloq./ Desbloq. ","celda01",ComunesPDF.CENTER); 
			pdfDoc.setCell("Usuario ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Causa ","celda01",ComunesPDF.CENTER);
			
			
			while (rs.next())	{		
				String producto = (rs.getString("ic_nombre") == null) ? "" : rs.getString("ic_nombre");	
				String numPyme = (rs.getString("ic_nafin_electronico") == null) ? "" : rs.getString("ic_nafin_electronico");				
				String nombrePyme = (rs.getString("cg_razon_social") == null) ? "" : rs.getString("cg_razon_social");
				String rfc = (rs.getString("cg_rfc") == null) ? "" : rs.getString("cg_rfc");
				String email = (rs.getString("cg_email") == null) ? "" : rs.getString("cg_email");
				String fechaModificacion = (rs.getString("df_modificacion") == null) ? "" : rs.getString("df_modificacion");
				String usuario = (rs.getString("ic_usuario") == null) ? "" : rs.getString("ic_usuario");
				String causaBloqueo = (rs.getString("cg_causa_bloqueo") == null) ? "" : rs.getString("cg_causa_bloqueo");
				String bloqueo = (rs.getString("CS_BLOQUEO") == null) ? "" : rs.getString("CS_BLOQUEO");
				String nombreEpo = (rs.getString("nombre_epo") == null) ? "" : rs.getString("nombre_epo");
				
				if(bloqueo.equals("B")){
					bloqueo = "Bloqueado";
				} else if(bloqueo.equals("D")){
					bloqueo = "Desbloqueado";
				}

				pdfDoc.setCell(producto,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(nombreEpo,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(numPyme,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(nombrePyme,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(rfc,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(email,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(bloqueo,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaModificacion,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(usuario,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(causaBloqueo,"formas",ComunesPDF.CENTER);
			}
		
			pdfDoc.addTable();
			pdfDoc.endDocument();	
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		}		
		
		log.info("crearPageCustomFile (S) "  + nombreArchivo);
		return nombreArchivo;
	}

/************************************************************************
 *									GETTERS AND SETTERS									*
 ************************************************************************/	
	public void setNum_pyme(String num_pyme) {
		this.num_pyme = num_pyme;
	}

	public String getNum_pyme() {
		return num_pyme;
	}

	public void setRfc_pyme(String rfc_pyme) {
		this.rfc_pyme = rfc_pyme;
	}

	public String getRfc_pyme() {
		return rfc_pyme;
	}

	public void setNombre_pyme(String nombre_pyme) {
		this.nombre_pyme = nombre_pyme;
	}

	public String getNombre_pyme() {
		return nombre_pyme;
	}

	public void setIc_producto_nafin(String ic_producto_nafin) {
		this.ic_producto_nafin = ic_producto_nafin;
	}

	public String getIc_producto_nafin() {
		return ic_producto_nafin;
	}
	
	public List getConditions(){
			return conditions;
		}


	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}


	public String getIc_epo() {
		return ic_epo;
	}
}