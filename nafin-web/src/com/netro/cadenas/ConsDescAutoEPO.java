package com.netro.cadenas;

import com.netro.exception.NafinException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import netropology.utilerias.AccesoDB;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * clase para la pantalla  Admin EPO /Administración /parametrización/ Descuento Automático EPO
 * @author Deysi Laura Hernández Contreras
 */
public class ConsDescAutoEPO implements IQueryGeneratorRegExtJS {
   
    private static final Log LOG = LogFactory.getLog(ConsDescAutoEPO.class);
    
    private String paginaOffset;
    private String paginaNo;
    private List conditions;
    StringBuilder qrySentencia = new StringBuilder("");
    private String idclaveEpo;   
    private String fechaDescuentoIni;
    private String fechaDescuentoFin;
    
    /**
     * constructor
     */
    public ConsDescAutoEPO() {

    }

    /**
     * @return
     */
    public String getAggregateCalculationQuery() {
        
        return qrySentencia.toString();
    }

    /**
     * @return
     */
    public String getDocumentQuery() {    
        return qrySentencia.toString();
    }


    /**
     * @param pageIds
     * @return
     */
    public String getDocumentSummaryQueryForIds(List pageIds) {     
        return qrySentencia.toString();
    }

    /**
     * @return
     */
    public String getDocumentQueryFile() {
        LOG.info("getDocumentQueryFile(E)");
        qrySentencia = new StringBuilder();
        conditions = new ArrayList();

        qrySentencia.append(" select   " + 
                            " TO_CHAR (DF_DESCUENTO, 'DD/MM/YYYY') AS FECHA_DESCUENTO ,  " +
                            " TO_CHAR (DF_PARAMETIZADA, 'DD/MM/YYYY') AS FECHA_PARAMETRIZACION , " +
                            " CG_USUARIO AS  USUARIO  " +
                            " FROM COM_DESC_AUTO_EPO " +
                            " WHERE  IC_EPO =  ?  ");
        
        this.conditions.add(idclaveEpo);
               
        if (!"".equals(fechaDescuentoIni)  &&  !"".equals(fechaDescuentoFin) ) {           
            qrySentencia.append(" AND DF_DESCUENTO >= TO_DATE(?,'DD/MM/YYYY')  AND DF_DESCUENTO < TO_DATE(?,'DD/MM/YYYY') + 1 ");  
            this.conditions.add(fechaDescuentoIni);
            this.conditions.add(fechaDescuentoFin);
        }              
                
        qrySentencia.append("   order by df_descuento asc   ");
                     
                     
        LOG.debug("qrySentencia  " + qrySentencia+" \n  conditions " + conditions);
        
        LOG.info("getDocumentQueryFile(S)");
        return qrySentencia.toString();
    }

    /**
     * @param request
     * @param rs
     * @param path
     * @param tipo
     * @return
     */
    public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path,   String tipo) {
       
        return "";

    }

    /**
     * @param request
     * @param rs
     * @param path
     * @param tipo
     * @return
     */
    public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
        LOG.debug("crearCustomFile (E)");     
        return "";
    }


    /**
     * metodo para guardar la parametrización de la fecha de descuento
     * @param claveEpo
     * @param fechaOperacion
     * @param usuario
     * @param acuse
     * @throws NafinException
     */
    public void capturaFecha(String claveEpo,  String fechaOperacion, String usuario, String acuse) throws NafinException {
        LOG.info (" capturaFecha (e)  ");
        
        PreparedStatement ps = null;        
        AccesoDB con = new AccesoDB();
        StringBuilder qrySQL = new StringBuilder();
        List varBind = new ArrayList();
        boolean lbOK = true;

        try {

            con.conexionDB();

            qrySQL = new StringBuilder();
            varBind = new ArrayList();
            qrySQL.append(" insert into COM_DESC_AUTO_EPO " +
                                " ( IC_DESC_AUTO_EPO, IC_EPO, DF_DESCUENTO, DF_PARAMETIZADA ,  CG_USUARIO, CC_ACUSE   ) " +
                                " values( COM_DESC_AUTO_EPO_SEQ.nextval, ?,  to_date(?,'dd/mm/yyyy'), SYSDATE, ? , ? ) "); 
            varBind.add(claveEpo);
            varBind.add(fechaOperacion);
            varBind.add(usuario);
            varBind.add(acuse);

            LOG.debug(" qrySQL "+qrySQL.toString()+ " \n varBind  "+ varBind );            
            ps = con.queryPrecompilado(qrySQL.toString(), varBind);
            ps.executeUpdate();
            ps.close();


        } catch (Exception e) {
            lbOK = false;
            LOG.error("::(Exception) " + e);
            throw new NafinException("SIST0001");
        } finally {
            con.terminaTransaccion(lbOK);
            if (con.hayConexionAbierta())
                con.cierraConexionDB();
        }
        LOG.info (" capturaFecha (s)  ");        
    }

    /**
     * @param fecha
     * @param claveEpo
     * @return
     * @throws NafinException
     */
    public boolean getDiaHabil(String fecha, String claveEpo ) throws NafinException {
        LOG.info (" getDiaHabil (e)  ");
        
        AccesoDB con = new AccesoDB();       
        StringBuilder qrySQL = new StringBuilder();
        boolean bFechaInhabil = false;       
        List varBind = new ArrayList();
        try {

            con.conexionDB();

            String anio = fecha.substring(6, 10);
            
              qrySQL.append("  SELECT   "+                           
                           " cg_dia_inhabil ||'/"+ anio+"' AS clave   "+
                           " FROM comrel_dia_inhabil_x_epo d, comcat_epo e    "+         
                           " WHERE d.df_dia_inhabil IS NULL   "+
                           " AND d.ic_epo = e.ic_epo AND e.ic_epo = ?   "+            
                           " UNION  "+
                           " SELECT  TO_CHAR (df_dia_inhabil, 'dd/mm/yyyy') AS clave  "+
                           " FROM comcat_dia_inhabil         "+
                           " WHERE df_dia_inhabil IS NOT NULL  "+
                           " UNION   "+                
                           " SELECT  CG_DIA_INHABIL ||'/"+ anio+"' AS clave FROM COMCAT_DIA_INHABIL   "+
                           " WHERE CG_DIA_INHABIL IS NOT NULL "+
                           " UNION " +
                            " SELECT  TO_CHAR (df_dia_inhabil, 'dd/mm/yyyy') AS clave " +                             
                            " FROM comrel_dia_inhabil_x_epo d, comcat_epo e	" + 
                            " WHERE d.df_dia_inhabil IS NOT NULL " + 
                            " AND d.ic_epo = e.ic_epo " + 
                            " AND e.ic_epo = ? ");
            
            bFechaInhabil = false;
            varBind = new ArrayList();
            varBind.add(claveEpo);
            varBind.add(claveEpo);
            
            System.out.println ("qrySQL  >>>>>"+qrySQL.toString()+"   \n  varBind "+varBind);
            
            PreparedStatement  ps = con.queryPrecompilado(qrySQL.toString(), varBind);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String aux1 = (rs.getString(1)) == null ? "" : rs.getString(1).trim();                  
                if (aux1.equals(fecha)) {
                    bFechaInhabil = true;
                    break;
                }
            }
            rs.close();
            ps.close();
            

            if (!bFechaInhabil) {

                Calendar gcDiaFecha = new GregorianCalendar();
                java.util.Date fechaVencNvo = Comunes.parseDate(fecha);
              
                gcDiaFecha.setTime(fechaVencNvo);
                int iDiaSemana = gcDiaFecha.get(Calendar.DAY_OF_WEEK);

                if (iDiaSemana == 7) {
                    bFechaInhabil = true;
                }
                if (iDiaSemana == 1) {
                    bFechaInhabil = true;
                }
            }


        } catch (Exception e) {
            LOG.error("Exception " + e + "  Message" + e.getMessage());
            throw new NafinException("SIST0001");
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        LOG.info (" getDiaHabil (s)  ");
        return bFechaInhabil;
    }


    /**
	 Obtiene la lista de parámetros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones
     */
    public List getConditions() {
        return conditions;
    }

    /**
     * Obtiene el numero de pagina.
     * @return Cadena con el numero de pagina
     */
    public String getPaginaNo() {
        return paginaNo;
    }

    /**
     * Obtiene el offset de la pagina.
     * @return Cadena con el offset de pagina
     */
    public String getPaginaOffset() {
        return paginaOffset;
    }


    /*****************************************************
					 SETTERS
*******************************************************/

    /**
     * Establece el numero de pagina.
     * @param  newPaginaNo Cadena con el numero de pagina
     */
    public void setPaginaNo(String newPaginaNo) {
        paginaNo = newPaginaNo;
    }

    /**
     * Establece el offset de la pagina.
     * @param newPaginaOffset Cadena con el offset de pagina
     */
    public void setPaginaOffset(String paginaOffset) {
        this.paginaOffset = paginaOffset;
    }

    /**
     * @param idclaveEpo
     */
    public void setIdclaveEpo(String idclaveEpo) {
        this.idclaveEpo = idclaveEpo;
    }

    /**
     * @return
     */
    public String getIdclaveEpo() {
        return idclaveEpo;
    }
    
    /**
     * @param fechaDescuentoIni
     */
    public void setFechaDescuentoIni(String fechaDescuentoIni) {
        this.fechaDescuentoIni = fechaDescuentoIni;
    }

    /**
     * @return
     */
    public String getFechaDescuentoIni() {
        return fechaDescuentoIni;
    }

    /**
     * @param fechaDescuentoFin
     */
    public void setFechaDescuentoFin(String fechaDescuentoFin) {
        this.fechaDescuentoFin = fechaDescuentoFin;
    }

    /**
     * @return
     */
    public String getFechaDescuentoFin() {
        return fechaDescuentoFin;
    }


}
