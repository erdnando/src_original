package com.netro.cadenas;

import com.netro.xls.ComunesXLS;
import netropology.utilerias.*;
import java.util.Collection;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import org.apache.commons.logging.Log;
import java.util.*;     
import com.netro.pdf.ComunesPDF;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;  
import com.netro.exception.*;
import java.io.FileOutputStream;
import javax.naming.*;
import java.io.*;
import com.netro.dispersion.*;  

public class RepOpPymesDescAutomatico  {

  
//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(RepOpPymesDescAutomatico.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	
	
		 
	public RepOpPymesDescAutomatico() { }
	
	
	/**
	 * 
	 * @return 
	 * @param informacion
	 * @param registros
	 * @param tipo
	 * @param path
	 * @param request
	 */
	
	public String getArchivosConst(HttpServletRequest request, String path, String tipo, List registros, String informacion  ) {
		
		log.debug("crearCustomFile (E)");
		 
		 HttpSession session = request.getSession();	
		 CreaArchivo archivo = new CreaArchivo();
		 ComunesXLS xls = new ComunesXLS();;		
		 ComunesPDF pdfDoc = new ComunesPDF();		 
		 String nombreArchivo = "";  
	 
		try {	
			
			
			if(tipo.equals("PDF") ){
			
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
			
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
							
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
				
			}else  if(tipo.equals("XLS")) {
				
				 nombreArchivo = archivo.nombreArchivo()+".xls";
				 xls = new ComunesXLS(path+nombreArchivo);
			
			}
			  
			if(tipo.equals("XLS") && informacion.equals("Generar_Arch")  ) {
									
				  xls.setTabla(14);
				  xls.setCelda("", "celda01", ComunesXLS.CENTER, 4);
				  xls.setCelda("Descuento Autm�tico Habilitado", "celda01", ComunesXLS.CENTER, 5);
				  xls.setCelda("Descuento Autom�tico No Habilitado", "celda01", ComunesXLS.CENTER, 5);
		
				  xls.setCelda("EPO", "celda01", ComunesXLS.CENTER, 1);
				  xls.setCelda("IF", "celda01", ComunesXLS.CENTER, 1);
				  xls.setCelda("Convenio", "celda01", ComunesXLS.CENTER, 1);
				  xls.setCelda("Total de PyME's", "celda01", ComunesXLS.CENTER, 1);
				  xls.setCelda("No de PyME's con Desc. Autom�tico", "celda01", ComunesXLS.CENTER, 1);
				  xls.setCelda("No. Doctos. Publicados", "celda01", ComunesXLS.CENTER, 1);
				  xls.setCelda("Monto Publicado", "celda01", ComunesXLS.CENTER, 1);
				  xls.setCelda("No. Doctos. Operados", "celda01", ComunesXLS.CENTER, 1);
				  xls.setCelda("Monto Operado", "celda01", ComunesXLS.CENTER, 1);
				  xls.setCelda("No de PyME's sin Desc. Autom�tico", "celda01", ComunesXLS.CENTER, 1);
				  xls.setCelda("No. Doctos. Publicados", "celda01", ComunesXLS.CENTER, 1);
				  xls.setCelda("Monto Publicado", "celda01", ComunesXLS.CENTER, 1);
				  xls.setCelda("No. Doctos. Operados", "celda01", ComunesXLS.CENTER, 1);
				  xls.setCelda("Monto Operado", "celda01", ComunesXLS.CENTER, 1);
				
				  for(int i= 0; i < registros.size(); i++){
					 List operacionesDescuentoAutomatico = (List)registros.get(i);
		
					 xls.setCelda((String)operacionesDescuentoAutomatico.get(2), "formas", ComunesXLS.CENTER, 1);
					 xls.setCelda((String)operacionesDescuentoAutomatico.get(3), "formas", ComunesXLS.CENTER, 1);
					 xls.setCelda((String)operacionesDescuentoAutomatico.get(4), "formas", ComunesXLS.CENTER, 1);
					 xls.setCelda((String)operacionesDescuentoAutomatico.get(5), "formas", ComunesXLS.CENTER, 1);
					 xls.setCelda((String)operacionesDescuentoAutomatico.get(6), "formas", ComunesXLS.CENTER, 1);
					 xls.setCelda((String)operacionesDescuentoAutomatico.get(7), "formas", ComunesXLS.CENTER, 1);
					 xls.setCelda("$" + Comunes.formatoDecimal((String)operacionesDescuentoAutomatico.get(8), 2), "formas", ComunesXLS.CENTER, 1);
					 xls.setCelda((String)operacionesDescuentoAutomatico.get(9), "formas", ComunesXLS.CENTER, 1);
					 xls.setCelda("$" + Comunes.formatoDecimal((String)operacionesDescuentoAutomatico.get(10), 2), "formas", ComunesXLS.CENTER, 1);
					 xls.setCelda((String)operacionesDescuentoAutomatico.get(11), "formas", ComunesXLS.CENTER, 1);
					 xls.setCelda((String)operacionesDescuentoAutomatico.get(12), "formas", ComunesXLS.CENTER, 1);
					 xls.setCelda("$" + Comunes.formatoDecimal((String)operacionesDescuentoAutomatico.get(13), 2), "formas", ComunesXLS.CENTER, 1);
					 xls.setCelda((String)operacionesDescuentoAutomatico.get(14), "formas", ComunesXLS.CENTER, 1);
					 xls.setCelda("$" + Comunes.formatoDecimal((String)operacionesDescuentoAutomatico.get(15), 2), "formas", ComunesXLS.CENTER, 1);
				  }
				  
				  xls.cierraTabla();
				  xls.cierraXLS();
				  
			}else  if(tipo.equals("PDF") && informacion.equals("Generar_Arch")  ) {
			
				pdfDoc.setTable(14, 100 );   
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER, 4);				
				pdfDoc.setCell("Descuento Autm�tico Habilitado", "celda01",ComunesPDF.CENTER, 5);	
				pdfDoc.setCell("Descuento Autom�tico No Habilitado", "celda01", ComunesPDF.CENTER, 5);					  
				pdfDoc.setCell("EPO", "celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("IF", "celda01", ComunesPDF.CENTER);	
				pdfDoc.setCell("Convenio", "celda01", ComunesPDF.CENTER);	
				pdfDoc.setCell("Total de PyME's", "celda01", ComunesPDF.CENTER);	
				pdfDoc.setCell("No de PyME's con Desc. Autom�tico", "celda01", ComunesPDF.CENTER);	
				pdfDoc.setCell("No. Doctos. Publicados", "celda01", ComunesPDF.CENTER);	
				pdfDoc.setCell("Monto Publicado", "celda01", ComunesPDF.CENTER);	
				pdfDoc.setCell("No. Doctos. Operados", "celda01", ComunesPDF.CENTER);	
				pdfDoc.setCell("Monto Operado", "celda01", ComunesPDF.CENTER);	
				pdfDoc.setCell("No de PyME's sin Desc. Autom�tico", "celda01", ComunesPDF.CENTER);	
				pdfDoc.setCell("No. Doctos. Publicados", "celda01", ComunesPDF.CENTER);	
				pdfDoc.setCell("Monto Publicado", "celda01", ComunesPDF.CENTER);	
				pdfDoc.setCell("No. Doctos. Operados", "celda01", ComunesPDF.CENTER);	
				pdfDoc.setCell("Monto Operado", "celda01", ComunesPDF.CENTER);	
				  
			 
			 for(int i= 0; i < registros.size(); i++){
					 List operacionesDescuentoAutomatico = (List)registros.get(i);
		
					pdfDoc.setCell((String)operacionesDescuentoAutomatico.get(2), "formas", ComunesPDF.CENTER);	
					 pdfDoc.setCell((String)operacionesDescuentoAutomatico.get(3), "formas", ComunesPDF.CENTER);	
					 pdfDoc.setCell((String)operacionesDescuentoAutomatico.get(4), "formas", ComunesPDF.CENTER);	
					 pdfDoc.setCell((String)operacionesDescuentoAutomatico.get(5), "formas", ComunesPDF.CENTER);	
					 pdfDoc.setCell((String)operacionesDescuentoAutomatico.get(6), "formas", ComunesPDF.CENTER);	
					 pdfDoc.setCell((String)operacionesDescuentoAutomatico.get(7), "formas", ComunesPDF.CENTER);	
					 pdfDoc.setCell("$" + Comunes.formatoDecimal((String)operacionesDescuentoAutomatico.get(8), 2), "formas", ComunesPDF.CENTER);	
					 pdfDoc.setCell((String)operacionesDescuentoAutomatico.get(9), "formas",ComunesPDF.CENTER);	
					 pdfDoc.setCell("$" + Comunes.formatoDecimal((String)operacionesDescuentoAutomatico.get(10), 2), "formas", ComunesPDF.CENTER);	
					 pdfDoc.setCell((String)operacionesDescuentoAutomatico.get(11), "formas", ComunesPDF.CENTER);	
					 pdfDoc.setCell((String)operacionesDescuentoAutomatico.get(12), "formas", ComunesPDF.CENTER);	
					 pdfDoc.setCell("$" + Comunes.formatoDecimal((String)operacionesDescuentoAutomatico.get(13), 2), "formas",ComunesPDF.CENTER);	
					 pdfDoc.setCell((String)operacionesDescuentoAutomatico.get(14), "formas",ComunesPDF.CENTER);	
					 pdfDoc.setCell("$" + Comunes.formatoDecimal((String)operacionesDescuentoAutomatico.get(15), 2), "formas", ComunesPDF.CENTER);	
				  }
				  
			
				pdfDoc.addTable();
				pdfDoc.endDocument();	
			
			}else  if(tipo.equals("XLS") && informacion.equals("Generar_Arch_Det")  ) {			
				
										
				xls.setTabla(3);
				xls.setCelda("No. Nafin Electr�nico", "celda01", ComunesXLS.CENTER);
				xls.setCelda("Nombre de la PyME", "celda01", ComunesXLS.CENTER);
				xls.setCelda("RFC", "celda01", ComunesXLS.CENTER);
				
				for(int i= 0; i < registros.size(); i++){
					List reg = (List)registros.get(i);
					xls.setCelda(reg.get(0) , "formas", ComunesXLS.CENTER, 1);
					xls.setCelda( reg.get(1) , "formas", ComunesXLS.CENTER, 1);
					xls.setCelda(reg.get(2) , "formas", ComunesXLS.CENTER, 1);				
				}
				xls.cierraTabla();
				xls.cierraXLS();
			
			}else  if(tipo.equals("PDF") && informacion.equals("Generar_Arch_Det")  ) {	  
			
				pdfDoc.setTable(3, 50 );   
				pdfDoc.setCell("No. Nafin Electr�nico","celda01",ComunesPDF.CENTER);				
				pdfDoc.setCell("Nombre de la PyME", "celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell("RFC", "celda01", ComunesPDF.CENTER);		
			
				for(int i= 0; i < registros.size(); i++){
					List reg = (List)registros.get(i);
					pdfDoc.setCell( reg.get(0).toString() , "formas", ComunesPDF.CENTER);	
					pdfDoc.setCell( reg.get(1).toString() , "formas", ComunesPDF.CENTER);	
				   pdfDoc.setCell( reg.get(2).toString() , "formas", ComunesPDF.CENTER);				
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();	
			
			
			}
		
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
			

}