package com.netro.cadenas;

import java.io.Serializable;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.CreaArchivo;

public class Tablaimgtxt implements Serializable {

	String qry_exec="";
	AccesoDB con = null;
	boolean ok = true;
	
	//Constructor
	public Tablaimgtxt() {
	}
	
	//Para guardar los datos de la imagen con texto
	public void guardaImgTxt( String cadena, String titulo, String fec_ini, String fec_fin, String contenido, String imgn_destino, String arch_destino, String multiS ) {
		String tipo_pub = "2";
		String ic_publicacion = "";
		try {
			con = new AccesoDB();
			con.conexionDB();
			qry_exec = "select max(IC_PUBLICACION)+1 n_pub from com_publicacion";
			ResultSet rsSel = con.queryDB(qry_exec);
			if (rsSel.next())
				ic_publicacion = (rsSel.getString(1) == null) ? "1" : rsSel.getString(1);
			rsSel.close();
			
			qry_exec = "insert into com_publicacion (IC_PUBLICACION,IC_EPO,IC_TIPO_PUBLICACION,CG_NOMBRE,DF_INI_PUB,DF_FIN_PUB,TT_CONTENIDO,CI_IMAGEN,IN_COLUMNAS,CG_ARCHIVO) values (" + ic_publicacion + "," + cadena + "," + tipo_pub + ",'" + titulo + "',TO_DATE('" + fec_ini + "','DD/MM/YYYY'),TO_DATE('" + fec_fin + "','DD/MM/YYYY'),'" + contenido.replace('\'',' ') + "','" + imgn_destino + "',null,'" + arch_destino + "')";
			con.ejecutaSQL(qry_exec);
			//Para los valores seleccionados en el combo multiple
			StringTokenizer tokens=new StringTokenizer(multiS, ",");
			while(tokens.hasMoreTokens()){
				qry_exec = 
					"INSERT INTO comrel_publicacion_usuario "+
					"            (ic_publicacion, ic_nafin_electronico) "+
					"     VALUES("+ic_publicacion+", '"+tokens.nextToken()+"')";
				con.ejecutaSQL(qry_exec);
			} 
			
		} catch(Exception e){
			System.out.println("Exception: " + e.toString());
			e.printStackTrace();
			ok = false;
		} finally {
			con.terminaTransaccion(ok);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
	}
	
	//Para actualizar los datos de la imagen con texto
	public void actualizaDatosImg(String titulo, String fec_ini, String fec_fin, String contenido, String arch_destino, String imgn_destino, String cadena, String publicacion){
		con = new AccesoDB();
		try{
			con.conexionDB();
			qry_exec = "update com_publicacion set cg_nombre = '" + titulo + "', df_ini_pub = TO_DATE('" + fec_ini + "','DD/MM/YYYY'), df_fin_pub = TO_DATE('" + fec_fin + "','DD/MM/YYYY'), tt_contenido = '" + contenido.replace('\'',' ') + "', cg_archivo = '" + arch_destino + "', ci_imagen = '" + imgn_destino + "' where ic_epo = " + cadena + " and ic_publicacion = " + publicacion;
			con.ejecutaSQL(qry_exec);
		} catch(Exception e){
			System.out.println("Exception: " + e.toString());
			e.printStackTrace();
		} finally {
			con.terminaTransaccion(ok);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
	}
	
	/*********ESTO SE AGREGO EL DIA 11-JULIO-2013*********/
	
	//Para crear la tabla con campos fijos
	public String creaTablaFijos(String accion, int miNumPublicacion,String [] nombre_campo, String [] tipo_campo, String [] long_campo_Fijo){
		con = new AccesoDB();
		String 	lsQryTabla     = "",
				lsDatoTmp      = "";
		try	{
			con.conexionDB();
			int 	liContadorLong = 0,
				liIndiceCampo  = 1;
		
			if (nombre_campo != null)	{
				if ( accion.equals("Modifica") )	{
					lsQryTabla = "ALTER TABLE tabla_" + miNumPublicacion + " ADD ( ";
					liIndiceCampo = obtenNumMaxCampos(miNumPublicacion, "F") - nombre_campo.length + 1; //Corrección x GEAG
				} else {
					lsQryTabla = "CREATE TABLE tabla_" + miNumPublicacion + "( "	+
							"	id NUMBER NOT NULL PRIMARY KEY, " +
							"	campo0 VARCHAR2(20) NOT NULL , ";
				}
			
				for (int i=0; i<nombre_campo.length; i++)	{
					lsDatoTmp = "campo" + liIndiceCampo;
				
					if ( tipo_campo[i].equals("alfanumerico") ) {
								lsQryTabla += lsDatoTmp + " VARCHAR2(" + long_campo_Fijo[liContadorLong] + ") ";
					} else if	( tipo_campo[i].equals("numerico") )	{	/////////// *********   NUMERICO
						int liLongTot = Integer.parseInt(long_campo_Fijo[liContadorLong]);
						int liLogDeci = Integer.parseInt(long_campo_Fijo[++liContadorLong]);
						liLongTot += liLogDeci;
						lsQryTabla += lsDatoTmp + " NUMBER(" + liLongTot + ", "+ liLogDeci +"  ) ";
					} else if	( tipo_campo[i].equals("fecha") ||  tipo_campo[i].equals("hora") ) {
						lsQryTabla += lsDatoTmp + " DATE ";
					} else if	( tipo_campo[i].equals("seleccion") ) {
						lsQryTabla += lsDatoTmp + " CHAR(1) ";
					}
				
					if ( nombre_campo.length-1 != i)	lsQryTabla += " , ";
				
					liContadorLong++;
					liIndiceCampo++;
				}
				lsQryTabla += ")";
			
				System.out.print("\t\n\n" + lsQryTabla);			
				//lbNoError = BeanTablasDinamicas.modificaPublicaciones( lsQryTabla ); Esto va en el .data
			}
		} catch(Exception e){
			System.out.println("Exception: " + e.toString());
			e.printStackTrace();
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();		
		}
		return lsQryTabla;
	}
	
	//Para crear la tabla con campos editables
	public String creaTablaEditables(String accion, int miNumPublicacion,String [] nombre_campo_edi, String [] tipo_campo_edi, String [] long_campo_Editable, boolean mbExisteTabEdit){
		con = new AccesoDB();
		String 	lsQryTabla     = "",
				lsDatoTmp      = "";
		try	{
			con.conexionDB();
			int 	liContadorLong = 0,
				liIndiceCampo  = 1;
		
			if ( nombre_campo_edi != null )	{
				String lsNombreCampoE = "";
				liIndiceCampo = 0;
			
				if ( accion.equals("Modifica")  &&   mbExisteTabEdit  )	{
					lsQryTabla    = "ALTER TABLE tabla_edit_" + miNumPublicacion + " ADD ( ";
					liIndiceCampo = obtenNumMaxCampos(miNumPublicacion, "E") - nombre_campo_edi.length; //Corrección x GEAG
				} else {
					lsQryTabla = "CREATE TABLE tabla_edit_" + miNumPublicacion + "( "	+
						"	id NUMBER NOT NULL, " +
						"	ic_pyme number(8), ";
				}
			
				liContadorLong = 0;
				for (int i=0; i<nombre_campo_edi.length; i++)	{
					lsNombreCampoE = "campoe" + liIndiceCampo;
				
					if ( tipo_campo_edi[i].equals("alfanumerico") ) {
						lsQryTabla += lsNombreCampoE + " VARCHAR2(" + long_campo_Editable[liContadorLong] + ") ";
					} else if	( tipo_campo_edi[i].equals("numerico") )	{	/////////// *********   NUMERICO
						int liLongTot = Integer.parseInt(long_campo_Editable[liContadorLong]);
						int liLogDeci = Integer.parseInt(long_campo_Editable[++liContadorLong]);
						liLongTot += liLogDeci;
						lsQryTabla += lsNombreCampoE + " NUMBER(" + liLongTot + ", "+ liLogDeci +"  ) ";
					} else if	( tipo_campo_edi[i].equals("fecha") ||  tipo_campo_edi[i].equals("hora") ) {
						lsQryTabla += lsNombreCampoE + " DATE ";
					} else if	( tipo_campo_edi[i].equals("seleccion") ) {
						lsQryTabla += lsNombreCampoE + " CHAR(1) ";
					}
				
					if ( nombre_campo_edi.length-1 != i)	lsQryTabla += " , ";
				
					liContadorLong++;
					liIndiceCampo++;
				} //fin for
					
				if ( accion.equals("Modifica") ) {
					lsQryTabla += " ) ";
				} else {
					lsQryTabla += ", PRIMARY KEY (id, ic_pyme)  ) ";
				}
			
				//lbNoError = BeanTablasDinamicas.modificaPublicaciones( lsQryTabla ); Esto va en el .data
			}//if ( nombre_campo_edi != null )	{
		} catch(Exception e){
			System.out.println("Exception: " + e.toString());
			e.printStackTrace();
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		return lsQryTabla;
	}
	
	//Para crear la tabla con campos de detalle
	public String creaTablaDetalles(String accion, int miNumPublicacion,String [] nombre_campo_det, String [] tipo_campo_det, String [] long_campo_Detalle, boolean mbExisteTabDet){
		con = new AccesoDB();
		String 	lsQryTabla     = "",
				lsDatoTmp      = "";
		try	{
			con.conexionDB();
			int 	liContadorLong = 0,
				liIndiceCampo  = 1;
		
			if ( nombre_campo_det != null )	{
				String lsNombreCampoD = "";
				liIndiceCampo = 0;
				
				if ( accion.equals("Modifica")   &&  mbExisteTabDet )	{
					lsQryTabla = "ALTER TABLE tabla_d_" + miNumPublicacion + " ADD ( ";
					liIndiceCampo = obtenNumMaxCampos(miNumPublicacion, "D") - nombre_campo_det.length; //Corrección x GEAG
				} else {
					lsQryTabla = "CREATE TABLE tabla_d_" + miNumPublicacion + "( "	+
						"	id   NUMBER NOT NULL, " + 
						"	id_d NUMBER NOT NULL, ";
				}
				liContadorLong = 0;
				for (int i=0; i<nombre_campo_det.length; i++)	{
					lsNombreCampoD = "campod" + liIndiceCampo;
					
					if ( tipo_campo_det[i].equals("alfanumerico") ) {
						lsQryTabla += lsNombreCampoD + " VARCHAR2(" + long_campo_Detalle[liContadorLong] + ") ";
					} else if	( tipo_campo_det[i].equals("numerico") )	{	/////////// *********   NUMERICO
						int liLongTot = Integer.parseInt(long_campo_Detalle[liContadorLong]);
						int liLogDeci = Integer.parseInt(long_campo_Detalle[++liContadorLong]);
						liLongTot += liLogDeci;
						lsQryTabla += lsNombreCampoD + " NUMBER(" + liLongTot + ", "+ liLogDeci +"  ) ";
					} else if	( tipo_campo_det[i].equals("fecha") ||  tipo_campo_det[i].equals("hora") ) {
						lsQryTabla += lsNombreCampoD + " DATE ";
					} else if	( tipo_campo_det[i].equals("seleccion") ) {
						lsQryTabla += lsNombreCampoD + " CHAR(1) ";
					}
				
					if ( nombre_campo_det.length-1 != i)	lsQryTabla += " , ";
					liContadorLong++;
					liIndiceCampo++;
				} //fin for
					
				if ( accion.equals("Modifica") ) {
					lsQryTabla += 	" )";
				} else {
					lsQryTabla += 	", PRIMARY KEY (id, id_d) )";
				}
				
				//lbNoError = BeanTablasDinamicas.modificaPublicaciones( lsQryTabla ); Esto va en el .data
			} //if ( nombre_campo_det != null )	{
		} catch(Exception e){
			System.out.println("Exception: " + e.toString());
			e.printStackTrace();
		} finally {
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		return lsQryTabla;
	}
	
	/*****************************************************************************
		*	SE OBTIENEN DATOS PARA COPIA DE ESTRUCTURA DE TABLA
	******************************************************************************/
	
	public List obtenFijos(String copiar){
		List lista = new ArrayList();
		Vector 	lVecFijos  = null;
		ResultSet lCurEstruc= null;
		String lsQryEstructura = "SELECT cg_nombre_atrib, cg_tipo_dato, ig_long_atrib, ig_long_atrib_d, cs_total, ig_ordenamiento " +
								 "FROM com_tabla_pub_atrib WHERE cs_tipo_atrib = 'F' AND ic_publicacion = " + copiar + " " +
								 "ORDER BY ic_atributo";
		con = new AccesoDB();
		try{
		con.conexionDB();
		lCurEstruc = con.queryDB( lsQryEstructura );	
		while ( lCurEstruc.next() )	{
				lVecFijos = new Vector();
				lVecFijos.addElement( lCurEstruc.getString("cg_nombre_atrib") );
				lVecFijos.addElement( lCurEstruc.getString("cg_tipo_dato") );
				
				lVecFijos.addElement( lCurEstruc.getString("ig_long_atrib") );
				if ( lCurEstruc.getString("cg_tipo_dato").equals("numerico") )
					lVecFijos.addElement( lCurEstruc.getString("ig_long_atrib_d") );
				
				if ( lCurEstruc.getString("cs_total").equals("S") )
					lVecFijos.addElement( "1");
				else
					lVecFijos.addElement( "");
				
				lVecFijos.addElement( lCurEstruc.getString("ig_ordenamiento") );
				lista.add(lVecFijos);
			}
			lCurEstruc.close();
			
		}catch(Exception err)	{
			System.out.print( err.getMessage() );
		}
		finally	{
			if ( con != null)	con.cierraConexionDB();
		}
		return lista;
	}
	
	public List obtenEditables(String copiar){
		List lista = new ArrayList();
		Vector 	lVecEditables  = null;
		ResultSet lCurEstruc= null;
		String lsQryEstructEdi = "SELECT cg_nombre_atrib, cg_tipo_dato, ig_long_atrib, ig_long_atrib_d, cs_total, ig_ordenamiento " +
								 "FROM com_tabla_pub_atrib WHERE cs_tipo_atrib = 'E' AND ic_publicacion = " + copiar + " " +
								 "ORDER BY ic_atributo";
		con = new AccesoDB();		
		try{
		con.conexionDB();
		lCurEstruc = con.queryDB( lsQryEstructEdi );	
		while ( lCurEstruc.next() )	{
				lVecEditables = new Vector();
				lVecEditables.addElement( lCurEstruc.getString("cg_nombre_atrib") );
				lVecEditables.addElement( lCurEstruc.getString("cg_tipo_dato") );
				
				lVecEditables.addElement( lCurEstruc.getString("ig_long_atrib") );
				if ( lCurEstruc.getString("cg_tipo_dato").equals("numerico") )
					lVecEditables.addElement( lCurEstruc.getString("ig_long_atrib_d") );
				
				if ( lCurEstruc.getString("cs_total").equals("S") )
					lVecEditables.addElement( "1");
				else
					lVecEditables.addElement( "");
				
				lVecEditables.addElement( lCurEstruc.getString("ig_ordenamiento") );
				lista.add(lVecEditables);
		}
		}catch(Exception err)	{
			System.out.print( err.getMessage() );
		}
		finally	{
			if ( con != null)	con.cierraConexionDB();
		}
		return lista;
	}
	
	public List obtenDetalle(String copiar){
		List lista = new ArrayList();
		Vector 	lVecDetalle  = null;
		ResultSet lCurEstruc= null;
		String lsQryEstructDet = "SELECT cg_nombre_atrib, cg_tipo_dato, ig_long_atrib, ig_long_atrib_d, cs_total, ig_ordenamiento " +
								 "FROM com_tabla_pub_atrib_d WHERE ic_publicacion = " + copiar + " " +
								 "ORDER BY ic_atributo_d";
		con = new AccesoDB();		
		try{
			con.conexionDB();
			lCurEstruc = con.queryDB( lsQryEstructDet );	
			while ( lCurEstruc.next() )	{
				lVecDetalle = new Vector();
				lVecDetalle.addElement( lCurEstruc.getString("cg_nombre_atrib") );
				lVecDetalle.addElement( lCurEstruc.getString("cg_tipo_dato") );
				
				lVecDetalle.addElement( lCurEstruc.getString("ig_long_atrib") );
				if ( lCurEstruc.getString("cg_tipo_dato").equals("numerico") )
					lVecDetalle.addElement( lCurEstruc.getString("ig_long_atrib_d") );
				
				if ( lCurEstruc.getString("cs_total").equals("S") )
					lVecDetalle.addElement( "1");
				else
					lVecDetalle.addElement( "");
				
				lVecDetalle.addElement( lCurEstruc.getString("ig_ordenamiento") );
				lista.add(lVecDetalle);
			}
			lCurEstruc.close();
		}catch(Exception err)	{
			System.out.print( err.getMessage() );
		}
		finally	{
			if ( con != null)	con.cierraConexionDB();
		}
		return lista;
	}
	
	public Vector traeFechas(String copiar){
		ResultSet lCurEstruc= null;
		Vector 	lVecFechas  = new Vector();
		String lsQryDatosGen   = "SELECT TO_CHAR(DF_INI_PUB, 'dd/mm/yyyy') AS fch_ini, TO_CHAR(DF_FIN_PUB, 'dd/mm/yyyy') AS fch_fin " +
								  "FROM com_publicacion WHERE ic_publicacion = " + copiar;
		con = new AccesoDB();	
		try{
			con.conexionDB();
			lCurEstruc = con.queryDB( lsQryDatosGen );
			if (lCurEstruc.next())	{
				lVecFechas.addElement( lCurEstruc.getString("fch_ini"));
				lVecFechas.addElement( lCurEstruc.getString("fch_fin"));
			}
			lCurEstruc.close();
		} catch(Exception err)	{
			System.out.print( err.getMessage() );
		}
		finally {
			if ( con != null)	con.cierraConexionDB();
		}
		return lVecFechas;
	}
	
	public String traeUsuarios(String copiar){
		String usr = "";
		ResultSet lCurEstruc= null;
		String lsQryUsuarios   = "SELECT ic_nafin_electronico FROM comrel_publicacion_usuario WHERE ic_publicacion = " + copiar;
		con = new AccesoDB();	
		try{
			con.conexionDB();
			lCurEstruc = con.queryDB( lsQryUsuarios );
			while (lCurEstruc.next())	{
				usr += lCurEstruc.getString("ic_nafin_electronico") + "|";
			}
			lCurEstruc.close();
		}catch(Exception err)	{
			System.out.print( err.getMessage() );
		}
		finally {
			if ( con != null)	con.cierraConexionDB();
		}
		return usr;
	}

	/*************************************************
	*	OBTENIENDO NUMERO DE PUBLICACION
	***************************************************/

	public int obtenNumPublicacionSig()	{
		String 	lsQryNumMaxPub = "SELECT NVL(MAX (ic_publicacion),0) + 1 as num_pub FROM com_publicacion";
		int		liNumPublicacion = 0;
	
		con = new AccesoDB();
		try	{
			con.conexionDB();
			ResultSet lCurNumMaxPub = con.queryDB( lsQryNumMaxPub );
			while ( lCurNumMaxPub.next() )
				liNumPublicacion = lCurNumMaxPub.getInt("num_pub");
			lCurNumMaxPub.close();
		}
		catch(Exception err)	{
			System.out.print( err.getMessage() );
		}
		finally	{
			if ( con != null)	con.cierraConexionDB();
		}
	
		return liNumPublicacion;
	}
	
	/******************************************************
	*	OBTIENE EL NUMERO MAXIMO DE CAMPOS
	***************************************************/

	public int obtenNumMaxCampos(int liNumPublicacion, String lsCaracterTabla)	{
		String 	lsQry = "";
		int		liNumCampo = 0;
		
		if ( lsCaracterTabla.equals("D") )
			lsQry = "SELECT COUNT(ic_atributo_d) num_campo FROM com_tabla_pub_atrib_d " +
					"WHERE ic_publicacion = " + liNumPublicacion;
		else
			lsQry = "SELECT COUNT(ic_atributo) num_campo FROM com_tabla_pub_atrib " +
					"WHERE cs_tipo_atrib = '"+lsCaracterTabla+"' AND ic_publicacion = " + liNumPublicacion;
		
		con = new AccesoDB();
		try	{
			con.conexionDB();
			ResultSet lCursor = con.queryDB( lsQry );
			if ( lCursor.next() )
				liNumCampo = lCursor.getInt("num_campo");
			lCursor.close();
		}
		catch(Exception err)	{
			System.out.print( err.getMessage() );
		}
		finally	{
			if ( con != null)	con.cierraConexionDB();
		}
	
		return liNumCampo;
	}
	
	/*************************************************
	*	OBTIENE NUMERO DE CAMPOS YA GRABADOS  
	***************************************************/
	
	public int obtenNumeroCampos(int liNumPublicacion, String lsCaracterCampo)	{
		int  	liNumCampos = 0;
		String 	lsQry = "";
		
		if ( lsCaracterCampo.equals("E") )	{
			lsQry = "SELECT COUNT(ic_atributo) AS num_campos FROM com_tabla_pub_atrib " +
					"WHERE cs_tipo_atrib = 'E' AND ic_publicacion = " + liNumPublicacion;
		}
		else if ( lsCaracterCampo.equals("D") )	{
			lsQry = "SELECT COUNT(ic_atributo_d) AS num_campos FROM com_tabla_pub_atrib_d " +
					"WHERE ic_publicacion = " + liNumPublicacion;
		}
		
		AccesoDB lobjConexion = new AccesoDB();
		try	{
			lobjConexion.conexionDB();
			ResultSet lCurNomPub = lobjConexion.queryDB( lsQry );
			while ( lCurNomPub.next() )
				liNumCampos = lCurNomPub.getInt("num_campos");
			lCurNomPub.close();
		}
		catch(Exception err)	{
			System.out.print( err.getMessage() );
		}
		finally	{
			if ( lobjConexion != null)	lobjConexion.cierraConexionDB();
		}
		
		return liNumCampos;
	}
	
	/*****************************************************************************
		*	VERIFICA QUE NO SE REPITA EL NOMBRE DE LA TABLA CON RESPECTO A LA EPO
	******************************************************************************/
		
		public boolean tablaRepetida(String lsNombreTabla, String lsNumEpo)	{
			
			String 	lsQryNombreTabla= "SELECT cg_nombre FROM com_publicacion " +
									  "WHERE ic_epo = "+lsNumEpo+" AND cg_nombre = '"+lsNombreTabla+"'";
			boolean lbRepetido = false;
			
			AccesoDB lobjConexion = new AccesoDB();
			try	{
				lobjConexion.conexionDB();
				ResultSet lCursor = lobjConexion.queryDB( lsQryNombreTabla );
				if (lCursor.next())	lbRepetido = true;
				lCursor.close();
			}
			catch(Exception err)	{
				System.out.print( err.getMessage() );
			}
			finally	{
				if ( lobjConexion != null)	lobjConexion.cierraConexionDB();
			}
			
			return lbRepetido;
		}
		
		/*****************************************************************************
		*	CREA ARCHIVOS CSV
		******************************************************************************/
		public String crearCSV(HttpServletRequest request, List lDatos, String path, String tipo) {
			String nombreArchivo = "";
			StringBuffer 	contenidoArchivo 	= new StringBuffer();
			CreaArchivo 	archivo 			= new CreaArchivo();
			try {
	
				int registros = 0;
				
				for(int i=0; i<lDatos.size(); i++){
					List aux=(List)lDatos.get(i);
					for(int j=0; j<aux.size(); j++){
						contenidoArchivo.append( aux.get(j) +",");
					}
					contenidoArchivo.append("\n");
					registros++;
				}
				if(registros >= 1){
					if(!archivo.make(contenidoArchivo.toString(),path, ".csv")){
						nombreArchivo="ERROR";
					}else{
						nombreArchivo = archivo.nombre;
					}
				}
				
			}catch (Throwable e) {
					throw new AppException("Error al generar el archivo", e);
			}finally {
			
			}

			return nombreArchivo;
		}
	
}