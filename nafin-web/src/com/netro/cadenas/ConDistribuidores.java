package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConDistribuidores implements IQueryGeneratorRegExtJS {

	private final static Log log = ServiceLocator.getInstance().getLog(ConDistribuidores.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	
	private String numElectronico;
	private String nombre;
	private String seleccionEstado;
	private String claveRFC;
	private String seleccionCadenaProductiva;
	private String numDistribuidor;
	private String numSIRAC;
	private String modPorPropietario;
	
	private String claveEpo;
	private String clavePyme;
	private String tipoPersona;
	
	private String clavePais;
	private String claveEstado;
	private String claveMunicipio;
	
	private String claveSector;
	private String claveSubSector;
	private String claveRama;
	
	private String tipoCredito;
	

	public ConDistribuidores() {
	}

	/**
	 * 
	 * @return 
	 */
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	/**
	 * 
	 * @return 
	 */
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		qrySentencia.append("  SELECT /*+  LEADING(t2) index(t2 IN_COMCAT_PYME_09_NUK) USE_NL (t6 t2 t8 t1 t7 t5 t3) */	"+
								"		 t1.ic_pyme cliente	," +
								"		 t5.ic_epo epo	" +
							/*	"			 DECODE (t5.cs_habilitado,	"+
								"						'N', '<font color=\"Blue\">*</font>',	"+
								"						'S', ' '	"+
								"					  )	"+
								"		 || ' '	"+
								"		 || DECODE (t2.cs_tipo_persona,	"+
								"						'F', t2.cg_nombre || ' ' || t2.cg_appat || ' '	"+
								"						 || t2.cg_apmat,	"+
								"						t2.cg_razon_social	"+
								"					  ) AS razonsocial,	"+
								"		 t1.cg_calle calle, t1.cg_numero_ext numext, t1.cg_numero_int numint,	"+
								"		 t1.cg_colonia colonia, t3.cd_nombre estado,	"+
								"		 t1.cg_telefono1 AS telefono, t5.cg_pyme_epo_interno AS numpd,	"+
								"		 t6.ic_nafin_electronico AS elect, t2.cs_tipo_persona,	"+
								"		 t7.cg_razon_social, t7.ic_epo, t2.in_numero_sirac,	"+
								"		 t8.cd_version_convenio, TO_CHAR (t2.df_version_convenio, 'DD/MM/YYYY') AS FECHA_CONSULTA_CONVENIO,	"+
								"		 t5.cg_num_distribuidor AS num_distribuidor, t2.cg_rfc rfc,	"+
								"		 t2.cs_convenio_unico convenio_unico	"+*/
								"  FROM comrel_nafin t6,	"+
								"		 comcat_pyme t2,	"+
								"		 comcat_version_convenio t8,	"+
								"		 com_domicilio t1,	"+
								"		 comcat_estado t3,	"+
								"		 comrel_pyme_epo t5,	"+
								"		 comcat_epo t7	"+
								"  WHERE t5.ic_pyme = t2.ic_pyme		"+
								"		AND t1.ic_pyme = t2.ic_pyme	"+
								"		AND t2.ic_pyme = t6.ic_epo_pyme_if	"+
								"		AND t1.ic_estado = t3.ic_estado	"+
								"		AND t5.ic_epo = t7.ic_epo	"+
								"		AND t2.ic_version_convenio = t8.ic_version_convenio(+)	"+
								"		AND t2.ic_tipo_cliente IN (?, 4)	"+//Variable de tipo de usuario
								"		AND t6.cg_tipo = 'P'	"+
								"		AND t5.cg_num_distribuidor IS NOT NULL	"+
								"		AND (t2.cs_invalido IS NULL OR t2.cs_invalido != 'S')	"+
								"		AND t2.cs_tipo_afilia_distri IN ('B', 'C')	"+
								"		AND t1.cs_fiscal = 'S'	");
						conditions.add("2");		
								
			if (!numElectronico.equals("")){
				qrySentencia.append(" AND t6.ic_nafin_electronico = ? ");
				conditions.add(numElectronico);
			}
			if(!nombre.equals("")){
				qrySentencia.append("	AND (   (UPPER (TRIM (t2.cg_razon_social)) LIKE UPPER (?))	"+
										"			OR (UPPER (TRIM (t2.cg_nombre || ' ' || t2.cg_appat || ' '	|| t2.cg_apmat	)	) LIKE UPPER (?))	"+
										"		)"
										) ;
				conditions.add(nombre+"%");
				conditions.add(nombre+"%");
			}
			if(!seleccionEstado.equals("")){
				qrySentencia.append(" AND t1.ic_estado = ? ");
				conditions.add(seleccionEstado);
			}	
			if(!claveRFC.equals("")){
				qrySentencia.append(" AND t2.cg_rfc = ? ");
				conditions.add(claveRFC);
			}	
			if(!seleccionCadenaProductiva.equals("")){
				qrySentencia.append(" AND t5.ic_epo = ? ");
				conditions.add(seleccionCadenaProductiva);
			}	
			if(!numDistribuidor.equals("")){
				qrySentencia.append(" AND t5.cg_num_distribuidor  = ? ");
				conditions.add(numDistribuidor);
			}
			if(!numSIRAC.equals("")){
				qrySentencia.append(" AND t2.in_numero_sirac = ? ");
				conditions.add(numSIRAC);
			}
	
			if(modPorPropietario.equals("S")){
				qrySentencia.append(" AND t2.cs_datos_pers_modif = ? ");
				conditions.add(modPorPropietario);
			}		

		log.info("getDocumentQuery_qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}

	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		
		qrySentencia.append("  SELECT /*+  LEADING(t2) index(t2 IN_COMCAT_PYME_09_NUK) USE_NL (t6 t2 t8 t1 t7 t5 t3) */	"+
								"		 t1.ic_pyme cliente,	" +
								"		 t2.IC_TIPO_CLIENTE, 	"+//MOD
								"			 DECODE (t5.cs_habilitado,	"+
								"						'N', '<font color=\"Blue\">*</font>',	"+
								"						'S', ' '	"+
								"					  )	"+
								"		 || ' '	"+
								"		 || DECODE (t2.cs_tipo_persona,	"+
								"						'F', t2.cg_nombre || ' ' || t2.cg_appat || ' '	"+
								"						 || t2.cg_apmat,	"+
								"						t2.cg_razon_social	"+
								"					  ) AS razonsocial,	"+
								"		 t1.cg_calle calle, t1.cg_numero_ext numext, t1.cg_numero_int numint,	"+
								"		 t1.cg_colonia colonia, t3.cd_nombre estado,	"+
								"		 t1.cg_telefono1 AS telefono, t5.cg_pyme_epo_interno AS numpd,	"+
								"		 t6.ic_nafin_electronico AS elect, t2.cs_tipo_persona,	"+
								"		 t7.cg_razon_social, t7.ic_epo, t2.in_numero_sirac,	"+
								"		 t8.cd_version_convenio, TO_CHAR (t2.df_version_convenio, 'DD/MM/YYYY')AS FECHA_CONSULTA_CONVENIO,	"+
								"		 t5.cg_num_distribuidor AS num_distribuidor, t2.cg_rfc rfc,	"+
								"		 t2.cs_convenio_unico convenio_unico, t5.CS_ACEPTACION "+
								//"	,t2.cs_opera_fideicomiso AS OPERA_FIDEICOMISO	"+
								"  FROM comrel_nafin t6,	"+
								"		 comcat_pyme t2,	"+
								"		 comcat_version_convenio t8,	"+
								"		 com_domicilio t1,	"+
								"		 comcat_estado t3,	"+
								"		 comrel_pyme_epo t5,	"+
								"		 comcat_epo t7	"+
								"  WHERE t5.ic_pyme = t2.ic_pyme		"+
								"		AND t1.ic_pyme = t2.ic_pyme	"+
								"		AND t2.ic_pyme = t6.ic_epo_pyme_if	"+
								"		AND t1.ic_estado = t3.ic_estado	"+
								"		AND t5.ic_epo = t7.ic_epo	"+
								"		AND t2.ic_version_convenio = t8.ic_version_convenio(+)	"+
								"		AND t2.ic_tipo_cliente IN (?, 4)	"+//Variable de tipo de usuario
								"		AND t6.cg_tipo = 'P'	"+
								"		AND t5.cg_num_distribuidor IS NOT NULL	"+
								"		AND (t2.cs_invalido IS NULL OR t2.cs_invalido != 'S')	"+
								"		AND t2.cs_tipo_afilia_distri IN ('B', 'C')	"+
								"		AND t1.cs_fiscal = 'S'	");
								
				conditions.add("2");///////////////////////////////Hacer un metodo para determinar el tipo de cliente				
								
			if (!numElectronico.equals("")){
				qrySentencia.append(" AND t6.ic_nafin_electronico = ? ");
				conditions.add(numElectronico);
			}
			if(!nombre.equals("")){
				qrySentencia.append("	AND (   (UPPER (TRIM (t2.cg_razon_social)) LIKE UPPER (?))	"+
										"			OR (UPPER (TRIM (t2.cg_nombre || ' ' || t2.cg_appat || ' '	|| t2.cg_apmat	)	) LIKE UPPER (?))	"+
										"		)"
										) ;
				conditions.add(nombre+"%");
				conditions.add(nombre+"%");
			}
			if(!seleccionEstado.equals("")){
				qrySentencia.append(" AND t1.ic_estado = ? ");
				conditions.add(seleccionEstado);
			}	
			if(!claveRFC.equals("")){
				qrySentencia.append(" AND t2.cg_rfc = ? ");
				conditions.add(claveRFC);
			}	
			if(!seleccionCadenaProductiva.equals("")){
				qrySentencia.append(" AND t5.ic_epo = ? ");
				conditions.add(seleccionCadenaProductiva);
			}	
			if(!numDistribuidor.equals("")){
				qrySentencia.append(" AND t5.cg_num_distribuidor  = ? ");
				conditions.add(numDistribuidor);
			}
			if(!numSIRAC.equals("")){
				qrySentencia.append(" AND t2.in_numero_sirac = ? ");
				conditions.add(numSIRAC);
			}
	
			if(modPorPropietario.equals("S")){
				qrySentencia.append(" AND t2.cs_datos_pers_modif = ? ");
				conditions.add(modPorPropietario);
			}					
			
			qrySentencia.append(" AND (");
			
			for (int i = 0; i < pageIds.size(); i++) { 
				List lItem = (ArrayList)pageIds.get(i);
				
				if(i > 0){qrySentencia.append("  OR  ");}
				qrySentencia.append(" t1.IC_PYME =  ?  AND     t5.IC_EPO =  ?   " );
				conditions.add(new Long(lItem.get(0).toString()));
				conditions.add(new Long(lItem.get(1).toString()));
			}
			
			qrySentencia.append(" ) ");
			/*for(int i=0;i<pageIds.size();i++) {
				List lItem = (List)pageIds.get(i);
				if(i==0) {
					condicion.append(" AND t1.ic_pyme in (");
				}
				condicion.append("?");
				conditions.add(new Long(lItem.get(0).toString()));
				if(i!=(pageIds.size()-1)) {
					condicion.append(",");
				} else {
					condicion.append(" ) ");
				}			
			}*/
			qrySentencia.append(condicion.toString());
		
		log.info("getDocumentSummaryQueryForIds_qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();  
	qrySentencia.append("  SELECT /*+  LEADING(t2) index(t2 IN_COMCAT_PYME_09_NUK) USE_NL (t6 t2 t8 t1 t7 t5 t3) */	"+
								"		 t1.ic_pyme cliente,	" +
								"			 DECODE (t5.cs_habilitado,	"+
								"						'N', '<font color=\"Blue\">*</font>',	"+
								"						'S', ' '	"+
								"					  )	"+
								"		 || ' '	"+
								"		 || DECODE (t2.cs_tipo_persona,	"+
								"						'F', t2.cg_nombre || ' ' || t2.cg_appat || ' '	"+
								"						 || t2.cg_apmat,	"+
								"						t2.cg_razon_social	"+
								"					  ) AS razonsocial,	"+
								"		 t1.cg_calle calle, t1.cg_numero_ext numext, t1.cg_numero_int numint,	"+
								"		 t1.cg_colonia colonia, t3.cd_nombre estado,	"+
								"		 t1.cg_telefono1 AS telefono, t5.cg_pyme_epo_interno AS numpd,	"+
								"		 t6.ic_nafin_electronico AS elect, t2.cs_tipo_persona,	"+
								"		 t7.cg_razon_social, t7.ic_epo, t2.in_numero_sirac,	"+
								"		 t8.cd_version_convenio, TO_CHAR (t2.df_version_convenio, 'DD/MM/YYYY') AS FECHA_CONSULTA_CONVENIO,	"+
								"		 t5.cg_num_distribuidor AS num_distribuidor, t2.cg_rfc rfc,	"+
								"		 t2.cs_convenio_unico convenio_unico "+
								//"	,t2.cs_opera_fideicomiso AS OPERA_FIDEICOMISO 	"+//Este campo sera util para el fodea 19
								"  FROM comrel_nafin t6,	"+
								"		 comcat_pyme t2,	"+
								"		 comcat_version_convenio t8,	"+
								"		 com_domicilio t1,	"+
								"		 comcat_estado t3,	"+
								"		 comrel_pyme_epo t5,	"+
								"		 comcat_epo t7	"+
								"  WHERE t5.ic_pyme = t2.ic_pyme		"+
								"		AND t1.ic_pyme = t2.ic_pyme	"+
								"		AND t2.ic_pyme = t6.ic_epo_pyme_if	"+
								"		AND t1.ic_estado = t3.ic_estado	"+
								"		AND t5.ic_epo = t7.ic_epo	"+
								"		AND t2.ic_version_convenio = t8.ic_version_convenio(+)	"+
								"		AND t2.ic_tipo_cliente IN (?, 4)	"+//Variable de tipo de usuario
								"		AND t6.cg_tipo = 'P'	"+
								"		AND t5.cg_num_distribuidor IS NOT NULL	"+
								"		AND (t2.cs_invalido IS NULL OR t2.cs_invalido != 'S')	"+
								"		AND t2.cs_tipo_afilia_distri IN ('B', 'C')	"+
								"		AND t1.cs_fiscal = 'S' 	");
			conditions.add("2");
								
			if (!numElectronico.equals("")){
				qrySentencia.append(" AND t6.ic_nafin_electronico = ? ");
				conditions.add(numElectronico);
			}
			if(!nombre.equals("")){
				qrySentencia.append("	AND (   (UPPER (TRIM (t2.cg_razon_social)) LIKE UPPER (?))	"+
										"			OR (UPPER (TRIM (t2.cg_nombre || ' ' || t2.cg_appat || ' '	|| t2.cg_apmat	)	) LIKE UPPER (?))	"+
										"		)"
										) ;
				conditions.add(nombre+"%");
				conditions.add(nombre+"%");
			}
			if(!seleccionEstado.equals("")){
				qrySentencia.append(" AND t1.ic_estado = ? ");
				conditions.add(seleccionEstado);
			}	
			if(!claveRFC.equals("")){
				qrySentencia.append(" AND t2.cg_rfc = ? ");
				conditions.add(claveRFC);
			}	
			if(!seleccionCadenaProductiva.equals("")){
				qrySentencia.append(" AND t5.ic_epo = ? ");
				conditions.add(seleccionCadenaProductiva);
			}	
			if(!numDistribuidor.equals("")){
				qrySentencia.append(" AND t5.cg_num_distribuidor  = ? ");
				conditions.add(numDistribuidor);
			}
			if(!numSIRAC.equals("")){
				qrySentencia.append(" AND t2.in_numero_sirac = ? ");
				conditions.add(numSIRAC);
			}
	
			if(modPorPropietario.equals("S")){
				qrySentencia.append(" AND t2.cs_datos_pers_modif = ? ");
				conditions.add(modPorPropietario);
			}					
		log.info("getDocumentQueryFile_qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
			
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
			
			pdfDoc.setTable(12,100);
			pdfDoc.setCell("Cadena Productiva ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("N�mero de Distribuidor ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("N�mero de Nafin Electr�nico ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("N�mero de SIRAC	","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Nombre o Raz�n Social	","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("RFC ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Domicilio ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Estado ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tel�fono ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Versi�n Convenio ","celda01",ComunesPDF.CENTER); 
			pdfDoc.setCell("Fecha de Consulta convenio ","celda01",ComunesPDF.CENTER); 
			pdfDoc.setCell("Convenio �nico ","celda01",ComunesPDF.CENTER);
			//pdfDoc.setCell("Opera Fideicomiso para Desarrollo de Proveedores ","celda01",ComunesPDF.CENTER); 
			
			while (rs.next())	{					
				String cadenaProductiva = (rs.getString("CG_RAZON_SOCIAL") == null) ? "" : rs.getString("CG_RAZON_SOCIAL");
				String numDistribuidor = (rs.getString("NUM_DISTRIBUIDOR") == null) ? "" : rs.getString("NUM_DISTRIBUIDOR");
				String numNAE = (rs.getString("ELECT") == null) ? "" : rs.getString("ELECT");
				String numSIRAC = (rs.getString("IN_NUMERO_SIRAC") == null) ? "" : rs.getString("IN_NUMERO_SIRAC");
				String nombreRazonSocia = (rs.getString("RAZONSOCIAL") == null) ? "" : rs.getString("RAZONSOCIAL");
				String rfc = (rs.getString("RFC") == null) ? "" : rs.getString("RFC");
				String calle = (rs.getString("CALLE") == null) ? "" : rs.getString("CALLE");
				String colonia = (rs.getString("COLONIA") == null) ? "" : rs.getString("COLONIA");
				String domicilio = calle + colonia;
				String estado = (rs.getString("ESTADO") == null) ? "" : rs.getString("ESTADO");
				String telefono = (rs.getString("TELEFONO") == null) ? "" : rs.getString("TELEFONO");
				String versionConvenio = (rs.getString("CD_VERSION_CONVENIO") == null) ? "" : rs.getString("CD_VERSION_CONVENIO");
			   String fechaConsultaConvenio = (rs.getString("FECHA_CONSULTA_CONVENIO") == null) ? "" : rs.getString("FECHA_CONSULTA_CONVENIO");
			   //String operaFideicomiso = (rs.getString("OPERA_FIDEICOMISO") == "") ? "--" : rs.getString("OPERA_FIDEICOMISO");//Este campo sera util para el fodea 19
				String convenioUnico = (rs.getString("CONVENIO_UNICO") == null) ? "" : rs.getString("CONVENIO_UNICO");
				String convenioUnicoAux="";
					if(convenioUnico.equals("S")){
						convenioUnicoAux="SI";
					}else{
						convenioUnicoAux="NO";
					}
				pdfDoc.setCell(cadenaProductiva,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(numDistribuidor,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(numNAE,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(numSIRAC,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(nombreRazonSocia,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(rfc,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(domicilio,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(estado,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(telefono,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(versionConvenio,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaConsultaConvenio,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell((convenioUnicoAux ),"formas",ComunesPDF.CENTER);
				//pdfDoc.setCell(((operaFideicomiso=="S")?"SI":"NO"),"formas",ComunesPDF.CENTER);//Este campo sera util para el fodea 19
				
			}
		
			pdfDoc.addTable();
			pdfDoc.endDocument();	
			
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		int total = 0;
		
		try {
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);
		
			contenidoArchivo = new StringBuffer();	
			contenidoArchivo.append("No. de Distribuidor,	No. Nafin Electr�nico,	"+
											"No. Sirac,	Cadena Productiva,	Nombre o Raz�n Social,"+	
											"RFC,	Domicilio,	Estado,	Tel�fono,	 Versi�n Convenio,"+
											"Fecha de Consulta,	 Convenio �nico\n");//,Opera Fideicomiso para Desarrollo de Proveedores\n");//Este campo sera util para el fodea 19

			
		while (rs.next())	{					
				String cadenaProductiva = (rs.getString("CG_RAZON_SOCIAL") == null) ? "" : rs.getString("CG_RAZON_SOCIAL");
				String numDistribuidor = (rs.getString("NUM_DISTRIBUIDOR") == null) ? "" : rs.getString("NUM_DISTRIBUIDOR");
				String numNAE = (rs.getString("ELECT") == null) ? "" : rs.getString("ELECT");
				String numSIRAC = (rs.getString("IN_NUMERO_SIRAC") == null) ? "" : rs.getString("IN_NUMERO_SIRAC");
				String nombreRazonSocia = (rs.getString("RAZONSOCIAL") == null) ? "" : rs.getString("RAZONSOCIAL");
				String rfc = (rs.getString("RFC") == null) ? "" : rs.getString("RFC");
				String calle = (rs.getString("CALLE") == null) ? "" : rs.getString("CALLE");
				String colonia = (rs.getString("COLONIA") == null) ? "" : rs.getString("COLONIA");
				String domicilio = calle + colonia;
				String estado = (rs.getString("ESTADO") == null) ? "" : rs.getString("ESTADO");
				String telefono = (rs.getString("TELEFONO") == null) ? "" : rs.getString("TELEFONO");
				String versionConvenio = (rs.getString("CD_VERSION_CONVENIO") == null) ? "--" : rs.getString("CD_VERSION_CONVENIO");
				String fechaConsultaConvenio = (rs.getString("CD_VERSION_CONVENIO") == null) ? "--" : rs.getString("FECHA_CONSULTA_CONVENIO");
				String convenioUnico = (rs.getString("CONVENIO_UNICO") == null) ? "" : rs.getString("CONVENIO_UNICO");
				//String operaFideicomiso = (rs.getString("OPERA_FIDEICOMISO") == "") ? "--" : rs.getString("OPERA_FIDEICOMISO");//Este campo sera util para el fodea 19
				String convenioUnicoAux="";
					if(convenioUnico.equals("S")){
						convenioUnicoAux="SI";
					}else{
						convenioUnicoAux="NO";
					}				
			
				contenidoArchivo.append(numDistribuidor.replace(',',' ')+", "+
												numNAE.replace(',',' ')+", "+
												numSIRAC.replace(',',' ')+", "+
												cadenaProductiva.replace(',',' ')+", "+
												nombreRazonSocia.replace(',',' ')+", "+
												rfc.replace(',',' ')+", "+
												domicilio.replace(',',' ')+", "+
												estado.replace(',',' ')+", "+
												telefono.replace(',',' ')+", "+
												versionConvenio.replace(',',' ')+", "+
												fechaConsultaConvenio.replace(',',' ')+", "+				
												convenioUnicoAux.replace(',',' ')+"\n");//","+
												//((operaFideicomiso=="S")?"SI":"NO")+"\n");		//Este campo sera util para el fodea 19
			}
				
		creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
		nombreArchivo = creaArchivo.getNombre();
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}

	public String imprimirCuentas(HttpServletRequest request, List cuentas, String tituloF,  String tituloC, String path ) {  
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		try {
				
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				
			pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);
			pdfDoc.addText(tituloF,"formas",ComunesPDF.CENTER);
			pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);
			
			pdfDoc.setTable(1,30);			
			pdfDoc.setCell(tituloC, "formas",ComunesPDF.LEFT);  
			pdfDoc.setCell("Login del Usuario: ", "celda02",ComunesPDF.CENTER);	
			  
			
			Iterator itCuentas = cuentas.iterator();	
			while (itCuentas.hasNext()) {
				String cuenta = (String) itCuentas.next();		
				pdfDoc.setCell(cuenta, "formas",ComunesPDF.CENTER);	
			}	
			
			pdfDoc.addTable();
			pdfDoc.endDocument();
			
		 
			
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
	
	public Registros getInfoModificar(){
		log.info("getInfoModificar(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		
		AccesoDB con = new AccesoDB(); 
		
		
		Registros registros  = new Registros();
		try{
			con.conexionDB();
			qrySentencia.append(	" SELECT "+
											" pyme.ic_pyme, "+
											" pyme.in_numero_sirac, "+
											" pyme.CS_TIPO_PERSONA, "+
											" pyme.ic_tipo_cliente, "+
											" pyme.CS_TIPO_AFILIA_DISTRI, "+
											" pyme.cg_appat,  "+
											" pyme.cg_apmat,  "+
											" pyme.cg_nombre, "+
											" pyme.cg_rfc, "+
											" dom.cg_calle,  "+
											" dom.cg_colonia,  "+
											" dom.ic_estado, "+
											" e.cd_nombre AS estado,  "+
											" m.ic_municipio || '|' || m.cd_nombre AS municipio, "+
											//" m.ic_municipio  AS municipio, "+
											" dom.cn_cp,  "+
											" dom.ic_pais,  "+
											" dom.cg_telefono1,  "+
											" dom.cg_email,  "+
											" dom.cg_fax,  "+
											" cont.ic_contacto, "+
											" cont.cg_appat AS appat_c,  "+
											" cont.cg_apmat AS apmat_c,  "+
											" cont.cg_nombre AS nombre_c,  "+
											" cont.cg_tel AS tel_c,  "+
											" cont.cg_fax AS fax_c,  "+
											" cont.cg_email AS email_c, "+
											" py_ep.ic_epo as relpy_epo,  "+
											" py_ep.cs_aceptacion AS aceptacion, "+
											" py_ep.cg_num_distribuidor  AS num_distribuidor,  "+
											" py_ep.cs_distribuidores    AS financiamiento,  "+
											" py_ep.cg_pyme_epo_interno  AS num_proveedor, "+
											" pep.ic_producto_nafin as IC_PRODUCTO_NAFIN_PEP, "+
											" pep.cg_tipo_credito AS tipo_credito_epo,  "+
											" pn.cg_tipos_credito AS tipo_credito_nafin, "+
											" PN.IC_PRODUCTO_NAFIN as IC_PRODUCTO_NAFIN_PN, "+
											" pyme.CG_RAZON_SOCIAL, "+
											" pyme.CG_NO_ESCRITURA, "+
											" to_char(pyme.DF_CONSTITUCION,'dd/mm/yyyy') as DF_CONSTITUCION, "+
											" pyme.CG_APPAT_REP_LEGAL, "+
											" pyme.CG_APMAT_LEGAL, "+
											" pyme.CG_NOMBREREP_LEGAL, "+
											" pyme.CG_RFC_LEGAL, "+
											" pyme.CG_TELEFONO CG_TELEFONO_RL, "+
											" pyme.CG_FAX CG_FAX_RL, "+
											" pyme.CG_EMAIL CG_EMAIL_RL, "+
											" pyme.IC_IDENTIFICACION IC_IDENTIFICACION_RL, "+
											" pyme.CG_NO_IDENTIFICACION CG_NO_IDENTIFICACION_RL, "+
											" pyme.CG_NO_ESCRITURA_RL, "+
											" to_char(pyme.DF_PODER_NOTARIAL_RL,'dd/mm/yyyy') as DF_PODER_NOTARIAL_RL, "+
											" pyme.FN_VENTAS_NET_TOT, "+
											" pyme.IN_NUMERO_EMP, "+
											" pyme.IC_SECTOR_ECON, "+
											" pyme.IC_SUBSECTOR, "+
											" pyme.IC_RAMA, "+
											" pyme.IC_CLASE, "+
											" pyme.CG_PRODUCTOS, "+
											" pyme.IC_TIPO_CATEGORIA, "+
											" pyme.IC_TIPO_EMPRESA, "+
											" pyme.CG_SEXO, "+
											" pyme.IC_ESTADO_CIVIL, "+
											" to_char(pyme.DF_NACIMIENTO,'dd/mm/yyyy') AS DF_NACIMIENTO, "+
											" pyme.IC_PAIS_ORIGEN, "+
											" pyme.IC_GRADO_ESC, "+
											" pyme.CG_REGIMEN_MAT, "+
											" dom.ic_domicilio_epo ic_domicilio , "+
											" cont.ic_contacto, "+
											" pep.ic_oficina_tramitadora, " +
										" pyme.cg_curp as curp, "+
										" pyme.cn_fiel as fiel, "+
										" dom.ic_ciudad as ciudad "+
										" FROM comcat_pyme   pyme,  "+
											" com_domicilio dom,  "+
											" com_contacto  cont,  "+
											" comrel_pyme_epo py_ep, "+
											" comrel_pyme_epo_x_producto pep, "+
											" comcat_municipio m, "+
											" comcat_estado e, "+
											" comcat_producto_nafin pn "+
										" WHERE e.ic_estado(+) = dom.ic_estado  "+
										  " AND m.ic_municipio (+)= dom.ic_municipio  "+
										  " AND m.ic_estado (+)= dom.ic_estado  "+
										  " AND m.ic_pais (+)= dom.ic_pais  "+
										  " AND pyme.ic_pyme  = dom.ic_pyme (+)  "+
										  " AND pep.IC_PRODUCTO_NAFIN=pn.IC_PRODUCTO_NAFIN (+) "+
										  " AND pep.ic_pyme (+)= pyme.ic_pyme  "+
										  " AND py_ep.ic_pyme (+)= pyme.ic_pyme  "+
										  " AND pyme.ic_pyme  = cont.ic_pyme (+)   "+
										  " AND pn.ic_producto_nafin(+) = 4  "+
										  " AND pep.ic_producto_nafin(+) = 4   "+
										  " AND dom.cs_fiscal (+)= 'S' "+
										  " AND cont.cs_primer_contacto (+) = 'S' "+
										  " AND pyme.cs_tipo_persona = ?	"+//'"+strTipoPersona+"'"+
										  " AND py_ep.ic_epo (+) = ?	"+ //claveEpo+
										  " AND pep.ic_epo (+) = ?	"+//claveEpo+
										  " AND dom.ic_pyme(+) = ?	"+//clavePyme+
										  " AND pep.ic_pyme(+) = ?	"+//clavePyme+
										  " AND py_ep.ic_pyme(+) = ?	"+//clavePyme+
										  " AND cont.ic_pyme(+) = ?	"+//clavePyme+
										  " AND pyme.ic_pyme = ?	");  
			conditions.add(tipoPersona);											
			conditions.add(claveEpo);conditions.add(claveEpo);
			conditions.add(clavePyme);conditions.add(clavePyme);conditions.add(clavePyme);
			conditions.add(clavePyme);conditions.add(clavePyme);
			
			registros = con.consultarDB(qrySentencia.toString(),conditions);
			
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("getInfoModificar  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("conditions "+conditions);
		log.info("qrySentencia : "+qrySentencia.toString());
		
		log.info("getInfoModificar (S) ");
		return registros;
		
	}
	/**
	 * Metodo para generar le catalogo de sector ecomonico 
	 * @return 
	 */
	public Registros getDatosCatalogoSectorEconomico(){
	log.info("getDatosCatalogoSectorEconomico(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 
		Registros registros  = new Registros();
		try{
			con.conexionDB();
			qrySentencia.append(	"	SELECT   s.ic_sector_econ as clave, (t.cd_nombre ||' - '|| s.cd_nombre) descripcion	"+
										"	FROM comcat_sector_econ s, comcat_tipo_sector t "+
										"	WHERE t.ic_tipo_sector = s.ic_tipo_sector	"+
										"	ORDER BY descripcion	");  
			
			registros = con.consultarDB(qrySentencia.toString(),conditions);
			
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("getDatosCatalogoSectorEconomico  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 	
	
		
		log.info("qrySentencia SECTOR ECON : "+qrySentencia.toString());
		log.info("conditions "+conditions);
		
		log.info("getDatosCatalogoSectorEconomico (S) ");
		return registros;
	}
	public List  getDatosCatalogoSectorEconomicoList( ){
		log.info("getDatosCatalogoSectorEconomicoList (E) ");
		AccesoDB con = new AccesoDB();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		HashMap datos =  new HashMap();
		List registros  = new ArrayList();
		try{
			con.conexionDB();
			String 	strSQL =	"	SELECT   s.ic_sector_econ as clave, (t.cd_nombre ||' - '|| s.cd_nombre) descripcion	"+
										"	FROM comcat_sector_econ s, comcat_tipo_sector t "+
										"	WHERE t.ic_tipo_sector = s.ic_tipo_sector	"+
										"	ORDER BY descripcion	";  
			log.debug(" strSQL SECTOR ECON  "+strSQL );
			pstmt=con.queryPrecompilado(strSQL);
			rs = pstmt.executeQuery();		
			while(rs.next()) {			
				datos = new HashMap();			
				datos.put("clave", rs.getString("clave") );
				datos.put("descripcion", rs.getString("descripcion"));
				registros.add(datos);
			}
			rs.close();
			con.cierraStatement();
		
		} catch (Exception e) {
			log.error("getDatosCatalogoSectorEconomicoList  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("getDatosCatalogoSectorEconomicoList (S) ");
		return registros;
	}

	public Registros getDatosCatalogoSubSectorEconomico(){
	log.info("getDatosCatalogoSubSectorEconomico(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 
		Registros registros  = new Registros();
		try{
			con.conexionDB();
			qrySentencia.append(	"	SELECT IC_SUBSECTOR AS CLAVE, "+
										" CD_NOMBRE AS DESCRIPCION "+
										" from COMCAT_subSECTOR  "+
										" where 1 = 1  ");
			
			if(!claveSector.equals("")){							
				qrySentencia.append(" and  IC_SECTOR_ECON=nvl(to_number(?),-1)	");  
				conditions.add(claveSector);
			}
			
			log.debug("qrySentencia: " + qrySentencia);			
			log.debug("conditions: " + conditions);
			
			registros = con.consultarDB(qrySentencia.toString(),conditions);
			
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("getDatosCatalogoSubSectorEconomico  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 	
	
		
		log.info("qrySentencia SUBSECTOR ECON : "+qrySentencia.toString());
		log.info("conditions "+conditions);
		
		log.info("getDatosCatalogoSubSectorEconomico (S) ");
		return registros;
	}
	
	public List getDatosCatalogoSubSectorEconomicoList(){
		log.info("getDatosCatalogoSubSectorEconomicoList(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		AccesoDB con = new AccesoDB();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		HashMap datos =  new HashMap();
		List registros  = new ArrayList();
		
		try{
			con.conexionDB();
			
			qrySentencia.append(	"	SELECT IC_SUBSECTOR AS CLAVE,CD_NOMBRE AS DESCRIPCION "+
										" from COMCAT_subSECTOR  "+
										" where 1= 1  ");
										
			log.debug("claveSector" + claveSector);					
			
			if(!claveSector.equals("")){
				qrySentencia.append("  and IC_SECTOR_ECON=nvl(to_number(?),-1)	");  
				conditions.add(claveSector);	
			}
			
			log.debug("qrySentencia: " + qrySentencia);			
			log.debug("conditions: " + conditions);
			
			pstmt = con.queryPrecompilado(qrySentencia.toString(), conditions);
			rs = pstmt.executeQuery();
			while(rs.next()) {			
				datos = new HashMap();			
				datos.put("clave", rs.getString("clave") );
				datos.put("descripcion", rs.getString("descripcion"));
				registros.add(datos);
			}
			rs.close();
			pstmt.close();
			
			
		} catch (Exception e) {
			log.error("getDatosCatalogoSubSectorEconomico  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 	
	
		log.info("getDatosCatalogoSubSectorEconomico (S) ");
		return registros;
		
	}
	public Registros getDatosCatalogoMunicipio(){
	log.info("getDatosCatalogoMunicipio(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 
		Registros registros  = new Registros();
		try{
			con.conexionDB();
			
			qrySentencia.append(	"	SELECT (m.ic_municipio||'|'||REPLACE(m.cd_nombre,',',' ')) as clave,	"+
														"	REPLACE(m.cd_nombre,',',' ')as descripcion "+
														"	FROM comcat_municipio m WHERE m.IC_PAIS=? and m.IC_ESTADO=?"	+
														"	ORDER by descripcion	");

			conditions.add(clavePais);
			conditions.add(claveEstado );
			
			
			registros = con.consultarDB(qrySentencia.toString(),conditions);
			
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("getDatosCatalogoMunicipio  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 	
	
		log.info("qrySentencia Municipios: "+qrySentencia.toString());
		log.info("conditions "+conditions);
		
		log.info("getDatosCatalogoMunicipio (S) ");
		return registros;
	
	}	
	
	public List getDatosCatalogoCiudadList(){
	log.info("getDatosCatalogoCiudadList(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		HashMap datos =  new HashMap();
		List registros  = new ArrayList();
		try{
			con.conexionDB();
			
		qrySentencia.append(	"	SELECT   codigo_ciudad as clave, REPLACE(nombre,',',' ') as descripcion	"+
										"	FROM comcat_ciudad	"+
										"	WHERE codigo_pais = ?	"+ 
										"	AND codigo_departamento = NVL (TO_NUMBER (?), 0)	"+
										"	OR codigo_departamento = NVL (TO_NUMBER (?), 0)	"	+
										"	order by descripcion	");
			pstmt = con.queryPrecompilado(qrySentencia.toString());
			pstmt.setString(1,clavePais);
			pstmt.setString(2,claveEstado);
			pstmt.setString(3,claveEstado);
			
			rs= pstmt.executeQuery();
			
			while(rs.next()){
					datos= new HashMap();
					datos.put("clave",rs.getString("clave"));
					datos.put("descripcion", rs.getString("descripcion"));
					registros.add(datos);
			}
			conditions.add(clavePais);
			conditions.add(claveEstado );conditions.add(claveEstado );
			rs.close();				
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("getDatosCatalogoCiudadList  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 	
	
		log.info("qrySentencia Ciudad: "+qrySentencia.toString());
		log.info("conditions "+conditions);
		
		log.info("getDatosCatalogoCiudadList (S) ");
		return registros;
	
	}
	public Registros getDatosCatalogoCiudad(){
	log.info("getDatosCatalogoSectorEconomico(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 
		Registros registros  = new Registros();
		try{
			con.conexionDB();
			qrySentencia.append(	"	SELECT   codigo_ciudad as clave, REPLACE(nombre,',',' ') as descripcion	"+
										"	FROM comcat_ciudad	"+
										"	WHERE codigo_pais = ?	"+ 
										"	AND codigo_departamento = NVL (TO_NUMBER (?), 0)	"+
										"	OR codigo_departamento = NVL (TO_NUMBER (?), 0)	"	+
										"	order by descripcion	");

			conditions.add(clavePais);
			conditions.add(claveEstado );conditions.add(claveEstado );
			
			
			registros = con.consultarDB(qrySentencia.toString(),conditions);
			
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("getDatosCatalogoSectorEconomico  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 	
	
		log.info("qrySentencia CIUDAD: "+qrySentencia.toString());
		log.info("conditions "+conditions);
		
		log.info("getDatosCatalogoSectorEconomico (S) ");
		return registros;
															
	}
	
	
	public Registros getDatosCatalogoRama(){
	log.info("getDatosCatalogoRama(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 
		Registros registros  = new Registros();
		try{
			con.conexionDB();
			
			qrySentencia.append(	"	SELECT ic_rama as clave, cd_nombre as descripcion	"+
										"	FROM comcat_rama	"+
										" WHERE  1 = 1 ");
										
				if(!claveSector.equals("")){						
					qrySentencia.append("	and ic_sector_econ = NVL (TO_NUMBER (?), -1)	");
					conditions.add(claveSector);
				}
				if(!claveSubSector.equals("")){	
					qrySentencia.append(" AND ic_subsector = NVL (TO_NUMBER (?), -1)	");
					conditions.add(claveSubSector );
				}
			
			log.debug("qrySentencia: " + qrySentencia);			
			log.debug("conditions: " + conditions);
			
			registros = con.consultarDB(qrySentencia.toString(),conditions);
			
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("getDatosCatalogoRama  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 	
	
		log.info("qrySentencia RAMA: "+qrySentencia.toString());
		log.info("conditions "+conditions);
		
		log.info("getDatosCatalogoRama (S) ");
		return registros;
															
	}
	
	public List getDatosCatalogoRamaList(){
	log.info("getDatosCatalogoRamaList(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		HashMap datos =  new HashMap();
		List registros  = new ArrayList();
		try{
			con.conexionDB();
			qrySentencia.append(	"	SELECT ic_rama as clave, cd_nombre as descripcion	"+
										"	FROM comcat_rama	"+
										" where 1 = 1 ");
				if(!claveSector.equals("")){						
					qrySentencia.append(" AND ic_sector_econ = NVL (TO_NUMBER (?), -1)	");
					conditions.add(claveSector);
				}
				if(!claveSubSector.equals("")){	
					qrySentencia.append(" AND ic_subsector = NVL (TO_NUMBER (?), -1)	");
					conditions.add(claveSubSector);
				}
			
			log.debug("qrySentencia: " + qrySentencia);			
			log.debug("conditions: " + conditions);
			
			pstmt = con.queryPrecompilado(qrySentencia.toString(), conditions );
			rs =pstmt.executeQuery();
			while(rs.next()){
				datos= new HashMap();
				datos.put("clave",rs.getString("clave"));
				datos.put("descripcion", rs.getString("descripcion"));
				registros.add(datos);
			}
			rs.close();
			pstmt.close(); 
			
			
		} catch (Exception e) {
			log.error("getDatosCatalogoRamaList  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 	
		
		log.info("getDatosCatalogoRamaList (S) ");
		return registros;
															
	}
	
	public Registros getDatosCatalogoClase(){
	log.info("getDatosCatalogoClase(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 
		Registros registros  = new Registros();
		try{
			con.conexionDB();
			qrySentencia.append(	"	 SELECT ic_clase as clave, cd_nombre as descripcion	"+
										"	FROM comcat_clase	"+
										"  WHERE 1 = 1 ");
			if(!claveSector.equals("")){							
				qrySentencia.append("	AND  ic_sector_econ = NVL (TO_NUMBER (?), -1)	");
				conditions.add(claveSector);
			}
			if(!claveSubSector.equals("")){	
				qrySentencia.append("	AND ic_subsector = NVL (TO_NUMBER (?), -1)	");
				conditions.add(claveSubSector );
			}
			if(!claveRama.equals("")){	
				qrySentencia.append("	AND ic_rama = NVL (TO_NUMBER (?), -1)	");
				conditions.add(claveRama);
			}
			
			log.debug("qrySentencia: " + qrySentencia);			
			log.debug("conditions: " + conditions);			
			
			registros = con.consultarDB(qrySentencia.toString(),conditions);
			
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("getDatosCatalogoClase  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 	
			
		log.info("getDatosCatalogoClase (S) ");
		return registros;
															
	}
	
	public List getDatosCatalogoClaseList(){
	log.info("getDatosCatalogoClaseList(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		HashMap datos =  new HashMap();
		List registros  = new ArrayList();
		try{
			con.conexionDB();
			qrySentencia.append(	"	 SELECT ic_clase as clave, cd_nombre as descripcion	"+
										"	FROM comcat_clase	"+
										" WHERE 1 = 1  " );
										
			if(!claveSector.equals("")){							
				qrySentencia.append(" AND  ic_sector_econ = NVL (TO_NUMBER (?), -1)	");
				conditions.add(claveSector);
			}
			if(!claveSubSector.equals("")){
				qrySentencia.append(	"	AND ic_subsector = NVL (TO_NUMBER (?), -1)	");
				conditions.add(claveSubSector);
			}
			if(!claveRama.equals("")){
				qrySentencia.append(" AND ic_rama = NVL (TO_NUMBER (?), -1)	");	
				conditions.add(claveRama);
			}
			
			log.debug("qrySentencia: " + qrySentencia);			
			log.debug("conditions: " + conditions);
			
			pstmt = con.queryPrecompilado(qrySentencia.toString(), conditions);			
			rs =pstmt.executeQuery();
			
			while(rs.next()){
				datos= new HashMap();
				datos.put("clave",rs.getString("clave"));
				datos.put("descripcion", rs.getString("descripcion"));
				registros.add(datos);
			}			
			rs.close();
			pstmt.close();
			
			
		} catch (Exception e) {
			log.error("getDatosCatalogoClaseList  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 	
		
		log.info("getDatosCatalogoClaseList (S) ");
		return registros;
															
	}
	
	public String getTipoCredito(){
	log.info("getTipoCredito(E)");
		qrySentencia 	= new StringBuffer();
		ResultSet rs = null;
		PreparedStatement pstmt = null;
		
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB(); 
		String registros  = new String();
		
		try{
			con.conexionDB();
			
			qrySentencia.append(	"	SELECT nvl(cg_tipos_credito,'N') cg_tipos_credito	"+
										"	FROM comrel_producto_epo	"+
										"	WHERE ic_producto_nafin = 4 "+
										"  AND  ic_epo= ?	");			
			
			System.out.println("claveEpo ==="+claveEpo );    
			
			log.info("qrySentencia : "+qrySentencia.toString());
			log.info("conditions : "+conditions);
			
			conditions.add(claveEpo);	
			pstmt = con.queryPrecompilado(qrySentencia.toString(), conditions) ; 								
			rs=pstmt.executeQuery();
			
			while (rs.next()) {
				registros=rs.getString("cg_tipos_credito");
			}
			rs.close();
			pstmt.close(); 
			
		} catch (Exception e) {
			log.error("getTipoCredito  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 	
		
		log.info("getTipoCredito (S) ");
		return registros;
															
	}
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina 
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/	

	public void setNumElectronico(String numElectronico) {
		this.numElectronico = numElectronico;
	}


	public String getNumElectronico() {
		return numElectronico;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getNombre() {
		return nombre;
	}


	public void setSeleccionEstado(String seleccionEstado) {
		this.seleccionEstado = seleccionEstado;
	}


	public String getSeleccionEstado() {
		return seleccionEstado;
	}


	public void setClaveRFC(String claveRFC) {
		this.claveRFC = claveRFC;
	}


	public String getClaveRFC() {
		return claveRFC;
	}


	public void setSeleccionCadenaProductiva(String seleccionCadenaProductiva) {
		this.seleccionCadenaProductiva = seleccionCadenaProductiva;
	}


	public String getSeleccionCadenaProductiva() {
		return seleccionCadenaProductiva;
	}


	public void setNumDistribuidor(String numDistribuidor) {
		this.numDistribuidor = numDistribuidor;
	}


	public String getNumDistribuidor() {
		return numDistribuidor;
	}


	public void setNumSIRAC(String numSIRAC) {
		this.numSIRAC = numSIRAC;
	}


	public String getNumSIRAC() {
		return numSIRAC;
	}


	public void setModPorPropietario(String modPorPropietario) {
		this.modPorPropietario = modPorPropietario;
	}


	public String getModPorPropietario() {
		return modPorPropietario;
	}


	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}


	public String getClaveEpo() {
		return claveEpo;
	}


	public void setClavePyme(String clavePyme) {
		this.clavePyme = clavePyme;
	}


	public String getClavePyme() {
		return clavePyme;
	}


	public void setTipoPersona(String tipoPersona) {
		this.tipoPersona = tipoPersona;
	}


	public String getTipoPersona() {
		return tipoPersona;
	}


	public void setClavePais(String clavePais) {
		this.clavePais = clavePais;
	}


	public String getClavePais() {
		return clavePais;
	}


	public void setClaveEstado(String claveEstado) {
		this.claveEstado = claveEstado;
	}


	public String getClaveEstado() {
		return claveEstado;
	}


	public void setClaveMunicipio(String claveMunicipio) {
		this.claveMunicipio = claveMunicipio;
	}


	public String getClaveMunicipio() {
		return claveMunicipio;
	}


	public void setClaveSector(String claveSector) {
		this.claveSector = claveSector;
	}


	public String getClaveSector() {
		return claveSector;
	}


	public void setClaveSubSector(String claveSubSector) {
		this.claveSubSector = claveSubSector;
	}


	public String getClaveSubSector() {
		return claveSubSector;
	}


	public void setClaveRama(String claveRama) {
		this.claveRama = claveRama;
	}


	public String getClaveRama() {
		return claveRama;
	}


	public void setTipoCredito(String tipoCredito) {
		this.tipoCredito = tipoCredito;
	}


	public String get_tipoCredito() {
		return tipoCredito;
	}
}