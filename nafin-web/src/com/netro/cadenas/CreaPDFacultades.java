package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 *
 * Clase que genera un documento PDF a partir de una Lista de datos
 * @author Jos� Williams Alvarado
 */

public class CreaPDFacultades  {
	//Constructor
	public CreaPDFacultades() {
	}
	//Variable para enviar mensajes al log.
	private static final Log log = ServiceLocator.getInstance().getLog(CreaPDFacultades.class);
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en una Lista que se recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param List pasado como parametro desde la clase invocante
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */	
	public String crearPDFFac(HttpServletRequest request, List lDoctosAceptados, String path, String tipo){
		String nombreArchivo = "";
		log.debug("crearCustomFile (E)");
		try {
			HttpSession session = request.getSession();
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);

			String meses[]	=	{"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual	=	new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual	=	fechaActual.substring(0,2);
			String mesActual	=	meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual	=	fechaActual.substring(6,10);
			String horaActual	=	new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
			((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
			(String)session.getAttribute("sesExterno"),
			(String)session.getAttribute("strNombre"),
			(String)session.getAttribute("strNombreUsuario"),
			(String)session.getAttribute("strLogo"),
			(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

			pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + "-----------------------------" +
			horaActual, "formas", ComunesPDF.CENTER);
			
			pdfDoc.setTable(2, 33);
			pdfDoc.setCell("Fecha de Modificaci�n","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell(fechaActual,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("Hora de Modificaci�n","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell(horaActual,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("Usuario","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell((String)session.getAttribute("strNombreUsuario"),"formas",ComunesPDF.CENTER);
			pdfDoc.addTable();
			
			pdfDoc.setTable(6, 100);
			pdfDoc.setCell("Clave","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Intermediario","celda01",ComunesPDF.LEFT);
			pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto Anterior","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto Actual","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Aplica Facultades por IF","celda01",ComunesPDF.CENTER);

			String  cveIF= "", intermediario = "", moneda= "", monAnt="", monAct="", facIF= "";
		
			for(int i=0; i<lDoctosAceptados.size(); i++){
				List aux=(List)lDoctosAceptados.get(i);
				cveIF = aux.get(0)==null?"":aux.get(0).toString();
				intermediario = aux.get(1)==null?"":aux.get(1).toString();
				moneda = aux.get(2)==null?"":aux.get(2).toString();
				monAnt = Comunes.formatoDecimal((aux.get(3)==null?"":aux.get(3).toString()),2);
				monAct= Comunes.formatoDecimal((aux.get(4)==null?"":aux.get(4).toString()),2);
				facIF = aux.get(5).toString().equals("true") ? "SI" : "";
				
				pdfDoc.setCell(cveIF,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(intermediario,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell("$"+monAnt,"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+monAct,"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell(facIF,"formas",ComunesPDF.CENTER);
			}
			pdfDoc.addTable();
			pdfDoc.endDocument();

		}catch (Throwable e) {
			throw new AppException("Error al generar el archivo",e);
		}finally {

		}

		return nombreArchivo;
	}
	
}