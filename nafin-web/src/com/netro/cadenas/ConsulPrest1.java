package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsulPrest1 implements  IQueryGeneratorRegExtJS {
	public ConsulPrest1() {  }
	private List 	conditions;
	private String 	paginaOffset;
	private String 	paginaNo;
	StringBuffer 	strQuery;
	StringBuffer qrySentencia = new StringBuffer("");
	private static final Log log = ServiceLocator.getInstance().getLog(ConsulPrest1.class);//Variable para enviar mensajes al log.
	private String txtCadProductiva;
	private String num_electronico;
	private String fec_ini;
	private String fec_fin;
	private String convenio_unico;
	private String codigoCliente;
	private String numeroPrestamo;
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
 	}  
		 
	public String getDocumentQuery(){
		conditions = new ArrayList();	
		strQuery 		= new StringBuffer(); 
		log.debug("getDocumentQuery)"+strQuery.toString()); 
		log.debug("getDocumentQuery)"+conditions);
		return strQuery.toString();
 	}  
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
		strQuery = new StringBuffer(); 
		log.debug("getDocumentSummaryQueryForIds "+strQuery.toString()); 
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
		return strQuery.toString();
 	} 
					
	public String getDocumentQueryFile(){
		conditions = new ArrayList();   
		strQuery 		= new StringBuffer(); 
		StringBuffer condicion    = new StringBuffer();
		log.info("getDocumentQueryFile(E)");
		if(!codigoCliente.equals("")&&codigoCliente!=null){
				condicion.append(" AND pre.codigo_cliente = ?");
				conditions.add(codigoCliente);
		}		
		if(!numeroPrestamo.equals("")&&numeroPrestamo!=null){
				condicion.append(" AND pre.numero_prestamo = ? ");
				conditions.add(numeroPrestamo);
		}
		strQuery.append(" select pre.codigo_empresa, pre.codigo_agencia"+
				" , pre.codigo_sub_aplicacion, c.codigo_linea_credito, pre.numero_contrato"+
				" , pre.numero_prestamo, pro.codigo_financiera, f.nombre"+
				" , pro.codigo_base_operacion, pre.valor_inicial"+
				" , pre.tipo_subsidio, pre.codigo_valor_tasa_cartera"+
				" , pre.codigo_tipo_amortizacion, pre.valor_presente"+
				" , pre.tipo_interes"+
				" , TO_CHAR(pre.fecha_apertura,'yyyy/mm/dd') as fecha_apertura"+
				" , TO_CHAR(pre.fecha_ultimo_pago,'yyyy/mm/dd') as fecha_ultimo_pago"+
				" , TO_CHAR(pre.fecha_primer_pago_capital,'yyyy/mm/dd') as fecha_primer_pago_capital"+
				" , TO_CHAR(pre.fecha_primer_pago_interes,'yyyy/mm/dd') as fecha_primer_pago_interes"+
				" , TO_CHAR(pre.fecha_proximo_pago_capital,'yyyy/mm/dd') as fecha_proximo_pago_capital"+
				" , TO_CHAR(pre.fecha_proximo_pago_interes,'yyyy/mm/dd') as fecha_proximo_pago_interes"+
				" ,'' AS total_adeudo"+
				" from lc_proyecto pro, pr_prestamos pre, mg_financieras f"+
				" , pr_contratos c"+
				" where"+
				" pro.codigo_financiera = f.codigo_financiera"+
				" and pro.tipo_financiera = f.tipo_financiera"+
				" and pro.numero_linea_credito = c.codigo_linea_credito"+
				" and pro.codigo_empresa = c.codigo_empresa_lc"+
				" and pro.codigo_agencia = c.codigo_agencia_lc"+
				" and pro.codigo_sub_aplicacion = c.codigo_sub_aplicacion_lc"+
				" and pro.codigo_aplicacion='BLC'"+
				" and c.codigo_empresa = pre.codigo_empresa"+
				" and c.codigo_agencia = pre.codigo_agencia"+
				" and rownum <= 15"+
				" and c.codigo_sub_aplicacion = pre.codigo_sub_aplicacion"+
				" and c.numero_contrato = pre.numero_contrato"+condicion.toString());
		log.debug("getDocumentQueryFile "+strQuery.toString());
		log.debug("getDocumentQueryFile)"+conditions);
		log.info("getDocumentQueryFile(S)");
		return strQuery.toString();
		
 	} 
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		log.debug("::crearCustomFile(E)");
		String linea = "";  
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		int numeroRegistros = 0;
		double totalAdeudo = 0.0;
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		CreaArchivo 	archivo 			= new CreaArchivo();
		if ("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.setTable(10, 80);
				pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Proyecto","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Pr�stamo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Intermediario","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Base de operaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto inicial","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo subsidio","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Grupo Tasa","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo Amortizaci�n","celda01",ComunesPDF.CENTER);   
				pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Valor presente","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Inter�s Compuesto","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Operaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha �ltimo pago","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("1er. pago capital","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("1er. pago inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Prox. pago capital","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Prox. pago inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Total adeudo ","celda01",ComunesPDF.CENTER);
				while (rs.next()) {
					numeroRegistros++;
					String codigoEmpresa = rs.getString("codigo_empresa")==null?"":rs.getString("codigo_empresa");
					String codigoAgencia = rs.getString("codigo_agencia")==null?"":rs.getString("codigo_agencia");
					String codigoSubAplicacion = rs.getString("codigo_sub_aplicacion")==null?"":rs.getString("codigo_sub_aplicacion");
					String codigoLineaCredito = rs.getString("codigo_linea_credito")==null?"":rs.getString("codigo_linea_credito");
					String numeroContrato = rs.getString("numero_contrato")==null?"":rs.getString("numero_contrato");
					String numeroPrestamo = rs.getString("numero_prestamo")==null?"":rs.getString("numero_prestamo");
					String codigoFinanciera = rs.getString("codigo_financiera")==null?"":rs.getString("codigo_financiera");
					String intermediario = rs.getString("nombre")==null?"":rs.getString("nombre");
					String codigoBaseOperacion = rs.getString("codigo_base_operacion")==null?"":rs.getString("codigo_base_operacion");
					String codigoValorTasaCartera = rs.getString("codigo_valor_tasa_cartera")==null?"":rs.getString("codigo_valor_tasa_cartera");
					String codigoTipoAmortizacion = rs.getString("codigo_tipo_amortizacion")==null?"":rs.getString("codigo_tipo_amortizacion");
					String valorInicial = rs.getString("valor_inicial")==null?"":rs.getString("valor_inicial");
					String valorPresente = rs.getString("valor_presente")==null?"":rs.getString("valor_presente");
					String tipoInteres = rs.getString("tipo_interes")==null?"":rs.getString("tipo_interes");
					String tipoSubsidio = rs.getString("tipo_subsidio")==null?"":rs.getString("tipo_subsidio");
					String fechaApertura = rs.getString("fecha_apertura")==null?"":rs.getString("fecha_apertura");
					String fechaUltimoPago = rs.getString("fecha_ultimo_pago")==null?"":rs.getString("fecha_ultimo_pago");
					String fechaPrimerPagoCapital = rs.getString("fecha_primer_pago_capital")==null?"":rs.getString("fecha_primer_pago_capital");
					String fechaPrimerPagoInteres = rs.getString("fecha_primer_pago_interes")==null?"":rs.getString("fecha_primer_pago_interes");
					String fechaProximoPagoCapital = rs.getString("fecha_proximo_pago_capital")==null?"":rs.getString("fecha_proximo_pago_capital");
					String fechaProximoPagoInteres = rs.getString("FECHA_PROXIMO_PAGO_INTERES")==null?"":rs.getString("FECHA_PROXIMO_PAGO_INTERES");
					HashMap registros = this.getValorTotal(numeroPrestamo,codigoEmpresa,codigoAgencia,codigoSubAplicacion) ;
					String totalAdeudoAux =registros.get("TOTAL_ADEUDO").toString();
				   totalAdeudo +=  Double.parseDouble(totalAdeudoAux);
					pdfDoc.setCell("A","fromas",ComunesPDF.CENTER);
					pdfDoc.setCell(codigoLineaCredito,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(numeroContrato,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(numeroPrestamo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(intermediario,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(codigoBaseOperacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$ "+Comunes.formatoDecimal(valorInicial,2) ,"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(tipoSubsidio,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(codigoValorTasaCartera,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(codigoTipoAmortizacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("B","fromas",ComunesPDF.CENTER);
					pdfDoc.setCell(valorPresente,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(tipoInteres,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaApertura,"formas",ComunesPDF.CENTER);		
					pdfDoc.setCell(fechaUltimoPago,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaPrimerPagoCapital,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaPrimerPagoInteres,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaProximoPagoCapital,"formas",ComunesPDF.CENTER);		
					pdfDoc.setCell(fechaProximoPagoInteres,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalAdeudoAux,2) ,"formas",ComunesPDF.RIGHT);
					
				}
				/*pdfDoc.setCell("Monto total ","formas",ComunesPDF.CENTER,2);
				pdfDoc.setCell("Total Pr�stamo","formas",ComunesPDF.CENTER,8);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalAdeudo,2),"formas",ComunesPDF.RIGHT,2);
				pdfDoc.setCell(Integer.toString(numeroRegistros),"formas",ComunesPDF.CENTER,8);*/
				pdfDoc.addTable();
				pdfDoc.endDocument();
			} catch(Throwable e) {
					throw new AppException("Error al generar el archivo", e);
				}
		}
		log.info(":: crearCustomFile(S)");
		return nombreArchivo;			
	}
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		
		return "";
	}
	/**
	 * 
	 * @throws java.lang.Exception
	 * @return 
	 * @param numeroPrestamo, codigoEmpresa, codigoAgencia, codigoSubAplicacion
	 */
	public HashMap getValorTotal(String numeroPrestamo, String codigoEmpresa, String codigoAgencia, String codigoSubAplicacion )throws Exception{
		log.info("getValorTotal (E)");
		AccesoDB 	con = new AccesoDB();
		conditions = new ArrayList();
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean		commit = true;
		strQuery 		= new StringBuffer();
		List varBind = new ArrayList();
		HashMap	registros = new HashMap();
		String totalAdeudo = "0";
		try{
			con.conexionDB();
			strQuery.append("select sum(sp.valor) as valor"+
					" from PR_SALDOS_PRESTAMO sp, PR_TIPOS_SALDO ts"+
					" where sp.codigo_tipo_saldo = ts.codigo_tipo_saldo"+
					" and ts.tipo_abono in ('C', 'I', 'G', 'D', 'R')"+
					" and sp.numero_prestamo = ? "+
					" and sp.codigo_empresa = ? "+
					" and sp.codigo_agencia = ? "+
					" and sp.codigo_sub_aplicacion = ?"
					);
			varBind.add(numeroPrestamo);
			varBind.add(codigoEmpresa);
			varBind.add(codigoAgencia);
			varBind.add(codigoSubAplicacion);
			ps = con.queryPrecompilado(strQuery.toString(),varBind);
			rs = ps.executeQuery();
			if( rs.next() ){
				totalAdeudo =  rs.getString("valor")==null?"":rs.getString("valor");
			}
			rs.close();
			ps.close();	
			registros.put("TOTAL_ADEUDO",totalAdeudo);
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			log.error("Error getValorTotal "+e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getValorTotal (S)");
		}	
		return registros;
 	}
	  
	
	public List getConditions() {
		return conditions;
	}
	public String getPaginaNo() { return paginaNo; 	}
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}
	

	public String getCodigoCliente() {
		return codigoCliente;
	}

	public void setCodigoCliente(String codigoCliente) {
		this.codigoCliente = codigoCliente;
	}
	public String getNumeroPrestamo() {
		return numeroPrestamo;
	}

	public void setNumeroPrestamo(String numeroPrestamo) {
		this.numeroPrestamo = numeroPrestamo;
	}

}