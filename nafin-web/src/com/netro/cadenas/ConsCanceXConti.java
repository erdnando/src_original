package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsCanceXConti implements IQueryGeneratorRegExtJS {

//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsCanceXConti.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	StringBuffer qryCondicion 	= new StringBuffer("");
	private String ic_producto;
	private String ic_if;
	private String ig_numero_prestamo;
	private String df_operacion;
	private String solicitudes;
	private String pantalla;
		
		 
	public ConsCanceXConti() { }
	
	  
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		qryCondicion 	= new StringBuffer();
		conditions 		= new ArrayList();
		String hint ="";
		
		if (pantalla.equals("Consulta"))  {
		
			if (ic_producto.equals("0"))  { // Credito Electronico 
				
					qrySentencia.append(" SELECT "+
						 " '0' AS PRODUCTO, "+
						 " 'Credito Electronico' AS NOMBREPRODUCTO,  "+
						 " SP.IC_SOLIC_PORTAL AS NU_SOLICITUD,  "+
						 " I.cg_razon_social as INTERMEDIARIO,   "+
						 " 'N/A' as NOMBRE_EPO,   "+
						 " P.cg_razon_social as NOMBRE_PYME, "+  
						 " M.cd_nombre as MONEDA,  "+
						 " 'N/A' as TIPO_FACTORAJE,  "+
						 " TO_CHAR(SP.df_operacion, 'DD/MM/YYYY') as FECHA_OPERACION,  "+
						 " SP.ig_numero_prestamo as NUM_PRESTAMO, "+ 
						 " SP.FN_IMPORTE_DSCTO AS MONTO_OPERAR, "+
						 " ES.cd_descripcion as ESTATUS_ACTUAL,   "+ 
						 " 'Credito' as ORIGEN "+
						 " , '' as SELECCION " +
						" from   "+
						" comcat_moneda m, "+
						" comcat_pyme p,   "+
						" comcat_if i,   "+
						" comcat_estatus_solic es, "+
						" com_interfase inter,   "+
						" com_solic_portal sp   "+
						" where  "+
						" es.ic_estatus_solic=sp.ic_estatus_solic and "+
						" sp.IC_SOLIC_PORTAL = inter.IC_SOLIC_PORTAL and   "+
						" m.ic_moneda=sp.ic_moneda and  "+
						" sp.IC_IF = i.IC_IF and  "+
						" sp.IG_CLAVE_SIRAC = p.IN_NUMERO_SIRAC (+) and   "+
						" sp.IC_ESTATUS_SOLIC=3 ");
											
						if ( ic_if.equals("") && ig_numero_prestamo.equals("") && df_operacion.equals("") ) {							
							qrySentencia.append("	AND SP.df_operacion >= TRUNC(SYSDATE) AND SP.df_operacion < TRUNC (SYSDATE)+1 "); 
						} else {
							if (!ic_if.equals("") && !ic_if.equals("0") ) {
							qrySentencia.append(" and i.ic_if = ? " +
														" and sp.ic_if= ? ");
														conditions.add(ic_if);
														conditions.add(ic_if);
							}
							
							if (!ig_numero_prestamo.equals("")) {
								qrySentencia.append(" AND SP.ig_numero_prestamo IN(" + ig_numero_prestamo + ")"); 
							}
							if (!df_operacion.equals("")) {						
								qrySentencia.append(" AND sp.df_operacion >= to_date( ?, 'dd/mm/yyyy' )" +
														  " AND sp.df_operacion < to_date(?,  'dd/mm/yyyy') + 1");
														  conditions.add(df_operacion);
														  conditions.add(df_operacion);
							}
					}	
					
					qrySentencia.append(" ORDER BY SP.df_operacion DESC ");
						
				} else if (ic_producto.equals("1"))	 { //Descuento //FOEA 047-2010 Rebos
									
					if ( ic_if.equals("") && ig_numero_prestamo.equals("") && df_operacion.equals("") ) {					
						qryCondicion.append("	AND s.df_operacion >= TRUNC(SYSDATE) AND s.df_operacion < TRUNC (SYSDATE)+1 ");
						hint = "/*+ ordered index( s IN_COM_SOLICITUD_05_NUK) use_nl(s ds d i p e m es tf)*/";
					} else {
					
						if (!ic_if.equals("") && !ic_if.equals("0") ) {
							qryCondicion.append(" and ds.ic_if =  ?");
							 conditions.add(ic_if);						
							hint = "/*+ ordered index( s IN_COM_SOLICITUD_02_NUK) use_nl(s ds d i p e m es tf)*/";
						}
						if (!ig_numero_prestamo.equals("")) {
							qryCondicion.append("	AND s.ig_numero_prestamo IN( " + ig_numero_prestamo + ")"); 
							hint = "/*+ ordered index( s IN_COM_SOLICITUD_15_NUK) use_nl(s ds d i p e m es tf)*/";
						}
						
						if (!df_operacion.equals("")) {						
							qryCondicion.append(" AND s.df_operacion >= to_date( ?, 'dd/mm/yyyy' )"
													+  " AND s.df_operacion < to_date( ?, 'dd/mm/yyyy') + 1");
							conditions.add(df_operacion);	
							conditions.add(df_operacion);	
											
							if (ig_numero_prestamo.equals("")) {
								hint = "/*+ ordered index( s IN_COM_SOLICITUD_05_NUK) use_nl(s ds d i p e m es tf)*/";
							}
						}
					}
					
					qrySentencia.append("SELECT "+hint+
								"	'1' AS PRODUCTO, "+
								" 'Descuento Electronico' AS NOMBREPRODUCTO, "+
								" s.ic_folio as NU_SOLICITUD, "+
								" i.cg_razon_social AS INTERMEDIARIO, "+
								" e.cg_razon_social AS NOMBRE_EPO,  "+
								" p.cg_razon_social AS NOMBRE_PYME, "+
								" m.cd_nombre as MONEDA  , "+
								" tf.cg_nombre AS TIPO_FACTORAJE, "+
								" TO_CHAR (s.df_operacion, 'DD/MM/YYYY') AS  FECHA_OPERACION, "+
								" s.ig_numero_prestamo as NUM_PRESTAMO,  "+
								" ds.in_importe_recibir as MONTO_OPERAR, "+
								" es.cd_descripcion as ESTATUS_ACTUAL, "+
								" 'Cadenas' AS ORIGEN "+	
								" , '' as SELECCION " +
								" FROM com_solicitud s, " +
								"  com_docto_seleccionado ds, " +
								"  com_documento d, " +
								"  comcat_if i, " +
								"  comcat_pyme p, " +
								"	 comcat_epo e, " +
								"  comcat_moneda m, " +
								"  comcat_estatus_solic es, " +
								"	 comcat_tipo_factoraje tf " +
								" WHERE s.ic_estatus_solic = 3 " +
								"  AND s.ic_bloqueo = 4 " +
								"  AND s.ic_documento = ds.ic_documento " +
								"  AND s.ic_documento = d.ic_documento " +
								"  AND s.ic_estatus_solic = es.ic_estatus_solic " +
								"  AND d.ic_if = i.ic_if " +
								"  AND d.ic_moneda = m.ic_moneda " +
								"  AND d.ic_pyme = p.ic_pyme " +
								"  AND d.ic_epo = e.ic_epo " +							
								"  AND d.cs_dscto_especial = tf.cc_tipo_factoraje ");
								qrySentencia.append(qryCondicion);
								qrySentencia.append(" ORDER BY S.df_operacion DESC ");
							
				} else if (ic_producto.equals("2"))  { // Anticipos
			
					qrySentencia.append(" SELECT "+
						" '2' AS PRODUCTO, "+
						" 'Financiamiento a Pedidos' AS NOMBREPRODUCTO, "+
						" A.ic_PEDIDO AS NU_SOLICITUD,  "+
						" I.cg_razon_social as INTERMEDIARIO,   "+
						" E.cg_razon_social as NOMBRE_EPO,   "+
						" P.cg_razon_social as NOMBRE_PYME,   "+
						" M.cd_nombre AS MONEDA ,  "+
						" 'N/A' AS TIPO_FACTORAJE,  "+
						" TO_CHAR(A.df_operacion, 'DD/MM/YYYY') as FECHA_OPERACION,  "+
						" A.ig_numero_prestamo AS NUM_PRESTAMO,  "+
						" PS.fn_credito_recibir AS MONTO_OPERAR, "+
						" ES.cd_descripcion AS ESTATUS_ACTUAL,  "+
						" 'Cadenas' as ORIGEN "+
						" , '' as SELECCION " +
						" FROM COM_ANTICIPO A,  "+
						" COM_CONTROLANT04 C4,  "+
						" COM_PEDIDO_SELECCIONADO PS, "+
						" COM_LINEA_CREDITO LC,  "+
						" COM_PEDIDO PED,  "+
						" COMCAT_IF I,  "+
						" COMCAT_PYME P, "+
						" COMCAT_EPO E,  "+
						" COMCAT_MONEDA M,  "+
						" COMCAT_ESTATUS_SOLIC ES  "+
						" WHERE A.ic_PEDIDO = C4.ic_PEDIDO  "+
						" AND A.ic_PEDIDO = PS.ic_PEDIDO  "+
						" AND PS.ic_PEDIDO = PED.ic_PEDIDO "+
						" AND LC.IC_LINEA_CREDITO=PS.IC_LINEA_CREDITO  "+
						" AND LC.ic_if = I.ic_if  "+
						" AND PED.ic_moneda = M.ic_moneda  "+
						" AND PED.ic_pyme = P.ic_pyme "+
						" AND PED.ic_epo = E.ic_epo  "+
						" AND A.ic_estatus_antic = ES.ic_estatus_solic "+
						" AND A.ic_estatus_antic = 3   "+
						" AND A.ic_bloqueo = 4  "+
						" AND C4.cs_estatus = 'S'  ");
						
							
					if ( ic_if.equals("") && ig_numero_prestamo.equals("") && df_operacion.equals("") ) {					
						qrySentencia.append( "	AND A.df_operacion >= TRUNC(SYSDATE) AND A.df_operacion <= TRUNC (SYSDATE)+1 "); //FOEA 047-2010 Rebos
					} else {
						if (!ic_if.equals("") && !ic_if.equals("0") ) {
							qrySentencia.append(" and LC.ic_if = ? ");
							conditions.add(ic_if);	  
						}
						if (!ig_numero_prestamo.equals("")) {
							qrySentencia.append("	AND A.ig_numero_prestamo IN(" + ig_numero_prestamo + ")"); //FODEA 047-2010 Rebos
						}
						if (!df_operacion.equals("")) {
							qrySentencia.append(" AND A.df_operacion >= to_date( ? , 'dd/mm/yyyy' )"+
											  " AND A.df_operacion < to_date( ?, 'dd/mm/yyyy') + 1");
											conditions.add(df_operacion);	
											conditions.add(df_operacion);	
						}
					}					
					qrySentencia.append(" ORDER BY A.df_operacion DESC ");
					
				} else if (ic_producto.equals("4")) { // Financiamiento a Distribuidores
					
									
					qrySentencia.append(
						" SELECT TO_CHAR(doc.ic_producto_nafin) AS PRODUCTO,"   +
						"     'Financiamiento a Distribuidores' AS NOMBREPRODUCTO,"   +
						"     TO_CHAR(doc.ic_documento) AS NU_SOLICITUD,"   +
						"     i.cg_razon_social AS INTERMEDIARIO,"   +
						"     epo.cg_razon_social AS NOMBRE_EPO,"   +
						"     p.cg_razon_social AS NOMBRE_PYME,"   +
						"     m.cd_nombre AS MONEDA ,"   +
						"     'N/A' AS TIPO_FACTORAJE,"   +
						"     TO_CHAR(sol.df_operacion, 'DD/MM/YYYY') AS FECHA_OPERACION,"   +
						"     sol.ig_numero_prestamo AS NUM_PRESTAMO ,"   +
						"     sel.fn_importe_recibir AS MONTO_OPERAR,"   +
						"     es.cd_descripcion AS ESTATUS_ACTUAL ,"   +
						"     'Cadenas' AS ORIGEN  "   +
						" , '' as SELECCION " +
						
						"  FROM comcat_moneda m, "   +
						"     comcat_epo epo, "   +
						"     comcat_pyme p, "   +
						"     comcat_if i, "   +
						"     comcat_estatus_solic es, "   +
						"     comcat_tasa tas, "   +
						"     dis_linea_credito_dm lc, "   +
						"     dis_documento doc, "   +
						"     dis_docto_seleccionado sel,     "   +
						"     dis_solicitud sol "   +
						"  WHERE es.ic_estatus_solic = sol.ic_estatus_solic "   +
						"     AND sol.ic_documento = doc.ic_documento "   +
						"     AND doc.ic_documento = sel.ic_documento"   +
						"     AND sel.ic_tasa = tas.ic_tasa "   +
						"     AND m.ic_moneda = tas.ic_moneda "   +
						"     AND doc.ic_epo = epo.ic_epo "   +
						"     AND doc.ic_linea_credito_dm = lc.ic_linea_credito_dm "   +
						"     AND lc.ic_if = i.ic_if "   +
						"     AND doc.ic_pyme = p.ic_pyme "   +
						"     AND sol.ic_estatus_solic = 3 "   +
						"     AND sol.ic_bloqueo = 4 ");
						
						if ( ic_if.equals("") && ig_numero_prestamo.equals("") && df_operacion.equals("") ) {					
							qrySentencia.append( "	AND sol.df_operacion >= TRUNC(SYSDATE) AND sol.df_operacion <= TRUNC (SYSDATE)+1 "); //FOEA 047-2010 Rebos
						} else {
							if (!ic_if.equals("") && !ic_if.equals("0") ) {
								qrySentencia.append( " and lc.ic_if = ? ");
								conditions.add(ic_if);	
							}
							if (!ig_numero_prestamo.equals("")) {
								qrySentencia.append( "	AND sol.ig_numero_prestamo IN(" + ig_numero_prestamo + ")"); //FODEA 047-2010 Rebos
							}
							if (!df_operacion.equals("")) {
								qrySentencia.append( " AND sol.df_operacion >= to_date(? , 'dd/mm/yyyy' )"
													+  " AND sol.df_operacion < to_date( ?, 'dd/mm/yyyy') + 1");
													conditions.add(df_operacion);
													conditions.add(df_operacion);
							}
						}	
									
						qrySentencia.append(" UNION    "   +
						" SELECT TO_CHAR(doc.ic_producto_nafin) AS PRODUCTO,"   +
						"     'Financiamiento a Distribuidores' AS NOMBREPRODUCTO,"   +
						"     TO_CHAR(doc.ic_documento) AS NU_SOLICITUD,"   +
						"     i.cg_razon_social AS INTERMEDIARIO,"   +
						"     epo.cg_razon_social AS NOMBRE_EPO,"   +
						"     p.cg_razon_social AS NOMBRE_PYME,"   +
						"     m.cd_nombre AS MONEDA ,"   +
						"     'N/A'  AS TIPO_FACTORAJE,"   +
						"     TO_CHAR(sol.df_operacion, 'DD/MM/YYYY') AS FECHA_OPERACION ,"   +
						"     sol.ig_numero_prestamo AS NUM_PRESTAMO ,"   +
						"     sel.fn_importe_recibir AS MONTO_OPERAR,"   +
						"     es.cd_descripcion AS ESTATUS_ACTUAL ,"   +
						"     'Cadenas' AS ORIGEN "   +
						" , '' as SELECCION " +
						"  FROM comcat_moneda m, "   +
						"     comcat_epo epo, "   +
						"     comcat_pyme p, "   +
						"     comcat_if i, "   +
						"     comcat_estatus_solic es, "   +
						"     comcat_tasa tas, "   +
						"     com_linea_credito lc, "   +
						"     dis_documento doc, "   +
						"     dis_docto_seleccionado sel,     "   +
						"     dis_solicitud sol "   +
						"  WHERE es.ic_estatus_solic = sol.ic_estatus_solic "   +
						"     AND sol.ic_documento = doc.ic_documento "   +
						"     AND doc.ic_documento = sel.ic_documento"   +
						"     AND sel.ic_tasa = tas.ic_tasa "   +
						"     AND m.ic_moneda = tas.ic_moneda "   +
						"     AND doc.ic_epo = epo.ic_epo "   +
						"     AND doc.ic_linea_credito = lc.ic_linea_credito "   +
						"     AND lc.ic_if = i.ic_if "   +
						"     AND doc.ic_pyme = p.ic_pyme "   +
						"     AND sol.ic_estatus_solic = 3 "   +
						"     AND sol.ic_bloqueo = 4" );
						
						if ( ic_if.equals("") && ig_numero_prestamo.equals("") && df_operacion.equals("") ) {					
							qrySentencia.append( "	AND sol.df_operacion >= TRUNC(SYSDATE) AND sol.df_operacion <= TRUNC (SYSDATE)+1 "); //FOEA 047-2010 Rebos
						} else {
							if (!ic_if.equals("") && !ic_if.equals("0") ) {
								qrySentencia.append( " and lc.ic_if = ? ");
								conditions.add(ic_if);	
							}
							if (!ig_numero_prestamo.equals("")) {
								qrySentencia.append( "	AND sol.ig_numero_prestamo IN(" + ig_numero_prestamo + ")"); //FODEA 047-2010 Rebos
							}
							if (!df_operacion.equals("")) {
								qrySentencia.append( " AND sol.df_operacion >= to_date(? , 'dd/mm/yyyy' )"
													+  " AND sol.df_operacion < to_date( ?, 'dd/mm/yyyy') + 1");
													conditions.add(df_operacion);
													conditions.add(df_operacion);
							}
						}	
									
						qrySentencia.append("  ORDER BY FECHA_OPERACION DESC"  );
						
				} else if (ic_producto.equals("5")) { // Credicadenas
					
					qrySentencia.append(
						" SELECT TO_CHAR(disp.ic_producto_nafin) AS PRODUCTO, "   +
						"     'Credicadenas' AS NOMBREPRODUCTO, "   +
						"     TO_CHAR(disp.cc_disposicion) AS NU_SOLICITUD, "   +
						"     i.cg_razon_social AS INTERMEDIARIO, "   +
						"     epo.cg_razon_social AS NOMBRE_EPO, "   +
						"     p.cg_razon_social AS NOMBRE_PYME,"   +
						"     m.cd_nombre AS  MONEDA , "   +
						"     'N/A'  AS TIPO_FACTORAJE , "   +
						"     TO_CHAR(sol.df_operacion, 'DD/MM/YYYY') AS FECHA_OPERACION,"   +
						"     sol.ig_numero_prestamo AS NUM_PRESTAMO, "   +
						"     disp.fn_monto_credito AS MONTO_OPERAR, "   +
						"     es.cd_descripcion AS ESTATUS_ACTUAL ,"   +
						"     'Cadenas' AS ORIGEN "   +
						" , '' as SELECCION " +
						"   FROM comcat_moneda m, "   +
						"     comcat_epo epo,   "   +
						"     comcat_pyme p, "   +
						"     comcat_if i, "   +
						"     comcat_estatus_solic es, "   +
						"     comcat_tasa tas,    "   +
						"     com_linea_credito lc,     "   +
						"     inv_disposicion disp,     "   +
						"     inv_solicitud sol "   +
						"  WHERE es.ic_estatus_solic = sol.ic_estatus_solic "   +
						"     AND sol.cc_disposicion = disp.cc_disposicion  "   +
						"     AND disp.ic_tasa = tas.ic_tasa"   +
						"     AND m.ic_moneda = tas.ic_moneda"   +
						"     AND disp.ic_epo = epo.ic_epo"   +
						"     AND disp.ic_linea_credito = lc.ic_linea_credito "   +
						"     AND lc.ic_if = i.ic_if "   +
						"     AND disp.ic_pyme = p.ic_pyme "   +
						"     AND sol.ic_estatus_solic = 3 "   +
						"     AND sol.ic_bloqueo = 4"  );
						
						
						if ( ic_if.equals("") && ig_numero_prestamo.equals("") && df_operacion.equals("") ) {					
						qrySentencia.append( "	AND sol.df_operacion >= TRUNC(SYSDATE) AND sol.df_operacion <= TRUNC (SYSDATE)+1 ");
						} else {
							if (!ic_if.equals("") && !ic_if.equals("0") ) {
								qrySentencia.append(" and lc.ic_if = ? ");
								conditions.add(ic_if);	
							}
							if (!ig_numero_prestamo.equals("")) {
								qrySentencia.append("	AND sol.ig_numero_prestamo IN(" + ig_numero_prestamo + ")"); 
							}
							if (!df_operacion.equals("")) {						
								qrySentencia.append(" AND sol.df_operacion >= to_date( ?, 'dd/mm/yyyy' )"
												+  " AND sol.df_operacion < to_date( ?, 'dd/mm/yyyy') + 1");
												conditions.add(df_operacion);	
												conditions.add(df_operacion);	
							}
						}	
						
						qrySentencia.append("  ORDER BY sol.df_operacion DESC") ;
						
				} else if (ic_producto.equals("8")) { // Factoraje con Recurso
				
					qrySentencia.append( "SELECT 'FR' AS PRODUCTO, "+
								"	 'Factoraje con Recurso' as NOMBREPRODUCTO, "+
								"	 TO_CHAR(s.ic_folio) NU_SOLICITUD, "+
								"    I.cg_razon_social as INTERMEDIARIO,  " +
								"    E.cg_razon_social as NOMBRE_EPO,  " +
								"    P.cg_razon_social as NOMBRE_PYME,  " +
								"    M.cd_nombre AS MONEDA , " +
								"    decode(D.cs_dscto_especial,'N','Normal', 'V', 'Vencido') as TIPO_FACTORAJE, " +
								"    TO_CHAR(S.df_operacion, 'DD/MM/YYYY') as FECHA_OPERACION, " +
								"    S.ig_numero_prestamo AS NUM_PRESTAMO , " +
								"    DS.in_importe_recibir AS MONTO_OPERAR , " +
								"    ES.cd_descripcion AS ESTATUS_ACTUAL , " +
								"    'Cadenas' as ORIGEN  " +
								" , '' as SELECCION " +
								" FROM COM_SOLICITUD S, " +
								"    COM_CONTROL04 C4, " +
								"    COM_DOCTO_SELECCIONADO DS, " +
								"    COM_DOCUMENTO D, " +
								"    EMP_LINEA_CREDITO LC, " +
								"    COMCAT_IF I, " +
								"    COMCAT_PYME P, " +
								"	COMCAT_EPO E, " +
								"    COMCAT_MONEDA M, " +
								"    COMCAT_ESTATUS_SOLIC ES " +
								" WHERE S.ic_folio = C4.ic_folio " +
								"    AND S.ic_estatus_solic = 3  " +
								"    AND S.ic_bloqueo = 4 " +
								"    AND C4.cs_estatus = 'S' " +
								"    AND S.ic_documento = DS.ic_documento " +
								"    AND DS.ic_documento = D.ic_documento " +
								"    AND DS.ic_linea_credito_emp = LC.IC_LINEA_CREDITO " +
								"    AND DS.ic_if = I.ic_if " +
								"    AND D.ic_moneda = M.ic_moneda " +
								"    AND D.ic_pyme = P.ic_pyme " +
								"	AND D.ic_epo = E.ic_epo " +
								"    AND S.ic_estatus_solic = ES.ic_estatus_solic" );
					if ( ic_if.equals("") && ig_numero_prestamo.equals("") && df_operacion.equals("") ) {
						qrySentencia.append("	AND S.df_operacion >= TRUNC(SYSDATE) AND S.df_operacion < TRUNC(SYSDATE+1) ");
					} else {
						if (!ic_if.equals("") && !ic_if.equals("0") ) {
							qrySentencia.append(" and ds.ic_if = ?  ");
							conditions.add(ic_if);	
						}
						if (!ig_numero_prestamo.equals("")) {
							qrySentencia.append("	AND S.ig_numero_prestamo IN(" + ig_numero_prestamo + ")"); 
						}
						if (!df_operacion.equals("")) {						
								qrySentencia.append(" AND s.df_operacion >= to_date(? , 'dd/mm/yyyy' )"
											+  " AND s.df_operacion < to_date(? , 'dd/mm/yyyy') + 1");
											conditions.add(df_operacion);	
											conditions.add(df_operacion);	
						}
					}
				
					qrySentencia.append(
						" UNION ALL "+
						" SELECT "+
						" 'FC' AS PRODUCTO, "+
						" 'Financiamiento a Contratos' AS NOMBREPRODUCTO, "+
						" TO_CHAR(A.ic_PEDIDO) AS IC_FOLIO,  "+
						" I.cg_razon_social as INTERMEDIARIO,   "+
						" E.cg_razon_social as NOMBRE_EPO,   "+
						" P.cg_razon_social as NOMBRE_PYME,   "+
						" M.cd_nombre AS MONEDA ,  "+
						" 'N/A' as TIPO_FACTORAJE,  "+
						" TO_CHAR(A.df_operacion, 'DD/MM/YYYY') as FECHA_OPERACION,  "+
						" A.ig_numero_prestamo AS NUM_PRESTAMO  ,  "+
						" PS.fn_credito_recibir AS MONTO_OPERAR, "+
						" ES.cd_descripcion AS ESTATUS_ACTUAL ,  "+
						" 'Cadenas' as ORIGEN "+
						" , '' as SELECCION " +
						" FROM COM_ANTICIPO A,  "+
						" COM_CONTROLANT04 C4,  "+
						" COM_PEDIDO_SELECCIONADO PS, "+
						" COM_LINEA_CREDITO LC,  "+
						" COM_PEDIDO PED,  "+
						" COMCAT_IF I,  "+
						" COMCAT_PYME P, "+
						" COMCAT_EPO E,  "+
						" COMCAT_MONEDA M,  "+
						" COMCAT_ESTATUS_SOLIC ES  "+
						" WHERE A.ic_PEDIDO = C4.ic_PEDIDO  "+
						" AND A.ic_PEDIDO = PS.ic_PEDIDO  "+
						" AND PS.ic_PEDIDO = PED.ic_PEDIDO "+
						" AND LC.IC_LINEA_CREDITO=PS.IC_LINEA_CREDITO  "+
						" AND LC.ic_if = I.ic_if  "+
						" AND PED.ic_moneda = M.ic_moneda  "+
						" AND PED.ic_pyme = P.ic_pyme "+
						" AND PED.ic_epo = E.ic_epo  "+
						" AND A.ic_estatus_antic = ES.ic_estatus_solic "+
						" AND A.ic_estatus_antic = 3   "+
						" AND A.ic_bloqueo = 4  "+
						" AND C4.cs_estatus = 'S'  ");
								
					if ( ic_if.equals("") && ig_numero_prestamo.equals("") && df_operacion.equals("") ) {
						qrySentencia.append( "	AND A.df_operacion >= TRUNC(SYSDATE) "+
							"	AND A.df_operacion < TRUNC(SYSDATE+1) ");
					} else {
						if (!ic_if.equals("") && !ic_if.equals("0") ) {
							qrySentencia.append(" and LC.ic_if = ? " );
							conditions.add(ic_if);	
						}
						if (!ig_numero_prestamo.equals("")) {
							qrySentencia.append("	AND A.ig_numero_prestamo IN(" + ig_numero_prestamo + ")"); //FODEA 047-2010 Rebos
						}
						if (!df_operacion.equals("")) {
							qrySentencia.append(	"	AND A.df_operacion >= TO_DATE(?, 'DD/MM/YYYY') "+
														 "	AND A.df_operacion < TO_DATE( ?, 'DD/MM/YYYY')+1 ");
														 conditions.add(df_operacion);	
														conditions.add(df_operacion);	
						}
					}
					
					qrySentencia.append(
						" UNION ALL "   +
						" SELECT decode(disp.cg_exp_emp,'S', 'FE', 'LD' ) AS PRODUCTO, "   +
						"     decode(disp.cg_exp_emp,'S', 'Financiamiento de Exportacion', 'Libre Disposicion' ) AS NOMBREPRODUCTO , "   +
						"     TO_CHAR(disp.cc_disposicion) AS NU_SOLICITUD, "   +
						"     i.cg_razon_social AS INTERMEDIARIO, "   +
						"     epo.cg_razon_social AS NOMBRE_EPO, "   +
						"     p.cg_razon_social AS NOMBRE_PYME,"   +
						"     m.cd_nombre AS MONEDA , "   +
						"     'N/A' AS TIPO_FACTORAJE, "   +
						"     TO_CHAR(sol.df_operacion, 'DD/MM/YYYY') AS FECHA_OPERACION,"   +
						"     sol.ig_numero_prestamo AS NUM_PRESTAMO  , "   +
						"     disp.fn_monto_credito AS MONTO_OPERAR, "   +
						"     es.cd_descripcion AS ESTATUS_ACTUAL ,"   +
						"     'Cadenas' AS ORIGEN "   +
						" , '' as SELECCION " +
						"   FROM comcat_moneda m, "   +
						"     comcat_epo epo,   "   +
						"     comcat_pyme p, "   +
						"     comcat_if i, "   +
						"     comcat_estatus_solic es, "   +
						"     comcat_tasa tas,    "   +
						"     com_linea_credito lc,     "   +
						"     inv_disposicion disp,     "   +
						"     inv_solicitud sol "   +
						"  WHERE es.ic_estatus_solic = sol.ic_estatus_solic "   +
						"     AND sol.cc_disposicion = disp.cc_disposicion  "   +
						"     AND disp.ic_tasa = tas.ic_tasa"   +
						"     AND m.ic_moneda = tas.ic_moneda"   +
						"     AND disp.ic_epo = epo.ic_epo"   +
						"     AND disp.ic_linea_credito = lc.ic_linea_credito "   +
						"     AND lc.ic_if = i.ic_if "   +
						"     AND disp.ic_pyme = p.ic_pyme "   +
						"     AND sol.ic_estatus_solic = 3 "   +
						"     AND sol.ic_bloqueo = 4" );
						
						if ( ic_if.equals("") && ig_numero_prestamo.equals("") && df_operacion.equals("") ) {
						qrySentencia.append(	"	AND sol.df_operacion >= TRUNC(SYSDATE) "+	"	AND sol.df_operacion < TRUNC(SYSDATE+1) ");
					} else {
						if (!ic_if.equals("") && !ic_if.equals("0") ) {
							qrySentencia.append(" and lc.ic_if = ? ");
							conditions.add(ic_if);	
						}
						if (!ig_numero_prestamo.equals("")) {
							qrySentencia.append("	AND sol.ig_numero_prestamo IN(" + ig_numero_prestamo + ")");
						}
						if (!df_operacion.equals("")) {
							qrySentencia.append("	AND sol.df_operacion >= TO_DATE( ? , 'DD/MM/YYYY') "+
													  "	AND sol.df_operacion < TO_DATE(? , 'DD/MM/YYYY')+1 ");
													  conditions.add(df_operacion);	
													conditions.add(df_operacion);	
						}
					}
					
						qrySentencia.append("  ORDER BY df_operacion DESC" ) ;
				}// Fin de la creacion de la consulta por producto
				
				
			}else  if (pantalla.equals("Procesados"))  {
				
				if (ic_producto.equals("0")) {   // Credito
					
					qrySentencia.append(" SELECT "+
						" '0' AS PRODUCTO, "+
						" 'Credito Electronico' AS NOMBREPRODUCTO,  "+
						" SP.IC_SOLIC_PORTAL AS NU_SOLICITUD,  "+
						" I.cg_razon_social as INTERMEDIARIO,   "+
						" 'N/A' as NOMBRE_EPO, "+
						" P.cg_razon_social as NOMBRE_PYME,   "+
						" M.cd_nombre as MONEDA ,  "+
						" 'N/A' as TIPO_FACTORAJE,  "+
						" TO_CHAR(SP.df_operacion, 'DD/MM/YYYY') as FECHA_OPERACION,  "+
						" SP.ig_numero_prestamo as NUM_PRESTAMO ,  "+
						" SP.FN_IMPORTE_DSCTO AS MONTO_OPERAR, "+
						" 'Rechazada' as ESTATUS_ACTUAL,  "+
						" 'Credito' as ORIGEN "+
						" from   "+
						" comcat_moneda m, "+
						" comcat_pyme p,   "+
						" comrel_nafin n,   "+
						" comcat_if i,   "+
						" com_interfase inter,   "+
						" com_solic_portal sp   "+
						" where  "+
						" sp.IC_SOLIC_PORTAL = inter.IC_SOLIC_PORTAL and   "+
						" m.ic_moneda=sp.ic_moneda and  "+
						" sp.IC_IF = i.IC_IF and  "+
						" sp.IG_CLAVE_SIRAC = p.IN_NUMERO_SIRAC (+) and   "+
						" n.IC_EPO_PYME_IF (+) = p.IC_PYME and  "+
						" n.CG_TIPO (+) = 'P' and  "+ 
						" sp.ic_solic_portal in ("+solicitudes +") and "+
						" sp.IC_ESTATUS_SOLIC=3 "+
						" ORDER BY SP.df_operacion DESC ");
						
					} else if (ic_producto.equals("1"))   { // Descuento
		
					qrySentencia.append(" SELECT "+
									"   '0' AS PRODUCTO ,"+
									"   'Descuento Electronico' as NOMBREPRODUCTO, "+
									"    S.ic_folio as NU_SOLICITUD , "+
									"    I.cg_razon_social as INTERMEDIARIO,  " +
									"    E.cg_razon_social as NOMBRE_EPO,  " +
									"    P.cg_razon_social as NOMBRE_PYME,  " +
									"    M.cd_nombre as  MONEDA , " +
									"    decode(D.cs_dscto_especial,'N','Normal', 'V', 'Vencido') as TIPO_FACTORAJE, " +
									"    TO_CHAR(S.df_operacion, 'DD/MM/YYYY') as FECHA_OPERACION, " +
									"    S.ig_numero_prestamo as NUM_PRESTAMO , " +
									"    DS.in_importe_recibir as MONTO_OPERAR , " +
									"    'Seleccionada IF' as ESTATUS_ACTUAL, " +
									"    'Cadenas' as ORIGEN " +
									" FROM COM_SOLICITUD S, " +
									"    COM_DOCTO_SELECCIONADO DS, " +
									"    COM_DOCUMENTO D, " +
									"    COMCAT_IF I, " +
									"    COMCAT_PYME P, " +
									"	COMCAT_EPO E, " +
									"    COMCAT_MONEDA M, " +
									"    COMCAT_ESTATUS_SOLIC ES " +
									" WHERE S.ic_documento = DS.ic_documento " +
									"    AND DS.ic_documento = D.ic_documento " +
									"    AND DS.ic_if = I.ic_if " +
									"    AND D.ic_moneda = M.ic_moneda " +
									"    AND D.ic_pyme = P.ic_pyme " +
									"	 AND D.ic_epo = E.ic_epo " +
									"    AND S.ic_estatus_solic = ES.ic_estatus_solic " +
									"	 AND S.ic_folio in ("+ solicitudes+") "+
									" ORDER BY S.df_operacion DESC ");
									
				} else if (ic_producto.equals("2")) { // Anticipo
				
						qrySentencia.append(" SELECT "+
							" '2' AS PRODUCTO, "+
							" 'Financiamiento a Pedidos' AS NOMBREPRODUCTO, "+
							" A.ic_PEDIDO AS NU_SOLICITUD,  "+
							" I.cg_razon_social as INTERMEDIARIO,   "+
							" E.cg_razon_social as NOMBRE_EPO,   "+
							" P.cg_razon_social as NOMBRE_PYME,   "+
							" M.cd_nombre as MONEDA ,  "+
							" 'N/A' as TIPO_FACTORAJE,  "+
							" TO_CHAR(A.df_operacion, 'DD/MM/YYYY') as FECHA_OPERACION,  "+
							" A.ig_numero_prestamo as NUM_PRESTAMO,  "+
							" PS.fn_credito_recibir AS IN_IMPORTE_RECIBIR, "+
							" 'Seleccionada IF' as ESTATUS_ACTUAL,  "+
							" 'Cadenas' as ORIGEN "+
							" FROM COM_ANTICIPO A,  "+
							" COM_CONTROLANT04 C4,  "+
							" COM_PEDIDO_SELECCIONADO PS, "+
							" COM_LINEA_CREDITO LC,  "+
							" COM_PEDIDO PED,  "+
							" COMCAT_IF I,  "+
							" COMCAT_PYME P, "+
							" COMCAT_EPO E,  "+
							" COMCAT_MONEDA M,  "+
							" COMCAT_ESTATUS_SOLIC ES  "+
							" WHERE A.ic_PEDIDO = C4.ic_PEDIDO  "+
							" AND A.ic_PEDIDO = PS.ic_PEDIDO  "+
							" AND PS.ic_PEDIDO = PED.ic_PEDIDO "+
							" AND LC.IC_LINEA_CREDITO=PS.IC_LINEA_CREDITO  "+
							" AND LC.ic_if = I.ic_if  "+
							" AND PED.ic_moneda = M.ic_moneda  "+
							" AND PED.ic_pyme = P.ic_pyme "+
							" AND PED.ic_epo = E.ic_epo  "+
							" AND A.ic_estatus_antic = ES.ic_estatus_solic "+
							" AND A.ic_bloqueo = 4  "+
							" AND C4.cs_estatus = 'S'  "+
							" AND A.IC_PEDIDO IN ("+ solicitudes+") "+
							" AND A.ic_estatus_antic = 3   "+
							" ORDER BY A.df_operacion DESC ");		
							
				} else if (ic_producto.equals("4"))  { // Financiamiento a Distribuidores
				
					qrySentencia.append(	" SELECT TO_CHAR(doc.ic_producto_nafin) AS PRODUCTO ,"   +
							"     'Financiamiento a Distribuidores' AS NOMBREPRODUCTO,"   +
							"     TO_CHAR(doc.ic_documento) AS NU_SOLICITUD,"   +
							"     i.cg_razon_social AS INTERMEDIARIO,"   +
							"     epo.cg_razon_social AS NOMBRE_EPO,"   +
							"     p.cg_razon_social AS NOMBRE_PYME,"   +
							"     m.cd_nombre as MONEDA ,"   +
							"     'N/A' AS TIPO_FACTORAJE,"   +
							"     TO_CHAR(sol.df_operacion, 'DD/MM/YYYY') AS FECHA_OPERACION,"   +
							"     sol.ig_numero_prestamo as NUM_PRESTAMO ,"   +
							"     sel.fn_importe_recibir AS MONTO_OPERAR,"   +
							"     'Seleccionada IF' as ESTATUS_ACTUAL,"   +
							"     'Cadenas' AS ORIGEN "   +
							"  FROM comcat_moneda m, "   +
							"     comcat_epo epo, "   +
							"     comcat_pyme p, "   +
							"     comcat_if i, "   +
							"     comcat_estatus_solic es, "   +
							"     comcat_tasa tas, "   +
							"     dis_linea_credito_dm lc, "   +
							"     dis_documento doc, "   +
							"     dis_docto_seleccionado sel,     "   +
							"     dis_solicitud sol "   +
							"  WHERE es.ic_estatus_solic = sol.ic_estatus_solic "   +
							"     AND sol.ic_documento = doc.ic_documento "   +
							"     AND doc.ic_documento = sel.ic_documento"   +
							"     AND sel.ic_tasa = tas.ic_tasa "   +
							"     AND m.ic_moneda = tas.ic_moneda "   +
							"     AND doc.ic_epo = epo.ic_epo "   +
							"     AND doc.ic_linea_credito_dm = lc.ic_linea_credito_dm "   +
							"     AND lc.ic_if = i.ic_if "   +
							"     AND doc.ic_pyme = p.ic_pyme "   +
							"     AND sol.ic_estatus_solic = 3 "   +
							"     AND sol.ic_bloqueo = 4 "   + 
							" 	  AND sol.ic_documento in ("+solicitudes+") "+
							" UNION    "   +
							" SELECT TO_CHAR(doc.ic_producto_nafin) AS PRODUCTO,"   +
							"     'Financiamiento a Distribuidores' AS NOMBREPRODUCTO,"   +
							"     TO_CHAR(doc.ic_documento) AS NU_SOLICITUD,"   +
							"     i.cg_razon_social AS INTERMEDIARIO,"   +
							"     epo.cg_razon_social AS NOMBRE_EPO,"   +
							"     p.cg_razon_social AS NOMBRE_PYME,"   +
							"     m.cd_nombre as MONEDA ,"   +
							"     'N/A' AS TIPO_FACTORAJE,"   +
							"     TO_CHAR(sol.df_operacion, 'DD/MM/YYYY') AS FECHA_OPERACION,"   +
							"     sol.ig_numero_prestamo as NUM_PRESTAMO ,"   +
							"     sel.fn_importe_recibir AS MONTO_OPERAR,"   +
							"     'Seleccionada IF' as ESTATUS_ACTUAL,"   +
							"     'Cadenas' AS ORIGEN "   +
							"  FROM comcat_moneda m, "   +
							"     comcat_epo epo, "   +
							"     comcat_pyme p, "   +
							"     comcat_if i, "   +
							"     comcat_estatus_solic es, "   +
							"     comcat_tasa tas, "   +
							"     com_linea_credito lc, "   +
							"     dis_documento doc, "   +
							"     dis_docto_seleccionado sel,     "   +
							"     dis_solicitud sol "   +
							"  WHERE es.ic_estatus_solic = sol.ic_estatus_solic "   +
							"     AND sol.ic_documento = doc.ic_documento "   +
							"     AND doc.ic_documento = sel.ic_documento"   +
							"     AND sel.ic_tasa = tas.ic_tasa "   +
							"     AND m.ic_moneda = tas.ic_moneda "   +
							"     AND doc.ic_epo = epo.ic_epo "   +
							"     AND doc.ic_linea_credito = lc.ic_linea_credito "   +
							"     AND lc.ic_if = i.ic_if "   +
							"     AND doc.ic_pyme = p.ic_pyme "   +
							"     AND sol.ic_estatus_solic = 3 "   +
							"     AND sol.ic_bloqueo = 4"  + 
							" 	  AND sol.ic_documento in ("+solicitudes+") "+
							"  ORDER BY FECHA_OPERACION DESC")  ;
							
				} else if (ic_producto.equals("5"))  { // Credicadenas
				
						qrySentencia.append(
						" SELECT TO_CHAR(disp.ic_producto_nafin) AS PRODUCTO, "   +
							"     'Credicadenas' AS NOMBREPRODUCTO, "   +
							"     TO_CHAR(disp.cc_disposicion) AS NU_SOLICITUD, "   +
							"     i.cg_razon_social AS INTERMEDIARIO, "   +
							"     epo.cg_razon_social AS NOMBRE_EPO, "   +
							"     p.cg_razon_social AS NOMBRE_PYME,"   +
							"     m.cd_nombre as MONEDA, "   +
							"     'N/A' AS TIPO_FACTORAJE, "   +
							"     TO_CHAR(sol.df_operacion, 'DD/MM/YYYY') AS FECHA_OPERACION,"   +
							"     sol.ig_numero_prestamo as NUM_PRESTAMO , "   +
							"     disp.fn_monto_credito AS MONTO_OPERAR, "   +
							"     'Seleccionada IF' as ESTATUS_ACTUAL,"   +
							"     'Cadenas' AS ORIGEN "   +
							"   FROM comcat_moneda m, "   +
							"     comcat_epo epo,   "   +
							"     comcat_pyme p, "   +
							"     comcat_if i, "   +
							"     comcat_estatus_solic es, "   +
							"     comcat_tasa tas,    "   +
							"     com_linea_credito lc,     "   +
							"     inv_disposicion disp,     "   +
							"     inv_solicitud sol "   +
							"  WHERE es.ic_estatus_solic = sol.ic_estatus_solic "   +
							"     AND sol.cc_disposicion = disp.cc_disposicion  "   +
							"     AND disp.ic_tasa = tas.ic_tasa"   +
							"     AND m.ic_moneda = tas.ic_moneda"   +
							"     AND disp.ic_epo = epo.ic_epo"   +
							"     AND disp.ic_linea_credito = lc.ic_linea_credito "   +
							"     AND lc.ic_if = i.ic_if "   +
							"     AND disp.ic_pyme = p.ic_pyme "   +
							"     AND sol.ic_estatus_solic = 3 "   +
							"     AND sol.ic_bloqueo = 4"   +
							" 	  AND sol.cc_disposicion in ("+solicitudes+") "+
							"  ORDER BY sol.df_operacion DESC")  ;
							
					} else if (ic_producto.equals("8")) { // Credicadenas
						
						if(solicitudes!=null) {
							qrySentencia.append( 
								"SELECT "+
								"   'FR' AS PRODUCTO ,"+
								"   'Factoraje con Recurso' as NOMBREPRODUCTO, "+
								"    S.ic_folio as NU_SOLICITUD , "+
								"    I.cg_razon_social as INTERMEDIARIO,  " +
								"    E.cg_razon_social as NOMBRE_EPO,  " +
								"    P.cg_razon_social as NOMBRE_PYME,  " +
								"    M.cd_nombre as MONEDA , " +
								"    decode(D.cs_dscto_especial,'N','Normal', 'V', 'Vencido') as TIPO_FACTORAJE, " +
								"    TO_CHAR(S.df_operacion, 'DD/MM/YYYY') as FECHA_OPERACION, " +
								"    S.ig_numero_prestamo as NUM_PRESTAMO , " +
								"    DS.in_importe_recibir as MONTO_OPERAR , " +
								"    'Seleccionada IF' as ESTATUS_ACTUAL, " +
								"    'Cadenas' as ORIGEN " +
								" FROM COM_SOLICITUD S, " +
								"    COM_DOCTO_SELECCIONADO DS, " +
								"    COM_DOCUMENTO D, " +
								"    COMCAT_IF I, " +
								"    COMCAT_PYME P, " +
								"	COMCAT_EPO E, " +
								"    COMCAT_MONEDA M, " +
								"    COMCAT_ESTATUS_SOLIC ES " +
								" WHERE S.ic_documento = DS.ic_documento " +
								"    AND DS.ic_documento = D.ic_documento " +
								"    AND DS.ic_if = I.ic_if " +
								"    AND D.ic_moneda = M.ic_moneda " +
								"    AND D.ic_pyme = P.ic_pyme " +
								"	   AND D.ic_epo = E.ic_epo " +
								"    AND S.ic_estatus_solic = ES.ic_estatus_solic " +
								"	 AND S.ic_folio in ("+ solicitudes+") ");
						}
						if(solicitudes!=null) {
							if(!"".equals(qrySentencia.toString())) qrySentencia.append(" UNION ALL ");
							qrySentencia.append(
								" SELECT "+
								" 'FC' AS PRODUCTO, "+
								" 'Financiamiento a Contratos' AS NOMBREPRODUCTO, "+
								" A.ic_PEDIDO AS NU_SOLICITUD,  "+
								" I.cg_razon_social as INTERMEDIARIO,   "+
								" E.cg_razon_social as NOMBRE_EPO,   "+
								" P.cg_razon_social as NOMBRE_PYME,   "+
								" M.cd_nombre as MONEDA ,  "+
								" 'N/A' as TIPO_FACTORAJE,  "+
								" TO_CHAR(A.df_operacion, 'DD/MM/YYYY') as FECHA_OPERACION,  "+
								" A.ig_numero_prestamo as NUM_PRESTAMO ,  "+
								" PS.fn_credito_recibir AS MONTO_OPERAR, "+
								" 'Seleccionada IF' as ESTATUS_ACTUAL,  "+
								" 'Cadenas' as ORIGEN "+
								" FROM COM_ANTICIPO A,  "+
								" COM_CONTROLANT04 C4,  "+
								" COM_PEDIDO_SELECCIONADO PS, "+
								" COM_LINEA_CREDITO LC,  "+
								" COM_PEDIDO PED,  "+
								" COMCAT_IF I,  "+
								" COMCAT_PYME P, "+
								" COMCAT_EPO E,  "+
								" COMCAT_MONEDA M,  "+
								" COMCAT_ESTATUS_SOLIC ES  "+
								" WHERE A.ic_PEDIDO = C4.ic_PEDIDO  "+
								" AND A.ic_PEDIDO = PS.ic_PEDIDO  "+
								" AND PS.ic_PEDIDO = PED.ic_PEDIDO "+
								" AND LC.IC_LINEA_CREDITO=PS.IC_LINEA_CREDITO  "+
								" AND LC.ic_if = I.ic_if  "+
								" AND PED.ic_moneda = M.ic_moneda  "+
								" AND PED.ic_pyme = P.ic_pyme "+
								" AND PED.ic_epo = E.ic_epo  "+
								" AND A.ic_estatus_antic = ES.ic_estatus_solic "+
								" AND A.ic_bloqueo = 4  "+
								" AND C4.cs_estatus = 'S'  "+
								" AND A.IC_PEDIDO IN ("+ solicitudes+") "+
								" AND A.ic_estatus_antic = 3   ");
						}
						if(solicitudes!=null) {
							if(!"".equals(qrySentencia.toString())) qrySentencia.append(" UNION ALL ");
							
							qrySentencia.append(
								" SELECT 'DS' AS PRODUCTO, "   +
								"     decode(disp.cg_exp_emp,'S', 'Financiamiento de Exportacion', 'Libre Disposicion' ) AS NOMBREPRODUCTO, "   +
								"     TO_CHAR(disp.cc_disposicion) AS NU_SOLICITUD, "   +
								"     i.cg_razon_social AS INTERMEDIARIO, "   +
								"     epo.cg_razon_social AS NOMBRE_EPO, "   +
								"     p.cg_razon_social AS NOMBRE_PYME,"   +
								"     m.cd_nombre as  MONEDA , "   +
								"     'N/A' AS TIPO_FACTORAJE, "   +
								"     TO_CHAR(sol.df_operacion, 'DD/MM/YYYY') AS FECHA_OPERACION,"   +
								"     sol.ig_numero_prestamo as NUM_PRESTAMO , "   +
								"     disp.fn_monto_credito AS MONTO_OPERAR, "   +
								"     'Seleccionada IF' as ESTATUS_ACTUAL,"   +
								"     'Cadenas' AS ORIGEN "   +
								"   FROM comcat_moneda m, "   +
								"     comcat_epo epo,   "   +
								"     comcat_pyme p, "   +
								"     comcat_if i, "   +
								"     comcat_estatus_solic es, "   +
								"     comcat_tasa tas,    "   +
								"     com_linea_credito lc,     "   +
								"     inv_disposicion disp,     "   +
								"     inv_solicitud sol "   +
								"  WHERE es.ic_estatus_solic = sol.ic_estatus_solic "   +
								"     AND sol.cc_disposicion = disp.cc_disposicion  "   +
								"     AND disp.ic_tasa = tas.ic_tasa"   +
								"     AND m.ic_moneda = tas.ic_moneda"   +
								"     AND disp.ic_epo = epo.ic_epo"   +
								"     AND disp.ic_linea_credito = lc.ic_linea_credito "   +
								"     AND lc.ic_if = i.ic_if "   +
								"     AND disp.ic_pyme = p.ic_pyme "   +
								"     AND sol.ic_estatus_solic = 3 "   +
								"     AND sol.ic_bloqueo = 4"   +
								" 	  AND sol.cc_disposicion in ("+solicitudes+") ");
						}
						qrySentencia.append(" ORDER BY S.df_operacion DESC ");
					}// Fin de la creacion de la consulta por producto
							
			}
			
			
		log.debug("ic_producto  ---->"+ic_producto);		
		log.debug("qrySentencia  "+qrySentencia);
		log.debug("conditions "+conditions);
		
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
			
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		int total =0;
		try {
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
            (String) session.getAttribute("strNombre"),
            (String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
 
				pdfDoc.setTable(11,100);										
				pdfDoc.setCell("Producto","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Intermediario","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("PYME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Solicitud","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER); 			
				pdfDoc.setCell("Tipo de Factoraje","celda01",ComunesPDF.CENTER); 			
				pdfDoc.setCell("Fecha Operaci�n SIRAC","celda01",ComunesPDF.CENTER); 			
				pdfDoc.setCell("N�mero de Pr�stamo","celda01",ComunesPDF.CENTER); 			
				pdfDoc.setCell("Monto a Operar","celda01",ComunesPDF.CENTER); 			
				pdfDoc.setCell("Estatus Actual","celda01",ComunesPDF.CENTER); 			
				
				while (rs.next())	{
					String nombreProducto = (rs.getString("NOMBREPRODUCTO") == null) ? "" : rs.getString("NOMBREPRODUCTO");
					String intemediario = (rs.getString("INTERMEDIARIO") == null) ? "" : rs.getString("INTERMEDIARIO");
					String nombreEPO = (rs.getString("NOMBRE_EPO") == null) ? "" : rs.getString("NOMBRE_EPO");
					String nombrePyme = (rs.getString("NOMBRE_PYME") == null) ? "" : rs.getString("NOMBRE_PYME");
					String noSolicitud = (rs.getString("NU_SOLICITUD") == null) ? "" : rs.getString("NU_SOLICITUD");
					String moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
					String tipoFactoraje = (rs.getString("TIPO_FACTORAJE") == null) ? "" : rs.getString("TIPO_FACTORAJE");
					String fechaOperacion = (rs.getString("FECHA_OPERACION") == null) ? "" : rs.getString("FECHA_OPERACION");
					String no_prestamo = (rs.getString("NUM_PRESTAMO") == null) ? "" : rs.getString("NUM_PRESTAMO");
					String montoOperar = (rs.getString("MONTO_OPERAR") == null) ? "" : rs.getString("MONTO_OPERAR");
					String estatus = (rs.getString("ESTATUS_ACTUAL") == null) ? "" : rs.getString("ESTATUS_ACTUAL");					
					
					pdfDoc.setCell(nombreProducto,"formas",ComunesPDF.LEFT);	
					pdfDoc.setCell(intemediario,"formas",ComunesPDF.LEFT);	
					pdfDoc.setCell(nombreEPO,"formas",ComunesPDF.LEFT);	
					pdfDoc.setCell(nombrePyme,"formas",ComunesPDF.LEFT);	
					pdfDoc.setCell(noSolicitud,"formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(tipoFactoraje,"formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(fechaOperacion,"formas",ComunesPDF.CENTER);	
					pdfDoc.setCell(no_prestamo,"formas",ComunesPDF.CENTER);	
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoOperar,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(estatus,"formas",ComunesPDF.CENTER);	
					
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();	
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
	


	public String  procesarCancela(int numRegistros, String []  folios,  String  ic_producto,  String []  productos ){
		
		log.info("procesarCancela (E)");
	
		AccesoDB con =new AccesoDB();
		PreparedStatement ps	= null;
		ResultSet			rs1	= null;	
		String mensaje ="", qrySentencia ="";
		int numRegistrosAfectar =0;
		boolean errorConcurrencia=true;

		try{
		
			con.conexionDB();
			
			for(int i=0; i<numRegistros; i++) {
		
				String solicitud =   folios[i];				
				String producto =   productos[i];
									
				if (ic_producto.equals("1")|| producto.equals("FR"))	{	
					qrySentencia = " SELECT count(*) "+
						 " FROM com_solicitud "+
						 " WHERE ic_folio ='"+solicitud+"'"+
						 "	AND ic_estatus_solic = 3";
						 
					} else if (ic_producto.equals("2")||producto.equals("FC"))	{	
						qrySentencia = " SELECT count(*) "+
						 " FROM com_anticipo "+
						 " WHERE ic_pedido ='"+solicitud+"'"+
						 "	AND ic_estatus_antic = 3";
						 
					} else if (ic_producto.equals("0"))	{	
						qrySentencia = " SELECT count(*) "+
						 " FROM com_solic_portal "+
						 " WHERE ic_solic_portal ='"+solicitud+"'"+
						 "	AND ic_estatus_solic = 3";
						 
					} else if (ic_producto.equals("4"))	{	
						qrySentencia = " SELECT count(*) "+
						 " FROM dis_solicitud "+
						 " WHERE ic_documento ='"+solicitud+"'"+
						 "	AND ic_estatus_solic = 3";
						 
					} else if (ic_producto.equals("5")||producto.equals("DS"))	{
						qrySentencia = " SELECT count(*) "+
						 " FROM inv_solicitud "+
						 " WHERE CC_DISPOSICION ='"+solicitud+"'"+
						 "	AND ic_estatus_solic = 3";
					}
					rs1 = con.queryDB(qrySentencia);
					numRegistrosAfectar=0;
					while (rs1.next()) {
						numRegistrosAfectar = numRegistrosAfectar + rs1.getInt(1);
					}
					rs1.close();
					con.cierraStatement();
					
					if (numRegistrosAfectar==1)	{ //Control de concurrencia (Debe existir el registro en la tabla para poder ser reprocesado o cancelado)
						errorConcurrencia=false;					
						if (ic_producto.equals("1")||producto.equals("FR"))		{
							qrySentencia = "UPDATE com_solicitud " + 
								" SET ic_estatus_solic=1, "+
								"	ic_bloqueo = 5, "+
								"	df_operacion = null,"+
								"	ig_numero_prestamo = null, "+
								"	df_cancelacion = SYSDATE "+ //FODEA 047-2010 Rebos
								" WHERE ic_folio ='"+solicitud+"'";														
							con.ejecutaSQL(qrySentencia);
							
							qrySentencia = "delete from com_control04"+
								" where ic_folio ='"+solicitud+"'";						
							con.ejecutaSQL(qrySentencia);
							
						} else if (ic_producto.equals("2")||producto.equals("FC")) 	{
							qrySentencia = "UPDATE com_anticipo " + 
								" SET ic_estatus_antic=1, "+
								"	ic_bloqueo = 5, "+
								"	df_operacion = null, "+
								"	ig_numero_prestamo = null, "+
								"	df_cancelacion = SYSDATE "+ //FODEA 047-2010 Rebos
								" WHERE ic_pedido ='"+solicitud+"'";							
							con.ejecutaSQL(qrySentencia);
							
							qrySentencia = "delete from com_controlant04"+
								" where ic_pedido ='"+solicitud+"'";
							con.ejecutaSQL(qrySentencia);						
							
						} else if (ic_producto.equals("0"))	{
							qrySentencia = " UPDATE com_solic_portal "+
								" SET ic_bloqueo=5, ic_estatus_solic=4, "+
								"	df_cancelacion = SYSDATE "+ //FODEA 047-2010 Rebos
								" WHERE ic_solic_portal = '"+solicitud+"'";
							con.ejecutaSQL(qrySentencia);
							
							qrySentencia = "delete com_interfase "+
								" where ic_solic_portal = '"+solicitud+"'";
							con.ejecutaSQL(qrySentencia);		
							
						} else if (ic_producto.equals("4"))	{ 
						
							qrySentencia = " UPDATE dis_solicitud "+
								" SET ic_bloqueo=5, ic_estatus_solic=1, "+
								" df_operacion=null, ig_numero_prestamo=null, "+	
								"	df_cancelacion = SYSDATE "+ //FODEA 047-2010 Rebos
								" WHERE ic_documento = '"+solicitud+"'";
							con.ejecutaSQL(qrySentencia);
						
							qrySentencia = "delete dis_interfase "+
								" where ic_documento = '"+solicitud+"'";
							con.ejecutaSQL(qrySentencia);		
						
							qrySentencia = "delete dishis_interfase "+
								" where ic_documento = '"+solicitud+"'";
							con.ejecutaSQL(qrySentencia);		
						
						} else if (ic_producto.equals("5")||producto.equals("DS"))	{	
							
							qrySentencia = " UPDATE inv_solicitud "+
								" SET ic_bloqueo=5, ic_estatus_solic=1, "+
								" df_operacion=null, ig_numero_prestamo=null, "+							
								"	df_cancelacion = SYSDATE "+ //FODEA 047-2010 Rebos
								" WHERE CC_DISPOSICION = '"+solicitud+"'";
							con.ejecutaSQL(qrySentencia);
							
							qrySentencia = "delete inv_interfase "+
								" where CC_DISPOSICION = '"+solicitud+"'";
							con.ejecutaSQL(qrySentencia);		
							
							qrySentencia = "delete invhis_interfase "+
								" where CC_DISPOSICION = '"+solicitud+"'";
							con.ejecutaSQL(qrySentencia);		
							
						}
					} else {	//Problema de concurrencia
						errorConcurrencia=true;
						//break;	//sale de while
					}
			}
			con.cierraStatement();
			
			if (errorConcurrencia) {
				con.terminaTransaccion(false);
				mensaje = "<span style='color:red;'> 	Existen inconsistencias en la informaci&oacute;n, debido a la "+
							" concurrencia de usuarios. El registro mostrado ya fue seleccionado"+
							" previamente. Intente nuevamente el proceso.</span>";						
			//fin de mensaje de error de concurrencia
			} else {
				con.terminaTransaccion(true);
			}			
		
		
		} catch(Exception e) {			
			e.printStackTrace();			
			log.error("error al procesarCancela  " +e); 			
		} finally {	
			if(con.hayConexionAbierta()) {				
				con.cierraConexionDB();	
			}
		}  
		log.info("procesarCancela (S)");  
		return mensaje;
	}
	
	
		
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getIc_producto() {
		return ic_producto;
	}
 
	public void setIc_producto(String ic_producto) {
		this.ic_producto = ic_producto;
	}

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getIg_numero_prestamo() {
		return ig_numero_prestamo;
	}

	public void setIg_numero_prestamo(String ig_numero_prestamo) {
		this.ig_numero_prestamo = ig_numero_prestamo;
	}

	public String getDf_operacion() {
		return df_operacion;
	}

	public void setDf_operacion(String df_operacion) {
		this.df_operacion = df_operacion;
	}

	public String getSolicitudes() {
		return solicitudes;
	}

	public void setSolicitudes(String solicitudes) {
		this.solicitudes = solicitudes;
	}

	public String getPantalla() {
		return pantalla;
	}

	public void setPantalla(String pantalla) {
		this.pantalla = pantalla;
	}

	

}