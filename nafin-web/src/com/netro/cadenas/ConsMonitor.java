package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsMonitor implements IQueryGeneratorRegExtJS {


//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsMonitor.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String ic_producto;
	private String pantalla;
	
		 
	public ConsMonitor() { }
	
	  
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
	
		if(pantalla.equals("ConsultaMonitor")) {	
		
			
		qrySentencia.append(" select cc_contador as CONTADOR , "+
						" sum(ig_valor)as VALOR from com_monitor_int" + ("0".equals(ic_producto.substring(0,1))?"_ce ":" ")+
						" where cc_producto like '%" + ic_producto + "%' " +
						 " group by cc_contador");
		}
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
			
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		int total =0;
		try {
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
            (String) session.getAttribute("strNombre"),
            (String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
 
			
		
			while (rs.next())	{
				
			}
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
	
	
	public List  getCatalogoProduto(String pantalla ){

	AccesoDB con = new AccesoDB();
	ResultSet rs = null;
	ResultSet rs2 = null;
	HashMap datos =  new HashMap();
	List registros  = new ArrayList();
	
	try{
		con.conexionDB();
		if(pantalla.equals("Detalle_Registos")){ 
			datos = new HashMap();	
			datos.put("clave", "T");
			datos.put("descripcion", "Seleccionar Todos");
			registros.add(datos);
		}
			
		String strQryPrincipal = " select ic_producto_nafin,ic_nombre from comcat_producto_nafin " +
							  " where ic_producto_nafin in (0,1,4) order by  ic_producto_nafin ";
		rs = con.queryDB(strQryPrincipal);
		while(rs.next()){
			String clave = rs.getString(1);
			String nombre = rs.getString(2);
				
			datos = new HashMap();	
			datos.put("clave", clave);
			datos.put("descripcion", nombre);
			registros.add(datos);
				
			String	strQrySec = " select cc_producto, cg_nombre from comcat_productos_api " +
										" where cc_producto like '%" + clave + "%'" +
										"  and cc_producto not in ('0', '0E')";
			rs2 = con.queryDB(strQrySec);
			while(rs2.next()){
				String id_api = rs2.getString(1);
				String nombre_api =" -    "+rs2.getString(2);					
					
				datos = new HashMap();			
				datos.put("clave", id_api);
				datos.put("descripcion", nombre_api);
				registros.add(datos);				
			}
			rs2.close();// fin del while
		}rs.close(); // fin del while
		
		con.cierraStatement();
	
	} catch (Exception e) {
		System.err.println("Error: " + e);
	} finally {
		if (con.hayConexionAbierta()) {
			con.cierraConexionDB();
		}
	} 
	return registros;
}

    public void borraMonitor(String  producto){
        log.info("ConsNonitor.LimpiarMonitor(E)");
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        String qry = "";                
        try{
            con.conexionDB();
            if (producto.startsWith("0") ){
                qry = " UPDATE COM_MONITOR_INT_CE SET ig_valor = 0 ";
            }
            if (producto.startsWith("1") ){
                qry = " UPDATE COM_MONITOR_INT SET ig_valor = 0 WHERE cc_producto IN ( '1A', '1B', '1C', '1D' ) ";
            }
            if (producto.startsWith("4") ){
                qry = " UPDATE COM_MONITOR_INT SET ig_valor = 0 WHERE cc_producto IN ( '4A', '4B', '4C', '4D' ) ";
            }
            log.info("ConsNonitor.LimpiarMonitor.SQL: "+qry);
            ps = con.queryPrecompilado(qry);
            ps.executeUpdate();
            ps.close();
            
        } catch (Exception e) {
            System.err.println("Error: " + e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        log.info("ConsNonitor.LimpiarMonitor(S)");
    }

    public List  setPausaInterfaz(String  producto){
        
        ArrayList varBind = new ArrayList();
        List estatusPausa = this.getPausaInterfaz(producto);
        Iterator estatusPausaIterator = estatusPausa.iterator();
        String clave="";
        String descripcion="";
        while (estatusPausaIterator.hasNext()) {
            clave= ""+estatusPausaIterator.next();
            descripcion= ""+estatusPausaIterator.next();
        }
        if(descripcion.equals("N") ){
            varBind.add("S");
        }
        if(descripcion.equals("S") ){
            varBind.add("N");
        }
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        String qry = "";
                
        try{
            con.conexionDB();
            if (producto.startsWith("0") ){
                qry = " update COM_PARAM_GRAL set IC_PAUSA_INTERFAZ_CREDE= ? ";
            }
            if (producto.startsWith("1") ){
                qry = " update COM_PARAM_GRAL set IC_PAUSA_INTERFAZ_DSCTOE= ? ";
            }
            if (producto.startsWith("4") ){
                qry = " update COM_PARAM_GRAL set IC_PAUSA_INTERFAZ_DISTR= ? ";
            }
            ps = con.queryPrecompilado(qry, varBind);
            ps.executeUpdate();
            ps.close();
            
        } catch (Exception e) {
            System.err.println("Error: " + e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }
        estatusPausa = this.getPausaInterfaz(producto);
        return estatusPausa;
    }
    public List  getPausaInterfaz(String  producto){

    AccesoDB con = new AccesoDB();
    ResultSet rs = null;
    List registros  = new ArrayList();
    
    try{
        con.conexionDB();
        if (producto.startsWith("0") ){
            String strCreElec ="select 'CREDE', IC_PAUSA_INTERFAZ_CREDE from COM_PARAM_GRAL";
            rs = con.queryDB(strCreElec);
            if(rs.next()){
                String clave = rs.getString(1);
                String nombre = rs.getString(2);
                registros.add(clave);
                registros.add(nombre);
            }
            rs.close();
        }
        if (producto.startsWith("1") ){
            String strDescuenElec ="select 'DSCTOE', IC_PAUSA_INTERFAZ_DSCTOE from COM_PARAM_GRAL";
            rs = con.queryDB(strDescuenElec);
            if(rs.next()){
                String clave = rs.getString(1);
                String nombre = rs.getString(2);
                registros.add(clave);
                registros.add(nombre);
            }
            rs.close();

        }
        if (producto.startsWith("4") ){
            String strFinanDistr ="select 'DISTR', IC_PAUSA_INTERFAZ_DISTR from COM_PARAM_GRAL";
            rs = con.queryDB(strFinanDistr);
            if(rs.next()){
                String clave = rs.getString(1);
                String nombre = rs.getString(2);
                registros.add(clave);
                registros.add(nombre);
            }
            rs.close();

        }
        con.cierraStatement();
    
    } catch (Exception e) {
            System.err.println("Error: " + e);
    } finally {
            if (con.hayConexionAbierta()) {
                    con.cierraConexionDB();
            }
    } 
    return registros;
}
	
		
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getIc_producto() {
		return ic_producto;
	}
 
	public void setIc_producto(String ic_producto) {
		this.ic_producto = ic_producto;
	}

	public String getPantalla() {
		return pantalla;
	}

	public void setPantalla(String pantalla) {
		this.pantalla = pantalla;
	}

	

}