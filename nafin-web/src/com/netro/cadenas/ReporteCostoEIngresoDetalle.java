package com.netro.cadenas;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;
import netropology.utilerias.*;
import org.apache.commons.logging.Log;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import netropology.utilerias.ServiceLocator;

public class ReporteCostoEIngresoDetalle implements IQueryGeneratorRegExtJS{

	private final static Log log = ServiceLocator.getInstance().getLog(ReporteCostoEIngresoDetalle.class);
	private List   conditions;
	private List   gridTotales1;
	private List   gridTotales2;
	private String anioDetalle;
	private String numMes;


	/**
	 * Obtiene las llaves primarias
	 * @return sentencia sql
	 */
	public String getDocumentQuery() {

		log.info("getDocumentQuery(E)");
		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();

		String fechaNotificacion = "01/" + this.numMes+ "/" + this.anioDetalle;

		qrySentencia.append("  SELECT /*+ use_nl (bcb crn cp cc ce cd)*/ \n"                            );
		qrySentencia.append("         CRN.IC_NAFIN_ELECTRONICO NO_NAFIN, \n"                            );
		qrySentencia.append("         '" + fechaNotificacion + "' AS FECHA \n"                          );
		qrySentencia.append("    FROM BIT_COSTO_BENEF BCB, \n"                                          );
		qrySentencia.append("         COMREL_NAFIN CRN, \n"                                             );
		qrySentencia.append("         COMCAT_PYME CP, \n"                                               );
		qrySentencia.append("         COM_CONTACTO CC, \n"                                              );
		qrySentencia.append("         COMCAT_ESTADO CE, \n"                                             );
		qrySentencia.append("         COM_DOMICILIO CD \n"                                              );
		qrySentencia.append("   WHERE BCB.DF_FECHA_CORTE >= TO_DATE (?, 'DD/MM/YYYY') \n"               );
		qrySentencia.append("     AND BCB.DF_FECHA_CORTE < ADD_MONTHS( TO_DATE (?, 'DD/MM/YYYY'), 1) \n");
		qrySentencia.append("     AND CRN.IC_EPO_PYME_IF = BCB.IC_PYME \n"                              );
		qrySentencia.append("     AND CRN.CG_TIPO = ? \n"                                               );
		qrySentencia.append("     AND CC.CS_PRIMER_CONTACTO = ? \n"                                     );
		qrySentencia.append("     AND CP.IC_PYME = BCB.IC_PYME \n"                                      );
		qrySentencia.append("     AND CC.IC_PYME = BCB.IC_PYME \n"                                      );
		qrySentencia.append("     AND CD.IC_PYME = BCB.IC_PYME \n"                                      );
		qrySentencia.append("     AND CD.IC_PAIS = CE.IC_PAIS \n"                                       );
		qrySentencia.append("     AND CD.IC_ESTADO = CE.IC_ESTADO\n"                                    );
		qrySentencia.append("ORDER BY CRN.IC_NAFIN_ELECTRONICO ASC"                                     );

		conditions.add(fechaNotificacion);
		conditions.add(fechaNotificacion);
		conditions.add("P");
		conditions.add("S");

		log.info("Sentencia:\n"   + qrySentencia.toString());
		log.info("Ccondiciones: " + conditions.toString());
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();

	}

	/**
	 * Obtiene la lista de los Ids en turno para realizar la paginacion
	 * @return sentencia sql
	 * @param ids
	 */
	public String getDocumentSummaryQueryForIds(List ids) {

		log.info("getDocumentSummaryQueryForIds(E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion    = new StringBuffer();
		String fechaNotificacion = "";
		conditions = new ArrayList();

		condicion.append("     AND CRN.IC_NAFIN_ELECTRONICO IN(");
		for (int i = 0; i < ids.size(); i++) {
			List lItem = (ArrayList)ids.get(i);
			if(i == 0){
				fechaNotificacion = lItem.get(1).toString();
			}
			if(i > 0){
				condicion.append(",");
			}
			condicion.append("?" );
			conditions.add(new Long(lItem.get(0).toString()));
		}
		condicion.append(") \n");

		conditions.add(fechaNotificacion);
		conditions.add(fechaNotificacion);
		conditions.add("P");
		conditions.add("S");

		qrySentencia.append("  SELECT /*+ use_nl (bcb crn cp cc ce cd)*/ \n"                            );
		qrySentencia.append("         CRN.IC_NAFIN_ELECTRONICO NO_NAFIN, \n"                            );
		qrySentencia.append("         BCB.IC_PYME PYME, \n"                                             );
		qrySentencia.append("         CP.CG_RFC RFC, \n"                                                );
		qrySentencia.append("         CP.CG_RAZON_SOCIAL RAZON_SOCIAL, \n"                              );
		qrySentencia.append("         CC.CG_CELULAR CELULAR, \n"                                        );
		qrySentencia.append("         CE.CD_NOMBRE ESTADO, \n"                                          );
		qrySentencia.append("         BCB.IG_NUM_DOCTOS_OPER DOCTOS_OPER, \n"                           );
		qrySentencia.append("         BCB.FG_MONTO_OPER MONTO_OPER, \n"                                 );
		qrySentencia.append("         BCB.IG_NUM_NOT_OPER NUM_NOT_OPER, \n"                             );
		qrySentencia.append("         BCB.FG_COSTO_NOT_OPER COSTO_NOT_OPER, \n"                         );
		qrySentencia.append("         BCB.FG_INT_DOCTOS_OPER INTERESES, \n"                             );
		qrySentencia.append("         BCB.IG_NUM_DOCTOS_NEG DOCTOS_NEG, \n"                             );
		qrySentencia.append("         BCB.FG_MONTO_NEG MONTO_NEG, \n"                                   );
		qrySentencia.append("         BCB.IG_NUM_NOT_NEG NUM_NOT_NEG, \n"                               );
		qrySentencia.append("         BCB.FG_COSTO_NOT_NEG COSTO_NOT_NEG, \n"                           );
		qrySentencia.append("         BCB.IG_NUM_DOCTOS_VEN NUM_DOCTOS_VEN, \n"                         );
		qrySentencia.append("         BCB.FG_MONTO_VEN MONTO_VEN, \n"                                   );
		qrySentencia.append("         BCB.IG_NUM_NOT_VEN NUM_NOT_VEN, \n"                               );
		qrySentencia.append("         BCB.FG_COSTO_NOT_VEN COSTO_NOT_VEN, \n"                           );
		qrySentencia.append("         (SELECT DISTINCT CASE \n"                                         );
		qrySentencia.append("                             WHEN E.IC_TIPO_EPO IN (1, 2, 3, 4) \n"        );
		qrySentencia.append("                                THEN 'AMBAS' \n"                           );
		qrySentencia.append("                             WHEN E.IC_TIPO_EPO IN (1, 2) \n"              );
		qrySentencia.append("                                THEN 'PUBLICAS' \n"                        );
		qrySentencia.append("                             WHEN E.IC_TIPO_EPO IN (3, 4) \n"              );
		qrySentencia.append("                                THEN 'PRIVADAS' \n"                        );
		qrySentencia.append("                          END \n"                                          );
		qrySentencia.append("                     FROM COMCAT_EPO E, COMREL_PYME_EPO CPE \n"            );
		qrySentencia.append("                    WHERE CPE.IC_EPO = E.IC_EPO \n"                        );
		qrySentencia.append("                      AND E.IC_TIPO_EPO IS NOT NULL \n"                    );
		qrySentencia.append("                      AND CPE.CS_ACEPTACION = 'H' \n"                      );
		qrySentencia.append("                      AND CPE.CS_HABILITADO = 'S' \n"                      );
		qrySentencia.append("                      AND CPE.IC_PYME = BCB.IC_PYME) AS TIPO_EPO \n"       );
		qrySentencia.append("    FROM BIT_COSTO_BENEF BCB, \n"                                          );
		qrySentencia.append("         COMREL_NAFIN CRN, \n"                                             );
		qrySentencia.append("         COMCAT_PYME CP, \n"                                               );
		qrySentencia.append("         COM_CONTACTO CC, \n"                                              );
		qrySentencia.append("         COMCAT_ESTADO CE, \n"                                             );
		qrySentencia.append("         COM_DOMICILIO CD \n"                                              );
		qrySentencia.append("   WHERE CRN.IC_EPO_PYME_IF = BCB.IC_PYME \n"                              );
		qrySentencia.append(condicion.toString()                                                        );
		qrySentencia.append("     AND BCB.DF_FECHA_CORTE >= TO_DATE (?, 'DD/MM/YYYY') \n"               );
		qrySentencia.append("     AND BCB.DF_FECHA_CORTE < ADD_MONTHS( TO_DATE (?, 'DD/MM/YYYY'), 1) \n");
		qrySentencia.append("     AND CRN.CG_TIPO = ? \n"                                               );
		qrySentencia.append("     AND CC.CS_PRIMER_CONTACTO = ? \n"                                     );
		qrySentencia.append("     AND CP.IC_PYME = BCB.IC_PYME \n"                                      );
		qrySentencia.append("     AND CC.IC_PYME = BCB.IC_PYME \n"                                      );
		qrySentencia.append("     AND CD.IC_PYME = BCB.IC_PYME \n"                                      );
		qrySentencia.append("     AND CD.IC_PAIS = CE.IC_PAIS \n"                                       );
		qrySentencia.append("     AND CD.IC_ESTADO = CE.IC_ESTADO \n"                                   );
		qrySentencia.append("ORDER BY CRN.IC_NAFIN_ELECTRONICO ASC"                                     );

		log.info("Sentencia:\n"  + qrySentencia.toString());
		log.info("Condiciones: " + conditions.toString());
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();

	}

	/**
	 * Obtiene los totales
	 * @return sentencia sql
	 */
	public String getAggregateCalculationQuery() {

		log.info("getAggregateCalculationQuery (E)");
		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();

		String fechaNotificacion = "01/" + this.numMes+ "/" + this.anioDetalle;

		qrySentencia.append("SELECT   SUM (BCB.IG_NUM_DOCTOS_OPER)\n"                               );
		qrySentencia.append("       + SUM (BCB.IG_NUM_DOCTOS_NEG)\n"                                );
		qrySentencia.append("       + SUM (BCB.IG_NUM_DOCTOS_VEN) AS DOCTOS_NOTIFICADOS,\n"         );
		qrySentencia.append("         SUM (BCB.FG_MONTO_OPER)\n"                                    );
		qrySentencia.append("       + SUM (BCB.FG_MONTO_NEG)\n"                                     );
		qrySentencia.append("       + SUM (BCB.FG_MONTO_VEN) AS MONTO_NOTIFICADO,\n"                );
		qrySentencia.append("         SUM (BCB.IG_NUM_NOT_OPER)\n"                                  );
		qrySentencia.append("       + SUM (BCB.IG_NUM_NOT_NEG)\n"                                   );
		qrySentencia.append("       + SUM (BCB.IG_NUM_NOT_VEN) AS NUM_NOTIFICACIONES,\n"            );
		qrySentencia.append("         SUM (BCB.FG_COSTO_NOT_OPER)\n"                                );
		qrySentencia.append("       + SUM (BCB.FG_COSTO_NOT_NEG)\n"                                 );
		qrySentencia.append("       + SUM (BCB.FG_COSTO_NOT_VEN) AS COSTO_NOTIFICACIONES,\n"        );
		qrySentencia.append("       SUM (BCB.FG_INT_DOCTOS_OPER) AS INTERESES_OPERADOS\n"           );
		qrySentencia.append("  FROM BIT_COSTO_BENEF BCB,\n"                                         );
		qrySentencia.append("       COMREL_NAFIN CRN,\n"                                            );
		qrySentencia.append("       COMCAT_PYME CP,\n"                                              );
		qrySentencia.append("       COM_CONTACTO CC,\n"                                             );
		qrySentencia.append("       COMCAT_ESTADO CE,\n"                                            );
		qrySentencia.append("       COM_DOMICILIO CD\n"                                             );
		qrySentencia.append(" WHERE BCB.DF_FECHA_CORTE >= TO_DATE (?, 'DD/MM/YYYY')\n"              );
		qrySentencia.append("   AND BCB.DF_FECHA_CORTE <\n"                                         );
		qrySentencia.append("                          ADD_MONTHS (TO_DATE (?, 'DD/MM/YYYY'), 1)\n" );
		qrySentencia.append("   AND CRN.IC_EPO_PYME_IF = BCB.IC_PYME\n"                             );
		qrySentencia.append("   AND CRN.CG_TIPO = ?\n"                                              );
		qrySentencia.append("   AND CC.CS_PRIMER_CONTACTO = ?\n"                                    );
		qrySentencia.append("   AND CP.IC_PYME = BCB.IC_PYME\n"                                     );
		qrySentencia.append("   AND CC.IC_PYME = BCB.IC_PYME\n"                                     );
		qrySentencia.append("   AND CD.IC_PYME = BCB.IC_PYME\n"                                     );
		qrySentencia.append("   AND CD.IC_PAIS = CE.IC_PAIS\n"                                      );
		qrySentencia.append("   AND CD.IC_ESTADO = CE.IC_ESTADO"                                    );

		conditions.add(fechaNotificacion);
		conditions.add(fechaNotificacion);
		conditions.add("P");
		conditions.add("S");

		log.info("Sentencia:\n"  + qrySentencia.toString());
		log.info("Condiciones: " + conditions.toString());
		log.info("getAggregateCalculationQuery (S)");
		return qrySentencia.toString();

	}

	/**
	 * Realiza la consulta para generar el archivo csv
	 * @return sentencia sql
	 */
	public String getDocumentQueryFile() {

		log.info("getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();

		String fechaNotificacion = "01/" + this.numMes+ "/" + this.anioDetalle;

		qrySentencia.append("  SELECT /*+ use_nl (bcb crn cp cc ce cd)*/ \n"                            );
		qrySentencia.append("         CRN.IC_NAFIN_ELECTRONICO NO_NAFIN, \n"                            );
		qrySentencia.append("         BCB.IC_PYME PYME, \n"                                             );
		qrySentencia.append("         CP.CG_RFC RFC, \n"                                                );
		qrySentencia.append("         CP.CG_RAZON_SOCIAL RAZON_SOCIAL, \n"                              );
		qrySentencia.append("         CC.CG_CELULAR CELULAR, \n"                                        );
		qrySentencia.append("         CE.CD_NOMBRE ESTADO, \n"                                          );
		qrySentencia.append("         BCB.IG_NUM_DOCTOS_OPER DOCTOS_OPER, \n"                           );
		qrySentencia.append("         BCB.FG_MONTO_OPER MONTO_OPER, \n"                                 );
		qrySentencia.append("         BCB.IG_NUM_NOT_OPER NUM_NOT_OPER, \n"                             );
		qrySentencia.append("         BCB.FG_COSTO_NOT_OPER COSTO_NOT_OPER, \n"                         );
		qrySentencia.append("         BCB.FG_INT_DOCTOS_OPER INTERESES, \n"                             );
		qrySentencia.append("         BCB.IG_NUM_DOCTOS_NEG DOCTOS_NEG, \n"                             );
		qrySentencia.append("         BCB.FG_MONTO_NEG MONTO_NEG, \n"                                   );
		qrySentencia.append("         BCB.IG_NUM_NOT_NEG NUM_NOT_NEG, \n"                               );
		qrySentencia.append("         BCB.FG_COSTO_NOT_NEG COSTO_NOT_NEG, \n"                           );
		qrySentencia.append("         BCB.IG_NUM_DOCTOS_VEN NUM_DOCTOS_VEN, \n"                         );
		qrySentencia.append("         BCB.FG_MONTO_VEN MONTO_VEN, \n"                                   );
		qrySentencia.append("         BCB.IG_NUM_NOT_VEN NUM_NOT_VEN, \n"                               );
		qrySentencia.append("         BCB.FG_COSTO_NOT_VEN COSTO_NOT_VEN, \n"                           );
		qrySentencia.append("         (SELECT DISTINCT CASE \n"                                         );
		qrySentencia.append("                             WHEN E.IC_TIPO_EPO IN (1, 2, 3, 4) \n"        );
		qrySentencia.append("                                THEN 'AMBAS' \n"                           );
		qrySentencia.append("                             WHEN E.IC_TIPO_EPO IN (1, 2) \n"              );
		qrySentencia.append("                                THEN 'PUBLICAS' \n"                        );
		qrySentencia.append("                             WHEN E.IC_TIPO_EPO IN (3, 4) \n"              );
		qrySentencia.append("                                THEN 'PRIVADAS' \n"                        );
		qrySentencia.append("                          END \n"                                          );
		qrySentencia.append("                     FROM COMCAT_EPO E, COMREL_PYME_EPO CPE \n"            );
		qrySentencia.append("                    WHERE CPE.IC_EPO = E.IC_EPO \n"                        );
		qrySentencia.append("                      AND E.IC_TIPO_EPO IS NOT NULL \n"                    );
		qrySentencia.append("                      AND CPE.CS_ACEPTACION = 'H' \n"                      );
		qrySentencia.append("                      AND CPE.CS_HABILITADO = 'S' \n"                      );
		qrySentencia.append("                      AND CPE.IC_PYME = BCB.IC_PYME) AS TIPO_EPO \n"       );
		qrySentencia.append("    FROM BIT_COSTO_BENEF BCB, \n"                                          );
		qrySentencia.append("         COMREL_NAFIN CRN, \n"                                             );
		qrySentencia.append("         COMCAT_PYME CP, \n"                                               );
		qrySentencia.append("         COM_CONTACTO CC, \n"                                              );
		qrySentencia.append("         COMCAT_ESTADO CE, \n"                                             );
		qrySentencia.append("         COM_DOMICILIO CD \n"                                              );
		qrySentencia.append("   WHERE BCB.DF_FECHA_CORTE >= TO_DATE (?, 'DD/MM/YYYY') \n"               );
		qrySentencia.append("     AND BCB.DF_FECHA_CORTE < ADD_MONTHS( TO_DATE (?, 'DD/MM/YYYY'), 1) \n");
		qrySentencia.append("     AND CRN.IC_EPO_PYME_IF = BCB.IC_PYME \n"                              );
		qrySentencia.append("     AND CRN.CG_TIPO = ? \n"                                               );
		qrySentencia.append("     AND CC.CS_PRIMER_CONTACTO = ? \n"                                     );
		qrySentencia.append("     AND CP.IC_PYME = BCB.IC_PYME \n"                                      );
		qrySentencia.append("     AND CC.IC_PYME = BCB.IC_PYME \n"                                      );
		qrySentencia.append("     AND CD.IC_PYME = BCB.IC_PYME \n"                                      );
		qrySentencia.append("     AND CD.IC_PAIS = CE.IC_PAIS \n"                                       );
		qrySentencia.append("     AND CD.IC_ESTADO = CE.IC_ESTADO\n"                                    );
		qrySentencia.append("ORDER BY CRN.IC_NAFIN_ELECTRONICO ASC"                                     );

		conditions.add(fechaNotificacion);
		conditions.add(fechaNotificacion);
		conditions.add("P");
		conditions.add("S");

		log.info("Sentencia:\n" + qrySentencia.toString());
		log.info("Condiciones: "  + conditions.toString());
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();

	}

	/**
	 * Metodo para generar un archivo PDF, utilizando paginaci�n
	 * @return nombreArchivo
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearPageCustomFile(HttpServletRequest request, Registros rs, String path, String tipo) {
		return null;
	}

	/**
	 * Metodo para generar un archivo CSV, de todos los registros
	 * @return nombre del archivo
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo) {

		log.info("crearCustomFile (E)");

		String             nombreArchivo    = "";
		StringBuffer       contenidoArchivo = new StringBuffer();
		OutputStreamWriter writer           = null;
		BufferedWriter     buffer           = null;
		int                total            = 0;

		try {

			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer        = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer        = new BufferedWriter(writer);

			contenidoArchivo = new StringBuffer();
			contenidoArchivo.append("No. N@E, No. PyME, RFC, Raz�n Social, N�m. Celular, Estado, Tipo EPO," +
			"No. de Documentos, Monto Operado, No. de Notificaciones, Costo de Notificaciones, Inter�s por Doctos. Operados," +
			"No. de Documentos, Monto Negociable, No. de Notificaciones, Costo de Notificaciones," +
			"No. de Documentos, Monto Vencido, No. de Notificaciones, Costo de Notificaciones \n");
			while (rs.next()){
				String noNafin      = (rs.getString("NO_NAFIN")       == null) ? "" : rs.getString("NO_NAFIN");
				String pyme         = (rs.getString("PYME")           == null) ? "" : rs.getString("PYME");
				String rfc          = (rs.getString("RFC")            == null) ? "" : rs.getString("RFC");
				String razonSocial  = (rs.getString("RAZON_SOCIAL")   == null) ? "" : rs.getString("RAZON_SOCIAL");
				String celular      = (rs.getString("CELULAR")        == null) ? "" : rs.getString("CELULAR");
				String estado       = (rs.getString("ESTADO")         == null) ? "" : rs.getString("ESTADO");
				String doctosOper   = (rs.getString("DOCTOS_OPER")    == null) ? "0": rs.getString("DOCTOS_OPER");
				String montoOper    = (rs.getString("MONTO_OPER")     == null) ? "0": rs.getString("MONTO_OPER");
				String numNotOper   = (rs.getString("NUM_NOT_OPER")   == null) ? "0": rs.getString("NUM_NOT_OPER");
				String costoNotOper = (rs.getString("COSTO_NOT_OPER") == null) ? "0": rs.getString("COSTO_NOT_OPER");
				String intereses    = (rs.getString("INTERESES")      == null) ? "0": rs.getString("INTERESES");
				String doctosNeg    = (rs.getString("DOCTOS_NEG")     == null) ? "0": rs.getString("DOCTOS_NEG");
				String montoNeg     = (rs.getString("MONTO_NEG")      == null) ? "0": rs.getString("MONTO_NEG");
				String numNotNeg    = (rs.getString("NUM_NOT_NEG")    == null) ? "0": rs.getString("NUM_NOT_NEG");
				String costoNotNeg  = (rs.getString("COSTO_NOT_NEG")  == null) ? "0": rs.getString("COSTO_NOT_NEG");
				String numDoctosVen = (rs.getString("NUM_DOCTOS_VEN") == null) ? "0": rs.getString("NUM_DOCTOS_VEN");
				String montoVen     = (rs.getString("MONTO_VEN")      == null) ? "0": rs.getString("MONTO_VEN");
				String numNotVen    = (rs.getString("NUM_NOT_VEN")    == null) ? "0": rs.getString("NUM_NOT_VEN");
				String costoNotVen  = (rs.getString("COSTO_NOT_VEN")  == null) ? "0": rs.getString("COSTO_NOT_VEN");
				String tipoEpo      = (rs.getString("TIPO_EPO")       == null) ? "" : rs.getString("TIPO_EPO");

				contenidoArchivo.append(noNafin.replace(',',' ')                                      +", "+
												pyme.replace(',',' ')                                         +", "+
												rfc.replace(',',' ')                                          +", "+
												razonSocial.replace(',',' ')                                  +", "+
												celular.replace(',',' ')                                      +", "+
												estado.replace(',',' ')                                       +", "+
												tipoEpo.replace(',',' ')                                      +", "+
												doctosOper.replace(',',' ')                                   +", "+
												"$" + Comunes.formatoDecimal(montoOper,2).replace(',',' ')    +", "+
												numNotOper.replace(',',' ')                                   +", "+
												"$" + Comunes.formatoDecimal(costoNotOper,2).replace(',',' ') +", "+
												"$" + Comunes.formatoDecimal(intereses,2).replace(',',' ')    +", "+
												doctosNeg.replace(',',' ')                                    +", "+
												"$" + Comunes.formatoDecimal(montoNeg,2).replace(',',' ')     +", "+
												numNotNeg.replace(',',' ')                                    +", "+
												"$" + Comunes.formatoDecimal(costoNotNeg,2).replace(',',' ')  +", "+
												numDoctosVen.replace(',',' ')                                 +", "+
												"$" + Comunes.formatoDecimal(montoVen,2).replace(',',' ')     +", "+
												numNotVen.replace(',',' ')                                    +", "+
												"$" + Comunes.formatoDecimal(costoNotVen,2).replace(',',' ')  +"\n");

				total++;
				if(total==1000){
					total=0;
					buffer.write(contenidoArchivo.toString());
					contenidoArchivo = new StringBuffer();//Limpio
				}
			}

			if(!this.gridTotales1.isEmpty()){
				HashMap datosGrid = new HashMap();
				contenidoArchivo.append("Totales\n");
				contenidoArchivo.append("Documentos Notificados, Monto Notificado, No. de Notificaciones, Costo de las Notificaciones, Inter�ses por Doctos. Operados \n");
				for(int i = 0; i < this.gridTotales1.size(); i++){
					datosGrid = (HashMap) this.gridTotales1.get(i);
					contenidoArchivo.append(
					datosGrid.get("DOCTOS_NOTIFICADOS")                                                    + ", " +
					"$" + Comunes.formatoDecimal(datosGrid.get("MONTO_NOTIFICADO"),2).replace(',',' ')     + ", " +
					datosGrid.get("NUM_NOTIFICACIONES")                                                    + ", " +
					"$" + Comunes.formatoDecimal(datosGrid.get("COSTO_NOTIFICACIONES"),2).replace(',',' ') + ", " +
					"$" + Comunes.formatoDecimal(datosGrid.get("INTERESES_OPERADOS"),2).replace(',',' ')   + "\n"
					);
				}
			}

			if(!this.gridTotales2.isEmpty()){
				HashMap datosGrid = new HashMap();
				contenidoArchivo.append("\n");
				contenidoArchivo.append("Doctos. en N@E, No. Total de Doctos. en N@E, Monto Total de Doctos. en N@E \n");
				for(int i = 0; i < this.gridTotales2.size(); i++){
					datosGrid = (HashMap) this.gridTotales2.get(i);
					contenidoArchivo.append(
					datosGrid.get("DESCRIPCION")                                  + ", " +
					datosGrid.get("DOCTOS_NE")                                    + ", " +
					"$" + Comunes.formatoDecimal(datosGrid.get("MONTO_NE"),2)     + "\n"
					);
				}
			}

			buffer.write(contenidoArchivo.toString());
			buffer.close();	
			contenidoArchivo = new StringBuffer();

		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo csv ", e);
		} finally {
			try {
			} catch(Exception e) {}
		}

		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}

/*****************************************
 *          M�TODOS DEL FODEA            *
 *****************************************/
	public List getTotales(){

		log.info("getTotales(E)");

		List              lista      = new ArrayList();
		List              varBind   = new ArrayList();
		StringBuffer      sentencia = new StringBuffer();
		AccesoDB          con       = new AccesoDB();
		PreparedStatement pst       = null;
		ResultSet         rst       = null;

		String fechaNotificacion = "01/" + this.numMes+ "/" + this.anioDetalle;

		sentencia.append("SELECT SUM (IG_NUMERO_NEG) NUM_NEG, SUM (FN_MONTO_NEG) MONTO_NEG, \n"          );
		sentencia.append("       SUM (IG_NUMERO_OPERADOS) NUM_OPE, SUM (FN_MONTO_OPERADOS) MONTO_OPE, \n");
		sentencia.append("       SUM (IG_NUMERO_VENCIDOS) NUM_VEN, SUM (FN_MONTO_VENCIDOS) MONTO_VEN \n" );
		sentencia.append("  FROM BIT_DOCTOS_GLOBAL \n"                                                   );
		sentencia.append(" WHERE DF_FECHA_PROC >= TO_DATE (?, 'DD/MM/YYYY') \n"                          );
		sentencia.append("   AND DF_FECHA_PROC < TO_DATE (?, 'DD/MM/YYYY') + 1"                          );

		try{
			con.conexionDB();

			varBind.add(fechaNotificacion);
			varBind.add(fechaNotificacion);
			log.debug("strSQL: \n" + sentencia.toString());
			log.debug("varBind: "  + varBind.toString());
			pst = con.queryPrecompilado(sentencia.toString(), varBind);
			rst = pst.executeQuery();

			if(rst.next()){
				lista.add(rst.getString(1)  == null?"": rst.getString(1));
				lista.add(rst.getString(2)  == null?"": rst.getString(2));
				lista.add(rst.getString(3)  == null?"": rst.getString(3));
				lista.add( rst.getString(4) == null?"": rst.getString(4));
				lista.add(rst.getString(5)  == null?"": rst.getString(5));
				lista.add(rst.getString(6)  == null?"": rst.getString(6));
			} else{
				lista.add("0");
				lista.add("0");
				lista.add("0");
				lista.add("0");
				lista.add("0");
				lista.add("0");
			}

		} catch(Exception e){
			log.info("getTotales.Exception: ");
			e.printStackTrace();
		} finally{
			try{
				rst.close();
				pst.close();
			} catch(Exception e){}
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}

		log.info("getTotales(S)");
		return lista;
	}
 
/*****************************************
 *          GETTERS AND SETTERS          *
 *****************************************/
	public List getConditions() {
		return conditions;
	}

	public List getGridTotales1() {
		return gridTotales1;
	}

	public void setGridTotales1(List gridTotales1) {
		this.gridTotales1 = gridTotales1;
	}

	public List getGridTotales2() {
		return gridTotales2;
	}

	public void setGridTotales2(List gridTotales2) {
		this.gridTotales2 = gridTotales2;
	}

	public String getAnioDetalle() {
		return anioDetalle;
	}

	public void setAnioDetalle(String anioDetalle) {
		this.anioDetalle = anioDetalle;
	}

	public String getNumMes() {
		return numMes;
	}

	public void setNumMes(String numMes) {
		this.numMes = numMes;
	}

}