package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsultaMandatoDocumentos implements IQueryGeneratorReg, IQueryGeneratorRegExtJS {
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaMandatoDocumentos.class);
	
	/**
	 * Constructor de la clase.
	 */
	public ConsultaMandatoDocumentos(){}
	
	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer 	strSQL;
	private List 	conditions;
	private String paginaOffset;
	private String paginaNo;
	private String perfil_usuario;
	private String claveTipoCadena;
	private String clave_epo;
	private String clave_if;
	private String clave_pyme;
	private String numero_nafele;
	private String nombre_pyme;
	private String clave_estatus;
	private String fecha_solicitud_ini;
	private String fecha_solicitud_fin;
	private String fecha_aceptacion_ini;
	private String fecha_aceptacion_fin;
	private String directorio_publicacion;
	private String operacion;
	private String orden;
	private String nombreEpo = "";
	private String nombreDescontante = "";
	private String folioInstruccionIrrevocable = "";
	private String fechaAct = "";
	private String horaAct = "";
	private String nombreIf = "";
	private String usuarioCarga = "";
	private Registros regAux = null;

	/**
	* Obtiene el query para obtener los totales de la consulta
	* @return Cadena con la consulta de SQL, para obtener los totales
	*/
	public String getAggregateCalculationQuery(){
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		
		log.info("getAggregateCalculationQuery(I)");

		strSQL.append(" SELECT COUNT(DISTINCT smd.ic_folio) numero_registros");
		strSQL.append(" FROM com_solic_mand_doc smd");
		strSQL.append(", comcat_pyme pym");
		strSQL.append(", comrel_nafin crn");
		strSQL.append(", comcat_epo epo");
		strSQL.append(", comcat_if cif");
		strSQL.append(", comcat_tipo_cta_man_doc cbm");
		strSQL.append(", comcat_moneda mon");
		strSQL.append(", comcat_estatus_man_doc emd");
		strSQL.append(" WHERE smd.ic_pyme = pym.ic_pyme");
		strSQL.append(" AND smd.ic_pyme = crn.ic_epo_pyme_if");
		strSQL.append(" AND smd.ic_epo = epo.ic_epo");
		strSQL.append(" AND smd.ic_if = cif.ic_if");
		strSQL.append(" AND smd.cc_tipo_cuenta = cbm.cc_tipo_cuenta(+)");
		strSQL.append(" AND smd.ic_moneda = mon.ic_moneda(+)");
		strSQL.append(" AND smd.ic_estatus_man_doc = emd.ic_estatus_man_doc");
		strSQL.append(" AND crn.cg_tipo = ?");

		conditions.add("P");

		//Tipo de cadena //FODEA 012 - 2010 ACF
		if(claveTipoCadena != null && !claveTipoCadena.equals("")){
			strSQL.append(" AND epo.ic_tipo_epo = ?");
			conditions.add(new Integer(claveTipoCadena));
		}
		//EPO
		if(clave_epo != null && !clave_epo.equals("")){
			strSQL.append(" AND epo.ic_epo = ?");
			conditions.add(new Long(clave_epo));
		}
		//Intermediario Financiero//FODEA 012 - 2010 ACF
    if(clave_if != null && !clave_if.equals("")){
      if(!perfil_usuario.equals("DESCONT IF")){
        strSQL.append(" AND cif.ic_if = ?");
      }  else {
        strSQL.append(" AND smd.ic_descontante = ?");
      }
      conditions.add(new Long(clave_if.trim()));
    }
		//PyME
		if(numero_nafele != null && !numero_nafele.equals("")){
			strSQL.append(" AND crn.ic_nafin_electronico = ?");
			conditions.add(new Long(numero_nafele));
		}
		//Estatus de la solicitud
		if(clave_estatus != null && !clave_estatus.equals("")){
			strSQL.append(" AND smd.ic_estatus_man_doc = ?");
			conditions.add(new Integer(clave_estatus));
		}
		//Fechas de solicitud de mandato de documentos
		if(fecha_solicitud_ini != null && !fecha_solicitud_ini.equals("") && fecha_solicitud_fin != null && !fecha_solicitud_fin.equals("")){
      strSQL.append(" AND smd.df_solicitud >= TO_DATE(?, 'dd/mm/yyyy')");
      strSQL.append(" AND smd.df_solicitud < TO_DATE(?, 'dd/mm/yyyy') + 1");
			conditions.add(fecha_solicitud_ini);
			conditions.add(fecha_solicitud_fin);
		}
		//Fechas de Aceptaci�n IF
		if(fecha_aceptacion_ini != null && !fecha_aceptacion_ini.equals("") && fecha_aceptacion_fin != null && !fecha_aceptacion_fin.equals("")){
      strSQL.append(" AND smd.df_aceptacion >= TO_DATE(?, 'dd/mm/yyyy')");
      strSQL.append(" AND smd.df_aceptacion < TO_DATE(?, 'dd/mm/yyyy') + 1");
			conditions.add(fecha_aceptacion_ini);
			conditions.add(fecha_aceptacion_fin);
		}
		log.debug("..:: strSQL : " + strSQL.toString());
		log.debug("..:: conditions : " + conditions);
		
		log.info("getAggregateCalculationQuery(F)");
		return strSQL.toString();
	}//getAggregateCalculationQuery
	
	/**
	* Obtiene el query para obtener las llaves primarias de la consulta
	* @return Cadena con la consulta de SQL, para obtener llaves primarias
	*/
	public String getDocumentQuery(){
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		
		log.info("getDocumentQuery(I)");
		strSQL.append(" SELECT DISTINCT a.folio  FROM ( " );

		//strSQL.append(" SELECT /*+ use_nl(smd crn) */ smd.ic_folio folio");
		strSQL.append(" SELECT /*+ use_nl(smd crn) */ smd.ig_folio_instruccion folio");//FODEA 033 - 2010 ACF
		strSQL.append(" FROM com_solic_mand_doc smd");
		strSQL.append(", comcat_pyme pym");
		strSQL.append(", comrel_nafin crn");
		strSQL.append(", comcat_epo epo");
		strSQL.append(", comcat_if cif");
		strSQL.append(", comcat_tipo_cta_man_doc cbm");
		strSQL.append(", comcat_moneda mon");
		strSQL.append(", comcat_estatus_man_doc emd");
		strSQL.append(" WHERE smd.ic_pyme = pym.ic_pyme");
		strSQL.append(" AND smd.ic_pyme = crn.ic_epo_pyme_if");
		strSQL.append(" AND smd.ic_epo = epo.ic_epo");
		strSQL.append(" AND smd.ic_if = cif.ic_if");
		strSQL.append(" AND smd.cc_tipo_cuenta = cbm.cc_tipo_cuenta(+)");
		strSQL.append(" AND smd.ic_moneda = mon.ic_moneda(+)");
		strSQL.append(" AND smd.ic_estatus_man_doc = emd.ic_estatus_man_doc");
		strSQL.append(" AND crn.cg_tipo = ?");

		conditions.add("P");
		
		//Tipo de cadena  //FODEA 012 - 2010 ACF
		if(claveTipoCadena != null && !claveTipoCadena.equals("")){
			strSQL.append(" AND epo.ic_tipo_epo = ?");
			conditions.add(new Integer(claveTipoCadena));
		}
		//EPO
		if(clave_epo != null && !clave_epo.equals("")){
			strSQL.append(" AND epo.ic_epo = ?");
			conditions.add(new Long(clave_epo));
		}
		//Intermediario Financiero//FODEA 012 - 2010 ACF
    if(clave_if != null && !clave_if.equals("")){
      if(!perfil_usuario.equals("DESCONT IF")){
        strSQL.append(" AND cif.ic_if = ?");
      }  else {
        strSQL.append(" AND smd.ic_descontante = ?");
      }
      conditions.add(new Long(clave_if.trim()));
    }
		//PyME
		if(numero_nafele != null && !numero_nafele.equals("")){
			strSQL.append(" AND crn.ic_nafin_electronico = ?");
			conditions.add(new Long(numero_nafele));
		}
		//Estatus de la solicitud
		if(clave_estatus != null && !clave_estatus.equals("")){
			strSQL.append(" AND smd.ic_estatus_man_doc = ?");
			conditions.add(new Integer(clave_estatus));
		}
		//Fechas de solicitud de mandato de documentos
		if(fecha_solicitud_ini != null && !fecha_solicitud_ini.equals("") && fecha_solicitud_fin != null && !fecha_solicitud_fin.equals("")){
      strSQL.append(" AND smd.df_solicitud >= TO_DATE(?, 'dd/mm/yyyy')");
      strSQL.append(" AND smd.df_solicitud < TO_DATE(?, 'dd/mm/yyyy') + 1");
			conditions.add(fecha_solicitud_ini);
			conditions.add(fecha_solicitud_fin);
		}
		//Fechas de Aceptaci�n IF
		if(fecha_aceptacion_ini != null && !fecha_aceptacion_ini.equals("") && fecha_aceptacion_fin != null && !fecha_aceptacion_fin.equals("")){
      strSQL.append(" AND smd.df_aceptacion >= TO_DATE(?, 'dd/mm/yyyy')");
      strSQL.append(" AND smd.df_aceptacion < TO_DATE(?, 'dd/mm/yyyy') + 1");
			conditions.add(fecha_aceptacion_ini);
			conditions.add(fecha_aceptacion_fin);
		}
		
		strSQL.append("  ) a order by folio");
		
		log.debug("..:: strSQL : " + strSQL.toString());
		log.debug("..:: conditions : " + conditions);
		
		log.info("getDocumentQuery(F)");
		
		return strSQL.toString();
	}//getDocumentQuery
	
	/**
	* Obtiene el query necesario para mostrar la informaci�n completa de 
	* una p�gina a partir de las llaves primarias enviadas como par�metro
	* @return Cadena con la consulta de SQL, para obtener la informaci�n
	* 	completa de los registros con las llaves especificadas
	*/
	public String getDocumentSummaryQueryForIds(List pageIds){
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		
		log.info("getDocumentSummaryQueryForIds(I)");
	
    //strSQL.append(" SELECT DISTINCT /*+ use_nl(smd crn pym cdi epo cif cds bsr mon emd tepo) index(smd cp_com_solic_mand_doc_pk) */");//FODEA 012 - 2010 ACF
		//strSQL.append(" smd.ic_folio ic_folio,");//FODEA 033 - 2010 ACF
		strSQL.append(" SELECT DISTINCT /*+ use_nl(smd crn pym cdi epo cif cds bsr mon mnd emd tepo) index(smd cp_com_solic_mand_doc_pk)*/");//FODEA 015 - 2011 ACF
		strSQL.append(" smd.ig_folio_instruccion folio_instruccion,");//FODEA 033 - 2010 ACF
		strSQL.append(" crn.ic_nafin_electronico nafin_electronico,");
		strSQL.append(" pym.ic_pyme,");	//FODEA 022 HVC
		strSQL.append(" pym.cg_razon_social nombre_pyme,");
		strSQL.append(" epo.ic_epo,");	//FODEA 022 HVC
		strSQL.append(" epo.cg_razon_social nombre_epo,");
		strSQL.append(" cif.ic_if,");		//FODEA 022 HVC
		strSQL.append(" cif.cg_razon_social nombre_if,");//FODEA 012 - 2010 ACF
		strSQL.append(" DECODE (smd.ic_descontante, null, cif.cg_razon_social, cds.cg_razon_social) nombre_descontante,");//FODEA 012 - 2010 ACF
		strSQL.append(" tepo.cg_descripcion tipo_cadena,");//FODEA 012 - 2010 ACF
		strSQL.append(" TO_CHAR(smd.df_solicitud, 'dd/mm/yyyy') fecha_solicitud,");
		strSQL.append(" DECODE(smd.ic_estatus_man_doc, 3, 'NA', TO_CHAR(smd.df_aceptacion, 'dd/mm/yyyy')) fecha_autorizacion_if,");
		strSQL.append(" DECODE(smd.ic_estatus_man_doc, 3, 'NA', smd.fn_monto) monto_credito,");
		strSQL.append(" DECODE(smd.ic_estatus_man_doc, 3, 'NA', TO_CHAR(smd.df_vencimiento, 'dd/mm/yyyy')) fecha_vencimiento,");
		strSQL.append(" bsr.ic_if clave_banco_servicio,");//FODEA 012 - 2010 ACF		//FODEA 022 HVC
		strSQL.append(" bsr.cg_razon_social banco_servicio,");//FODEA 012 - 2010 ACF
		strSQL.append(" DECODE(smd.ic_estatus_man_doc, 3, 'NA', smd.cg_cuenta_bancaria) cuenta_bancaria,");
		strSQL.append(" DECODE(smd.ic_estatus_man_doc, 3, 'NA', cbm.cc_tipo_cuenta) clave_tipo_cuenta,");	//FODEA 022 HVC
		strSQL.append(" DECODE(smd.ic_estatus_man_doc, 3, 'NA', cbm.cg_descripcion) tipo_cuenta,");
		strSQL.append(" DECODE(smd.ic_estatus_man_doc, 3, 'NA', smd.cg_credito) numero_credito,");
		strSQL.append(" DECODE(smd.ic_estatus_man_doc, 3, 'NA', mon.cd_nombre) moneda,");
		strSQL.append(" DECODE(smd.ic_estatus_man_doc, 3, 'NA', DECODE(smd.ic_moneda_dscto, null, 'AMBAS', mnd.cd_nombre)) AS moneda_descuento,");//FODEA 015 - 2011 ACF
		strSQL.append(" smd.cg_observaciones observaciones,");
		strSQL.append(" emd.cg_descripcion estatus_solicitud,");
		strSQL.append(" smd.ic_estatus_man_doc clave_estatus,");
    strSQL.append(" DECODE(smd.cg_usuario_solicitud, null, 'N', 'S') usuario_solicitud,");//FODEA 012 - 2010 ACF
	   strSQL.append(" nvl(TO_CHAR(smd.df_nuevo_vencimiento, 'dd/mm/yyyy'),'') as nueva_fecha_vencimiento,  ");//FODEA 055 - 2010 By JSHD
	   strSQL.append(" nvl(TO_CHAR(smd.df_mod_monto,         'dd/mm/yyyy'),'') as fecha_modificacion,        ");//FODEA 055 - 2010 By JSHD
		strSQL.append(" smd.cs_dscto_aut_irrev as descAut,");
		strSQL.append(" DECODE(smd.ic_estatus_man_doc, 2, DECODE(smd.cs_dscto_aut_irrev, 'S', 'Activado', 'Desactivado') || ' ' || TO_CHAR(smd.df_dscto_aut_irrev, 'dd/mm/yyyy hh:mi:ss am'), 'NA') AS fechaDescAut");//FODEA 015 - 2011 ACF
		strSQL.append(" , 'N'as HABILITACAMPOS ");			
		
		strSQL.append(" FROM com_solic_mand_doc smd");
    strSQL.append(", comrel_nafin crn");
		strSQL.append(", comcat_pyme pym");
		strSQL.append(", comcat_epo epo");
		strSQL.append(", comcat_if cif");
    strSQL.append(", comcat_if cds");//FODEA 012 - 2010 ACF
    strSQL.append(", comcat_if bsr");//FODEA 012 - 2010 ACF
		strSQL.append(", comcat_tipo_cta_man_doc cbm");
		strSQL.append(", comcat_moneda mon");
		strSQL.append(", comcat_moneda mnd");//FODEA 015 - 2011 ACF
		strSQL.append(", comcat_estatus_man_doc emd");
    strSQL.append(", comcat_tipo_epo tepo");//FODEA 012 - 2010 ACF
		strSQL.append(" WHERE smd.ic_pyme = pym.ic_pyme");
		strSQL.append(" AND smd.ic_pyme = crn.ic_epo_pyme_if");
		strSQL.append(" AND smd.ic_epo = epo.ic_epo");
		strSQL.append(" AND smd.ic_if = cif.ic_if");
		strSQL.append(" AND smd.cc_tipo_cuenta = cbm.cc_tipo_cuenta(+)");
		strSQL.append(" AND smd.ic_moneda = mon.ic_moneda(+)");
		strSQL.append(" AND smd.ic_moneda_dscto = mnd.ic_moneda(+)");//FODEA 015 - 2011 ACF
		strSQL.append(" AND smd.ic_estatus_man_doc = emd.ic_estatus_man_doc");
    strSQL.append(" AND smd.ic_descontante = cds.ic_if(+)");//FODEA 012 - 2010 ACF
    strSQL.append(" AND smd.ic_banco_servicio = bsr.ic_if(+)");//FODEA 012 - 2010 ACF
    strSQL.append(" AND epo.ic_tipo_epo = tepo.ic_tipo_epo(+)");//FODEA 012 - 2010 ACF
		strSQL.append(" AND crn.cg_tipo = ?");
		strSQL.append(" AND (");
		
		conditions.add("P");
		
		for(int i = 0; i < pageIds.size(); i++){
			List lItem = (ArrayList)pageIds.get(i);
			
			if(i > 0){strSQL.append("  OR  ");}
			
			//strSQL.append("smd.ic_folio = ?");
			strSQL.append("smd.ig_folio_instruccion = ?");//FODEA 033 - 2010 ACF
			conditions.add(new Long(lItem.get(0).toString()));
		}

		strSQL.append(" ) ");
		
		
		strSQL.append(" ORDER BY ig_folio_instruccion, clave_estatus  ASC"); 
		
	//	strSQL.append(" ORDER BY 1 DESC, 4 ASC");
		
		log.debug("..:: strSQL : " + strSQL.toString());
		log.debug("..:: conditions : " + conditions);
		
		log.info("getDocumentSummaryQueryForIds(F)");
		return strSQL.toString();
	}//getDocumentSummaryQueryForIds	
	
	public String getDocumentQueryFile(){
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		
		log.info("getDocumentQueryFile(I)");
    
    //strSQL.append(" SELECT /*+ use_nl(smd crn pym cdi epo cif cds bsr mon emd tepo) index(smd cp_com_solic_mand_doc_pk) */");//FODEA 012 - 2010 ACF
		strSQL.append(" SELECT /*+ use_nl(smd crn pym cdi epo cif cds bsr mon mnd emd tepo) index(smd cp_com_solic_mand_doc_pk) */");//FODEA 015 - 2011 ACF
		strSQL.append(" smd.ic_folio folio,");
		strSQL.append(" smd.ig_folio_instruccion, ");
		if(!perfil_usuario.equals("ADMIN PYME")){strSQL.append(" crn.ic_nafin_electronico nafin_electronico,");}
		if(!perfil_usuario.equals("ADMIN PYME")){strSQL.append(" pym.cg_razon_social pyme,");}
		strSQL.append(" epo.cg_razon_social epo,");
    strSQL.append(" cif.cg_razon_social if_tramita_credito,");//FODEA 012 - 2010 ACF
    strSQL.append(" DECODE (cds.cg_razon_social, null, cif.cg_razon_social, cds.cg_razon_social) descontante,");//FODEA 012 - 2010 ACF
    if(!perfil_usuario.equals("ADMIN PYME")){strSQL.append(" tepo.cg_descripcion tipo_cadena,");}//FODEA 012 - 2010 ACF
		strSQL.append(" TO_CHAR(smd.df_solicitud, 'dd/mm/yyyy') fecha_solicitud,");
		strSQL.append(" DECODE(smd.ic_estatus_man_doc, 3, 'NA', TO_CHAR(smd.df_aceptacion, 'dd/mm/yyyy')) fecha_aceptacion,");
		strSQL.append(" DECODE(smd.ic_estatus_man_doc, 3, 'NA', NVL(to_char(smd.fn_monto),'NA')) monto_credito,");
		strSQL.append(" DECODE(smd.ic_estatus_man_doc, 3, 'NA', TO_CHAR(smd.df_vencimiento, 'dd/mm/yyyy')) fecha_vencimiento_credito,");
    strSQL.append(" bsr.cg_razon_social banco_de_servicio,");//FODEA 012 - 2010 ACF
		strSQL.append(" DECODE(smd.ic_estatus_man_doc, 3, 'NA', 1, '', 'NC: ' || smd.cg_cuenta_bancaria) cuenta_bancaria,");
		strSQL.append(" DECODE(smd.ic_estatus_man_doc, 3, 'NA', cbm.cg_descripcion) tipo_cuenta,");
		strSQL.append(" DECODE(smd.ic_estatus_man_doc, 3, 'NA', smd.cg_credito) numero_credito,");
		strSQL.append(" DECODE(smd.ic_estatus_man_doc, 3, 'NA', mon.cd_nombre) moneda_credito,");
		strSQL.append(" nvl(TO_CHAR(smd.df_nuevo_vencimiento, 'dd/mm/yyyy'),'') as nueva_fecha_vencimiento,  ");
		strSQL.append(" DECODE(smd.ic_estatus_man_doc, 3, 'NA', DECODE(smd.ic_moneda_dscto, null, 'AMBAS', mnd.cd_nombre)) AS moneda_descuento,");//FODEA 015 - 2011 ACF
		strSQL.append(" smd.cg_observaciones observaciones,");
		strSQL.append(" emd.cg_descripcion estatus_solicitud, ");
	   strSQL.append(" nvl(TO_CHAR(smd.df_mod_monto,         'dd/mm/yyyy'),'') as fecha_modificacion,        ");//FODEA 055 - 2010 By JSHD

		strSQL.append(" smd.ic_estatus_man_doc ");
		strSQL.append(" FROM com_solic_mand_doc smd");
		strSQL.append(", comrel_nafin crn ");
    strSQL.append(", comcat_pyme pym");
		strSQL.append(", comcat_epo epo");
		strSQL.append(", comcat_if cif");
    strSQL.append(", comcat_if cds");//FODEA 012 - 2010 ACF
    strSQL.append(", comcat_if bsr");//FODEA 012 - 2010 ACF
		strSQL.append(", comcat_tipo_cta_man_doc cbm");
		strSQL.append(", comcat_moneda mon");
		strSQL.append(", comcat_moneda mnd");//FODEA 015 - 2011 ACF
		strSQL.append(", comcat_estatus_man_doc emd");
    strSQL.append(", comcat_tipo_epo tepo");//FODEA 012 - 2010 ACF
		strSQL.append(" WHERE smd.ic_pyme = pym.ic_pyme");
		strSQL.append(" AND smd.ic_pyme = crn.ic_epo_pyme_if");
		strSQL.append(" AND smd.ic_epo = epo.ic_epo");
		strSQL.append(" AND smd.ic_if = cif.ic_if");
		strSQL.append(" AND smd.cc_tipo_cuenta = cbm.cc_tipo_cuenta(+)");
		strSQL.append(" AND smd.ic_moneda = mon.ic_moneda(+)");		
		strSQL.append(" AND smd.ic_moneda_dscto = mnd.ic_moneda(+)");//FODEA 015 - 2011 ACF
		strSQL.append(" AND smd.ic_estatus_man_doc = emd.ic_estatus_man_doc");
    strSQL.append(" AND smd.ic_descontante = cds.ic_if(+)");//FODEA 012 - 2010 ACF
    strSQL.append(" AND smd.ic_banco_servicio = bsr.ic_if(+)");//FODEA 012 - 2010 ACF
    strSQL.append(" AND epo.ic_tipo_epo = tepo.ic_tipo_epo(+)");//FODEA 012 - 2010 ACF
		strSQL.append(" AND crn.cg_tipo = ?");
		
		conditions.add("P");
		
		//Tipo de cadena //FODEA 012 - 2010 ACF
		if(claveTipoCadena != null && !claveTipoCadena.equals("")){
			strSQL.append(" AND epo.ic_tipo_epo = ?");
			conditions.add(new Integer(claveTipoCadena));
		}
		//EPO
		if(clave_epo != null && !clave_epo.equals("")){
			strSQL.append(" AND epo.ic_epo = ?");
			conditions.add(new Long(clave_epo));
		}
		//Intermediario Financiero//FODEA 012 - 2010 ACF
    if(clave_if != null && !clave_if.equals("")){
      if(!perfil_usuario.equals("DESCONT IF")){
        strSQL.append(" AND cif.ic_if = ?");
      }  else {
        strSQL.append(" AND smd.ic_descontante = ?");
      }
      conditions.add(new Long(clave_if.trim()));
    }
		//PyME
		if(numero_nafele != null && !numero_nafele.equals("")){
			strSQL.append(" AND crn.ic_nafin_electronico = ?");
			conditions.add(new Long(numero_nafele));
		}
		//Estatus de la solicitud
		if(clave_estatus != null && !clave_estatus.equals("")){
			strSQL.append(" AND smd.ic_estatus_man_doc = ?");
			conditions.add(new Integer(clave_estatus));
		}
		//Fechas de solicitud de mandato de documentos
		if(fecha_solicitud_ini != null && !fecha_solicitud_ini.equals("") && fecha_solicitud_fin != null && !fecha_solicitud_fin.equals("")){
      strSQL.append(" AND smd.df_solicitud >= TO_DATE(?, 'dd/mm/yyyy')");
      strSQL.append(" AND smd.df_solicitud < TO_DATE(?, 'dd/mm/yyyy') + 1");
			conditions.add(fecha_solicitud_ini);
			conditions.add(fecha_solicitud_fin);
		}
		//Fechas de Aceptaci�n IF
		if(fecha_aceptacion_ini != null && !fecha_aceptacion_ini.equals("") && fecha_aceptacion_fin != null && !fecha_aceptacion_fin.equals("")){
      strSQL.append(" AND smd.df_aceptacion >= TO_DATE(?, 'dd/mm/yyyy')");
      strSQL.append(" AND smd.df_aceptacion < TO_DATE(?, 'dd/mm/yyyy') + 1");
			conditions.add(fecha_aceptacion_ini);
			conditions.add(fecha_aceptacion_fin);
		}
		
		strSQL.append(" ORDER BY 1 ASC");
		
		log.debug("..:: strSQL : " + strSQL.toString());
		log.debug("..:: conditions : " + conditions);
		
		log.info("getDocumentQueryFile(F)");
		return strSQL.toString();
	}//getDocumentQueryFile
	
	//GETTERS
  /**
	 * Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 * @return Lista con los parametros de las condiciones
	 */
	public List getConditions(){return conditions;}
  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() {return paginaNo;}
	
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset(){return paginaOffset;}
	
	//Parametros del formulario
	public String getPerfil_usuario(){return perfil_usuario;}
  public String getClaveTipoCadena(){return claveTipoCadena;}
	public String getClave_epo(){return clave_epo;}
	public String getClave_if(){return clave_if;}
	public String getClave_pyme(){return clave_pyme;}
	public String getNumero_nafele(){return numero_nafele;}
	public String getNombre_pyme(){return nombre_pyme;}
	public String getClave_estatus(){return clave_estatus;}
	public String getFecha_solicitud_ini(){return fecha_solicitud_ini;}
	public String getFecha_solicitud_fin(){return fecha_solicitud_fin;}
	public String getFecha_aceptacion_ini(){return fecha_aceptacion_ini;}
	public String getFecha_aceptacion_fin(){return fecha_aceptacion_fin;}
	public String getDirectorio_publicacion(){return directorio_publicacion;}
	public String getOrden(){return orden;}
	
	//SETTERS
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo){paginaNo = newPaginaNo;}
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset){this.paginaOffset=paginaOffset;}
		
	//Parametros del formulario
	public void setPerfil_usuario(String perfil_usuario){this.perfil_usuario = perfil_usuario;}
  public void setClaveTipoCadena(String claveTipoCadena){this.claveTipoCadena = claveTipoCadena;}
	public void setClave_epo(String clave_epo){this.clave_epo = clave_epo;}
	public void setClave_if(String clave_if){this.clave_if = clave_if;}
	public void setClave_pyme(String clave_pyme){this.clave_pyme = clave_pyme;}
	public void setNumero_nafele(String numero_nafele){this.numero_nafele = numero_nafele;}
	public void setNombre_pyme(String nombre_pyme){this.nombre_pyme = nombre_pyme;}
	public void setClave_estatus(String clave_estatus){this.clave_estatus = clave_estatus;}
	public void setFecha_solicitud_ini(String fecha_solicitud_ini){this.fecha_solicitud_ini = fecha_solicitud_ini;}
	public void setFecha_solicitud_fin(String fecha_solicitud_fin){this.fecha_solicitud_fin = fecha_solicitud_fin;}
	public void setFecha_aceptacion_ini(String fecha_aceptacion_ini){this.fecha_aceptacion_ini = fecha_aceptacion_ini;}
	public void setFecha_aceptacion_fin(String fecha_aceptacion_fin){this.fecha_aceptacion_fin = fecha_aceptacion_fin;}
	public void setDirectorio_publicacion(String directorio_publicacion){this.directorio_publicacion = directorio_publicacion;}
	public void setOrden(String orden){this.orden = orden;}

	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet reg, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		if ("CSV".equals(tipo)){
			try{
				List listaEpos = new ArrayList();
				int rowspan = 0;
				String folioActual = "";
				String folioAnterior = "";
				HttpSession session = request.getSession();
	
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
				buffer = new BufferedWriter(writer);
	
				linea = "FOLIO,N@E PyME,PYME, EPO, IF TRAMITA CR�DITO, DESCONTANTE, FECHA SOLICITUD, FECHA ACEPTACI�N, MONTO CR�DITO, FECHA VENCIMIENTO CR�DITO, TIPO CUENTA, BANCO DE SERVICIO, CUENTA BANCARIA, N�MERO CR�DITO, MONEDA CR�DITO, MONEDA DESCUENTO, OBSERVACIONES, NUEVA FECHA VENCIMIENTO CREDITO, FECHA MODIFICACI�N MONTO, ESTATUS SOLICITUD,\n";//FODEA 015 - 2011 ACF
				buffer.write(linea);
				
				String Folio,nafinEle,pyme, epo,ifTramita,descontante,fechaSolic,fechaAceptacion,montoCredito,fechaVenc
				,tipoCuenta,bancoServicio,cuentaBanc,numCredito,monedaCredito,monedaDesc,observaciones,nuevaFec,fecMod,estatus;
				while (reg.next()) {
						
					Folio	=	(reg.getString("ig_folio_instruccion")==null)?"":reg.getString("ig_folio_instruccion");
					nafinEle		=	(reg.getString("nafin_electronico")==null)?"":reg.getString("nafin_electronico");
					pyme				=	(reg.getString("pyme")==null)?"":reg.getString("pyme");
					epo		=	(reg.getString("epo")==null)?"":reg.getString("epo");
					ifTramita				=	(reg.getString("if_tramita_credito")==null)?"":reg.getString("if_tramita_credito");
					descontante			=	(reg.getString("descontante")==null)?"":reg.getString("descontante");
					fechaSolic			=	(reg.getString("fecha_solicitud")==null)?"":reg.getString("fecha_solicitud");
					fechaAceptacion			=	(reg.getString("fecha_aceptacion")==null)?"":reg.getString("fecha_aceptacion");
					montoCredito				=	(reg.getString("monto_credito")==null)?"":reg.getString("monto_credito");
					fechaVenc		=	(reg.getString("fecha_vencimiento_credito")==null)?"":reg.getString("fecha_vencimiento_credito");
					tipoCuenta				=	(reg.getString("tipo_cuenta")==null)?"":reg.getString("tipo_cuenta");
					bancoServicio		=	(reg.getString("banco_de_servicio")==null)?"":reg.getString("banco_de_servicio");
					cuentaBanc				=	(reg.getString("cuenta_bancaria")==null)?"":reg.getString("cuenta_bancaria");
					numCredito		=	(reg.getString("numero_credito")==null)?"":reg.getString("numero_credito");
					monedaCredito				=	(reg.getString("moneda_credito")==null)?"":reg.getString("moneda_credito");
					monedaDesc		=	(reg.getString("moneda_descuento")==null)?"":reg.getString("moneda_descuento");
					
					observaciones				=	(reg.getString("observaciones")==null)?"":reg.getString("observaciones");
					nuevaFec		=	(reg.getString("nueva_fecha_vencimiento")==null)?"":reg.getString("nueva_fecha_vencimiento");
					fecMod				=	(reg.getString("fecha_modificacion")==null)?"":reg.getString("fecha_modificacion");
					estatus		=	(reg.getString("estatus_solicitud")==null)?"":reg.getString("estatus_solicitud");
					linea=Folio+","+nafinEle+","+pyme.replaceAll(",", "")+","+ epo.replaceAll(",", "")+","+ifTramita.replaceAll(",", "")+","+descontante.replaceAll(",", "")+","+fechaSolic+","
						+fechaAceptacion+","+montoCredito.replaceAll(",", "")+","+fechaVenc
						+","+tipoCuenta+","+bancoServicio.replaceAll(",", "")+","+cuentaBanc+","+numCredito+","
						+monedaCredito+","+monedaDesc+","+observaciones.replaceAll(",", "").replaceAll("[\n\r]","")+","+nuevaFec+","+fecMod+","+estatus;	
						
						linea += ",\n";
						buffer.write(linea);
					
				}
				buffer.close();
			} catch (Exception e) {
				throw new AppException("Error al generar el archivo", e);
			}	finally {
				}
		}
		return nombreArchivo;	 
	}
	public String crearCustomFiles(HttpServletRequest request, Registros reg, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
	
		if ("ConsultaCSV".equals(tipo)){
			try{
				List listaEpos = new ArrayList();
				int rowspan = 0;
				String folioActual = "";
				String folioAnterior = "";
				HttpSession session = request.getSession();
	
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
				buffer = new BufferedWriter(writer);
	
				linea = "FOLIO, N@E PyME,  PyME,  EPO, IF TRAMITA CR�DITO, DESCONTANTE, FECHA SOLICITUD, FECHA ACEPTACI�N, MONTO CR�DITO, FECHA VENCIMIENTO CR�DITO, BANCO DE SERVICIO, CUENTA BANCARIA, TIPO CUENTA, N�MERO CR�DITO, MONEDA CR�DITO, MONEDA DESCUENTO, OBSERVACIONES, NUEVA FECHA VENCIMIENTO CR�DITO, FECHA MODIFICACI�N MONTO, ESTATUS SOLICITUD,\n";//FODEA 015 - 2011 ACF
				buffer.write(linea);

				while (reg.next()) {
					//int iRegEnturno = reg.getNumRegistro();
					folioActual = reg.getString("folio_instruccion");
					if (!folioActual.equals(folioAnterior)) {
						listaEpos = new ArrayList();
						rowspan = 0;
						//reg.rewind();
						while (regAux.next()) {
							if (folioActual.equals(regAux.getString("folio_instruccion"))) {
								listaEpos.add(regAux.getString("epo"));
								rowspan++;
							}
						}
						//reg.setNumRegistro(iRegEnturno);
						for (int i = 0; i < rowspan; i++) {
							if (i == 0) {
								linea = reg.getString("folio_instruccion") + "," + 
								reg.getString("nafin_electronico") + "," +
								reg.getString("pyme").toString().replaceAll(",", "") + "," +								
								listaEpos.get(i).toString().replaceAll(",", "") + "," + reg.getString("if_tramita_credito").replaceAll(",", "") + ",";
								buffer.write(linea);
								linea =reg.getString("descontante").replaceAll(",", "") + "," + reg.getString("fecha_solicitud")+ "," + reg.getString("fecha_aceptacion")+ ",";
								buffer.write(linea);
								linea =(reg.getString("monto_credito").equals("NA")?reg.getString("monto_credito"):reg.getString("monto_credito")) + ",";
								buffer.write(linea);
								linea =reg.getString("fecha_vencimiento_credito")+ "," + reg.getString("banco_de_servicio").replaceAll(",", "") + ",";
								buffer.write(linea);
								linea =reg.getString("cuenta_bancaria") + "," + reg.getString("tipo_cuenta") + "," + reg.getString("numero_credito") + ",";
								buffer.write(linea);
								linea =reg.getString("moneda_credito") + "," + (reg.getString("ic_estatus_man_doc").equals("1")?"":reg.getString("moneda_descuento")) + ",";//FODEA 015 - 2011 ACF
								buffer.write(linea);
								linea =reg.getString("observaciones").replaceAll("\r\n", " ").replaceAll("\n", " ").replaceAll("\r", " ").replaceAll(",", "") + ",";
								buffer.write(linea);
								linea =(!reg.getString("nueva_fecha_vencimiento").trim().equals("")	?reg.getString("nueva_fecha_vencimiento")	:"NA")+ ",";
								buffer.write(linea);
								linea =(!reg.getString("fecha_modificacion").trim().equals("")		?reg.getString("fecha_modificacion")		:"NA")+ ",";
								buffer.write(linea);
								linea =reg.getString("estatus_solicitud") + "\n";
								buffer.write(linea);
							} else {
								linea =",,,";
								buffer.write(linea);
								linea =listaEpos.get(i)+ ",";
								buffer.write(linea);
								linea =",,,,,,,,,,,,,";//FODEA 015 - 2011 ACF
								buffer.write(linea);
								linea ="\n";
								buffer.write(linea);
							}
						}
						linea = ",\n";
						buffer.write(linea);
					}
					folioAnterior = folioActual;
				}
				buffer.close();
			} catch (Exception e) {
				throw new AppException("Error al generar el archivo", e);
			}	finally {
				}
		}
		
	
		return nombreArchivo;
	}

	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		if ("PDF".equals(tipo)){
		
			try{
				String epos = "";
				String folioActual = "";
				String folioAnterior = "";
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				int nRow = 0;
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
            String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
            String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
            String diaActual    = fechaActual.substring(0,2);
            String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
            String anioActual   = fechaActual.substring(6,10);
            String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
                
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico")!=null?session.getAttribute("iNoNafinElectronico").toString():"",
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
                
            pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
            pdfDoc.addText(" ","formas", ComunesPDF.CENTER);
				
				pdfDoc.setTable(11,100);
				pdfDoc.setCell("A","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Folio","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("N@E PyME","celda01rep",ComunesPDF.CENTER);
            pdfDoc.setCell(" PyME","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("EPO","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("IF Tramita Cr�dito","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Descontante","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Solicitud","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Aceptaci�n","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Cr�dito","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Vencimiento Cr�dito","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("B","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo Cuenta","celda01rep",ComunesPDF.CENTER);//
				pdfDoc.setCell("Banco de Servicio","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Cuenta Bancaria","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Cr�dito","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda Cr�dito","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda Descuento","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Observaciones","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Nueva Fecha Vencimiento Cr�dito","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Modificaci�n Monto","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus","celda01rep",ComunesPDF.CENTER);
				
				String separa = "";
				String Folio,nafinEle,pyme, epo,ifTramita,descontante,fechaSolic,fechaAceptacion,montoCredito,fechaVenc
				,tipoCuenta,bancoServicio,cuentaBanc,numCredito,monedaCredito,monedaDesc,observaciones,nuevaFec,fecMod,estatus;
				while (reg.next()) {
						
					Folio	=	(reg.getString("folio_instruccion")==null)?"":reg.getString("folio_instruccion");
					nafinEle		=	(reg.getString("nafin_electronico")==null)?"":reg.getString("nafin_electronico");
					pyme				=	(reg.getString("nombre_pyme")==null)?"":reg.getString("nombre_pyme");
					epo		=	(reg.getString("nombre_epo")==null)?"":reg.getString("nombre_epo");
					ifTramita				=	(reg.getString("nombre_if")==null)?"":reg.getString("nombre_if");
					descontante			=	(reg.getString("nombre_descontante")==null)?"":reg.getString("nombre_descontante");
					fechaSolic			=	(reg.getString("fecha_solicitud")==null)?"":reg.getString("fecha_solicitud");
					fechaAceptacion			=	(reg.getString("fecha_autorizacion_if")==null)?"":reg.getString("fecha_autorizacion_if");
					montoCredito				=	(reg.getString("monto_credito")==null)?"":reg.getString("monto_credito");
					fechaVenc		=	(reg.getString("nueva_fecha_vencimiento")==null)?"":reg.getString("nueva_fecha_vencimiento");
					tipoCuenta				=	(reg.getString("tipo_cuenta")==null)?"":reg.getString("tipo_cuenta");
					bancoServicio		=	(reg.getString("banco_servicio")==null)?"":reg.getString("banco_servicio");
					cuentaBanc				=	(reg.getString("cuenta_bancaria")==null)?"":reg.getString("cuenta_bancaria");
					numCredito		=	(reg.getString("numero_credito")==null)?"":reg.getString("numero_credito");
					monedaCredito				=	(reg.getString("moneda")==null)?"":reg.getString("moneda");
					monedaDesc		=	(reg.getString("moneda_descuento")==null)?"":reg.getString("moneda_descuento");
					
					observaciones				=	(reg.getString("observaciones")==null)?"":reg.getString("observaciones");
					nuevaFec		=	(reg.getString("nueva_fecha_vencimiento")==null)?"":reg.getString("nueva_fecha_vencimiento");
					fecMod				=	(reg.getString("fecha_modificacion")==null)?"":reg.getString("fecha_modificacion");
					estatus		=	(reg.getString("estatus_solicitud")==null)?"":reg.getString("estatus_solicitud");
						pdfDoc.setCell("A","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell(Folio,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(nafinEle,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(pyme,"formasrep",ComunesPDF.LEFT);						
						pdfDoc.setCell(epo,"formasrep",ComunesPDF.LEFT);						
						pdfDoc.setCell(ifTramita,"formasrep",ComunesPDF.LEFT);
						pdfDoc.setCell(descontante,"formasrep",ComunesPDF.LEFT);
						pdfDoc.setCell(fechaSolic,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaAceptacion,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal((montoCredito.equals("NA")?montoCredito:montoCredito),2),"formasrep",ComunesPDF.RIGHT);
						pdfDoc.setCell(fechaVenc,"formasrep",ComunesPDF.CENTER);
						
						pdfDoc.setCell("B","celda01rep",ComunesPDF.CENTER);
                  pdfDoc.setCell(tipoCuenta,"formasrep",ComunesPDF.CENTER);//fodea 022-2012: cambio de posicion
						pdfDoc.setCell(bancoServicio,"formasrep",ComunesPDF.LEFT);
						pdfDoc.setCell(cuentaBanc,"formasrep",ComunesPDF.CENTER);
						//pdfDoc.setCell(reg.getString("tipo_cuenta"),"formasrep",ComunesPDF.LEFT); //fodea 022-2012: cambio de posicion
						pdfDoc.setCell(numCredito,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(monedaCredito,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(monedaDesc,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(observaciones.replaceAll("\r\n", " ").replaceAll("\n", " ").replaceAll("\r", " "),"formasrep",ComunesPDF.LEFT);
						pdfDoc.setCell((!nuevaFec.trim().equals("")?nuevaFec	:"NA"),"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell((!fecMod.trim().equals("")?fecMod:"NA"),"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(estatus,"formasrep",ComunesPDF.CENTER);
					
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
			}
		}else if ("CSV".equals(tipo)){
			try{
				List listaEpos = new ArrayList();
				int rowspan = 0;
				String folioActual = "";
				String folioAnterior = "";
				HttpSession session = request.getSession();
	
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
				buffer = new BufferedWriter(writer);
	
				linea = "FOLIO, EPO, IF TRAMITA CREDITO, DESCONTANTE, FECHA SOLICITUD, FECHA ACEPTACION, MONTO CREDITO, FECHA VENCIMIENTO CREDITO, TIPO CUENTA, BANCO DE SERVICIO, CUENTA BANCARIA, NUMERO CREDITO, MONEDA CREDITO, MONEDA DESCUENTO, OBSERVACIONES, NUEVA FECHA VENCIMIENTO CREDITO, FECHA MODIFICACION MONTO, ESTATUS SOLICITUD,\n";//FODEA 015 - 2011 ACF
				buffer.write(linea);

				while (reg.next()) {
					//int iRegEnturno = reg.getNumRegistro();
					folioActual = reg.getString("folio_instruccion");
						
						//reg.setNumRegistro(iRegEnturno);
						for (int i = 0; i < rowspan; i++) {
							if (i == 0) {
								linea = reg.getString("folio_instruccion") + "," + listaEpos.get(i).toString().replaceAll(",", "") + "," + reg.getString("if_tramita_credito").replaceAll(",", "") + ",";
								buffer.write(linea);
								linea =reg.getString("descontante").replaceAll(",", "") + "," + reg.getString("fecha_solicitud")+ "," + reg.getString("fecha_aceptacion")+ ",";
								buffer.write(linea);
								linea =(reg.getString("monto_credito").equals("NA")?reg.getString("monto_credito"):reg.getString("monto_credito")) + ",";
								buffer.write(linea);
								linea =reg.getString("fecha_vencimiento_credito")+ "," + reg.getString("tipo_cuenta") + "," + reg.getString("banco_de_servicio").replaceAll(",", "") + ",";
								buffer.write(linea);
								linea =reg.getString("cuenta_bancaria")  + "," + reg.getString("numero_credito") + ",";
								buffer.write(linea);
								linea =reg.getString("moneda_credito") + "," + (reg.getString("ic_estatus_man_doc").equals("1")?"":reg.getString("moneda_descuento")) + ",";//FODEA 015 - 2011 ACF
								buffer.write(linea);
								linea =reg.getString("observaciones").replaceAll("\r\n", " ").replaceAll("\n", " ").replaceAll("\r", " ").replaceAll(",", "") + ",";
								buffer.write(linea);
								linea =(!reg.getString("nueva_fecha_vencimiento").trim().equals("")	?reg.getString("nueva_fecha_vencimiento")	:"NA")+ ",";
								buffer.write(linea);
								linea =(!reg.getString("fecha_modificacion").trim().equals("")		?reg.getString("fecha_modificacion")		:"NA")+ ",";
								buffer.write(linea);
								linea =reg.getString("estatus_solicitud") + "\n";
								buffer.write(linea);
							
						}
						linea = ",\n";
						buffer.write(linea);
					}
				}
				buffer.close();
			} catch (Exception e) {
				throw new AppException("Error al generar el archivo", e);
			}	finally {
				}
		}else if ("ConsultaPDF".equals(tipo)){
			try{
				String epos = "";
				String folioActual = "";
				String folioAnterior = "";
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				int nRow = 0;
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
            String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
            String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
            String diaActual    = fechaActual.substring(0,2);
            String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
            String anioActual   = fechaActual.substring(6,10);
            String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
                
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
                
            pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
            pdfDoc.addText(" ","formas", ComunesPDF.CENTER);
				
				pdfDoc.setTable(10,100);
				pdfDoc.setCell("Folio","celda01rep",ComunesPDF.CENTER);
				if( "ADMIN IF".equals(perfil_usuario) ||  "DESCONT IF".equals(perfil_usuario) ) {
               pdfDoc.setCell("N@E PyME","celda01rep",ComunesPDF.CENTER);
               pdfDoc.setCell(" PyME","celda01rep",ComunesPDF.CENTER);
				}							
				pdfDoc.setCell("EPO","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("IF Tramita Cr�dito","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Descontante","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Solicitud","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Aceptaci�n","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Cr�dito","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Vencimiento Cr�dito","celda01rep",ComunesPDF.CENTER);
				if(!"ADMIN IF".equals(perfil_usuario) &&  !"DESCONT IF".equals(perfil_usuario) ) {
					pdfDoc.setCell(" ","celda01rep",ComunesPDF.CENTER,2);				
				}
				
            pdfDoc.setCell("Tipo Cuenta","celda01rep",ComunesPDF.CENTER); //fodea 022-2012:cambio de posicion
				pdfDoc.setCell("Banco de Servicio","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Cuenta Bancaria","celda01rep",ComunesPDF.CENTER);
				//pdfDoc.setCell("Tipo Cuenta","celda01rep",ComunesPDF.CENTER);//fodea 022-2012: cambio de posicion
				pdfDoc.setCell("N�mero Cr�dito","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda Cr�dito","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda Descuento","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Observaciones","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Nueva Fecha Vencimiento Cr�dito","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Modificaci�n Monto","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus","celda01rep",ComunesPDF.CENTER);
				String separa = "";
				while (reg.next()) {
					int iRegEnturno = reg.getNumRegistro();
					folioActual = reg.getString("folio_instruccion");
					if (!folioActual.equals(folioAnterior)) {
						epos = "";
						//reg.rewind();
						separa = "";
						while (regAux.next()) {
							if (folioActual.equals(regAux.getString("folio_instruccion"))) {
								epos = epos + separa + regAux.getString("epo") +  "\n" ;
								separa = " / ";
							}
						}
						//reg.setNumRegistro(iRegEnturno);
						pdfDoc.setCell(reg.getString("folio_instruccion"),"formasrep",ComunesPDF.CENTER);
						if( "ADMIN IF".equals(perfil_usuario) ||  "DESCONT IF".equals(perfil_usuario) ) {
						pdfDoc.setCell(reg.getString("nafin_electronico"),"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(reg.getString("pyme"),"formasrep",ComunesPDF.CENTER);						
						}
						pdfDoc.setCell(epos,"formasrep",ComunesPDF.LEFT);
						pdfDoc.setCell(reg.getString("if_tramita_credito"),"formasrep",ComunesPDF.LEFT);
						pdfDoc.setCell(reg.getString("descontante"),"formasrep",ComunesPDF.LEFT);
						pdfDoc.setCell(reg.getString("fecha_solicitud"),"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(reg.getString("fecha_aceptacion"),"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+Comunes.formatoDecimal((reg.getString("monto_credito").equals("NA")?reg.getString("monto_credito"):reg.getString("monto_credito")),2),"formasrep",ComunesPDF.LEFT);
						pdfDoc.setCell(reg.getString("fecha_vencimiento_credito"),"formasrep",ComunesPDF.CENTER);
						if(!"ADMIN IF".equals(perfil_usuario) &&   !"DESCONT IF".equals(perfil_usuario) ) {
							pdfDoc.setCell(" ","formasrep",ComunesPDF.CENTER,2);				
						}					
						
                  pdfDoc.setCell(reg.getString("tipo_cuenta"),"formasrep",ComunesPDF.LEFT);//fodea 022-2012: cambio de posicion
						pdfDoc.setCell(reg.getString("banco_de_servicio"),"formasrep",ComunesPDF.LEFT);
						pdfDoc.setCell(reg.getString("cuenta_bancaria"),"formasrep",ComunesPDF.LEFT);
						//pdfDoc.setCell(reg.getString("tipo_cuenta"),"formasrep",ComunesPDF.LEFT); //fodea 022-2012: cambio de posicion
						pdfDoc.setCell(reg.getString("numero_credito"),"formasrep",ComunesPDF.LEFT);
						pdfDoc.setCell(reg.getString("moneda_credito"),"formasrep",ComunesPDF.LEFT);
						pdfDoc.setCell((reg.getString("ic_estatus_man_doc").equals("1")?"":reg.getString("moneda_descuento")),"formasrep",ComunesPDF.LEFT);
						pdfDoc.setCell(reg.getString("observaciones").replaceAll("\r\n", " ").replaceAll("\n", " ").replaceAll("\r", " "),"formasrep",ComunesPDF.LEFT);
						pdfDoc.setCell((!reg.getString("nueva_fecha_vencimiento").trim().equals("")?reg.getString("nueva_fecha_vencimiento")	:"NA"),"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell((!reg.getString("fecha_modificacion").trim().equals("")?reg.getString("fecha_modificacion")		:"NA"),"formasrep",ComunesPDF.CENTER);
						pdfDoc.setCell(reg.getString("estatus_solicitud"),"formasrep",ComunesPDF.CENTER);
					}
					folioAnterior = folioActual;
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
			}
		}else if ("ConsultaCSV".equals(tipo)){
			try{
				List listaEpos = new ArrayList();
				int rowspan = 0;
				String folioActual = "";
				String folioAnterior = "";
				HttpSession session = request.getSession();
	
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
				buffer = new BufferedWriter(writer);
	
				linea = "FOLIO, EPO, IF TRAMITA CREDITO, DESCONTANTE, FECHA SOLICITUD, FECHA ACEPTACION, MONTO CREDITO, FECHA VENCIMIENTO CREDITO, TIPO CUENTA, BANCO DE SERVICIO, CUENTA BANCARIA, NUMERO CREDITO, MONEDA CREDITO, MONEDA DESCUENTO, OBSERVACIONES, NUEVA FECHA VENCIMIENTO CREDITO, FECHA MODIFICACION MONTO, ESTATUS SOLICITUD,\n";//FODEA 015 - 2011 ACF
				buffer.write(linea);

				while (reg.next()) {
					//int iRegEnturno = reg.getNumRegistro();
					folioActual = reg.getString("folio_instruccion");
					if (!folioActual.equals(folioAnterior)) {
						listaEpos = new ArrayList();
						rowspan = 0;
						//reg.rewind();
						while (regAux.next()) {
							if (folioActual.equals(regAux.getString("folio_instruccion"))) {
								listaEpos.add(regAux.getString("epo"));
								rowspan++;
							}
						}
						//reg.setNumRegistro(iRegEnturno);
						for (int i = 0; i < rowspan; i++) {
							if (i == 0) {
								linea = reg.getString("folio_instruccion") + "," + listaEpos.get(i).toString().replaceAll(",", "") + "," + reg.getString("if_tramita_credito").replaceAll(",", "") + ",";
								buffer.write(linea);
								linea =reg.getString("descontante").replaceAll(",", "") + "," + reg.getString("fecha_solicitud")+ "," + reg.getString("fecha_aceptacion")+ ",";
								buffer.write(linea);
								linea =(reg.getString("monto_credito").equals("NA")?reg.getString("monto_credito"):reg.getString("monto_credito")) + ",";
								buffer.write(linea);
								linea =reg.getString("fecha_vencimiento_credito")+ "," + reg.getString("tipo_cuenta") + "," + reg.getString("banco_de_servicio").replaceAll(",", "") + ",";
								buffer.write(linea);
								linea =reg.getString("cuenta_bancaria")  + "," + reg.getString("numero_credito") + ",";
								buffer.write(linea);
								linea =reg.getString("moneda_credito") + "," + (reg.getString("ic_estatus_man_doc").equals("1")?"":reg.getString("moneda_descuento")) + ",";//FODEA 015 - 2011 ACF
								buffer.write(linea);
								linea =reg.getString("observaciones").replaceAll("\r\n", " ").replaceAll("\n", " ").replaceAll("\r", " ").replaceAll(",", "") + ",";
								buffer.write(linea);
								linea =(!reg.getString("nueva_fecha_vencimiento").trim().equals("")	?reg.getString("nueva_fecha_vencimiento")	:"NA")+ ",";
								buffer.write(linea);
								linea =(!reg.getString("fecha_modificacion").trim().equals("")		?reg.getString("fecha_modificacion")		:"NA")+ ",";
								buffer.write(linea);
								linea =reg.getString("estatus_solicitud") + "\n";
								buffer.write(linea);
							} else {
								linea =",";
								buffer.write(linea);
								linea =listaEpos.get(i)+ ",";
								buffer.write(linea);
								linea =",,,,,,,,,,,,,";//FODEA 015 - 2011 ACF
								buffer.write(linea);
								linea ="\n";
								buffer.write(linea);
							}
						}
						linea = ",\n";
						buffer.write(linea);
					}
					folioAnterior = folioActual;
				}
				buffer.close();
			} catch (Exception e) {
				throw new AppException("Error al generar el archivo", e);
			}	finally {
				}
		}else if ("AcusePDF".equals(tipo)) {
			try{
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				int nRow = 0;
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
            String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
            String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
            String diaActual    = fechaActual.substring(0,2);
            String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
            String anioActual   = fechaActual.substring(6,10);
            String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
                
				  
				String pais = (String)(request.getSession().getAttribute("strPais") == null?"":request.getSession().getAttribute("strPais"));
				String logo = (String)(request.getSession().getAttribute("strLogo") == null?"":request.getSession().getAttribute("strLogo"));
        
				if(usuarioCarga.length()>0)  { 
					String loginUsuario = usuarioCarga.substring(0, usuarioCarga.indexOf("-"));				
					String nombreUsuario = usuarioCarga.substring(usuarioCarga.indexOf("-") + 1);
				}
		
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico")!=null?session.getAttribute("iNoNafinElectronico").toString():"",
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				 
				
            pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
            pdfDoc.addText(" ","formas", ComunesPDF.CENTER);
				
				pdfDoc.setTable(2,50);
				pdfDoc.setCell("Cifras de Control","celda01",ComunesPDF.CENTER,2);
				pdfDoc.setCell("Nombre de la(s) EPO(s):","formas");
				pdfDoc.setCell(nombreEpo,"formas");
				pdfDoc.setCell("IF que tramita el cr�dito:","formas");//FODEA 012 - 2010 ACF
				pdfDoc.setCell(nombreIf,"formas");
				pdfDoc.setCell("Descontante:","formas");//FODEA 012 - 2010 ACF
				pdfDoc.setCell(nombreDescontante,"formas");//FODEA 012 - 2010 ACF
				pdfDoc.setCell("N�mero de Folio:","formas");
				pdfDoc.setCell(folioInstruccionIrrevocable,"formas");
				pdfDoc.setCell("Fecha de carga:","formas");
				pdfDoc.setCell(fechaAct,"formas");
				pdfDoc.setCell("Hora de carga:","formas");
				pdfDoc.setCell(horaAct,"formas");
				pdfDoc.setCell("Usuario:","formas");
				pdfDoc.setCell(usuarioCarga,"formas");
			  pdfDoc.addTable();
				if (claveTipoCadena.equals("1")) {
						pdfDoc.addText(" ","formasB",ComunesPDF.CENTER);
						pdfDoc.addText("INSTRUCCI�N IRREVOCABLE","formasB",ComunesPDF.CENTER);
					pdfDoc.setTable(1,100);//13-04-2010 REBOS
					pdfDoc.setCell(
						"Para efectos de las presentes instrucciones irrevocables se entiende por:\n\n"+
						"CADENAS PRODUCTIVAS: Productos y servicios a trav�s de un sistema en Internet, desarrollado por NAFIN, para, entre otras cosas consultar "+ 
						"informaci�n, intercambiar datos, enviar mensajes de datos y realizar transacciones financieras.\n\n"+
						"CONTRATO:	Instrumento jur�dico formalizado entre una DEPENDENCIA O ENTIDAD y el SUJETO DE APOYO, con objeto de que �ste le provea "+
						"determinados bienes o servicios o realice obra p�blica.\n\n"+
						"CR�DITO:	Financiamiento otorgado por el INTERMEDIARIO FINANCIERO al SUJETO DE APOYO, en relaci�n con un CONTRATO hasta por el 50% (cincuenta "+
						"por ciento) del monto del mismo.\n\n"+
						"CUENTA BANCARIA:	La aperturada por el SUJETO DE APOYO con el INTERMEDIARIO FINANCIERO, con el fin de que en �sta se realice el dep�sito de las "+
						"disposiciones del CR�DITO otorgado por el INTERMEDIARIO FINANCIERO al SUJETO DE APOYO, as� como la totalidad de los recursos que tenga derecho "+
						"a recibir el SUJETO DE APOYO como contraprestaci�n por las operaciones de FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO, as� como para que el "+
						"INTERMEDIARIO FINANCIERO realice los cargos para el pago del CR�DITO y sus accesorios, de conformidad a lo se�alado en este instrumento y en el "+
						"contrato de cr�dito correspondiente.\n\n"+
						"DEPENDENCIA O ENTIDAD:	Dependencias y Entidades de la Administraci�n P�blica Federal centralizada y paraestatal que en CADENAS PRODUCTIVAS "+
						"tienen el car�cter de EMPRESA DE PRIMER ORDEN, y que tengan celebrado un CONTRATO con el SUJETO DE APOYO.\n\n"+	
						"DOCUMENTOS:	Contrarrecibos o cualquier otro instrumento en papel o en cualquier medio electr�nico que emite la DEPENDENCIA O ENTIDAD con motivo "+
						"de uno o m�s CONTRATOS, en los cuales se hacen constar derechos de cr�dito a su cargo y en favor del SUJETO DE APOYO.\n\n"+
						"DESCONTANTE: Entidad financiera, del mismo grupo financiero del que forma parte el INTERMEDIARIO FINANCIERO que, de conformidad a la INSTRUCCI�N "+
						"IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE, realizar� las operaciones de FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO con el SUJETO DE "+
						"APOYO a trav�s de CADENAS PRODUCTIVAS.\n\n"+
						"EMPRESA DE PRIMER ORDEN:	Sociedades mercantiles, Dependencias o Entidades de la Administraci�n P�blica Federal, Gobiernos de los Estados y Municipios, "+
						"que reciben bienes o servicios u obra p�blica y que tiene el car�cter de EMPRESAS DE PRIMER ORDEN en CADENAS PRODUCTIVAS.\n\n"+
						"FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO:	Operaciones que se realizan en forma autom�tica, a trav�s de CADENAS PRODUCTIVAS una vez que el SUJETO "+
						"DE APOYO haya otorgado la INSTRUCCI�N IRREVOCABLE NAFIN y la INSTRUCCION IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE; mediante las cuales el "+
						"INTERMEDIARIO FINANCIERO o el DESCONTANTE adquiere la propiedad de los derechos de cr�dito que se hacen constar en los DOCUMENTOS sin necesidad de que "+
						"se otorgue para cada operaci�n el consentimiento del SUJETO DE APOYO.\n\n"+
						"INSTRUCCI�N IRREVOCABLE NAFIN:	Es la otorgada por el SUJETO DE APOYO a NAFIN para el efecto de que NAFIN por cuenta y orden del SUJETO DE APOYO, active "+
						"en CADENAS PRODUCTIVAS �nicamente al INTERMEDIARIO FINANCIERO o DESCONTANTE, a efecto de que sea �ste exclusivamente quien realice el FACTORAJE O "+
						"DESCUENTO ELECTR�NICO AUTOM�TICO de la totalidad de los DOCUMENTOS emitidos por la DEPENDENCIA O ENTIDAD con la que el SUJETO DE APOYO celebr� el "+
						"CONTRATO relacionado con el CR�DITO, a�n cuando dichos DOCUMENTOS no hayan derivado de un CONTRATO relacionado con un CR�DITO, as� como para que NAFIN "+
						"d� a conocer exclusivamente al INTERMEDIARIO FINANCIERO o DESCONTANTE, a trav�s de CADENAS PRODUCTIVAS todos los DOCUMENTOS que han sido publicados por "+
						"dicha DEPENDENCIA O ENTIDAD.\n\n"+ 
						"En el entendido de que la instrucci�n irrevocable implica que con los recursos del FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO se pueda pagar el CR�DITO "+
						"que otorg� el INTERMEDIARIO FINANCIERO al SUJETO DE APOYO. \n\n"+
						"INSTRUCCI�N IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE:	Es la otorgada por el SUJETO DE APOYO al INTERMEDIARIO FINANCIERO o DESCONTANTE para "+
						"que opere en FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO, en CADENAS PRODUCTIVAS, la totalidad de los DOCUMENTOS publicados por la DEPENDENCIA O "+
						"ENTIDAD que se�ale el SUJETO DE APOYO al otorgar la instrucci�n, as� como para que el INTERMEDIARIO FINANCIERO o DESCONTANTE deposite en la CUENTA "+
						"BANCARIA los recursos que tenga derecho a recibir el SUJETO DE APOYO como contraprestaci�n por las operaciones de FACTORAJE O DESCUENTO ELECTR�NICO "+
						"AUTOM�TICO, adem�s para que dichos recursos puedan ser aplicados por el INTERMEDIARIO FINANCIERO al pago del CR�DITO y sus accesorios.\n\n"+
						"INTERMEDIARIO FINANCIERO:	Instituci�n que otorgue cr�dito al SUJETO DE APOYO relacionado con un CONTRATO y en su caso, autorizada para realizar "+
						"operaciones de FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO con el SUJETO DE APOYO a trav�s de CADENAS PRODUCTIVAS.\n\n"+
						"L�NEA DE FINANCIAMIENTO: Recursos que el INTERMEDIARIO FINANCIERO pone a disposici�n del SUJETO DE APOYO para ser ejercidos de conformidad con lo "+
						"se�alado en el convenio y al CR�DITO.\n\n"+
						"REDESCUENTO ELECTR�NICO:	Acto por el cual el INTERMEDIARIO FINANCIERO o DESCONTANTE, transmite bajo su responsabilidad a NAFIN, el mensaje de datos "+
						"para solicitar a NAFIN recursos para su operaci�n.\n\n"+
						"SUJETO DE APOYO	Micro, peque�as y medianas empresas y personas f�sicas con actividad empresarial que se encuentren dentro de los rangos fijados por "+
						"la Secretar�a de Econom�a o que �sta llegue a establecer, as� como en los par�metros establecidos por NAFIN en sus diversas circulares y todas aquellas "+
						"personas morales que sean proveedores de una DEPENDENCIA O ENTIDAD, que cuenten con un CR�DITO otorgado por el INTERMEDIARIO FINANCIERO y que hayan "+
						"otorgado la INSTRUCCI�N IRREVOCABLE NAFIN y la INSTRUCCI�N IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE.	\n\n"
					,"formas");
					pdfDoc.addTable();
					
					pdfDoc.addText("INSTRUCCI�N IRREVOCABLE NAFIN","formasB",ComunesPDF.CENTER);
					pdfDoc.setTable(1,100);//13-04-2010 REBOS
					pdfDoc.setCell(	
						"Que en este acto otorga el SUJETO DE APOYO a Nacional Financiera, S.N.C. Instituci�n de Banca de Desarrollo (NAFIN), derivada del CR�DITO autorizado "+
						"por parte del INTERMEDIARIO FINANCIERO, relacionado con el CONTRATO y que dicha instrucci�n aceptar� NAFIN, a efecto de que:\n\n"+
						"a)	NAFIN por cuenta y orden del SUJETO DE APOYO, active �nicamente en CADENAS PRODUCTIVAS al INTERMEDIARIO FINANCIERO o DESCONTANTE para el efecto de "+
						"que sea �ste exclusivamente quien realice el FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO de todos y cada uno de los DOCUMENTOS en que consten derechos "+
						"de cr�dito a favor del SUJETO DE APOYO emitidos por la DEPENDENCIA O ENTIDAD con la que tenga celebrado el CONTRATO relacionado con el CR�DITO, a�n "+
						"cuando dichos DOCUMENTOS no hayan derivado de un CONTRATO relacionado con un CR�DITO.\n\n"+
						"b)	NAFIN d� a conocer exclusivamente al INTERMEDIARIO FINANCIERO o DESCONTANTE, que haya elegido el SUJETO DE APOYO, los DOCUMENTOS que han sido publicados "+
						"por la DEPENDENCIA O ENTIDAD correspondiente en las CADENAS PRODUCTIVAS, de los que sea beneficiario el SUJETO DE APOYO.\n\n"+ 
						"c)	NAFIN d� a conocer al INTERMEDIARIO FINANCIERO y/o al DESCONTATARIO a trav�s de CADENAS PRODUCTIVAS los DOCUMENTOS y montos que se encuentren publicados "+
						"en dicho sistema de CADENAS PRODUCTIVAS.\n\n"+
						"Lo anterior para el efecto de que el INTERMEDIARIO FINANCIERO o DESCONTANTE se encuentre en facultad de operar en FACTORAJE O DESCUENTO ELECTR�NICO "+
						"AUTOM�TICO, en CADENAS PRODUCTIVAS, los derechos de cobro o cr�dito a favor del SUJETO DE APOYO que otorga la presente instrucci�n y que se hacen constar "+
						"en los DOCUMENTOS y, que los recursos que deba recibir el SUJETO DE APOYO como contraprestaci�n por las operaciones de FACTORAJE O DESCUENTO ELECTR�NICO "+
						"AUTOM�TICO los deposite el DESCONTANTE en la CUENTA BANCARIA, manifestando en este acto, que dichos recursos podr�n ser aplicados por el INTERMEDIARIO "+
						"FINANCIERO al pago del CR�DITO y sus accesorios.\n\n"+
						"Esta instrucci�n irrevocable no podr� ser extinguida o modificada por el SUJETO DE APOYO mientras exista cualquier saldo pendiente de pago al INTERMEDIARIO "+
						"FINANCIERO, y solo podr� ser revocada con la autorizaci�n expresa del mismo.\n\n"+
						"Asimismo, como SUJETO DE APOYO manifiesto mi conformidad para que NAFIN se reserve el derecho de no proveer al amparo de esta instrucci�n el servicio de "+
						"Cadenas Productivas en caso de incumplimiento del SUJETO DE APOYO con el INTERMEDIARIO FINANCIERO.\n\n"+ 
						"En este acto como SUJETO DE APOYO me obligo a no modificar la forma de pago y a no solicitar mi baja de CADENAS PRODUCTIVAS, mientras exista cualquier saldo "+
						"pendiente de pago al INTERMEDIARIO FINANCIERO, sin la autorizaci�n expresa de �ste.\n\n"+
						"Me obligo a no ceder los derechos de cobro de ning�n CONTRATO adjudicado por la DEPENDENCIA O ENTIDAD seleccionada al otorgar esta instrucci�n, a persona "+
						"alguna distinta del INTERMEDIARIO FINANCIERO o el DESCONTANTE durante la vigencia del CR�DITO.\n\n"+
						"Al transmitir el mensaje de datos que contiene esta instrucci�n irrevocable, manifiesto mi conformidad con la misma, para todos los efectos legales "+
						"correspondientes, liberando a NAFIN de cualquier responsabilidad por su aceptaci�n y cumplimiento.\n\n"
					,"formas");
					pdfDoc.addTable();
					
					pdfDoc.addText("INSTRUCCI�N IRREVOCABLE - INTERMEDIARIO FINANCIERO Y/O DESCONTANTE","formasB",ComunesPDF.CENTER);
					pdfDoc.setTable(1,100);//13-04-2010 REBOS
					pdfDoc.setCell(
						"Que en este acto otorga el SUJETO DE APOYO al INTERMEDIARIO FINANCIERO o DESCONTANTE, derivado del CR�DITO que le otorg� el INTERMEDIARIO FINANCIERO, a "+
						"efecto de que:\n\n"+
						"a)	El INTERMEDIARIO FINANCIERO o el DESCONTANTE opere en FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO, en CADENAS PRODUCTIVAS todos y cada uno de los "+
						"DOCUMENTOS publicados por la DEPENDENCIA O ENTIDAD con la que el SUJETO DE APOYO tenga celebrado el CONTRATO relacionado con el CR�DITO, a�n cuando dichos "+
						"DOCUMENTOS no hayan derivado de un CONTRATO relacionado con un CR�DITO.\n\n"+
						"b)	El INTERMEDIARIO FINANCIERO o el DESCONTANTE deposite los recursos que deba recibir el SUJETO DE APOYO como contraprestaci�n por el FACTORAJE O DESCUENTO "+
						"ELECTR�NICO AUTOM�TICO en la CUENTA BANCARIA, en el entendido de que dichos recursos podr�n ser aplicados por el INTERMEDIARIO FINANCIERO al pago del CR�DITO "+
						"y sus accesorios.\n\n"+
						"Lo anterior no exime al SUJETO DE APOYO de continuar pagando el CR�DITO, m�s sus accesorios, en las fechas convenidas con el INTERMEDIARIO FINANCIERO.\n\n"+
						"c)	El INTERMEDIARIO FINANCIERO proporcione a NAFIN y al DESCONTANTE, en su caso,  las caracter�sticas del CR�DITO que me fue otorgado como SUJETO DE APOYO "+
						"(cuenta, monto, fecha de vencimiento, banco para dep�sito etc.).\n\n"+
						"Esta instrucci�n irrevocable no podr� ser extinguida o modificada por el SUJETO DE APOYO mientras exista cualquier saldo pendiente de pago al INTERMEDIARIO "+
						"FINANCIERO, y solo podr� ser revocada con la autorizaci�n expresa del mismo.\n\n"+
						"En este acto como SUJETO DE APOYO me obligo a no modificar la forma de pago y a no solicitar mi baja de CADENAS PRODUCTIVAS, mientras exista cualquier saldo "+
						"pendiente de pago al INTERMEDIARIO FINANCIERO, sin la autorizaci�n expresa de �ste.\n\n"+
						"Me obligo como SUJETO DE APOYO a no ceder los derechos de cobro de ning�n CONTRATO adjudicado por la DEPENDENCIA O ENTIDAD seleccionada al otorgar esta "+
						"instrucci�n, a persona alguna distinta del INTERMEDIARIO FINANCIERO o el DESCONTANTE durante la vigencia del CR�DITO.\n\n"+
						"Al transmitir el mensaje de datos que contiene esta instrucci�n irrevocable, manifiesto mi conformidad con la misma, para todos los efectos legales "+
						"correspondientes, liberando al INTERMEDIARIO FINANCIERO y/o al DESCONTANTE de cualquier responsabilidad por su aceptaci�n y cumplimiento.\n\n"
					,"formas");	    
					pdfDoc.addTable();
				} else if (claveTipoCadena.equals("2")) {
					pdfDoc.addText(" ","formasB",ComunesPDF.CENTER);
					pdfDoc.addText("INSTRUCCI�N IRREVOCABLE","formasB",ComunesPDF.CENTER);
					pdfDoc.setTable(1,100);//13-04-2010 REBOS
					pdfDoc.setCell(
						"Para efectos de las presentes instrucciones irrevocables se entiende por:\n\n"+
						"CADENAS PRODUCTIVAS: Productos y servicios a trav�s de un sistema en Internet, desarrollado por NAFIN, para, entre otras cosas consultar "+ 
						"informaci�n, intercambiar datos, enviar mensajes de datos y realizar transacciones financieras.\n\n"+
						"CR�DITO:	Financiamiento otorgado por el INTERMEDIARIO FINANCIERO al SUJETO DE APOYO.\n\n"+
						"CUENTA BANCARIA:	La aperturada por el SUJETO DE APOYO con el INTERMEDIARIO FINANCIERO, con el fin de que en �sta se realice el dep�sito de las "+
						"disposiciones del CR�DITO otorgado por el INTERMEDIARIO FINANCIERO al SUJETO DE APOYO, as� como la totalidad de los recursos que tenga derecho "+
						"a recibir el SUJETO DE APOYO como contraprestaci�n por las operaciones de FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO, as� como para que el "+
						"INTERMEDIARIO FINANCIERO realice los cargos para el pago del CR�DITO y sus accesorios, de conformidad a lo se�alado en este instrumento y en el "+
						"contrato de cr�dito correspondiente.\n\n"+
						"DOCUMENTOS:	Contrarrecibos o cualquier otro instrumento en papel o en cualquier medio electr�nico que emite la EMPRESA DE PRIMER ORDEN en los cuales se hacen constar derechos de cr�dito a su cargo y en favor del SUJETO DE APOYO.\n\n"+
						"DESCONTANTE: Entidad financiera, del mismo grupo financiero del que forma parte el INTERMEDIARIO FINANCIERO que, de conformidad a la INSTRUCCI�N "+
						"IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE, realizar� las operaciones de FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO con el SUJETO DE "+
						"APOYO a trav�s de CADENAS PRODUCTIVAS.\n\n"+
						"EMPRESA DE PRIMER ORDEN:	Sociedad mercantil que recibe bienes o servicios del SUJETO DE APOYO.\n\n"+
						"FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO:	Operaciones que se realizan en forma autom�tica, a trav�s de CADENAS PRODUCTIVAS una vez que el SUJETO "+
						"DE APOYO haya otorgado la INSTRUCCI�N IRREVOCABLE NAFIN y la INSTRUCCION IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE; mediante las cuales el "+
						"INTERMEDIARIO FINANCIERO o el DESCONTANTE adquiere la propiedad de los derechos de cr�dito que se hacen constar en los DOCUMENTOS sin necesidad de que "+
						"se otorgue para cada operaci�n el consentimiento del SUJETO DE APOYO.\n\n"+
						"INSTRUCCI�N IRREVOCABLE NAFIN:	Es la otorgada por el SUJETO DE APOYO a NAFIN para el efecto de que NAFIN por cuenta y orden del SUJETO DE APOYO, active "+
						"en CADENAS PRODUCTIVAS �nicamente al INTERMEDIARIO FINANCIERO o DESCONTANTE, a efecto de que sea �ste exclusivamente quien realice el FACTORAJE O "+
						"DESCUENTO ELECTR�NICO AUTOM�TICO de la totalidad de los DOCUMENTOS emitidos por la EMPRESA DE PRIMER ORDEN as� como para que NAFIN "+
						"d� a conocer exclusivamente al INTERMEDIARIO FINANCIERO o DESCONTANTE, a trav�s de CADENAS PRODUCTIVAS todos los DOCUMENTOS que han sido publicados por "+
						"dicha EMPRESA DE PRIMER ORDEN.\n\n"+ 
						"En el entendido de que la instrucci�n irrevocable implica que con los recursos del FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO se pueda pagar el CR�DITO "+
						"que otorg� el INTERMEDIARIO FINANCIERO al SUJETO DE APOYO. \n\n"+
						"INSTRUCCI�N IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE:	Es la otorgada por el SUJETO DE APOYO al INTERMEDIARIO FINANCIERO o DESCONTANTE para "+
						"que opere en FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO, en CADENAS PRODUCTIVAS, la totalidad de los DOCUMENTOS publicados por la EMPRESA DE PRIMER ORDEN "+
						"que se�ale el SUJETO DE APOYO al otorgar la instrucci�n, as� como para que el INTERMEDIARIO FINANCIERO o DESCONTANTE deposite en la CUENTA "+
						"BANCARIA los recursos que tenga derecho a recibir el SUJETO DE APOYO como contraprestaci�n por las operaciones de FACTORAJE O DESCUENTO ELECTR�NICO "+
						"AUTOM�TICO, adem�s para que dichos recursos puedan ser aplicados por el INTERMEDIARIO FINANCIERO al pago del CR�DITO y sus accesorios.\n\n"+
						"INTERMEDIARIO FINANCIERO:	Instituci�n que otorgue cr�dito al SUJETO DE APOYO relacionado con un CONTRATO y en su caso, autorizada para realizar "+
						"operaciones de FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO con el SUJETO DE APOYO a trav�s de CADENAS PRODUCTIVAS.\n\n"+
						"REDESCUENTO ELECTR�NICO:	Acto por el cual el INTERMEDIARIO FINANCIERO o DESCONTANTE, transmite bajo su responsabilidad a NAFIN, el mensaje de datos "+
						"para solicitar a NAFIN recursos para su operaci�n.\n\n"+
						"SUJETO DE APOYO	Micro, peque�as y medianas empresas y personas f�sicas con actividad empresarial que se encuentren dentro de los rangos fijados por "+
						"la Secretar�a de Econom�a o que �sta llegue a establecer, as� como en los par�metros establecidos por NAFIN en sus diversas circulares y todas aquellas "+
						"personas morales que sean proveedores de una EMPRESA DE PRIMER ORDEN, que cuenten con un CR�DITO otorgado por el INTERMEDIARIO FINANCIERO y que hayan "+
						"otorgado la INSTRUCCI�N IRREVOCABLE NAFIN y la INSTRUCCI�N IRREVOCABLE INTERMEDIARIO FINANCIERO Y/O DESCONTANTE.	\n\n"
					,"formas");
					pdfDoc.addTable();
					
					pdfDoc.addText("INSTRUCCI�N IRREVOCABLE NAFIN","formasB",ComunesPDF.CENTER);
					pdfDoc.setTable(1,100);//13-04-2010 REBOS
					pdfDoc.setCell(	
						"Que en este acto otorga el SUJETO DE APOYO a Nacional Financiera, S.N.C. Instituci�n de Banca de Desarrollo (NAFIN), derivada del CR�DITO y que aceptada por NAFIN, a efecto de que:\n\n"+
						"a)	NAFIN por cuenta y orden del SUJETO DE APOYO, active �nicamente en CADENAS PRODUCTIVAS al INTERMEDIARIO FINANCIERO o DESCONTANTE para el efecto de "+
						"que sea �ste exclusivamente quien realice el FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO de todos y cada uno de los DOCUMENTOS en que consten derechos "+
						"de cr�dito a favor del SUJETO DE APOYO emitidos por la EMPRESA DE PRIMER ORDEN.\n\n"+
						"b)	NAFIN d� a conocer exclusivamente al INTERMEDIARIO FINANCIERO o DESCONTANTE, que haya elegido el SUJETO DE APOYO, los DOCUMENTOS que han sido publicados "+
						"por la EMPRESA DE PRIMER ORDEN correspondiente en las CADENAS PRODUCTIVAS, de los que sea beneficiario el SUJETO DE APOYO.\n\n"+ 
						"c)	NAFIN d� a conocer al INTERMEDIARIO FINANCIERO y/o al DESCONTATARIO a trav�s de CADENAS PRODUCTIVAS los DOCUMENTOS y montos que se encuentren publicados "+
						"en dicho sistema de CADENAS PRODUCTIVAS.\n\n"+
						"Lo anterior para el efecto de que el INTERMEDIARIO FINANCIERO o DESCONTANTE se encuentre en facultad de operar en FACTORAJE O DESCUENTO ELECTR�NICO "+
						"AUTOM�TICO, en CADENAS PRODUCTIVAS, los DOCUMENTOS a favor del SUJETO DE APOYO que otorga la presente instrucci�n y, que los recursos que deba recibir el SUJETO DE APOYO como contraprestaci�n por las operaciones de FACTORAJE O DESCUENTO ELECTR�NICO "+
						"AUTOM�TICO los deposite el DESCONTANTE en la CUENTA BANCARIA, manifestando en este acto, que dichos recursos podr�n ser aplicados por el INTERMEDIARIO "+
						"FINANCIERO al pago del CR�DITO y sus accesorios.\n\n"+
						"Esta instrucci�n irrevocable no podr� ser extinguida o modificada por el SUJETO DE APOYO mientras exista cualquier saldo pendiente de pago al INTERMEDIARIO "+
						"FINANCIERO, y solo podr� ser revocada con la autorizaci�n expresa del mismo.\n\n"+
						"Asimismo, como SUJETO DE APOYO manifiesto mi conformidad para que NAFIN se reserve el derecho de no proveer al amparo de esta instrucci�n el servicio de "+
						"Cadenas Productivas en caso de incumplimiento del SUJETO DE APOYO con el INTERMEDIARIO FINANCIERO.\n\n"+ 
						"En este acto como SUJETO DE APOYO me obligo a no modificar la forma de pago y a no solicitar mi baja de CADENAS PRODUCTIVAS, mientras exista cualquier saldo "+
						"pendiente de pago al INTERMEDIARIO FINANCIERO, sin la autorizaci�n expresa de �ste.\n\n"+
						"Me obligo a no ceder los derechos de cobro de los DOCUMENTOS publicados en CADENAS PRODUCTIVAS por la EMPRESA DE PRIMER ORDEN seleccionada al otorgar esta instrucci�n, a persona "+
						"alguna distinta del INTERMEDIARIO FINANCIERO o el DESCONTANTE durante la vigencia del CR�DITO.\n\n"+
						"Al transmitir el mensaje de datos que contiene esta instrucci�n irrevocable, manifiesto mi conformidad con la misma, para todos los efectos legales "+
						"correspondientes, liberando a NAFIN de cualquier responsabilidad por su aceptaci�n y cumplimiento.\n\n"
					,"formas");
					pdfDoc.addTable();
					
					pdfDoc.addText("INSTRUCCI�N IRREVOCABLE - INTERMEDIARIO FINANCIERO Y/O DESCONTANTE","formasB",ComunesPDF.CENTER);
					pdfDoc.setTable(1,100);//13-04-2010 REBOS
					pdfDoc.setCell(	
						"Que en este acto otorga el SUJETO DE APOYO al INTERMEDIARIO FINANCIERO o DESCONTANTE, derivado del CR�DITO que le otorg� el INTERMEDIARIO FINANCIERO, a "+
						"efecto de que:\n\n"+
						"a)	El INTERMEDIARIO FINANCIERO o el DESCONTANTE opere en FACTORAJE O DESCUENTO ELECTR�NICO AUTOM�TICO, en CADENAS PRODUCTIVAS todos y cada uno de los "+
						"DOCUMENTOS publicados por la EMPRESA DE PRIMER ORDEN.\n\n"+
						"b)	El INTERMEDIARIO FINANCIERO o el DESCONTANTE deposite los recursos que deba recibir el SUJETO DE APOYO como contraprestaci�n por el FACTORAJE O DESCUENTO "+
						"ELECTR�NICO AUTOM�TICO en la CUENTA BANCARIA, en el entendido de que dichos recursos podr�n ser aplicados por el INTERMEDIARIO FINANCIERO al pago del CR�DITO "+
						"y sus accesorios.\n\n"+
						"Lo anterior no exime al SUJETO DE APOYO de continuar pagando el CR�DITO, m�s sus accesorios, en las fechas convenidas con el INTERMEDIARIO FINANCIERO.\n\n"+
						"c)	El INTERMEDIARIO FINANCIERO proporcione a NAFIN y al DESCONTANTE, en su caso,  las caracter�sticas del CR�DITO que me fue otorgado como SUJETO DE APOYO "+
						"(cuenta, monto, fecha de vencimiento, banco para dep�sito etc.).\n\n"+
						"Esta instrucci�n irrevocable no podr� ser extinguida o modificada por el SUJETO DE APOYO mientras exista cualquier saldo pendiente de pago al INTERMEDIARIO "+
						"FINANCIERO, y solo podr� ser revocada con la autorizaci�n expresa del mismo.\n\n"+
						"En este acto como SUJETO DE APOYO me obligo a no modificar la forma de pago y a no solicitar mi baja de CADENAS PRODUCTIVAS, mientras exista cualquier saldo "+
						"pendiente de pago al INTERMEDIARIO FINANCIERO, sin la autorizaci�n expresa de �ste.\n\n"+
						"Me obligo como SUJETO DE APOYO a no ceder los derechos de cobro de los DOCUMENTOS publicados por la EMPRESA DE PRIMER ORDEN seleccionada al otorgar esta "+
						"instrucci�n, a persona alguna distinta del INTERMEDIARIO FINANCIERO o el DESCONTANTE durante la vigencia del CR�DITO.\n\n"+
						"Al transmitir el mensaje de datos que contiene esta instrucci�n irrevocable, manifiesto mi conformidad con la misma, para todos los efectos legales "+
						"correspondientes, liberando al INTERMEDIARIO FINANCIERO y/o al DESCONTANTE de cualquier responsabilidad por su aceptaci�n y cumplimiento.\n\n"
					,"formas");	    
					pdfDoc.addTable();
				}
			  pdfDoc.endDocument();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
			}
		}
		return nombreArchivo;
	}
	
	public void setRegistrosAux(Registros regAux){this.regAux = regAux;}
	public void setNombreEpo(String nombreEpo){this.nombreEpo = nombreEpo;}
	public void setNombreIf(String nombreIf){this.nombreIf = nombreIf;}
	public void setNombreDescontante(String nombreDescontante){this.nombreDescontante = nombreDescontante;}
	public void setFolioInstruccion(String folioInstruccionIrrevocable){this.folioInstruccionIrrevocable = folioInstruccionIrrevocable;}
	public void setFechaAct(String fechaAct){this.fechaAct = fechaAct;}
	public void setHoraAct(String horaAct){this.horaAct = horaAct;}
	public void setUsuarioCarga(String usuarioCarga){this.usuarioCarga = usuarioCarga;}

}