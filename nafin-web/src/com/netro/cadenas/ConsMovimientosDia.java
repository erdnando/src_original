package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsMovimientosDia implements IQueryGeneratorRegExtJS  {
	
	private final static Log log = ServiceLocator.getInstance().getLog(ConsMovimientosDia.class);
	List conditions;
	private String idEPO;
	private String fecha;
	private String tipoConsulta;
	
	StringBuffer qrySentencia = new StringBuffer("");
		
	public ConsMovimientosDia() { }
	
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
				
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile(){
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		if(tipoConsulta.equals("1")){
			qrySentencia.append("SELECT CG_BANCO_RETIRO, "+
				"CG_CTA_RETIRO "+
				"FROM COMCAT_EPO "+
				"WHERE IC_EPO = ? ");
			conditions.add(idEPO);
		}else{
			qrySentencia.append("SELECT PY.IC_PYME, "+ //1
					"EP.IC_EPO, "+//2
					"DOC.CS_DSCTO_ESPECIAL, "+//3
					"PY.CG_RAZON_SOCIAL RAZON_PYME, "+//4
					"PI.IC_IF, "+//5
					"MO.CD_NOMBRE, "+//6
					"CB.CG_NUMERO_CUENTA NC_PYME, "+//7
					"CB.CG_BANCO BANCO_PYME, "+//8
					"CB.CG_SUCURSAL SUC_PYME, "+//9
					"NVL(PL.cd_descripcion,'SIN PLAZA')||', '||PL.CG_ESTADO PLAZA_PYME, "+//10
					"SUM(DOC.FN_MONTO_DSCTO - DS.IN_IMPORTE_INTERES) IMPORTE_PYME, "+//11
					"'SIN' RAZON_BEN, "+//12
					"'SIN' NC_BEN, "+//13
					"'SIN' BANCO_BEN, "+//14
					"'SIN' SUC_BEN, "+//15
					"'SIN' PLAZA_BEN, "+//16
					"0 IMPORTE_BEN, "+//17
					"TRUNC(SOL.DF_OPERACION), "+//18
					"MO.IC_MONEDA, "+//19
					"CB.IC_PLAZA, "+//20
					"UPPER(EP.CG_NOMBRE_COMERCIAL), "+//21
					"EP.CG_BANCO_RETIRO, "+//22
					"EP.CG_CTA_RETIRO, "+//23
					"SB.IC_CLAVE IC_CLAVE,  "+//24
					"0 ClaveBancoBen,  "+//25
					"0 CVE_PLAZA_BEN "+//26
				"FROM COMCAT_MONEDA MO, "+
					"COMCAT_PLAZA PL, "+
					"COMCAT_PYME PY, "+
					"COMCAT_EPO EP, "+
					"COMREL_PYME_IF PI, "+
					"COMREL_CUENTA_BANCARIA CB, "+
					"COM_DOCUMENTO DOC, "+
					"COM_DOCTO_SELECCIONADO DS, "+
					"COMCAT_IF CIF, "+
					"COM_SOLICITUD SOL, "+
					"COMCAT_STAN_BANCOS sb "+
				"WHERE  SOL.IC_DOCUMENTO = DS.IC_DOCUMENTO "+
				"AND DS.IC_DOCUMENTO = DOC.IC_DOCUMENTO "+
				"AND CB.IC_PYME = DOC.IC_PYME "+
				"AND CB.IC_CUENTA_BANCARIA = PI.IC_CUENTA_BANCARIA "+
				"AND CB.IC_MONEDA = DOC.IC_MONEDA "+
				"AND DS.IC_IF = PI.IC_IF "+
				"AND PI.IC_EPO = DS.IC_EPO "+
				"AND DOC.IC_EPO = EP.IC_EPO "+
				"AND DOC.IC_PYME = PY.IC_PYME "+
				"AND CB.IC_MONEDA = MO.IC_MONEDA "+
				"AND DOC.IC_MONEDA = MO.IC_MONEDA "+
				"AND CB.IC_PLAZA = PL.IC_PLAZA(+) "+
				"AND CB.cg_banco = sb.cg_financieras "+
				"AND DS.IC_IF = CIF.IC_IF "+
				"AND CIF.IG_TIPO_PISO = 1 "+
				"AND PI.CS_BORRADO = 'N' "+
				"AND PI.CS_VOBO_IF = 'S' "+
				"AND CB.CS_BORRADO = 'N' "+
				"AND DOC.CS_DSCTO_ESPECIAL != 'D' "+
				"AND DOC.IC_ESTATUS_DOCTO IN (4,11,12,16) "+
				"AND TRUNC(SOL.DF_OPERACION) = NVL( TO_DATE(?,'DD/MM/YYYY') ,TRUNC(SYSDATE)) "+
				"AND EP.IC_EPO = ? "+
				"GROUP BY PY.IC_PYME, "+
					"EP.IC_EPO, "+
					"DOC.CS_DSCTO_ESPECIAL, "+
					"PY.CG_RAZON_SOCIAL, "+
					"PI.IC_IF, "+
					"MO.CD_NOMBRE, "+
					"CB.CG_NUMERO_CUENTA, "+
					"CB.CG_BANCO, "+
					"CB.CG_SUCURSAL, "+
					"NVL(PL.CD_DESCRIPCION,'SIN PLAZA')||', '||PL.CG_ESTADO, "+
					"TRUNC(SOL.DF_OPERACION), "+
					"MO.IC_MONEDA, "+
					"CB.IC_PLAZA, "+
					"UPPER(EP.CG_NOMBRE_COMERCIAL), "+
					"EP.CG_BANCO_RETIRO, "+
					"EP.CG_CTA_RETIRO, "+
					"SB.IC_CLAVE "+
				"UNION ALL "+
				"SELECT PY.IC_PYME, "+
					"EP.IC_EPO, "+
					"DOC.CS_DSCTO_ESPECIAL, "+
					"PY.CG_RAZON_SOCIAL RAZON_PYME, "+
					"PI.IC_IF, "+
					"MO.CD_NOMBRE, "+
					"CB.CG_NUMERO_CUENTA NC_PYME, "+
					"CB.CG_BANCO BANCO_PYME, "+
					"CB.CG_SUCURSAL SUC_PYME, "+
					"NVL(PL.CD_DESCRIPCION,'SIN PLAZA')||', '||PL.CG_ESTADO PLAZA_PYME, "+
					"SUM(ROUND((DOC.FN_MONTO_DSCTO - DS.IN_IMPORTE_INTERES)*((100-DOC.FN_PORC_BENEFICIARIO)/100),2)) IMPORTE_PYME, "+
					"CIF.CG_RAZON_SOCIAL RAZON_BEN, "+
					"IE.CG_NUM_CUENTA NC_BEN, "+
					"IE.CG_BANCO BANCO_BEN, "+
					"IE.IG_SUCURSAL SUC_BEN, "+
					"NVL(PLB.CD_DESCRIPCION,'SIN PLAZA')||', '||PLB.CG_ESTADO PLAZA_BEN, "+
					"SUM(ROUND((DOC.FN_MONTO_DSCTO - DS.IN_IMPORTE_INTERES)*(DOC.FN_PORC_BENEFICIARIO/100),2)) IMPORTE_BEN, "+
					"TRUNC(SOL.DF_OPERACION), "+
					"MO.IC_MONEDA, "+
					"CB.IC_PLAZA, "+
					"UPPER(EP.CG_NOMBRE_COMERCIAL), "+
					"EP.CG_BANCO_RETIRO, "+
					"EP.CG_CTA_RETIRO, "+
					"SB.IC_CLAVE IC_CLAVE, " +
					"sb1.ic_clave ClaveBancoBen, " +
					"NVL(PLB.IC_PLAZA,0) AS CVE_PLAZA_BEN "+
				"FROM COMCAT_MONEDA MO, "+
					"COMCAT_PLAZA PL, "+
					"COMCAT_PYME PY, "+
					"COMCAT_IF CIF, "+
					"COMCAT_PLAZA PLB, "+
					"COMCAT_EPO EP, "+
					"COMREL_PYME_IF PI, "+
					"COMCAT_IF IF2, " +
					"COMREL_IF_EPO IE, "+
					"COMREL_CUENTA_BANCARIA CB, "+
					"COM_DOCUMENTO DOC, "+
					"COM_DOCTO_SELECCIONADO DS, "+
					"COM_SOLICITUD SOL, "+
					"COMCAT_STAN_BANCOS SB, "+
					"COMCAT_STAN_BANCOS SB1 "+
				"WHERE SOL.IC_DOCUMENTO = DS.IC_DOCUMENTO "+
				"AND DS.IC_DOCUMENTO = DOC.IC_DOCUMENTO "+
				"AND CB.IC_PYME = DOC.IC_PYME "+
				"AND CB.IC_CUENTA_BANCARIA = PI.IC_CUENTA_BANCARIA "+
				"AND CB.IC_MONEDA = DOC.IC_MONEDA "+
				"AND DS.IC_IF = PI.IC_IF "+
				"AND PI.IC_EPO = DS.IC_EPO "+
				"AND DOC.IC_EPO = EP.IC_EPO "+
				"AND DOC.IC_PYME = PY.IC_PYME "+
				"AND CB.IC_MONEDA = MO.IC_MONEDA "+
				"AND DOC.IC_MONEDA = MO.IC_MONEDA "+
				"AND CB.IC_PLAZA = PL.IC_PLAZA(+) "+
				"AND IE.IC_PLAZA =PLB.IC_PLAZA(+) "+
				"AND IE.IC_IF = DOC.IC_BENEFICIARIO "+
				"AND CB.cg_banco = sb.cg_financieras "+
				"AND ie.cg_banco = sb1.cg_financieras " +
				"AND CIF.IC_IF = IE.IC_IF "+
				"AND IE.IC_EPO = EP.IC_EPO "+
				"AND PI.IC_IF = IF2.IC_IF "+
				"AND IF2.IG_TIPO_PISO = 1 "+
				"AND PI.CS_BORRADO = 'N' "+
				"AND PI.CS_VOBO_IF = 'S' "+
				"AND CB.CS_BORRADO = 'N' "+
				"AND DOC.CS_DSCTO_ESPECIAL = 'D' "+
				"AND DOC.IC_ESTATUS_DOCTO IN (4,11,12) "+
				"AND TRUNC(SOL.DF_OPERACION) = NVL( TO_DATE( ?,'DD/MM/YYYY') ,TRUNC(SYSDATE)) "+
				"AND EP.IC_EPO = ? "+
				"GROUP BY  PY.IC_PYME, "+
					"EP.IC_EPO, "+
					"DOC.CS_DSCTO_ESPECIAL, "+
					"PY.CG_RAZON_SOCIAL, "+
					"PI.IC_IF, "+
					"MO.CD_NOMBRE, "+
					"CB.CG_NUMERO_CUENTA, "+
					"CB.CG_BANCO, "+
					"CB.CG_SUCURSAL, "+
					"NVL(PL.CD_DESCRIPCION,'SIN PLAZA')||', '||PL.CG_ESTADO, "+
					"CIF.CG_RAZON_SOCIAL, "+
					"IE.CG_NUM_CUENTA, "+
					"IE.CG_BANCO, "+
					"IE.IG_SUCURSAL, "+
					"NVL(PLB.CD_DESCRIPCION,'SIN PLAZA')||', '||PLB.CG_ESTADO, "+
					"TRUNC(SOL.DF_OPERACION), "+
					"MO.IC_MONEDA, "+
					"CB.IC_PLAZA, "+
					"UPPER(EP.CG_NOMBRE_COMERCIAL), "+
					"EP.CG_BANCO_RETIRO, "+
					"EP.CG_CTA_RETIRO, "+
					"SB.IC_CLAVE," +
					"sb1.ic_clave,"+
					"NVL(PLB.IC_PLAZA,0)");
	
			conditions.add(fecha);				
			conditions.add(idEPO);
			conditions.add(fecha);				
			conditions.add(idEPO);
		}
		log.debug("qrySentencia  "+qrySentencia);
		log.debug("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		log.debug("crearPageCustomFile (E)");	
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		log.debug("crearPageCustomFile (S)");	
		return nombreArchivo;
					
	}
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");		
		String nombreArchivo = "";
		String SEP = ",";
		String SEPSTAN = ";";
		
		HttpSession session = request.getSession();	
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contArchivoCVS = new StringBuffer();
		StringBuffer contArchivoSTAN = new StringBuffer();
		ComunesPDF pdfDoc = new ComunesPDF();
		
		int numRegistros = 0;
		int numRegistrosMN =0; 
		int numRegistrosDLS=0;
		double total_desarrolladorMN = 0.0;
		double total_beneficiarioMN = 0.0;
		double total_desarrolladorDLS = 0.0;
		double total_beneficiarioDLS = 0.0;
			
		int totalDoc= 0;
			
		try {		
			if(tipo.equals("PDF") ) {				
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
					
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+
				" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);			
				
				pdfDoc.setTable(13,100);	
				
			}else if(tipo.equals("CVS")){
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".cvs";	
			}else if(tipo.equals("STAN")){
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".txt";	
			}
			while(rs.next()){			
				if(rs.getInt("IC_MONEDA") == 1){
					numRegistrosMN++;
					total_desarrolladorMN += rs.getDouble("IMPORTE_PYME");
					total_beneficiarioMN += rs.getDouble("IMPORTE_BEN");
				}else{
					numRegistrosDLS++;
					total_desarrolladorDLS += rs.getDouble("IMPORTE_PYME");
					total_beneficiarioDLS += rs.getDouble("IMPORTE_BEN");
				}
				
				String nombre = (rs.getString("RAZON_PYME") == null) ? "" : rs.getString("RAZON_PYME");
				String moneda = (rs.getString("CD_NOMBRE") == null) ? "" : rs.getString("CD_NOMBRE");
				String noCuenta = (rs.getString("NC_PYME") == null) ? "" : rs.getString("NC_PYME");
				String deposito = (rs.getString("BANCO_PYME") == null) ? "" : rs.getString("BANCO_PYME");
				String sucursal = (rs.getString("SUC_PYME") == null) ? "" : rs.getString("SUC_PYME");
				String plaza = (rs.getString("PLAZA_PYME") == null) ? "" : rs.getString("PLAZA_PYME");
				String importe = (rs.getString("IMPORTE_PYME") == null) ? "" : rs.getString("IMPORTE_PYME");
				String nombreBen = ((rs.getString("RAZON_BEN") == null) || 
								(rs.getString("RAZON_BEN").equals("SIN"))) ? "" : rs.getString("RAZON_BEN");
				String noCuentaBen = ((rs.getString("NC_BEN") == null) || 
								(rs.getString("NC_BEN").equals("SIN"))) ? "" : rs.getString("NC_BEN");
				String depositoBen = ((rs.getString("BANCO_BEN") == null) || 
								(rs.getString("BANCO_BEN").equals("SIN"))) ? "" : rs.getString("BANCO_BEN");
				String sucursalBen = ((rs.getString("SUC_BEN") == null) || 
								(rs.getString("SUC_BEN").equals("SIN"))) ? "" : rs.getString("SUC_BEN");
				String plazaBen = ((rs.getString("PLAZA_BEN") == null) || 
								(rs.getString("PLAZA_BEN").equals("SIN"))) ? "" : rs.getString("PLAZA_BEN");
				String importeBen = (rs.getString("IMPORTE_BEN") == null) ? "" : rs.getString("IMPORTE_BEN");
				String clavePlaza = rs.getString("IC_PLAZA")== null ? "" : rs.getString("IC_PLAZA");
				String clave = rs.getString("IC_CLAVE")==null?"":rs.getString("IC_CLAVE");
				String claveBancoBen = rs.getString("CLAVEBANCOBEN")==null?"":rs.getString("CLAVEBANCOBEN");
				String clavePlazaBen = rs.getString("CVE_PLAZA_BEN")==null?"":rs.getString("CVE_PLAZA_BEN");
				
				BigDecimal bdImpPyme = new BigDecimal(importe);
				if(tipo.equals("PDF")){
					if(numRegistros == 0){
						pdfDoc.setCell("Movimiento para el d�a: "+getFecha(),"celda01",ComunesPDF.CENTER,13);
						pdfDoc.setCell("Otorgamiento: "+rs.getString(21),"celda01",ComunesPDF.CENTER,13);
						pdfDoc.setCell("Banco de Retiro: " + rs.getString("CG_BANCO_RETIRO") + 
							"\t Cuenta de Retiro: " + rs.getString("CG_CTA_RETIRO")
							,"celda01",ComunesPDF.LEFT,13);
						pdfDoc.setCell("Desarrollador","celda01",ComunesPDF.CENTER,7);
						pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER,6);
						pdfDoc.setCell("Nombre","celda01",ComunesPDF.LEFT);
						pdfDoc.setCell("Moneda","celda01",ComunesPDF.LEFT);
						pdfDoc.setCell("No. de Cuenta","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Deposito en","celda01",ComunesPDF.LEFT);
						pdfDoc.setCell("Sucursal","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Plaza","celda01",ComunesPDF.LEFT);
						pdfDoc.setCell("Importe","celda01",ComunesPDF.RIGHT);
						pdfDoc.setCell("Nombre Beneficiario","celda01",ComunesPDF.LEFT);
						pdfDoc.setCell("Cta. Beneficiario","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Deposito en","celda01",ComunesPDF.LEFT);
						pdfDoc.setCell("Sucursal Beneficiario","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("Plaza Beneficiario","celda01",ComunesPDF.LEFT);
						pdfDoc.setCell("Importe Beneficiario","celda01",ComunesPDF.RIGHT);
					}
					pdfDoc.setCell(nombre,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(moneda,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(noCuenta,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(deposito,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(sucursal,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(plaza,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(importe, 2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(nombreBen,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(noCuentaBen,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(depositoBen,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(sucursalBen,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(plazaBen,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(importeBen, 2),"formas",ComunesPDF.RIGHT);
				}else if(tipo.equals("CVS")){
					if(numRegistros == 0){
						contArchivoCVS.append("Movimiento para el d�a: " + getFecha() + "\n" +
								"Banco de Retiro: " + rs.getString("CG_BANCO_RETIRO") + "    " + 
								"Cuenta de Retiro: " + rs.getString("CG_CTA_RETIRO") + "\n" +
								"Nombre" + SEP + "Moneda" + SEP + "No. de Cuenta" + SEP + "Deposito en" +
								SEP + "Sucursal" + SEP + "Plaza" + SEP + "Importe" + SEP + 
								"Nombre del Beneficiario" + SEP + "Cta. Beneficiario" + SEP + 
								"Deposito en" + SEP + "Sucursal Beneficiario" + SEP + 
								"Plaza Beneficiario" + SEP + "Importe Beneficiario\n");
					}
					contArchivoCVS.append(nombre.replaceAll(",","") + SEP +
											moneda.replaceAll(",","") + SEP +
											noCuenta.replaceAll(",","") + SEP +
											deposito.replaceAll(",","") + SEP +
											sucursal.replaceAll(",","") + SEP +
											plaza.replaceAll(",","") + SEP +
											Comunes.formatoDecimal(importe, 2).replaceAll(",","") + SEP +
											nombreBen.replaceAll(",","") + SEP +
											noCuentaBen.replaceAll(",","") + SEP +
											depositoBen.replaceAll(",","") + SEP +
											sucursalBen.replaceAll(",","") + SEP +
											plazaBen.replaceAll(",","") + SEP +
											Comunes.formatoDecimal(importeBen, 2).replaceAll(",","") +"\n");
				}else if(tipo.equals("STAN")){
					if(bdImpPyme.doubleValue() != 0){
						contArchivoSTAN.append("320000" + SEPSTAN + "2" + SEPSTAN + 
								"E" + SEPSTAN + "123" + SEPSTAN + "MXN" + SEPSTAN);
						contArchivoSTAN.append(bdImpPyme.toPlainString() + SEPSTAN);
						contArchivoSTAN.append(getFecha() + SEPSTAN + "SPE" + SEPSTAN +
								"ICN NAFIN DESC" + SEPSTAN);
						contArchivoSTAN.append(rs.getString(21).replace(';',' ') +
								SEPSTAN + "" + SEPSTAN + nombre.replace(';',' ') + 
								SEPSTAN + clavePlaza + SEPSTAN + "CHQ" + SEPSTAN + 
								clave.replace(';',' ') + SEPSTAN + 
								noCuenta.replace(';',' ') + SEPSTAN + "" + SEPSTAN);
						contArchivoSTAN.append("" + SEPSTAN + "1001" + SEPSTAN + 
								"CHQ" + SEPSTAN + "135" + SEPSTAN + "2647324927" + SEPSTAN +
								"407482" + SEPSTAN + "BC" + SEPSTAN + "" + SEPSTAN);
						contArchivoSTAN.append("AUT" + SEPSTAN + "" + SEPSTAN + "" +
								SEPSTAN + "ARCHIVO\n");
					}
					if ("D".equals(rs.getString("CS_DSCTO_ESPECIAL"))){
						BigDecimal bdImpBen = new BigDecimal(importeBen);
						if(bdImpBen.doubleValue() != 0){
							contArchivoSTAN.append("320000" + SEPSTAN + "2" + SEPSTAN +
									"E" + SEPSTAN + "123" + SEPSTAN + "MXN" + SEPSTAN);
							contArchivoSTAN.append(bdImpBen.toPlainString() + SEPSTAN);
							contArchivoSTAN.append(getFecha() + SEPSTAN + "SPE" + SEPSTAN +
									"ICN NAFIN DESC" + SEPSTAN);
							contArchivoSTAN.append(rs.getString(21).replace(';',' ') +
									SEPSTAN + "" + SEPSTAN + nombreBen.replace(';',' ') + 
									SEPSTAN + clavePlazaBen + SEPSTAN + "CHQ" + SEPSTAN + 
									claveBancoBen.replace(';',' ') + SEPSTAN + 
									noCuentaBen.replace(';',' ') + SEPSTAN + "" + SEPSTAN);
							contArchivoSTAN.append("" + SEPSTAN + "1001" + SEPSTAN + 
									"CHQ" + SEPSTAN + "135" + SEPSTAN + "2647324927" + SEPSTAN +
									"407482" + SEPSTAN + "BC" + SEPSTAN + "" + SEPSTAN);
							contArchivoSTAN.append("AUT" + SEPSTAN + "" + SEPSTAN + "" +
									SEPSTAN + "ARCHIVO\n");
						}
					}
				}
				numRegistros++;
			}
			if(tipo.equals("PDF")){
				pdfDoc.setCell("Total MN","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(""+numRegistrosMN,"celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(total_desarrolladorMN, 2),"celda01",ComunesPDF.RIGHT);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(total_beneficiarioMN, 2),"celda01",ComunesPDF.RIGHT);
				
				pdfDoc.setCell("Total DLS","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(""+numRegistrosDLS,"celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(total_desarrolladorDLS, 2),"celda01",ComunesPDF.RIGHT);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("$" + Comunes.formatoDecimal(total_beneficiarioDLS, 2),"celda01",ComunesPDF.RIGHT);
				
				pdfDoc.addTable();
				pdfDoc.endDocument();
			}else	if(tipo.equals("CVS")){
				if(numRegistros >= 1){
					contArchivoCVS.append("\n\nTotal MN" + SEP + numRegistrosMN +
					SEP + "" + SEP + "" + SEP + "" + SEP + "" + SEP +
					Comunes.formatoDecimal(total_desarrolladorMN, 2).replaceAll(",","") + 
					SEP + "" + SEP + "" + SEP + "" + SEP + "" + 	SEP + "" + SEP + 
					Comunes.formatoDecimal(total_beneficiarioMN, 2).replaceAll(",","") + "\n" + 
					"Total DLS" + SEP + numRegistrosDLS +
					SEP + "" + SEP + "" + SEP + "" + SEP + "" + SEP +
					Comunes.formatoDecimal(total_desarrolladorDLS, 2).replaceAll(",","") + 
					SEP + "" + SEP + "" + SEP + "" + SEP + "" + 	SEP + "" + SEP + 
					Comunes.formatoDecimal(total_beneficiarioDLS, 2).replaceAll(",","") + "\n");
									
					if(!creaArchivo.make(contArchivoCVS.toString(),path, ".csv")){
						nombreArchivo="ERROR";
					}else{
						nombreArchivo = creaArchivo.nombre;
					}
				}
			}else	if(tipo.equals("STAN")){
				if(!creaArchivo.make(contArchivoSTAN.toString(),path, ".txt")){
					nombreArchivo="ERROR";
				}else{
					nombreArchivo = creaArchivo.nombre;
				}
			}
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
	
	public List getConditions() {  return conditions;  }

	public void setIdEPO(String idEPO) {
		this.idEPO = idEPO;
	}


	public String getIdEPO() {
		return idEPO;
	}


	public void setFecha(String fecha) {
		this.fecha = fecha;
	}


	public String getFecha() {
		return fecha;
	}


	public void setTipoConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}


	public String getTipoConsulta() {
		return tipoConsulta;
	}
	
}