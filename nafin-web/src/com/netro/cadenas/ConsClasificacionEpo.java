package com.netro.cadenas;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsClasificacionEpo implements IQueryGeneratorRegExtJS {
	public ConsClasificacionEpo() { }
	
	private List conditions;
	StringBuffer strQuery;
	String claveEpo;
	String numProveedor;
	String nomProveedor;
	String tipoClasificacion;
	
	private static final Log log = ServiceLocator.getInstance().getLog(ConsClasificacionEpo.class);//Variable para enviar mensajes al log.  
	
	public String getAggregateCalculationQuery() {
	 return "";
	}
	
	public String getDocumentQuery() {
	
	  return "";
	}
	public String getDocumentSummaryQueryForIds(List pageIds) {
	 return "";
	}
	
	public String getDocumentQueryFile(){
		conditions	= new ArrayList();
		strQuery		= new StringBuffer();
		if (numProveedor != null && !numProveedor.equals("")) {
			strQuery.append("SELECT a.cg_pyme_epo_interno,   decode (a.cs_habilitado,'N','<font color=\"Blue\">*</font>','S',' ')||' '||b.cg_razon_social AS razon_social,a.ic_calificacion,"+
							"c.cd_descripcion, a.ic_pyme, a.cs_habilitado, '' AS flag_check " +
							" FROM comrel_pyme_epo a, comcat_pyme b, comrel_calif_epo c" +
							" WHERE a.ic_epo= ? "+
							" AND b.ic_pyme=a.ic_pyme" +
							" AND a.ic_calificacion=c.ic_calificacion" +
							" AND a.cg_pyme_epo_interno= ? " +
							" AND a.ic_epo=c.ic_epo");
			conditions.add(new Integer(claveEpo));
			conditions.add(numProveedor);
		}else if (nomProveedor != null && !nomProveedor.equals("")) {
			strQuery.append("SELECT a.cg_pyme_epo_interno, decode (a.cs_habilitado,'N','<font color=\"Blue\">*</font>','S',' ')||' '||  b.cg_razon_social AS razon_social,a.ic_calificacion," +
								"c.cd_descripcion, a.ic_pyme, a.cs_habilitado, '' AS flag_check " +
								" FROM comrel_pyme_epo a, comcat_pyme b, comrel_calif_epo c");
			if (tipoClasificacion != null && !tipoClasificacion.equals("")){
				strQuery.append(" WHERE a.ic_epo= ? "+
									" AND b.ic_pyme=a.ic_pyme" + 
									" AND a.ic_calificacion= ? " +
									" AND (UPPER(b.cg_razon_social) LIKE UPPER(?)" +
									" OR UPPER(b.cg_nombre_comercial) LIKE UPPER(?))" +
									" AND a.ic_calificacion=c.ic_calificacion" +
									" AND a.ic_epo=c.ic_epo"+
									" ORDER BY 2");
				conditions.add(new Integer(claveEpo));
				conditions.add(tipoClasificacion);
				conditions.add(nomProveedor+"%");
				conditions.add(nomProveedor+"%");
			}else{
				strQuery.append(" WHERE a.ic_epo= ? "+
									" AND b.ic_pyme=a.ic_pyme " +
									" AND (upper(b.cg_razon_social) LIKE upper(?)" +
									" OR upper(b.cg_nombre_comercial) LIKE upper(?))" +
									" AND a.ic_calificacion=c.ic_calificacion" +
									" AND a.ic_epo=c.ic_epo"+
									" ORDER BY 2");
				conditions.add(new Integer(claveEpo));
				conditions.add(nomProveedor+'%');
				conditions.add(nomProveedor+'%');
			}
		}else if (tipoClasificacion != null && !tipoClasificacion.equals("")){
			strQuery.append("SELECT a.cg_pyme_epo_interno, decode (a.cs_habilitado,'N','<font color=\"Blue\">*</font>','S',' ')||' '||b.cg_razon_social AS razon_social, a.ic_calificacion," +
								" c.cd_descripcion, a.ic_pyme, a.cs_habilitado, '' AS flag_check" +
								" FROM comrel_pyme_epo a, comcat_pyme b, comrel_calif_epo c"+
								" WHERE a.ic_epo= ? "+
								" AND b.ic_pyme=a.ic_pyme" +
								" AND a.ic_calificacion= ?"+
								" AND a.ic_calificacion=c.ic_calificacion" +
								" AND a.ic_epo=c.ic_epo"+
								" ORDER BY 2");
			conditions.add(new Integer(claveEpo));
			conditions.add(tipoClasificacion);
		}
		
		log.debug("getDocumentQueryField)"+strQuery.toString());
		log.debug("getDocumentQueryField)"+conditions);
		return strQuery.toString();
	}
	/**
	 * En este método se debe realizar la implementación de la generación de archivo
	 * con base en el resultset que se recibe como parámetro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generará el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		
		if ("CSV".equals(tipo)) {
			try {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
				buffer = new BufferedWriter(writer);

				int numRegistros = 0;
				String numProv = "";
				String nomProv = "";
				String cveCalif = "";
				String descCalif = "";
				while (rs.next()) {
					numRegistros++;
					numProv		= (rs.getString("CG_PYME_EPO_INTERNO")==null)?"":rs.getString("CG_PYME_EPO_INTERNO");
					nomProv		= (rs.getString("RAZON_SOCIAL")==null)?"":rs.getString("RAZON_SOCIAL");
					cveCalif		= (rs.getString("IC_CALIFICACION")==null)?"":rs.getString("IC_CALIFICACION");
					descCalif		= (rs.getString("CD_DESCRIPCION")==null)?"":rs.getString("CD_DESCRIPCION");

					if (numRegistros == 1){
						linea = "NUMERO_PROVEEDOR,NOMBRE_PROVEEDOR,CVE_CALIFICACION,DESCRIPCION \n";
						buffer.write(linea);
					}
					linea = numProv.replace(',',' ') +"," + nomProv.replace(',',' ') + "," + cveCalif + "," + descCalif.replace(',',' ') + "\n";
					buffer.write(linea);
				}
				buffer.close();
		  }catch (Throwable e) {
			 throw new AppException("Error al generar el archivo", e);
		  } finally {
			 try {
				rs.close();
			 } catch(Exception e) {}
		  }
		  
		}else if ("PDF".equals(tipo)) {
		}
		return nombreArchivo;
	}
	/**
	 * En este método se debe realizar la implementación de la generación de archivo
	 * con base en el objeto Registros que recibe como parámetro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generará el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	
	public List getConditions() {
		return conditions;
	}
	
	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}
	
	public String getClaveEpo() {
		return claveEpo;
	}
	
	public void setNumProveedor(String numProv) {
		this.numProveedor = numProv;
	}
	
	public void setNomProveedor(String nomProv) {
		this.nomProveedor = nomProv;
	}
	
	public void setTipoClasificacion(String tipoClasificacion) {
		this.tipoClasificacion = tipoClasificacion;
	}

}