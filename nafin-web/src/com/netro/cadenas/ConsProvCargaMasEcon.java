package com.netro.cadenas;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsProvCargaMasEcon implements IQueryGeneratorRegExtJS {

	public ConsProvCargaMasEcon() {  }

	private List conditions;
	StringBuffer query;

	// PARAMETROS PARA CONSULTAS 
	private String numResumen = "";
	private String txtCadProductiva = "";
	private String valProcesado = "";
	private String fecIni = "";
	private String fecFin = "";
	
	
	private static final Log log = ServiceLocator.getInstance().getLog(ConsProvCargaMasEcon.class);//Variable para enviar mensajes al log.

	public String getAggregateCalculationQuery() {
		return "";
 	}
		

	public String getDocumentQuery(){
		conditions = new ArrayList();	
		query 		= new StringBuffer();
			
		query.append(
				"SELECT EAR.IC_RESUMEN " +
				"FROM ECON_AFILIA_PYME_MASIVA EAP, ECON_AFILIA_RESUMEN_CARGA EAR, ECON_CAT_EPOS CE  " +
				"WHERE EAP.IC_NUMERO_PROCESO = EAR.IC_NUMERO_PROCESO  " +
				"AND EAP.IC_EPO = CE.ECON_CAT_EPOS_PK "
				);
		
		
		if(numResumen!=null && !numResumen.equals("")){
			query.append(" AND EAR.IC_RESUMEN = ? ");
			conditions.add(new Integer(numResumen));
		}
		if( txtCadProductiva!=null && !txtCadProductiva.equals("")){
			query.append(" AND EAP.IC_EPO = ? ");
			conditions.add(new Integer(txtCadProductiva));
		}
		if(valProcesado!=null && !valProcesado.equals("A")){
			query.append(" AND EAR.CS_PROCESADO_X_ECON = ? ");
			conditions.add(valProcesado);
		}
		if(fecIni!=null && !fecIni.equals("")){
			query.append(" AND EAR.DF_CARGA >= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");
			conditions.add(fecIni);
		}
		if(fecFin!=null && !fecFin.equals("")){
			query.append(" AND EAR.DF_CARGA < TRUNC(TO_DATE(?,'DD/MM/YYYY')+1) ");
			conditions.add(fecFin);
		}
		
		query.append(" GROUP BY EAR.IC_RESUMEN ");
		query.append(" ORDER BY EAR.IC_RESUMEN ");
		
		log.debug("getDocumentQuery)"+query.toString());
		log.debug("getDocumentQuery)"+conditions);
		return query.toString();
 	}
	
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
		query 		= new StringBuffer();
		
		log.debug("pageIds)"+pageIds);
			
		query.append("SELECT EAR.IC_RESUMEN IC_RESUMEN, EAP.IC_EPO IC_EPO, CE.VRAZON_SOCIAL VRAZON_SOCIAL, EAR.CG_NOMBRE_ARCHIVO CG_NOMBRE_ARCHIVO, EAR.IG_REG_VALIDOS IG_REG_VALIDOS,   ");
		query.append("       EAR.IG_REG_ERROR IG_REG_ERROR, EAR.IG_SUSCEPTIBLES IG_SUSCEPTIBLES, EAR.IG_DUPLICADOS IG_DUPLICADOS, EAR.IG_AFILIADOS IG_AFILIADOS, EAR.IG_REAFILIA_NUEVAS IG_REAFILIA_NUEVAS,  ");
		query.append("        EAR.IG_REAFI_AUT IG_REAFI_AUT, EAR.IG_X_AFILIAR IG_X_AFILIAR, EAR.IG_ECON_CARGADOS IG_ECON_CARGADOS, EAR.IG_ECON_X_CARGAR IG_ECON_X_CARGAR,  ");
		query.append("        EAR.CS_PROCESADO_X_ECON CS_PROCESADO_X_ECON, TO_CHAR(EAR.DF_CARGA, 'DD/MM/YYYY HH24:MI:SS') DF_CARGA, EAR.CG_TIPO_CARGA CG_TIPO_CARGA,  ");
		query.append("        EAR.CG_CARGA_ECON CG_CARGA_ECON, EAR.CG_CRUCE_DB CG_CRUCE_DB, EAR.IC_PRODUCTO IC_PRODUCTO, EAR.CS_DIRECTO_PROMO CS_DIRECTO_PROMO, EAR.CS_VENTA_CRUZADA CS_VENTA_CRUZADA, EAR.IG_CONV_UNICO IG_CONV_UNICO,  ");
		query.append("        EAR.IG_REG_TOTALES IG_REG_TOTALES, EAR.IG_REG_SINERROR_NE IG_REG_SINERROR_NE, EAR.IG_REG_CONERROR_NE IG_REG_CONERROR_NE, EAR.IG_REG_PROV_YA_EXISTENTES_NE IG_REG_PROV_YA_EXISTENTES_NE, ");
		query.append("        SUM(DECODE(EAP.ECON_AFI_REAFI,'A',1,0)) C_AFIL, SUM(DECODE(EAP.ECON_AFI_REAFI,'R',1,0)) C_REAFIL  ");
		query.append("FROM ECON_AFILIA_PYME_MASIVA EAP, ECON_AFILIA_RESUMEN_CARGA EAR, ECON_CAT_EPOS CE  ");
		query.append("WHERE EAP.IC_NUMERO_PROCESO = EAR.IC_NUMERO_PROCESO  ");
		query.append("AND EAP.IC_EPO = CE.ECON_CAT_EPOS_PK ");
		


		/*
		query.append( " AND ff.ic_fianza = ? " );
		conditions.add(claveIf );
		*/
						
		for(int i=0;i<pageIds.size();i++) {
			List lItem = (List)pageIds.get(i);
			if(i==0) {
				query.append(" AND EAR.IC_RESUMEN  IN ( ");
			}
			query.append("?");
			conditions.add(new Long(lItem.get(0).toString()));					
			if(i!=(pageIds.size()-1)) {
				query.append(",");
			} else {
				query.append(" ) ");
			}			
		}
		
		query.append("GROUP BY EAR.IC_RESUMEN, EAP.IC_EPO, CE.VRAZON_SOCIAL, EAR.CG_NOMBRE_ARCHIVO, EAR.IG_REG_VALIDOS,   ");
		query.append("       EAR.IG_REG_ERROR, EAR.IG_SUSCEPTIBLES, EAR.IG_DUPLICADOS, EAR.IG_AFILIADOS, EAR.IG_REAFILIA_NUEVAS,  ");
		query.append("        EAR.IG_REAFI_AUT, EAR.IG_X_AFILIAR, EAR.IG_ECON_CARGADOS, EAR.IG_ECON_X_CARGAR,  ");
		query.append("        EAR.CS_PROCESADO_X_ECON, TO_CHAR(EAR.DF_CARGA, 'DD/MM/YYYY HH24:MI:SS'), EAR.CG_TIPO_CARGA,  ");
		query.append("        EAR.CG_CARGA_ECON, EAR.CG_CRUCE_DB, EAR.IC_PRODUCTO, EAR.CS_DIRECTO_PROMO, EAR.CS_VENTA_CRUZADA, EAR.IG_CONV_UNICO,  ");
		query.append("        EAR.IG_REG_TOTALES, EAR.IG_REG_SINERROR_NE, EAR.IG_REG_CONERROR_NE, EAR.IG_REG_PROV_YA_EXISTENTES_NE ");
		query.append("ORDER BY EAR.IC_RESUMEN ");
	
	
		log.debug("getDocumentSummaryQueryForIds "+query.toString());
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
		
						
		return query.toString();
 	}
		
			
	public String getDocumentQueryFile(){
		conditions = new ArrayList();
		query = new StringBuffer();
		
		query.append("SELECT EAR.IC_RESUMEN IC_RESUMEN, EAP.IC_EPO IC_EPO, CE.VRAZON_SOCIAL VRAZON_SOCIAL, EAR.CG_NOMBRE_ARCHIVO CG_NOMBRE_ARCHIVO, EAR.IG_REG_VALIDOS IG_REG_VALIDOS,   ");
		query.append("       EAR.IG_REG_ERROR IG_REG_ERROR, EAR.IG_SUSCEPTIBLES IG_SUSCEPTIBLES, EAR.IG_DUPLICADOS IG_DUPLICADOS, EAR.IG_AFILIADOS IG_AFILIADOS, EAR.IG_REAFILIA_NUEVAS IG_REAFILIA_NUEVAS,  ");
		query.append("        EAR.IG_REAFI_AUT IG_REAFI_AUT, EAR.IG_X_AFILIAR IG_X_AFILIAR, EAR.IG_ECON_CARGADOS IG_ECON_CARGADOS, EAR.IG_ECON_X_CARGAR IG_ECON_X_CARGAR,  ");
		query.append("        EAR.CS_PROCESADO_X_ECON CS_PROCESADO_X_ECON, TO_CHAR(EAR.DF_CARGA, 'DD/MM/YYYY HH24:MI:SS') DF_CARGA, EAR.CG_TIPO_CARGA CG_TIPO_CARGA,  ");
		query.append("        EAR.CG_CARGA_ECON CG_CARGA_ECON, EAR.CG_CRUCE_DB CG_CRUCE_DB, EAR.IC_PRODUCTO IC_PRODUCTO, EAR.CS_DIRECTO_PROMO CS_DIRECTO_PROMO, EAR.CS_VENTA_CRUZADA CS_VENTA_CRUZADA, EAR.IG_CONV_UNICO IG_CONV_UNICO,  ");
		query.append("        EAR.IG_REG_TOTALES IG_REG_TOTALES, EAR.IG_REG_SINERROR_NE IG_REG_SINERROR_NE, EAR.IG_REG_CONERROR_NE IG_REG_CONERROR_NE, EAR.IG_REG_PROV_YA_EXISTENTES_NE IG_REG_PROV_YA_EXISTENTES_NE, ");
		query.append("        SUM(DECODE(EAP.ECON_AFI_REAFI,'A',1,0)) C_AFIL, SUM(DECODE(EAP.ECON_AFI_REAFI,'R',1,0)) C_REAFIL  ");
		query.append("FROM ECON_AFILIA_PYME_MASIVA EAP, ECON_AFILIA_RESUMEN_CARGA EAR, ECON_CAT_EPOS CE  ");
		query.append("WHERE EAP.IC_NUMERO_PROCESO = EAR.IC_NUMERO_PROCESO  ");
		query.append("AND EAP.IC_EPO = CE.ECON_CAT_EPOS_PK ");
		
		if(numResumen!=null && !numResumen.equals("")){
			query.append(" AND EAR.IC_RESUMEN = ? ");
			conditions.add(new Integer(numResumen));
		}
		if( txtCadProductiva!=null && !txtCadProductiva.equals("")){
			query.append(" AND EAP.IC_EPO = ? ");
			conditions.add(new Integer(txtCadProductiva));
		}
		if(valProcesado!=null && !valProcesado.equals("A")){
			query.append(" AND EAR.CS_PROCESADO_X_ECON = ? ");
			conditions.add(valProcesado);
		}
		if(fecIni!=null && !fecIni.equals("")){
			query.append(" AND EAR.DF_CARGA >= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");
			conditions.add(fecIni);
		}
		if(fecFin!=null && !fecFin.equals("")){
			query.append(" AND EAR.DF_CARGA < TRUNC(TO_DATE(?,'DD/MM/YYYY')+1) ");
			conditions.add(fecFin);
		}
		
		query.append("GROUP BY EAR.IC_RESUMEN, EAP.IC_EPO, CE.VRAZON_SOCIAL, EAR.CG_NOMBRE_ARCHIVO, EAR.IG_REG_VALIDOS,   ");
		query.append("       EAR.IG_REG_ERROR, EAR.IG_SUSCEPTIBLES, EAR.IG_DUPLICADOS, EAR.IG_AFILIADOS, EAR.IG_REAFILIA_NUEVAS,  ");
		query.append("        EAR.IG_REAFI_AUT, EAR.IG_X_AFILIAR, EAR.IG_ECON_CARGADOS, EAR.IG_ECON_X_CARGAR,  ");
		query.append("        EAR.CS_PROCESADO_X_ECON, TO_CHAR(EAR.DF_CARGA, 'DD/MM/YYYY HH24:MI:SS'), EAR.CG_TIPO_CARGA,  ");
		query.append("        EAR.CG_CARGA_ECON, EAR.CG_CRUCE_DB, EAR.IC_PRODUCTO, EAR.CS_DIRECTO_PROMO, EAR.CS_VENTA_CRUZADA, EAR.IG_CONV_UNICO,  ");
		query.append("        EAR.IG_REG_TOTALES, EAR.IG_REG_SINERROR_NE, EAR.IG_REG_CONERROR_NE, EAR.IG_REG_PROV_YA_EXISTENTES_NE ");
		query.append("ORDER BY EAR.IC_RESUMEN ");
		
		log.debug("getDocumentQueryFile "+query.toString());
		log.debug("getDocumentQueryFile)"+conditions);
	
		return query.toString();
 	} 		
	
	/**
	 * En este método se debe realizar la implementación de la generación de archivo
	 * con base en el resultset que se recibe como parámetro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generará el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		
		/*****valores de despliegue*****/
		String c_cargaId				= "";
		String c_nombreArchivo	= "";
		int c_reg_proc					= 0;
		int c_reg_no_proc				= 0;
		int c_reg_dupli				= 0;
		int c_reg_susc					= 0;
		int c_reg_afilia				= 0;
		int c_reg_reafilia			= 0;
		int c_reg_reafi_aut			= 0;
		int c_reg_x_afilia			= 0;
		int c_reg_en_econ				= 0;
		int c_reg_x_econ				= 0;
		int c_afil						= 0;
		int c_reafil					= 0;
		int c_reg_conv_unico			= 0;
		//Fodea 020
		int c_reg_total_ne  	 = 0; 
		int c_reg_sinerror_ne = 0; 
		int c_reg_conerror_ne = 0; 
		int c_reg_prov_ya_ne  = 0; 
		
		String c_fecCargaArch		= "";
		String c_procesado			= "";
		String c_nomEpo					= "";
		String c_tipoCarga			= "";
		
		String c_carga_econ		= "";
		String c_cruce_db			= "";
		String c_ic_producto		= "";
		String c_directo_prom	= "";
		String c_venta_cruzada	= "";
		String datosDeCarga		= "";
		String datosDeCargaFile	= "";

		if ("CSV".equals(tipo)) {
			//linea = "Afianzadora,No. Fianza,No. Contrato,Monto,Ramo,Sub Ramo,Tipo Fianza,Fecha de Emisión,Vigencia,Fiado,Destinatario,Beneficiario,Estatus,Observaciones\n";
			linea = "Numero de Proceso,EPO,Registros Totales,Registros sin Error ,Registros con Error ,Ya Existentes,Registros Totales,Duplicados,Unicos,Ya Afiliados,Reafiliaciones,Reafiliaciones Auto.,";
			linea += "Con Contrato Unico,Por Afiliar,Ya Cargados,Por Cargar,Nombre de Archivo,Fecha de Carga,Datos de Carga,Procesado\n";
			try {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
				buffer = new BufferedWriter(writer);
				buffer.write(linea);

				while (rs.next()) {
					c_carga_econ		= "";
					c_cruce_db			= "";
					c_ic_producto		= "";
					c_directo_prom	= "";
					c_venta_cruzada	= "";
					datosDeCarga		= "";
					datosDeCargaFile	= "";
					
					
					c_cargaId	= rs.getString("IC_RESUMEN");
					c_nombreArchivo	= rs.getString("CG_NOMBRE_ARCHIVO");
					c_reg_proc 			= rs.getInt("IG_REG_VALIDOS");
					c_reg_no_proc		= rs.getInt("IG_REG_ERROR");
		
					c_reg_dupli			= rs.getInt("IG_DUPLICADOS");
					c_reg_susc			= rs.getInt("IG_SUSCEPTIBLES");
					c_reg_afilia		= rs.getInt("IG_AFILIADOS");
					c_reg_reafilia	= rs.getInt("IG_REAFILIA_NUEVAS");
					c_reg_reafi_aut	= rs.getInt("IG_REAFI_AUT");
					c_reg_x_afilia	= rs.getInt("IG_X_AFILIAR");
					c_reg_en_econ		= rs.getInt("IG_ECON_CARGADOS");
					c_reg_x_econ		= rs.getInt("IG_ECON_X_CARGAR");
					c_reg_conv_unico	= rs.getInt("IG_CONV_UNICO"); 
		
					//Fodea 020 Detalle Carga N@E
					c_reg_total_ne		= rs.getInt("IG_REG_TOTALES");
															 
					c_reg_sinerror_ne	= rs.getInt("IG_REG_SINERROR_NE");
					c_reg_conerror_ne	= rs.getInt("IG_REG_CONERROR_NE");
					c_reg_prov_ya_ne	= rs.getInt("IG_REG_PROV_YA_EXISTENTES_NE");
		
					c_fecCargaArch	= rs.getString("DF_CARGA");
					c_procesado			= rs.getString("CS_PROCESADO_X_ECON");
					c_nomEpo				= rs.getString("VRAZON_SOCIAL");
					c_tipoCarga			= rs.getString("CG_TIPO_CARGA");
		
					c_carga_econ		= rs.getString("CG_CARGA_ECON")!=null?rs.getString("CG_CARGA_ECON"):"";
					c_cruce_db			= rs.getString("CG_CRUCE_DB")!=null?rs.getString("CG_CRUCE_DB"):"";
					c_ic_producto		= rs.getString("IC_PRODUCTO")!=null?rs.getString("IC_PRODUCTO"):"";
					c_directo_prom	= rs.getString("CS_DIRECTO_PROMO")!=null?rs.getString("CS_DIRECTO_PROMO"):"";
					c_venta_cruzada	= rs.getString("CS_VENTA_CRUZADA")!=null?rs.getString("CS_VENTA_CRUZADA"):"";
					
					if(!c_carga_econ.equals("")){
						datosDeCargaFile += "*Carga";
					}
					if(!c_cruce_db.equals("")){
						datosDeCargaFile += !datosDeCargaFile.equals("")?" y Cruce":"*Cruce";
					}
					if(c_ic_producto!=null && c_ic_producto.equals("1")){
						datosDeCargaFile +="- Factoraje";
					}
					if(c_ic_producto!=null && c_ic_producto.equals("11")){
						datosDeCargaFile +="- Reafiliacion IF";
					}
					if(!c_directo_prom.equals("")){
						datosDeCargaFile +="- Directo a Promotoria";
					}
					if(!c_venta_cruzada.equals("")){
						datosDeCargaFile +="- Venta Cruzada";
					}
				

					linea = c_cargaId+","+c_nomEpo.replace(',',' ')+","+String.valueOf(c_reg_total_ne)+","+String.valueOf(c_reg_sinerror_ne)+","+String.valueOf(c_reg_conerror_ne)+",";
					linea += String.valueOf(c_reg_prov_ya_ne)+","+String.valueOf(c_reg_proc+c_reg_dupli)+","+String.valueOf(c_reg_dupli)+","+String.valueOf(c_reg_proc)+","+String.valueOf(c_reg_afilia)+","+String.valueOf(c_reg_reafilia)+",";
					linea += String.valueOf(c_reg_reafi_aut)+","+String.valueOf(c_reg_conv_unico)+","+String.valueOf(c_reg_x_afilia)+","+String.valueOf(c_reg_en_econ)+","+String.valueOf(c_reg_x_econ)+",";
					linea += c_nombreArchivo+","+c_fecCargaArch+","+datosDeCargaFile+","+c_procesado+"\n";
					
					buffer.write(linea);
				}
				buffer.close();
			} catch (Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		}

		return nombreArchivo;
	}
	
	/**
	 * En este método se debe realizar la implementación de la generación de archivo
	 * con base en el objeto Registros que recibe como parámetro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generará el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}

	
	public List getConditions() {
		return conditions;
	}


	public void setNumResumen(String numResumen) {
		this.numResumen = numResumen;
	}


	public String getNumResumen() {
		return numResumen;
	}


	public void setTxtCadProductiva(String txtCadProductiva) {
		this.txtCadProductiva = txtCadProductiva;
	}


	public String getTxtCadProductiva() {
		return txtCadProductiva;
	}


	public void setValProcesado(String valProcesado) {
		this.valProcesado = valProcesado;
	}


	public String getValProcesado() {
		return valProcesado;
	}


	public void setFecIni(String fecIni) {
		this.fecIni = fecIni;
	}


	public String getFecIni() {
		return fecIni;
	}


	public void setFecFin(String fecFin) {
		this.fecFin = fecFin;
	}


	public String getFecFin() {
		return fecFin;
	}



	

}