package com.netro.cadenas;

import com.netro.anticipos.Parametro;
import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ReporteEPOs implements IQueryGeneratorReg, IQueryGeneratorRegExtJS {

	public ReporteEPOs(){}
	
	//Variable para enviar mensajes al log.
	private static final Log log = ServiceLocator.getInstance().getLog(ReporteEPOs.class);
	
	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer 		qrySentencia;
	
	private List 		conditions;
	private String 	paginaOffset;
	private String 	paginaNo;	
	public  String 	paginar;
	
	private String 	intermediario;
//	private String 	noEpo;
	private String 	moneda;
 
	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */
	public String getAggregateCalculationQuery() {
		
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
 
		qrySentencia.append(
			"SELECT "  +
			"    COUNT(1) TOTAL_REGISTROS "  +
			"FROM "  +
			"   ( "  +
			"      SELECT "  +
			"         PROV_PUBLICACION.IC_EPO "  +
			"      FROM "  +
			"         VM_PROV_PUBLICACION  PROV_PUBLICACION, "  +
			"         COMCAT_MONEDA        MONEDA, "  +
			"         COMCAT_EPO           EPO, "  +
			"         COMREL_NAFIN         REL_NAFIN,  "  +
			"         COM_CONTACTO         CONTACTO "  +
			"      WHERE "  +
			"          PROV_PUBLICACION.IC_MONEDA   = MONEDA.IC_MONEDA "  +
			"      AND PROV_PUBLICACION.IC_EPO      = EPO.IC_EPO "  +
			"      AND PROV_PUBLICACION.IC_EPO      = REL_NAFIN.IC_EPO_PYME_IF "  +
			"      AND REL_NAFIN.CG_TIPO            = 'E' "  +
			"      AND PROV_PUBLICACION.IC_PYME     = CONTACTO.IC_PYME             "  +
			"      AND CONTACTO.CS_PRIMER_CONTACTO  = 'S' "
		);
 
		// Especificar Intermediario Financiero
		qrySentencia.append(" AND PROV_PUBLICACION.IC_IF = ? ");
		conditions.add(intermediario);
 
		// Especificar la Clave de la Moneda
		qrySentencia.append(" AND PROV_PUBLICACION.IC_MONEDA = ? ");
		conditions.add(moneda);
 
		qrySentencia.append(
			"      GROUP BY  "  +
			"         PROV_PUBLICACION.IC_EPO  "  +
			"   ) A "
		);
		
		log.debug("getAggregateCalculationQuery "+conditions.toString());
		log.debug("getAggregateCalculationQuery "+qrySentencia.toString());

		return qrySentencia.toString();

	}

/* M�todos �para nueva versi�n */
/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
*/	

	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
			log.info("crearCustomFile(E)");
			CreaArchivo 	archivo 				= new CreaArchivo();
			StringBuffer contenidoArchivo = new StringBuffer();
			String nombreArchivo = "";
			int i = 0;
			try{
				Parametro parametro = ServiceLocator.getInstance().lookup("ParametroEJB", Parametro.class);
				
				String nombreIF = parametro.getNombreIF(intermediario); 
				contenidoArchivo.append("INTERMEDIARIO FINANCIERO:,\"" +nombreIF.replaceAll("\"","\"\"")+"\"\n");
				
				//AGREGAR ENCABEZADO 		
				contenidoArchivo.append("Ranking, ");
				contenidoArchivo.append("No Nafin Electr�nico, ");
				contenidoArchivo.append("RFC, ");
				contenidoArchivo.append("EPO, ");
				contenidoArchivo.append("Moneda, ");
				contenidoArchivo.append("Publicaci�n Vigente\n");
				// AGREGAR REGISTROS CONSULTADOS 
				
				while(rs.next()){
					i++; 
					//LEER CAMPOS ESPECIALES 
					String epo 						= rs.getString("EPO");								
					String nombreMoneda			= rs.getString("MONEDA"); 							
					String publicacionVigente 	= rs.getString("PUBLICACION_VIGENTE");
					// REVISAR QUE LOS VALORES LEIDOS SEAN VALIDOS 
					epo 					= epo 					== null?""		:epo.trim();						
					nombreMoneda 		= nombreMoneda 		== null?""		:nombreMoneda.trim();					
					publicacionVigente = publicacionVigente == null?"0.00"	:publicacionVigente.trim();	
					// AGREGAR DATOS LEIDOS AL ARCHIVO
					contenidoArchivo.append(rs.getString("RANKING")											+",");
					contenidoArchivo.append(rs.getString("NO_NAFIN_ELECTRONICO")						+",");
					contenidoArchivo.append(rs.getString("RFC")												+",");
					contenidoArchivo.append("\""  + epo.replaceAll("\"","\"\"")							+"\",");
					contenidoArchivo.append("\""  + nombreMoneda.replaceAll("\"","\"\"")				+"\",");
					contenidoArchivo.append("\"$" + Comunes.formatoDecimal(publicacionVigente,2)	+"\",");
					contenidoArchivo.append("\n");
				}
			
				if(archivo.make(contenidoArchivo.toString(), path, ".csv")){
					nombreArchivo = archivo.getNombre();
				}
				
			}catch(Exception e){
				log.info("crearCustomFile(S)");	
				log.error("Error al generar Archivo CSV ",e);
			}
			log.info("crearCustomFile(S)");
			return nombreArchivo;
	}


/* M�tdodo Agregado para nueva versi�n */

/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		log.info("crearPageCustomFile(E)");
		String nombreArchivo="";
		if ("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
				Parametro parametro = ServiceLocator.getInstance().lookup("ParametroEJB", Parametro.class);
				
				String nombreIF = parametro.getNombreIF(intermediario); 
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico") == null ? "" : session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
					
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText("Intermediario Financiero: "+ nombreIF ,"formas",ComunesPDF.LEFT);

				pdfDoc.setTable(6,90);
				pdfDoc.setCell("Ranking","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No Nafin Electronico","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("RFC","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Publicaci�n Vigente","celda01",ComunesPDF.CENTER);				
							
				while (reg.next()) {
					String ranking 		= reg.getString("RANKING")==null?"":reg.getString("RANKING");
					String nafine 		= reg.getString("NO_NAFIN_ELECTRONICO")==null?"":reg.getString("NO_NAFIN_ELECTRONICO");
					String rfc 	= reg.getString("RFC")==null?"":reg.getString("RFC");
					String epo 		= reg.getString("EPO")==null?"":reg.getString("EPO");
					String moneda 		= reg.getString("MONEDA")==null?"":reg.getString("MONEDA");
					String vigente 	= reg.getString("PUBLICACION_VIGENTE")==null?"":reg.getString("PUBLICACION_VIGENTE");
				
					pdfDoc.setCell(ranking,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nafine,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(rfc,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(epo,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(moneda,"formas",ComunesPDF.LEFT);					
					pdfDoc.setCell("$"+Comunes.formatoDecimal(vigente,2),"formas",ComunesPDF.RIGHT);
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				log.info("crearPageCustomFile(S)");
			}
		}
		return nombreArchivo;
	}


	 /**
	  * Obtiene el query para obtener las llaves primarias de la consulta
	  * @return Cadena con la consulta de SQL, para obtener llaves primarias
	  */
	 public String getDocumentQuery(){
		
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
    
		qrySentencia.append(
			"SELECT "  +
			"   ROWNUM   AS RANKING, "  +
			"   A.IC_EPO AS IC_EPO   "  +
			"FROM "  +
			"   ( "  +
			"      SELECT "  +
			"         PROV_PUBLICACION.IC_EPO, "  +
			"         SUM(PROV_PUBLICACION.FN_MONTO_PUBLICACION) PUBLICACION_VIGENTE "  +
			"      FROM "  +
			"         VM_PROV_PUBLICACION  PROV_PUBLICACION, "  +
			"         COMCAT_MONEDA        MONEDA, "  +
			"         COMCAT_EPO           EPO, "  +
			"         COMREL_NAFIN         REL_NAFIN,  "  +
			"         COM_CONTACTO         CONTACTO "  +
			"      WHERE "  +
			"          PROV_PUBLICACION.IC_MONEDA   = MONEDA.IC_MONEDA "  +
			"      AND PROV_PUBLICACION.IC_EPO      = EPO.IC_EPO "  +
			"      AND PROV_PUBLICACION.IC_EPO      = REL_NAFIN.IC_EPO_PYME_IF "  +
			"      AND REL_NAFIN.CG_TIPO            = 'E' "  +
			"      AND PROV_PUBLICACION.IC_PYME     = CONTACTO.IC_PYME             "  +
			"      AND CONTACTO.CS_PRIMER_CONTACTO  = 'S' "
		);
 
		// Especificar Intermediario Financiero
		qrySentencia.append(" AND PROV_PUBLICACION.IC_IF = ? ");
		conditions.add(intermediario);
		
		// Especificar la Clave de la Moneda
		qrySentencia.append(" AND PROV_PUBLICACION.IC_MONEDA = ? ");
		conditions.add(moneda);
		
		qrySentencia.append(
			"      GROUP BY  "  +
			"         PROV_PUBLICACION.IC_EPO  "  +
			"      ORDER BY "  +
			"         PUBLICACION_VIGENTE DESC "  +
			"   ) A "
		);
		
		log.debug("getDocumentQuery "+conditions.toString());
		log.debug("getDocumentQuery "+qrySentencia.toString());

		return qrySentencia.toString();
		
	}

	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
  
		// Obtener Offset del Ranking
		StringBuffer rankings = new StringBuffer();
		rankings.append("  DECODE( A.IC_EPO ");
		for(int i = 0; i < pageIds.size(); i++){
			
			rankings.append(", ");
			
			List 		item 			= (ArrayList)pageIds.get(i);
			String 	rankingEpo 	= item.get(0).toString();
			String 	claveEpo 	= item.get(1).toString();
			
			rankings.append(claveEpo + ", " + rankingEpo );
			      
		}
		rankings.append(" ) AS RANKING, ");
		
		// Construir query de documentos
		qrySentencia.append(
			"SELECT "
		);
		qrySentencia.append(rankings);
		qrySentencia.append(	
			"   A.* "  +
			"FROM "  +
			"   ( "  +
			"      SELECT "  +
			"         PROV_PUBLICACION.IC_EPO, "  +
			"         REL_NAFIN.IC_NAFIN_ELECTRONICO AS NO_NAFIN_ELECTRONICO, "  +
			"         EPO.CG_RFC                     AS RFC, "  +
			"         EPO.CG_RAZON_SOCIAL            AS EPO, "  +
			"         MONEDA.CD_NOMBRE               AS MONEDA, "  +
			"         SUM(PROV_PUBLICACION.FN_MONTO_PUBLICACION) PUBLICACION_VIGENTE "  +
			"      FROM "  +
			"         VM_PROV_PUBLICACION  PROV_PUBLICACION, "  +
			"         COMCAT_MONEDA        MONEDA, "  +
			"         COMCAT_EPO           EPO, "  +
			"         COMREL_NAFIN         REL_NAFIN,  "  +
			"         COM_CONTACTO         CONTACTO "  +
			"      WHERE "  +
			"          PROV_PUBLICACION.IC_MONEDA   = MONEDA.IC_MONEDA "  +
			"      AND PROV_PUBLICACION.IC_EPO      = EPO.IC_EPO "  +
			"      AND PROV_PUBLICACION.IC_EPO      = REL_NAFIN.IC_EPO_PYME_IF "  +
			"      AND REL_NAFIN.CG_TIPO            = 'E' "  +
			"      AND PROV_PUBLICACION.IC_PYME     = CONTACTO.IC_PYME             "  +
			"      AND CONTACTO.CS_PRIMER_CONTACTO  = 'S' "
		); 
		
		// Especificar Intermediario Financiero
		qrySentencia.append(" AND PROV_PUBLICACION.IC_IF     = ? ");
		conditions.add(intermediario);
		
		// Especificar la Clave de la Moneda
		qrySentencia.append(" AND PROV_PUBLICACION.IC_MONEDA = ? ");
		conditions.add(moneda);
		
		// Especificar las EPOs
		qrySentencia.append("   AND ( ");
		for(int i = 0; i < pageIds.size(); i++){
			List lItem = (ArrayList)pageIds.get(i);
			if(i > 0){qrySentencia.append("  OR  ");}
			qrySentencia.append(" PROV_PUBLICACION.IC_EPO  = ?");
			conditions.add(new Long(lItem.get(1).toString()));      
		}
		qrySentencia.append(" ) ");
			
		qrySentencia.append(
			"      GROUP BY  "  +
			"         PROV_PUBLICACION.IC_EPO,  "  +
			"         REL_NAFIN.IC_NAFIN_ELECTRONICO,  "  +
			"         EPO.CG_RFC,  "  +
			"         EPO.CG_RAZON_SOCIAL,  "  +
			"         MONEDA.CD_NOMBRE "  +
			"      ORDER BY "  +
			"         PUBLICACION_VIGENTE DESC "  +
			"   ) A "
		);
             
		log.debug("getDocumentSummaryQueryForIds "+conditions.toString());
		log.debug(" getDocumentSummaryQueryForIds "+qrySentencia.toString());
 
		return qrySentencia.toString();
		
	}
	
	/**
	 * Genera el archivo 
	 * @return String con el query
	 */
	public String getDocumentQueryFile(){
		
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		qrySentencia.append(
			"SELECT "  +
			"   ROWNUM AS RANKING, "  +
			"   A.* "  +
			"FROM "  +
			"   ( "  +
			"      SELECT "  +
			"         PROV_PUBLICACION.IC_EPO, "  +
			"         REL_NAFIN.IC_NAFIN_ELECTRONICO AS NO_NAFIN_ELECTRONICO, "  +
			"         EPO.CG_RFC                     AS RFC, "  +
			"         EPO.CG_RAZON_SOCIAL            AS EPO, "  +
			"         MONEDA.CD_NOMBRE               AS MONEDA, "  +
			"         SUM(PROV_PUBLICACION.FN_MONTO_PUBLICACION) PUBLICACION_VIGENTE "  +
			"      FROM "  +
			"         VM_PROV_PUBLICACION  PROV_PUBLICACION, "  +
			"         COMCAT_MONEDA        MONEDA, "  +
			"         COMCAT_EPO           EPO, "  +
			"         COMREL_NAFIN         REL_NAFIN,  "  +
			"         COM_CONTACTO         CONTACTO "  +
			"      WHERE "  +
			"          PROV_PUBLICACION.IC_MONEDA   = MONEDA.IC_MONEDA "  +
			"      AND PROV_PUBLICACION.IC_EPO      = EPO.IC_EPO "  +
			"      AND PROV_PUBLICACION.IC_EPO      = REL_NAFIN.IC_EPO_PYME_IF "  +
			"      AND REL_NAFIN.CG_TIPO            = 'E' "  +
			"      AND PROV_PUBLICACION.IC_PYME     = CONTACTO.IC_PYME             "  +
			"      AND CONTACTO.CS_PRIMER_CONTACTO  = 'S' "
		);
 
		// Especificar la Clave del Intermediario
		qrySentencia.append(" AND PROV_PUBLICACION.IC_IF = ? ");
		conditions.add(intermediario);
 
		// Especificar la clave de la moneda
		qrySentencia.append(" AND PROV_PUBLICACION.IC_MONEDA = ? ");
		conditions.add(moneda);
 
		qrySentencia.append(
			"      GROUP BY  "  +
			"         PROV_PUBLICACION.IC_EPO,  "  +
			"         REL_NAFIN.IC_NAFIN_ELECTRONICO,  "  +
			"         EPO.CG_RFC,  "  +
			"         EPO.CG_RAZON_SOCIAL,  "  +
			"         MONEDA.CD_NOMBRE "  +
			"      ORDER BY "  +
			"         PUBLICACION_VIGENTE DESC "  +
			"   ) A "
		);
		
		log.debug("getDocumentQueryFile "+conditions.toString());
		log.debug("getDocumentQueryFile "+qrySentencia.toString());
		
		return qrySentencia.toString();
		
	}//getDocumentQueryFile
	

	/*****************************************************
		 GETTERS
	*******************************************************/
 
	/**
	  Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
	
   /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */ 
	public String getPaginaNo() { return paginaNo; 	}
	
   /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
 	
	 
	/*****************************************************
						 SETTERS
	*******************************************************/

	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
   /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}
 
	public String getPaginar() {
		return paginar;
	}

	public void setPaginar(String paginar) {
		this.paginar = paginar;
	}

	public String getIntermediario() {
		return intermediario;
	}

	public void setIntermediario(String intermediario) {
		this.intermediario = intermediario;
	}
	
	/*
	public String getNoEpo() {
		return noEpo;
	}

	public void setNoEpo(String noEpo) {
		this.noEpo = noEpo;
	}
	*/
	
	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
 
}