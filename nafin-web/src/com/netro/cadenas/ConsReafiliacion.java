package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsReafiliacion implements IQueryGeneratorRegExtJS {


//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsReafiliacion.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String fechaFin;
	private String fechaIni;
	private String ic_epo;
	private String ic_if;
	private String pantalla;
	 
	public ConsReafiliacion() { }
	   
	  
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	/**
	 * metodo que obtiene las llaves primaria 
	 * @return 
	 */
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
				
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	/**
	 * metodo que obtienes la paginación 
	 * @return 
	 * @param pageIds
	 */
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
								
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		if(pantalla.equals("Captura"))  {
		
			qrySentencia.append( " select  " +
										" e.ic_epo as IC_EPO ,   " +
										" e.cg_razon_social as NOMBRE_EPO ,  "+
										" re.cs_estatus_reafiliacion  as REAFILIACION,  "+										
										" to_char(re.df_activacion,'dd/mm/yyyy') as FECHA_ACTIVACION,  " +
										" re.cg_usuario as USUARIO,  " +
										" re.CS_REAFILIA_AUTO as PROVEEDORES "+
										" from comcat_epo e, com_reafiliacion_epo re "+
										" where e.ic_epo = ?  " +
										" and e.ic_epo = re.ic_epo(+)  " +
										" order by 2 ");	
				conditions.add(ic_epo);
			
		}else  if(pantalla.equals("Consulta"))  {
			
			qrySentencia.append( "  SELECT e.cg_razon_social as NOMBRE_EPO, " +
									  " p.cg_razon_social as NOMBRE_PYME,  "+
									  " i.cg_razon_social as NOMBRE_IF,  "+
									  " m.cd_nombre as MONEDA, "+
									  " cb.cg_numero_cuenta as NO_CUENTA, "+
									  " TO_CHAR(pi.df_reasigno_cta,'dd/mm/yyyy') as FECHA_REAFILIACION "+
									  " FROM comcat_epo e, "+  
									  " comcat_pyme p,  "+
									  " comrel_pyme_epo pe, "+
									  " comcat_if i,  "+
									  " comrel_pyme_if pi, "+
									  " comrel_cuenta_bancaria cb,  "+
									  " comcat_moneda m  "+
									  "  WHERE "+
									  " pi.ic_epo = e.ic_epo "+
									  " AND pi.ic_if = i.ic_if "+
									  " AND pi.ic_cuenta_bancaria = cb.ic_cuenta_bancaria "+
									  " AND pi.ic_epo = pe.ic_epo    "+
									  " AND cb.ic_pyme = pe.ic_pyme "+  
									  " AND cb.ic_pyme = p.ic_pyme "+
									  " AND m.ic_moneda = cb.ic_moneda  ");
									  
			if(!fechaIni.equals("")  &&  !fechaFin.equals("") ) {
				qrySentencia.append( " AND pi.df_reasigno_cta >= TO_DATE(? ,'DD/MM/YYYY') and pi.df_reasigno_cta < TO_DATE(?,'DD/MM/YYYY')+1   "); 
				conditions.add(fechaIni);
				conditions.add(fechaFin);
			}
			
			if(!ic_epo.equals("")){
				qrySentencia.append(" and pi.ic_epo = ? ");
				conditions.add(ic_epo);
			}
			
			if(!ic_if.equals("")){
				qrySentencia.append(" and pi.ic_if = ?") ;
				conditions.add(ic_if);
			}
		
			
			
		}
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		int i=0;
		try {
		
			
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		
		String nombreArchivo = "";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;	
		int total =0;
		try {
		
			if(tipo.equals("CSV"))  {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			  writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			  buffer = new BufferedWriter(writer);
			  contenidoArchivo.append("EPO, PYME, IF, Moneda, No. Cuenta, Fecha Reafiliación  \n");
				
			}
			
			if(tipo.equals("PDF"))  {
			
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				
				pdfDoc.addText("México, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
				pdfDoc.setTable(6, 80);
				pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("PYME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No. Cuenta","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Reafiliación","celda01",ComunesPDF.CENTER);
				
			}
	
			while (rs.next())	{	
				String nombreEPO = (rs.getString("NOMBRE_EPO") == null) ? "" : rs.getString("NOMBRE_EPO");
				String nombrePYME = (rs.getString("NOMBRE_PYME") == null) ? "" : rs.getString("NOMBRE_PYME");
				String nombreIF = (rs.getString("NOMBRE_IF") == null) ? "" : rs.getString("NOMBRE_IF");
				String moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
				String no_cuenta = (rs.getString("NO_CUENTA") == null) ? "" : rs.getString("NO_CUENTA");
				String fecha_reafiliacion = (rs.getString("FECHA_REAFILIACION") == null) ? "" : rs.getString("FECHA_REAFILIACION");
			
				if(tipo.equals("CSV"))  {
					contenidoArchivo.append(nombreEPO.replaceAll(",","")+",");
					contenidoArchivo.append(nombrePYME.replaceAll(",","")+",");
					contenidoArchivo.append(nombreIF.replaceAll(",","")+",");
					contenidoArchivo.append(moneda.replaceAll(",","")+",");
					contenidoArchivo.append(no_cuenta.replaceAll(",","")+",");
					contenidoArchivo.append(fecha_reafiliacion.replaceAll(",","")+"\n");
					
					total++;
					if(total==1000){					
						total=0;	
						buffer.write(contenidoArchivo.toString());				
						contenidoArchivo = new StringBuffer();//Limpio
					}					
				}
				
				if(tipo.equals("PDF"))  {
					pdfDoc.setCell(nombreEPO,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(nombrePYME,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(nombreIF,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(no_cuenta,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_reafiliacion,"formas",ComunesPDF.CENTER);				
				}
			}//while (rs.next())	
		
				
			if(tipo.equals("CSV") ) {
				buffer.write(contenidoArchivo.toString());
				buffer.close();	
				contenidoArchivo = new StringBuffer();//Limpio  
			}
			if(tipo.equals("PDF")) { 	
				pdfDoc.addTable();
				pdfDoc.endDocument();
			}
			
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
	
	
	/**
	 * Metodo para realizar la captura de las  Reafiliaciones Automaticas 
	 * @return 
	 * @param usuario
	 * @param fecha_activacion
	 * @param chkREAFILIACION
	 * @param claveEPO
	 * @param numRegistros
	 */
	public String capturaReafiliacion(int  numRegistros, String[] claveEPO, String[] chkREAFILIACION, String[] fecha_activacion , String usuario,  String[] proveedores){
		
	
		PreparedStatement ps				= null;
		ResultSet			rs				= null;
		AccesoDB con =new AccesoDB();
		StringBuffer qrySentencia = new StringBuffer();
		List varBind = new ArrayList();
		String mensaje=  "", reafiliacion ="";
		try{	
		
			con.conexionDB();
		
			for(int i=0;i<numRegistros;i++){
				
				qrySentencia = new StringBuffer();
				varBind = new ArrayList();				
				qrySentencia.append("delete from com_reafiliacion_epo where ic_epo = ? ");				
				varBind.add(claveEPO[i]);		
				
				System.out.println("qrySentencia  "+qrySentencia);
				System.out.println("lVarBind  "+varBind);
			
				ps = con.queryPrecompilado(qrySentencia.toString(), varBind);
				ps.executeUpdate();
				ps.close();
				
				qrySentencia = new StringBuffer();
				varBind = new ArrayList();	
				qrySentencia.append(" insert into com_reafiliacion_epo "+
				" (ic_epo,  cs_estatus_reafiliacion, df_activacion ,  cg_usuario , CS_REAFILIA_AUTO ) "+
				" values(? ,? ,to_date(?,'dd/mm/yyyy'), ?, ? ) ");
				varBind.add(claveEPO[i]);	
				varBind.add(chkREAFILIACION[i]);	
				varBind.add(fecha_activacion[i]);	
				varBind.add(usuario);	
				varBind.add(proveedores[i]);	
				
				System.out.println("qrySentencia  "+qrySentencia);
				System.out.println("lVarBind  "+varBind);
			
				ps = con.queryPrecompilado(qrySentencia.toString(), varBind);
				ps.executeUpdate();
				ps.close();
				
				if(chkREAFILIACION[i].equals("N")) {	
				
					qrySentencia = new StringBuffer();
					varBind = new ArrayList();	
					qrySentencia.append(" UPDATE  COMREL_IF_EPO "+
					" SET  CS_REAFILIA_AUTO = 'N' "+
					" WHERE IC_EPO  = ? ");
					
					varBind.add(claveEPO[i]);	
					System.out.println("qrySentencia  "+qrySentencia);
					System.out.println("lVarBind  "+varBind);
				
					ps = con.queryPrecompilado(qrySentencia.toString(), varBind);
					ps.executeUpdate();
					ps.close();
				}
				
			}				  
			
			mensaje= "Parametrización realizada con éxito";
			
	} catch(Exception e) {	
		e.printStackTrace();		
		con.terminaTransaccion(false);
	} finally {	
		con.terminaTransaccion(true);
      if(con.hayConexionAbierta()) con.cierraConexionDB();
	}
	return mensaje;
}
	
		
	/**
	 Obtiene la lista de parámetros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getFechaIni() {
		return fechaIni;
	}

	public void setFechaIni(String fechaIni) {
		this.fechaIni = fechaIni;
	}


	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getPantalla() {
		return pantalla;
	}

	public void setPantalla(String pantalla) {
		this.pantalla = pantalla;
	}
  
}
