package com.netro.cadenas;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class BloqueoPymeConsultaHistorico implements IQueryGeneratorRegExtJS  {
	public BloqueoPymeConsultaHistorico() {}
	
	private final static Log log = ServiceLocator.getInstance().getLog(BloqueoPymeConsulta.class);
	private List conditions;
	String num_pyme;
	String rfc_pyme;
	String nombre_pyme;
	String ic_producto_nafin;
	String noBancoFondeo="";
	private String ic_pyme;
	

	public String getDocumentQuery(){ return null; }

	public String getDocumentSummaryQueryForIds(List ids){ return null; }
	
	public String getAggregateCalculationQuery(){ return null; }	
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){ return null; }
	
	/**
	 * Realiza la consulta de los hist�ricos de bloqueo y desbloqueo de pymes
	 * @return Sentencia sql
	 */
	public String getDocumentQueryFile(){
		log.debug("ic_producto_nafin: " + ic_producto_nafin);
		log.debug("num_pyme: " + ic_pyme);
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();
		log.info("***** getDocumentQueryFile (E) *****");
		if (!"".equals(ic_producto_nafin)){
			condicion.append(" AND bpxp.ic_producto_nafin = ? ");
			conditions.add(ic_producto_nafin);	
		}
		if (!"".equals(ic_pyme)){
			condicion.append(" AND bpxp.ic_pyme = ? "); 
			conditions.add(ic_pyme);
		}
		try{
			qrySentencia.append(
				" SELECT pn.ic_nombre,"+
				" 	n.ic_nafin_electronico,"+
				" 	p.cg_razon_social,"+
				" 	p.cg_rfc,"+
				" 	c.cg_email,"+
				" 	TO_CHAR(bpxp.df_modificacion,'dd/mm/yyyy hh24:mi') as df_modificacion, "+
				" 	bpxp.cg_causa_bloqueo,"+
				" 	bpxp.ic_usuario, "+
				" 	bpxp.cs_bloqueo"+
				" FROM"+
				" 	comhis_bloqueo_pyme_x_producto bpxp,"+
				" 	comcat_producto_nafin pn,"+
				" 	com_contacto c, comrel_nafin n, comcat_pyme p"+
				" WHERE"+
				" 	bpxp.ic_pyme = p.ic_pyme"+
				" 	AND bpxp.ic_producto_nafin = pn.ic_producto_nafin"+
				" 	AND bpxp.ic_pyme = c.ic_pyme"+
				" 	AND bpxp.ic_pyme = n.ic_epo_pyme_if "+
				" 	AND c.cs_primer_contacto = 'S'"+
				" 	AND n.cg_tipo = 'P'"+
				condicion +
				" ORDER BY bpxp.df_modificacion DESC");	
					
		}catch(Exception e){
			log.warn("BloqueoPymeConsultaHistorico::getDocumentQueryFileException "+e);
		}
		log.debug("Sentencia(getDocumentQueryFile): " + qrySentencia.toString());
		log.debug("Condicion(getDocumentQueryFile): " + condicion.toString());
		log.info("***** getDocumentQueryFile(S) *****");
		return qrySentencia.toString();
	}	
	
	/**
	 * Genera el archivo PDF de los detalles hist�ricos de bloqueo y desbloqueo de pymes
	 * @return nombre del archivo
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){ 
	
		log.debug("********** crearCustomFile (E)**********");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		int total = 0;
		String producto = "";	
		String nafinElectronico =  "";				
		String pyme = "";
		String rfc = "";
		String email = "";
		String bloqueo = "";
		String fecha = "";
		String usuario = "";
		String causa = "";

		try {
			Comunes comunes = new Comunes();
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
		
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
			((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
			(String)session.getAttribute("sesExterno"),
			(String) session.getAttribute("strNombre"),
			(String) session.getAttribute("strNombreUsuario"),
			(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			pdfDoc.addText(" 		","formas",ComunesPDF.RIGHT);
			pdfDoc.addText("Bloqueo de Pymes ","formas",ComunesPDF.LEFT);
			
			pdfDoc.setTable(9,100);
			pdfDoc.setCell("Producto ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("N@E ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Pyme ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("RFC ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("e-mail ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Bloqueo ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha y Hora Bloq. / Desbloq. ","celda01",ComunesPDF.CENTER); 
			pdfDoc.setCell("Usuario ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Causa ","celda01",ComunesPDF.CENTER);
			
			while (rs.next())	{		
				producto = (rs.getString("ic_nombre") == null) ? "" : rs.getString("ic_nombre");	
				nafinElectronico = (rs.getString("ic_nafin_electronico") == null) ? "" : rs.getString("ic_nafin_electronico");
				pyme = (rs.getString("cg_razon_social") == null) ? "" : rs.getString("cg_razon_social");
				rfc = (rs.getString("cg_rfc") == null) ? "" : rs.getString("cg_rfc");
				email = (rs.getString("cg_email") == null) ? "" : rs.getString("cg_email");
				bloqueo = (rs.getString("cs_bloqueo") == null) ? "" : rs.getString("cs_bloqueo");
				fecha = (rs.getString("df_modificacion") == null) ? "" : rs.getString("df_modificacion");
				usuario = (rs.getString("ic_usuario") == null) ? "" : rs.getString("ic_usuario");
				causa = (rs.getString("cg_causa_bloqueo") == null) ? "" : rs.getString("cg_causa_bloqueo");
				
				if(bloqueo.equals("B")){
					bloqueo = "Bloqueado";
				} else if(bloqueo.equals("D")){
					bloqueo = "Desbloqueado";
				}
				
				pdfDoc.setCell(producto,"formas",ComunesPDF.CENTER);			
				pdfDoc.setCell(nafinElectronico,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(pyme,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(rfc,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(email,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(bloqueo,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fecha,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(usuario,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(causa,"formas",ComunesPDF.CENTER);
				
				}
				pdfDoc.addTable();				
				pdfDoc.endDocument();	
			
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo ", e);
			} finally {
				try {
					//rs.close();
				} catch(Exception e) {}
			}

		log.debug("******** crearCustomFile (S)");
		return nombreArchivo;	
	}
	
/************************************************************************
 *									GETTERS AND SETTERS									*
 ************************************************************************/	
	public void setNum_pyme(String num_pyme) {
		this.num_pyme = num_pyme;
	}

	public String getNum_pyme() {
		return num_pyme;
	}

	public void setRfc_pyme(String rfc_pyme) {
		this.rfc_pyme = rfc_pyme;
	}

	public String getRfc_pyme() {
		return rfc_pyme;
	}

	public void setNombre_pyme(String nombre_pyme) {
		this.nombre_pyme = nombre_pyme;
	}

	public String getNombre_pyme() {
		return nombre_pyme;
	}

	public void setIc_producto_nafin(String ic_producto_nafin) {
		this.ic_producto_nafin = ic_producto_nafin;
	}

	public String getIc_producto_nafin() {
		return ic_producto_nafin;
	}
	
	public List getConditions(){
			return conditions;
		}	

	public String getIc_pyme() {
		return ic_pyme;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}

}