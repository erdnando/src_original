package com.netro.model.catalogos;

import netropology.utilerias.AppException;
import netropology.utilerias.ServiceLocator;
import org.apache.commons.logging.Log;

/**
 * Este cat�logo se cre� para las pantallas:
 * Admin IF - Cesi�n de Derechos - Capturas - Datos Cedente - Captura
 * Admin IF - Cesi�n de Derechos - Capturas - Datos Cedente - Consulta
 * Del Fodea 23-2015.
 * Despliega las EPOS que tengan una solicitud en estatus 1 (Por solicitar), 
 * 2 (En proceso) o 3 (Solicitud Aceptada).
 */
public class CatalogoEPOCedente extends CatalogoSimple{

/*
 * SELECT DISTINCT C.IC_EPO, E.CG_RAZON_SOCIAL
 * FROM CDER_SOLICITUD C, COMCAT_EPO E
 * WHERE C.IC_EPO = E.IC_EPO
 * AND E.CS_CESION_DERECHOS = 'S'
 * AND C.IC_ESTATUS_SOLIC IN(1, 2, 3)
 * ORDER BY E.CG_RAZON_SOCIAL;
 */
	public CatalogoEPOCedente(){
		super.setTabla(" CDER_SOLICITUD C, COMCAT_EPO E ");
	}

	public void setClave(String clave){
		super.setCampoClave(clave);
	}

	public void setDescripcion(String descripcion){
		super.setCampoDescripcion(descripcion);
	}

	protected void setCondicionesAdicionales() {

		String condicionAdicional = "";
		condicionAdicional = " C.IC_EPO = E.IC_EPO "
		+ " AND E.CS_CESION_DERECHOS = ? "
		+ " AND C.IC_ESTATUS_SOLIC IN(1, 2, 3, 13, 24) ";
		super.addCondicionAdicional(condicionAdicional);
		//agregar a variables bind estos valores
		super.addVariablesBind(new String("S"));
	}

}