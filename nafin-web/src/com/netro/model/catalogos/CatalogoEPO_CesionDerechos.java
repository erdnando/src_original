package com.netro.model.catalogos;
import netropology.utilerias.AppException;
import netropology.utilerias.ServiceLocator;
import org.apache.commons.logging.Log;

public class CatalogoEPO_CesionDerechos extends CatalogoSimple{

	public CatalogoEPO_CesionDerechos() {
		super.setTabla("COMCAT_EPO");
	}

	public void setClave(String clave){
		super.setCampoClave(clave);
	}

	public void setDescripcion(String descripcion){
		super.setCampoDescripcion(descripcion);
	}

	protected void setCondicionesAdicionales() {

		String condicionAdicional = "";
		condicionAdicional = " CS_CESION_DERECHOS = ? AND CS_HABILITADO = ? ";
		super.addCondicionAdicional(condicionAdicional);
		//agregar a variables bind estos valores
		super.addVariablesBind(new String("S"));
		super.addVariablesBind(new String("S"));
	}

}