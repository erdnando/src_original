package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


/**
 * Catalogo de Pymes (comcat_pyme)
 *
 * Ejemplo de uso:
 * CatalogoPYME cat = new CatalogoPYME();
 * cat.setCampoClave("ic_pyme");
 * cat.setCampoDescripcion("cg_razon_social");
 *
 * cat.setCesionDerechos("S");
 * cat.setClaveEpo("1");  //o cat.setClavesEpo("1,10,23,256");
 * cat.setConDocumentosPublicados(false);
 * cat.setEstatusDocumentos("2");
 * cat.setConDocumentosCambioEstatus(true);
 * cat.setTipoCambioEstatus("2,23");
 * cat.setIfCuentaBancaria("1");
 * cat.setConNumeroProveedor(true);
 *
 * cat.setOrden("cg_razon_social");
 *
 *
 * @author Gilberto Aparicio
 */
public class CatalogoPYME extends CatalogoAbstract {


	//Variable para enviar mensajes al log.
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoPYME.class);

	private String cesionDerechos = "";
	private String clavesEpo = "";
	private boolean conDocumentosPublicados = false;
	private String estatusDocumentos = "";
	private boolean conDocumentosCambioEstatus = false;	//true para Pymes con cambios de estatus false de lo contrario
	private String tipoCambioEstatus = "";	//Pyme con documentos con cambio de estatus segun lo especificado
	private String ifCuentaBancaria = "";	//Pyme con cuenta bancaria con el IF especificado
	private String claveMonedaCuentaBancaria = "";
	private boolean conNumeroProveedor = true;	//true para pymes con numero de proveedor, false de lo contrario
	private boolean conPymeEpoInterno = false;
	
	StringBuffer tablas = new StringBuffer();

	public CatalogoPYME() {
		super();
		super.setPrefijoTablaPrincipal("cp."); //from comcat_pyme cp
	}

	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colección de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();

		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer condicionJoin = new StringBuffer();
		StringBuffer hint = new StringBuffer();
		hint.append(" /*+ USE_NL( ");
		if(this.clavesEpo != null && !this.clavesEpo.equals("")){
			tablas.append(" comrel_pyme_epo pe, comcat_pyme cp " );
			condicionJoin.append(" pe.ic_pyme = cp.ic_pyme ");
			hint.append(" pe cp ");
		} else if (this.cesionDerechos != null && this.cesionDerechos.equals("S")){
			tablas.append(" cder_solicitud cs, comcat_pyme cp " );
			condicionJoin.append("cs.ic_pyme = cp.ic_pyme ");
			hint.append(" cs cp ");
		}

		if (this.conDocumentosPublicados||this.conDocumentosCambioEstatus){
			tablas.append(", com_documento d");
			hint.append(" d ");
			if (this.conDocumentosCambioEstatus){
				tablas.append(", comhis_cambio_estatus ce  ");
				condicionJoin.append(
						" AND pe.ic_pyme = d.ic_pyme " +
						" AND pe.ic_epo = d.ic_epo" +
						" AND d.ic_documento = ce.ic_documento");
				hint.append(" ce ");
			} else if (this.conDocumentosPublicados) {
				condicionJoin.append(
						" AND PE.ic_pyme = d.ic_pyme "+
						" AND PE.ic_epo = d.ic_epo");
			}

		}

		if(!"".equals(this.ifCuentaBancaria)){
			tablas.append(", comrel_pyme_if pi, comrel_cuenta_bancaria cb");
			condicionJoin.append(
					" AND pi.ic_cuenta_bancaria = cb.ic_cuenta_bancaria"+
					" AND cb.ic_pyme = PE.ic_pyme"+
					" AND pi.ic_epo = PE.ic_epo");
			hint.append(" pi cb ");
		}

		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind

		hint.append(" ) */ ");
		StringBuffer qrySQL = new StringBuffer();

		qrySQL.append(
				" SELECT " + hint + " DISTINCT " + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
				);


		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para
		//IN y NOT IN... y además se manda a llamar la implementación especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();

		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}

		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}

		log.debug("qrySQL ::: "+qrySQL);
		
		log.debug("vars ::: "+super.getValoresBindCondicion());

System.out.println("*------qrySQL--------::"+qrySQL);
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}

		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}

		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		String condicionAdicional = "";

		condicionAdicional = 
				" cp.cs_habilitado = ? " +
				" and cp.cs_invalido != ? ";
		super.addCondicionAdicional(condicionAdicional);
		super.addVariablesBind("S");
		super.addVariablesBind("S");


		if(this.cesionDerechos != null && !this.cesionDerechos.equals("")){
			condicionAdicional = " cp.cs_cesion_derechos = ? ";

			super.addCondicionAdicional(condicionAdicional);
			//agregar a variables bind estos valores
			super.addVariablesBind(this.cesionDerechos);
		}
		
		if (this.conPymeEpoInterno){
			condicionAdicional = " pe.cg_pyme_epo_interno IS NOT NULL ";

			super.addCondicionAdicional(condicionAdicional);
		}

		if(this.clavesEpo != null && !this.clavesEpo.equals("")){
			List lClavesEpos = Comunes.explode(",", clavesEpo ,Integer.class);
			condicionAdicional =
					" pe.cs_habilitado = ? " +
					" AND pe.ic_epo IN (" + Comunes.repetirCadenaConSeparador("?",",",lClavesEpos.size()) + ")";

			super.addCondicionAdicional(condicionAdicional);
			//agregar a variables bind estos valores
			super.addVariablesBind("S");
			super.addVariablesBind(lClavesEpos);
			//log.debug("setCondicionesAdicionales(S)");
		}

		if(this.ifCuentaBancaria !=null && !this.ifCuentaBancaria.equals("")){
			List lClavesEpos = Comunes.explode(",", clavesEpo ,Integer.class);

			condicionAdicional =
					" pi.ic_epo IN (" + Comunes.repetirCadenaConSeparador("?",",",lClavesEpos.size()) + ") " +
					" AND pi.ic_if = ? " +
					" AND pi.cs_vobo_if = ? " +
					" AND pi.cs_borrado = ? " +
					" AND cb.cs_borrado = ? ";

			super.addCondicionAdicional(condicionAdicional);
			//agregar a variables bind estos valores
			super.addVariablesBind(lClavesEpos);
			super.addVariablesBind(new Integer(this.ifCuentaBancaria));
			super.addVariablesBind("S");
			super.addVariablesBind("N");
			super.addVariablesBind("N");

			if (this.claveMonedaCuentaBancaria!= null && !this.claveMonedaCuentaBancaria.equals("")) {
				condicionAdicional = " cb.ic_moneda = ? ";
				super.addCondicionAdicional(condicionAdicional);
				super.addVariablesBind(new Integer(this.claveMonedaCuentaBancaria));
			}
		}

		if (this.estatusDocumentos!=null && !this.estatusDocumentos.equals("")&&(this.conDocumentosPublicados||this.conDocumentosCambioEstatus)) {

			List lEstatusDocto = Comunes.explode(",", this.estatusDocumentos, Integer.class);

			condicionAdicional = " d.ic_estatus_docto in (" + Comunes.repetirCadenaConSeparador("?",",",lEstatusDocto.size()) + ")";
			super.addCondicionAdicional(condicionAdicional);
			//agregar a variables bind estos valores
			super.addVariablesBind(lEstatusDocto);	//El metodo recibe o un objeto o una lista que agrega a la lista actual de variables bind
		}

		if (this.tipoCambioEstatus!=null && !"".equals(this.tipoCambioEstatus)&&this.conDocumentosCambioEstatus) {
			List lTipoCambioEstatus = Comunes.explode(",", this.tipoCambioEstatus,Integer.class);
			condicionAdicional = " ce.ic_cambio_estatus in (" + Comunes.repetirCadenaConSeparador("?",",",lTipoCambioEstatus.size()) + ")";
			super.addCondicionAdicional(condicionAdicional);
			//agregar a variables bind estos valores
			super.addVariablesBind(lTipoCambioEstatus);	//El metodo recibe o un objeto o una lista que agrega a la lista actual de variables bind
		}

		//AGREGADO POR FODEA 034-2008 CALL CENTER
		if (!this.conNumeroProveedor) {
			condicionAdicional = " pe.cs_num_proveedor = ? ";
			super.addCondicionAdicional(condicionAdicional);
			//agregar a variables bind estos valores
			super.addVariablesBind("N");	//El metodo recibe o un objeto o una lista que agrega a la lista actual de variables bind
		}

	}

	
	/**
	 * @deprecated Usar setCesionDerechos(String cesionDerechos)
	 * @param cs_cesion_derechos
	 */
	public void setG_cs_cesion_derechos(String cs_cesion_derechos){//FODEA 37 - 2009
		this.cesionDerechos = cs_cesion_derechos;
	}

	/**
	 * @deprecated Usar getClaveEpo()
	 */
	public String getNoepo() {
		return this.clavesEpo;
	}

	/**
	 * @deprecated Usar setClaveEpo(String claveEpo)
	 */
	public void setNoepo(String noepo) {
		this.clavesEpo = noepo;
	}

//true para Pyme con documentos publicados o false de lo contrario

	/**
	 * Establece si se deben mostrar solamente pymes con documentos publicados o no
	 * El valor predeterminado es false
	 * @param conDocumentosPublicados true para mostrar solamente pymes con
	 * 	publicación o false de lo contrario
	 */
	public void setConDocumentosPublicados(boolean conDocumentosPublicados) {
		this.conDocumentosPublicados = conDocumentosPublicados;
	}


	public boolean isConDocumentosPublicados() {
		return conDocumentosPublicados;
	}

/**
	 * Establece si se deben mostrar solamente pymes con documentos publicados o no
	 * El valor predeterminado es false
	 * @param conDocumentosPublicados true para mostrar solamente pymes con
	 * 	publicación o false de lo contrario
	 */
	public void setConPymeEpoInterno(boolean conPymeEpoInterno) {
		this.conPymeEpoInterno = conPymeEpoInterno;
	}


	public boolean isConPymeEpoInterno() {
		return conPymeEpoInterno;
	}




	/**
	 * Estable los estatus de los documentos que la Pyme
	 * debe tener
	 * @param estatusDocumentos Cadena con los estatus de los documentos
	 * 	separados por coma
	 */
	public void setEstatusDocumentos(String estatusDocumentos) {
		this.estatusDocumentos = estatusDocumentos;
	}


	public String getEstatusDocumentos() {
		return estatusDocumentos;
	}


	/**
	 * Establece si solamente deben mostrarse pymes con documentos que
	 * tengan cambio de estatus o no.
	 * El valor predeterminado es false
	 * @param conDocumentosCambioEstatus true para mostrar unicamente pymes con
	 * 	cambio de estatus, false de lo contrario
	 */
	public void setConDocumentosCambioEstatus(boolean conDocumentosCambioEstatus) {
		this.conDocumentosCambioEstatus = conDocumentosCambioEstatus;
	}


	public boolean isConDocumentosCambioEstatus() {
		return conDocumentosCambioEstatus;
	}


	/**
	 * Establece las claves de los cambios de estatus de los documentos que deben
	 * considerarse para que una Pyme sea mostrada o no.
	 * @param tipoCambioEstatus Cadena con las claves de cambio de estatus
	 * 		de documento separadas por coma
	 */
	public void setTipoCambioEstatus(String tipoCambioEstatus) {
		this.tipoCambioEstatus = tipoCambioEstatus;
	}


	public String getTipoCambioEstatus() {
		return tipoCambioEstatus;
	}


	/**
	 * Establece el IF con el que la pyme debe de tener una cuenta bancaria
	 * @param ifCuentaBancaria Clave del IF (ic_if)
	 */
	public void setIfCuentaBancaria(String ifCuentaBancaria) {
		this.ifCuentaBancaria = ifCuentaBancaria;
	}


	public String getIfCuentaBancaria() {
		return ifCuentaBancaria;
	}


	/**
	 * Establece si las pymes deben de tener o no numero de proveedor.
	 * El valor predeterminado es true
	 * @param conNumeroProveedor false para mostrar pymes sin numero de proveedor.
	 */
	public void setConNumeroProveedor(boolean conNumeroProveedor) {
		this.conNumeroProveedor = conNumeroProveedor;
	}


	public boolean isConNumeroProveedor() {
		return conNumeroProveedor;
	}


	/**
	 * Especifica la epo con la que la pyme tiene relación.
	 * @param claveEpo Clave de la epo (ic_epo)
	 */
	public void setClaveEpo(String claveEpo) {
		this.clavesEpo = claveEpo;
	}


	public String getClaveEpo() {
		return this.clavesEpo;
	}


	/**
	 * Determina si mostrara Pymes relacionadas al producto de Cesion de Derechos
	 * @param cesionDerechos indicador para mostrar o no pymes relacionadas
	 * 	a Cesion de Derechos. "S" para mostrar las que si manejen o "N"
	 *    para las que no  ¿????
	 *
	 */
	public void setCesionDerechos(String cesionDerechos) {
		this.cesionDerechos = cesionDerechos;
	}


	public String getCesionDerechos() {
		return cesionDerechos;
	}


	/**
	 * Establece un conjunto de EPOS con las que la Pyme debe tener relacion
	 * @param clavesEpo Claves de epo sepradas por coma
	 */
	public void setClavesEpo(String clavesEpo) {
		this.clavesEpo = clavesEpo;
	}


	public String getClavesEpo() {
		return clavesEpo;
	}

	/**
	 * Establece la clave de la moneda de la cuenta bancaria que debe tener con
	 * el if especificado. Este campo solo aplica cuando se especifica
	 * el if de la cuenta bancaria (setIfCuentaBancaria)
	 * @param claveMonedaCuentaBancaria Clave de la moneda
	 */
	public void setClaveMonedaCuentaBancaria(String claveMonedaCuentaBancaria) {
		this.claveMonedaCuentaBancaria = claveMonedaCuentaBancaria;
	}


	public String getClaveMonedaCuentaBancaria() {
		return claveMonedaCuentaBancaria;
	}
 

}