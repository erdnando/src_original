package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Esta clase genera u obtiene los valores del catalogo
 * CatalogoVentanilla
 * @author Deysi Laura Hernandez Contreras
 */
 
public class CatalogoVentanilla extends CatalogoAbstract
{
	private String clave;
	private String descripcion;
	private String seleccion;
	private String order;
	
	public CatalogoVentanilla()
	{
	}
	
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(CatalogoVentanilla.class);
		
	/**
	 * Obtiene una lista de elementos de tipo ElementoCatalogo
	 * @return regresa una colecci�n de elementos obtenidos
	 */	
	public List getListaElementos()
	{
		
		log.info("getListaElementos:::::::(E)");
		
		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		StringBuffer tables = new StringBuffer();
		StringBuffer condiciones = new StringBuffer();
		StringBuffer orden = new StringBuffer();
		StringBuffer qryCatIF = new StringBuffer();
	
		try
		{
			con.conexionDB();
			Registros regCatEstado	=	null;
			
			
			tables.append(" comcat_ventanilla_epo ");	
			
		
			
			
			if(this.seleccion!=null && !this.seleccion.equals("") ){
				condiciones.append(" where ic_epo  = "+this.seleccion+"");
			}
		
										
			orden.append(" ORDER BY  DESCRIPCION ");
			
			qryCatIF.append(" select  "+this.clave+" AS CLAVE, " + 
										this.descripcion +" AS DESCRIPCION "+
										" FROM " + tables + condiciones);
		
		
			if(this.order!=null && !this.order.equals(""))
				qryCatIF.append("order by "+this.order);
			 
			log.debug("CatalogoVentanilla::::::::::"+qryCatIF);
			
			regCatEstado = con.consultarDB(qryCatIF.toString());
			
			while(regCatEstado.next()) {
					ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					elementoCatalogo.setClave(regCatEstado.getString("CLAVE"));
					elementoCatalogo.setDescripcion(regCatEstado.getString("DESCRIPCION"));
					coleccionElementos.add(elementoCatalogo);
			}
		
		
		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		log.info("getListaElementos:::::::(S)");	
		return coleccionElementos;													
		
	}
	
	

	public String getClave()
	{
		return clave;
	}
	public String getDescripcion()
	{
		return descripcion;
	}
	public String getSeleccion()
	{
		return seleccion;
	}

	
/**
* Establece la clave(id) que se va obtener en la consulta
*/	
	public void setClave(String clave)
	{
		this.clave = clave;		
	}

/**
* Establece la descripcion  que se va obtener en la consulta
*/		
	public void setDescripcion(String descripcion)
	{
		this.descripcion = descripcion;
	}

/**
* Establece valor para indicar algun valor en especifico a consultar
*/	
	public void setSeleccion(String seleccion)
	{
		this.seleccion = seleccion;
	}
/**
* Establece el campo por el cual se va a ordenar
*/	
	public void setOrden(String order)
	{
		this.order = order;
	}

}