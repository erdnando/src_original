package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Catalogo de Mandante (comcat_mandante)
 *
 * Ejemplo de uso:
 * CatalogoMandante cat = new CatalogoMandante();
 * cat.setCampoClave("ic_mandante");
 * cat.setCampoDescripcion("cg_razon_social");
 * cat.setTipoAfiliado("M");
 * cat.setOrden("cg_razon_social");
 * List l = cat.getListaElementos();  //Lista con elementos ElementoCatalogo
 *
 * @author Hugo Vargas
 */
public class CatalogoMandante extends CatalogoAbstract {
	
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoMandante.class);
	private String tipoAfiliado;
	private String claveEpo;
	
	StringBuffer tablas = new StringBuffer();

	public CatalogoMandante() {
		super();
		super.setPrefijoTablaPrincipal("cm."); //from comcat_mandante m
		//super.setTabla("comcat_mandante");
	}
	
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colección de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer hint = new StringBuffer();
	
		tablas.append(" comcat_mandante cm ");
		
		
			tablas.append(" ,comrel_mandante_epo cme ");
				

		//hint.append(" /*+ USE_NL(ie e param) */ " );

		StringBuffer condicionJoin = new StringBuffer();
		condicionJoin.append(" cm.ic_mandante = cm.ic_mandante ");
		if (claveEpo!= null && !claveEpo.equals(""))
			condicionJoin.append(" AND cm.ic_mandante = cme.ic_mandante ");

		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
		
		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(
				" SELECT  " + hint + " DISTINCT " + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
				);
				
				
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y además se manda a llamar la implementación especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}
	
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales necesarias para contemplar el
	 * tipoAfiliado.
	 */
	protected void setCondicionesAdicionales() {
		//log.debug("setCondicionesAdicionales(E)");

		//Se valida que esten los campos requeridos:
		try {
			if (this.tipoAfiliado == null || this.tipoAfiliado.equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		} catch (Exception e) {
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer condicionAdicional = new StringBuffer();
		
		condicionAdicional.append(" ic_no_afiliado = ? ");
		
		super.addCondicionAdicional(condicionAdicional.toString());
		//agregar a variables bind estos valores
		super.addVariablesBind(this.tipoAfiliado);
		//log.debug("setCondicionesAdicionales(S)");
		
		if (this.claveEpo != null && !this.claveEpo.equals("")) {
			condicionAdicional = new StringBuffer();
			condicionAdicional.append(" cme.ic_epo in( "+this.claveEpo+" )");
			super.addCondicionAdicional(condicionAdicional.toString());
			//super.addVariablesBind(new Integer(this.claveEpo));
		}
		
		
	}

  public void setTipoAfiliado(String tipoAfiliado)
  {
    this.tipoAfiliado = tipoAfiliado;
  }


	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}


	public String getClaveEpo() {
		return claveEpo;
	}


}