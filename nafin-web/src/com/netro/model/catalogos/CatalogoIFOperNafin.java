package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class CatalogoIFOperNafin extends CatalogoSimple
{
	private String clave;     
	private String descripcion;
	private String seleccion;
	private String order;
	
	private static final Log log = ServiceLocator.getInstance().getLog(CatalogoIFOperNafin.class);
	private String cs_tipo;
	private String financiera;
	private String tipoLinea;


/**
 * Obtiene una lista de elementos de tipo ElementoCatalogo
 * @return regresa una colecci�n de elementos obtenidos
 */ 
	public List getListaElementos()
	{
		log.info("getListaElementos(E)");
		List coleccionElementos = new ArrayList();
		
		AccesoDB con = new AccesoDB();		      
		String tables, condiciones, orden;
		String qryCatalogo;
		try
		{
			con.conexionDB();
			Registros regCatSubsectorECO	=	null;
			
			if(!"C".equals(tipoLinea)){
			tables			= " com_cedula_concilia cc, comcat_if i ";			
			condiciones	= " WHERE  cc.ic_if = i.ic_if ";
			
			if(!cs_tipo.equals("")) {
				condiciones	+= "and I.CS_TIPO = '"+ this.cs_tipo+"'";
			}
				
			qryCatalogo = "select "+this.clave+" AS CLAVE, " +
								this.descripcion +" AS DESCRIPCION ";
								
				if(!financiera.equals("")) {		
				qryCatalogo  += ", "+this.financiera +" AS FINANCIERA ";
			}
			
			qryCatalogo +=	" FROM " + tables + condiciones;
				qryCatalogo +=" GROUP BY i.ic_if, '('||ic_financiera||') '||i.cg_razon_social, ic_financiera  ";
				
				if(this.order!=null && !this.order.equals("")){
					qryCatalogo += " ORDER BY "+this.order;
				}
			}else
			{
				if(!"CE".equals(cs_tipo)){
					tables		= " com_cedula_concilia cc, comcat_if i ";			
					condiciones	= " WHERE  cc.ic_if = i.ic_if "+
								  " AND cc.IN_NUMERO_SIRAC is not null " +
								  " AND cc.IC_CLIENTE_EXTERNO is null ";
											
					if(!cs_tipo.equals("")) {
						condiciones	+= "and I.CS_TIPO = '"+ this.cs_tipo+"'";
					}
					
					qryCatalogo = "select "+this.clave+" AS CLAVE, " +
										this.descripcion +" AS DESCRIPCION, cc.IN_NUMERO_SIRAC ";
											
					qryCatalogo +=	" FROM " + tables + condiciones;
					qryCatalogo +=" GROUP BY i.ic_if, '('||cc.IN_NUMERO_SIRAC||') '||i.cg_razon_social, cc.IN_NUMERO_SIRAC  ";
			
			if(this.order!=null && !this.order.equals("")){
				qryCatalogo += " ORDER BY "+this.order;
			}
				}else
				{
					tables		= " com_cedula_concilia cc, comcat_cli_externo i ";			
					condiciones	= " WHERE  cc.IC_CLIENTE_EXTERNO = i.IC_NAFIN_ELECTRONICO ";
					
					qryCatalogo = "select "+this.clave+" AS CLAVE, " +
										this.descripcion +" AS DESCRIPCION, cc.IN_NUMERO_SIRAC ";
			 
					qryCatalogo +=	" FROM " + tables + condiciones;
					qryCatalogo +=" GROUP BY cc.IC_CLIENTE_EXTERNO, '('||cc.IN_NUMERO_SIRAC||') '||i.cg_razon_social, cc.IN_NUMERO_SIRAC  ";
			 
					if(this.order!=null && !this.order.equals("")){
						qryCatalogo += " ORDER BY "+this.order;
					}
				}
			}
				
			
			log.debug("qryCatalogo  "+qryCatalogo);
			regCatSubsectorECO = con.consultarDB(qryCatalogo);
		
			while(regCatSubsectorECO.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(regCatSubsectorECO.getString("CLAVE"));
				elementoCatalogo.setDescripcion(regCatSubsectorECO.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}


		} catch(Exception e) {
				e.printStackTrace();
				throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
		}

		return coleccionElementos;

	}



/**
* Establece la clave(id) que se va obtener en la consulta
*/
	public void setClave(String clave)
	{
		this.clave = clave;
	}
/**
* Establece la descripcion  que se va obtener en la consulta
*/
	public void setDescripcion(String descripcion)
	{
		this.descripcion = descripcion;
	}
/**
* Establece valor para indicar algun valor en especifico a consultar
*/
	public void setSeleccion(String seleccion)
	{
		this.seleccion = seleccion;
	}

/**
* Establece el campo por el cual se va a ordenar
*/
	public void setOrden(String order)
	{
		this.order = order;
	}

	public String getCs_tipo() {
		return cs_tipo;
	}

	public void setCs_tipo(String cs_tipo) {
		this.cs_tipo = cs_tipo;
	}

	public String getFinanciera() {
		return financiera;
	}

	public void setFinanciera(String financiera) {
		this.financiera = financiera;
	}


	public void setTipoLinea(String tipoLinea)
	{
		this.tipoLinea = tipoLinea;
	}


	public String getTipoLinea()
	{
		return tipoLinea;
	}

}