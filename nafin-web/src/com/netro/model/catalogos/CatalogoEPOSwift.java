package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class CatalogoEPOSwift extends CatalogoAbstract {

	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoEPOSwift.class);
	private String tipoAfiliado = "";
	private String cveProducto = "";
	private String cvePyme = "";
	private String cveIf = "";
  private String csModifica = "";
	
	
	StringBuffer tablas = new StringBuffer();
	
	public CatalogoEPOSwift() {
		super();
		super.setPrefijoTablaPrincipal("e.");
	}
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colección de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ||
				this.tipoAfiliado == null ||this.tipoAfiliado.equals("")) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
			
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer hint = new StringBuffer();
	
		tablas.append(" comcat_epo e  ");
		if("P".equals(tipoAfiliado)){
			tablas.append(" ,comrel_pyme_epo pe, comrel_producto_epo cpe ");
		}else if("I".equals(tipoAfiliado) || "B".equals(tipoAfiliado)){
			tablas.append(" ,comrel_producto_epo cpe, comrel_if_epo ie, comcat_if i ");
      if("S".equals(csModifica))
      {
        tablas.append(" ,comrel_if_epo_x_producto iep ");
      }
		}
				
		//hint.append(" /*+ USE_NL(fi ff) */ " );

		StringBuffer condicionJoin = new StringBuffer();
		
		if("P".equals(tipoAfiliado)){
			condicionJoin.append(" pe.ic_epo = e.ic_epo ");
			condicionJoin.append(" AND pe.ic_epo = cpe.ic_epo ");
		}else if("I".equals(tipoAfiliado) || "B".equals(tipoAfiliado)){
			condicionJoin.append(" e.ic_epo = ie.ic_epo");
			condicionJoin.append(" AND ie.ic_epo = cpe.ic_epo" );
			condicionJoin.append(" AND i.ic_if = ie.ic_if" );
      if("S".equals(csModifica))
      {
        condicionJoin.append(" AND ie.ic_epo = iep.ic_epo ");
        condicionJoin.append(" AND ie.ic_if = iep.ic_if ");
        condicionJoin.append(" AND cpe.ic_producto_nafin = iep.ic_producto_nafin ");
      }
		}

		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
		
		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(
				" SELECT " + hint + " DISTINCT "  + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
				);
				
				
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y además se manda a llamar la implementación especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}
	
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}
	
	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		String condicionAdicional = "";
		
		
		
		try{
			if (this.cvePyme != null && !this.cvePyme.equals("") ){
				Integer.parseInt(cvePyme);
				//throw new Exception("Los parametros requeridos no has sido establecidos");
			}
			if (this.cveIf != null && !this.cveIf.equals("") ){
				Integer.parseInt(cveIf);
				//throw new Exception("Los parametros requeridos no has sido establecidos");
			}
			
			
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
		
		
		if("P".equals(tipoAfiliado)){
			condicionAdicional +="      e.cs_habilitado = ? ";
			condicionAdicional +="      AND pe.cs_habilitado = ? ";
			condicionAdicional +="      AND cpe.cg_dispersion = ? ";
		}else if("I".equals(tipoAfiliado) || "B".equals(tipoAfiliado)){
			condicionAdicional +="      e.cs_habilitado = ? ";
			condicionAdicional +="      AND cpe.cg_dispersion = ? ";
      if("S".equals(csModifica))
      {
        condicionAdicional += " AND iep.cs_habilitado IN (?, ?) ";
      }
		}
		
		
		
		if(cveProducto!=null && !cveProducto.equals("")){
			condicionAdicional +="      AND cpe.ic_producto_nafin = ? " ;
		}
		if(cvePyme!=null && !cvePyme.equals("")){
			condicionAdicional +="      AND pe.ic_pyme = ?";
		}
		if(cveIf!=null && !cveIf.equals("")){
			condicionAdicional +="      AND i.ic_if = ?";
		}
		
	
		if(condicionAdicional.length()>0)
			super.addCondicionAdicional(condicionAdicional);
		
		//agregar a variables bind estos valores
		super.addVariablesBind("S");
		if("P".equals(tipoAfiliado)){
			super.addVariablesBind("S");
		}
		super.addVariablesBind("S");
    
    if("I".equals(tipoAfiliado) || "B".equals(tipoAfiliado)){
      if("S".equals(csModifica))
      {
        super.addVariablesBind("S");
        super.addVariablesBind("H");
      }
    }
    
		if(cveProducto!=null && !cveProducto.equals("")){
			super.addVariablesBind(new Integer(this.cveProducto));
		}
		if(cvePyme!=null && !cvePyme.equals("")){
			super.addVariablesBind(new Integer(this.cvePyme));
		}
		if(cveIf!=null && !cveIf.equals("")){
			super.addVariablesBind(new Integer(this.cveIf));
		}
		
	}


	public void setTipoAfiliado(String tipoAfiliado) {
		this.tipoAfiliado = tipoAfiliado;
	}


	public String getTipoAfiliado() {
		return tipoAfiliado;
	}


	public void setCveProducto(String cveProducto) {
		this.cveProducto = cveProducto;
	}


	public String getCveProducto() {
		return cveProducto;
	}


	public void setCvePyme(String cvePyme) {
		this.cvePyme = cvePyme;
	}


	public String getCvePyme() {
		return cvePyme;
	}


	public void setCveIf(String cveIf) {
		this.cveIf = cveIf;
	}


	public String getCveIf() {
		return cveIf;
	}


  public void setCsModifica(String csModifica)
  {
    this.csModifica = csModifica;
  }


  public String getCsModifica()
  {
    return csModifica;
  }
}