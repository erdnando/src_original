package com.netro.model.catalogos;

import netropology.utilerias.AppException;

/**
 * Catalogo de Perfiles (seg_perfil)
 *
 * Ejemplo de uso:
 * CatalogoPerfil cat = new CatalogoPerfil();
 * cat.setCampoClave("cc_perfil");
 * cat.setCampoDescripcion("cc_perfil");
 * cat.setClaveTipoAfiliado("4");
 * cat.setValoresCondicionIn("ADMIN NAFIN", String.class);
 * cat.setOrden("cc_perfil");
 * List l = cat.getListaElementos();  //Lista con elementos ElementoCatalogo
 *
 * para el caso en el que el campo clave no concuerde con la llave y se emplee
 * valores para in o not in, es necesario especificar el campo llave. ejemplo:
 * cat.setCampoClave("ic_tipo_afiliado||cc_perfil");
 * cat.setCampoDescripcion("cc_perfil");
 * cat.setClaveTipoAfiliado("4");
 * cat.setCampoLlave("cc_perfil");
 * cat.setValoresCondicionIn("ADMIN NAFIN, ADMIN NAFIN SUP", String.class);
 * cat.setOrden("2");
 *
 * @author Gilberto Aparicio
 */
public class CatalogoPerfil extends CatalogoSimple {
	
	private String claveTipoAfiliado;

	public CatalogoPerfil() {
		super.setTabla("seg_perfil");
	}

	/**
	 * Establece las condiciones adicionales necesarias para contemplar el
	 * la clave del tipo de afiliado.
	 */
	protected void setCondicionesAdicionales() {
		//log.debug("setCondicionesAdicionales(E)");

		//Se valida que esten los campos requeridos:
		try {
			if (this.claveTipoAfiliado == null || this.claveTipoAfiliado.equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		} catch (Exception e) {
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		String condicionAdicional = "";
		
		condicionAdicional = " sc_tipo_usuario = ? ";
		
		super.addCondicionAdicional(condicionAdicional);
		//agregar a variables bind estos valores
		super.addVariablesBind(new Integer(this.claveTipoAfiliado));
		//log.debug("setCondicionesAdicionales(S)");
	}

	public String getClaveTipoAfiliado() {
		return claveTipoAfiliado;
	}

	public void setClaveTipoAfiliado(String claveTipoAfiliado) {
		this.claveTipoAfiliado = claveTipoAfiliado;
	}




}