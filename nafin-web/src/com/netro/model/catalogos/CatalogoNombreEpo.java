package com.netro.model.catalogos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Esta clase obtiene los valores del catalogo
 * de Epos para la pantalla de Bases de operacion
 * Ejemplo de uso:
 * CatalogoNombreEpo cat = new CatalogoNombreEpo();
 * String infoRegresar = cat.getJSONElementos();	
 */
 
public class CatalogoNombreEpo extends CatalogoSimple  {
	public CatalogoNombreEpo() {}
	
private static final Log log = ServiceLocator.getInstance().getLog(CatalogoNombreEpo.class);

/**
 * Obtiene una lista de elementos de tipo ElementoCatalogo
 * @return regresa una colecci�n de elementos obtenidos
 */
	public List getListaElementos()
	{
		log.info("getListaElementos(E)");
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;
		ArrayList datos = new ArrayList();
		ArrayList coleccionElementos = new ArrayList();		
		StringBuffer query = new StringBuffer();
		try
		{
			con.conexionDB();

			query.append(" SELECT DISTINCT ce.ic_epo as CLAVE, ce.cg_razon_social as DESCRIPCION ");
			query.append(" FROM comcat_epo ce, comrel_base_operacion crbo ");
			query.append(" WHERE ce.ic_epo = crbo.ic_epo ");
			query.append(" AND ce.cs_habilitado = ? ");
			query.append(" ORDER BY cg_razon_social ");
			
			ps = con.queryPrecompilado(query.toString());
			ps.setString(1, "S");
			log.debug(query.toString());
			rs = ps.executeQuery();
			
			while(rs.next()){				
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(rs.getString("CLAVE"));
				elementoCatalogo.setDescripcion(rs.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
			}

			rs.close();
			ps.close();
		
			return coleccionElementos;
		
		} catch(Exception e) {
				e.printStackTrace();
				throw new RuntimeException("El catalogo NombreEpo no pudo ser generado");
		} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
				log.info("getListaElementos(S)");
		}
	}		

}// fin clase