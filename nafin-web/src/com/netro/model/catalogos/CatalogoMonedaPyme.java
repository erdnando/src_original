package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Catalogo de Monedas (comcat_moneda)
 *
 * Ejemplo de uso:
 * CatalogoMoneda cat = new CatalogoMoneda();
 * cat.setCampoClave("ic_moneda");
 * cat.setCampoDescripcion("cd_nombre");
 * cat.setValoresCondicionIn("1,54", Integer.class);
 * cat.setOrden("CLAVE");	//Solo acepta CLAVE � DESCRIPCION (predeterminado)  (Tambien se puede usar 1 para clave y 2 para la descripcion)
 * List l = cat.getListaElementos();  //Lista con elementos ElementoCatalogo
 *
 */
public class CatalogoMonedaPyme extends CatalogoAbstract {

	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoMoneda.class);

	private String clave;
	private String descripcion;
	private String seleccion;
	private String order;
	private String clavePyme="";
	
	public CatalogoMonedaPyme() {
		super();
		super.setPrefijoTablaPrincipal("M."); //comcat_moneda M
	}
	
	/**
	 * Obtiene una lista de elementos de tipo ElementoCatalogo
	 * @return regresa una colecci�n de elementos obtenidos
	 */	
	public List getListaElementos()
	{
		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		String tables, condiciones, orden;
		StringBuffer qrySQL = new StringBuffer();
		String tablas=" COMCAT_MONEDA M, COMREL_CUENTA_BANCARIA CB ";
		String condicionJoin="  CB.IC_MONEDA = M.IC_MONEDA ";
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		try {
			con.conexionDB();
			Registros regCat	=	null;
			
//////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////Esta seccion esta mal dise�ada, este querie se puede hacer usando CatalogoSimple//////////
//////////////// Se deja por motivos de compatibilidad ///////////////////////////////////////////////////
			//Fodea 037-2009
			
				
				qrySQL.append(
						" SELECT  DISTINCT " + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin);
       
					super.setCondicion();
				
				if (super.getCondicion().length()>0) {
					qrySQL.append(" AND " + super.getCondicion());
				}
			
				if (super.getOrden() == null || super.getOrden().equals("")) {
					qrySQL.append(
							" ORDER BY " + super.getCampoDescripcion() );
				} else {
					qrySQL.append(
							" ORDER BY " + super.getOrden() );
				}
			
				
			
			log.debug("qrySQL ::: "+qrySQL);
			log.debug("vars ::: "+super.getValoresBindCondicion());
				
			regCat = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
			while(regCat.next()) {
					ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					elementoCatalogo.setClave(regCat.getString("CLAVE"));
					elementoCatalogo.setDescripcion(regCat.getString("DESCRIPCION"));
					coleccionElementos.add(elementoCatalogo);
			}
		
		
		} catch(Exception e) {
			e.printStackTrace();
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
				
		return coleccionElementos;													
		
	}
	
	public void setCondicionesAdicionales(){
		StringBuffer condicionAdicional = new StringBuffer("");
		
		try{
			if (this.clavePyme == null || this.clavePyme.equals("")){
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
			Integer.parseInt(this.clavePyme);
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
		
		condicionAdicional.append(
				"  CB.IC_PYME = ? "
				);
		

    
		super.addCondicionAdicional(condicionAdicional.toString());
		//agregar a variables bind estos valores
		super.addVariablesBind(new Integer(this.clavePyme));
	
	}
	/**
	 * @deprecated usar getCampoClave
	 */
	public String getClave()
	{
		return super.getCampoClave();
	}
	/**
	 * @deprecated usar getCampoDescripcion
	 */
	public String getDescripcion()
	{
		return super.getCampoDescripcion();
	}
	
	public String getSeleccion()
	{
		return seleccion;
	}


	
	/**
	* Establece la clave(id) que se va obtener en la consulta
	* @deprecated usar setCampoClave
	*/	
	public void setClave(String clave)
	{
		super.setCampoClave(clave);
	}

	/**
	* Establece la descripcion  que se va obtener en la consulta
	*/		
	public void setDescripcion(String descripcion)
	{
		super.setCampoDescripcion(descripcion);
	}

	/**
	* Establece valor para indicar algun valor en especifico a consultar
	* @deprecated usar setValoresCondicionIn()
	*/	
	public void setSeleccion(String seleccion)
	{
		super.setValoresCondicionIn(seleccion, Integer.class);
	}


	public void setOrder(String order) {
		this.order = order;
	}


	public String getOrder() {
		return order;
	}


	public void setClavePyme(String clavePyme) {
		this.clavePyme = clavePyme;
	}


	public String getClavePyme() {
		return clavePyme;
	}

	/**
	* Establece el campo por el cual se va a ordenar
	*/	
/*	public void setOrden(String order)
	{
		this.order = order;
	}
*/

}