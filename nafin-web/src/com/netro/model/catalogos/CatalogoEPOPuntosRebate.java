package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Catalogo de EPOs para el cálculo de los Puntos Rebate
 *
 * Ejemplo de uso:
 * CatalogoEPOPuntosRebate cat = new CatalogoEPOPuntosRebate();
 * cat.setCampoClave("ic_if");
 * cat.setCampoDescripcion("cg_razon_social");
 * cat.setClaveIf();
 * @author Andrea Luna
 */
public class CatalogoEPOPuntosRebate  extends CatalogoAbstract{

	StringBuffer tablas = new StringBuffer();
	
	private String claveIf = "";
	private String claveProductoNafin = "";
	
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	 private final static Log log = ServiceLocator.getInstance().getLog(CatalogoIF.class);
	
	public CatalogoEPOPuntosRebate() {
		super();
		super.setPrefijoTablaPrincipal("ci.");
	}
	/**
	 * Obtiene una lista de elementos de tipo ElementoCatalogo
	 * @return regresa una coleccion de elementos obtenidos
	 */
	 public List getListaElementos(){
		log.info("getListaElementos(E)");
		
		List coleccionElementos = new ArrayList();
		StringBuffer condicionJoin = new StringBuffer();
		
		StringBuffer hint = new StringBuffer();
		StringBuffer orden = new StringBuffer();
		StringBuffer qryCatIf = new StringBuffer();
		try{
			if(super.getCampoClave() == null || super.getCampoClave().equals("")||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("")){
					throw new Exception("Los parámetros requeridos no han sido establecidos");
				}
		}catch(Exception e){
			throw new AppException("Error en los parámetros establecidos." + e.getMessage());
		}
		tablas.append(" comcat_epo ci, comrel_if_epo_x_producto pro ");
		condicionJoin.append(" ci.ic_epo = pro.ic_epo ");
		
		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
		
		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(
				" SELECT " + hint + " DISTINCT " + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
				);		

		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y además se manda a llamar la implementación especifica
		//setCondicionesAdicionales de la clase
		
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}
	
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	 }
	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales(){
		String condicionAdicional = "";
		/*try{
			if(this.claveIf == null || this.claveIf.equals("")){
				throw new Exception("Los parámetros requeridos no han sido establecidos");
			}	
		}catch(Exception e){
			throw new AppException("Error en los parámetros establecidos. " + e.getMessage());
		}*/
		condicionAdicional = "pro.fg_puntos_rebate is not null ";
		super.addCondicionAdicional(condicionAdicional.toString());
		
		if(this.claveIf!=null && !this.claveIf.equals("")){
			condicionAdicional = " pro.ic_if = ? ";
			super.addVariablesBind(new Integer(this.claveIf));
			super.addCondicionAdicional(condicionAdicional);
		}
	}
	public void setClaveIf(String claveIf) {
		this.claveIf = claveIf;
	}
	public String getClaveIf() {
		return claveIf;
	}
	/**
	 * Establece la clave(id) que se va a poner en la consulta
	 */
	public void setCampoClave(String clave){
		super.setCampoClave(clave);
	 }
	 /**
	  * Establece la descripción que se va a obtener en la consulta
	  */
	public void setCampoDescripcion(String descripcion){
		super.setCampoDescripcion(descripcion);
	  }
}