package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class CatalogoCiudad extends CatalogoAbstract  {

private static Log log = ServiceLocator.getInstance().getLog(CatalogoCiudad.class);
private String tabla;
private String Pais;
private String Estado;
		
	public CatalogoCiudad() {
		super();		
	}
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colecci�n de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		///////////////////////////////////////
		/*String SQLciudad = " SELECT codigo_ciudad,  nombre FROM MG_CIUDADES1 "+
                                           " WHERE  
                                           " order by nombre";*/
		///////////////////////////////////////
			
		StringBuffer qrySQL = new StringBuffer();
		qrySQL.append("SELECT " + super.getCampoClave() + " AS CLAVE, ");
		qrySQL.append(super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + this.tabla+
				"	WHERE codigo_pais = '" + Pais + "'" +
            " AND codigo_departamento = " + Estado );		
	
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {}


	public void setTabla(String tabla) {
		this.tabla = tabla;
	}


	public String getTabla() {
		return tabla;
	}


	public void setPais(String Pais) {
		this.Pais = Pais;
	}


	public String getPais() {
		return Pais;
	}


	public void setEstado(String Estado) {
		this.Estado = Estado;
	}


	public String getEstado() {
		return Estado;
	}
}