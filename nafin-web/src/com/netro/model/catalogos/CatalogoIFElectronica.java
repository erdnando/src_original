package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class CatalogoIFElectronica  extends CatalogoAbstract {

	StringBuffer tablas = new StringBuffer();

	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(CatalogoIFElectronica.class);
	
	public CatalogoIFElectronica() {    }
	
	/**
	 * establece utilizar la lista de elementos del Catalogo
	 * @deprecated Utilizar getListaElementos()
	 */
	public List getListaElementosEmp() {
		return this.getListaElementos();
	}	



	/**
	 * Obtiene una lista de elementos de tipo ElementoCatalogo
	 * @return regresa una colección de elementos obtenidos
	 */	
	public List getListaElementos(){
		log.info("getListaElementos(E)");

		List coleccionElementos = new ArrayList();
		
		StringBuffer condicionJoin = new StringBuffer();
		
		StringBuffer hint = new StringBuffer();
		StringBuffer orden = new StringBuffer();
		StringBuffer qryCatIf = new StringBuffer();
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
					super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
		
		tablas.append(" comcat_if  i, ope_reg_solicitud  s ");
		condicionJoin.append( "  i.ic_if = s.ic_if  ");
		
		
		
		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(
				" SELECT " + hint + " DISTINCT " + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
				);
				
				
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y además se manda a llamar la implementación especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}
	
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
		
	}


	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		String condicionAdicional = "";


	} 

	
}




