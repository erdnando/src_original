package com.netro.model.catalogos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.naming.NamingException;
import netropology.utilerias.*;

public class CatalogoEpoXProductoReafiliacion {

	private String clave;
	private String descripcion;
	private String seleccion;
	private String order;
	private int icPyme;
	private String filtroEpo;

	public CatalogoEpoXProductoReafiliacion(){}
	
	/**
	 * Obtiene una lista de elementos de tipo ElementoCatalogo
	 * @return regresa una colecci�n de elementos obtenidos
	 */	
	public List getListaElementos()
	{
		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		String tables, condiciones, orden;
		String qryCatGrupo;
		try
		{
			con.conexionDB();
			Registros regCatExportador	=	null;
																				
			tables		= " COMCAT_EPO ";
			condiciones	= " WHERE IC_EPO NOT IN (SELECT IC_EPO FROM COMREL_PYME_EPO_X_PRODUCTO WHERE IC_PYME = "+this.icPyme+") AND CS_HABILITADO = 'S' ";
			if(!this.filtroEpo.equals("")){
				condiciones	+= " AND "+ this.descripcion +" LIKE '"+ this.filtroEpo.toUpperCase() +"%' ";
			}
			orden       = " ORDER BY CG_RAZON_SOCIAL ";
										
			qryCatGrupo = "select "+this.clave+" AS CLAVE, " + 
											this.descripcion +" AS DESCRIPCION "+
											" FROM " + tables + condiciones;
			
			if(this.order!=null && !this.order.equals(""))
				qryCatGrupo += "ORDER BY "+this.order;
			else
				qryCatGrupo += orden;
			
			regCatExportador = con.consultarDB(qryCatGrupo);
			while(regCatExportador.next()) {
					ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					elementoCatalogo.setClave(regCatExportador.getString("CLAVE"));
					elementoCatalogo.setDescripcion(regCatExportador.getString("DESCRIPCION"));
					coleccionElementos.add(elementoCatalogo);
			}
		
		
		} catch(Exception e) {
				e.printStackTrace();
				throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
		}
				
		return coleccionElementos;													
		
	}	

	/**
	 * Establece la clave(id) que se va obtener en la consulta
	 */	
	public void setClave(String clave)
	{
		this.clave = clave;		
	}
	
	/**
	* Establece la descripcion  que se va obtener en la consulta
	*/		
	public void setDescripcion(String descripcion)
	{
		this.descripcion = descripcion;
	}
	
	/**
	* Establece la clave(id) que se va obtener en la consulta
	*/	
	public void setOrder(String order)
	{
		this.order = order;		
	}
	
	/**
	* Establece valor para indicar algun valor en especifico a consultar
	*/	
	public void setSeleccion(String seleccion)
	{
		this.seleccion = seleccion;
	}

	public int getIcPyme() {
		return icPyme;
	}

	public void setIcPyme(int icPyme) {
		this.icPyme = icPyme;
	}

	public String getFiltroEpo() {
		return filtroEpo;
	}

	public void setFiltroEpo(String filtroEpo) {
		this.filtroEpo = filtroEpo;
	}

}