package com.netro.model.catalogos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Comunes;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Esta clase genera u obtiene los valores del catalogo
 * comcat_epo, en base a ciertas condiciones establecidas
 *
 * NO TOMAR COMO REFERENCIA A ESTA CLASE para generar otro catalogo, ya que tiene
 * funcionalidad no estandar de catalogo, adem�s de que no usa variables bind
 *
 * @author Erik Barba
 * @author Gilberto Aparicio
 * @author Salim Hernandez
 */


public class CatalogoEPO extends CatalogoAbstract{
	private String seleccion;
	private String claveBancoFondeo;
	private String claveIf;	//Clave de intermediario financiero
	private String mandatoDocumentos;//FODEA 041 - 2009 ACF
	private String cesionDerechos;//FODEA 037 - 2009 ACF
	private String tipoCadena;//FODEA 012 - 2010 ACF
	private String habilitado = "S";
	private String aceptacionRelacionIfEpo;
	private Collection valoresCondicionInDeprecated;
	private Collection valoresCondicionNotInDeprecated;
	private List valoresBindCondicionDeprecated = new ArrayList();
	public String bancofondeo;
	private String listaClavesEPO;
	private String pyme;  // getListaElementosCedulaEvaluacionPYME
	
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(CatalogoEPO.class);

	public CatalogoEPO() {
		super();
		super.setPrefijoTablaPrincipal("CE."); //comcat_epo CE
	}

	/**
	 * Obtiene una lista de elementos de tipo ElementoCatalogo
	 * @return regresa una colecci�n de elementos obtenidos
	 */
	public List getListaElementos(){
		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		String tables = "";
		StringBuffer condiciones = new StringBuffer();
		String orden = "";
		String qryCatEPOs = "";

		log.info("getListaElementos(E)");

		try{
			con.conexionDB();

			Registros regCatEPOs	= null;

			tables = " COMCAT_EPO CE ";
			if (this.habilitado != null && !this.habilitado.equals("")) {
				condiciones.append(" AND CE.cs_habilitado = '" + this.habilitado + "' ");
			}

			if(this.claveIf != null && !this.claveIf.equals("")){tables += ", COMREL_IF_EPO CIE";}

			//if(this.bancofondeo != null && !this.bancofondeo.equals("")){	tables += " , comcat_if I "; }


			if(this.claveIf != null && !this.claveIf.equals("")){
				condiciones.append(" AND CE.IC_EPO = CIE.IC_EPO AND CIE.CS_VOBO_NAFIN = 'S' AND CIE.IC_IF = " + this.claveIf);
				if (this.aceptacionRelacionIfEpo != null && !this.aceptacionRelacionIfEpo.equals("")) {
					condiciones.append(" AND cie.cs_aceptacion = '" + aceptacionRelacionIfEpo + "' ");
				}
			}
			if(this.claveBancoFondeo!=null && !this.claveBancoFondeo.equals("vacio") && !this.claveBancoFondeo.equals("")){condiciones.append(" AND CE.IC_BANCO_FONDEO =  " + this.claveBancoFondeo);}
			if(this.mandatoDocumentos != null && !this.mandatoDocumentos.equals("")){condiciones.append(" AND CE.CS_MANDATO_DOCUMENTO =  '" + this.mandatoDocumentos + "'");}
			if(this.cesionDerechos != null && !this.cesionDerechos.equals("")){condiciones.append(" AND ce.cs_cesion_derechos = '" + this.cesionDerechos + "'");}
			if(this.tipoCadena != null && !this.tipoCadena.equals("")){condiciones.append(" AND ce.ic_tipo_epo = " + this.tipoCadena);}


			if(this.bancofondeo != null && !this.bancofondeo.equals("") ) {
				condiciones.append(" AND CE.ic_banco_fondeo = " + this.bancofondeo);
				if(this.claveIf != null && !this.claveIf.equals("")){
					tables += " , comcat_if I ";
					condiciones.append(" AND CE.cs_habilitado = 'S'  AND CIE.cs_bloqueo = 'N'");
					condiciones.append(" AND I.ic_if = CIE.ic_if ");
				}
			}

			orden = " ORDER BY CE.CG_RAZON_SOCIAL ";

			qryCatEPOs += " SELECT DISTINCT " + super.getCampoClave() + " AS CLAVE, " +
					super.getCampoDescripcion() +" AS DESCRIPCION " +
					" FROM " + tables +
					" WHERE 1 = 1 " + condiciones;

			//Genera las condiciones necesarias y las coloca en this.condicion
			//Al llamar a setCondicion se establecen las condiciones para 
			//IN y NOT IN... y adem�s se manda a llamar la implementaci�n especifica
			//setCondicionesAdicionales de la clase
			super.setCondicion();
	
			if (super.getCondicion().length()>0) {
				qryCatEPOs += " AND " + super.getCondicion();
			}
			
			if (super.getOrden() == null || super.getOrden().equals("")) {
				qryCatEPOs += 
						" ORDER BY " + super.getCampoDescripcion() ;
			} else {
				qryCatEPOs += 
						" ORDER BY " + super.getOrden() ;
			}


			log.debug("..:: qryCatEPOs : " + qryCatEPOs);

			regCatEPOs = con.consultarDB(qryCatEPOs,super.getValoresBindCondicion());

			while(regCatEPOs.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(regCatEPOs.getString("CLAVE"));
				elementoCatalogo.setDescripcion(regCatEPOs.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
			}
		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		log.info("getListaElementos(S)");
		return coleccionElementos;
	}

	/**
	 * Obtiene una lista de elementos de tipo ElementoCatalogo
	 * @return regresa una colecci�n de elementos obtenidos
	 */
	public List getListaElementosDispersion()
	{
		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		String tables, condiciones = "", orden;
		String qryCatEPOs;
		try
		{
			con.conexionDB();
			Registros regCatEPOs	=	null;

			tables			= " COMCAT_EPO CE , COMREL_PRODUCTO_EPO CPE ";

			condiciones	= " WHERE CE.IC_EPO  = CPE.IC_EPO " +
										" AND CPE.IC_PRODUCTO_NAFIN = 1 " +
										" AND CPE.CG_DISPERSION = 'S' ";
			if (this.bancofondeo!=null&&!this.bancofondeo.equals(""))
				condiciones+=(" AND CE.ic_banco_fondeo = " + this.bancofondeo);
			orden				= " ORDER BY CE.CG_RAZON_SOCIAL ";

			qryCatEPOs = "SELECT "+super.getCampoClave()+" AS CLAVE, " +
											" "+super.getCampoDescripcion() +" AS DESCRIPCION, "+
											" 'NAFINWEBMODEL:CATALOGOEPO:getListaElementosDispersion METHOD' AS QUERY "+
											" FROM " + tables + condiciones + orden;


//			System.out.println("*------qryCatEPOsDISP--------::"+qryCatEPOs);

			regCatEPOs = con.consultarDB(qryCatEPOs);
			while(regCatEPOs.next()) {
					ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					elementoCatalogo.setClave(regCatEPOs.getString("CLAVE"));
					elementoCatalogo.setDescripcion(regCatEPOs.getString("DESCRIPCION"));
					coleccionElementos.add(elementoCatalogo);
			}

		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}

		return coleccionElementos;

	}


	/**
	 * Obtiene una lista de elementos de tipo ElementoCatalogo
	 * @return regresa una colecci�n de elementos obtenidos
	 */
	public List getListaElementosModalidad()
	{
		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		String tables, condiciones = "", orden;
		String qryCatEPOs;
		try
		{
			con.conexionDB();
			Registros regCatEPOs	=	null;

			tables			= " COMCAT_EPO CE , COMREL_PRODUCTO_EPO PE ";

			condiciones	= " WHERE CE.IC_EPO  = PE.IC_EPO " +
										" AND PE.IC_PRODUCTO_NAFIN = 1 " +
										" AND (PE.IC_MODALIDAD IS NULL OR PE.IC_MODALIDAD = 1) " +
										" AND CE.CS_HABILITADO='S' ";
				if (this.bancofondeo!=null&&!this.bancofondeo.equals(""))
					condiciones+=(" AND CE.ic_banco_fondeo = " + this.bancofondeo);
			orden				= " ORDER BY CE.CG_RAZON_SOCIAL ";

			qryCatEPOs = "SELECT "+super.getCampoClave()+" AS CLAVE, " +
											" "+super.getCampoDescripcion() +" AS DESCRIPCION, "+
											" 'NAFINWEBMODEL:CATALOGOEPO:getListaElementosDispersion METHOD' AS QUERY "+
											" FROM " + tables + condiciones + orden;


//			System.out.println("*------qryCatEPOsDISP--------::"+qryCatEPOs);

			regCatEPOs = con.consultarDB(qryCatEPOs);
			while(regCatEPOs.next()) {
					ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					elementoCatalogo.setClave(regCatEPOs.getString("CLAVE"));
					elementoCatalogo.setDescripcion(regCatEPOs.getString("DESCRIPCION"));
					coleccionElementos.add(elementoCatalogo);
			}

		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}

		return coleccionElementos;

	}

	/**
		 * Obtiene una lista de elementos de tipo ElementoCatalogo
		 * @return regresa una colecci�n de elementos obtenidos
		 */
		public List getListaElementosEmpresarial()
		{
			AccesoDB con = new AccesoDB();
			List coleccionElementos = new ArrayList();
			String tables, condiciones = "", orden;
			String qryCatEPOs;
			try
			{
				con.conexionDB();
				Registros regCatEPOs	=	null;

				tables			= " COMCAT_EPO CE , COMREL_PRODUCTO_EPO CPE ";

				condiciones	= " WHERE CE.IC_EPO  = CPE.IC_EPO " +
											" AND CPE.IC_PRODUCTO_NAFIN = 8 "+
											" AND CE.CS_HABILITADO = 'S' ";

				orden				= " ORDER BY CE.CG_RAZON_SOCIAL ";

				qryCatEPOs = "SELECT "+super.getCampoClave()+" AS CLAVE, " +
												" "+super.getCampoDescripcion() +" AS DESCRIPCION, "+
												" 'NAFINWEBMODEL:CATALOGOEPO:getListaElementosEmpresarial METHOD' AS QUERY "+
												" FROM " + tables + condiciones + orden;


	//			System.out.println("*------qryCatEPOsDISP--------::"+qryCatEPOs);

				regCatEPOs = con.consultarDB(qryCatEPOs);
				while(regCatEPOs.next()) {
						ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
						elementoCatalogo.setClave(regCatEPOs.getString("CLAVE"));
						elementoCatalogo.setDescripcion(regCatEPOs.getString("DESCRIPCION"));
						coleccionElementos.add(elementoCatalogo);
				}

			} catch(Exception e) {
				e.printStackTrace();
				throw new RuntimeException("El catalogo no pudo ser generado");
			} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
			}

			return coleccionElementos;

		}

	/**
	* Obtiene una lista de elementos de tipo ElementoCatalogo
	* @return regresa una colecci�n de elementos obtenidos
	*/
	public List getListaElementosConvenioUnico(){
		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		String tables, condiciones = "", orden;
		String qryCatEPOs;

		try{
			con.conexionDB();
			Registros regCatEPOs	=	null;

			tables = " COMCAT_EPO CE, COMREL_IF_EPO CIE";
			condiciones	= " WHERE CE.IC_EPO = CIE.IC_EPO AND CIE.CS_VOBO_NAFIN = 'S'";
			if(this.claveIf != null && !this.claveIf.equals("")){condiciones += " AND CIE.IC_IF = "+this.claveIf;}
			orden	= " ORDER BY CE.CG_RAZON_SOCIAL";

			qryCatEPOs = "SELECT " + super.getCampoClave() + " AS CLAVE," +
			" " + super.getCampoDescripcion() + " AS DESCRIPCION"+
			" FROM" + tables + condiciones + orden;

			regCatEPOs = con.consultarDB(qryCatEPOs);
			while(regCatEPOs.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(regCatEPOs.getString("CLAVE"));
				elementoCatalogo.setDescripcion(regCatEPOs.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
			}
		}catch(Exception e){
			e.printStackTrace();
		} finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return coleccionElementos;
	}


	/**
	* Obtiene una lista de elementos de tipo ElementoCatalogo
	* FODEA 032-2010 FVR
	* @return regresa una colecci�n de elementos obtenidos
	*/
	public List getListaElementosAutorizaCtasBenef(){
		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		String tables, condiciones = "", orden;
		String qryCatEPOs;

		try{
			con.conexionDB();
			Registros regCatEPOs	=	null;

			tables = " COMCAT_EPO CE, COMCAT_PARAMETRO_EPO CPE, COM_PARAMETRIZACION_EPO CPA ";

			condiciones	= " WHERE CE.IC_EPO = CPA.IC_EPO "+
							  " AND CPE.CC_PARAMETRO_EPO = CPA.CC_PARAMETRO_EPO "+
							  " AND cpe.cc_parametro_epo = 'AUTORIZA_CTAS_BANC_ENTIDADES' "+
							  " AND CPA.CG_VALOR='S' ";
			orden	= " ORDER BY CE.CG_RAZON_SOCIAL";

			if(claveIf!=null && !claveIf.equals("")){
					tables += ", COMREL_IF_EPO CIE ";
					condiciones +=" AND CE.IC_EPO = CIE.IC_EPO "+
									  " AND CIE.IC_IF = "+claveIf;
			}

			qryCatEPOs = "SELECT " + super.getCampoClave() + " AS CLAVE," +
			" " + super.getCampoDescripcion() + " AS DESCRIPCION"+
			" FROM" + tables + condiciones + orden;

			log.info("qryCatEPOs>>>"+qryCatEPOs);
			regCatEPOs = con.consultarDB(qryCatEPOs);
			while(regCatEPOs.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(regCatEPOs.getString("CLAVE"));
				elementoCatalogo.setDescripcion(regCatEPOs.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
			}
		}catch(Exception e){
			e.printStackTrace();
		} finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return coleccionElementos;
	}


	/**
	 * Genera las condiciones de acuerdo a si existen valores en las
	 * restriciones como In o NotIn
	 * @deprecated
	 * @return Cadena con la condicion
	 */
/*	protected StringBuffer getCondicion() {
		StringBuffer condicion = new StringBuffer();
		if (this.valoresCondicionInDeprecated != null && !this.valoresCondicionInDeprecated.isEmpty()) {
			condicion.append(
					super.getCampoClave() + " IN (" +
					Comunes.repetirCadenaConSeparador("?", ",", this.valoresCondicionInDeprecated.size()) + ") "
			);
			this.valoresBindCondicion.addAll(this.valoresCondicionInDeprecated);

		}

		if (this.valoresCondicionNotInDeprecated != null && !this.valoresCondicionNotInDeprecated.isEmpty()) {
			//Si ya existe una condicion se agrega el operador AND
			if (condicion.length() > 0) {
				condicion.append(" AND ");
			}

			condicion.append(
					super.getCampoClave() + " NOT IN (" +
					Comunes.repetirCadenaConSeparador("?", ",", this.valoresCondicionNotInDeprecated.size()) + ") "
			);
			valoresBindCondicion.addAll(this.valoresCondicionNotInDeprecated);
		}
		//Si hay condiciones agrega el WHERE
		if (condicion.length() > 0) {
			condicion.insert(0, " AND ");
		}

		return condicion;
	}
*/
/**
	 * Obtiene una lista de elementos de tipo ElementoCatalogo
	 * @return regresa una colecci�n de elementos obtenidos
	 */
	public List getListaElementosNotificaciones(){
		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		String tables = "";
		StringBuffer condiciones = new StringBuffer();
		String orden = "";
		String qryCatEPOs = "";

		log.info("getListaElementosNotificaciones(E)");

		try{
			con.conexionDB();

			Registros regCatEPOs	= null;

			tables = " comcat_epo ce, comrel_if_epo ie, comrel_producto_epo cpe, comcat_if i ";
			
			condiciones.append(" AND ce.ic_epo = ie.ic_epo");
			condiciones.append(" AND ie.ic_epo = cpe.ic_epo");
			condiciones.append(" AND i.ic_if = ie.ic_if");
			condiciones.append(" AND i.ic_if = '" + this.get_claveIf() + "'");
			condiciones.append(" AND UPPER (ie.cs_vobo_nafin) = 'S'");
			condiciones.append(" AND ce.cs_habilitado = 'S'");
			condiciones.append(" AND cpe.ic_producto_nafin = 1");

			orden = " ORDER BY 2";

			qryCatEPOs = " SELECT DISTINCT " + super.getCampoClave() + " AS CLAVE, " +
					super.getCampoDescripcion() +" AS DESCRIPCION " +
					" FROM " + tables +
					" WHERE 1 = 1 " + condiciones;
			//Genera las condiciones necesarias y las coloca en this.condicion
			//Al llamar a setCondicion se establecen las condiciones para 
			//IN y NOT IN... y adem�s se manda a llamar la implementaci�n especifica
			//setCondicionesAdicionales de la clase
			super.setCondicion();
	
			if (super.getCondicion().length()>0) {
				qryCatEPOs += " AND " + super.getCondicion();
			}
			
			if (super.getOrden() == null || super.getOrden().equals("")) {
				qryCatEPOs += 
						" ORDER BY " + super.getCampoDescripcion() ;
			} else {
				qryCatEPOs += 
						" ORDER BY " + super.getOrden() ;
			}


			log.debug("..:: qryCatEPOs : " + qryCatEPOs);

			regCatEPOs = con.consultarDB(qryCatEPOs,super.getValoresBindCondicion());

			while(regCatEPOs.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(regCatEPOs.getString("CLAVE"));
				elementoCatalogo.setDescripcion(regCatEPOs.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
			}
		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		log.info("getListaElementos(S)");
		return coleccionElementos;
	}
	
	
	/**
	 * Obtiene una lista de elementos de tipo ElementoCatalogo
	 * @return regresa una colecci�n de elementos obtenidos
	 * GArellano
	 */
	public List getListaElementosInfoDoctos()
	{
		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		String tables, condiciones = "", orden;
		String qryCatEPOs;
		try
		{
			con.conexionDB();
			Registros regCatEPOs	=	null;

			tables			= " COMCAT_EPO CE, COMREL_IF_EPO RIE, COMREL_PRODUCTO_EPO CPE ";

			condiciones	= " WHERE RIE.ic_epo=CE.ic_epo " +
										" AND CE.cs_habilitado='S'" +
										" AND CPE.ic_epo = CE.ic_epo" +
										" AND CPE.ic_producto_nafin = 1" +
										" AND CPE.cs_factoraje_distribuido='S'" +
										" AND RIE.ic_if= '" + this.get_claveIf() + "'";

			orden				= " ";

			qryCatEPOs = "SELECT "+super.getCampoClave()+" AS CLAVE, " +
											" "+super.getCampoDescripcion() +" AS DESCRIPCION, "+
											" 'NAFINWEBMODEL:CATALOGOEPO:getListaElementosDispersion METHOD' AS QUERY "+
											" FROM " + tables + condiciones + orden;
   

//			System.out.println("*------qryCatEPOsDISP--------::"+qryCatEPOs);

			regCatEPOs = con.consultarDB(qryCatEPOs);
			while(regCatEPOs.next()) {
					ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					elementoCatalogo.setClave(regCatEPOs.getString("CLAVE"));
					elementoCatalogo.setDescripcion(regCatEPOs.getString("DESCRIPCION"));
					coleccionElementos.add(elementoCatalogo);
			}

		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}

		return coleccionElementos;

	}
	
	/**
	 * Obtiene una lista de elementos de tipo ElementoCatalogo
	 * @return regresa una colecci�n de elementos obtenidos
	 * GArellano
	 */
	public List getListaElementosInfoDoctosNafin()
	{
		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		String tables, condiciones = "", orden;
		String qryCatEPOs;
		try
		{
			con.conexionDB();
			Registros regCatEPOs	=	null;

			tables			= " COMCAT_EPO CE, COMREL_PRODUCTO_EPO PE ";

			condiciones	= " WHERE CE.IC_EPO = PE.IC_EPO " +
										" AND CE.cs_habilitado='S'" +
										" AND PE.IC_PRODUCTO_NAFIN = 1  ";
									
			
			if(this.claveBancoFondeo != null && !this.claveBancoFondeo.equals("")){condiciones += " AND CE.ic_banco_fondeo = "+this.claveBancoFondeo;}
								
			
			orden				= " ORDER BY 2 ";

			qryCatEPOs = "SELECT "+super.getCampoClave()+" AS CLAVE, " +
											" "+super.getCampoDescripcion() +" AS DESCRIPCION, "+
											" 'NAFINWEBMODEL:CATALOGOEPO:getListaElementosInfoDoctosNafin METHOD' AS QUERY "+
											" FROM " + tables + condiciones + orden;
   

			System.out.println("*------qryCatEPOs--------::"+qryCatEPOs);

			regCatEPOs = con.consultarDB(qryCatEPOs);
			while(regCatEPOs.next()) {
					ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					elementoCatalogo.setClave(regCatEPOs.getString("CLAVE"));
					elementoCatalogo.setDescripcion(regCatEPOs.getString("DESCRIPCION"));
					coleccionElementos.add(elementoCatalogo);
			}

		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}

		return coleccionElementos;

	}
	/**
	 * @deprecated usar getCampoClave()
	 */
	public String getClave()
	{
		return super.getCampoClave();
	}

	/**
	 * @deprecated usar getCampoDescripcion()
	 */
	public String getDescripcion()
	{
		return super.getCampoDescripcion();
	}
	public String getSeleccion()
	{
		return seleccion;
	}

	/**
	 * @deprecated usar getClaveBancoFondeo()
	 */
	public String getIDIF()
	{
		return claveBancoFondeo;
	}

	/**
	 * @deprecated usar getCesionDerechos()
	 */
	public String getG_cs_cesion_derechos(){
		return cesionDerechos;
	}//FODEA 37- 2009 ACF

	/**
	 * Establece la clave(id) que se va obtener en la consulta
	 * @deprecated. Usar setCampoClave()
	 */
	public void setClave(String clave)
	{
		this.setCampoClave(clave);
	}

/**
* Establece la descripcion  que se va obtener en la consulta
* @deprecated usar setCampoDescripcion().
*/
	public void setDescripcion(String descripcion)
	{
		this.setCampoDescripcion(descripcion);
	}

/**
* Establece valor para indicar algun valor en especifico a consultar
*/
	public void setSeleccion(String seleccion)
	{
		this.seleccion = seleccion;
	}

	/**
	 * @deprecated usar setClaveBancoFondeo(String claveBancoFondeo)
	 */
	public void setIDIF(String id_if)
	{
		this.claveBancoFondeo = id_if;
	}


	/**
	 * Establece los valores de las claves que se incluyen
	 * @deprecated usar setValoresCondicionIn
	 */
	public void setCondicionIn(String cadenaValoresCondicionIn, String clase) {
		super.setValoresCondicionIn( Comunes.explode(",", cadenaValoresCondicionIn, clase) );
	}

	/**
	 * Establece los valores de las claves que no se incluyen
	 * @deprecated usar setValoresCondicionNotIn
	 */
	public void setCondicionNotIn(Collection valoresCondicionNotIn) {
		super.setValoresCondicionNotIn( valoresCondicionNotIn );
	}

	/**
	 *	Establece la clave de if (Clave del intermediario financiero, no del banco de fondeo)
	 * @deprecated usar setClaveIf(String claveIf)
	 */
	public void setClave_if(String clave_if){
		this.claveIf = clave_if;
	}

	/**
	 *	Establece la bandera que indica si la epo tiene parametrizado el mandato de documentos.
	 * @deprecated usar setMandatoDocumentos(String mandatoDocumentos)
	 */
	public void setCs_mandato_documento(String cs_mandato_documento) {
		this.mandatoDocumentos = cs_mandato_documento;
	}

	/**
	 * @deprecated usar setCesionDerechos(String cesionDerechos)
	 */
	public void setCs_cesion_derechos(String cs_cesion_derechos){//FODEA 37 - 2009
		this.cesionDerechos = cs_cesion_derechos;
	}

	public String getBancofondeo() {
		return bancofondeo;
	}

	public void setBancofondeo(String bancofondeo) {
		this.bancofondeo = bancofondeo;
	}

  public String getTipoCadena() {return tipoCadena;}

	public void setTipoCadena(String tipoCadena) {
		this.tipoCadena = tipoCadena;
	}


	public void setClaveBancoFondeo(String claveBancoFondeo) {
		this.claveBancoFondeo = claveBancoFondeo;
	}


	public String getClaveBancoFondeo() {
		return claveBancoFondeo;
	}


	/**
	 * Establece la clave del IF con el cual la EPO debe tener una relaci�n.
	 * @param claveIf
	 */
	public void setClaveIf(String claveIf) {
		this.claveIf = claveIf;
	}


	public String getClaveIf() {
		return claveIf;
	}


	public void set_claveIf(String claveIf) {
		this.claveIf = claveIf;
	}


	public String get_claveIf() {
		return claveIf;
	}


	public void setMandatoDocumentos(String mandatoDocumentos) {
		this.mandatoDocumentos = mandatoDocumentos;
	}


	public String getMandatoDocumentos() {
		return mandatoDocumentos;
	}


	public void setCesionDerechos(String cesionDerechos) {
		this.cesionDerechos = cesionDerechos;
	}


	public String getCesionDerechos() {
		return cesionDerechos;
	}


	/**
	 * Establece si la EPO debe estar habilitada o no.
	 * De manera predeterminada el valor es S Si
	 * @param habilitado S Habilitado, N Deshabilitado
	 */
	public void setHabilitado(String habilitado) {
		this.habilitado = habilitado;
	}


	public String getHabilitado() {
		return habilitado;
	}


	/**
	 * Solo aplica si la clave de IF con la cual la EPO debe tener relaci�n,
	 * es especificada.
	 * Sirve para especificar si la relacion IF-EPO debe estar aceptada o no
	 * @param aceptacionRelacionIfEpo Aceptacion IF-EPO: S Si, N no
	 */
	public void setAceptacionRelacionIfEpo(String aceptacionRelacionIfEpo) {
		this.aceptacionRelacionIfEpo = aceptacionRelacionIfEpo;
	}


	public String getAceptacionRelacionIfEpo() {
		return aceptacionRelacionIfEpo;
	}
	
	/**
	 *
	 * Obtiene una lista de EPOs (de tipo <tt>ElementoCatalogo</tt>) que est�n 
	 * habilitadas y operan con dispersion.
	 * PANTALLA: ADMIN NAFIN - ADMINISTRACION - DISPERSION - MONITOR (Tipo de Monitoreo: EPO's)
	 * @throws RuntimeException
	 *
	 * @return regresa una colecci�n de elementos obtenidos
	 *
	 * @author jshernandez
	 * @since  F### - 2013 - ADMIN NAFIN - Migraci�n Monitor; 21/06/2013 01:24:18 p.m.
	 *
	 */
	public List getListaElementosMonitorDispersionEPO(){
		
		AccesoDB con 						= new AccesoDB();
		List 		coleccionElementos 	= new ArrayList();
		String 	tables;
		String 	condiciones = "";
		String 	orden;
		String 	qryCatEPOs	= "";
		
		try {
			
			con.conexionDB();
			
			Registros regCatEPOs	=	null;
			tables					= " COMCAT_EPO CE , COMREL_PRODUCTO_EPO CPE ";

			condiciones	= " WHERE " +
							  " CPE.IC_EPO            = CE.IC_EPO AND "  +
							  ( this.habilitado != null && !this.habilitado.equals("") ?
							  	  " CE.CS_HABILITADO   = '"+this.habilitado+"' AND ":""
							  ) +
							  " CPE.IC_PRODUCTO_NAFIN = 1         AND "  +
							  " CPE.CG_DISPERSION     = 'S'           ";
							  
			orden			= " ORDER BY 2 ";

			qryCatEPOs 	= "SELECT " +
							  " " + super.getCampoClave()       + " AS CLAVE,       " +
							  " " + super.getCampoDescripcion() + " AS DESCRIPCION, " +
							  " 'NAFINWEBMODEL:CATALOGOEPO:getListaElementosMonitorDispersionEPO METHOD' AS QUERY "+
							  " FROM " + 
							 	  tables + 
							 	  condiciones + 
							 	  orden;

			log.debug("getListaElementosMonitorDispersionEPO.qryCatEPOs = <" + qryCatEPOs + ">");
			
			regCatEPOs = con.consultarDB(qryCatEPOs);
			while(regCatEPOs.next()) {
				
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				
				elementoCatalogo.setClave(regCatEPOs.getString("CLAVE"));
				elementoCatalogo.setDescripcion(regCatEPOs.getString("DESCRIPCION"));
				
				coleccionElementos.add(elementoCatalogo);
				
			}

		} catch(Exception e) {
			
			log.error("getListaElementosMonitorDispersionEPO(Exception)");
			log.error("getListaElementosMonitorDispersionEPO.qryCatEPOs       = <" + qryCatEPOs                  + ">");
			log.error("getListaElementosMonitorDispersionEPO.habilitado       = <" + this.habilitado             + ">");
			log.error("getListaElementosMonitorDispersionEPO.campoClave       = <" + super.getCampoClave()       + ">");
			log.error("getListaElementosMonitorDispersionEPO.campoDescripcion = <" + super.getCampoDescripcion() + ">");
			e.printStackTrace();
			
			throw new RuntimeException("El catalogo no pudo ser generado");
			
		} finally {
			
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			
		}

		return coleccionElementos;

	}
	
	/**
	 *
	 * Obtiene una lista de EPOs (de tipo <tt>ElementoCatalogo</tt>) que est�n 
	 * habilitadas y operan con un IF en especifico.
	 * PANTALLA: ADMIN NAFIN - ADMINISTRACION - DISPERSION - MONITOR (Tipo de Monitoreo: IF's)
	 * @throws RuntimeException
	 *
	 * @return regresa una colecci�n de elementos obtenidos
	 *
	 * @author jshernandez
	 * @since  F### - 2013 - ADMIN NAFIN - Migraci�n Monitor; 21/06/2013 03:48:27 p.m.
	 *
	 */
	public List getListaElementosMonitorDispersionIF(){
		
		AccesoDB con 						= new AccesoDB();
		List 		coleccionElementos 	= new ArrayList();
		String 	tables;
		String 	condiciones = "";
		String 	orden;
		String 	qryCatEPOs	= "";
		
		try {
			
			con.conexionDB();
			
			Registros regCatEPOs	=	null;
			tables					= " COMCAT_EPO CE, COMREL_IF_EPO IE, COMCAT_IF I ";
					
			condiciones	 = " WHERE " +
								" CE.IC_EPO           = IE.IC_EPO AND  "  +
								" I.IC_IF             = IE.IC_IF  AND  "  +
								( this.claveIf    != null && !this.claveIf.equals("") ?
								" I.IC_IF             = "+this.claveIf +" AND ":""
								) +
								" IE.CS_VOBO_NAFIN    = 'S'       AND  "  +
								( this.habilitado != null && !this.habilitado.equals("") ?
							  	" CE.CS_HABILITADO    = '"+this.habilitado+"' AND ":""
							   ) +
								" IE.CS_BLOQUEO       = 'N'            ";

			orden			= " ORDER BY 2 ";

			qryCatEPOs 	= "SELECT " +
							  " " + super.getCampoClave()       + " AS CLAVE,       " +
							  " " + super.getCampoDescripcion() + " AS DESCRIPCION, " +
							  " 'NAFINWEBMODEL:CATALOGOEPO:getListaElementosMonitorDispersionIF METHOD' AS QUERY "+
							  " FROM " + 
							 	  tables + 
							 	  condiciones + 
							 	  orden;

			log.debug("getListaElementosMonitorDispersionIF.qryCatEPOs = <" + qryCatEPOs + ">");
			
			regCatEPOs = con.consultarDB(qryCatEPOs);
			while(regCatEPOs.next()) {
				
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				
				elementoCatalogo.setClave(regCatEPOs.getString("CLAVE"));
				elementoCatalogo.setDescripcion(regCatEPOs.getString("DESCRIPCION"));
				
				coleccionElementos.add(elementoCatalogo);
				
			}

		} catch(Exception e) {
			
			log.error("getListaElementosMonitorDispersionIF(Exception)");
			log.error("getListaElementosMonitorDispersionIF.qryCatEPOs       = <" + qryCatEPOs                  + ">");
			log.error("getListaElementosMonitorDispersionIF.claveIf          = <" + this.claveIf                + ">");
			log.error("getListaElementosMonitorDispersionIF.habilitado       = <" + this.habilitado             + ">");
			log.error("getListaElementosMonitorDispersionIF.campoClave       = <" + super.getCampoClave()       + ">");
			log.error("getListaElementosMonitorDispersionIF.campoDescripcion = <" + super.getCampoDescripcion() + ">");
			e.printStackTrace();
			
			throw new RuntimeException("El catalogo no pudo ser generado");
			
		} finally {
			
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			
		}

		return coleccionElementos;

	}
	
	/**
	 *
	 * Obtiene una lista de EPOs (de tipo <tt>ElementoCatalogo</tt>) que est�n 
	 * habilitadas.
	 * PANTALLA: ADMIN NAFIN - ADMINISTRACION - DISPERSION - MONITOR (Tipo de Monitoreo: INFONAVIT)
	 * Nota: La version original del catalogo, tra�a la propiedad aceptacion="H", la cual
	 * se queDA sin utilizar a la hora de construir el query, por lo que en esta versi�n del 
	 * catalogo se omiti� dicha propiedad. Para m�s informaci�n ver codigo de Cat�logo EPO: 
	 * 	<tt>nafin-web/15cadenas/15mantenimiento/15dispersion/15monitordispinfo.jsp</tt>
	 * @throws RuntimeException
	 *
	 * @return regresa una colecci�n de elementos obtenidos
	 *
	 * @author jshernandez
	 * @since  F### - 2013 - ADMIN NAFIN - Migraci�n Monitor; 21/06/2013 06:45:27 p.m.
	 *
	 */
	public List getListaElementosMonitorDispersionINFONAVIT(){
		
		AccesoDB 		con 						= new AccesoDB();
		List 				coleccionElementos 	= new ArrayList();
		String 			tables;
		StringBuffer 	condiciones 			= new StringBuffer("");
		String 			orden;
		String 			qryCatEPOs				= "";
				
		try {
			
			con.conexionDB();
 
			Registros regCatEPOs	= null;
			tables					= " COMCAT_EPO CE ";
				
			boolean hayCampoHabilitado 		= this.habilitado     != null && !this.habilitado.equals("")    ?true:false;
			boolean hayCampoListaClavesEPO	= this.listaClavesEPO != null && !"".equals(this.listaClavesEPO)?true:false;
			boolean hayCondicionAnterior		= false;
			
			if( hayCampoHabilitado || hayCampoListaClavesEPO ){
				condiciones.append(" WHERE ");
			}
			if( hayCampoHabilitado ){
				condiciones.append(" CE.CS_HABILITADO    = '");
				condiciones.append(this.habilitado);
				condiciones.append("' ");
				hayCondicionAnterior = true;
			}
			if( hayCampoListaClavesEPO ){
				if( hayCondicionAnterior ){
					condiciones.append(" AND ");
				}
			  	condiciones.append(" CE.IC_EPO IN (");
			  	condiciones.append(this.listaClavesEPO);
			  	condiciones.append(") "); 
			  	hayCondicionAnterior = true;
			}

			orden			= " ORDER BY 2 ";

			qryCatEPOs 	= "SELECT " +
							  " " + super.getCampoClave()       + " AS CLAVE,       " +
							  " " + super.getCampoDescripcion() + " AS DESCRIPCION, " +
							  " 'NAFINWEBMODEL:CATALOGOEPO:getListaElementosMonitorDispersionINFONAVIT METHOD' AS QUERY "+
							  " FROM " + 
							 	  tables + 
							 	  condiciones.toString() + 
							 	  orden;

			log.debug("getListaElementosMonitorDispersionINFONAVIT.qryCatEPOs = <" + qryCatEPOs + ">");
			
			regCatEPOs = con.consultarDB(qryCatEPOs);
			while(regCatEPOs.next()) {
				
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				
				elementoCatalogo.setClave(regCatEPOs.getString("CLAVE"));
				elementoCatalogo.setDescripcion(regCatEPOs.getString("DESCRIPCION"));
				
				coleccionElementos.add(elementoCatalogo);
				
			}

		} catch(Exception e) {
			
			log.error("getListaElementosMonitorDispersionINFONAVIT(Exception)");
			log.error("getListaElementosMonitorDispersionINFONAVIT.qryCatEPOs       = <" + qryCatEPOs                  + ">");
			log.error("getListaElementosMonitorDispersionINFONAVIT.listaClavesEPO   = <" + this.listaClavesEPO         + ">");
			log.error("getListaElementosMonitorDispersionINFONAVIT.habilitado       = <" + this.habilitado             + ">");
			log.error("getListaElementosMonitorDispersionINFONAVIT.campoClave       = <" + super.getCampoClave()       + ">");
			log.error("getListaElementosMonitorDispersionINFONAVIT.campoDescripcion = <" + super.getCampoDescripcion() + ">");
			e.printStackTrace();
			
			throw new RuntimeException("El catalogo no pudo ser generado");
			
		} finally {
			
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			
		}

		return coleccionElementos;

	}
	
	public String getListaClavesEPO() {		
		return listaClavesEPO;	
	}



	public void setListaClavesEPO(String listaClavesEPO) {		
		this.listaClavesEPO = listaClavesEPO;	
	}

	/**
	 *
	 * Obtiene una lista de EPOs (de tipo <tt>ElementoCatalogo</tt>).
	 * 	PANTALLA: ADMIN NAFIN - DESCUENTO ELECTRONICO - CONSULTAS - CEDULA EVALUACION PYME.
	 * Para las pymes afiliadas a una Epo con contrato parametrizado
	 * se les revisa que hayan aceptado este, de lo contrario 
	 * el combo de EPOs no mostrar� dicha EPO.			
	 * @throws RuntimeException
	 *
	 * @return regresa una colecci�n de elementos obtenidos
	 *
	 * @author jshernandez
	 * @since  F006 - 2013 - ADMIN NAFIN - Migraci�n C�dula Evaluaci�n PYME; 15/07/2013 01:47:12 p.m.
	 *
	 */
	public List getListaElementosCedulaEvaluacionPYME(){
	
		AccesoDB 			con 						= new AccesoDB();
		PreparedStatement	ps							= null;
		ResultSet			rs							= null;
		List 					coleccionElementos 	= new ArrayList();
		StringBuffer		qryCatEPOs				= new StringBuffer(128);
		StringBuffer 		queryContrato 			= new StringBuffer(128);
		String 				tables;
		StringBuffer 		condiciones 			= new StringBuffer("");
		String 				orden						= "";
		List					param						= new ArrayList();
		
		try {
 
			// Conectarse a la Base de Datos
			con.conexionDB();
 
			if( this.pyme != null && !this.pyme.equals("") ){
				
				// Obtener lista de EPOs
				queryContrato.append( 
					" SELECT pe.ic_epo, 'CatalogoEpo.java' as query " +
					" FROM  " +
					" comrel_pyme_epo pe,  " +
					" ( " +
					" SELECT ic_epo, MAX(ic_consecutivo) as ic_consecutivo " +
					" FROM com_contrato_epo ce " +
					" WHERE cs_mostrar='S' " +
					" GROUP BY ic_epo " +
					" ) contrato_epo " +
					" WHERE pe.ic_pyme = ? " +
					" AND pe.ic_epo = contrato_epo.ic_epo " +
					" AND NOT EXISTS ( " +
					"          SELECT ic_epo, ic_consecutivo " +
					"          FROM com_aceptacion_contrato ac " +
					"          WHERE " +
					"          ac.ic_pyme = ? " +
					" 	        AND ac.ic_epo = contrato_epo.ic_epo " +
					"          AND ac.ic_consecutivo = contrato_epo.ic_consecutivo " +
					"          ) "
				);
							
				ps = con.queryPrecompilado(queryContrato.toString());
				//ps.setInt(1, Integer.parseInt(claveEPOEntregaAnticipadaVivienda));
	
				StringBuffer eposNotIn = new StringBuffer();
				ps.setInt(1, Integer.parseInt(this.pyme));
				ps.setInt(2, Integer.parseInt(this.pyme));
				rs = ps.executeQuery();
				while (rs.next()) {
					eposNotIn.append(rs.getString("ic_epo") + ",");
				}
				rs.close();
				ps.close();
					
				String 	condicionContrato = "";
				List 		lEpos 				= null;
				if (eposNotIn.length() > 0) {
					
					eposNotIn.deleteCharAt(eposNotIn.length()-1);
						
					//Obtiene la lista de valores separados por coma y los coloca dentro de una lista como objetos Integer.
					lEpos = Comunes.explode("," , eposNotIn.toString(), java.lang.Integer.class);
						
					condicionContrato = " AND PE.ic_epo NOT IN (" + 
						Comunes.repetirCadenaConSeparador("?", ",", lEpos.size()) + ")";
						
					//param.addAll(lEpos); //Se pasa a la parte de abajao para que conserve el orden adecuado los par�metros
						
				}
				
				//**********************************************************
					
				tables = " COMCAT_EPO CE, COMREL_PYME_EPO PE ";
	
				condiciones.append(
					" WHERE                               "  +
					"    PE.ic_pyme       = ?         and "  + 
					"    PE.ic_epo 		 = CE.ic_epo and "  +
					"    CE.cs_habilitado = ?         and "  +
					"    PE.cs_habilitado = ?             "  + 
					condicionContrato
				);
				param.add(new Integer(this.pyme));
				param.add("S");
				param.add("S");
				if ( lEpos != null ) { // Si hay Valores para la condicionContrato
					param.addAll(lEpos);
				}

			} else {
			
				tables = " COMCAT_EPO CE ";
				condiciones.append(
					" WHERE                  "  +
					"    CE.cs_habilitado = ? "
				);
				param.add("S");
				
			}
			
			List listaCondicionesNotIn = (List)this.getValoresCondicionNotIn();
			String notIn 					  = "";
			for(int i = 0; i < listaCondicionesNotIn.size(); i++){
				if( i>0) notIn += ",";
				notIn += (String) listaCondicionesNotIn.get(i);
			}
			
			if ( notIn != null && !notIn.equals("") ) {
				List lEposNotIn = Comunes.explode("," , notIn, java.lang.Integer.class);
					
				condiciones.append(" AND CE.ic_epo NOT IN (" + 
					Comunes.repetirCadenaConSeparador("?", ",", lEposNotIn.size()) + ")");
					
				param.addAll(lEposNotIn);
			}
			
			orden = this.getOrden();
			if (!orden.equals("")) {
				condiciones.append(" order by "+orden+" ");
			} else {
				condiciones.append(" order by 2 ");
			}
						
			//**********************************************************************************
				
			qryCatEPOs.append( 
				"SELECT " +
				" " + super.getCampoClave()       + " AS CLAVE,       " +
				" " + super.getCampoDescripcion() + " AS DESCRIPCION, " +
				" 'NAFINWEBMODEL:CATALOGOEPO:getListaElementosCedulaEvaluacionPYME METHOD' AS QUERY "+
				" FROM " + 
					tables + 
				 	condiciones.toString() 
			);

			log.debug("getListaElementosCedulaEvaluacionPYME.qryCatEPOs = <" + qryCatEPOs + ">");
			
			Registros regCatEPOs = con.consultarDB(qryCatEPOs.toString(),param);
			while(regCatEPOs.next()) {
				
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				
				elementoCatalogo.setClave(regCatEPOs.getString("CLAVE"));
				elementoCatalogo.setDescripcion(regCatEPOs.getString("DESCRIPCION"));
				
				coleccionElementos.add(elementoCatalogo);
				
			}
			
			/*
			
			SELECT 
			  E.IC_EPO, 
			  E.CG_RAZON_SOCIAL 
			FROM  
			  COMCAT_EPO      E, 
			  COMREL_PYME_EPO PE  
			WHERE 
			  PE.IC_PYME       = ?         AND 
			  PE.IC_EPO        = E.IC_EPO  AND 
			  E.CS_HABILITADO  = ?         AND 
			  PE.CS_HABILITADO = ?         AND 
			  E.IC_EPO not in (?) 
			ORDER BY 2

			*/
			
		} catch(Exception e) {
			
			log.error("getListaElementosCedulaEvaluacionPYME(Exception)");
			log.error("getListaElementosCedulaEvaluacionPYME.qryCatEPOs        = <" + qryCatEPOs        + ">");
			log.error("getListaElementosCedulaEvaluacionPYME.queryContrato     = <" + queryContrato     + ">");
			log.error("getListaElementosCedulaEvaluacionPYME.param             = <" + param             + ">");
			log.error("getListaElementosCedulaEvaluacionPYME.pyme              = <" + pyme              + ">");
			e.printStackTrace();
			
			throw new RuntimeException("El cat�logo no pudo ser generado.");
			
		} finally {
			
			if( rs != null ){ try{ rs.close(); }catch(Exception e){} }
			if( ps != null ){ try{ ps.close(); }catch(Exception e){} }
			
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			
		}

		return coleccionElementos;
	
	}
 
	/**
	 *
	 * Obtiene una lista de EPOs para el Tipo de Dispersion: PEMEX-REF (de tipo <tt>ElementoCatalogo</tt>).
	 * 	PANTALLA: ADMIN_NAFIN - ADMINISTRACI�N � FLUJO DE FONDOS � CONSULTAS 
	 * Las EPOS deben estar parametrizadas en COMREL_EPO_TIPO_FINANCIAMIENTO con el 
	 * Producto Nafin: 4 (Financiamiento a Distribuidores) y con el 
	 * Tipo de Financiamiento: 5 (Plazo Ampliado PEMEX).
	 * @throws RuntimeException
	 *
	 * @return regresa una colecci�n de elementos obtenidos.
	 *
	 * @author jshernandez
	 * @since  F017 - 2013 -- DSCTO ELECT - Fideicomiso para el Desarrollo de Proveedores; 24/09/2013 07:20:15 p.m.
	 *
	 */
	public List getListaElementosDistribuidoresPemexRef(){
	
		AccesoDB 			con 						= new AccesoDB();
		List 					coleccionElementos 	= new ArrayList();
		StringBuffer		qryCatEPOs				= new StringBuffer(128);
		String 				tables;
		StringBuffer 		condiciones 			= new StringBuffer("");
		String 				orden						= "";
		List					param						= new ArrayList();
		
		try {
 
			// Conectarse a la Base de Datos
			con.conexionDB();
 
			// Preparar Tablas de la Consulta
			tables = " COMCAT_EPO CE, COMREL_EPO_TIPO_FINANCIAMIENTO TF ";
			
			// Preparar Condiciones
			condiciones.append(
				" WHERE                                        "  +
				"  	 CE.IC_EPO                 = TF.IC_EPO   "  +
			   "  AND TF.IC_PRODUCTO_NAFIN      = ?           "  + // 4 (Financiamiento a Distribuidores)
			   "  AND TF.IC_TIPO_FINANCIAMIENTO = ?           "    // 5 (Plazo Ampliado PEMEX)
			);
			param.add(new Integer("4"));
			param.add(new Integer("5"));
 
			// Agregar ordenamiento de la informacion
			orden = this.getOrden();
			if (!orden.equals("")) {
				condiciones.append(" order by "+orden+" ");
			} else {
				condiciones.append(" order by 2 ");
			}
						
			//**********************************************************************************
				
			qryCatEPOs.append( 
				"SELECT " +
				" " + super.getCampoClave()       + " AS CLAVE,       " +
				" " + super.getCampoDescripcion() + " AS DESCRIPCION, " +
				" 'NAFINWEBMODEL:CATALOGOEPO:getListaElementosDistribuidoresPemexRef METHOD' AS QUERY "+
				" FROM " + 
					tables + 
				 	condiciones.toString() 
			);

			log.debug("getListaElementosDistribuidoresPemexRef.qryCatEPOs = <" + qryCatEPOs + ">");
			
			Registros regCatEPOs = con.consultarDB(qryCatEPOs.toString(),param);
			while(regCatEPOs.next()) {
				
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				
				elementoCatalogo.setClave(regCatEPOs.getString("CLAVE"));
				elementoCatalogo.setDescripcion(regCatEPOs.getString("DESCRIPCION"));
				
				coleccionElementos.add(elementoCatalogo);
				
			}
 
			// SELECT   /*+INDEX(EPO CP_COMCAT_EPO_PK)*/

			// 	EPO.IC_EPO, 
			// 	EPO.CG_RAZON_SOCIAL

			// FROM 
			// 	COMCAT_EPO                     EPO, 
			// 	COMREL_EPO_TIPO_FINANCIAMIENTO TF

			// WHERE 
			// 		 EPO.IC_EPO                = TF.IC_EPO

			//    AND TF.IC_PRODUCTO_NAFIN      = 4

			//    AND TF.IC_TIPO_FINANCIAMIENTO = 5

 
		} catch(Exception e) {
			
			log.error("getListaElementosDistribuidoresPemexRef(Exception)");
			log.error("getListaElementosDistribuidoresPemexRef.qryCatEPOs = <" + qryCatEPOs + ">");
			e.printStackTrace();
			
			throw new RuntimeException("El cat�logo no pudo ser generado.");
			
		} finally {
						
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			
		}

		return coleccionElementos;
	
	}
	
	public String getPyme() {		
		return this.pyme;	
	}
 
	public void setPyme(String pyme) {		
		this.pyme = pyme;	
	}
 
}