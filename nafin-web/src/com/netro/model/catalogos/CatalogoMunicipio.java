package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;

/**
 * Esta clase genera u obtiene los valores del catalogo
 * COMCAT_MUNICIPIO, en base a ciertas condiciones establecidas
 * @author Fabian Valenzuela
 */
public class CatalogoMunicipio
{
	private String clave;
	private String descripcion;
	private String seleccion;
	private String pais;
	private String estado;
	private String order;

/**
 * Obtiene una lista de elementos de tipo ElementoCatalogo
 * @return regresa una colecci�n de elementos obtenidos
 */
	public List getListaElementos()
	{
		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		String tables, condiciones, orden;
		String qryCatMunicipio;
		try
		{
			con.conexionDB();
			Registros regCatMunicipio	=	null;
			String tmpDesc						= "";
			String tmpClave						= "";


			tables			= " COMCAT_MUNICIPIO ";
			condiciones	= " WHERE IC_PAIS =  " + this.pais +
										" AND IC_ESTADO = " + this.estado;
         if(seleccion != null && !seleccion.equals("")){
				condiciones	= " WHERE IC_PAIS =  " + this.pais +
										" AND IC_ESTADO = " + this.estado+
										" AND IC_MUNICIPIO = " + this.seleccion;
			}
			
			orden				= " ORDER BY CD_NOMBRE ";   

			qryCatMunicipio = "select "+this.clave+" AS CLAVE, " +
											this.descripcion +" AS DESCRIPCION "+
											" FROM " + tables + condiciones;
			if(this.order!=null && !this.order.equals(""))
				qryCatMunicipio += "ORDER BY "+this.order;
			else
				qryCatMunicipio += orden;
				
System.out.println("qryCatMunicipio:::....." +qryCatMunicipio );

			regCatMunicipio = con.consultarDB(qryCatMunicipio);
			while(regCatMunicipio.next()) {
					if((regCatMunicipio.getString("CLAVE")).equals("0")){
						tmpDesc		= regCatMunicipio.getString("DESCRIPCION");
						tmpClave	= regCatMunicipio.getString("Clave");
					}else{
					ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					elementoCatalogo.setClave(regCatMunicipio.getString("CLAVE"));
					elementoCatalogo.setDescripcion(regCatMunicipio.getString("DESCRIPCION"));
					coleccionElementos.add(elementoCatalogo);
					}

			}
			if(!tmpDesc.equals("") && !tmpClave.equals(""))
					{
						ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
						elementoCatalogo.setClave(tmpClave);
						elementoCatalogo.setDescripcion(tmpDesc);
						coleccionElementos.add(elementoCatalogo);
					}


		} catch(Exception e) {
				e.printStackTrace();
				throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
		}

		return coleccionElementos;

	}




/**
* Establece la clave(id) que se va obtener en la consulta
*/
	public void setClave(String clave)
	{
		this.clave = clave;
	}
/**
* Establece la descripcion  que se va obtener en la consulta
*/
	public void setDescripcion(String descripcion)
	{
		this.descripcion = descripcion;
	}
/**
* Establece valor para indicar algun valor en especifico a consultar
*/
	public void setSeleccion(String seleccion)
	{
		this.seleccion = seleccion;
	}
/**
* Establece el id del pais para condicion de la consulta
*/
	public void setPais(String pais)
	{
		this.pais = pais;
	}
/**
* Establece el id del estado para condicion de la consulta
*/
	public void setEstado(String estado)
	{
		this.estado = estado;
	}
/**
* Establece el campo por el cual se va a ordenar
*/
	public void setOrden(String order)
	{
		this.order = order;
	}

}