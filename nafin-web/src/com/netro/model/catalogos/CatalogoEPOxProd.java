package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 *
 */
public class CatalogoEPOxProd extends CatalogoAbstract {

	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(CatalogoEPOxProd.class);
	private String producto;
	private String habilitado;
	private String empresarial;
	private String pyme;
	private String intermediario;
	private String clave;
	private String descripcion;


	public CatalogoEPOxProd() {     }


	public List getListaElementos(){
		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		String tablas = "", condicion ="";
			
		try{
			con.conexionDB();

			Registros regs	= null;
			
			
			if (!intermediario.equals("") && pyme.equals("")) {

				tablas = " comcat_epo E, comrel_if_epo IE, comrel_if_epo_x_producto IEP, comrel_producto_epo PRE ";
				habilitado = (habilitado.equals("")?"'S'":habilitado);
				condicion = " where IE.ic_epo = E.ic_epo "+
					" AND E.CS_HABILITADO = 'S' "+
					" AND IE.ic_if = " + intermediario +
					" AND IE.ic_if = IEP.ic_if "+
					" AND IE.ic_epo = IEP.ic_epo "+
					" AND IEP.CS_HABILITADO = 'S' "+
					" AND IEP.ic_producto_nafin in ("+ producto +") "+
					" AND IEP.cs_habilitado in ("+habilitado+")"+
					" AND IE.cs_vobo_nafin = 'S' "+
					" AND PRE.ic_producto_nafin in ("+producto +") "+
					" AND PRE.ic_epo = E.ic_epo "+
					" AND PRE.cs_habilitado = 'S' ";
					
				if("S".equals(empresarial)||"N".equals(empresarial)) {
					
					if("N".equals(empresarial))
						condicion += "    AND NOT EXISTS(SELECT 1";
					else if("S".equals(empresarial))
						condicion += "    AND EXISTS(SELECT 1";
						
					condicion +=
						"                     FROM comrel_producto_epo cpe"   +
						"                    WHERE cpe.ic_epo = e.ic_epo"   +
						"                      AND cpe.ic_producto_nafin = 8"   +
						"                      AND ic_modalidad = 2)";
				}

			} else if (!pyme.equals("") && intermediario.equals("")) {

				tablas = " comcat_epo E, comrel_pyme_epo PE, comrel_pyme_epo_x_producto PEP ";
				habilitado = (habilitado.equals("")?"'H'":habilitado);
				condicion = " where PE.ic_epo = E.ic_epo "+
					" and PE.ic_pyme = "+ pyme +
					" and PE.cs_habilitado = 'S' "+
					" and E.cs_habilitado = 'S' "+
					" and PEP.ic_epo = pe.ic_epo "+
					" and PEP.ic_pyme = pe.ic_pyme "+
					" and PEP.cs_habilitado in ("+habilitado+")"+
					" and PEP.ic_producto_nafin in (" + producto+") ";
					
					if("S".equals(empresarial)||"N".equals(empresarial)) {
					
					if("N".equals(empresarial))
						condicion += "    AND NOT EXISTS(SELECT 1";
					else if("S".equals(empresarial))
						condicion += "    AND EXISTS(SELECT 1";
						
					condicion +=
						"                     FROM comrel_producto_epo cpe"   +
						"                    WHERE cpe.ic_epo = e.ic_epo"   +
						"                      AND cpe.ic_producto_nafin = 8"   +
						"                      AND ic_modalidad = 2)";
					}//fin if empresarial

			} else if (!pyme.equals("") && !intermediario.equals("")) {

				tablas = " comcat_epo E, comrel_if_epo IE, comrel_pyme_epo PE, comrel_if_epo_x_producto IEP, comrel_pyme_epo_x_producto PEP ";
				habilitado = (habilitado.equals("")?"'H'":habilitado);
				condicion = " where PE.ic_epo = E.ic_epo "+
					" AND PE.ic_pyme = "+ pyme +
					" AND PE.cs_habilitado = 'S' "+
					" AND E.cs_habilitado = 'S' "+
					" AND PEP.ic_epo = pe.ic_epo "+
					" AND PEP.ic_pyme = pe.ic_pyme "+
					" AND PEP.cs_habilitado in ("+habilitado+")";
					habilitado = (habilitado.equals("")?"'S'":habilitado);					
				condicion += " AND IEP.cs_habilitado in ("+habilitado+")"+
					" AND PEP.ic_producto_nafin in (" + producto+") "+
					" AND E.ic_epo = IE.ic_epo "+
					" AND E.CS_HABILITADO = 'S' "+
					" AND IE.ic_if = " + intermediario +
					" AND IE.ic_if = IEP.ic_if "+
					" AND IE.ic_epo = IEP.ic_epo "+
					" AND PEP.ic_epo = IEP.ic_epo " +
					" AND IEP.CS_HABILITADO = 'S' "+
					" AND IEP.ic_producto_nafin in ("+ producto +") "+
					" AND IE.cs_vobo_nafin = 'S' ";
					
					
					if("S".equals(empresarial)||"N".equals(empresarial)) {
					
					if("N".equals(empresarial))
						condicion += "    AND NOT EXISTS(SELECT 1";
					else if("S".equals(empresarial))
						condicion += "    AND EXISTS(SELECT 1";
						
					condicion +=
						"                     FROM comrel_producto_epo cpe"   +
						"                    WHERE cpe.ic_epo = e.ic_epo"   +
						"                      AND cpe.ic_producto_nafin = 8"   +
						"                      AND ic_modalidad = 2)";
					}//fin if empresarial

					
					
					
					
					
			} else {

				tablas = " COMCAT_EPO E, COMREL_PRODUCTO_EPO PRE ";
				condicion = " WHERE E.cs_habilitado = 'S' "+
					" AND PRE.ic_producto_nafin in (" + producto +") "+
					" AND E.ic_epo = PRE.ic_epo "+
					" AND PRE.cs_habilitado = 'S' ";
				if("S".equals(empresarial)||"N".equals(empresarial)) {
					
					if("N".equals(empresarial))
						condicion += "    AND NOT EXISTS(SELECT 1";
					else if("S".equals(empresarial))
						condicion += "    AND EXISTS(SELECT 1";
						
					condicion +=
						"                     FROM comrel_producto_epo cpe"   +
						"                    WHERE cpe.ic_epo = e.ic_epo"   +
						"                      AND cpe.ic_producto_nafin = 8"   +
						"                      AND ic_modalidad = 2)";
				}
			}
			
			String qrySentencia = "select distinct "+this.clave+" AS CLAVE ,"+this.descripcion+ " AS DESCRIPCION " +
			" from "+tablas+	condicion +" ORDER BY " + super.getOrden();
			
			System.out.println("qrySentencia---->  "+qrySentencia);
			
			regs = con.consultarDB(qrySentencia,super.getValoresBindCondicion());
	
			while(regs.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					elementoCatalogo.setClave(regs.getString("CLAVE"));
					elementoCatalogo.setDescripcion(regs.getString("DESCRIPCION"));
					coleccionElementos.add(elementoCatalogo);
			}
			
		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		log.info("getListaElementos(S)");
		return coleccionElementos;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getHabilitado() {
		return habilitado;
	}

	public void setHabilitado(String habilitado) {
		this.habilitado = habilitado;
	}

	public String getEmpresarial() {
		return empresarial;
	}

	public void setEmpresarial(String empresarial) {
		this.empresarial = empresarial;
	}

	public String getPyme() {
		return pyme;
	}

	public void setPyme(String pyme) {
		this.pyme = pyme;
	}

	public String getIntermediario() {
		return intermediario;
	}

	public void setIntermediario(String intermediario) {
		this.intermediario = intermediario;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
		
}

