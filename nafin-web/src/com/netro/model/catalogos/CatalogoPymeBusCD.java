package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Catalogo de Pymes - Busqueda Avanzada (comcat_pyme)
 * @author Deysi Laura Hernandez Contreras
 */
 
public class CatalogoPymeBusCD extends CatalogoAbstract {
	
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoPymeBusCD.class);
	
	private String claveEpo = "";
	private String nombrePyme = "";
	private String rfcPyme = "";
	private String clavePyme = "";
	
	StringBuffer tablas = new StringBuffer();
	
	public CatalogoPymeBusCD() {		
	}
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colección de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		
		StringBuffer hint = new StringBuffer();
		StringBuffer grupo = new StringBuffer();
		StringBuffer info = new StringBuffer();
		tablas.append(" comrel_pyme_epo pe,  ");
		tablas.append(" com_documento d, ");
		tablas.append(" comrel_nafin crn,  ");
		tablas.append(" comcat_pyme p " );
				
		hint.append(" /*+index(pe CP_COMREL_PYME_EPO_PK) index(p CP_COMCAT_PYME_PK) use_nl(pe d p)*/   " );

		info.append(" p.ic_pyme, pe.cg_pyme_epo_interno, p.cg_razon_social, " );
		
			 
		StringBuffer condicionJoin = new StringBuffer();
		
		condicionJoin.append(" p.ic_pyme = pe.ic_pyme ");
    condicionJoin.append(" AND p.ic_pyme = crn.ic_epo_pyme_if ");
    condicionJoin.append(" AND pe.cs_habilitado = 'S' ");
    condicionJoin.append(" AND p.cs_habilitado = 'S' ");
    condicionJoin.append(" AND p.cs_invalido != 'S' ");
    condicionJoin.append(" AND d.ic_pyme = pe.ic_pyme ");
    condicionJoin.append(" AND pe.cs_num_proveedor = 'N' ");
    condicionJoin.append(" AND d.ic_epo = pe.ic_epo ");
    condicionJoin.append(" AND crn.cg_tipo = 'P' ");
			
		grupo.append(" GROUP BY p.ic_pyme,"+
               " pe.cg_pyme_epo_interno,"+
               " p.cg_razon_social,"+
               " RPAD (pe.cg_pyme_epo_interno, 10, ' ') || p.cg_razon_social,"+
               " crn.ic_nafin_electronico");
			 
       
		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(" SELECT " + hint + " DISTINCT "+ info  + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin			
				);
				
				
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y además se manda a llamar la implementación especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}
	
		
			qrySQL.append(grupo);
		
		
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		String condicionAdicional = "";
		boolean and = false;
		
		try{
			if (this.claveEpo != null && !this.claveEpo.equals("") ){
				Integer.parseInt(this.claveEpo);
				//throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		
			
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
		
			if(claveEpo!=null && !claveEpo.equals("")){
				condicionAdicional +=(and?" AND ":"")+" pe.ic_epo = ? ";
				and = true;
			}
			if(clavePyme!=null && !clavePyme.equals("")){
				condicionAdicional +=(and?" AND ":"")+" cg_pyme_epo_interno LIKE  NVL (REPLACE (UPPER (?), '*', '%'), '%') ";
				and = true;
			}
			if(nombrePyme!=null && !nombrePyme.equals("")){
				condicionAdicional += (and?" AND ":"")+" cg_razon_social LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') ";
				and = true;
			}
			if(rfcPyme!=null && !rfcPyme.equals("")){
				condicionAdicional += (and?" AND ":"")+" cg_rfc LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') ";
				and = true;
			}
		
			if(condicionAdicional.length()>0)
				super.addCondicionAdicional(condicionAdicional);
			
			//agregar a variables bind estos valores
			if(claveEpo!=null && !claveEpo.equals(""))
				super.addVariablesBind(new Integer(this.claveEpo));
			if(clavePyme!=null && !clavePyme.equals(""))	
				super.addVariablesBind((this.clavePyme).replaceAll("[*]","%").toUpperCase());
			if(nombrePyme!=null && !nombrePyme.equals(""))
				super.addVariablesBind((this.nombrePyme).replaceAll("[*]","%").toUpperCase());
			if(rfcPyme!=null && !rfcPyme.equals(""))
				super.addVariablesBind((this.rfcPyme).replaceAll("[*]","%").toUpperCase());		
			
	}


	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}


	public String getClaveEpo() {
		return claveEpo;
	}


	public void setRfcPyme(String rfcPyme) {
		this.rfcPyme = rfcPyme;
	}


	public String getRfcPyme() {
		return rfcPyme;
	}


	public void setNombrePyme(String nombrePyme) {
		this.nombrePyme = nombrePyme;
	}


	public String getNombrePyme() {
		return nombrePyme;
	}


	public void setClavePyme(String clavePyme) {
		this.clavePyme = clavePyme;
	}


	public String getClavePyme() {
		return clavePyme;
	}



}