package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 *
 * Catalogo de Catalogos N@E de la Pantalla de Parametrizacion.
 * PANTALLA: ADMIN NAFIN - ADMINISTRACION - PARAMETRIZACION - CATALOGOS
 *
 * Ejemplo de uso:
 * CatalogosParametrizacion cat = new CatalogosParametrizacion();
 * cat.setPerfil("PARAMCATEPO");
 *
 * @author Salim Hernandez
 * @since July 1, 2011
 *
 */
public class CatalogosParametrizacion extends CatalogoAbstract {
	
	//Variable para enviar mensajes al log. 
	private 	static Log 		log 						= ServiceLocator.getInstance().getLog(CatalogosParametrizacion.class);
	
	private 	String 			perfil 					= "";
	private 	boolean 			catalogoHabilitado	= false;
	private 	boolean 			perfilHabilitado		= false;
	private	StringBuffer 	tablas 					= new StringBuffer();
	
	public CatalogosParametrizacion() {
		
		super();
		super.setPrefijoTablaPrincipal("catalogos."); // from COMCAT_CATALOGOS_PARAM  CATALOGOS
		this.catalogoHabilitado  	= true;
		this.perfilHabilitado		= true;
		super.setCampoClave("ic_catalogos_param");
		super.setCampoDescripcion("cg_descripcion");
		
	}
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colección de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		
		log.debug("getListaElementos (E)");
		
		List coleccionElementos = new ArrayList();

		try{
			
			// Validar que se haya especificado el campo clave
			if(
					super.getCampoClave()       == null || super.getCampoClave().trim().equals("") 
			){
				throw new AppException("El campo clave no ha sido establecido.");
			}
			
			// Validar que se haya especificado el campo descripcion
			if(
					super.getCampoDescripcion() == null || super.getCampoDescripcion().trim().equals("")
			){
				throw new Exception("El campo descripción no ha sido establecido.");
			}
			
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer hint = new StringBuffer();
		hint.append(" /*+ USE_NL(CATALOGOS REL_CAT_PERFIL) */ " );
	
		tablas.append(
			"	COMCAT_CATALOGOS_PARAM CATALOGOS,     " +
			"	COMREL_CATALOGOS_PARAM REL_CAT_PERFIL "
		);

		StringBuffer condicionJoin = new StringBuffer();		
		condicionJoin.append(
			" CATALOGOS.IC_CATALOGOS_PARAM  = REL_CAT_PERFIL.IC_CATALOGOS_PARAM "
		);

		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
		
		StringBuffer qrySQL = new StringBuffer();
		qrySQL.append(
			" SELECT DISTINCT " + hint + 
			" " + super.getCampoClave()       + " AS CLAVE,      " +
			" " + super.getCampoDescripcion() + " AS DESCRIPCION " +
			" FROM  " + tablas +
			" WHERE " + condicionJoin
		);
 	
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y además se manda a llamar la implementación especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}
	
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY DESCRIPCION " );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		log.debug("qrySQL = <"+qrySQL+">");
		log.debug("vars   = <"+super.getValoresBindCondicion()+">");
		
		// Consultar Catalogo
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		// Elaborar Catalogo
		while(registros.next()) {
			ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
			elementoCatalogo.setClave(registros.getString("CLAVE"));
			elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
			coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
		
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		
		String condicionAdicional = "";
		
		try{
			if (this.perfil == null){
				throw new Exception("El parametro requerido: \"perfil\", no ha sido establecido.");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
		
		condicionAdicional = 
			"  CATALOGOS.CS_ACTIVO                       = ? AND "  + // Estatus del Catalogo
			"	REL_CAT_PERFIL.C_PERFIL_CATALOGOS_PARAM 	= ? AND "  + // Clave del Perfil
			"	REL_CAT_PERFIL.CS_ACTIVO 						= ? ";
 
		super.addCondicionAdicional(condicionAdicional);
		
		//agregar a variables bind estos valores
		super.addVariablesBind(this.catalogoHabilitado?"S":"N");
		super.addVariablesBind(this.perfil);
		super.addVariablesBind(this.perfilHabilitado  ?"S":"N");

	}

	/**
    * Clave del <tt>perfil</tt> de la pantalla para la cual se consultaran sus catalogos
	 */
	public String getPerfil(){
		return this.perfil;
	}

	/**
	 * Especifica el <tt>perfil</tt> de la pantalla para la cual se consultaran sus catalogos
	 * asociados. Este parametro es obligatorio.
	 *
	 * @param perfil <tt>String</tt> con la clave del perfil(pantalla). Este campo corresponde 
	 *                al campo (SEG_PERFIL.C_PERFIL_CATALOGOS_PARAM). 
	 */
	public void setPerfil(String perfil){
		this.perfil = ( perfil == null || perfil.trim().equals("") )?null:perfil;
	}

	/**
	 * Estatus de consulta de los catalogos
	 */
	public boolean getCatalogoHabilitado(){
		return this.catalogoHabilitado;
	}

	/**
	 * Especifica el Estatus de los catalogos que traera la consulta.  Por default este valor viene en <tt>true</tt>.
	 *
	 * @param catalogoHabilitado <tt>boolean</tt> con valor <tt>true</tt> para los catalogos activos
	 *                   y <tt>false</tt> para los catalogos inactivos.
	 *
	 */
	public void setCatalogoHabilitado(boolean catalogoHabilitado){
		this.catalogoHabilitado = catalogoHabilitado;
	}
	
	/**
	 * Estatus del perfil
	 */
	public boolean getPerfilHabilitado(){
		return this.perfilHabilitado;
	}

	/**
	 * Especifica el Estatus del perfil. Por default este valor viene en <tt>true</tt>.
	 *
	 * @param perfilHabilitado <tt>boolean</tt> con valor <tt>true</tt> para los perfiles activos
	 *                   y <tt>false</tt> para los perfiles inactivos.
	 *
	 */
	public void setPerfilHabilitado(boolean perfilHabilitado){
		this.perfilHabilitado = perfilHabilitado;
	}
 
}