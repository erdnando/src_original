package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;


public class CatalogoOperacionFJR 
{
	private String clave;
	private String descripcion;
	private String programa;
	private String order;
  
	/** comcat_oper_fondo_jr
  *  @author Deysi Laura Hernandez Contreras
  */
	public CatalogoOperacionFJR()
	{
	}
	/**
	 * Obtiene una lista de elementos de tipo ElementoCatalogo
	 * @return regresa una colecci�n de elementos obtenidos
	 */	
	public List getListaElementos()
	{
		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		String tables, condiciones, orden;
		String qryCatOperacionJR;
		try
		{
			con.conexionDB();
			Registros regCatEstado	=	null;
			
			
			tables			= " comcat_oper_fondo_jr ";
			condiciones	= " where IC_PROGRAMA_FONDOJR  = ("+this.programa+") ";		
		
     
			orden				= " ORDER BY  DESCRIPCION" ;
			
			qryCatOperacionJR = "select ig_oper_fondo_jr AS CLAVE, cg_descripcion AS DESCRIPCION " + 
										 " FROM " + tables + condiciones +orden;
      
      orden				= " ORDER BY  DESCRIPCION" ;
        
				
			System.out.println("comcat_oper_fondo_jr::::::::::"+qryCatOperacionJR);
			regCatEstado = con.consultarDB(qryCatOperacionJR);
			while(regCatEstado.next()) {
					ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					elementoCatalogo.setClave(regCatEstado.getString("CLAVE"));
					elementoCatalogo.setDescripcion(regCatEstado.getString("DESCRIPCION"));
					coleccionElementos.add(elementoCatalogo);
			}
		
		
		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
				
		return coleccionElementos;													
		
	}
	
	

	public String getClave()
	{
		return clave;
	}
	public String getDescripcion()
	{
		return descripcion;
	}
	public String getPrograma()
	{
		return programa;
	}


	
/**
* Establece la clave(id) que se va obtener en la consulta
*/	
	public void setClave(String clave)
	{
		this.clave = clave;		
	}

/**
* Establece la descripcion  que se va obtener en la consulta
*/		
	public void setDescripcion(String descripcion)
	{
		this.descripcion = descripcion;
	}

/**
* Establece valor para indicar algun valor en especifico a consultar
*/	
	public void setPrograma(String programa)
	{
		this.programa = programa;
	}
/**
* Establece el campo por el cual se va a ordenar
*/	
	public void setOrden(String order)
	{
		this.order = order;
	}

	
}