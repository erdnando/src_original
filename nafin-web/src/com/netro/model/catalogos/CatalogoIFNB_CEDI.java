package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;


public class CatalogoIFNB_CEDI extends CatalogoAbstract {
	
	private String estatus; 
	private String ejecutivoAsignado;
	  
  
	/** 
  *  @author Deysi Laura Hernandez Contreras
  */
	public CatalogoIFNB_CEDI() {  	}
	/**
	 * Obtiene una lista de elementos de tipo ElementoCatalogo
	 * @return regresa una colecci�n de elementos obtenidos
	 */	
	public List getListaElementos() 	{
		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		
	   StringBuffer qrySQL = new StringBuffer();
		try
		{
			con.conexionDB();
			Registros reg	=	null;
			
			
		  qrySQL.append( " SELECT DISTINCT a." + getCampoClave() + " AS CLAVE, " + 
		   " a." + getCampoDescripcion()+ " AS DESCRIPCION "+
		   " FROM  CEDI_IFNB_ASPIRANTE a  ,  CEDI_SOLICITUD  s "+
		   " WHERE  a.IC_IFNB_ASPIRANTE  = s.IC_IFNB_ASPIRANTE "+
			" AND  s.IC_ESTATUS_SOLICITUD = " + getEstatus()  +
		   " AND  s.cg_usuario_asignado = " + getEjecutivoAsignado()  );
			
			System.out.println("qrySQL==== "+qrySQL); 
			
			reg = con.consultarDB(qrySQL.toString());
			while(reg.next()) {
					ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					elementoCatalogo.setClave(reg.getString("CLAVE"));
					elementoCatalogo.setDescripcion(reg.getString("DESCRIPCION"));
					coleccionElementos.add(elementoCatalogo);
			}		
		
		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
				
		return coleccionElementos;													
		
	}
	

	public String getEstatus() 	{
		return estatus;
	}
	

	public void setEstatus(String estatus) 	{
		this.estatus = estatus;
	}

	public void setEjecutivoAsignado(String ejecutivoAsignado) {
		this.ejecutivoAsignado = ejecutivoAsignado;
	}

	public String getEjecutivoAsignado() {
		return ejecutivoAsignado;
	}
	
}