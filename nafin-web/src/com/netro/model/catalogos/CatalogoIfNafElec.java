package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


/**
 * Catalogo de IF con numero de nafin electronico como clave (comcat_if)
 *
 * Ejemplo de uso:
 * CatalogoIfNafElec cat = new CatalogoIfNafElec();
 * cat.setCampoClave("cn.ic_nafin_electronico");
 * cat.setCampoDescripcion("ci.cg_razon_social");
 * cat.setTipoBanco("B");
 * cat.setOrden("cg_razon_social");
 *
 * @author Fabian Valenzuela
 */
public class CatalogoIfNafElec extends CatalogoAbstract {
	
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoIfNafElec.class);
	
	private String tipoBanco = "";
		
	StringBuffer tablas = new StringBuffer();
	
	public CatalogoIfNafElec() {
		super();
		super.setPrefijoTablaPrincipal(""); //from comcat_if
	}
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colección de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer hint = new StringBuffer();
	
		tablas.append(" comcat_if ci ");
		tablas.append(" ,comrel_nafin cn ");
				
		//hint.append(" /*+ USE_NL(fi ff) */ " );

		StringBuffer condicionJoin = new StringBuffer();
		
		condicionJoin.append(" ci.ic_if = cn.ic_epo_pyme_if ");

		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
		
		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(
				" SELECT " + hint + " DISTINCT "  + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
				);
				
				
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y además se manda a llamar la implementación especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}
	
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		String condicionAdicional = "";
		
		
		/*
		try{
			if (this.claveEpo != null && !this.claveEpo.equals("") ){
				Integer.parseInt(this.claveEpo);
				//throw new Exception("Los parametros requeridos no has sido establecidos");
			}
			
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
		*/
		condicionAdicional +=" cn.cg_tipo = ? ";
		condicionAdicional +=" AND ci.cs_habilitado = ? ";
		
		if(tipoBanco!=null && !tipoBanco.equals("") && (tipoBanco.equals("B") || tipoBanco.equals("NB")) ){
			condicionAdicional +=" AND ci.cs_tipo = ? ";
		}
		
	
		if(condicionAdicional.length()>0)
			super.addCondicionAdicional(condicionAdicional);
		
		//agregar a variables bind estos valores
		super.addVariablesBind("I");
		super.addVariablesBind("S");
		if(tipoBanco!=null && !tipoBanco.equals("") && (tipoBanco.equals("B") || tipoBanco.equals("NB")) ){
			super.addVariablesBind(this.tipoBanco);
		}
			
			
			
	}


	public void setTipoBanco(String tipoBanco) {
		this.tipoBanco = tipoBanco;
	}


	public String getTipoBanco() {
		return tipoBanco;
	}


	



}