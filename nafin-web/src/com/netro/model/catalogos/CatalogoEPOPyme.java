package com.netro.model.catalogos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


/**
 * Catalogo de Epos para Pyme (comcat_epo)
 *
 * Ejemplo de uso:
 * CatalogoEPOPyme cat = new CatalogoEPOPyme();
 * cat.setCampoClave("ic_epo");
 * cat.setCampoDescripcion("cg_razon_social");
 * cat.setClavePyme("1");
 * cat.setAceptacion("");
 * cat.setValoresCondicionNotIn("?,?", java.lang.Integer.class); //En caso de requerirse (Opcional)
 * cat.setOrden("cg_razon_social");
 *
 * @author Hugo Vargas
 */
public class CatalogoEPOPyme extends CatalogoAbstract {
	
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoEPOPyme.class);
	
	StringBuffer tablas = new StringBuffer();
  private String clavePyme;
  private String operaNotasCredito;
  private String aceptacion;
  private String tipoFactoraje;
  private String producto;
  private String paramEpo;
  
	
	public CatalogoEPOPyme() {
		super();
		super.setPrefijoTablaPrincipal("e."); //from comcat_epo e
	}
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colección de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer hint = new StringBuffer();
	
		tablas.append(" comcat_epo E, comrel_pyme_epo PE " );
    if (this.operaNotasCredito != null && !this.operaNotasCredito.equals("")) {
      tablas.append(",comrel_producto_epo PRE ");
    }
	 
	 if ( this.tipoFactoraje!=null && !this.tipoFactoraje.equals("") ) {//se agrega por F017-2011 FVR
		 tablas.append(" , comrel_producto_epo cpe ");
	 }
	 
	 if ( this.paramEpo!=null && !this.paramEpo.equals("") ) {//se agrega por F017-2011 FVR
		tablas.append(" , com_parametrizacion_epo ppe  ");
	 }
	 

		//hint.append(" /*+ USE_NL(ie e param) */ " );

		StringBuffer condicionJoin = new StringBuffer();
		
		condicionJoin.append(
				" pe.ic_epo = e.ic_epo ");

    if (this.operaNotasCredito != null && !this.operaNotasCredito.equals("")) {
      condicionJoin.append(" AND e.ic_epo = pre.ic_epo ");
    }
	 
	 if ( this.paramEpo!=null && !this.paramEpo.equals("") ) {//se agrega por F017-2011 FVR
		condicionJoin.append(" AND pe.ic_epo = ppe.IC_EPO ");
	 }

		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
		
// AQUI VA EL QUERY PRINCIPAL

//Para obtener los NOT IN
		AccesoDB con = new AccesoDB();
		String condicionContrato = "";
		try{
			con.conexionDB();
			String strSQLContrato = 
				" SELECT pe.ic_epo, 'CatalogoEpo.java' as query " +
				" FROM  " +
				" comrel_pyme_epo pe,  " +
				" ( " +
				" SELECT ic_epo, MAX(ic_consecutivo) as ic_consecutivo " +
				" FROM com_contrato_epo ce " +
				" WHERE cs_mostrar= ? " +
				" GROUP BY ic_epo " +
				" ) contrato_epo " +
				" WHERE pe.ic_pyme = ? " +
				" AND pe.ic_epo = contrato_epo.ic_epo " +
				" AND NOT EXISTS ( " +
				"          SELECT ic_epo, ic_consecutivo " +
				"          FROM com_aceptacion_contrato ac " +
				"          WHERE " +
				"          ac.ic_pyme = ? " +
				" 	        AND ac.ic_epo = contrato_epo.ic_epo " +
				"          AND ac.ic_consecutivo = contrato_epo.ic_consecutivo " +
				"          ) ";

			PreparedStatement ps = con.queryPrecompilado(strSQLContrato);
			//ps.setInt(1, Integer.parseInt(claveEPOEntregaAnticipadaVivienda));

			StringBuffer eposNotIn = new StringBuffer();
			ps.setString(1, "S");
			ps.setInt(2, Integer.parseInt(clavePyme));
			ps.setInt(3, Integer.parseInt(clavePyme));
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				eposNotIn.append(rs.getString("ic_epo") + ",");
			}
			rs.close();
			ps.close();

			List lEpos = null;
			if (eposNotIn.length() > 0) {
				eposNotIn.deleteCharAt(eposNotIn.length()-1);
				lEpos = Comunes.explode("," , eposNotIn.toString(), java.lang.Integer.class);
				if (lEpos != null && !lEpos.isEmpty() ) {
					if (super.getValoresCondicionNotIn() != null) {
						super.getValoresCondicionNotIn().addAll(lEpos);
					} else {
						super.setValoresCondicionNotIn(lEpos);
					}
				}
			}
		}catch (Exception e){
			throw new AppException("Error al obtener contratos pendientes de autorizar. ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}

		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(
				" SELECT " + hint + " " + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
				);
		
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y además se manda a llamar la implementación especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}
	
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		//AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		StringBuffer condicionAdicional = new StringBuffer("");
		
		try{
			if (this.clavePyme == null || this.clavePyme.equals("")){
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
			Integer.parseInt(this.clavePyme);
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
		
		condicionAdicional.append(
				" PE.ic_pyme=? " + 
				" AND E.cs_habilitado=? " +
				" AND PE.cs_habilitado=? "
		);
		
		if ( this.tipoFactoraje!=null && !this.tipoFactoraje.equals("") ) {//se agrega por F017-2011 FVR
			condicionAdicional.append(
					" AND cpe.ic_epo = E.ic_epo "+
					" AND cpe.ic_producto_nafin = ? ");
		}
    
		super.addCondicionAdicional(condicionAdicional.toString());
		//agregar a variables bind estos valores
		super.addVariablesBind(new Integer(this.clavePyme));
		super.addVariablesBind("S");
		super.addVariablesBind("S");
		if ( this.tipoFactoraje!=null && !this.tipoFactoraje.equals("") ) {//se agrega por F017-2011 FVR
			super.addVariablesBind(new Integer(1)); //1 = Descuento Electronico
		}
		
		if (this.aceptacion != null && !this.aceptacion.equals("")) {
			condicionAdicional = new StringBuffer();
			condicionAdicional.append(" PE.cs_aceptacion = ? ");
			super.addCondicionAdicional(condicionAdicional.toString());
			super.addVariablesBind(this.aceptacion);
		}
		if (this.operaNotasCredito != null && !this.operaNotasCredito.equals("")) {
			condicionAdicional = new StringBuffer();
			condicionAdicional.append(	
					" PRE.ic_producto_nafin = ? " +
					" and nvl(PRE.cs_opera_notas_cred,?) = ? ");
			super.addCondicionAdicional(condicionAdicional.toString());
			super.addVariablesBind(new Integer(1));
			super.addVariablesBind("N");
			super.addVariablesBind(this.operaNotasCredito);
		}
		
		if ( this.tipoFactoraje!=null && !this.tipoFactoraje.equals("") ) {
			if (tipoFactoraje.equals("V")) {
				condicionAdicional = new StringBuffer();
				condicionAdicional.append(" cpe.cs_factoraje_vencido = ? ");
				super.addCondicionAdicional(condicionAdicional.toString());
				super.addVariablesBind("S");
			} else if (tipoFactoraje.equals("D")) {
				condicionAdicional = new StringBuffer();
				condicionAdicional.append(" cpe.cs_factoraje_distribuido=? ");
				super.addCondicionAdicional(condicionAdicional.toString());
				super.addVariablesBind("S");
			}
		}
		
		if ( this.paramEpo!=null && !this.paramEpo.equals("") ) {//se agrega por F017-2011 FVR
			condicionAdicional = new StringBuffer();
			condicionAdicional.append("  ppe.CC_PARAMETRO_EPO = ? ");
			condicionAdicional.append(" AND ppe.CG_VALOR = ? ");
			
			super.addCondicionAdicional(condicionAdicional.toString());
			super.addVariablesBind("PUB_EPO_OPERA_MANDATO");
			super.addVariablesBind("S");
		}
		
	}

	public void setClavePyme(String clavePyme) {
		this.clavePyme = clavePyme;
	}
	
	public void setOperaNotasCredito(String operaNotasCredito) {
		this.operaNotasCredito = operaNotasCredito;
	}
	
	public void setAceptacion(String aceptacion) {
		this.aceptacion = aceptacion;
	}


	public void setTipoFactoraje(String tipoFactoraje) {
		this.tipoFactoraje = tipoFactoraje;
	}


	public String getTipoFactoraje() {
		return tipoFactoraje;
	}


	public void setParamEpo(String paramEpo) {
		this.paramEpo = paramEpo;
	}


	public String getParamEpo() {
		return paramEpo;
	}

}