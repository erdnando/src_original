package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.ElementoCatalogo;


/**
 * La clase es una plantilla para la generaci�n de Catalogos.
 *
 * @author Gilberto Aparicio
 */
public abstract class CatalogoAbstract implements Catalogo {
	private String campoClave;
	private String campoDescripcion;
	private String orden;
	private Collection valoresCondicionIn;
	private Collection valoresCondicionNotIn;
	private String campoLlave;
	private List valoresBindCondicion = new ArrayList();
	private StringBuffer condicion = new StringBuffer();
	private String prefijoTablaPrincipal;

	public CatalogoAbstract() {
	}

	/**
	 * Establece el nombre del campo que representar� la clave
	 * a obtener en la consulta.
	 * @param campoClave Campo de la clave
	 */
	public void setCampoClave(String campoClave) {
		if (this.getPrefijoTablaPrincipal() != null && !this.getPrefijoTablaPrincipal().equals("") &&
				campoClave.trim().indexOf(this.getPrefijoTablaPrincipal()) !=0 ) {
			//Si hay prefijo y no esta especificado ya en el parametro campoClave, le agrega el prefijo
			this.campoClave = this.getPrefijoTablaPrincipal() + campoClave;
		} else {
			this.campoClave = campoClave;
		}
	}

	/**
	 * Obtiene el nombre del campo que representar� la clave
	 * a obtener en la consulta.
	 * @return Cadena con el nombre del campo de la clave
	 */
	public String getCampoClave() {
		return this.campoClave;
	}


	/**
	 * Establece el nombre del campo que representar� la descripci�n
	 * a obtener en la consulta
	 * @param campoDescripcion Campo de la descripcion
	 */
	public void setCampoDescripcion(String campoDescripcion) {
		if ( this.getPrefijoTablaPrincipal() != null && !this.getPrefijoTablaPrincipal().equals("") &&
				campoDescripcion.trim().indexOf(this.getPrefijoTablaPrincipal()) != 0 ) {
			this.campoDescripcion = this.getPrefijoTablaPrincipal() + campoDescripcion;
		} else {
			this.campoDescripcion = campoDescripcion;
		}
	}

	/**
	 * Obtiene el nombre del campo que representar� la descripci�n
	 * a obtener en la consulta
	 * @return Cadena con el campo de la descripcion
	 */
	public String getCampoDescripcion() {
		return this.campoDescripcion;
	}


	/**
	 * Establece el campo o campos (separados por comas) del orden
	 * en el que deben ser obtenidos los registros
	 * @param orden Cadena con los criterios de ordenacion
	 */
	public void setOrden(String orden) {
		this.orden = orden;
	}

	/**
	 * Obtiene el campo o campos (separados por comas) del orden
	 * en el que deben ser obtenidos los registros
	 * @return Cadena con los criterios de ordenacion
	 */
	public String  getOrden() {
		return this.orden;
	}


	/**
	 * Establece los valores de las claves que se incluyen a trav�s de una
	 * colecci�n de objetos.
	 * @param valoresCondicionIn Colecci�n de valores.
	 */
	public void setValoresCondicionIn(Collection valoresCondicionIn) {
		this.valoresCondicionIn = valoresCondicionIn;
	}

	/**
	 * Obtiene los valores de las claves que se incluyen a trav�s de una
	 * colecci�n de objetos.
	 * @return Colecci�n de valores.
	 */
	public Collection getValoresCondicionIn() {
		return this.valoresCondicionIn;
	}



	/**
	 * Establece los valores de las claves NO se incluyen, a trav�s de una
	 * colecci�n de objetos.
	 * @param valoresCondicionIn Colecci�n de valores.
	 */
	public void setValoresCondicionNotIn(Collection valoresCondicionNotIn) {
		this.valoresCondicionNotIn = valoresCondicionNotIn;
	}

	/**
	 * Obtiene los valores de las claves que NO se incluyen, a trav�s de una
	 * colecci�n de objetos.
	 * @return Colecci�n de valores.
	 */
	public Collection  getValoresCondicionNotIn(){
		return this.valoresCondicionNotIn;
	}

	/**
	 * Permite establecer el nombre del campo de la llave primaria,
	 * cuando esta no concuerda con la especificada usando
	 * CatalogoAbstract.setCampoClave(String)
	 * @param campoLlave Nombre del campo llave
	 */
	public void setCampoLlave(String campoLlave) {
		this.campoLlave = campoLlave;
	}


	/**
	 * Obtiene el nombre del campo de la llave primaria,
	 * cuando esta no concuerda con la especificada usando
	 * setCampoClave(String)
	 * @return Nombre del campo llave
	 */
	public String getCampoLlave() {
		return this.campoLlave;
	}


	/**
	 * Genera las condiciones de acuerdo a si existen valores en las
	 * restriciones como In o NotIn y manda a llamar a la implementaci�n
	 * espec�fica de condiciones setCondicionesAdicionales()
	 */
	protected void setCondicion() {
		String nombreCampo = (this.getCampoLlave() == null || this.getCampoLlave().equals(""))?
				this.getCampoClave():this.getCampoLlave();

		if (this.getValoresCondicionIn() != null && !this.getValoresCondicionIn().isEmpty()) {

			condicion.append(
				nombreCampo + " IN (" +
				Comunes.repetirCadenaConSeparador("?", ",", this.getValoresCondicionIn().size()) + ") "
			);
			this.addVariablesBind(this.getValoresCondicionIn());
		}

		if (this.getValoresCondicionNotIn() != null && !this.getValoresCondicionNotIn().isEmpty()) {
			//Si ya existe una condicion se agrega el operador AND
			if (condicion.length() > 0) {
				condicion.append(" AND ");
			}

			condicion.append(
					nombreCampo + " NOT IN (" +
					Comunes.repetirCadenaConSeparador("?", ",", this.getValoresCondicionNotIn().size()) + ") "
			);
			this.addVariablesBind(this.getValoresCondicionNotIn());
		}

		this.setCondicionesAdicionales();
	}


	/**
	 * Establece las variables bind para la consulta.
	 * Puede ser una lista de valores u otro objeto individual como Integer,
	 * Long, Float, Double, BigDecimal, BigInteger, String, etc.
	 *
	 * @param objeto Objeto a utilizar como valor de variable bind
	 */
	protected void addVariablesBind(Object objeto) {
		if (objeto instanceof java.util.List) {
			this.valoresBindCondicion.addAll((List)objeto);
		} else {
			this.valoresBindCondicion.add(objeto);
		}
	}


	/**
	 * Establece condiciones adicionales. Esta implementacion est� vac�a
	 * Las clases que extiendan esta CatalogoSimple, deben sobreescribirla
	 * mandando a llamar addCondicionAdicional(String) para poner la
	 * cadena de SQL que contenga la condicion y
	 * addVariablesBind(Object) para establecer sus valores respectivos
	 */
	protected void setCondicionesAdicionales() {
	}


	/**
	 * Establece los valores de las claves que unicamente se deben contemplar.
	 * Este m�todo es de conveniencia, para cuando la llave primaria es simple
	 * y en una cadena se pueden especificar los valores separados por coma.

	 *
	 * @param cadenaValoresCondicionIn Cadena con los valores separados por coma.
	 * @param clase Clase a la que ser�n convertidos cada unos de los elementos
	 */
	public void setValoresCondicionIn(String cadenaValoresCondicionIn, Class clase) {
		//La sig. instruccion es para partir la cadena, por ejemplo "1,54"
		//y si clase=java.lang.Long genera una lista con los elementos
		//new Long("1") y new Long("54")
		Collection listaIn = Comunes.explode(",", cadenaValoresCondicionIn, clase);
		this.setValoresCondicionIn(listaIn);
	}

	/**
	 * Establece los valores de las claves que no se incluyen.
	 * Este m�todo es de conveniencia, para cuando la llave primaria es simple
	 * y en una cadena se pueden especificar los valores separados por coma.
	 *
	 * @param cadenaValoresCondicionNotIn Cadena con los valores separados por coma.
	 * @param clase Clase a la que ser�n convertidos cada unos de los elementos
	 */
	public void setValoresCondicionNotIn(String cadenaValoresCondicionNotIn, Class clase) {
		//La sig. instruccion es para partir la cadena, por ejemplo "1,54"
		//y si clase=java.lang.Long genera una lista con los elementos
		//new Long("1") y new Long("54")
		Collection listaNotIn = Comunes.explode(",", cadenaValoresCondicionNotIn, clase);
		this.setValoresCondicionNotIn(listaNotIn);
	}


	/**
	 * Agrega una cadena SQL, con condiciones adicionales a las de IN/NOT IN
	 *
	 * @param cadena Condicion SQL
	 */
	protected void addCondicionAdicional(String cadena){
			if (this.condicion.length() > 0) {
				this.condicion.append(" AND ");
			}
			this.condicion.append(cadena);
	}


	/**
	 * Obtiene la cadena en donde se colocan las condiciones.
	 * @return Cadena con las condiciones
	 */
	protected StringBuffer getCondicion() {
		return this.condicion;
	}

	/**
	 * Obtiene la lista en donde se coloca los valores bind requeridas en las
	 * condiciones
	 * @return
	 */
	public List getValoresBindCondicion() {
		return this.valoresBindCondicion;
	}


	/**
	 * Obtiene la lista de elementos llamando getListaElementos
	 * y genera un XML.
	 * (Para usar este m�todo la lista debe tener elementos
	 * netropology.utilerias.ElementoCatalogo)
	 * @return Cadena con el XML de los elementos
	 */
	public String getXMLElementos() throws AppException {
		return getXMLElementos("");
	}

	/**
	 * Obtiene la lista llamando getListaElementos y genera un XML de los
	 * elementos, y coloca un atributo para marcar como seleccionado
	 * predeterminado a la clave especificada.
	 * (Para usar este m�todo la lista debe tener elementos
	 * netropology.utilerias.ElementoCatalogo)
	 * @return Cadena con el XML de los elementos
	 */
	public String getXMLElementos(String claveSeleccionada) throws AppException {
		List elementos = this.getListaElementos();
		StringBuffer sb =  new StringBuffer("<options>" + "\n");

		if (claveSeleccionada == null) {
			claveSeleccionada = "";
		}

		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				sb.append("<option>" + "\n");
				sb.append("<option_value" + ((claveSeleccionada.equals(ec.getClave()))?" selected='selected'":"") + ">" + ec.getClave() + "</option_value>" + "\n");
				sb.append("<option_text>" + ec.getDescripcion() + "</option_text>" + "\n");
				sb.append("</option>" + "\n");
			}
		}
		sb.append("</options>" + "\n");
		return sb.toString();
	}

	/**
	 * Obtiene la lista llamando getListaElementos y genera un JSON de los
	 * elementos.
	 * (Para usar este m�todo la lista debe tener elementos
	 * netropology.utilerias.ElementoCatalogo)
	 * @return Cadena con el JSON de los elementos
	 */
	public String getJSONElementos() throws AppException {
		List elementos = this.getListaElementos();
		JSONArray jsonArr = new JSONArray();
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		return "{\"success\": true, \"total\": \"" + 
				jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";
	}
	
	/**
	 * Obtiene la lista llamando <tt>getListaElementos</tt> y genera un <tt>JSONArray</tt> de los
	 * elementos.
	 * (Para usar este m&eacute;todo la lista debe tener elementos
	 * <tt>netropology.utilerias.ElementoCatalogo</tt>)
	 * @return <tt>JSONArray</tt> con los elementos del Cat&aacute;logo.
	 */
	public JSONArray getJSONArray() 
		throws AppException {
			
		List 			elementos 	= this.getListaElementos();
		JSONArray 	jsonArr 		= new JSONArray();
		Iterator 	it 			= elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		return jsonArr;
		
	}

	/**
	 * Establece el prefijo de la tabla principal, por ejemplo si un catalogo
	 * utiliza un alias de la tabla: comcat_epo CE, el prefijo ser�a CE.
	 * de manera que si se especifica setCampoClave(ic_epo) le anexe el prefijo
	 * y quede como CE.ic_epo para evitar ambig�edades
	 * @param prefijoTablaPrincipal nombre del alias de la tabla seguido de un punto
	 */
	public void setPrefijoTablaPrincipal(String prefijoTablaPrincipal) {
		this.prefijoTablaPrincipal = prefijoTablaPrincipal;
	}

	/**
	 * Obtiene el prefijo de la tabla principal
	 * @return cadena con el nombre del alias de la tabla seguido de un punto
	 */
	public String getPrefijoTablaPrincipal() {
		return prefijoTablaPrincipal;
	}
	
	/** 
	 *
	 * De la lista elementos proporcionada como parametro, genera una cadena con formato
	 * JSON y con propiedades adicionales, para que puedan ser leidas por un JSON Store.
	 * (Para usar este m�todo la lista debe tener elementos
	 * netropology.utilerias.ElementoCatalogo)
	 * @throws AppException
	 *
	 * @param elementos <tt>List</tt> con los elementos a convertir.
	 *
	 * @return Cadena con el JSON de los elementos
	 *
	 * @author jshernandez
	 * @since  F### - 2013 - ADMIN NAFIN - Migraci�n Monitor; 21/06/2013 12:45:02 p.m.
	 *
	 */
	public String getJSONElementos(List elementos) 
		throws AppException {
		JSONArray 	jsonArr	= new JSONArray();
		Iterator 	it			= elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		return "{\"success\": true, \"total\": \"" + 
				jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";
	}

	public abstract List getListaElementos() throws AppException;
	
}