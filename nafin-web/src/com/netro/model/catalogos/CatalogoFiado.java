package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


/**
 * Catalogo de Fiados (fe_afianzadoras)
 *
 * Ejemplo de uso:
 * CatalogoFiado cat = new CatalogoFiado();
 * cat.setCampoClave("ic_fiado");
 * cat.setCampoDescripcion("cg_nombre");
 * cat.setClaveEpo("1");
 * cat.setOrden("cg_razon_social");
 *
 * @author Fabian Valenzuela
 */
public class CatalogoFiado extends CatalogoAbstract {
	
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoFiado.class);
	
	private String claveEpo = "";
	private String claveAfianza = "";
	private String rfcFiado = "";
	private String nombreFiado = "";
	
	StringBuffer tablas = new StringBuffer();
	
	public CatalogoFiado() {
		super();
		super.setPrefijoTablaPrincipal("fi."); //from fe_fiados
	}
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colección de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer hint = new StringBuffer();
	
		tablas.append(" fe_fiado fi ");
		tablas.append(" ,fe_fianza ff ");
				
		hint.append(" /*+ USE_NL(fi ff) */ " );

		StringBuffer condicionJoin = new StringBuffer();
		
		condicionJoin.append(" fi.ic_fiado = ff.ic_fiado ");

		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
		
		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(
				" SELECT " + hint + " DISTINCT "  + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
				);
				
				
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y además se manda a llamar la implementación especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}
	
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		String condicionAdicional = "";
		boolean and = false;
		
		try{
			if (this.claveEpo != null && !this.claveEpo.equals("") ){
				Integer.parseInt(this.claveEpo);
				//throw new Exception("Los parametros requeridos no has sido establecidos");
			}
			if (this.claveAfianza != null && !this.claveAfianza.equals("") ){
				Integer.parseInt(this.claveAfianza);
				//throw new Exception("Los parametros requeridos no has sido establecidos");
			}
			
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
		
			if(claveEpo!=null && !claveEpo.equals("")){
				condicionAdicional +=(and?" AND ":"")+" ff.ic_epo = ? ";
				and = true;
			}
			if(claveAfianza!=null && !claveAfianza.equals("")){
				condicionAdicional +=(and?" AND ":"")+" ff.ic_afianzadora = ? ";
				and = true;
			}
			if(nombreFiado!=null && !nombreFiado.equals("")){
				condicionAdicional += (and?" AND ":"")+" fi.cg_nombre like ? ";
				and = true;
			}
			if(rfcFiado!=null && !rfcFiado.equals("")){
				condicionAdicional += (and?" AND ":"")+" fi.cg_rfc like ? ";
				and = true;
			}
		
			if(condicionAdicional.length()>0)
				super.addCondicionAdicional(condicionAdicional);
			
			//agregar a variables bind estos valores
			if(claveEpo!=null && !claveEpo.equals(""))
				super.addVariablesBind(new Integer(this.claveEpo));
			if(claveAfianza!=null && !claveAfianza.equals(""))
				super.addVariablesBind(new Integer(this.claveAfianza));
			if(nombreFiado!=null && !nombreFiado.equals(""))
				super.addVariablesBind((this.nombreFiado).replaceAll("[*]","%").toUpperCase());
			if(rfcFiado!=null && !rfcFiado.equals(""))
				//super.addVariablesBind(new Integer(this.rfcFiado.toUpperCase()));
				super.addVariablesBind((this.rfcFiado).replaceAll("[*]","%").toUpperCase());
			
			
	}


	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}


	public String getClaveEpo() {
		return claveEpo;
	}


	public void setRfcFiado(String rfcFiado) {
		this.rfcFiado = rfcFiado;
	}


	public String getRfcFiado() {
		return rfcFiado;
	}


	public void setNombreFiado(String nombreFiado) {
		this.nombreFiado = nombreFiado;
	}


	public String getNombreFiado() {
		return nombreFiado;
	}


	public void setClaveAfianza(String claveAfianza) {
		this.claveAfianza = claveAfianza;
	}


	public String getClaveAfianza() {
		return claveAfianza;
	}



}