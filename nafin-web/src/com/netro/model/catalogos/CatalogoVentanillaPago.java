package com.netro.model.catalogos;

import netropology.utilerias.AppException;

public class CatalogoVentanillaPago extends CatalogoSimple {
	private String claveEpo;

	/**
	 * Constructor de la clase
	 */	
	public CatalogoVentanillaPago() {
		super.setTabla("cdercat_ventanilla_pago");
	}

	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}
	
	protected void setCondicionesAdicionales () {
		try {
			if (this.claveEpo == null || this.claveEpo.equals("")) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		} catch (Exception e) {
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
		super.addCondicionAdicional("ic_epo = ?");
		super.addVariablesBind(new Integer(claveEpo));
	}
}