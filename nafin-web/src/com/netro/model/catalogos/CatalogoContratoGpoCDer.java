package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import org.apache.commons.logging.Log;


/**
 * Catalogo de EPO's que pertencen a un Grupo de Cesion de Derechos
 *
 * Ejemplo de uso:
 * CatalogoContratoGpoCDer cat = new CatalogoContratoGpoCDer();
 * cat.setCampoClave("ic_epo");
 * cat.setCampoDescripcion("cg_razon_social");
 * cat.setCsPymePrincipal("S"); //opcional: indica que solo regresara las EPOs de los grupos que pertencen a la pyme principal
 * cat.setCsSolicAsociada("S"); //opcional: indica que solo regresara las EPOs de los grupos que ya tienen asociada una solicitud
 * cat.setOrden("cg_razon_social");
 *
 * @author Fabian Valenzuela
 */
public class CatalogoContratoGpoCDer extends CatalogoAbstract {

	//Variable para enviar mensajes al log.
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoContratoGpoCDer.class);

	private String csPymePrincipal = "";
	private String csSolicAsociada = "";
	private String cvePyme = "";
	private String cveEpo = "";

	StringBuffer tablas = new StringBuffer();

	public CatalogoContratoGpoCDer() {
		super();
		super.setPrefijoTablaPrincipal("cg.");
	}

	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colección de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();

		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer hint = new StringBuffer();

		tablas.append(" cder_grupo cg ");
		tablas.append(" ,cderrel_pyme_x_grupo cpg  ");
		tablas.append(" ,cder_solicitud cs ");

		StringBuffer condicionJoin = new StringBuffer();

		condicionJoin.append("  cg.ic_grupo_cesion = cpg.ic_grupo_cesion  ");
		condicionJoin.append(" AND cg.ic_grupo_cesion = cpg.ic_grupo_cesion  ");
		if(!"S".equals(csSolicAsociada)) {
			condicionJoin.append(" AND cg.ic_grupo_cesion = cs.ic_grupo_cesion(+)  ");
		}else{
			condicionJoin.append(" AND cg.ic_grupo_cesion = cs.ic_grupo_cesion ");
		}

		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind

		StringBuffer qrySQL = new StringBuffer();

		qrySQL.append(
				" SELECT " + hint + " DISTINCT "  + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
				);


		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para
		//IN y NOT IN... y además se manda a llamar la implementación especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();

		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}

		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}

		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());

		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}

		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}

		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		String condicionAdicional = "";


		condicionAdicional +=" cg.ic_epo = ? ";

		if("S".equals(csPymePrincipal)){
			condicionAdicional +=" AND cpg.cs_pyme_principal = ? ";
		}

		if(!"".equals(cvePyme)){
			condicionAdicional +=" AND cpg.ic_pyme = ? ";
		}

		if(!"S".equals(csSolicAsociada)) {
			condicionAdicional += " AND (cs.ic_grupo_cesion IS NULL OR cs.ic_estatus_solic = ? ) ";
		}

		if(condicionAdicional.length()>0)
			super.addCondicionAdicional(condicionAdicional);

		//agregar a variables bind estos valores
		super.addVariablesBind(cveEpo);

		if("S".equals(csPymePrincipal)){
			super.addVariablesBind("S");
		}

		if(!"".equals(cvePyme)){
			super.addVariablesBind(cvePyme);
		}

		if(!"S".equals(csSolicAsociada)) {
			super.addVariablesBind("24");
		}

	}

	public void setCsPymePrincipal(String csPymePrincipal) {
		this.csPymePrincipal = csPymePrincipal;
	}

	public void setCsSolicAsociada(String csSolicAsociada) {
		this.csSolicAsociada = csSolicAsociada;
	}

	public void setCvePyme(String cvePyme) {
		this.cvePyme = cvePyme;
	}

	public void setCveEpo(String cveEpo) {
		this.cveEpo = cveEpo;
	}

}