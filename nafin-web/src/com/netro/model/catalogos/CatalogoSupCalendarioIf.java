package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;
import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import org.apache.commons.logging.Log;

/**
 * Obtiene la lista de meses por a�o seleccionado en la tabla SUP_CALENDARIO_IF
 */
public class CatalogoSupCalendarioIf extends CatalogoAbstract {
	
	private static final Log LOGGER = ServiceLocator.getInstance().getLog(CatalogoSupCalendarioIf.class);
	private Integer calAnio;
	private Integer icCveSiag;
	private String usuario;
        private String esResultados;
        
    /**
     * Constructor por default
     */
	public CatalogoSupCalendarioIf() {
	}

	@Override
	public List getListaElementos() throws AppException {
		LOGGER.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		StringBuilder qrySQL = new StringBuilder();
		qrySQL.append(
		"SELECT DISTINCT A.CAL_MES AS CLAVE,  " + 
		"CASE WHEN A.CAL_MES = 1 THEN 'Enero'  " + 
		"WHEN A.CAL_MES = 2 THEN 'Febrero'  " + 
		"WHEN A.CAL_MES = 3 THEN 'Marzo'  " + 
		"WHEN A.CAL_MES = 4 THEN 'Abril'  " + 
		"WHEN A.CAL_MES = 5 THEN 'Mayo'  " + 
		"WHEN A.CAL_MES = 6 THEN 'Junio'  " + 
		"WHEN A.CAL_MES = 7 THEN 'Julio'  " + 
		"WHEN A.CAL_MES = 8 THEN 'Agosto'  " + 
		"WHEN A.CAL_MES = 9 THEN 'Septiembre'  " + 
		"WHEN A.CAL_MES = 10 THEN 'Octubre'  " + 
		"WHEN A.CAL_MES = 11 THEN 'Noviembre'  " + 
		"WHEN A.CAL_MES = 12 THEN 'Diciembre'  " + 
                "WHEN A.CAL_MES = 13 THEN '1er Semestre'  " + 
                "WHEN A.CAL_MES = 14 THEN '2do Semestre'  " + 
		"ELSE '' END AS DESCRIPCION  " + 
		"FROM SUP_CALENDARIO_IF A  " + 
		",SUP_DATOS_IF B  " + 
		"WHERE A.IC_CVE_SIAG = B.IC_CVE_SIAG AND A.CAL_ESTATUS = 'A'  "
		);
		if(null != calAnio && calAnio > 0) {
			qrySQL.append("AND A.CAL_ANIO = " + calAnio + " ");
		}
		if(null != icCveSiag && icCveSiag > 0) {
			qrySQL.append("AND B.IC_CVE_SIAG = " + icCveSiag + " ");
		}
		if(null != usuario && !"".equals(usuario)) {
			qrySQL.append("AND B.IC_CLIENTE = '" + usuario + "' ");
		}
                if(null != esResultados && "S".equals(esResultados)) {
                        qrySQL.append("AND A.cal_fec_lim_ent_r IS NOT NULL ");
                }
		qrySQL.append("ORDER BY A.CAL_MES ASC");
		LOGGER.debug("qrySQL ::: "+qrySQL);
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		while(registros.next()) {
			ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
			elementoCatalogo.setClave(registros.getString("CLAVE"));
			elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
			coleccionElementos.add(elementoCatalogo);
		}
		LOGGER.debug("getListaElementos (S)");
		return coleccionElementos;
	}


	public Integer getCalAnio() {
		return calAnio;
	}

	public void setCalAnio(Integer calAnio) {
		this.calAnio = calAnio;
	}

	public Integer getIcCveSiag() {
		return icCveSiag;
	}

	public void setIcCveSiag(Integer icCveSiag) {
		this.icCveSiag = icCveSiag;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
        
        public void setEsResultados(String esResultados) {
            this.esResultados = esResultados;
        }
        
        public String getEsResultados() {
            return esResultados;
        }
        
}