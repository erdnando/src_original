package com.netro.model.catalogos;


public class CatalogoRedInformacionMes extends CatalogoSimple{

	private String nombreTabla;
	private int anioIni;

	public CatalogoRedInformacionMes(){
		//super.setTabla(this.nombreTabla);
	}

	public void setClave(String clave){
		super.setCampoClave(clave);
	}

	public void setDescripcion(String descripcion){
		super.setCampoDescripcion(descripcion);
	}

	public void setNombreTabla(String nombreTabla){
		this.nombreTabla = nombreTabla;
	}

	public void setAnioIni(int anioIni){
		this.anioIni = anioIni;
	}

	protected void setCondicionesAdicionales() {

		String condicionAdicional = "";
		if(nombreTabla.equals("COM_CRUCE_CADENAS")){
			condicionAdicional = " SUBSTR (cg_mes, 5, 7) = ? ";
		} else if(nombreTabla.equals("COM_CRUCE_COMPRANET")){
			condicionAdicional = " TO_CHAR(df_fecha_emision,'yyyy') = ? ";
		}
		super.addCondicionAdicional(condicionAdicional);
		super.addVariablesBind(new Integer(anioIni));
	}

}
