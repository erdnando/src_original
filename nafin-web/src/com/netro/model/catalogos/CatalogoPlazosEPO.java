package com.netro.model.catalogos;

import netropology.utilerias.AppException;

/**
 *
 * Ejemplo de uso:
 * CatalogoPlazosEPO cat = new CatalogoPlazosEPO();
 * cat.setCampoClave("ic_plazo");
 * cat.setCampoDescripcion("cg_descripcion");
 * cat.setCveProducto("cveProducto");
 * cat.setCveEPO("cveEpo");
 * cat.setCveMoneda("cveMoneda");
 * List l = cat.getListaElementos();  //Lista con elementos ElementoCatalogo
 *
 * @author Jos� Alvarado
 */

public class CatalogoPlazosEPO extends CatalogoSimple {
	private String claveProducto;
	private String claveEPO;
	private String claveMoneda;
	private String cvePlazo;
	private String bandera;
	//Constructor de Clase
	public CatalogoPlazosEPO() {
	}
	
	/**
	 * Establece las condiciones adicionales necesarias para contemplar el
	 * pais y el estado al momento de obtener los municipios.
	 */
	protected void setCondicionesAdicionales() {
	
		if(bandera.equals("0")){
			//Se valida que esten los campos requeridos:
			try {
				if (getCveProducto() == null || getCveProducto().equals("") || getCveEPO() == null || getCveEPO().equals("") || getCveMoneda() == null || getCveMoneda().equals("")) {
					throw new Exception("Los parametros requeridos no has sido establecidos");
				}
			} catch (Exception e) {
				throw new AppException("Error en los parametros establecidos. " + e.getMessage());
			}
		
			String condicion = "";
			if (claveEPO.equals("TODAS")) {
				condicion = " AND tpe.ic_epo is null ";
			} else {
				condicion = " AND tpe.ic_epo = "+claveEPO ;
			}
	
			String condicionAdicional = "";
		
			condicionAdicional =	" 	ic_producto_nafin = ?" + 
										" 	AND in_plazo_inicio = ( " +
										" 		SELECT nvl(max(p.in_plazo_dias) + 1, 1)    " +
										" 		FROM comrel_tasa_producto_epo tpe, comcat_plazo p,  " +
										" 			comcat_moneda m, comcat_tasa t " +
										" 		WHERE tpe.ic_plazo = p.ic_plazo " +
										" 			AND tpe.ic_tasa = t.ic_tasa " +
										" 			AND t.ic_moneda = m.ic_moneda " +
										condicion +
										" 			AND tpe.ic_producto_nafin = ?" + 
										" 			AND t.ic_moneda = ?" + 
										" ) ";
										
										super.addVariablesBind(claveProducto);
										super.addVariablesBind(claveProducto);
										super.addVariablesBind(claveMoneda);
		
			super.addCondicionAdicional(condicionAdicional);
			
		}else{
			//Se valida que esten los campos requeridos:
			try {
				if (getCveProducto() == null || getCveProducto().equals("") || getCvePlazo() == null || getCvePlazo().equals("")) {
					throw new Exception("Los parametros requeridos no has sido establecidos");
				}
			} catch (Exception e) {
				throw new AppException("Error en los parametros establecidos. " + e.getMessage());
			}
			
			String condicionAdicional = "";
		
			condicionAdicional = 	" 	ic_producto_nafin = ?" +
											" 	AND in_plazo_inicio = ?";
											
											super.addVariablesBind(claveProducto);
											super.addVariablesBind(cvePlazo);
											
			super.addCondicionAdicional(condicionAdicional);
		}
	}
	
	/**
	 * GETTERS AND SETTERS PARA LOS ATRIBUTOS DE CLASE
	 */
	public String getCveProducto(){
		return claveProducto;
	} 
	
	public String getCveEPO(){
		return claveEPO;
	} 
	
	public String getCveMoneda(){
		return claveMoneda;
	} 
	 
	public String getCvePlazo(){
		return cvePlazo;
	} 
	 
	public void setCveProducto(String cveProd){
		this.claveProducto=cveProd;
	}
	
	public void setCveEPO(String cveEpo){
		this.claveEPO=cveEpo;
	}
	
	public void setCveMoneda(String cveMoneda){
		this.claveMoneda=cveMoneda;
	}
	
	public void setCvePlazo(String cvePlazo){
		this.cvePlazo=cvePlazo;
	}
	
	public void setBandera(String bandera){
		this.bandera= bandera;
	}
}