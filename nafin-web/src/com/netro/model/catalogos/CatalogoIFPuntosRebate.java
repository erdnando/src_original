package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Catalogo IFs Puntos Rebate
 * CatalogoIFPuntosRebate cat = new CatalogoIFPuntosRebate();
 * cat.setCampoClave("ic_if");
 * cat.setCampoDescripcion("cg_razon_social");
 * cat.setClaveEpo(iNoEPO);
 */
public class CatalogoIFPuntosRebate  extends CatalogoAbstract{
	//public CatalogoIFPuntosRebate() {}
	StringBuffer tablas = new StringBuffer();
	
	private String claveEpo = "";
	private String claveProductoNafin = "";
	private String csDisponible = "";
	private String csHabilitado = "";
	
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	 private final static Log log = ServiceLocator.getInstance().getLog(CatalogoIF.class);
	 
	 public CatalogoIFPuntosRebate() {
		super();
		super.setPrefijoTablaPrincipal("ci.");//from comcat_if ci
	 }
	 /**
	  * Obtiene una lista de elementos de tipo ElementoCatalogo
	  * @return regresa una colecci�n de elementos obtenidos
	  */
	  public List getListaElementos(){
			log.info("getListaElementos(E)");
			
			List coleccionElementos = new ArrayList();
			StringBuffer condicionJoin = new StringBuffer();
			
			StringBuffer hint = new StringBuffer();
			StringBuffer orden = new StringBuffer();
			StringBuffer qryCatIf = new StringBuffer();
			try{
				if(super.getCampoClave() == null || super.getCampoClave().equals("")||
						super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("")){
						throw new Exception("Los par�metros requeridos no han sido establecidos");
				}
			}catch(Exception e){
				throw new AppException("Error en los par�metros establecidos."+ e.getMessage());
			}
			tablas.append(" comcat_if ci, comrel_tasa_autorizada ta, comcat_tasa t, comrel_if_epo_moneda iem,comrel_tasa_base tb,com_mant_tasa mt,comrel_if_epo_x_producto ie," +
								"(SELECT   ic_tasa, MAX (dc_fecha) AS dc_fecha FROM com_mant_tasa GROUP BY ic_tasa) x," +
								"(SELECT   tb.ic_epo, tb.ic_tasa,MAX (tb.dc_fecha_tasa) AS dc_fecha_tasa FROM comrel_tasa_base tb WHERE tb.ic_epo = ? AND tb.cs_tasa_activa = ? GROUP BY tb.ic_epo, tb.ic_tasa) tbu");
			//Este es un caso especial en donde se agregan variables bind debido a que en las vistas usadas se usan parametros
			super.addVariablesBind(this.claveEpo);
			super.addVariablesBind("S");
			condicionJoin.append(
					"t.ic_tasa = x.ic_tasa" +
					" AND ta.ic_if = ci.ic_if" +
					" AND ta.ic_epo = tbu.ic_epo" +
					" AND ta.dc_fecha_tasa = tbu.dc_fecha_tasa" +
					" AND tbu.ic_tasa = t.ic_tasa" +
					" AND iem.ic_epo = ta.ic_epo" +
					" AND iem.ic_if = ta.ic_if" +
					" AND tbu.ic_tasa = tb.ic_tasa" +
					" AND tb.dc_fecha_tasa = tbu.dc_fecha_tasa" +
					" AND mt.dc_fecha = x.dc_fecha" +
					" AND t.ic_tasa = mt.ic_tasa" +
					" AND iem.ic_epo = ie.ic_epo" +
					" AND iem.ic_if = ie.ic_if"
			);
			//CONDICIONES ADICIONALES que no sean de join y que requieran
			//de un valor NO DENEN IR AQU�. Ponerlo en el m�todo de condiciones
			//adicionales para que se pueda manejar variables Bind
			StringBuffer qrySQL = new StringBuffer();
			
			qrySQL.append(
					" SELECT " + hint + " DISTINCT " + super.getCampoClave() + " AS CLAVE, " +
					super.getCampoDescripcion() + " AS DESCRIPCION " +
					" FROM " + tablas +
					" WHERE " + condicionJoin
			);
			//Genera las condiciones necesarias y las coloca en this.condicion
			//Al llamar a setCondicion se establecen las condiciones para
			//IN y NOT IN... y adem�s se manda a llamar la implementaci�n espec�fica
			//setCondicionesAdicionales de la clase
			super.setCondicion();
			
			if (super.getCondicion().length()>0) {
				qrySQL.append(" AND " + super.getCondicion());
			}
			if (super.getOrden() == null || super.getOrden().equals("")) {
				qrySQL.append(
						" ORDER BY " + super.getCampoDescripcion());
			} else {
				qrySQL.append(
						" ORDER BY " + super.getOrden());
			}
			log.debug("qrySQL ::: " + qrySQL);
			log.debug("vars ::: " + super.getValoresBindCondicion());
			
			AccesoDB con = new AccesoDB();
			Registros registros = null;
			try{
				con.conexionDB();
				registros = con.consultarDB(qrySQL.toString(),super.getValoresBindCondicion());
			} catch (Exception e){
				throw new AppException("El listado de cat�logo no pudo ser generado", e);
			}finally{
				if(con.hayConexionAbierta()){
					con.cierraConexionDB();
				}
			}
			
			while(registros.next()){
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
			}
			log.debug("getListaElementos (S)");
			return coleccionElementos;
	}
	/**
	 * Establece las condiciones adicionales de acuerdo a los par�metros establecidos.
	 * Se establecen aqu�, para poder utilizar las variables bind.
	 */
	 public void setCondicionesAdicionales(){
		String condicionAdicional = "";
		//Preguntar validaci�n claveEpo
		try{
			if(this.claveEpo==null||this.claveEpo.equals("")){
				throw new Exception("Los par�metros requeridos no han sido establecidos.");
			}
		}catch(Exception e){
			throw new AppException("Error en los par�metros establecidos." + e.getMessage());
		}
		condicionAdicional = 
				"t.cs_disponible = ?" +
				" AND ci.cs_habilitado = ?" +
				" AND ie.ic_producto_nafin = ?" +
				" AND ie.fg_puntos_rebate is not null";
		super.addVariablesBind("S");
		super.addVariablesBind("S");
		super.addVariablesBind(new Integer(1));
		super.addCondicionAdicional(condicionAdicional.toString());
		
		if(this.claveEpo != null && !this.claveEpo.equals("")){
			condicionAdicional = " ta.ic_epo = ?";
			super.addVariablesBind(new Integer(this.claveEpo));
			super.addCondicionAdicional(condicionAdicional);
		}
	 }
	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}
	public String getClaveEpo() {
		return claveEpo;
	}
	/**
	 * Establece la clave(id) que se va a poner en la consulta
	 */
	 public void setCampoClave(String clave){
		super.setCampoClave(clave);
	 }
	 /**
	  * Establece la descripci�n que se va a obtener en la consulta
	  */
	  public void setCampoDescripcion(String descripcion){
		super.setCampoDescripcion(descripcion);
	  }
}