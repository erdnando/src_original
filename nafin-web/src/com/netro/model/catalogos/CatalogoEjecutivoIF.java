package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * CLASE QUE OBTIENE LOS EJECUTIVOS  QUE OPERAN COTIZADOR
 * @author Deysi Laura Hern�ndez Contreras
 */
public class CatalogoEjecutivoIF extends CatalogoSimple {

    private String area;
    private static final Log LOG = ServiceLocator.getInstance().getLog(CatalogoEjecutivoIF.class);
       
    public CatalogoEjecutivoIF() {

    }


    /**
     * Obtiene una lista de elementos de tipo ElementoCatalogo
     * @return regresa una colecci�n de elementos obtenidos
     */
    public List getListaElementos() {
        LOG.info("getListaElementos(E)");

        List coleccionElementos = new ArrayList();
        StringBuilder qrySQL = new StringBuilder();
        StringBuilder condicionJoin = new StringBuilder();
        
        try {
            if (super.getCampoClave() == null || "".equals(super.getCampoClave()) ||
                super.getCampoDescripcion() == null || "".equals(super.getCampoDescripcion()) ) {
                throw new Exception("Los parametros requeridos no has sido establecidos");
            }
        } catch (Exception e) {
            LOG.error("Error en los parametros establecidos. " + e);
        }
        
        condicionJoin.append("  FROM cotcat_ejecutivo " +   
                            "  WHERE cs_director = 'N' " );

        qrySQL.append(" SELECT " + super.getCampoClave() + " AS CLAVE, " +
                      super.getCampoDescripcion() + " AS DESCRIPCION " + condicionJoin );

        LOG.info("getArea   "+this.getArea());
        
        
        if (this.getArea() == null ||    !"".equals(this.getArea())  ) {
            qrySQL.append(" AND  ic_area = " + this.getArea() );
        }

        if (super.getOrden() == null || "".equals(this.getOrden())   ) {
            qrySQL.append(" ORDER BY " + super.getCampoDescripcion());
        } else {
            qrySQL.append(" ORDER BY " + super.getOrden());
        }

        LOG.debug("qrySQL ::: " + qrySQL);
        LOG.debug("vars ::: " + super.getValoresBindCondicion());

        AccesoDB con = new AccesoDB();
        Registros registros = null;
        try {
            con.conexionDB();
            registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
        } catch (Exception e) {
            LOG.error("El listado de catalogo no pudo ser generado", e);
        } finally {
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }

        while (registros.next()) {
            ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
            elementoCatalogo.setClave(registros.getString("CLAVE"));
            elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
            coleccionElementos.add(elementoCatalogo);
        }

        LOG.debug("getListaElementos (S)");
        return coleccionElementos;

    }
    
    public void setArea(String area) {
        this.area = area;
    }

    public String getArea() {
        return area;
    }

}
