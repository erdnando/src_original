package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;

/**
 * Genera los combos a partir del catalogo
 * COMCAT_IVA
 * @author ALBERTO CRUZ FLORES
 */
 
public class CatalogoIva{
	private String clave;
	private String descripcion;
	private String seleccion;
	private String orden;
	
	public CatalogoIva(){}
	
	/**
	 * Obtiene una lista de elementos de tipo ElementoCatalogo
	 * @return regresa una colecci�n de elementos obtenidos
	 */	
	public List getListaElementos(){
		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		String strQuery;
		String tablas;
		String condiciones;
		
		try{
			con.conexionDB();
			Registros registros	=	null;			
			
			tablas = " COMCAT_IVA ";
			
			if(this.seleccion != null){
				condiciones	= " WHERE IC_IVA =  " + this.seleccion;
			}else{
				condiciones = "";
			}
			
			if(this.orden != null){
				orden = " ORDER BY "+this.orden;
			}else{
				orden = "";
			}
			
			strQuery = "SELECT "+this.clave+" AS CLAVE, "+
													this.descripcion +" AS DESCRIPCION "+
									" FROM " + tablas + condiciones + orden;
			
			System.out.println("..:: CatalogoIva :: Query ==>> "+strQuery);
			
			registros = con.consultarDB(strQuery);
			
			while(registros.next()) {
					ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					elementoCatalogo.setClave(registros.getString("CLAVE"));
					elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
					coleccionElementos.add(elementoCatalogo);
			}
			
		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
			if (con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return coleccionElementos;
	}

	public String getClave(){return clave;}
	public String getDescripcion(){return descripcion;}
	public String getSeleccion(){return seleccion;}
	
/**
* Establece la clave(id) que se va obtener en la consulta
*/	
	public void setClave(String clave){this.clave = clave;}

/**
* Establece la descripcion  que se va obtener en la consulta
*/		
	public void setDescripcion(String descripcion){this.descripcion = descripcion;}

/**
* Establece valor para indicar alguna condicion especifica a consultar
*/	
	public void setSeleccion(String seleccion){this.seleccion = seleccion;}

/**
* Establece valor para indicar el orden de la consulta
*/
	public void setOrden(String orden){this.orden = orden;}
}