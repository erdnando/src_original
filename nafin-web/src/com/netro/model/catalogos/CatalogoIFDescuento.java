package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Catalogo de IFs
 *
 * Ejemplo de uso:
 * CatalogoIF cat = new CatalogoIF();
 * cat.setCampoClave("ic_if");
 * cat.setCampoDescripcion("cg_razon_social");
 * cat.setClaveEpo("1"); //opcional
 * cat.setMandatoDocumentos("S");
 * cat.setOrden("cg_razon_social");
 *
 * @author Gilberto Aparicio
 */
public class CatalogoIFDescuento  extends CatalogoAbstract {

	StringBuffer tablas = new StringBuffer();

	private String claveEpo			= "";
	private String g_ic_if			= "";
	private String g_vobonafin		= "";
	private String g_piso			= "";
	private String g_tipo			= "";
	private String g_tipo_piso		= "";
	private String g_producto		= "";
	private String g_dispersion	= "";;
	private String g_fondeo			= "";
	private String g_orden			= "";
	private String producto			="";
	private String opera_fideicomiso = ""; // FODEA 17 - 2013 ADMIN NAFIN
	
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(CatalogoIF.class);
	
	public CatalogoIFDescuento() {
		super();
		super.setPrefijoTablaPrincipal("i."); //from comcat_if i
	}
	
	/**
	 * establece utilizar la lista de elementos del Catalogo
	 * @deprecated Utilizar getListaElementos()
	 */
	public List getListaElementosEmp() {
		return this.getListaElementos();
	}	



	/**
	 * Obtiene una lista de elementos de tipo ElementoCatalogo
	 * @return regresa una colección de elementos obtenidos
	 */	
	public List getListaElementos(){
		log.info("getListaElementos(E)");

		List coleccionElementos = new ArrayList();
		
		StringBuffer condicionJoin = new StringBuffer();
		
		StringBuffer hint = new StringBuffer();
		StringBuffer orden = new StringBuffer();
		StringBuffer qryCatIf = new StringBuffer();
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
					super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
		
		tablas.append(" comcat_if i, comrel_if_epo ie ");
		condicionJoin.append(
				" i.ic_if = ie.ic_if "+
				" AND i.cs_habilitado = 'S' "+				
				" AND ie.cs_vobo_nafin = 'S' "	);
		if(!producto.equals("")){
			tablas.append(" ,comrel_producto_if CPI  ");
			condicionJoin.append(" and CPI.ic_if = I.ic_if " +
						" and CPI.ic_producto_nafin = 1 "+
						" and ie.cs_aceptacion = 'S' ");
		}
		
		if(!"".equals(opera_fideicomiso)){
			condicionJoin.append(" and i.cs_opera_fideicomiso = 'N' ");
		}
		if(this.claveEpo!=null  && !this.claveEpo.equals("")){
			condicionJoin.append( " and  ie.ic_epo = ? ");
			super.addVariablesBind(new Integer(this.claveEpo)); 	
		}	
		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
		
		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(
				" SELECT " + hint + " DISTINCT " + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
				);
				
				
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y además se manda a llamar la implementación especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}
	
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
		
	}


	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		String condicionAdicional = "";
		
		
		

	}

	
	/**
	 * @deprecated usar getCampoClave
	 * @return 
	 */
	public String getClave()
	{
		return super.getCampoClave();
	}
	
	/**
	 * @deprecated Usar getClaveEpo
	 */
	public String getIc_epo()
	{
		return this.claveEpo;
	}
	
	
	
	/*****mETODOS PARA CATALGO GENERAL*****/
	public String getG_ic_if(){ return g_ic_if; }
	public String getG_vobonafin(){ return g_vobonafin; }
	public String getG_piso(){ return g_piso; }
	public String getG_tipo(){ return g_tipo; }
	public String getG_tipo_piso(){ return g_tipo_piso; }
	public String getG_producto(){ return g_producto; }
	public String getG_dispersion(){ return g_dispersion; }
	public String getG_fondeo(){ return g_fondeo; }
	public String getG_orden(){ return g_orden; }
	
	
	/**
	 * Establece la clave(id) que se va obtener en la consulta
	 * @deprecated usar setCampoClave
	 */	
	public void setClave(String clave)
	{
		super.setCampoClave(clave);
	}

	/**
	 * Establece la descripcion  que se va obtener en la consulta
	 * @deprecated usar setCampoDescripcion
	 */		
	public void setDescripcion(String descripcion)
	{
		super.setCampoDescripcion(descripcion);
	}

	/**
	 * Establece valor para indicar algun valor en especifico a consultar
	 * @deprecated usar setValoresCondicionIn
	 */	
	public void setSeleccion(String seleccion)
	{
		super.setValoresCondicionIn(seleccion, Integer.class);
	}
	
	/**
	 * Establece el id de la EPO para condicion de la consulta
	 * @deprecated usar setClaveEpo
	 */	
	public void setIc_epo(String ic_epo)
	{
		this.claveEpo = ic_epo;
	}
	

	public void setFideicomiso(String opera_fideicomiso){//FODEA 17 - 2013
		this.opera_fideicomiso = opera_fideicomiso;
	}

/*********************************************************/
	public void setG_ic_if(String g_ic_if){
		this.g_ic_if = g_ic_if;
	}
	public void setG_vobonafin(String g_vobonafin){
		this.g_vobonafin = g_vobonafin;
	}
	public void setG_piso(String g_piso){
		this.g_piso = g_piso;
	}
	public void setG_tipo(String g_tipo){
		this.g_tipo = g_tipo;
	}
	public void setG_tipo_piso(String g_tipo_piso){
		this.g_tipo_piso = g_tipo_piso;
	}
	public void setG_producto(String g_producto){
		this.g_producto = g_producto;
	}
	public void setG_dispersion(String g_dispersion){
		this.g_dispersion = g_dispersion;
	}
	public void setG_fondeo(String g_fondeo){
		this.g_fondeo = g_fondeo;
	}
	public void setG_orden(String g_orden){
		this.g_orden = g_orden;
	}



	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}


	public String getClaveEpo() {
		return claveEpo;
	}


	public void setProducto(String producto) {
		this.producto = producto;
	}


	public String getProducto() {
		return producto;
	}




}




