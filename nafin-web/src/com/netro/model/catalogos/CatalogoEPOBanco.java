package com.netro.model.catalogos;

import netropology.utilerias.AppException;

/**
 *
 * Ejemplo de uso:
 * CatalogoEPOBanco cat = new CatalogoEPOBanco();
 * cat.setCampoClave("ic_epo");
 * cat.setCampoDescripcion("cd_razon_social");
 * cat.setCveBanco("cveBanco");
 * List l = cat.getListaElementos();  //Lista con elementos ElementoCatalogo
 *
 * @author Jos� Alvarado
 */

public class CatalogoEPOBanco extends CatalogoSimple {

	private String claveBanco;
	//Constructor de Clase
	public CatalogoEPOBanco() {
	}
	
	/**
	 * Establece las condiciones adicionales necesarias para contemplar el
	 * pais y el estado al momento de obtener los municipios.
	 */
	protected void setCondicionesAdicionales() {
		//Se valida que esten los campos requeridos:
		try {
			if (this.claveBanco == null || this.claveBanco.equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		} catch (Exception e) {
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		String condicionAdicional = "";
		
		condicionAdicional = " cs_habilitado = ? "+
									"AND IC_BANCO_FONDEO = ?";
		
		super.addCondicionAdicional(condicionAdicional);
		//agregar a variables bind estos valores
		super.addVariablesBind("S");
		super.addVariablesBind(claveBanco);
	}
	
	
	/**
	 * Se establece el metodo get y set para el atributo de la clase
	 */
	public void setCveBanco(String cveBanco){
		this.claveBanco = cveBanco;
	}
	
	public String getCveBanco(){
		return claveBanco;
	}
}