package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


/**
 * Catalogo de Epos para Pyme (comcat_epo)
 *
 * Ejemplo de uso:
 * CatalogoEPOPyme cat = new CatalogoEPOPyme();
 * cat.setCampoClave("ic_epo");
 * cat.setCampoDescripcion("cg_razon_social");
 * cat.setClavePyme("1");
 * cat.setAceptacion("");
 * cat.setValoresCondicionNotIn("?,?", java.lang.Integer.class); //En caso de requerirse (Opcional)
 * cat.setOrden("cg_razon_social");
 *
 * @author Gerardo P�rez
 */
public class CatalogoEPOPorPyme extends CatalogoAbstract {
	
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoEPOPyme.class);
	
	StringBuffer tablas = new StringBuffer();
  private String clavePyme;
  private String claveIF;
  private String moneda;
	private String operaFide_Pyme;
  
	
	public CatalogoEPOPorPyme() {
		super();
		super.setPrefijoTablaPrincipal("e."); //from comcat_epo e
	}
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colecci�n de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer hint = new StringBuffer();
	
		tablas.append(" comcat_epo E, comrel_pyme_epo PE " );

	 

		//hint.append(" /*+ USE_NL(ie e param) */ " );

		StringBuffer condicionJoin = new StringBuffer();
		
		condicionJoin.append(
				" pe.ic_epo = e.ic_epo ");



		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
// AQUI VA EL QUERY PRINCIPAL

//Para obtener los NOT IN	
		
		//Fodea 019-2019	
		if(operaFide_Pyme !=null) {
			if(operaFide_Pyme.equals("N"))  {		
				tablas.append(" , comrel_producto_epo rp " );
				condicionJoin.append(" and rp.ic_epo = e.ic_epo ");
				condicionJoin.append(" AND rp.CS_OPERA_FIDEICOMISO = 'N'");
				condicionJoin.append(" and rp.IC_PRODUCTO_NAFIN = 1 ");
			}
		}
		
		

		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(
				" SELECT " + hint + " " + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
				);
		
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y adem�s se manda a llamar la implementaci�n especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}
	
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		//AccesoDB con = new AccesoDB();
		Registros registros = null;
		AccesoDB con = new AccesoDB();
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		StringBuffer condicionAdicional = new StringBuffer("");
		
		try{
			if (this.clavePyme == null || this.clavePyme.equals("")){
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
			Integer.parseInt(this.clavePyme);
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
		
		condicionAdicional.append(
				" PE.ic_pyme=? " + 
				" AND E.cs_habilitado=? " 	+
				" AND pe.cs_aceptacion in ( ? , ? ) "
				);
		

    
		super.addCondicionAdicional(condicionAdicional.toString());
		//agregar a variables bind estos valores
		super.addVariablesBind(new Integer(this.clavePyme));
		super.addVariablesBind("S");
		super.addVariablesBind("R");
		super.addVariablesBind("H");
		
	}
		public List getEpoPymeIF(){
		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		String tables, condiciones = "", orden;
		String qryCatEPOs;

		try{
			con.conexionDB();
			Registros regCatEPOs	=	null;

			tables = " COMCAT_EPO E, COMREL_PYME_EPO PE, COMREL_IF_EPO IE ";

			condiciones	= " WHERE e.ic_epo = pe.ic_epo " +
								" 	AND pe.ic_epo = ie.ic_epo " +
								" 	AND pe.ic_pyme = ? "  +
								" 	AND ie.ic_if = ? " +
								" 	AND ie.cs_vobo_nafin = 'S' " +
								" 	AND pe.ic_epo not in ( " +
								" 		SELECT  " +
								" 		DISTINCT pi.ic_epo " +
								" 		FROM comrel_pyme_if pi, comrel_cuenta_bancaria cb " +
								" 		WHERE pi.ic_cuenta_bancaria = cb.ic_cuenta_bancaria " +
								" 			AND pi.cs_borrado = 'N' " +
								" 			AND cb.cs_borrado = 'N' " +
								" 			AND pi.cs_vobo_if = 'S' " +
								" 			AND ic_moneda = ? " +
								" 			AND cb.ic_pyme = ? "  +
								" 			AND pi.ic_if = ? " +
								" ) " +
								" ORDER BY e.cg_razon_social ";
		List conditions=new ArrayList();
		conditions.add(this.clavePyme);
		conditions.add(this.claveIF);
		conditions.add(this.moneda);
		conditions.add(this.clavePyme);
		conditions.add(this.claveIF);

			qryCatEPOs = "SELECT " + super.getCampoClave() + " AS CLAVE," +
			" " + super.getCampoDescripcion() + " AS DESCRIPCION"+
			" FROM" + tables + condiciones;

			log.info("qryCatEPOs>>>"+qryCatEPOs); 
			
			regCatEPOs = con.consultarDB(qryCatEPOs,conditions);
			while(regCatEPOs.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(regCatEPOs.getString("CLAVE"));
				elementoCatalogo.setDescripcion(regCatEPOs.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
			}
		}catch(Exception e){
			e.printStackTrace();
		} finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return coleccionElementos;
	}

	public void setClavePyme(String clavePyme) {
		this.clavePyme = clavePyme;
	}


	public void set_clavePyme(String clavePyme) {
		this.clavePyme = clavePyme;
	}


	public String getClavePyme() {
		return clavePyme;
	}


	public void setClaveIF(String claveIF) {
		this.claveIF = claveIF;
	}


	public String getClaveIF() {
		return claveIF;
	}





	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}


	public String getMoneda() {
		return moneda;
	}

	public String getOperaFide_Pyme() {
		return operaFide_Pyme;
	}

	public void setOperaFide_Pyme(String operaFide_Pyme) {
		this.operaFide_Pyme = operaFide_Pyme;
	}
	


}