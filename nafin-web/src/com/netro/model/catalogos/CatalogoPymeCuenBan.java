package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Catalogo de Catalogo Pyme Cuentas Bancaias
 * Ejemplo de uso:
 * @author Hugo Vargas
 */
public class CatalogoPymeCuenBan extends CatalogoAbstract {
	
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoPymeCuenBan.class);
		
	StringBuffer tablas = new StringBuffer();
	private String claveIF;
	private String claveEPO;
	private String cesion;

	public CatalogoPymeCuenBan() {
		super();
		
	}
	
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colecci�n de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer hint = new StringBuffer();
	
		tablas.append(" COMREL_PYME_EPO PE,   COMCAT_PYME PYME, "+
						  " COMREL_CUENTA_BANCARIA RELCTABANC, COMREL_PYME_IF RELPYMEIF, "+
						  " COMCAT_MONEDA M ");

		StringBuffer condicionJoin = new StringBuffer();
		condicionJoin.append(" PE.IC_PYME = PYME.IC_PYME "+
									" AND   PYME.IC_PYME = RELCTABANC.IC_PYME "+
									" AND RELPYMEIF.IC_CUENTA_BANCARIA = RELCTABANC.IC_CUENTA_BANCARIA "+
									" AND RELCTABANC.IC_MONEDA = M.IC_MONEDA " +
									" AND RELPYMEIF.CS_BORRADO='N' "+
									" AND PE.cs_habilitado='S' "+
									" AND PYME.cs_habilitado = 'S' "+
									" AND RELPYMEIF.CS_VOBO_IF ='S' "+
									" AND RELPYMEIF.IC_EPO = PE.IC_EPO ");
									
									
		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
		
		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(
				" SELECT  " + hint + " DISTINCT " + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
				);
				
				
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y adem�s se manda a llamar la implementaci�n especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}
	
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales necesarias para contemplar el
	 * tipoAfiliado.
	 */
	protected void setCondicionesAdicionales() {
		//Se valida que esten los campos requeridos:
		try {
			
		} catch (Exception e) {
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer condicionAdicional = new StringBuffer();

		if (this.claveEPO != null && !this.claveEPO.equals("")) {
			condicionAdicional = new StringBuffer();
			condicionAdicional.append("  PE.IC_EPO = ? ");
			super.addCondicionAdicional(condicionAdicional.toString());
			super.addVariablesBind(this.claveEPO);
			
		}
		if (this.claveIF != null && !this.claveIF.equals("")) {
			condicionAdicional = new StringBuffer();
			condicionAdicional.append("  RELPYMEIF.IC_IF = ? ");
			super.addCondicionAdicional(condicionAdicional.toString());
			super.addVariablesBind(this.claveIF);
			
		}	
		
		//Condici�n que se agrego por FODEA 033 Cesion de derechos
		if (this.cesion != null && !this.cesion.equals("")) {
			condicionAdicional = new StringBuffer();
			condicionAdicional.append(" PYME.CS_CESION_DERECHOS= ? ");
			super.addCondicionAdicional(condicionAdicional.toString());
			super.addVariablesBind(this.cesion);
			
		}
		
	}

	public String getClaveIF() {
		return claveIF;
	}

	public void setClaveIF(String claveIF) {
		this.claveIF = claveIF;
	}

	public String getClaveEPO() {
		return claveEPO;
	}

	public void setClaveEPO(String claveEPO) {
		this.claveEPO = claveEPO;
	}


	public void setCesion(String cesion) {
		this.cesion = cesion;
	}


	public String getCesion() {
		return cesion;
	}

}