package com.netro.model.catalogos;

import netropology.utilerias.AppException;


/**
 * Catalogo de Clasificacion Epo (comrel_calif_epo)
 *
 * Ejemplo de uso:
 * CatalogoPlazos cat = new CatalogoPlazos();
 * cat.setCampoClave("ic_calificacion");
 * cat.setCampoDescripcion("cd_descripcion");
 * cat.setClaveEpo(iNoEPO);
 * cat.setOrden("ic_calificacion");
 * List l = cat.getListaElementos();  //Lista con elementos ElementoCatalogo
 *
 * @author Hugo Vargas
 */
public class CatalogoEpoClasificacion extends CatalogoSimple  {
	//Variable para enviar mensajes al log. 
	private String claveEpo;
	
	public CatalogoEpoClasificacion() {
		super.setTabla("comrel_calif_epo");
	}
	/**
	 * Establece las condiciones adicionales necesarias para contemplar el
	 * pais y el estado al momento de obtener los municipios.
	 */
	protected void setCondicionesAdicionales() {
		//Se valida que esten los campos requeridos:
		try {
			if (this.claveEpo == null || this.claveEpo.equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		} catch (Exception e) {
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		String condicionAdicional = "";
		
		condicionAdicional = " ic_epo = ? ";
		
		super.addCondicionAdicional(condicionAdicional);
		//agregar a variables bind estos valores
		super.addVariablesBind(new Integer(this.claveEpo));
	}

	/**
	 * Establece el producto nafin del cual se desean obetener los plazos
	 * @param claveProducto Clave del producto (ic_producto_nafin)
	 */
	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}

	public String getClaveEpo() {
		return claveEpo;
	}
	
}