package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


/**
 * Catalogo de Pymes - Busqueda Avanzada para Mantenimiento de Documentos ()
 *
 * Ejemplo de uso:
 * CatalogoPymeBusqAvanzadaManto cat = new CatalogoPymeBusqAvanzadaManto();
 * cat.setCampoClave("ic_epo");
 * cat.setCampoDescripcion("cg_razon_social");
 * cat.setOrden("cg_razon_social");
 *
 * @author Fabian Valenzuela
 */
public class CatalogoPymeBusqAvanzadaManto extends CatalogoAbstract {
	
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoPymeBusqAvanzadaManto.class);
	
	private String cveEpo = "";
	private String numPyme = "";
	private String rfcPyme = "";
	private String nombrePyme = "";
	private String cvePyme = "";
	private String pantalla = "";
	
	StringBuffer tablas = new StringBuffer();
	String group = "";
	
	public CatalogoPymeBusqAvanzadaManto() {
		super();
		//super.setPrefijoTablaPrincipal("p."); //from comcat_pyme p
	}
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colección de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer hint = new StringBuffer();
	
		tablas.append(" comrel_pyme_epo pe, comrel_nafin crn, comcat_pyme p ");
		
		if("MantenimientoDoctos".equals(pantalla)){
			tablas.append(" ,com_documento d ");
		}

		hint.append(" /*+index(pe CP_COMREL_PYME_EPO_PK) index(p CP_COMCAT_PYME_PK) use_nl(pe p)*/ " );
		
		group = "  GROUP BY p."+super.getCampoClave()+", "+super.getCampoDescripcion();

		StringBuffer condicionJoin = new StringBuffer();
		
		condicionJoin.append(" p.ic_pyme = pe.ic_pyme" );
		condicionJoin.append(" AND p.ic_pyme = crn.ic_epo_pyme_if ");
		if("MantenimientoDoctos".equals(pantalla)){
			condicionJoin.append(" AND d.ic_pyme = pe.ic_pyme ");
			condicionJoin.append(" AND d.ic_epo = pe.ic_epo ");
		}

		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
		
		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(
				" SELECT " + hint + " p." + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION "+
				//"(pe.cg_pyme_epo_interno ||' '|| p.cg_razon_social) DESPLIEGA " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
				);
				
				
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y además se manda a llamar la implementación especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}
		
		qrySQL.append(" AND ROWNUM <1001 ");
	
		if(!group.equals("")){
				qrySQL.append(group);
		}
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		
		
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		String condicionAdicional = "";
		
		try{
			
			if (this.cveEpo != null && !this.cveEpo.equals(""))
				Integer.parseInt(this.cveEpo);
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
		
		condicionAdicional = 
				"    crn.cg_tipo = ? "   +
				"    AND pe.cs_habilitado = ? "   +
				"    AND p.cs_habilitado = ? "+
				"	  AND p.cs_invalido != ? "+
				"	  AND pe.cs_num_proveedor = ? ";
				
		
			super.addCondicionAdicional(condicionAdicional);
			//agregar a variables bind estos valores
			super.addVariablesBind("P");
			super.addVariablesBind("S");
			super.addVariablesBind("S");
			super.addVariablesBind("S");
			super.addVariablesBind("N");
			
			if(!"".equals(cveEpo)){
				condicionAdicional =  " pe.ic_epo = ? ";
				super.addCondicionAdicional(condicionAdicional);
				super.addVariablesBind(new Integer(cveEpo));
			}
			if(!"".equals(numPyme)){
				condicionAdicional = "  (cg_pyme_epo_interno LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') OR CG_NUM_DISTRIBUIDOR LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%'))";
				super.addCondicionAdicional(condicionAdicional);
				super.addVariablesBind(numPyme);
				super.addVariablesBind(numPyme);
			}
			if(!"".equals(rfcPyme)){
				condicionAdicional = "   cg_rfc LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')";
				super.addCondicionAdicional(condicionAdicional);
				super.addVariablesBind(rfcPyme);
			}
			if(!"".equals(nombrePyme)){
				condicionAdicional =  "  cg_razon_social LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')";
				super.addCondicionAdicional(condicionAdicional);
				super.addVariablesBind(nombrePyme);
			}
	}


	public void setCveEpo(String cveEpo) {
		this.cveEpo = cveEpo;
	}


	public String getCveEpo() {
		return cveEpo;
	}


	public void setNumPyme(String numPyme) {
		this.numPyme = numPyme;
	}


	public String getNumPyme() {
		return numPyme;
	}


	public void setRfcPyme(String rfcPyme) {
		this.rfcPyme = rfcPyme;
	}


	public String getRfcPyme() {
		return rfcPyme;
	}


	public void setNombrePyme(String nombrePyme) {
		this.nombrePyme = nombrePyme;
	}


	public String getNombrePyme() {
		return nombrePyme;
	}


	public void setCvePyme(String cvePyme) {
		this.cvePyme = cvePyme;
	}


	public String getCvePyme() {
		return cvePyme;
	}


	public void setPantalla(String pantalla) {
		this.pantalla = pantalla;
	}


	public String getPantalla() {
		return pantalla;
	}




}