package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Catalogo de Epos
 * Ejemplo de uso:
 * CatalogoEpoDispersion catEpo = new CatalogoEpoDispersion();
 * catEpo.setCampoClave("ic_epo");
 * catEpo.setCampoDescripcion("cg_razon_social");
 */

public class CatalogoEpoDispersion extends CatalogoAbstract	
{
	public CatalogoEpoDispersion() {
		super();
		super.setPrefijoTablaPrincipal("e."); //comcat_epo e
	}

	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(CatalogoEpoDispersion.class);

	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * @return regresa una colección de elementos 
	 */
	public List getListaElementos() {
	
		log.info("Elementos:::::::(E)");

		List coleccionElementos = new ArrayList();
		StringBuffer tables = new StringBuffer();
		StringBuffer hint = new StringBuffer();
		StringBuffer condicionJoin = new StringBuffer();
		StringBuffer orden = new StringBuffer();

		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		Registros regCatEstado	=	null;


		tables.append(" comcat_epo e, comrel_producto_epo cpe  ");
		hint.append("");
		condicionJoin.append(" cpe.ic_epo = e.ic_epo ");

		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind

		StringBuffer qrySQL = new StringBuffer();

		qrySQL.append(
				" SELECT " + hint + " " + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tables + 
				" WHERE " + condicionJoin
				);


		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para
		//IN y NOT IN... y además se manda a llamar la implementación especifica
		//setCondicionesAdicionales de la clase
		
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}

		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}

		log.debug("qrySQL :: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());

		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}

		while(registros.next()) {
			ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
			elementoCatalogo.setClave(registros.getString("CLAVE"));
			elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
			coleccionElementos.add(elementoCatalogo);
		}

		log.debug("Elementos de la lista: -> "+coleccionElementos);
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		StringBuffer condicionAdicional = new StringBuffer();

		/*try{
			if (this.claveProducto == null || this.claveProducto.equals("") ||
				this.dispersion==null || this.dispersion.equals("") 
					) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
			Integer.parseInt(this.claveProducto);
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}*/

		super.addCondicionAdicional(" cpe.ic_producto_nafin = ? ");
		//agregar a variables bind estos valores
		
		super.addCondicionAdicional("cpe.cg_dispersion = ? ");
		//agregar a variables bind estos valores
	
		super.addCondicionAdicional(" e.cs_habilitado = ? ");
		//agregar a variables bind estos valores
		
		super.addVariablesBind(new Integer(1));
		super.addVariablesBind("S");
		super.addVariablesBind("S");				
		super.setOrden("2");		
	}

}