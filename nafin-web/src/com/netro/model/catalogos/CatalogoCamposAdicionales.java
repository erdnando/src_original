package com.netro.model.catalogos;

import netropology.utilerias.AppException;

/**
 * Catalogo de CamposAdicionales (comcat_mandante)
 *
 * Ejemplo de uso:
 * CatalogoCamposAdicionales cat = new CatalogoCamposAdicionales();
 * cat.setCampoClave("campo_clave");
 * cat.setCampoDescripcion("campo_descripcion");
 * cat.setClavePyme(clavePyme);	// Campo obligatorio para el caso de Pyme.
 * cat.setClaveEpo(claveEPO);
 * cat.setOrden("campo_descripcion");
 * List l = cat.getListaElementos();  //Lista con elementos ElementoCatalogo
 *
 * @author Hugo Vargas
 */
public class CatalogoCamposAdicionales extends CatalogoSimple {
	
  private String ic_pyme;
  private String ic_epo;
  private String icEstatusDocto;
  private String validaJR;
  

	public CatalogoCamposAdicionales() {
		super.setTabla("com_documento");
		super.setDistinc(true);
	}

	/**
	 * Establece las condiciones adicionales necesarias para contemplar el
	 * la clave pyme y la clave epo.
	 */
	protected void setCondicionesAdicionales() {
		
		try {
			if (this.ic_epo == null || this.ic_epo.equals("")) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		} catch (Exception e) {
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer condicionAdicional;
		condicionAdicional = new StringBuffer();
		if ( this.ic_pyme != null && !this.ic_pyme.equals("") ){
			condicionAdicional.append(" ic_pyme =  ? AND ");
			super.addVariablesBind(this.ic_pyme);
		}
		
		condicionAdicional.append(" ic_epo = ? AND " + super.getCampoClave() + " IS NOT NULL ");
		super.addVariablesBind(this.ic_epo);
		
		if ( this.icEstatusDocto != null &&   !"".equals(this.icEstatusDocto)  ){
			condicionAdicional.append(" and  ic_estatus_docto =  ?  ");			
			super.addVariablesBind(this.icEstatusDocto);
		}
		if ( this.validaJR != null &&   !"".equals(this.validaJR)  ){
			condicionAdicional.append(" and  CS_VALIDACION_JUR =  ?  ");			
			super.addVariablesBind(this.validaJR);
		}
		
		super.addCondicionAdicional(condicionAdicional.toString());
		
	}

  public void setClavePyme(String ic_Pyme)
  {
    this.ic_pyme = ic_Pyme;
  }

  public void setClaveEpo(String ic_Epo)
  {
    this.ic_epo = ic_Epo;
  }

    /**
     * @param icEstatusDocto
     */
    public void setIcEstatusDocto(String icEstatusDocto)   {
    this.icEstatusDocto = icEstatusDocto;
  }
    /**
     *
     * @param validaJR
     */
    public void setValidaJR(String validaJR)   {
    this.validaJR = validaJR;
  }

}