package com.netro.model.catalogos;

import netropology.utilerias.AppException;

/**
 * Catalogo de Fecha de Corte (com_vencimiento)
 * Ejemplo de uso:
 * CatalogoFechaCorte	cfc 		= new CatalogoFechaCorte();
 * cfc.setIntermediario(interm);
 * cfc.setValoresCondicionIn("ADMIN NAFIN", String.class);
 * List l = cfc.getListaElementos();
 */
public class CatalogoFechaCorte extends CatalogoSimple {
	
	private String intermediario;
	private String tipoLinea;
	private String cliente;

	public CatalogoFechaCorte() {
		super.setTabla("com_vencimiento");
	}
	/**
	 * Establece las condiciones adicionales necesarias para 
	 * la fecha de corte.
	 */
	protected void setCondicionesAdicionales() {
		try {
			if (this.intermediario == null || this.intermediario.equals("") ) {
				throw new Exception("No se ha establecido el intermediario");
			}
		} catch (Exception e) {
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
		String condicionAdicional = "";

		condicionAdicional = " ic_if = ? ";
		super.addCondicionAdicional(condicionAdicional);
		super.addVariablesBind(new Integer(this.intermediario));

		if(!"".equals(cliente))
		{
			super.addCondicionAdicional(" ig_cliente = ? ");
			super.addVariablesBind(new Integer(this.cliente));
		}
		
		if("C".equals(tipoLinea))
		{
			super.addCondicionAdicional("ig_cliente is not null ");
		}
		
		//agregar a variables bind estos valores 
		
		
		
	}

	public void setIntermediario(String intermediario) {
		this.intermediario = intermediario;
	}

	public String getIntermediario() {
		return intermediario;
	}


	public void setTipoLinea(String tipoLinea)
	{
		this.tipoLinea = tipoLinea;
	}


	public String getTipoLinea()
	{
		return tipoLinea;
	}


	public void setCliente(String cliente)
	{
		this.cliente = cliente;
	}


	public String getCliente()
	{
		return cliente;
	}

}