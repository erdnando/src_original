package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Catalogo de IFs del producto de DISTRIBUIDORES
 *
 * Ejemplo de uso:
 * CatalogoIFDistribuidores cat;
 * cat = new CatalogoIFDistribuidores(prefijo);
 * cat.setCampoClave("ic_if");
 * cat.setCampoDescripcion("cg_razon_social");
 * cat.setClaveEpo("1"); //opcional
 * cat.setOrden("cg_razon_social");
 * cat.setSTipoCatalogo("catalogoEstados");
 * cat.setTipoAfiliado("1");
 *
 * @author Gustavo Arellano
 */
public class CatalogoConsultasIF extends CatalogoAbstract {
	
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoConsultasIF.class);
	
	StringBuffer tablas = new StringBuffer();
	
	private String sTipoAfiliado;
	private String tipoCatalogo;
   private String iNoCliente;
	
	public CatalogoConsultasIF(String prefijo) {
		super();
		super.setPrefijoTablaPrincipal(prefijo + "."); //from comcat_if ci
	}
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colección de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		System.out.println(tipoCatalogo);
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer hint = new StringBuffer();
	
		StringBuffer condicionJoin = new StringBuffer();
		
		if(this.sTipoAfiliado.equals("1")) {
			if(this.tipoCatalogo.equals("catalogoEstados")){
            hint.append(" /*+ index(pi IN_COMREL_PYME_IF_01_NUK) use_nl(pi,cb,p,d,edo) */ ");
				tablas.append(" comrel_pyme_if pi,"   +
			"  	comrel_cuenta_bancaria cb,"   +
			"  	comcat_pyme p,"   +
			"  	com_domicilio d,"   +
			" 	comcat_estado edo"); 
	
				condicionJoin.append(
               "p.ic_pyme = cb.ic_pyme and"   +
			"    	 p.ic_pyme = d.ic_pyme  and"   +
			"  	 pi.ic_cuenta_bancaria = cb.ic_cuenta_bancaria and"   +
			" 	    d.ic_estado = edo.ic_estado and"   +
			"  	 d.cs_fiscal = 'S'      and"   +
			"  	 pi.cs_borrado = 'N'	and"   +
			"  	 cb.cs_borrado = 'N'    and"   +
			"  	 pi.ic_if = '" + this.iNoCliente + "' and"   +
			"  	 decode(p.ic_tipo_cliente,4,1,p.ic_tipo_cliente) = 1");
			}else{
            hint.append(" /*+ index(pi IN_COMREL_PYME_IF_01_NUK) use_nl(pi,cb,p,e) */ ");
				tablas.append(" comrel_pyme_if pi,"   +
			"  	comrel_cuenta_bancaria cb,"   +
			"  	comcat_pyme p,"   +
			"  	comcat_epo e"); 
	
				condicionJoin.append(
               " p.ic_pyme = cb.ic_pyme and"   +
			"  	 pi.ic_cuenta_bancaria = cb.ic_cuenta_bancaria and"   +
			"  	 pi.ic_epo = e.ic_epo  	and"   +
			" 		e.cs_habilitado = 'S' and " +
			"  	 pi.cs_borrado = 'N'	and"   +
			"  	 cb.cs_borrado = 'N'    and"   +
			"  	 pi.ic_if = '" + this.iNoCliente + "'		and"   +
			"  	 decode(p.ic_tipo_cliente,4,1,p.ic_tipo_cliente) = 1");
			}
		} else {
			if(this.tipoCatalogo.equals("catalogoEstados")){
            hint.append(" /*+ index(aut CP_COM_CUENTA_AUT_X_PROD_PK) use_nl(cbp,p,aut,d,edo) */ ");
				tablas.append(
               " comrel_cuenta_bancaria_x_prod cbp,"   +
			"  	  comcat_pyme p,"   +
			"  	  com_cuenta_aut_x_prod aut,"   +
			"  	  com_domicilio d,"   +
			"  	  comcat_estado edo"); 
		
					condicionJoin.append(" cbp.ic_pyme = p.ic_pyme AND"   +
			"   	  cbp.ic_cuenta_bancaria = aut.ic_cuenta_bancaria(+) AND"   +
			"    	  p.ic_pyme = d.ic_pyme  and"   +
			" 	     d.ic_estado=edo.ic_estado and"   +
			"  	  cbp.ic_if= '" + this.iNoCliente + "' AND"   +
			"  	 d.cs_fiscal = 'S'      and"   +
			"  	 aut.cs_borrado(+) = 'N'	and"   +
			"  	 aut.IC_EPO(+) = cbp.ic_epo and"   +
			"  	 aut.IC_IF(+) = '" + this.iNoCliente + "'	and"   +
			"  	 cbp.cs_borrado = 'N'    and"   +
			"  	 decode(p.ic_tipo_cliente,4,2,p.ic_tipo_cliente) = 2  and"   +
			"     cbp.ic_producto_nafin = 4");
			}else{
            hint.append(" /*+ index(aut CP_COM_CUENTA_AUT_X_PROD_PK) use_nl(cbp,p,aut,e) */ ");
				tablas.append(" comrel_cuenta_bancaria_x_prod cbp,"   +
			"  	  comcat_pyme p,"   +
			"  	  com_cuenta_aut_x_prod aut,"   +
			"  	  comcat_epo e "); 
	
				condicionJoin.append(" cbp.ic_pyme = p.ic_pyme AND"   +
			"   	  cbp.ic_cuenta_bancaria = aut.ic_cuenta_bancaria(+) AND"   +
			" 	  cbp.ic_epo = e.ic_epo AND"   +
			" 		e.cs_habilitado = 'S' AND " +
			"  	  cbp.ic_if= '" + this.iNoCliente + "' AND"   +
			"  	 aut.cs_borrado(+) = 'N'	and"   +
			"  	 aut.IC_EPO(+) = cbp.ic_epo and"   +
			"  	 aut.IC_IF(+) = '" + this.iNoCliente + "' and"   +
			"  	 cbp.cs_borrado = 'N'    and"   +
			"  	 decode(p.ic_tipo_cliente,4,2,p.ic_tipo_cliente) = 2  and"   +
			"     cbp.ic_producto_nafin = 4 "   +
			" ");
			}
		}

		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
		
		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(
				" SELECT " + hint + " DISTINCT " + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
				);
				
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y además se manda a llamar la implementación especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion(); 
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}
	
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}   

	/**   
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		StringBuffer condicionAdicional = new StringBuffer(128);
		
					
		super.addCondicionAdicional(condicionAdicional.toString());
	}

     
	/**
	 * Establece la clave del afiliado para obtener los intermediarios con los
	 * que trabaja
	 * @param sTipoAfiliado Clave del tipo de Afiliado (sTipoAfiliado)
	 */
	public void setSTipoAfiliado(String sTipoAfiliado) {
		this.sTipoAfiliado = sTipoAfiliado;
	}


	public void setTipoCatalogo(String tipoCatalogo) {
		this.tipoCatalogo = tipoCatalogo;
	}


   public void set_tipoCatalogo(String tipoCatalogo) {
      this.tipoCatalogo = tipoCatalogo;
   }


   public String getTipoCatalogo() {
      return tipoCatalogo;
   }


   public void setINoCliente(String iNoCliente) {
      this.iNoCliente = iNoCliente;
   }
}