package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Esta clase genera u obtiene los valores del catalogo
 * COMCAT_TASA y COMREL_TASA_PRODUCTO, en base a ciertas condiciones establecidas
 * ejemplom de uso:
 * CatalogoCargaTasas cat= new CatalogoCargaTasas();
 * cat.setClave("ic_tasa");
 * cat.setDescripcion("cd_nombre");
 * cat.setCveProd(ic_prod);
 * cat.setCveMoneda(ic_moneda);
 * @author Jos� Alvarado
 */

public class CatEPOFide extends CatalogoSimple {
	private String clave;
	private String descripcion;
	private static final Log log = ServiceLocator.getInstance().getLog(CatEPOFide.class);
	
	public CatEPOFide() {
	}
	
	/**
	* Obtiene una lista de elementos de tipo ElementoCatalogo
	* @return regresa una colecci�n de elementos obtenidos
	*/
	public List getListaElementos()
	{
		log.info("getListaElementos(E)");
		List coleccionElementos = new ArrayList();

		AccesoDB con = new AccesoDB();
		String tables, condiciones;
		String qryCatPantallas;
		try
		{
			con.conexionDB();
			Registros regCatPantallas	=	null;

			//tables			= " comcat_epo e, com_docto_seleccionado d "; 
			tables			= " comcat_epo e, comrel_producto_epo d ";

			//condiciones	= " WHERE e.ic_epo = d.ic_epo "+"AND d.cs_opera_fiso = 'S' "; 
			condiciones	= " WHERE e.ic_epo = d.ic_epo "+"AND d.cs_opera_fideicomiso = 'S' AND e.cs_habilitado='S'";

			qryCatPantallas = "SELECT DISTINCT e."+this.clave+" AS CLAVE, e." +this.descripcion +" AS DESCRIPCION "+
											" FROM " + tables + condiciones+" ORDER BY e.cg_razon_social";
			
			log.debug("qryEpos: "+qryCatPantallas);

			regCatPantallas = con.consultarDB(qryCatPantallas);

			while(regCatPantallas.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(regCatPantallas.getString("CLAVE"));
				elementoCatalogo.setDescripcion(regCatPantallas.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}


		} catch(Exception e) {
				e.printStackTrace();
				throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
		}
		return coleccionElementos;

	}
	
	
	/**
	* Establece la clave(id) que se va obtener en la consulta
	*/
	public void setClave(String clave)
	{
		this.clave = clave;
	}
	/**
	* Establece la descripcion  que se va obtener en la consulta
	*/
	public void setDescripcion(String descripcion)
	{
		this.descripcion = descripcion;
	}

	
}