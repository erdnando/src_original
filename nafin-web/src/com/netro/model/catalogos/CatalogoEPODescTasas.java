package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


/**
 * @author Deysi Hern�ndez Contreras
 */
public class CatalogoEPODescTasas extends CatalogoAbstract {
	
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoEPODescTasas.class);
	 
	
	
	StringBuffer tablas = new StringBuffer();
	String group = "";
	public String claveIF;
	private String lsTipoTasa;
	private String ambos;
	private String parametro;
	
	public CatalogoEPODescTasas() {
		super();		
	}
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colecci�n de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
 
		
		StringBuffer hint = new StringBuffer();
		StringBuffer condicionJoin = new StringBuffer();
		StringBuffer group = new StringBuffer();
		StringBuffer tablas = new StringBuffer();
		
		
		tablas.append( " comrel_if_epo cie, comcat_epo ce ,com_parametrizacion_epo epo ");
	 
		condicionJoin.append( " cie.ic_epo = ce.ic_epo"   +												
									"  AND EPO.IC_EPO = CE.IC_EPO  ");
	
	

		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
		
		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(
				" SELECT  " + hint + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
				);
				
				
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y adem�s se manda a llamar la implementaci�n especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}
		
	
		if(!group.equals("")){
				qrySQL.append(group);
		}
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		
		
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		String  condicionAdicional ="";
		
		try{
			
			condicionAdicional = "  cie.cs_vobo_nafin = ? "   +
										" AND cie.cs_aceptacion = ? "   +
										" AND CE.cs_habilitado= ? ";   
										
			super.addCondicionAdicional(condicionAdicional);
			super.addVariablesBind("S");
			super.addVariablesBind("S");	
			super.addVariablesBind("S");	
					
			if(!"".equals(claveIF)) {
				condicionAdicional ="  cie.ic_if = ? ";
				super.addCondicionAdicional(condicionAdicional);
				super.addVariablesBind(claveIF);
			}
			
			if(!"".equals(parametro)) {
				condicionAdicional ="  epo.cc_parametro_epo = ?";
				super.addCondicionAdicional(condicionAdicional);
				super.addVariablesBind(parametro);
			}
			
			if(lsTipoTasa != null ||  ambos != null  ) {
				condicionAdicional = " epo.cg_valor in('"+lsTipoTasa+ "','"+ambos+ "')";
				 super.addCondicionAdicional(condicionAdicional);
			}
			
				
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
		

	}

	public String getClaveIF() {
		return claveIF;
	}

	public void setClaveIF(String claveIF) {
		this.claveIF = claveIF;
	}

	public String getLsTipoTasa() {
		return lsTipoTasa;
	}

	public void setLsTipoTasa(String lsTipoTasa) {
		this.lsTipoTasa = lsTipoTasa;
	}

	public String getAmbos() {
		return ambos;
	}

	public void setAmbos(String ambos) {
		this.ambos = ambos;
	}

	public String getParametro() {
		return parametro;
	}

	public void setParametro(String parametro) {
		this.parametro = parametro;
	}

	


	




}