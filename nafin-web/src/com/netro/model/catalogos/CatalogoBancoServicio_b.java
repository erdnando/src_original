package com.netro.model.catalogos;

import java.util.List;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;

/**
 * Catalogo de Banco Servicio (comcat_financiera)
 *
 * Ejemplo de uso:
 * CatalogoBancoServicio_b cat = new CatalogoBancoServicio_b();
 * cat.setCampoClave("ic_plazo");
 * cat.setCampoDescripcion("cg_descripcion");
 * cat.setTipoFinanciera("1,2,3");
 * cat.setOrden("cg_descripcion");
 * List l = cat.getListaElementos();  //Lista con elementos ElementoCatalogo
 *
 * @author Hugo Vargas
 */
public class CatalogoBancoServicio_b extends CatalogoSimple  {
	//Variable para enviar mensajes al log. 
	private String tipoFinanciera = "";

	public CatalogoBancoServicio_b() {
		super.setTabla("comcat_financiera_nafin");
	}
	/**
	 * Establece las condiciones adicionales necesarias para contemplar el
	 * pais y el estado al momento de obtener los municipios.
	 */
	protected void setCondicionesAdicionales() {
		//Se valida que esten los campos requeridos:
		try {
			if (this.tipoFinanciera == null || this.tipoFinanciera.equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		} catch (Exception e) {
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		String condicionAdicional = "";

		if(!"".equals(tipoFinanciera)) {

			List listTipo = Comunes.explode(",", tipoFinanciera ,Integer.class);
			condicionAdicional = " ic_tipo_financiera IN (" + Comunes.repetirCadenaConSeparador("?",",",listTipo.size()) + ")";
			super.addCondicionAdicional(condicionAdicional);
			super.addVariablesBind(listTipo);
		}
	}

	/**
	 * Establece el tipoFinanciera nafin del cual se desean obetener los plazos
	 * @param tipoFinanciera
	 */
	public void setTipoFinanciera(String tipoFinanciera) {
		this.tipoFinanciera = tipoFinanciera;
	}

	public String getTipoFinanciera() {
		return tipoFinanciera;
	}
	
}