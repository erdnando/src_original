package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


/**
 * Catalogo de IFs del producto de EPO
 *
 * Ejemplo de uso:
 * CatalogoIFEpo cat = new CatalogoIFEpo();
 * cat.setCampoClave("ic_if");
 * cat.setCampoDescripcion("cg_razon_social");
 * cat.setUsuario(usuario);
 *
 * @author Gerardo P�rez
 */
public class CatalogoIFEpoProveedores extends CatalogoAbstract {
	
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoIFEpoProveedores.class);
	
	StringBuffer tablas = new StringBuffer();
	
	private String usuario;
	private boolean bandera=false;
	private String claveAdicional;
	private String descripcionAdicional;
	
	public CatalogoIFEpoProveedores() {
		super();
		super.setPrefijoTablaPrincipal("e."); //from comcat_if ci
	}
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colecci�n de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer hint = new StringBuffer();
	
		StringBuffer condicionJoin = new StringBuffer();
		tablas.append("comcat_epo e, comrel_if_epo r");
		condicionJoin.append("e.ic_epo = r.ic_epo");
		
	

		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
		
		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(
				" SELECT " + hint + " DISTINCT " + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
				);
				
				
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y adem�s se manda a llamar la implementaci�n especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" " + super.getCondicion());
		}
	
		
			qrySQL.append(
					" ORDER BY 2 " );
		
		
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		if(bandera!=false){
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(this.getClaveAdicional());
				elementoCatalogo.setDescripcion(this.getDescripcionAdicional());
				coleccionElementos.add(elementoCatalogo);
		
		
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		StringBuffer condicionAdicional = new StringBuffer(128);
		
                        if( this.usuario!=null && !this.usuario.equals("") ){
                            condicionAdicional.append(" AND ic_if = ? ");
                        }

			condicionAdicional.append(" AND e.cs_habilitado = ? ");

                        if( this.usuario!=null && !this.usuario.equals("") ){
			super.addVariablesBind(this.usuario);
                        }
			super.addVariablesBind("S");


		super.addCondicionAdicional(condicionAdicional.toString());
	}
	
	/**
	 * Establece si se quiere agregar un valor adicional al catalogo, 
	 * ejemplo TODAS LAS OPCIONES
	 * @param Bandera para determar la acci�n. 
	 */
	public void setValorInicial(boolean bandera){
		this.bandera=bandera;
}
	public boolean gerValorInicial(){
	return bandera;
	}


	/**
	 * Establece el usuario para obtener su catalogo de EPO
	 * @param Usuario 
	 */
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

/**
	 * Determina la clave adicional
	 * @param clave 
	 */
	
	
	public void setClaveAdicional(String claveAdicional) {
		this.claveAdicional = claveAdicional;
	}


	public String getClaveAdicional() {
		return claveAdicional;
	}


	public void setDescripcionAdicional(String descripcionAdicional) {
		this.descripcionAdicional = descripcionAdicional;
	}


	public String getDescripcionAdicional() {
		return descripcionAdicional;
	}





}