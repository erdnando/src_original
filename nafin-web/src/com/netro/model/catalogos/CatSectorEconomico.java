package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Esta clase genera u obtiene los valores del catalogo
 * COMCAT_SUBSECTOR_EPO, en base a ciertas condiciones establecidas
 * @author
 */
public class CatSectorEconomico extends CatalogoSimple
{
	private String clave;     
	private String descripcion;
	private String seleccion;
	private String order;
	
	private static final Log log = ServiceLocator.getInstance().getLog(CatSectorEconomico.class);


/**
 * Obtiene una lista de elementos de tipo ElementoCatalogo
 * @return regresa una colecci�n de elementos obtenidos
 */ 
	public List getListaElementos()
	{
		log.info("getListaElementos(E)");
		List coleccionElementos = new ArrayList();
		
		AccesoDB con = new AccesoDB();		      
		String tables, condiciones, orden;
		String qryCatSubsectorECO;
		try
		{
			con.conexionDB();
			Registros regCatSubsectorECO	=	null;
			
			tables			= " comcat_sector_econ s, comcat_tipo_sector t  ";			
         
			condiciones	= " WHERE t.ic_tipo_sector = s.ic_tipo_sector";
			
			

			qryCatSubsectorECO = "select "+this.clave+" AS CLAVE, " +
											this.descripcion +" AS DESCRIPCION "+
											" FROM " + tables + condiciones;
			if(this.order!=null && !this.order.equals("")){
				qryCatSubsectorECO += " ORDER BY "+this.order;
			}
			 
				
			
			regCatSubsectorECO = con.consultarDB(qryCatSubsectorECO);
		
			while(regCatSubsectorECO.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(regCatSubsectorECO.getString("CLAVE"));
				elementoCatalogo.setDescripcion(regCatSubsectorECO.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}


		} catch(Exception e) {
				e.printStackTrace();
				throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
		}

		return coleccionElementos;

	}



/**
* Establece la clave(id) que se va obtener en la consulta
*/
	public void setClave(String clave)
	{
		this.clave = clave;
	}
/**
* Establece la descripcion  que se va obtener en la consulta
*/
	public void setDescripcion(String descripcion)
	{
		this.descripcion = descripcion;
	}
/**
* Establece valor para indicar algun valor en especifico a consultar
*/
	public void setSeleccion(String seleccion)
	{
		this.seleccion = seleccion;
	}

/**
* Establece el campo por el cual se va a ordenar
*/
	public void setOrden(String order)
	{
		this.order = order;
	}

}