package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


/**
 * Catalogo de Pymes (comcat_pyme)
 *
 * Ejemplo de uso:
 * CatalogoPYME cat = new CatalogoPYME();
 * cat.setCampoClave("ic_pyme");
 * cat.setCampoDescripcion("cg_razon_social");
 *
 * cat.setCesionDerechos("S");
 * cat.setClaveEpo("1");  //o cat.setClavesEpo("1,10,23,256");
 * cat.setConDocumentosPublicados(false);
 * cat.setEstatusDocumentos("2");
 * cat.setConDocumentosCambioEstatus(true);
 * cat.setTipoCambioEstatus("2,23");
 * cat.setIfCuentaBancaria("1");
 * cat.setConNumeroProveedor(true);
 *
 * cat.setOrden("cg_razon_social");
 *
 *
 * @author Gilberto Aparicio
 */
public class CatalogoPYMED extends CatalogoAbstract {

 
	//Variable para enviar mensajes al log.
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoPYMED.class);	
	private String clavesEpo = "";
	private boolean conNumeroProveedor = true;	//true para pymes con numero de proveedor, false de lo contrario

	StringBuffer tablas = new StringBuffer();	
	private String groupBy;

	public CatalogoPYMED() {
		super();	
			
		
	}  

	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colecci�n de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();

		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer condicionJoin = new StringBuffer();
		StringBuffer hint = new StringBuffer();
		hint.append(" /*+ USE_NL( ");
		if(this.clavesEpo != null && !this.clavesEpo.equals("")){
			tablas.append(" comrel_pyme_epo pe, comcat_pyme cp " );
			condicionJoin.append(" pe.ic_pyme = cp.ic_pyme ");
			hint.append(" pe cp ");
		} 

		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind

		hint.append(" ) */ ");
		StringBuffer qrySQL = new StringBuffer();

		qrySQL.append(
				" SELECT " + hint + " DISTINCT " + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
				);
  

		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para
		//IN y NOT IN... y adem�s se manda a llamar la implementaci�n especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();

		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}
  	
		if (this.groupBy != null || this.groupBy.equals("S")) {
			qrySQL.append(" GROUP BY " + super.getCampoClave() +", "+ super.getCampoDescripcion() );
		}


		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		} 
     
		log.debug(" ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: ");
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
	log.debug(" ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: ");

		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}  
		}
  
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}

		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		String condicionAdicional = "";

		condicionAdicional = 
				" cp.cs_habilitado = ? " + 
				" and cp.cs_invalido != ? "+
				" AND pe.cs_num_proveedor = ? ";
			
		super.addCondicionAdicional(condicionAdicional);
		super.addVariablesBind("S");
		super.addVariablesBind("S");
		super.addVariablesBind("N");

		if(this.clavesEpo != null && !this.clavesEpo.equals("")){
			List lClavesEpos = Comunes.explode(",", clavesEpo ,Integer.class);
			condicionAdicional =
					" pe.cs_habilitado = ? " +
					" AND pe.ic_epo IN (" + Comunes.repetirCadenaConSeparador("?",",",lClavesEpos.size()) + ")";

			super.addCondicionAdicional(condicionAdicional);
			//agregar a variables bind estos valores
			super.addVariablesBind("S");
			super.addVariablesBind(lClavesEpos);
			//log.debug("setCondicionesAdicionales(S)");
		}


	}


	/**
	 * @deprecated Usar getClaveEpo()
	 */
	public String getNoepo() {
		return this.clavesEpo;
	}

	/**
	 * @deprecated Usar setClaveEpo(String claveEpo)
	 */
	public void setNoepo(String noepo) {
		this.clavesEpo = noepo;
	}

	/**
	 * Especifica la epo con la que la pyme tiene relaci�n.
	 * @param claveEpo Clave de la epo (ic_epo)
	 */
	public void setClaveEpo(String claveEpo) {
		this.clavesEpo = claveEpo;
	}


	public String getClaveEpo() {
		return this.clavesEpo;
	}



	/**
	 * Establece un conjunto de EPOS con las que la Pyme debe tener relacion
	 * @param clavesEpo Claves de epo sepradas por coma
	 */
	public void setClavesEpo(String clavesEpo) {
		this.clavesEpo = clavesEpo;
	}

 
	public String getClavesEpo() {
		return clavesEpo;
	}


	public String getGroupBy() {
		return groupBy;
	}

	public void setGroupBy(String groupBy) {
		this.groupBy = groupBy;
	}


	/**
	 * Establece si las pymes deben de tener o no numero de proveedor.
	 * El valor predeterminado es true
	 * @param conNumeroProveedor false para mostrar pymes sin numero de proveedor.
	 */
	public void setConNumeroProveedor(boolean conNumeroProveedor) {
		this.conNumeroProveedor = conNumeroProveedor;
	}


	public boolean isConNumeroProveedor() {
		return conNumeroProveedor;
	}

}