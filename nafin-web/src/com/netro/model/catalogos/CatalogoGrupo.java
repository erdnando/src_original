package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;

/**
 * Esta clase genera u obtiene los valores del catalogo
 * comcat_grupo_epo, en base a ciertas condiciones establecidas
 */
public class CatalogoGrupo
{
	private String clave;
	private String descripcion;
	private String seleccion;
	private String order;
	
/**
 * Obtiene una lista de elementos de tipo ElementoCatalogo
 * @return regresa una colecci�n de elementos obtenidos
 */	
	public List getListaElementos()
	{
		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		String tables, condiciones, orden;
		String qryCatGrupo;
		try
		{
			con.conexionDB();
			Registros regCatExportador	=	null;
																				
			tables			= " comcat_grupo_epo ";
			condiciones	= " WHERE cs_activo = 'S' ";
			orden       = " ORDER BY DESCRIPCION ";
										
			qryCatGrupo = "select "+this.clave+" AS CLAVE, " + 
											this.descripcion +" AS DESCRIPCION "+
											" FROM " + tables + condiciones;
			
			if(this.order!=null && !this.order.equals(""))
				qryCatGrupo += "ORDER BY "+this.order;
			else
				qryCatGrupo += orden;
			
			regCatExportador = con.consultarDB(qryCatGrupo);
			while(regCatExportador.next()) {
					ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					elementoCatalogo.setClave(regCatExportador.getString("CLAVE"));
					elementoCatalogo.setDescripcion(regCatExportador.getString("DESCRIPCION"));
					coleccionElementos.add(elementoCatalogo);
			}
		
		
		} catch(Exception e) {
				e.printStackTrace();
				throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
		}
				
		return coleccionElementos;													
		
	}
  
	
/**
* Establece la clave(id) que se va obtener en la consulta
*/	
	public void setClave(String clave)
	{
		this.clave = clave;		
	}
/**
* Establece la descripcion  que se va obtener en la consulta
*/		
	public void setDescripcion(String descripcion)
	{
		this.descripcion = descripcion;
	}
	
/**
* Establece la clave(id) que se va obtener en la consulta
*/	
	public void setOrder(String order)
	{
		this.order = order;		
	}
	
/**
* Establece valor para indicar algun valor en especifico a consultar
*/	
	public void setSeleccion(String seleccion)
	{
		this.seleccion = seleccion;
	}

}