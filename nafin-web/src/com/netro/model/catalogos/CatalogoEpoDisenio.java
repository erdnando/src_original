package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Catalogo de Epos para com_epo_diseno
 *
 * Ejemplo de uso:
 * CatalogoIFDistribuidores cat;
 * cat = new CatalogoIFDistribuidores(prefijo);
 * cat.setCampoClave("ic_if");
 * cat.setCampoDescripcion("cg_razon_social");
 * cat.setOrden("cg_razon_social");
 * cat.setNuevaEpo("S");
 * cat.setIcBanco("1");
 *
 * @author Gustavo Arellano
 */
public class CatalogoEpoDisenio extends CatalogoAbstract {
	
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoConsultasIF.class);
	
	StringBuffer tablas = new StringBuffer();
	
	private boolean nuevaEpo;
	private String icBanco;
	
	public CatalogoEpoDisenio() {
		super();
		super.setPrefijoTablaPrincipal("e."); //from comcat_if ci
	}
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colección de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		StringBuffer hint = new StringBuffer();
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

	
		StringBuffer condicionJoin = new StringBuffer();
				tablas.append(" comcat_epo e "); 
				if(!nuevaEpo){
					tablas.append(", com_diseno_epo de");
				}
				
			
	
		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
		
		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(
				" SELECT " + hint + " DISTINCT " + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
		);
				
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y además se manda a llamar la implementación especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append( super.getCondicion());
		}
	
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		StringBuffer condicionAdicional = new StringBuffer(128);
		
		condicionAdicional.append(" E.cs_habilitado= ? ");
		super.addVariablesBind("S");
		
		if(!icBanco.equals("")){
			condicionAdicional.append(" AND ic_banco_fondeo = ? " );
			super.addVariablesBind(icBanco);
		
		}
		if(nuevaEpo){
			condicionAdicional.append(" 	AND NOT EXISTS (select DE.ic_epo " +
												"		FROM com_diseno_epo DE " +
												" 		WHERE DE.ic_epo = E.ic_epo AND DE.cs_borrado = ? ) ");
			super.addVariablesBind("N");
		}else{
			condicionAdicional.append(" 	AND DE.ic_epo = E.ic_epo  " +
			" 	AND DE.cs_borrado = ? " );
			super.addVariablesBind("N");
		}
		
					
		super.addCondicionAdicional(condicionAdicional.toString());
	}


	public void setNuevaEpo(boolean nuevaEpo) {
		this.nuevaEpo = nuevaEpo;
	}


	public boolean isNuevaEpo() {
		return nuevaEpo;
	}


	public void setIcBanco(String icBanco) {
		this.icBanco = icBanco;
	}


	public String getIcBanco() {
		return icBanco;
	}



}     