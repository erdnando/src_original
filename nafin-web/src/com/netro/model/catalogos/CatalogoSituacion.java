package com.netro.model.catalogos;

import netropology.utilerias.AppException;

/**
 * Catalogo de Situacion de Garantias (gticat_situacion)
 *
 * Ejemplo de uso:
 * CatalogoSituacion cat = new CatalogoSituacion();
 * cat.setCampoClave("ic_situacion");
 * cat.setCampoDescripcion("cg_descripcion");
 * cat.setClaveTipoOperacion("4");
 * cat.setOrden("cg_descripcion");
 * List l = cat.getListaElementos();  //Lista con elementos ElementoCatalogo
 *
 * para el caso en el que el campo clave no concuerde con la llave y se emplee
 * valores para in o not in, es necesario especificar el campo llave. ejemplo:
 * cat.setCampoClave("ic_tipo_operacion||ic_situacion");
 * cat.setCampoDescripcion("cg_descripcion");
 * cat.setClaveTipoOperacion("4");
 * cat.setCampoLlave("ic_situacion");
 * cat.setValoresCondicionIn("1,4", Integer.class);
 * cat.setOrden("2");
 *
 * @author Gilberto Aparicio
 */
public class CatalogoSituacion extends CatalogoSimple {
	private String claveTipoOperacion;

	
	public CatalogoSituacion() {
		super.setTabla("gticat_situacion");
	}

	/**
	 * Establece las condiciones adicionales necesarias para contemplar el
	 * pais y el estado al momento de obtener los municipios.
	 */
	protected void setCondicionesAdicionales() {
		//log.debug("setCondicionesAdicionales(E)");

		//Se valida que esten los campos requeridos:
		try {
			if (this.claveTipoOperacion == null || this.claveTipoOperacion.equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		} catch (Exception e) {
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		String condicionAdicional = "";
		
		condicionAdicional = " ic_tipo_operacion = ? ";
		
		super.addCondicionAdicional(condicionAdicional);
		//agregar a variables bind estos valores
		super.addVariablesBind(new Integer(claveTipoOperacion));
		//log.debug("setCondicionesAdicionales(S)");
	}


/**
* Establece el id del tipo de operacion para condicion de la consulta
* @param claveTipoOperacion Clave del tipo de operacion de garantias por el que se filtran las situaciones
*/	
	public void setClaveTipoOperacion(String claveTipoOperacion)
	{
		this.claveTipoOperacion = claveTipoOperacion;
	}


}