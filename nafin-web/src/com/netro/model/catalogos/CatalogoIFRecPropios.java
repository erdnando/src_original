package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class CatalogoIFRecPropios extends CatalogoSimple
{
	private String clave;     
	private String descripcion;
	private String seleccion;
	private String order;
	
	private static final Log log = ServiceLocator.getInstance().getLog(CatalogoIFRecPropios.class);



/**
 * Obtiene una lista de elementos de tipo ElementoCatalogo
 * @return regresa una colecci�n de elementos obtenidos
 */ 
	public List getListaElementos()
	{
		log.info("getListaElementos(E)");
		List coleccionElementos = new ArrayList();
		
		AccesoDB con = new AccesoDB();		      
		String tables, condiciones, orden;
		String qryCatalogo="";
		try
		{
			con.conexionDB();
			Registros regCatSubsectorECO	=	null;  
			
			tables			= " comcat_if ci, comrel_if_epo_x_producto rie, comcat_epo epo, comcat_banco_fondeo cbf  ";			
         
			condiciones	= " WHERE  ci.ic_if = rie.ic_if"+                         
							  "  AND rie.cs_tipo_fondeo in('P', 'M') " +
                       " AND rie.ic_epo = epo.ic_epo " +                          
                       " AND ci.ig_mes_ajuste_de IS NOT null "+
                       " AND ci.ig_mes_ajuste_a IS NOT null "+
                       " AND ci.ig_anio_ajuste IS NOT null ";
                          
			
			qryCatalogo = "select distinct  "+this.clave+" AS CLAVE, " +
								this.descripcion +" AS DESCRIPCION ";
								
			qryCatalogo +=	" FROM " + tables + condiciones;
											
	
			
			if(this.order!=null && !this.order.equals("")){
				qryCatalogo += " ORDER BY "+this.order;
			}
			 
			 
			 log.debug("qryCatalogo  "+qryCatalogo); 
				
			
			regCatSubsectorECO = con.consultarDB(qryCatalogo);
		
			while(regCatSubsectorECO.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(regCatSubsectorECO.getString("CLAVE"));
				elementoCatalogo.setDescripcion(regCatSubsectorECO.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}


		} catch(Exception e) {
				e.printStackTrace();
				throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
		}

		return coleccionElementos;

	}



/**
* Establece la clave(id) que se va obtener en la consulta
*/
	public void setClave(String clave)
	{
		this.clave = clave;
	}
/**
* Establece la descripcion  que se va obtener en la consulta
*/
	public void setDescripcion(String descripcion)
	{
		this.descripcion = descripcion;
	}
/**
* Establece valor para indicar algun valor en especifico a consultar
*/
	public void setSeleccion(String seleccion)
	{
		this.seleccion = seleccion;
	}

/**
* Establece el campo por el cual se va a ordenar
*/
	public void setOrden(String order)
	{
		this.order = order;
	}

	

}