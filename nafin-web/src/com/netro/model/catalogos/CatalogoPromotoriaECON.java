package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


/**
 * Catalogo de Afianzadoras (fe_afianzadoras)
 *
 * Ejemplo de uso:
 * CatalgoAfianzadora cat = new CatalgoAfianzadora();
 * cat.setCampoClave("ic_afianzadora");
 * cat.setCampoDescripcion("cg_razon_social");
 * cat.setClaveEpo("1");
 * cat.setOrden("cg_razon_social");
 *
 * @author Gerardo Perez
 */
public class CatalogoPromotoriaECON extends CatalogoAbstract {
	
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoPromotoriaECON.class);
	private String epo ;
	private String producto;

	
	StringBuffer tablas = new StringBuffer();
	
	public CatalogoPromotoriaECON() {
		super();
		super.setPrefijoTablaPrincipal("promotoria."); //from comcat_epo e
	}
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colección de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer hint = new StringBuffer();
	
		tablas.append("econ_ope_contratos_if contrato_if, " +
											"         econ_ope_contratos_epos contrato_epo, " +
											"         econ_ope_promo_canal promo_canal, " +
											"         econ_cat_promotoria promotoria " );
				

		hint.append(" /*+  use_nl (promo_canal,contrato_epo,contrato_if,promotoria)*/" );

		StringBuffer condicionJoin = new StringBuffer();
		
		condicionJoin.append("contrato_if.econ_ope_contratos_if_pk = contrato_epo.econ_ope_contratos_if_fk " +
										" AND contrato_if.econ_cat_productos_fk = promo_canal.econ_cat_productos_fk " +
										"     AND promo_canal.econ_cat_promotoria_fk = promotoria.econ_cat_promotoria_pk " +
										"  	AND promotoria.cregistro_activo_ck  = 'S'             " +
										"     AND promo_canal.CREGISTRO_ACTIVO_CK = 'S' " +
										"     AND contrato_if.CREGISTRO_ACTIVO_CK = 'S' " +
										"     AND contrato_epo.CREGISTRO_ACTIVO_CK = 'S' " +
										"     AND promotoria.cen_operacion_ck     = 'S' "+
										"     AND promotoria.ctipo_ck     = 'E' ");
		
		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
		
		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(
				" SELECT "+hint+" DISTINCT " + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
				);
				
				
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y además se manda a llamar la implementación especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append( super.getCondicion());
		}
	
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		String condicionAdicional = "";
		if(epo!=null&&!epo.equals("")){
			super.addCondicionAdicional("     AND contrato_epo.econ_cat_epos_fk =  ? ");
			super.addVariablesBind(epo);
		}
		if(producto!=null&&!producto.equals("")){
			super.addCondicionAdicional("   promo_canal.econ_cat_productos_fk IN (?) ");
			super.addVariablesBind(producto);
		
		}
		boolean and = false;	
	}


	public void setEpo(String epo) {
		this.epo = epo;
	}


	public String getEpo() {
		return epo;
	}


	public void setProducto(String producto) {
		this.producto = producto;
	}


	public String getProducto() {
		return producto;
	}

}