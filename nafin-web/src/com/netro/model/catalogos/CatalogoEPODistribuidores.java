package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


/**
 * Catalogo de Epos del producto de DISTRIBUIDORES
 *
 * Ejemplo de uso:
 * CatalogoEPODistribuidores cat = new CatalogoEPODistribuidores();
 * cat.setCampoClave("ic_epo");
 * cat.setCampoDescripcion("cg_razon_social");
 * cat.setClaveIf("1");
 * cat.setCdoctos("S");
 * cat.setOrden("cg_razon_social");
 * cat.setTipoCredito("?");	//	opcional
 *
 * @author Gilberto Aparicio
 */
public class CatalogoEPODistribuidores extends CatalogoAbstract {
	
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoEPODistribuidores.class);
	
	private String 	clavePyme = "";
	private String 	claveIf = "";
   private String 	tipoCredito = "";
  	private String 	cdoctos = "";
	private String 	estatusDocto = "";
	
	private String 	pantalla = "";//Se usa para la pantalla AFORO POR EPO

	StringBuffer tablas = new StringBuffer();
	
	public CatalogoEPODistribuidores() {
		super();
		super.setPrefijoTablaPrincipal("e."); //from comcat_epo e
	}
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colección de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());    
		}

		StringBuffer hint = new StringBuffer();
	
		StringBuffer condicionJoin = new StringBuffer();
		
		if (claveIf != null && !claveIf.equals("")) {
			if(!pantalla.equals("") &&pantalla.equals("AFOROEPO")){
				tablas.append(
					" comcat_epo e, comrel_if_epo IE, comrel_if_epo_x_producto IEP "+
					" ,comrel_producto_epo PRE, comcat_producto_nafin PN,com_parametrizacion_epo pe ");
				
			}else{
			 tablas.append(
					" comcat_epo e, comrel_if_epo IE, comrel_if_epo_x_producto IEP "+
					" ,comrel_producto_epo PRE, comcat_producto_nafin PN ");
			
			}
			//hint.append(" /*+ USE_NL(ie e param) */ " );
			if(!pantalla.equals("") &&pantalla.equals("AFOROEPO")){
				condicionJoin.append(
					" IE.ic_epo = E.ic_epo "+
					" AND E.ic_epo = PRE.ic_epo"+
					" AND E.ic_epo = pe.ic_epo"+
					" AND PRE.ic_producto_nafin = PN.ic_producto_nafin"+
					" AND IE.ic_if = IEP.ic_if "+
					
					" AND IE.ic_epo = IEP.ic_epo "
				);
			
			}else{
				condicionJoin.append(
					" IE.ic_epo = E.ic_epo "+
					" AND E.ic_epo = PRE.ic_epo"+
					" AND PRE.ic_producto_nafin = PN.ic_producto_nafin"+
					" AND IE.ic_if = IEP.ic_if "+
					" AND IE.ic_epo = IEP.ic_epo "
				);
			}
			
		} else if (this.clavePyme != null && !this.clavePyme.equals("")) {
		
			tablas.append(
					" comcat_epo E, comrel_pyme_epo PE, comrel_pyme_epo_x_producto PEP " );
			
			condicionJoin.append(
					" PE.ic_epo = E.ic_epo "+
					" AND PEP.ic_epo = pe.ic_epo "+
					" AND PEP.ic_pyme = pe.ic_pyme " );
		} else {
			tablas.append(
					" COMCAT_EPO E, COMREL_PRODUCTO_EPO PRE,COMCAT_PRODUCTO_NAFIN PN" );
			condicionJoin.append(
					" E.ic_epo = PRE.ic_epo "+
					" AND PRE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN ");
					
		}

		if (cdoctos != null && "S".equals(cdoctos)) {
			tablas.append(", dis_documento D ");
			condicionJoin.append(
					" AND D.ic_epo = E.ic_epo " );
			
		}

		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
		
		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(
				" SELECT " + hint + " DISTINCT " + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
				);
				
				
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y además se manda a llamar la implementación especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}
	
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		log.debug("qrySQL ::: "+qrySQL.toString());
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		StringBuffer condicionAdicional = new StringBuffer(128);
		
/*		try{
			if (this.claveIf == null || this.claveIf.equals("") ||
				this.tipoTasa  == null || this.tipoTasa.equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
			Integer.parseInt(this.claveIf);
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
*/		
		if (this.claveIf != null && !this.claveIf.equals("")) {
			if(!pantalla.equals("") &&pantalla.equals("AFOROEPO")){///para la pantalla aforo por epo 
				condicionAdicional.append(
					" E.CS_HABILITADO = ? " +
					" AND IE.cs_distribuidores IN(?,?) " +
					" AND IEP.CS_HABILITADO = ? " +
					" AND IEP.ic_producto_nafin = ? " +
					" AND PN.IC_PRODUCTO_NAFIN = ? " +
					
					" AND pe.CC_PARAMETRO_EPO = ? " +//////////7
					" AND pe.cg_vALOR = ? " +//////////////7
					
					" AND IE.ic_if = ? " );
			
			}else{
				condicionAdicional.append(
					" E.CS_HABILITADO = ? " +
					" AND IE.cs_distribuidores IN(?,?) " +
					" AND IEP.CS_HABILITADO = ? " +
					" AND IEP.ic_producto_nafin = ? " +
					" AND PN.IC_PRODUCTO_NAFIN = ? " +
					" AND IE.ic_if = ? " );
				
			}
					
			//agregar a variables bind estos valores
			super.addVariablesBind("S");
			super.addVariablesBind("S");
			super.addVariablesBind("R");
			super.addVariablesBind("S");
			super.addVariablesBind(new Integer(4));
			super.addVariablesBind(new Integer(4));
			if(!pantalla.equals("") &&pantalla.equals("AFOROEPO")){
				super.addVariablesBind("PUB_EPO_VENTA_CARTERA");
				super.addVariablesBind("S");
			}
			super.addVariablesBind(new Integer(this.claveIf));
			
			if(this.tipoCredito!=null && !this.tipoCredito.equals("")) {
				condicionAdicional.append(
						" AND NVL(PRE.cg_tipos_credito,PN.cg_tipos_credito) IN (?,?)" );
				super.addVariablesBind("A");
				super.addVariablesBind(this.tipoCredito);
			}
			
			super.addCondicionAdicional(condicionAdicional.toString());
			
		} else if (this.clavePyme != null && !this.clavePyme.equals("")) {
			condicionAdicional.append(
					" PE.ic_pyme = ? " +
					" and PE.cs_distribuidores in(?,?) "+
					" and E.cs_habilitado = ? "+
					" and PEP.ic_producto_nafin = ? " );
			
			super.addVariablesBind(new Integer(this.clavePyme));
			super.addVariablesBind("S");
			super.addVariablesBind("R");
			super.addVariablesBind("S");
			super.addVariablesBind(new Integer(4));
			
			if(this.tipoCredito!=null && !this.tipoCredito.equals("")) {
				condicionAdicional.append(
						" AND PEP.cg_tipo_credito = ? " );
				
				super.addVariablesBind(this.tipoCredito);
			}
			
			super.addCondicionAdicional(condicionAdicional.toString());
		} else {
			condicionAdicional.append(
					" E.cs_habilitado = ? "+
					" AND PRE.ic_producto_nafin = ? "+
					" AND PRE.cs_habilitado = ? " );
			
			super.addVariablesBind("S");
			super.addVariablesBind(new Integer(4));
			super.addVariablesBind("S");
			
			if(this.tipoCredito!=null && !this.tipoCredito.equals("")) {
				condicionAdicional.append(
						" and NVL(PRE.cg_tipos_credito,PN.cg_tipos_credito) in (?,?)" );
				super.addVariablesBind("A");
				super.addVariablesBind(this.tipoCredito);
			}
			super.addCondicionAdicional(condicionAdicional.toString());
		}

		if (cdoctos != null && "S".equals(cdoctos)) {
			condicionAdicional = new StringBuffer(128);
			if (this.estatusDocto != null && !this.estatusDocto.equals("")) {
				Collection listaIn = Comunes.explode(",", this.estatusDocto, java.lang.Integer.class);
				condicionAdicional.append( " d.ic_estatus_docto IN (" + Comunes.repetirCadenaConSeparador("?",",",listaIn.size()) + ")" );
				
				this.addVariablesBind(listaIn);
				super.addCondicionAdicional(condicionAdicional.toString());
			}
			if ("D".equals(this.tipoCredito)) {
				condicionAdicional = new StringBuffer(128);
				condicionAdicional.append(" d.ic_linea_credito_dm IS NOT NULL ");
				if (this.claveIf != null && !this.claveIf.equals("")) {
					condicionAdicional.append(
								" AND d.ic_linea_credito_dm IN(" +
								" 		SELECT ic_linea_credito_dm " +
								" 		FROM dis_linea_credito_dm " +
								" 		WHERE ic_if = ? )" );
					this.addVariablesBind(new Integer(this.claveIf));
				}
				super.addCondicionAdicional(condicionAdicional.toString());
			} else if ("C".equals(this.tipoCredito) || "F".equals(this.tipoCredito)) {
				condicionAdicional = new StringBuffer(128);
				condicionAdicional.append(" d.ic_linea_credito_dm IS NULL ");
				if (this.claveIf != null && !this.claveIf.equals("")) {
					condicionAdicional.append(
							" AND d.ic_linea_credito IN( " +
							" 		SELECT ic_linea_credito " +
							" 		FROM com_linea_credito " +
							" 		WHERE ic_if = ? )" );
					this.addVariablesBind(new Integer(this.claveIf));
				}
				super.addCondicionAdicional(condicionAdicional.toString());
			}
		}
	}

	public String getClaveIf() {
		return claveIf;
	}

	public void setClaveIf(String claveIf) {
		this.claveIf = claveIf;
	}


	public void setClavePyme(String clavePyme) {
		this.clavePyme = clavePyme;
	}


	public String getClavePyme() {
		return clavePyme;
	}
///pantalla
	public void setPantalla(String pantalla) {
		this.pantalla = pantalla;
	}


	public String getPantalla() {
		return pantalla;
	}

	public void setTipoCredito(String tipoCredito) {
		this.tipoCredito = tipoCredito;
	}


	public String getTipoCredito() {
		return tipoCredito;
	}


	public void setEstatusDocto(String estatusDocto) {
		this.estatusDocto = estatusDocto;
	}


	public String getEstatusDocto() {
		return estatusDocto;
	}


	public void setCdoctos(String cdoctos) {
		this.cdoctos = cdoctos;
	}


	public String getCdoctos() {
		return cdoctos;
	}




}