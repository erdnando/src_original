package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Catalogo de Pyme del producto de DISTRIBUIDORES
 *
 * Ejemplo de uso:
 * CatalogoPymeDistribuidores cat = new CatalogoPymeDistribuidores();
 * cat.setCampoClave("cp.ic_pyme"); y/o para el caso de obtener el numero de dist.  cat.setCampoClave("cg_num_distribuidor");
 * cat.setCampoDescripcion("cg_razon_social");
 * cat.setClaveEpo("1"); 			//opcional
 * cat.setHabilitado("S"); 		//opcional
 * cat.setTipoCredito("D"); 		//opcional
 * cat.setIntermediario("123");	//opcional
 * cat.setOrden("cg_razon_social");
 *
 * @author Hugo Vargas
 */
public class CatalogoPymeDistribuidores extends CatalogoAbstract {
	
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoPymeDistribuidores.class);
	
	StringBuffer tablas = new StringBuffer();
	
	private String claveEpo;
	private String tipoCredito;
	private String intermediario;
   private String cdoctos;
	private String estatusdocto;
	private String habilitado;
	private String clasifica = "";
	private boolean condicionBancaria=false;
	private String icIF;
	/*public CatalogoPymeDistribuidores() {
		super();
		super.setPrefijoTablaPrincipal("cp."); //from comcat_pyme cp
	}*/
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colecci�n de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();

		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer hint = new StringBuffer();
		StringBuffer condicionJoin = new StringBuffer();
		
		tablas.append(" comcat_pyme cp, comrel_pyme_epo cpe, comrel_pyme_epo_x_producto pexp ");
		condicionJoin.append("	cp.ic_pyme = cpe.ic_pyme "+
									"	AND cp.ic_pyme = pexp.ic_pyme "+
									"	AND cpe.ic_epo = pexp.ic_epo ");
		
		if (this.intermediario != null && !this.intermediario.equals("")){
			tablas.append(" , com_linea_credito lc ");
			condicionJoin.append("	AND lc.ic_pyme = cp.ic_pyme ");
		}
		if (this.cdoctos != null && this.cdoctos.equals("S")){
			tablas.append(", dis_documento D ");
			condicionJoin.append(" AND D.ic_pyme = cp.ic_pyme ");
		}

		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
		
		StringBuffer qrySQL = new StringBuffer();
		if (this.clasifica.equals("")){
			qrySQL.append(
					" SELECT " + hint + " DISTINCT " + super.getCampoClave() + " AS CLAVE, " +
					super.getCampoDescripcion() + " AS DESCRIPCION " +
					" FROM " + tablas +
					" WHERE " + condicionJoin
					);
		}else{
			qrySQL.append(
					" SELECT " + hint + " DISTINCT cpe.cg_num_distribuidor, " + super.getCampoClave() + " AS CLAVE, " +
					super.getCampoDescripcion() + " AS DESCRIPCION " +
					" FROM " + tablas +
					" WHERE " + condicionJoin
					);
		}

		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y adem�s se manda a llamar la implementaci�n especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}
	
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		log.debug("qrySQL ::: "+qrySQL.toString());
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				if (this.clasifica.equals("")){
					elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				}else{
					elementoCatalogo.setDescripcion(registros.getString("CG_NUM_DISTRIBUIDOR")+" "+registros.getString("DESCRIPCION"));
				}
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		StringBuffer condicionAdicional = new StringBuffer(128);

		condicionAdicional.append( "	cpe.cs_distribuidores in(?,?) "+
											"	AND pexp.ic_producto_nafin = ? "+
											"	AND cp.cs_invalido != ? "	);
		super.addVariablesBind("S");
		super.addVariablesBind("R");
		super.addVariablesBind(new Integer(4));
		super.addVariablesBind("S");

		if (this.claveEpo != null && !this.claveEpo.equals("")) {
			if( Comunes.esUnaLista(claveEpo) ){
				java.util.StringTokenizer st = new java.util.StringTokenizer(claveEpo, ",");
				int x=0;
				while (st.hasMoreElements()) {
					if(x==0){
						condicionAdicional.append(" AND pexp.ic_epo in (?");
					}else{
						condicionAdicional.append(",?");
					}
					super.addVariablesBind(st.nextElement());
					x++;
				}
				condicionAdicional.append(" ) ");
			}else{
				condicionAdicional.append(" AND pexp.ic_epo = ? ");
				super.addVariablesBind(claveEpo);
			}
		}
		
		if (condicionBancaria){
			condicionAdicional.append(" AND cp.ic_pyme NOT IN ( SELECT cbp.ic_pyme "   +
								" FROM comrel_cuenta_bancaria_x_prod cbp, com_cuenta_aut_x_prod cap "   +
								" WHERE cbp.ic_cuenta_bancaria = cap.ic_cuenta_bancaria "   +
								" AND cbp.ic_epo = pexp.ic_epo" +" AND cbp.ic_if = ? ) ");
						super.addVariablesBind(icIF);
		}

		if (this.habilitado !=null && !this.habilitado.equals("")){
			condicionAdicional.append(" AND pexp.cs_habilitado in (?) ");
			super.addVariablesBind(habilitado);
		}else{
			condicionAdicional.append(" AND pexp.cs_habilitado in (?,?) ");
			super.addVariablesBind("S");
			super.addVariablesBind("N");
		}
		if (this.tipoCredito != null && !this.tipoCredito.equals("")){
			condicionAdicional.append(" AND pexp.cg_tipo_credito = ? ");
			super.addVariablesBind(tipoCredito);
		}
		if (this.intermediario != null && !this.intermediario.equals("")){
			condicionAdicional.append(	"	AND lc.ic_if = ? "+
												"	AND lc.ic_producto_nafin = ? "+
												"	AND TRUNC (lc.df_vencimiento_adicional) > TRUNC (SYSDATE) "+
												"	AND lc.cg_tipo_solicitud IN (?, ?) ");
			super.addVariablesBind(intermediario);
			super.addVariablesBind(new Integer(4));
			super.addVariablesBind("I");
			super.addVariablesBind("R");
		}
		if (this.cdoctos != null && this.cdoctos.equals("S")){
			if (this.estatusdocto != null && !this.estatusdocto.equals("")){
				condicionAdicional.append(" AND d.ic_estatus_docto in (?)");
				super.addVariablesBind(estatusdocto);
			}
			if (this.tipoCredito !=null && this.tipoCredito.equals("D")){
				condicionAdicional.append(" AND d.ic_linea_credito_dm IS NOT NULL");
			}else if (this.tipoCredito !=null && this.tipoCredito.equals("C")){
				condicionAdicional.append(" AND d.ic_linea_credito_dm IS NULL");
			}
		}
		if (this.clasifica.equals("Cl")){
			condicionAdicional.append(" AND (   pexp.ic_clasificacion IS NOT NULL OR pexp.ig_dias_maximo IS NOT NULL) ");
		}
		if (this.clasifica.equals("R")){
			condicionAdicional.append(" AND (   pexp.ic_clasificacion IS NULL OR pexp.ig_dias_maximo IS NULL) ");
		}

		super.addCondicionAdicional(condicionAdicional.toString());
	}

	/**
	 * Establece la clave de la epo.
	 * @param claveEpo Clave de la epo (ic_epo)
	 */
	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}

	/**
	 * Establece el tipo de cr�dito
	 * @param claveEpo Clave de la epo (ic_epo)
	 */
	public void setTipoCredito(String tipoCredito) {
		this.tipoCredito = tipoCredito;
	}

	public void setIntermediario(String intermediario) {
		this.intermediario = intermediario;
	}

	public void setCdoctos(String cdoctos) {
		this.cdoctos = cdoctos;
	}
        
	public void setEstatusDocto(String estatusdocto) {
		this.estatusdocto = estatusdocto;
	}
	
	public void setHabilitado(String habilitado){
		this.habilitado = habilitado;
	}

	public void setClasifica(String clasifica){
		this.clasifica = clasifica;
	}


	public void setCondicionBancaria(boolean condicionBancaria) {
		this.condicionBancaria = condicionBancaria;
	}


	public boolean isCondicionBancaria() {
		return condicionBancaria;
	}


	public void setIcIF(String icIF) {
		this.icIF = icIF;
	}


	public String getIcIF() {
		return icIF;
	}

}