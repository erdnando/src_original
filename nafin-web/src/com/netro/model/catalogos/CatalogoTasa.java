package com.netro.model.catalogos;


/**
 * Catalogo de Tasa (comcat_tasa)
 *
 * Ejemplo de uso:
 * CatalogoTasa cat = new CatalogoTasa();
 * cat.setCampoClave("ic_tasa");
 * cat.setCampoDescripcion("cd_nombre");
 * cat.setDisponible("S");
 * cat.setHabilitado("S");
 * cat.setMoneda("1");
 * cat.setOrden("cd_nombre");
 * List l = cat.getListaElementos();  //Lista con elementos ElementoCatalogo
 *
 * @author Mariana Vidal
 */
public class CatalogoTasa extends CatalogoSimple  {


  private String disponible;
  private String habilitado;
  private String moneda;
	public CatalogoTasa() {
		super.setTabla("comcat_tasa");
	}
	/**
	 * Establece las condiciones adicionales necesarias para contemplar el
	 * los parametros adicionales especificados
	 */
	protected void setCondicionesAdicionales() {
		//Se valida que esten los campos requeridos:
		String condicionAdicional = "";
		if (this.disponible != null && !this.disponible.equals("")){
			condicionAdicional = " cs_disponible = ? ";
			super.addCondicionAdicional(condicionAdicional);
			super.addVariablesBind(this.disponible);
		}
		if (this.habilitado != null && !this.habilitado.equals("")){
			condicionAdicional = " cs_creditoelec = ? ";
			super.addCondicionAdicional(condicionAdicional);
			super.addVariablesBind(this.habilitado);
		}
		//Migracion 2013 ic_moneda
		if (this.moneda != null && !this.moneda.equals("")){
			condicionAdicional = " ic_moneda = ? ";
			super.addCondicionAdicional(condicionAdicional);
			super.addVariablesBind(this.moneda);
		}
		//agregar a variables bind estos valores
	}

  public String getDisponible()
  {
    return disponible;
  }

  /**
   * Establece si la tasa esta disponible o no 
   * @param disponible Si el estado esta como disponible es = 'S'
   */
  public void setDisponible(String disponible)
  {
    this.disponible = disponible;
  }
	public void setMoneda (String moneda) {
		this.moneda = moneda;
	}
  /**
   * Establece si la tasa esta habilitada o no 
   * @param disponible Si el estado esta como disponible es = 'S'
   */
  public void setHabilitado(String habilitado)
  {
    this.habilitado = habilitado;
  }
	
	
}