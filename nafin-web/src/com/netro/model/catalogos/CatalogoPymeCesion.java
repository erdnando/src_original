package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


/**
 * Catalogo de Pymes (comcat_pyme)
 *
 * Ejemplo de uso:
 * CatalogoEPOCesion cat = new CatalogoEPOCesion();
 * cat.setCampoClave("ic_pyme");
 * cat.setCampoDescripcion("cg_razon_social");
 * @author Deysi Laura Hernandez Contreras */ 
 
 
public class CatalogoPymeCesion extends CatalogoAbstract {


	//Variable para enviar mensajes al log.
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoPymeCesion.class);	
	private String clavesEpo = "";
	

	StringBuffer tablas = new StringBuffer();	
	private String claveIF;


	public CatalogoPymeCesion() {
			
	}  

	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colecci�n de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();

		try{
			if (getCampoClave() == null || getCampoClave().equals("") ||
				getCampoDescripcion() == null || getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer condicionJoin = new StringBuffer();
		StringBuffer hint = new StringBuffer();
		
		tablas.append(" cder_solicitud cds,  comcat_pyme pyme" );
		condicionJoin.append(" cds.ic_pyme = pyme.ic_pyme ");
		
		if(this.clavesEpo != null && !this.clavesEpo.equals("")){
			condicionJoin.append(" AND cds.ic_estatus_solic IN (9, 11, 12, 15, 16 ) ");
					
		} 
		 
		if(this.claveIF != null && !this.claveIF.equals("")){
		
			condicionJoin.append(" AND cds.ic_estatus_solic IN (7, 9, 10, 12 ) ");
					
		} 	
		
		
		
				
		StringBuffer qrySQL = new StringBuffer();

		qrySQL.append(
				" SELECT " + hint + " DISTINCT " + getCampoClave() + " AS CLAVE, " +
				getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin			 
			);
     

		if(this.clavesEpo != null && !this.clavesEpo.equals("")){			
			qrySQL.append(" AND cds.ic_epo = ?  ");
			addVariablesBind(clavesEpo);
		}

		if(this.claveIF != null && !this.claveIF.equals("")){			
			qrySQL.append(" AND (cds.ic_if = ? OR cds.ic_if = (select ic_if from comcat_cesionario where ic_if_rep2 = ? ))");
			addVariablesBind(claveIF);
			addVariablesBind(claveIF);
		}
		

		if (getOrden() == null || getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + getOrden() );
		} 
     
		log.debug(" ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: ");
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+getValoresBindCondicion());
		log.debug(" ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: ");

		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}  
		}
  
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}

		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}


	/**
	 * @deprecated Usar getClaveEpo()
	 */
	public String getNoepo() {
		return this.clavesEpo;
	}

	/**
	 * @deprecated Usar setClaveEpo(String claveEpo)
	 */
	public void setNoepo(String noepo) {
		this.clavesEpo = noepo;
	}

	/**
	 * Especifica la epo con la que la pyme tiene relaci�n.
	 * @param claveEpo Clave de la epo (ic_epo)
	 */
	public void setClaveEpo(String claveEpo) {
		this.clavesEpo = claveEpo;
	}


	public String getClaveEpo() {
		return this.clavesEpo;
	}



	/**
	 * Establece un conjunto de EPOS con las que la Pyme debe tener relacion
	 * @param clavesEpo Claves de epo sepradas por coma
	 */
	public void setClavesEpo(String clavesEpo) {
		this.clavesEpo = clavesEpo;
	}

 
	public String getClavesEpo() {
		return clavesEpo;
	}

	public String getClaveIF() {
		return claveIF;
	}

	public void setClaveIF(String claveIF) {
		this.claveIF = claveIF;
	}





}