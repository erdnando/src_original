package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


/**
 * Catalogo de IFs del producto de DISTRIBUIDORES
 *
 * Ejemplo de uso:
 * CatalogoIFDistribuidores cat = new CatalogoIFDistribuidores();
 * cat.setCampoClave("ic_if");
 * cat.setCampoDescripcion("cg_razon_social");
 * cat.setClaveEpo("1"); //opcional
 * * cat.setProducto("1"); //opcional, solo para el caso del cat�logo en perfil nafin
 * cat.setOrden("cg_razon_social");
 *
 * @author Gilberto Aparicio
 */
public class CatalogoIFDistribuidores extends CatalogoAbstract {
	
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoIFDistribuidores.class);
	
	StringBuffer tablas = new StringBuffer();
	
	private String claveEpo;
	private String producto = "4";
	
	public CatalogoIFDistribuidores() {
		super();
		super.setPrefijoTablaPrincipal("ci."); //from comcat_if ci
	}
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colecci�n de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer hint = new StringBuffer();
	
		StringBuffer condicionJoin = new StringBuffer();
		
		if (this.claveEpo == null || this.claveEpo.equals("")) {
	
			tablas.append(" comcat_if ci, comrel_producto_if rpi ");

			//hint.append(" /*+ USE_NL(ie e param) */ " );

			condicionJoin.append(" ci.ic_if = rpi.ic_if ");
		} else {
		
			tablas.append(
					" comcat_if ci, comrel_if_epo rie, comrel_if_epo_x_producto iexp " );
			
			condicionJoin.append(
					" ci.ic_if = rie.ic_if "+
					" and iexp.ic_if = ci.ic_if "+
					" and iexp.ic_epo = rie.ic_epo ");
		}

		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
		
		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(
				" SELECT " + hint + " DISTINCT " + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
				);
				
				
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y adem�s se manda a llamar la implementaci�n especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}
	
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		StringBuffer condicionAdicional = new StringBuffer(128);
		
/*		try{
			if (this.claveIf == null || this.claveIf.equals("") ||
				this.tipoTasa  == null || this.tipoTasa.equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
			Integer.parseInt(this.claveIf);
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
*/		
		if (this.claveEpo == null || this.claveEpo.equals("")) {
	
			condicionAdicional.append(" rpi.ic_producto_nafin = ? ");
			super.addVariablesBind(this.producto);

		} else {
	
			condicionAdicional.append(
					" rie.ic_epo = ? "+
					" AND iexp.ic_producto_nafin = ? "+
					" AND rie.cs_distribuidores = ? ");
			super.addVariablesBind(new Integer(this.claveEpo));
			super.addVariablesBind(new Integer(4));
			super.addVariablesBind("S");
		}
					
		super.addCondicionAdicional(condicionAdicional.toString());
	}


	/**
	 * Establece la clave de la epo para obtener los intermediarios con los
	 * que trabaja
	 * @param claveEpo Clave de la epo (ic_epo)
	 */
	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}



}