package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class CatalogoFinanciera  extends CatalogoAbstract {
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(CatalogoIF.class);
	
	private String clave = "";
	private String descripcion = "";
	private String tipo = "";
	
	/**
	 * Obtiene una lista de elementos de tipo ElementoCatalogo
	 * @return regresa una colecci�n de elementos obtenidos
	 */	
	public List getListaElementos()
	{
		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		StringBuffer tablas = new StringBuffer();
		StringBuffer condicion = new StringBuffer();		
		String qryCatBenef;
		try
		{
			con.conexionDB();
			Registros regCatFinanciera	=	null;

			tablas.append(" comcat_financiera f ");
			
			if(!tipo.equals("")){
				condicion.append(" WHERE f.ic_tipo_financiera = "+tipo+" ");
			}
		
			condicion.append(" ORDER BY  "+this.descripcion);
			

			qryCatBenef = "SELECT "+this.clave+" as CLAVE,"+this.descripcion+" as DESCRIPCION "+
					" FROM "+tablas+ condicion;

			log.info("qryCatBenef>>>"+qryCatBenef);
			regCatFinanciera = con.consultarDB(qryCatBenef);
			while(regCatFinanciera.next()) {
					ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					elementoCatalogo.setClave(regCatFinanciera.getString("CLAVE"));
					elementoCatalogo.setDescripcion(regCatFinanciera.getString("DESCRIPCION"));
					coleccionElementos.add(elementoCatalogo);
			}
		
		
		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
				
		return coleccionElementos;													
		
	}	


	public void setClave(String clave) {
		this.clave = clave;
	}


	public String getClave() {
		return clave;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setTipo(String tipo) {
		this.tipo = tipo;
	}


	public String getTipo() {
		return tipo;
	}

}