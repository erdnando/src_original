package com.netro.model.catalogos;

import com.netro.appinit.InicializacionContextListener;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Catalogo de IFs
 *
 * Ejemplo de uso:
 * CatalogoIF cat = new CatalogoIF();
 * cat.setCampoClave("ic_if");
 * cat.setCampoDescripcion("cg_razon_social");
 * cat.setClaveEpo("1"); //opcional
 * cat.setMandatoDocumentos("S");
 * cat.setOrden("cg_razon_social");
 *
 * @author Erik Barba
 */
public class CatalogoIF  extends CatalogoAbstract {

	StringBuffer tablas = new StringBuffer();

	private String claveEpo			= "";
	private String g_ic_if			= "";
	private String g_vobonafin		= "";
	private String g_piso			= "";
	private String g_tipo			= "";
	private String g_tipo_piso		= "";
	private String g_producto		= "";
	private String g_dispersion	= "";;
	private String g_fondeo			= "";
	private String g_orden			= "";
	private String sConsEstatusAfiliacion = "";
	
	private String convenio_unico	= "";//FODEA 020 - 2009
	private String mandatoDocumentos = "";	//FODEA 041 - 2009 ACF
	private String cesionDerechos = ""; //FODEA 37- 2009 
	private String csBeneficiario = ""; //FODEA 032-2010 FVR
	private String g_tieneClaveSIAG = ""; //FODEA 002 - 2014 JSHD
	
	private String cveSubasta = ""; //FODEA 032-2010 FVR
	private String cveSegmento = ""; //FODEA 032-2010 FVR
	
	private String listaClavesIF = ""; //FODEA ### - 2013 - ADMIN NAFIN - Migraci�n Monitor JSHD
	
	private String opera_fideicomiso = ""; // FODEA 17 - 2013 ADMIN NAFIN
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(CatalogoIF.class);
	
	public CatalogoIF() {
		super();
		super.setPrefijoTablaPrincipal("ci."); //from comcat_if ci
	}
	
	/**
	 * establece utilizar la lista de elementos del Catalogo
	 * @deprecated Utilizar getListaElementos()
	 */
	public List getListaElementosEmp() {
		return this.getListaElementos();
	}	



	/**
	 * Obtiene una lista de elementos de tipo ElementoCatalogo
	 * @return regresa una colecci�n de elementos obtenidos
	 */	
	public List getListaElementos(){
		log.info("getListaElementos(E)");

		List coleccionElementos = new ArrayList();
		
		StringBuffer condicionJoin = new StringBuffer();
		
		StringBuffer hint = new StringBuffer();
		StringBuffer orden = new StringBuffer();
		StringBuffer qryCatIf = new StringBuffer();
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
					super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
		
		tablas.append(" comcat_if ci, comrel_if_epo cie, comrel_producto_epo cpe ");
		condicionJoin.append(
				" ci.ic_if = cie.ic_if " +
				" AND cie.ic_epo = cpe.ic_epo ");
		
		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
		
		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(
				" SELECT " + hint + " DISTINCT " + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
				);
				
				
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y adem�s se manda a llamar la implementaci�n especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}
	
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
		
	}


	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		String condicionAdicional = "";
		
/*		try{
			if (this.claveEpo == null || this.claveEpo.equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
			Integer.parseInt(this.claveEpo);
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
*/		

		if(this.sConsEstatusAfiliacion.equals("S")  ){
			condicionAdicional =
				" ci.cs_habilitado = ? " +
				" AND cie.cs_vobo_nafin = ? ";
		}else{
			condicionAdicional =
				" ci.cs_habilitado = ? " +
				" AND cie.cs_vobo_nafin = ? " +
				" AND cie.ic_if in (SELECT DISTINCT rm.ic_if FROM comrel_if_epo_moneda rm WHERE rm.ic_epo = cie.ic_epo) ";
		}
		super.addVariablesBind("S");
		super.addVariablesBind("S");
		super.addCondicionAdicional(condicionAdicional.toString());
		
		
		if(this.claveEpo!=null  && !this.claveEpo.equals("")){
			condicionAdicional = " cie.ic_epo = ? ";
			super.addVariablesBind(new Integer(this.claveEpo)); 
			super.addCondicionAdicional(condicionAdicional);
		}
		
		if(this.convenio_unico != null && !this.convenio_unico.equals("")){
			condicionAdicional = " ci.cs_convenio_unico = ? ";
			super.addVariablesBind(this.convenio_unico); 
			super.addCondicionAdicional(condicionAdicional);
		}
		
		if(this.mandatoDocumentos != null && !this.mandatoDocumentos.equals("")){
			condicionAdicional = " ci.cs_mandato_documento = ? ";
			super.addVariablesBind(this.mandatoDocumentos); 
			super.addCondicionAdicional(condicionAdicional);
		}

		if(this.cesionDerechos != null && !this.cesionDerechos.equals("")){
			condicionAdicional = " ci.cs_cesion_derechos = ? ";
			super.addVariablesBind(this.cesionDerechos); 
			super.addCondicionAdicional(condicionAdicional);
		}
		
		if(!"".equals(g_piso)){
			condicionAdicional = "  ci.ig_tipo_piso =  ?  ";   
			super.addVariablesBind(this.g_piso); 
			super.addCondicionAdicional(condicionAdicional);
		}
			
	}

	
	/**
	 * Obtiene una lista de elementos de tipo ElementoCatalogo
	 * @deprecated este metodo deberia sacarse y crear otro CatalogoIF. se deja por motivos de compatibilidad, aunque se tiene que parchar para que funcione
	 * @return regresa una colecci�n de elementos obtenidos
	 */	
	public List getListaElementosGral()
	{
		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		StringBuffer tablas = new StringBuffer();
		StringBuffer condicion = new StringBuffer();		
		String qryCatIf;
		try
		{
			con.conexionDB();
			Registros regCatIf	=	null;
			
			if (!claveEpo.equals("")) {

				tablas.append(
					" comcat_if I, comrel_if_epo IE ");
				condicion.append(
					" WHERE IE.ic_if=I.ic_if "+
					" AND I.cs_habilitado='S' "+
					" AND IE.ic_epo="+claveEpo+
					" AND IE.cs_vobo_nafin = ");
				condicion.append( (!"".equals(g_vobonafin))?g_vobonafin:" 'S' ");
				
				if(!"".equals(csBeneficiario)){
					condicion.append(" AND IE.cs_beneficiario = '"+csBeneficiario+"' ");
				}

			} else {
				tablas.append( 
					" comcat_if I ");
				condicion.append(
					" WHERE I.cs_habilitado='S' ");
			}

			if(!"".equals(g_piso)){
				condicion.append(" AND I.ig_tipo_piso = "+g_piso+" ");
			}
			if(!"".equals(g_tipo)){
				condicion.append(" AND I.cs_tipo = '"+g_tipo+"' ");
			}
			
			if(!"".equals(g_ic_if)) {
				condicion.append(" AND I.ic_if in("+g_ic_if+") ");
			}
			
			if(        "S".equals(g_tieneClaveSIAG) ){
				condicion.append(" AND I.IC_IF_SIAG IS NOT NULL ");
			} else if( "N".equals(g_tieneClaveSIAG) ){
				condicion.append(" AND I.IC_IF_SIAG IS NULL     ");
			}
			
			if(!"".equals(g_producto)){
				tablas.append(" , comrel_producto_if cpi ");
				condicion.append( 
			  		" AND cpi.ic_if = I.ic_if "+
			  		" AND cpi.ic_producto_nafin = "+g_producto+" ");
				if(!"".equals(g_dispersion)){
					condicion.append(" AND cpi.cg_dispersion = '"+g_dispersion+"' ");
				}
			}
						
      
			if(!"".equals(g_fondeo)){
			
				condicion.append( 
					" AND EXISTS( " +
					" 		SELECT rie.ic_if " +
					" 		FROM comrel_if_epo_x_producto rie, "+
					" 			comcat_epo epo " +
					" 		WHERE " +
					" 			rie.ic_if = I.ic_if " +
					" 			AND rie.ic_epo = epo.ic_epo " +
					" 			AND epo.ic_banco_fondeo = " + g_fondeo + ")");
			}
			String parcheCampoClave = super.getCampoClave().substring(super.getCampoClave().indexOf(".") + 1);	//quita el prefijo predeterminado "ci."
			String parcheCampoDescripcion = super.getCampoDescripcion().substring(super.getCampoDescripcion().indexOf(".") + 1); //quita el prefijo predeterminado "ci."
			
			if (!g_orden.equals("")) {
				condicion.append(" ORDER BY "+g_orden+" ");
			} else {
				condicion.append(" ORDER BY  "+parcheCampoDescripcion);
			}
			qryCatIf = "SELECT "+parcheCampoClave+" as CLAVE,"+parcheCampoDescripcion+" as DESCRIPCION "+
					" FROM "+tablas+ condicion;

			log.info("qryCatIf>>>"+qryCatIf);
			regCatIf = con.consultarDB(qryCatIf);
			while(regCatIf.next()) {
					ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					elementoCatalogo.setClave(regCatIf.getString("CLAVE"));
					elementoCatalogo.setDescripcion(regCatIf.getString("DESCRIPCION"));
					coleccionElementos.add(elementoCatalogo);
			}
		
		
		} catch(Exception e) {
			throw new AppException("El catalogo no pudo ser generado", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
				
		return coleccionElementos;													
		
	}	
	
	/**
	 * Obtiene una lista de elementos de tipo ElementoCatalogo
	 * @return regresa una colecci�n de elementos obtenidos
	 */
	public List getListaElementosIF()
	{
		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		StringBuffer tablas = new StringBuffer();
		StringBuffer condicion = new StringBuffer();		
		String qryCatIf;
		try
		{
			con.conexionDB();
			Registros regCatIf	=	null;
			
			

				tablas.append(
					" comcat_if CI, comcat_epo E, comrel_if_epo CIE, comrel_producto_if CPI ");
				condicion.append(
					" WHERE CI.cs_habilitado='S' and CIE.ic_if=CI.ic_if and CIE.ic_epo = E.ic_epo " +
					"  and CPI.ic_if = CI.ic_if  and CPI.ic_producto_nafin = 1 ");
					
				
				
				if(!"".equals(g_fondeo)){
					condicion.append(" and E.ic_banco_fondeo= '"+g_fondeo+"' ");
				}
				
				if(!"".equals(claveEpo)){
					condicion.append(" and E.ic_epo = '"+claveEpo+"' ");
				}
				
				if(!"".equals(opera_fideicomiso)){
					condicion.append(" and CI.cs_opera_fideicomiso = 'N' ");
				}
				
				condicion.append(" ORDER BY 2");
			
			//String parcheCampoClave = super.getCampoClave().substring(super.getCampoClave().indexOf(".") + 1);	//quita el prefijo predeterminado "ci."
			//String parcheCampoDescripcion = super.getCampoDescripcion().substring(super.getCampoDescripcion().indexOf(".") + 1); //quita el prefijo predeterminado "ci."
	
			
			qryCatIf = "SELECT distinct "+super.getCampoClave()+" as CLAVE,"+super.getCampoDescripcion()+" as DESCRIPCION "+
					" FROM "+tablas+ condicion;

			log.info("qryCatIf>>>"+qryCatIf);
			regCatIf = con.consultarDB(qryCatIf);
			while(regCatIf.next()) {
					ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					elementoCatalogo.setClave(regCatIf.getString("CLAVE"));
					elementoCatalogo.setDescripcion(regCatIf.getString("DESCRIPCION"));
					coleccionElementos.add(elementoCatalogo);
			}
		
		
		} catch(Exception e) {
			throw new AppException("El catalogo no pudo ser generado", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
				
		return coleccionElementos;													
		
	}	
	/**
	 * 
	 * @deprecated este metodo deberia sacarse y crear otro CatalogoIF. se deja por motivos de compatibilidad	
	 * 
	 */
	public List getListaElementosSubasta() throws AppException {
		log.info("getListaElementosSubasta(I)");
		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		StringBuffer tables = new StringBuffer();
		
		StringBuffer condiciones = new StringBuffer();
		StringBuffer orden = new StringBuffer();
		StringBuffer qryCatIf = new StringBuffer();
		
	
		try{
			con.conexionDB();
			
			Registros regCatIf	=	null;
			
			tables.append(" COMCAT_IF CI");
			/*
			 * "SELECT   ci.ic_if, ci.cg_razon_social " +
				"    FROM gti_postura_if_detalle gpi, comcat_if ci " +
				"   WHERE gpi.ic_if = ci.ic_if AND gpi.ic_subasta = 68 " +
				"GROUP BY ci.ic_if, ci.cg_razon_social " +
				"ORDER BY ci.ic_if "
			 */
			
			
			tables.append(", gti_postura_if_detalle gpi ");
			
			condiciones.append(" WHERE gpi.ic_if = ci.ic_if ");
			condiciones.append(" AND gpi.ic_subasta = "+this.cveSubasta+" ");
			condiciones.append(" AND gpi.ic_segmento = "+this.cveSegmento+" ");

			
			orden.append(" GROUP BY ci.ic_if, ci.cg_razon_social ");
			orden.append(" ORDER BY CI.CG_RAZON_SOCIAL ");

			qryCatIf.append(" SELECT DISTINCT " + super.getCampoClave() + " as CLAVE, " + super.getCampoDescripcion() + " as DESCRIPCION ");
			qryCatIf.append(" FROM " + tables );
			qryCatIf.append(condiciones);
			qryCatIf.append(orden);
			
			log.debug("..:: qryCatIf : "+qryCatIf.toString());

			regCatIf = con.consultarDB(qryCatIf.toString());

			while(regCatIf.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(regCatIf.getString("CLAVE"));
				elementoCatalogo.setDescripcion(regCatIf.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
			}
		}catch(Exception e){
			throw new AppException("Error al obtener los elementos", e);
		}finally{
			if (con.hayConexionAbierta()){con.cierraConexionDB();}
		}
		log.info("getListaElementosSubasta(F)");
		return coleccionElementos;		
	}
	
	
	/**
	 * @deprecated usar getCampoClave
	 * @return 
	 */
	public String getClave()
	{
		return super.getCampoClave();
	}
	
	/**
	 * @deprecated Usar getClaveEpo
	 */
	public String getIc_epo()
	{
		return this.claveEpo;
	}
	/*****mETODOS PARA CATALGO GENERAL*****/
	public String getG_ic_if(){ return g_ic_if; }
	public String getG_vobonafin(){ return g_vobonafin; }
	public String getG_piso(){ return g_piso; }
	public String getG_tipo(){ return g_tipo; }
	public String getG_tipo_piso(){ return g_tipo_piso; }
	public String getG_producto(){ return g_producto; }
	public String getG_dispersion(){ return g_dispersion; }
	public String getG_fondeo(){ return g_fondeo; }
	public String getG_orden(){ return g_orden; }
	public String getG_convenio_unico(){ return convenio_unico; }//FODEA 020 - 2009
	
	/**
	 * @deprecated usar getMandatoDocumentos()
	 */
	public String getG_cs_mandato_documento(){ return mandatoDocumentos; }//FODEA 041 - 2009 ACF
	/**
	 * @deprecated usar getCesionDerechos()
	 */
	public String getG_cs_cesion_derechos(){ return cesionDerechos; }//FODEA 37- 2009 ACF
	
	/**
	 * Establece la clave(id) que se va obtener en la consulta
	 * @deprecated usar setCampoClave
	 */	
	public void setClave(String clave)
	{
		super.setCampoClave(clave);
	}

	/**
	 * Establece la descripcion  que se va obtener en la consulta
	 * @deprecated usar setCampoDescripcion
	 */		
	public void setDescripcion(String descripcion)
	{
		super.setCampoDescripcion(descripcion);
	}

	/**
	 * Establece valor para indicar algun valor en especifico a consultar
	 * @deprecated usar setValoresCondicionIn
	 */	
	public void setSeleccion(String seleccion)
	{
		super.setValoresCondicionIn(seleccion, Integer.class);
	}
	
	/**
	 * Establece el id de la EPO para condicion de la consulta
	 * @deprecated usar setClaveEpo
	 */	
	public void setIc_epo(String ic_epo)
	{
		this.claveEpo = ic_epo;
	}
	
/*********************************************************/
	public void setG_ic_if(String g_ic_if){
		this.g_ic_if = g_ic_if;
	}
	public void setG_vobonafin(String g_vobonafin){
		this.g_vobonafin = g_vobonafin;
	}
	public void setG_piso(String g_piso){
		this.g_piso = g_piso;
	}
	public void setG_tipo(String g_tipo){
		this.g_tipo = g_tipo;
	}
	public void setG_tipo_piso(String g_tipo_piso){
		this.g_tipo_piso = g_tipo_piso;
	}
	public void setG_producto(String g_producto){
		this.g_producto = g_producto;
	}
	public void setG_dispersion(String g_dispersion){
		this.g_dispersion = g_dispersion;
	}
	public void setG_fondeo(String g_fondeo){
		this.g_fondeo = g_fondeo;
	}
	public void setG_orden(String g_orden){
		this.g_orden = g_orden;
	}
	public void setG_convenio_unico(String convenio_unico){//FODEA 020 - 2009
		this.convenio_unico = convenio_unico;
	}
	
	public void setFideicomiso(String opera_fideicomiso){//FODEA 17 - 2013
		this.opera_fideicomiso = opera_fideicomiso;
	}
	/**
	 * @deprecated usar setMandatoDocumentos()
	 */
	public void setG_cs_mandato_documento(String cs_mandato_documento){//FODEA 041 - 2009 ACF
		this.mandatoDocumentos = cs_mandato_documento;
	}
	/**
	 * @deprecated usar setCesionDerechos()
	 */
	public void setG_cs_cesion_derechos(String cs_cesion_derechos){//FODEA 37 - 2009 
		this.cesionDerechos = cs_cesion_derechos;
	}


	public void setCsBeneficiario(String csBeneficiario) {
		this.csBeneficiario = csBeneficiario;
	}


	public String getCsBeneficiario() {
		return csBeneficiario;
	}


	public void setCveSubasta(String cveSubasta) {
		this.cveSubasta = cveSubasta;
	}


	public String getCveSubasta() {
		return cveSubasta;
	}


	public void setCveSegmento(String cveSegmento) {
		this.cveSegmento = cveSegmento;
	}


	public String getCveSegmento() {
		return cveSegmento;
	}


	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}


	public String getClaveEpo() {
		return claveEpo;
	}


	public void setCesionDerechos(String cesionDerechos) {
		this.cesionDerechos = cesionDerechos;
	}


	public String getCesionDerechos() {
		return cesionDerechos;
	}


	public void setMandatoDocumentos(String mandatoDocumentos) {
		this.mandatoDocumentos = mandatoDocumentos;
	}


	public String getMandatoDocumentos() {
		return mandatoDocumentos;
	}
	
	/**
	 *
	 * Obtiene una lista de IF (de tipo <tt>ElementoCatalogo</tt>) que est�n 
	 * habilitadas
	 * PANTALLA: ADMIN NAFIN - ADMINISTRACION - DISPERSION - MONITOR (Tipo de Monitoreo: INFONAVIT)
	 * @throws AppException
	 *
	 * @return regresa una colecci�n de elementos obtenidos
	 *
	 * @author jshernandez
	 * @since  F### - 2013 - ADMIN NAFIN - Migraci�n Monitor; 21/06/2013 04:24:34 p.m.
	 *
	 */
	public List getListaElementosMonitorDispersionINFONAVIT(){

		AccesoDB 		con 						= new AccesoDB();
		List 				coleccionElementos 	= new ArrayList();
		StringBuffer 	tablas 					= new StringBuffer();
		StringBuffer 	condicion 				= new StringBuffer();		
		String 			qryCatIf					= "";
		String 			orden;
		
		try {
			
			con.conexionDB();
			
			Registros regCatIf	=	null;
						
			tablas.append(
				" comcat_if CI "
			);
			condicion.append(
				" WHERE " + 
				"  CI.cs_habilitado = 'S' "
			);
			if( this.listaClavesIF != null && !"".equals(this.listaClavesIF) ){
				condicion.append(" and CI.ic_if in ("+ this.listaClavesIF+") "); 
			}
			orden = " ORDER BY 2";
 
			qryCatIf = 
				"SELECT "     +
					super.getCampoClave()       +" as CLAVE,       "  +
					super.getCampoDescripcion() +" as DESCRIPCION, "  +
					" 'NAFINWEBMODEL:CATALOGOIF:getListaElementosMonitorDispersionINFONAVIT METHOD' AS QUERY "  +
				" FROM "      +
					tablas     +
					condicion  +
					orden;

			log.debug("getListaElementosMonitorDispersionINFONAVIT.qryCatIf = <" + qryCatIf+ ">");

			regCatIf = con.consultarDB(qryCatIf);
			while(regCatIf.next()){
				
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				
				elementoCatalogo.setClave(regCatIf.getString("CLAVE"));
				elementoCatalogo.setDescripcion(regCatIf.getString("DESCRIPCION"));
				
				coleccionElementos.add(elementoCatalogo);
				
			}
		
		
		} catch(Exception e) {
			
			log.error("getListaElementosMonitorDispersionINFONAVIT(Exception)");
			log.error("getListaElementosMonitorDispersionINFONAVIT.qryCatIf         = <" + qryCatIf                    + ">");
			log.error("getListaElementosMonitorDispersionINFONAVIT.listaClavesIF    = <" + this.listaClavesIF          + ">");
			log.error("getListaElementosMonitorDispersionINFONAVIT.campoClave       = <" + super.getCampoClave()       + ">");
			log.error("getListaElementosMonitorDispersionINFONAVIT.campoDescripcion = <" + super.getCampoDescripcion() + ">");
			e.printStackTrace();
 
			throw new AppException("El catalogo no pudo ser generado", e);
			
		} finally {
			
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			
		}
		log.debug("qryCatIf"+ qryCatIf);	
		log.debug("coleccionElementos"+ coleccionElementos);	
		return coleccionElementos;													
		
	}	
	
	
	/**
	 * Metodo para obtener los Beneficiario de la pantalla  
	 * @return
	 */
	public List getListaElementosBeneficiario() {
		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		String tables, condiciones = "", orden;
		String qrysCatIFs;
		try	{
			con.conexionDB();
			Registros regCatEPOs	=	null;

			tables			= " comcat_if ci,  COMREL_CTA_BAN_DISTRIBUIDO c ";

			condiciones	= "  where  ci.cs_habilitado = 'S'  "+
							  "	and ci.ic_if in (select e.ic_if from comrel_if_epo e where e.ic_epo ="+claveEpo+" and e.cs_beneficiario='S')   "+
							  "	and ci.ic_if  = c.IC_BENEFICIARIO  "+
							  "   and c.IC_EPO = "+claveEpo;
			
			orden				= " 	order  by  ci.cg_razon_social ";

			qrysCatIFs = "SELECT DISTINCT "+super.getCampoClave()+" AS CLAVE, " +
											" "+super.getCampoDescripcion() +" AS DESCRIPCION "+											
											" FROM " + tables + condiciones + orden;
   

		log.debug("*------qrysCatIFs--------::"+qrysCatIFs); 
			

			regCatEPOs = con.consultarDB(qrysCatIFs);
			while(regCatEPOs.next()) {
					ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					elementoCatalogo.setClave(regCatEPOs.getString("CLAVE"));
					elementoCatalogo.setDescripcion(regCatEPOs.getString("DESCRIPCION"));
					coleccionElementos.add(elementoCatalogo);
			}

		} catch(Exception e) {			
			throw new AppException("El catalogo no pudo ser generado"+e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}

		return coleccionElementos;

	}
	
	
	
	
	public String getListaClavesIF() {		
		return this.listaClavesIF;	
	}


	public void setEstatusAfiliacion(String sConsEstatusAfiliacion){
		this.sConsEstatusAfiliacion = sConsEstatusAfiliacion;
	}
 
	public void setListaClavesIF(String listaClavesIF) {		
		this.listaClavesIF = listaClavesIF;	
	}
	
	public String getG_tieneClaveSIAG() {
		return g_tieneClaveSIAG;
	}

	public void setG_tieneClaveSIAG(String g_tieneClaveSIAG) {
		this.g_tieneClaveSIAG = g_tieneClaveSIAG;
	}

}




