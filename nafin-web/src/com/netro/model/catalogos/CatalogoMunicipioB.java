package com.netro.model.catalogos;

import netropology.utilerias.AppException;


public class CatalogoMunicipioB extends CatalogoSimple {

	private String clavePais;
	private String claveEstado;
	
	public CatalogoMunicipioB() {
		super.setTabla("comcat_municipio");
	}


	
/**
 * Establece la clave(id) que se va obtener en la consulta
 * @deprecated Utilizar setCampoClave()
 */	
	public void setClave(String clave)
	{
		super.setCampoClave(clave);
	}

/**
 * Establece la descripcion  que se va obtener en la consulta
 * @deprecated Utilizar setCampoDescripcion()
 */		
	public void setDescripcion(String descripcion)
	{
		super.setCampoDescripcion(descripcion);
	}

/**
* Establece valor para indicar algun valor en especifico a consultar
* @deprecated usar setValoresCondicionIn
*/	
	public void setSeleccion(String seleccion)
	{
		super.setValoresCondicionIn(seleccion, Integer.class);
	}
/**
* Establece el id del pais para condicion de la consulta
* @deprecated usar setClavepais()
*/	
	public void setPais(String pais)
	{
		this.clavePais = pais;
	}
	
/**
* Establece el id del estado para condicion de la consulta
*/
	public void setEstado(String estado)
	{
		this.claveEstado = estado;
	}

/**
* Establece el campo por el cual se va a ordenar
*/	
/*	public void setOrden(String order)
	{
		this.order = order;
	}
*/
	/**
	 * Establece las condiciones adicionales necesarias para contemplar el
	 * pais y el estado al momento de obtener los municipios.
	 */
	protected void setCondicionesAdicionales() {
		//log.debug("setCondicionesAdicionales(E)");

		//Se valida que esten los campos requeridos:
		try {
			if (this.clavePais == null || this.clavePais.equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		} catch (Exception e) {
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		String condicionAdicional = "";
		
		condicionAdicional = " ic_pais = ? ";
		
		super.addCondicionAdicional(condicionAdicional);
		//agregar a variables bind estos valores
		super.addVariablesBind(new Integer(clavePais));
		//log.debug("setCondicionesAdicionales(S)");
		
		condicionAdicional = " ic_estado = ? ";
		super.addCondicionAdicional(condicionAdicional);
		super.addVariablesBind(new Integer(claveEstado));
	}


/**
* Establece el id del pais para condicion de la consulta
* @param clavePais Clave del pais por el que se filtran los estados
*/	
	public void setClavePais(String clavePais)
	{
		this.clavePais = clavePais;
	}

/**
* Establece el id del pais para condicion de la consulta
* @param clavePais Clave del pais por el que se filtran los estados
*/	
	public void setClaveEstado(String claveEstado)
	{
		this.claveEstado = claveEstado;
	}

}