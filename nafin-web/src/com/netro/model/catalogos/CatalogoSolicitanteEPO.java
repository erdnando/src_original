package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/******************************************************************************************
 * Fodea 11-2014																									*
 * Descripci�n: Migraci�n de la pantalla Administraci�n-Afiliados-Consultas-Proveedores	*
 * 				 a ExtJS. La clase obtiene el cat�logo del solicitante de modificaci�n 		*
 *              por EPO																							*
 * Elabor�:     Agust�n Bautista Ruiz																		*
 * Fecha:       03/07/2014																						*
 ******************************************************************************************/
public class CatalogoSolicitanteEPO extends CatalogoAbstract {
	
	private String clave;
	private String descripcion;
	private String seleccion;
	private String order;
	private int icPyme;
	
	private static final Log log = ServiceLocator.getInstance().getLog(CatalogoSimple.class);
	
	public CatalogoSolicitanteEPO(){}
	
	public List getListaElementos() {
		
		log.info("getListaElementos(E)");
		List coleccionElementos = new ArrayList();
		
		AccesoDB con = new AccesoDB();		      
		String tables, condiciones, orden;
		String qryCatComRelPymeEpo;
		try
		{
			con.conexionDB();
			Registros regCatComRelPymeEpo	=	null;
			
			tables			= " COMREL_PYME_EPO CPE, COMCAT_EPO CE ";
			
         if(seleccion != null && !seleccion.equals("")){
				condiciones	= " WHERE CPE.IC_EPO = CE.IC_EPO AND CPE.IC_PYME = " + this.seleccion + "";
			}
			else{
				condiciones = "";
			}
			orden				= " ORDER BY CE.CG_RAZON_SOCIAL ";   
			
			qryCatComRelPymeEpo = "SELECT "+this.clave+" AS CLAVE, " +
											this.descripcion +" AS DESCRIPCION "+
											" FROM " + tables + condiciones;
			if(this.order!=null && !this.order.equals(""))
				qryCatComRelPymeEpo += "ORDER BY "+this.order;
			else
				qryCatComRelPymeEpo += orden;
				
			System.out.println("qryCatComRelPymeEpo:::....." +qryCatComRelPymeEpo );
			
			regCatComRelPymeEpo = con.consultarDB(qryCatComRelPymeEpo);
		
			while(regCatComRelPymeEpo.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(regCatComRelPymeEpo.getString("CLAVE"));
				elementoCatalogo.setDescripcion(regCatComRelPymeEpo.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		} catch(Exception e) {
				e.printStackTrace();
				throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
		}
		
		return coleccionElementos;
		
	}

/************************************************************************
 *									GETTERS AND SETTERS									*
 ************************************************************************/	
	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public String getSeleccion() {
		return seleccion;
	}
	
	public void setSeleccion(String seleccion) {
		this.seleccion = seleccion;
	}
	
	public String getOrder() {
		return order;
	}
	
	public void setOrder(String order) {
		this.order = order;
	}
	
	public int getIcPyme() {
		return icPyme;
	}
	
	public void setIcPyme(int icPyme) {
		this.icPyme = icPyme;
	}
	
}	