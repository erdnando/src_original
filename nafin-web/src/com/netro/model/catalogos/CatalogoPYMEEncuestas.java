package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


/**
 * Catalogo de EPO para encuestas (comcat_epo)
 *
 * Ejemplo de uso:
 * CatalogoEPOEncuestas cat = new CatalogoEPOEncuestas();
 * cat.setCampoClave("ic_epo");
 * cat.setCampoDescripcion("cg_razon_social");
 * cat.setOrden("cg_razon_social");
 *
 * @author Fabian Valenzuela
 */
public class CatalogoPYMEEncuestas extends CatalogoAbstract {
	
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoPYMEEncuestas.class);
	
	private String cveEpo = "";
	private String tipoCliente = "";
		
	StringBuffer tablas = new StringBuffer();
	
	public CatalogoPYMEEncuestas() {
		super();
		super.setPrefijoTablaPrincipal("t1."); //from comcat_if
	}
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colección de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer hint = new StringBuffer();
	
		tablas.append(" comcat_pyme t1 ");
		tablas.append(" ,comrel_pyme_epo t2 ");
		tablas.append(" ,comrel_nafin t3 ");
				
		hint.append(" /*+index(t2 CP_COMREL_PYME_EPO_PK) use_nl(t2 t3 t1)*/ " );

		StringBuffer condicionJoin = new StringBuffer();
		
		condicionJoin.append(" t2.ic_pyme = t1.ic_pyme ");
		condicionJoin.append("	 AND t1.ic_pyme = t3.ic_epo_pyme_if ");

		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
		
		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(
				" SELECT " + hint + " DISTINCT "  + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
				);
				
				
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y además se manda a llamar la implementación especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}
	
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		String condicionAdicional = "";
		
		
		/*
		try{
			if (this.claveEpo != null && !this.claveEpo.equals("") ){
				Integer.parseInt(this.claveEpo);
				//throw new Exception("Los parametros requeridos no has sido establecidos");
			}
			
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
		*/
		condicionAdicional +=" t3.cg_tipo = ? ";
		condicionAdicional +=" AND t2.cs_habilitado = ? ";
		
		if(cveEpo!=null && !cveEpo.equals("")  ){
			condicionAdicional +=" AND t2.ic_epo = ? ";
		}
		
		if(tipoCliente!=null && !tipoCliente.equals("") &&  (tipoCliente.equals("P") || tipoCliente.equals("D")) ){
			condicionAdicional +=" AND t1.ic_tipo_cliente IN (?,?) ";
		}
		
		
	
		if(condicionAdicional.length()>0)
			super.addCondicionAdicional(condicionAdicional);
		
		//agregar a variables bind estos valores
		super.addVariablesBind("P");
		super.addVariablesBind("S");
		if(cveEpo!=null && !cveEpo.equals("")){
			super.addVariablesBind(new Integer(this.cveEpo));
		}
		if(tipoCliente!=null && !tipoCliente.equals("") && tipoCliente.equals("P") ){
			super.addVariablesBind(new Integer(1));
			super.addVariablesBind(new Integer(4));
		}
		if(tipoCliente!=null && !tipoCliente.equals("") && tipoCliente.equals("D") ){
			super.addVariablesBind(new Integer(2));
			super.addVariablesBind(new Integer(4));
		}
			
			
			
	}

	public void setCveEpo(String cveEpo) {
		this.cveEpo = cveEpo;
	}


	public String getCveEpo() {
		return cveEpo;
	}


	public void setTipoCliente(String tipoCliente) {
		this.tipoCliente = tipoCliente;
	}


	public String getTipoCliente() {
		return tipoCliente;
	}


	



}