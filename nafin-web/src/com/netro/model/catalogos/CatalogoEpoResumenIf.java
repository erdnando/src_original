package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/******************************************************************************************
 * Fodea 11-2014																									*
 * Descripci�n: Migraci�n de la pantalla Administraci�n-Dispersi�n-Resumen IFs a ExtJS.	*
 * 				 La clase obtiene las EPOS para esta pantalla										*																							*
 * Elabor�:     Agust�n Bautista Ruiz																		*
 * Fecha:       04/08/2014																						*
 ******************************************************************************************/
public class CatalogoEpoResumenIf extends CatalogoSimple  {

	private String clave;
	private String descripcion;
	private int icIf;

	private static final Log log = ServiceLocator.getInstance().getLog(CatalogoEpoResumenIf.class);
	
	public CatalogoEpoResumenIf() {}	
	
	public List getListaElementos() {
		
		log.info("CatalogoEpoResumenIf (E)");
		List coleccionElementos = new ArrayList();
		
		AccesoDB con = new AccesoDB();		      
		String tables, condiciones, orden;
		StringBuffer qrySentencia = new StringBuffer();
		try
		{
			con.conexionDB();
			Registros reg	=	null;
			
			tables		= " COMCAT_EPO E, COMREL_IF_EPO IE, COMCAT_IF I ";
							
         condiciones	= " E.IC_EPO = IE.IC_EPO " +
							  " AND I.IC_IF = IE.IC_IF " +
							  " AND I.IC_IF = " + this.icIf +
							  " AND IE.CS_VOBO_NAFIN = 'S'" +
							  " AND E.CS_HABILITADO = 'S' " +							  
							  " AND IE.CS_BLOQUEO = 'N' " ;
								
			orden			= " E.CG_RAZON_SOCIAL ";   
					
			qrySentencia.append( " SELECT "+this.clave+" AS CLAVE, " + 
										  this.descripcion +" AS DESCRIPCION " +
										" FROM " + tables + 
										" WHERE " + condiciones + 
										" ORDER BY " + orden );
											
			log.info("Sentencia: " + qrySentencia.toString() );
			
			reg = con.consultarDB(qrySentencia.toString());
		
			while(reg.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(reg.getString("CLAVE"));
				elementoCatalogo.setDescripcion(reg.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
			}
		
		} catch(Exception e) {
				e.printStackTrace();
				throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
		}
		log.info("CatalogoEpoResumenIf (S)");
		return coleccionElementos;
		
	}
	
/************************************************************************
 *									GETTERS AND SETTERS									*
 ************************************************************************/	
	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getIcIf() {
		return icIf;
	}

	public void setIcIf(int icIf) {
		this.icIf = icIf;
	}
	
	
}