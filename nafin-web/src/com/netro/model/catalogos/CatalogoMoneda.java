package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Catalogo de Monedas (comcat_moneda)
 *
 * Ejemplo de uso:
 * CatalogoMoneda cat = new CatalogoMoneda();
 * cat.setCampoClave("ic_moneda");
 * cat.setCampoDescripcion("cd_nombre");
 * cat.setValoresCondicionIn("1,54", Integer.class);
 * cat.setOrden("CLAVE");	//Solo acepta CLAVE � DESCRIPCION (predeterminado)  (Tambien se puede usar 1 para clave y 2 para la descripcion)
 * List l = cat.getListaElementos();  //Lista con elementos ElementoCatalogo
 *
 */
public class CatalogoMoneda extends CatalogoAbstract {

	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoMoneda.class);

	private String clave;
	private String descripcion;
	private String seleccion;
	private String order;
	private String seleccionCesion;
	private String clavePyme="";
	
	public CatalogoMoneda() {
		super();
		super.setPrefijoTablaPrincipal("M."); //comcat_moneda M
	}
	
	/**
	 * Obtiene una lista de elementos de tipo ElementoCatalogo
	 * @return regresa una colecci�n de elementos obtenidos
	 */	
	public List getListaElementos()
	{
		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		String tables, condiciones, orden;
		StringBuffer qrySQL = new StringBuffer();

		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		try {
			con.conexionDB();
			Registros regCat	=	null;
			
//////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////Esta seccion esta mal dise�ada, este querie se puede hacer usando CatalogoSimple//////////
//////////////// Se deja por motivos de compatibilidad ///////////////////////////////////////////////////
			//Fodea 037-2009
			if(this.seleccionCesion!=null && !this.seleccionCesion.equals("")){
				
				qrySQL.append(
						" SELECT DISTINCT ic_moneda AS clave, m.cd_nombre AS descripcion"+
						"  FROM comcat_moneda m  "+
						"  where m.ic_moneda IN ("+this.seleccionCesion+") ");
       
			} else {
//////////////////////////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------------------//
//////////////////////////////////////////////////////////////////////////////////////////////
				//Genera las condiciones necesarias y las coloca en this.condicion
				//Al llamar a setCondicion se establecen las condiciones para 
				//IN y NOT IN... y adem�s se manda a llamar la implementaci�n especifica
				//setCondicionesAdicionales de la clase
				super.setCondicion();
				
				//dado que existe un UNION y las condiciones del segundo query
				//son iguales al de la primera parte se deben duplicar los valores
				//para establecer las variables BIND
				super.addVariablesBind(super.getValoresBindCondicion());  //Esta instrucci�n solo va debido al UNION..
			
			
			
	
										
				qrySQL.append(
						" SELECT DISTINCT " + super.getCampoClave() + " AS CLAVE, " + 
						" " + super.getCampoDescripcion() + " AS DESCRIPCION "+
						" FROM comcat_moneda M, comcat_tasa T " +
						" WHERE M.ic_moneda = T.ic_moneda "+
						" AND T.cs_disponible = 'S' " +
						((super.getCondicion().length()>0)?"AND " + super.getCondicion():"")+	//Para condiciones de In, Not in y adicionales especificas
						" UNION "+
						" SELECT DISTINCT "+super.getCampoClave()+", "+super.getCampoDescripcion()+ 
						" FROM comcat_moneda M, comcat_tasa T, com_tipo_cambio TC "+
						" WHERE M.ic_moneda = T.ic_moneda "+
						" AND T.cs_disponible = 'S' "+
						" AND M.ic_moneda = TC.ic_moneda " +
						((super.getCondicion().length()>0)?"AND " + super.getCondicion():"")	//Para condiciones de In, Not in y adicionales especificas
				);
											
				if (super.getOrden() == null || super.getOrden().equals("")) {
					qrySQL.append(
							" ORDER BY DESCRIPCION ");	//Como es un UNION, el ordenamiento debe ser basado en un alias o posicion 1,2
				} else {
					qrySQL.append(
							" ORDER BY " + super.getOrden() );
				}
			}
			
			if(!clavePyme.equals("")){
			
			
			}
				
			
			log.debug("qrySQL ::: "+qrySQL);
			log.debug("vars ::: "+super.getValoresBindCondicion());
				
			regCat = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
			while(regCat.next()) {
					ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					elementoCatalogo.setClave(regCat.getString("CLAVE"));
					elementoCatalogo.setDescripcion(regCat.getString("DESCRIPCION"));
					coleccionElementos.add(elementoCatalogo);
			}
		
		
		} catch(Exception e) {
			e.printStackTrace();
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
				
		return coleccionElementos;													
		
	}
	
	
	/**
	 * @deprecated usar getCampoClave
	 */
	public String getClave()
	{
		return super.getCampoClave();
	}
	/**
	 * @deprecated usar getCampoDescripcion
	 */
	public String getDescripcion()
	{
		return super.getCampoDescripcion();
	}
	
	public String getSeleccion()
	{
		return seleccion;
	}

	public String getSeleccionCesion() //Fodea 037-2009
	{
		return seleccionCesion;
	}
	
	/**
	* Establece la clave(id) que se va obtener en la consulta
	* @deprecated usar setCampoClave
	*/	
	public void setClave(String clave)
	{
		super.setCampoClave(clave);
	}

	/**
	* Establece la descripcion  que se va obtener en la consulta
	*/		
	public void setDescripcion(String descripcion)
	{
		super.setCampoDescripcion(descripcion);
	}

	/**
	* Establece valor para indicar algun valor en especifico a consultar
	* @deprecated usar setValoresCondicionIn()
	*/	
	public void setSeleccion(String seleccion)
	{
		super.setValoresCondicionIn(seleccion, Integer.class);
	}

	/**
	* Establece el campo por el cual se va a ordenar
	*/	
/*	public void setOrden(String order)
	{
		this.order = order;
	}
*/
	public void setSeleccionCesion(String seleccionCesion) //Fodea 037-2009
	{
		this.seleccionCesion = seleccionCesion;
	}
}