package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;

public class CatCausaRechazo extends  CatalogoAbstract {

	public CatCausaRechazo() {    }
	
	public List getListaElementos(){

		List coleccionElementos = new ArrayList();
		AccesoDB con = new AccesoDB();
		Registros regCat	=	null;
	
	
		try {
			
			String datos = "";			
			String condicion = "";
			String querySentencia="";
			String qrySentenciaDE="";
			con.conexionDB();
				
			if(!diversos.equals("SI")){//Para Rechazados Proyectos, Contratos, Prestamos, Disposiciones
					
				switch(Integer.parseInt(this.icproducto)){
					
				    case 1:                   //comcontrol01 � comcontrol02 � comcontrol03 ....
				    	   querySentencia = " FROM com_control0"+this.comcontrol+" c"+this.comcontrol+", com_control05 c5, com_documento d, com_docto_seleccionado ds, com_solicitud s, comcat_error_procesos cep " +
											" WHERE c"+this.comcontrol+".ic_folio = s.ic_folio AND c"+this.comcontrol+".ic_folio = c5.ic_folio "+
											" AND s.ic_documento = ds.ic_documento AND ds.ic_documento = d.ic_documento "+ 
											" AND c"+this.comcontrol+"."+this.cgestatus;
						   break;
				    case 2:
						   querySentencia=  "   FROM com_controlant0"+this.comcontrol+"  c"+this.comcontrol+", com_controlant05 c5, com_pedido p, com_anticipo a, com_pedido_seleccionado ps,comcat_error_procesos cep " +
											"  WHERE c"+this.comcontrol+".ic_pedido = a.ic_pedido"   +
											"    AND c"+this.comcontrol+".ic_pedido = c5.ic_pedido"   +
											"    AND a.ic_pedido = ps.ic_pedido "   +
											"    AND ps.ic_pedido = p.ic_pedido " +
											"    AND c"+this.comcontrol+"."+this.cgestatus;
						   break;
				    case 0:			    
						   querySentencia=  "   FROM com_interfase inter, com_solic_portal sp,comcat_error_procesos cep " +
											"  WHERE sp.ic_solic_portal = inter.ic_solic_portal";
				           break;
				    case 5:
				    	   querySentencia=  "   FROM inv_interfase inter, inv_disposicion disp, inv_solicitud sol,comcat_error_procesos cep " +
											"  WHERE sol.cc_disposicion = inter.cc_disposicion"   +
											"    AND inter.cc_disposicion = disp.cc_disposicion" ;
				           break;
				    case 4:
				    	   querySentencia=  "   FROM dis_interfase inter, dis_documento doc, dis_solicitud sol,comcat_error_procesos cep " +
											"  WHERE sol.ic_documento = inter.ic_documento"   +
											"    AND inter.ic_documento = doc.ic_documento";									
						        break;			   
				}
										
				if(this.icproducto.equals("1") || this.icproducto.equals("2"))
				{
					datos = " SELECT c5.ic_error_proceso || '|'||  c5.cc_codigo_aplicacion as CLAVE , cep.cg_descripcion as DESCRIPCION, 'CatalogoCausaRechazo.class' as pantalla ";
					condicion = " AND (c5.cc_codigo_aplicacion = '"+this.cccodigoaplicacion+"' OR ( c5.cc_codigo_aplicacion = 'NE' AND c5.ic_error_proceso in ("+this.in+"))) " +
								" and c5.cc_codigo_aplicacion = cep.cc_codigo_aplicacion " +
								" and c5.ic_error_proceso = cep.ic_error_proceso " +
								" GROUP BY c5.ic_error_proceso, c5.cc_codigo_aplicacion, cep.cg_descripcion ";										
				}
				if(this.icproducto.equals("0") || this.icproducto.equals("5") || this.icproducto.equals("4"))
				{
					datos = 	" SELECT inter.ic_error_proceso || '|'|| inter.cc_codigo_aplicacion as CLAVE , cep.cg_descripcion as DESCRIPCION , 'CatalogoCausaRechazo.class' as pantalla ";
			 		condicion =	"	AND (inter.cc_codigo_aplicacion = '"+this.cccodigoaplicacion+"' OR ( inter.cc_codigo_aplicacion = 'NE' AND inter.ic_error_proceso in ("+this.in+"))) " +
								"    AND inter.ig_proceso_numero = "+ this.igprocesonumero +
								"    and inter.cc_codigo_aplicacion = cep.cc_codigo_aplicacion " +
								"    and inter.ic_error_proceso = cep.ic_error_proceso " +
								"  GROUP BY inter.ic_error_proceso,inter.cc_codigo_aplicacion, cep.cg_descripcion ";	
				}	
				
				
			}else{ //Para Rechazados Diversos
										
				switch(Integer.parseInt(this.icproducto)){
					
				    case 1:                   
				    	   querySentencia= " FROM com_control05 c5, com_documento d, com_docto_seleccionado ds, com_solicitud s, comcat_error_procesos cep "+
				    	   				   " WHERE c5.ic_folio = s.ic_folio " +
										   " AND d.ic_documento = ds.ic_documento "  +
										   " AND ds.ic_documento = s.ic_documento " +
										   " AND c5.ic_error_proceso IN (0,  1,  2,  3,  4,  5,  10,  11) ";

						   break;
				    case 2:
						   querySentencia= " FROM com_controlant05 c5, com_pedido p, com_pedido_seleccionado ps, com_anticipo a, comcat_error_procesos cep "+
						   				   " WHERE c5.ic_pedido = a.ic_pedido "  +
										   " AND p.ic_pedido = ps.ic_pedido "  +
										   " AND ps.ic_pedido = a.ic_pedido "  +
										   " AND c5.ic_error_proceso IN (0,  1,  2,  3,  4,  5) " ;
						   
						   break;
				    case 0:			    
						   querySentencia= " FROM com_interfase inter, com_solic_portal sp,comcat_error_procesos cep "+
						   				   " WHERE sp.ic_solic_portal = inter.ic_solic_portal ";
				           break;
				    case 5:
				    	   querySentencia= " FROM inv_interfase inter, inv_disposicion disp, inv_solicitud sol,comcat_error_procesos cep " +
				    	   				   " WHERE sol.cc_disposicion = inter.cc_disposicion " +
				    	   				   " AND inter.cc_disposicion = disp.cc_disposicion " ;
				           break;
				    case 4:
				    	   querySentencia= " FROM dis_interfase inter, dis_documento doc, dis_solicitud sol,comcat_error_procesos cep " +
				    	   				   " WHERE sol.ic_documento = inter.ic_documento " +
				    	   				   " AND inter.ic_documento = doc.ic_documento " ;
						   break;			   
				}					
												
				if(this.icproducto.equals("1") || this.icproducto.equals("2"))	{
					datos = " SELECT ";
					if(this.icproducto.equals("1")){
						datos = datos + " /*+index(s CP_COM_SOLICITUD_PK)*/ ";
					}
					datos += " c5.ic_error_proceso || '|'||  c5.cc_codigo_aplicacion as CLAVE, cep.cg_descripcion  as DESCRIPCION";					
					condicion = " AND c5.cc_codigo_aplicacion = 'NE' "   +
								" AND c5.cc_codigo_aplicacion = cep.cc_codigo_aplicacion " +
								" AND c5.ic_error_proceso = cep.ic_error_proceso " +
								" GROUP BY c5.ic_error_proceso, c5.cc_codigo_aplicacion, cep.cg_descripcion ";
					
					if(this.icproducto.equals("1")){					
						qrySentenciaDE = " SELECT /*+index(s CP_COM_SOLICITUD_PK) index(c1 CP_COM_CONTROL01_PK) use_nl(s c1)*/ " +
							   			 " COUNT (1), 'CatalogoCausaRechazo.class' as pantalla " +
							  			 " FROM com_control01 c1, "+
							       		 " com_documento d, "+
							       		 " com_docto_seleccionado ds, "+
							             " com_solicitud s " +
										 " WHERE c1.ic_folio = s.ic_folio "+
										 " AND d.ic_documento = ds.ic_documento "+ 
										 " AND ds.ic_documento = s.ic_documento "+
										 " AND c1.cg_estatus = 'N' ";
					}
					
					if(this.icproducto.equals("2")){					
						qrySentenciaDE = " SELECT COUNT (1)   , 'CatalogoCausaRechazo.class' as pantalla" +
										 " FROM com_controlant01 c1, " +
										 " com_pedido p, " + 
										 " com_pedido_seleccionado ps, " + 
										 " com_anticipo a " +
										 " WHERE c1.ic_pedido = a.ic_pedido " +
										 " AND p.ic_pedido = ps.ic_pedido " +
										 " AND ps.ic_pedido = a.ic_pedido " +
										 " AND c1.cg_estatus = 'N' "  ;
					}													
				}
				if(this.icproducto.equals("0") || this.icproducto.equals("5") || this.icproducto.equals("4"))
				{
					datos = " SELECT inter.ic_error_proceso || '|'|| inter.cc_codigo_aplicacion as CLAVE , cep.cg_descripcion as DESCRIPCION, 'CatalogoCausaRechazo.class' as pantalla ";
			 		condicion = " AND inter.ic_error_proceso = cep.ic_error_proceso " +
			 					" AND inter.cc_codigo_aplicacion = cep.cc_codigo_aplicacion " +
								" AND inter.ig_proceso_numero IS NULL" +
								" AND inter.cc_codigo_aplicacion = 'NE'" +
								" GROUP BY inter.ic_error_proceso, inter.cc_codigo_aplicacion, cep.cg_descripcion ";
				}						
				
			}//else
			
				querySentencia = datos + querySentencia + condicion;	
				
				System.out.println("querySentencia---------------------->  "+querySentencia); 
				System.out.println("getValoresBindCondicion---------------------->  "+getValoresBindCondicion()); 
				
				regCat = con.consultarDB(querySentencia,super.getValoresBindCondicion());
					ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					elementoCatalogo.setClave("T");
					elementoCatalogo.setDescripcion("Todas las Causas");	
					coleccionElementos.add(elementoCatalogo);
				while(regCat.next()) {
					elementoCatalogo = new ElementoCatalogo();
					elementoCatalogo.setClave(regCat.getString("CLAVE"));
					elementoCatalogo.setDescripcion(regCat.getString("DESCRIPCION"));
					coleccionElementos.add(elementoCatalogo);
				}
			
			if(!qrySentenciaDE.equals("")){		
				querySentencia = qrySentenciaDE;	
				
				regCat = con.consultarDB(querySentencia,super.getValoresBindCondicion());
				if(regCat.next()){
					if(!regCat.getString(1).equals("0")){
						String  id = "7|NE";
						String descripcion = "Registro Necesita aprobacion de comite";
						elementoCatalogo = new ElementoCatalogo();
						elementoCatalogo.setClave(id);
						elementoCatalogo.setDescripcion(descripcion);
						coleccionElementos.add(elementoCatalogo);
					}
				}				
			}
			
			System.out.println("********************  "); 
			System.out.println("querySentencia  "+querySentencia); 
			System.out.println("********************  "); 
			
			
					
		} catch (Exception e) {
			System.err.println("Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		return coleccionElementos;
	}
	
	public void setIcproducto(String icproducto){
	  this.icproducto = icproducto;
	}
	public void setIgprocesonumero(String igprocesonumero){
	  this.igprocesonumero=igprocesonumero;
	}	
	public void setCmbestatus(String cmbestatus){
	  this.cmbestatus = cmbestatus;
	}
	public void setComcontrol(String comcontrol){
	  this.comcontrol = comcontrol;
	}
	public void setIn(String in){
	  this.in= in;
	}
  	public void setCccodigoaplicacion(String cccodigoaplicacion){
	  this.cccodigoaplicacion = cccodigoaplicacion; 
	  }
	public void setCgestatus(String cgestatus){
	  this.cgestatus=cgestatus;
  	}
	public void setDiversos(String diversos){
	  this.diversos=diversos;
  	}
  
  private String icproducto = "";
  private String igprocesonumero = "";
  private String cmbestatus="";
  private String comcontrol="";
  private String in="";
  private String cccodigoaplicacion="";
  private String cgestatus="";
  private String diversos="";
  
}