package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;

/**
 * Esta clase genera u obtiene los valores del catalogo
 * BMXCAT_ESTRATO, en base a condiciones establecidas
 * @author Fabian Valenzuela
 */
public class CatalogoEstrato 
{
	private String clave;
	private String descripcion;
	private String seleccion;
	private String sector;
	private String order;
	
	
	/**
	 * Obtiene una lista de elementos de tipo ElementoCatalogo
	 * @return regresa una colecci�n de elementos obtenidos
	 */	
	public List getListaElementos()
	{
		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		String tables, condiciones, orden;
		String qryCatEstrato;
		try
		{
			con.conexionDB();
			Registros regCatEstado	=	null;
			
			
			tables			= " BMXCAT_ESTRATO ";
			condiciones	= " WHERE IC_SECTOR =  " + this.sector;
			orden				= " ORDER BY IC_ESTRATO ";
			
			qryCatEstrato = "select "+this.clave+" AS CLAVE, " + 
											this.descripcion +" AS DESCRIPCION "+
											" FROM " + tables + condiciones;
			if(this.order!=null && !this.order.equals(""))
				qryCatEstrato += "ORDER BY "+this.order;
			else
				qryCatEstrato += orden;
			
			regCatEstado = con.consultarDB(qryCatEstrato);
			while(regCatEstado.next()) {
					ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					elementoCatalogo.setClave(regCatEstado.getString("CLAVE"));
					elementoCatalogo.setDescripcion(regCatEstado.getString("DESCRIPCION"));
					coleccionElementos.add(elementoCatalogo);
			}
		
		
		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
				
		return coleccionElementos;													
		
	}
		
/**
* Establece la clave(id) que se va obtener en la consulta
*/	
	public void setClave(String clave)
	{
		this.clave = clave;		
	}

/**
* Establece la descripcion  que se va obtener en la consulta
*/		
	public void setDescripcion(String descripcion)
	{
		this.descripcion = descripcion;
	}

/**
* Establece valor para indicar algun valor en especifico a consultar
*/	
	public void setSeleccion(String seleccion)
	{
		this.seleccion = seleccion;
	}
/**
* Establece el id del sector para condicion de la consulta
*/	
	public void setSector(String sector)
	{
		this.sector = sector;
	}
/**
* Establece el campo por el cual se va a ordenar
*/	
	public void setOrden(String order)
	{
		this.order = order;
	}

}