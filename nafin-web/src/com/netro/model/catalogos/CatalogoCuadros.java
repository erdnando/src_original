package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/******************************************************************************************
 * Fodea XX-2014                                                                          *
 * Descripci�n: Migraci�n de la pantalla Tasas de Credito-Tablas de Precios a ExtJS.  *
 * Elabor�:     Agust�n Bautista Ruiz                                                     *
 * Fecha:       22/09/2014                                                                *
 ******************************************************************************************/
public class CatalogoCuadros extends CatalogoSimple  {

	private String clave;
	private String descripcion;
	private String idMoneda;
	private static final Log log = ServiceLocator.getInstance().getLog(CatalogoCuadros.class);

	public CatalogoCuadros(){}

	public List getListaElementos() {

		log.info("CatalogoCuadros (E)");
		List coleccionElementos = new ArrayList();

		AccesoDB con = new AccesoDB();
		String tables, condiciones, orden;
		StringBuffer qrySentencia = new StringBuffer();
		try
		{
			con.conexionDB();
			Registros reg = null;

			if(this.idMoneda.equals("1")){
				condiciones = " AND IDCUADRO NOT IN (15,16) ";
			} else{
				condiciones = " AND IDCUADRO NOT IN (14) ";
			}

			qrySentencia.append( " SELECT " + this.clave + " AS CLAVE, " + 
										  this.descripcion +" AS DESCRIPCION " +
										" FROM CUADROS " + 
										" WHERE IDMONEDA = " + this.idMoneda +
										condiciones + 
										" ORDER BY IDCUADRO ASC");

			log.info("Sentencia: " + qrySentencia.toString() );

			reg = con.consultarDB(qrySentencia.toString());

			while(reg.next()){
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(reg.getString("CLAVE"));
				elementoCatalogo.setDescripcion(reg.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
			}

		} catch(Exception e){
				e.printStackTrace();
				throw new RuntimeException("El catalogo no pudo ser generado");
		} finally{
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
		}
		log.info("CatalogoCuadros (S)");
		return coleccionElementos;

	}

/************************************************************************
 *                 GETTERS AND SETTERS                                  *
 ************************************************************************/
	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getIdMoneda() {
		return idMoneda;
	}

	public void setIdMoneda(String idMoneda) {
		this.idMoneda = idMoneda;
	}

}