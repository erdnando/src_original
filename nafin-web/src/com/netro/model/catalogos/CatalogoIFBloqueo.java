package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Esta clase genera u obtiene los valores del catalogo
 * de IF,s para bloqueo, en base a ciertas condiciones establecidas
 * Catalogo de IF's (IF's con bases de operacion de credito)
 * Ejemplo de uso:
 * CatalogoIFBoqueo cat = new CatalogoIFBOGarant();
 * cat.setClave("ic_if");
 * cat.setDescripcion("cg_razon_social");
 * @author Jos� Alvarado
 */
public class CatalogoIFBloqueo extends CatalogoSimple
{
	private String clave;
	private String descripcion;
	private String order;

	private static final Log log = ServiceLocator.getInstance().getLog(CatalogoIFBloqueo.class);


/**
 * Obtiene una lista de elementos de tipo ElementoCatalogo
 * @return regresa una colecci�n de elementos obtenidos
 */
	public List getListaElementos()
	{
		log.info("getListaElementos(E)");
		List coleccionElementos = new ArrayList();

		AccesoDB con = new AccesoDB();
		String tables, condiciones, orden;
		String qryCatPantallas;
		try
		{
			con.conexionDB();
			Registros regCatPantallas	=	null;

			tables			= " comcat_if i, com_base_op_credito bop ";

			condiciones	= " WHERE cs_habilitado='S' AND i.ic_if = bop.ic_if";
			
			orden				= " ORDER BY i.cg_razon_social ";

			qryCatPantallas = "SELECT DISTINCT i."+this.clave+" AS CLAVE, i." +this.descripcion +" AS DESCRIPCION "+
											" FROM " + tables + condiciones;
			if(this.order!=null && !this.order.equals(""))
				qryCatPantallas += " ORDER BY "+this.order;
			else
				qryCatPantallas += orden;

			System.out.println("qryCatalogoIFBloqueo:::....." +qryCatPantallas );

			regCatPantallas = con.consultarDB(qryCatPantallas);

			while(regCatPantallas.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(regCatPantallas.getString("CLAVE"));
				elementoCatalogo.setDescripcion(regCatPantallas.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}


		} catch(Exception e) {
				e.printStackTrace();
				throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
		}

		return coleccionElementos;

	}



/**
* Establece la clave(id) que se va obtener en la consulta
*/
	public void setClave(String clave)
	{
		this.clave = clave;
	}
/**
* Establece la descripcion  que se va obtener en la consulta
*/
	public void setDescripcion(String descripcion)
	{
		this.descripcion = descripcion;
	}

/**
* Establece el campo por el cual se va a ordenar
*/
	public void setOrden(String order)
	{
		this.order = order;
	}

}