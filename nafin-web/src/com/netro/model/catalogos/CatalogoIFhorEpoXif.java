package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class CatalogoIFhorEpoXif extends CatalogoAbstract {
	
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoIFhorEpoXif.class);
	private String claveEpo;
	private String claveProducto;
	
	StringBuffer tablas = new StringBuffer();

	public CatalogoIFhorEpoXif() {
		super();
		super.setPrefijoTablaPrincipal("ci."); //from comcat_ic ci
	}
	
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colección de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer hint = new StringBuffer();
	
		tablas.append(" comrel_if_epo_x_producto cie ");
		tablas.append(" ,comcat_producto_nafin pn ");
		tablas.append(" ,comcat_epo e ");
		tablas.append(" ,comcat_if ci ");
		tablas.append(" ,comrel_horario_epo_x_if he ");


		StringBuffer condicionJoin = new StringBuffer();
		condicionJoin.append(" cie.ic_epo = e.ic_epo ");
		condicionJoin.append(" AND cie.ic_if = ci.ic_if ");
		condicionJoin.append(" AND cie.ic_producto_nafin = pn.ic_producto_nafin ");
		condicionJoin.append(" AND cie.ic_producto_nafin = he.ic_producto_nafin(+) ");
		condicionJoin.append(" AND cie.ic_epo = he.ic_epo(+) ");
		condicionJoin.append(" AND cie.ic_if = he.ic_if(+) ");
		

		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
		
		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(
				" SELECT  " + hint + " DISTINCT " + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
				);
				
				
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y además se manda a llamar la implementación especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}
	
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales necesarias para contemplar el
	 * tipoAfiliado.
	 */
	protected void setCondicionesAdicionales() {
		//log.debug("setCondicionesAdicionales(E)");

		//Se valida que esten los campos requeridos:
		try {
			if (this.claveEpo == null || this.claveEpo.equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		} catch (Exception e) {
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
		
		StringBuffer condicionAdicional = new StringBuffer();
		
		condicionAdicional.append(" cie.cs_habilitado = ? ");
		super.addCondicionAdicional(condicionAdicional.toString());
		super.addVariablesBind("S");
		
		if (this.claveEpo != null && !this.claveEpo.equals("")) {
			condicionAdicional = new StringBuffer();
			condicionAdicional.append(" cie.ic_epo = ? ");
			super.addCondicionAdicional(condicionAdicional.toString());
			super.addVariablesBind(new Integer(this.claveEpo));
		}
		
		if (this.claveProducto != null && !this.claveProducto.equals("")) {
			condicionAdicional = new StringBuffer();
			condicionAdicional.append(" cie.ic_producto_nafin = ? ");
			super.addCondicionAdicional(condicionAdicional.toString());
			super.addVariablesBind(new Integer(this.claveProducto));
		}else{
			condicionAdicional = new StringBuffer();
			condicionAdicional.append(" cie.ic_producto_nafin in (?) ");
			super.addCondicionAdicional(condicionAdicional.toString());
			super.addVariablesBind(new Integer("1"));
		}
		
		
	}


	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}


	public String getClaveEpo() {
		return claveEpo;
	}


	public void setClaveProducto(String claveProducto) {
		this.claveProducto = claveProducto;
	}


	public String getClaveProducto() {
		return claveProducto;
	}


}