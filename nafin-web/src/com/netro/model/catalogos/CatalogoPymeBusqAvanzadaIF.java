package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


/**
 * Catalogo de Pymes - Busqueda Avanzada (comcat_pyme)
 *
 * Ejemplo de uso:

 * @author Deysi Hern�ndez Contreras
 */
public class CatalogoPymeBusqAvanzadaIF extends CatalogoAbstract {
	
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoPymeBusqAvanzadaIF.class);
	 
	
	
	StringBuffer tablas = new StringBuffer();
	String group = "";
	private String ic_epo;
	private String num_pyme;
	private String rfc_pyme;
	private String nombre_pyme;
	private String ic_pyme;
	
	public CatalogoPymeBusqAvanzadaIF() {
		super();		
	}
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colecci�n de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
 
		
		StringBuffer hint = new StringBuffer();
		StringBuffer condicionJoin = new StringBuffer();
		StringBuffer group = new StringBuffer();
		
		
		
		if(!"".equals(ic_pyme)) {
			
			hint.append(" /*+index(crn CP_COMREL_NAFIN_PK) use_nl(r p)*/  " );
		
			tablas.append(" comrel_nafin crn, comrel_pyme_epo r, comcat_pyme p  ");
			
			condicionJoin.append("  crn.ic_epo_pyme_if = p.ic_pyme"   +
				"    AND p.ic_pyme = r.ic_pyme"   +
				"    AND crn.cg_tipo = 'P' "   );
		
			group	.append(" GROUP BY p.ic_pyme, p.cg_razon_social,crn.ic_nafin_electronico || ' ' || p.cg_razon_social   ");


		
		}else  {
			
			hint.append(" /*+index(pe CP_COMREL_PYME_EPO_PK) index(p CP_COMCAT_PYME_PK) use_nl(pe p)*/  " );
			
			
			tablas.append(" comrel_pyme_epo pe, comrel_nafin crn, comcat_pyme p  ");
			
			
			condicionJoin.append(" p.ic_pyme = pe.ic_pyme"   +
				"    AND p.ic_pyme = crn.ic_epo_pyme_if"   +
				"    AND crn.cg_tipo = 'P'"   +
				"    AND pe.cs_habilitado = 'S'"  +
				"    AND p.cs_habilitado = 'S'"   );
		
			group	.append("  GROUP BY p.ic_pyme,  RPAD (crn.ic_nafin_electronico, 10, ' ') || p.cg_razon_social, crn.ic_nafin_electronico, p.cg_razon_social , p.cg_rfc "  );
		}
		
	 
	
	

		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
		
		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(
				" SELECT  " + hint + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
				);
				
				
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y adem�s se manda a llamar la implementaci�n especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}
		
	
		if(!group.equals("")){
				qrySQL.append(group);
		}
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		
		
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		String  condicionAdicional ="";
		
		try{
			
			if (this.ic_pyme != null && !this.ic_pyme.equals(""))
				Integer.parseInt(this.ic_pyme);				
				
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
		
	
		if(!"".equals(ic_pyme)) {
			if(!"".equals(ic_epo)) { 
				condicionAdicional ="  r.ic_epo = ? ";
				super.addCondicionAdicional(condicionAdicional);
				super.addVariablesBind(ic_epo);	
			}
		 
			condicionAdicional = "   p.ic_pyme = ? "   +
											" AND r.ic_pyme = ?  ";
			super.addCondicionAdicional(condicionAdicional);
			super.addVariablesBind(ic_pyme);	
			super.addVariablesBind(ic_pyme);	
			
		}else {
			
			if(!"".equals(ic_epo)){
				condicionAdicional= "  pe.ic_epo = ?";
				super.addCondicionAdicional(condicionAdicional);
				super.addVariablesBind(ic_epo);	
			}
			if(!"".equals(num_pyme)) {
				condicionAdicional= "   (cg_pyme_epo_interno LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') OR CG_NUM_DISTRIBUIDOR LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%'))";
				super.addCondicionAdicional(condicionAdicional);
				super.addVariablesBind(num_pyme);	
				super.addVariablesBind(num_pyme);	
			}
			if(!"".equals(rfc_pyme)) {
				condicionAdicional ="   cg_rfc LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')";
				super.addCondicionAdicional(condicionAdicional);
				super.addVariablesBind(rfc_pyme);
			}
				
			if(!"".equals(nombre_pyme)) {
				condicionAdicional = "   cg_razon_social LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')";
				super.addCondicionAdicional(condicionAdicional);
				super.addVariablesBind(nombre_pyme);
			}
			  
		}
	}

	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getNum_pyme() {
		return num_pyme;
	}

	public void setNum_pyme(String num_pyme) {
		this.num_pyme = num_pyme;
	}

	public String getRfc_pyme() {
		return rfc_pyme;
	}

	public void setRfc_pyme(String rfc_pyme) {
		this.rfc_pyme = rfc_pyme;
	}

	public String getNombre_pyme() {
		return nombre_pyme;
	}

	public void setNombre_pyme(String nombre_pyme) {
		this.nombre_pyme = nombre_pyme;
	}

	public String getIc_pyme() {
		return ic_pyme;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}


	




}