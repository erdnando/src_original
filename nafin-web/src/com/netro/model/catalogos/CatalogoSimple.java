package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * La clase permite obtener los valores de un cat�logo simple, es decir,
 * aquel que involucra s�lo una tabla.
 * Notas de empleo:
 * - Se debe establecer la tabla a consultar mediante setTabla()
 * - Se debe establecer el campo clave para el listado. Este puede ser
 * 	incluso un campo compuesto o con funcion
 * - opcionalmente se puede especificar un campo de ordenamiento (setOrden()),
 * 	y valores para condicion IN (setValoresCondicionIn()) o
 *    NOT IN (setValoresCondicionNotIn()).
 * - De ser necesario, en caso de que el campo clave no sea igual al campo llave,
 * 	se puede establecer el campo llave
 *
 *    Para extender esta clase de ser necesario,
 *    sobreescribir el metodo setCondicionesAdicionales()
 * 	para contemplar condiciones especificas de un cat�logo y agregar en el
 *    constructor super.setTabla("mi_tabla_xxx");
 *
 * Ejemplo de Uso:
 * CatalogoSimple cat = new CatalogoSimple();
 * cat.setTabla("cmp_tipo_documentacion");
 * cat.setDistinc(false);
 * cat.setCampoClave("ic_tipo_documentacion");
 * cat.setCampoDescripcion("cg_nombre");
 * cat.setOrden("ic_tipo_documentacion");
 * List lista = cat.getListaElementos();
 *
 * El contenido de la lista es de objetos netropology.utilerias.ElementoCatalogo
 * los cuales almacenan la clave y descripcion de los elementos obtenidos
 * de consultar el cat�logo en la BD.
 *
 * @author Gilberto Aparicio
 */
public class CatalogoSimple extends CatalogoAbstract {

	private String tabla;
	private boolean distinc = false;

	//Variable para enviar mensajes al log.
	private static final Log log = ServiceLocator.getInstance().getLog(CatalogoSimple.class);


	public CatalogoSimple(){} 



	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts.
	 * @return regresa una colecci�n de elementos obtenidos
	 */
	public List getListaElementos() {
		log.info("getListaElementos(E)");
		List coleccionElementos = new ArrayList();

		try {
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ||
				this.tabla == null || this.tabla.equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		} catch (Exception e) {
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer query = new StringBuffer();
			if (distinc){
				query.append(" SELECT DISTINCT " + super.getCampoClave() + " AS CLAVE, ");
			}else{
				query.append(" SELECT " + super.getCampoClave() + " AS CLAVE, ");
			}
				query.append(super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + this.tabla);
		//Genera las condiciones necesarias y las coloca en super.condicion
		super.setCondicion();

		if (super.getCondicion().length()>0) {
			query.append(" WHERE " + super.getCondicion());
		}

		if (super.getOrden() == null || super.getOrden().equals("")) {
			query.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			query.append(
					" ORDER BY " + super.getOrden() );
		}

		log.debug("query:" + query.toString());
		log.debug("vars:" + super.getValoresBindCondicion());

		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try {
			con.conexionDB();
			registros = con.consultarDB(query.toString(), this.getValoresBindCondicion());
		} catch(Exception e) {
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}

		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}

		log.info("getListaElementos(S)");
		return coleccionElementos;
	}


	/**
	 * Establece el nombre de la tabla de la que se desea obtener el
	 * listado de elementos.
	 * @param tabla Nombre de la tabla
	 */
	public void setTabla(String tabla) {
		this.tabla = tabla;
	}

	/**
	 * Establece el nombre de la tabla de la que se desea obtener el
	 * listado de elementos.
	 * @param DISTINC para consultar 
	 */
	public void setDistinc(boolean dato) {
		this.distinc = dato;
	}
	/**
	 * Establece condiciones adicionales. Esta implementacion (YA NO) est� vac�a
	 * Las clases que extiendan esta CatalogoSimple, deben sobreescribirla
	 * mandando a llamar addCondicionAdicional(String) para poner la
	 * cadena de SQL que contenga la condicion y
	 * addVariablesBind(Object) para establecer sus valores respectivos
	 */
	public void setCondicionesAdicionales(String condicion) {
        this.addCondicionAdicional(condicion);
	}

















	/**
	 * Establece los valores de las claves que se incluyen
	 * @deprecated Utilizar setValoresCondicionIn
	 * @see Catalogo#setValoresCondicionIn(List)
	 */
	public void setCondicionIn(String cadenaValoresCondicionIn, String clase) {
		super.setValoresCondicionIn( Comunes.explode(",", cadenaValoresCondicionIn, clase) );
	}

	/**
	 * Establece los valores de las claves que se incluyen
	 * @deprecated Utilizar setValoresCondicionIn
	 * @see Catalogo#setValoresCondicionIn(List)
	 */
	public void setCondicionIn(Collection valoresCondicionIn) {
		super.setValoresCondicionIn( valoresCondicionIn );
	}


	/**
	 * Establece los valores de las claves que no se incluyen
	 * @deprecated Utilizar setValoresCondicionIn
	 * @see Catalogo#setValoresCondicionIn(List)
	 */
	public void setCondicionNotIn(String cadenaValoresCondicionNotIn, String clase) {
		super.setValoresCondicionNotIn( Comunes.explode(",", cadenaValoresCondicionNotIn, clase) );
	}

	/**
	 * Establece los valores de las claves que no se incluyen
	 * @deprecated Utilizar setValoresCondicionIn
	 * @see Catalogo#setValoresCondicionIn(List)
	 */
	public void setCondicionNotIn(Collection valoresCondicionNotIn) {
		super.setValoresCondicionNotIn( valoresCondicionNotIn );
	}

}
