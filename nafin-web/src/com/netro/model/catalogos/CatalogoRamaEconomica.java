package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Esta clase genera u obtiene los valores del catalogo
 * COMCAT_RAMA_ECON_EPO, en base a ciertas condiciones establecidas
 * @author
 */
public class CatalogoRamaEconomica extends CatalogoSimple
{
	//private String tabla;
	private String clave;     
	private String descripcion;
	private String sectorEcon;
	private String subsectorEcon;	
	private String order;
	
	private static final Log log = ServiceLocator.getInstance().getLog(CatalogoSimple.class);


/**
 * Obtiene una lista de elementos de tipo ElementoCatalogo
 * @return regresa una colecci�n de elementos obtenidos
 */ 
	public List getListaElementos()
	{
		log.info("getListaElementos(E)");
		List coleccionElementos = new ArrayList();
		
		AccesoDB con = new AccesoDB();		      
		String tables, condiciones, orden;
		String qryCatalogo;
		try
		{
			con.conexionDB();
			Registros regCatatalogo	=	null;
			
			tables			= " COMCAT_RAMA_ECON_EPO ";
			condiciones	   = " WHERE  IC_SECTOR_ECON_EPO =  " + this.sectorEcon +
										" AND IC_SUBSECTOR_EPO  = "  + this.subsectorEcon;

			orden				= " ORDER BY CG_DESCRIPCION ";   

			qryCatalogo = "select "+this.clave+" AS CLAVE, " +
											this.descripcion +" AS DESCRIPCION "+
											" FROM " + tables + condiciones;
			
			if(this.order!=null && !this.order.equals(""))
				qryCatalogo += "ORDER BY "+this.order;
			else
				qryCatalogo += orden;
				
			System.out.println("qryCatalogo:::....." +qryCatalogo );

			regCatatalogo = con.consultarDB(qryCatalogo);
		
			while(regCatatalogo.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(regCatatalogo.getString("CLAVE"));
				elementoCatalogo.setDescripcion(regCatatalogo.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}


		} catch(Exception e) {
				e.printStackTrace();
				throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
		}

		return coleccionElementos;

	}

/**
* Establece la clave(id) que se va obtener en la consulta
*/
	public void setClave(String clave)
	{
		this.clave = clave;
	}
/**
* Establece la descripcion  que se va obtener en la consulta
*/
	public void setDescripcion(String descripcion)
	{
		this.descripcion = descripcion;
	}
/**
* Establece valor para indicar el Sector Econ�mico a consultar
*/
	public void setSectorEcon(String sectorEcon)
	{
		this.sectorEcon = sectorEcon;
	}

/**
* Establece valor para indicar el Subsector Econ�mico a consultar
*/
	public void setSubSectorEcon(String subsectorEcon)
	{
		this.subsectorEcon = subsectorEcon;
	}
/**
* Establece el campo por el cual se va a ordenar
*/
	public void setOrden(String order)
	{
		this.order = order;
	}

}