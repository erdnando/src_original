package com.netro.model.catalogos;

import netropology.utilerias.AppException;

/**
 * Catalogo de Estado (comcat_estado)
 *
 * Ejemplo de uso:
 * CatalogoEstado cat = new CatalogoEstado();
 * cat.setCampoClave("ic_estado");
 * cat.setCampoDescripcion("cg_nombre");
 * cat.setClavePais("24");
 * cat.setOrden("ic_estado");
 * List l = cat.getListaElementos();  //Lista con elementos ElementoCatalogo
 *
 * para el caso en el que el campo clave no concuerde con la llave y se emplee
 * valores para in o not in, es necesario especificar el campo llave. ejemplo:
 * cat.setCampoClave("ic_pais||ic_estado");
 * cat.setCampoDescripcion("cd_nombre");
 * cat.setClavePais("24");
 * cat.setCampoLlave("ic_estado");
 * cat.setValoresCondicionIn("1,9", Integer.class);
 * cat.setOrden("2");
 *
 * @author Gilberto Aparicio
 */
public class CatalogoEstado extends CatalogoSimple {
	//private String clave;
	//private String descripcion;
	//private String seleccion;
	private String clavePais;
//	private String order;
	
	public CatalogoEstado() {
		super.setTabla("comcat_estado");
	}

	/**
	 * Obtiene una lista de elementos de tipo ElementoCatalogo
	 * @return regresa una colecci�n de elementos obtenidos
	 */	
/*	public List getListaElementos()
	{
		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		String tables, condiciones, orden;
		String qryCatEstado;
		try
		{
			con.conexionDB();
			Registros regCatEstado	=	null;
			
			
			tables			= " COMCAT_ESTADO ";
			condiciones	= " WHERE IC_PAIS =  " + this.pais;
			orden				= " ORDER BY CD_NOMBRE ";
			
			qryCatEstado = "select "+this.clave+" AS CLAVE, " + 
											this.descripcion +" AS DESCRIPCION "+
											" FROM " + tables + condiciones;
			if(this.order!=null && !this.order.equals(""))
				qryCatEstado += "ORDER BY "+this.order;
			else
				qryCatEstado += orden;
			
			regCatEstado = con.consultarDB(qryCatEstado);
			while(regCatEstado.next()) {
					ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					elementoCatalogo.setClave(regCatEstado.getString("CLAVE"));
					elementoCatalogo.setDescripcion(regCatEstado.getString("DESCRIPCION"));
					coleccionElementos.add(elementoCatalogo);
			}
		
		
		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
				
		return coleccionElementos;													
		
	}
*/	

	
/**
 * Establece la clave(id) que se va obtener en la consulta
 * @deprecated Utilizar setCampoClave()
 */	
	public void setClave(String clave)
	{
		super.setCampoClave(clave);
	}

/**
 * Establece la descripcion  que se va obtener en la consulta
 * @deprecated Utilizar setCampoDescripcion()
 */		
	public void setDescripcion(String descripcion)
	{
		super.setCampoDescripcion(descripcion);
	}

/**
* Establece valor para indicar algun valor en especifico a consultar
* @deprecated usar setValoresCondicionIn
*/	
	public void setSeleccion(String seleccion)
	{
		super.setValoresCondicionIn(seleccion, Integer.class);
	}
/**
* Establece el id del pais para condicion de la consulta
* @deprecated usar setClavepais()
*/	
	public void setPais(String pais)
	{
		this.clavePais = pais;
	}
/**
* Establece el campo por el cual se va a ordenar
*/	
/*	public void setOrden(String order)
	{
		this.order = order;
	}
*/
	/**
	 * Establece las condiciones adicionales necesarias para contemplar el
	 * pais y el estado al momento de obtener los municipios.
	 */
	protected void setCondicionesAdicionales() {
		//log.debug("setCondicionesAdicionales(E)");

		//Se valida que esten los campos requeridos:
		try {
			if (this.clavePais == null || this.clavePais.equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		} catch (Exception e) {
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		String condicionAdicional = "";
		
		condicionAdicional = " ic_pais = ? ";
		
		super.addCondicionAdicional(condicionAdicional);
		//agregar a variables bind estos valores
		super.addVariablesBind(new Integer(clavePais));
		//log.debug("setCondicionesAdicionales(S)");
	}


/**
* Establece el id del pais para condicion de la consulta
* @param clavePais Clave del pais por el que se filtran los estados
*/	
	public void setClavePais(String clavePais)
	{
		this.clavePais = clavePais;
	}


}