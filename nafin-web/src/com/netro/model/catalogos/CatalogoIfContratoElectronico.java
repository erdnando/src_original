package com.netro.model.catalogos;

import netropology.utilerias.AppException;


/**
 * Catalogo de If con contrato electronico (comcat_plazo)
 *
 * Ejemplo de uso:
 * CatalogoIfContratoElectronico cat = new CatalogoIfContratoElectronico();
 * cat.setCampoClave("ic_if");
 * cat.setCampoDescripcion("cg_razon_social");
 * cat.setClaveProducto("1");
 * cat.setOrden("cg_razon_social");
 * List l = cat.getListaElementos();  //Lista con elementos ElementoCatalogo
 *
 * @author Hugo Vargas
 */
public class CatalogoIfContratoElectronico extends CatalogoSimple  {
	//Variable para enviar mensajes al log. 
	private String bancoFondeo = "";
	
	public CatalogoIfContratoElectronico() {
		super.setTabla("comcat_if i");
		super.setPrefijoTablaPrincipal("i.");
	}
	/**
	 * Establece las condiciones adicionales necesarias para contemplar el
	 * pais y el estado al momento de obtener los municipios.
	 */
	protected void setCondicionesAdicionales() {
		//Se valida que esten los campos requeridos:
		try {
			if (this.bancoFondeo == null || this.bancoFondeo.equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		} catch (Exception e) {
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		String condicionAdicional = "";
		
		condicionAdicional = " EXISTS ( " +
				" SELECT ci.ic_if " +
				" FROM com_contrato_if ci " +
				" WHERE ci.ic_if = i.ic_if) " +
				" AND EXISTS ("+
				"			SELECT rie.ic_if"+
				" 			FROM comrel_if_epo_x_producto rie,"+
				" 		 			comcat_epo epo"+
				" 			WHERE rie.ic_if = i.ic_if"+
				"			AND rie.ic_epo = epo.ic_epo"+
				"			AND rie.ic_producto_nafin = 1 " +
				"			AND epo.ic_banco_fondeo = ? )";
		
		super.addCondicionAdicional(condicionAdicional);
		//agregar a variables bind estos valores
		super.addVariablesBind(new Integer(this.bancoFondeo));
	}

	/**
	 * Establece el producto nafin del cual se desean obetener los plazos
	 * @param claveProducto Clave del producto (ic_producto_nafin)
	 */
	public void setBancoFondeo(String fondeo) {
		this.bancoFondeo = fondeo;
	}


	public String getBancoFondeo() {
		return bancoFondeo;
	}
	
	
}