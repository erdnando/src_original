package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 *
 *
 * Ejemplo de uso:
 * CatalogoCreditCons cat = new CatalogoCreditoCons();
 * cat.setCampoClave("cp.ic_pyme"); y/o para el caso de obtener el numero de dist.  cat.setCampoClave("cg_num_distribuidor");
 * cat.setCampoDescripcion("cg_razon_social");
 * ///Opcionales ....
 * cat.setOrden("cg_razon_social");
 * cat.setIntermediario(String intermediario);
 * cat.setTasa(String tasa);
 *	cat.setCredito(String credito);
 *	cat.setAmort(String amort);
 *	cat.setEmisor(String emisor);
 *	cat.setPlazo(String plazo);
 *	cat.setSeleccionado(String seleccionado):
 *	cat.setHabilitado(String habilitado);
 *	cat.setPiso(String piso);
 * cat.setValoresCondicionIn("?,?", java.lang.Integer.class); //En caso de requerirse (Opcional)
 * cat.setValoresCondicionNotIn("?,?", java.lang.Integer.class); //En caso de requerirse (Opcional)
 *	cat.setOperacion(String operacion);
 *
 */
 
public class CatalogoCreditoCons extends CatalogoAbstract {
	
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoCreditoCons.class);

	StringBuffer tablas = new StringBuffer();

	public CatalogoCreditoCons() {
		super();
		super.setPrefijoTablaPrincipal("t.");
	}
	
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colección de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		boolean hayElementos = false;

		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer hint = new StringBuffer();
		StringBuffer condicionJoin = new StringBuffer();
		String aux="";
		
		if (super.getCampoClave().equals("t.ic_if")){
			aux="bo.ic_if";
			tablas.append("comcat_if t");
		}
		if (super.getCampoClave().equals("t.ic_tasa")){
			aux="bo.ic_tasa";
			tablas.append("comcat_tasa t");
		}
		if (super.getCampoClave().equals("t.ic_tipo_credito")){
			aux="bo.ic_tipo_credito";
			tablas.append("comcat_tipo_credito t");
		}
		if (super.getCampoClave().equals("t.ic_tabla_amort")){
			aux="bo.ic_tabla_amort";
			tablas.append("comcat_tabla_amort t");
		}
		if (super.getCampoClave().equals("t.ic_emisor")){
			aux="bo.ic_emisor";
			tablas.append("comcat_emisor t");
		}

		if (super.getCampoClave().equals("t.ic_if")||!intermediario.equals("")||!tasa.equals("")
			||!credito.equals("")||!amort.equals("")||!emisor.equals("")||!plazo.equals("")
			||!piso.equals("")||(operacion.equals("equipamiento")&&super.getCampoClave().equals("ic_if"))){

			tablas.append(", com_base_op_credito bo");
			condicionJoin.append(aux +"= "+super.getCampoClave());

			hayElementos = true;
			if(this.operacion != null && this.operacion.equals("credito")){
				tablas.append(", comcat_tasa ta, comcat_tipo_credito tc");
				condicionJoin.append(" AND bo.ic_tasa = ta.ic_tasa " +
											" AND bo.ic_tipo_credito = tc.ic_tipo_credito ");
			}
		}
		if (this.habilitado != null && !this.habilitado.equals("")) {
			if(!super.getCampoClave().equals("ic_if")){
				
			}
		}

		
		if(!hayElementos){
			condicionJoin.append("1 = 1");
		}
		
		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind

		StringBuffer qrySQL = new StringBuffer();
		qrySQL.append(
				" SELECT " + hint + " DISTINCT " + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
		);

		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y además se manda a llamar la implementación especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}

		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}

		log.debug("qrySQL ::: "+qrySQL.toString());
		log.debug("vars ::: "+super.getValoresBindCondicion());

		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}

		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		StringBuffer condicionAdicional = new StringBuffer(128);

		if (this.intermediario != null && !this.intermediario.equals("")){
			condicionAdicional = new StringBuffer(128);
			condicionAdicional.append(" bo.ic_if = ? ");
			super.addVariablesBind(this.intermediario);
			super.addCondicionAdicional(condicionAdicional.toString());
		}
		if (this.tasa != null && !this.tasa.equals("")){
			condicionAdicional = new StringBuffer(128);
			condicionAdicional.append(" bo.ic_tasa = ? ");
			super.addVariablesBind(this.tasa);
			super.addCondicionAdicional(condicionAdicional.toString());
		}
		if (this.credito != null && !this.credito.equals("")){
			condicionAdicional = new StringBuffer(128);
			condicionAdicional.append(" bo.ic_tipo_credito = ? ");
			super.addVariablesBind(this.credito);
			super.addCondicionAdicional(condicionAdicional.toString());
		}
		if (this.amort != null && !this.amort.equals("")){
			condicionAdicional = new StringBuffer(128);
			condicionAdicional.append(" bo.ic_tabla_amort = ? ");
			super.addVariablesBind(this.amort);
			super.addCondicionAdicional(condicionAdicional.toString());
		}
		if (this.emisor != null && !this.emisor.equals("")){
			condicionAdicional = new StringBuffer(128);
			if(this.emisor.equals("0")){
				condicionAdicional.append(" bo.ic_emisor IS null ");
			}else{
				condicionAdicional.append(" bo.ic_emisor = ? ");
				super.addVariablesBind(this.emisor);
			}
			super.addCondicionAdicional(condicionAdicional.toString());
		}
		if (this.plazo != null && !this.plazo.equals("")){
			condicionAdicional = new StringBuffer(128);
			condicionAdicional.append(" bo.cs_tipo_plazo = ? ");
			super.addVariablesBind(this.plazo);
			super.addCondicionAdicional(condicionAdicional.toString());
		}
		if (!this.piso.equals("")&&super.getCampoClave().equals("t.ic_if")){
			condicionAdicional = new StringBuffer(128);
			condicionAdicional.append(" t.ig_tipo_piso = ? ");
			super.addVariablesBind(this.piso);
			super.addCondicionAdicional(condicionAdicional.toString());
		}
		if (!this.piso.equals("") && !super.getCampoClave().equals("t.ic_if")){
			condicionAdicional = new StringBuffer(128);
			condicionAdicional.append(" bo.ig_tipo_cartera = ? ");
			super.addVariablesBind(this.piso);
			super.addCondicionAdicional(condicionAdicional.toString());
		}
		if (this.habilitado != null && !this.habilitado.equals("")){
			condicionAdicional = new StringBuffer(128);
			if(super.getCampoClave().equals("t.ic_if")){
					condicionAdicional.append(" t.cs_habilitado = ? ");
					super.addVariablesBind(this.habilitado);
			}else{
					condicionAdicional.append(" t.cs_creditoelec = ? ");
					super.addVariablesBind(this.habilitado);			
			}
			super.addCondicionAdicional(condicionAdicional.toString());
		}
		
		if (super.getCampoClave().equals("t.ic_if")||!intermediario.equals("")||!tasa.equals("")
			||!credito.equals("")||!amort.equals("")||!emisor.equals("")||!plazo.equals("")
			||!piso.equals("")||(operacion.equals("equipamiento")&&super.getCampoClave().equals("ic_if"))){
			
			if(this.operacion != null && this.operacion.equals("credito")){
				condicionAdicional = new StringBuffer(128);
				condicionAdicional.append(" ta.cs_creditoelec = ? AND tc.cs_creditoelec = ? ");
				super.addVariablesBind("S");
				super.addVariablesBind("S");
				super.addCondicionAdicional(condicionAdicional.toString());
			}
			if (this.operacion != null && this.operacion.equals("equipamiento")) {
				condicionAdicional = new StringBuffer(128);
				condicionAdicional.append(" bo.ic_producto_nafin = ? ");
				super.addVariablesBind(new Integer(7));

				if(super.getCampoClave().equals("t.ic_if")){
					condicionAdicional.append(" AND bo.cs_tipo_plazo= ? ");
					super.addVariablesBind("C");
				}
			}else{
				condicionAdicional = new StringBuffer(128);
				condicionAdicional.append(" bo.ic_producto_nafin IS NULL");
			}
			super.addCondicionAdicional(condicionAdicional.toString());
		}
	}

	/**
	 * Establece la clave de la epo.
	 * @param claveEpo Clave de la epo (ic_epo)
	 */

	public void setIntermediario(String intermediario) {
		this.intermediario = intermediario;
	}
	public void setTasa(String tasa) {
		this.tasa = tasa;
	}
	public void setCredito(String credito) {
		this.credito = credito;
	}
	public void setAmort(String amort) {
		this.amort = amort;
	}
	public void setEmisor(String emisor) {
		this.emisor = emisor;
	}
	public void setPlazo(String plazo) {
		this.plazo = plazo;
	}
	public void setSeleccionado(String seleccionado) {
		this.seleccionado = seleccionado;
	}
	public void setHabilitado(String habilitado) {
		this.habilitado = habilitado;
	}
	public void setPiso(String piso) {
		this.piso = piso;
	}	

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	private String campoClave;
	private String campoDescripcion;
	private String intermediario = "";
	private String tasa = "";
	private String credito = "";
	private String amort = "";
	private String emisor = "";
	private String plazo = "";
	private String seleccionado = "";
	private String habilitado = "";
	private String piso = "";
	private String operacion = "";

}