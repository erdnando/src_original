package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


/**
 * @author Deysi Hern�ndez Contreras
 */
public class CatalogoPYMEDescTasas extends CatalogoAbstract {
	
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoPYMEDescTasas.class);
	 
	
	
	StringBuffer tablas = new StringBuffer();
	String group = "";
	public String claveIF;
	private String claveMoneda;
	private String claveEPO;
	
	
	public CatalogoPYMEDescTasas() {
		super();		
	}
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colecci�n de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
 
		
		StringBuffer hint = new StringBuffer();
		StringBuffer condicionJoin = new StringBuffer();
		StringBuffer group = new StringBuffer();
		StringBuffer tablas = new StringBuffer();
		
		
		tablas.append( " comrel_pyme_if cpi, comrel_cuenta_bancaria ccb, comcat_pyme cp ");
	 
		condicionJoin.append(" cpi.ic_cuenta_bancaria = ccb.ic_cuenta_bancaria");
		condicionJoin.append(" AND ccb.ic_pyme = cp.ic_pyme");
	

		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
		
		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(
				" SELECT  " + hint  +"distinct "+ super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
				);
				
				
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y adem�s se manda a llamar la implementaci�n especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}		
	
		if(!group.equals("")){
				qrySQL.append(group);
		}
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		
		
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		String  condicionAdicional ="";
		
		try{
			
			condicionAdicional = "  cpi.cs_vobo_if = ? "+
										" AND cpi.cs_borrado = ? "+ 
										" AND ccb.cs_borrado = ? ";
										
			super.addCondicionAdicional(condicionAdicional);
			super.addVariablesBind("S");
			super.addVariablesBind("N");	
			super.addVariablesBind("N");	
			
			
			if(!"".equals(claveIF)) {
				condicionAdicional ="  cpi.ic_if = ? ";
				super.addCondicionAdicional(condicionAdicional);
				super.addVariablesBind(claveIF);
			}
			
			if(claveMoneda !=null) {
				condicionAdicional =" ic_moneda =  ? "; 
				super.addCondicionAdicional(condicionAdicional);
				super.addVariablesBind(claveMoneda);
			}
			
			if(!"".equals(claveEPO)) {
				condicionAdicional ="  cpi.ic_epo in ( "+claveEPO+" )";
				super.addCondicionAdicional(condicionAdicional);
			}			
				
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
		

	}

	public String getClaveIF() {
		return claveIF;
	}

	public void setClaveIF(String claveIF) {
		this.claveIF = claveIF;
	}

	public String getClaveMoneda() {
		return claveMoneda;
	}

	public void setClaveMoneda(String claveMoneda) {
		this.claveMoneda = claveMoneda;
	}

	public String getClaveEPO() {
		return claveEPO;
	}

	public void setClaveEPO(String claveEPO) {
		this.claveEPO = claveEPO;
	}


	


	




}