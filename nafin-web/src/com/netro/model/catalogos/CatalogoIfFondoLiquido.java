package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


/**
 * Catalogo de If involucrados en las cargas de Fondos Liquidos
 *
 * Ejemplo de uso:
 * CatalogoIfFondoLiquido cat = new CatalogoIfFondoLiquido();
 * cat.setCampoClave("ic_if");
 * cat.setCampoDescripcion("cg_razon_social");
 * cat.setOrden("cg_razon_social");
 *
 * @author Fabian Valenzuela
 */
public class CatalogoIfFondoLiquido extends CatalogoAbstract {
	
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoIfFondoLiquido.class);
	
	private String tipoUsuario, strPerfil, strUsuario;
		
	StringBuffer tablas = new StringBuffer();
	
	public CatalogoIfFondoLiquido() {
		super();
		super.setPrefijoTablaPrincipal("odi."); 
	}
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colección de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		boolean tablaExtra = false;
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer hint = new StringBuffer();
	
		tablas.append(" ope_carga_saldo_fliquido ocs ");
		tablas.append(" ,comcat_if odi ");
		if("OP OP".equals(strPerfil)){
			tablas.append(" ,ope_reg_solicitud op "); tablaExtra = true;
		}
		if("SUP SEG".equals(strPerfil)){
			tablas.append(" ,ope_datos_if op "); tablaExtra = true;
		}
		if("EJE PROMO".equals(strPerfil)){
			tablas.append(" ,ope_datos_if op "); tablaExtra = true;
		}
		if("SUB LI CP".equals(strPerfil)){
			tablas.append(" ,ope_reg_solicitud op "); tablaExtra = true;
		}
		if("SUB MICRO".equals(strPerfil)){
			tablas.append(" ,ope_reg_solicitud op "); tablaExtra = true;
		}
		//hint.append(" /*+ USE_NL(fi ff) */ " );

		StringBuffer condicionJoin = new StringBuffer();
		
		condicionJoin.append(" ocs.ic_if = odi.ic_if ");
		if(tablaExtra){
			condicionJoin.append(" AND ocs.ic_if = op.ic_if ");
		}
		

		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
		
		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(
				" SELECT " + hint + " DISTINCT "  + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
				);
				
				
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y además se manda a llamar la implementación especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}
	
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		String condicionAdicional = "";

		try{
			//if (this.claveEpo != null && !this.claveEpo.equals("") ){
				//Integer.parseInt(this.claveEpo);
				//throw new Exception("Los parametros requeridos no has sido establecidos");
			//}
			
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
		

		
		if("OP OP".equals(strPerfil) && strUsuario!=null && !strUsuario.equals("")){
			condicionAdicional +=" op.ic_ejecutivo_op = ? ";
		}
		if("SUP SEG".equals(strPerfil) && strUsuario!=null && !strUsuario.equals("")){
			condicionAdicional +=" op.cg_ejec_seg = ? ";
		}
		if("EJE PROMO".equals(strPerfil) && strUsuario!=null && !strUsuario.equals("")){
			condicionAdicional +=" op.cg_ejec_promo = ? ";
		}
		if("SUB LI CP".equals(strPerfil) && strUsuario!=null && !strUsuario.equals("")){
			condicionAdicional +=" op.ic_contrato in (?,?,?) ";
		}
		if("SUB MICRO".equals(strPerfil) && strUsuario!=null && !strUsuario.equals("")){
			condicionAdicional +=" op.ic_contrato in (?,?) ";
		}
		
	
		if(condicionAdicional.length()>0)
			super.addCondicionAdicional(condicionAdicional);
		
		//agregar a variables bind estos valores
		if("OP OP".equals(strPerfil) && strUsuario!=null && !strUsuario.equals("")){
			super.addVariablesBind(strUsuario);
		}
		if("SUP SEG".equals(strPerfil) && strUsuario!=null && !strUsuario.equals("")){
			super.addVariablesBind(strUsuario);
		}
		if("EJE PROMO".equals(strPerfil) && strUsuario!=null && !strUsuario.equals("")){
			super.addVariablesBind(strUsuario);
		}
		if("SUB LI CP".equals(strPerfil) && strUsuario!=null && !strUsuario.equals("")){
			super.addVariablesBind(new Integer(1));
			super.addVariablesBind(new Integer(2));
			super.addVariablesBind(new Integer(3));
		}
		if("SUB MICRO".equals(strPerfil) && strUsuario!=null && !strUsuario.equals("")){
			super.addVariablesBind(new Integer(4));
			super.addVariablesBind(new Integer(5));
		}
			
			
	}


	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}


	public String getTipoUsuario() {
		return tipoUsuario;
	}


	public void setStrPerfil(String strPerfil) {
		this.strPerfil = strPerfil;
	}


	public String getStrPerfil() {
		return strPerfil;
	}


	public void setStrUsuario(String strUsuario) {
		this.strUsuario = strUsuario;
	}


	public String getStrUsuario() {
		return strUsuario;
	}
}