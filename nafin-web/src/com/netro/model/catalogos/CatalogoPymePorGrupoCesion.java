package com.netro.model.catalogos;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.naming.NamingException;
import netropology.utilerias.*;
import org.apache.commons.logging.Log;

public class CatalogoPymePorGrupoCesion extends CatalogoAbstract{

	private final static Log log = ServiceLocator.getInstance().getLog(CatalogoPymePorGrupoCesion.class);
	private String razonSocial;
	private String rfc;
	private String nafinElectronico;
	private String icEpo;


	public CatalogoPymePorGrupoCesion() {
		super();
	}
	
	public List getListaElementos(){

		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		String tables = "";
		StringBuffer condiciones = new StringBuffer();
		String orden = "";
		String query = "";

		log.info("getListaElementos(E)");

		try{

			con.conexionDB();
			Registros regCatalogo = null;
			tables = " COMREL_PYME_EPO C, COMCAT_PYME P, COMREL_NAFIN N ";

			if (this.icEpo != null && !this.icEpo.equals("")) {
				this.icEpo = this.icEpo.replace('*','%');
				condiciones.append(" AND C.IC_EPO = " + this.icEpo + " ");
			}
			if (this.razonSocial != null && !this.razonSocial.equals("")) {
				this.razonSocial = this.razonSocial.replace('*','%');
				condiciones.append(" AND P.CG_RAZON_SOCIAL LIKE '" + this.razonSocial + "' ");
			}
			if (this.rfc != null && !this.rfc.equals("")) {
				this.rfc = this.rfc.replace('*','%');
				condiciones.append(" AND P.CG_RFC LIKE '" + this.rfc + "' ");
			}
			if (this.nafinElectronico != null && !this.nafinElectronico.equals("")) {
				this.nafinElectronico = this.nafinElectronico.replace('*','%');
				condiciones.append(" AND N.IC_NAFIN_ELECTRONICO LIKE '" + this.nafinElectronico + "' ");
			}

			orden = " ORDER BY P.CG_RAZON_SOCIAL ";

			query += " SELECT DISTINCT " + super.getCampoClave() + " AS CLAVE, " +
						super.getCampoDescripcion() +" AS DESCRIPCION " +
						" FROM " + tables +
						" WHERE C.IC_PYME = P.IC_PYME " +
						" AND C.IC_PYME = N.IC_EPO_PYME_IF " +
						" AND P.IC_PYME = N.IC_EPO_PYME_IF " +
						" AND C.CS_ACEPTACION = 'H' " +
						" AND C.CS_HABILITADO = 'S' " +
						" AND P.CS_CESION_DERECHOS = 'S' " +
						" AND N.CG_TIPO = 'P' " + condiciones.toString();

			//Genera las condiciones necesarias y las coloca en this.condicion
			//Al llamar a setCondicion se establecen las condiciones para 
			//IN y NOT IN... y además se manda a llamar la implementación especifica
			//setCondicionesAdicionales de la clase
			super.setCondicion();
	
			if (super.getCondicion().length()>0) {
				query += " AND " + super.getCondicion();
			}
			
			if (super.getOrden() == null || super.getOrden().equals("")) {
				query += 
						" ORDER BY " + super.getCampoDescripcion() ;
			} else {
				query += 
						" ORDER BY " + super.getOrden() ;
			}

			log.debug("..:: query: " + query);

			regCatalogo = con.consultarDB(query,super.getValoresBindCondicion());
			
			int cont = 0;
			while(regCatalogo.next() && cont < 2001) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(regCatalogo.getString("CLAVE"));
				elementoCatalogo.setDescripcion(regCatalogo.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
				cont++;
			}

		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}

		log.info("getListaElementos(S)");
		return coleccionElementos;
	}

/**********************************************************************
 *                         GETTERS Y SETTERS                          *
 **********************************************************************/
 	public String getIcEpo() {		
		return this.icEpo;	
	}
 
	public void setIcEpo(String icEpo) {		
		this.icEpo = icEpo;	
	}

 	public String getRazonSocial() {		
		return this.razonSocial;	
	}
 
	public void setRazonSocial(String razonSocial) {		
		this.razonSocial = razonSocial;	
	}

 	public String getRfc() {		
		return this.rfc;	
	}
 
	public void setRfc(String rfc) {		
		this.rfc = rfc;	
	}

 	public String getNafinElectronico() {		
		return this.nafinElectronico;	
	}
 
	public void setNafinElectronico(String nafinElectronico) {		
		this.nafinElectronico = nafinElectronico;	
	}

}