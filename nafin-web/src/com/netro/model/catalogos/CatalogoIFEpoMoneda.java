package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


/**
 * Catalogo de IFs del producto de EPO
 *
 * Ejemplo de uso:
 * CatalogoIFEpo cat = new CatalogoIFEpo();
 * cat.setCampoClave("ic_if");
 * cat.setCampoDescripcion("cg_razon_social");
 * cat.setClaveEpo("1");//Opcional
 * cat.setOrden("cg_razon_social");
 *
 * @author Gerardo Perez
 */
public class CatalogoIFEpoMoneda extends CatalogoAbstract {
	
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoIFEpo.class);
	
	StringBuffer tablas = new StringBuffer();
	
	private String claveEpo;
	private String claveMoneda;
	private String operaFideicomisoEPO;
	
	public CatalogoIFEpoMoneda() {
		super();
		super.setPrefijoTablaPrincipal("i."); //from comcat_if ci
	}
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colección de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer hint = new StringBuffer();
	
		StringBuffer condicionJoin = new StringBuffer();
		tablas.append(" comcat_if i, comrel_if_epo ie, COMREL_IF_EPO_MONEDA m ");
			
		condicionJoin.append(" i.ic_if = ie.ic_if "+
										" AND ie.ic_epo = m.ic_epo "+ 
										" AND ie.ic_if = m.ic_if  ");

		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
		
		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(
				" SELECT " + hint + " DISTINCT " + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
				);
				
				
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y además se manda a llamar la implementación especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}
	
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		StringBuffer condicionAdicional = new StringBuffer(128);
		
		if (!this.claveEpo.equals("") && operaFideicomisoEPO.equals("N") ) {

			condicionAdicional.append(
					 " ie.ic_epo = ? "+
					" AND ie.cs_aceptacion = ? "+
					" AND ie.cs_vobo_nafin = ? "+
					 " AND ie.cs_bloqueo = ? ");
			super.addVariablesBind(claveEpo);		 
			super.addVariablesBind("S");
			super.addVariablesBind("S");
			super.addVariablesBind("N");
		}
		if (!this.claveEpo.equals("") && operaFideicomisoEPO.equals("S") ) {
			
			condicionAdicional.append(
					 " ie.ic_epo = ? "+
					" AND ie.cs_aceptacion = ? "+					
					" and i.CS_OPERA_FIDEICOMISO = ? ");
			super.addVariablesBind(claveEpo);		 
			super.addVariablesBind("S");			
			super.addVariablesBind("S");	
		}
		if(!claveMoneda.equals("")){
			condicionAdicional.append(" AND m.ic_moneda = ? ");			
			super.addVariablesBind(claveMoneda);
		}
		
		
		super.addCondicionAdicional(condicionAdicional.toString());
	}

	/**
	 * Establece la clave de la epo para obtener los intermediarios con los
	 * que trabaja
	 * @param claveEpo Clave de la epo (ic_epo)
	 */
	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}


	public void set_claveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}


	public String getClaveEpo() {
		return claveEpo;
	}


	public void setClaveMoneda(String claveMoneda) {
		this.claveMoneda = claveMoneda;
	}


	public String getClaveMoneda() {
		return claveMoneda;
	}

	public String getOperaFideicomisoEPO() {
		return operaFideicomisoEPO;
	}

	public void setOperaFideicomisoEPO(String operaFideicomisoEPO) {
		this.operaFideicomisoEPO = operaFideicomisoEPO;
	}

}