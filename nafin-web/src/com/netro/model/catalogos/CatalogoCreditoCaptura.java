package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 *
 *
 * Ejemplo de uso:
 * CatalogoCreditoCaptura cat = new CatalogoCreditoCaptura();
 * cat.setCampoClave("cp.ic_pyme"); y/o para el caso de obtener el numero de dist.  cat.setCampoClave("cg_num_distribuidor");
 * cat.setCampoDescripcion("cg_razon_social");
 * ///Opcionales ....
 * cat.setOrden("cg_razon_social");
 * cat.setIntermediario(String intermediario);
 * cat.setTasa(String tasa);
 *	cat.setCredito(String credito);
 *	cat.setAmort(String amort);
 *	cat.setEmisor(String emisor);
 *	cat.setPlazo(String plazo);
 *	cat.setSeleccionado(String seleccionado):
 *	cat.setHabilitado(String habilitado);
 *	cat.setPiso(String piso);
 * cat.setValoresCondicionIn("?,?", java.lang.Integer.class); //En caso de requerirse (Opcional)
 * cat.setValoresCondicionNotIn("?,?", java.lang.Integer.class); //En caso de requerirse (Opcional)
 *	cat.setOperacion(String operacion);
 *
 *
 */
 
public class CatalogoCreditoCaptura extends CatalogoAbstract {
	
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoCreditoCaptura.class);

	StringBuffer tablas = new StringBuffer();

	public CatalogoCreditoCaptura() {
		super();
		super.setPrefijoTablaPrincipal("t.");
	}
	
	/*public CatalogoPymeDistribuidores() {

	}*/
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colección de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		boolean hayElementos = false;

		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer hint = new StringBuffer();
		StringBuffer condicionJoin = new StringBuffer();
		String aux="";
		
		if (super.getCampoClave().equals("t.ic_if")){
			aux="bo.ic_if";
			tablas.append("comcat_if t");
		}
		if (super.getCampoClave().equals("t.ic_tasa")){
			aux="bo.ic_tasa";
			tablas.append("comcat_tasa t");
		}
		if (super.getCampoClave().equals("t.ic_tipo_credito")){
			aux="bo.ic_tipo_credito";
			tablas.append("comcat_tipo_credito t");
		}
		if (super.getCampoClave().equals("t.ic_tabla_amort")){
			aux="bo.ic_tabla_amort";
			tablas.append("comcat_tabla_amort t");
		}
		
		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind

		StringBuffer qrySQL = new StringBuffer();
		qrySQL.append(
				" SELECT DISTINCT " + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE 1 = 1 " + condicionJoin
		);

		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y además se manda a llamar la implementación especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}

		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}

		log.debug("qrySQL ::: "+qrySQL.toString());
		log.debug("vars ::: "+super.getValoresBindCondicion());

		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}

		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		StringBuffer condicionAdicional = new StringBuffer(128);

		if (!this.piso.equals("")){
			condicionAdicional = new StringBuffer(128);
			condicionAdicional.append(" t.ig_tipo_piso = ? ");
			super.addVariablesBind(this.piso);
			super.addCondicionAdicional(condicionAdicional.toString());
		}

		if (this.habilitado != null && !this.habilitado.equals("")){
			condicionAdicional = new StringBuffer(128);
			if(super.getCampoClave().equals("t.ic_if")){
					condicionAdicional.append(" t.cs_habilitado = ? ");
					super.addVariablesBind(this.habilitado);
			}else{
					condicionAdicional.append(" t.cs_creditoelec = ? ");
					super.addVariablesBind(this.habilitado);			
			}
			super.addCondicionAdicional(condicionAdicional.toString());
		}
	}

	/**
	 * Establece la clave de la epo.
	 * @param claveEpo Clave de la epo (ic_epo)
	 */

	public void setHabilitado(String habilitado) {
		this.habilitado = habilitado;
	}
	public void setPiso(String piso) {
		this.piso = piso;
	}	

	private String campoClave;
	private String campoDescripcion;
	private String habilitado = "";
	private String piso = "";

}