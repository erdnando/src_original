package com.netro.model.catalogos;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import netropology.utilerias.AccesoDB;


public class CatalogoElementosIF{
	private String clave;
	private String descripcion;
	private String orden;
	public CatalogoElementosIF(){}
	
	/**
	 * Obtiene una lista de elementos de tipo ElementoCatalogoIF
	 * @return regresa una colecci�n de elementos obtenidos
	 */	
	public  Vector getListaElementos(){
		System.out.println("..:::getListaElementos(E):");

		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		String strQuery;
		String condiciones;
		String		strSQL			= "";
		String		cg_razon_social	= "";
		String		ic_if			= "";
		String		cs_tipo_fondeo	= "";
		String		strFondeo		= "";
		ResultSet	rs				= null;
		Vector		vecFilas		= new Vector();
		try{
			con.conexionDB();
			if(this.orden != null){
				orden = " ORDER BY ci."+this.orden;
			}else{
				orden = "";
			}
			strQuery = "Select distinct ci."+this.clave+" AS CLAVE, ci."+this.descripcion+" AS DESCRIPCION " +
						"From comcat_if ci , comrel_if_epo_x_producto rie, comcat_epo epo, comcat_banco_fondeo cbf " +
						"Where ci.cs_habilitado = 'S' " +
						"And rie.ic_if = ci."+this.clave + 
						" And rie.ic_epo = epo.ic_epo " +
						"And epo.ic_banco_fondeo = cbf.ic_banco_fondeo " +
						" And cbf.ic_banco_fondeo = 1 " +orden;
			System.out.println("..:::: Query ==>> "+strQuery);
			
			rs = con.queryDB(strQuery);

			while(rs.next()) {
				Vector	vecColumnas	= new Vector();
					cg_razon_social = rs.getString("DESCRIPCION");
					ic_if = rs.getString("CLAVE");
					strFondeo = "SinValor";
					cs_tipo_fondeo = "N";
					vecColumnas.add(0, cg_razon_social);
					vecColumnas.add(1, ic_if);
					vecColumnas.add(2, cs_tipo_fondeo);
					//List ajuste = consultarAjusteAnual(ic_if, perfil);
					vecFilas.addElement(vecColumnas);
			}
			rs.close();
			con.cierraStatement();
			
		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
			if (con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		System.out.println("..:::getListaElementos(S):");
		return vecFilas;
	} 
	/**
	 * Funci�n que obtiene la clave de un IF en especial 
	 * @return regresa string con la clave del IF deseado
	 */
	public  String getIntermediarioFinanciero(String IF){
		System.out.println("..:::getIntermediarioFinanciero(E):");
		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		String strQuery;
		String condiciones;
		String		strSQL			= "";
		String		cg_razon_social	= "";
		String		ic_if			= "";
		String		cs_tipo_fondeo	= "";
		String		strFondeo		= "";
		ResultSet	rs				= null;
		Vector		vecFilas		= new Vector();
		try{
			con.conexionDB();
			strQuery = "Select distinct ci.ic_if, ci.cg_razon_social " +
						"From comcat_if ci , comrel_if_epo_x_producto rie, comcat_epo epo, comcat_banco_fondeo cbf " +
						"Where ci.cs_habilitado = 'S' " +
						"And rie.ic_if = ci.ic_if "+ 
						" And rie.ic_epo = epo.ic_epo " +
						" And ci.ic_if = " +IF+
						" And epo.ic_banco_fondeo = cbf.ic_banco_fondeo " +
						" And cbf.ic_banco_fondeo = 1 ";
			System.out.println("..:: : Query ==>> "+strQuery);
			rs = con.queryDB(strQuery);
			if(rs.next()) {
					cg_razon_social = rs.getString("cg_razon_social");
			}
			rs.close();
			con.cierraStatement();
		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException("No se pudo obtener dato");
		} finally {
			if (con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		System.out.println("..:::getIntermediarioFinanciero(S):");
		return cg_razon_social;
	}  
	
	

	public String getClave(){return clave;}
	public String getDescripcion(){return descripcion;}
	
/**
* Establece la clave(id) que se va obtener en la consulta
*/	
	public void setClave(String clave){
	this.clave = clave;
	}

/**
* Establece la descripcion  que se va obtener en la consulta
*/		
	public void setDescripcion(String descripcion){this.descripcion = descripcion;}
/**
* Establece valor para indicar alguna condicion especifica a consultar
*/	
/**
* Establece valor para indicar el orden de la consulta
*/
	public void setOrden(String orden){this.orden = orden;}
}