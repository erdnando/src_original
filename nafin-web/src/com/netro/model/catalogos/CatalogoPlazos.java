package com.netro.model.catalogos;

import netropology.utilerias.AppException;


/**
 * Catalogo de Plazos (comcat_plazo)
 *
 * Ejemplo de uso:
 * CatalogoPlazos cat = new CatalogoPlazos();
 * cat.setCampoClave("ic_plazo");
 * cat.setCampoDescripcion("cg_descripcion");
 * cat.setClaveProducto("1");
 * cat.setOrden("in_plazo_dias");
 * List l = cat.getListaElementos();  //Lista con elementos ElementoCatalogo
 *
 * para el caso en el que el campo clave no concuerde con la llave y se emplee
 * valores para in o not in, es necesario especificar el campo llave. ejemplo:
 * cat.setCampoClave("ic_plazo||ic_producto_nafin");
 * cat.setCampoDescripcion("cg_descripcion");
 * cat.setClaveProducto("1");
 * cat.setCampoLlave("ic_plazo");
 * cat.setValoresCondicionIn("1,9", Integer.class);
 * cat.setOrden("2");
 *
 * @author Gilberto Aparicio
 */
public class CatalogoPlazos extends CatalogoSimple  {
	//Variable para enviar mensajes al log. 
	private String claveProducto = "";
	
	public CatalogoPlazos() {
		super.setTabla("comcat_plazo");
	}
	/**
	 * Establece las condiciones adicionales necesarias para contemplar el
	 * pais y el estado al momento de obtener los municipios.
	 */
	protected void setCondicionesAdicionales() {
		//Se valida que esten los campos requeridos:
		try {
			if (this.claveProducto == null || this.claveProducto.equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		} catch (Exception e) {
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		String condicionAdicional = "";
		
		condicionAdicional = " ic_producto_nafin = ? ";
		
		super.addCondicionAdicional(condicionAdicional);
		//agregar a variables bind estos valores
		super.addVariablesBind(new Integer(this.claveProducto));
	}

	/**
	 * Establece el producto nafin del cual se desean obetener los plazos
	 * @param claveProducto Clave del producto (ic_producto_nafin)
	 */
	public void setClaveProducto(String claveProducto) {
		this.claveProducto = claveProducto;
	}


	public String getClaveProducto() {
		return claveProducto;
	}
	
	
}