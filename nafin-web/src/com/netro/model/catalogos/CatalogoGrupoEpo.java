package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;

/**
 * Esta clase genera u obtiene los valores del catalogo
 * comcat_grupo_epo, en base a ciertas condiciones establecidas
 */
public class CatalogoGrupoEpo extends CatalogoAbstract
{

	private String seleccion;
	private String order;
	private String clavePyme;
	
	public CatalogoGrupoEpo() {
		super();
		super.setPrefijoTablaPrincipal("cge."); //comcat_moneda M
	}
/**
 * Obtiene una lista de elementos de tipo ElementoCatalogo
 * @return regresa una colecci�n de elementos obtenidos
 */	
	public List getListaElementos()
	{
		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		String tables, condiciones, orden;
		StringBuffer qryCatGrupo=new StringBuffer("");
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
		try
		{
			con.conexionDB();
			Registros regCatExportador	=	null;
																				
			tables			= " comrel_pyme_epo pe, comrel_grupo_x_epo rge, comcat_grupo_epo cge ";
			
										
			qryCatGrupo.append( "select "+super.getCampoClave()+" AS CLAVE, " + 
											super.getCampoDescripcion() +" AS DESCRIPCION "+
											" FROM " + tables+" where" );
			super.setCondicion();
				
				if (super.getCondicion().length()>0) {
					qryCatGrupo.append( super.getCondicion());
				}
			
				if (super.getOrden() == null || super.getOrden().equals("")) {
					qryCatGrupo.append(
							" ORDER BY " + super.getCampoDescripcion() );
				} else {
					qryCatGrupo.append(
							" ORDER BY " + super.getOrden() );
				}
			
			
			regCatExportador = con.consultarDB(qryCatGrupo.toString(),super.getValoresBindCondicion());
			while(regCatExportador.next()) {
					ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					elementoCatalogo.setClave(regCatExportador.getString("CLAVE"));
					elementoCatalogo.setDescripcion(regCatExportador.getString("DESCRIPCION"));
					coleccionElementos.add(elementoCatalogo);
			}
		
		
		} catch(Exception e) {
				e.printStackTrace();
				throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
		}
				
		return coleccionElementos;													
		
	}
  
	public void setCondicionesAdicionales(){
		StringBuffer condicionAdicional = new StringBuffer("");
		
		try{
			if (this.clavePyme == null || this.clavePyme.equals("")){
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
			Integer.parseInt(this.clavePyme);
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
		
		condicionAdicional.append(
				"  pe.ic_epo = rge.ic_epo " +
				"AND rge.ic_grupo_epo =  cge.ic_grupo_epo " +
				"AND pe.ic_pyme = ? " +
				"AND pe.cs_aceptacion in (?, ?)  " +
				"and cge.CS_ACTIVO = ? " +
				"group by  cge.IC_GRUPO_EPO, cge.cg_descripcion " 

				);
		

    
		super.addCondicionAdicional(condicionAdicional.toString());
		//agregar a variables bind estos valores
		super.addVariablesBind(new Integer(this.clavePyme));
		super.addVariablesBind("R");
		super.addVariablesBind("H");
		super.addVariablesBind("S");

	}

	
/**
* Establece la clave(id) que se va obtener en la consulta
*/	
	public void setOrder(String order)
	{
		this.order = order;		
	}
	
/**
* Establece valor para indicar algun valor en especifico a consultar
*/	
	public void setSeleccion(String seleccion)
	{
		this.seleccion = seleccion;
	}




	public String getClave() {
		return super.getCampoClave();
	}





	public String getDescripcion() {
		return super.getCampoDescripcion();
	}





	public String getSeleccion() {
		return seleccion;
	}


	public void set_order(String order) {
		this.order = order;
	}


	public String getOrder() {
		return order;
	}


	public void setClavePyme(String clavePyme) {
		this.clavePyme = clavePyme;
	}


	public String getClavePyme() {
		return clavePyme;
	}

}