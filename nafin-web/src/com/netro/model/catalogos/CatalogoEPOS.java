package com.netro.model.catalogos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Catalogo de Epos
 *
 * Ejemplo de uso:
 * CatalogoEPOS cat = new CatalogoEPOS();
 * cat.setCampoClave("ic_epo");
 * cat.setCampoDescripcion("cg_razon_social");
 * cat.setOrden("cg_razon_social");
 *
 * @author Deysi Laura Hern�ndez Contreras
 */
public class CatalogoEPOS extends CatalogoAbstract {
	
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoEPOS.class);
	
	StringBuffer qrySQL = new StringBuffer();	
	
	private String aceptacion;
	private String intermediario;
	private String pyme;
	private String operaNotasCredito;
	private String producto;
	private String servicioDeDispersionBloqueado;
	private String dispersion;
	private String in;
	private String notin;
	private String tipoFactoraje;
	private String bancoFondeo;
	private String empresarial;
	
	public CatalogoEPOS() {     }
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colecci�n de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		
		try{
			if (getCampoClave() == null || getCampoClave().equals("") ||
				getCampoDescripcion() == null || getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
		
		StringBuffer tablas = new StringBuffer();
		StringBuffer hint = new StringBuffer();
		StringBuffer condicionJoin = new StringBuffer();
	
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
	
			if(this.intermediario != null) {
			
				tablas.append("comcat_epo E, comrel_if_epo IE, comcat_if I ");
				condicionJoin.append(" WHERE E.ic_epo = IE.ic_epo and I.ic_if=IE.ic_if"+
								" and I.ic_if = ?"+ 
								" and IE.cs_vobo_nafin = ? " +
								" and E.cs_habilitado = ?"+
								" and IE.cs_bloqueo = ?");// FODEA 002 - 2009
								
				addVariablesBind(intermediario);
				addVariablesBind("S");		
				addVariablesBind("S");
				addVariablesBind("N");// FODEA 002 - 2009
				
			} else if (this.pyme != null ) {
			
			
				// Para las pymes afiliadas a una Epo con contrato parametrizado
				// se les revisa que hayan aceptado este, de lo contrario 
				// el combo de EPOs no mostrar� dicha EPO.
					
				String strSQLContrato = 
							" SELECT pe.ic_epo, 'CatalogoEpo.java' as query " +
							" FROM  " +
							" comrel_pyme_epo pe,  " +
							" ( " +
							" SELECT ic_epo, MAX(ic_consecutivo) as ic_consecutivo " +
							" FROM com_contrato_epo ce " +
							" WHERE cs_mostrar='S' " +
							" GROUP BY ic_epo " +
							" ) contrato_epo " +
							" WHERE pe.ic_pyme = ? " +
							" AND pe.ic_epo = contrato_epo.ic_epo " +
							" AND NOT EXISTS ( " +
							"          SELECT ic_epo, ic_consecutivo " +
							"          FROM com_aceptacion_contrato ac " +
							"          WHERE " +
							"          ac.ic_pyme = ? " +
							" 	        AND ac.ic_epo = contrato_epo.ic_epo " +
							"          AND ac.ic_consecutivo = contrato_epo.ic_consecutivo " +
							"          ) ";
	
				PreparedStatement ps = con.queryPrecompilado(strSQLContrato);
				StringBuffer eposNotIn = new StringBuffer();
				ps.setInt(1, Integer.parseInt(pyme));
				ps.setInt(2, Integer.parseInt(pyme));
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					eposNotIn.append(rs.getString("ic_epo") + ",");
				}
				rs.close();
				ps.close();
			
				String condicionContrato = "";
				List lEpos = null;
				if (eposNotIn.length() > 0) {
					eposNotIn.deleteCharAt(eposNotIn.length()-1);
					//Obtiene la lista de valores separados por coma y los coloca dentro de una lista como objetos Integer.
					lEpos = Comunes.explode("," , eposNotIn.toString(), java.lang.Integer.class);
					condicionContrato = " AND PE.ic_epo NOT IN (" + 
					Comunes.repetirCadenaConSeparador("?", ",", lEpos.size()) + ")";
						
				}
				tablas.append(" comcat_epo E, comrel_pyme_epo PE ");
				
				if (this.operaNotasCredito != null ) {
					tablas.append(",comrel_producto_epo PRE ");
				}
				
				condicionJoin.append(" where PE.ic_pyme = ? " + 
					" and PE.ic_epo = E.ic_epo " +
					" and E.cs_habilitado=? " +
					" and PE.cs_habilitado=? " + condicionContrato);
				addVariablesBind(new Integer(pyme));
				addVariablesBind("S");
				addVariablesBind("S");
				
				if (lEpos != null ) {//Si hay Valores para la condicionContrato
					addVariablesBind(lEpos);
				}
				if(this.aceptacion != null  ){			
					condicionJoin.append(" and PE.cs_aceptacion = ? ");
					addVariablesBind(aceptacion);
				}
								
				if(this.operaNotasCredito != null  ){
					condicionJoin.append(" and PRE.ic_epo = E.ic_epo " +
							" and PRE.ic_producto_nafin = ? " +
							" and nvl(PRE.cs_opera_notas_cred,?) = ? ");
					addVariablesBind(new Integer(1)); //1=Descuento Electronico
					addVariablesBind("N");
					addVariablesBind(this.operaNotasCredito);
				}
			}else {
			
				tablas.append(" comcat_epo E ");
				condicionJoin.append(" where E.cs_habilitado=? ");
				addVariablesBind("S");
				
				if(this.producto != null  ){
					tablas.append(" , comrel_producto_epo cpe ");
					condicionJoin.append(
						" and cpe.ic_epo = E.ic_epo "+
				  		" and cpe.ic_producto_nafin = ? ");
				  	addVariablesBind(new Integer(producto));
					
					if( this.dispersion != null ){
						condicionJoin.append(" and cpe.cg_dispersion = ? ");
						addVariablesBind(dispersion);
					}
					
					
					if( this.servicioDeDispersionBloqueado != null ){
						condicionJoin.append(" and cpe.cs_bloqueado = ? ");
						addVariablesBind(servicioDeDispersionBloqueado);
					}
				}
				
			}
			
		
			if (this.in != null && !this.in.equals("")) {
			
				List lEposIn = Comunes.explode("," , this.in, java.lang.Integer.class);					
				condicionJoin.append(" AND E.ic_epo IN (" + 
				Comunes.repetirCadenaConSeparador("?", ",", lEposIn.size()) + ")");					
				addVariablesBind(lEposIn);
			}
			
			if (this.notin != null && !this.notin.equals("")) {
			
				List lEposNotIn = Comunes.explode("," , this.notin, java.lang.Integer.class);					
				condicionJoin.append(" AND E.ic_epo NOT IN (" + 
				Comunes.repetirCadenaConSeparador("?", ",", lEposNotIn.size()) + ")");					
				addVariablesBind(lEposNotIn);
			}
			
						
			if (this.tipoFactoraje != null && this.producto != null ) {
				if (tipoFactoraje.equals("V")) {
					condicionJoin.append(" AND cpe.cs_factoraje_vencido=? ");
					addVariablesBind("S");
				} else if (tipoFactoraje.equals("D")) {
					condicionJoin.append(" AND cpe.cs_factoraje_distribuido=? ");
					addVariablesBind("S");
				}
			} else if ( this.tipoFactoraje != null && this.producto != null ) {
			
				if (tipoFactoraje.equals("I")) {
					tablas.append(" , com_parametrizacion_epo cpe ");
					condicionJoin.append(
							" and cpe.ic_epo = E.ic_epo "+
						  " and cpe.cc_parametro_epo = 'PUB_EPO_VENC_INFONAVIT' " +
						  " and cpe.cg_valor = ? ");
					addVariablesBind("S"); //S = Si
				} else {
						tablas.append(" , comrel_producto_epo cpe ");
						condicionJoin.append(
								" and cpe.ic_epo = E.ic_epo "+
								" and cpe.ic_producto_nafin = ? ");
						addVariablesBind(new Integer(1)); //1 = Descuento Electronico
				}
				if (tipoFactoraje.equals("V")) {
					condicionJoin.append(" AND cpe.cs_factoraje_vencido=? ");
					addVariablesBind("S");
				} else if (tipoFactoraje.equals("D")) {
					condicionJoin.append(" AND cpe.cs_factoraje_distribuido=? ");
					addVariablesBind("S");
				}
			}
			
			if (   this.bancoFondeo != null  ) {
				condicionJoin.append(" AND IC_BANCO_FONDEO = ? ");
				addVariablesBind(new Integer(this.bancoFondeo));
			}
			
			
			log.debug("empresarial ::: "+empresarial);
			if (   this.empresarial != null  ) {
				//Condicion para filtrar dependiendo si son o no de Nafin Empresarial
				if (this.empresarial.equals("S") || this.empresarial.equals("N")) {
					condicionJoin.append(
								" AND EXISTS(" +
								" 	SELECT ic_modalidad " +
								" 	FROM comrel_producto_epo prodepo " +
								" 	WHERE prodepo.ic_epo = E.ic_epo " +
								" 	AND prodepo.ic_producto_nafin = ? ");
					addVariablesBind(new Integer(8));
					
					if (this.empresarial.equals("S")) {
						condicionJoin.append(" AND prodepo.ic_modalidad=? ");
						addVariablesBind(new Integer(2));
					} else {
						condicionJoin.append(" AND (prodepo.ic_modalidad IS NULL OR prodepo.ic_modalidad=?) ");
						addVariablesBind(new Integer(1));
					}
					condicionJoin.append(" ) ");
				}			
			}
		
		
		qrySQL.append(
				" SELECT " + hint + " DISTINCT " + getCampoClave() + " AS CLAVE, " +
				getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				 condicionJoin
				);
				
				
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y adem�s se manda a llamar la implementaci�n especifica
		//setCondicionesAdicionales de la clase
		setCondicion();
		
		
		if (getCondicion().length()>0) {
			qrySQL.append(" AND " + getCondicion());
		}
	
		if (getOrden() == null || getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + getOrden() );
		}
		  
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+getValoresBindCondicion());
		
		
			registros = con.consultarDB(qrySQL.toString(), getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		StringBuffer condicionAdicional = new StringBuffer(128);
		

	}
  

	public String getAceptacion() {
		return aceptacion;
	}

	public void setAceptacion(String aceptacion) {
		this.aceptacion = aceptacion;
	}

	public String getIntermediario() {
		return intermediario;
	}

	public void setIntermediario(String intermediario) {
		this.intermediario = intermediario;
	}

	public String getPyme() {
		return pyme;
	}

	public void setPyme(String pyme) {
		this.pyme = pyme;
	}

	public String getOperaNotasCredito() {
		return operaNotasCredito;
	}

	public void setOperaNotasCredito(String operaNotasCredito) {
		this.operaNotasCredito = operaNotasCredito;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getServicioDeDispersionBloqueado() {
		return servicioDeDispersionBloqueado;
	}

	public void setServicioDeDispersionBloqueado(String servicioDeDispersionBloqueado) {
		this.servicioDeDispersionBloqueado = servicioDeDispersionBloqueado;
	}

	public String getDispersion() {
		return dispersion;
	}

	public void setDispersion(String dispersion) {
		this.dispersion = dispersion;
	}

	public String getIn() {
		return in;
	}

	public void setIn(String in) {
		this.in = in;
	}

	public String getNotin() {
		return notin;
	}

	public void setNotin(String notin) {
		this.notin = notin;
	}

	public String getTipoFactoraje() {
		return tipoFactoraje;
	}

	public void setTipoFactoraje(String tipoFactoraje) {
		this.tipoFactoraje = tipoFactoraje;
	}

	public String getBancoFondeo() {
		return bancoFondeo;
	}

	public void setBancoFondeo(String bancoFondeo) {
		this.bancoFondeo = bancoFondeo;
	}

	public String getEmpresarial() {
		return empresarial;
	}

	public void setEmpresarial(String empresarial) {
		this.empresarial = empresarial;
	}




	


}