package com.netro.model.catalogos;

import netropology.utilerias.AppException;


/**
 * Catalogo de Pantalla de bitcoras ()
 *
 * Ejemplo de uso:
 * CatalogoPlazos cat = new CatalogoPlazos();
 * cat.setCampoClave("cc_pantalla_bitacora");
 * cat.setCampoDescripcion("cg_descripcion");
 * cat.setClaveAfiliacion("P");
 * cat.setValoresCondicionIn("A","C","D");
 * cat.setOrden("cc_pantalla_bitacora");
 * List l = cat.getListaElementos();  //Lista con elementos ElementoCatalogo
 *
 * @author Hugo Vargas
 */
public class CatalogoPantallaBitacora extends CatalogoSimple  {
	//Variable para enviar mensajes al log. 
	private String claveAfilia;
	
	public CatalogoPantallaBitacora() {
		super.setTabla("comcat_pantalla_bitacora");
	}
	/**
	 * Establece las condiciones adicionales necesarias para contemplar el
	 * pais y el estado al momento de obtener los municipios.
	 */
	protected void setCondicionesAdicionales() {
		//Se valida que esten los campos requeridos:

		try {
			if (this.claveAfilia == null || this.claveAfilia.equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		} catch (Exception e) {
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		String condicionAdicional = "";
		
		condicionAdicional = " cg_clave_afiliado = ? ";
		
		super.addCondicionAdicional(condicionAdicional);
		//agregar a variables bind estos valores
		super.addVariablesBind(this.claveAfilia);
	}

	public void setClaveAfiliacion(String claveAfilia) {
		this.claveAfilia = claveAfilia;
	}
	
}