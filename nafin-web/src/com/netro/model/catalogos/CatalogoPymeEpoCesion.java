package com.netro.model.catalogos;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.naming.NamingException;
import netropology.utilerias.*;
import org.apache.commons.logging.Log;

/**
 * Obtiene las PYMES afiliadas a una EPO y que son suceptibles a cesi�n de derechos
 */
public class CatalogoPymeEpoCesion extends CatalogoAbstract{

	private static Log log = ServiceLocator.getInstance().getLog(CatalogoPymeEpoCesion.class);
	StringBuffer tablas = new StringBuffer();
	private String icEpo;
	
	public CatalogoPymeEpoCesion(){
		super();
	}

	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colecci�n de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();

		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer hint = new StringBuffer();

		tablas.append(" COMREL_PYME_EPO C, COMCAT_PYME P ");

		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind

		StringBuffer qrySQL = new StringBuffer();

		qrySQL.append(" SELECT  " + hint + " DISTINCT " + super.getCampoClave() + " AS CLAVE, ");
		qrySQL.append(super.getCampoDescripcion() + " AS DESCRIPCION ");
		qrySQL.append(" FROM " + tablas);
		qrySQL.append(" WHERE C.CS_HABILITADO = 'S' ");
		qrySQL.append(" AND C.IC_PYME = P.IC_PYME ");
		qrySQL.append(" AND P.CS_CESION_DERECHOS = 'S' ");
		if(icEpo != null && !icEpo.equals("")){
			qrySQL.append(" AND C.IC_EPO = " + icEpo);
		}
		

		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y adem�s se manda a llamar la implementaci�n especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();

		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}

		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}

		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());

		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}

		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}

		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	public String getIcEpo() {
		return icEpo;
	}

	public void setIcEpo(String icEpo) {
		this.icEpo = icEpo;
	}
}