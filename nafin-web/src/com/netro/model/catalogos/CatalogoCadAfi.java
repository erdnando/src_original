package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Catalogo de IFs de PROVEEDORES Y DISTRIBUIDORES
 *
 * Ejemplo de uso:
 * CatalogoCadAfi cat = new CatalogoCadAfi();
 * cat.setCampoClave("ic_if");
 * cat.setCampoDescripcion("cg_razon_social");
 * cat.setClaveEpo("1"); //opcional
 * * cat.setProducto("1"); //opcional, solo para el caso del cat�logo en perfil nafin
 * cat.setOrden("cg_razon_social");
 *
 * @author Jos� Alvarado
 */
public class CatalogoCadAfi extends CatalogoAbstract {
	
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoIFDistribuidores.class);
	
	StringBuffer tablas = new StringBuffer();
	
	private String cveCad;
	private String tipoAfi;
	public String epos;

	public CatalogoCadAfi() {
		super();
	}
		
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colecci�n de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer hint = new StringBuffer();
	
		StringBuffer condicionJoin = new StringBuffer();
		
		tablas.append("comcat_pyme t1 ,comrel_pyme_epo t2 ,comrel_nafin cn");
				
		condicionJoin.append("t1.IC_PYME = t2.IC_PYME "+
									" and t1.ic_pyme = cn.ic_epo_pyme_if"+
									" and cn.cg_tipo = 'P'");
		

		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
		
		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(
				" SELECT DISTINCT " + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
				);
				
				
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y adem�s se manda a llamar la implementaci�n especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}
	
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}
	
	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		StringBuffer condicionAdicional = new StringBuffer(128);
		
		if(tipoAfi.equals("1") || tipoAfi.equals("2")){
			if(cveCad.equals("T"))
				epos = "";
			else
				epos = "and t2.IC_EPO = " + cveCad;
		}

		condicionAdicional.append(
					" t1.IC_TIPO_CLIENTE = ? "+" "+epos);
		super.addVariablesBind(tipoAfi);
		
					
		super.addCondicionAdicional(condicionAdicional.toString());
	}
	
	/**
	 * Se Establece la clave de la cadena y la del proveedor o distribuidor para obtener los intermediarios con los
	 * que trabaja
	 * 
	 */
	public void setCveCad(String cveCad) {
		this.cveCad = cveCad;
	}

	public void setTipoAfi(String tipoAfi) {
		this.tipoAfi = tipoAfi;
	}
	
}