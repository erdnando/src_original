package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Catalogo de PYMES
 * para descuento electronico
 * Ejemplo de uso:
 * CatalogoPYMEDes cat = new CatalogoPYMEDes();
 * cat.setCampoClave("ic_pyme");
 * cat.setCampoDescripcion("cg_razon_social");
 * cat.setOrden("cg_razon_social");
 *
 * @author Deysi Laura Hern�ndez Contreras
 */
public class CatalogoPYMEDes extends CatalogoAbstract {
	
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoPYMEDes.class);
	
	StringBuffer qrySQL = new StringBuffer();	
	private String estatusDocumentos;
	private String claveIF;
	private String ClaveEPO;
	

	public CatalogoPYMEDes() {     }
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colecci�n de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		
		try{
			if (getCampoClave() == null || getCampoClave().equals("") ||
				getCampoDescripcion() == null || getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
		
		StringBuffer tablas = new StringBuffer();
		StringBuffer hint = new StringBuffer();
		StringBuffer condicionJoin = new StringBuffer();
	
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
	
			hint.append(" /*+ index(d IN_COM_DOCUMENTO_04_NUK) use_nl(d p ds)*/  ");
				
			tablas.append("  comcat_pyme p  "+
							  " , com_documento d"   +							
			      		  " , com_docto_seleccionado ds");	
							  
			condicionJoin.append("     where   d.ic_pyme = p.ic_pyme   "   +
										"     and d.ic_documento=ds.ic_documento   "   +
										"     and d.ic_pyme=p.ic_pyme" );									
				
				if(this.estatusDocumentos != null && !this.estatusDocumentos.equals("")){
					condicionJoin.append(" and d.ic_estatus_docto in( "+this.estatusDocumentos +")" );
				}
				if(this.ClaveEPO != null && !this.ClaveEPO.equals("")){
					condicionJoin.append(" and d.ic_epo = "+this.ClaveEPO  );
					condicionJoin.append(" and  ds.ic_epo = "+this.ClaveEPO  );
				}
				
				if(this.claveIF != null && !this.claveIF.equals("")){
					condicionJoin.append(" and  ds.ic_if = "+this.claveIF  );
				}
									
			qrySQL.append(
				" SELECT " + hint + " DISTINCT " + getCampoClave() + " AS CLAVE, " +
				getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				 condicionJoin
				);
				
				
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y adem�s se manda a llamar la implementaci�n especifica
		//setCondicionesAdicionales de la clase
		setCondicion();
		
		
		if (getCondicion().length()>0) {
			qrySQL.append(" AND " + getCondicion());
		}
	
		if (getOrden() == null || getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + getOrden() );
		}
		  
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+getValoresBindCondicion());
		
		
			registros = con.consultarDB(qrySQL.toString(), getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}
 
	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		StringBuffer condicionAdicional = new StringBuffer(128);
		

	}

	public String getEstatusDocumentos() {
		return estatusDocumentos;
	}

	public void setEstatusDocumentos(String estatusDocumentos) {
		this.estatusDocumentos = estatusDocumentos;
	}

	public String getClaveIF() {
		return claveIF;
	}

	public void setClaveIF(String claveIF) {
		this.claveIF = claveIF; 
	}

	public String getClaveEPO() {
		return ClaveEPO;
	}

	public void setClaveEPO(String ClaveEPO) {
		this.ClaveEPO = ClaveEPO;
	}
  
}