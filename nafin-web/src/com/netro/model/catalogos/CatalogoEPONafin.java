package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


/**
 * Catalogo de Epos del producto de NAfin
 *
 * Ejemplo de uso:
 * CatalogoEPONafin!!!
 * CatalogoEPONafin cat = new CatalogoEPONafin();
 * cat.setCampoClave("ic_epo");
 * cat.setCampoDescripcion("cg_razon_social");
 * cat.setProducto("1");
 * cat.setModalidad("S");
 * cat.setHabilitado("S");
 * cat.setContratoElectronico("S");
 * cat.setOrden("cg_razon_social");
 *
 * @author Lola
 */
public class CatalogoEPONafin extends CatalogoAbstract {
	
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoEPONafin.class);
	
	private String producto		= "";
	private String modalidad	= "";
   private String bancoFondeo	= "";
	private String habilitado	= "S"; 
	private String contratoElectronico	= "";

	StringBuffer tablas = new StringBuffer();
	
	public CatalogoEPONafin() {
		super();
		super.setPrefijoTablaPrincipal("e."); //from comcat_epo e
	}
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colección de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer hint = new StringBuffer();
	
		StringBuffer condicionJoin = new StringBuffer();
		
		tablas.append(" comcat_epo e ");

		if (	(this.producto != null && !this.producto.equals("")) ||	(this.modalidad!=null && !this.modalidad.equals(""))	){
			tablas.append(" ,comrel_producto_epo cpe ");
			condicionJoin.append(" AND cpe.ic_epo = e.ic_epo ");
		}

		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
		
		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(
			" SELECT DISTINCT " + super.getCampoClave() + " AS CLAVE, " +
			super.getCampoDescripcion() + " AS DESCRIPCION " +
			" FROM " + tablas +
			" WHERE 1=1 " + condicionJoin
		);

		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y además se manda a llamar la implementación especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}
	
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		log.debug("qrySQL ::: "+qrySQL.toString());
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {

		StringBuffer condicionAdicional = new StringBuffer(128);	

		if (this.habilitado != null && !this.habilitado.equals(""))	{
		
			condicionAdicional = new StringBuffer(128);
			condicionAdicional.append(" e.cs_habilitado=? ");
			super.addVariablesBind("S");
			super.addCondicionAdicional(condicionAdicional.toString());	
		}

		if (this.contratoElectronico != null && !this.contratoElectronico.equals(""))	{
		
			condicionAdicional = new StringBuffer(128);
			condicionAdicional.append(" EXISTS ( " +
												" 		SELECT ce.ic_epo " +
												" 		FROM com_contrato_epo ce " +
												" 		WHERE ce.ic_epo = e.ic_epo) ");
			super.addCondicionAdicional(condicionAdicional.toString());	
		}

		if (this.producto != null && !this.producto.equals(""))	{

			condicionAdicional = new StringBuffer(128);
			condicionAdicional.append(" cpe.ic_producto_nafin = ? ");
			
			super.addVariablesBind(new Integer(this.producto));			
			super.addCondicionAdicional(condicionAdicional.toString());	
		}

		if(this.modalidad!=null && !this.modalidad.equals(""))	{

			condicionAdicional = new StringBuffer(128);
			condicionAdicional.append(" (cpe.ic_modalidad IS NULL OR cpe.ic_modalidad = ? ) ");
			
			super.addVariablesBind(new Integer(this.modalidad));			
			super.addCondicionAdicional(condicionAdicional.toString());	
		}

		if(this.bancoFondeo!=null && !this.bancoFondeo.equals(""))	{

			condicionAdicional = new StringBuffer(128);
			condicionAdicional.append(" e.ic_banco_fondeo = ? ");
			
			super.addVariablesBind(new Integer(this.bancoFondeo));			
			super.addCondicionAdicional(condicionAdicional.toString());	
		}
	}

	public void setProducto(String prod){
		this.producto = prod;
	}
	
	public void setModalidad(String modalidad){
		this.modalidad = modalidad;
	}
	
	public void setBancoFondeo(String banco){
		this.bancoFondeo = banco;
	}

	public void setHabilitado(String habilitado){
		this.habilitado = habilitado;
	}

	public void setContratoElectronico(String contrato){
		this.contratoElectronico = contrato;
	}

}