package com.netro.model.catalogos;

public class CatalogoPymeExtincion  extends CatalogoSimple{

	private String claveIf;

	public CatalogoPymeExtincion(){
		super.setTabla(" CDER_SOLICITUD CDS, COMCAT_PYME PYME ");
	}

	public void setClave(String clave){
		super.setCampoClave(clave);
	}

	public void setDescripcion(String descripcion){
		super.setCampoDescripcion(descripcion);
	}

	public void setClaveIf(String claveIf){
		this.claveIf = claveIf;
	}

	protected void setCondicionesAdicionales() {

		String condicionAdicional = "";
		condicionAdicional = " CDS.IC_PYME = PYME.IC_PYME "
		+ " AND (CDS.IC_IF = ? OR CDS.IC_IF = (SELECT IC_IF "
		+ " FROM COMCAT_CESIONARIO WHERE IC_IF = ?)) ";
		super.addCondicionAdicional(condicionAdicional);
		//agregar a variables bind estos valores
		super.addVariablesBind(new Integer(Integer.parseInt(claveIf)));
		super.addVariablesBind(new Integer(Integer.parseInt(claveIf)));
	}

}