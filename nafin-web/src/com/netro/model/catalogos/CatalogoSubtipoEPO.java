package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class CatalogoSubtipoEPO extends CatalogoAbstract {   
	
	private final static Log log = ServiceLocator.getInstance().getLog(CatalogoSubtipoEPO.class);

	private String ic_tipo_epo;

	public CatalogoSubtipoEPO() {     }
	
  
	public List getListaElementos(){
		StringBuffer tablas = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		Registros regs	= null;
		List coleccionElementos = new ArrayList();
		
  
		AccesoDB con = new AccesoDB();
		try {
    
			con.conexionDB();
	 
			tablas.append("comcat_tipo_epo T, comcat_subtipo_epo S ");
		
			condicion.append(" WHERE s.ic_tipo_epo = t.ic_tipo_epo ");
		
			if(!ic_tipo_epo.equals("")){
				condicion.append(" and s.ic_tipo_epo = "+ic_tipo_epo);
			}else{
				//condicion.append(" and s.ic_tipo_epo = null ");   
			}
      
			String qrySentencia = "select  "+super.getCampoClave()+" AS CLAVE ,"+super.getCampoDescripcion()+ " AS DESCRIPCION " +
			" from "+tablas+	condicion +" ORDER BY " + super.getOrden();
			
					
			System.out.println(qrySentencia);
			regs = con.consultarDB(qrySentencia,super.getValoresBindCondicion());
	

			while(regs.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(regs.getString("CLAVE"));
				elementoCatalogo.setDescripcion(regs.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
			}
			

		} catch (Exception e) {
			System.err.println("Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		return coleccionElementos;
	}

	public String getIc_tipo_epo() {
		return ic_tipo_epo;
	}

	public void setIc_tipo_epo(String ic_tipo_epo) {
		this.ic_tipo_epo = ic_tipo_epo;
	}   	   
	
}