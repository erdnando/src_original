package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Clase que genera la informacion para los combos de a�o y mes de calendarios de supervision
 */
public class CatalogoAnioMesSupCalendarioIf extends CatalogoAbstract  {
	private static final Log LOG = ServiceLocator.getInstance().getLog(CatalogoAnioMesSupCalendarioIf.class);
	private Integer calAnio;
	private Integer icCveSiag;
	private String usuario;
	private String calTipo;
	
	StringBuilder tablas = new StringBuilder();

	/**
	 * Constructor por default
	 */
	public CatalogoAnioMesSupCalendarioIf() {
	}

	@Override
	public List getListaElementos() throws AppException {
		LOG.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		
	    try{
	        if (super.getCampoClave() == null || "".equals(super.getCampoClave()) ||
	            super.getCampoDescripcion() == null || "".equals(super.getCampoDescripcion()) ) {
	            throw new AppException("Los parametros requeridos no has sido establecidos");
	        }
	    }catch (Exception e){
	        throw new AppException("Error en los parametros establecidos. " + e.getMessage(), e);
	    }
		
	    tablas.append(" SUP_CALENDARIO_IF A ");
	    tablas.append(" , SUP_DATOS_IF B ");
		
	    StringBuilder condicionJoin = new StringBuilder();
		condicionJoin.append(" a.IC_CVE_SIAG = b.IC_CVE_SIAG ");
		
		StringBuilder qrySQL = new StringBuilder();
		
	    qrySQL.append(
	            " SELECT DISTINCT "  + super.getCampoClave() + " AS CLAVE, " +
	            super.getCampoDescripcion() + " AS DESCRIPCION " +
	            " FROM " + tablas +
	            " WHERE " + condicionJoin
	            );
		
	    super.setCondicion();
		
	    if (super.getCondicion().length()>0) {
	        qrySQL.append(" AND " + super.getCondicion());
	    }
	    
	    if (super.getOrden() == null || "".equals(super.getOrden())) {
	        qrySQL.append(
	                " ORDER BY " + super.getCampoDescripcion() );
	    } else {
	        qrySQL.append(
	                " ORDER BY " + super.getOrden() );
	    }
		
		
		LOG.debug("qrySQL ::: "+qrySQL);
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		while(registros.next()) {
			ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
			elementoCatalogo.setClave(registros.getString("CLAVE"));
			elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
			coleccionElementos.add(elementoCatalogo);
		}
		LOG.debug("getListaElementos (S)");
		return coleccionElementos;
	}
	
	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		StringBuilder condicionAdicional = new StringBuilder("");
	    boolean and = false;
		
	    if(null != calAnio && calAnio > 0) {
	        condicionAdicional.append((and?" AND ":"")+" A.CAL_ANIO = ? " );
	        and = true;
	    }
	    if(null != icCveSiag && icCveSiag > 0) {
	        condicionAdicional.append((and?" AND ":"")+" B.IC_CVE_SIAG = ? ");
	        and = true;
	    }
	    if(null != usuario && !"".equals(usuario)) {
	        condicionAdicional.append((and?" AND ":"")+" B.IC_CLIENTE = ?");
	        and = true;
	    }
	    if(null != calTipo && !"".equals(calTipo)) {
	        condicionAdicional.append((and?" AND ":"")+" A.CAL_TIPO = ? ");
	        and = true;
	    }
		
		if(condicionAdicional.length()>0){
	        super.addCondicionAdicional(condicionAdicional.toString());
		}
		
		
	    if(null != calAnio && calAnio > 0) {
	        super.addVariablesBind(new Integer(this.calAnio));
	    }
	    if(null != icCveSiag && icCveSiag > 0) {
	        super.addVariablesBind(new Integer(this.icCveSiag));
	    }
	    if(null != usuario && !"".equals(usuario)) {
	        super.addVariablesBind(usuario);
	    }
	    if(null != calTipo && !"".equals(calTipo)) {
	        super.addVariablesBind(calTipo);
	    }
		
			
	}

	public Integer getCalAnio() {
		return calAnio;
	}

	public void setCalAnio(Integer calAnio) {
		this.calAnio = calAnio;
	}

	public Integer getIcCveSiag() {
		return icCveSiag;
	}

	public void setIcCveSiag(Integer icCveSiag) {
		this.icCveSiag = icCveSiag;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public void setCalTipo(String calTipo) {
		this.calTipo = calTipo;
	}

	public String getCalTipo() {
		return calTipo;
	}

}
