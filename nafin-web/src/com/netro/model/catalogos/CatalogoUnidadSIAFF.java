package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Esta clase genera u obtiene los valores del catalogo
 * COMCAT_SIAFF_UNIDADES, en base a ciertas condiciones establecidas
 * @author
 */
public class CatalogoUnidadSIAFF extends CatalogoSimple
{
	private String tabla;
	private String clave;
	private String descripcion;
	private String seleccion;
	private String order;
	
	private static final Log log = ServiceLocator.getInstance().getLog(CatalogoSimple.class);


/**
 * Obtiene una lista de elementos de tipo ElementoCatalogo
 * @return regresa una colecci�n de elementos obtenidos
 */ 
	public List getListaElementos()
	{
		log.info("getListaElementos(E)");
		List coleccionElementos = new ArrayList();
		
		AccesoDB con = new AccesoDB();		      
		String tables, condiciones, orden;
		String qryCatUnidadSIAFF;
		try
		{
			con.conexionDB();
			Registros regCatUnidadSIAFF	=	null;
			String tmpDesc						= "";
			String tmpClave					= "";


			tables			= " COMCAT_SIAFF_UNIDADES ";
			
         if(seleccion != null && !seleccion.equals("")){
				condiciones	= " WHERE CC_RAMO_SIAFF =  '" + this.seleccion + "'";
			}
			else{
				condiciones = "";
			}
			orden				= " ORDER BY CG_DESCRIPCION ";   

			qryCatUnidadSIAFF = "select "+this.clave+" AS CLAVE, " +
											this.descripcion +" AS DESCRIPCION "+
											" FROM " + tables + condiciones;
			if(this.order!=null && !this.order.equals(""))
				qryCatUnidadSIAFF += "ORDER BY "+this.order;
			else
				qryCatUnidadSIAFF += orden;
				
			System.out.println("qryCatUnidadSIAFF:::....." +qryCatUnidadSIAFF );

			regCatUnidadSIAFF = con.consultarDB(qryCatUnidadSIAFF);
			/*
			while(regCatUnidadSIAFF.next()) {
					if((regCatUnidadSIAFF.getString("CLAVE")).equals("0")){
						tmpDesc		= regCatUnidadSIAFF.getString("DESCRIPCION");
						tmpClave	= regCatUnidadSIAFF.getString("Clave");
					}else{
					ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					elementoCatalogo.setClave(regCatUnidadSIAFF.getString("CLAVE"));
					elementoCatalogo.setDescripcion(regCatUnidadSIAFF.getString("DESCRIPCION"));
					coleccionElementos.add(elementoCatalogo);
					}

			}
			if(!tmpDesc.equals("") && !tmpClave.equals(""))
					{
						ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
						elementoCatalogo.setClave(tmpClave);
						elementoCatalogo.setDescripcion(tmpDesc);
						coleccionElementos.add(elementoCatalogo);
					}
					*/
			
			while(regCatUnidadSIAFF.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(regCatUnidadSIAFF.getString("CLAVE"));
				elementoCatalogo.setDescripcion(regCatUnidadSIAFF.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}


		} catch(Exception e) {
				e.printStackTrace();
				throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
		}
		return coleccionElementos;

	}


	


/**
* Establece la clave(id) que se va obtener en la consulta
*/
	public void setClave(String clave)
	{
		this.clave = clave;
	}
/**
* Establece la descripcion  que se va obtener en la consulta
*/
	public void setDescripcion(String descripcion)
	{
		this.descripcion = descripcion;
	}
/**
* Establece valor para indicar algun valor en especifico a consultar
*/
	public void setSeleccion(String seleccion)
	{
		this.seleccion = seleccion;
	}

/**
* Establece el campo por el cual se va a ordenar
*/
	public void setOrden(String order)
	{
		this.order = order;
	}

}