package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Obtiene la lista de IF con dispersion habilitada para el
 * producto de descuento electronico.
 *
 * PANTALLA: ADMIN NAFIN - ADMINISTRACION - DISPERSION - MONITOR (Tipo de Monitoreo: IF's)
 *
 * @author Salim Hernandez
 * @since F### - 2013 - ADMIN NAFIN - Migracion Monitor; 28/06/2013 06:25:48 p.m.
 *
 */
 
public class CatalogoIFMonitorDispersion extends CatalogoAbstract  {
	
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoIFMonitorDispersion.class);
	
	private String producto;
	private String dispersion;
	private String operaFideicomiso; 
	
	private boolean showCampoOperaFideicomiso;
	
	public CatalogoIFMonitorDispersion(){
		super.setPrefijoTablaPrincipal("I."); // from COMCAT_IF I
		this.showCampoOperaFideicomiso = false;
	}
	
	/**
	 * Obtiene una lista de elementos de tipo ElementoCatalogo
	 * @return regresa una colecci�n de elementos obtenidos
	 */	
	public List getListaElementos(){
		
		log.info("getListaElementos(E)");
		
		AccesoDB 		con 						= new AccesoDB();
		List 				coleccionElementos 	= new ArrayList();
		StringBuffer	query						= new StringBuffer();
		
		try {
			
			con.conexionDB();
			
			Registros 	registros		= null;			
			boolean  	hayProducto		= this.producto   != null && !"".equals(this.producto)  ?true:false;
			boolean  	hayDispersion	= this.dispersion != null && !"".equals(this.dispersion)?true:false;
			
			// DEFINIR CAMPOS DE CONSULTA
			query.append( 
				"SELECT     "  +
					this.getCampoClave()       + " AS CLAVE,      "  +
					this.getCampoDescripcion() + " AS DESCRIPCION "
			);
			if( this.getShowCampoOperaFideicomiso() ){
				query.append(
					", NVL(I.CS_OPERA_FIDEICOMISO,'N') AS OPERA_FIDEICOMISO "
				);
			};
			
			// DEFINIR LAS TABLAS SOBRE LAS QUE SE REALIZAR� LA CONSULTA
			query.append(" FROM COMCAT_IF I ");
 
			if( hayProducto || hayDispersion ){
				query.append(", COMREL_PRODUCTO_IF CPI ");
			}
 
			// DEFINIR LA CONDICIONES DE LA CONSULTA
			query.append(	
				"WHERE                                       "  +
				"   I.cs_habilitado         = 'S'            "
			);
			
			// AGREGAR LAS CONDICIONES ADICIONALES: Dispersion, Producto.
			super.setCondicion();
			if (super.getCondicion().length()>0) {
				query.append(" AND " + getCondicion());
			}
		
			// DEFINIR ORDENAMIENTO DE LA CONSULTA
			if( this.getOrden() != null ){
				query.append(
					" ORDER BY "+this.getOrden()
				);
			} else {
				query.append(
					" ORDER BY "+this.getCampoDescripcion()
				);
			}
			
			// Realizar consulta
			log.debug("getListaElementos.query       = <" + query.toString()          + ">");
			log.debug("getListaElementos.condiciones = <" + getValoresBindCondicion() + ">");
			registros = con.consultarDB(query.toString(),getValoresBindCondicion());
			
			if( this.getShowCampoOperaFideicomiso() ){
				
				while(registros.next()) {
					
					HashMap elementoCatalogo = new HashMap();
					elementoCatalogo.put( "clave",       			registros.getString("CLAVE")             );
					elementoCatalogo.put( "descripcion", 			registros.getString("DESCRIPCION") 		  );
					elementoCatalogo.put( "opera_fideicomiso", 	registros.getString("OPERA_FIDEICOMISO") );
					
					coleccionElementos.add(elementoCatalogo);
					
				}
				
			} else {
				
				while(registros.next()) {
					
					ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
					elementoCatalogo.setClave(registros.getString("CLAVE"));
					elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
					
					coleccionElementos.add(elementoCatalogo);
					
				}
				
			}
			
		} catch(Exception e) {
 
			log.error("getListaElementos(Exception)");
			log.error("getListaElementos.producto    = <" + producto    		        + ">");
			log.error("getListaElementos.dispersion  = <" + dispersion  		        + ">");
			log.debug("getListaElementos.query       = <" + query.toString()	        + ">");
			log.debug("getListaElementos.condiciones = <" + getValoresBindCondicion() + ">");
			e.printStackTrace();
			
			throw new RuntimeException("El cat�logo no pudo ser generado");
			
		} finally {
			
			if (con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			
			log.info("getListaElementos(S)");
			
		}
		
		return coleccionElementos;
		
	}

	public void setCondicionesAdicionales(){

		boolean  		hayProducto							= getProducto()   		!= null && !"".equals(getProducto())  			?true:false;
		boolean  		hayDispersion						= getDispersion() 		!= null && !"".equals(getDispersion())			?true:false;			
		boolean        hayCondicionOperaFideicomiso	= getOperaFideicomiso() != null && !"".equals(getOperaFideicomiso())	?true:false;
		
		if( hayProducto || hayDispersion ){
			addCondicionAdicional( 
				"   cpi.ic_if             = I.ic_if  " 
			); 
		}
		
		if( hayProducto ){
			addCondicionAdicional( 
				"   cpi.ic_producto_nafin = ? "  
			); 
			addVariablesBind(getProducto());
		}
		
		if( hayDispersion && hayCondicionOperaFideicomiso ){
			
			addCondicionAdicional(  
				"  (  cpi.cg_dispersion     =  ? OR I.cs_opera_fideicomiso    =  ? ) "
			);
			addVariablesBind(getDispersion());
			addVariablesBind(getOperaFideicomiso());
			
		} else if( hayDispersion ){
			
			addCondicionAdicional(  
				"   cpi.cg_dispersion     =  ? "
			);
			addVariablesBind(getDispersion());
			
		} else if( hayCondicionOperaFideicomiso ){ // F017 - 2013
			
			addCondicionAdicional(  
				"   I.cs_opera_fideicomiso    =  ? "
			);
			addVariablesBind(getOperaFideicomiso());
			
		}
		
	}
	
	public String getProducto() {		
		return producto;	
	}

	public String getDispersion() {		
		return dispersion;	
	}
 
	/**
	 * Se utiliza para indicar el producto, generalmente se consulta el 
	 * producto 1: Descuento Electronico
	 */
	public void setProducto(String producto) {		
		this.producto = producto;	
	}
 
	/**
	 * Se utiliza para restringir la busqueda a aquellos IFs que tiene la dispersion 
	 * habilitada (S) o deshabilita (N)
	 */
	public void setDispersion(String dispersion) {		
		this.dispersion = dispersion;	
	}

	/**
	 * Obtiene el valor de la variable: operaFideicomiso, con "S" para S� o "N" para No.
	 */
	public String getOperaFideicomiso() {

		return operaFideicomiso;

	}



	/**
	 * Se utiliza para restringir la b�squeda a aquellos IFs que operan Fideicomiso para 
	 * Desarrollo de Proveedores: "S" (S�) o "N" (No).
	 */
	public void setOperaFideicomiso(String operaFideicomiso) {

		this.operaFideicomiso = operaFideicomiso;

	}

	/**
	 * Indica si en la consulta, tambien se enviar� el valor del campo: <tt>COMCAT_IF.CS_OPERA_FIDEICOMISO</tt>, con <tt>true</tt> para 
	 * enviarlo y <tt>false</tt> para no enviarlo.
	 */
	public boolean getShowCampoOperaFideicomiso() {

		
		return this.showCampoOperaFideicomiso;

		
	}



	/**
	 * Se utiliza si se enviar� el contenido del campo: <tt>COMCAT_IF.CS_OPERA_FIDEICOMISO</tt> en la consulta de los
	 * componentes del cat�logo.
	 */
	public void setShowCampoOperaFideicomiso(boolean showCampoOperaFideicomiso) {

		
		this.showCampoOperaFideicomiso = showCampoOperaFideicomiso;

		
	}



	/**
	 * Establece el nombre del campo que representar� la descripci�n
	 * a obtener en la consulta
	 * @param campoDescripcion Campo de la descripcion
	 */
	public void setOrden(String orden) {
		
		if ( 
			this.getPrefijoTablaPrincipal() != null 
				&& 
			!this.getPrefijoTablaPrincipal().equals("") 
				&&
			orden.trim().indexOf(this.getPrefijoTablaPrincipal()) != 0 
		) {
			super.setOrden( this.getPrefijoTablaPrincipal() + orden );
		} else {
			super.setOrden( orden );
		}
				
	}
	
	/**
	 * Obtiene la lista llamando getListaElementos y genera un JSON de los
	 * elementos.
	 * (Para usar este m�todo la lista debe tener elementos
	 * netropology.utilerias.ElementoCatalogo)
	 * @throws AppException
	 *
	 * @return Cadena con el JSON de los elementos
	 *
	 * @since  F017 - 2013 -- DSCTO ELECT - Fideicomiso para el Desarrollo de Proveedores; 24/09/2013 01:23:39 p.m.
	 * @author jshernandez.
	 *
	 */
	public String getJSONElementos() throws AppException {
		List elementos = this.getListaElementos();
		JSONArray jsonArr = new JSONArray();
		Iterator it = elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			} else if (obj instanceof java.util.HashMap ) {
				HashMap ec = (HashMap)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		return "{\"success\": true, \"total\": \"" + 
				jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";
	}
	
	/** 
	 *
	 * De la lista elementos proporcionada como parametro, genera una cadena con formato
	 * JSON y con propiedades adicionales, para que puedan ser leidas por un JSON Store.
	 * (Para usar este m�todo la lista debe tener elementos
	 * netropology.utilerias.ElementoCatalogo)
	 * @throws AppException
	 *
	 * @param elementos <tt>List</tt> con los elementos a convertir.
	 *
	 * @return Cadena con el JSON de los elementos
	 *
	 * @since  F017 - 2013 -- DSCTO ELECT - Fideicomiso para el Desarrollo de Proveedores; 24/09/2013 01:28:54 p.m.
	 * @author jshernandez.
	 *
	 */
	public String getJSONElementos(List elementos) 
		throws AppException {
		JSONArray 	jsonArr	= new JSONArray();
		Iterator 	it			= elementos.iterator();
		while(it.hasNext()) {
			Object obj = it.next();
			if (obj instanceof netropology.utilerias.ElementoCatalogo) {
				ElementoCatalogo ec = (ElementoCatalogo)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			} else if (obj instanceof java.util.HashMap ) {
				HashMap ec = (HashMap)obj;
				jsonArr.add(JSONObject.fromObject(ec));
			}
		}
		return "{\"success\": true, \"total\": \"" + 
				jsonArr.size() + "\", \"registros\": " + jsonArr.toString()+"}";
	}
 
}