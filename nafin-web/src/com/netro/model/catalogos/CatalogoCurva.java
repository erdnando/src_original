package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class CatalogoCurva extends CatalogoAbstract  {
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoMoneda.class);

	private String clave;
	private String descripcion;
	private String moneda;
	
	public CatalogoCurva() {}
	public List getListaElementos() {

		log.info("CatalogoCurvas (E)");
		List coleccionElementos = new ArrayList();

		AccesoDB con = new AccesoDB();
		String tables, condiciones, orden;
		StringBuffer qrySentencia = new StringBuffer();
		try
		{
			con.conexionDB();
			Registros reg = null;

			qrySentencia.append( " SELECT " + this.clave + " AS CLAVE, " + 
										  this.descripcion +" AS DESCRIPCION " +
										" FROM curva " + 
										" WHERE ic_moneda = " + this.moneda  );

			log.info("Sentencia: " + qrySentencia.toString() );

			reg = con.consultarDB(qrySentencia.toString());

			while(reg.next()){
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(reg.getString("CLAVE"));
				elementoCatalogo.setDescripcion(reg.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
			}

		} catch(Exception e){
				e.printStackTrace();
				throw new RuntimeException("El catalogo no pudo ser generado");
		} finally{
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
		}
		log.info("CatalogoCurvas (S)");
		return coleccionElementos;

	}


	public void setClave(String clave) {
		this.clave = clave;
	}


	public String getClave() {
		return clave;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}


	public String getMoneda() {
		return moneda;
	}
	

}