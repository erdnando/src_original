package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Esta clase genera u obtiene los valores del catalogo
 * COMCAT_TASA y COMREL_TASA_PRODUCTO, en base a ciertas condiciones establecidas
 * ejemplom de uso:
 * CatalogoCargaTasas cat= new CatalogoCargaTasas();
 * cat.setClave("ic_tasa");
 * cat.setDescripcion("cd_nombre");
 * cat.setCveProd(ic_prod);
 * cat.setCveMoneda(ic_moneda);
 * @author Jos� Alvarado
 */

public class CatalogoCargaTasas extends CatalogoSimple {
	private String clave;
	private String descripcion;
	private String cveProducto;
	private String cveMoneda;
	private static final Log log = ServiceLocator.getInstance().getLog(CatalogoCargaTasas.class);
	
	public CatalogoCargaTasas() {
	}
	
	/**
	* Obtiene una lista de elementos de tipo ElementoCatalogo
	* @return regresa una colecci�n de elementos obtenidos
	*/
	public List getListaElementos()
	{
		log.info("getListaElementos(E)");
		List coleccionElementos = new ArrayList();

		AccesoDB con = new AccesoDB();
		String tables, condiciones;
		String qryCatPantallas;
		try
		{
			con.conexionDB();
			Registros regCatPantallas	=	null;

			tables			= " comcat_tasa t, comrel_tasa_producto tp ";

			condiciones	= " WHERE t.ic_tasa = tp.ic_tasa "+"AND tp.ic_producto_nafin = "+cveProducto+" AND t.ic_moneda = "+cveMoneda;

			qryCatPantallas = "SELECT t."+this.clave+" AS CLAVE, t." +this.descripcion +" AS DESCRIPCION "+
											" FROM " + tables + condiciones+" ORDER BY t.cd_nombre";
			

			regCatPantallas = con.consultarDB(qryCatPantallas);

			while(regCatPantallas.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(regCatPantallas.getString("CLAVE"));
				elementoCatalogo.setDescripcion(regCatPantallas.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}


		} catch(Exception e) {
				e.printStackTrace();
				throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
		}
		return coleccionElementos;

	}
	
	
	/**
	* Establece la clave(id) que se va obtener en la consulta
	*/
	public void setClave(String clave)
	{
		this.clave = clave;
	}
	/**
	* Establece la descripcion  que se va obtener en la consulta
	*/
	public void setDescripcion(String descripcion)
	{
		this.descripcion = descripcion;
	}
	
	public void setCveProd(String cveProducto){
		this.cveProducto = cveProducto;
	}
	
	public void setCveMoneda(String cveMoneda){
		this.cveMoneda = cveMoneda;
	}
	
}