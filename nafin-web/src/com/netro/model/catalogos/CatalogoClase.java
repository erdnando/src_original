package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class CatalogoClase extends CatalogoAbstract  {

private static Log log = ServiceLocator.getInstance().getLog(CatalogoClase.class);
private String tabla;
private String Sector;
private String SubSector;
private String Rama;
		
	public CatalogoClase() {
		super();		
	}
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colecci�n de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		///////////////////////////////////////
				/*query = "select IC_CLASE, CD_NOMBRE from comcat_clase "+
"where ic_sector_econ =" + Sector_economico + " and ic_subsector =" + Subsector + 
" and ic_rama = "+ Rama + " order by cd_nombre";
	*/
		///////////////////////////////////////
			
		StringBuffer qrySQL = new StringBuffer();
		qrySQL.append("SELECT " + super.getCampoClave() + " AS CLAVE, ");
		qrySQL.append(super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + this.tabla+
				"	WHERE  ic_sector_econ = " + Sector +"	 AND ic_subsector="+SubSector+
				"	AND ic_rama="+Rama);		
	
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {}


	public void setTabla(String tabla) {
		this.tabla = tabla;
	}


	public String getTabla() {
		return tabla;
	}


	public void setSector(String Sector) {
		this.Sector = Sector;
	}


	public String getSector() {
		return Sector;
	}


	public void setSubSector(String SubSector) {
		this.SubSector = SubSector;
	}


	public String getSubSector() {
		return SubSector;
	}


	public void setRama(String Rama) {
		this.Rama = Rama;
	}


	public String getRama() {
		return Rama;
	}


	
}