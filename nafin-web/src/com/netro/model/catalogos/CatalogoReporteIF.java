package com.netro.model.catalogos;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Esta clase obtiene los valores del catalogo
 * de IF,s para la pantalla de reporte IF
 * Ejemplo de uso:
 * CatalogoReporteIF cat = new CatalogoReporteIF();
 * String infoRegresar = cat.getJSONElementos();	
 */
public class CatalogoReporteIF extends CatalogoSimple  {

	public CatalogoReporteIF() {}
	
	private static final Log log = ServiceLocator.getInstance().getLog(CatalogoReporteIF.class);

/**
 * Obtiene una lista de elementos de tipo ElementoCatalogo
 * @return regresa una colecci�n de elementos obtenidos
 */
	public List getListaElementos()
	{
		log.info("getListaElementos(E)");
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		PreparedStatement ps = null;
		ArrayList datos = new ArrayList();
		ArrayList coleccionElementos = new ArrayList();		
		StringBuffer query = new StringBuffer();
		try
		{
			con.conexionDB();

			query.append(" SELECT  distinct i.ic_if as CLAVE , i.CG_RAZON_SOCIAL as  DESCRIPCION " );
			query.append(" FROM comcat_if i, comrel_if_epo a ");
			query.append(" WHERE i.cs_habilitado = ?");
			query.append(" AND a.cs_vobo_nafin = ? ");
			query.append(" and i.ic_if =  a.ic_if ");
			query.append(" ORDER BY cg_razon_social ");
			
			ps = con.queryPrecompilado(query.toString());
			ps.setString(1, "S");
			ps.setString(2, "S");
			log.debug(query.toString());
			rs = ps.executeQuery();
			
			while(rs.next()){				
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(rs.getString("CLAVE"));
				elementoCatalogo.setDescripcion(rs.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
			}

			rs.close();
			ps.close();
		
			return coleccionElementos;
		
		} catch(Exception e) {
				e.printStackTrace();
				throw new RuntimeException("El catalogo ReporteIF no pudo ser generado");
		} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
				log.info("getListaElementos(S)");
		}
	}	
}//fin clase