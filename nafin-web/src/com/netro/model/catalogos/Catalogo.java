package com.netro.model.catalogos;

import java.util.Collection;
import java.util.List;

import netropology.utilerias.AppException;

/**
 * Clase para realizar consultas simples a los catalogos, con la finalidad
 * de obtener un listado de Clave - Descripcion para su
 * presentaci�n en combos, principalmente
 * @author Gilberto Aparicio
 */
public interface Catalogo  {

	/**
	 * Establece el nombre del campo que representar� la clave
	 * a obtener en la consulta.
	 * @param campoClave Campo de la clave
	 */	
	public void setCampoClave(String campoClave);

	/**
	 * Obtiene el nombre del campo que representar� la clave
	 * a obtener en la consulta.
	 * @return Cadena con el nombre del campo de la clave
	 */	
	public String getCampoClave();

	
	/**
	 * Establece el nombre del campo que representar� la descripci�n
	 * a obtener en la consulta
	 * @param campoDescripcion Campo de la descripcion
	 */		
	public void setCampoDescripcion(String campoDescripcion);


	/**
	 * Obtiene el nombre del campo que representar� la descripci�n
	 * a obtener en la consulta
	 * @return Cadena con el nombre del campo de la descripcion
	 */		
	public String getCampoDescripcion();
	
	
	/**
	 * Establece el campo o campos (separados por comas) del orden
	 * en el que deben ser obtenidos los registros
	 * @param orden Cadena con los criterios de ordenacion
	 */
	public void setOrden(String orden);

	/**
	 * Obtiene el campo o campos (separados por comas) del orden
	 * en el que deben ser obtenidos los registros
	 * @return Cadena con los criterios de ordenacion
	 */
	public String getOrden();

	
	/**
	 * Establece los valores de las claves que se incluyen a trav�s de una 
	 * colecci�n de objetos.
	 * @param valoresCondicionIn Colecci�n de valores. Si la llave es compuesta 
	 * 		usar una coleccion de objetos: java.util.List o netropology.utilerias.Registros
	 */
	public void setValoresCondicionIn(Collection valoresCondicionIn);

	/**
	 * Obtiene los valores de las claves que se incluyen a trav�s de una 
	 * colecci�n de objetos.
	 * @return Colecci�n de valores. Si la llave es compuesta se espera
	 * 		una coleccion de objetos: java.util.List o netropology.utilerias.Registros
	 */
	public Collection getValoresCondicionIn();


	/**
	 * Establece los valores de las claves NO se incluyen, a trav�s de una 
	 * colecci�n de objetos.
	 * @param valoresCondicionIn Colecci�n de valores. Si la llave es compuesta 
	 * 		usar una coleccion de objetos: java.util.List o netropology.utilerias.Registros
	 */
	public void setValoresCondicionNotIn(Collection valoresCondicionNotIn);


	/**
	 * Obtiene los valores de las claves que NO se incluyen, a trav�s de una 
	 * colecci�n de objetos.
	 * @return Colecci�n de valores. Si la llave es compuesta se espera
	 * 		una coleccion de objetos: java.util.List o netropology.utilerias.Registros
	 */
	public Collection getValoresCondicionNotIn();

	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos (Se recomienda usar netropology.utilerias.ElementoCatalogo)
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colecci�n de elementos obtenidos
	 */
	public List getListaElementos() throws AppException;

	/**
	 * Obtiene la lista de elementos llamando getListaElementos 
	 * y genera un XML. 
	 * (Para usar este m�todo la lista debe tener elementos 
	 * netropology.utilerias.ElementoCatalogo)
	 * @return Cadena con el XML de los elementos
	 */
	public String getXMLElementos() throws AppException;

	/**
	 * Obtiene la lista llamando getListaElementos y genera un XML de los 
	 * elementos, y coloca un atributo para marcar como seleccionado 
	 * predeterminado a la clave especificada.
	 * (Para usar este m�todo la lista debe tener elementos 
	 * netropology.utilerias.ElementoCatalogo)
	 * @return Cadena con el XML de los elementos
	 */
	public String getXMLElementos(String claveSeleccionada) throws AppException;

}