package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class CatalogoMenuSeccion extends CatalogoAbstract {
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoMenuSeccion.class);
	
	private String claveSeccion = "";
	private String claveMenu = "";
	private String nombreMenu = "";
	
	StringBuffer tablas = new StringBuffer();
	
	public CatalogoMenuSeccion() {
		super();
		super.setPrefijoTablaPrincipal("b."); //from admcon_seccion_cont
	}
	
	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de 
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colección de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer hint = new StringBuffer();
	
		tablas.append(" admcon_seccion a  ");
		tablas.append(" ,admcon_seccion_cont b ");
				
		hint.append(" /*+ USE_NL(as asc) */ " );

		StringBuffer condicionJoin = new StringBuffer();
		
		condicionJoin.append(" a.ic_seccion = b.ic_seccion ");

		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
		
		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(
				" SELECT " + hint + " DISTINCT "  + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin
				);
				
				
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y además se manda a llamar la implementación especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}
	
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());
		
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}
		
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		String condicionAdicional = "";
		boolean and = false;
		
		try{
			if (this.claveSeccion != null && !this.claveSeccion.equals("") ){
				Integer.parseInt(this.claveSeccion);
				//throw new Exception("Los parametros requeridos no has sido establecidos");
			}

			
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}
		
			if(claveSeccion!=null && !claveSeccion.equals("")){
				condicionAdicional +=(and?" AND ":"")+" a.ic_seccion = ?  ";
				and = true;
			}
			if(claveMenu!=null && !claveMenu.equals("")){
				condicionAdicional +=(and?" AND ":"")+" b.ic_opcion = ? ";
				and = true;
			}
					
			if(condicionAdicional.length()>0)
				super.addCondicionAdicional(condicionAdicional);
			
			//agregar a variables bind estos valores
			if(claveSeccion!=null && !claveSeccion.equals(""))
				super.addVariablesBind(new Integer(this.claveSeccion));
			if(claveMenu!=null && !claveMenu.equals(""))
				super.addVariablesBind(new Integer(this.claveMenu));
			
			
	}


	public void setClaveSeccion(String claveSeccion) {
		this.claveSeccion = claveSeccion;
	}


	public String getClaveSeccion() {
		return claveSeccion;
	}


	public void setClaveMenu(String claveMenu) {
		this.claveMenu = claveMenu;
	}


	public String getClaveMenu() {
		return claveMenu;
	}


	public void setNombreMenu(String nombreMenu) {
		this.nombreMenu = nombreMenu;
	}


	public String getNombreMenu() {
		return nombreMenu;
	}


	



}