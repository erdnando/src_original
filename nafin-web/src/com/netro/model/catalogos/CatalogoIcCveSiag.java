package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;
import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Catálogo para la pantalla Proceso de Supervisión
 */
public class CatalogoIcCveSiag extends CatalogoAbstract{

	private static final Log LOGGER = ServiceLocator.getInstance().getLog(CatalogoIcCveSiag.class);
	StringBuilder tablas = new StringBuilder();
	private String icCliente;

	/**
	 * Constructor por default
	 */
	public CatalogoIcCveSiag() {
	}

	@Override
	public List getListaElementos() throws AppException {
		LOGGER.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();
		tablas.append(" SUP_DATOS_IF A, COMCAT_IF B ");
		StringBuilder qrySQL = new StringBuilder();
		qrySQL.append(" SELECT DISTINCT " + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE A.IC_IF = B.IC_IF AND A.IC_CVE_SIAG IS NOT NULL"
		);
		if(null != icCliente && !"".equals(icCliente)) {
			qrySQL.append(" AND A.IC_CLIENTE = '" + icCliente + "'");
		}
		qrySQL.append(" ORDER BY B.CG_RAZON_SOCIAL");
		LOGGER.debug("qrySQL ::: "+qrySQL);
		LOGGER.debug("vars ::: "+super.getValoresBindCondicion());
		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		while(registros.next()) {
			ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
			elementoCatalogo.setClave(registros.getString("CLAVE"));
			elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
			coleccionElementos.add(elementoCatalogo);
		}
		LOGGER.debug("getListaElementos (S)");
		return coleccionElementos;
	}

	public String getIcCliente() {
		return icCliente;
	}

	public void setIcCliente(String icCliente) {
		this.icCliente = icCliente;
	}

}