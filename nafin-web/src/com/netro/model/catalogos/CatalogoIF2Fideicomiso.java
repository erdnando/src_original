package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Obtiene el listado de Intermediarios Financieros que tienen relaci�n con la EPO afiliada al IF Fideicomiso, excluyendo
 * el IF que Opera Fideicomiso para Desarrollo de Proveedores. Los IF devueltos poseen dispersion habilitada y est�n configurados
 * para operar con el producto de descuento electronico.
 *
 * PANTALLA: ADMIN_NAFIN - ADMINISTRACI�N � DISPERSI�N � IF FIDEICOMISO
 *
 * Query de Ejemplo:
 *
 *		     SELECT
 * 			  I.ic_if           AS CLAVE,
 * 			  I.cg_razon_social AS DESCRIPCION
 * 			FROM
 * 			  COMCAT_IF I,
 * 			  COMREL_PRODUCTO_IF CPI ,
 * 			  COMCAT_EPO CE ,
 * 			  COMREL_IF_EPO IE
 * 			WHERE
 * 			  I.cs_habilitado          = 'S'
 * 			AND CPI.ic_if              = I.ic_if
 * 			and CPI.IC_PRODUCTO_NAFIN  = 1   -- PRODUCTO DESCUENTO ELECTRONICO
 * 			and CPI.CG_DISPERSION      = 'S' -- IF CON DISPERSION
 * 			AND I.cs_opera_fideicomiso = 'N' -- EL IF NO OPERA FIDEICOMISO
 * 			AND I.ic_if                = IE.ic_if
 * 			AND IE.cs_vobo_nafin       = 'S'
 * 			AND IE.cs_bloqueo          = 'N'
 * 			and IE.IC_EPO              = CE.IC_EPO
 * 			AND CE.ic_epo              = 411 -- EPO QUE ESTA AFIALIADA AL IF FIDEICOMISO
 * 			AND CE.cs_habilitado       = 'S'
 * 			ORDER BY
 * 			  I.cg_razon_social
 *
 * @author jshernandez
 * @since  F017 - 2013 -- DSCTO ELECT - Fideicomiso para el Desarrollo de Proveedores; 11/09/2013 07:36:08 p.m.
 *
 *
 */
public class CatalogoIF2Fideicomiso extends CatalogoAbstract  {
	
	//Variable para enviar mensajes al log. 
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoIF2Fideicomiso.class);
	
	private String producto;
	private String dispersion;
	private String operaFideicomiso; 
	private String claveEPO;
	
	public CatalogoIF2Fideicomiso(){
		super.setPrefijoTablaPrincipal("I."); // from COMCAT_IF I
	}
	
	/**
	 * Obtiene una lista de elementos de tipo ElementoCatalogo
	 * @return regresa una colecci�n de elementos obtenidos
	 */	
	public List getListaElementos(){
		
		log.info("getListaElementos(E)");
		
		AccesoDB 		con 						= new AccesoDB();
		List 				coleccionElementos 	= new ArrayList();
		StringBuffer	query						= new StringBuffer();
		
		try {
			
			con.conexionDB();
			
			Registros 	registros		= null;			
			boolean  	hayProducto		= this.producto   != null && !"".equals(this.producto)  ?true:false;
			boolean  	hayDispersion	= this.dispersion != null && !"".equals(this.dispersion)?true:false;
			boolean 		hayClaveEPO		= this.claveEPO   != null && !"".equals(this.claveEPO)  ?true:false;
				
			// DEFINIR CAMPOS DE CONSULTA
			query.append( 
				"SELECT     "  +
					this.getCampoClave()       + " AS CLAVE,      "  +
					this.getCampoDescripcion() + " AS DESCRIPCION "
			);
			
			// DEFINIR LAS TABLAS SOBRE LAS QUE SE REALIZAR� LA CONSULTA
			query.append(" FROM COMCAT_IF I ");
 
			if( hayProducto || hayDispersion ){
				query.append(", COMREL_PRODUCTO_IF CPI ");
			}
 
			if( hayClaveEPO ){
				query.append(", COMCAT_EPO         CE  ");                            
				query.append(", COMREL_IF_EPO      IE  ");  	
			}
			
			// DEFINIR LA CONDICIONES DE LA CONSULTA
			query.append(	
				"WHERE                                       "  +
				"   I.cs_habilitado         = 'S'            "
			);
			
			// AGREGAR LAS CONDICIONES ADICIONALES: Dispersion, Producto.
			super.setCondicion();
			if (super.getCondicion().length()>0) {
				query.append(" AND " + getCondicion());
			}
		
			// DEFINIR ORDENAMIENTO DE LA CONSULTA
			if( this.getOrden() != null ){
				query.append(
					" ORDER BY "+this.getOrden()
				);
			} else {
				query.append(
					" ORDER BY "+this.getCampoDescripcion()
				);
			}
			
			// Realizar consulta
			log.debug("getListaElementos.query       = <" + query.toString()          + ">");
			log.debug("getListaElementos.condiciones = <" + getValoresBindCondicion() + ">");
			registros = con.consultarDB(query.toString(),getValoresBindCondicion());
			
			while(registros.next()) {
				
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				
				coleccionElementos.add(elementoCatalogo);
				
			}
			
		} catch(Exception e) {
 
			log.error("getListaElementos(Exception)");
			log.error("getListaElementos.producto         = <" + producto    		          + ">");
			log.error("getListaElementos.dispersion       = <" + dispersion  		          + ">");
			log.error("getListaElementos.operaFideicomiso = <" + operaFideicomiso          + ">"); 
			log.error("getListaElementos.claveEPO         = <" + claveEPO                  + ">");
			log.debug("getListaElementos.query            = <" + query.toString()	       + ">");
			log.debug("getListaElementos.condiciones      = <" + getValoresBindCondicion() + ">");
			e.printStackTrace();
			
			throw new RuntimeException("El cat�logo no pudo ser generado");
			
		} finally {
			
			if (con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			
			log.info("getListaElementos(S)");
			
		}
		
		return coleccionElementos;
		
	}

	public void setCondicionesAdicionales(){

		boolean  	hayProducto							= this.producto   		!= null && !"".equals(this.producto)  			?true:false;
		boolean  	hayDispersion						= this.dispersion 		!= null && !"".equals(this.dispersion)			?true:false;
		boolean     hayCondicionOperaFideicomiso	= this.operaFideicomiso != null && !"".equals(this.operaFideicomiso)	?true:false;
		boolean 		hayClaveEPO							= this.claveEPO   		!= null && !"".equals(this.claveEPO)  			?true:false;

		if( hayProducto || hayDispersion ){
			addCondicionAdicional( 
				"   CPI.ic_if             = I.ic_if "  
			);
		}
		
		if( hayProducto ){
			addCondicionAdicional( 
				"   CPI.ic_producto_nafin = ? "  
			); 
			addVariablesBind(getProducto());
		}
		
		if( hayDispersion ){
			addCondicionAdicional(  
				"   CPI.cg_dispersion     =  ? "
			);
			addVariablesBind(getDispersion());
		}
		
		if( hayCondicionOperaFideicomiso ){
			addCondicionAdicional(  
				"   I.cs_opera_fideicomiso    =  ? "
			);
			addVariablesBind(getOperaFideicomiso());
		}    
		
		if( hayClaveEPO ){
			addCondicionAdicional(  
				"   I.ic_if            = IE.ic_if  and "  +
				"   IE.cs_vobo_nafin   = 'S'       and "  +
				"   IE.cs_bloqueo      = 'N'       and "  +
				"   IE.ic_epo          = CE.ic_epo and "  +
				"   CE.ic_epo          =  ?        and "  + // Clave de la EPO
				"   CE.cs_habilitado   = 'S'           "
			);
			addVariablesBind(getClaveEPO());
		}
		
	}
	
	/**
	 * Establece el nombre del campo que representar� la descripci�n
	 * a obtener en la consulta
	 * @param campoDescripcion Campo de la descripcion
	 */
	public void setOrden(String orden) {
		
		if ( 
			this.getPrefijoTablaPrincipal() != null 
				&& 
			!this.getPrefijoTablaPrincipal().equals("") 
				&&
			orden.trim().indexOf(this.getPrefijoTablaPrincipal()) != 0 
		) {
			super.setOrden( this.getPrefijoTablaPrincipal() + orden );
		} else {
			super.setOrden( orden );
		}
				
	}
	
	/**
	 * Obtiene el valor de la clave del producto, por ejemplo 1, para "Descuento Electronico".
	 */
	public String getProducto() {		
		return producto;	
	}

	/**
	 * Se utiliza para indicar el producto, generalmente se consulta el 
	 * producto 1: Descuento Electronico
	 */
	public void setProducto(String producto) {		
		this.producto = producto;	
	}
 
	/**
	 * Obtiene el valor de la variable: dispersion, con "S" para S� o "N" para No.
	 */
	public String getDispersion() {		
		return dispersion;	
	}
	
	/**
	 * Se utiliza para restringir la busqueda a aquellos IFs que tiene la dispersion 
	 * habilitada (S) o deshabilita (N)
	 */
	public void setDispersion(String dispersion) {		
		this.dispersion = dispersion;	
	}

	/**
	 * Obtiene el valor de la variable: operaFideicomiso, con "S" para S� o "N" para No.
	 */
	public String getOperaFideicomiso() {
		return operaFideicomiso;
	}
 
	/**
	 * Se utiliza para restringir la b�squeda a aquellos IFs que operan Fideicomiso para 
	 * Desarrollo de Proveedores: "S" (S�) o "N" (No).
	 */
	public void setOperaFideicomiso(String operaFideicomiso) {
		this.operaFideicomiso = operaFideicomiso;
	}

	/**
	 * Obtiene el valor de la variable: claveEPO ( COMCAT_EPO.IC_EPO )
	 */
	public String getClaveEPO() {
		return claveEPO;
	}

	/**
	 * Se utiliza para indicar la clave de la EPO ( COMCAT_EPO.IC_EPO ) que est� afiliada 
	 * al IF FIDEICOMISO.
	 */
	public void setClaveEPO(String claveEPO) {
		this.claveEPO = claveEPO;
	}

}