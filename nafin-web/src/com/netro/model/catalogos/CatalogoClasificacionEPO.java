package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Esta clase genera u obtiene los valores del catalogo
 * cdercat_clasificacion_epo, en base a ciertas condiciones establecidas
 * @author Deysi Laura Hernandez Contreras
 */
 
public class CatalogoClasificacionEPO extends CatalogoAbstract
{
	private String claveEpo;
	
	public CatalogoClasificacionEPO() {
		super();
		super.setPrefijoTablaPrincipal("c."); //cdercat_clasificacion_epo c
	}
	
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(CatalogoClasificacionEPO.class);
		
	/**
	 * Obtiene una lista de elementos de tipo ElementoCatalogo
	 * @return regresa una colección de elementos obtenidos
	 */	
	public List getListaElementos() {
		
		log.info("getListaElementos:::::::(E)");
		
		List coleccionElementos = new ArrayList();
		StringBuffer tables = new StringBuffer();
		StringBuffer hint = new StringBuffer();
		StringBuffer condicionJoin = new StringBuffer();
		StringBuffer orden = new StringBuffer();
	
		try{
			if (super.getCampoClave() == null || super.getCampoClave().equals("") ||
				super.getCampoDescripcion() == null || super.getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		Registros regCatEstado	=	null;
		
		
		tables.append(" cdercat_clasificacion_epo c, comcat_epo e  ");
		hint.append("");
		condicionJoin.append(" c.ic_epo = e.ic_epo ");
			
//		if(this.seleccion!=null && !this.seleccion.equals("")){
//			condiciones.append(" and c.ic_epo in ("+this.seleccion+") ");
//		}

		//CONDICIONES ADICIONALES que no sean de join y que requieran
		//de un valor NO DEBEN IR AQUI. ponerlo en el metodo de condiciones
		//adicionales para que se pueda manejar variables Bind
			
		StringBuffer qrySQL = new StringBuffer();
		
		qrySQL.append(
				" SELECT " + hint + " " + super.getCampoClave() + " AS CLAVE, " +
				super.getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tables +
				" WHERE " + condicionJoin
				);

								
		//Genera las condiciones necesarias y las coloca en this.condicion
		//Al llamar a setCondicion se establecen las condiciones para 
		//IN y NOT IN... y además se manda a llamar la implementación especifica
		//setCondicionesAdicionales de la clase
		super.setCondicion();
		
		if (super.getCondicion().length()>0) {
			qrySQL.append(" AND " + super.getCondicion());
		}
	
		if (super.getOrden() == null || super.getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + super.getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + super.getOrden() );
		}
		
		log.debug("qrySQL CatalogoClasificacionEPO::: "+qrySQL);
		log.debug("vars ::: "+super.getValoresBindCondicion());

		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
			
		while(registros.next()) {
			ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
			elementoCatalogo.setClave(registros.getString("CLAVE"));
			elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
			coleccionElementos.add(elementoCatalogo);
		}
			
		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}
	
	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		StringBuffer condicionAdicional = new StringBuffer();
		
//		try{
//			if (this.xxx == null || this.xxx.equals("")  ) {
//				throw new Exception("Los parametros requeridos no has sido establecidos");
//			}
//			Integer.parseInt(this.xxx);
//		}catch (Exception e){
//			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
//		}
		
		if(this.claveEpo!=null && !this.claveEpo.equals("")){
			condicionAdicional.append(" c.ic_epo = ? ");
			super.addCondicionAdicional(condicionAdicional.toString());
			//agregar a variables bind estos valores
			super.addVariablesBind(new Integer(this.claveEpo));
		}
	}


	/**
	 * 
	 * @deprecated Usar getCampoClave()
	 */
	public String getClave()
	{
		return super.getCampoClave();
	}

	/**
	 * 
	 * @deprecated Usar getCampoDescripcion()
	 */
	public String getDescripcion()
	{
		return super.getCampoDescripcion();
	}
	
	/**
	 * 
	 * @deprecated Usar getClaveEpo()
	 */
	public String getSeleccion()
	{
		return this.claveEpo;
	}

	
	/**
	 * Establece la clave(id) que se va obtener en la consulta
	 * @deprecated setCampoClave
	 */	
	public void setClave(String clave)
	{
		super.setCampoClave(clave);		
	}

	/**
	 * Establece la descripcion  que se va obtener en la consulta
	 * @deprecated setCampoDescripcion
	 */		
	public void setDescripcion(String descripcion)
	{
		super.setCampoDescripcion(descripcion);
	}

	/**
	 * Establece valor para indicar algun valor en especifico a consultar
	 * @deprecated
	 */	
	public void setSeleccion(String seleccion)
	{
		this.claveEpo = seleccion;
	}


	/**
	 * Establece la clave de la epo a la cual corresponden el catalogo de clasificacion epo.
	 * @param claveEpo Clave de la epo (ic_epo)
	 */
	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}


	/**
	 * Obtiene la clave de la epo
	 * @return Cadena con al clave de la epo (ic_epo)
	 */
	public String getClaveEpo() {
		return claveEpo;
	}

}