package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


/**
 * Catalogo de Pymes (comcat_pyme)
 *
 * Ejemplo de uso:
 * CatalogoBancoServicio cat = new CatalogoBancoServicio();
 * cat.setCampoClave("ic_pyme");
 * cat.setCampoDescripcion("cg_razon_social");
 * @author Deysi Laura Hernandez Contreras */ 
 
 
public class CatalogoBancoServicio extends CatalogoAbstract {


	//Variable para enviar mensajes al log.
	private static Log log = ServiceLocator.getInstance().getLog(CatalogoBancoServicio.class);	
	
	StringBuffer tablas = new StringBuffer();	
	private String tipoFinanciera;
	


	public CatalogoBancoServicio() {
			
	}  

	/**
	 * Ejecuta la consulta necesaria y obtiene una lista de
	 * elementos de tipo netropology.utilerias.ElementoCatalogo
	 * para facilitar su uso en struts en caso de requerirse.
	 * @return regresa una colecci�n de elementos obtenidos
	 */
	public List getListaElementos() throws AppException{
		log.debug("getListaElementos (E)");
		List coleccionElementos = new ArrayList();

		try{
			if (getCampoClave() == null || getCampoClave().equals("") ||
				getCampoDescripcion() == null || getCampoDescripcion().equals("") ) {
				throw new Exception("Los parametros requeridos no has sido establecidos");
			}
		}catch (Exception e){
			throw new AppException("Error en los parametros establecidos. " + e.getMessage());
		}

		StringBuffer condicionJoin = new StringBuffer();
		StringBuffer hint = new StringBuffer();
		
		
		hint.append("/*+ use_nl(cif tif) index(cif in_comcat_if_07_nuk) ordered*/");
		
	   tablas.append(" comcat_if cif, comcat_financiera tif");
		
		
      condicionJoin.append(" cif.ic_financiera = tif.ic_financiera");
      
    
				
		StringBuffer qrySQL = new StringBuffer();

		qrySQL.append(
				" SELECT " + hint + " DISTINCT " + getCampoClave() + " AS CLAVE, " +
				getCampoDescripcion() + " AS DESCRIPCION " +
				" FROM " + tablas +
				" WHERE " + condicionJoin			 
			);
     

		if (getOrden() == null || getOrden().equals("")) {
			qrySQL.append(
					" ORDER BY " + getCampoDescripcion() );
		} else {
			qrySQL.append(
					" ORDER BY " + getOrden() );
		} 
     
		log.debug(" ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: ");
		log.debug("qrySQL ::: "+qrySQL);
		log.debug("vars ::: "+getValoresBindCondicion());
		log.debug(" ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: ");

		AccesoDB con = new AccesoDB();
		Registros registros = null;
		try{
			con.conexionDB();
			registros = con.consultarDB(qrySQL.toString(), super.getValoresBindCondicion());
		} catch (Exception e){
			throw new AppException("El listado de catalogo no pudo ser generado",e);
		}finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}  
		}
  
		while(registros.next()) {
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(registros.getString("CLAVE"));
				elementoCatalogo.setDescripcion(registros.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
		}

		log.debug("getListaElementos (S)");
		return coleccionElementos;
	}




	/**
	 * Establece las condiciones adicionales de acuerdo a los parametros establecidos
	 * Se establecen aqui, para poder utilizar las variables bind.
	 */
	public void setCondicionesAdicionales() {
		String  condicionAdicional ="";
		
			
		if(!"".equals(tipoFinanciera)) {
			if(!"".equals(tipoFinanciera)) { 
				condicionAdicional= " AND tif.ic_tipo_financiera IN (?, ?, ?, ?, ?, ?, ?)";
				super.addCondicionAdicional(condicionAdicional);
				super.addVariablesBind(tipoFinanciera);	
			}		 
		}	
		
	}

	public String getTipoFinanciera() {
		return tipoFinanciera;
	}

	public void setTipoFinanciera(String tipoFinanciera) {
		this.tipoFinanciera = tipoFinanciera;
	}
	
	






}