package com.netro.model.catalogos;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import org.apache.commons.logging.Log;

public class CatalogoNumContrato extends CatalogoAbstract{

	private final static Log log = ServiceLocator.getInstance().getLog(CatalogoNumContrato.class);
	private String icEpo;
	private String estatus;

	public CatalogoNumContrato(){
		super();
	}

	public List getListaElementos(){

		AccesoDB con = new AccesoDB();
		List coleccionElementos = new ArrayList();
		String tables = "";
		StringBuffer condiciones = new StringBuffer();
		String orden = "";
		String query = "";

		log.info("getListaElementos(E)");

		try{

			con.conexionDB();
			Registros regCatalogo = null;
			tables = " CDER_SOLICITUD ";

			if (this.icEpo != null && !this.icEpo.equals("")) {
				condiciones.append(" AND IC_EPO = " + this.icEpo + " ");
			}
			if (this.estatus != null && !this.estatus.equals("")) {
				condiciones.append(" AND IC_ESTATUS_SOLIC IN (" + this.estatus + ") ");
			}

			orden = " ORDER BY CG_NO_CONTRATO ";

			query += " SELECT DISTINCT " + super.getCampoClave() + " AS CLAVE, " +
						super.getCampoDescripcion() +" AS DESCRIPCION " +
						" FROM " + tables +
						" WHERE 1 = 1 " +
						condiciones.toString();

			//Genera las condiciones necesarias y las coloca en this.condicion
			//Al llamar a setCondicion se establecen las condiciones para 
			//IN y NOT IN... y además se manda a llamar la implementación especifica
			//setCondicionesAdicionales de la clase
			super.setCondicion();
	
			if (super.getCondicion().length()>0) {
				query += " AND " + super.getCondicion();
			}
			
			if (super.getOrden() == null || super.getOrden().equals("")) {
				query += 
						" ORDER BY " + super.getCampoDescripcion() ;
			} else {
				query += 
						" ORDER BY " + super.getOrden() ;
			}

			log.debug("..:: query: " + query);

			regCatalogo = con.consultarDB(query,super.getValoresBindCondicion());

			while(regCatalogo.next()){
				ElementoCatalogo elementoCatalogo = new ElementoCatalogo();
				elementoCatalogo.setClave(regCatalogo.getString("CLAVE"));
				elementoCatalogo.setDescripcion(regCatalogo.getString("DESCRIPCION"));
				coleccionElementos.add(elementoCatalogo);
			}

		} catch(Exception e) {
			e.printStackTrace();
			throw new RuntimeException("El catalogo no pudo ser generado");
		} finally {
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}

		log.info("getListaElementos(S)");
		return coleccionElementos;
	}

/**********************************************************************
 *                         GETTERS Y SETTERS                          *
 **********************************************************************/
 	public String getIcEpo() {		
		return this.icEpo;	
	}
 
	public void setIcEpo(String icEpo) {		
		this.icEpo = icEpo;	
	}

 	public String getEstatus() {		
		return this.estatus;	
	}
 
	public void setEstatus(String estatus) {		
		this.estatus = estatus;	
	}

}