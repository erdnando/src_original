package com.netro.factdistribuido;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import org.apache.commons.logging.Log;

/**
 * Clase para la pantalla Administraci�n / Parametrizaci�n / Cuentas Bancarias Pymes - Dactoraje Distribuido.
 * Obtiene los datos para generar el grid y los archivos a descargar (PDF y XLS)
 */
public class ConsultaFactDistribuido implements IQueryGeneratorRegExtJS {

	private static final Log LOGGER = ServiceLocator.getInstance().getLog(ConsultaFactDistribuido.class);
	private Long icNafinElectronico;
	private Long icPyme;
	private Long icEpo;
	private Long icIf;
	private Long icBeneficiario;
	private Long icMoneda;
	private List conditions;

	//Para la generaci�n del pdf
	private String[] icConsecutivos;
	private String fecha;
	private String usuario;
	private String totalReg;

	//Variables para la generaci�n del archivo csv
	private String nombreEpo;
	private String proveedor;
	private String noNafinElectronico;
	private String nombreIf;
	private String bancoServicio;
	private String beneficiario;
	private String sucursal;
	private String moneda;
	private String noCuenta;
	private String noCuentaClabe;
	private String noCuentaSpid;
	private String swift;
	private String aba;
	private String plaza;
	private String autorizacionIf;
	private String cambioCuenta;

	/**
	 * Constructor vac�o
	 */
	public ConsultaFactDistribuido() {
	}


	public Long getIcNafinElectronico() {
		return icNafinElectronico;
	}

	public void setIcNafinElectronico(Long icNafinElectronico) {
		this.icNafinElectronico = icNafinElectronico;
	}

	public Long getIcPyme() {
		return icPyme;
	}

	public void setIcPyme(Long icPyme) {
		this.icPyme = icPyme;
	}

	public Long getIcEpo() {
		return icEpo;
	}

	public void setIcEpo(Long icEpo) {
		this.icEpo = icEpo;
	}

	public Long getIcIf() {
		return icIf;
	}

	public void setIcIf(Long icIf) {
		this.icIf = icIf;
	}

	public Long getIcBeneficiario() {
		return icBeneficiario;
	}

	public void setIcBeneficiario(Long icBeneficiario) {
		this.icBeneficiario = icBeneficiario;
	}

	public Long getIcMoneda() {
		return icMoneda;
	}

	public void setIcMoneda(Long icMoneda) {
		this.icMoneda = icMoneda;
	}

	public String[] getIcConsecutivos() {
		return icConsecutivos;
	}

	public void setIcConsecutivos(String[] icConsecutivos) {
		this.icConsecutivos = icConsecutivos;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getTotalReg() {
		return totalReg;
	}

	public void setTotalReg(String totalReg) {
		this.totalReg = totalReg;
	}

	@Override
	public List getConditions() {
		return conditions;
	}

	/**
	 * Query para obtener las llaves primarias
	 * @return Cadena con la consulta de SQL para obtener llaves primarias
	 */
	@Override
	public String getDocumentQuery() {
		LOGGER.info("getDocumentQuery(E)");

		StringBuilder strQuery = new StringBuilder();
		conditions = new ArrayList();

		strQuery.append("SELECT A.IC_CONSECUTIVO \n" + 
		"FROM COMREL_CTA_BAN_DISTRIBUIDO A \n" + 
		"INNER JOIN COMCAT_EPO B ON A.IC_EPO=B.IC_EPO \n" + 
		"INNER JOIN COMCAT_IF C ON A.IC_IF=C.IC_IF\n" + 
		"INNER JOIN COMCAT_IF D ON A.IC_BENEFICIARIO=D.IC_IF \n" + 
		"INNER JOIN COMCAT_PYME E ON A.IC_PYME=E.IC_PYME \n" + 
		"INNER JOIN COMCAT_MONEDA F ON A.IC_MONEDA=F.IC_MONEDA \n" + 
		"LEFT JOIN COMCAT_BANCOS_TEF G ON A.IC_BANCOS_TEF=G.IC_BANCOS_TEF \n" + 
		"LEFT JOIN COMCAT_PLAZA H ON A.IC_PLAZA=H.IC_PLAZA WHERE 1=1\n");

		if(null != icNafinElectronico && icNafinElectronico > 0) {
			strQuery.append("AND A.IC_NAFIN_ELECTRONICO = ? \n");
			conditions.add(icNafinElectronico);
		}
		if(null != icEpo && icEpo > 0) {
			strQuery.append("AND A.IC_EPO = ? \n");
			conditions.add(icEpo);
		}
		if(null != icIf && icIf > 0) {
			strQuery.append("AND A.IC_IF = ? \n");
			conditions.add(icIf);
		}
		if(null != icBeneficiario && icBeneficiario > 0) {
			strQuery.append("AND A.IC_BENEFICIARIO = ? \n");
			conditions.add(icBeneficiario);
		}
		if(null != icMoneda && icMoneda > 0) {
			strQuery.append("AND A.IC_MONEDA = ? \n");
			conditions.add(icMoneda);
		}

		LOGGER.debug("getDocumentQuery(strQuery): " + strQuery.toString());
		LOGGER.debug("getDocumentQuery(conditions): " + conditions);
		LOGGER.info("getDocumentQuery(S)");
		return strQuery.toString();
	}

	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @param ids
	 * @return Cadena con la consulta de SQL para obtener la informaci�n
	 * completa de los registros con las llaves especificadas
	 */
	@Override
	public String getDocumentSummaryQueryForIds(List ids) {
		LOGGER.info("getDocumentSummaryQueryForIds(E)");

		StringBuilder strQuery = new StringBuilder();
		conditions = new ArrayList();

		strQuery.append("SELECT A.IC_CONSECUTIVO\n"
		+",A.IC_IF\n"
		+",A.IC_EPO\n"
		+",A.IC_PYME\n"
		+",A.IC_BENEFICIARIO\n"
		+",A.IC_MONEDA\n"
		+",A.IC_NAFIN_ELECTRONICO\n"
		+",A.IC_BANCOS_TEF\n"
		+",A.CG_SUCURSAL\n"
		+",A.CG_NUMERO_CUENTA\n"
		+",A.CG_CUENTA_CLABE\n"
		+",A.CG_CUENTA_SPID\n"
		+",A.CG_SWIFT\n"
		+",A.CG_ABA\n"
		+",A.IC_PLAZA\n"
		+",CASE WHEN A.CG_AUTORIZA_IF='S' THEN 'SI' ELSE 'NO' END AS CG_AUTORIZA_IF\n"
		+",CASE WHEN A.CG_CAMBIO_CUENTA='S' THEN 'SI' ELSE 'NO' END AS CG_CAMBIO_CUENTA\n"
		+",CASE WHEN A.CG_AUTORIZA_IF='N' THEN 'ROJO' ELSE '' END AS COLOR\n"
		+",B.CG_RAZON_SOCIAL AS NOMBRE_EPO\n"
		+",E.CG_RAZON_SOCIAL AS NOMBRE_PYME\n"
		+",C.CG_RAZON_SOCIAL AS INTERMEDIARIO_FINANCIERO\n"
		+",G.CD_DESCRIPCION AS BANCO_SERVICIO\n"
		+",D.CG_RAZON_SOCIAL AS NOMBRE_BENEFICIARIO\n"
		+",F.CD_NOMBRE AS NOMBRE_MONEDA\n"
		+",H.CD_DESCRIPCION AS CG_PLAZA\n"
		+"FROM COMREL_CTA_BAN_DISTRIBUIDO A\n"
		+"INNER JOIN COMCAT_EPO B ON A.IC_EPO=B.IC_EPO\n"
		+"INNER JOIN COMCAT_IF C ON A.IC_IF=C.IC_IF\n"
		+"INNER JOIN COMCAT_IF D ON A.IC_BENEFICIARIO=D.IC_IF\n"
		+"INNER JOIN COMCAT_PYME E ON A.IC_PYME=E.IC_PYME\n"
		+"INNER JOIN COMCAT_MONEDA F ON A.IC_MONEDA=F.IC_MONEDA\n"
		+"LEFT JOIN COMCAT_BANCOS_TEF G ON A.IC_BANCOS_TEF=G.IC_BANCOS_TEF\n"
		+"LEFT JOIN COMCAT_PLAZA H ON A.IC_PLAZA=H.IC_PLAZA\n");
		strQuery.append("WHERE A.IC_CONSECUTIVO IN(");
		for (int i = 0; i < ids.size(); i++) {
			List lItem = (ArrayList)ids.get(i);
			if(i > 0){
				strQuery.append(",");
			}
			strQuery.append("?");
			conditions.add(new Long(lItem.get(0).toString()));
		}
		strQuery.append(") \n");
		strQuery.append("ORDER BY B.CG_RAZON_SOCIAL, C.CG_RAZON_SOCIAL, E.CG_RAZON_SOCIAL");

		LOGGER.debug("getDocumentSummaryQueryForIds(strQuery): " + strQuery.toString());
		LOGGER.debug("getDocumentSummaryQueryForIds(conditions): " + conditions);
		LOGGER.info("getDocumentSummaryQueryForIds(S)");
		return strQuery.toString();
	}

	/**
	 * Obtiene los totales
	 * @return
	 */
	@Override
	public String getAggregateCalculationQuery() {
		LOGGER.info("getAggregateCalculationQuery(E)");

		StringBuilder strQuery = new StringBuilder();
		conditions = new ArrayList();

		strQuery.append("SELECT COUNT(A.IC_CONSECUTIVO) as total \n" + 
		"FROM COMREL_CTA_BAN_DISTRIBUIDO A \n" + 
		"INNER JOIN COMCAT_EPO B ON A.IC_EPO=B.IC_EPO \n" + 
		"INNER JOIN COMCAT_IF C ON A.IC_IF=C.IC_IF\n" + 
		"INNER JOIN COMCAT_IF D ON A.IC_BENEFICIARIO=D.IC_IF \n" + 
		"INNER JOIN COMCAT_PYME E ON A.IC_PYME=E.IC_PYME \n" + 
		"INNER JOIN COMCAT_MONEDA F ON A.IC_MONEDA=F.IC_MONEDA \n" + 
		"LEFT JOIN COMCAT_BANCOS_TEF G ON A.IC_BANCOS_TEF=G.IC_BANCOS_TEF \n" + 
		"LEFT JOIN COMCAT_PLAZA H ON A.IC_PLAZA=H.IC_PLAZA WHERE 1=1\n");

		if(null != icNafinElectronico && icNafinElectronico > 0) {
			strQuery.append("AND A.IC_NAFIN_ELECTRONICO = ? \n");
			conditions.add(icNafinElectronico);
		}
		if(null != icEpo && icEpo > 0) {
			strQuery.append("AND A.IC_EPO = ? \n");
			conditions.add(icEpo);
		}
		if(null != icIf && icIf > 0) {
			strQuery.append("AND A.IC_IF = ? \n");
			conditions.add(icIf);
		}
		if(null != icBeneficiario && icBeneficiario > 0) {
			strQuery.append("AND A.IC_BENEFICIARIO = ? \n");
			conditions.add(icBeneficiario);
		}
		if(null != icMoneda && icMoneda > 0) {
			strQuery.append("AND A.IC_MONEDA = ? \n");
			conditions.add(icMoneda);
		}

		LOGGER.debug("getAggregateCalculationQuery(strQuery): " + strQuery.toString());
		LOGGER.debug("getAggregateCalculationQuery(conditions): " + conditions);
		LOGGER.info("getAggregateCalculationQuery(S)");
		return strQuery.toString();
	}

	/**
	 * Consulta para generar el archivo
	 * @return
	 */
	@Override
	public String getDocumentQueryFile() {
		LOGGER.info("getDocumentQueryFile(E)");

		StringBuilder strQuery = new StringBuilder();
		conditions = new ArrayList();

		strQuery.append("SELECT A.IC_CONSECUTIVO\n"
		+",A.IC_IF\n"
		+",A.IC_EPO\n"
		+",A.IC_PYME\n"
		+",A.IC_BENEFICIARIO\n"
		+",A.IC_MONEDA\n"
		+",A.IC_NAFIN_ELECTRONICO\n"
		+",A.IC_BANCOS_TEF\n"
		+",A.CG_SUCURSAL\n"
		+",A.CG_NUMERO_CUENTA\n"
		+",A.CG_CUENTA_CLABE\n"
		+",A.CG_CUENTA_SPID\n"
		+",A.CG_SWIFT\n"
		+",A.CG_ABA\n"
		+",A.IC_PLAZA\n"
		+",CASE WHEN A.CG_AUTORIZA_IF='S' THEN 'SI' ELSE 'NO' END AS CG_AUTORIZA_IF\n"
		+",CASE WHEN A.CG_CAMBIO_CUENTA='S' THEN 'SI' ELSE 'NO' END AS CG_CAMBIO_CUENTA\n"
		+",B.CG_RAZON_SOCIAL AS NOMBRE_EPO\n"
		+",E.CG_RAZON_SOCIAL AS NOMBRE_PYME\n"
		+",C.CG_RAZON_SOCIAL AS INTERMEDIARIO_FINANCIERO\n"
		+",G.CD_DESCRIPCION AS BANCO_SERVICIO\n"
		+",D.CG_RAZON_SOCIAL AS NOMBRE_BENEFICIARIO\n"
		+",F.CD_NOMBRE AS NOMBRE_MONEDA\n"
		+",H.CD_DESCRIPCION AS CG_PLAZA\n"
		+"FROM COMREL_CTA_BAN_DISTRIBUIDO A\n"
		+"INNER JOIN COMCAT_EPO B ON A.IC_EPO=B.IC_EPO\n"
		+"INNER JOIN COMCAT_IF C ON A.IC_IF=C.IC_IF\n"
		+"INNER JOIN COMCAT_IF D ON A.IC_BENEFICIARIO=D.IC_IF\n"
		+"INNER JOIN COMCAT_PYME E ON A.IC_PYME=E.IC_PYME\n"
		+"INNER JOIN COMCAT_MONEDA F ON A.IC_MONEDA=F.IC_MONEDA\n"
		+"LEFT JOIN COMCAT_BANCOS_TEF G ON A.IC_BANCOS_TEF=G.IC_BANCOS_TEF \n"
		+"LEFT JOIN COMCAT_PLAZA H ON A.IC_PLAZA=H.IC_PLAZA\n"
		+"WHERE 1=1 \n");

		if("".equals(usuario)) {
			if(null != icNafinElectronico && icNafinElectronico > 0) {
				strQuery.append("AND A.IC_NAFIN_ELECTRONICO = ? \n");
				conditions.add(icNafinElectronico);
			}
			if(null != icEpo && icEpo > 0) {
				strQuery.append("AND A.IC_EPO = ? \n");
				conditions.add(icEpo);
			}
			if(null != icIf && icIf > 0) {
				strQuery.append("AND A.IC_IF = ? \n");
				conditions.add(icIf);
			}
			if(null != icBeneficiario && icBeneficiario > 0) {
				strQuery.append("AND A.IC_BENEFICIARIO = ? \n");
				conditions.add(icBeneficiario);
			}
			if(null != icMoneda && icMoneda > 0) {
				strQuery.append("AND A.IC_MONEDA = ? \n");
				conditions.add(icMoneda);
			}
			strQuery.append(" ORDER BY B.CG_RAZON_SOCIAL, C.CG_RAZON_SOCIAL, E.CG_RAZON_SOCIAL");
		} else {
			strQuery.append("AND A.IC_CONSECUTIVO IN( \n");
			for(int i=0; i<icConsecutivos.length;i++){
				if(i > 0){
					strQuery.append(",");
				}
				strQuery.append("?");
				conditions.add(icConsecutivos[i]);
			}
			strQuery.append(") ORDER BY B.CG_RAZON_SOCIAL, C.CG_RAZON_SOCIAL, E.CG_RAZON_SOCIAL");
		}

		LOGGER.debug("getDocumentQueryFile(strQuery): " + strQuery.toString());
		LOGGER.debug("getDocumentQueryFile(conditions): " + conditions);
		LOGGER.info("getDocumentSummaryQueryForIds(S)");
		return strQuery.toString();
	}

	/**
	 * M�todo para generar los archivos CSV y PDF
	 * @param request
	 * @param rs
	 * @param path ruta donde se genera el archivo
	 * @param tipo PDF o CSV
	 * @return nombre del archivo
	 */
	@Override
	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo) {

		LOGGER.info("crearCustomFile(E)");

		String nombreArchivo ="";

		try {

			if("CSV".equals(tipo)) {
				StringBuilder contenidoArchivo = new StringBuilder();
				OutputStreamWriter writer = null;
				BufferedWriter buffer = null;
				int total = 0;

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
				buffer = new BufferedWriter(writer);

				contenidoArchivo = new StringBuilder();
				contenidoArchivo.append("EPO,"
					+"Proveedor,"
					+"N�mero de Nafin Electr�nico,"
					+"IF,"
					+"Banco de servicio,"
					+"Beneficiario,"
					+"Sucursal,"
					+"Moneda,"
					+"No. de cuenta,"
					+"No. Cuenta CLABE,"
					+"No. Cuenta Spid,"
					+"SWIFT,"
					+"ABA,"
					+"Plaza,"
					+"Autorizaci�n IF,"
					+"Cambio de Cuenta\n");

				while (rs.next()) {

					nombreEpo          = (rs.getString("NOMBRE_EPO")               == null) ? "" : rs.getString("NOMBRE_EPO");
					proveedor          = (rs.getString("NOMBRE_PYME")              == null) ? "" : rs.getString("NOMBRE_PYME");
					noNafinElectronico = (rs.getString("IC_NAFIN_ELECTRONICO")     == null) ? "" : rs.getString("IC_NAFIN_ELECTRONICO");
					nombreIf           = (rs.getString("INTERMEDIARIO_FINANCIERO") == null) ? "" : rs.getString("INTERMEDIARIO_FINANCIERO");
					bancoServicio      = (rs.getString("BANCO_SERVICIO")           == null) ? "" : rs.getString("BANCO_SERVICIO");
					beneficiario       = (rs.getString("NOMBRE_BENEFICIARIO")      == null) ? "" : rs.getString("NOMBRE_BENEFICIARIO");
					sucursal           = (rs.getString("CG_SUCURSAL")              == null) ? "" : rs.getString("CG_SUCURSAL");
					moneda             = (rs.getString("NOMBRE_MONEDA")            == null) ? "" : rs.getString("NOMBRE_MONEDA");
					noCuenta           = (rs.getString("CG_NUMERO_CUENTA")         == null) ? "" : rs.getString("CG_NUMERO_CUENTA");
					noCuentaClabe      = (rs.getString("CG_CUENTA_CLABE")          == null) ? "" : rs.getString("CG_CUENTA_CLABE");
					noCuentaSpid       = (rs.getString("CG_CUENTA_SPID")           == null) ? "" : rs.getString("CG_CUENTA_SPID");
					swift              = (rs.getString("CG_SWIFT")                 == null) ? "" : rs.getString("CG_SWIFT");
					aba                = (rs.getString("CG_ABA")                   == null) ? "" : rs.getString("CG_ABA");
					plaza              = (rs.getString("CG_PLAZA")                 == null) ? "" : rs.getString("CG_PLAZA");
					autorizacionIf     = (rs.getString("CG_AUTORIZA_IF")           == null) ? "" : rs.getString("CG_AUTORIZA_IF");
					cambioCuenta       = (rs.getString("CG_CAMBIO_CUENTA")         == null) ? "" : rs.getString("CG_CAMBIO_CUENTA");

					contenidoArchivo.append(
						nombreEpo.replace(',',' ')          +","+
						proveedor.replace(',',' ')          +","+
						noNafinElectronico.replace(',',' ') +","+
						nombreIf.replace(',',' ')           +","+
						bancoServicio.replace(',',' ')      +","+
						beneficiario.replace(',',' ')       +","+
						sucursal.replace(',',' ')           +","+
						moneda.replace(',',' ')             +","+
						noCuenta.replace(',',' ')           +","+
						noCuentaClabe.replace(',',' ')      +","+
						noCuentaSpid.replace(',',' ')       +","+
						swift.replace(',',' ')              +","+
						aba.replace(',',' ')                +","+
						plaza.replace(',',' ')              +","+
						autorizacionIf.replace(',',' ')     +","+
						cambioCuenta.replace(',',' ')       +"\n"
					);

					total++;
					if(total==1000){
						total=0;
						buffer.write(contenidoArchivo.toString());
						contenidoArchivo = new StringBuilder();//Limpio
					}
				}

				buffer.write(contenidoArchivo.toString());
				buffer.close(); 
				contenidoArchivo = new StringBuilder();//Limpio

			} else if("PDF".equals(tipo)) {

				ComunesPDF pdfDoc = new ComunesPDF();
				HttpSession session  = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				String[] meses     = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
					(String)session.getAttribute("sesExterno"),
					(String)session.getAttribute("strNombre"),
					(String)session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

				pdfDoc.setLTable(2, 50);
				pdfDoc.setLCell("ACUSE",               "celda02",ComunesPDF.CENTER,2);
				pdfDoc.setLCell("Fecha y hora:",       "formas", ComunesPDF.LEFT);
				pdfDoc.setLCell(fecha,                 "formas", ComunesPDF.LEFT);
				pdfDoc.setLCell("Usuario",             "formas", ComunesPDF.LEFT);
				pdfDoc.setLCell(usuario,               "formas", ComunesPDF.LEFT);
				pdfDoc.setLCell("Total de registros:", "formas", ComunesPDF.LEFT);
				pdfDoc.setLCell(totalReg,              "formas", ComunesPDF.LEFT);
				pdfDoc.addLTable();

				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

				pdfDoc.setLTable(14, 85);
				pdfDoc.setLCell("EPO",                         "celda02",ComunesPDF.CENTER);
				pdfDoc.setLCell("Proveedor",                   "celda02",ComunesPDF.CENTER);
				pdfDoc.setLCell("N�mero de Nafin Electr�nico", "celda02",ComunesPDF.CENTER);
				pdfDoc.setLCell("IF",                          "celda02",ComunesPDF.CENTER);
				pdfDoc.setLCell("Banco de servicio",           "celda02",ComunesPDF.CENTER);
				pdfDoc.setLCell("Beneficiario",                "celda02",ComunesPDF.CENTER);
				pdfDoc.setLCell("Sucursal",                    "celda02",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda",                      "celda02",ComunesPDF.CENTER);
				pdfDoc.setLCell("No. de cuenta",               "celda02",ComunesPDF.CENTER);
				pdfDoc.setLCell("No. Cuenta CLABE",            "celda02",ComunesPDF.CENTER);
				pdfDoc.setLCell("No. Cuenta Spid",             "celda02",ComunesPDF.CENTER);
				pdfDoc.setLCell("SWIFT",                       "celda02",ComunesPDF.CENTER);
				pdfDoc.setLCell("ABA",                         "celda02",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plaza",                       "celda02",ComunesPDF.CENTER);

				while (rs.next()) {
					pdfDoc.setLCell((rs.getString("NOMBRE_EPO")               == null) ? "" : rs.getString("NOMBRE_EPO"),               "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell((rs.getString("NOMBRE_PYME")              == null) ? "" : rs.getString("NOMBRE_PYME"),              "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell((rs.getString("IC_NAFIN_ELECTRONICO")     == null) ? "" : rs.getString("IC_NAFIN_ELECTRONICO"),     "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell((rs.getString("INTERMEDIARIO_FINANCIERO") == null) ? "" : rs.getString("INTERMEDIARIO_FINANCIERO"), "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell((rs.getString("BANCO_SERVICIO")           == null) ? "" : rs.getString("BANCO_SERVICIO"),           "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell((rs.getString("NOMBRE_BENEFICIARIO")      == null) ? "" : rs.getString("NOMBRE_BENEFICIARIO"),      "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell((rs.getString("CG_SUCURSAL")              == null) ? "" : rs.getString("CG_SUCURSAL"),              "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell((rs.getString("NOMBRE_MONEDA")            == null) ? "" : rs.getString("NOMBRE_MONEDA"),            "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell((rs.getString("CG_NUMERO_CUENTA")         == null) ? "" : rs.getString("CG_NUMERO_CUENTA"),         "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell((rs.getString("CG_CUENTA_CLABE")          == null) ? "" : rs.getString("CG_CUENTA_CLABE"),          "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell((rs.getString("CG_CUENTA_SPID")           == null) ? "" : rs.getString("CG_CUENTA_SPID"),           "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell((rs.getString("CG_SWIFT")                 == null) ? "" : rs.getString("CG_SWIFT"),                 "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell((rs.getString("CG_ABA")                   == null) ? "" : rs.getString("CG_ABA"),                   "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell((rs.getString("CG_PLAZA")                 == null) ? "" : rs.getString("CG_PLAZA"),                 "formas", ComunesPDF.CENTER);
				}

				pdfDoc.addLTable();
				pdfDoc.endDocument();

			}

		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo", e);
		}
		LOGGER.info("crearCustomFile(S)");
		return nombreArchivo;
	}

	@Override
	public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo) {
		return null;
	}

}
