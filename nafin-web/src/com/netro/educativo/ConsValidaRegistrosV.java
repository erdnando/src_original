package com.netro.educativo;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsValidaRegistrosV implements IQueryGeneratorRegExtJS {


//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsValidaRegistrosV.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String intermediario;
	private String fechaCargaIni;
	private String fechaCargaFin;
	private String credito;
	private String matricula;
	private String estudios;
	private String fechaAcreditadoIni;
	private String fechaAcreditadoFin;
	private String anio;
	private String periodo;
	private String modalidad;
	private String fechaDisposicionIni;
	private String fechaDisposicionFin;
	private String no_disposicion;
	private String impDisposicion;
	private String montoTotal;
	private String saldoLinea;
	private String universidad;
	private String estatus;
	private String tipoConsulta;
	private String acuse;
	private String regAceptados;
	private String regRechazados;  
	private String monitor;
	private String modalidaPrim;  
			
	public ConsValidaRegistrosV() { }
	
	  
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	/**
	 * metodo que obtiene las llaves primaria 
	 * @return 
	 */
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		
		qrySentencia.append("SELECT "+  
										" a.ic_registro as IC_REGISTRO  "+
									" FROM ce_contenido_educativo a, "+
										" comcat_universidad u, "+
										" comcat_if i, "+
										" cerel_if_universidad  r "+	
									" WHERE a.ic_universidad = u.ic_universidad  "+
										" and a.ic_if = i.ic_if "+
										" and  u.ic_universidad = r.ic_universidad  "+
										" and i.ic_if = r.ic_if  ");
											
		if(!estatus.equals("")) {	
				qrySentencia.append(" and ic_estatus_cred_educa = ? ");
				conditions.add(estatus);
			}
			
			if(!universidad.equals("")) {	
				qrySentencia.append(" and a.ic_universidad =? ");
				conditions.add(universidad);
			}
			
			if(!intermediario.equals("")) {	
				qrySentencia.append(" and i.ic_if =? ");
				conditions.add(intermediario);
			}
			if(!fechaCargaIni.equals("")  &&  !fechaCargaFin.equals("") ) {	
				qrySentencia.append(" and a.fn_fecha_carga >= TO_DATE(?, 'dd/mm/yyyy') "+
											"and  a.fn_fecha_carga < TO_DATE(?, 'dd/mm/yyyy')+1 ");
				conditions.add(fechaCargaIni);	
				conditions.add(fechaCargaFin);	
			}
		
			if(!credito.equals("")) {	
				qrySentencia.append(" and a.CG_FINANCIAMIENTO  =? ");
				conditions.add(credito);	
			}
			
			if(!matricula.equals("")) {	
				qrySentencia.append(" and a.cg_matricula =? ");
				conditions.add(matricula);	
			}
			if(!estudios.equals("")) {
				qrySentencia.append(" AND a.cg_desc_estudios  LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')  ");
    			conditions.add(estudios);	
			}
		
			if(!fechaAcreditadoIni.equals("")  &&  !fechaAcreditadoFin.equals("") ) {
				qrySentencia.append("  and a.df_ingreso >= TO_DATE(?, 'dd/mm/yyyy') "+
										  " and  a.df_ingreso < TO_DATE(?, 'dd/mm/yyyy')+1 ");
				conditions.add(fechaAcreditadoIni);	
				conditions.add(fechaAcreditadoFin);	
			}
			if(!anio.equals("")) {
				qrySentencia.append(" and a.cg_anio =? ");
				conditions.add(anio);	
			}
			if(!periodo.equals("")) {	
				qrySentencia.append(" and  a.ic_periodo = ?  ");
				conditions.add(periodo);
			}	
			
			if(!modalidad.equals("")) {	
				qrySentencia.append(" AND a.cg_modalidad LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')  ");
				conditions.add(modalidad);	
			}
			if(!fechaDisposicionIni.equals("")  &&  !fechaDisposicionFin.equals("") ) {
				qrySentencia.append(" and a.df_disposicion >= TO_DATE(?, 'dd/mm/yyyy') "+
										  " and  a.df_disposicion < TO_DATE(?, 'dd/mm/yyyy')+1  ");
				conditions.add(fechaDisposicionIni);	
				conditions.add(fechaDisposicionFin);	
			}
			
			if(no_disposicion.equals("1")   || modalidaPrim.equals("1")  ) {	
				qrySentencia.append(" and a.ic_num_disposicion = ? ");
				conditions.add("1");	
				
			}else if(! no_disposicion.equals("1")  &&  monitor.equals("S") ) {	
				qrySentencia.append(" and a.ic_num_disposicion != ? ");
				conditions.add(new Integer(1));	
				
			}else if( !no_disposicion.equals("")  &&  monitor.equals("N")  ) {	
			
				qrySentencia.append(" and a.ic_num_disposicion = ? ");
				conditions.add(new Integer(no_disposicion ));
				
			}			
			if(modalidaPrim.equals("2")) {
				qrySentencia.append(" and a.ic_num_disposicion != 1 ");
			}
			
			
			if(!impDisposicion.equals("")) {	
				qrySentencia.append(" and a.fn_imp_disposicion BETWEEN  ? and  ? ");				
				conditions.add(impDisposicion);	
				conditions.add(impDisposicion);					
			}
			
			
			if(!montoTotal.equals("")) {	
				qrySentencia.append(" and a.FN_FINANCIAMIENTO BETWEEN  ? and  ? ");				
				conditions.add(montoTotal);	
				conditions.add(montoTotal);					
			}
			
			if(!saldoLinea.equals("")) {
				qrySentencia.append("  and a.fn_saldolineas BETWEEN  ? and  ? ");
				conditions.add(saldoLinea);
				conditions.add(saldoLinea);
			}
			
			log.info("regRechazados //////////////////////"+regRechazados);
			log.info("regAceptados  "+regAceptados);
		
					
			if(!regRechazados.equals("") ) {  	
				qrySentencia.append(" and a.ic_registro in ("+regRechazados+")");		  		
			}
			
			if(!regAceptados.equals("")  ) {	
				qrySentencia.append(" and a.ic_registro in ("+regAceptados+")");				
			}
			
			if(!acuse.equals("") ) {	
				qrySentencia.append("  and a.cc_acuse  = ? ");
				conditions.add(acuse);
			}
				
			qrySentencia.append(" order by a.fn_fecha_carga  ");
		
		
		
		log.info("getDocumentQuery(E)");
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	/**
	 * metodo que obtienes la paginaci�n 
	 * @return 
	 * @param pageIds
	 */
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		qrySentencia.append("SELECT "+  
											" a.ic_registro as IC_REGISTRO , "+
											" u.cg_razon_social as UNIVERSIDAD, "+
											" u.cg_nombre_campus as CAMPUS, "+
											" i.cg_razon_social as INTERMEDIARIO, "+
											" i.ic_if  as IC_IF, "+										
											" TO_CHAR(a.fn_fecha_carga,'dd/mm/yyyy hh:mm:ss') as  FECHA_CARGA, "+
											" a.CG_FINANCIAMIENTO as CREDITO , "+
											" a.cg_razon_social as NOMBRE_ACREDITADO , "+
											" a.cg_registro as RFC ,  "+
											" a.cg_matricula as MATRICULA ,  "+
											" a.cg_desc_estudios as ESTUDIOS ,  "+
											" TO_CHAR(a.df_ingreso,'dd/mm/yyyy')   as FECHA_INGRESO ,"+
											" a.cg_anio as ANIO_COLOCACION , "+
											" a.ic_periodo  as PERIODO_EDUCATIVO , "+
											" a.cg_modalidad as MODALIDAD_PROGRAMA , "+
											" TO_CHAR(a.df_disposicion,'dd/mm/yyyy')  as FECHA_UL_DISPOSICION , "+
											" a.ic_num_disposicion as NO_DISPOSICION , "+
											" a.fn_imp_disposicion as IMPORTE_DISPOSICON , "+
											" a.FN_FINANCIAMIENTO as MONTO_TOTAL, "+
											" a.fn_saldolineas  as SALDO_LINEA,  "+										
											" decode(ic_estatus_cred_educa, 3, a.cg_causa_rechazo) AS CAUSA_RECHAZO, "+										
											" 'N' AS RECHAZADOS, "+
											" 'S' AS ACEPTADOS , "+	
											" ic_folio_operacion as FOLIO_OPERACION, "+
											" ic_estatus_cred_educa as ESTATUS,  "+		
											" a.fn_financiamiento FN_FINANCIAMIENTO, "+
											" DECODE ( bo.cg_modalidad, 1, 'Pari Passu ',2, 'Primeras Perdidas')  As MODALIDAD " +
										" FROM ce_contenido_educativo a, "+
											" comcat_universidad u, "+
											" comcat_if i, "+
											" cerel_if_universidad  r,  "+	
											" cecat_bases_opera bo "+
										" WHERE a.ic_universidad = u.ic_universidad  "+
											" and a.ic_if = i.ic_if "+
											" and  u.ic_universidad = r.ic_universidad  "+
											" and i.ic_if = r.ic_if  "+
											" and a.ic_programa = bo.ic_clave  ");
	
			if(!estatus.equals("")) {	
				qrySentencia.append(" and ic_estatus_cred_educa = ? ");
				conditions.add(estatus);
			}
			
			if(!universidad.equals("")) {	
				qrySentencia.append(" and a.ic_universidad =? ");
				conditions.add(universidad);
			}
			
			if(!intermediario.equals("")) {	
				qrySentencia.append(" and i.ic_if =? ");
				conditions.add(intermediario);
			}
			if(!fechaCargaIni.equals("")  &&  !fechaCargaFin.equals("") ) {	
				qrySentencia.append(" and a.fn_fecha_carga >= TO_DATE(?, 'dd/mm/yyyy') "+
											"and  a.fn_fecha_carga < TO_DATE(?, 'dd/mm/yyyy')+1 ");
				conditions.add(fechaCargaIni);	
				conditions.add(fechaCargaFin);	
			}
		
			if(!credito.equals("")) {	
				qrySentencia.append(" and a.CG_FINANCIAMIENTO =? ");
				conditions.add(credito);	
			}
			
			if(!matricula.equals("")) {	
				qrySentencia.append(" and a.cg_matricula =? ");
				conditions.add(matricula);	
			}
			if(!estudios.equals("")) {
				qrySentencia.append(" AND a.cg_desc_estudios  LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')  ");
    			conditions.add(estudios);	
			}
		
			if(!fechaAcreditadoIni.equals("")  &&  !fechaAcreditadoFin.equals("") ) {
				qrySentencia.append("  and a.df_ingreso >= TO_DATE(?, 'dd/mm/yyyy') "+
										  " and  a.df_ingreso < TO_DATE(?, 'dd/mm/yyyy')+1 ");
				conditions.add(fechaAcreditadoIni);	
				conditions.add(fechaAcreditadoFin);	
			}
			if(!anio.equals("")) {
				qrySentencia.append(" and a.cg_anio =? ");
				conditions.add(anio);	
			}
			if(!periodo.equals("")) {	
				qrySentencia.append(" and  a.ic_periodo = ?  ");
				conditions.add(periodo);
			}	
			
			if(!modalidad.equals("")) {	
				qrySentencia.append(" AND a.cg_modalidad LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')  ");
				conditions.add(modalidad);	
			}
			if(!fechaDisposicionIni.equals("")  &&  !fechaDisposicionFin.equals("") ) {
				qrySentencia.append(" and a.df_disposicion >= TO_DATE(?, 'dd/mm/yyyy') "+
										  " and  a.df_disposicion < TO_DATE(?, 'dd/mm/yyyy')+1  ");
				conditions.add(fechaDisposicionIni);	
				conditions.add(fechaDisposicionFin);	
			}
			
			if(no_disposicion.equals("1")   || modalidaPrim.equals("1")  ) {	
				qrySentencia.append(" and a.ic_num_disposicion = ? ");
				conditions.add("1");	
				
			}else if(! no_disposicion.equals("1")  &&  monitor.equals("S") ) {	
				qrySentencia.append(" and a.ic_num_disposicion != ? ");
				conditions.add(new Integer(1));	
				
			}else if( !no_disposicion.equals("")  &&  monitor.equals("N")  ) {	
			
				qrySentencia.append(" and a.ic_num_disposicion = ? ");
				conditions.add(new Integer(no_disposicion ));
				
			}			
			if(modalidaPrim.equals("2")) {
				qrySentencia.append(" and a.ic_num_disposicion != 1 ");
			}
			
			
			if(!impDisposicion.equals("")) {	
				qrySentencia.append(" and a.fn_imp_disposicion BETWEEN  ? and  ? ");				
				conditions.add(impDisposicion);	
				conditions.add(impDisposicion);					
			}
						
			if(!montoTotal.equals("")) {	
				qrySentencia.append(" and a.FN_FINANCIAMIENTO BETWEEN  ? and  ? ");				
				conditions.add(montoTotal);	
				conditions.add(montoTotal);					
			}
			
			if(!saldoLinea.equals("")) {
				qrySentencia.append("  and a.fn_saldolineas BETWEEN  ? and  ? ");
				conditions.add(saldoLinea);
				conditions.add(saldoLinea);
			}
			
			if(!acuse.equals("") ) {	
				qrySentencia.append("  and a.cc_acuse  =  ?");
				conditions.add(acuse);
			}
			
			qrySentencia.append(" AND (");
			
			for (int i = 0; i < pageIds.size(); i++) { 
				List lItem = (ArrayList)pageIds.get(i);
				
				if(i > 0){qrySentencia.append("  OR  ");}
				
				qrySentencia.append("a.ic_registro  = ?");
				conditions.add(new Long(lItem.get(0).toString()));
			}
			
			qrySentencia.append(" ) ");
			qrySentencia.append(" order by a.fn_fecha_carga  ");
			
						
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		System.out.println("tipoConsulta   "+tipoConsulta);
		
		if(tipoConsulta.equals("ArchivoAcuse")) {
		
			qrySentencia.append( " SELECT  "+
				" cg_razon_social as NOMBRE_IF, "+
				" TO_CHAR(df_operacion,'dd/mm/yyyy, hh:mm')  as FECHAHORA ,  "+
				" in_registros as N0REGISTROSV, "+
				" in_registros_acep as ACEPTADOS, "+
				" in_registros_rech as RECHAZADOS, "+
				" ic_folio_operacion as FOLIO,"+
				" ic_usuario  || ' -' || cg_nombre_usuario as NOMBRE_USUARIO "+
				" FROM ce_acuse_educativo_val   "+
				" where cc_acuse =  ? ");
				conditions.add(acuse);
				
		
		}else if(tipoConsulta.equals("ConsultarMonitor")) {
		
			qrySentencia.append( " SELECT  "+			 
										" i.ic_if as IC_IF, "+
										 " i.cg_razon_social as NOMBRE_IF, "+										 
										 " CASE WHEN bo.cg_modalidad in(1,2)  and a.ic_num_disposicion =1  THEN 'Primer Perdidas- 1ra Disposici�n y/o Pari Passu' ELSE 'Primer Perdidas - Disposiciones correlativas a la 1ra' END as MODALIDAD, "+
    									 " COUNT(*)  AS NO_REGISTROS, "+
										 " SUM(a.fn_financiamiento) MONTO_ENVIADO  "+
										 " FROM ce_contenido_educativo a,  "+
										 " comcat_universidad u,  "+
										 " comcat_if i, "+
										 " cerel_if_universidad  r,  "+
										 "  cecat_bases_opera bo "+
										 " WHERE a.ic_universidad = u.ic_universidad  "+
										 " and  u.ic_universidad = r.ic_universidad  "+
										 "  and a.ic_if = i.ic_if  "+
										 " and i.ic_if = r.ic_if " +
										 " and a.ic_programa = bo.ic_clave "+
										 " and ic_estatus_cred_educa = ? ");
										conditions.add("1");
			  
			    
			if(!universidad.equals("")) {	
				qrySentencia.append(" and a.ic_universidad =? ");
				conditions.add(universidad);
			}
			qrySentencia.append(" GROUP BY i.ic_if, i.cg_razon_social,  CASE WHEN bo.cg_modalidad in(1,2)  and a.ic_num_disposicion =1  THEN 'Primer Perdidas- 1ra Disposici�n y/o Pari Passu' ELSE 'Primer Perdidas - Disposiciones correlativas a la 1ra' END     ");
		
		
		}else  {
 
 
			qrySentencia.append("SELECT "+  
											" a.ic_registro as IC_REGISTRO , "+
											" u.cg_razon_social as UNIVERSIDAD, "+
											" u.cg_nombre_campus as CAMPUS, "+
											" i.cg_razon_social as INTERMEDIARIO, "+
											" i.ic_if  as IC_IF, "+										
											" TO_CHAR(a.fn_fecha_carga,'dd/mm/yyyy hh:mm:ss') as  FECHA_CARGA, "+
											" a.CG_FINANCIAMIENTO as CREDITO , "+
											" a.cg_razon_social as NOMBRE_ACREDITADO , "+
											" a.cg_registro as RFC ,  "+
											" a.cg_matricula as MATRICULA ,  "+
											" a.cg_desc_estudios as ESTUDIOS ,  "+
											" TO_CHAR(a.df_ingreso,'dd/mm/yyyy')   as FECHA_INGRESO ,"+
											" a.cg_anio as ANIO_COLOCACION , "+
											" a.ic_periodo  as PERIODO_EDUCATIVO , "+
											" a.cg_modalidad as MODALIDAD_PROGRAMA , "+
											" TO_CHAR(a.df_disposicion,'dd/mm/yyyy')  as FECHA_UL_DISPOSICION , "+
											" a.ic_num_disposicion as NO_DISPOSICION , "+
											" a.fn_imp_disposicion as IMPORTE_DISPOSICON , "+
											" a.FN_FINANCIAMIENTO as MONTO_TOTAL, "+
											" a.fn_saldolineas  as SALDO_LINEA,  "+										
											" decode(ic_estatus_cred_educa, 3, a.cg_causa_rechazo) AS CAUSA_RECHAZO, "+										
											" 'N' AS RECHAZADOS, "+
											" 'S' AS ACEPTADOS , "+	
											" ic_folio_operacion as FOLIO_OPERACION, "+
											" ic_estatus_cred_educa as ESTATUS,  "+
											" a.fn_financiamiento FN_FINANCIAMIENTO , "+
											" DECODE ( bo.cg_modalidad, 1, 'Pari Passu ',2, 'Primeras Perdidas')  As MODALIDAD "+
										" FROM ce_contenido_educativo a, "+
											" comcat_universidad u, "+
											" comcat_if i, "+
											" cerel_if_universidad  r,  "+	
											" cecat_bases_opera bo "+
										" WHERE a.ic_universidad = u.ic_universidad  "+
											" and a.ic_if = i.ic_if "+
											" and  u.ic_universidad = r.ic_universidad  "+
											" and i.ic_if = r.ic_if  "+
											" and a.ic_programa = bo.ic_clave  ");
	
			if(!estatus.equals("")) {	
				qrySentencia.append(" and ic_estatus_cred_educa = ? ");
				conditions.add(estatus);
			}
			
			if(!universidad.equals("")) {	
				qrySentencia.append(" and a.ic_universidad =? ");
				conditions.add(universidad);
			}
			
			if(!intermediario.equals("")) {	
				qrySentencia.append(" and i.ic_if =? ");
				conditions.add(intermediario);
			}
			if(!fechaCargaIni.equals("")  &&  !fechaCargaFin.equals("") ) {	
				qrySentencia.append(" and a.fn_fecha_carga >= TO_DATE(?, 'dd/mm/yyyy') "+
											"and  a.fn_fecha_carga < TO_DATE(?, 'dd/mm/yyyy')+1 ");
				conditions.add(fechaCargaIni);	
				conditions.add(fechaCargaFin);	
			}
		
			if(!credito.equals("")) {	
				qrySentencia.append(" and a.CG_FINANCIAMIENTO =? ");
				conditions.add(credito);	
			}
			
			if(!matricula.equals("")) {	
				qrySentencia.append(" and a.cg_matricula =? ");
				conditions.add(matricula);	
			}
			if(!estudios.equals("")) {
				qrySentencia.append(" AND a.cg_desc_estudios  LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')  ");
    			conditions.add(estudios);	
			}
		
			if(!fechaAcreditadoIni.equals("")  &&  !fechaAcreditadoFin.equals("") ) {
				qrySentencia.append("  and a.df_ingreso >= TO_DATE(?, 'dd/mm/yyyy') "+
										  " and  a.df_ingreso < TO_DATE(?, 'dd/mm/yyyy')+1 ");
				conditions.add(fechaAcreditadoIni);	
				conditions.add(fechaAcreditadoFin);	
			}
			if(!anio.equals("")) {
				qrySentencia.append(" and a.cg_anio =? ");
				conditions.add(anio);	
			}
			if(!periodo.equals("")) {	
				qrySentencia.append(" and  a.ic_periodo = ?  ");
				conditions.add(periodo);
			}	
			
			if(!modalidad.equals("")) {	
				qrySentencia.append(" AND a.cg_modalidad LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')  ");
				conditions.add(modalidad);	
			}
			if(!fechaDisposicionIni.equals("")  &&  !fechaDisposicionFin.equals("") ) {
				qrySentencia.append(" and a.df_disposicion >= TO_DATE(?, 'dd/mm/yyyy') "+
										  " and  a.df_disposicion < TO_DATE(?, 'dd/mm/yyyy')+1  ");
				conditions.add(fechaDisposicionIni);	
				conditions.add(fechaDisposicionFin);	
			}
						
		   if(no_disposicion.equals("1")   || modalidaPrim.equals("1")  ) {	
				qrySentencia.append(" and a.ic_num_disposicion = ? ");
				conditions.add("1");	
				
			}else if(! no_disposicion.equals("1")  &&  monitor.equals("S") ) {	
				qrySentencia.append(" and a.ic_num_disposicion != ? ");
				conditions.add(new Integer(1));	
				
			}else if( !no_disposicion.equals("")  &&  monitor.equals("N")  ) {	
			
				qrySentencia.append(" and a.ic_num_disposicion = ? ");
				conditions.add(new Integer(no_disposicion ));
				
			}			
			if(modalidaPrim.equals("2")) {
				qrySentencia.append(" and a.ic_num_disposicion != 1 ");
			}
			
			
			if(!impDisposicion.equals("")) {	
				qrySentencia.append(" and a.fn_imp_disposicion BETWEEN  ? and  ? ");				
				conditions.add(impDisposicion);	
				conditions.add(impDisposicion);					
			}
			
			
			if(!montoTotal.equals("")) {	
				qrySentencia.append(" and a.FN_FINANCIAMIENTO BETWEEN  ? and  ? ");				
				conditions.add(montoTotal);	
				conditions.add(montoTotal);					
			}
			
			if(!saldoLinea.equals("")) {
				qrySentencia.append("  and a.fn_saldolineas BETWEEN  ? and  ? ");
				conditions.add(saldoLinea);
				conditions.add(saldoLinea);
			}
			
						
			if(!regRechazados.equals("")) {	
				qrySentencia.append(" and a.ic_registro in ("+regRechazados+")");				
			}
			
			if(!regAceptados.equals("")) {	
				qrySentencia.append(" and a.ic_registro in ("+regAceptados+")");				
			}
			
			
			if(!acuse.equals("") ) {	
				qrySentencia.append("  and a.cc_acuse  = ? ");
				conditions.add(acuse);
			}
			
			qrySentencia.append(" order by a.fn_fecha_carga  ");
			
			
		}
	
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		int i=0;
		try {
		
			if(!tipoConsulta.equals("ArchivoAcuse")) {
		 
				while (rs.next())	{					
					
					String universisad = (rs.getString("UNIVERSIDAD") == null) ? "" : rs.getString("UNIVERSIDAD");
					String campus = (rs.getString("CAMPUS") == null) ? "" : rs.getString("CAMPUS");
					String nombreIF = (rs.getString("INTERMEDIARIO") == null) ? "" : rs.getString("INTERMEDIARIO");					
					String fecha_carga = (rs.getString("FECHA_CARGA") == null) ? "" : rs.getString("FECHA_CARGA");
					String credito = (rs.getString("CREDITO") == null) ? "" : rs.getString("CREDITO");
					String nombreAcreditado = (rs.getString("NOMBRE_ACREDITADO") == null) ? "" : rs.getString("NOMBRE_ACREDITADO");
					String rfc = (rs.getString("RFC") == null) ? "" : rs.getString("RFC");
					String matricula = (rs.getString("MATRICULA") == null) ? "" : rs.getString("MATRICULA");
					String estudios = (rs.getString("ESTUDIOS") == null) ? "" : rs.getString("ESTUDIOS");
					String fecha_ingreso = (rs.getString("FECHA_INGRESO") == null) ? "" : rs.getString("FECHA_INGRESO");
					String anio_colocacion = (rs.getString("ANIO_COLOCACION") == null) ? "" : rs.getString("ANIO_COLOCACION");
					String periodo_educativo = (rs.getString("PERIODO_EDUCATIVO") == null) ? "" : rs.getString("PERIODO_EDUCATIVO");
					String modalidad_programa = (rs.getString("MODALIDAD_PROGRAMA") == null) ? "" : rs.getString("MODALIDAD_PROGRAMA");
					String fecha_ul_disposicion = (rs.getString("FECHA_UL_DISPOSICION") == null) ? "" : rs.getString("FECHA_UL_DISPOSICION");
					String no_disposicion = (rs.getString("NO_DISPOSICION") == null) ? "" : rs.getString("NO_DISPOSICION");
					String importe_disposicion = (rs.getString("IMPORTE_DISPOSICON") == null) ? "" : rs.getString("IMPORTE_DISPOSICON");
					String monto_total = (rs.getString("MONTO_TOTAL") == null) ? "" : rs.getString("MONTO_TOTAL");
					String saldo_linea = (rs.getString("SALDO_LINEA") == null) ? "" : rs.getString("SALDO_LINEA");
					String causa_rechazo = (rs.getString("CAUSA_RECHAZO") == null) ? "" : rs.getString("CAUSA_RECHAZO");
					String folio_operacion = (rs.getString("FOLIO_OPERACION") == null) ? "" : rs.getString("FOLIO_OPERACION");
					String estatusC = (rs.getString("ESTATUS") == null) ? "" : rs.getString("ESTATUS");
					String modalidad = (rs.getString("MODALIDAD") == null) ? "" : rs.getString("MODALIDAD");
					
					
					if(i==0){
						contenidoArchivo.append("Universidad: "+universisad.replaceAll(",","")+"\n"); 
						contenidoArchivo.append("Campus: "+campus.replaceAll(",","")+"\n"); 
						contenidoArchivo.append("Intermediario Financiero: "+nombreIF.replaceAll(",","")+"\n");
						contenidoArchivo.append("  "+"\n");
						
						if(estatusC.equals("2")) {
							contenidoArchivo.append("Registros Validos - Folio de Operaci�n : "+folio_operacion.replaceAll(",","")+"\n"); 
						}
						if(estatusC.equals("3")) {
							contenidoArchivo.append("Registros Rechazados \n"); 
						}
						
						
						contenidoArchivo.append("Fecha y Hora de Carga,Cr�dito,Nombre del acreditado,RFC,Matricula, Estudios, Fecha de Ingreso, A�o de colocaci�n, Periodo Educativo, Modalidad Programa, ");
						contenidoArchivo.append("Fecha �ltima disposici�n,No. de disposici�n, Importe Disposici�n, Monto Total, Saldo de L�nea, Modalidad,  ");
						if(!causa_rechazo.equals("")) {
							contenidoArchivo.append(" Causas de rechazo ");
						}
						contenidoArchivo.append(" \n");
						
					}
					
					contenidoArchivo.append(fecha_carga.replaceAll(",","")+",");
					contenidoArchivo.append(credito.replaceAll(",","")+",");
					contenidoArchivo.append(nombreAcreditado.replaceAll(",","")+",");
					contenidoArchivo.append(rfc.replaceAll(",","")+",");
					contenidoArchivo.append(matricula.replaceAll(",","")+",");
					contenidoArchivo.append(estudios.replaceAll(",","")+",");
					contenidoArchivo.append(fecha_ingreso.replaceAll(",","")+",");
					contenidoArchivo.append(anio_colocacion.replaceAll(",","")+",");
					contenidoArchivo.append(periodo_educativo.replaceAll(",","")+",");
					contenidoArchivo.append(modalidad_programa.replaceAll(",","")+",");
					contenidoArchivo.append(fecha_ul_disposicion.replaceAll(",","")+",");
					contenidoArchivo.append(no_disposicion.replaceAll(",","")+",");
					contenidoArchivo.append(importe_disposicion.replaceAll(",","")+",");
					contenidoArchivo.append(monto_total.replaceAll(",","")+",");
					contenidoArchivo.append(saldo_linea.replaceAll(",","")+",");
					contenidoArchivo.append(modalidad.replaceAll(",","")+",");
					if(!causa_rechazo.equals("")) {
						contenidoArchivo.append(causa_rechazo.replaceAll(",","")+",");
					}	
					contenidoArchivo.append(" \n");
					i++;
				}
			
				if(tipo.equals("CSV") ) {
					creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
					nombreArchivo = creaArchivo.getNombre();
				}
				
			}	
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		
		String nombreArchivo = "";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		int i=0;
			
		try {
		
			if(!tipoConsulta.equals("ArchivoAcuse")) {
		 
				while (rs.next())	{					
					
					String universisad = (rs.getString("UNIVERSIDAD") == null) ? "" : rs.getString("UNIVERSIDAD");
					String campus = (rs.getString("CAMPUS") == null) ? "" : rs.getString("CAMPUS");
					String nombreIF = (rs.getString("INTERMEDIARIO") == null) ? "" : rs.getString("INTERMEDIARIO");					
					String fecha_carga = (rs.getString("FECHA_CARGA") == null) ? "" : rs.getString("FECHA_CARGA");
					String credito = (rs.getString("CREDITO") == null) ? "" : rs.getString("CREDITO");
					String nombreAcreditado = (rs.getString("NOMBRE_ACREDITADO") == null) ? "" : rs.getString("NOMBRE_ACREDITADO");
					String rfc = (rs.getString("RFC") == null) ? "" : rs.getString("RFC");
					String matricula = (rs.getString("MATRICULA") == null) ? "" : rs.getString("MATRICULA");
					String estudios = (rs.getString("ESTUDIOS") == null) ? "" : rs.getString("ESTUDIOS");
					String fecha_ingreso = (rs.getString("FECHA_INGRESO") == null) ? "" : rs.getString("FECHA_INGRESO");
					String anio_colocacion = (rs.getString("ANIO_COLOCACION") == null) ? "" : rs.getString("ANIO_COLOCACION");
					String periodo_educativo = (rs.getString("PERIODO_EDUCATIVO") == null) ? "" : rs.getString("PERIODO_EDUCATIVO");
					String modalidad_programa = (rs.getString("MODALIDAD_PROGRAMA") == null) ? "" : rs.getString("MODALIDAD_PROGRAMA");
					String fecha_ul_disposicion = (rs.getString("FECHA_UL_DISPOSICION") == null) ? "" : rs.getString("FECHA_UL_DISPOSICION");
					String no_disposicion = (rs.getString("NO_DISPOSICION") == null) ? "" : rs.getString("NO_DISPOSICION");
					String importe_disposicion = (rs.getString("IMPORTE_DISPOSICON") == null) ? "" : rs.getString("IMPORTE_DISPOSICON");
					String monto_total = (rs.getString("MONTO_TOTAL") == null) ? "" : rs.getString("MONTO_TOTAL");
					String saldo_linea = (rs.getString("SALDO_LINEA") == null) ? "" : rs.getString("SALDO_LINEA");
					String causa_rechazo = (rs.getString("CAUSA_RECHAZO") == null) ? "" : rs.getString("CAUSA_RECHAZO");
					String folio_operacion = (rs.getString("FOLIO_OPERACION") == null) ? "" : rs.getString("FOLIO_OPERACION");
					String estatusC = (rs.getString("ESTATUS") == null) ? "" : rs.getString("ESTATUS");
					String modalidad = (rs.getString("MODALIDAD") == null) ? "" : rs.getString("MODALIDAD");					
					
					if(i==0){
						contenidoArchivo.append("Universidad: "+universisad.replaceAll(",","")+"\n"); 
						contenidoArchivo.append("Campus: "+campus.replaceAll(",","")+"\n"); 
						contenidoArchivo.append("Intermediario Financiero: "+nombreIF.replaceAll(",","")+"\n");
						contenidoArchivo.append("  "+"\n");
						
						if(estatusC.equals("2")) {
							contenidoArchivo.append("Registros Validos - Folio de Operaci�n : "+folio_operacion.replaceAll(",","")+"\n"); 
						}
						if(estatusC.equals("3")) {
							contenidoArchivo.append("Registros Rechazados \n"); 
						}
						
						
						contenidoArchivo.append("Fecha y Hora de Carga,Cr�dito,Nombre del acreditado,RFC,Matricula, Estudios, Fecha de Ingreso, A�o de colocaci�n, Periodo Educativo, Modalidad Programa, ");
						contenidoArchivo.append("Fecha �ltima disposici�n,No. de disposici�n, Importe Disposici�n, Monto Total, Saldo de L�nea, Modalidad , ");
						if(!causa_rechazo.equals("")) {
							contenidoArchivo.append(" Causas de rechazo ");
						}
						contenidoArchivo.append(" \n");
						
					}
					
					contenidoArchivo.append(fecha_carga.replaceAll(",","")+",");
					contenidoArchivo.append(credito.replaceAll(",","")+",");
					contenidoArchivo.append(nombreAcreditado.replaceAll(",","")+",");
					contenidoArchivo.append(rfc.replaceAll(",","")+",");
					contenidoArchivo.append(matricula.replaceAll(",","")+",");
					contenidoArchivo.append(estudios.replaceAll(",","")+",");
					contenidoArchivo.append(fecha_ingreso.replaceAll(",","")+",");
					contenidoArchivo.append(anio_colocacion.replaceAll(",","")+",");
					contenidoArchivo.append(periodo_educativo.replaceAll(",","")+",");
					contenidoArchivo.append(modalidad_programa.replaceAll(",","")+",");
					contenidoArchivo.append(fecha_ul_disposicion.replaceAll(",","")+",");
					contenidoArchivo.append(no_disposicion.replaceAll(",","")+",");
					contenidoArchivo.append(importe_disposicion.replaceAll(",","")+",");
					contenidoArchivo.append(monto_total.replaceAll(",","")+",");
					contenidoArchivo.append(saldo_linea.replaceAll(",","")+",");
					contenidoArchivo.append(modalidad.replaceAll(",","")+",");
					if(!causa_rechazo.equals("")) {
						contenidoArchivo.append(causa_rechazo.replaceAll(",","")+",");
					}	
					contenidoArchivo.append(" \n");
					i++;
				}
			
				if(tipo.equals("CSV") ) {
					creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
					nombreArchivo = creaArchivo.getNombre();
				}
			
			}else if(tipoConsulta.equals("ArchivoAcuse")) {
			
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				pdfDoc.addText("Acuse Validaci�n de registros de Cr�dito Educativo ","formasB",ComunesPDF.CENTER);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				
				String texto = "Al transmitir los registros electr�nicamente, usted est� confirmando la validez de la informaci�n \n y acepta los t�rminos relacionados  con la operaci�n del Programa Nacional de Financiamiento a la Educaci�n Superior.";
				while (rs.next())	{					
					
					String  nombreIF= (rs.getString("NOMBRE_IF") == null) ? "" : rs.getString("NOMBRE_IF");
					String fechHora = (rs.getString("FECHAHORA") == null) ? "" : rs.getString("FECHAHORA");
					String registosT = (rs.getString("N0REGISTROSV") == null) ? "" : rs.getString("N0REGISTROSV");
					String aceptados = (rs.getString("ACEPTADOS") == null) ? "" : rs.getString("ACEPTADOS");
					String rechazados = (rs.getString("RECHAZADOS") == null) ? "" : rs.getString("RECHAZADOS");
					String nombreUsuario = (rs.getString("NOMBRE_USUARIO") == null) ? "" : rs.getString("NOMBRE_USUARIO");
					String folio = (rs.getString("FOLIO") == null) ? "" : rs.getString("FOLIO");
						
					pdfDoc.setTable(2, 50);	
					pdfDoc.setCell("Cifras Control","celda01",ComunesPDF.CENTER, 2);
					pdfDoc.setCell("Tipo de Operaci�n","celda01",ComunesPDF.LEFT);
					pdfDoc.setCell("Cr�dito Educativo","formas",ComunesPDF.LEFT);
					pdfDoc.setCell("Intermediario Financiero","celda01",ComunesPDF.LEFT);
					pdfDoc.setCell(nombreIF,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell("Fecha y Hora de Validaci�n","celda01",ComunesPDF.LEFT);
					pdfDoc.setCell(fechHora,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell("Usuario","celda01",ComunesPDF.LEFT);
					pdfDoc.setCell(nombreUsuario,"formas",ComunesPDF.LEFT);
					pdfDoc.setCell("N�mero de Registros Validados","celda01",ComunesPDF.LEFT);
					pdfDoc.setCell(registosT,"formas",ComunesPDF.LEFT);
					pdfDoc.addTable();	
						
				
					pdfDoc.setTable(3, 50);			
					pdfDoc.setCell("No Registros","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Folio Operaci�n","celda01",ComunesPDF.CENTER);					
					pdfDoc.setCell(aceptados,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("REGISTRO VALIDO-UNIVERSIDAD","formas",ComunesPDF.LEFT);
					pdfDoc.setCell(folio,"formas",ComunesPDF.CENTER);					
					pdfDoc.setCell(rechazados,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("REGISTRO RECHAZADO-UNIVERSIDAD","formas",ComunesPDF.LEFT);
					pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);					
					pdfDoc.addTable();	
					
					pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
					pdfDoc.addText(texto,"formas",ComunesPDF.CENTER);
					pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);					
					
				}
			
				pdfDoc.endDocument();			
				
			}
			
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
		
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getIntermediario() {
		return intermediario;
	}

	public void setIntermediario(String intermediario) {
		this.intermediario = intermediario;
	}

	public String getFechaCargaIni() {
		return fechaCargaIni;
	}

	public void setFechaCargaIni(String fechaCargaIni) {
		this.fechaCargaIni = fechaCargaIni;
	}

	public String getFechaCargaFin() {
		return fechaCargaFin;
	}

	public void setFechaCargaFin(String fechaCargaFin) {
		this.fechaCargaFin = fechaCargaFin;
	}

	public String getCredito() {
		return credito;
	}

	public void setCredito(String credito) {
		this.credito = credito;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getEstudios() {
		return estudios;
	}

	public void setEstudios(String estudios) {
		this.estudios = estudios;
	}

	public String getFechaAcreditadoIni() {
		return fechaAcreditadoIni;
	}

	public void setFechaAcreditadoIni(String fechaAcreditadoIni) {
		this.fechaAcreditadoIni = fechaAcreditadoIni;
	}

	public String getFechaAcreditadoFin() {
		return fechaAcreditadoFin;
	}

	public void setFechaAcreditadoFin(String fechaAcreditadoFin) {
		this.fechaAcreditadoFin = fechaAcreditadoFin;
	}

	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public String getModalidad() {
		return modalidad;
	}

	public void setModalidad(String modalidad) {
		this.modalidad = modalidad;
	}

	public String getFechaDisposicionIni() {
		return fechaDisposicionIni;
	}

	public void setFechaDisposicionIni(String fechaDisposicionIni) {
		this.fechaDisposicionIni = fechaDisposicionIni;
	}

	public String getFechaDisposicionFin() {
		return fechaDisposicionFin;
	}

	public void setFechaDisposicionFin(String fechaDisposicionFin) {
		this.fechaDisposicionFin = fechaDisposicionFin;
	}

	public String getNo_disposicion() {
		return no_disposicion;
	}

	public void setNo_disposicion(String no_disposicion) {
		this.no_disposicion = no_disposicion;
	}

	public String getImpDisposicion() {
		return impDisposicion;
	}

	public void setImpDisposicion(String impDisposicion) {
		this.impDisposicion = impDisposicion;
	}

	public String getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(String montoTotal) {
		this.montoTotal = montoTotal;
	}

	public String getSaldoLinea() {
		return saldoLinea;
	}

	public void setSaldoLinea(String saldoLinea) {
		this.saldoLinea = saldoLinea;
	}

	public String getUniversidad() {
		return universidad;
	}

	public void setUniversidad(String universidad) {
		this.universidad = universidad;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getTipoConsulta() {
		return tipoConsulta;
	}

	public void setTipoConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}

	public String getAcuse() {
		return acuse;
	}

	public void setAcuse(String acuse) {
		this.acuse = acuse;
	}

	public String getRegAceptados() {
		return regAceptados;
	}

	public void setRegAceptados(String regAceptados) {
		this.regAceptados = regAceptados;
	}

	public String getRegRechazados() {
		return regRechazados;
	}

	public void setRegRechazados(String regRechazados) {
		this.regRechazados = regRechazados;
	}

	public String getMonitor() {
		return monitor;
	}

	public void setMonitor(String monitor) {
		this.monitor = monitor;
	}

	public String getModalidaPrim() {
		return modalidaPrim;
	}

	public void setModalidaPrim(String modalidaPrim) {
		this.modalidaPrim = modalidaPrim;
	}

		

}