package com.netro.educativo;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsultaRegistroIndividual implements IQueryGeneratorRegExtJS {
private final static Log log = ServiceLocator.getInstance().getLog(ConsultaRegistros.class);
	private String paginaOffset;
	private String paginaNo;
	private String folio;
	private String estatusReg;
	private boolean calculaMonto = false;
	StringBuffer qrySentencia = new StringBuffer("");
	private List 		conditions;
	public ConsultaRegistroIndividual() {
	}
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();

		conditions 		= new ArrayList();

		qrySentencia.append(
			"SELECT COUNT(1) AS registros, "+
			"		  SUM(cce.fn_imp_disposicion) AS TOTAL_IMPORTE,"+
			"       SUM(cce.fn_financiamiento) AS TOTAL_MONTO,"+
			"       SUM(cce.fn_saldolineas) AS TOTAL_SALDO,"
		);

		if(this.calculaMonto){

			qrySentencia.append(
				"       SUM(ROUND(CASE"+
				"              WHEN cbo.cg_modalidad = 1"+
				"                 THEN (  (cce.fn_financiamiento * cbo.fg_porcentaje_aportacion)/ 100)"+
				"              WHEN cbo.cg_modalidad = 2"+
				"                 THEN (  (cce.fn_imp_disposicion * cbo.fg_porcentaje_aportacion)/ 100)"+
				"              ELSE 0"+
				"            END,2)) AS TOTAL_CONTRA,"
			);
		}else{
			qrySentencia.append("	SUM(NVL(cce.fn_monto_garantia,0)) AS	TOTAL_CONTRA,");
		}

		qrySentencia.append(
			" 'ConsultaRegistrosIndividual::getAggregateCalculationQuery' AS pantalla "+
			" FROM "+
			"	ce_contenido_educativo cce, cecat_bases_opera cbo "+
			" WHERE " + 
			" cce.ic_programa=cbo.ic_clave	"
		);

		if(!estatusReg.equals("")){
			qrySentencia.append(" AND cce.cg_aceptado_siag = ? ");
			conditions.add(estatusReg);
		}

		if(!folio.equals("")){
			qrySentencia.append(" AND cce.ic_folio_operacion = ? ");
			conditions.add(folio);
		}
		
		log.info("qrySentencia  "+qrySentencia.toString());
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();
	}

	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		qrySentencia.append("SELECT cce.ic_registro "+
								"	FROM ce_contenido_educativo cce,cecat_bases_opera cbo"+
								"	WHERE "+
								"		cce.ic_programa=cbo.ic_clave ");
		if(!estatusReg.equals("")){
			qrySentencia.append(" AND cce.cg_aceptado_siag = ? ");
			conditions.add(estatusReg);
		}
		if(!folio.equals("")){
			qrySentencia.append(" AND cce.ic_folio_operacion = ? ");
			conditions.add(folio);
		}
	
		log.info("qrySentencia  "+qrySentencia.toString());
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		qrySentencia.append("SELECT cce.cg_financiamiento AS credi,"+
			"cce.cg_razon_social AS nombre,	cce.cg_matricula AS matricula,"+
			" cce.cg_registro AS rfc,	cce.cg_desc_estudios AS estudios,	TO_CHAR(cce.df_ingreso, 'dd/mm/yyyy') AS fecha_ingreso,"+
			"cce.cg_anio AS anio,	cce.ic_periodo AS periodo,	cce.cg_modalidad AS modalidad, TO_CHAR(cce.df_disposicion, 'dd/mm/yyyy') AS fecha_dis,"+
			"cce.ic_num_disposicion AS num_dis,	cce.fn_imp_disposicion AS importe,	cce.fn_financiamiento AS monto_total,"+
			" cce.fn_saldolineas AS saldolineas,	cbo.fg_porcentaje_aportacion,"+
			"  DECODE(cbo.cg_modalidad,1,'Pari Pasu', 2,'Primeras Perdidas') AS modalidad_bases_oper,"
		);
		if(this.calculaMonto){

			qrySentencia.append(
				"ROUND(CASE "+
            "  WHEN cbo.cg_modalidad = 1 "+
            "     THEN (  (cce.fn_financiamiento * cbo.fg_porcentaje_aportacion)/ 100) "+
            "  WHEN cbo.cg_modalidad = 2 "+
            "     THEN (  (cce.fn_imp_disposicion * cbo.fg_porcentaje_aportacion)/ 100) "+
            "  ELSE 0 "+
            "END,2) AS fn_monto_garantia	"
			);
		}else{
			qrySentencia.append("	cce.fn_monto_garantia	");
		}
		
		qrySentencia.append(
			"FROM "+
			"	ce_contenido_educativo cce, cecat_bases_opera cbo "+
			"WHERE " + 
			" cce.ic_programa=cbo.ic_clave	"
		);

		for(int i=0;i<pageIds.size();i++) {
				List lItem = (ArrayList)pageIds.get(i);
				if(i==0) {
					qrySentencia.append("  AND  ( "); 
				} else {
					qrySentencia.append("  OR  "); 
				}
				qrySentencia.append(" ( cce.ic_registro = ? ) "); 
				conditions.add(new Long(lItem.get(0).toString()));
		}//for(int i=0;i<ids.size();i++)
					qrySentencia.append("  ) "); 
		
		log.info("qrySentencia  "+qrySentencia.toString());
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}

	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		qrySentencia.append("SELECT cce.cg_financiamiento AS credi,"+
			"cce.cg_razon_social AS nombre,	cce.cg_matricula AS matricula,"+
			" cce.cg_registro AS rfc,	cce.cg_desc_estudios AS estudios,	TO_CHAR(cce.df_ingreso, 'dd/mm/yyyy') AS fecha_ingreso,"+
			"cce.cg_anio AS anio,	cce.ic_periodo AS periodo,	cce.cg_modalidad AS modalidad, TO_CHAR(cce.df_disposicion, 'dd/mm/yyyy') AS fecha_dis,"+
			"cce.ic_num_disposicion AS num_dis,	cce.fn_imp_disposicion AS importe,	cce.fn_financiamiento AS monto_total,"+
			" cce.fn_saldolineas AS saldolineas,	cbo.fg_porcentaje_aportacion,"+
			"  DECODE(cbo.cg_modalidad,1,'Pari Pasu', 2,'Primeras Perdidas') AS modalidad_bases_oper,"
		);
		if(this.calculaMonto){

			qrySentencia.append(
				"ROUND(CASE "+
            "  WHEN cbo.cg_modalidad = 1 "+
            "     THEN (  (cce.fn_financiamiento * cbo.fg_porcentaje_aportacion)/ 100) "+
            "  WHEN cbo.cg_modalidad = 2 "+
            "     THEN (  (cce.fn_imp_disposicion * cbo.fg_porcentaje_aportacion)/ 100) "+
            "  ELSE 0 "+
            "END,2) AS fn_monto_garantia	"
			);
		}else{
			qrySentencia.append("	cce.fn_monto_garantia	");
		}
		
		qrySentencia.append(
			"FROM "+
			"	ce_contenido_educativo cce, cecat_bases_opera cbo "+
			"WHERE " + 
			" cce.ic_programa=cbo.ic_clave	"
		);

		if(!estatusReg.equals("")){
			qrySentencia.append(" AND cce.cg_aceptado_siag = ? ");
			conditions.add(estatusReg);
		}
		if(!folio.equals("")){
			qrySentencia.append(" AND cce.ic_folio_operacion = ? ");
			conditions.add(folio);
		}
	
		log.info("qrySentencia  "+qrySentencia.toString());
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
	
		try {
	
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return "";
					
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		String linea = "";
		String nombreArchivo = "";
		String espacio = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		try {
		 if(tipo.equals("CSV")) {
		 if(estatusReg.equals("S"))
		  linea = "Cr�dito,Nombre de Acreditado,RFC,Matric�la,Estudios,Fecha de Ingreso,"+
		  "A�o de Colocaci�n,Periodo Educativo,Modalidad Programa,"+
		  "Fecha �ltima Disposici�n,No. Disposiciones,Importe Disposici�n,Monto Total,"+
		  "Saldo de Linea,Modalidad,% de aportaci�n,Monto de Contragarant�a a Pagar\n";
		  else
		  linea = "Cr�dito,Nombre de Acreditado,RFC,Matric�la,Estudios,Fecha de Ingreso,"+
		  "A�o de Colocaci�n,Periodo Educativo,Modalidad Programa,"+
		  "Fecha �ltima Disposici�n,No. Disposiciones,Importe Disposici�n,Monto Total,"+
		  "Saldo de Linea,Modalidad\n";
		  nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
		  writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
		  buffer = new BufferedWriter(writer);
			buffer.write(linea);
			 
		  while (rs.next()) {
					
					String credi = (rs.getString("credi") == null) ? "" : rs.getString("credi");
					String nombre = (rs.getString("nombre") == null) ? "" : rs.getString("nombre");
					String matricula = (rs.getString("matricula") == null) ? "" : rs.getString("matricula");
					String rfc = (rs.getString("rfc") == null) ? "" : rs.getString("rfc");
					String estudios = (rs.getString("estudios") == null) ? "" : rs.getString("estudios");
					String fechaIngreso = (rs.getString("fecha_ingreso") == null) ? "" : rs.getString("fecha_ingreso");
					String anio = (rs.getString("anio") == null) ? "" : rs.getString("anio");
					String periodo = (rs.getString("periodo") == null) ? "" : rs.getString("periodo");
					String modalidad = (rs.getString("modalidad") == null) ? "" : rs.getString("modalidad");
					String fechaDis = (rs.getString("fecha_dis") == null) ? "" : rs.getString("fecha_dis");
					String numDis = (rs.getString("num_dis") == null) ? "" : rs.getString("num_dis");
					String importe = (rs.getString("importe") == null) ? "" : rs.getString("importe");
					String montoTotal = (rs.getString("monto_total") == null) ? "" : rs.getString("monto_total");
					String saldoLineas = (rs.getString("saldolineas") == null) ? "" : rs.getString("saldolineas");
					String porAportacion = (rs.getString("fg_porcentaje_aportacion") == null) ? "" : rs.getString("fg_porcentaje_aportacion");
					String montoGarantia = (rs.getString("fn_monto_garantia") == null) ? "" : rs.getString("fn_monto_garantia");
					String modalidad_bases_oper =(rs.getString("modalidad_bases_oper") == null) ? "" : rs.getString("modalidad_bases_oper");
					if(estatusReg.equals("S"))
					linea = credi.replace(',',' ') + ", " + 
									nombre + ", " +
									rfc + ", " +
									 matricula + ", " +
									estudios + ", " +
									fechaIngreso + ", " +
									anio + ", " +
									periodo+ ", " +
									modalidad + ", " +
									fechaDis + ", " +
									numDis + ", " +
									importe + ", " +
									montoTotal + ", " +
									saldoLineas + ","+	
									modalidad_bases_oper+","+
									porAportacion + ", " +
									montoGarantia + "\n" ;
					else 
						linea = credi.replace(',',' ') + ", " + 
									nombre + ", " +
									rfc + ", " +
									 matricula + ", " +
									estudios + ", " +
									fechaIngreso + ", " +
									anio + ", " +
									periodo+ ", " +
									modalidad + ", " +
									fechaDis + ", " +
									numDis + ", " +
									importe + ", " +
									montoTotal + ", " +
									saldoLineas +","+
									modalidad_bases_oper+"\n" ;
					buffer.write(linea);
		  }
		  buffer.close();
		 }
		 }catch (Throwable e) {
			throw new AppException("Error al generar el archivo",e);
		}
		finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
	return nombreArchivo;
	}//1367
		public List getConditions(){
		return this.conditions;
	}

	public void setPaginaOffset(String paginaOffset) {
		this.paginaOffset = paginaOffset;
	}

	public String getPaginaOffset() {
		return paginaOffset;
	}

	public void setPaginaNo(String paginaNo) {
		this.paginaNo = paginaNo;
	}

	public String getPaginaNo() {
		return paginaNo;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getFolio() {
		return folio;
	}

	public void setEstatusReg(String estatusReg) {
		this.estatusReg = estatusReg;
	}

	public String getEstatusReg() {
		return estatusReg;
	}
	public void setCalculaMonto(boolean calculaMonto) {
		this.calculaMonto = calculaMonto;
	}
	
	

}