package com.netro.educativo;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsultaCreditos implements IQueryGeneratorRegExtJS {

//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaCreditos.class);
	StringBuffer qrySentencia = new StringBuffer("");
	private String paginaOffset;
	private String paginaNo;
	private String intermediario;
	private String fechaCargaIni;
	private String fechaCargaFin;
	private String credito;
	private String matricula;
	private String estudios;
	private String fechaAcreditadoIni;
	private String fechaAcreditadoFin;
	private String anio;
	private String periodo;
	private String modalidad;
	private String fechaDisposicionIni;
	private String fechaDisposicionFin;
	private String no_disposicion;
	private String impDisposicion;
	private String montoTotal;
	private String saldoLinea;
	private String universidad;
	private String estatus;
	private String tipoConsulta;
	private String acuse;
	private String regAceptados;
	private String regRechazados;  
	private String folio;
	private List conditions;

	public ConsultaCreditos(){}

	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}

	/**
	 * metodo que obtiene las llaves primaria 
	 * @return 
	 */
	public String getDocumentQuery(){

		log.info("getDocumentQuery(E)");
		qrySentencia = new StringBuffer();
		conditions   = new ArrayList();

		qrySentencia.append("SELECT "+  
										" a.ic_registro as IC_REGISTRO  "+
									" FROM ce_contenido_educativo a,  " +
											" comcat_universidad u,  " +
											" comcat_if i,  " +
											" cerel_if_universidad  r, " +
											" gti_estatus_solic es,  " +
											" cecat_estatus_cred_educa ece_ce,  " +
											" cecat_estatus_cred_educa ece_es  " +
											" WHERE a.ic_universidad = u.ic_universidad   " +
											" and a.ic_if = i.ic_if  " +
											"and a.ic_folio_operacion = es.ic_folio (+)  " +
											"and a.ic_estatus_cred_educa = ece_ce.ic_estatus_cred_educa  " +
											"and es.ic_estatus_cred_educa = ece_es.ic_estatus_cred_educa (+)  " +
											" and  u.ic_universidad = r.ic_universidad   " +
											" and i.ic_if = r.ic_if ");
											
		qrySentencia.append("  and a.cg_aceptado_siag =  ? ");
		conditions.add("S");
											
		if(!estatus.equals("")){
			qrySentencia.append(" and ic_estatus_cred_educa = ? ");
			conditions.add(estatus);
		}
		if(!folio.equals("")){
			qrySentencia.append(" and a.ic_folio_operacion = ? ");
			conditions.add(folio);
		}
		if(!universidad.equals("")){
			qrySentencia.append(" and a.ic_universidad =? ");
			conditions.add(universidad);
		}
		if(!intermediario.equals("")){
			qrySentencia.append(" and i.ic_if =? ");
			conditions.add(intermediario);
		}
		if(!fechaCargaIni.equals("") && !fechaCargaFin.equals("")){
			qrySentencia.append(" and a.fn_fecha_carga >= TO_DATE(?, 'dd/mm/yyyy') "+
								"and  a.fn_fecha_carga < TO_DATE(?, 'dd/mm/yyyy')+1 ");
			conditions.add(fechaCargaIni);
			conditions.add(fechaCargaFin);
		}
		if(!credito.equals("")){
			qrySentencia.append(" and a.cg_financiamiento =? ");
			conditions.add(credito);
		}
		if(!matricula.equals("")){
			qrySentencia.append(" and a.cg_matricula =? ");
			conditions.add(matricula);
		}
		if(!estudios.equals("")){
			qrySentencia.append(" AND a.cg_desc_estudios  LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')  ");
			conditions.add(estudios);
		}
		if(!fechaAcreditadoIni.equals("")  &&  !fechaAcreditadoFin.equals("")){
			qrySentencia.append(" and a.df_ingreso >= TO_DATE(?, 'dd/mm/yyyy') "+
								" and  a.df_ingreso < TO_DATE(?, 'dd/mm/yyyy')+1 ");
			conditions.add(fechaAcreditadoIni);
			conditions.add(fechaAcreditadoFin);
		}
		if(!anio.equals("")){
			qrySentencia.append(" and a.cg_anio =? ");
			conditions.add(anio);
		}
		if(!periodo.equals("")){
			qrySentencia.append(" and  a.ic_periodo = ?  ");
			conditions.add(periodo);
		}
		if(!modalidad.equals("")){
			qrySentencia.append(" AND a.cg_modalidad LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')  ");
			conditions.add(modalidad);
		}
		if(!fechaDisposicionIni.equals("")  &&  !fechaDisposicionFin.equals("")){
			qrySentencia.append(" and a.df_disposicion >= TO_DATE(?, 'dd/mm/yyyy') "+
							  " and  a.df_disposicion < TO_DATE(?, 'dd/mm/yyyy')+1  ");
			conditions.add(fechaDisposicionIni);
			conditions.add(fechaDisposicionFin);
		}
		if(!no_disposicion.equals("")){
			qrySentencia.append(" and a.ic_num_disposicion BETWEEN  ? and  ? ");
			conditions.add(no_disposicion);
			conditions.add(no_disposicion);
		}
		if(!impDisposicion.equals("")){
			qrySentencia.append(" and a.fn_imp_disposicion BETWEEN  ? and  ? ");
			conditions.add(impDisposicion);
			conditions.add(impDisposicion);
		}
		if(!montoTotal.equals("")){
			qrySentencia.append(" and a.FN_FINANCIAMIENTO BETWEEN  ? and  ? ");
			conditions.add(montoTotal);
			conditions.add(montoTotal);
		}
		if(!saldoLinea.equals("")){
			qrySentencia.append("  and a.fn_saldolineas BETWEEN  ? and  ? ");
			conditions.add(saldoLinea);
			conditions.add(saldoLinea);
		}
		log.info("regRechazados: " + regRechazados);
		log.info("regAceptados: " + regAceptados);
		if(!regRechazados.equals("")){
			qrySentencia.append(" and a.ic_registro in ("+regRechazados+")");
		}
		if(!regAceptados.equals("")){
			qrySentencia.append(" and a.ic_registro in ("+regAceptados+")");
		}
		if(!acuse.equals("")){
			qrySentencia.append("  and a.cc_acuse  = ? ");
			conditions.add(acuse);
		}
		qrySentencia.append(" order by a.ic_folio_operacion, a.cg_razon_social  ");

		log.info("qrySentencia: " + qrySentencia);
		log.info("conditions: " + conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	/**
	 * metodo que obtienes la paginaci�n 
	 * @return 
	 * @param pageIds
	 */
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();

		qrySentencia.append("SELECT ");
		qrySentencia.append(" a.ic_registro as IC_REGISTRO, ");
		qrySentencia.append(" u.cg_razon_social as UNIVERSIDAD, ");
		qrySentencia.append(" u.cg_nombre_campus as CAMPUS, ");
		qrySentencia.append(" i.cg_razon_social as INTERMEDIARIO, ");
		qrySentencia.append(" i.ic_if  as IC_IF, ");
		qrySentencia.append(" TO_CHAR(a.fn_fecha_carga,'dd/mm/yyyy hh:mm:ss') as  FECHA_CARGA, ");
		qrySentencia.append(" a.ic_folio_operacion||' ' as FOLIO,");
		qrySentencia.append(" a.cg_financiamiento as CREDITO, ");
		qrySentencia.append(" a.cg_razon_social as NOMBRE_ACREDITADO, ");
		qrySentencia.append(" a.cg_registro as RFC, ");
		qrySentencia.append(" a.cg_matricula as MATRICULA , ");
		qrySentencia.append(" a.cg_desc_estudios as ESTUDIOS, ");
		qrySentencia.append(" TO_CHAR(a.df_ingreso,'dd/mm/yyyy') as FECHA_INGRESO, ");
		qrySentencia.append(" a.cg_anio as ANIO_COLOCACION, ");
		qrySentencia.append(" a.ic_periodo  as PERIODO_EDUCATIVO, ");
		qrySentencia.append(" a.cg_modalidad as MODALIDAD_PROGRAMA, ");
		qrySentencia.append(" TO_CHAR(a.df_disposicion,'dd/mm/yyyy') as FECHA_UL_DISPOSICION, ");
		qrySentencia.append(" a.ic_num_disposicion as NO_DISPOSICION, ");
		qrySentencia.append(" a.fn_imp_disposicion as IMPORTE_DISPOSICON, ");
		qrySentencia.append(" a.fn_financiamiento as MONTO_TOTAL, ");
		qrySentencia.append(" a.fn_saldolineas  as SALDO_LINEA, ");
		qrySentencia.append(" decode(a.ic_estatus_cred_educa, 3, a.cg_causa_rechazo) AS CAUSA_RECHAZO, ");
		qrySentencia.append(" 'N' AS RECHAZADOS, ");
		qrySentencia.append(" 'S' AS ACEPTADOS, ");
		qrySentencia.append(" CASE WHEN es.ic_folio is not null THEN ece_es.cd_descripcion ELSE ece_ce.cd_descripcion END as ESTATUS, ");
		qrySentencia.append(" a.fn_financiamiento FN_FINANCIAMIENTO, ");
		qrySentencia.append(" a.fn_monto_garantia as MONTO_GARANTIA ");
		qrySentencia.append(" FROM ce_contenido_educativo a, ");
		qrySentencia.append(" comcat_universidad u, ");
		qrySentencia.append(" comcat_if i, ");
		qrySentencia.append(" cerel_if_universidad r, ");
		qrySentencia.append(" gti_estatus_solic es, ");
		qrySentencia.append(" cecat_estatus_cred_educa ece_ce, ");
		qrySentencia.append(" cecat_estatus_cred_educa ece_es ");
		qrySentencia.append(" WHERE a.ic_universidad = u.ic_universidad ");
		qrySentencia.append(" and a.ic_if = i.ic_if ");
		qrySentencia.append(" and a.ic_folio_operacion = es.ic_folio (+) ");
		qrySentencia.append(" and a.ic_estatus_cred_educa = ece_ce.ic_estatus_cred_educa ");
		qrySentencia.append(" and es.ic_estatus_cred_educa = ece_es.ic_estatus_cred_educa (+) ");
		qrySentencia.append(" and  u.ic_universidad = r.ic_universidad ");
		qrySentencia.append(" and i.ic_if = r.ic_if ");

		qrySentencia.append(" AND (");

		for (int i = 0; i < pageIds.size(); i++){
			List lItem = (ArrayList)pageIds.get(i);
			if(i > 0){
				qrySentencia.append("  OR  ");
			}
			qrySentencia.append("a.ic_registro  = ?");
			conditions.add(new Long(lItem.get(0).toString()));
		}
		qrySentencia.append(" ) ");
		qrySentencia.append(" order by a.fn_fecha_carga , a.cg_razon_social ");

		log.info("qrySentencia: " + qrySentencia);
		log.info("conditions: " + conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();

 
				qrySentencia.append("SELECT "+  
											" a.ic_registro as IC_REGISTRO , "+
											" u.cg_razon_social as UNIVERSIDAD, "+
											" u.cg_nombre_campus as CAMPUS, "+
											" i.cg_razon_social as INTERMEDIARIO, "+
											" i.ic_if  as IC_IF, "+										
											" TO_CHAR(a.fn_fecha_carga,'dd/mm/yyyy hh:mm:ss') as  FECHA_CARGA, "+
											" a.ic_folio_operacion as FOLIO ,"+
											" a.cg_financiamiento as CREDITO , "+
											" a.cg_razon_social as NOMBRE_ACREDITADO , "+
											" a.cg_registro as RFC ,  "+
											" a.cg_matricula as MATRICULA ,  "+
											" a.cg_desc_estudios as ESTUDIOS ,  "+
											" TO_CHAR(a.df_ingreso,'dd/mm/yyyy')   as FECHA_INGRESO ,"+
											" a.cg_anio as ANIO_COLOCACION , "+
											" a.ic_periodo  as PERIODO_EDUCATIVO , "+
											" a.cg_modalidad as MODALIDAD_PROGRAMA , "+
											" TO_CHAR(a.df_disposicion,'dd/mm/yyyy')  as FECHA_UL_DISPOSICION , "+
											" a.ic_num_disposicion as NO_DISPOSICION , "+
											" a.fn_imp_disposicion as IMPORTE_DISPOSICON , "+
											" a.FN_FINANCIAMIENTO as MONTO_TOTAL, "+
											" a.fn_saldolineas  as SALDO_LINEA,  "+										
											" decode(a.ic_estatus_cred_educa, 3, a.cg_causa_rechazo) AS CAUSA_RECHAZO, "+										
											" 'N' AS RECHAZADOS, "+
											" 'S' AS ACEPTADOS , "+	
											" a.ic_folio_operacion as FOLIO_OPERACION, "+
											" CASE WHEN es.ic_folio is not null THEN ece_es.cd_descripcion ELSE ece_ce.cd_descripcion END as ESTATUS,  " + 		
											" a.fn_financiamiento FN_FINANCIAMIENTO, "+
											" NVL(a.fn_monto_garantia,0) as MONTO_GARANTIA "+
										" FROM ce_contenido_educativo a,  " +
											" comcat_universidad u,  " +
											" comcat_if i,  " +
											" cerel_if_universidad  r, " +
											" gti_estatus_solic es,  " +
											" cecat_estatus_cred_educa ece_ce,  " +
											" cecat_estatus_cred_educa ece_es  " +
											" WHERE a.ic_universidad = u.ic_universidad   " +
											" and a.ic_if = i.ic_if  " +
											"and a.ic_folio_operacion = es.ic_folio (+)  " +
											"and a.ic_estatus_cred_educa = ece_ce.ic_estatus_cred_educa  " +
											"and es.ic_estatus_cred_educa = ece_es.ic_estatus_cred_educa (+)  " +
											" and  u.ic_universidad = r.ic_universidad   " +
											" and i.ic_if = r.ic_if ");
			qrySentencia.append("  and a.cg_aceptado_siag =  ? ");
			conditions.add("S");
	
			if(!estatus.equals("")) {	
				qrySentencia.append(" and ic_estatus_cred_educa = ? ");
				conditions.add(estatus);
			}
			if(!folio.equals("")){
			qrySentencia.append(" and a.ic_folio_operacion = ? ");
			conditions.add(folio);
		}
			if(!universidad.equals("")) {	
				qrySentencia.append(" and a.ic_universidad =? ");
				conditions.add(universidad);
			}
			
			if(!intermediario.equals("")) {	
				qrySentencia.append(" and i.ic_if =? ");
				conditions.add(intermediario);
			}
			if(!fechaCargaIni.equals("")  &&  !fechaCargaFin.equals("") ) {	
				qrySentencia.append(" and a.fn_fecha_carga >= TO_DATE(?, 'dd/mm/yyyy') "+
											"and  a.fn_fecha_carga < TO_DATE(?, 'dd/mm/yyyy')+1 ");
				conditions.add(fechaCargaIni);	
				conditions.add(fechaCargaFin);	
			}
		
			if(!credito.equals("")) {	
				qrySentencia.append(" and a.cg_financiamiento =? ");
				conditions.add(credito);	
			}
			
			if(!matricula.equals("")) {	
				qrySentencia.append(" and a.cg_matricula =? ");
				conditions.add(matricula);	
			}
			if(!estudios.equals("")) {
				qrySentencia.append(" AND a.cg_desc_estudios  LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')  ");
    			conditions.add(estudios);	
			}
		
			if(!fechaAcreditadoIni.equals("")  &&  !fechaAcreditadoFin.equals("") ) {
				qrySentencia.append("  and a.df_ingreso >= TO_DATE(?, 'dd/mm/yyyy') "+
										  " and  a.df_ingreso < TO_DATE(?, 'dd/mm/yyyy')+1 ");
				conditions.add(fechaAcreditadoIni);	
				conditions.add(fechaAcreditadoFin);	
			}
			if(!anio.equals("")) {
				qrySentencia.append(" and a.cg_anio =? ");
				conditions.add(anio);	
			}
			if(!periodo.equals("")) {	
				qrySentencia.append(" and  a.ic_periodo = ?  ");
				conditions.add(periodo);
			}	
			
			if(!modalidad.equals("")) {	
				qrySentencia.append(" AND a.cg_modalidad LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')  ");
				conditions.add(modalidad);	
			}
			if(!fechaDisposicionIni.equals("")  &&  !fechaDisposicionFin.equals("") ) {
				qrySentencia.append(" and a.df_disposicion >= TO_DATE(?, 'dd/mm/yyyy') "+
										  " and  a.df_disposicion < TO_DATE(?, 'dd/mm/yyyy')+1  ");
				conditions.add(fechaDisposicionIni);	
				conditions.add(fechaDisposicionFin);	
			}
			if(!no_disposicion.equals("")) {	
				qrySentencia.append(" and a.ic_num_disposicion BETWEEN  ? and  ? ");
				conditions.add(no_disposicion);	
				conditions.add(no_disposicion);	
			}
			
			if(!impDisposicion.equals("")) {	
				qrySentencia.append(" and a.fn_imp_disposicion BETWEEN  ? and  ? ");				
				conditions.add(impDisposicion);	
				conditions.add(impDisposicion);					
			}
			
			
			if(!montoTotal.equals("")) {	
				qrySentencia.append(" and a.FN_FINANCIAMIENTO BETWEEN  ? and  ? ");				
				conditions.add(montoTotal);	
				conditions.add(montoTotal);					
			}
			
			if(!saldoLinea.equals("")) {
				qrySentencia.append("  and a.fn_saldolineas BETWEEN  ? and  ? ");
				conditions.add(saldoLinea);
				conditions.add(saldoLinea);
			}
		
					
			if(!regRechazados.equals("") ) {  	
				qrySentencia.append(" and a.ic_registro in ("+regRechazados+")");		  		
			}
			
			if(!regAceptados.equals("")){
				qrySentencia.append(" and a.ic_registro in ("+regAceptados+")");
			}

			if(!acuse.equals("")){
				qrySentencia.append("  and a.cc_acuse  = ? ");
				conditions.add(acuse);
			}

			qrySentencia.append(" order by a.ic_folio_operacion, a.cg_razon_social  ");

		log.info("qrySentencia: " + qrySentencia);
		log.info("conditions: " + conditions);
		log.info("getDocumentQueryFile (S)");
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		return "";
					
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		
		String nombreArchivo = "";
		HttpSession session = request.getSession();

		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		int i=0;
			
		try {
		
		 
				while (rs.next())	{					
					
					String universisad          = (rs.getString("UNIVERSIDAD")          == null) ? "" : rs.getString("UNIVERSIDAD");
					String campus               = (rs.getString("CAMPUS")               == null) ? "" : rs.getString("CAMPUS");
					String nombreIF             = (rs.getString("INTERMEDIARIO")        == null) ? "" : rs.getString("INTERMEDIARIO");
					String fecha_carga          = (rs.getString("FECHA_CARGA")          == null) ? "" : rs.getString("FECHA_CARGA");
					String folio                = (rs.getString("FOLIO")                == null) ? "" : rs.getString("FOLIO");
					String credito              = (rs.getString("CREDITO")              == null) ? "" : rs.getString("CREDITO");
					String nombreAcreditado     = (rs.getString("NOMBRE_ACREDITADO")    == null) ? "" : rs.getString("NOMBRE_ACREDITADO");
					String rfc                  = (rs.getString("RFC")                  == null) ? "" : rs.getString("RFC");
					String matricula            = (rs.getString("MATRICULA")            == null) ? "" : rs.getString("MATRICULA");
					String estudios             = (rs.getString("ESTUDIOS")             == null) ? "" : rs.getString("ESTUDIOS");
					String fecha_ingreso        = (rs.getString("FECHA_INGRESO")        == null) ? "" : rs.getString("FECHA_INGRESO");
					String anio_colocacion      = (rs.getString("ANIO_COLOCACION")      == null) ? "" : rs.getString("ANIO_COLOCACION");
					String periodo_educativo    = (rs.getString("PERIODO_EDUCATIVO")    == null) ? "" : rs.getString("PERIODO_EDUCATIVO");
					String modalidad_programa   = (rs.getString("MODALIDAD_PROGRAMA")   == null) ? "" : rs.getString("MODALIDAD_PROGRAMA");
					String fecha_ul_disposicion = (rs.getString("FECHA_UL_DISPOSICION") == null) ? "" : rs.getString("FECHA_UL_DISPOSICION");
					String no_disposicion       = (rs.getString("NO_DISPOSICION")       == null) ? "" : rs.getString("NO_DISPOSICION");
					String importe_disposicion  = (rs.getString("IMPORTE_DISPOSICON")   == null) ? "" : rs.getString("IMPORTE_DISPOSICON");
					String monto_total          = (rs.getString("MONTO_TOTAL")          == null) ? "" : rs.getString("MONTO_TOTAL");
					String saldo_linea          = (rs.getString("SALDO_LINEA")          == null) ? "" : rs.getString("SALDO_LINEA");
					String causa_rechazo        = (rs.getString("CAUSA_RECHAZO")        == null) ? "" : rs.getString("CAUSA_RECHAZO");
					String folio_operacion      = (rs.getString("FOLIO_OPERACION")      == null) ? "" : rs.getString("FOLIO_OPERACION");
					String estatusC             = (rs.getString("ESTATUS")              == null) ? "" : rs.getString("ESTATUS");
					String montoGarantia        = (rs.getString("MONTO_GARANTIA")       == null) ? "" : rs.getString("MONTO_GARANTIA");

					if(i==0){
						contenidoArchivo.append("Universidad: "+universisad.replaceAll(",","")+"\n"); 
						contenidoArchivo.append("Campus: "+campus.replaceAll(",","")+"\n"); 
						contenidoArchivo.append("Intermediario Financiero: "+nombreIF.replaceAll(",","")+"\n");
						contenidoArchivo.append("  "+"\n");

						contenidoArchivo.append("Fecha y Hora de Carga,Folio de operaci�n,Cr�dito,Nombre del acreditado,RFC,Matricula, Estudios, Fecha de Ingreso, A�o de colocaci�n, Periodo Educativo, Modalidad Programa, ");
						contenidoArchivo.append("Fecha �ltima disposici�n,No. de disposici�n, Importe Disposici�n, Monto Total, Saldo de L�nea,Monto de Contragarant�a a Pagar,Estatus,Causas del Rechazo");
						contenidoArchivo.append(" \n");
					}

					contenidoArchivo.append("'"+fecha_carga.replaceAll(",","")+",");
					contenidoArchivo.append("'"+folio.replaceAll(",","")+",");
					contenidoArchivo.append(credito.replaceAll(",","")+",");
					contenidoArchivo.append(nombreAcreditado.replaceAll(",","")+",");
					contenidoArchivo.append(rfc.replaceAll(",","")+",");
					contenidoArchivo.append(matricula.replaceAll(",","")+",");
					contenidoArchivo.append(estudios.replaceAll(",","")+",");
					contenidoArchivo.append(fecha_ingreso.replaceAll(",","")+",");
					contenidoArchivo.append(anio_colocacion.replaceAll(",","")+",");
					contenidoArchivo.append(periodo_educativo.replaceAll(",","")+",");
					contenidoArchivo.append(modalidad_programa.replaceAll(",","")+",");
					contenidoArchivo.append(fecha_ul_disposicion.replaceAll(",","")+",");
					contenidoArchivo.append(no_disposicion.replaceAll(",","")+",");
					contenidoArchivo.append(importe_disposicion.replaceAll(",","")+",");
					contenidoArchivo.append(monto_total.replaceAll(",","")+",");
					contenidoArchivo.append(saldo_linea.replaceAll(",","")+",");
					contenidoArchivo.append(montoGarantia+",");
					contenidoArchivo.append(estatusC.replaceAll(",","")+",");
					contenidoArchivo.append((causa_rechazo.equals("")?"N/A":causa_rechazo.replaceAll(",",""))+",");
					contenidoArchivo.append(" \n");
					i++;
				}

				if(tipo.equals("CSV")){
					creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
					nombreArchivo = creaArchivo.getNombre();
				}

		} catch(Throwable e){
			throw new AppException("Error al generar el archivo. ", e);
		} finally {
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
		
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getIntermediario() {
		return intermediario;
	}

	public void setIntermediario(String intermediario) {
		this.intermediario = intermediario;
	}

	public String getFechaCargaIni() {
		return fechaCargaIni;
	}

	public void setFechaCargaIni(String fechaCargaIni) {
		this.fechaCargaIni = fechaCargaIni;
	}

	public String getFechaCargaFin() {
		return fechaCargaFin;
	}

	public void setFechaCargaFin(String fechaCargaFin) {
		this.fechaCargaFin = fechaCargaFin;
	}

	public String getCredito() {
		return credito;
	}

	public void setCredito(String credito) {
		this.credito = credito;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getEstudios() {
		return estudios;
	}

	public void setEstudios(String estudios) {
		this.estudios = estudios;
	}

	public String getFechaAcreditadoIni() {
		return fechaAcreditadoIni;
	}

	public void setFechaAcreditadoIni(String fechaAcreditadoIni) {
		this.fechaAcreditadoIni = fechaAcreditadoIni;
	}

	public String getFechaAcreditadoFin() {
		return fechaAcreditadoFin;
	}

	public void setFechaAcreditadoFin(String fechaAcreditadoFin) {
		this.fechaAcreditadoFin = fechaAcreditadoFin;
	}

	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public String getModalidad() {
		return modalidad;
	}

	public void setModalidad(String modalidad) {
		this.modalidad = modalidad;
	}

	public String getFechaDisposicionIni() {
		return fechaDisposicionIni;
	}

	public void setFechaDisposicionIni(String fechaDisposicionIni) {
		this.fechaDisposicionIni = fechaDisposicionIni;
	}

	public String getFechaDisposicionFin() {
		return fechaDisposicionFin;
	}

	public void setFechaDisposicionFin(String fechaDisposicionFin) {
		this.fechaDisposicionFin = fechaDisposicionFin;
	}

	public String getNo_disposicion() {
		return no_disposicion;
	}

	public void setNo_disposicion(String no_disposicion) {
		this.no_disposicion = no_disposicion;
	}

	public String getImpDisposicion() {
		return impDisposicion;
	}

	public void setImpDisposicion(String impDisposicion) {
		this.impDisposicion = impDisposicion;
	}

	public String getMontoTotal() {
		return montoTotal;
	}

	public void setMontoTotal(String montoTotal) {
		this.montoTotal = montoTotal;
	}

	public String getSaldoLinea() {
		return saldoLinea;
	}

	public void setSaldoLinea(String saldoLinea) {
		this.saldoLinea = saldoLinea;
	}

	public String getUniversidad() {
		return universidad;
	}

	public void setUniversidad(String universidad) {
		this.universidad = universidad;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getTipoConsulta() {
		return tipoConsulta;
	}

	public void setTipoConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}

	public String getAcuse() {
		return acuse;
	}

	public void setAcuse(String acuse) {
		this.acuse = acuse;
	}

	public String getRegAceptados() {
		return regAceptados;
	}

	public void setRegAceptados(String regAceptados) {
		this.regAceptados = regAceptados;
	}

	public String getRegRechazados() {
		return regRechazados;
	}

	public void setRegRechazados(String regRechazados) {
		this.regRechazados = regRechazados;
	}


	public void setFolio(String folio) {
		this.folio = folio;
	}


	public String getFolio() {
		return folio;
	}


}