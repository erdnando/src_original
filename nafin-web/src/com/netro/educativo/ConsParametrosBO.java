package com.netro.educativo;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsParametrosBO implements IQueryGeneratorRegExtJS {

//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsParametrosBO.class);
	private List conditions;
	private String paginaOffset;
	private String paginaNo;
	private String intermediario;
	private String baseOperacion;
	private String operacion;
	private String modalidad;
	private String estatus;
	private String universidad;
	StringBuffer qrySentencia = new StringBuffer("");
	 
	public ConsParametrosBO(){}
	public String getAggregateCalculationQuery(){return null;}
	public String getDocumentQuery(){return null;}
	public String getDocumentSummaryQueryForIds(List pageIds){return null;}
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){return null;}

	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();

		qrySentencia.append("SELECT I.CG_RAZON_SOCIAL AS NOMBRE_IF, BO.IC_BASE_OPERACION AS CONSECUTIVO, ");
		qrySentencia.append("BO.IC_CLAVE AS CLAVE, BO.CG_DESCRIPCION AS DESCRIPCION, ");
		qrySentencia.append("DECODE (BO.CS_OPERA_CREDITO, 'S', 'Si', 'N', 'No') AS OPERA_CREDITO, ");
		qrySentencia.append("BO.FG_PORCENTAJE_APORTACION AS POR_APORTACION, ");
		qrySentencia.append("DECODE (BO.CG_MODALIDAD, ");
		qrySentencia.append("'1', 'Pari Passu', ");
		qrySentencia.append("'2', 'Primeras Perdidas' ");
		qrySentencia.append(") AS MODALIDAD, ");
		qrySentencia.append("DECODE (BO.CS_ACTIVO, 'A', 'Activo', 'I', 'Inactivo') AS ESTATUS, ");
		qrySentencia.append("BO.CS_ACTIVO AS CLAVE_ESTATUS, BO.CG_USUARIO AS USUARIO_ALTA, ");
		qrySentencia.append("TO_CHAR (BO.DF_ALTA, 'dd/mm/yyyy') AS FECHA_INGRESO, ");
		qrySentencia.append("BO.CG_USUARIO_MOD AS USUARIO_MOD, ");
		qrySentencia.append("TO_CHAR (BO.DF_MODIFICACION, 'dd/mm/yyyy') AS FECHA_MOD, ");
		qrySentencia.append("U.CG_RAZON_SOCIAL AS UNIVERSIDAD, BO.IC_IF, U.IC_UNIVERSIDAD AS IC_UNIV ");
		qrySentencia.append("FROM CECAT_BASES_OPERA BO, ");
		qrySentencia.append("COMCAT_IF I, ");
		qrySentencia.append("COMCAT_UNIVERSIDAD U, ");
		qrySentencia.append("CEREL_IF_UNIVERSIDAD R ");
		qrySentencia.append("WHERE I.IC_IF = BO.IC_IF ");
		qrySentencia.append("AND I.IC_IF = R.IC_IF ");
		qrySentencia.append("AND U.IC_UNIVERSIDAD = R.IC_UNIVERSIDAD ");
		qrySentencia.append("AND BO.IC_UNIVERSIDAD = R.IC_UNIVERSIDAD ");
		qrySentencia.append("AND R.CG_ACTIVO = 'S' ");
		qrySentencia.append("");
		qrySentencia.append("AND BO.IC_IF = R.IC_IF ");
		
		if(!intermediario.equals("")) {	
			qrySentencia.append("AND I.IC_IF = ? " );
			conditions.add(intermediario);
		}
		if(!baseOperacion.equals("")) {	
			qrySentencia.append("AND BO.IC_BASE_OPERACION = ? " );
			conditions.add(baseOperacion);
		}
		
		if(!operacion.equals("")) {	
			qrySentencia.append("AND BO.CS_OPERA_CREDITO = ? " );
			conditions.add(operacion);
		}
		
		if(!modalidad.equals("")) {	
			qrySentencia.append("AND BO.CG_MODALIDAD = ? " );
			conditions.add(modalidad);
		}
		if(!estatus.equals("")) {	
			qrySentencia.append("AND BO.CS_ACTIVO = ? " );
			conditions.add(estatus);
		}
		if(!universidad.equals("")) {	
			qrySentencia.append("AND BO.IC_UNIVERSIDAD = ? " );
			conditions.add(universidad);
		}

		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}

	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		ComunesPDF pdfDoc = new ComunesPDF();
	
			
		try {
		
			if(tipo.equals("PDF") ) {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(), 
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),  
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			}
				
			pdfDoc.setTable(2,100);
			pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Nombre EPO","celda01",ComunesPDF.CENTER);

			if(tipo.equals("PDF") ) {
				pdfDoc.addTable();
				pdfDoc.endDocument();	
			}
			if(tipo.equals("CSV") ) {
				creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
				nombreArchivo = creaArchivo.getNombre();
			}
		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
		
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getIntermediario() {
		return intermediario;
	}

	public void setIntermediario(String intermediario) {
		this.intermediario = intermediario;
	}

	public String getBaseOperacion() {
		return baseOperacion;
	}

	public void setBaseOperacion(String baseOperacion) {
		this.baseOperacion = baseOperacion;
	}

	public String getOperacion() {
		return operacion;
	}

	public void setOperacion(String operacion) {
		this.operacion = operacion;
	}

	public String getModalidad() {
		return modalidad;
	}

	public void setModalidad(String modalidad) {
		this.modalidad = modalidad;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getUniversidad() {
		return universidad;
	}

	public void setUniversidad(String universidad) {
		this.universidad = universidad;
	}

	

}