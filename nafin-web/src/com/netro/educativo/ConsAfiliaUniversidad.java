package com.netro.educativo;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsAfiliaUniversidad implements IQueryGeneratorRegExtJS {// Fodea024-2013 DBM

	
//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsAfiliaUniversidad.class);

	StringBuffer qrySentencia = new StringBuffer("");
	StringBuffer	condicion  =new StringBuffer(); 		
	private List   conditions;

	private String numeroNE="";
	private String cgrupo="";
	private String sUniversidad="";
	private String sCampus="";
	private String cEstado="";
	
	
	/*Constructor*/
	public ConsAfiliaUniversidad() { }
	
  
	/**
	* Obtiene el query para obtener los totales de la consulta
	* @return Cadena con la consulta de SQL, para obtener los totales
	*/
	public String getAggregateCalculationQuery() { 
		return "";
	}
	
	/**
	* Obtiene el query para la CONSULTA
	* @return Cadena con la consulta de SQL
	*/
	public String getDocumentQuery() { // las  LLAVES
		log.info("getDocumentQuery(E) :::...");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();		//binds
		condicion		= new StringBuffer();	//condiciones del query
		
		if(!"".equals(numeroNE)) {
			condicion.append( "and n.ic_nafin_electronico = ?" );  
			conditions.add(numeroNE);
		}
		
		if(!"".equals(cgrupo)) {
			condicion.append( "and u.ic_grupo = ?" );  
			conditions.add(cgrupo);
		}
		
		if (!"".equals(sUniversidad)) {
			condicion.append( "and u.cg_razon_social like ?" );  
			conditions.add(sUniversidad+"%");
		}
			
		if (!"".equals(sCampus)) {
			condicion.append( "and u.cg_nombre_campus like ?" );  
			conditions.add(sCampus+"%");
		}
		
		if (!"".equals(cEstado)) {
			condicion.append( "and e.ic_estado = ?" );  
			conditions.add(cEstado);
		}
		
		qrySentencia.append(
					" SELECT  u.ic_universidad " +
					" FROM comcat_universidad u" +
					"		, comrel_nafin n"+
					"		, com_domicilio d"+
					"		, com_contacto c" +
					"		, comcat_estado e" +
					"		, comcat_pais p " +
					" WHERE u.ic_universidad = d.ic_universidad " +
					" 		AND u.ic_universidad  = c.ic_universidad "+
					" 		AND d.ic_estado = e.ic_estado " +
					" 		AND d.ic_pais = p.ic_pais " +
					" 		AND n.ic_epo_pyme_if = u.ic_universidad " +
					" 		AND n.cg_tipo = 'U'" +
					condicion);
	
		log.info("getDocumentQuery :::...  "+qrySentencia);
		log.info("conditions :::..."+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	/**
	* Obtiene el query para obtener la consulta
	* @return Cadena con la consulta de SQL, para obtener la consulta con PAGINADOR
	*/
	public String getDocumentSummaryQueryForIds(List pageIds) 
	{		
		log.info("getDocumentSummaryQueryForIds(E) :::...");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();		//binds
		condicion		= new StringBuffer();	//condiciones del query
		
			
		if(!"".equals(numeroNE)) {
			condicion.append( "and n.ic_nafin_electronico = ?" );  
			conditions.add(numeroNE);
		}
		
		if(!"".equals(cgrupo)) {
			condicion.append( "and u.ic_grupo = ?" );  
			conditions.add(cgrupo);
		}	
		
		if (!"".equals(sUniversidad)) {
			condicion.append( "and u.cg_razon_social like ?" );  
			conditions.add(sUniversidad+"%");
		}
			
		if (!"".equals(sCampus)) {
			condicion.append( "and u.cg_nombre_campus like ?" );  
			conditions.add(sCampus+"%");
		}
		if (!"".equals(cEstado)) {
			condicion.append( "and e.ic_estado = ?" );  
			conditions.add(cEstado);
		}

		qrySentencia.append(
					" SELECT  u.ic_universidad AS IC_UNIVERSIDAD, n.ic_nafin_electronico  AS NONAFIN" +
					"		, g.cd_descripcion AS GRUPO" +
					"		, u.cg_razon_social  AS RAZON_SOCIAL" +
					"		, u.cg_nombre_campus AS CAMPUS" +
					"		, d.cg_calle || '  ' || d.cg_colonia || '  ' || d.cg_municipio AS DOMICILIO" +
					"		, e.cd_nombre AS ESTADO, d.cg_telefono1 AS TELEFONO" +
					"		, c.cg_nombre || '  ' || c.cg_appat || '  ' || c.cg_apmat AS APODERADO" +
					"		, TO_CHAR (u.df_alta, 'dd/mm/yyyy hh24:mi:ss') AS ALTA" +
					"		,DECODE (u.cg_cg_estatus,'A','Activo','I','Inactivo') AS ESTATUS " + 
					" FROM comcat_universidad u" +
					"		, comrel_nafin n"+
					"		, com_domicilio d"+
					"		, com_contacto c" +
					"		, comcat_estado e" +
					"		, comcat_pais p " +
					"		, cecat_grupo g " +
					" WHERE u.ic_universidad = d.ic_universidad " +
					" 		AND u.ic_universidad  = c.ic_universidad "+
					" 		AND u.ic_grupo  = g.ic_grupo "+
					" 		AND d.ic_estado = e.ic_estado " +
					" 		AND d.ic_pais = p.ic_pais " +
					" 		AND n.ic_epo_pyme_if = u.ic_universidad " +
					" 		AND n.cg_tipo = 'U'" +
					condicion);
										
			qrySentencia.append(" AND (");
			for(int i = 0; i < pageIds.size(); i++){
				List lItem = (ArrayList)pageIds.get(i);					
				if(i > 0){ qrySentencia.append("OR"); }
				qrySentencia.append("( u.ic_universidad = ? )");
				conditions.add(lItem.get(0).toString());
			}		
			qrySentencia.append(" ) " +
			" ORDER BY u.cg_razon_social");
		
		log.info("getDocumentSummaryQueryForIds :::...  "+qrySentencia);
		log.info("conditions :::..."+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	
	public String getDocumentQueryFile() {
		
		log.info("getDocumentQueryFile(E) :::...");
		
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();		//binds
		condicion		= new StringBuffer();	//condiciones del query
		
		if(!"".equals(numeroNE)) {
			condicion.append( "and n.ic_nafin_electronico = ?" );  
			conditions.add(numeroNE);
		}
		
		if(!"".equals(cgrupo)) {
			condicion.append( "and u.ic_grupo = ?" );  
			conditions.add(cgrupo);
		}	
		
		if (!"".equals(sUniversidad)) {
			condicion.append( "and u.cg_razon_social like ?" );  
			conditions.add(sUniversidad+"%");
		}
			
		if (!"".equals(sCampus)) {
			condicion.append( "and u.cg_nombre_campus like ?" );  
			conditions.add(sCampus+"%");
		}
		
		if (!"".equals(cEstado)) {
			condicion.append( "and e.ic_estado = ?" );  
			conditions.add(cEstado);
		}
	
		qrySentencia.append(
					" SELECT  u.ic_universidad AS IC_UNIVERSIDAD, n.ic_nafin_electronico  AS NONAFIN" +
					"		, g.cd_descripcion AS GRUPO" +
					"		, u.cg_razon_social  AS RAZON_SOCIAL" +
					"		, u.cg_nombre_campus AS CAMPUS" +
					"		, d.cg_calle || '  ' || d.cg_colonia || '  ' || d.cg_municipio AS DOMICILIO" +
					"		, e.cd_nombre AS ESTADO, d.cg_telefono1 AS TELEFONO" +
					"		, c.cg_nombre || '  ' || c.cg_appat || '  ' || c.cg_apmat AS APODERADO" +
					"		, TO_CHAR (u.df_alta, 'dd/mm/yyyy hh24:mi:ss') AS ALTA" +
					"		,DECODE (u.cg_cg_estatus,'A','Activo','I','Inactivo') AS ESTATUS " + 
					" FROM comcat_universidad u" +
					"		, comrel_nafin n"+
					"		, com_domicilio d"+
					"		, com_contacto c" +
					"		, comcat_estado e" +
					"		, comcat_pais p " +
					"		, cecat_grupo g " +
					" WHERE u.ic_universidad = d.ic_universidad " +
					" 		AND u.ic_universidad  = c.ic_universidad "+
					" 		AND u.ic_grupo  = g.ic_grupo "+
					" 		AND d.ic_estado = e.ic_estado " +
					" 		AND d.ic_pais = p.ic_pais " +
					" 		AND n.ic_epo_pyme_if = u.ic_universidad " +
					" 		AND n.cg_tipo = 'U'" +
					condicion);
			
		log.info("getDocumentQueryFile :::...  "+qrySentencia);
		log.info("conditions :::..."+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	} 
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	/*Este metodo se utiliza para crear archivos con PAGINADOR*/		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo)
	{	
		String nombreArchivo = "";
				
		if("PDF".equals(tipo)){
			try{
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
				
				String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual		= fechaActual.substring(0,2);
				String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual		= fechaActual.substring(6,10);
				String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				(session.getAttribute("iNoNafinElectronico")==null?"":session.getAttribute("iNoNafinElectronico")).toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				
			
				pdfDoc.setTable(10, 100); //numero de columnas y el ancho que ocupara la tabla en el documento
			
				pdfDoc.setCell("N�mero de Nafin Electr�nico","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Grupo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre Universidad","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre Campus","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Domicilio","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estado","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tel�fono","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Apoderado Legal","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Alta Universidad","celda01",ComunesPDF.CENTER);//pendiente
				pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
									
				
				while (reg.next())
				{
					String numNE 			= (reg.getString("NONAFIN") == null ? "" : reg.getString("NONAFIN"));
					String grupo 			= (reg.getString("GRUPO") == null ? "" : reg.getString("GRUPO"));
					String sUniver			= (reg.getString("RAZON_SOCIAL") == null ? "" : reg.getString("RAZON_SOCIAL"));
					String sCampus			= (reg.getString("CAMPUS")== null) ? "": reg.getString("CAMPUS");
					String sDomicilio 	= (reg.getString("DOMICILIO") == null ? "" : reg.getString("DOMICILIO"));
					String sEstado			= (reg.getString("ESTADO") == null ? "" : reg.getString("ESTADO"));
					String iTelefono 		= (reg.getString("TELEFONO") == null ? "" : reg.getString("TELEFONO"));
					String sApoderado 	= (reg.getString("APODERADO") == null ? "" : reg.getString("APODERADO"));
					String sFchAlta 		= (reg.getString("ALTA") == null ? "" : reg.getString("ALTA"));
					String sEstatus 		= (reg.getString("ESTATUS") == null ? "" : reg.getString("ESTATUS"));
					
					pdfDoc.setCell(numNE,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(grupo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(sUniver,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(sCampus,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(sDomicilio,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(sEstado,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(iTelefono,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(sApoderado,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(sFchAlta,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(sEstatus,"formas",ComunesPDF.CENTER);
			
				}	
								
				pdfDoc.addTable();
				pdfDoc.endDocument();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo",e);
			}finally{
				try{
				}catch(Exception e){}
			}
		} // if (tipo)	
		
		return  nombreArchivo;
	}
	
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	/*Este metodo se utiliza para crear archivos segun su tipo (PDF, CSV) SIN paginador*/
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet reg, String path, String tipo) {

		String nombreArchivo = "";
		
		if("CSV".equals(tipo))
		{
				String linea = "";
				OutputStreamWriter writer = null;
				BufferedWriter buffer = null;
				StringBuffer 	contenidoArchivo 	= new StringBuffer();
				CreaArchivo 	archivo 			= new CreaArchivo();
				String temp = "", temp1 = "";
					
				try {
				
					nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
					writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
					buffer = new BufferedWriter(writer);
					buffer.write(linea);
					
					
					linea="N�mero de Nafin Electr�nico,Grupo,Nombre Universidad,Nombre Campus,Domicilio,Estado,Tel�fono,Apoderado Legal,Fecha de Alta Universidad,Estatus,"+"\n";
										
					buffer.write(linea);
									
					while (reg.next()) 
					{
						String numNE 			= (reg.getString("NONAFIN") == null ? "" : reg.getString("NONAFIN"));
						String grupo 			= (reg.getString("GRUPO") == null ? "" : reg.getString("GRUPO"));
						String sUniver			= (reg.getString("RAZON_SOCIAL") == null ? "" : reg.getString("RAZON_SOCIAL"));
						String sCampus			= (reg.getString("CAMPUS")== null) ? "": reg.getString("CAMPUS");
						String sDomicilio 	= (reg.getString("DOMICILIO") == null ? "" : reg.getString("DOMICILIO"));
						String sEstado			= (reg.getString("ESTADO") == null ? "" : reg.getString("ESTADO"));
						String iTelefono 		= (reg.getString("TELEFONO") == null ? "" : reg.getString("TELEFONO"));
						String sApoderado 	= (reg.getString("APODERADO") == null ? "" : reg.getString("APODERADO"));
						String sFchAlta 		= (reg.getString("ALTA") == null ? "" : reg.getString("ALTA"));
						String sEstatus 		= (reg.getString("ESTATUS") == null ? "" : reg.getString("ESTATUS"));
						
						linea =	numNE.replaceAll(",", "")+","+
									grupo.replaceAll(",", "")+","+
									sUniver.replaceAll(",", "")+","+
									sCampus.replaceAll(",", "")+","+ 
									sDomicilio.replaceAll(",", "")+","+
									sEstado.replaceAll(",", "")+","+"'"+
									iTelefono.replaceAll(",", "")+","+
									sApoderado.replaceAll(",", "")+","+
									sFchAlta+","+
									sEstatus.replaceAll(",", "")+"\n";
								
						buffer.write(linea);
					}

					buffer.close();
				} catch (Throwable e) 
				{
					throw new AppException("Error al generar el archivo ", e);
				} 
		}
			return  nombreArchivo;
	}
	
	
	
	/*****************************************************
									SETTERS         
	*******************************************************/
	
	public void setNumeroNE (String numeroNE) { this.numeroNE = numeroNE; }
	
	public void setCgrupo (String cgrupo) {	this.cgrupo = cgrupo; 	}
	
	public void setUniversidad(String sUniversidad) { this.sUniversidad = sUniversidad; }
	
	public void setCampus (String sCampus) { this.sCampus = sCampus ; }
	
	public void setEstado (String cEstado) { this.cEstado = cEstado; }
	
	
	/*****************************************************
								GETTERS
	*******************************************************/
	public String getNumeroNE() { return numeroNE; }

	public String getCgrupo() {	return cgrupo; }
	
	public String getUniversidad() { return sUniversidad; }
	
	public String getCampus() { return sCampus; }
	
	public String getEstado() { return cEstado; }
	
	public List getConditions() {  return conditions;  }  
  
	

	

}