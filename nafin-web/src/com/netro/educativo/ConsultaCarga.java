package com.netro.educativo;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.sql.ResultSetMetaData;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsultaCarga implements IQueryGeneratorRegExtJS {
	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaCarga.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
		
	private String grupo="";
	private String universidad="";
	private String folioCarga="";
	private String folioOper="";
	private String credito="";
	private String rfc="";
	private String matricula="";
	private String estatus="";
	private String fechaCargaIni="";
	private String fechaCargaFin="";
	private String icIF="";
	
	public ConsultaCarga() {
	}
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();

		conditions 		= new ArrayList();

		qrySentencia.append(
					
			"select sum(ce.FN_IMP_DISPOSICION) as  disp,sum(ce.FN_FINANCIAMIENTO) as monto,sum (ce.FN_SALDOLINEAS) as saldo_lineas ,\n" + 
			"             est.CD_DESCRIPCION AS ESTATUS ,count(* ) as numero\n" + 
			"             from ce_contenido_educativo ce,COMCAT_UNIVERSIDAD catu,\n" + 
			"             CECAT_ESTATUS_CRED_EDUCA est\n" + 
			"              where catu.IC_UNIVERSIDAD=ce.IC_UNIVERSIDAD\n" + 
			"              and ce.IC_ESTATUS_CRED_EDUCA=est.IC_ESTATUS_CRED_EDUCA  and ce.IC_IF=?\n"
		);
		conditions.add(this.getIcIF());
	if(!grupo.equals("")){
			qrySentencia.append(" and catu.IC_GRUPO = ? ");
			conditions.add(grupo);
		}
		if(!universidad.equals("")){
			qrySentencia.append(" and ce.IC_UNIVERSIDAD = ? ");
			conditions.add(universidad);
		}
		if(!folioCarga.equals("")){
			qrySentencia.append(" and ce.IC_FOLIO_CARGA=? ");
			conditions.add(folioCarga);
		}
		if(!folioOper.equals("")){
			qrySentencia.append(" and ce.IC_FOLIO_OPERACION=? ");
			conditions.add(folioOper);
		}
		if(!credito.equals("")){
			qrySentencia.append(" and ce.CG_FINANCIAMIENTO=? ");
			conditions.add(credito);
		}
		if(!rfc.equals("")){
			qrySentencia.append(" and ce.CG_REGISTRO=? ");
			conditions.add(rfc);
		}
		if(!matricula.equals("")){
			qrySentencia.append(" and ce.CG_MATRICULA=? ");
			conditions.add(matricula);
		}
		if(!estatus.equals("")){
			qrySentencia.append(" and ce.ic_estatus_cred_educa = ? ");
			conditions.add(estatus);
		}
		else
		{
			qrySentencia.append(" and ce.IC_ESTATUS_CRED_EDUCA in(1,2,3) ");
		}
		
		if(!fechaCargaIni.equals("")&&!fechaCargaFin.equals("")){
			qrySentencia.append(" and ce.FN_FECHA_CARGA >=  to_date(?, 'dd/mm/yyyy')  ") ;
			qrySentencia.append("	and ce.FN_FECHA_CARGA < to_date(?, 'dd/mm/yyyy')+1  ") ;
			conditions.add(fechaCargaIni);
			conditions.add(fechaCargaFin);
		}
		qrySentencia.append("	and  not exists (select 1  from gti_estatus_solic where IC_ESTATUS_CRED_EDUCA <>2 and ic_folio=ce.IC_FOLIO_OPERACION )");
		qrySentencia.append("	group by est.CD_DESCRIPCION") ;
		
		
		log.info("qrySentencia  "+qrySentencia.toString());
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();
	}
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		qrySentencia.append(		"select    ce.IC_REGISTRO,ce.IC_UNIVERSIDAD,catu.IC_GRUPO\n" + 
		"     from ce_contenido_educativo ce,COMCAT_UNIVERSIDAD catu\n" + 
		"     where catu.IC_UNIVERSIDAD=ce.IC_UNIVERSIDAD and ce.IC_IF=? ");
		conditions.add(this.getIcIF());
		
		if(!grupo.equals("")){
			qrySentencia.append(" and catu.IC_GRUPO=? ");
			conditions.add(grupo);
		}
		if(!universidad.equals("")){
			qrySentencia.append(" and ce.IC_UNIVERSIDAD=? ");
			conditions.add(universidad);
		}
		if(!folioCarga.equals("")){
			qrySentencia.append(" and ce.IC_FOLIO_CARGA=? ");
			conditions.add(folioCarga);
		}
		if(!folioOper.equals("")){
			qrySentencia.append(" and ce.IC_FOLIO_OPERACION=? ");
			conditions.add(folioOper);
		}
		if(!credito.equals("")){
			qrySentencia.append(" and ce.CG_FINANCIAMIENTO=? ");
			conditions.add(credito);
		}
		if(!rfc.equals("")){
			qrySentencia.append(" and ce.CG_REGISTRO=? ");
			conditions.add(rfc);
		}
		if(!matricula.equals("")){
			qrySentencia.append(" and ce.CG_MATRICULA=? ");
			conditions.add(matricula);
		}
		if(!estatus.equals("")){
			qrySentencia.append(" and ce.ic_estatus_cred_educa = ? ");
			conditions.add(estatus);
		}else{
			qrySentencia.append(" and ce.IC_ESTATUS_CRED_EDUCA in(1,2,3) ");
		
		}
		
		if(!fechaCargaIni.equals("")&&!fechaCargaFin.equals("")){
			qrySentencia.append(" and ce.FN_FECHA_CARGA >=  to_date(?, 'dd/mm/yyyy')  ") ;
			qrySentencia.append("and ce.FN_FECHA_CARGA < to_date(?, 'dd/mm/yyyy')+1  ") ;
			conditions.add(fechaCargaIni);
			conditions.add(fechaCargaFin);
		}
		qrySentencia.append("	and  not exists (select 1  from gti_estatus_solic where IC_ESTATUS_CRED_EDUCA <>2 and ic_folio=ce.IC_FOLIO_OPERACION )");
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		qrySentencia.append("select CG.CD_DESCRIPCION AS GRUPO, CATU.CG_RAZON_SOCIAL as UNIVERSIDAD, " );
		qrySentencia.append("to_char(ce.FN_FECHA_CARGA,'dd/mm/yyyy HH:mm:ss') AS FECHA_CARGA,\n" );
		qrySentencia.append("ce.IC_FOLIO_CARGA AS FOLIO_CARGA,\n");
		qrySentencia.append("(CASE WHEN ce.IC_ESTATUS_CRED_EDUCA=2 THEN TO_CHAR(ce.IC_FOLIO_OPERACION) ELSE 'N/A' END )AS FOLIO_OPER,\n");
		qrySentencia.append("ce.CG_FINANCIAMIENTO AS IC_CREDITO ,\n");
		qrySentencia.append("ce.CG_RAZON_SOCIAL AS NOMBRE ,\n");
		qrySentencia.append("ce.CG_REGISTRO AS RFC,    \n");
		qrySentencia.append("ce.CG_MATRICULA AS MATRICULA,\n");
		qrySentencia.append("ce.CG_DESC_ESTUDIOS AS ESTUDIOS,\n" );
		qrySentencia.append("to_char(ce.DF_INGRESO,'dd/mm/yyyy') AS FECHA_INGRESO,\n");
		qrySentencia.append("ce.CG_ANIO AS ANIO,\n");
		qrySentencia.append("ce.IC_PERIODO AS PERIODO,\n" );
		qrySentencia.append("ce.CG_MODALIDAD AS MODALIDAD,\n");
		qrySentencia.append("catu.CG_NOMBRE_CAMPUS AS CAMPUS,\n");
		qrySentencia.append("to_char(ce.DF_DISPOSICION,'dd/mm/yyyy') AS FECHA_DISPO,    \n");
		qrySentencia.append("ce.IC_NUM_DISPOSICION AS NUM_DISPO,\n");
		qrySentencia.append("ce.FN_IMP_DISPOSICION as IMPORTE_DISP, \n");
		qrySentencia.append("ce.FN_FINANCIAMIENTO MONTO_TOTAL,\n");
		qrySentencia.append("ce.FN_SALDOLINEAS SALDO_LINEAS,\n");
		qrySentencia.append("est.CD_DESCRIPCION AS ESTATUS, \n");
		qrySentencia.append("(CASE WHEN ce.IC_ESTATUS_CRED_EDUCA=3 THEN ce.CG_CAUSA_RECHAZO else '' end) as CAUSA, \n");
		qrySentencia.append("acu.ic_usuario||'  '||acu.CG_NOMBRE_USUARIO as DES_USUARIO    \n" );
		qrySentencia.append("from ce_contenido_educativo ce, COMCAT_UNIVERSIDAD catu, \n");
		qrySentencia.append("CECAT_ESTATUS_CRED_EDUCA est, CE_ACUSE_EDUCATIVO_VAL acu,CECAT_GRUPO CG   \n");
		qrySentencia.append("where catu.IC_UNIVERSIDAD=ce.IC_UNIVERSIDAD \n");
		qrySentencia.append("AND catu.IC_GRUPO = CG.IC_GRUPO ");
		qrySentencia.append("and ce.IC_ESTATUS_CRED_EDUCA=est.IC_ESTATUS_CRED_EDUCA and ce.CC_ACUSE=acu.CC_ACUSE(+)" );
		
		
		for(int i=0;i<pageIds.size();i++) {
				List lItem = (ArrayList)pageIds.get(i);
				if(i==0) {
					qrySentencia.append("  AND  ( "); 
				} else {
					qrySentencia.append("  OR  "); 
				}
				qrySentencia.append("  (ce.IC_REGISTRO=? and ce.IC_UNIVERSIDAD=? and catu.IC_GRUPO=?   )  "); 

				conditions.add(new Long(lItem.get(0).toString()));
				conditions.add(new Long(lItem.get(1).toString()));
				conditions.add(new Long(lItem.get(2).toString()));
				//conditions.add(new Long(lItem.get(2).toString()));
				//conditions.add(new Long(lItem.get(3).toString()));
		}//for(int i=0;i<ids.size();i++)
					qrySentencia.append("  ) "); 
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
	
	
		qrySentencia.append(	"select CG.CD_DESCRIPCION AS GRUPO, CATU.CG_RAZON_SOCIAL as UNIVERSIDAD, "+
		"    to_char(ce.FN_FECHA_CARGA,'dd/mm/yyyy HH:mm:ss') AS FECHA_CARGA,\n" + 
		"    ce.IC_FOLIO_CARGA AS FOLIO_CARGA,\n" + 
		"    (CASE WHEN ce.IC_ESTATUS_CRED_EDUCA=2 THEN TO_CHAR(ce.IC_FOLIO_OPERACION) ELSE 'N/A' END )AS FOLIO_OPER,\n" + 
		"    ce.CG_FINANCIAMIENTO AS IC_CREDITO ,\n" + 
		"    ce.CG_RAZON_SOCIAL AS NOMBRE ,\n" + 
		"    ce.CG_REGISTRO AS RFC,    \n" + 
		"    ce.CG_MATRICULA AS MATRICULA,\n" + 
		"    ce.CG_DESC_ESTUDIOS AS ESTUDIOS,\n" + 
		"    to_char(ce.DF_INGRESO,'dd/mm/yyyy') AS FECHA_INGRESO,\n" + 
		"    ce.CG_ANIO AS ANIO,\n" + 
		"    ce.IC_PERIODO AS PERIODO,\n" + 
		"    ce.CG_MODALIDAD AS MODALIDAD,\n" + 
		"    catu.CG_NOMBRE_CAMPUS AS CAMPUS,\n" + 
		"    to_char(ce.DF_DISPOSICION,'dd/mm/yyyy') AS FECHA_DISPO,    \n" + 
		"    ce.IC_NUM_DISPOSICION AS NUM_DISPO,\n" + 
		"    ce.FN_IMP_DISPOSICION as IMPORTE_DISP, \n" + 
		"    ce.FN_FINANCIAMIENTO MONTO_TOTAL,\n" + 
		"    ce.FN_SALDOLINEAS SALDO_LINEAS,\n" + 
		"    est.CD_DESCRIPCION AS ESTATUS, \n" + 
		"    (CASE WHEN ce.IC_ESTATUS_CRED_EDUCA=3 THEN ce.CG_CAUSA_RECHAZO else '' end) as CAUSA\n" + 
		"    ,acu.ic_usuario||'  '||acu.CG_NOMBRE_USUARIO as DES_USUARIO    \n" + 
		"        from ce_contenido_educativo ce,COMCAT_UNIVERSIDAD catu \n" + 
		"        ,CECAT_ESTATUS_CRED_EDUCA est,CE_ACUSE_EDUCATIVO_VAL acu, CECAT_GRUPO CG   \n" + 
		"        where catu.IC_UNIVERSIDAD=ce.IC_UNIVERSIDAD AND catu.IC_GRUPO = CG.IC_GRUPO \n" + 
		"         and ce.IC_ESTATUS_CRED_EDUCA=est.IC_ESTATUS_CRED_EDUCA and ce.CC_ACUSE=acu.CC_ACUSE(+)\n"+
		"		and ce.IC_IF=? 	");
		conditions.add(this.getIcIF());
		if(!grupo.equals("")){
			qrySentencia.append(" and catu.IC_GRUPO=? ");
			conditions.add(grupo);
		}
		if(!universidad.equals("")){
			qrySentencia.append(" and ce.IC_UNIVERSIDAD=? ");
			conditions.add(universidad);
		}
		if(!folioCarga.equals("")){
			qrySentencia.append(" and ce.IC_FOLIO_CARGA=? ");
			conditions.add(folioCarga);
		}
		if(!folioOper.equals("")){
			qrySentencia.append(" and ce.IC_FOLIO_OPERACION=? ");
			conditions.add(folioOper);
		}
		if(!credito.equals("")){
			qrySentencia.append(" and ce.CG_FINANCIAMIENTO=? ");
			conditions.add(credito);
		}
		if(!rfc.equals("")){
			qrySentencia.append(" and ce.CG_REGISTRO=? ");
			conditions.add(rfc);
		}
		if(!matricula.equals("")){
			qrySentencia.append(" and ce.CG_MATRICULA=? ");
			conditions.add(matricula);
		}
		if(!estatus.equals("")){
			qrySentencia.append(" and ce.ic_estatus_cred_educa = ? ");
			conditions.add(estatus);
		}
		else{
			qrySentencia.append(" and ce.IC_ESTATUS_CRED_EDUCA in(1,2,3) ");
		
		}
		
		if(!fechaCargaIni.equals("")&&!fechaCargaFin.equals("")){
			qrySentencia.append(" and ce.FN_FECHA_CARGA >=  to_date(?, 'dd/mm/yyyy')  ") ;
			qrySentencia.append("and ce.FN_FECHA_CARGA < to_date(?, 'dd/mm/yyyy')+1  ") ;
			conditions.add(fechaCargaIni);
			conditions.add(fechaCargaFin);
		}
		qrySentencia.append("	and  not exists (select 1  from gti_estatus_solic where IC_ESTATUS_CRED_EDUCA <>2 and ic_folio=ce.IC_FOLIO_OPERACION )");
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
	
		

		try {		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return "";
					
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		String linea = "";
		String nombreArchivo = "";
		String espacio = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		try {
		 if(tipo.equals("CSV")) {
		  linea = "Fecha y hora de Carga,Folio de Carga,Folio de Operaci�n,Cr�dito,Nombre del Acreditado,RFC,Matricula,"+
		  "Estudios,Fecha de Ingreso, A�o de colocaci�n,"+
		  "Periodo Educativo,Modalidad Programa,Grupo,Universidad,Campus,Fecha �ltima disposici�n,No. Disposici�n,"+
		  "Importe Disposici�n,Monto Total,Saldo de L�nea,Estatus,Observaciones,Usuario\n";
		  nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
		  writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
		  buffer = new BufferedWriter(writer);
			buffer.write(linea);
			int rowcount = 0;
			ResultSetMetaData rsmd = rs.getMetaData();
			String name = rsmd.getColumnName(1);
		  while (rs.next()) {
					rowcount++;
					String dateCarga = (rs.getString("fecha_carga") == null) ? "" : rs.getString("fecha_carga");
					String folioCarga = (rs.getString("FOLIO_CARGA") == null) ? "" : "'"+rs.getString("FOLIO_CARGA");
					String folioOper = (rs.getString("FOLIO_OPER") == null) ? "" : "'"+rs.getString("FOLIO_OPER");
					String credito = (rs.getString("IC_CREDITO") == null) ? "" : rs.getString("IC_CREDITO");
					String nombre = (rs.getString("NOMBRE") == null) ? "" : rs.getString("NOMBRE");
					String rfc = (rs.getString("RFC") == null) ? "" : rs.getString("RFC");
					String matricula = (rs.getString("MATRICULA") == null) ? "" : rs.getString("MATRICULA");
					String estudios = (rs.getString("ESTUDIOS") == null) ? "" : rs.getString("ESTUDIOS");
					String fechaIngreso = (rs.getString("FECHA_INGRESO") == null) ? "" : rs.getString("FECHA_INGRESO");
					String annio = (rs.getString("ANIO") == null) ? "" : rs.getString("ANIO");
					String periodo = (rs.getString("PERIODO") == null) ? "" : rs.getString("PERIODO");
					String modProg = (rs.getString("MODALIDAD") == null) ? "" : rs.getString("MODALIDAD");
					String grupo = (rs.getString("GRUPO") == null) ? "" : rs.getString("GRUPO");
					String universidad = (rs.getString("UNIVERSIDAD") == null) ? "" : rs.getString("UNIVERSIDAD");
					String campus = (rs.getString("CAMPUS") == null) ? "" : rs.getString("CAMPUS");
					String fechaDisp = (rs.getString("FECHA_DISPO") == null) ? "" : rs.getString("FECHA_DISPO");
					String numDisp = (rs.getString("NUM_DISPO") == null) ? "" : rs.getString("NUM_DISPO");
					String importeDisp = (rs.getString("IMPORTE_DISP") == null) ? "" : rs.getString("IMPORTE_DISP");
					String montoTotal = (rs.getString("MONTO_TOTAL") == null) ? "" : rs.getString("MONTO_TOTAL");
					String saldoLinea = (rs.getString("SALDO_LINEAS") == null) ? "" : rs.getString("SALDO_LINEAS");
					String estatus = (rs.getString("ESTATUS") == null) ? "" : rs.getString("ESTATUS");
					String observaciones = (rs.getString("CAUSA") == null) ? "" : rs.getString("CAUSA");
					String usuario = (rs.getString("DES_USUARIO") == null) ? "" : rs.getString("DES_USUARIO");
					
					
					linea = dateCarga + ", " + 
									folioCarga + ", " +
									folioOper + " , " +
									credito + ", " +
									nombre + ", " +
									rfc + ", " +
									matricula + ", " +
									estudios+ ", " +
									fechaIngreso + ", " +
									annio + ", " +
									periodo+ ", " +
									modProg + ", " +
									grupo.replaceAll(","," ") + ", " +
									universidad.replaceAll(","," ") + ", " +
									campus.replaceAll(","," ") + ", " +
									fechaDisp + ", " +
									numDisp + ", " +
									importeDisp + ", " +
									montoTotal + ", " +
									saldoLinea + ", " +
									estatus.replaceAll(","," ") + ", " +
									observaciones.replaceAll(","," ") + ", " +
									usuario.replaceAll(","," ") +"\n" ;
												 
					buffer.write(linea);
		  }
		  buffer.close();
		 }
		 }catch (Throwable e) {
			throw new AppException("Error al generar el archivo",e);
		}
		finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
	return nombreArchivo;
	}


	public void setPaginaOffset(String paginaOffset) {
		this.paginaOffset = paginaOffset;
	}


	public String getPaginaOffset() {
		return paginaOffset;
	}


	public void setPaginaNo(String paginaNo) {
		this.paginaNo = paginaNo;
	}


	public String getPaginaNo() {
		return paginaNo;
	}
	public List getConditions(){
		return this.conditions;
	}


	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}


	public String getGrupo() {
		return grupo;
	}


	public void setUniversidad(String universidad) {
		this.universidad = universidad;
	}


	public String getUniversidad() {
		return universidad;
	}


	public void setFolioCarga(String folioCarga) {
		this.folioCarga = folioCarga;
	}


	public String getFolioCarga() {
		return folioCarga;
	}


	public void setFolioOper(String folioOper) {
		this.folioOper = folioOper;
	}


	public String getFolioOper() {
		return folioOper;
	}


	public void setCredito(String credito) {
		this.credito = credito;
	}


	public String getCredito() {
		return credito;
	}


	public void setRfc(String rfc) {
		this.rfc = rfc;
	}


	public String getRfc() {
		return rfc;
	}


	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}


	public String getMatricula() {
		return matricula;
	}


	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}


	public String getEstatus() {
		return estatus;
	}


	public void setFechaCargaIni(String fechaCargaIni) {
		this.fechaCargaIni = fechaCargaIni;
	}


	public String getFechaCargaIni() {
		return fechaCargaIni;
	}


	public void setFechaCargaFin(String fechaCargaFin) {
		this.fechaCargaFin = fechaCargaFin;
	}


	public String getFechaCargaFin() {
		return fechaCargaFin;
	}


	public void setIcIF(String icIF) {
		this.icIF = icIF;
	}


	public String getIcIF() {
		return icIF;
	}	
}