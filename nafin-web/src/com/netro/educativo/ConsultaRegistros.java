package com.netro.educativo;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsultaRegistros implements IQueryGeneratorRegExtJS {
	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaRegistros.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String intermediario="";
	private String universidad="";
	private String grupo="";
	private String estatus="";
	private String folio="";
	private String fechaValidaIni="";
	private String fechaValidaFin="";
	private String fechaOperaIni="";
	private String fechaOperaFin="";
	public ConsultaRegistros() {
	}
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		qrySentencia.append("SELECT   SUM (ccm.fn_monto_contragan) AS total_contra,\n" + 
			"         SUM (ges.fn_monto_valido) AS total_valido,\n" + 
			"         cece.cd_descripcion AS estatus, COUNT (1) AS registros\n" + 
			"    FROM gti_estatus_solic ges,\n" + 
			"         ce_confirma_monto ccm,\n" + 
			"         ce_deposito cd,\n" + 
			"         comcat_if cif,\n" + 
			"         cecat_estatus_cred_educa cece,\n" + 
			"         comcat_universidad cuni\n" + 
			" WHERE \n" + 
				"  ges.ic_folio = ccm.ic_folio_operacion\n" + 
				"   AND ges.ic_if_siag = cif.ic_if_siag  \n" + 
				"   AND ges.ic_universidad = cuni.ic_universidad \n" + 
				"   AND cd.ic_folio_operacion(+)=ges.ic_folio\n" + 
				"	and cece.ic_estatus_cred_educa=ges.ic_estatus_cred_educa "+
				"   AND ges.ic_tipo_operacion = ? \n" + 
				"   AND ges.cg_programa = ?  "); 
				conditions.add("1");
				conditions.add("E");
				
		if(!intermediario.equals("")){
			qrySentencia.append(" and cif.ic_if = ? ");
			conditions.add(intermediario);
		}
		if(!universidad.equals("")){
			qrySentencia.append(" and cuni.ic_universidad=? ");
			conditions.add(universidad);
		}
		if(!grupo.equals("")){
			qrySentencia.append(" and cuni.ic_grupo=? ");
			conditions.add(grupo);
		}
		if(!estatus.equals("")){
			qrySentencia.append(" and ges.ic_estatus_cred_educa = ? ");
			conditions.add(estatus);
		}else{
			qrySentencia.append("   AND ges.IC_ESTATUS_CRED_EDUCA in (?,?,?) ");
			conditions.add("4");
			conditions.add("6");
			conditions.add("7");
		}
		if(!folio.equals("")){
			qrySentencia.append(" and ges.ic_folio=? ");
			conditions.add(folio);
		}
		if(!fechaValidaIni.equals("")&&!fechaValidaFin.equals("")){
			qrySentencia.append(" and ccm.DF_HR_COMFMONTO >=  to_date(?, 'dd/mm/yyyy')  ") ;
			qrySentencia.append("and ccm.DF_HR_COMFMONTO < to_date(?, 'dd/mm/yyyy')+1  ") ;
			conditions.add(fechaValidaIni);
			conditions.add(fechaValidaFin);
		}
		if(!fechaOperaIni.equals("")&&!fechaOperaFin.equals("")){
			qrySentencia.append(" and cd.DF_VALIDACION >=  to_date(?, 'dd/mm/yyyy')  ") ;
			qrySentencia.append("and cd.DF_VALIDACION < to_date(?, 'dd/mm/yyyy')+1  ") ;
			conditions.add(fechaOperaIni);
			conditions.add(fechaOperaFin);
		}
				
			qrySentencia.append(" GROUP BY cece.cd_descripcion");
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		qrySentencia.append("select cuni.ic_universidad,ges.ic_folio,ges.ic_if_siag\n" + 
			" from \n" + 
				" gti_estatus_solic ges,\n" + 
				" ce_confirma_monto  ccm,\n" + 
				" ce_deposito cd,\n" + 
				" comcat_if cif,\n" + 
				" cecat_estatus_cred_educa cece,\n" + 
				" comcat_universidad cuni\n" + 
				" WHERE \n" + 
				"  ges.ic_folio = ccm.ic_folio_operacion\n" + 
				"   AND ges.ic_if_siag = cif.ic_if_siag  \n" + 
				"   AND ges.ic_universidad = cuni.ic_universidad \n" + 
				"   AND cd.ic_folio_operacion(+)=ges.ic_folio\n" + 
				"	and cece.ic_estatus_cred_educa=ges.ic_estatus_cred_educa "+
				"   AND ges.ic_tipo_operacion = ? \n" + 
				"   AND ges.cg_programa = ?  ");
				conditions.add("1");
				conditions.add("E");
				
		if(!intermediario.equals("")){
			qrySentencia.append(" and cif.ic_if = ? ");
			conditions.add(intermediario);
		}
		if(!universidad.equals("")){
			qrySentencia.append(" and cuni.ic_universidad=? ");
			conditions.add(universidad);
		}
		if(!grupo.equals("")){
			qrySentencia.append(" and cuni.ic_grupo=? ");
			conditions.add(grupo);
		}
		if(!estatus.equals("")){
			qrySentencia.append(" and ges.ic_estatus_cred_educa = ? ");
			conditions.add(estatus);
		}else{
			qrySentencia.append("   AND ges.IC_ESTATUS_CRED_EDUCA in (?,?,?) ");
			conditions.add("4");
			conditions.add("6");
			conditions.add("7");
		}
		if(!folio.equals("")){
			qrySentencia.append(" and ges.ic_folio=? ");
			conditions.add(folio);
		}
		if(!fechaValidaIni.equals("")&&!fechaValidaFin.equals("")){
			qrySentencia.append(" and ccm.DF_HR_COMFMONTO >=  to_date(?, 'dd/mm/yyyy')  ") ;
			qrySentencia.append("and ccm.DF_HR_COMFMONTO < to_date(?, 'dd/mm/yyyy')+1  ") ;
			conditions.add(fechaValidaIni);
			conditions.add(fechaValidaFin);
		}
		if(!fechaOperaIni.equals("")&&!fechaOperaFin.equals("")){
			qrySentencia.append(" and cd.DF_VALIDACION >=  to_date(?, 'dd/mm/yyyy')  ") ;
			qrySentencia.append("and cd.DF_VALIDACION < to_date(?, 'dd/mm/yyyy')+1  ") ;
			conditions.add(fechaOperaIni);
			conditions.add(fechaOperaFin);
		}
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		qrySentencia.append("select cif.cg_razon_social as cd_if,\n" + 
				" cuni.cg_razon_social as universidad, \n" + 
				" cuni.cg_nombre_campus as campus,\n" + 
				" ges.ic_folio||' ' as ic_folio, \n" + 
				" TO_CHAR (ges.df_fecha_hora, 'dd/mm/yyyy HH24:MI:SS')  as fecha_oper,\n" + 
				" ges.fn_monto_valido AS fn_financiamiento,\n" + 
				" ges.in_registros_acep,\n" + 
				" ges.in_registros_rech,\n" + 
				" ges.in_registros,\n" + 
				" ccm.fn_monto_contragan,\n" + 
				" cece.cd_descripcion as estatus, \n" + 
				" cd.cg_causa_rechazo, \n" + 
				" TO_CHAR (cd.df_deposito, 'dd/mm/yyyy') as df_deposito ,\n" + 
				" cd.cg_nombre_banco,\n" +
				" cd.cg_clave_referencia,\n" + 
				" cd.fn_importe\n" + 
				" from \n" + 
				" gti_estatus_solic ges,\n" + 
				" ce_confirma_monto  ccm,\n" + 
				" ce_deposito cd,\n" + 
				" comcat_if cif,\n" + 
				" cecat_estatus_cred_educa cece,\n" + 
				" comcat_universidad cuni\n" + 
				" WHERE \n" + 
		
				"  ges.ic_folio = ccm.ic_folio_operacion\n" + 
				"   AND ges.ic_if_siag = cif.ic_if_siag  \n" + 
				"	and cece.ic_estatus_cred_educa=ges.ic_estatus_cred_educa "+
				"   AND ges.ic_universidad = cuni.ic_universidad \n" + 
				"   AND cd.ic_folio_operacion(+)=ges.ic_folio\n"  );
		
		
		for(int i=0;i<pageIds.size();i++) {
				List lItem = (ArrayList)pageIds.get(i);
				if(i==0) {
					qrySentencia.append("  AND  ( "); 
				} else {
					qrySentencia.append("  OR  "); 
				}
				qrySentencia.append("  (cuni.ic_universidad=? and ges.ic_folio=? and ges.ic_if_siag=? )  "); 

				conditions.add(new Long(lItem.get(0).toString()));
				conditions.add(new Long(lItem.get(1).toString()));
				conditions.add(new Long(lItem.get(2).toString()));
		}//for(int i=0;i<ids.size();i++)
					qrySentencia.append("  ) "); 
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
	
	
		qrySentencia.append("select cif.cg_razon_social as cd_if,\n" + 
				" cuni.cg_razon_social as universidad, \n" + 
				" cuni.cg_nombre_campus as campus,\n" + 
				" ges.ic_folio, \n" + 
				" TO_CHAR (ges.df_fecha_hora, 'dd/mm/yyyy HH24:MI:SS')  as fecha_oper,\n" + 
				" ges.fn_monto_valido AS fn_financiamiento,\n" + 
				" ges.in_registros_acep,\n" + 
				" ges.in_registros_rech,\n" + 
				" ges.in_registros,\n" + 
				" ccm.fn_monto_contragan,\n" + 
				" cece.cd_descripcion as estatus, \n" + 
				" cd.cg_causa_rechazo, \n" + 
				" cd.df_deposito,\n" + 
				" cd.cg_nombre_banco,\n" +
				" cd.cg_clave_referencia,\n" + 
				" cd.fn_importe\n" + 
				" from \n" + 
				" gti_estatus_solic ges,\n" + 
				" ce_confirma_monto  ccm,\n" + 
				" ce_deposito cd,\n" + 
				" comcat_if cif,\n" + 
				" cecat_estatus_cred_educa cece,\n" + 
				" comcat_universidad cuni\n" + 
				" WHERE \n" + 
				"  ges.ic_folio = ccm.ic_folio_operacion\n" + 
				"   AND ges.ic_if_siag = cif.ic_if_siag  \n" + 
				"   AND ges.ic_universidad = cuni.ic_universidad \n" + 
				"   AND cd.ic_folio_operacion(+)=ges.ic_folio\n" + 
				"	and cece.ic_estatus_cred_educa=ges.ic_estatus_cred_educa "+
				"   AND ges.ic_tipo_operacion = ? \n" + 
				"   AND ges.cg_programa = ?  ");
				conditions.add("1");
				conditions.add("E");
				
		if(!intermediario.equals("")){
			qrySentencia.append(" and cif.ic_if = ? ");
			conditions.add(intermediario);
		}
		if(!universidad.equals("")){
			qrySentencia.append(" and cuni.ic_universidad=? ");
			conditions.add(universidad);
		}
		if(!grupo.equals("")){
			qrySentencia.append(" and cuni.ic_grupo=? ");
			conditions.add(grupo);
		}
		if(!estatus.equals("")){
			qrySentencia.append(" and ges.ic_estatus_cred_educa = ? ");
			conditions.add(estatus);
		}else{
			qrySentencia.append("   AND ges.IC_ESTATUS_CRED_EDUCA in (?,?,?) ");
			conditions.add("4");
			conditions.add("6");
			conditions.add("7");
		}
		if(!folio.equals("")){
			qrySentencia.append(" and ges.ic_folio=? ");
			conditions.add(folio);
		}
		if(!fechaValidaIni.equals("")&&!fechaValidaFin.equals("")){
			qrySentencia.append(" and ccm.DF_HR_COMFMONTO >=  to_date(?, 'dd/mm/yyyy')  ") ;
			qrySentencia.append("and ccm.DF_HR_COMFMONTO < to_date(?, 'dd/mm/yyyy')+1  ") ;
			conditions.add(fechaValidaIni);
			conditions.add(fechaValidaFin);
		}
		if(!fechaOperaIni.equals("")&&!fechaOperaFin.equals("")){
			qrySentencia.append(" and cd.DF_VALIDACION >=  to_date(?, 'dd/mm/yyyy')  ") ;
			qrySentencia.append("and cd.DF_VALIDACION < to_date(?, 'dd/mm/yyyy')+1  ") ;
			conditions.add(fechaOperaIni);
			conditions.add(fechaOperaFin);
		}
	
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
	
		

		try {
		
		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return "";
					
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		String linea = "";
		String nombreArchivo = "";
		String espacio = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		try {
		 if(tipo.equals("CSV")) {
		    linea = "Intermediario Financiero,Universidad,Campus,Fecha y hora de la operación,Folio,Monto enviado,No Operaciones Aceptadas,"+
		    "No Operaciones con errores,Total de Operaciones, Monto de Contragarantía a pagar,"+
		    "Estatus,Causas de Rechazo,Fecha de Depósito,Importe Depositado,"+
		    "Banco De Deposito,Clave de Referencia\n";
		    nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
		    writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
		    buffer = new BufferedWriter(writer);
		    buffer.write(linea);

		  while (rs.next()) {
					
		     String cdIF        = (rs.getString("cd_if")               == null) ? "" : rs.getString("cd_if");
		     String uni         = (rs.getString("universidad")         == null) ? "" : rs.getString("universidad");
		     String campus      = (rs.getString("campus")              == null) ? "" : rs.getString("campus");
		     String fechaOper   = (rs.getString("fecha_oper")          == null) ? "" : rs.getString("fecha_oper");
		     String folio       = (rs.getString("ic_folio")            == null) ? "" : rs.getString("ic_folio");
		     String montoContra = (rs.getString("fn_monto_contragan")  == null) ? "" : rs.getString("fn_monto_contragan");
		     String aceptados   = (rs.getString("in_registros_acep")   == null) ? "" : rs.getString("in_registros_acep");
		     String rechazados  = (rs.getString("in_registros_rech")   == null) ? "" : rs.getString("in_registros_rech");
		     String registros   = (rs.getString("in_registros")        == null) ? "" : rs.getString("in_registros");
		     String finan       = (rs.getString("fn_financiamiento")   == null) ? "" : rs.getString("fn_financiamiento");
		     String estatus     = (rs.getString("estatus")             == null) ? "" : rs.getString("estatus");
		     String causaRec    = (rs.getString("cg_causa_rechazo")    == null) ? "" : rs.getString("cg_causa_rechazo");
		     String deposito    = (rs.getString("df_deposito")         == null) ? "" : rs.getString("df_deposito");
		     String importe     = (rs.getString("fn_importe")          == null) ? "" : rs.getString("fn_importe");
		     String banco       = (rs.getString("cg_nombre_banco")     == null) ? "" : rs.getString("cg_nombre_banco");
		     String ref         = (rs.getString("cg_clave_referencia") == null) ? "" : rs.getString("cg_clave_referencia");

		     linea = cdIF.replace(',',' ')      + ", " +
		           uni.replace(',',' ')         + ", " +
		           campus.replace(',',' ')      + ", " +
		           fechaOper.replace(',',' ')   + ",'" +
		           folio.replace(',',' ')       + ", " +
		           finan.replace(',',' ')       + ", " +
		           aceptados.replace(',',' ')   + ", " +
		           rechazados.replace(',',' ')  + ", " +
		           registros.replace(',',' ')   + ", " +
		           montoContra.replace(',',' ') + ", " +
		           estatus.replace(',',' ')     + ", " +
		           causaRec.replaceAll("\r", "").replaceAll("\n", "")  + ", " +
		           deposito.replace(',',' ')    + ", " +
		           importe.replace(',',' ')     + ", " +
		           banco.replace(',',' ')       + ", " +
		           ref.replace(',',' ')         + "\n" ;

					buffer.write(linea);
		  }
		  buffer.close();
		 }
		 }catch (Throwable e) {
			throw new AppException("Error al generar el archivo",e);
		}
		finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
	return nombreArchivo;
	}


	public void setIntermediario(String intermediario) {
		this.intermediario = intermediario;
	}


	public String getIntermediario() {
		return intermediario;
	}


	public void setUniversidad(String universidad) {
		this.universidad = universidad;
	}


	public String getUniversidad() {
		return universidad;
	}


	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}


	public String getGrupo() {
		return grupo;
	}


	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}


	public String getEstatus() {
		return estatus;
	}


	public void setFolio(String folio) {
		this.folio = folio;
	}


	public String getFolio() {
		return folio;
	}






	public void setPaginaOffset(String paginaOffset) {
		this.paginaOffset = paginaOffset;
	}


	public String getPaginaOffset() {
		return paginaOffset;
	}


	public void setPaginaNo(String paginaNo) {
		this.paginaNo = paginaNo;
	}


	public String getPaginaNo() {
		return paginaNo;
	}
	public List getConditions(){
		return this.conditions;
	}


	public void setFechaValidaIni(String fechaValidaIni) {
		this.fechaValidaIni = fechaValidaIni;
	}


	public String getFechaValidaIni() {
		return fechaValidaIni;
	}


	public void setFechaValidaFin(String fechaValidaFin) {
		this.fechaValidaFin = fechaValidaFin;
	}


	public String getFechaValidaFin() {
		return fechaValidaFin;
	}


	public void setFechaOperaIni(String fechaOperaIni) {
		this.fechaOperaIni = fechaOperaIni;
	}


	public String getFechaOperaIni() {
		return fechaOperaIni;
	}


	public void setFechaOperaFin(String fechaOperaFin) {
		this.fechaOperaFin = fechaOperaFin;
	}


	public String getFechaOperaFin() {
		return fechaOperaFin;
	}
}