package com.netro.educativo;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsultaValidaGarantia implements IQueryGeneratorRegExtJS {
	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaValidaGarantia.class);
	private String paginaOffset;
	private String paginaNo;
	private List 	conditions;
	StringBuffer	qrySentencia	= new StringBuffer("");
	private String intermediario;
	private String universidad;
	private String grupo;
	private String folio;

	public ConsultaValidaGarantia() {
	}

	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		qrySentencia.append(
			"SELECT	COUNT (1) AS registros, "+
			"			SUM (gti.fn_monto_valido) AS total_importe, "+
			"			SUM (ccm.fn_monto_contragan) AS total_contra "+
			"  FROM gti_estatus_solic gti, "+
			"       ce_confirma_monto ccm, "+
			"       comcat_if cif, "+
			"       comcat_universidad cu "+
			" WHERE gti.ic_folio = ccm.ic_folio_operacion "+
			"   AND gti.ic_if_siag = cif.ic_if_siag  "+
			"   AND gti.ic_universidad = cu.ic_universidad "+
			"   AND gti.ic_tipo_operacion = ? "+
			"   AND gti.cg_programa = ?  "+
			"	 AND gti.ic_estatus_cred_educa = ? "
		);
		conditions.add(new Integer(1));
		conditions.add("E");
		conditions.add("4");

		if(intermediario != null && !intermediario.equals("")){
			qrySentencia.append(" AND cif.ic_if = ? ");
			conditions.add(intermediario);
		}

		if(this.grupo != null && !this.grupo.equals("")){
			qrySentencia.append(" AND cu.ic_grupo = ? ");
			conditions.add(this.grupo);
		}

		if(this.universidad != null && !this.universidad.equals("")){
			qrySentencia.append(" AND cu.ic_universidad = ? ");
			conditions.add(this.universidad);
		}

		if(this.folio != null && !this.folio.equals("")){
			qrySentencia.append("	AND ccm.ic_folio_operacion = ? ");
			conditions.add(this.folio);
		}
		
		log.info("qrySentencia  "+qrySentencia.toString());
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		qrySentencia.append(
			" SELECT ccm.ic_folio_operacion AS folio_operacion "+
			"  FROM gti_estatus_solic gti, "+
			"       ce_confirma_monto ccm, "+
			"       comcat_if cif, "+
			"       comcat_universidad cu "+
			" WHERE gti.ic_folio = ccm.ic_folio_operacion "+
			"   AND gti.ic_if_siag = cif.ic_if_siag  "+
			"   AND gti.ic_universidad = cu.ic_universidad "+
			"   AND gti.ic_tipo_operacion = ? "+
			"   AND gti.cg_programa = ?  "+
			"	 AND gti.ic_estatus_cred_educa = ? "
		);
		conditions.add(new Integer(1));
		conditions.add("E");
		conditions.add("4");

		if(intermediario != null && !intermediario.equals("")){
			qrySentencia.append(" AND cif.ic_if = ? ");
			conditions.add(intermediario);
		}

		if(grupo != null && !grupo.equals("")){
			qrySentencia.append(" AND cu.ic_grupo = ? ");
			conditions.add(grupo);
		}

		if(universidad != null && !universidad.equals("")){
			qrySentencia.append(" AND cu.ic_universidad = ? ");
			conditions.add(universidad);
		}

		if(!folio.equals("")){
			qrySentencia.append("	AND ccm.ic_folio_operacion = ? ");
			conditions.add(folio);
		}

		log.info("qrySentencia  "+qrySentencia.toString());
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();

		qrySentencia.append(
			"SELECT TO_CHAR(ccm.ic_folio_operacion) AS folio_operacion, "+
			"		  cif.cg_razon_social AS nombre_if, cif.ic_if AS clave_if, cu.ic_universidad,	ic_grupo,"+
			"       cu.cg_razon_social ||'-'|| cu.cg_nombre_campus AS universidad_campus, "+
			"       TO_CHAR (ccm.df_hr_comfmonto, 'dd/mm/yyyy HH24:MI:SS') AS fecha_hora, "+
			"       gti.fn_monto_valido AS monto_valido, "+
			"       gti.in_registros_acep AS num_aceptadas, "+
			"       gti.in_registros_rech AS num_rechazadas, "+
			"       gti.in_registros AS total_operaciones, "+
			"       ccm.fn_monto_contragan AS monto_garantia "+
			"  FROM gti_estatus_solic gti, "+
			"       ce_confirma_monto ccm, "+
			"       comcat_if cif, "+
			"       comcat_universidad cu "+
			" WHERE gti.ic_folio = ccm.ic_folio_operacion "+
			"   AND gti.ic_if_siag = cif.ic_if_siag  "+
			"   AND gti.ic_universidad = cu.ic_universidad "+
			"   AND gti.ic_tipo_operacion = ? "+
			"   AND gti.cg_programa = ?  "+
			"	 AND gti.ic_estatus_cred_educa = ? "
		);
		conditions.add(new Integer(1));
		conditions.add("E");
		conditions.add("4");

		for(int i=0;i<pageIds.size();i++) {
			List lItem = (ArrayList)pageIds.get(i);
			if(i==0) {
				qrySentencia.append("  AND  ( "); 
			} else {
				qrySentencia.append("  OR  "); 
			}
			qrySentencia.append(" ( ccm.ic_folio_operacion = ? ) "); 
			conditions.add(new Long(lItem.get(0).toString()));
		}//for(int i=0;i<ids.size();i++)
		qrySentencia.append("  ) "); 
		
		log.info("qrySentencia  "+qrySentencia.toString());
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}

	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
	
		log.info("qrySentencia  "+qrySentencia.toString());
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}

	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		return "";
	}

	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {		
		return "";
	}

	public void setPaginaOffset(String paginaOffset) {
		this.paginaOffset = paginaOffset;
	}

	public String getPaginaOffset() {
		return paginaOffset;
	}

	public void setPaginaNo(String paginaNo) {
		this.paginaNo = paginaNo;
	}

	public String getPaginaNo() {
		return paginaNo;
	}

	public void setIntermediario(String intermediario) {
		this.intermediario = intermediario;
	}

	public String getIntermediario() {
		return intermediario;
	}

	public void setUniversidad(String universidad) {
		this.universidad = universidad;
	}

	public String getUniversidad() {
		return universidad;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getFolio() {
		return folio;
	}

	public List getConditions(){
		return this.conditions;
	}

}