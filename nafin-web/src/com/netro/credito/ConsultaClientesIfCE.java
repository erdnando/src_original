package com.netro.credito;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorRegExtJS;

public class ConsultaClientesIfCE implements IQueryGenerator ,IQueryGeneratorRegExtJS {
  private List conditions;
  private String noDocSIRAC;
  private String RFC;
  private String iNoCliente;
  private String razonSoc;
  private String fechaReg;
  private String fechaReg2;
  public ConsultaClientesIfCE()  {  }
  //Metodos IQueryGeneratorRegExtJS
  public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
  String nombreArchivo = "";
			if("PDF".equals(tipo)){
			try{
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
				
				String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual		= fechaActual.substring(0,2);
				String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual		= fechaActual.substring(6,10);
				String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
				
				//pdfDoc.setTable(16, 100);
				List lEncabezados = new ArrayList();
				
			
				
				lEncabezados.add("N�mero de Cliente Sirac");
				lEncabezados.add("Nombre Completo o Razon Social");
				lEncabezados.add("RFC");
				lEncabezados.add("Tipo Persona");
				lEncabezados.add("Sector Economico");
				lEncabezados.add("Subsector");
				lEncabezados.add("Rama");
				lEncabezados.add("Clase");
				lEncabezados.add("Estrato");
				lEncabezados.add("Nafin Electr�nico");
				pdfDoc.setTable(lEncabezados, "formasmenB", 100);
				
				
				while (reg.next()){
					String sirac= (reg.getString("SIRAC") == null) ? "" : reg.getString("SIRAC");
					String razon=(reg.getString("CG_RAZON_SOCIAL") == null) ? "" : reg.getString("CG_RAZON_SOCIAL");
					String rfc=(reg.getString("CG_RFC") == null) ? "" : reg.getString("CG_RFC");
					String tipoPersona=(reg.getString("CS_TIPO_PERSONA") == null) ? "" : reg.getString("CS_TIPO_PERSONA");
					String seNombre=(reg.getString("SE_NOMBRE") == null) ? "" : reg.getString("SE_NOMBRE");
					String subNombre=(reg.getString("SUB_NOMBRE") == null) ? "" : reg.getString("SUB_NOMBRE");
					String rNombre=(reg.getString("R_NOMBRE") == null) ? "" : reg.getString("R_NOMBRE");
					String cNombre=(reg.getString("C_NOMBRE") == null) ? "" : reg.getString("C_NOMBRE");
					String icEstrato=(reg.getString("IC_ESTRATO") == null) ? "" : reg.getString("IC_ESTRATO");
					String nafinE=(reg.getString("IC_NAFIN_ELECTRONICO") == null) ? "" : reg.getString("IC_NAFIN_ELECTRONICO");
					
					
					pdfDoc.setCell(sirac,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(razon,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(rfc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(tipoPersona,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(seNombre,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(subNombre,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(rNombre,"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(cNombre,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(icEstrato,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nafinE,"formas",ComunesPDF.CENTER);
					
				}
				//pdfDoc.addTable();
				
				
				pdfDoc.addTable();
				pdfDoc.endDocument();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo",e);
			}finally{
				try{
				}catch(Exception e){}
			}
		}	
			return  nombreArchivo;
  }
  public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		CreaArchivo 	archivo 			= new CreaArchivo();
		try {
		nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
				buffer = new BufferedWriter(writer);
				buffer.write(linea);
				
				//Encabezado
				linea="N�mero de Cliente Sirac,Nombre Completo o Razon Social,"+
				"RFC,Tipo Persona,Sector Economico,Subsector,Rama,Clase,Estrato,Nafin Electr�nico\n";
				buffer.write(linea);
				int i=0;
				while (rs.next()) {
					i++;
					String sirac= (rs.getString("SIRAC") == null) ? "" : rs.getString("SIRAC");
					String razon=(rs.getString("CG_RAZON_SOCIAL") == null) ? "" : rs.getString("CG_RAZON_SOCIAL");
					String rfc=(rs.getString("CG_RFC") == null) ? "" : rs.getString("CG_RFC");
					String tipoPersona=(rs.getString("CS_TIPO_PERSONA") == null) ? "" : rs.getString("CS_TIPO_PERSONA");
					String seNombre=(rs.getString("SE_NOMBRE") == null) ? "" : rs.getString("SE_NOMBRE");
					String subNombre=(rs.getString("SUB_NOMBRE") == null) ? "" : rs.getString("SUB_NOMBRE");
					String rNombre=(rs.getString("R_NOMBRE") == null) ? "" : rs.getString("R_NOMBRE");
					String cNombre=(rs.getString("C_NOMBRE") == null) ? "" : rs.getString("C_NOMBRE");
					String icEstrato=(rs.getString("IC_ESTRATO") == null) ? "" : rs.getString("IC_ESTRATO");
					String nafinE=(rs.getString("IC_NAFIN_ELECTRONICO") == null) ? "" : rs.getString("IC_NAFIN_ELECTRONICO");
					

					if(tipoPersona.equals("F")){
						tipoPersona="Fisica";
					}else{
						tipoPersona="Moral";
					}
					linea =sirac+","+razon.replace(',',' ')+","+rfc+","+tipoPersona.replace(',',' ')+","+seNombre.replace(',',' ')+","+subNombre.replace(',',' ')+","+rNombre.replace(',',' ')+","+cNombre.replace(',',' ')
					+","+icEstrato.replace(',',' ')+","+nafinE+"\n";
					
					buffer.write(linea);
				}
				buffer.write("Total de Clientes,"+i);
				buffer.close();
			} catch (Throwable e) {
					throw new AppException("Error al generar el archivo", e);
				} finally {
					try {
						rs.close();
					} catch(Exception e) {}
				}
	
		return nombreArchivo;
  }
  
  public String getDocumentQueryFile() {
	StringBuffer condicion= new StringBuffer();
	StringBuffer qrySentencia = new StringBuffer();
		String NumClienteSIRAC = noDocSIRAC;
			String NombreoRS = razonSoc;
            //New
        	String Fecha = fechaReg;
          String Fecha2 = fechaReg2;
        	System.out.println("4.-La fecha a consultar es de:"+Fecha+" "+"Hasta "+Fecha2);
			//end New
  //Num de IF
	    try {
			qrySentencia.append(
				" SELECT  /*+ use_nl(p rn c r sub se)*/"   +
			"        p.in_numero_sirac as sirac, p.cg_nombre, p.cg_appat,"   +
			"        p.cg_apmat,"   +
			"        p.cg_razon_social, p.cg_rfc,"+
			//"			p.cs_tipo_persona as CS_TIPO_PERSONA,"   +	
			"    		DECODE (p.cs_tipo_persona,'F', 'Fisica','M','Moral','CV') AS CS_TIPO_PERSONA,"   +
			"        se.cd_nombre as se_nombre, sub.cd_nombre as sub_nombre, r.cd_nombre r_nombre, c.cd_nombre as c_nombre,"   +
			"        p.ic_estrato, rn.ic_nafin_electronico,"   +
			"        PIC.df_alta "   +
				"   FROM comcat_pyme p,"   +
				"        comrel_nafin rn,"   +
				"        comcat_clase c,"   +
				"        comcat_rama r,"   +
				"        comcat_subsector sub,"   +
				"        comcat_sector_econ se, "   +
				"        comrel_pyme_if_credele PIC "+ //New
				"  WHERE p.ic_clase = c.ic_clase"   +
				"    AND P.ic_pyme = PIC.ic_pyme "+ //New
				"    AND p.ic_rama = c.ic_rama"   +
				"    AND p.ic_subsector = c.ic_subsector"   +
				"    AND p.ic_sector_econ = c.ic_sector_econ"   +
				"    AND p.ic_pyme = rn.ic_epo_pyme_if"   +
				"    AND r.ic_rama = c.ic_rama"   +
				"    AND r.ic_sector_econ = c.ic_sector_econ"   +
				"    AND r.ic_subsector = c.ic_subsector"   +
				"    AND sub.ic_subsector = r.ic_subsector"   +
				"    AND sub.ic_sector_econ = r.ic_sector_econ"   +
				"    AND sub.ic_sector_econ = se.ic_sector_econ"   +
				"    AND p.cs_credele = 'S'"   +
				"    AND rn.cg_tipo = 'P'");
        
     conditions=new ArrayList();

		if(!"".equals(NumClienteSIRAC)){
			condicion.append(" and P.in_numero_sirac = ? ");
			conditions.add(NumClienteSIRAC);
		}
		
		if(!"".equals(RFC)){
			condicion.append(" and P.cg_rfc = ? ");
			conditions.add(RFC);
		}
		
		if(!"".equals(NombreoRS)){
			condicion.append(" and (upper(trim(P.cg_razon_social)) LIKE upper(?)) ");
			conditions.add(NombreoRS);
		}
		if(!"".equals(Fecha)&&!"".equals(Fecha2)){
			condicion.append(" and trunc(pic.df_alta) between to_date(?,'dd/mm/yyyy') and to_date(?,'dd/mm/yyyy') ");
			conditions.add(Fecha);
			conditions.add(Fecha2);
		}
				
				
            
      condicion.append(" AND pic.ic_if= ? ");
		conditions.add(iNoCliente);
      
	    }catch(Exception e){
			System.out.println("ConsultaClientesIfCE::getDocumentQueryFileException "+e);
		}
		return qrySentencia.toString()+condicion.toString();
  

	}
  public String getAggregateCalculationQuery() {
	return "";
  }
  public String getDocumentSummaryQueryForIds(List pageIds) {
  //
  StringBuffer condicion= new StringBuffer();
  int i=0;
		StringBuffer qrySentencia = new StringBuffer();
//New
   String Fecha = fechaReg;
   String Fecha2 = fechaReg2;
   System.out.println("2.-La fecha a consultar es de:"+Fecha+" "+"Hasta "+Fecha2);
//end NEW
//Num de IF
		qrySentencia.append(
			" SELECT  /*+ use_nl(p rn c r sub se)*/"   +
			"        p.in_numero_sirac as sirac, p.cg_nombre, p.cg_appat,"   +
			"        p.cg_apmat,"   +
			"        p.cg_razon_social, p.cg_rfc," +
			//"			p.cs_tipo_persona as CS_TIPO_PERSONA,"   +
			"    		DECODE (p.cs_tipo_persona,'F', 'Fisica','M','Moral','CV') AS CS_TIPO_PERSONA,"   +
			"        se.cd_nombre as se_nombre, sub.cd_nombre as sub_nombre, r.cd_nombre r_nombre, c.cd_nombre as c_nombre,"   +
			"        p.ic_estrato, rn.ic_nafin_electronico,"   +
			"        PIC.df_alta "     +  //New
			"   FROM comcat_pyme p,"   +
			"        comrel_nafin rn,"   +
			"        comcat_clase c,"   +
			"        comcat_rama r,"   +
			"        comcat_subsector sub,"   +
			"        comcat_sector_econ se,"   +
			"        comrel_pyme_if_credele PIC "+ //New
			"  WHERE p.ic_clase = c.ic_clase"   +
			"    AND P.ic_pyme = PIC.ic_pyme "+ //New
			"    AND p.ic_rama = c.ic_rama"   +
			"    AND p.ic_subsector = c.ic_subsector"   +
			"    AND p.ic_sector_econ = c.ic_sector_econ"   +
			"    AND p.ic_pyme = rn.ic_epo_pyme_if"   +
			"    AND r.ic_rama = c.ic_rama"   +
			"    AND r.ic_sector_econ = c.ic_sector_econ"   +
			"    AND r.ic_subsector = c.ic_subsector"   +
			"    AND sub.ic_subsector = r.ic_subsector"   +
			"    AND sub.ic_sector_econ = r.ic_sector_econ"   +
			"    AND sub.ic_sector_econ = se.ic_sector_econ"   +
			"    AND p.cs_credele = 'S'"   +
			"    AND rn.cg_tipo = 'P'"  +
			"    AND P.IC_pyme in(");

		conditions 		= new ArrayList();
		for(int j = 0; j < pageIds.size(); j++){
			List lItem = (ArrayList)pageIds.get(j);
				qrySentencia.append(new String(lItem.get(0).toString())+",");
				//conditions.add(new String(lItem.get(0).toString()));      
			}
			qrySentencia.deleteCharAt(qrySentencia.length()-1);
			qrySentencia.append(") ");
			
			  conditions=new ArrayList();




		if(!"".equals(noDocSIRAC)){
			condicion.append(" and P.in_numero_sirac = ? ");
			conditions.add(noDocSIRAC);
		}
		
		if(!"".equals(RFC)){
			condicion.append(" and P.cg_rfc = ? ");
			conditions.add(RFC);
		}
		
		if(!"".equals(razonSoc)){
			condicion.append(" and (upper(trim(P.cg_razon_social)) LIKE upper(?)) ");
			conditions.add(razonSoc);
		}
		if(!"".equals(Fecha)&&!"".equals(Fecha2)){
			condicion.append(" and trunc(pic.df_alta) between to_date(?,'dd/mm/yyyy') and to_date(?,'dd/mm/yyyy') ");
			conditions.add(Fecha);
			conditions.add(Fecha2);
		}
				
				
            
      condicion.append(" AND pic.ic_if= ? ");
		conditions.add(iNoCliente);
		//New
		//qrySentencia.append(((Fecha.equals(""))?"":" and trunc(pic.df_alta) = to_date('"+Fecha+"','dd/mm/yyyy') "));
		//qrySentencia.append(((Fecha.equals(""))?"":" and trunc(pic.df_alta) between to_date('"+Fecha+"','dd/mm/yyyy') and to_date('"+Fecha2+"','dd/mm/yyyy')"));
	    //end New
    //Num IF
     //qrySentencia.append(" AND pic.ic_if="+iNoCliente); 
		//System.out.println("el query queda de la siguiente manera "+qrySentencia.toString()+condicion.toString());
		return qrySentencia.toString()+condicion.toString();
  }
  public String getDocumentQuery() {
	StringBuffer qrySentencia = new StringBuffer();
	StringBuffer condicion =new StringBuffer();
		String NumClienteSIRAC = noDocSIRAC;
		String	NombreoRS = razonSoc;
		//New
        String Fecha = fechaReg;
        String Fecha2 = fechaReg2;
        System.out.println("3.-La fecha a consultar es de:"+Fecha+" "+"Hasta "+Fecha2);
		//end New
    //Num de IF

    	try{

			qrySentencia.append(
				" SELECT  /*+ use_nl(p rn c r sub se)*/"   +
				"        P.IC_PYME"   +
				"   FROM comcat_pyme p,"   +
				"        comrel_nafin rn,"   +
				"        comcat_clase c,"   +
				"        comcat_rama r,"   +
				"        comcat_subsector sub,"   +
				"        comcat_sector_econ se,"   +
				"        comrel_pyme_if_credele PIC "+ //New
				"  WHERE p.ic_clase = c.ic_clase"   +
				"    AND P.ic_pyme = PIC.ic_pyme "+ //New
				"    AND p.ic_rama = c.ic_rama"   +
				"    AND p.ic_subsector = c.ic_subsector"   +
				"    AND p.ic_sector_econ = c.ic_sector_econ"   +
				"    AND p.ic_pyme = rn.ic_epo_pyme_if"   +
				"    AND r.ic_rama = c.ic_rama"   +
				"    AND r.ic_sector_econ = c.ic_sector_econ"   +
				"    AND r.ic_subsector = c.ic_subsector"   +
				"    AND sub.ic_subsector = r.ic_subsector"   +
				"    AND sub.ic_sector_econ = r.ic_sector_econ"   +
				"    AND sub.ic_sector_econ = se.ic_sector_econ"   +
				"    AND p.cs_credele = 'S'"   +
				"    AND rn.cg_tipo = 'P'");
				
		conditions=new ArrayList();

		if(!"".equals(NumClienteSIRAC)){
			condicion.append(" and P.in_numero_sirac = ? ");
			conditions.add(NumClienteSIRAC);
		}
		
		if(!"".equals(RFC)){
			condicion.append(" and P.cg_rfc = ? ");
			conditions.add(RFC);
		}
		
		if(!"".equals(NombreoRS)){
			condicion.append(" and (upper(trim(P.cg_razon_social)) LIKE upper(?)) ");
			conditions.add(NombreoRS);
		}
		if(!"".equals(Fecha)&&!"".equals(Fecha2)){
			condicion.append(" and trunc(pic.df_alta) between to_date(?,'dd/mm/yyyy') and to_date(?,'dd/mm/yyyy') ");
			conditions.add(Fecha);
			conditions.add(Fecha2);
		}
				
				
            
      condicion.append(" AND pic.ic_if= ? ");
		conditions.add(iNoCliente);
		qrySentencia.append(condicion.toString());
		    System.out.println("EL query de la llave primaria: "+qrySentencia.toString());
		}catch(Exception e){
			System.out.println("ConsultaClientesIfCE::getDocumentQueryException "+e);
    	}
		return qrySentencia.toString();
  
  }
  
  
    
  //Fin de Metodos IQueryGeneratorRegExtJS



  
	public String getAggregateCalculationQuery(HttpServletRequest request)  {
    StringBuffer qrySentencia = new StringBuffer();
	String NumClienteSIRAC = (request.getParameter("NumClienteSIRAC")==null)?"":request.getParameter("NumClienteSIRAC"),
		RFC = (request.getParameter("RFC")==null)?"":request.getParameter("RFC"),
		NombreoRS = (request.getParameter("NombreoRS")==null)?"":request.getParameter("NombreoRS"),
		consultar = (request.getParameter("consultar")==null)?"":request.getParameter("consultar");
//New
   String Fecha = (request.getParameter("Fecha")==null)?"":request.getParameter("Fecha");
   String Fecha2 = (request.getParameter("Fecha2")==null)?"":request.getParameter("Fecha2");
   System.out.println("1.-La fecha a consultar es de:"+Fecha+" "+"Hasta "+Fecha2);
//end NEW
//Num de IF
  String iNoCliente = (request.getParameter("iNoCliente")==null)?"":request.getParameter("iNoCliente");
    try{
		qrySentencia.append(
			" SELECT   /*+ use_nl(p rn c r sub se)*/"   +
			" 		 count(1) as ConsultaClientesIfCE "+
			"   FROM comcat_pyme p,"   +
			"        comrel_nafin rn,"   +
			"        comcat_clase c,"   +
			"        comcat_rama r,"   +
			"        comcat_subsector sub,"   +
			"        comcat_sector_econ se,"   +
			"        comrel_pyme_if_credele PIC "+ //New
			"  WHERE p.ic_clase = c.ic_clase"   +
			"    AND P.ic_pyme = PIC.ic_pyme "+ //New
			"    AND p.ic_rama = c.ic_rama"   +
			"    AND p.ic_subsector = c.ic_subsector"   +
			"    AND p.ic_sector_econ = c.ic_sector_econ"   +
			"    AND p.ic_pyme = rn.ic_epo_pyme_if"   +
			"    AND r.ic_rama = c.ic_rama"   +
			"    AND r.ic_sector_econ = c.ic_sector_econ"   +
			"    AND r.ic_subsector = c.ic_subsector"   +
			"    AND sub.ic_subsector = r.ic_subsector"   +
			"    AND sub.ic_sector_econ = r.ic_sector_econ"   +
			"    AND sub.ic_sector_econ = se.ic_sector_econ"   +
			"    AND p.cs_credele = 'S'"   +
			"    AND rn.cg_tipo = 'P'");
      qrySentencia.append(((NumClienteSIRAC.equals(""))?"":" and P.in_numero_sirac = " +NumClienteSIRAC));
			qrySentencia.append(((RFC.equals(""))?"":" and P.cg_rfc = '"+RFC+"'"));
			qrySentencia.append(((NombreoRS.equals(""))?"":" and (upper(trim(P.cg_razon_social)) LIKE upper('"+NombreoRS+"%')) "));
			//New
			//qrySentencia.append(((Fecha.equals(""))?"":" and trunc(pic.df_alta) = to_date('"+Fecha+"','dd/mm/yyyy') "));
			qrySentencia.append(((Fecha.equals(""))?"":" and trunc(pic.df_alta) between to_date('"+Fecha+"','dd/mm/yyyy') and to_date('"+Fecha2+"','dd/mm/yyyy')"));
			//end New
      //Num IF
      qrySentencia.append(" AND pic.ic_if="+iNoCliente);
    }catch(Exception e){
      System.out.println("ConsultaClientesIfCE::getDocumentQueryFileException "+e);
    }
    return qrySentencia.toString();
  }

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids)  {
  		int i=0;
		StringBuffer qrySentencia = new StringBuffer();
//New
   String Fecha = (request.getParameter("Fecha")==null)?"":request.getParameter("Fecha");
   String Fecha2 = (request.getParameter("Fecha2")==null)?"":request.getParameter("Fecha2");
   System.out.println("2.-La fecha a consultar es de:"+Fecha+" "+"Hasta "+Fecha2);
//end NEW
//Num de IF
  String iNoCliente = (request.getParameter("iNoCliente")==null)?"":request.getParameter("iNoCliente");

		qrySentencia.append(
			" SELECT  /*+ use_nl(p rn c r sub se)*/"   +
			"        p.in_numero_sirac, p.cg_nombre, p.cg_appat,"   +
			"        p.cg_apmat,"   +
			"        p.cg_razon_social, p.cg_rfc, "+
			//p.cs_tipo_persona,"   +
			"    		DECODE (p.cs_tipo_persona,'F', 'Fisica','M','Moral','CV') AS CS_TIPO_PERSONA,"   +
			"        se.cd_nombre, sub.cd_nombre, r.cd_nombre, c.cd_nombre,"   +
			"        p.ic_estrato, rn.ic_nafin_electronico,"   +
			"        PIC.df_alta "     +  //New
			"   FROM comcat_pyme p,"   +
			"        comrel_nafin rn,"   +
			"        comcat_clase c,"   +
			"        comcat_rama r,"   +
			"        comcat_subsector sub,"   +
			"        comcat_sector_econ se,"   +
			"        comrel_pyme_if_credele PIC "+ //New
			"  WHERE p.ic_clase = c.ic_clase"   +
			"    AND P.ic_pyme = PIC.ic_pyme "+ //New
			"    AND p.ic_rama = c.ic_rama"   +
			"    AND p.ic_subsector = c.ic_subsector"   +
			"    AND p.ic_sector_econ = c.ic_sector_econ"   +
			"    AND p.ic_pyme = rn.ic_epo_pyme_if"   +
			"    AND r.ic_rama = c.ic_rama"   +
			"    AND r.ic_sector_econ = c.ic_sector_econ"   +
			"    AND r.ic_subsector = c.ic_subsector"   +
			"    AND sub.ic_subsector = r.ic_subsector"   +
			"    AND sub.ic_sector_econ = r.ic_sector_econ"   +
			"    AND sub.ic_sector_econ = se.ic_sector_econ"   +
			"    AND p.cs_credele = 'S'"   +
			"    AND rn.cg_tipo = 'P'"  +
			"    AND P.IC_pyme in(");

		i=0;
		for (Iterator it = ids.iterator(); it.hasNext();i++){
        	if(i>0){
          		qrySentencia.append(",");
        	}
			qrySentencia.append("'"+it.next().toString()+"'");
		}
		qrySentencia.append(")");
		//New
		//qrySentencia.append(((Fecha.equals(""))?"":" and trunc(pic.df_alta) = to_date('"+Fecha+"','dd/mm/yyyy') "));
		qrySentencia.append(((Fecha.equals(""))?"":" and trunc(pic.df_alta) between to_date('"+Fecha+"','dd/mm/yyyy') and to_date('"+Fecha2+"','dd/mm/yyyy')"));
	    //end New
    //Num IF
     qrySentencia.append(" AND pic.ic_if="+iNoCliente); 
		System.out.println("el query queda de la siguiente manera "+qrySentencia.toString());
		return qrySentencia.toString();
  }

	public String getDocumentQuery(HttpServletRequest request)	{
    	StringBuffer qrySentencia = new StringBuffer();
		String NumClienteSIRAC = (request.getParameter("NumClienteSIRAC")==null)?"":request.getParameter("NumClienteSIRAC"),
			RFC = (request.getParameter("RFC")==null)?"":request.getParameter("RFC"),
			NombreoRS = (request.getParameter("NombreoRS")==null)?"":request.getParameter("NombreoRS"),
			consultar = (request.getParameter("consultar")==null)?"":request.getParameter("consultar");
		//New
        String Fecha = (request.getParameter("Fecha")==null)?"":request.getParameter("Fecha");
        String Fecha2 = (request.getParameter("Fecha2")==null)?"":request.getParameter("Fecha2");
        System.out.println("3.-La fecha a consultar es de:"+Fecha+" "+"Hasta "+Fecha2);
		//end New
    //Num de IF
    String iNoCliente = (request.getParameter("iNoCliente")==null)?"":request.getParameter("iNoCliente");

    	try{

			qrySentencia.append(
				" SELECT  /*+ use_nl(p rn c r sub se)*/"   +
				"        P.IC_PYME,'ConsultaClientesIfCE::getDocumentQuery'"   +
				"   FROM comcat_pyme p,"   +
				"        comrel_nafin rn,"   +
				"        comcat_clase c,"   +
				"        comcat_rama r,"   +
				"        comcat_subsector sub,"   +
				"        comcat_sector_econ se,"   +
				"        comrel_pyme_if_credele PIC "+ //New
				"  WHERE p.ic_clase = c.ic_clase"   +
				"    AND P.ic_pyme = PIC.ic_pyme "+ //New
				"    AND p.ic_rama = c.ic_rama"   +
				"    AND p.ic_subsector = c.ic_subsector"   +
				"    AND p.ic_sector_econ = c.ic_sector_econ"   +
				"    AND p.ic_pyme = rn.ic_epo_pyme_if"   +
				"    AND r.ic_rama = c.ic_rama"   +
				"    AND r.ic_sector_econ = c.ic_sector_econ"   +
				"    AND r.ic_subsector = c.ic_subsector"   +
				"    AND sub.ic_subsector = r.ic_subsector"   +
				"    AND sub.ic_sector_econ = r.ic_sector_econ"   +
				"    AND sub.ic_sector_econ = se.ic_sector_econ"   +
				"    AND p.cs_credele = 'S'"   +
				"    AND rn.cg_tipo = 'P'");
            qrySentencia.append(((NumClienteSIRAC.equals(""))?"":" and P.in_numero_sirac = " +NumClienteSIRAC));
			qrySentencia.append(((RFC.equals(""))?"":" and P.cg_rfc = '"+RFC+"'"));
			qrySentencia.append(((NombreoRS.equals(""))?"":" and (upper(trim(P.cg_razon_social)) LIKE upper('"+NombreoRS+"%')) "));
			//New
			//qrySentencia.append(((Fecha.equals(""))?"":" and trunc(pic.df_alta) = to_date('"+Fecha+"','dd/mm/yyyy') "));
			qrySentencia.append(((Fecha.equals(""))?"":" and trunc(pic.df_alta) between to_date('"+Fecha+"','dd/mm/yyyy') and to_date('"+Fecha2+"','dd/mm/yyyy')"));
			//end New
      //Num IF
      qrySentencia.append(" AND pic.ic_if="+iNoCliente);
		    System.out.println("EL query de la llave primaria: "+qrySentencia.toString());
		}catch(Exception e){
			System.out.println("ConsultaClientesIfCE::getDocumentQueryException "+e);
    	}
		return qrySentencia.toString();
	}

	public String getDocumentQueryFile(HttpServletRequest request) {
    	StringBuffer qrySentencia = new StringBuffer();
		String NumClienteSIRAC = (request.getParameter("NumClienteSIRAC")==null)?"":request.getParameter("NumClienteSIRAC"),
			RFC = (request.getParameter("RFC")==null)?"":request.getParameter("RFC"),
			NombreoRS = (request.getParameter("NombreoRS")==null)?"":request.getParameter("NombreoRS"),
			consultar = (request.getParameter("consultar")==null)?"":request.getParameter("consultar");
            //New
        	String Fecha = (request.getParameter("Fecha")==null)?"":request.getParameter("Fecha");
          String Fecha2 = (request.getParameter("Fecha2")==null)?"":request.getParameter("Fecha2");
        	System.out.println("4.-La fecha a consultar es de:"+Fecha+" "+"Hasta "+Fecha2);
			//end New
  //Num de IF
    String iNoCliente = (request.getParameter("iNoCliente")==null)?"":request.getParameter("iNoCliente");
    
	    try {

			qrySentencia.append(
				" SELECT  /*+ use_nl(p rn c r sub se)*/"   +
				"        p.in_numero_sirac, p.cg_nombre, p.cg_appat,"   +
				"        p.cg_apmat,"   +
				"        p.cg_razon_social, p.cg_rfc,"+
				//p.cs_tipo_persona,"   +
				"    		DECODE (p.cs_tipo_persona,'F', 'Fisica','M','Moral','CV') AS CS_TIPO_PERSONA,"   +
				"        se.cd_nombre, sub.cd_nombre, r.cd_nombre, c.cd_nombre,"   +
				"        p.ic_estrato, rn.ic_nafin_electronico"   +
				"   FROM comcat_pyme p,"   +
				"        comrel_nafin rn,"   +
				"        comcat_clase c,"   +
				"        comcat_rama r,"   +
				"        comcat_subsector sub,"   +
				"        comcat_sector_econ se, "   +
				"        comrel_pyme_if_credele PIC "+ //New
				"  WHERE p.ic_clase = c.ic_clase"   +
				"    AND P.ic_pyme = PIC.ic_pyme "+ //New
				"    AND p.ic_rama = c.ic_rama"   +
				"    AND p.ic_subsector = c.ic_subsector"   +
				"    AND p.ic_sector_econ = c.ic_sector_econ"   +
				"    AND p.ic_pyme = rn.ic_epo_pyme_if"   +
				"    AND r.ic_rama = c.ic_rama"   +
				"    AND r.ic_sector_econ = c.ic_sector_econ"   +
				"    AND r.ic_subsector = c.ic_subsector"   +
				"    AND sub.ic_subsector = r.ic_subsector"   +
				"    AND sub.ic_sector_econ = r.ic_sector_econ"   +
				"    AND sub.ic_sector_econ = se.ic_sector_econ"   +
				"    AND p.cs_credele = 'S'"   +
				"    AND rn.cg_tipo = 'P'");
        
      qrySentencia.append(((NumClienteSIRAC.equals(""))?"":" and P.in_numero_sirac = " +NumClienteSIRAC));
			qrySentencia.append(((RFC.equals(""))?"":" and P.cg_rfc = '"+RFC+"'"));
			qrySentencia.append(((NombreoRS.equals(""))?"":" and (upper(trim(P.cg_razon_social)) LIKE upper('"+NombreoRS+"%')) "));
			//New
			//qrySentencia.append(((Fecha.equals(""))?"":" and trunc(pic.df_alta) = to_date('"+Fecha+"','dd/mm/yyyy') "));
			qrySentencia.append(((Fecha.equals(""))?"":" and trunc(pic.df_alta) between to_date('"+Fecha+"','dd/mm/yyyy') and to_date('"+Fecha2+"','dd/mm/yyyy')"));
			//end New
      //Num IF
      qrySentencia.append(" AND pic.ic_if="+iNoCliente);
      
	    }catch(Exception e){
			System.out.println("ConsultaClientesIfCE::getDocumentQueryFileException "+e);
		}
		return qrySentencia.toString();
	}
	
	
		public void setConditions(List conditions) {
		this.conditions = conditions;
		}
	
	
		public List getConditions() {
			return conditions;
		}


	public void setNoDocSIRAC(String noDoc) {
		this.noDocSIRAC = noDoc;
	}


	public String getNoDocSIRAC() {
		return noDocSIRAC;
	}


	public void setRFC(String RFC) {
		this.RFC = RFC;
	}


	public String getRFC() {
		return RFC;
	}


	public void setRazonSoc(String razonSoc) {
		if(razonSoc.equals("")){
			this.razonSoc=razonSoc;
		}else
		this.razonSoc = razonSoc+"%";
	}


	public String getRazonSoc() {
		return razonSoc;
	}


	public void setFechaReg(String fechaReg) {
		this.fechaReg = fechaReg;
	}


	public String getFechaReg() {
		return fechaReg;
	}


	public void setFechaReg2(String fechaReg2) {
		this.fechaReg2 = fechaReg2;
	}


	public String getFechaReg2() {
		return fechaReg2;
	}


	public void setINoCliente(String iNoCliente) {
		this.iNoCliente = iNoCliente;
	}


	public String getINoCliente() {
		return iNoCliente;
	}
}
