package com.netro.credito;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class RepOperEstatus implements IQueryGeneratorRegExtJS  {	
	
	private int numList = 1;
	private String ic_if;
  private String estatus;
  private String strTipoUsuario;
  
	public static final boolean SIN_COMAS = false;
		
	public RepOperEstatus() {}

	// Variable para enviar mensajes al log.
	private static final Log log = ServiceLocator.getInstance().getLog(RepOperEstatus.class);
  
	public  StringBuffer strQuery;
	private List conditions = new ArrayList(); 
	
	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */
	public String getAggregateCalculationQuery() { // Obtiene los totales 
		return "";
 	}  
	/**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */	 
	public String getDocumentQuery(){  // para las llaves primarias		
		return "";		
 	}  
	
	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */	  		
	public String getDocumentSummaryQueryForIds(List pageIds){ // paginacion 			
		return "";
 	}  
	
	public String getDocumentQueryFile(){ // para todos los registros				
		conditions = new ArrayList();   
		String query = new String();   
    String hint = new String();
		    
		if("T".equals(estatus)||"2".equals(estatus)){
      if("NAFIN".equals(strTipoUsuario)){
        //Intermediario Financiero
      }
      
      hint = "/*+ index (sp IN_COM_SOLIC_PORTAL_04_NUK) use_nl(sp ci p tc m cif)*/";
      query = "select " + hint +
          " P.cg_razon_social as nombrePyme, SP.ig_clave_sirac as claveSirac, "+
          "TO_CHAR(SP.ig_numero_docto) as numDocto, SP.ic_solic_esp as numSolicitud, TO_CHAR(SP.df_etc,'DD/MM/YYYY') as fechaETC, "+
          "TO_CHAR(SP.df_v_descuento,'DD/MM/YYYY') as fechaDscto, M.cd_nombre as nombreMoneda, "+
          "TC.cd_descripcion as descTipoCred, SP.fn_importe_docto as importeDocto, "+
          "SP.fn_importe_dscto as importeDscto, SP.ic_solic_portal as Folio "+
          " ,SP.ig_numero_prestamo as numPrestamo, CIF.cg_razon_social as cg_razon_social "+
          " , '26credele\26if\26reporte1.jsp' as pantalla"+
          " from com_solic_portal SP"+
          " ,com_interfase CI "+
          " ,comcat_pyme P"+
          " , comcat_tipo_credito TC"+
          " , comcat_moneda M "+
          " ,comcat_if CIF "+
          " where SP.ic_tipo_credito = TC.ic_tipo_credito "+
          " and SP.ig_clave_sirac = P.in_numero_sirac(+) "+
          " and SP.ic_solic_portal= CI.ic_solic_portal "+
          " and SP.ic_if= CIF.ic_if "+
          " and SP.ic_moneda = M.ic_moneda(+) "+
          " and SP.ic_estatus_solic = 2 "+
          //" and trunc(SP.df_carga) = trunc(SYSDATE) "+
          " AND sp.df_carga >= trunc(SYSDATE) "+
          " AND sp.df_carga < trunc(SYSDATE+1) "+
          " and CI.ic_error_proceso is NULL ";
      if("IF".equals(strTipoUsuario)){
        query=query+" and SP.ic_if = " + ic_if;
      }
    }else if("T".equals(estatus)||"D".equals(estatus)){
      hint = "/*+ index (sp IN_COM_SOLIC_PORTAL_01_NUK) use_nl(sp ci p tc m cif)*/";

      query = "select " + hint +
			"P.cg_razon_social as nombrePyme, SP.ig_clave_sirac as claveSirac, "+
			"TO_CHAR(SP.ig_numero_docto) as numDocto, SP.ic_solic_esp as numSolicitud, TO_CHAR(SP.df_etc,'DD/MM/YYYY') as fechaETC, "+
			"TO_CHAR(SP.df_v_descuento,'DD/MM/YYYY') as fechaDscto, M.cd_nombre as nombreMoneda, "+
			"TC.cd_descripcion as descTipoCred, SP.fn_importe_docto as importeDocto, "+
			"SP.fn_importe_dscto as importeDscto, SP.ic_solic_portal as Folio "+
			" ,SP.ig_numero_prestamo as numPrestamo,  CIF.cg_razon_social as cg_razon_social"+
			" , '26credele\26if\26reporte1.jsp' as pantalla"+
			" from com_solic_portal SP"+
			" ,com_interfase CI "+
			" ,comcat_pyme P"+
			" , comcat_tipo_credito TC"+
			" , comcat_moneda M "+
			" ,comcat_if CIF "+
			" where SP.ic_tipo_credito = TC.ic_tipo_credito "+
			" and SP.ig_clave_sirac = P.in_numero_sirac(+) "+
			" and SP.ic_solic_portal= CI.ic_solic_portal "+
			" and SP.ic_if= CIF.ic_if "+
			" and SP.ic_moneda = M.ic_moneda(+) "+
			" and SP.ic_estatus_solic = 2 "+
			//" and trunc(SP.df_carga) = trunc(SYSDATE) "+
			" and CI.ic_error_proceso is NOT NULL ";

      if("IF".equals(strTipoUsuario)){
        query=query+" and SP.ic_if = " + ic_if;
      }
    }else if("T".equals(estatus)||"3".equals(estatus)){
      hint = "/*+ index (sp IN_COM_SOLIC_PORTAL_05_NUK) use_nl(sp p tc m cif)*/";
      query = "select " + hint +
          " P.cg_razon_social as nombrePyme, SP.ig_clave_sirac as claveSirac, "+
          "TO_CHAR(SP.ig_numero_docto) as numDocto, SP.ic_solic_esp as numSolicitud, TO_CHAR(SP.df_etc,'DD/MM/YYYY') as fechaETC, "+
          "TO_CHAR(SP.df_v_descuento,'DD/MM/YYYY') as fechaDscto, M.cd_nombre as nombreMoneda, "+
          "TC.cd_descripcion as descTipoCred, SP.fn_importe_docto as importeDocto, "+
          "SP.fn_importe_dscto as importeDscto, SP.ic_solic_portal as Folio "+
          " ,SP.ig_numero_prestamo as numPrestamo ,CIF.cg_razon_social as cg_razon_social"+
          " , '26credele\26if\26reporte1.jsp' as pantalla"+
          " from com_solic_portal SP"+
          " ,comcat_pyme P"+
          " ,comcat_tipo_credito TC"+
          " ,comcat_moneda M "+
          " ,comcat_if CIF "+
          " where SP.ic_tipo_credito = TC.ic_tipo_credito "+
          " and SP.ic_if= CIF.ic_if "+
          " and SP.ig_clave_sirac = P.in_numero_sirac(+) "+
          " and SP.ic_moneda = M.ic_moneda(+) "+
          " and SP.ic_estatus_solic = 3 "+
          " AND sp.df_operacion >= trunc(SYSDATE) "+
          " AND sp.df_operacion < trunc(SYSDATE+1) ";
          //" and trunc(SP.df_operacion) = trunc(SYSDATE) ";
    
       if("IF".equals(strTipoUsuario)){
       query=query+" and SP.ic_if = " + ic_if;
       }

    }else if("T".equals(estatus)||"4".equals(estatus)){
      hint = "/*+ index (sp IN_COM_SOLIC_PORTAL_04_NUK) use_nl(sp p tc m cif)*/";
      query = "select " + hint +
          " P.cg_razon_social as nombrePyme, SP.ig_clave_sirac as claveSirac, "+
          " TO_CHAR(SP.ig_numero_docto) as numDocto, SP.ic_solic_esp as numSolicitud , TO_CHAR(SP.df_etc,'DD/MM/YYYY') as fechaETC, "+
          " TO_CHAR(SP.df_v_descuento,'DD/MM/YYYY') as fechaDscto, M.cd_nombre as nombreMoneda, "+
          " TC.cd_descripcion as descTipoCred, SP.fn_importe_docto as importeDocto, "+
          " SP.fn_importe_dscto as importeDscto, SP.ic_solic_portal as Folio "+
          ", SP.ig_numero_prestamo as numPrestamo, CIF.cg_razon_social as cg_razon_social "+
          " , '26credele\26if\26reporte1.jsp' as pantalla"+
          " from com_solic_portal SP"+
          " ,comcat_pyme P"+
          " ,comcat_if CIF "+
          " ,comcat_tipo_credito TC "+
          " ,comcat_moneda M "+
          " where SP.ic_tipo_credito = TC.ic_tipo_credito "+
          " and SP.ig_clave_sirac = P.in_numero_sirac(+) "+
          " and SP.ic_moneda = M.ic_moneda(+) "+
          " and SP.ic_if= CIF.ic_if "+
          " and SP.ic_estatus_solic = 4 "+
          " AND sp.df_carga >= trunc(SYSDATE) "+
          " AND sp.df_carga < trunc(SYSDATE+1) ";
    
        if("IF".equals(strTipoUsuario)){
          query=query+" and SP.ic_if = " + ic_if;
        }
    }else if("T".equals(estatus)||"12".equals(estatus)){
      hint = "/*+ index (sp IN_COM_SOLIC_PORTAL_01_NUK) use_nl(sp p tc m cif)*/";
      query = "select " + hint +
          " P.cg_razon_social as nombrePyme, SP.ig_clave_sirac as claveSirac, "+
          "TO_CHAR(SP.ig_numero_docto) as numDocto, SP.ic_solic_esp as numSolicitud,  TO_CHAR(SP.df_etc,'DD/MM/YYYY') as fechaETC, "+
          "TO_CHAR(SP.df_v_descuento,'DD/MM/YYYY') as fechaDscto, M.cd_nombre as nombreMoneda, "+
          "TC.cd_descripcion as descTipoCred, SP.fn_importe_docto as importeDocto, "+
          "SP.fn_importe_dscto as importeDscto, SP.ic_solic_portal as Folio "+
          " ,SP.ig_numero_prestamo as numPrestamo, CIF.cg_razon_social as cg_razon_social"+
          " , '26credele\26if\26reporte1.jsp' as pantalla"+
          " from com_solic_portal SP"+
          " ,comcat_pyme P"+
          " , comcat_tipo_credito TC"+
          " , comcat_moneda M "+
          " ,comcat_if CIF "+
          " where SP.ic_tipo_credito = TC.ic_tipo_credito "+
          " and SP.ig_clave_sirac = P.in_numero_sirac(+) "+
          " and SP.ic_if= CIF.ic_if "+
          " and SP.ic_moneda = M.ic_moneda(+) "+
          " and SP.ic_estatus_solic = 12 ";
    
       if("IF".equals(strTipoUsuario)){
        query=query+" and SP.ic_if = " + ic_if;
       }
    }
    
		return query.toString();
 	}   
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	///para formar el csv o pdf de los todos los registros
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";  
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		CreaArchivo 	archivo 			= new CreaArchivo();

			if ("XLS".equals(tipo)) {
				try {
					int registros = 0;	
					while(rs.next()){
						if(registros==0) { 
							contenidoArchivo.append("Nombre del proveedor, Numero del proveedor, Numero de Transferencia, Fecha de pago, Montonto total de la transferencia, Moneda, Fecha de la factura, Monto de la factura, Fecha de operacion");
						}
						registros++;
						String nomproveedor = rs.getString("nomproveedor")==null?"":rs.getString("nomproveedor");
						String numproveedor = rs.getString("numproveedor")==null?"":rs.getString("numproveedor");
						String numtrans = rs.getString("numtrans")==null?"":rs.getString("numtrans");
						String fechapago = rs.getString("fechapago")==null?"":rs.getString("fechapago");
						String montocheque = rs.getString("montocheque")==null?"":rs.getString("montocheque");
						String moneda = rs.getString("moneda")==null?"":rs.getString("moneda");
						String fechafactura = rs.getString("fechafactura")==null?"":rs.getString("fechafactura");
						String montofactura = rs.getString("montofactura")==null?"":rs.getString("montofactura");
						String fechaoperacion = rs.getString("fechaoperacion")==null?"":rs.getString("fechaoperacion");
							
						contenidoArchivo.append("\n"+
							nomproveedor.replaceAll(",","") + "," +
							numproveedor + "," +
							numtrans + "," +
							fechapago.replaceAll("-","/") + "," +
							"$ " +  Comunes.formatoDecimal(montocheque, 2, false) + "," +
							moneda + "," +
							fechafactura.replaceAll("-","/") + "," +
							"$ " + Comunes.formatoDecimal(montofactura, 2, false) + "," +
							fechaoperacion.replaceAll("-","/") + ",");
					}
					
					if(registros >= 1){
						if(!archivo.make(contenidoArchivo.toString(),path, ".csv")){
							nombreArchivo="ERROR";
						}else{
							nombreArchivo = archivo.nombre;
						}
					}	 
				} catch (Throwable e) {
						throw new AppException("Error al generar el archivo", e);
				} finally {
					try {
						rs.close();
					} catch(Exception e) {}
				}
			}else if ("PDF".equals(tipo)) {
				try {
					HttpSession session = request.getSession();
					nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
					ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					
					String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
					String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
					String diaActual    = fechaActual.substring(0,2);
					String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
					String anioActual   = fechaActual.substring(6,10);
					String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
							
					pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					session.getAttribute("iNoNafinElectronico").toString(),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
							
					pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
					pdfDoc.setTable(9, 80);
					pdfDoc.setCell("Nombre del Proveedor","celda01",ComunesPDF.LEFT);
					pdfDoc.setCell("Numero del Proveedor","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Numero de Transferencia","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Pagado","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Total de la Transferencia","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de la Factura","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto de la Factura","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Operacion","celda01",ComunesPDF.CENTER);
													
					while (rs.next()) {
						String nombreproveedor = rs.getString("nomproveedor")==null?"":rs.getString("nomproveedor");
						String numeroproveedor = rs.getString("numproveedor")==null?"":rs.getString("numproveedor");
						String numtrans = rs.getString("numtrans")==null?"":rs.getString("numtrans");
						String fechapago = rs.getString("fechapago")==null?"":rs.getString("fechapago");
						String montocheque = rs.getString("montocheque")==null?"":rs.getString("montocheque");
						String moneda = rs.getString("moneda")==null?"":rs.getString("moneda");
						String fechafactura = rs.getString("fechafactura")==null?"":rs.getString("fechafactura");
						String montofactura = rs.getString("montofactura")==null?"":rs.getString("montofactura");
						String fechaoperacion = rs.getString("fechaoperacion")==null?"":rs.getString("fechaoperacion");
						 
						pdfDoc.setCell(nombreproveedor,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(numeroproveedor,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(numtrans,"formas",ComunesPDF.CENTER);		
						pdfDoc.setCell(fechapago,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(montocheque, 2, false),"formas",ComunesPDF.CENTER);		
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechafactura,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(montocheque, 2, false),"formas",ComunesPDF.CENTER);	
						pdfDoc.setCell(fechaoperacion,"formas",ComunesPDF.CENTER);				
					}
					pdfDoc.addTable();
					pdfDoc.endDocument();
				} catch(Throwable e) {
					throw new AppException("Error al generar el archivo", e);
				} finally {
					try {
						rs.close();
					} catch(Exception e) {}
				}
			}
		return nombreArchivo;	
	}	
	
	//para formar  csv o pdf por pagina  15 registros	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	
	public List getConditions() {
		return conditions;
	}


	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}


	public String getIc_if() {
		return ic_if;
	}


	

  public void set_ic_if(String ic_if) {
    this.ic_if = ic_if;
  }


  public String get_ic_if() {
    return ic_if;
  }


  public void setEstatus(String estatus) {
    this.estatus = estatus;
  }


  public String getEstatus() {
    return estatus;
  }


  public void setStrTipoUsuario(String strTipoUsuario) {
    this.strTipoUsuario = strTipoUsuario;
  }


  public String getStrTipoUsuario() {
    return strTipoUsuario;
  }
}      