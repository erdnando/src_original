package com.netro.credito;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsultaOperIfCE implements IQueryGenerator,IQueryGeneratorRegExtJS {
  public ConsultaOperIfCE()  {  }
	private List conditions;
	private String numAcuse;
	private String numFolio;
	private String numPrest;
	private String fechaIF;
	private String fechaIF2;
	private String fechaOper;
	private String fechaOper2;
	private String icEstatus;
	private String iNoCliente;
	private static final Log log = ServiceLocator.getInstance().getLog(ConsultaOperIfCE.class);

	
	// Inicio Metodos  IQueryGeneratorRegExtJS
 public String getAggregateCalculationQuery() {
	return "";
  }
 public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
	 String nombreArchivo = "";
			if("PDF".equals(tipo)){
			try{
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
				
				String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual		= fechaActual.substring(0,2);
				String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual		= fechaActual.substring(6,10);
				String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
				
				//pdfDoc.setTable(16, 100);
				
				
					pdfDoc.setTable(10,100,new float[]{2f,10.88f,10.88f,10.88f,10.88f,10.88f,10.88f,10.88f,10.88f,10.88f});
					pdfDoc.setCell("A","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Nombre de Cliente","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Sirac","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Docto.","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Period. Pago Cap.","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Emisi�n T�tulo Cred.","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Vto. del Descuento","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Tipo de Cr�dito","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto del Docto.","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("B","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto a Descontar","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Folio","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha Oper.","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Num. Pr�stamo","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Num. Acuse","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Tasa Usuario Final","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Rel. Mat.","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Sobre Tasa Usuario Final","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Estatus","celda01rep",ComunesPDF.CENTER);
					//FODEA 011-2010
					pdfDoc.setCell("C","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Env�o del IF","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("C�digo de Error","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("Descripci�n","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER,6);
				
				
				while (reg.next()){
					String rs_nomCliente	= (reg.getString(1)==null)?"":reg.getString(1);
				String rs_numSirac		= (reg.getString(2)==null)?"":reg.getString(2);
				String rs_numDoc		= (reg.getString(3)==null)?"":reg.getString(3);
				String rs_fechaEmision	= (reg.getString(4)==null)?"":reg.getString(4);
				String rs_fechaVenc		= (reg.getString(5)==null)?"":reg.getString(5);
				String rs_moneda		= (reg.getString(6)==null)?"":reg.getString(6);
				String rs_tipoCred		= (reg.getString(7)==null)?"":reg.getString(7);
				String rs_montoDoc		= (reg.getString(8)==null)?"":reg.getString(8);
				String rs_montoDesc		= (reg.getString(9)==null)?"":reg.getString(9);
				String rs_folio			= (reg.getString(10)==null)?"":reg.getString(10);
				String rs_fechaOper		= (reg.getString(11)==null)?"":reg.getString(11);
				String rs_numPrestamo	= (reg.getString(12)==null)?"":reg.getString(12);
				String rs_numAcuse		= (reg.getString(13)==null)?"":reg.getString(13);
				String rs_tasaUsuarioF	= (reg.getString(14)==null)?"":reg.getString(14);
				String rs_relMat		= (reg.getString(15)==null)?"":reg.getString(15);
				String rs_sobreTasaUF	= (reg.getString(16)==null)?"":Comunes.formatoDecimal(reg.getString(16),2);
				String rs_period_pago	= (reg.getString("periodicidad_pago")==null)?"":reg.getString("periodicidad_pago");
				String rs_estatus		= (reg.getString("estatus")==null)?"":reg.getString("estatus");
				
				//FODEA 011-2010 FVR-INI
				String rs_fechaenvioif	= (reg.getString("fechaEnvioIf")==null)?"":reg.getString("fechaEnvioIf");
				String rs_codigo_error	= ((reg.getString("codigoaplicacion")==null)?"":reg.getString("codigoaplicacion"))+
												  ((reg.getString("errorproceso")==null)?"":reg.getString("errorproceso"));
				String rs_errordescripcion	= (reg.getString("errordescripcion")==null)?"":reg.getString("errordescripcion");
				//FODEA 011-2010 FVR-FIN
				
				pdfDoc.setCell("A","celda01rep",ComunesPDF.CENTER);				
				pdfDoc.setCell(rs_nomCliente,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(rs_numSirac,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(rs_numDoc,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(rs_period_pago,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(rs_fechaEmision,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(rs_fechaVenc,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(rs_moneda,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(rs_tipoCred,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoMN(rs_montoDoc),"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell("B","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoMN(rs_montoDesc),"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(rs_folio,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(rs_fechaOper,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(rs_numPrestamo,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(rs_numAcuse,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(rs_tasaUsuarioF,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(rs_relMat,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(rs_sobreTasaUF,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(rs_estatus,"formasrep",ComunesPDF.CENTER);
				
				
				//FODEA 011-2010 FVR-INI
				pdfDoc.setCell("C","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell(rs_fechaenvioif,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(rs_codigo_error,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(rs_errordescripcion,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell("","formasrep",ComunesPDF.CENTER,6);
					
				}
				//pdfDoc.addTable();
				
				
				pdfDoc.addTable();
				pdfDoc.endDocument();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo",e);
			}finally{
				try{
				}catch(Exception e){}
			}
		}	
			return  nombreArchivo;
 }
 public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
	String nombreArchivo = "";
	try {
	CreaArchivo archivo = new CreaArchivo();
	
	StringBuffer contenidoArchivo = new StringBuffer();
	int registros=0;
	contenidoArchivo.append(
					"Nombre del Cliente,"+
					"N�mero de Sirac,"+
					"N�mero de Documento,"+
					"Periodicidad de Pago de Capital,"+
					"Fecha Emisi�n T�tulo de Cr�dito,"+
					"Fecha Vencimiento Descuento,"+
					"Moneda,"+
					"Tipo de Cr�dito,"+
					"Monto del Documento,"+
					"Monto del Descuento,"+
					"N�mero de Folio,"+
					"Fecha de Operaci�n,"+
					"N�mero de Prestamo,"+
					"N�mero de Acuse,"+
					"Tasa Usuario Final,"+
					"Relaci�n Matem�tica,"+
					"Sobre Tasa Usuario Final,"+
					"Estatus,"+
					"Fecha de Envio del IF,"+
					"Codigo de Error,"+
					"Descripcion");
	while(rs.next()){
	registros++;
		String rs_nomCliente	= (rs.getString(1)==null)?"":rs.getString(1);
		String rs_numSirac		= (rs.getString(2)==null)?"":rs.getString(2);
		String rs_numDoc		= (rs.getString(3)==null)?"":rs.getString(3);
		String rs_fechaEmision	= (rs.getString(4)==null)?"":rs.getString(4);
		String rs_fechaVenc		= (rs.getString(5)==null)?"":rs.getString(5);
		String rs_moneda		= (rs.getString(6)==null)?"":rs.getString(6);
		String rs_tipoCred		= (rs.getString(7)==null)?"":rs.getString(7);
		String rs_montoDoc		= (rs.getString(8)==null)?"":rs.getString(8);
		String rs_montoDesc		= (rs.getString(9)==null)?"":rs.getString(9);
		String rs_folio			= (rs.getString(10)==null)?"":rs.getString(10);
		String rs_fechaOper		= (rs.getString(11)==null)?"":rs.getString(11);
		String rs_numPrestamo	= (rs.getString(12)==null)?"":rs.getString(12);
		String rs_numAcuse		= (rs.getString(13)==null)?"":rs.getString(13);
		String rs_tasaUsuarioF	= (rs.getString(14)==null)?"":rs.getString(14);
		String rs_relMat		= (rs.getString(15)==null)?"":rs.getString(15);
		String rs_sobreTasaUF	= (rs.getString(16)==null)?"":rs.getString(16);
		String rs_period_pago	= (rs.getString("periodicidad_pago")==null)?"":rs.getString("periodicidad_pago");
		String rs_estatus		= (rs.getString("estatus")==null)?"":rs.getString("estatus");
		
		//FODEA 011-2010 FVR-INI
		String rs_fechaenvioif	= (rs.getString("fechaEnvioIf")==null)?"":rs.getString("fechaEnvioIf");
		String rs_codigo_error	= ((rs.getString("codigoaplicacion")==null)?"":rs.getString("codigoaplicacion"))+
										  ((rs.getString("errorproceso")==null)?"":rs.getString("errorproceso"));
		String rs_errordescripcion	= (rs.getString("errordescripcion")==null)?"":rs.getString("errordescripcion");
		//FODEA 011-2010 FVR-FIN
		
		contenidoArchivo.append("\n"+
				rs_nomCliente.replace(',',' ')+","+
				rs_numSirac+","+
				rs_numDoc+","+
				rs_period_pago+","+
				rs_fechaEmision+","+
				rs_fechaVenc+","+
				rs_moneda+","+
				rs_tipoCred.replace(',',' ')+","+
				rs_montoDoc+","+
				rs_montoDesc+","+
				rs_folio+","+
				rs_fechaOper+","+
				rs_numPrestamo+","+
				rs_numAcuse+","+
				rs_tasaUsuarioF+","+
				rs_relMat+","+
				rs_sobreTasaUF+","+
				rs_estatus+","+
				rs_fechaenvioif+","+//FODEA 011-2010 FVR
				rs_codigo_error+","+//FODEA 011-2010 FVR
				rs_errordescripcion);//FODEA 011-2010 FVR
	} // while
	if(registros >= 1){
		contenidoArchivo.append("\nTotal de Clientes,"+registros);
		if(!archivo.make(contenidoArchivo.toString(), path, ".csv")){}
		else{
			nombreArchivo = archivo.nombre;
		}
	}
	} catch (Throwable e) {
					throw new AppException("Error al generar el archivo", e);
				} finally {
					try {
						rs.close();
					} catch(Exception e) {}
				}
	return nombreArchivo;
	
 }
 public String getDocumentQueryFile() {
	StringBuffer qrySentencia = new StringBuffer();
    	
	    try {
			qrySentencia.append(
				" SELECT   /*+use_nl(sp p1 p2 cd tc m e tituf i)*/p.cg_razon_social"   +
				"              AS nombrepyme, sp.ig_clave_sirac AS clavesirac,"   +
				"        sp.ig_numero_docto AS numdocto,"   +
				"        TO_CHAR (sp.df_etc, 'DD/MM/YYYY') AS fechaetc,"   +
				"        TO_CHAR (sp.df_v_descuento, 'DD/MM/YYYY') AS fechadscto,"   +
				"        m.cd_nombre AS nombremoneda, tc.cd_descripcion AS desctipocred,"   +
				"        sp.fn_importe_docto AS importedocto,"   +
				"        sp.fn_importe_dscto AS importedscto,"   +
				"        sp.ic_solic_portal AS numfolio,"   +
				"        TO_CHAR (sp.df_operacion, 'DD/MM/YYYY') AS fechaoperacion,"   +
				"        sp.ig_numero_prestamo AS numprestamo, sp.cc_acuse AS numacuse"   +
				"			,sp.ic_tasauf, sp.cg_rmuf, sp.fg_stuf "+
				"			,p1.cd_descripcion AS periodicidad_pago,solic.cd_descripcion as estatus" +
				"        ,TO_CHAR (sp.df_carga, 'DD/MM/YYYY') AS fechaEnvioIf"   +//FODEA 011-2010 FVR 
				" 		 	,sp.ic_error_proceso AS errorproceso, sp.cc_codigo_aplicacion as codigoaplicacion" +//FODEA 011-2010 FVR 
				" 		 	,ep.cg_descripcion AS errordescripcion" +//FODEA 011-2010 FVR
				"   FROM com_solic_portal sp,"   +
				"        comcat_pyme p,"   +
				"        comcat_tasa ti,"   +
				"        comcat_tasa tuf,"   +
				"        comcat_emisor e,"   +
				"        comcat_moneda m,"   +
				"        comcat_tipo_credito tc,"   +
				"        comcat_estatus_solic solic,"   +
				"        comcat_clase_docto cd,"   +
				"        comcat_periodicidad p1,"   +
				"        comcat_periodicidad p2,"   +
				"        comcat_tabla_amort ta,"   +
				"        comcat_error_procesos ep"   +//FODEA 011-2010 FVR
				"  WHERE p.in_numero_sirac (+) = sp.ig_clave_sirac"   +
				"    AND ti.ic_tasa = sp.ic_tasaif"   +
				"    AND tuf.ic_tasa (+) = sp.ic_tasauf"   +
				"    AND sp.ic_emisor = e.ic_emisor (+)"   +
				"    AND sp.ic_tipo_credito = tc.ic_tipo_credito"   +
				"    AND sp.ic_periodicidad_c = p1.ic_periodicidad"   +
				"    AND sp.ic_periodicidad_i = p2.ic_periodicidad"   +
				"    AND sp.ic_tabla_amort = ta.ic_tabla_amort"   +
				"    AND sp.ic_moneda = m.ic_moneda (+)"   +
				"    AND sp.ic_estatus_solic = solic.ic_estatus_solic"+
				"    AND sp.ic_clase_docto = cd.ic_clase_docto (+)"+
				"    AND sp.ic_error_proceso = ep.ic_error_proceso (+)"+//FODEA 011-2010 FVR
				"    AND sp.cc_codigo_aplicacion = ep.cc_codigo_aplicacion (+)");//FODEA 011-2010 FVR
				
			conditions=new ArrayList();

			if(!"".equals(iNoCliente)){
					qrySentencia.append(" and SP.ic_if =? ");
					conditions.add(iNoCliente);
				}
			if(!"".equals(numAcuse)){
				qrySentencia.append(" and SP.cc_acuse =? ");
				conditions.add(numAcuse);
			}
			if(!"".equals(numFolio)){
				qrySentencia.append(" and SP.IC_SOLIC_PORTAL =?  ");
				conditions.add(numFolio);
			}
			if(!"".equals(numPrest)){
				qrySentencia.append(" and SP.ig_numero_prestamo = ? ");
				conditions.add(numPrest);
			}
			//MODIFICACION FODEA 011-2010 FVR INI
			if(!"".equals(fechaOper)){
				qrySentencia.append(" and sp.df_operacion >=  TRUNC(TO_DATE(?,'dd/mm/yyyy'))");
				conditions.add(fechaOper);
			}
			if(!"".equals(fechaOper2)){
				qrySentencia.append(" and sp.df_operacion <  TRUNC(TO_DATE(?,'dd/mm/yyyy')+1)");
				conditions.add(fechaOper2);

			}
			//MODIFICACION FODEA 011-2010 FVR FIN
			
			//FODEA 011-2010 FVR INI
			if(!"".equals(fechaIF)){
				qrySentencia.append(" and sp.df_carga >=  TRUNC(TO_DATE(?,'dd/mm/yyyy'))");
				conditions.add(fechaIF);
			}
			if(!"".equals(fechaIF2)){
				qrySentencia.append(" and sp.df_carga <  TRUNC(TO_DATE(?,'dd/mm/yyyy')+1)");
				conditions.add(fechaIF2);
			}
			//FODEA 011-2010 FVR FIN
			if(!"".equals(icEstatus)){
				qrySentencia.append(" and SP.ic_estatus_solic =? ");
				conditions.add(icEstatus);
			}
			
			System.out.println("ConsultaOperIfCE::getDocumentQueryFile::qrySentencia:"+qrySentencia.toString());
	    }catch(Exception e){
			System.out.println("ConsultaOperIfCE::getDocumentQueryFileException "+e);
		}
		return qrySentencia.toString();
 }
 public String getDocumentSummaryQueryForIds(List pageIds) {
 
 int i=0;
		StringBuffer qrySentencia = new StringBuffer();
		qrySentencia.append(
			" SELECT   /*+use_nl(sp p1 p2 cd tc m e tituf i)*/p.cg_razon_social"   +
			"              AS nombrepyme, sp.ig_clave_sirac AS clavesirac,"   +
			"        sp.ig_numero_docto AS numdocto,"   +
			"        TO_CHAR (sp.df_etc, 'DD/MM/YYYY') AS fechaetc,"   +
			"        TO_CHAR (sp.df_v_descuento, 'DD/MM/YYYY') AS fechadscto,"   +
			"        m.cd_nombre AS nombremoneda, tc.cd_descripcion AS desctipocred,"   +
			"        sp.fn_importe_docto AS importedocto,"   +
			"        sp.fn_importe_dscto AS importedscto,"   +
			"        sp.ic_solic_portal AS numfolio,"   +
			"        TO_CHAR (sp.df_operacion, 'DD/MM/YYYY') AS fechaoperacion,"   +
			"        sp.ig_numero_prestamo AS numprestamo, sp.cc_acuse AS numacuse"   +
			"        ,sp.ic_tasauf, sp.cg_rmuf, sp.fg_stuf, sp.ic_tabla_amort "+
			" 		 	,p1.cd_descripcion AS periodicidad_pago,solic.cd_descripcion as estatus" +
			"        ,TO_CHAR (sp.df_carga, 'DD/MM/YYYY') AS fechaEnvioIf"   +//FODEA 011-2010 FVR 
			" 		 	,sp.ic_error_proceso AS errorproceso, sp.cc_codigo_aplicacion as codigoaplicacion" +//FODEA 011-2010 FVR 
			" 		 	,ep.cg_descripcion AS errordescripcion" +//FODEA 011-2010 FVR
			"   FROM com_solic_portal sp,"   +
			"        comcat_pyme p,"   +
			"        comcat_tasa ti,"   +
			"        comcat_tasa tuf,"   +
			"        comcat_emisor e,"   +
			"        comcat_moneda m,"   +
			"        comcat_tipo_credito tc,"   +
			"        comcat_estatus_solic solic,"   +
			"        comcat_clase_docto cd,"   +
			"        comcat_periodicidad p1,"   +
			"        comcat_periodicidad p2,"   +
			"        comcat_tabla_amort ta,"   +
			"        comcat_error_procesos ep"   +//FODEA 011-2010 FVR
			"  WHERE p.in_numero_sirac (+) = sp.ig_clave_sirac"   +
			"    AND ti.ic_tasa = sp.ic_tasaif"   +
			"    AND tuf.ic_tasa (+) = sp.ic_tasauf"   +
			"    AND sp.ic_emisor = e.ic_emisor (+)"   +
			"    AND sp.ic_tipo_credito = tc.ic_tipo_credito"   +
			"    AND sp.ic_periodicidad_c = p1.ic_periodicidad"   +
			"    AND sp.ic_periodicidad_i = p2.ic_periodicidad"   +
			"    AND sp.ic_tabla_amort = ta.ic_tabla_amort"   +
			"    AND sp.ic_moneda = m.ic_moneda (+)"   +
			"    AND sp.ic_estatus_solic = solic.ic_estatus_solic"+
			"    AND sp.ic_clase_docto = cd.ic_clase_docto (+)"+
			"    AND sp.ic_error_proceso = ep.ic_error_proceso (+)"+//FODEA 011-2010 FVR
			"    AND sp.cc_codigo_aplicacion = ep.cc_codigo_aplicacion (+)"+//FODEA 011-2010 FVR
			"    AND sp.IC_SOLIC_PORTAL in(");
					conditions=new ArrayList();

		for(int j = 0; j < pageIds.size(); j++){
			List lItem = (ArrayList)pageIds.get(j);
				qrySentencia.append("?,");
				conditions.add(new String(lItem.get(0).toString()));      
			}
			qrySentencia.deleteCharAt(qrySentencia.length()-1);
			
			
		qrySentencia.append(")");
		//System.out.println("el query queda de la siguiente manera "+qrySentencia.toString());
		return qrySentencia.toString();
 }
 public String getDocumentQuery() {
 StringBuffer qrySentencia = new StringBuffer();
    	
	
	
    	try{
			qrySentencia.append(
				" SELECT /*+use_nl(sp p1 p2 cd tc m e ti tuf i)*/ "+
				"        sp.IC_SOLIC_PORTAL,'ConsultaOperIfCE::getDocumentQuery'"   + 
				"   FROM com_solic_portal sp,"   +
				"        comcat_pyme p,"   +
				"        comcat_tasa ti,"   +
				"        comcat_tasa tuf,"   +
				"        comcat_emisor e,"   +
				"        comcat_moneda m,"   +
				"        comcat_tipo_credito tc,"   +
				"        comcat_clase_docto cd,"   +
				"        comcat_periodicidad p1,"   +
				"        comcat_periodicidad p2,"   +
				"        comcat_tabla_amort ta"   +
				"  WHERE p.in_numero_sirac (+) = sp.ig_clave_sirac"   +
				"    AND ti.ic_tasa = sp.ic_tasaif"   +
				"    AND tuf.ic_tasa (+) = sp.ic_tasauf"   +
				"    AND sp.ic_emisor = e.ic_emisor (+)"   +
				"    AND sp.ic_tipo_credito = tc.ic_tipo_credito"   +
				"    AND sp.ic_periodicidad_c = p1.ic_periodicidad"   +
				"    AND sp.ic_periodicidad_i = p2.ic_periodicidad"   +
				"    AND sp.ic_tabla_amort = ta.ic_tabla_amort"   +
				"    AND sp.ic_moneda = m.ic_moneda (+)"   +
				"    AND sp.ic_clase_docto = cd.ic_clase_docto (+)");
				
			conditions=new ArrayList();
			if(!"".equals(iNoCliente)){
					qrySentencia.append(" and SP.ic_if =? ");
					conditions.add(iNoCliente);
				}
			if(!"".equals(numAcuse)){
				qrySentencia.append(" and SP.cc_acuse =? ");
				conditions.add(numAcuse);
			}
			if(!"".equals(numFolio)){
				qrySentencia.append(" and SP.IC_SOLIC_PORTAL =?  ");
				conditions.add(numFolio);
			}
			if(!"".equals(numPrest)){
				qrySentencia.append(" and SP.ig_numero_prestamo = ? ");
				conditions.add(numPrest);
			}
			//MODIFICACION FODEA 011-2010 FVR INI
			if(!"".equals(fechaOper)){
				qrySentencia.append(" and sp.df_operacion >=  TRUNC(TO_DATE(?,'dd/mm/yyyy'))");
				conditions.add(fechaOper);
			}
			if(!"".equals(fechaOper2)){
				qrySentencia.append(" and sp.df_operacion <  TRUNC(TO_DATE(?,'dd/mm/yyyy')+1)");
				conditions.add(fechaOper2);

			}
			//MODIFICACION FODEA 011-2010 FVR FIN
			
			//FODEA 011-2010 FVR INI
			if(!"".equals(fechaIF)){
				qrySentencia.append(" and sp.df_carga >=  TRUNC(TO_DATE(?,'dd/mm/yyyy'))");
				conditions.add(fechaIF);
			}
			if(!"".equals(fechaIF2)){
				qrySentencia.append(" and sp.df_carga <  TRUNC(TO_DATE(?,'dd/mm/yyyy')+1)");
				conditions.add(fechaIF2);
			}
			//FODEA 011-2010 FVR FIN
			if(!"".equals(icEstatus)){
				qrySentencia.append(" and SP.ic_estatus_solic =? ");
				conditions.add(icEstatus);
			}
			
		    System.out.println("EL query de la llave primaria: "+qrySentencia.toString());
		}catch(Exception e){
			System.out.println("ConsultaOperIfCE::getDocumentQueryException "+e);
    	}
		return qrySentencia.toString();
 }
	// Fin Metodos  IQueryGeneratorRegExtJS

	
	
	
	
	//Listo
	public String getAggregateCalculationQuery(HttpServletRequest request)  {

	StringBuffer qrySentencia = new StringBuffer();
	
	String	numPrestamo = (request.getParameter("numPrestamo")==null)?"":request.getParameter("numPrestamo");
	String	solicPortal = (request.getParameter("solicPortal")==null)?"":request.getParameter("solicPortal").trim(),
			acuse 		= (request.getParameter("acuse")==null)?"":request.getParameter("acuse").trim(),
			consultar 	= (request.getParameter("consultar")==null)?"":request.getParameter("consultar"),
			estatus 	= (request.getParameter("estatus")==null)?"":request.getParameter("estatus");
	String	ic_if 		= (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
	String	fecOper_de 	= (request.getParameter("fecOper_de")==null)?"":request.getParameter("fecOper_de");
	String	fecOper_a= (request.getParameter("fecOper_a")==null)?"":request.getParameter("fecOper_a");
	//FODEA 011-2010 FVR - INI
	String	fechaEnvioIf= (request.getParameter("fechaEnvioIf")==null)?"":request.getParameter("fechaEnvioIf");
	String	fechaEnvioIfFin= (request.getParameter("fechaEnvioIfFin")==null)?"":request.getParameter("fechaEnvioIfFin");
	
    try{
		qrySentencia.append(
			" SELECT /*+use_nl(sp p1 p2 cd tc m e ti tuf i)*/ "+
			" 		 count(1) as ConsultaOperIfCE "+
			"   FROM com_solic_portal sp,"   +
			"        comcat_pyme p,"   +
			"        comcat_tasa ti,"   + 
			"        comcat_tasa tuf,"   +
			"        comcat_emisor e,"   +
			"        comcat_moneda m,"   +
			"        comcat_tipo_credito tc,"   +
			"        comcat_clase_docto cd,"   +
			"        comcat_periodicidad p1,"   +
			"        comcat_periodicidad p2,"   +
			"        comcat_tabla_amort ta"   +
			"  WHERE p.in_numero_sirac (+) = sp.ig_clave_sirac"   +
			"    AND ti.ic_tasa = sp.ic_tasaif"   +
			"    AND tuf.ic_tasa (+) = sp.ic_tasauf"   +
			"    AND sp.ic_emisor = e.ic_emisor (+)"   +
			"    AND sp.ic_tipo_credito = tc.ic_tipo_credito"   +
			"    AND sp.ic_periodicidad_c = p1.ic_periodicidad"   +
			"    AND sp.ic_periodicidad_i = p2.ic_periodicidad"   +
			"    AND sp.ic_tabla_amort = ta.ic_tabla_amort"   +
			"    AND sp.ic_moneda = m.ic_moneda (+)"   +
			"    AND sp.ic_clase_docto = cd.ic_clase_docto (+)");//FODEA 011-2010 FVR

			qrySentencia.append((ic_if.equals("")?"":" and SP.ic_if = "+ic_if));

			if(!"".equals(acuse))
				qrySentencia.append(" and SP.cc_acuse ='"+acuse+"'");
			if(!"".equals(solicPortal))
				qrySentencia.append(" and SP.IC_SOLIC_PORTAL = "+solicPortal);
			if(!"".equals(numPrestamo))
				qrySentencia.append(" and SP.ig_numero_prestamo = "+numPrestamo);
			//MODIFICACION FODEA 011-2010 FVR INI
			if(!"".equals(fecOper_de))
				qrySentencia.append(" and sp.df_operacion >=  TRUNC(TO_DATE('"+fecOper_de+"','dd/mm/yyyy'))");
			if(!"".equals(fecOper_a))
				qrySentencia.append(" and sp.df_operacion <  TRUNC(TO_DATE('"+fecOper_a+"','dd/mm/yyyy')+1)");
			//MODIFICACION FODEA 011-2010 FVR FIN
			
			//FODEA 011-2010 FVR INI
			if(!"".equals(fechaEnvioIf))
				qrySentencia.append(" and sp.df_carga >=  TRUNC(TO_DATE('"+fechaEnvioIf+"','dd/mm/yyyy'))");
			if(!"".equals(fechaEnvioIfFin))
				qrySentencia.append(" and sp.df_carga <  TRUNC(TO_DATE('"+fechaEnvioIfFin+"','dd/mm/yyyy')+1)");
			//FODEA 011-2010 FVR FIN
			if(!"".equals(estatus))
				qrySentencia.append(" and SP.ic_estatus_solic = "+estatus);
			System.out.println("ConsultaOperIfCE::getAggregateCalculationQuery::qrySentencia:"+qrySentencia.toString());

    }catch(Exception e){
      System.out.println("ConsultaOperIfCE::getDocumentQueryFileException "+e);
    }
    return qrySentencia.toString();
  }
	//Listo
	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids)  {
  		int i=0;
		StringBuffer qrySentencia = new StringBuffer();
		qrySentencia.append(
			" SELECT   /*+use_nl(sp p1 p2 cd tc m e tituf i)*/p.cg_razon_social"   +
			"              AS nombrepyme, sp.ig_clave_sirac AS clavesirac,"   +
			"        sp.ig_numero_docto AS numdocto,"   +
			"        TO_CHAR (sp.df_etc, 'DD/MM/YYYY') AS fechaetc,"   +
			"        TO_CHAR (sp.df_v_descuento, 'DD/MM/YYYY') AS fechadscto,"   +
			"        m.cd_nombre AS nombremoneda, tc.cd_descripcion AS desctipocred,"   +
			"        sp.fn_importe_docto AS importedocto,"   +
			"        sp.fn_importe_dscto AS importedscto,"   +
			"        sp.ic_solic_portal AS numfolio,"   +
			"        TO_CHAR (sp.df_operacion, 'DD/MM/YYYY') AS fechaoperacion,"   +
			"        sp.ig_numero_prestamo AS numprestamo, sp.cc_acuse AS numacuse"   +
			"        ,sp.ic_tasauf, sp.cg_rmuf, sp.fg_stuf, sp.ic_tabla_amort "+
			" 		 	,p1.cd_descripcion AS periodicidad_pago,solic.cd_descripcion as estatus" +
			"        ,TO_CHAR (sp.df_carga, 'DD/MM/YYYY') AS fechaEnvioIf"   +//FODEA 011-2010 FVR 
			" 		 	,sp.ic_error_proceso AS errorproceso, sp.cc_codigo_aplicacion as codigoaplicacion" +//FODEA 011-2010 FVR 
			" 		 	,ep.cg_descripcion AS errordescripcion" +//FODEA 011-2010 FVR
			"   FROM com_solic_portal sp,"   +
			"        comcat_pyme p,"   +
			"        comcat_tasa ti,"   +
			"        comcat_tasa tuf,"   +
			"        comcat_emisor e,"   +
			"        comcat_moneda m,"   +
			"        comcat_tipo_credito tc,"   +
			"        comcat_estatus_solic solic,"   +
			"        comcat_clase_docto cd,"   +
			"        comcat_periodicidad p1,"   +
			"        comcat_periodicidad p2,"   +
			"        comcat_tabla_amort ta,"   +
			"        comcat_error_procesos ep"   +//FODEA 011-2010 FVR
			"  WHERE p.in_numero_sirac (+) = sp.ig_clave_sirac"   +
			"    AND ti.ic_tasa = sp.ic_tasaif"   +
			"    AND tuf.ic_tasa (+) = sp.ic_tasauf"   +
			"    AND sp.ic_emisor = e.ic_emisor (+)"   +
			"    AND sp.ic_tipo_credito = tc.ic_tipo_credito"   +
			"    AND sp.ic_periodicidad_c = p1.ic_periodicidad"   +
			"    AND sp.ic_periodicidad_i = p2.ic_periodicidad"   +
			"    AND sp.ic_tabla_amort = ta.ic_tabla_amort"   +
			"    AND sp.ic_moneda = m.ic_moneda (+)"   +
			"    AND sp.ic_estatus_solic = solic.ic_estatus_solic"+
			"    AND sp.ic_clase_docto = cd.ic_clase_docto (+)"+
			"    AND sp.ic_error_proceso = ep.ic_error_proceso (+)"+//FODEA 011-2010 FVR
			"    AND sp.cc_codigo_aplicacion = ep.cc_codigo_aplicacion (+)"+//FODEA 011-2010 FVR
			"    AND sp.IC_SOLIC_PORTAL in(");
		i=0;
		for (Iterator it = ids.iterator(); it.hasNext();i++){
        	if(i>0){
          		qrySentencia.append(",");
        	}
			qrySentencia.append("'"+it.next().toString()+"'");
		}
		qrySentencia.append(")");
		System.out.println("el query queda de la siguiente manera "+qrySentencia.toString());
		return qrySentencia.toString();
  }
	//Migrado
	public String getDocumentQuery(HttpServletRequest request)	{
    	StringBuffer qrySentencia = new StringBuffer();
    	
		String	numPrestamo = (request.getParameter("numPrestamo")==null)?"":request.getParameter("numPrestamo");
		String	solicPortal = (request.getParameter("solicPortal")==null)?"":request.getParameter("solicPortal").trim();
		String	acuse 		= (request.getParameter("acuse")==null)?"":request.getParameter("acuse").trim();
		String	consultar 	= (request.getParameter("consultar")==null)?"":request.getParameter("consultar");
		String	estatus		= (request.getParameter("estatus")==null)?"":request.getParameter("estatus");
		String	ic_if 		= (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String	fecOper_de 	= (request.getParameter("fecOper_de")==null)?"":request.getParameter("fecOper_de");
		String	fecOper_a= (request.getParameter("fecOper_a")==null)?"":request.getParameter("fecOper_a");
		
		//FODEA 011-2010 FVR INI
		String	fechaEnvioIf= (request.getParameter("fechaEnvioIf")==null)?"":request.getParameter("fechaEnvioIf");
		String	fechaEnvioIfFin= (request.getParameter("fechaEnvioIfFin")==null)?"":request.getParameter("fechaEnvioIfFin");
		
	
    	try{
			qrySentencia.append(
				" SELECT /*+use_nl(sp p1 p2 cd tc m e ti tuf i)*/ "+
				"        sp.IC_SOLIC_PORTAL,'ConsultaOperIfCE::getDocumentQuery'"   + 
				"   FROM com_solic_portal sp,"   +
				"        comcat_pyme p,"   +
				"        comcat_tasa ti,"   +
				"        comcat_tasa tuf,"   +
				"        comcat_emisor e,"   +
				"        comcat_moneda m,"   +
				"        comcat_tipo_credito tc,"   +
				"        comcat_clase_docto cd,"   +
				"        comcat_periodicidad p1,"   +
				"        comcat_periodicidad p2,"   +
				"        comcat_tabla_amort ta"   +
				"  WHERE p.in_numero_sirac (+) = sp.ig_clave_sirac"   +
				"    AND ti.ic_tasa = sp.ic_tasaif"   +
				"    AND tuf.ic_tasa (+) = sp.ic_tasauf"   +
				"    AND sp.ic_emisor = e.ic_emisor (+)"   +
				"    AND sp.ic_tipo_credito = tc.ic_tipo_credito"   +
				"    AND sp.ic_periodicidad_c = p1.ic_periodicidad"   +
				"    AND sp.ic_periodicidad_i = p2.ic_periodicidad"   +
				"    AND sp.ic_tabla_amort = ta.ic_tabla_amort"   +
				"    AND sp.ic_moneda = m.ic_moneda (+)"   +
				"    AND sp.ic_clase_docto = cd.ic_clase_docto (+)");
				
			qrySentencia.append((ic_if.equals("")?"":" and SP.ic_if = "+ic_if));

			if(!"".equals(acuse))
				qrySentencia.append(" and SP.cc_acuse ='"+acuse+"'");
			if(!"".equals(solicPortal))
				qrySentencia.append(" and SP.IC_SOLIC_PORTAL = "+solicPortal);
			if(!"".equals(numPrestamo))
				qrySentencia.append(" and SP.ig_numero_prestamo = "+numPrestamo);
			//MODIFICACION FODEA 011-2010 FVR INI
			if(!"".equals(fecOper_de))
				qrySentencia.append(" and sp.df_operacion >=  TRUNC(TO_DATE('"+fecOper_de+"','dd/mm/yyyy'))");
			if(!"".equals(fecOper_a))
				qrySentencia.append(" and sp.df_operacion <  TRUNC(TO_DATE('"+fecOper_a+"','dd/mm/yyyy')+1)");
			//MODIFICACION FODEA 011-2010 FVR FIN
			
			//FODEA 011-2010 FVR INI
			if(!"".equals(fechaEnvioIf))
				qrySentencia.append(" and sp.df_carga >=  TRUNC(TO_DATE('"+fechaEnvioIf+"','dd/mm/yyyy'))");
			if(!"".equals(fechaEnvioIfFin))
				qrySentencia.append(" and sp.df_carga <  TRUNC(TO_DATE('"+fechaEnvioIfFin+"','dd/mm/yyyy')+1)");
			//FODEA 011-2010 FVR FIN
			if(!"".equals(estatus))
				qrySentencia.append(" and SP.ic_estatus_solic = "+estatus);
			
		    System.out.println("EL query de la llave primaria: "+qrySentencia.toString());
		}catch(Exception e){
			System.out.println("ConsultaOperIfCE::getDocumentQueryException "+e);
    	}
		return qrySentencia.toString();
	}
	//Migrado
	public String getDocumentQueryFile(HttpServletRequest request) {
    	StringBuffer qrySentencia = new StringBuffer();
    	
		String	numPrestamo = (request.getParameter("numPrestamo")==null)?"":request.getParameter("numPrestamo");
		String	solicPortal = (request.getParameter("solicPortal")==null)?"":request.getParameter("solicPortal").trim();
		String	acuse 		= (request.getParameter("acuse")==null)?"":request.getParameter("acuse").trim();
		String	consultar 	= (request.getParameter("consultar")==null)?"":request.getParameter("consultar");
		String	estatus	 	= (request.getParameter("estatus")==null)?"":request.getParameter("estatus");
		String	ic_if 		= (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String	fecOper_de 	= (request.getParameter("fecOper_de")==null)?"":request.getParameter("fecOper_de");
		String	fecOper_a= (request.getParameter("fecOper_a")==null)?"":request.getParameter("fecOper_a");
		
		String	fechaEnvioIf= (request.getParameter("fechaEnvioIf")==null)?"":request.getParameter("fechaEnvioIf");//FODEA 011-2010 FVR
		String	fechaEnvioIfFin= (request.getParameter("fechaEnvioIfFin")==null)?"":request.getParameter("fechaEnvioIfFin");//FODEA 011-2010 FVR

	    try {
			qrySentencia.append(
				" SELECT   /*+use_nl(sp p1 p2 cd tc m e tituf i)*/p.cg_razon_social"   +
				"              AS nombrepyme, sp.ig_clave_sirac AS clavesirac,"   +
				"        sp.ig_numero_docto AS numdocto,"   +
				"        TO_CHAR (sp.df_etc, 'DD/MM/YYYY') AS fechaetc,"   +
				"        TO_CHAR (sp.df_v_descuento, 'DD/MM/YYYY') AS fechadscto,"   +
				"        m.cd_nombre AS nombremoneda, tc.cd_descripcion AS desctipocred,"   +
				"        sp.fn_importe_docto AS importedocto,"   +
				"        sp.fn_importe_dscto AS importedscto,"   +
				"        sp.ic_solic_portal AS numfolio,"   +
				"        TO_CHAR (sp.df_operacion, 'DD/MM/YYYY') AS fechaoperacion,"   +
				"        sp.ig_numero_prestamo AS numprestamo, sp.cc_acuse AS numacuse"   +
				"			,sp.ic_tasauf, sp.cg_rmuf, sp.fg_stuf "+
				"			,p1.cd_descripcion AS periodicidad_pago,solic.cd_descripcion as estatus" +
				"        ,TO_CHAR (sp.df_carga, 'DD/MM/YYYY') AS fechaEnvioIf"   +//FODEA 011-2010 FVR 
				" 		 	,sp.ic_error_proceso AS errorproceso, sp.cc_codigo_aplicacion as codigoaplicacion" +//FODEA 011-2010 FVR 
				" 		 	,ep.cg_descripcion AS errordescripcion" +//FODEA 011-2010 FVR
				"   FROM com_solic_portal sp,"   +
				"        comcat_pyme p,"   +
				"        comcat_tasa ti,"   +
				"        comcat_tasa tuf,"   +
				"        comcat_emisor e,"   +
				"        comcat_moneda m,"   +
				"        comcat_tipo_credito tc,"   +
				"        comcat_estatus_solic solic,"   +
				"        comcat_clase_docto cd,"   +
				"        comcat_periodicidad p1,"   +
				"        comcat_periodicidad p2,"   +
				"        comcat_tabla_amort ta,"   +
				"        comcat_error_procesos ep"   +//FODEA 011-2010 FVR
				"  WHERE p.in_numero_sirac (+) = sp.ig_clave_sirac"   +
				"    AND ti.ic_tasa = sp.ic_tasaif"   +
				"    AND tuf.ic_tasa (+) = sp.ic_tasauf"   +
				"    AND sp.ic_emisor = e.ic_emisor (+)"   +
				"    AND sp.ic_tipo_credito = tc.ic_tipo_credito"   +
				"    AND sp.ic_periodicidad_c = p1.ic_periodicidad"   +
				"    AND sp.ic_periodicidad_i = p2.ic_periodicidad"   +
				"    AND sp.ic_tabla_amort = ta.ic_tabla_amort"   +
				"    AND sp.ic_moneda = m.ic_moneda (+)"   +
				"    AND sp.ic_estatus_solic = solic.ic_estatus_solic"+
				"    AND sp.ic_clase_docto = cd.ic_clase_docto (+)"+
				"    AND sp.ic_error_proceso = ep.ic_error_proceso (+)"+//FODEA 011-2010 FVR
				"    AND sp.cc_codigo_aplicacion = ep.cc_codigo_aplicacion (+)");//FODEA 011-2010 FVR
				
			qrySentencia.append((ic_if.equals("")?"":" and SP.ic_if = "+ic_if));

			if(!"".equals(acuse))
				qrySentencia.append(" and SP.cc_acuse ='"+acuse+"'");
			if(!"".equals(solicPortal))
				qrySentencia.append(" and SP.IC_SOLIC_PORTAL = "+solicPortal);
			if(!"".equals(numPrestamo))
				qrySentencia.append(" and SP.ig_numero_prestamo = "+numPrestamo);
			//MODIFICACION FODEA 011-2010 FVR INI
			if(!"".equals(fecOper_de))
				qrySentencia.append(" and sp.df_operacion >=  TRUNC(TO_DATE('"+fecOper_de+"','dd/mm/yyyy'))");
			if(!"".equals(fecOper_a))
				qrySentencia.append(" and sp.df_operacion <  TRUNC(TO_DATE('"+fecOper_a+"','dd/mm/yyyy')+1)");
			//MODIFICACION FODEA 011-2010 FVR FIN
			
			//FODEA 011-2010 FVR INI
			if(!"".equals(fechaEnvioIf))
				qrySentencia.append(" and sp.df_carga >=  TRUNC(TO_DATE('"+fechaEnvioIf+"','dd/mm/yyyy'))");
			if(!"".equals(fechaEnvioIfFin))
				qrySentencia.append(" and sp.df_carga <  TRUNC(TO_DATE('"+fechaEnvioIfFin+"','dd/mm/yyyy')+1)");
			//FODEA 011-2010 FVR FIN
			if(!"".equals(estatus))
				qrySentencia.append(" and SP.ic_estatus_solic = "+estatus);
			
			System.out.println("ConsultaOperIfCE::getDocumentQueryFile::qrySentencia:"+qrySentencia.toString());
	    }catch(Exception e){
			System.out.println("ConsultaOperIfCE::getDocumentQueryFileException "+e);
		}
		return qrySentencia.toString();
	}


	public void setNumAcuse(String numAcuse) {
		this.numAcuse = numAcuse;
	}


	public String getNumAcuse() {
		return numAcuse;
	}


	public void setNumFolio(String numFolio) {
		this.numFolio = numFolio;
	}


	public String getNumFolio() {
		return numFolio;
	}


	public void setNumPrest(String numPrest) {
		this.numPrest = numPrest;
	}


	public String getNumPrest() {
		return numPrest;
	}


	public void setFechaIF(String fechaIF) {
		this.fechaIF = fechaIF;
	}


	public String getFechaIF() {
		return fechaIF;
	}


	public void setFechaIF2(String fechaIF2) {
		this.fechaIF2 = fechaIF2;
	}


	public String getFechaIF2() {
		return fechaIF2;
	}


	public void setFechaOper(String fechaOper) {
		this.fechaOper = fechaOper;
	}


	public String getFechaOper() {
		return fechaOper;
	}


	public void setFechaOper2(String fechaOper2) {
		this.fechaOper2 = fechaOper2;
	}


	public String getFechaOper2() {
		return fechaOper2;
	}


	public void setIcEstatus(String icEstatus) {
		this.icEstatus = icEstatus;
	}


	public String getIcEstatus() {
		return icEstatus;
	}
	public void setConditions(List conditions) {
		this.conditions = conditions;
		}
	
	
	public List getConditions() {
			return conditions;
		}


	public void setINoCliente(String iNoCliente) {
		this.iNoCliente = iNoCliente;
	}


	public String getINoCliente() {
		return iNoCliente;
	}
}