package com.netro.credito;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class GeneraPDFCreditoElectronico  {

	private final static Log log = ServiceLocator.getInstance().getLog(GeneraPDFCreditoElectronico.class);
	private String acuse;
	private String fecha;
	private String hora;
	private String usuario;
	private String solicProcesadas;
	
	public GeneraPDFCreditoElectronico() {}
	
	/**
	 * Recibe paramentros del jsp 26forma03ext.data.jsp para generar el PDF de las
	 * solicitudes a las que se les cambi� el estatus
	 * @return nombreArchivo
	 */
	public String generaPdfSolicitudesProcesadas(Vector vecFilas, HttpServletRequest request, String path){//
	log.info(" generaPdfSolicitudesProcesadas (E)");
	String nombreArchivo = "";
	HttpSession session = request.getSession();
	ComunesPDF pdfDoc = new ComunesPDF();
	CreaArchivo creaArchivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer();
	OutputStreamWriter writer = null;
	BufferedWriter buffer = null;
	Vector vecColumnas = null;

	try{

			Comunes comunes = new Comunes();
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
		
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
			((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
			(String)session.getAttribute("sesExterno"),
			(String) session.getAttribute("strNombre"),
			(String) session.getAttribute("strNombreUsuario"),
			(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			pdfDoc.addText(" 		","formas",ComunesPDF.RIGHT);
			pdfDoc.addText("OPERACIONES QUE SUFRIERON CAMBIO DE ESTATUS ","formas",ComunesPDF.CENTER);
			
			pdfDoc.setTable(2,75);
			pdfDoc.setCell("Resumen de solicitudes procesadas ","celda01",ComunesPDF.CENTER,2);
			pdfDoc.setCell("N�mero de Acuse ","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(acuse,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("N�mero de solicitudes procesadas ","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(solicProcesadas,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de proceso ","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fecha,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("Hora de proceso ","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(hora,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell("Usuario ","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(usuario,"formas",ComunesPDF.CENTER);
			pdfDoc.addTable();	
			
			pdfDoc.addText(" 		","formas",ComunesPDF.RIGHT);
			
			pdfDoc.setTable(3,75);
			pdfDoc.setCell("N�mero de folio ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("N�mero de pr�stamo ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de operaci�n ","celda01",ComunesPDF.CENTER);
			for (int i=0; i<vecFilas.size(); i++) {
				vecColumnas = (Vector)vecFilas.get(i);
				String folio = vecColumnas.get(0).toString();
				String prestamo = vecColumnas.get(1).toString();
				String fecOpe = vecColumnas.get(2).toString();
				pdfDoc.setCell(folio,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(prestamo,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fecOpe,"formas",ComunesPDF.CENTER);
			}
			pdfDoc.addTable();			
			
			pdfDoc.endDocument();		
		
	} catch(Throwable e) {
		throw new AppException("Error al generar el archivo PDF", e);
	}

	log.info(" generaPdfSolicitudesProcesadas (S)");
	return nombreArchivo;
	
	}

	public String getAcuse() {
		return acuse;
	}

	public void setAcuse(String acuse) {
		this.acuse = acuse;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getSolicProcesadas() {
		return solicProcesadas;
	}

	public void setSolicProcesadas(String solicProcesadas) {
		this.solicProcesadas = solicProcesadas;
	}


	

	
}