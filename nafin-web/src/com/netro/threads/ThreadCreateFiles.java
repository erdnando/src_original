package com.netro.threads;

import java.io.Serializable;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.IQueryGeneratorThreadRegExtJS;
import netropology.utilerias.RequestFake;

public class ThreadCreateFiles extends Thread {
    private int countReg;
    private int totalCountReg;

    private int percent;
    private int status;
    private String nombreArchivo;
    private IQueryGeneratorThreadRegExtJS qryGen;
    private RequestFake requestFake;
    
    private ResultSet rs;
    private String query;
    private List condiciones;

    private String path;
    private String tipo;
    
    private boolean started;
    private boolean running;
    private boolean error;

    
    public ThreadCreateFiles() {
        started = false;
        running = false;
        error = false;
        nombreArchivo = "";
        status = 0;
    }

    
    protected void createFilePaginador(){
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            con.conexionDB();
            
            ps = con.queryPrecompilado("Select count(*) from ( "+ this.qryGen.getDocumentQueryFile() + " )", this.qryGen.getConditions());
            rs = ps.executeQuery();
            rs.next();
            totalCountReg = rs.getInt(1);
            rs.close();
            ps.close();
            
            ps = con.queryPrecompilado(this.qryGen.getDocumentQueryFile(), this.qryGen.getConditions()); 
            long tiempoInicial = (new java.util.Date()).getTime();
            rs = ps.executeQuery();
            long tiempoFinal = (new java.util.Date()).getTime();
            System.out.println("ThreadCreateFiles.createFilePaginador():: t=" + String.valueOf(tiempoFinal-tiempoInicial));
            nombreArchivo =  qryGen.crearCustomFile(requestFake, rs, path, tipo);
            System.out.println("nombreArchivo =========== "+nombreArchivo);
            percent = 100;
        }catch(Exception e){
            setRunning(false);
            setError(true);
            percent = 100;
            e.printStackTrace();
        }finally{
            if (con.hayConexionAbierta()) {
                try {
                        if (rs != null) {
                                rs.close();
                        }
                        if (ps!=null) {
                                ps.close();
                        }
                } catch(Exception ex) {}
                con.terminaTransaccion(true); //Para consultas que hagan uso de tablas remotas via dblink
                con.cierraConexionDB();
            }
        }
    }
    
    public int getPercent() {
        if(getTotalCountReg()>0){
            percent = (getCountReg() * 100)/getTotalCountReg();
        }
        return percent;
    }

    public String getNombreArchivo() {
        return nombreArchivo;
    }

    @Override
    public void run() {
        // TODO Implement this method
        try{
            setRunning(true);
            createFilePaginador();
        }finally{
            setRunning(false);
        }
    }

    public int getCountReg() {
        countReg = qryGen.getCountReg();
        return countReg;
    }

    public int getTotalCountReg() {
        return totalCountReg;
    }
    
    public void setTotalCountReg(int totalCountReg) {
        this.totalCountReg = totalCountReg;
    }
    
    public void setInterface(IQueryGeneratorThreadRegExtJS qryGen) {
        this.qryGen = qryGen;
    }

    public void setQueryResult(ResultSet rs) {
        this.rs = rs;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
    
    public void setQuery(String query) {
        this.query = query;
    }

    public void setCondiciones(List condiciones) {
        this.condiciones = condiciones;
    }
    
    public void setRequestFake(RequestFake requestFake) {
        this.requestFake = requestFake;
    }

    public RequestFake getRequestFake() {
        return requestFake;
    }
    //******************************************************
    public synchronized boolean isStarted() {
       return started;
    }
    
    public synchronized boolean isCompleted() {
                return((percent==100));
    }
    
    public synchronized boolean isRunning() {
       return running;
    }
    
    public synchronized void setRunning(boolean running) {
       this.running = running;
       if (running)
           started = true;
    }
         
        
    public synchronized void setError(boolean error) {
       this.error = error;
    }
        
    
    public synchronized boolean hasError() {
       return error;
    }
}
