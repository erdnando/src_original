package com.netro.garantias;

import com.netro.pdf.ComunesPDF;

import java.util.HashMap;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**

	Esta clase se encarga de generar el archivo PDF del acuse de la carga masiva de calificaciones de cartera.
		ADMIN_IF_GARAN - PROGRAMA DE GARANT�A AUTOM�TICA - CARGA DE CALIFICACION DE CARTERA - CARGA DE ARCHIVO
	
	@author Jesus Salim Hernandez David
	@since  Fodea 004 - 2012 -- Migracion IF
	@date	  31/05/2012 06:22:08 p.m.
	
 */
public class CargaArchivoCalificacionCartera {
	
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(CargaArchivoCalificacionCartera.class);

	/**
		Crea un archivo PDF con la informaci�n del acuse de la carga
		@throws AppException 
		
		@param rg 												<tt>ResultadosGar</tt> con el resultado de la transmision de solicitudes.
		@param loginUsuario  								<tt>String</tt> con el login del usuario.
		@param cabecera       								<tt>HashMap</tt> con la informacion de la cabcera del PDF.
		@param strDirectorioTemp   						<tt>String</tt> con la ruta del directorio temporal donde se creara el archivo PDF.
		@param strDirectorioPublicacion 					<tt>String</tt> con la ruta WEB del directorio de publicacion. 
		@param totalRegistros    							<tt>String</tt> con el numero de solicitudes transmitidas.
		@param montoExposicionIncumplimientoControl  <tt>String</tt> con el Monto total de Exposicion al Incumplimiento.
		@param nombreUsuario   								<tt>String</tt> con el nombre del usuario firmado en el sistema.
		
		@return <tt>String</tt> con el nombre del archivo generado.
		
		@author jshernandez
		@since F036 - 2013 -- GARANTIAS - Nueva metodolog�a calificaci�n; 19/11/2013 07:23:00 p.m.
	*/	
	public static String generaAcuseArchivoPDF(
			ResultadosGar 	rg, 
			String 			loginUsuario, 
			HashMap 			cabecera, 
			String 			strDirectorioTemp, 
			String 			strDirectorioPublicacion, 
			String 			totalRegistros, 
			String 			montoExposicionIncumplimientoControl,
			String 			nombreUsuario )
		throws AppException {
 
		log.info("generaAcuseArchivoPDF(E)");
		
		String 		nombreArchivo 	= null;
		CreaArchivo archivo 			= null;
		ComunesPDF 	pdfDoc 			= null;
		
		try {
			
			archivo 			= new CreaArchivo();
			nombreArchivo 	= archivo.nombreArchivo()+".pdf";
	 
			// pdfDoc 			= new ComunesPDF(2,strDirectorioTemp+loginUsuario+"."+nombreArchivo);
			pdfDoc 			= new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
			
			String meses[]	 		= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    	= fechaActual.substring(0,2);
			String mesActual   	= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   	= fechaActual.substring(6,10);
			String horaActual  	= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
			pdfDoc.encabezadoConImagenes(
						pdfDoc,
						(String)	cabecera.get("strPais"),
						(String) cabecera.get("iNoNafinElectronico"),
						(String)	cabecera.get("sesExterno"),
						(String) cabecera.get("strNombre"),
						(String) cabecera.get("strNombreUsuario"),
						(String)	cabecera.get("strLogo"),
						strDirectorioPublicacion);
	 
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
			pdfDoc.addText("Su solicitud fue recibida con el n�mero de folio: "+rg.getFolio(),"formas",ComunesPDF.CENTER);
			pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
			
			pdfDoc.setTable(2,70);
			
			pdfDoc.setCell("CIFRAS CONTROL",									"celda03",	ComunesPDF.CENTER,2,1,1);
			
			pdfDoc.setCell("Tipo de Operaci�n",								"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell("05 Calificacion de Cartera",					"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.setCell("No. total de registros transmitidos",						"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(totalRegistros,									"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.setCell("Total de Exposici�n al Incumplimiento",	"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(montoExposicionIncumplimientoControl,2),"formas",ComunesPDF.RIGHT);
					
			pdfDoc.setCell("Fecha de carga",									"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(rg.getFecha(),										"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.setCell("Hora de carga",									"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(rg.getHora(),										"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.setCell("Usuario",											"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(loginUsuario+" - "+nombreUsuario,	   	"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.addTable();
			pdfDoc.endDocument();
		
		}catch(Exception e){
			
			log.error("generaAcuseArchivoPDF(Exception)");
			e.printStackTrace();
			log.error("rg                                   = <" + rg                                		+ ">");
			log.error("loginUsuario                         = <" + loginUsuario                      		+ ">");
			log.error("cabecera                             = <" + cabecera                          		+ ">");
			log.error("strDirectorioTemp                    = <" + strDirectorioTemp                 		+ ">");
			log.error("strDirectorioPublicacion             = <" + strDirectorioPublicacion          		+ ">");
			log.error("totalRegistros                       = <" + totalRegistros                    		+ ">");
			log.error("montoExposicionIncumplimientoControl = <" + montoExposicionIncumplimientoControl  + ">");
			log.error("nombreUsuario                        = <" + nombreUsuario                     		+ ">"); 
			throw new AppException("Ocurri� un error al generar el PDF con la informaci�n del acuse.");
			
		}
		log.info("generaAcuseArchivoPDF(S)");
		
		return nombreArchivo;	
	
	}
	
	/**
		@deprecated A partir del F036 - 2013 -- GARANTIAS - Nueva metodolog�a calificaci�n este m�todo ha quedado deprecado.
		Crea un archivo PDF con la informaci�n del acuse de la carga
		@throws AppException 
		
		@param rg 												<tt>ResultadosGar</tt> con el resultado de la transmision de solicitudes.
		@param loginUsuario  								<tt>String</tt> con el login del usuario.
		@param cabecera       								<tt>HashMap</tt> con la informacion de la cabcera del PDF.
		@param strDirectorioTemp   						<tt>String</tt> con la ruta del directorio temporal donde se creara el archivo PDF.
		@param strDirectorioPublicacion 					<tt>String</tt> con la ruta WEB del directorio de publicacion. 
		@param totalRegistros    							<tt>String</tt> con el numero de solicitudes transmitidas.
		@param numeroSumatoriaReservasCubiertas    	<tt>String</tt> con el Monto total de reservas cubiertas.
		@param numeroSumatoriaReservasExpuestas    	<tt>String</tt> con el Monto total de reservas expuestas.
		@param nombreUsuario   								<tt>String</tt> con el nombre del usuario firmado en el sistema.
		
		@return <tt>String</tt> con el nombre del archivo generado.
		
		@modified_by jshernandez
		@modified_since F036 - 2013 -- GARANTIAS - Nueva metodolog�a calificaci�n; 19/11/2013 07:23:00 p.m.
	*/	
	public static String generaAcuseArchivoPDF(
			ResultadosGar 	rg, 
			String 			loginUsuario, 
			HashMap 			cabecera, 
			String 			strDirectorioTemp, 
			String 			strDirectorioPublicacion, 
			String 			totalRegistros, 
			String 			numeroSumatoriaReservasCubiertas, 
			String 			numeroSumatoriaReservasExpuestas,
			String 			nombreUsuario )
		throws AppException{
 
		log.info("generaAcuseArchivoPDF(E)");
		
		String 		nombreArchivo 	= null;
		CreaArchivo archivo 			= null;
		ComunesPDF 	pdfDoc 			= null;
		
		try {
			
			archivo 			= new CreaArchivo();
			nombreArchivo 	= archivo.nombreArchivo()+".pdf";
	 
			// pdfDoc 			= new ComunesPDF(2,strDirectorioTemp+loginUsuario+"."+nombreArchivo);
			pdfDoc 			= new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
			
			String meses[]	 		= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    	= fechaActual.substring(0,2);
			String mesActual   	= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   	= fechaActual.substring(6,10);
			String horaActual  	= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
			pdfDoc.encabezadoConImagenes(
						pdfDoc,
						(String)	cabecera.get("strPais"),
						(String) cabecera.get("iNoNafinElectronico"),
						(String)	cabecera.get("sesExterno"),
						(String) cabecera.get("strNombre"),
						(String) cabecera.get("strNombreUsuario"),
						(String)	cabecera.get("strLogo"),
						strDirectorioPublicacion);
	 
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
			pdfDoc.addText("Su solicitud fue recibida con el n�mero de folio: "+rg.getFolio(),"formas",ComunesPDF.CENTER);
			pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
			
			pdfDoc.setTable(2,70);
			
			pdfDoc.setCell("CIFRAS CONTROL",								"celda03",	ComunesPDF.CENTER,2,1,1);
			
			pdfDoc.setCell("Tipo de Operaci�n",							"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell("05 Calificacion de Cartera",				"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.setCell("No. total de registros transmitidos",					"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(totalRegistros,								"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.setCell("Monto total de reservas cubiertas",	"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(numeroSumatoriaReservasCubiertas,2),"formas",ComunesPDF.RIGHT);
		
			pdfDoc.setCell("Monto total de reservas expuestas",	"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(numeroSumatoriaReservasExpuestas,2),"formas",ComunesPDF.RIGHT);
			
			pdfDoc.setCell("Fecha de carga",								"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(rg.getFecha(),									"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.setCell("Hora de carga",								"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(rg.getHora(),									"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.setCell("Usuario",										"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(loginUsuario+" - "+nombreUsuario,	   "formas",	ComunesPDF.RIGHT);
			
			pdfDoc.addTable();
			pdfDoc.endDocument();
		
		}catch(Exception e){
			
			log.error("generaAcuseArchivoPDF(Exception)");
			e.printStackTrace();
			log.error("rg                               = <" + rg                                + ">");
			log.error("loginUsuario                     = <" + loginUsuario                      + ">");
			log.error("cabecera                         = <" + cabecera                          + ">");
			log.error("strDirectorioTemp                = <" + strDirectorioTemp                 + ">");
			log.error("strDirectorioPublicacion         = <" + strDirectorioPublicacion          + ">");
			log.error("totalRegistros                   = <" + totalRegistros                    + ">");
			log.error("numeroSumatoriaReservasCubiertas = <" + numeroSumatoriaReservasCubiertas  + ">");
			log.error("numeroSumatoriaReservasExpuestas = <" + numeroSumatoriaReservasExpuestas  + ">");
			log.error("nombreUsuario                    = <" + nombreUsuario                     + ">"); 
			throw new AppException("Ocurri� un error al generar el PDF con la informaci�n del acuse.");
			
		}
		log.info("generaAcuseArchivoPDF(S)");
		
		return nombreArchivo;	
	
	}
	
}