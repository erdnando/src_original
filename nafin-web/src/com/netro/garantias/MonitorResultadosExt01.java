package com.netro.garantias;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.math.BigDecimal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class MonitorResultadosExt01 implements IQueryGeneratorRegExtJS {

  private final static Log log = ServiceLocator.getInstance().getLog(MonitorResultadosExt01.class);
    
  private String claveIF;
	private String folio;
	private String opcion;
  private int tipoOper=0;
  private String fechaIni;
  private String fechaFin;
  private String iNoCliente="0";
  private String programa="";
  private String enviadoSiag="";
  private String strDirectorioTemp;
  private String origen;
  private String cvePerf;
  private List 	 conditions;
  StringBuffer qrySentencia = new StringBuffer("");
  
  public MonitorResultadosExt01()  {  }
  
  public String crearArchivo (){
  AccesoDB con = new AccesoDB();
  PreparedStatement	ps = null;
  ResultSet rs = null;
  String qrySentencia	= "";
  String tabla = "";
  int i = 0;
  
  CreaArchivo archivo = new CreaArchivo();
  StringBuffer contenidoArchivo = new StringBuffer("");
  String nombreArchivo = null;

  log.info("MonitorResultadosExt01::crearArchivo(E)");
	try {
    con.conexionDB();
    if(opcion.equals("origen")){
      tabla = "gti_contenido_arch";
    }else{
      tabla = "gti_arch_rechazo";
    }
    if(!"".equals(claveIF))
      iNoCliente = claveIF;
  
	qrySentencia =
		"select cg_contenido"+
		" from "+tabla+" A"+
		" ,comcat_if I"+
		" where A.ic_if_siag = I.ic_if_siag"+
		" and A.ic_folio = ?"+
		" and I.ic_if = ?"+
		" order by A.ic_linea";
	  
    ps = con.queryPrecompilado(qrySentencia);
    ps.setLong(1, Long.parseLong(folio));
    ps.setLong(2, Long.parseLong(iNoCliente));
    
    rs = ps.executeQuery();
    while(rs.next()){
      contenidoArchivo.append(rs.getString("cg_contenido")+"\n");
      i++;
    }
    ps.close();
    
    if (i>0) {
      if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".txt"))
          nombreArchivo = "0";
      else
          nombreArchivo = archivo.nombre;
    } else {
      nombreArchivo = "0";
    }
  
  } catch (Exception e){
    log.debug("MonitorResultadosExt01::crearArchivo: ERROR "+ e);
  }finally{
	if(con.hayConexionAbierta())
		con.cierraConexionDB();
  }
  
	log.info("MonitorResultadosExt01::crearArchivo (S) ");
  return nombreArchivo;
  }

/**
	 * Este m�todo debe regresar un query con el que se obtienen los 
	 * identificadores unicos de los registros a mostrar en la consulta
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public  String getDocumentQuery(){
  return "";
  }
  
	/**
	 * Este m�todo debe regresar un query con el que se obtienen los 
	 * datos a mostrar en la consulta basandose en los identificadores unicos 
	 * especificados en las lista de ids
	 * @param ids Lista de los identificadoes unicos. El tama�o de la lista 
	 * 	recibida ser� de acuerdo a la configuraci�n de registros x p�gina
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public  String getDocumentSummaryQueryForIds(List ids){
  return "";
  }
  
  
	/**
	 * Este m�todo debe regresar un query con el que se obtienen totales de la
	 * consulta.
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public  String getAggregateCalculationQuery(){
  return "";
  }
  
  
	/**
	 * Este m�todo debe regresar una Lista de parametros que ser�n usados
	 * como valor de las variables BIND de los queries generados.
	 * @return Lista con los valores a usar en las variables bind
	 */
	public  List getConditions(){
  
  return conditions;
  }
  
	/**
	 * Este m�todo debe regresar un query que obtendr� todos los registros
	 * resultantes de la b�squeda, con la finalidad de generar un archivo.
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public  String getDocumentQueryFile(){
  StringBuffer	sbQuery		= new StringBuffer();
		String 			sHint 		= " /*+index(es)*/ ";
		String 			sCondicion	= "";
		String 			sReferencia	= ",'MonitorResultadosExt01:getDocumentQueryFile("+claveIF+")' as PANTALLA ";
    conditions = new ArrayList();
		
		if(tipoOper!=0){
			sCondicion += " AND s.ic_tipo_operacion = ? ";
      String tP = String.valueOf(tipoOper);
      conditions.add(tP);
    }
		if(tipoOper==2){
			sCondicion += " AND s.ic_situacion != 7 ";
      String tP = String.valueOf(tipoOper);
      conditions.add(tP);
    }
/*FODEA 000 - 2009 Garantias*/
		if(!claveIF.equals("0")){
      sCondicion += " AND i.ic_if = ? ";
      conditions.add(claveIF);
    }
		if(!fechaIni.equals("") && !fechaFin.equals("")){
      sCondicion += " AND es.df_fecha_hora BETWEEN TO_DATE(?, 'DD/MM/YYYY') AND TO_DATE(?, 'DD/MM/YYYY') + 1 ";
      conditions.add(fechaIni);
      conditions.add(fechaFin);
    }
		if(!opcion.equals("")){
      sCondicion += " AND es.ic_situacion = ? ";
      conditions.add(opcion);
    }
	 if(!folio.equals("")){
		sCondicion+=" and es.ic_folio = ? ";
    conditions.add(folio);
	 }
	 if (!programa.equals("")){
		sCondicion+= " and es.cg_programa = ? ";
    conditions.add(programa);
	 }
	 if (!enviadoSiag.equals("")){
		sCondicion+= " and es.cg_pperd_disp_post = ?";
    conditions.add(enviadoSiag);
	 }
/*FODEA 000 - 2009 Garantias*/
		sbQuery.append(
			" SELECT   "+sHint+" "   +
      " i.ic_if AS clave_if, i.cg_razon_social AS nombre_if,"+  /*FODEA 000 - 2009 Garantias*/
      " es.ic_usuario_facultado AS tipo_envio,"+  /*FODEA 000 - 2009 Garantias*/
			" TO_CHAR (es.df_fecha_hora, 'dd/mm/yyyy hh24:mi') AS fecha,"   +
			" es.ic_folio, s.cg_descripcion AS situacion, es.in_registros_acep,"   +
			" es.in_registros_rech, es.in_registros, es.ic_situacion,"   +  /*FODEA 000 - 2009 Garantias*/
      " es.fn_impte_total as monto_enviado "+  /*FODEA 000 - 2009 Garantias*/
		", es.in_folio_reproceso AS reproceso "+//FODEA 012-2009
			sReferencia+
			" , es.cg_programa as programa	"+//Fodea 024-2012
			" FROM gti_estatus_solic es, gticat_situacion s, comcat_if i"   +
			" WHERE es.ic_situacion = s.ic_situacion"   +
			" AND es.ic_tipo_operacion = s.ic_tipo_operacion"   +
			" AND es.ic_if_siag = i.ic_if_siag  "+
			" AND s.ic_tipo_operacion = 2 "+
			" AND s.ic_situacion !=7 " +
			sCondicion+
			" ORDER BY es.df_fecha_hora desc");
    log.debug("qry: "+sbQuery.toString());
    log.debug("conditions : "+conditions);
		return sbQuery.toString();
  }
  
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public  String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
    log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		//int total = 0;
    
    try {
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".txt";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);
		
			contenidoArchivo = new StringBuffer();	
			contenidoArchivo.append(
      "Nombre IF,Fecha y Hora de Operaci�n,Folio de la Operaci�n,Situaci�n,Usuario,N�mero de operaciones aceptadas,	N�mero de operaciones con errores,Total de Operaciones\n");
       BigDecimal newc = new BigDecimal("0");
       BigDecimal suma = new BigDecimal("0");
       BigDecimal res  = new BigDecimal("0");
      while (rs.next()){
        String nomIF = (rs.getString("NOMBRE_IF") == null) ? "" : rs.getString("NOMBRE_IF");
        String fecha = (rs.getString("FECHA") == null) ? "" : rs.getString("FECHA");
        String folio = "";//(rs.getString("IC_FOLIO") == null) ? "" : rs.getString("IC_FOLIO");
        try {
             folio = (rs.getString("IC_FOLIO") == null) ? "" : rs.getString("IC_FOLIO");
            if (folio.equals("")){
              folio = "0";
              }
          } catch (Exception e){
            folio = "0";
          }
         newc= new BigDecimal(folio);
				 res=suma.add(newc);
         
        String situa = (rs.getString("SITUACION") == null) ? "" : rs.getString("SITUACION");
        String usuar = (rs.getString("TIPO_ENVIO") == null) ? "" : rs.getString("TIPO_ENVIO");
        String acept = (rs.getString("IN_REGISTROS_ACEP") == null) ? "" : rs.getString("IN_REGISTROS_ACEP");
        String error = (rs.getString("IN_REGISTROS_RECH") == null) ? "" : rs.getString("IN_REGISTROS_RECH");
        String total = (rs.getString("IN_REGISTROS") == null) ? "" : rs.getString("IN_REGISTROS");
        
        contenidoArchivo.append(
          nomIF.replace(',',' ')+","+fecha.replace(',',' ')+","+"\"\"\""+res.toPlainString()+"\"\"\""+","+
          situa.replace(',',' ')+",\'"+usuar.replace(',',' ')+","+acept.replace(',',' ')+","+
          error.replace(',',' ')+","+total.replace(',',' ')+","+"\n");	
      }

      creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
      nombreArchivo = creaArchivo.getNombre();
    } catch (Exception e ){
      log.debug("Error al generar el archivo: "+e);
    }
  log.debug("crearCustomFile (S)");  
  return nombreArchivo;
  }
  
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public  String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo){
  return "";
  }
  
  public String miPDF(HttpServletRequest request, String path, String tipo){
  AccesoDB con = new AccesoDB();
  PreparedStatement	ps	= null;
  ResultSet rs	= null;
  String qrySentencia	= "";
  String tabla = "";
  
  HttpSession session = request.getSession();
  CreaArchivo creaArchivo = new CreaArchivo();
  String nombreArchivo = "";  
  ComunesPDF pdfDoc = new ComunesPDF();	
	//StringBuffer contenidoArchivo = new StringBuffer();
  String icIfSiag = "";
  int i = 0;
   try {
    nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
		pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
		pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		
    try{    //QRY 1
      con.conexionDB();	
      qrySentencia =
		"select "+
		" ES.ic_folio"+
		" ,I.cg_razon_social as NOMBRE_IF"+
		" ,to_char(df_validacion_siag,'dd/mm/yyyy') as FECHA_PROCESO "+
		" ,to_char(df_autorizacion_siag,'dd/mm/yyyy') as FECHA_AUTORIZA "+
		" ,NVL(ES.in_registros_acep,0) as ACEP"+
		" ,NVL(ES.in_registros_rech,0) as RECH"+
		" ,NVL(ES.in_registros,0) as TOTAL"+
		" ,es.IC_IF_SIAG"+
		" ,es.ic_situacion"+
		" ,es.ig_anio_trim_calif"+
		" ,es.ig_trim_calif"+
		" ,nvl(es.CG_USUARIO_ACTUALIZA, ' ') as CG_USUARIO_ACTUALIZA"+
		" ,cs.cg_descripcion "+
		" from gti_estatus_solic ES"+
		" ,comcat_if I"+
		" ,gticat_situacion cs"+
		" where ES.ic_if_siag = I.ic_if_siag"+
		" and ES.IC_SITUACION = cs.IC_SITUACION"+
		" and ES.IC_TIPO_OPERACION = cs.IC_TIPO_OPERACION"+
		" and ES.ic_folio = ?"+
		" and I.ic_if = ?";
      ps = con.queryPrecompilado(qrySentencia);  
      ps.setString(1,folio);
      if("4".equals(cvePerf)){
        ps.setString(2,claveIF);
      } else {
        ps.setString(2,iNoCliente);
      }
      rs = ps.executeQuery();
      if(rs.next()){
      int situacion = rs.getInt("ic_situacion");
      icIfSiag = rs.getString("IC_IF_SIAG");
      pdfDoc.setTable(1,100);
      if("CONSALTA".equals(origen)) {
        pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
        pdfDoc.setCell("RESULTADOS DE SOLICITUD DE ALTA DE GARANTIAS","celda04",ComunesPDF.CENTER);
      }else if("CONSSALCOM".equals(origen)){
        pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
        pdfDoc.setCell("RESULTADOS DE SOLICITUD DE CARGA DE SALDOS Y PAGO DE COMISIONES","celda04",ComunesPDF.CENTER);		
      }else if("CONSCALIF".equals(origen)){
        pdfDoc.addText("\nA�o: "+rs.getString("IG_ANIO_TRIM_CALIF")+"   Trimestre: "+rs.getString("IG_TRIM_CALIF")+"\n","formas",ComunesPDF.CENTER);
        pdfDoc.setCell("RESULTADOS DE SOLICITUD DE CARGA DE CALIFICACION DE CARTERA","celda04",ComunesPDF.CENTER);		
      }else if("CONSREC".equals(origen)){
        pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
        pdfDoc.setCell("RESULTADOS DE SOLICITUD DE CARGA DE RECUPERACIONES","celda04",ComunesPDF.CENTER);		
      }else{
        pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
        pdfDoc.setCell("RESULTADOS","celda04",ComunesPDF.CENTER);
      }
      pdfDoc.addTable();
  
      if(!"CONSSALCOM".equals(origen)){
        if("CONSREC".equals(origen)){
          String aux = situacion +" " +rs.getString("cg_descripcion") + "\n Fecha de Operaci�n en Firme:" + ((rs.getString("FECHA_PROCESO")==null)?"":rs.getString("FECHA_PROCESO")) +"\n"; 
          pdfDoc.setTable(2);    
          pdfDoc.setCell("USUARIO: "+rs.getString("CG_USUARIO_ACTUALIZA"),"formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.    
          pdfDoc.setCell(aux,"formas",ComunesPDF.RIGHT,1,1,0);//El parametro 0, es para que la celda no tenga borde.    
          pdfDoc.addTable();
        } else {
          if(rs.getString("FECHA_PROCESO") == null ){
            pdfDoc.addText("Fecha de Operaci�n en Firme: \n","formas",ComunesPDF.RIGHT);
          }else{
            pdfDoc.addText("Fecha de Operaci�n en Firme: "+rs.getString("FECHA_PROCESO")+"\n","formas",ComunesPDF.RIGHT);
          }
        }
      }else{
          if(situacion ==5){
            if(rs.getString("FECHA_AUTORIZA") == null ){
              pdfDoc.addText("Fecha de Operaci�n en Firme: \n","formas",ComunesPDF.RIGHT);
            }else{
              pdfDoc.addText("Fecha de Operaci�n en Firme: "+rs.getString("FECHA_AUTORIZA")+"\n","formas",ComunesPDF.RIGHT);
            }
          }else{
            if (rs.getString("FECHA_PROCESO") == null ){
              pdfDoc.addText("Fecha de C�lculo de Comisiones: \n","formas",ComunesPDF.RIGHT);
            }else{
              pdfDoc.addText("Fecha de C�lculo de Comisiones: "+rs.getString("FECHA_PROCESO")+"\n","formas",ComunesPDF.RIGHT);
            }
          }
      }
      pdfDoc.setTable(5,95);
        pdfDoc.setCell("FOLIO DE LA OPERACION","celda01",ComunesPDF.CENTER);
        pdfDoc.setCell("INTERMEDIARIO FINANCIERO","celda01",ComunesPDF.CENTER);
        pdfDoc.setCell("REGISTROS SIN ERRORES","celda01",ComunesPDF.CENTER);
        pdfDoc.setCell("REGISTROS CON ERRORES","celda01",ComunesPDF.CENTER);
        pdfDoc.setCell("TOTAL DE REGISTROS PROCESADOS","celda01",ComunesPDF.CENTER);
  
        pdfDoc.setCell(rs.getString("IC_FOLIO"),"formas",ComunesPDF.CENTER);
        pdfDoc.setCell(rs.getString("NOMBRE_IF"),"formas",ComunesPDF.CENTER);
        pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("ACEP"),0),"formas",ComunesPDF.CENTER);
        pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("RECH"),0),"formas",ComunesPDF.CENTER);
        pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("TOTAL"),0),"formas",ComunesPDF.CENTER);			
  
        pdfDoc.addTable();
      }
      ps.close();
      
      pdfDoc.addText("\n\n\n","formas",ComunesPDF.LEFT);	
      if("CONSALTA".equals(origen)) {
        qrySentencia =
          " select ic_programa"   +
          "  ,fn_porc_comision"   +
          "  ,in_registros_acep"   +
          "  ,in_registros_rech"   +
          "  ,in_registros"   +
          " from gti_det_alta_gtia GD"   +
          " , comcat_if I"   +
          " where GD.ic_if_siag = I.ic_if_siag" +
          " and GD.ic_folio = ?"+
          " and I.ic_if = ?";
        System.err.println ("qrySentencia 2: "+qrySentencia);
        ps = con.queryPrecompilado(qrySentencia);
        ps.setString(1,folio);
        if("4".equals(cvePerf)){
        ps.setString(2,claveIF);
        } else {
        ps.setString(2,iNoCliente);
        }		
        rs = ps.executeQuery();
        while(rs.next()){
          if(i==0){
            pdfDoc.setTable(1,100);
            pdfDoc.setCell("DETALLE DE GARANTIAS POR PROGRAMA","celda04",ComunesPDF.CENTER);
            pdfDoc.addTable();
            pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
            pdfDoc.setTable(5,95);
            pdfDoc.setCell("CLAVE DEL PROGRAMA","celda01",ComunesPDF.CENTER);
            pdfDoc.setCell("PORCENTAJE DE COMISION","celda01",ComunesPDF.CENTER);
            pdfDoc.setCell("REGISTROS SIN ERRORES","celda01",ComunesPDF.CENTER);
            pdfDoc.setCell("REGISTROS CON ERRORES","celda01",ComunesPDF.CENTER);
            pdfDoc.setCell("TOTAL DE REGISTROS PROCESADOS","celda01",ComunesPDF.CENTER);
          }
      
          pdfDoc.setCell(rs.getString("IC_PROGRAMA")+" ","formas",ComunesPDF.CENTER);
          pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("FN_PORC_COMISION"),4)+" ","formas",ComunesPDF.CENTER);
          pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("IN_REGISTROS_ACEP"),0)+"","formas",ComunesPDF.CENTER);
          pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("IN_REGISTROS_RECH"),0)+"","formas",ComunesPDF.CENTER);
          pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("IN_REGISTROS"),0)+" ","formas",ComunesPDF.CENTER);
          i++;
        }	// while
        ps.close();
      }// ALTA
      else if("CONSSALCOM".equals(origen)) {
		
      // FECHA DE OPERACION EN FIRME ( COMISIONES POR MONEDA )
      qrySentencia =
        "  SELECT "   +
        " 		to_char(DF_FECHA_FIRME,'dd/mm/yyyy') AS FECHA_OPERACION "  +
        "  FROM GTI_DETALLE_SALDOS ds"   +
        "   ,comcat_moneda m"   +
        "   WHERE ds.ic_moneda = m.ic_moneda(+)"   +
        "   and ic_if_siag = ? "+
        " AND ic_folio = ? "  +
        " AND ds.cs_comision = 'S' "  +
        " order by decode(m.ic_moneda,1,1,54,2,3)";
      System.err.println ("qrySentencia 3: "+qrySentencia);
      ps = con.queryPrecompilado(qrySentencia);
      ps.setString(1,icIfSiag);
      ps.setString(2,folio);
      rs = ps.executeQuery();
      if(rs.next()){
        if(rs.getString("FECHA_OPERACION") == null ){
          pdfDoc.addText("Fecha de Operaci�n en Firme: ","formas",ComunesPDF.RIGHT);
        }else{
          pdfDoc.addText("Fecha de Operaci�n en Firme: "+rs.getString("FECHA_OPERACION")+"","formas",ComunesPDF.RIGHT);
        }
      }
      rs.close();ps.close();
      
      // DETALLE DE COMISIONES POR MONEDA
      qrySentencia =
        "  SELECT nvl(m.cd_nombre,'NA') as cd_nombre,"   +
        "  		in_aceptados,"   +
        "         in_rechazados,"   +
        " 		in_tot_registros, "   +
        " 		fn_impte_autoriza"   +
        "  FROM GTI_DETALLE_SALDOS ds"   +
        "   ,comcat_moneda m"   +
        "   WHERE ds.ic_moneda = m.ic_moneda(+)"   +
        "   and ic_if_siag = ? "+
        " AND ic_folio = ? "  +
        " AND ds.cs_comision = 'S' "  +
        " order by decode(m.ic_moneda,1,1,54,2,3)";
        System.err.println ("qrySentencia 4: "+qrySentencia);
      ps = con.queryPrecompilado(qrySentencia);
      ps.setString(1,icIfSiag);
      ps.setString(2,folio);
      rs = ps.executeQuery();
      while(rs.next()){
        if(i==0){
          pdfDoc.setTable(1,100);
          pdfDoc.setCell("DETALLE DE COMISIONES POR MONEDA","celda04",ComunesPDF.CENTER);
          pdfDoc.addTable();
          pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
          pdfDoc.setTable(5,95);
          pdfDoc.setCell("MONEDA","celda01",ComunesPDF.CENTER);
          pdfDoc.setCell("REGISTROS SIN ERRORES","celda01",ComunesPDF.CENTER);
          pdfDoc.setCell("REGISTROS CON ERRORES","celda01",ComunesPDF.CENTER);
          pdfDoc.setCell("TOTAL DE REGISTROS","celda01",ComunesPDF.CENTER);
          pdfDoc.setCell("IMPORTE DE COMISI�N E IVA DE REGISTROS ACEPTADOS","celda01",ComunesPDF.CENTER);
        }
    
        pdfDoc.setCell(rs.getString("cd_nombre")+"","formas",ComunesPDF.CENTER);
        pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_aceptados"),0)+"","formas",ComunesPDF.CENTER);
        pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_rechazados"),0)+"","formas",ComunesPDF.CENTER);
        pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_tot_registros"),0)+"","formas",ComunesPDF.CENTER);
        pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("fn_impte_autoriza"),2)+" ","formas",ComunesPDF.CENTER);
        i++;
      }	// while
      rs.close();ps.close();
      if(i>0){
        pdfDoc.addTable();
      }
      
      // FECHA DE OPERACION EN FIRME ( SIN COMISIONES POR MONEDA )
      qrySentencia =
        "  SELECT "   +
        " 		to_char(DF_FECHA_FIRME,'dd/mm/yyyy') AS FECHA_OPERACION "  +
        "  FROM GTI_DETALLE_SALDOS ds"   +
        "   ,comcat_moneda m"   +
        "   WHERE ds.ic_moneda = m.ic_moneda(+)"   +
        "   and ic_if_siag = ? "+
        " AND ic_folio = ? "  +
        " AND ds.cs_comision = 'N' "  +
        " order by decode(m.ic_moneda,1,1,54,2,3)";
        System.err.println ("qrySentencia 5: "+qrySentencia);
      ps = con.queryPrecompilado(qrySentencia);
      ps.setString(1,icIfSiag);
      ps.setString(2,folio);
      rs = ps.executeQuery();
      if(rs.next()){
        pdfDoc.addText("\n\n\n","formas",ComunesPDF.LEFT);	
        if(rs.getString("FECHA_OPERACION") == null ){
          pdfDoc.addText("Fecha de Operaci�n en Firme: ","formas",ComunesPDF.RIGHT);
        }else{
          pdfDoc.addText("Fecha de Operaci�n en Firme: "+rs.getString("FECHA_OPERACION")+"","formas",ComunesPDF.RIGHT);
        }
      }
      rs.close();ps.close();
      
      // DETALLE SIN COMISIONES POR MONEDA
      i = 0;
      qrySentencia =
        "  SELECT nvl(m.cd_nombre,'NA') as cd_nombre,"   +
        "  		in_aceptados,   "   +
        "         in_rechazados, "   +
        " 		in_tot_registros,  "   +
        " 		fn_impte_autoriza  "   +
        "  FROM GTI_DETALLE_SALDOS ds "   +
        "   ,comcat_moneda m "   +
        "   WHERE ds.ic_moneda = m.ic_moneda(+) "   +
        "   and ic_if_siag = ? "+
        " AND ic_folio = ? "  +
        " AND ds.cs_comision = 'N' "  +
        " order by decode(m.ic_moneda,1,1,54,2,3)";
        System.err.println ("qrySentencia 6: "+qrySentencia);
      ps = con.queryPrecompilado(qrySentencia);
      ps.setString(1,icIfSiag);
      ps.setString(2,folio);
      rs = ps.executeQuery();
      while(rs.next()){
        if(i==0){
          pdfDoc.setTable(1,100);
          pdfDoc.setCell("DETALLE SIN COMISIONES POR MONEDA","celda04",ComunesPDF.CENTER);
          pdfDoc.addTable();
          pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
          pdfDoc.setTable(5,95);
          pdfDoc.setCell("MONEDA","celda01",ComunesPDF.CENTER);
          pdfDoc.setCell("REGISTROS SIN ERRORES","celda01",ComunesPDF.CENTER);
          pdfDoc.setCell("REGISTROS CON ERRORES","celda01",ComunesPDF.CENTER);
          pdfDoc.setCell("TOTAL DE REGISTROS","celda01",ComunesPDF.CENTER);
          pdfDoc.setCell("IMPORTE DE COMISI�N E IVA DE REGISTROS ACEPTADOS","celda01",ComunesPDF.CENTER);
        }
    
        pdfDoc.setCell(rs.getString("cd_nombre")+"","formas",ComunesPDF.CENTER);
        pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_aceptados"),0)+"","formas",ComunesPDF.CENTER);
        pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_rechazados"),0)+"","formas",ComunesPDF.CENTER);
        pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_tot_registros"),0)+"","formas",ComunesPDF.CENTER);
        pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("fn_impte_autoriza"),2)+" ","formas",ComunesPDF.CENTER);
        i++;
      }	// while
      rs.close();ps.close();
      
    }//CONSSALCOM 
    else if("CONSREC".equals(origen)) {
		qrySentencia =
			"  SELECT nvl(m.cd_nombre,'NA') as cd_nombre,"   +
			"  		SUM(in_aceptados) AS in_aceptados,"   +
			"       SUM(in_rechazados) AS in_rechazados,"   +
			" 		SUM(in_tot_registros) AS in_tot_registros, "   +
			" 		SUM(fn_impte_deposito) AS fn_impte_deposito, "   +
			" 		SUM(fn_impte_calculado) AS fn_impte_calculado, "   +
			" 		SUM(fn_diferencia) AS fn_diferencia, "   +
			" 		SUM(nvl(fn_impte_complemento,0)) AS fn_impte_complemento, "   +
			" 		decode(m.ic_moneda,1,1,54,2,3) "   +
			"  FROM GTI_DETALLE_RECUPERACIONES d "   +
			"   ,comcat_moneda m "   +
			"   WHERE d.ic_moneda = m.ic_moneda(+) "   +
			"   and ic_if_siag = ? "+
			"   AND ic_folio = ?"  +
			"   GROUP BY nvl(m.cd_nombre,'NA'), decode(m.ic_moneda,1,1,54,2,3)"  +
			" order by decode(m.ic_moneda,1,1,54,2,3) ";
      System.err.println ("qrySentencia 7: "+qrySentencia);
		ps = con.queryPrecompilado(qrySentencia);
		ps.setString(1,icIfSiag);
		ps.setString(2,folio);
		rs = ps.executeQuery();
		while(rs.next()){
			if(i==0){
				pdfDoc.setTable(1,100);
				pdfDoc.setCell("DETALLE DE RECUPERACIONES POR MONEDA","celda04",ComunesPDF.CENTER);
				pdfDoc.addTable();
				pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
				pdfDoc.setTable(8,95);
				pdfDoc.setCell("MONEDA","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("REGISTROS SIN ERRORES","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("REGISTROS CON ERRORES","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("TOTAL DE REGISTROS","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("IMPORTE DEPOSITO BANCO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("IMPORTE CALCULADO\n(A-B+C+D)","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("DIFERENCIA","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("DEPOSITO DE LA DIFERENCIA","celda01",ComunesPDF.CENTER);
			}
	
			pdfDoc.setCell(rs.getString("cd_nombre")+"","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_aceptados"),0)+"","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_rechazados"),0)+"","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_tot_registros"),0)+"","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("fn_impte_deposito"),2)+" ","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("fn_impte_calculado"),2)+" ","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("fn_diferencia"),2)+" ","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("fn_impte_complemento"),2)+" ","formas",ComunesPDF.CENTER);
			i++;
		}	// while
		rs.close();ps.close();
    }//CONSREC  
    if(i>0)
		pdfDoc.addTable();
    pdfDoc.addText("\n\n\n","formas",ComunesPDF.LEFT);				
    if("CONSREC".equals(origen)) {	}
    
    qrySentencia =
      "select cg_contenido"+
      " from gti_arch_resultado A"+
      " ,comcat_if I"+
      " where A.ic_if_siag = I.ic_if_siag"+
      " and A.ic_folio = ?"+
      " and I.ic_if = ?"+
      " order by A.ic_linea,A.ic_num_error";
    System.err.println ("qrySentencia 8: "+qrySentencia);
    ps = con.queryPrecompilado(qrySentencia);
    ps.setString(1,folio);
    if("4".equals(cvePerf)){
      ps.setString(2,claveIF);
    } else {
      ps.setString(2,iNoCliente);
    }	
    rs = ps.executeQuery();
    i=0;
    while(rs.next()){
		if(i==0){
			pdfDoc.setTable(1,100);
			pdfDoc.setCell("ERRORES GENERADOS","celda04",ComunesPDF.CENTER);
			pdfDoc.addTable();		
			
			if("CONSREC".equals(origen)) {
				pdfDoc.addText("Donde:\n","formas",ComunesPDF.LEFT);
				pdfDoc.addText("A: (CAPITAL + INTERESES + MORATORIOS)\n","formas",ComunesPDF.LEFT);			
				pdfDoc.addText("B: (GASTOS DE JUICIO)\n","formas",ComunesPDF.LEFT);			
				pdfDoc.addText("C: (INTERES + PENALIZACION)\n","formas",ComunesPDF.LEFT);	
				pdfDoc.addText("D: (I.V.A.)\n\n","formas",ComunesPDF.LEFT);	
				//pdfDoc.addText("ERRORES GENERADOS POR EL PROCESO DE CARGA DE RECUPERACIONES AUTOMATICAS\n","formas",ComunesPDF.LEFT);
			}
		}
		pdfDoc.addText(rs.getString("cg_contenido")+"\n","formas",ComunesPDF.LEFT);
		i++;
    }
    ps.close();
    
    pdfDoc.endDocument();  
    } catch (Exception e){
      System.err.println("ERROR: "+e);
    } //FIN TRY QRY 
		finally{
      if(con.hayConexionAbierta())
      con.cierraConexionDB();
    }
			
    	
  } catch (Exception e){ ///TRY PRINCIPAL
    System.err.println("ERROR: "+e);
  }finally{
	if(con.hayConexionAbierta())
		con.cierraConexionDB();
  }
  return nombreArchivo;
  }


  public void setClaveIF(String claveIF)
  {
    this.claveIF = claveIF;
  }


  public String getClaveIF()
  {
    return claveIF;
  }


  public void setFolio(String folio)
  {
    this.folio = folio;
  }


  public String getFolio()
  {
    return folio;
  }


  public void setOpcion(String opcion)
  {
    this.opcion = opcion;
  }


  public String getOpcion()
  {
    return opcion;
  }


  public void setStrDirectorioTemp(String strDirectorioTemp)
  {
    this.strDirectorioTemp = strDirectorioTemp;
  }


  public String getStrDirectorioTemp()
  {
    return strDirectorioTemp;
  }


  public void setOrigen(String origen)
  {
    this.origen = origen;
  }


  public String getOrigen()
  {
    return origen;
  }


  public void setCvePerf(String cvePerf)
  {
    this.cvePerf = cvePerf;
  }


  public String getCvePerf()
  {
    return cvePerf;
  }


  public void setFechaIni(String fechaIni)
  {
    this.fechaIni = fechaIni;
  }


  public String getFechaIni()
  {
    return fechaIni;
  }


  public void setFechaFin(String fechaFin)
  {
    this.fechaFin = fechaFin;
  }


  public String getFechaFin()
  {
    return fechaFin;
  }


  
}