package com.netro.garantias;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.HashMap;

import netropology.utilerias.AppException;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**

	Esta clase se encarga de generar los archivos PDF de la carga
		ADMIN_IF_GARAN - PROGRAMA DE GARANT�A AUTOM�TICA - CARGA DE CALIFICACION DE CARTERA - CONSULTA DE CIERRE DE RECEPCI�N DE CALIFICACIONES
	
	@author Jesus Salim Hernandez David
	@since  Fodea 004 - 2012 -- Migracion IF
	@date	  04/06/2012 10:49:09 a.m.
	
 */
public class MonitoreoCierreRecepcion {
	
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(MonitoreoCierreRecepcion.class);

	/**
		Crea un archivo PDF con los Creditos con Incumplimiento de Calificacion
		@throws AppException 
		
		@param directorio <tt>String</tt> con la ruta fisica del directorio donde se crear� el Archivo PDF.
		@param cabecera   <tt>HashMap</tt> con la informacion de la cabcera del PDF.
		@param claveIF    <tt>String</tt> con el ID del Intermediario Financiero.
		@param garantias  Instancia del EJB de Garant�as.
		
		@return <tt>String</tt> con el nombre del archivo generado.
	*/	
	public static String generaArchivoPDFCreditosIncumplimientoCalificacion(
			String 		directorio, 
			HashMap 		cabecera, 
			String 		claveIF, 
			Garantias 	garantias
	) throws AppException {
		
		log.info("generaArchivoPDFCreditosIncumplimientoCalificacion(E)");
		
		String 		nombreArchivo 	= null;
		CreaArchivo archivo 			= null;
		ComunesPDF 	documentoPdf 	= null;
		
		try {
			archivo 			= new CreaArchivo();
			nombreArchivo 	= archivo.nombreArchivo()+".pdf";
			documentoPdf 	= new ComunesPDF(2,directorio+nombreArchivo);

			// Encabezado
			documentoPdf.encabezadoConImagenes(	 	documentoPdf,
													 			"",
																"",
																"",
																"",
																"",
																(String) cabecera.get("LOGO"),
																(String) cabecera.get("DIRECTORIO_PUBLICACION")
														 );
			
			// Localidad, fecha y hora
			String meses[] 		= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaAct 		= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    	= fechaAct.substring(0,1).equals("0")?fechaAct.substring(1,2):fechaAct.substring(0,2);
			String mesActual    	= meses[Integer.parseInt(fechaAct.substring(3,5))-1];
			String anioActual   	= fechaAct.substring(6,10);
			String horaActual  	= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			documentoPdf.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+"                               "+horaActual+"             ","formas",ComunesPDF.RIGHT);
			documentoPdf.addText(" ","formas",ComunesPDF.RIGHT);
			documentoPdf.addText(" ","formas",ComunesPDF.RIGHT);
			documentoPdf.addText(" ","formas",ComunesPDF.RIGHT);
			documentoPdf.addText(" ","formas",ComunesPDF.RIGHT);
			documentoPdf.addText(" ","formas",ComunesPDF.RIGHT);
			
			HashMap registro=garantias.getDescripcionIntermediario(claveIF);

			if(registro != null){
			
				documentoPdf.setTable(7,100,new float[]{14.28f,14.28f,14.28f,14.28f,14.28f,14.28f,14.28f});
			
				// DATOS GENERALES
				// Agregar cabecera de la tabla
				documentoPdf.setCell("Intermediario",											"celda01rep",ComunesPDF.LEFT,7,1,1);
				documentoPdf.setCell("    " + (String)registro.get("INTERMEDIARIO"),	"formasrep", ComunesPDF.LEFT,7,1,1);
			

				// GARANT�AS CON INCLUMPIMIENTO EN CALIFICACI�N SUJETA A REGULARIZACI�N
				ArrayList registrosConIncumplimientoEnCalificacion = garantias.getGarantiasConIncumplimientoEnCalificacionSujetaARegularizacion(claveIF);
				
				if(registrosConIncumplimientoEnCalificacion.size() > 0){
					documentoPdf.setCell("Garant�as con Inclumpimiento en Calificaci�n Sujeta a Regularizaci�n\n ", "celda01rep",ComunesPDF.LEFT,7,1,1);
					documentoPdf.setCell("",																							  "formasrep",ComunesPDF.RIGHT,7,1,1);
			
					// Agregar cabecera de la tabla
					documentoPdf.setCell("No. Garant�a",								"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("A�o/Trimestre\nPrimer Incum.",			"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Fecha Primer\nIncum. Reg.",				"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Fecha Ult.\nIncum. Reg.",					"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Periodo\nConsecutivo\nIncum.",			"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Fecha\nSolicitud Baja\nPortafolio",	"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Sit.\nIncumplimiento",						"celda01rep",ComunesPDF.CENTER,1,1,1);
			
					// Llenar tabla
					for(int indice=0;indice<registrosConIncumplimientoEnCalificacion.size();indice++){
						HashMap reg = (HashMap) registrosConIncumplimientoEnCalificacion.get(indice);
						
						documentoPdf.setCell((String)reg.get("NUM_GARANTIA"),							"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("ANIO_TRIMESTRE_PRMR_INC"),			"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("FECHA_PRIMER_INCUMPLIMIENTO"),		"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("FECHA_ULTIMO_INCUMPLIMIENTO"),		"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("PERIODO_CONSEC_INCUMPLIMIENTO"),	"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("FECHA_SOLICITUD_BAJA"),				"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("SITUACION_INCUMPLIMIENTO"),			"formasrep",ComunesPDF.CENTER,1,1,1);
					}
				}else{
					documentoPdf.setCell("Garant�as con Inclumpimiento en Calificaci�n Sujeta a Regularizaci�n\n ", "celda01rep",ComunesPDF.LEFT,7,1,1);
					documentoPdf.setCell("",																							  "formasrep",ComunesPDF.RIGHT,7,1,1);
			
					// Agregar cabecera de la tabla
					documentoPdf.setCell("No. Garant�a",								"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("A�o/Trimestre\nPrimer Incum.",			"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Fecha Primer\nIncum. Reg.",				"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Fecha Ult.\nIncum. Reg.",					"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Periodo\nConsecutivo\nIncum.",			"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Fecha\nSolicitud Baja\nPortafolio",	"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Sit.\nIncumplimiento",						"celda01rep",ComunesPDF.CENTER,1,1,1);
			
					documentoPdf.setCell("",												"formasrep",ComunesPDF.RIGHT,7,1,1);
				}
			
				// GARANT�AS CON SITUACI�N DE BAJA DEL PORTAFOLIO GARANTIZADO
				ArrayList registrosConSituacionDeBajaDelPortafolio = garantias.getGarantiasConSituacionDeBajaDelPortafolioGarantizado(claveIF);
				
				if(registrosConSituacionDeBajaDelPortafolio.size() > 0){
					documentoPdf.setCell("Garant�as con Situaci�n de Baja del Portafolio Garantizado\n ", 	"celda01rep",ComunesPDF.LEFT,7,1,1);
					documentoPdf.setCell("",																					"formasrep",ComunesPDF.RIGHT,7,1,1);
			
					// Agregar cabecera de la tabla
					documentoPdf.setCell("No. Garant�a",								"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("A�o/Trimestre\nPrimer Incum.",			"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Fecha Primer\nIncum. Reg.",				"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Fecha Ult.\nIncum. Reg.",					"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Periodo\nConsecutivo\nIncum.",			"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Fecha\nSolicitud Baja\nPortafolio",	"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Sit.\nIncumplimiento",						"celda01rep",ComunesPDF.CENTER,1,1,1);
			
					for(int indice=0;indice<registrosConSituacionDeBajaDelPortafolio.size();indice++){
						HashMap reg = (HashMap) registrosConSituacionDeBajaDelPortafolio.get(indice);
						
						documentoPdf.setCell((String)reg.get("NUM_GARANTIA"),							"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("ANIO_TRIMESTRE_PRMR_INC"),			"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("FECHA_PRIMER_INCUMPLIMIENTO"),		"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("FECHA_ULTIMO_INCUMPLIMIENTO"),		"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("PERIODO_CONSEC_INCUMPLIMIENTO"),	"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("FECHA_SOLICITUD_BAJA"),				"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("SITUACION_INCUMPLIMIENTO"),			"formasrep",ComunesPDF.CENTER,1,1,1);
					}
				}else{
					documentoPdf.setCell("Garant�as con Situaci�n de Baja del Portafolio Garantizado\n ", 	"celda01rep",ComunesPDF.LEFT,7,1,1);
					documentoPdf.setCell("",																					"formasrep",ComunesPDF.RIGHT,7,1,1);
			
					// Agregar cabecera de la tabla
					documentoPdf.setCell("No. Garant�a",								"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("A�o/Trimestre\nPrimer Incum.",			"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Fecha Primer\nIncum. Reg.",				"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Fecha Ult.\nIncum. Reg.",					"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Periodo\nConsecutivo\nIncum.",			"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Fecha\nSolicitud Baja\nPortafolio",	"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Sit.\nIncumplimiento",						"celda01rep",ComunesPDF.CENTER,1,1,1);
					
					documentoPdf.setCell("",																					"formasrep",ComunesPDF.RIGHT,7,1,1);
			
				}
				
				documentoPdf.addTable();
							
			} else {
				documentoPdf.setTable(1,100,new float[]{100f});
				documentoPdf.setCell("\n    No se encontr� ning�n registro.\n ","celda01rep",ComunesPDF.LEFT,1,1,1);
				documentoPdf.addTable();				
			}
			
			// Terminar documento
			documentoPdf.endDocument();
			
		}catch(Exception e){
			log.error("generaArchivoPDFCreditosIncumplimientoCalificacion(Exception)");
			e.printStackTrace();
			log.error("directorio = <" + directorio + ">"); 
			log.error("cabecera   = <" + cabecera   + ">");
			log.error("claveIF    = <" + claveIF    + ">"); 
			log.error("garantias  = <" + garantias  + ">");
			throw new AppException("Ocurri� un error al generar un archivo pdf con los incumplimientos de la calificacion.");
		} finally {
			log.info("generaArchivoPDFCreditosIncumplimientoCalificacion(S)");
		}
		
		return nombreArchivo;	
		
	}
	
	/**
		Crea un archivo PDF con los Creditos con Incumplimiento de Calificacion
		@throws AppException 
		
		@param directorio <tt>String</tt> con la ruta fisica del directorio donde se crear� el Archivo PDF.
		@param cabecera   <tt>HashMap</tt> con la informacion de la cabcera del PDF.
		@param anio       <tt>String</tt> con el n&uacute;mero del a&ntilde;o.
		@param trimestre  <tt>String</tt> con el n&uacute;mero del trimestre.
		@param claveIF    <tt>String</tt> con el ID del Intermediario Financiero.
		@param fechaCorte <tt>String</tt> con la fecha de corte en formato <tt>dd/mm/aaaa</tt>. 
		@param garantias  Instancia del EJB de Garant�as.
		
		@return <tt>String</tt> con el nombre del archivo generado.
	*/	
	public static String generaArchivoPDFResultadosRecepcionCalificaciones(
			String    directorio, 
			HashMap   cabecera, 
			String    anio, 
			String    trimestre, 
			String    claveIF, 
			String    fechaCorte, 
			Garantias garantias
	) throws AppException {
		
		log.info("generaArchivoPDFResultadosRecepcionCalificaciones(E)");
		
		String 		nombreArchivo 	= null;
		CreaArchivo archivo 			= null;
		ComunesPDF 	documentoPdf 	= null;
		
		try {
			archivo 			= new CreaArchivo();
			nombreArchivo 	= archivo.nombreArchivo()+".pdf";
			documentoPdf 	= new ComunesPDF(2,directorio+nombreArchivo);

			// Encabezado
			documentoPdf.encabezadoConImagenes(	 	documentoPdf,
													 			"",
																"",
																"",
																"",
																"",
																(String) cabecera.get("LOGO"),
																(String) cabecera.get("DIRECTORIO_PUBLICACION")
														 );
			
			// Localidad, fecha y hora
			String meses[] 		= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaAct 		= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    	= fechaAct.substring(0,1).equals("0")?fechaAct.substring(1,2):fechaAct.substring(0,2);
			String mesActual    	= meses[Integer.parseInt(fechaAct.substring(3,5))-1];
			String anioActual   	= fechaAct.substring(6,10);
			String horaActual  	= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			documentoPdf.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+"                               "+horaActual+"             ","formas",ComunesPDF.RIGHT);
			documentoPdf.addText(" ","formas",ComunesPDF.RIGHT);
			documentoPdf.addText(" ","formas",ComunesPDF.RIGHT);
			documentoPdf.addText(" ","formas",ComunesPDF.RIGHT);
			documentoPdf.addText(" ","formas",ComunesPDF.RIGHT);
			documentoPdf.addText(" ","formas",ComunesPDF.RIGHT);
			
			
			HashMap registro=garantias.getDatosCierreRecepcionCalificacionesPorRegistro(anio,trimestre,claveIF,fechaCorte);

			if(registro != null){
			
				documentoPdf.setTable(10,100,new float[]{10f,10f,10f,10f,10f,10f,10f,10f,10f,10f});
			
				// DATOS GENERALES
				// Agregar cabecera de la tabla
				documentoPdf.setCell("Intermediario",						"celda01rep",ComunesPDF.CENTER,4,1,1);
				documentoPdf.setCell("A�o Trimestre",						"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setCell("Trimestre",							"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setCell("Total Garant�as",					"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setCell("No. Garant�as\nRecibidas",		"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setCell("No. Garant�as No\nRecibidas",	"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setCell("Situaci�n\nRecepci�n",				"celda01rep",ComunesPDF.CENTER,1,1,1);
			
				// Llenar tabla
				documentoPdf.setCell((String)registro.get("INTERMEDIARIO"),					 "formasrep",ComunesPDF.CENTER,4,1,1);
				documentoPdf.setCell((String)registro.get("A�O"),								 "formasrep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setCell((String)registro.get("TRIMESTRE"),						 "formasrep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setCell((String)registro.get("TOTAL_GARANTIAS"),				 "formasrep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setCell((String)registro.get("NUM_GARANTIAS_RECIBIDAS"),	 "formasrep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setCell((String)registro.get("NUM_GARANTIAS_NO_RECIBIDAS"), "formasrep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setCell((String)registro.get("SITUACION_RECEPCION"),			 "formasrep",ComunesPDF.CENTER,1,1,1);

				// CALIFICACIONES RECIBIDAS
				ArrayList registrosCalificacionesRecibidas = garantias.getCalificacionesRecibidas(anio,trimestre,claveIF,fechaCorte);
				
				if(registrosCalificacionesRecibidas.size() > 0){
					
					documentoPdf.setCell("Calificaciones Recibidas",	"celda01rep",ComunesPDF.LEFT,10,1,1);
					documentoPdf.setCell("",									"formasrep",ComunesPDF.RIGHT,10,1,1);
			
					// Agregar cabecera de la tabla
					documentoPdf.setCell("No. Cred",							"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("RFC",								"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Calif.\nDeudor",					"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Calif.\nCubierta",				"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Calif.\nExpuesta",				"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Porc. Calif.\nCubierta",		"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Porc. Calif.\nExpuesta",		"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Imp.\nReserva\nCubierta",		"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Imp.\nReserva\nExpuesta",		"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Sit.\nRecepci�n",				"celda01rep",ComunesPDF.CENTER,1,1,1);

					// Llenar tabla
					for(int indice=0;indice<registrosCalificacionesRecibidas.size();indice++){
						HashMap reg = (HashMap) registrosCalificacionesRecibidas.get(indice);
						
						documentoPdf.setCell((String)reg.get("NUMERO_CREDITO"),					"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("RFC"),									"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("CALIFICACION_DEUDOR"),			"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("CALIFICACION_CUBIERTA"),			"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("CALIFICACION_EXPUESTA"),			"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("PORC_CALIFICACION_CUBIERTA"),	"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("PORC_CALIFICACION_EXPUESTA"),	"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("IMPORTE_RESERVA_CUBIERTA"),		"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("IMPORTE_RESERVA_EXPUESTA"),		"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("SITUACION_RECEPCION"),			"formasrep",ComunesPDF.CENTER,1,1,1);
						
					}
			   }else{
					documentoPdf.setCell("Calificaciones Recibidas",	"celda01rep",ComunesPDF.LEFT,10,1,1);
					documentoPdf.setCell("",									"formasrep",ComunesPDF.RIGHT,10,1,1);
			
					// Agregar cabecera de la tabla
					documentoPdf.setCell("No. Cred",							"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("RFC",								"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Calif.\nDeudor",					"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Calif.\nCubierta",				"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Calif.\nExpuesta",				"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Porc. Calif.\nCubierta",		"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Porc. Calif.\nExpuesta",		"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Imp.\nReserva\nCubierta",		"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Imp.\nReserva\nExpuesta",		"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Sit.\nRecepci�n",				"celda01rep",ComunesPDF.CENTER,1,1,1);
					
					documentoPdf.setCell("","formasrep",ComunesPDF.LEFT,10,1,1);

				}
				
				// CALIFICACIONES NO RECIBIDAS
				ArrayList registrosCalificacionesNoRecibidas = garantias.getCalificacionesNoRecibidas(anio,trimestre,claveIF,fechaCorte);
				
				if(registrosCalificacionesNoRecibidas.size() > 0){
					
					documentoPdf.setCell("Calificaciones No Recibidas",	"celda01rep",ComunesPDF.LEFT,10,1,1);
					documentoPdf.setCell("",										"formasrep",ComunesPDF.RIGHT,10,1,1);
			
					// Agregar cabecera de la tabla
					documentoPdf.setCell("No. Cred",							"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("RFC",								"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Calif.\nDeudor",					"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Calif.\nCubierta",				"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Calif.\nExpuesta",				"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Porc. Calif.\nCubierta",		"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Porc. Calif.\nExpuesta",		"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Imp.\nReserva\nCubierta",		"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Imp.\nReserva\nExpuesta",		"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Sit.\nRecepci�n",				"celda01rep",ComunesPDF.CENTER,1,1,1);

					for(int indice=0;indice<registrosCalificacionesNoRecibidas.size();indice++){
						HashMap reg = (HashMap) registrosCalificacionesNoRecibidas.get(indice);
						
						documentoPdf.setCell((String)reg.get("NUMERO_CREDITO"),					"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("RFC"),									"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("CALIFICACION_DEUDOR"),			"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("CALIFICACION_CUBIERTA"),			"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("CALIFICACION_EXPUESTA"),			"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("PORC_CALIFICACION_CUBIERTA"),	"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("PORC_CALIFICACION_EXPUESTA"),	"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("IMPORTE_RESERVA_CUBIERTA"),		"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("IMPORTE_RESERVA_EXPUESTA"),		"formasrep",ComunesPDF.CENTER,1,1,1);
						documentoPdf.setCell((String)reg.get("SITUACION_RECEPCION"),			"formasrep",ComunesPDF.CENTER,1,1,1);
					}
					
				}else{
										
					documentoPdf.setCell("Calificaciones No Recibidas",	"celda01rep",ComunesPDF.LEFT,10,1,1);
					documentoPdf.setCell("",										"formasrep",ComunesPDF.RIGHT,10,1,1);
			
					// Agregar cabecera de la tabla
					documentoPdf.setCell("No. Cred",							"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("RFC",								"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Calif.\nDeudor",					"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Calif.\nCubierta",				"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Calif.\nExpuesta",				"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Porc. Calif.\nCubierta",		"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Porc. Calif.\nExpuesta",		"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Imp.\nReserva\nCubierta",		"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Imp.\nReserva\nExpuesta",		"celda01rep",ComunesPDF.CENTER,1,1,1);
					documentoPdf.setCell("Sit.\nRecepci�n",				"celda01rep",ComunesPDF.CENTER,1,1,1);
					
					documentoPdf.setCell("","formasrep",ComunesPDF.LEFT,10,1,1);
					
				}
				
				documentoPdf.addTable();
					
				
			} else {
				documentoPdf.setTable(1,100,new float[]{100f});
				documentoPdf.setCell("\n    No se encontr� ning�n registro.\n ","celda01rep",ComunesPDF.LEFT,1,1,1);
				documentoPdf.addTable();				
			}
			
			// Terminar documento
			documentoPdf.endDocument();
				
			
		}catch(Exception e){
			
			log.error("generaArchivoPDFResultadosRecepcionCalificaciones(Exception)");
			e.printStackTrace();
			log.error("directorio  = <" + directorio + ">"); 
			log.error("cabecera    = <" + cabecera   + ">");
			log.error("anio        = <" + anio       + ">");
			log.error("trimestre   = <" + trimestre  + ">"); 
			log.error("claveIF     = <" + claveIF    + ">");
			log.error("fechaCorte  = <" + fechaCorte + ">"); 
			log.error("garantias   = <" + garantias  + ">");
			throw new AppException("Ocurri� un error al generar un archivo pdf con los resultados de la recepci�n de la calificaci�n.");
			
		} finally {
			log.info("generaArchivoPDFResultadosRecepcionCalificaciones(S)");
		}
		
		return nombreArchivo;	
		
	}
	
}