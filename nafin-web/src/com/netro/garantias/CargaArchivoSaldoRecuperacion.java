package com.netro.garantias;

import com.netro.model.catalogos.CatalogoSimple;
import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.sf.json.JSONArray;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Fecha;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
			
	Esta clase se encarga de generar archivos referentes a la carga del Portafolio IF  de Conciliaci�n
	Autom�tica para el tipo de conciliaci�n: 2 (Saldo Pendiente Por Recuperar)
	
	Este m&eacute;todo se ocupa en las siguientes pantallas:
	   PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - SALDO PENDIENTE POR RECUPERAR (SPPR) - CARGA DE ARCHIVO
	
	@author jshernandez
	@since F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 31/10/2013 04:54:45 p.m.
	
 */
public class CargaArchivoSaldoRecuperacion {
	
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(CargaArchivoSaldoRecuperacion.class);

	/**
		Crea un archivo PDF con la informaci�n del acuse de la carga
		@throws AppException 
		
		@param rg 												<tt>ResultadosGar</tt> con el resultado de la transmision de solicitudes.
		@param strDirectorioTemp   						<tt>String</tt> con la ruta del directorio temporal donde se creara el archivo PDF.
		@param cabecera       								<tt>HashMap</tt> con la informacion de la cabcera del PDF.
		@param strDirectorioPublicacion 					<tt>String</tt> con la ruta WEB del directorio de publicacion.
		@param totalRegistros							   <tt>String</tt> con el numero total de registros a transmitidos.
		@param montoTotal									   <tt>String</tt> con el monto total de la recuperaci�n de importes.
		@param loginUsuario  								<tt>String</tt> con el login del usuario firmado en el sistema.
		@param nombreUsuario   								<tt>String</tt> con el nombre del usuario firmado en el sistema.
 
		@return <tt>String</tt> con el nombre del archivo generado.
	*/	
	public static String generaAcuseArchivoPDF(
			ResultadosGar 	rg,
			String 			strDirectorioTemp,
			HashMap			cabecera,
			String 			strDirectorioPublicacion,
			String			totalRegistros,
			String 			montoTotal,
			String 			loginUsuario,
			String 			nombreUsuario
	) throws AppException {
 
		log.info("generaAcuseArchivoPDF(E)");
		
		String 		nombreArchivo 	= null;
		ComunesPDF 	pdfDoc 			= null;
		
		try {
			
			nombreArchivo 				= Comunes.cadenaAleatoria(16) +".pdf";
	 
			// Obtener Descripci�n del Tipo de Operaci�n
			String 				tipoOperacion 	= "";
			// Preparar consulta
			CatalogoSimple 	cat 				= new CatalogoSimple();
			cat.setTabla("gticat_tipo_conciliacion");
			cat.setCampoClave("ic_tipo_conciliacion");
			cat.setCampoDescripcion("cg_descripcion");
			List claves = new ArrayList();
			claves.add("2");	// 2 = SALDO PENDIENTE POR RECUPERAR (SPPR) // #TAG
			cat.setValoresCondicionIn(claves);
			// Consultar descripcion del Tipo de Operacion 2.
			List listaResultados = cat.getListaElementos();
			if (listaResultados!=null && listaResultados.size()>0) {
				tipoOperacion = Comunes.rellenaCeros(((ElementoCatalogo)listaResultados.get(0)).getClave(),2) + " " + ((ElementoCatalogo)listaResultados.get(0)).getDescripcion();
			}
		
			pdfDoc 						= new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
			
			String 	meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String 	fechaActual  	= Fecha.getFechaActual();
			String 	diaActual   	= fechaActual.substring(0,2);
			String 	mesActual    	= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String 	anioActual   	= fechaActual.substring(6,10);
			String 	horaActual  	= Fecha.getHoraActual("HH24:MI:SS");
 
			pdfDoc.encabezadoConImagenes(
						pdfDoc,
						(String) cabecera.get("strPais"),
						(String) cabecera.get("iNoNafinElectronico"),
						(String) cabecera.get("sesExterno"),
						(String) cabecera.get("strNombre"),
						(String) cabecera.get("strNombreUsuario"),
						(String) cabecera.get("strLogo"),
						strDirectorioPublicacion
					);
 
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
			pdfDoc.addText("Su solicitud fue recibida con el n�mero de folio: "+rg.getFolio(),"formas",ComunesPDF.CENTER);
			pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
			
			pdfDoc.setTable(2,70);
			
			pdfDoc.setCell("CIFRAS CONTROL",										"celda03",	ComunesPDF.CENTER,2,1,1);
			
			pdfDoc.setCell("Tipo de Operaci�n",									"celda02",	ComunesPDF.CENTER);// Tipo de Operaci�n
			pdfDoc.setCell(tipoOperacion,											"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.setCell("No. total de registros",							"celda02",	ComunesPDF.CENTER);// No. total de registros
			pdfDoc.setCell(Comunes.formatoDecimal(totalRegistros,0),		"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.setCell("Saldo Pendiente Por Recuperar",					"celda02",	ComunesPDF.CENTER);// #TAG
			pdfDoc.setCell(Comunes.formatoDecimal(montoTotal,2),			"formas",	ComunesPDF.RIGHT);
 
			pdfDoc.setCell("Fecha de carga",										"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(rg.getFecha(),											"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.setCell("Hora de carga",										"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(rg.getHora(),											"formas",	ComunesPDF.RIGHT);
				
			pdfDoc.setCell("Usuario",												"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(loginUsuario+" - "+nombreUsuario,				"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.addTable();
			pdfDoc.endDocument();
		
		} catch(Exception e) {
			
			log.error("generaAcuseArchivoPDF(Exception)");
			log.error("generaAcuseArchivoPDF.rg                       = <" + rg                       + ">");
			log.error("generaAcuseArchivoPDF.strDirectorioTemp        = <" + strDirectorioTemp        + ">");
			log.error("generaAcuseArchivoPDF.cabecera                 = <" + cabecera                 + ">");
			log.error("generaAcuseArchivoPDF.strDirectorioPublicacion = <" + strDirectorioPublicacion + ">");
			log.error("generaAcuseArchivoPDF.totalRegistros           = <" + totalRegistros           + ">");
			log.error("generaAcuseArchivoPDF.montoTotal               = <" + montoTotal               + ">");
			log.error("generaAcuseArchivoPDF.loginUsuario             = <" + loginUsuario             + ">");
			log.error("generaAcuseArchivoPDF.nombreUsuario            = <" + nombreUsuario            + ">");
			e.printStackTrace();
			
			throw new AppException("Ocurri� un error al generar el PDF con la informaci�n del acuse.");
			
		} finally {
			
			log.info("generaAcuseArchivoPDF(S)");
			
		}
		
		return nombreArchivo;	
	
	}
	
	/**
		Crea un archivo CSV con el detalle de los errores presentados en la validaci�n.
		@throws AppException 
 
		@param detalleErroresVsCifrasControl         <tt>JSON</tt> array con el detalle de los Errores vs Cifras de Control.
		@param detalleRegistrosConErrores			   <tt>JSON</tt> array con el detalle de los Registros con Errores.
		@param strDirectorioTemp   						<tt>String</tt> con la ruta del directorio temporal donde se creara el archivo CSV.
		@param loginUsuario  								<tt>String</tt> con el login del usuario firmado en el sistema.
 
		@return <tt>String</tt> con el nombre del archivo generado.
	*/	
	public static String generaArchivoCSVErrores(
			String detalleErroresVsCifrasControl,
			String detalleRegistrosConErrores,
			String strDirectorioTemp,
			String loginUsuario
	) throws AppException{
	
		log.info("generaArchivoCSVErrores(E)");
		
		FileOutputStream		out 				= null;
		BufferedWriter 		csv 				= null;
		String 					nombreArchivo 	= null;
		
		try {

			nombreArchivo	= "CargaMasiva-ERRORES-" + Comunes.cadenaAleatoria(16) + ".csv";
			
			out 				= new FileOutputStream(strDirectorioTemp+nombreArchivo);
			csv 				= new BufferedWriter(new OutputStreamWriter(out, "Cp1252"));
			
			csv.write("Errores vs. Cifras de Control\r\n");
			csv.write("\r\n");
			JSONArray  erroresVsCifrasControl = JSONArray.fromObject(detalleErroresVsCifrasControl);
			for(int i=0,registrosGuardados=0;i<erroresVsCifrasControl.size();i++,registrosGuardados++){
				
				String linea =  erroresVsCifrasControl.getString(i);
 
				// Agregar l�nea con el detalle de los errores.
				csv.write("\""); 
				for(int j=0;j<linea.length();j++){
					char c = linea.charAt(j);
					// Escapar comillas dobles
					if( c == '"' ){
						csv.write('"');
					}
					// Enviar caracter al archivo
					csv.write(c);
				}
				csv.write("\"");
				// Agregar nueva l�nea 
				csv.write("\r\n");
				
				// Realizar flush si es conveniente
				if( ( registrosGuardados % 256 ) == 0){
					registrosGuardados = 0;
					csv.flush();
				}
 
			}
			csv.flush();
			
			csv.write("\r\n");
			
			csv.write("Registros con Errores\r\n");
			csv.write("\r\n");
			JSONArray  registrosConErrores 	 = JSONArray.fromObject(detalleRegistrosConErrores);
			for(int i=0,registrosGuardados=0;i<registrosConErrores.size();i++,registrosGuardados++){
				
				String linea =  registrosConErrores.getString(i);
 
				// Agregar l�nea con el detalle de los errores.
				csv.write("\""); 
				for(int j=0;j<linea.length();j++){
					char c = linea.charAt(j);
					// Escapar comillas dobles
					if( c == '"' ){
						csv.write('"');
					}
					// Enviar caracter al archivo
					csv.write(c);
				}
				csv.write("\"");
				// Agregar nueva l�nea 
				csv.write("\r\n");
				
				// Realizar flush si es conveniente
				if( ( registrosGuardados % 256 ) == 0){
					registrosGuardados = 0;
					csv.flush();
				}
 
			}
			csv.flush();
			
		} catch(Exception e) {
			
			log.error("generaArchivoCSVErrores(Exception)");
			log.error("generaArchivoCSVErrores.detalleErroresVsCifrasControl = <" + detalleErroresVsCifrasControl + ">");
			log.error("generaArchivoCSVErrores.detalleRegistrosConErrores    = <" + detalleRegistrosConErrores    + ">");
			log.error("generaArchivoCSVErrores.strDirectorioTemp             = <" + strDirectorioTemp             + ">");
			log.error("generaArchivoCSVErrores.loginUsuario                  = <" + loginUsuario                  + ">");
			e.printStackTrace();
			
			throw new AppException("Ocurri� un error al generar el CSV con el detalle de los errores.");
			
		} finally {
			
			// Cerrar archivo
			if(csv != null ){	try { csv.close();}catch(Exception e){} }
			
			log.info("generaArchivoCSVErrores(S)");
			
		}
		
		return nombreArchivo;
		
	}
	
}