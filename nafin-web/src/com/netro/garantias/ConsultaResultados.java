package com.netro.garantias;

import com.netro.pdf.ComunesPDF;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsultaResultados {
  private final static Log log = ServiceLocator.getInstance().getLog(ConsultaResultados.class);
  private String claveIF;
	private String folio;
	private String opcion;
  private int tipoOper=0;
  private String fechaIni;
  private String fechaFin;
  private String iNoCliente="0";
  private String programa="";
  private String cve_portafolio = "";
  private String enviadoSiag="";
  private String strDirectorioTemp;
  private String origen;
  private String cvePerf;
  private String ejecuta;	
  private List   conditions;
  
  StringBuffer qrySentencia = new StringBuffer("");
  
  public ConsultaResultados()  {  }
 
 /**
  * 
  */
  	/**
	 * Este m�todo debe regresar un query que obtendr� todos los registros
	 * resultantes de la b�squeda, con la finalidad de generar un archivo.
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public  Registros getDocumentQueryFile(){
	log.info("getDocumentQueryFile(E)");
	AccesoDB con = new AccesoDB(); 
	Registros registros  = new Registros();
	StringBuffer	query			= new StringBuffer("");
						conditions  = new ArrayList();
	query.append("SELECT i.ic_if AS clave_if, i.cg_razon_social AS nombre_if, "+
					" por.cg_descripcion AS nombre_portafolio, pp.ic_programa AS programa,"+
					" es.ic_usuario_facultado AS tipo_envio, TO_CHAR (es.df_fecha_hora, 'dd/mm/yyyy hh24:mi') AS fecha,"+
					" es.ic_folio, s.cg_descripcion AS situacion, DECODE (alt.ic_programa,NULL, es.in_registros_acep,alt.in_registros_acep) AS in_registros_acep,"+
					" DECODE (alt.ic_programa,NULL, es.in_registros_rech,alt.in_registros_rech) AS in_registros_rech,DECODE (alt.ic_programa,NULL, es.in_registros,"+
					" alt.in_registros) AS in_registros,es.ic_situacion, es.fn_impte_total AS monto_enviado,es.in_folio_reproceso AS reproceso FROM gti_estatus_solic es,"+
					" gticat_situacion s,comcat_if i,gti_det_alta_gtia alt,gti_parametros_programas pp,gticat_portafolios por WHERE es.ic_situacion = s.ic_situacion "+
					" AND es.ic_tipo_operacion = s.ic_tipo_operacion AND es.ic_if_siag = i.ic_if_siag AND es.ic_folio = alt.ic_folio(+) AND alt.ic_programa = pp.ic_programa(+) "+
					" AND pp.ic_cartera = por.ic_cartera(+) AND pp.ic_portafolio = por.ic_portafolio(+) AND s.ic_tipo_operacion = 4 ");
	if(claveIF.equals("")){
		System.err.println("IF: "+claveIF);
	} else {
		System.err.println("IF: "+claveIF);
		query.append(" AND i.ic_if ="+claveIF);
	}
	if(fechaIni.equals("")){
		System.err.println("fechaIni: "+fechaIni);
	} else {
		System.err.println("fechaIni: "+fechaIni);
		query.append(" AND es.df_fecha_hora BETWEEN TO_DATE ('"+fechaIni+"', 'DD/MM/YYYY') ");
	}
	if(fechaFin.equals("")){
		System.err.println("fechaFin: "+fechaFin);
	} else {
		System.err.println("fechaFin: "+fechaFin);
		query.append(" AND TO_DATE ('"+fechaFin+"', 'DD/MM/YYYY') + 1");
	}
	if(opcion.equals("")){
		System.err.println("situacion: "+opcion);
	} else {
		System.err.println("situacion: "+opcion);
		query.append(" AND es.ic_situacion = "+opcion );
	}
	query.append(" ORDER BY es.df_fecha_hora DESC" );
	
	
	StringBuffer	sbQuery		= new StringBuffer();
	String 			sHint 		= " /*+ index(es) index(i) index(alt) use_nl(es s i alt pp por)*/ ";
	String 			sCondicion	= "";
	String 			sReferencia	= ",'ConsultaResultados:getDocumentQueryFile("+claveIF+")' as PANTALLA ";
	
	
		if(tipoOper!=0){
			sCondicion += " AND s.ic_tipo_operacion = ? ";
      String tP = String.valueOf(tipoOper);
      conditions.add(tP);
    }
		if(tipoOper==2){
			sCondicion += " AND s.ic_situacion != 7 ";
      String tP = String.valueOf(tipoOper);
      conditions.add(tP);
    }
/*FODEA 000 - 2009 Garantias*/
	   if(claveIF == null){
		} else if(!claveIF.equals("0")){
			sCondicion += " AND i.ic_if = ? ";
			conditions.add(claveIF);
    }
    if(!cve_portafolio.equals("")){
		sCondicion += " AND por.ic_portafolio = ?  ";		
    conditions.add(cve_portafolio);
    }    
		if(!fechaIni.equals("") && !fechaFin.equals("")){
      sCondicion += " AND es.df_fecha_hora BETWEEN TO_DATE(?, 'DD/MM/YYYY') AND TO_DATE(?, 'DD/MM/YYYY') + 1 ";
      conditions.add(fechaIni);
      conditions.add(fechaFin);
    }
		if(!opcion.equals("")){
      sCondicion += " AND es.ic_situacion = ? ";
      conditions.add(opcion);
    }
/*FODEA 000 - 2009 Garantias*/
		sbQuery.append(
			" SELECT   "+sHint+" "   +
      " i.ic_if AS clave_if, i.cg_razon_social AS nombre_if, por.cg_descripcion as nombre_portafolio, "+  /*FODEA 000 - 2009 Garantias*/
		" pp.ic_programa AS programa, "+
      " es.ic_usuario_facultado AS tipo_envio,"+  /*FODEA 000 - 2009 Garantias*/
			" TO_CHAR (es.df_fecha_hora, 'dd/mm/yyyy hh24:mi') AS fecha,"   +
			" es.ic_folio, s.cg_descripcion AS situacion, "+
			" decode(alt.ic_programa, null, es.in_registros_acep, alt.in_registros_acep )as in_registros_acep, " +
			" decode(alt.ic_programa, null, es.in_registros_rech, alt.IN_REGISTROS_RECH )as in_registros_rech, " +
			" decode(alt.ic_programa, null, es.in_registros, alt.in_registros )as in_registros, " +
			" es.ic_situacion, " +  /*FODEA 000 - 2009 Garantias*/
      " es.fn_impte_total as monto_enviado "+  /*FODEA 000 - 2009 Garantias*/
		", es.in_folio_reproceso AS reproceso "+//FODEA 012-2009
		//	sReferencia+
			"     FROM gti_estatus_solic es, gticat_situacion s, comcat_if i, " +
			"		gti_det_alta_gtia alt, gti_parametros_programas pp, gticat_portafolios por " +
			"    WHERE es.ic_situacion = s.ic_situacion " +
			"      AND es.ic_tipo_operacion = s.ic_tipo_operacion " +
			"      AND es.ic_if_siag = i.ic_if_siag " +
			"		AND es.ic_folio = alt.ic_folio(+) " +
			"     AND alt.ic_programa = pp.ic_programa(+) " +
			"     AND pp.ic_cartera = por.ic_cartera(+) " +
			"     AND pp.ic_portafolio = por.ic_portafolio(+) AND s.ic_tipo_operacion = 4 " +
			sCondicion +
			" ORDER BY es.df_fecha_hora desc");
    try{
			con.conexionDB();
			if(ejecuta.equals("1")){
				log.debug("qry: "+query.toString());
				registros = con.consultarDB(query.toString());
			} else {
				System.err.println("conditions: "+conditions);
				log.debug("qry: "+sbQuery.toString());
				log.debug("conditions : "+conditions);
				registros = con.consultarDB(sbQuery.toString(),conditions);
			}
			con.cierraConexionDB();
    } catch (Exception e){
      log.error("getDocumentQueryFile  Error: " + e);
    } finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
    
		log.info("getDocumentQueryFile (S) ");
		return registros;
  }
 
 
 /**
  * 
  */
  public String crearArchivo (){
  AccesoDB con = new AccesoDB();
  PreparedStatement	ps = null;
  ResultSet rs = null;
  String qrySentencia	= "";
  String tabla = "";
  int i = 0;
  
  CreaArchivo archivo = new CreaArchivo();
  StringBuffer contenidoArchivo = new StringBuffer("");
  String nombreArchivo = null;

  log.info("MonitorResultadosExt01::crearArchivo(E)");
	try {
    con.conexionDB();
    if(opcion.equals("origen")){
      tabla = "gti_contenido_arch";
    }else{
      tabla = "gti_arch_rechazo";
    }
    if(!"".equals(claveIF))
      iNoCliente = claveIF;
  
	qrySentencia =
		"select cg_contenido"+
		" from "+tabla+" A"+
		" ,comcat_if I"+
		" where A.ic_if_siag = I.ic_if_siag"+
		" and A.ic_folio = ?"+
		" and I.ic_if = ?"+
		" order by A.ic_linea";
	  
    ps = con.queryPrecompilado(qrySentencia);
    ps.setLong(1, Long.parseLong(folio));
    ps.setLong(2, Long.parseLong(iNoCliente));
    
    rs = ps.executeQuery();
    while(rs.next()){
      contenidoArchivo.append(rs.getString("cg_contenido")+"\n");
      i++;
    }
    ps.close();
    
    if (i>0) {
      if(!archivo.make(contenidoArchivo.toString(), strDirectorioTemp, ".txt"))
          nombreArchivo = "0";
      else
          nombreArchivo = archivo.nombre;
    } else {
      nombreArchivo = "0";
    }
  
  } catch (Exception e){
    log.debug("MonitorResultadosExt01::crearArchivo: ERROR "+ e);
  }finally{
	if(con.hayConexionAbierta())
		con.cierraConexionDB();
  }
  
	log.info("MonitorResultadosExt01::crearArchivo (S) ");
  return nombreArchivo;
  }
  
  /**
   * 
   */
   public String miPDF(HttpServletRequest request, String path, String tipo){
  AccesoDB con = new AccesoDB();
  PreparedStatement	ps	= null;
  ResultSet rs	= null;
  String qrySentencia	= "";
  String tabla = "";
  
  HttpSession session = request.getSession();
  CreaArchivo creaArchivo = new CreaArchivo();
  String nombreArchivo = "";  
  ComunesPDF pdfDoc = new ComunesPDF();	
	//StringBuffer contenidoArchivo = new StringBuffer();
  String icIfSiag = "";
  int i = 0;
   try {
    nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
		pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
		pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		
    try{    //QRY 1
      con.conexionDB();	
      qrySentencia =
      "select "+
		" ES.ic_folio"+
		" ,I.cg_razon_social as NOMBRE_IF"+
		" ,to_char(df_validacion_siag,'dd/mm/yyyy') as FECHA_PROCESO "+
		" ,to_char(df_autorizacion_siag,'dd/mm/yyyy') as FECHA_AUTORIZA "+
		" ,NVL(ES.in_registros_acep,0) as ACEP"+
		" ,NVL(ES.in_registros_rech,0) as RECH"+
		" ,NVL(ES.in_registros,0) as TOTAL"+
		" ,es.IC_IF_SIAG"+
		" ,es.ic_situacion"+
		" ,es.ig_anio_trim_calif"+
		" ,es.ig_trim_calif"+
		" from gti_estatus_solic ES"+
		" ,comcat_if I"+
		" where ES.ic_if_siag = I.ic_if_siag"+
		" and ES.ic_folio = ?"+
		" and I.ic_if = ?";
    
      ps = con.queryPrecompilado(qrySentencia);  
      ps.setString(1,folio);
      ps.setString(2,claveIF);
      
      rs = ps.executeQuery();
      if(rs.next()){
      icIfSiag = rs.getString("IC_IF_SIAG");
      pdfDoc.setTable(1,100);
      if("CONSALTA".equals(origen)) {
        pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
        pdfDoc.setCell("RESULTADOS DE SOLICITUD DE ALTA DE GARANTIAS","celda04",ComunesPDF.CENTER);
      }else if("CONSSALCOM".equals(origen)){
        pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
        pdfDoc.setCell("RESULTADOS DE SOLICITUD DE CARGA DE SALDOS Y PAGO DE COMISIONES","celda04",ComunesPDF.CENTER);		
      }else if("CONSCALIF".equals(origen)){
        pdfDoc.addText("\nA�o: "+rs.getString("IG_ANIO_TRIM_CALIF")+"   Trimestre: "+rs.getString("IG_TRIM_CALIF")+"\n","formas",ComunesPDF.CENTER);
        pdfDoc.setCell("RESULTADOS DE SOLICITUD DE CARGA DE CALIFICACION DE CARTERA","celda04",ComunesPDF.CENTER);		
      }else{
        pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
        pdfDoc.setCell("RESULTADOS","celda04",ComunesPDF.CENTER);
      }
      pdfDoc.addTable();
  
      if(!"CONSSALCOM".equals(origen)){
        if(rs.getString("FECHA_PROCESO")== null ){
					pdfDoc.addText("Fecha de operaci�n en firme: \n","formas",ComunesPDF.RIGHT);
					}else{
					pdfDoc.addText("Fecha de operaci�n en firme: "+rs.getString("FECHA_PROCESO")+"\n","formas",ComunesPDF.RIGHT);
					}
      }else{
      int situacion = rs.getInt("ic_situacion");
          if(situacion ==5){
            if(rs.getString("FECHA_AUTORIZA") == null ){
              pdfDoc.addText("Fecha de Operaci�n en Firme: \n","formas",ComunesPDF.RIGHT);
            }else{
              pdfDoc.addText("Fecha de Operaci�n en Firme: "+rs.getString("FECHA_AUTORIZA")+"\n","formas",ComunesPDF.RIGHT);
            }
          }else{
            if (rs.getString("FECHA_PROCESO") == null ){
              pdfDoc.addText("Fecha de C�lculo de Comisiones: \n","formas",ComunesPDF.RIGHT);
            }else{
              pdfDoc.addText("Fecha de C�lculo de Comisiones: "+rs.getString("FECHA_PROCESO")+"\n","formas",ComunesPDF.RIGHT);
            }
          }
      }
      pdfDoc.setTable(5,95);
        pdfDoc.setCell("FOLIO DE LA OPERACION","celda01",ComunesPDF.CENTER);
        pdfDoc.setCell("INTERMEDIARIO FINANCIERO","celda01",ComunesPDF.CENTER);
        pdfDoc.setCell("REGISTROS SIN ERRORES","celda01",ComunesPDF.CENTER);
        pdfDoc.setCell("REGISTROS CON ERRORES","celda01",ComunesPDF.CENTER);
        pdfDoc.setCell("TOTAL DE REGISTROS PROCESADOS","celda01",ComunesPDF.CENTER);
  
        pdfDoc.setCell(rs.getString("IC_FOLIO"),"formas",ComunesPDF.CENTER);
        pdfDoc.setCell(rs.getString("NOMBRE_IF"),"formas",ComunesPDF.CENTER);
        pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("ACEP"),0),"formas",ComunesPDF.CENTER);
        pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("RECH"),0),"formas",ComunesPDF.CENTER);
        pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("TOTAL"),0),"formas",ComunesPDF.CENTER);			
  
        pdfDoc.addTable();
      }
      ps.close();
      
      pdfDoc.addText("\n\n\n","formas",ComunesPDF.LEFT);	
      if("CONSALTA".equals(origen)) {
        qrySentencia =
          " select ic_programa"   +
          "  ,fn_porc_comision"   +
          "  ,in_registros_acep"   +
          "  ,in_registros_rech"   +
          "  ,in_registros"   +
          " from gti_det_alta_gtia GD"   +
          " , comcat_if I"   +
          " where GD.ic_if_siag = I.ic_if_siag" +
          " and GD.ic_folio = ?"+
          " and I.ic_if = ?";
        System.err.println ("qrySentencia 2: "+qrySentencia);
        ps = con.queryPrecompilado(qrySentencia);
        ps.setString(1,folio);
        ps.setString(2,claveIF);
        		
        rs = ps.executeQuery();
        while(rs.next()){
          if(i==0){
            pdfDoc.setTable(1,100);
            pdfDoc.setCell("DETALLE DE GARANTIAS POR PROGRAMA","celda04",ComunesPDF.CENTER);
            pdfDoc.addTable();
            pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
            pdfDoc.setTable(5,95);
            pdfDoc.setCell("CLAVE DEL PROGRAMA","celda01",ComunesPDF.CENTER);
            pdfDoc.setCell("PORCENTAJE DE COMISION","celda01",ComunesPDF.CENTER);
            pdfDoc.setCell("REGISTROS SIN ERRORES","celda01",ComunesPDF.CENTER);
            pdfDoc.setCell("REGISTROS CON ERRORES","celda01",ComunesPDF.CENTER);
            pdfDoc.setCell("TOTAL DE REGISTROS PROCESADOS","celda01",ComunesPDF.CENTER);
          }
      
          pdfDoc.setCell(rs.getString("IC_PROGRAMA")+" ","formas",ComunesPDF.CENTER);
          pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("FN_PORC_COMISION"),4)+" ","formas",ComunesPDF.CENTER);
          pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("IN_REGISTROS_ACEP"),0)+"","formas",ComunesPDF.CENTER);
          pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("IN_REGISTROS_RECH"),0)+"","formas",ComunesPDF.CENTER);
          pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("IN_REGISTROS"),0)+" ","formas",ComunesPDF.CENTER);
          i++;
        }	// while
        ps.close();
      }// ALTA
      else if("CONSSALCOM".equals(origen)) {
      qrySentencia =
			"  SELECT nvl(m.cd_nombre,'NA') as cd_nombre,"   +
			"  		in_aceptados,"   +
			"         in_rechazados,"   +
			" 		in_tot_registros, "   +
			" 		fn_impte_autoriza"   +
			"  FROM GTI_DETALLE_SALDOS ds"   +
			"   ,comcat_moneda m"   +
			"   WHERE ds.ic_moneda = m.ic_moneda(+)"   +
			"   and ic_if_siag = ? "+
			" AND ic_folio = ?"  +
			" order by decode(m.ic_moneda,1,1,54,2,3)";
      System.err.println ("qrySentencia 3: "+qrySentencia);
      ps = con.queryPrecompilado(qrySentencia);
      ps.setString(1,icIfSiag);
      ps.setString(2,folio);
      rs = ps.executeQuery();
      while(rs.next()){
			if(i==0){
				pdfDoc.setTable(1,100);
				pdfDoc.setCell("DETALLE DE COMISIONES POR MONEDA","celda04",ComunesPDF.CENTER);
				pdfDoc.addTable();
				pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
				pdfDoc.setTable(5,95);
				pdfDoc.setCell("MONEDA","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("REGISTROS SIN ERRORES","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("REGISTROS CON ERRORES","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("TOTAL DE REGISTROS","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("IMPORTE DE COMISI�N E IVA DE REGISTROS ACEPTADOS","celda01",ComunesPDF.CENTER);
			}
	
			pdfDoc.setCell(rs.getString("cd_nombre")+"","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(rs.getInt("in_aceptados")+"","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(rs.getInt("in_rechazados")+"","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(rs.getInt("in_tot_registros")+"","formas",ComunesPDF.CENTER);
			pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("fn_impte_autoriza"),2)+" ","formas",ComunesPDF.CENTER);
			i++;
		}	// while
      rs.close();ps.close();
    }  //CONSSALCOM
      
    if(i>0)
      pdfDoc.addTable();
    
    pdfDoc.addText("\n\n\n","formas",ComunesPDF.LEFT);				
       
    qrySentencia =
      "select cg_contenido"+
      " from gti_arch_resultado A"+
      " ,comcat_if I"+
      " where A.ic_if_siag = I.ic_if_siag"+
      " and A.ic_folio = ?"+
      " and I.ic_if = ?"+
      " order by A.ic_linea,A.ic_num_error";
    System.err.println ("qrySentencia 8: "+qrySentencia);
    ps = con.queryPrecompilado(qrySentencia);
    ps.setString(1,folio);
    ps.setString(2,claveIF);
    
    rs = ps.executeQuery();
    i=0;
    while(rs.next()){
		if(i==0){
			pdfDoc.setTable(1,100);
			pdfDoc.setCell("ERRORES GENERADOS","celda04",ComunesPDF.CENTER);
			pdfDoc.addTable();	
		}
		pdfDoc.addText(rs.getString("cg_contenido")+"\n","formas",ComunesPDF.LEFT);
		i++;
    }
    ps.close();
    
    pdfDoc.endDocument();  
    } catch (Exception e){
      System.err.println("ERROR: "+e);
    } //FIN TRY QRY 
		finally{
      if(con.hayConexionAbierta())
      con.cierraConexionDB();
    }
			
    	
  } catch (Exception e){ ///TRY PRINCIPAL
    System.err.println("ERROR: "+e);
  }finally{
	if(con.hayConexionAbierta())
		con.cierraConexionDB();
  }
  return nombreArchivo;
  }


  public void setClaveIF(String claveIF)
  {
    this.claveIF = claveIF;
  }


  public String getClaveIF()
  {
    return claveIF;
  }


  public void setFolio(String folio)
  {
    this.folio = folio;
  }


  public String getFolio()
  {
    return folio;
  }


  public void setOpcion(String opcion)
  {
    this.opcion = opcion;
  }


  public String getOpcion()
  {
    return opcion;
  }


  public void setTipoOper(int tipoOper)
  {
    this.tipoOper = tipoOper;
  }


  public int getTipoOper()
  {
    return tipoOper;
  }


  public void setFechaIni(String fechaIni)
  {
    this.fechaIni = fechaIni;
  }


  public String getFechaIni()
  {
    return fechaIni;
  }


  public void setFechaFin(String fechaFin)
  {
    this.fechaFin = fechaFin;
  }


  public String getFechaFin()
  {
    return fechaFin;
  }


  public void setINoCliente(String iNoCliente)
  {
    this.iNoCliente = iNoCliente;
  }


  public String getINoCliente()
  {
    return iNoCliente;
  }


  public void setPrograma(String programa)
  {
    this.programa = programa;
  }


  public String getPrograma()
  {
    return programa;
  }


  public void setCve_portafolio(String cve_portafolio)
  {
    this.cve_portafolio = cve_portafolio;
  }


  public String getCve_portafolio()
  {
    return cve_portafolio;
  }


  public void setEnviadoSiag(String enviadoSiag)
  {
    this.enviadoSiag = enviadoSiag;
  }


  public String getEnviadoSiag()
  {
    return enviadoSiag;
  }


  public void setStrDirectorioTemp(String strDirectorioTemp)
  {
    this.strDirectorioTemp = strDirectorioTemp;
  }


  public String getStrDirectorioTemp()
  {
    return strDirectorioTemp;
  }


  public void setOrigen(String origen)
  {
    this.origen = origen;
  }


  public String getOrigen()
  {
    return origen;
  }


  public void setCvePerf(String cvePerf)
  {
    this.cvePerf = cvePerf;
  }


  public String getCvePerf()
  {
    return cvePerf;
  }


	public void setEjecuta(String ejecuta) {
		this.ejecuta = ejecuta;
	}


	public String getEjecuta() {
		return ejecuta;
	}
}