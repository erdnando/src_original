package com.netro.garantias;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**

	Esta clase se encarga de generar los archivos del "M&oacute;dulo" de Conciliaci&oacute;n Autom&aacute;tica de Garant&iacute;as.

	La pantallas donde se utiliza esta clase son las siguientes:
	 		PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - SALDOS INSOLUTOS - OBTENCION PORTAFOLIO A CONCILIAR
	 		PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - SALDO PENDIENTE POR RECUPERAR (SPPR) - OBTENCION PORTAFOLIO A CONCILIAR
	 		PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - COMISIONES COBRADAS - OBTENCION PORTAFOLIO A CONCILIAR
	 		PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - SALDOS INSOLUTOS - CARGA DE ARCHIVO
	 		PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - SALDO PENDIENTE POR RECUPERAR (SPPR) - CARGA DE ARCHIVO
	 		PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - COMISIONES COBRADAS - CARGA DE ARCHIVO
	 		
   @author jshernandez
	@since F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 28/10/2013 05:06:09 p.m.
	
 */
public class ConciliacionAutomatica {

	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ConciliacionAutomatica.class);
	
	/**
	 * Realiza la extracci�n de la Base de Datos los archivos de portafolio publicados por NAFIN(SIAG).
	 *
	 * Este m&eacute;todo se ocupa en las siguientes pantallas:
	 *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - SALDOS INSOLUTOS - OBTENCION PORTAFOLIO A CONCILIAR
	 *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - SALDO PENDIENTE POR RECUPERAR (SPPR) - OBTENCION PORTAFOLIO A CONCILIAR
	 *    PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - COMISIONES COBRADAS - OBTENCION PORTAFOLIO A CONCILIAR
	 *
	 * @throws AppException
	 *
	 *	@param claveMesConciliar <tt>String</tt> con el n&uacute;mero del Mes a Conciliar. Formato AAAAMM.
	 * @param claveFiso <tt>String</tt> con la Clave del Contragarante.
	 * @param claveIfSiag <tt>String</tt> con la Clave SIAG del IF.
	 * @param claveTipoConciliacion <tt>String</tt> con la Clave del Tipo de Conciliacion.
	 *	@param directorioTemporal <tt>String</tt> con el path del directorio donde se guardara el archivo generado.
	 *
	 * @return <tt>String</tt> con el nombre del archivo generado.
	 *
	 * @author jshernandez
	 * @since F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 28/10/2013 05:31:04 p.m.
	 *
	 */ 
	public static String extraeArchivoPortafolioNafin( String claveMesConciliar, String claveFiso, String claveIfSiag, String claveTipoConciliacion, String directorioTemporal )
		throws AppException {
	
		log.info("extraeArchivoPortafolioNafin(E)");
		
		AccesoDB 			con 				= new AccesoDB();
		PreparedStatement ps 				= null;
		ResultSet		   rs 				= null;
		StringBuffer		query 			= new StringBuffer();
		
		OutputStream 		outputStream 	= null;
		InputStream 		inputStream 	= null;
		
		String 				nombreArchivo	= null;
		
		try {

			// Conectarse a la Base  de Datos
			con.conexionDB();
			
			// Preparar Query
			query.append(
				"SELECT                                                            "  +
				"   BI_ARCHIVO_PORTAFOLIO_NAFIN                                    "  +
				"FROM                                                              "  +
				"   GTI_CONC_ARCHIVO_PORTAFOLIO                                    "  +
				"WHERE                                                             "  +
				"  IC_MES_CONCILIAR     = ? AND                                    "  +
				"  IC_FISO              = ? AND                                    "  +
				"  IC_IF_SIAG           = ? AND                                    "  +
				"  IC_TIPO_CONCILIACION = ?                                        "
			);
			
			// Realizar la consulta
			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1,  			Integer.parseInt(claveMesConciliar)		); 
			ps.setLong(2, 			Long.parseLong(claveFiso)					); 
			ps.setInt(3,  			Integer.parseInt(claveIfSiag)				); 
			ps.setInt(4,  			Integer.parseInt(claveTipoConciliacion));
			rs = ps.executeQuery();
			
			if( rs.next() ){

				// Asignar nombre al archivo
				nombreArchivo	= Comunes.cadenaAleatoria(16) + ".zip";
				outputStream	= new FileOutputStream( directorioTemporal + nombreArchivo );
				
				// Guardar archivo en disco
				inputStream 			= rs.getBinaryStream("BI_ARCHIVO_PORTAFOLIO_NAFIN");
				byte[]   buffer      = new byte [ 4 * 1024 ]; // 4 KB 
				int      bytesRead   = 0;
				while(  ( bytesRead  = inputStream.read( buffer ) )  != -1 ){
            	outputStream.write( buffer, 0, bytesRead );
            	outputStream.flush();
            }
			
			}
			
		} catch(Exception e){
			
			log.error("extraeArchivoPortafolioNafin(Exception)");
			log.error("extraeArchivoPortafolioNafin.claveMesConciliar     = <" + claveMesConciliar     + ">"); 
			log.error("extraeArchivoPortafolioNafin.claveFiso             = <" + claveFiso             + ">");
			log.error("extraeArchivoPortafolioNafin.claveIfSiag           = <" + claveIfSiag           + ">");
			log.error("extraeArchivoPortafolioNafin.claveTipoConciliacion = <" + claveTipoConciliacion + ">"); 
			log.error("extraeArchivoPortafolioNafin.directorioTemporal    = <" + directorioTemporal    + ">");

			e.printStackTrace();
			
			throw new AppException("Fallo la extraccion del archivo portafolio de la base de datos: " + e.getMessage());
			
		} finally {
			
			if( inputStream	!= null ){ try { inputStream.close();  } catch(Exception e){}  }
			if( outputStream	!= null ){ try { outputStream.close(); } catch(Exception e){}  }

			if( rs 				!= null ){ try { rs.close();				} catch(Exception e){} 	}
		 	if( ps 				!= null ){ try { ps.close();				} catch(Exception e){} 	}
		 	
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			
			log.info("extraeArchivoPortafolioNafin(S)");
			
		}
			
		return nombreArchivo;
		
	}
	
	
	public static String extraerArchivoIf(String tipo_arch, String claveMesConciliar, String claveFiso, String claveIfSiag, String claveTipoConciliacion, String directorioTemporal )
		throws AppException {
		
		log.info("extraerArchivoIf(E)");
		
		AccesoDB 			con 				= new AccesoDB();
		PreparedStatement ps 				= null;
		ResultSet		   rs 				= null;
		StringBuffer		query 			= new StringBuffer();
		String campo = "";
		
		OutputStream 		outputStream 	= null;
		InputStream 		inputStream 	= null;
		String 				nombreArchivo	= null;
		
		if(tipo_arch.equals("origen"))
			campo = " BI_ARCHIVO_PORTAFOLIO_IF ";
		else if(tipo_arch.equals("detalle"))
			campo = " BI_ARCHIVO_REPORTE_DETALLE ";
		
		try {

			// Conectarse a la Base  de Datos
			con.conexionDB();
			
			// Preparar Query
			query.append(
				"SELECT                                                            "  +
				campo +
				" FROM                                                              "  +
				"   GTI_CONC_ARCHIVO_PORTAFOLIO                                    "  +
				"WHERE                                                             "  +
				"  IC_MES_CONCILIAR     = ? AND                                    "  +
				"  IC_FISO              = ? AND                                    "  +
				"  IC_IF_SIAG           = ? AND                                    "  +
				"  IC_TIPO_CONCILIACION = ?                                        "
			);
			
			// Realizar la consulta
			log.debug("QRY-> "+query.toString());
			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1,  			Integer.parseInt(claveMesConciliar)		); 
			ps.setLong(2, 			Long.parseLong(claveFiso)					); 
			ps.setInt(3,  			Integer.parseInt(claveIfSiag)				); 
			ps.setInt(4,  			Integer.parseInt(claveTipoConciliacion));
			rs = ps.executeQuery();
			
			if( rs.next() ){

				// Asignar nombre al archivo
				nombreArchivo	= Comunes.cadenaAleatoria(16) + ".zip";
				outputStream	= new FileOutputStream( directorioTemporal + nombreArchivo );
				
				// Guardar archivo en disco
				if(tipo_arch.equals("origen"))
					inputStream 			= rs.getBinaryStream("BI_ARCHIVO_PORTAFOLIO_IF");
				else if(tipo_arch.equals("detalle"))
					inputStream 			= rs.getBinaryStream("BI_ARCHIVO_REPORTE_DETALLE");
					
				byte[]   buffer      = new byte [ 4 * 1024 ]; // 4 KB 
				int      bytesRead   = 0;
				while(  ( bytesRead  = inputStream.read( buffer ) )  != -1 ){
            	outputStream.write( buffer, 0, bytesRead );
            	outputStream.flush();
            }
			
			}
			
		} catch(Exception e){
			
			log.error("extraeArchivoPortafolioNafin(Exception)");
			log.error("extraeArchivoPortafolioNafin.claveMesConciliar     = <" + claveMesConciliar     + ">"); 
			log.error("extraeArchivoPortafolioNafin.claveFiso             = <" + claveFiso             + ">");
			log.error("extraeArchivoPortafolioNafin.claveIfSiag           = <" + claveIfSiag           + ">");
			log.error("extraeArchivoPortafolioNafin.claveTipoConciliacion = <" + claveTipoConciliacion + ">"); 
			log.error("extraeArchivoPortafolioNafin.directorioTemporal    = <" + directorioTemporal    + ">");

			e.printStackTrace();
			
			throw new AppException("Fallo la extraccion del archivo portafolio de la base de datos: " + e.getMessage());
			
		} finally {
			
			if( inputStream	!= null ){ try { inputStream.close();  } catch(Exception e){}  }
			if( outputStream	!= null ){ try { outputStream.close(); } catch(Exception e){}  }

			if( rs 				!= null ){ try { rs.close();				} catch(Exception e){} 	}
		 	if( ps 				!= null ){ try { ps.close();				} catch(Exception e){} 	}
		 	
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			
			log.info("extraerArchivoIf(S)");
			
		}
			
		return nombreArchivo;	
	}
	
	/**
	 * Realiza la extracci�n de la Base de Datos del archivo pdf de resultados publicado IF(SIAG).
	 * @throws AppException
	 *
	 *	@param claveMesConciliar <tt>String</tt> con el n&uacute;mero del Mes a Conciliar. Formato AAAAMM.
	 * @param claveFiso <tt>String</tt> con la Clave del Contragarante.
	 * @param claveIfSiag <tt>String</tt> con la Clave SIAG del IF.
	 * @param claveTipoConciliacion <tt>String</tt> con la Clave del Tipo de Conciliacion.
	 *	@param directorioTemporal <tt>String</tt> con el path del directorio donde se guardara el archivo generado.
	 *
	 * @return <tt>String</tt> con el nombre del archivo generado.
	 *
	 * @author jshernandez (ajustado por J�se Alvarado)
	 * @since F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 28/10/2013 05:31:04 p.m.
	 *
	 */ 
	public static String extraerArchivoPDFIf(String claveMesConciliar, String claveFiso, String claveIfSiag, String claveTipoConciliacion, String directorioTemporal )
		throws AppException {
		
		log.info("extraerArchivoPDFIf(E)");
		
		AccesoDB 			con 				= new AccesoDB();
		PreparedStatement ps 				= null;
		ResultSet		   rs 				= null;
		StringBuffer		query 			= new StringBuffer();
		
		OutputStream 		outputStream 	= null;
		InputStream 		inputStream 	= null;
		String 				nombreArchivo	= null;
		
		try{

			// Conectarse a la Base  de Datos
			con.conexionDB();
			
			// Preparar Query
			query.append(
				"SELECT                                                            "  +
				" BI_ARCHIVO_REPORTE_RESULTADOS " +
				" FROM                                                              "  +
				"   GTI_CONC_ARCHIVO_PORTAFOLIO                                    "  +
				"WHERE                                                             "  +
				"  IC_MES_CONCILIAR     = ? AND                                    "  +
				"  IC_FISO              = ? AND                                    "  +
				"  IC_IF_SIAG           = ? AND                                    "  +
				"  IC_TIPO_CONCILIACION = ?                                        "
			);
			
			// Realizar la consulta
			ps = con.queryPrecompilado(query.toString());
			System.out.println("QRY-> "+query.toString());
			ps.setInt(1,  			Integer.parseInt(claveMesConciliar)		); 
			ps.setLong(2, 			Long.parseLong(claveFiso)					); 
			ps.setInt(3,  			Integer.parseInt(claveIfSiag)				); 
			ps.setInt(4,  			Integer.parseInt(claveTipoConciliacion));
			rs = ps.executeQuery();
			
			if( rs.next() ){

				// Asignar nombre al archivo
				nombreArchivo	= Comunes.cadenaAleatoria(16) + ".pdf";
				outputStream	= new FileOutputStream( directorioTemporal + nombreArchivo );
				
				// Guardar archivo en disco
				inputStream 			= rs.getBinaryStream("BI_ARCHIVO_REPORTE_RESULTADOS");
					
				byte[]   buffer      = new byte [ 4 * 1024 ]; // 4 KB 
				int      bytesRead   = 0;
				while(  ( bytesRead  = inputStream.read( buffer ) )  != -1 ){
            	outputStream.write( buffer, 0, bytesRead );
            	outputStream.flush();
            }
			
			}
			
		}catch(Exception e){
			
			log.error("extraeArchivoPortafolioNafin(Exception)");
			log.error("extraeArchivoPortafolioNafin.claveMesConciliar     = <" + claveMesConciliar     + ">"); 
			log.error("extraeArchivoPortafolioNafin.claveFiso             = <" + claveFiso             + ">");
			log.error("extraeArchivoPortafolioNafin.claveIfSiag           = <" + claveIfSiag           + ">");
			log.error("extraeArchivoPortafolioNafin.claveTipoConciliacion = <" + claveTipoConciliacion + ">"); 
			log.error("extraeArchivoPortafolioNafin.directorioTemporal    = <" + directorioTemporal    + ">");

			e.printStackTrace();
			
			throw new AppException("Fallo la extraccion del archivo portafolio de la base de datos: " + e.getMessage());
			
		}finally {
			
			if( inputStream	!= null ){ try { inputStream.close();  } catch(Exception e){}  }
			if( outputStream	!= null ){ try { outputStream.close(); } catch(Exception e){}  }

			if( rs 				!= null ){ try { rs.close();				} catch(Exception e){} 	}
		 	if( ps 				!= null ){ try { ps.close();				} catch(Exception e){} 	}
		 	
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			
			log.info("extraerArchivoPDFIf(S)");
		}
		return nombreArchivo;	
	}
	
	/**
	 * Realiza la extracci�n de la Base de Datos de la C&eacute;dula Conciliada publicada por NAFIN(SIAG).
	 *
	 * Este m&eacute;todo se ocupa en la siguiente pantalla:
	 *		PANTALLA: ADMIN IF GARANT - CONCILIACION AUTOMATICA - CEDULA DE CONCILIACION
	 *
	 * @throws AppException
	 *
	 *	@param claveMesConciliar <tt>String</tt> con el n&uacute;mero del Mes Conciliado. Formato AAAAMM.
	 * @param claveFiso <tt>String</tt> con la Clave del Contragarante.
	 * @param claveIfSiag <tt>String</tt> con la Clave SIAG del IF.
	 * @param claveMoneda <tt>String</tt> con la Clave de la Moneda.
	 *	@param directorioTemporal <tt>String</tt> con el path del directorio donde se guardara el archivo generado.
	 *
	 * @return <tt>String</tt> con el nombre del archivo generado.
	 *
	 * @author jshernandez
	 * @since F027 - 2013 -- GARANTIAS - Conciliacion Automatica de Garantias; 07/11/2013 11:55:37 a.m.
	 *
	 */ 
	public static String extraerCedulaConciliada( String claveMesConciliar, String claveFiso, String claveIfSiag, String claveMoneda, String directorioTemporal )
		throws AppException {
	
		log.info("extraerCedulaConciliada(E)");
		
		AccesoDB 			con 				= new AccesoDB();
		PreparedStatement ps 				= null;
		ResultSet		   rs 				= null;
		StringBuffer		query 			= new StringBuffer();
		
		OutputStream 		outputStream 	= null;
		InputStream 		inputStream 	= null;
		
		String 				nombreArchivo	= null;
		
		try {

			// Conectarse a la Base  de Datos
			con.conexionDB();
			
			// Preparar Query
			query.append(
				"SELECT                                                            "  +
				"   BI_ARCH_CEDULA_CONCILIACIONES                                    "  +
				"FROM                                                              "  +
				"   GTI_CONC_CEDULA                                    "  +
				"WHERE                                                             "  +
				"  IC_MES_CONCILIAR     = ? AND                                    "  +
				"  IC_FISO              = ? AND                                    "  +
				"  IC_IF_SIAG           = ? AND                                    "  +
				"  IC_MONEDA            = ?                                        "
			);
			
			// Realizar la consulta
			ps = con.queryPrecompilado(query.toString());
			ps.setInt(1,  			Integer.parseInt(claveMesConciliar)		); 
			ps.setLong(2, 			Long.parseLong(claveFiso)					); 
			ps.setInt(3,  			Integer.parseInt(claveIfSiag)				); 
			ps.setInt(4,  			Integer.parseInt(claveMoneda)				);
			rs = ps.executeQuery();
			
			if( rs.next() ){

				// Asignar nombre al archivo
				nombreArchivo	= Comunes.cadenaAleatoria(16) + ".pdf";
				outputStream	= new FileOutputStream( directorioTemporal + nombreArchivo );
				
				// Guardar archivo en disco
				inputStream 			= rs.getBinaryStream("BI_ARCH_CEDULA_CONCILIACIONES");
				byte[]   buffer      = new byte [ 4 * 1024 ]; // 4 KB 
				int      bytesRead   = 0;
				while(  ( bytesRead  = inputStream.read( buffer ) )  != -1 ){
            	outputStream.write( buffer, 0, bytesRead );
            	outputStream.flush();
            }
			
			}
			
		} catch(Exception e){
			
			log.error("extraerCedulaConciliada(Exception)");
			log.error("extraerCedulaConciliada.claveMesConciliar     = <" + claveMesConciliar     + ">"); 
			log.error("extraerCedulaConciliada.claveFiso             = <" + claveFiso             + ">");
			log.error("extraerCedulaConciliada.claveIfSiag           = <" + claveIfSiag           + ">");
			log.error("extraerCedulaConciliada.claveMoneda           = <" + claveMoneda           + ">"); 
			log.error("extraerCedulaConciliada.directorioTemporal    = <" + directorioTemporal    + ">");

			e.printStackTrace();
			
			throw new AppException("Fallo la extraccion de la Cedula Conciliada de la base de datos: " + e.getMessage());
			
		} finally {
			
			if( inputStream	!= null ){ try { inputStream.close();  } catch(Exception e){}  }
			if( outputStream	!= null ){ try { outputStream.close(); } catch(Exception e){}  }

			if( rs 				!= null ){ try { rs.close();				} catch(Exception e){} 	}
		 	if( ps 				!= null ){ try { ps.close();				} catch(Exception e){} 	}
		 	
		 	if(con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
			
			log.info("extraerCedulaConciliada(S)");
			
		}
			
		return nombreArchivo;
		
	}
		
}
	