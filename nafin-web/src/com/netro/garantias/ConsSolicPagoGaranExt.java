package com.netro.garantias;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsSolicPagoGaranExt implements IQueryGeneratorRegExtJS   {
	public ConsSolicPagoGaranExt()  { 	}
//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsSolicPagoGaranExt.class);
		
	StringBuffer 		qrySentencia;
	private List 		conditions;
	private String 	paginaOffset;
	private String 	paginaNo;
	private String intermediario;
	private String foperacionIni;
	private String foperacionFin;
	private String situacion;
	private String folio; 
	private String ic_if_siag;
	private String cc_garantias;
	private String ic_contragarante;
	private String tipoArchivo;

			 
	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */

	public String getAggregateCalculationQuery() {
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
	 	qrySentencia.append("  Select  count(1) total "); 
		qrySentencia.append(" FROM gti_solicitud_pago sp, ");
		qrySentencia.append(" gticat_situacion s, ");
		qrySentencia.append(" gti_estatus_solic es  ");
		qrySentencia.append(" ,comcat_if i ");
		qrySentencia.append("  WHERE es.ic_situacion = s.ic_situacion ");
		qrySentencia.append("  AND es.ic_tipo_operacion = s.ic_tipo_operacion ");
		qrySentencia.append("  AND es.ic_folio = sp.ic_folio ");
		qrySentencia.append("  AND es.ic_if_siag = sp.ic_if_siag ");
		qrySentencia.append("  AND es.ic_tipo_operacion = 3 ");
		qrySentencia.append("  AND es.ic_if_siag = i.ic_if_siag ");
		qrySentencia.append("  AND sp.ic_if_siag = i.ic_if_siag ");
		 
		if(!intermediario.equals("")){ 		
			qrySentencia.append(" AND i.ic_if = ? ") ;
			conditions.add(intermediario);
		}
		
		if(!foperacionIni.equals("") &&  !foperacionFin.equals("") ){  		
			qrySentencia.append(" AND es.df_fecha_hora BETWEEN TO_DATE (?, 'DD/MM/YYYY') "); 
			qrySentencia.append(" AND TO_DATE (?, 'DD/MM/YYYY') + 1 "); 
			conditions.add(foperacionIni);
			conditions.add(foperacionFin);
		}	
		
			if(!situacion.equals("") ){ 			
			qrySentencia.append(" AND es.ic_situacion = ? ") ;
			conditions.add(situacion);
		}
		
		if(!folio.equals("")){ 		
			qrySentencia.append(" AND es.ic_folio = ? ") ;
			conditions.add(folio);
		}
		 
		qrySentencia.append(" 	ORDER BY es.df_fecha_hora DESC ");
	
			log.debug("getAggregateCalculationQuery "+conditions.toString());
			log.debug("getAggregateCalculationQuery "+qrySentencia.toString());
			return qrySentencia.toString();
				
	}
	
	
	 /**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery(){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
	  	
		qrySentencia.append(" SELECT es.ic_if_siag as ic_if_siag , i.cg_razon_social AS nombre_if, ");
		qrySentencia.append(" TO_CHAR (es.df_fecha_hora, 'dd/mm/yyyy hh24:mi') AS fecha, ");
		qrySentencia.append(" es.ic_folio as folio,  ");
		qrySentencia.append(" s.cg_descripcion AS situacion,");
		qrySentencia.append(" es.ic_situacion as ic_situacion, ");
		qrySentencia.append(" es.ic_contragarante  as ic_contragarante , ");
		qrySentencia.append(" sp.cc_garantia  as cc_garantia ");
		qrySentencia.append(" FROM gti_solicitud_pago sp, ");
		qrySentencia.append(" gticat_situacion s, ");
		qrySentencia.append(" gti_estatus_solic es  ");
		qrySentencia.append(" ,comcat_if i ");
		qrySentencia.append("  WHERE es.ic_situacion = s.ic_situacion ");
		qrySentencia.append("  AND es.ic_tipo_operacion = s.ic_tipo_operacion ");
		qrySentencia.append("  AND es.ic_folio = sp.ic_folio ");
		qrySentencia.append("  AND es.ic_if_siag = sp.ic_if_siag ");
		qrySentencia.append("  AND es.ic_tipo_operacion = 3 ");
		qrySentencia.append("  AND es.ic_if_siag = i.ic_if_siag ");
		qrySentencia.append("  AND sp.ic_if_siag = i.ic_if_siag ");
		
		 
		if(!intermediario.equals("")){ 		
			qrySentencia.append(" AND i.ic_if = ? ") ;
			conditions.add(intermediario);
		}
		
		if(!foperacionIni.equals("") &&  !foperacionFin.equals("") ){  		
			qrySentencia.append(" AND es.df_fecha_hora BETWEEN TO_DATE (?, 'DD/MM/YYYY') "); 
			qrySentencia.append(" AND TO_DATE (?, 'DD/MM/YYYY') + 1 "); 
			conditions.add(foperacionIni);
			conditions.add(foperacionFin);
		}	
		
		if(!situacion.equals("") ){ 	
			qrySentencia.append(" AND es.ic_situacion = ? ") ;
			conditions.add(situacion);
		}
		
		if(!folio.equals("")){ 		
			qrySentencia.append(" AND es.ic_folio = ? ") ;
			conditions.add(folio);
		}
		 
		qrySentencia.append(" ORDER BY es.df_fecha_hora DESC ");
		
			
			log.debug("getDocumentQuery "+conditions.toString());
			log.debug("getDocumentQuery "+qrySentencia.toString());
			return qrySentencia.toString();
	}

	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		qrySentencia.append(" SELECT es.ic_if_siag as ic_if_siag ,  i.cg_razon_social AS nombre_if, ");
		qrySentencia.append(" TO_CHAR (es.df_fecha_hora, 'dd/mm/yyyy hh24:mi') AS fecha, ");
		qrySentencia.append(" es.ic_folio as folio,  ");
		qrySentencia.append(" s.cg_descripcion AS situacion,");
		qrySentencia.append(" es.ic_situacion as ic_situacion, ");
		qrySentencia.append(" es.ic_contragarante  as ic_contragarante , ");
		qrySentencia.append(" sp.cc_garantia  as cc_garantia ");
		
		qrySentencia.append(" FROM gti_solicitud_pago sp, ");
		qrySentencia.append(" gticat_situacion s, ");
		qrySentencia.append(" gti_estatus_solic es  ");
		qrySentencia.append(" ,comcat_if i ");
		
		qrySentencia.append("  WHERE es.ic_situacion = s.ic_situacion ");
		qrySentencia.append("  AND es.ic_tipo_operacion = s.ic_tipo_operacion ");
		qrySentencia.append("  AND es.ic_folio = sp.ic_folio ");
		qrySentencia.append("  AND es.ic_if_siag = sp.ic_if_siag ");
		qrySentencia.append("  AND es.ic_tipo_operacion = 3 ");
		qrySentencia.append("  AND es.ic_if_siag = i.ic_if_siag ");
		qrySentencia.append("  AND sp.ic_if_siag = i.ic_if_siag ");
		
		if(!intermediario.equals("")){ 		
			qrySentencia.append(" AND i.ic_if = ? ") ;
			conditions.add(intermediario);
		}
		
		if(!foperacionIni.equals("") &&  !foperacionFin.equals("") ){  		
			qrySentencia.append(" AND es.df_fecha_hora BETWEEN TO_DATE (?, 'DD/MM/YYYY') "); 
			qrySentencia.append(" AND TO_DATE (?, 'DD/MM/YYYY') + 1 "); 
			conditions.add(foperacionIni);
			conditions.add(foperacionFin);
		}	
		
		if(!situacion.equals("") ){ 		
			qrySentencia.append(" AND es.ic_situacion = ? ") ;
			conditions.add(situacion);
		}
		
		if(!folio.equals("")){ 		
			qrySentencia.append(" AND es.ic_folio = ? ") ;
			conditions.add(folio);
		}
		 
		qrySentencia.append(" ORDER BY es.df_fecha_hora DESC ");
			
			
		log.debug("getDocumentSummaryQueryForIds "+conditions.toString());
		log.debug("getDocumentSummaryQueryForIds "+qrySentencia.toString());
		return qrySentencia.toString();
	}
	
	
	

	public String getDocumentQueryFile(){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		log.debug("getDocumentQueryFile(E)");
		try{
		 if(tipoArchivo.equals("PdfSolPagoGaran")){
			qrySentencia.append(
				"	SELECT PG.IC_FOLIO, "+
				"		ES.IC_SITUACION, "+
				"		TO_CHAR (PG.DF_FECHA_HORA, 'dd/mm/yyyy hh24:mi') AS FECHA,  "+
				"		PG.CG_RFC_ACREDITADO, "+
				"		PG.CG_NOMBRE_ACREDITADO, "+
				"		CM.CD_NOMBRE AS MONEDA, "+
				"		TO_CHAR (PG.DF_VTO, 'dd/mm/yyyy') AS FECHA_VTO,  "+
				"		PG.FN_PORC_PARTICIPACION, "+
				"		TO_CHAR (PG.DF_INCUMPLIMIENTO, 'dd/mm/yyyy') AS FECHA_ICTO,  "+
				" 		PG.CG_CAUSAS_INCUMPLIMIENTO, "+
				"		PG.CG_FUNCIONARIO_IF, "+
				"		PG.CG_TEL_FUNCIONARIO, "+
				" 		PG.CG_EXT_FUNCIONARIO,"+
				"		PG.CG_MAIL_FUNCIONARIO, "+
				"		PG.CG_LOCALIZACION_SUPERVISION,"+
				"		PG.CG_CIUDAD_SUPERVISION,"+
				"		PG.CG_ESTADO_SUPERVISION,"+
				"		CTO.CG_DESCRIPCION AS TIPO_OP,"+
				"		PG.CC_GARANTIA,"+
				"		PG.IG_SIT_CALIFICACION, "+
				"		 PG.IG_SIT_PAGO_COMISION,"+
				"		PG.IG_SIT_PAGO_PRORROGA,"+
				"		TO_CHAR (PG.DF_FECHA_HONRADA, 'dd/mm/yyyy') AS DF_FECHA_HONRADA,"+
				"		PG.CG_TIPO_TASA AS TIPO_TASA,"+
				"		PG.FN_TASA, "+
				"		TO_CHAR (PG.DF_FECHA_VTO_PRORROGA, 'dd/mm/yyyy') AS FECHA_VTO_PRORROGA,"+
				"		TO_CHAR (PG.DF_FECHA_ANIV, 'dd/mm/yyyy') AS FECHA_ANIV, "+
				"		PG.FN_PORC_ANIV,"+
				"		PG.FN_COMISION_ANIV,"+
				"		PG.FN_IVA_COMISION_ANIV,"+
				"		PG.FN_COMISION_ANIV + PG.FN_IVA_COMISION_ANIV AS COM,"+
				"		PG.FN_CAPITAL_SIAG,"+
				"		PG.FN_INTERES_SIAG,"+
				"		PG.FN_CAPITAL_SIAG+PG.FN_INTERES_SIAG AS  IMP_PAG_GAN,"+
				"		(PG.FN_CAPITAL_SIAG+PG.FN_INTERES_SIAG)-(PG.FN_COMISION_ANIV + PG.FN_IVA_COMISION_ANIV) AS IMP_NETO_DES,"+
				"		CS.CG_DESCRIPCION AS DESCRIPCION,"+
				"		PG.CG_CAUSAS_RECHAZO,"+
				"		PP.CG_DESCRIPCION AS PERIODICIDAD_PAGO,"+
				"		TO_CHAR (PG.DF_FECHA_SEGUNDO_INCUM, 'dd/mm/yyyy') AS DF_FECHA_SEGUNDO_INCUM "+
				"		FROM GTI_SOLICITUD_PAGO PG, COMCAT_MONEDA CM, GTI_ESTATUS_SOLIC ES, GTICAT_SITUACION CS, GTICAT_TIPOOPERACION CTO, GTICAT_PERIODICIDAD_PAGO PP"+
				"		WHERE ES.IC_IF_SIAG         = PG.IC_IF_SIAG"+
				"		AND   ES.IC_FOLIO           = PG.IC_FOLIO"+
				"		AND   ES.IC_SITUACION       = CS.IC_SITUACION"+
				"		AND   ES.IC_TIPO_OPERACION  = CS.IC_TIPO_OPERACION"+
				"		AND   ES.IC_TIPO_OPERACION  = CTO.IC_TIPO_OPERACION"+
				"		AND   CM.IC_MONEDA(+)       = PG.IC_MONEDA"+
				"		AND   PG.IC_PERIODICIDAD_PAGO = PP.IC_PERIODICIDAD_PAGO "+
				"		AND   ES.IC_TIPO_OPERACION  = 3 "+
				"		AND   PG.IC_FOLIO           = "+folio
			);
		 }else if(tipoArchivo.equals("PdfResultados")){
				qrySentencia   = new StringBuffer();		
			qrySentencia.append(
							" SELECT ES.IC_FOLIO, "+
							"        ES.IC_SITUACION, "+
							"        CCI.CG_RAZON_SOCIAL, "+
							"        PG.CC_GARANTIA, "+
							"        PG.FN_CAPITAL_SIAG, "+
							"        PG.FN_INTERES_SIAG, "+
							"        PG.FN_CAPITAL_SIAG+PG.FN_INTERES_SIAG AS DESEMBOLSE, "+
							"        TO_CHAR (PG.DF_FECHA_ANIV, 'dd/mm/yyyy') AS DF_FECHA_ANIV, "+
							"        PG.FN_PORC_ANIV, "+
							"        PG.FN_COMISION_ANIV, "+
							"        PG.FN_IVA_COMISION_ANIV, "+
							"        PG.FN_IVA_COMISION_ANIV + PG.FN_COMISION_ANIV AS COMISION, "+
							"        PG.IG_SIT_CALIFICACION, "+
							"        PG.IG_SIT_PAGO_PRORROGA, "+
							"        TO_CHAR (PG.DF_FECHA_VTO_PRORROGA, 'dd/mm/yyyy') AS DF_FECHA_VTO_PRORROGA, "+
							"        TO_CHAR (PG.DF_FECHA_HONRADA, 'dd/mm/yyyy') AS DF_FECHA_HONRADA, "+
							"        PG.CG_TIPO_TASA AS TIPO_TASA, "+
							"        PG.FN_TASA, "+
							"        (PG.FN_CAPITAL_SIAG+PG.FN_INTERES_SIAG) - (PG.FN_IVA_COMISION_ANIV + PG.FN_COMISION_ANIV) AS NETO, "+
							"        CM.CD_NOMBRE AS MONEDA, "+
							"        CS.CG_DESCRIPCION AS SITUACION, "+
							"        TO_CHAR(ES.DF_AUTORIZACION_SIAG,'dd/mm/yyyy') AS FECHA_PROCESO "+
							
							" 			FROM GTI_SOLICITUD_PAGO PG, GTI_ESTATUS_SOLIC ES, COMCAT_MONEDA CM, GTICAT_SITUACION CS, GTICAT_TIPOOPERACION CTO, "+
							" 			COMCAT_IF CCI " +
							"			WHERE ES.IC_IF_SIAG         = PG.IC_IF_SIAG "+
							"			AND   ES.IC_FOLIO           = PG.IC_FOLIO "+
							" 			AND   ES.IC_SITUACION       = CS.IC_SITUACION "+
							" 			AND   ES.IC_TIPO_OPERACION  = CS.IC_TIPO_OPERACION "+
							" 			AND   ES.IC_TIPO_OPERACION  = CTO.IC_TIPO_OPERACION "+
							" 			AND   CM.IC_MONEDA(+)       = PG.IC_MONEDA "+
							" 			AND   CCI.IC_IF_SIAG        = PG.IC_IF_SIAG "+
							" 			AND   ES.IC_TIPO_OPERACION  = 3 "+
							" 			AND   PG.IC_FOLIO           = "+ folio
				);
		 
		 
		 }
		}catch(Exception e){
		 System.out.println("getDocumentoQueryFile() :: "+e);	
		}
		log.debug("getDocumentQueryFile(E)");
		log.debug("qrySentencia ::"+qrySentencia.toString());
		return qrySentencia.toString();
	}//getDocumentQueryFile
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
		log.debug("crearCustomFile(E)");
		String nombreArchivo ="";
		String linea ="";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		HttpSession session = request.getSession();
		
		nombreArchivo = Comunes.cadenaAleatoria(16)+".pdf";
		ComunesPDF pdfDoc = new ComunesPDF(2,path+nombreArchivo);
		
		String pais        		= (String)(request.getSession().getAttribute("strPais") == null?"":request.getSession().getAttribute("strPais"));
		String noCliente   		= (String)(request.getSession().getAttribute("iNoCliente") == null?"":request.getSession().getAttribute("iNoCliente"));
		String strnombre   		= (String)(request.getSession().getAttribute("strNombre") == null?"":request.getSession().getAttribute("strNombre"));
		String nombreUsr   		= (String)(request.getSession().getAttribute("strNombreUsuario") == null?"":request.getSession().getAttribute("strNombreUsuario"));
		String logo      	 		= (String)(request.getSession().getAttribute("strLogo") == null?"":request.getSession().getAttribute("strLogo"));
		if(noCliente.equals("0")){
			noCliente = "";
		}		
		log.debug("logo "+logo);
		String icIfSiag       = "";
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
		if(tipo.equals("PdfSolPagoGaran")){
			try{
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);	
				if(rs.next()){
					String sIc_folio         				= (rs.getString("IC_FOLIO")== null)?"":rs.getString("IC_FOLIO");
					String sIc_situacion     				= (rs.getString("IC_FOLIO")==null)?"":rs.getString("IC_SITUACION");
					String sFecha            				= (rs.getString("FECHA")==null)?"":rs.getString("FECHA");
					String sCg_rfc_acreditado   			= (rs.getString("CG_NOMBRE_ACREDITADO")==null)?"":rs.getString("CG_NOMBRE_ACREDITADO");
					String sCg_nombre_acreditado       = (rs.getString("CG_NOMBRE_ACREDITADO") == null)?"":rs.getString("CG_NOMBRE_ACREDITADO") ;
					String sMoneda 							= (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
					String sFecha_vto 						= (rs.getString("FECHA_VTO")==null)?"":rs.getString("FECHA_VTO");
					String sFn_porc_participacion 		= (rs.getString("FN_PORC_PARTICIPACION")==null)?"":rs.getString("FN_PORC_PARTICIPACION");
					String sFecha_icto 						= (rs.getString("FECHA_ICTO")==null)?"":rs.getString("FECHA_ICTO");
					String sCg_causas_incumplimiento 	= (rs.getString("CG_CAUSAS_INCUMPLIMIENTO")==null)?"":rs.getString("CG_CAUSAS_INCUMPLIMIENTO");
					String sCg_funcionario_if 				= (rs.getString("CG_FUNCIONARIO_IF")==null)?"":rs.getString("CG_FUNCIONARIO_IF");
					String sCg_tel_funcionario 			= (rs.getString("CG_TEL_FUNCIONARIO")==null)?"":rs.getString("CG_TEL_FUNCIONARIO");
					String sCg_ext_funcionario 			= (rs.getString("CG_EXT_FUNCIONARIO")==null)?"":rs.getString("CG_EXT_FUNCIONARIO");
					String sCg_mail_funcionario 			= (rs.getString("CG_MAIL_FUNCIONARIO")==null)?"":rs.getString("CG_MAIL_FUNCIONARIO");
					String sCg_localizacion_supervision = (rs.getString("CG_LOCALIZACION_SUPERVISION")==null)?"":rs.getString("CG_LOCALIZACION_SUPERVISION");
					String sCg_ciudad_supervision 		= (rs.getString("CG_CIUDAD_SUPERVISION")==null)?"":rs.getString("CG_CIUDAD_SUPERVISION");
					String sCg_estado_supervision 		= (rs.getString("CG_ESTADO_SUPERVISION")==null)?"":rs.getString("CG_ESTADO_SUPERVISION");
					String sTipo_op 							= (rs.getString("TIPO_OP")==null)?"":rs.getString("TIPO_OP");
					String sCc_garantia 						= (rs.getString("CC_GARANTIA")==null)?"":rs.getString("CC_GARANTIA");
					String sIg_sit_calificacion 			= (rs.getString("IG_SIT_CALIFICACION")==null)?"":rs.getString("IG_SIT_CALIFICACION");
					String sIg_sit_pago_comision 			= (rs.getString("IG_SIT_PAGO_COMISION")==null)?"":rs.getString("IG_SIT_PAGO_COMISION");
					String sIg_sit_pago_prorroga 			= (rs.getString("IG_SIT_PAGO_PRORROGA")==null)?"":rs.getString("IG_SIT_PAGO_PRORROGA");
					String sDf_fecha_honrada				= (rs.getString("DF_FECHA_HONRADA")==null)?"":rs.getString("DF_FECHA_HONRADA");
					String sTipo_tasa							= (rs.getString("TIPO_TASA")==null)?"":rs.getString("TIPO_TASA");
					String sFn_tasa 							= (rs.getString("FN_TASA")==null)?"":rs.getString("FN_TASA");
					String sFecha_vto_prorroga         	= (rs.getString("FECHA_VTO_PRORROGA") == null)?"":rs.getString("FECHA_VTO_PRORROGA") ;
					String sFecha_aniv                 	= (rs.getString("FECHA_ANIV") == null)?"":rs.getString("FECHA_ANIV") ;
					String sFn_porc_aniv               	= (rs.getString("FN_PORC_ANIV") == null)?"":rs.getString("FN_PORC_ANIV") ;
					String sFn_comision_aniv           	= (rs.getString("FN_COMISION_ANIV") == null)?"":rs.getString("FN_COMISION_ANIV") ;
					String sFn_iva_comision_aniv       	= (rs.getString("FN_IVA_COMISION_ANIV") == null)?"":rs.getString("FN_IVA_COMISION_ANIV") ;
					String sCom                       	= (rs.getString("COM") == null)?"":rs.getString("COM") ;
					String sFn_capital_siag            	= (rs.getString("FN_CAPITAL_SIAG") == null)?"":rs.getString("FN_CAPITAL_SIAG") ;
					String sFn_interes_siag            	= (rs.getString("FN_INTERES_SIAG") == null)?"":rs.getString("FN_INTERES_SIAG") ;
					String sImp_pag_gan                	= (rs.getString("IMP_PAG_GAN") == null)?"":rs.getString("IMP_PAG_GAN") ;
					String sImp_neto_des               	= (rs.getString("IMP_NETO_DES") == null)?"":rs.getString("IMP_NETO_DES") ;
					String sDescripcion                	= (rs.getString("DESCRIPCION") == null)?"":rs.getString("DESCRIPCION") ;
					String sCg_causas_rechazo          	= (rs.getString("CG_CAUSAS_RECHAZO") == null)?"":rs.getString("CG_CAUSAS_RECHAZO") ;
					String sPeriodicidad_pago          	= (rs.getString("PERIODICIDAD_PAGO") == null)?"":rs.getString("PERIODICIDAD_PAGO") ; 
					String sDf_fecha_segundo_incum     	= (rs.getString("DF_FECHA_SEGUNDO_INCUM") == null)?"":rs.getString("DF_FECHA_SEGUNDO_INCUM") ; 
					
					if(sIg_sit_calificacion.equals("01") || sIg_sit_calificacion.equals("1"))
						sIg_sit_calificacion        = "Cumpli�" ;  
					if(sIg_sit_calificacion.equals("02") || sIg_sit_calificacion.equals("2"))
						sIg_sit_calificacion        = "No Cumpli�" ;
					
					if(sIg_sit_pago_comision.equals("01") || sIg_sit_pago_comision.equals("1"))
						sIg_sit_pago_comision       = "Cumpli�" ;
					if(sIg_sit_pago_comision.equals("02") || sIg_sit_pago_comision.equals("2"))
						sIg_sit_pago_comision       = "No Cumpli�" ;
		
					if(sIg_sit_pago_prorroga.equals("01") || sIg_sit_pago_prorroga.equals("1"))
						sIg_sit_pago_prorroga       = "Autorizada";			
					if(sIg_sit_pago_prorroga.equals("02") || sIg_sit_pago_prorroga.equals("2"))
						sIg_sit_pago_prorroga       = "Vencida";
					if(sIg_sit_pago_prorroga.equals("03") || sIg_sit_pago_prorroga.equals("3"))
						sIg_sit_pago_prorroga       = "Baja";
					if(sIg_sit_pago_prorroga.equals("04") || sIg_sit_pago_prorroga.equals("4"))
						sIg_sit_pago_prorroga       = "No Autorizada";
					if(sTipo_tasa.equals("1"))
						sTipo_tasa = "Tasa TIIE";
					if(sTipo_tasa.equals("2"))
						sTipo_tasa = "Tasa TIIE menor a 180 d�as";
					if(sTipo_tasa.equals("3"))
						sTipo_tasa = "Tasa Fija";
					if(sTipo_tasa.equals("4"))
						sTipo_tasa = "Tasa Fija menor a 180 d�as";
					
					pdfDoc.addText("Solicitud de Pago de Garant�a ", "formasG", ComunesPDF.CENTER);
					float width[] = {25f, 25f, 25f,25f};
				
					pdfDoc.setTable(4,100,width);
					pdfDoc.setCell("Datos de la solicitud","celda01",ComunesPDF.LEFT,4,1,0);
					pdfDoc.setCell("Folio","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sIc_folio,"formasrep",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("Fecha / Hora de operaci�n","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sFecha,"formasrep",ComunesPDF.LEFT,1,1,0);
		
					pdfDoc.addTable();
		
					pdfDoc.setTable(4, 100, width);
					pdfDoc.setCell("Datos de la captura de solicitud","celda01",ComunesPDF.LEFT,4,1,0);
					//row1
					pdfDoc.setCell("RFC Acreditado","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sCg_rfc_acreditado,"formasrep",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("Nombre del Acreditado","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sCg_nombre_acreditado,"formasrep",ComunesPDF.LEFT,1,1,0);
					//row2
					pdfDoc.setCell("Moneda","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sMoneda,"formasrep",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("Fecha de Vencimiento","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sFecha_vto,"formasrep",ComunesPDF.LEFT,1,1,0);
					//row4
					pdfDoc.setCell("Porcentaje de participaci�n","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(Comunes.formatoDecimal(sFn_porc_participacion,2,true) + " %","formasrep",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("Fecha del primer incumplimiento","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sFecha_icto,"formasrep",ComunesPDF.LEFT,1,1,0);
		
					//row5
					pdfDoc.setCell("Causa del Incumplimieto","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sCg_causas_incumplimiento,"formasrep",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("Fecha del segundo incumplimiento","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sDf_fecha_segundo_incum,"formasrep",ComunesPDF.LEFT,1,1,0);
				
					pdfDoc.setCell("","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("","formasrep",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("Periodicidad de pago","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sPeriodicidad_pago,"formasrep",ComunesPDF.LEFT,1,1,0);
					pdfDoc.addTable();
		
					pdfDoc.setTable(4, 100, width);
					pdfDoc.setCell("Funcionarios Asignado para supervisi�n","celda01",ComunesPDF.LEFT,4,1,0);
					//row1
					pdfDoc.setCell("Nombre","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sCg_funcionario_if,"formasrep",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("Tel�fono","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sCg_tel_funcionario,"formasrep",ComunesPDF.LEFT,1,1,0);
					//row2
					pdfDoc.setCell("Ext.:","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sCg_ext_funcionario,"formasrep",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("Mail","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sCg_mail_funcionario,"formasrep",ComunesPDF.LEFT,1,1,0);
		
					pdfDoc.addTable();
		
					pdfDoc.setTable(4, 100, width);
					pdfDoc.setCell("Datos de la ubicaci�n donde se realiza la supervisi�n","celda01",ComunesPDF.LEFT,4,1,0);
					//row1
					pdfDoc.setCell("Domicilio","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sCg_localizacion_supervision,"formasrep",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("","formasrep",ComunesPDF.LEFT,1,1,0);
					//row2
					pdfDoc.setCell("Ciudad","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sCg_ciudad_supervision,"formasrep",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("Estado","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sCg_estado_supervision,"formasrep",ComunesPDF.LEFT,1,1,0);
		
					pdfDoc.addTable();
		
					pdfDoc.setTable(4, 100, width);
					pdfDoc.setCell("Detalle de resultados","celda01",ComunesPDF.LEFT,4,1,0);
				//row1
					pdfDoc.setCell("Tipo de Solicitud","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sTipo_op,"formasrep",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("Clave de financiamiento","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sCc_garantia,"formasrep",ComunesPDF.LEFT,1,1,0);
				//row2
					pdfDoc.setCell("Situaci�n de calificaci�n","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sIg_sit_calificacion,"formasrep",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("Situaci�n de pago de comisiones","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sIg_sit_pago_comision,"formasrep",ComunesPDF.LEFT,1,1,0);
				//row3
					pdfDoc.setCell("Situaci�n de Prorroga","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sIg_sit_pago_prorroga,"formasrep",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("Fecha Honrada","formasrepB",ComunesPDF.LEFT,1,1,0);
				
					if("8".equals(sIc_situacion)) { //Si la solicitud es rechazada (8). No se coloca la Fecha Honrada aunque se tenga el valor.
						sDf_fecha_honrada = "";
					}
		
					pdfDoc.setCell(sDf_fecha_honrada,"formasrep",ComunesPDF.LEFT,1,1,0);
					//row4
					pdfDoc.setCell("Tipo de Tasa","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sTipo_tasa,"formasrep",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("Tasa","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(Comunes.formatoDecimal(sFn_tasa,2,true)+" %","formasrep",ComunesPDF.LEFT,1,1,0);
				//row5
					pdfDoc.setCell("Fecha de vencimiento de Prorroga","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sFecha_vto_prorroga,"formasrep",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("Fecha de comisi�n por aniversario","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sFecha_aniv,"formasrep",ComunesPDF.LEFT,1,1,0);
				//row6
					pdfDoc.setCell("Porcentaje de comisi�n por aniversario","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(Comunes.formatoDecimal(sFn_porc_aniv,4,true)+" %","formasrep",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("","formasrep",ComunesPDF.LEFT,1,1,0);
		
					pdfDoc.addTable();
					//end page 1
				///////////////////////////////////
				//begin page 2
					pdfDoc.newPage();
					pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
						
					pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
					pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);	
					pdfDoc.setTable(4, 100, width);
					pdfDoc.setCell("Detalle de cargo de comisi�n por aniversario","celda01",ComunesPDF.LEFT,4,1,0);
				//row1
					pdfDoc.setCell("Importe de comisi�n por aniversario","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(Comunes.formatoDecimal(sFn_comision_aniv,2,true),"formasrep",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("IVA de comisi�n por aniversario","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(Comunes.formatoDecimal(sFn_iva_comision_aniv,2,true),"formasrep",ComunesPDF.LEFT,1,1,0);
				//row2
					pdfDoc.setCell("Importe total de comisi�n por aniversario","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(Comunes.formatoDecimal(sCom,2,true),"formasrep",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("","formasrep",ComunesPDF.LEFT,1,1,0);
		
					pdfDoc.addTable();
		
					pdfDoc.setTable(4, 100, width);
					pdfDoc.setCell("Detalle de importe de pago de garant�a","celda01",ComunesPDF.LEFT,4,1,0);
				//row1
					pdfDoc.setCell("Importe de capital a pagar","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(Comunes.formatoDecimal(sFn_capital_siag,2,true),"formasrep",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("Importe de intereses a pagar","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(Comunes.formatoDecimal(sFn_interes_siag,2,true),"formasrep",ComunesPDF.LEFT,1,1,0);
				//row2
					pdfDoc.setCell("Importe de pago de garant�a","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(Comunes.formatoDecimal(sImp_pag_gan,2,true),"formasrep",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("","formasrep",ComunesPDF.LEFT,1,1,0);
		
					pdfDoc.addTable();
		
					pdfDoc.setTable(4, 100, width);
					pdfDoc.setCell("Importe neto a desembolsar (Pago de garant�a menos comisi�n por aniversario)","celda01",ComunesPDF.LEFT,4,1,0);
				//row1
					pdfDoc.setCell("Importe neto a desembolsar","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(Comunes.formatoDecimal(sImp_pag_gan,2,true)+" - "+Comunes.formatoDecimal(sCom,2,true)+" = "+Comunes.formatoDecimal(sImp_neto_des,2,true),"formasrep",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("Situaci�n de la solicitud","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sDescripcion,"formasrep",ComunesPDF.LEFT,1,1,0);
		
					pdfDoc.addTable();
			
		
					pdfDoc.setTable(4, 100, width);
					pdfDoc.setCell("Causas de rechazo de la operaci�n","celda01",ComunesPDF.LEFT,4,1,0);
					//row1
					pdfDoc.setCell("Causas de rechazo de la operaci�n","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sCg_causas_rechazo,"formasrep",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("","formasrepB",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell("","formasrep",ComunesPDF.LEFT,1,1,0);
			
					pdfDoc.addTable();
				//end page 2
					pdfDoc.endDocument();
					
					
				}
				
			}catch(Throwable e){
				e.printStackTrace();  
				throw new AppException("Error al generar el archivo", e);
				
			}
			
		}else if(tipo.equals("PdfResultados")){
			StringBuffer contratos = new StringBuffer();
			StringBuffer sArchi = new StringBuffer();
		  try{
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);	
				if(rs.next()){
					String sIc_folio              = (rs.getString("IC_FOLIO") == null)?"":rs.getString("IC_FOLIO") ;
					String sIc_situacion          = (rs.getString("IC_SITUACION") == null)?"":rs.getString("IC_SITUACION") ;
					String sCg_razon                = (rs.getString("CG_RAZON_SOCIAL") == null)?"":rs.getString("CG_RAZON_SOCIAL") ;
					String sCc_garantia           = (rs.getString("CC_GARANTIA") == null)?"":rs.getString("CC_GARANTIA") ;
					String sFn_capital_siag       = (rs.getString("FN_CAPITAL_SIAG") == null)?"":rs.getString("FN_CAPITAL_SIAG") ;
					String sFn_interes_siag       = (rs.getString("FN_INTERES_SIAG") == null)?"":rs.getString("FN_INTERES_SIAG") ;
					String sDesembolse            = (rs.getString("DESEMBOLSE") == null)?"":rs.getString("DESEMBOLSE") ;
					String sDf_fecha_aniv         = (rs.getString("DF_FECHA_ANIV") == null)?"":rs.getString("DF_FECHA_ANIV") ;
					String sFn_porc_aniv          = (rs.getString("FN_PORC_ANIV") == null)?"":rs.getString("FN_PORC_ANIV") ;
					String sFn_comision_aniv      = (rs.getString("FN_COMISION_ANIV") == null)?"":rs.getString("FN_COMISION_ANIV") ;
					String sFn_iva_comision_aniv  = (rs.getString("FN_IVA_COMISION_ANIV") == null)?"":rs.getString("FN_IVA_COMISION_ANIV") ;
					String sComision              = (rs.getString("COMISION") == null)?"":rs.getString("COMISION") ;
					String sIg_sit_calificacion   = (rs.getString("IG_SIT_CALIFICACION") == null)?"":rs.getString("IG_SIT_CALIFICACION") ;
					String sIg_sit_pago_prorroga  = (rs.getString("IG_SIT_PAGO_PRORROGA") == null)?"":rs.getString("IG_SIT_PAGO_PRORROGA") ;
					String sDf_fecha_vto_prorroga = (rs.getString("DF_FECHA_VTO_PRORROGA") == null)?"":rs.getString("DF_FECHA_VTO_PRORROGA") ;
					String sDf_fecha_honrada      = (rs.getString("DF_FECHA_HONRADA") == null)?"":rs.getString("DF_FECHA_HONRADA") ;
					String sTipo_tasa             = (rs.getString("TIPO_TASA") == null)?"":rs.getString("TIPO_TASA") ;
					String sFn_tasa               = (rs.getString("FN_TASA") == null)?"":rs.getString("FN_TASA") ;
					String sNeto                  = (rs.getString("NETO") == null)?"":rs.getString("NETO") ;
					String sMoneda                = (rs.getString("MONEDA") == null)?"":rs.getString("MONEDA") ;
					String sSituacionDescripcion = (rs.getString("SITUACION") == null)?"":rs.getString("SITUACION") ;
					String fechaProceso = (rs.getString("FECHA_PROCESO") == null)?"":rs.getString("FECHA_PROCESO");
						if(sTipo_tasa.equals("1"))
							sTipo_tasa = "Tasa TIIE";
						if(sTipo_tasa.equals("2"))
							sTipo_tasa = "Tasa TIIE menor a 180 d�as";
						if(sTipo_tasa.equals("3"))
							sTipo_tasa = "Tasa Fija";
						if(sTipo_tasa.equals("4"))
							sTipo_tasa = "Tasa Fija menor a 180 d�as";
				
						sSituacionDescripcion = "SOLICITUD " + sSituacionDescripcion;
		
						if(sIg_sit_calificacion.equals("01") || sIg_sit_calificacion.equals("1"))
							sIg_sit_calificacion        = "Cumpli�" ;
						if(sIg_sit_calificacion.equals("02") || sIg_sit_calificacion.equals("2"))
							sIg_sit_calificacion        = "No Cumpli�" ;
					
						if(sIg_sit_pago_prorroga.equals("01") || sIg_sit_pago_prorroga.equals("1"))
							sIg_sit_pago_prorroga       = "Autorizada";			
						if(sIg_sit_pago_prorroga.equals("02") || sIg_sit_pago_prorroga.equals("2"))
							sIg_sit_pago_prorroga       = "Vencida";
						if(sIg_sit_pago_prorroga.equals("03") || sIg_sit_pago_prorroga.equals("3"))
							sIg_sit_pago_prorroga       = "Baja";
						if(sIg_sit_pago_prorroga.equals("04") || sIg_sit_pago_prorroga.equals("4"))
							sIg_sit_pago_prorroga       = "No Autorizada";

					
					
		
				float widthtb1[] = {100f};
				pdfDoc.setTable(1, 100, widthtb1);
				pdfDoc.setCell("Resultados de solicitudes de pago de garant�as","celda01",ComunesPDF.CENTER,1,1,1);
				pdfDoc.addTable();
				pdfDoc.addText("Fecha de operaci�n en firme: "+fechaProceso+"\n","formas",ComunesPDF.RIGHT);
				pdfDoc.addText(sSituacionDescripcion,"formasG",ComunesPDF.CENTER);

				float widthtb2[] = {16.6f,16.6f,16.6f,16.6f,16.6f,16.6f};
				pdfDoc.setTable(6, 100, widthtb2);
				pdfDoc.setCell("Folio de la operaci�n","celda01",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell("Intermediario Financiero","celda01",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell("Numero de Garantia","celda01",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell("Capital","celda01",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell("Intereses","celda01",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell("Total desembolsado","celda01",ComunesPDF.CENTER,1,1,1);
				//row 1
				pdfDoc.setCell(sIc_folio+"\n ","formasrep",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell(sCg_razon,"formasrep",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell(sCc_garantia,"formasrep",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell(Comunes.formatoDecimal(sFn_capital_siag,2,true),"formasrep",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell(Comunes.formatoDecimal(sFn_interes_siag,2,true),"formasrep",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell(Comunes.formatoDecimal(sDesembolse,2,true),"formasrep",ComunesPDF.CENTER,1,1,1);
				
				pdfDoc.addTable();
		
				float widthtb3[] = {13f,13f,12f,12f,12.5f,12.5f,12.5f,12.5f};
				pdfDoc.setTable(8, 100, widthtb3);
				pdfDoc.setCell("Calculo de la comisi�n por aniversario","celda01",ComunesPDF.CENTER,5,1,1);
				pdfDoc.setCell("Verificaci�n calificaci�n y prorroga","celda01",ComunesPDF.CENTER,3,1,1);
				
				pdfDoc.setCell("Fecha de Aniversario","celda01",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell("% comisi�n","celda01",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell("Monto comisi�n","celda01",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell("Monto IVA","celda01",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell("Monto Total","celda01",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell("Situaci�n calificaci�n","celda01",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell("Situaci�n prorroga","celda01",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell("Fecha vencimiento prorroga","celda01",ComunesPDF.CENTER,1,1,1);
				//row 1
				pdfDoc.setCell(sDf_fecha_aniv+"\n ","formasrep",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell(Comunes.formatoDecimal(sFn_porc_aniv,4,true)+" %","formasrep",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell(Comunes.formatoDecimal(sFn_comision_aniv,2,true),"formasrep",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell(Comunes.formatoDecimal(sFn_iva_comision_aniv,2,true),"formasrep",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell(Comunes.formatoDecimal(sComision,2,true),"formasrep",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell(sIg_sit_calificacion,"formasrep",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell(sIg_sit_pago_prorroga,"formasrep",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell(sDf_fecha_vto_prorroga,"formasrep",ComunesPDF.CENTER,1,1,1);
				
				pdfDoc.addTable();
		
				pdfDoc.setTable(1, 100, widthtb1);
				pdfDoc.setCell("Datos complementarios","celda01",ComunesPDF.CENTER,1,1,1);
				pdfDoc.addTable();
		
				pdfDoc.setTable(6, 100, widthtb2);
				pdfDoc.setCell("Fecha honrada","celda01",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell("Tipo de tasa","celda01",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell("Tasa","celda01",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell("Importe Neto","celda01",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell("Contrato de Inversi�n","celda01",ComunesPDF.CENTER,1,1,1);
				//row 1
		
				if("8".equals(sIc_situacion)) { //Si la solicitud es rechazada (8). No se coloca la Fecha Honrada aunque se tenga el valor.
					sDf_fecha_honrada = "";
				}   
		
				pdfDoc.setCell(sDf_fecha_honrada+"\n ","formasrep",ComunesPDF.CENTER,1,1,1);
				
				
				pdfDoc.setCell(sTipo_tasa,"formasrep",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell(Comunes.formatoDecimal(sFn_tasa,2,true) + " %","formasrep",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell(Comunes.formatoDecimal(sNeto,2,true),"formasrep",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell(sMoneda,"formasrep",ComunesPDF.CENTER,1,1,1);
				if (ic_contragarante != null && !ic_contragarante.equals("") && cc_garantias != null && !cc_garantias.equals("")) {
					contratos= this.getNumeroContrato();
				}
				if(contratos.length() > 0){
					pdfDoc.setCell(contratos.toString(),"formasrep",ComunesPDF.CENTER,1,1,1);
				}else{
					pdfDoc.setCell("","formasrep",ComunesPDF.CENTER,1,1,1);
				}
		
				pdfDoc.addTable();

				pdfDoc.setTable(1, 100, widthtb1);
				pdfDoc.setCell("Errores Generados","celda01",ComunesPDF.CENTER,1,1,1);
				sArchi = this.getContenidoArchivo();
				pdfDoc.setCell(sArchi.toString(),"formasrep",ComunesPDF.LEFT,1,1,1);
				pdfDoc.addTable();
		
				pdfDoc.endDocument();	
					
					
				}
				
			}catch(Throwable e){
				e.printStackTrace();  
				throw new AppException("Error al generar el archivo", e);
				
			}
		
		}
		log.debug("crearCustomFile(S)");
		return nombreArchivo;
	}//getDocumentQueryFile
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		log.debug("crearCustomFile(E)");
		log.debug("crearCustomFile(S)");
		return "";
	}
	
	
	/**
	 * Funci�n que nos regresa una el numero de contrato
	 * @throws java.lang.Exception
	 * @return 
	 * @param ic_if
	 */
	public StringBuffer  getNumeroContrato ()   throws Exception{ 
		log.info("getFirmasModificatorias (E)");
   	AccesoDB 	con = new AccesoDB();
      boolean		commit = true;		
      PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer 	SQL = new StringBuffer();  
		StringBuffer contratos = new StringBuffer();  
		List varBind = new ArrayList();
		int i = 0;
   	try{
			con.conexionDB();
         
			qrySentencia   = new StringBuffer();
			qrySentencia.append(
				" SELECT /*+ use_nl(gar,des,a,b) */ "+
				" 	pp.numero_contrato AS contrato "+
				" FROM gia_garantias gar, "+
				" 	gia_desembolsos_fisos des, " +
				" 	siag_proyecto_producto pp, " +
				" 	siag_contragaranteproyecto cp "+
				" WHERE des.dfi_fiso_contraparte IS NOT NULL " +
				" 	AND nvl(des.dfi_procesado,0) = 0 " +
				" 	AND nvl(des.dfi_recuperacion,' ') = 'D' " +
				" 	AND des.dfi_fiso_clave = gar.gar_fiso " +
				" 	AND des.dfi_uso_contragarantia = gar.gar_uso_contragarantia "+
				" 	AND des.dfi_prg_clave = gar.tpro_clave "+
				" 	AND des.dfi_clave_garant = gar.clave " +
				" 	AND des.dfi_interm_clave = gar.inter_clave " +
				" 	AND pp.pfo_fiso_contraparte = des.dfi_fiso_contraparte "+
				" 	AND pp.clave_tipoprograma_siag = des.dfi_prg_clave " +
				" 	AND cp.uco_clave = decode(des.dfi_tipo_desembolso,'N',2,3) " +
				" 	AND pp.clave_contragarante = cp.clave_contragarante " +
				" 	AND pp.clave_proyecto = cp.clave_proyecto "+
				" 	AND pp.numero_contrato = cp.numero_contrato " +
				" 	AND gar.gar_fiso = ? " +
				" 	AND gar.inter_clave = ? "+
				" 	AND gar.clave = ? "+
				" ORDER BY pp.numero_contrato "
			);			
			varBind.add(ic_contragarante);
			varBind.add(ic_if_siag);
			varBind.add("'"+cc_garantias+"'");
			log.debug("qrySentencia ::"+qrySentencia.toString());
			log.debug("varBind ::"+varBind.toString());
			ps = con.queryPrecompilado(qrySentencia.toString(), varBind);
			rs = ps.executeQuery();
			
			
			
		   while( rs.next() ){
				contratos.append(rs.getString("contrato") + "\n");
			}
			rs.close();
			ps.close();		
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			log.error("Error getFirmasModificatorias "+e);
			throw new AppException("Error al getFirmasModificatorias",e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getFirmasModificatorias (S)");
		}	
		return contratos;
   }  
	
	/**
	 * Funci�n que nos regresa el contenido del Archivo
	 * @throws java.lang.Exception
	 * @return 
	 * @param ic_if
	 */
	public StringBuffer  getContenidoArchivo ()   throws Exception{ 
		log.info("getFirmasModificatorias (E)");
   	AccesoDB 	con = new AccesoDB();
      boolean		commit = true;		
      PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer 	SQL = new StringBuffer();  
		StringBuffer sArchi   = new StringBuffer();	
		List varBind = new ArrayList();
		int i = 0;
   	try{
			con.conexionDB();
         
			qrySentencia   = new StringBuffer();
			qrySentencia.append(
				" SELECT CG_CONTENIDO "+
				" 	FROM GTI_ARCH_RESULTADO"+
				" WHERE IC_IF_SIAG         = ? "+
				" 	AND   IC_FOLIO           = ? " +
				" 	ORDER BY IC_LINEA , IC_NUM_ERROR "
			);			
			varBind.add(ic_if_siag);
			varBind.add(folio);
			ps = con.queryPrecompilado(qrySentencia.toString(), varBind);
			rs = ps.executeQuery();
			log.debug("qrySentencia ::"+SQL.toString());
			log.debug("varBind ::"+varBind.toString());
		   while( rs.next() ){
				sArchi.append( ((rs.getString("CG_CONTENIDO") == null)?"":rs.getString("CG_CONTENIDO")+"\n") );
			}
			sArchi.append("\n ");
			rs.close();
			ps.close();		
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			log.error("Error getFirmasModificatorias "+e);
			throw new AppException("Error al getFirmasModificatorias",e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getFirmasModificatorias (S)");
		}	
		return sArchi;
   }  



/*****************************************************
	 GETTERS
*******************************************************/
 
	/**
	  Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
		
	
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}


public String getIntermediario() {
		return intermediario;
	}

	public void setIntermediario(String intermediario) {
		this.intermediario = intermediario;
	}

	public String getFoperacionIni() {
		return foperacionIni;
	}

	public void setFoperacionIni(String foperacionIni) {
		this.foperacionIni = foperacionIni;
	}

	public String getFoperacionFin() {
		return foperacionFin;
	}

	public void setFoperacionFin(String foperacionFin) {
		this.foperacionFin = foperacionFin;
	}

	public String getSituacion() {
		return situacion;
	}

	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

////////////////
	public String getIc_if_siag() {
		return ic_if_siag;
	}

	public void setIc_if_siag(String ic_if_siag) {
		this.ic_if_siag = ic_if_siag;
	}

	public String getCc_garantias() {
		return cc_garantias;
	}

	public void setCc_garantias(String cc_garantias) {
		this.cc_garantias = cc_garantias;
	}
	
	public String getIc_contragarante() {
		return ic_contragarante;
	}

	public void setIc_contragarante(String ic_contragarante) {
		this.ic_contragarante = ic_contragarante;
	}

//tipoArchivo
	public String getTipoArchivo() {
		return tipoArchivo;
	}

	public void setTipoArchivo(String tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}


	
}