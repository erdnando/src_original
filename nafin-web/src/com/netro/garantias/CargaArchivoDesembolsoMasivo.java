package com.netro.garantias;

import com.netro.pdf.ComunesPDF;

import java.util.HashMap;

import netropology.utilerias.AppException;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**



	@since  Fodea 004 - 2012 -- Migracion IF
	@date	  08/06/2012 12:54:10 p.m.
	
 */
public class CargaArchivoDesembolsoMasivo {
	
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(CargaArchivoSaldosYPagoComisiones.class);

	/**
		Crea un archivo PDF con la informaci�n del acuse de la carga
		@throws AppException 
		
		@param rg 												<tt>ResultadosGar</tt> con el resultado de la transmision de solicitudes.
		@param strDirectorioTemp   						<tt>String</tt> con la ruta del directorio temporal donde se creara el archivo PDF.
		@param cabecera       								<tt>HashMap</tt> con la informacion de la cabcera del PDF.
		@param strDirectorioPublicacion 					<tt>String</tt> con la ruta WEB del directorio de publicacion.
		@param totalRegistros							   <tt>String</tt> con el monto total de registros a transmitir.
		@param montoTotal									   <tt>String</tt> con el monto total.
		@param claveMes									   <tt>String</tt> con la clave del mes, con formado AAAAMM
		@param loginUsuario  								<tt>String</tt> con el login del usuario firmado en el sistema.
		@param nombreUsuario   								<tt>String</tt> con el nombre del usuario firmado en el sistema.
 
		@return <tt>String</tt> con el nombre del archivo generado.
	*/	
	public static String generaAcuseArchivoPDF(
			ResultadosGar 	rg,
			String 			strDirectorioTemp,
			HashMap			cabecera,
			String 			strDirectorioPublicacion,
			String			totalRegistros,
			String 			montoTotal,
			String 			claveMes,
			String 			loginUsuario,
			String 			nombreUsuario,
			String			tipoOperacion
	) throws AppException{
 
		log.info("generaAcuseArchivoPDF(E)");
		
		String 		nombreArchivo 	= null;
		CreaArchivo archivo 			= null;
		ComunesPDF 	pdfDoc 			= null;
		
		try {
			
			archivo 						= new CreaArchivo();
			nombreArchivo 				= archivo.nombreArchivo()+".pdf";
	 
			pdfDoc 						= new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
			
			String 	meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String 	fechaActual  	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String 	diaActual   	= fechaActual.substring(0,2);
			String 	mesActual    	= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String 	anioActual   	= fechaActual.substring(6,10);
			String 	horaActual  	= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
			// Determinar el mes de carga
			int 		indice 			= Integer.parseInt( claveMes.substring(4,6) ) - 1;
			String   nombreMes		= meses[indice] ;
			// Determinar el a�o de la carga
			String 	numeroAnio 		= claveMes.substring(0,4);
		
			pdfDoc.encabezadoConImagenes(
						pdfDoc,
						(String) cabecera.get("strPais"),
						(String) cabecera.get("iNoNafinElectronico"),
						(String) cabecera.get("sesExterno"),
						(String) cabecera.get("strNombre"),
						(String) cabecera.get("strNombreUsuario"),
						(String) cabecera.get("strLogo"),
						strDirectorioPublicacion
					);
		
			
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
			
			pdfDoc.addText("Solicitud de Desembolsos Masivos de Garant�as","formasG",ComunesPDF.CENTER);
			
			pdfDoc.addText("Su solicitud fue recibida con el n�mero de folio: "+rg.getFolio(),"formas",ComunesPDF.CENTER);
			pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
			
			pdfDoc.setTable(2,70);
			
			pdfDoc.setCell("CIFRAS CONTROL",										"celda03",	ComunesPDF.CENTER,2,1,1);
			
			pdfDoc.setCell("Tipo de Operaci�n",			"celda02",	ComunesPDF.CENTER);// No. total de registros
			pdfDoc.setCell(tipoOperacion,										"formas",	ComunesPDF.RIGHT);
							
			
			pdfDoc.setCell("Fecha de carga",										"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(rg.getFecha(),											"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.setCell("Hora de carga",										"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(rg.getHora(),											"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.setCell("Fecha valor",										"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(rg.getFechaValor(),											"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.setCell("Usuario",												"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(loginUsuario+" - "+nombreUsuario,				"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.addTable();
			pdfDoc.endDocument();
		
		}catch(Exception e){
			
			log.error("generaAcuseArchivoPDF(Exception)");
			e.printStackTrace();
			log.error("rg                       = <" + rg                       + ">");
			log.error("strDirectorioTemp        = <" + strDirectorioTemp        + ">");
			log.error("cabecera                 = <" + cabecera                 + ">");
			log.error("strDirectorioPublicacion = <" + strDirectorioPublicacion + ">");
			log.error("montoTotal               = <" + montoTotal               + ">");
			log.error("claveMes                 = <" + claveMes                 + ">");
			log.error("loginUsuario             = <" + loginUsuario             + ">");
			log.error("nombreUsuario            = <" + nombreUsuario            + ">");
			
			throw new AppException("Ocurri� un error al generar el PDF con la informaci�n del acuse.");
			
		}
		log.info("generaAcuseArchivoPDF(S)");
		
		return nombreArchivo;	
	
	}
}