package com.netro.garantias;
import java.util.ArrayList;
import netropology.utilerias.AccesoDB;
import netropology.utilerias.Registros;
import java.sql.*;
import java.util.*;
import netropology.utilerias.ServiceLocator;
import org.apache.commons.logging.Log;
/**
 * <Detailed description>
 * @see    	Esta clase es una copia de ConsSolicIFGAbean para generar las cosultas para nafin. 
 * @author 	Ivan Almaguer
 * @version 1.0 
 * @since	14/09/2009
 */
public class ConsSolicNafinGAbean01  {
	private int iTipoOperacion = 0;
	/*FODEA 000 - 2009 Garantias*/
	private int iIF = 0;
	private String cve_portafolio = "";
	private String fecha_oper_ini = "";
	private String fecha_oper_fin = "";
	private String cbo_situacion = "";
	private List conditions ;//MIGRACION FODEA-XX-2014
	private String ic_if;//MIGRACION FODEA-XX-2014
	
	private static final Log log = ServiceLocator.getInstance().getLog(ConsSolicNafinGAbean01.class);//Variable para enviar mensajes al log.
	
	
	/*FODEA 000 - 2009 Garantias*/
	
	public ConsSolicNafinGAbean01() {}
	
	public void setiTipoOperacion(int iTipoOperacion) {
		this.iTipoOperacion = iTipoOperacion;
	}
	public void setCve_portafolio(String cve_portafolio) {
		this.cve_portafolio = cve_portafolio;
	}
  /*FODEA 000 - 2009 Garantias*/
	public void setiIF(int iIF) {
		this.iIF = iIF;
	}
  public void setFechaOperIni(String fecha_oper_ini){
    this.fecha_oper_ini = fecha_oper_ini;
  }
  public void setFechaOperFin(String fecha_oper_fin){
    this.fecha_oper_fin = fecha_oper_fin;
  }
  public void setSituacion(String cbo_situacion){
    this.cbo_situacion = cbo_situacion;
  }

  /*FODEA 000 - 2009 Garantias*/
	public String getQuerySolicitudes() {
		StringBuffer	sbQuery		= new StringBuffer();
		String 			sHint 		= " /*+ index(es) index(i) index(alt) use_nl(es s i alt pp por)*/ ";   
		String 			sCondicion	= "";
		String 			sReferencia	= ",'ConsSolicNafinGAbean:getQueryPorEstatus("+iIF+")' as PANTALLA ";
		conditions = new ArrayList();
		if(iTipoOperacion!=0){
			sCondicion += " AND s.ic_tipo_operacion = ? ";
			//la variable ic_if al ser null provoca errores en : ADMIN GARANT Programa de Garantia Automatica/Recuperaciones/Consulta de Resultados de las solicitudes.
			if(ic_if !=null){ 
				conditions.add(new Integer(iTipoOperacion));
			}
		}	
		if(iTipoOperacion==2){
			sCondicion += " AND s.ic_situacion != 7 ";
		}		
/*FODEA 000 - 2009 Garantias*/
		if(iIF != 0 ){
			sCondicion += " AND i.ic_if = ? ";
			if(ic_if !=null){
				conditions.add(new Integer(iIF));
			}
    }
	 //FODEA-XX-2014
	 if(ic_if ==null){}else if(!ic_if.equals("0")&&!ic_if.equals("")){    
      sCondicion += " AND i.ic_if = ? ";
		conditions.add(ic_if);
    }
		if(!cve_portafolio.equals("")){
			sCondicion += " AND por.ic_portafolio = ?  ";		
			if(ic_if !=null){
				conditions.add(cve_portafolio);
			}
	 }
		if(!fecha_oper_ini.equals("") && !fecha_oper_fin.equals("")){
			sCondicion += " AND es.df_fecha_hora BETWEEN TO_DATE(?, 'DD/MM/YYYY') AND TO_DATE(?, 'DD/MM/YYYY') + 1 ";
			if(ic_if !=null){
				conditions.add(fecha_oper_ini);
				conditions.add(fecha_oper_fin);
			}
    }
		if(!cbo_situacion.equals("")){
			sCondicion += " AND es.ic_situacion = ? ";
			if(ic_if !=null){
				conditions.add(cbo_situacion);
			}
    }
/*FODEA 000 - 2009 Garantias*/
		sbQuery.append(
			" SELECT   "+sHint+" "   +
				" i.ic_if AS clave_if, i.cg_razon_social AS nombre_if, "+  /*FODEA 000 - 2009 Garantias*/
				" es.ic_folio,es.ic_usuario_facultado AS tipo_envio, "+
				" TO_CHAR (es.df_fecha_hora, 'dd/mm/yyyy hh24:mi') AS fecha, "+  /*FODEA 000 - 2009 Garantias*/
				"  s.cg_descripcion AS situacion, "   +
				" es.IN_REGISTROS_ACEP,  "+
				" es.in_registros_rech, " +
				" es.in_registros, " +
				" es.ic_situacion, s.ic_situacion AS ic_situacion, " +
				" TO_CHAR (es.df_fecha_hora, 'dd/mm/yyyy hh24:mi') AS df_fecha_hora, " +  /*FODEA 000 - 2009 Garantias*/
				" es.fn_impte_total AS monto_enviado, "+  /*FODEA 000 - 2009 Garantias*/
				"es.in_folio_reproceso AS reproceso "+//FODEA 012-2009
				sReferencia+
				"     FROM gti_estatus_solic es, " +
				"		gticat_situacion s, " +
				"		comcat_if i " +
				"    WHERE es.ic_situacion = s.ic_situacion " +
				"      AND es.ic_tipo_operacion = s.ic_tipo_operacion " +
				"      AND es.ic_if_siag = i.ic_if_siag " +
			sCondicion +
			" ORDER BY es.df_fecha_hora desc");
		System.out.println("sbQuery.toString()>>>>"+sbQuery.toString());
		System.out.println("conditions>>>>"+conditions);
		return sbQuery.toString();
	} // getQuerySolicitudes()
	
	public Registros consultar(){
		AccesoDB 				   con			= new AccesoDB();
		Registros reg = new Registros();
		try{
			con.conexionDB();
			reg = con.consultarDB(this.getQuerySolicitudes(),conditions);
		
		}catch(Exception e){
			System.err.println("Error consultar :"+e);
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(true); //Para consultas que hagan uso de tablas remotas via dblink
				con.cierraConexionDB();
			}
		} 
		return reg;
	}


	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}


	public String getIc_if() {
		return ic_if;
	}
	 public List  getTotalFolio(String ic_tipo_operacion,String ic_if,String ic_folio,String df_fecha_ini,String df_fecha_fin, String folio, String situacion)throws Exception{
		log.info("getTotalFolio (E)");  
		
		AccesoDB 	con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;  
		StringBuffer strQuery 		= new StringBuffer();
		boolean		commit = true;
		String total_folio ="";
		HashMap datos = new HashMap();
		List listReg = new ArrayList(); 
		try{
			con.conexionDB();
			String tOp =""; 
			String icIf =""; 
			String fol =""; 
			String sit =""; 
			String rang ="";
			System.out.println("ic_if *** "+ic_if);
			if(!ic_tipo_operacion.equals("")){
				tOp = " 	AND s.ic_tipo_operacion = "+ic_tipo_operacion;
			}
			if(!ic_if.equals("")){
				icIf ="	AND i.ic_if = "+ic_if;
			}
			if(!folio.equals("")){
				fol = "	AND es.ic_folio ="+folio;
			}
			if(!situacion.equals("")){
				sit = "	AND es.ic_situacion ="+situacion;
			}
			
			if(!df_fecha_ini.equals("")&&!df_fecha_fin.equals("")){
				rang = "	AND es.df_fecha_hora BETWEEN TO_DATE ('"+df_fecha_ini+"', 'DD/MM/YYYY') AND TO_DATE ('"+df_fecha_fin+"', 'DD/MM/YYYY') + 1" ;
			}
			strQuery.append(
				"	SELECT   count(es.ic_folio) as total_reg, es.ic_folio  " + 
				" 	FROM gti_estatus_solic es, " +
				"	gticat_situacion s, " +
				"	comcat_if i, " +
				"	gti_det_alta_gtia alt, " +
				"	gticat_portafolios por, " +
					"	gti_parametros_programas pp " +
				"	WHERE es.ic_situacion = s.ic_situacion " +
				"	AND es.ic_tipo_operacion = s.ic_tipo_operacion" +
				"	AND es.ic_if_siag = i.ic_if_siag" +
				"	AND es.ic_folio = alt.ic_folio(+)" +
				"	AND alt.ic_programa = pp.ic_programa(+)" +
				"	AND pp.ic_cartera = por.ic_cartera(+)" +
				"	AND pp.ic_portafolio = por.ic_portafolio(+)" +tOp+icIf+fol+sit+rang+
				"	group by es.ic_folio "
			);
			log.info("strQuery ::: "+strQuery.toString());
			ps = con.queryPrecompilado(strQuery.toString());
			rs = ps.executeQuery();
			if(rs.next()){
				datos = new HashMap();
				datos.put("TOTAL_REG",(rs.getString("TOTAL_REG")==null)?"":rs.getString("TOTAL_REG"));
				datos.put("IC_FOLIO",(rs.getString("IC_FOLIO")==null)?"":rs.getString("IC_FOLIO"));
				listReg.add(datos);
			}
			rs.close();
			ps.close();	
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			System.out.println("Error  "+e);
		}finally{
			try {
				if (rs != null)
					rs.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("getTotalFolio():: Error al cerrar Recursos: " + t.getMessage());
			}
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getTotalFolio (S)");
		}	
		return listReg;
	 }
	 
	 public List getDetalleReg(String CLAVE_IF,String IC_SITUACION,String TipoOperacion,String folio,String fechaMin,String fechaFin ) {
		StringBuffer	sbQuery		= new StringBuffer();
		String 			sCondicion	= "";
		AccesoDB 	con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet reg = null;  
		HashMap datos = new HashMap();
		List listReg = new ArrayList(); 
		boolean		commit = true;
		try{
			con.conexionDB();
			conditions = new ArrayList();
		
			if(!folio.equals("")){
				sCondicion += " AND alt.ic_folio = ? ";
				conditions.add(folio);
			}
	/*FODEA 000 - 2009 Garantias*/
			
		
			sbQuery.append(
					" SELECT   por.cg_descripcion AS nombre_portafolio, pp.ic_programa AS programa, "   +
					" alt.in_registros_acep, alt.in_registros_rech, alt.in_registros, "+
					" 'ConsSolicNafinGAbean:getQueryPorEstatus(0)' AS pantalla"+  /*FODEA 000 - 2009 Garantias*/
				"     FROM gti_det_alta_gtia alt, " +
				"		 gti_parametros_programas pp, " +
				"		 gticat_portafolios por " +
				"    WHERE alt.ic_programa = pp.ic_programa " +
				"      AND pp.ic_cartera = por.ic_cartera " +
				"      AND pp.ic_portafolio = por.ic_portafolio " +
				sCondicion);
			System.out.println("sbQuery.toString()>>>>"+sbQuery.toString());
			System.out.println("conditions>>>>"+conditions);
			
			ps = con.queryPrecompilado(sbQuery.toString(),conditions);
			reg = ps.executeQuery();
			while(reg.next()){
				datos = new HashMap();
				datos.put("NOMBRE_PORTAFOLIO",(reg.getString("NOMBRE_PORTAFOLIO")==null)?"":reg.getString("NOMBRE_PORTAFOLIO"));
				datos.put("PROGRAMA",(reg.getString("PROGRAMA")==null)?"":reg.getString("PROGRAMA"));
				datos.put("IN_REGISTROS_ACEP",(reg.getString("IN_REGISTROS_ACEP")==null)?"":reg.getString("IN_REGISTROS_ACEP"));
				datos.put("IN_REGISTROS_RECH",(reg.getString("IN_REGISTROS_RECH")==null)?"":reg.getString("IN_REGISTROS_RECH"));
				datos.put("IN_REGISTROS",(reg.getString("IN_REGISTROS")==null)?"":reg.getString("IN_REGISTROS"));
				listReg.add(datos);
			}
			reg.close();
			ps.close();	
			
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			System.out.println("Error  "+e);
		}finally{
			try {
				if (reg != null)
					reg.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("getTotalFolio():: Error al cerrar Recursos: " + t.getMessage());
			}
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getTotalFolio (S)");
		}	
		return listReg;
	}
	
	 public List getTotalRegistro(String CLAVE_IF,String IC_SITUACION,String TipoOperacion,String folio,String fechaMin,String fechaFin ) {
		StringBuffer	sbQuery		= new StringBuffer();
		String 			sCondicion	= "";
		AccesoDB 	con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet reg = null;  
		HashMap datos = new HashMap();
		List listReg = new ArrayList(); 
		boolean		commit = true;
		try{
			con.conexionDB();
			conditions = new ArrayList();
			if(!TipoOperacion.equals("0")){
				sCondicion += " AND s.ic_tipo_operacion = ? ";
				//la variable ic_if al ser null provoca errores en : ADMIN GARANT Programa de Garantia Automatica/Recuperaciones/Consulta de Resultados de las solicitudes.
				if(CLAVE_IF !=null){ 
					conditions.add(new Integer(TipoOperacion));
				}
			}	
			if(TipoOperacion.equals("2")){
				sCondicion += " AND s.ic_situacion != 7 ";
			}
			if(!folio.equals("")){
				sCondicion += " AND es.ic_folio = ? ";
				conditions.add(folio);
			}
	/*FODEA 000 - 2009 Garantias*/
			
		 //FODEA-XX-2014
		 if(CLAVE_IF ==null){}else if(!CLAVE_IF.equals("")){    
			sCondicion += " AND i.ic_if = ? ";
			conditions.add(CLAVE_IF);
		 }
			
			
			if(!IC_SITUACION.equals("")){
				sCondicion += " AND es.ic_situacion = ? ";
				if(CLAVE_IF !=null){
					conditions.add(IC_SITUACION);
				}
		 }
		 if(!fechaMin.equals("")&&!fechaFin.equals("")){
				sCondicion += "	AND es.df_fecha_hora BETWEEN TO_DATE ('"+fechaMin+"', 'DD/MM/YYYY') AND TO_DATE ('"+fechaFin+"', 'DD/MM/YYYY') + 1" ;
				
			}
			sbQuery.append(
				" 	SELECT  "   +
				" 	es.ic_folio, "+
				" 	SUM( DECODE (alt.ic_programa,NULL, es.in_registros_acep,alt.in_registros_acep)  ) as tatol_in_registros_acep, " +
				" 	SUM( decode(alt.ic_programa, null, es.in_registros_rech, alt.IN_REGISTROS_RECH ) ) as tatol_in_registros_rech, " +
				"	SUM( decode(alt.ic_programa, null, es.in_registros, alt.in_registros ) ) as total_in_registros "+
				"  FROM gti_estatus_solic es, gticat_situacion s, comcat_if i, " +
				"	gti_det_alta_gtia alt, gti_parametros_programas pp, gticat_portafolios por " +
				"   WHERE es.ic_situacion = s.ic_situacion " +
				"   AND es.ic_tipo_operacion = s.ic_tipo_operacion " +
				"  AND es.ic_if_siag = i.ic_if_siag " +
				"	AND es.ic_folio = alt.ic_folio(+) " +
				"  AND alt.ic_programa = pp.ic_programa(+) " +
				"  AND pp.ic_cartera = por.ic_cartera(+) " +
				"  AND pp.ic_portafolio = por.ic_portafolio(+) " +
				sCondicion +
				" GROUP BY es.ic_folio");
			System.out.println("sbQuery.toString()>>>>"+sbQuery.toString());
			System.out.println("conditions>>>>"+conditions);
			
			ps = con.queryPrecompilado(sbQuery.toString(),conditions);
			reg = ps.executeQuery();
			while(reg.next()){
				datos = new HashMap();
				datos.put("IC_FOLIO",(reg.getString("IC_FOLIO")==null)?"":reg.getString("IC_FOLIO"));
				datos.put("TATOL_IN_REGISTROS_ACEP",(reg.getString("TATOL_IN_REGISTROS_ACEP")==null)?"":reg.getString("TATOL_IN_REGISTROS_ACEP"));
				datos.put("TATOL_IN_REGISTROS_RECH",(reg.getString("TATOL_IN_REGISTROS_RECH")==null)?"":reg.getString("TATOL_IN_REGISTROS_RECH"));
				datos.put("TOTAL_IN_REGISTROS",(reg.getString("TOTAL_IN_REGISTROS")==null)?"":reg.getString("TOTAL_IN_REGISTROS"));
				listReg.add(datos);
			}
			reg.close();
			ps.close();	
			
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			System.out.println("Error  "+e);
		}finally{
			try {
				if (reg != null)
					reg.close();
				if (ps != null)
					ps.close();
			} catch (Exception t) {
				log.error("getTotalFolio():: Error al cerrar Recursos: " + t.getMessage());
			}
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getTotalFolio (S)");
		}	
		return listReg;
	}
}