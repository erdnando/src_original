package com.netro.garantias;

import com.netro.pdf.ComunesPDF;
import com.netro.xlsx.ComunesXLSX;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.Fecha;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**

	Esta clase se encarga de generar los archivos de la consulta de comisiones del programa
	de la sociedad hipotecaria federal.
	La pantalla donde se utiliza esta clase es la siguiente:
		ADMIN_IF_GARAN - PROGRAMA DE GARANT�A AUTOM�TICA � TRANSACCIONES � COMISIONES � OBTENCI�N DE COMISIONES
	
	@author Jesus Salim Hernandez David
	@since  Fodea 006 - 2012 -- Garantias Programa SHF
	@date	  13/06/2012 11:07:33 a.m.
	
 */
public class ExtraccionComisionesSHF {

	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ExtraccionComisionesSHF.class);
	
	/**
	 *	Realiza la extracci&oacute;n detallada y la guarda en un archivo TXT
	 *	@throws AppException 
	 *	
	 *	@param directorio <tt>String</tt> con el path del directorio donde se guardara el archivo generado.
	 *	@param query   	<tt>HashMap</tt> con el query para realizar la consulta.
	 *	
	 *	@return <tt>String</tt> con el nombre del archivo generado.
	 */	
	public static String generaArchivoTXTDetalle( String directorio, HashMap query )
		throws AppException{
			
		log.info("generaArchivoTXTDetalle(E)");
		
		FileOutputStream		out 				= null;
		BufferedWriter 		txt 				= null;
		CreaArchivo 			archivo 			= null;
		String					nombreArchivo 	= null;
		
		AccesoDB 				con 				= new AccesoDB();
		String 					querySentencia	= null;
		List						parametros		= null;
		HashMap 					registro 		= null;
		ArrayList 				lista				= new ArrayList();
		ResultSet 				rs 				= null;
		PreparedStatement 	ps 				= null;
 
		try {
 
			// Conectarse a la base de datos
			con.conexionDB();
			
			// Obtener query
			querySentencia = (String) 	query.get("text");
			ps = con.queryPrecompilado(querySentencia);
			// Definir los parametros
			int indice  = 1;
			parametros	= (List)		query.get("parameters");	
			for(int i=0;i<parametros.size();i++){
				Object o = (Object) parametros.get(i);
				if(        o instanceof String   ){
					ps.setString(indice++, (String)   o );
				} else if( o instanceof Long     ){
					ps.setLong(indice++,   ((Long)    o ).longValue() );
				} else if( o instanceof Integer  ){
					ps.setInt(indice++,    ((Integer) o ).intValue() );
				}
			}
			// Realizar consulta
			rs = ps.executeQuery();
 
			// Crear Archivo
			archivo 			= new CreaArchivo();
			nombreArchivo	= archivo.nombreArchivo()+".txt";
			out 				= new FileOutputStream(directorio+nombreArchivo);
			txt 				= new BufferedWriter(new OutputStreamWriter(out, "Cp1252"));

			int 	i 						= 0;
			long 	cuentaRegistros	= 0L;
			while( rs.next()) {
 
				if(cuentaRegistros > 0) txt.write("\r\n");
				txt.write((rs.getString("NOMBRE")		   == null)?"":rs.getString("NOMBRE")			); txt.write("@");
				txt.write((rs.getString("CURP")		      == null)?"":rs.getString("CURP")				); txt.write("@");
				txt.write((rs.getString("GAR_CLAVE_SHF")	== null)?"":rs.getString("GAR_CLAVE_SHF")	); txt.write("@");
				txt.write((rs.getString("MONTO_CREDITO")	== null)?"":rs.getString("MONTO_CREDITO")	); txt.write("@");
				txt.write((rs.getString("INTER_CLAVE")		== null)?"":rs.getString("INTER_CLAVE")	); txt.write("@");
				txt.write((rs.getString("MONTO_COMISION")	== null)?"":rs.getString("MONTO_COMISION")); txt.write("@");
				txt.write((rs.getString("MONTO_IVA")		== null)?"":rs.getString("MONTO_IVA")		); txt.write("@");
				txt.write((rs.getString("MONTO_COBRAR")	== null)?"":rs.getString("MONTO_COBRAR")	); txt.write("@");
				txt.write((rs.getString("FECHA_PAGO")		== null)?"":rs.getString("FECHA_PAGO")		); txt.write("@");
				txt.write((rs.getString("MONTO_PAGADO")	== null)?"":rs.getString("MONTO_PAGADO")	); txt.write("@");

				// Realizar flush si es conveniente
				if((i%250) == 0){
					i = 0;
					txt.flush();
				}
				i++;
				
				// Actualizar contador de registros
				cuentaRegistros++;
		
			}
 
			// Verificar si se encontraron registros
			if( cuentaRegistros == 0 ){
				txt.write("No hay datos"+"\n");
			}
			txt.flush();
			
		}catch(OutOfMemoryError om) { // Se acabo la memoria
			
			log.error("generaArchivoTXTDetalle(OutOfMemoryError)");
			log.error("generaArchivoPDFDetalle.directorio   = <" + directorio                        + ">");
			log.error("generaArchivoPDFDetalle.query    	   = <" + query                             + ">");
			log.error("generaArchivoPDFDetalle::Free Memory = <" + Runtime.getRuntime().freeMemory() + ">");
			log.error(om.getMessage());
			om.printStackTrace();
			
			throw new AppException("Se acab� la memoria.");
			
		} catch (Exception e) { // Ocurrio una excepcion
			
			log.error("generaArchivoTXTDetalle(Exception)");
			log.error("generaArchivoPDFDetalle.directorio  = <" + directorio  + ">");
			log.error("generaArchivoPDFDetalle.query    	  = <" + query       + ">");
			e.printStackTrace();
			throw new AppException("Ocurri� un error al generar el Archivo TXT con el detalle de las Comisiones.");
			
		} finally {
			
			if(rs  != null ){ try { rs.close(); }catch(Exception e){} }
			if(ps  != null ){ try { ps.close(); }catch(Exception e){} }
			
			// Cerrar archivo
			if(txt != null ){	try { txt.close();}catch(Exception e){} }
			
			// Cerrar conexion con la base de datos
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(true); // Debido a que se trata de una tabla remota
				con.cierraConexionDB();
			}
			log.info("generaArchivoTXTDetalle(S)");
		}
		
		
		return nombreArchivo;
		
	}
	
	/**
	 *	Realiza la extracci&oacute;n consolidada y la guarda en un archivo PDF
	 *	@throws AppException
	 *	
	 *	@param directorio 	<tt>String</tt> con el path del directorio donde se guardara el archivo generado.
	 *	@param query   		<tt>HashMap</tt> con el query para realizar la consulta.
	 *	@param cabecera   	<tt>HashMap</tt> con la informacion de cabcera del archivo PDF.
	 *	@param queryTotales  <tt>HashMap</tt> con el query para realizar la consulta de los totales.
	 *	
	 *	@return <tt>String</tt> con el nombre del archivo generado.
	 */
	 public static String generaArchivoPDFDetalle( String directorio, HashMap query, HashMap queryTotales, HashMap cabecera )
		throws AppException{
		
		log.info("generaArchivoPDFDetalle(E)");
		
		String 					nombreArchivo 	= null;
		CreaArchivo 			archivo 			= null;
		ComunesPDF 				documentoPDF 	= null;
		
		AccesoDB 				con 				= new AccesoDB();
		String 					querySentencia	= null;
		List						parametros		= null;
		HashMap 					registro 		= null;
		ArrayList 				lista				= new ArrayList();
		ResultSet 				rs 				= null;
		PreparedStatement 	ps 				= null;
		
		try {
			
			// Conectarse a la base de datos
			con.conexionDB();
 
			// Crear archivo PDF
			archivo 			= new CreaArchivo();
			nombreArchivo 	= archivo.nombreArchivo()+".pdf";
			documentoPDF 	= new ComunesPDF(2,directorio+nombreArchivo);
 					  
			// Definir el Encabezado
			documentoPDF.setTable(3,100,new float[]{23f,54f,23f});    
			documentoPDF.setCellImage( (String) cabecera.get("LOGO_NAFIN"),               ComunesPDF.LEFT,   173   );    
			documentoPDF.setCell(      (String) cabecera.get("DATOS_PROGRAMA"),"formasB", ComunesPDF.CENTER, 1,1,0 );//El parametro 0, es para que la celda no tenga borde.    
			documentoPDF.setCellImage( (String) cabecera.get("LOGO_SHF"),                 ComunesPDF.RIGHT,  173   );
			documentoPDF.addTable();
 
			// Datos del Usuario
			documentoPDF.setTable(4,100,new float[]{10f,67f,15f,8f});
			documentoPDF.setCell("USUARIO: ", 											"formasrepB",	ComunesPDF.LEFT, 1,1,0);
			documentoPDF.setCell((String) cabecera.get("NOMBRE_USUARIO"), 		"formasrep", 	ComunesPDF.LEFT, 1,1,0);
			documentoPDF.setCell("FECHA PROCESO: ", 									"formasrepB",  ComunesPDF.RIGHT,1,1,0); 
			documentoPDF.setCell(Fecha.getFechaActual().replaceAll("/","-"), 	"formasrep",   ComunesPDF.LEFT, 1,1,0); 
			
			documentoPDF.setCell("PRODUCTO: ",											"formasrepB", 	ComunesPDF.LEFT, 1,1,0);
			documentoPDF.setCell("REPORTE DE COMISIONES",							"formasrep", 	ComunesPDF.LEFT, 1,1,0); 
			documentoPDF.setCell("", 														"formasrep", 	ComunesPDF.LEFT, 1,1,0);
			documentoPDF.setCell("", 														"formasrep",	ComunesPDF.LEFT, 1,1,0);
			
			documentoPDF.addTable();
			
			//Agregar l�nea de texto vac�a
			documentoPDF.addText("\n",														"formas",	 ComunesPDF.CENTER);
			
			// Agregar Cabecera de Comisiones
			documentoPDF.addText("COMISIONES",											"formasG",	 ComunesPDF.CENTER);
			
			documentoPDF.setTable(2,100,new float[]{10f,90f});
			documentoPDF.setCell("Intermediario: ", 																					"formasrepB",  ComunesPDF.LEFT,1,1,0);
			documentoPDF.setCell(cabecera.get("NUM_NAFIN_ELECTRONICO")+ "              "+ cabecera.get("NOMBRE"), "formasrep", 	ComunesPDF.LEFT,1,1,0);
			documentoPDF.addTable();
 
			//Agregar l�nea de texto vac�a
			documentoPDF.addText("\n",														"formas",	 ComunesPDF.CENTER);
			
			// Tabla de Datos
			
			// Localidad, fecha y hora
			/*
			String meses[] 		= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaAct 		= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    	= fechaAct.substring(0,1).equals("0")?fechaAct.substring(1,2):fechaAct.substring(0,2);
			String mesActual    	= meses[Integer.parseInt(fechaAct.substring(3,5))-1];
			String anioActual   	= fechaAct.substring(6,10);
			String horaActual  	= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			documentoPDF.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+"                               "+horaActual+"             ","formas",ComunesPDF.RIGHT);
			documentoPDF.addText(" ","formas",ComunesPDF.RIGHT);
			documentoPDF.addText(" ","formas",ComunesPDF.RIGHT);
			*/
						
			// Obtener query
			querySentencia = (String) 	query.get("text");
			ps = con.queryPrecompilado(querySentencia);
			// Definir los parametros
			int indice  = 1;
			parametros	= (List)		query.get("parameters");	
			for(int i=0;i<parametros.size();i++){
				Object o = (Object) parametros.get(i);
				if(        o instanceof String   ){
					ps.setString(indice++, (String)   o );
				} else if( o instanceof Long     ){
					ps.setLong(indice++,   ((Long)    o ).longValue() );
				} else if( o instanceof Integer  ){
					ps.setInt(indice++,    ((Integer) o ).intValue() );
				}
			}
			// Realizar consulta
			rs = ps.executeQuery();
			
			// Definir cabecera de la tabla de datos
			documentoPDF.setLTable(9,100,new float[]{10f,9.4f,15.3f,10f,10f,10f,10f,5.3f,10f});
			// Agregar cabecera de la tabla
			documentoPDF.setLCell("Raz�n Social del Acreditado",	"celda01repBSmall",ComunesPDF.CENTER,1,1,1);
			documentoPDF.setLCell("CURP",									"celda01repBSmall",ComunesPDF.CENTER,1,1,1);
			documentoPDF.setLCell("Clave Financiamiento",			"celda01repBSmall",ComunesPDF.CENTER,1,1,1);
			documentoPDF.setLCell("Monto Financiamiento",			"celda01repBSmall",ComunesPDF.CENTER,1,1,1);
			documentoPDF.setLCell("Monto Comisi�n a Cobrar",		"celda01repBSmall",ComunesPDF.CENTER,1,1,1);
			documentoPDF.setLCell("Monto IVA",							"celda01repBSmall",ComunesPDF.CENTER,1,1,1);
			documentoPDF.setLCell("Total Monto a Cobrar",			"celda01repBSmall",ComunesPDF.CENTER,1,1,1);
			documentoPDF.setLCell("Fecha Pago de Comisi�n",			"celda01repBSmall",ComunesPDF.CENTER,1,1,1);
			documentoPDF.setLCell("Monto Pagado a la SHF",			"celda01repBSmall",ComunesPDF.CENTER,1,1,1);
				
			// Extraer datos
			boolean hayDatos = false;
			while( rs.next() ){
 
				String nombre 				= (rs.getString("NOMBRE")				== null)?"":rs.getString("NOMBRE");
				String curp 				= (rs.getString("CURP")					== null)?"":rs.getString("CURP");
				String garClaveShf 		= (rs.getString("GAR_CLAVE_SHF")		== null)?"":rs.getString("GAR_CLAVE_SHF");
				String montoCredito 		= (rs.getString("MONTO_CREDITO")		== null)?"":rs.getString("MONTO_CREDITO");
				String montoComision 	= (rs.getString("MONTO_COMISION")	== null)?"":rs.getString("MONTO_COMISION");
				String montoIva 			= (rs.getString("MONTO_IVA")			== null)?"":rs.getString("MONTO_IVA");
				String montoCobrar 		= (rs.getString("MONTO_COBRAR")		== null)?"":rs.getString("MONTO_COBRAR");
				String fechaPago 			= (rs.getString("FECHA_PAGO")			== null)?"":rs.getString("FECHA_PAGO");
				String montoPagado 		= (rs.getString("MONTO_PAGADO")		== null)?"":rs.getString("MONTO_PAGADO");
				
				montoCredito				= "".equals(montoCredito)	?"":Comunes.formatoDecimal(montoCredito,	2,true);
				montoComision				= "".equals(montoComision)	?"":Comunes.formatoDecimal(montoComision,	2,true);
				montoIva						= "".equals(montoIva)		?"":Comunes.formatoDecimal(montoIva,		2,true);
				montoCobrar					= "".equals(montoCobrar)	?"":Comunes.formatoDecimal(montoCobrar,	2,true);
				montoPagado					= "".equals(montoPagado)	?"":Comunes.formatoDecimal(montoPagado,	2,true);
				
				// Llenar tabla
				documentoPDF.setLCell(nombre, 			"formasrepNSmall",ComunesPDF.CENTER,1,1,1);
				documentoPDF.setLCell(curp, 				"formasrepNSmall",ComunesPDF.CENTER,1,1,1);
				documentoPDF.setLCell(garClaveShf, 		"formasrepNSmall",ComunesPDF.CENTER,1,1,1);
				documentoPDF.setLCell(montoCredito, 	"formasrepNSmall",ComunesPDF.CENTER,1,1,1);
				documentoPDF.setLCell(montoComision, 	"formasrepNSmall",ComunesPDF.CENTER,1,1,1);
				documentoPDF.setLCell(montoIva, 			"formasrepNSmall",ComunesPDF.CENTER,1,1,1);
				documentoPDF.setLCell(montoCobrar, 		"formasrepNSmall",ComunesPDF.CENTER,1,1,1);
				documentoPDF.setLCell(fechaPago, 		"formasrepNSmall",ComunesPDF.CENTER,1,1,1);
				documentoPDF.setLCell(montoPagado, 		"formasrepNSmall",ComunesPDF.CENTER,1,1,1);
 
				hayDatos = true;
				
			}
			
			// Agregar los totales
			if(hayDatos){
				
				rs.close();
				ps.close();
				con.terminaTransaccion(true); // Debido a que se trata de una tabla remota
				
				// Obtener query totales
				querySentencia = (String) 	queryTotales.get("text");
				ps = con.queryPrecompilado(querySentencia);
				// Definir los parametros
				indice  = 1;
				parametros	= (List)		query.get("parameters");	
				for(int i=0;i<parametros.size();i++){
					Object o = (Object) parametros.get(i);
					if(        o instanceof String   ){
						ps.setString(indice++, (String)   o );
					} else if( o instanceof Long     ){
						ps.setLong(indice++,   ((Long)    o ).longValue() );
					} else if( o instanceof Integer  ){
						ps.setInt(indice++,    ((Integer) o ).intValue() );
					}
				}
				// Realizar consulta
				rs = ps.executeQuery();
			
				if(rs.next()){
					
					String totalMontoCredito 	= (rs.getString("TOTAL_MONTO_CREDITO")		== null)?"":rs.getString("TOTAL_MONTO_CREDITO");
					String totalMontoComision	= (rs.getString("TOTAL_MONTO_COMISION")	== null)?"":rs.getString("TOTAL_MONTO_COMISION");
					String totalMontoIva			= (rs.getString("TOTAL_MONTO_IVA")			== null)?"":rs.getString("TOTAL_MONTO_IVA");
					String totalMontoCobrar		= (rs.getString("TOTAL_MONTO_COBRAR")		== null)?"":rs.getString("TOTAL_MONTO_COBRAR");
					String totalMontoPagado		= (rs.getString("TOTAL_MONTO_PAGADO")		== null)?"":rs.getString("TOTAL_MONTO_PAGADO");
					
					totalMontoCredito 			= "".equals(totalMontoCredito)	?"":Comunes.formatoDecimal(totalMontoCredito,	2,true);
					totalMontoComision			= "".equals(totalMontoComision)	?"":Comunes.formatoDecimal(totalMontoComision,	2,true);
					totalMontoIva					= "".equals(totalMontoIva)			?"":Comunes.formatoDecimal(totalMontoIva,			2,true);
					totalMontoCobrar				= "".equals(totalMontoCobrar)		?"":Comunes.formatoDecimal(totalMontoCobrar,		2,true);
					totalMontoPagado				= "".equals(totalMontoPagado)		?"":Comunes.formatoDecimal(totalMontoPagado,		2,true);
					
					documentoPDF.setLCell("", 							"formasrepNSmall",	ComunesPDF.CENTER,2,1,1);
					documentoPDF.setLCell("TOTALES", 				"celda01repBSmall",	ComunesPDF.CENTER,1,1,1);
					documentoPDF.setLCell(totalMontoCredito, 		"formasrepNSmall",	ComunesPDF.CENTER,1,1,1);
					documentoPDF.setLCell(totalMontoComision, 	"formasrepNSmall",	ComunesPDF.CENTER,1,1,1);
					documentoPDF.setLCell(totalMontoIva, 			"formasrepNSmall",	ComunesPDF.CENTER,1,1,1);
					documentoPDF.setLCell(totalMontoCobrar, 		"formasrepNSmall",	ComunesPDF.CENTER,1,1,1);
					documentoPDF.setLCell("", 							"formasrepNSmall",	ComunesPDF.CENTER,1,1,1);
					documentoPDF.setLCell(totalMontoPagado,		"formasrepNSmall",	ComunesPDF.CENTER,1,1,1);
					
				} 
					
				
			}
			
			// No hay registros
			if(!hayDatos){ 
				documentoPDF.setLCell("No se encontr� ning�n registro.","celda01repBSmall",ComunesPDF.CENTER,9,1,1);
			}
 
			// Finalizar tabla agregada
			documentoPDF.addLTable();
			
			// Terminar documento
			documentoPDF.endDocument();
			
		} catch(OutOfMemoryError om) { // Se acabo la memoria
			
			log.error("generaArchivoPDFDetalle(OutOfMemoryError)");
			log.error("generaArchivoPDFDetalle.directorio   = <" + directorio   + ">");
			log.error("generaArchivoPDFDetalle.query    	   = <" + query        + ">");
			log.error("generaArchivoPDFDetalle.queryTotales = <" + queryTotales + ">");
			log.error("generaArchivoPDFDetalle.cabecera     = <" + cabecera     + ">");
			log.error("generaArchivoPDFDetalle::Free Memory = <" + Runtime.getRuntime().freeMemory() + ">");
			log.error(om.getMessage());
			om.printStackTrace();
			
			throw new AppException("Se acab� la memoria.");
			
		} catch (Exception e) { // Ocurrio una excepcion
			
			log.error("generaArchivoPDFDetalle(Exception)");
			log.error("generaArchivoPDFDetalle.directorio   = <" + directorio   + ">");
			log.error("generaArchivoPDFDetalle.query    	   = <" + query        + ">");
			log.error("generaArchivoPDFDetalle.queryTotales = <" + queryTotales + ">");
			log.error("generaArchivoPDFDetalle.cabecera     = <" + cabecera     + ">");
			e.printStackTrace();
			throw new AppException("Ocurri� un error al generar el Archivo PDF con el detalle de las Comisiones.");
			
		} finally {
			
			if(rs  != null ){ try { rs.close(); }catch(Exception e){} }
			if(ps  != null ){ try { ps.close(); }catch(Exception e){} }
			
			// Cerrar conexion con la base de datos
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(true); // Debido a que se trata de una tabla remota
				con.cierraConexionDB();
			}
			log.info("generaArchivoPDFDetalle(S)");
			
		}
 
		return nombreArchivo;	
		
	}
 
	/**
	 *	Realiza la extracci&oacute;n consolidada y la guarda en un archivo XLS
	 *	@throws AppException
	 *	
	 *	@param directorio 	<tt>String</tt> con el path del directorio temporal donde se guardara el archivo generado.
	 *	@param query   		<tt>HashMap</tt> con el query para realizar la consulta.
	 *	@param cabecera   	<tt>HashMap</tt> con la informacion de cabcera del archivo XLS.
	 *	@param queryTotales  <tt>HashMap</tt> con el query para realizar la consulta de los totales.
	 *	@param directorioPlantilla 	<tt>String</tt> con el path del directorio donde se encuentra la plantilla XLSX.
	 *	
	 *	@return <tt>String</tt> con el nombre del archivo generado.
	 */
	public static String generaArchivoXLSXDetalle( String directorio, HashMap query, HashMap queryTotales, HashMap cabecera, String directorioPlantilla )
		throws AppException {
		
		log.info("generaArchivoXLSDetalle(E)");
 
		AccesoDB 				con 				= new AccesoDB();
		String 					querySentencia	= null;
		List						parametros		= null;
		HashMap 					registro 		= null;
		ResultSet 				rs 				= null;
		PreparedStatement 	ps 				= null;
		
		ComunesXLSX				documentoXLSX 	= null;
		Writer					writer			= null;
		String 					nombreArchivo 	= null;
		
		// Estilos en la plantilla xlsx
		HashMap 					estilos			= new HashMap();
		estilos.put("PERCENT",			"1");
		estilos.put("COEFF",				"2");
		estilos.put("CURRENCY",			"3");
		estilos.put("DATE",				"4");
		estilos.put("HEADER",			"5");
		estilos.put("CSTRING",			"6");
		estilos.put("TITLE",				"7");
		estilos.put("SUBTITLE",			"8");
		estilos.put("RIGHTCSTRING",	"9");
		estilos.put("CENTERCSTRING",	"10");
		
		try {
			
			// Conectarse a la base de datos
			con.conexionDB();
 
			// Crear archivo XLSX
			documentoXLSX 	= new ComunesXLSX( directorio, estilos );
			
			// Crear Detalle Comisiones
			writer 			= documentoXLSX.creaHoja();
			documentoXLSX.setLongitudesColumna(new String[]{"63.42578125","25.7109375","35.140625","23.7109375","15.7109375","23.7109375","23.7109375","23.7109375","22.7109375","23.7109375"});
			
 			short	indiceRenglon 	= 0;
 			short	indiceCelda		= 0;
 			
			// Definir el Encabezado PROGRAMA
			documentoXLSX.agregaRenglon(indiceRenglon++); 
			indiceCelda		= 0;
			documentoXLSX.agregaCelda(indiceCelda++, (String) cabecera.get("PROGRAMA"), 					"SUBTITLE", 10 );
			documentoXLSX.finalizaRenglon();
			
			// Definir el Encabezado DESCRIPCION_PROGRAMA
			documentoXLSX.agregaRenglon(indiceRenglon++); 
			indiceCelda		= 0;
			documentoXLSX.agregaCelda(indiceCelda++, (String) cabecera.get("DESCRIPCION_PROGRAMA"), 	"SUBTITLE", 10 );
			documentoXLSX.finalizaRenglon();
			// Definir el Encabezado ESQUEMA
			documentoXLSX.agregaRenglon(indiceRenglon++); 
			indiceCelda		= 0;
			documentoXLSX.agregaCelda(indiceCelda++, (String) cabecera.get("ESQUEMA"), 					"SUBTITLE", 10 );
			documentoXLSX.finalizaRenglon();
			// Definir el Encabezado CREDITOS
			documentoXLSX.agregaRenglon(indiceRenglon++);
			indiceCelda		= 0;
			documentoXLSX.agregaCelda(indiceCelda++, (String) cabecera.get("CREDITOS"), 					"SUBTITLE", 10 );
			documentoXLSX.finalizaRenglon();
			// Definir el Encabezado COMISIONES
			documentoXLSX.agregaRenglon(indiceRenglon++);
			indiceCelda		= 0;
			documentoXLSX.agregaCelda(indiceCelda++, "COMISIONES", 												"TITLE", 	10 );
			documentoXLSX.finalizaRenglon();
			
			// Tabla de Datos
 			
			// Obtener query
			querySentencia = (String) 	query.get("text");
			ps = con.queryPrecompilado(querySentencia);
			// Definir los parametros
			int indice  = 1;
			parametros	= (List)		query.get("parameters");	
			for(int i=0;i<parametros.size();i++){
				Object o = (Object) parametros.get(i);
				if(        o instanceof String   ){
					ps.setString(indice++, (String)   o );
				} else if( o instanceof Long     ){
					ps.setLong(indice++,   ((Long)    o ).longValue() );
				} else if( o instanceof Integer  ){
					ps.setInt(indice++,    ((Integer) o ).intValue() );
				}
			}
			// Realizar consulta
			rs = ps.executeQuery();
			
			// Definir cabecera de la tabla de datos
			documentoXLSX.agregaRenglon(indiceRenglon++); 
			indiceCelda		= 0;
			documentoXLSX.agregaCelda(indiceCelda++,"Nombre o Raz�n Social del Acreditado",	"HEADER" );
			documentoXLSX.agregaCelda(indiceCelda++,"CURP", 											"HEADER" );
			documentoXLSX.agregaCelda(indiceCelda++,"Clave del Financiamiento",					"HEADER" );
			documentoXLSX.agregaCelda(indiceCelda++,"Monto del Financiamiento",					"HEADER" );
			documentoXLSX.agregaCelda(indiceCelda++,"Clave del Intermediario Financiero",		"HEADER" );
			documentoXLSX.agregaCelda(indiceCelda++,"Monto Comisi�n a Cobrar",					"HEADER" );
			documentoXLSX.agregaCelda(indiceCelda++,"Monto I.V.A.",									"HEADER" );
			documentoXLSX.agregaCelda(indiceCelda++,"Total Monto a Cobrar",						"HEADER" );
			documentoXLSX.agregaCelda(indiceCelda++,"Fecha de Pago de la Comisi�n",				"HEADER" );
			documentoXLSX.agregaCelda(indiceCelda++,"Monto Pagado a la SHF",						"HEADER" );
			documentoXLSX.finalizaRenglon();
			
			// Extraer datos
			boolean 	hayDatos 		= false;
			int		ctaRegistros	= 0;
			while( rs.next()){
 
				String nombre 				= (rs.getString("NOMBRE")				== null)?"":rs.getString("NOMBRE");
				String curp 				= (rs.getString("CURP")					== null)?"":rs.getString("CURP");
				String garClaveShf 		= (rs.getString("GAR_CLAVE_SHF")		== null)?"":rs.getString("GAR_CLAVE_SHF");
				String montoCredito 		= (rs.getString("MONTO_CREDITO")		== null)?"":rs.getString("MONTO_CREDITO");
				String claveIFSIAG 		= (rs.getString("CLAVE_IF_SIAG")		== null)?"":rs.getString("CLAVE_IF_SIAG");
				String montoComision 	= (rs.getString("MONTO_COMISION")	== null)?"":rs.getString("MONTO_COMISION");
				String montoIva 			= (rs.getString("MONTO_IVA")			== null)?"":rs.getString("MONTO_IVA");
				String montoCobrar 		= (rs.getString("MONTO_COBRAR")		== null)?"":rs.getString("MONTO_COBRAR");
				String fechaPago 			= (rs.getString("FECHA_PAGO")			== null)?"":rs.getString("FECHA_PAGO");
				String montoPagado 		= (rs.getString("MONTO_PAGADO")		== null)?"":rs.getString("MONTO_PAGADO");
				
				montoCredito				= "".equals(montoCredito)	?"":Comunes.formatoDecimal(montoCredito,	2,true);
				montoComision				= "".equals(montoComision)	?"":Comunes.formatoDecimal(montoComision,	2,true);
				montoIva						= "".equals(montoIva)		?"":Comunes.formatoDecimal(montoIva,		2,true);
				montoCobrar					= "".equals(montoCobrar)	?"":Comunes.formatoDecimal(montoCobrar,	2,true);
				montoPagado					= "".equals(montoPagado)	?"":Comunes.formatoDecimal(montoPagado,	2,true);
				
				// Llenar tabla
				documentoXLSX.agregaRenglon(indiceRenglon++); 
				indiceCelda					= 0;
				documentoXLSX.agregaCelda(indiceCelda++,nombre,				"CSTRING"			);
				documentoXLSX.agregaCelda(indiceCelda++,curp,				"CSTRING"			);
				documentoXLSX.agregaCelda(indiceCelda++,garClaveShf,		"CENTERCSTRING"	);
				documentoXLSX.agregaCelda(indiceCelda++,montoCredito,		"RIGHTCSTRING"		);
				documentoXLSX.agregaCelda(indiceCelda++,claveIFSIAG,		"CENTERCSTRING"	);
				documentoXLSX.agregaCelda(indiceCelda++,montoComision,	"RIGHTCSTRING"		);
				documentoXLSX.agregaCelda(indiceCelda++,montoIva,			"RIGHTCSTRING"		);
				documentoXLSX.agregaCelda(indiceCelda++,montoCobrar,		"RIGHTCSTRING"		);
				documentoXLSX.agregaCelda(indiceCelda++,fechaPago,			"CENTERCSTRING"	);
				documentoXLSX.agregaCelda(indiceCelda++,montoPagado,		"RIGHTCSTRING"		);
				documentoXLSX.finalizaRenglon();
				
				if( ctaRegistros % 250 == 0 ){
					documentoXLSX.flush();
					ctaRegistros = 1;
				}
				
				ctaRegistros++;
				hayDatos = true;
				
			}
			
			// Agregar los totales
			if(hayDatos){
				
				rs.close();
				ps.close();
				con.terminaTransaccion(true); // Debido a que se trata de una tabla remota
				
				// Obtener query totales
				querySentencia = (String) 	queryTotales.get("text");
				ps = con.queryPrecompilado(querySentencia);
				// Definir los parametros
				indice  = 1;
				parametros	= (List)		query.get("parameters");	
				for(int i=0;i<parametros.size();i++){
					Object o = (Object) parametros.get(i);
					if(        o instanceof String   ){
						ps.setString(indice++, (String)   o );
					} else if( o instanceof Long     ){
						ps.setLong(indice++,   ((Long)    o ).longValue() );
					} else if( o instanceof Integer  ){
						ps.setInt(indice++,    ((Integer) o ).intValue() );
					}
				}
				// Realizar consulta
				rs = ps.executeQuery();
			
				if(rs.next()){
					
					String totalMontoCredito 	= (rs.getString("TOTAL_MONTO_CREDITO")		== null)?"":rs.getString("TOTAL_MONTO_CREDITO");
					String totalMontoComision	= (rs.getString("TOTAL_MONTO_COMISION")	== null)?"":rs.getString("TOTAL_MONTO_COMISION");
					String totalMontoIva			= (rs.getString("TOTAL_MONTO_IVA")			== null)?"":rs.getString("TOTAL_MONTO_IVA");
					String totalMontoCobrar		= (rs.getString("TOTAL_MONTO_COBRAR")		== null)?"":rs.getString("TOTAL_MONTO_COBRAR");
					String totalMontoPagado		= (rs.getString("TOTAL_MONTO_PAGADO")		== null)?"":rs.getString("TOTAL_MONTO_PAGADO");
					
					totalMontoCredito 			= "".equals(totalMontoCredito)	?"":Comunes.formatoDecimal(totalMontoCredito,	2,true);
					totalMontoComision			= "".equals(totalMontoComision)	?"":Comunes.formatoDecimal(totalMontoComision,	2,true);
					totalMontoIva					= "".equals(totalMontoIva)			?"":Comunes.formatoDecimal(totalMontoIva,			2,true);
					totalMontoCobrar				= "".equals(totalMontoCobrar)		?"":Comunes.formatoDecimal(totalMontoCobrar,		2,true);
					totalMontoPagado				= "".equals(totalMontoPagado)		?"":Comunes.formatoDecimal(totalMontoPagado,		2,true);
					
					documentoXLSX.agregaRenglon(indiceRenglon++); 
					indiceCelda		= 0;
					documentoXLSX.agregaCelda(indiceCelda++,"",						"CSTRING",   2	);
					indiceCelda++;
					documentoXLSX.agregaCelda(indiceCelda++,"TOTALES",				"HEADER"			);
					documentoXLSX.agregaCelda(indiceCelda++,totalMontoCredito,	"RIGHTCSTRING"	);
					documentoXLSX.agregaCelda(indiceCelda++,"",						"CSTRING"		);
					documentoXLSX.agregaCelda(indiceCelda++,totalMontoComision,	"RIGHTCSTRING"	);
					documentoXLSX.agregaCelda(indiceCelda++,totalMontoIva,		"RIGHTCSTRING"	);
					documentoXLSX.agregaCelda(indiceCelda++,totalMontoCobrar,	"RIGHTCSTRING"	);
					documentoXLSX.agregaCelda(indiceCelda++,"",						"CSTRING"		);
					documentoXLSX.agregaCelda(indiceCelda++,totalMontoPagado,	"RIGHTCSTRING"	);
					documentoXLSX.finalizaRenglon();
					
				} 
 
			}
			
			// No hay registros
			if(!hayDatos){
				
				documentoXLSX.agregaRenglon(indiceRenglon++); 
				indiceCelda		= 0;
				documentoXLSX.agregaCelda(indiceCelda++,"No se encontr� ning�n registro.","HEADER",10);
				documentoXLSX.finalizaRenglon();
				
			}
			
			// Finalizar Hoja
			documentoXLSX.finalizaHoja(); 
			
			// Terminar Documento: Crear en directorioTemporal archivo XLSX con la hoja generada usando la plantilla: 29ObtenerComisiones01ext.template.xlsx
			nombreArchivo = documentoXLSX.finalizaDocumento( directorioPlantilla+"29ObtenerComisiones01ext.template.xlsx" );
 
		} catch(OutOfMemoryError om) { // Se acabo la memoria
			
			log.error("generaArchivoXLSDetalle(OutOfMemoryError)");
			log.error("generaArchivoXLSDetalle.directorio   = <" + directorio   + ">");
			log.error("generaArchivoXLSDetalle.query    	   = <" + query        + ">");
			log.error("generaArchivoXLSDetalle.queryTotales = <" + queryTotales + ">");
			log.error("generaArchivoXLSDetalle.cabecera     = <" + cabecera     + ">");
			log.error("generaArchivoXLSDetalle::Free Memory = <" + Runtime.getRuntime().freeMemory() + ">");
			log.error(om.getMessage());
			om.printStackTrace();
			
			if(writer  	!= null ){ try { writer.close(); }catch(Exception e){} }
			
			throw new AppException("Se acab� la memoria.");
			
		} catch (Exception e) { // Ocurrio una excepcion
			
			log.error("generaArchivoXLSDetalle(Exception)");
			log.error("generaArchivoXLSDetalle.directorio   = <" + directorio   + ">");
			log.error("generaArchivoXLSDetalle.query    	   = <" + query        + ">");
			log.error("generaArchivoXLSDetalle.queryTotales = <" + queryTotales + ">");
			log.error("generaArchivoXLSDetalle.cabecera     = <" + cabecera     + ">");
			e.printStackTrace();
			
			if(writer  	!= null ){ try { writer.close(); }catch(Exception exception){} }
			
			throw new AppException("Ocurri� un error al generar el Archivo XLS con el detalle de las Comisiones.");
			
		} finally {
			
			if(rs  		!= null ){ try { rs.close(); 		}catch(Exception e){} }
			if(ps  		!= null ){ try { ps.close(); 		}catch(Exception e){} }
 
			// Cerrar conexion con la base de datos
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(true); // Debido a que se trata de una tabla remota
				con.cierraConexionDB();
			}
			log.info("generaArchivoXLSDetalle(S)");
			
		}
 
		return nombreArchivo;		
		
	}
 
}
