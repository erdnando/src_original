package com.netro.garantias;

import com.netro.model.catalogos.CatalogoSimple;
import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.math.BigDecimal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.sf.json.JSONArray;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Fecha;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 *
 *	Esta clase se encarga de generar el archivo PDF del acuse de la carga masiva del alta de reinstalaciones
 * de garant�as, as� como tambi�n, el archivo CSV con el detalle de los errores de la carga y el archivo
 * con los registros que fueron cargados exitosamente.
 *
 * Esta clase se ocupa en la siguiente pantalla:
 *
 *		ADMIN IF GARANT - PROGRAMA DE GARANT�A AUTOM�TICA - SERVICIOS DE TRANSACCI�N - REINSTALACI�N DE GARANT�AS - CARGA ARCHIVO
 *	
 * @author Salim Hernandez ( 20/03/2013 03:05:21 p.m. )
 * @since  F007 - 2013
 *	
 */
public class CargaArchivoAltaReinstalaciones {
	
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(CargaArchivoAltaReinstalaciones.class);

	/**
		Crea un archivo PDF con la informaci�n del acuse de la carga
		@throws AppException 
		
		@param rg 												<tt>ResultadosGar</tt> con el resultado de la transmision de solicitudes.
		@param strDirectorioTemp   						<tt>String</tt> con la ruta del directorio temporal donde se creara el archivo PDF.
		@param cabecera       								<tt>HashMap</tt> con la informacion de la cabcera del PDF.
		@param strDirectorioPublicacion 					<tt>String</tt> con la ruta WEB del directorio de publicacion.
		@param totalRegistros							   <tt>String</tt> con el numero total de registros a transmitidos.
		@param montoTotal									   <tt>String</tt> con el monto total de la recuperaci�n de importes.
		@param loginUsuario  								<tt>String</tt> con el login del usuario firmado en el sistema.
		@param nombreUsuario   								<tt>String</tt> con el nombre del usuario firmado en el sistema.
 
		@return <tt>String</tt> con el nombre del archivo generado.
	*/	
	public static String generaAcuseArchivoPDF(
			ResultadosGar 	rg,
			String 			strDirectorioTemp,
			HashMap			cabecera,
			String 			strDirectorioPublicacion,
			String			totalRegistros,
			String 			montoTotal,
			String 			loginUsuario,
			String 			nombreUsuario
	) throws AppException{
 
		log.info("generaAcuseArchivoPDF(E)");
		
		String 		nombreArchivo 	= null;
		CreaArchivo archivo 			= null;
		ComunesPDF 	pdfDoc 			= null;
		
		try {
			
			archivo 						= new CreaArchivo();
			nombreArchivo 				= archivo.nombreArchivo()+".pdf";
	 
			// Obtener Descripci�n del Tipo de Operaci�n
			String 				tipoOperacion 	= "";
			// Preparar consulta
			CatalogoSimple 	cat 				= new CatalogoSimple();
			cat.setTabla("gticat_tipooperacion");
			cat.setCampoClave("ic_tipo_operacion");
			cat.setCampoDescripcion("cg_descripcion");
			List claves = new ArrayList();
			claves.add("10");	//10= Reinstalaciones de Garant�as
			cat.setValoresCondicionIn(claves);
			// Consultar descripcion del Tipo de Operacion 10.
			List listaResultados = cat.getListaElementos();
			if (listaResultados!=null && listaResultados.size()>0) {
				tipoOperacion = Comunes.rellenaCeros(((ElementoCatalogo)listaResultados.get(0)).getClave(),2) + " " + ((ElementoCatalogo)listaResultados.get(0)).getDescripcion();
			}
		
			pdfDoc 						= new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
			
			String 	meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String 	fechaActual  	= Fecha.getFechaActual();
			String 	diaActual   	= fechaActual.substring(0,2);
			String 	mesActual    	= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String 	anioActual   	= fechaActual.substring(6,10);
			String 	horaActual  	= Fecha.getHoraActual("HH24:MI:SS");
 
			pdfDoc.encabezadoConImagenes(
						pdfDoc,
						(String) cabecera.get("strPais"),
						(String) cabecera.get("iNoNafinElectronico"),
						(String) cabecera.get("sesExterno"),
						(String) cabecera.get("strNombre"),
						(String) cabecera.get("strNombreUsuario"),
						(String) cabecera.get("strLogo"),
						strDirectorioPublicacion
					);
 
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
			pdfDoc.addText("Su solicitud fue recibida con el n�mero de folio: "+rg.getFolio(),"formas",ComunesPDF.CENTER);
			pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
			
			pdfDoc.setTable(2,70);
			
			pdfDoc.setCell("CIFRAS CONTROL",										"celda03",	ComunesPDF.CENTER,2,1,1);
			
			pdfDoc.setCell("Tipo de Operaci�n",									"celda02",	ComunesPDF.CENTER);// Tipo de Operaci�n
			pdfDoc.setCell(tipoOperacion,											"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.setCell("No. total de registros transmitidos",							"celda02",	ComunesPDF.CENTER);// No. total de registros
			pdfDoc.setCell(Comunes.formatoDecimal(totalRegistros,0),		"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.setCell("Monto total de los registros transmitidos",	"celda02",	ComunesPDF.CENTER);// Monto total de los registros transmitidos
			pdfDoc.setCell(Comunes.formatoDecimal(montoTotal,2),			"formas",	ComunesPDF.RIGHT);
 
			pdfDoc.setCell("Fecha de carga",										"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(rg.getFecha(),											"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.setCell("Hora de carga",										"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(rg.getHora(),											"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.setCell("Fecha valor",											"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(rg.getFechaValor(),									"formas",	ComunesPDF.RIGHT);
	
			pdfDoc.setCell("Usuario",												"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(loginUsuario+" - "+nombreUsuario,				"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.addTable();
			pdfDoc.endDocument();
		
		} catch(Exception e) {
			
			log.error("generaAcuseArchivoPDF(Exception)");
			log.error("generaAcuseArchivoPDF.rg                       = <" + rg                       + ">");
			log.error("generaAcuseArchivoPDF.strDirectorioTemp        = <" + strDirectorioTemp        + ">");
			log.error("generaAcuseArchivoPDF.cabecera                 = <" + cabecera                 + ">");
			log.error("generaAcuseArchivoPDF.strDirectorioPublicacion = <" + strDirectorioPublicacion + ">");
			log.error("generaAcuseArchivoPDF.totalRegistros           = <" + totalRegistros           + ">");
			log.error("generaAcuseArchivoPDF.montoTotal               = <" + montoTotal               + ">");
			log.error("generaAcuseArchivoPDF.loginUsuario             = <" + loginUsuario             + ">");
			log.error("generaAcuseArchivoPDF.nombreUsuario            = <" + nombreUsuario            + ">");
			e.printStackTrace();
			
			throw new AppException("Ocurri� un error al generar el PDF con la informaci�n del acuse.");
			
		} finally {
			
			log.info("generaAcuseArchivoPDF(S)");
			
		}
		
		return nombreArchivo;	
	
	}
	
	/**
		Crea un archivo CSV con el detalle de los errores presentados en la validaci�n.
		@throws AppException 
 
		@param detalleErroresVsCifrasControl         <tt>JSON</tt> array con el detalle de los Errores vs Cifras de Control.
		@param detalleRegistrosConErrores			   <tt>JSON</tt> array con el detalle de los Registros con Errores.
		@param strDirectorioTemp   						<tt>String</tt> con la ruta del directorio temporal donde se creara el archivo CSV.
		@param loginUsuario  								<tt>String</tt> con el login del usuario firmado en el sistema.
 
		@return <tt>String</tt> con el nombre del archivo generado.
	*/	
	public static String generaArchivoCSVErrores(
			String detalleErroresVsCifrasControl,
			String detalleRegistrosConErrores,
			String strDirectorioTemp,
			String loginUsuario
	) throws AppException{
	
		log.info("generaArchivoCSVErrores(E)");
		
		FileOutputStream		out 				= null;
		BufferedWriter 		csv 				= null;
		String 					nombreArchivo 	= null;
		CreaArchivo 			archivo 			= null;
		
		try {
			
			archivo 			= new CreaArchivo();
			nombreArchivo	= "CargaMasiva-ERRORES-" + archivo.nombreArchivo() + ".csv";
			
			out 				= new FileOutputStream(strDirectorioTemp+nombreArchivo);
			csv 				= new BufferedWriter(new OutputStreamWriter(out, "Cp1252"));
			
			csv.write("Errores vs. Cifras de Control\r\n");
			csv.write("\r\n");
			JSONArray  erroresVsCifrasControl = JSONArray.fromObject(detalleErroresVsCifrasControl);
			for(int i=0,registrosGuardados=0;i<erroresVsCifrasControl.size();i++,registrosGuardados++){
				
				String linea =  erroresVsCifrasControl.getString(i);
 
				// Agregar l�nea con el detalle de los errores.
				csv.write("\""); 
				for(int j=0;j<linea.length();j++){
					char c = linea.charAt(j);
					// Escapar comillas dobles
					if( c == '"' ){
						csv.write('"');
					}
					// Enviar caracter al archivo
					csv.write(c);
				}
				csv.write("\"");
				// Agregar nueva l�nea 
				csv.write("\r\n");
				
				// Realizar flush si es conveniente
				if( ( registrosGuardados % 256 ) == 0){
					registrosGuardados = 0;
					csv.flush();
				}
 
			}
			csv.flush();
			
			csv.write("\r\n");
			
			csv.write("Registros con Errores\r\n");
			csv.write("\r\n");
			JSONArray  registrosConErrores 	 = JSONArray.fromObject(detalleRegistrosConErrores);
			for(int i=0,registrosGuardados=0;i<registrosConErrores.size();i++,registrosGuardados++){
				
				String linea =  registrosConErrores.getString(i);
 
				// Agregar l�nea con el detalle de los errores.
				csv.write("\""); 
				for(int j=0;j<linea.length();j++){
					char c = linea.charAt(j);
					// Escapar comillas dobles
					if( c == '"' ){
						csv.write('"');
					}
					// Enviar caracter al archivo
					csv.write(c);
				}
				csv.write("\"");
				// Agregar nueva l�nea 
				csv.write("\r\n");
				
				// Realizar flush si es conveniente
				if( ( registrosGuardados % 256 ) == 0){
					registrosGuardados = 0;
					csv.flush();
				}
 
			}
			csv.flush();
			
		} catch(Exception e) {
			
			log.error("generaArchivoCSVErrores(Exception)");
			log.error("generaArchivoCSVErrores.detalleErroresVsCifrasControl = <" + detalleErroresVsCifrasControl + ">");
			log.error("generaArchivoCSVErrores.detalleRegistrosConErrores    = <" + detalleRegistrosConErrores    + ">");
			log.error("generaArchivoCSVErrores.strDirectorioTemp             = <" + strDirectorioTemp             + ">");
			log.error("generaArchivoCSVErrores.loginUsuario                  = <" + loginUsuario                  + ">");
			e.printStackTrace();
			
			throw new AppException("Ocurri� un error al generar el CSV con el detalle de los errores.");
			
		} finally {
			
			// Cerrar archivo
			if(csv != null ){	try { csv.close();}catch(Exception e){} }
			
			log.info("generaArchivoCSVErrores(S)");
			
		}
		
		return nombreArchivo;
		
	}
	
	/**
		Crea un archivo CSV con el detalle de los registros de la solicitud de reinstalaciones para 
		un folio especifico.
		@throws AppException 
 
		@param folio		 			  <tt>String</tt> con el numero de folio de la solicitud.
		@param queryDetalleSolicitud <tt>HashMap</tt> con el query para realizar la consulta.
		@param strDirectorioTemp     <tt>String</tt> con la ruta del directorio temporal donde se creara el archivo CSV.
		
		@return <tt>String</tt> con el nombre del archivo generado.
	*/	
	public static String generaArchivoCSVSolicitudReinstalaciones(
			String  folio,
			HashMap queryDetalleSolicitud,
			String  strDirectorioTemp
		) throws AppException {
	
		log.info("generaArchivoCSVSolicitudReinstalaciones(E)");
		
		FileOutputStream		out 				= null;
		BufferedWriter 		csv 				= null;
		String 					nombreArchivo 	= null;
		CreaArchivo 			archivo 			= null;
 
		AccesoDB					con				= new AccesoDB();
		PreparedStatement 	ps					= null;
		ResultSet				rs					= null;
		String 					querySentencia = null;
		List						parametros		= null;
		
		try {
 
			// 1. Definir nombre de archivo CSV con el detalle de los registros
			archivo 			= new CreaArchivo();
			nombreArchivo	= "CargaMasiva-Registros-" + archivo.nombreArchivo() + ".csv";
			
			// 2. Crear y abrir archivo CSV para su escritura
			out 				= new FileOutputStream(strDirectorioTemp+nombreArchivo);
			csv 				= new BufferedWriter(new OutputStreamWriter(out, "Cp1252"));
			
			// 3. Agregar Folio de la Operaci�n al archivo CSV.
			csv.write("Folio de Operaci�n: "); 
			csv.write(folio); 
			csv.write("\r\n");
 
			// Conectarse a la Base de Datos
			con.conexionDB();
			
			// 4. Obtener query para consultar el detalle de la solicitud de reinstalaciones de garantias
			querySentencia = (String) queryDetalleSolicitud.get("text");
			// 5. Preparar consulta de los Registros
			ps = con.queryPrecompilado(querySentencia);
			// Definir los parametros
			int indice  = 1;
			parametros	= (List)		queryDetalleSolicitud.get("parameters");	
			for(int i=0;i<parametros.size();i++){
				Object o = (Object) parametros.get(i);
				if(        o instanceof String     ){
					ps.setString(indice++, (String)   	o );
				} else if( o instanceof Long       ){
					ps.setLong(indice++,   ((Long)    	o ).longValue() );
				} else if( o instanceof Integer    ){
					ps.setInt(indice++,    ((Integer) 	o ).intValue() );
				} else if( o instanceof BigDecimal ){
					ps.setBigDecimal(indice++,    (BigDecimal) o  );
				}
			}
			
			// 6. Realizar consulta
			rs = ps.executeQuery();
			
			// 7. Agregar l�nea en blanco
			csv.write("\r\n");
			
			// 8. Agregar cabecera con el detalle de los registros
			csv.write("Clave del intermediario,");
			csv.write("Clave de la garant�a,");
			csv.write("Nombre del acreditado,");
			csv.write("Fecha honrada,");
			csv.write("Monto honrado,");
			csv.write("Monto total de la reinstalaci�n del acreditado,");
			csv.write("Moneda,");
			csv.write("Tipo de recuperaci�n,");
			csv.write("Devoluci�n de Comisi�n de Aniversario,");
			csv.write("Devoluci�n de IVA de Comisi�n de Aniversario,");
			csv.write("Total de Devoluci�n de Comisi�n e IVA de Aniversario,");
			csv.write("Capital reembolsado por el acreditado,");
			csv.write("Inter�s reembolsado por el acreditado,");
			csv.write("Moratorios reembolsado por el acreditado,");
			csv.write("Fecha del Dep�sito,");
			csv.write("Importe del Reembolso de Reinstalaci�n de NAFIN,");
			csv.write("Importe de Costo Financiero,");
			csv.write("Penalizaci�n,");
			csv.write("IVA generado por Costos Financieros / Penalizaci�n,");
			csv.write("Fecha de reembolso a NAFIN");
			csv.write("\r\n");
			
			// 9. Agregar detalle de los registros
			int 		cuentaRegistros       = 0;
			int		registrosTransmitidos = 0;
			while(rs.next()){
				
				// 9.1 Convertir registro al formato CSV
				String 	registro 			= (rs.getString("REGISTRO") == null)?"":rs.getString("REGISTRO");
				char 		previousChar 		= (char) 0;
				boolean  requiereComillas	= false;
				int		columna				= 0;
				boolean  inicioColumna		= false;
				boolean  finColumna			= false;
				for(int j=0;j<registro.length();j++){
					
					char c = registro.charAt(j);
					
					// Determinar si es inicio o fin de columna
					inicioColumna	= ( j == 0 || previousChar == '@') ?true:false;
					finColumna		=  c == '@'								  ?true:false;
					
					// Actualizar contador de columna
					if( inicioColumna ){
						columna++;
					}
					
					// Determinar si la columna en cuestion requiere que el campo este delitimitado por comillas
					// como sucede cuando se usan numeros ( con el proposito de evitar que excel convierta los numeros
					// a notaci�n cient�fica )
					switch(columna){
						case 2:	// Clave de la garant�a
						case 5:	// Monto honrado
						case 6:	// Monto total de la reinstalaci�n del acreditado
						case 9:	// Devoluci�n de Comisi�n de Aniversario
						case 10:	// Devoluci�n de IVA de Comisi�n de Aniversario
						case 11:	// Total de Devoluci�n de Comisi�n e IVA de Aniversario
						case 12:	// Capital reembolsado por el acreditado
						case 13:	// Inter�s reembolsado por el acreditado
						case 14:	// Moratorios reembolsado por el acreditado
						case 16:	// Importe del Reembolso de Reinstalaci�n de NAFIN
						case 17:	// Importe de Costo Financiero
						case 18:	// Penalizaci�n
						case 19:	// IVA generado por Costos Financieros / Penalizaci�n
							requiereComillas = true; // Si es columna vac�a, no agregar comillas.
							break;
						default:
							requiereComillas = false;
					}
					
					// Si se trata del primer caracter, o se encontro el final de un campo '@'
					// agregar comillas dobles de inicio de campo nuevo.
					if( j == 0           || previousChar == '@' ){
						csv.write("\"");
					}
 
					// Agregar comillas a los campos que lo requieran: 'CAMPO'
					if(        inicioColumna    && finColumna    ){
						// Si es columna vac�a, no agregar comillas
					} else if( requiereComillas && inicioColumna ){
						csv.write("'");
					} else if( requiereComillas && finColumna    ){
						csv.write("'");
					}
						
					// El caracter corresponde a comillas dobles por lo que debe escaparse.
					if(        c == '"' ){
						csv.write('"');
						csv.write('"');
					// Se encontr� fin del campo, por lo que hay que "cerrar" con comillas dobles y agregar una coma
					// para finalizar el campo CSV.
					} else if( c == '@' ){
						csv.write('"');
						csv.write(",");
					// El caracter leido es un caracter normal; enviarlo al archivo CSV.
					} else {
						csv.write(c);
					}
					
					// Guardar copia del caracter anterior
					previousChar = c;
  
				}
				csv.write("\r\n");
 
				// 9.2 Actualizar conteo de registros procesados
				cuentaRegistros++;
				registrosTransmitidos++;
				
				// 9.3 Realizar flush si es conveniente
				if( ( registrosTransmitidos % 128 ) == 0){
					registrosTransmitidos = 0;
					csv.flush();
				}
				
			}
			
			// 10. Verificar si se encontraron registros
			if( cuentaRegistros == 0 ){
				csv.write("No hay datos\r\n");
			}
			csv.flush();
 
		} catch(Exception e){
			
			log.error("generaArchivoCSVSolicitudReinstalaciones(Exception)");
			log.error("generaArchivoCSVSolicitudReinstalaciones.folio                 = <" + folio                 + ">");
			log.error("generaArchivoCSVSolicitudReinstalaciones.queryDetalleSolicitud = <" + queryDetalleSolicitud + ">");
			log.error("generaArchivoCSVSolicitudReinstalaciones.strDirectorioTemp     = <" + strDirectorioTemp     + ">");
			e.printStackTrace();
			
			throw new AppException("Ocurri� un error al generar el CSV con el detalle de los registros de la solicitud de reinstalaciones.");
			
		} finally {
			
			// Cerrar archivo
			if( csv != null ){ try { csv.close(); } catch(Exception e){} }
			// Cerrar ps, rs y conexion a la Base de Datos
			if( rs  != null ){ try { rs.close();  } catch(Exception e){} }
			if( ps  != null ){ try { ps.close();  } catch(Exception e){} }
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			
			log.info("generaArchivoCSVSolicitudReinstalaciones(S)");
			
		}
		
		return nombreArchivo;
		
	}
		
}