package com.netro.garantias;

import com.netro.pdf.ComunesPDF;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.Calendar;
import java.util.Date;

import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.Fecha;

public class ConsSolicIFGAbean {
	
	private int iTipoOperacion = 0;
	/*FODEA 000 - 2009 Garantias*/
	private int iIF = 0;
	private String fecha_oper_ini = "";
	private String fecha_oper_fin = "";
	private String cbo_situacion = "";
	private String programa="";
	private String folio="";
	private String enviadoSiag="";
  /*FODEA 000 - 2009 Garantias*/
	public ConsSolicIFGAbean() { }

	public void setiTipoOperacion(int iTipoOperacion) {
		this.iTipoOperacion = iTipoOperacion;
	}
	
  /*FODEA 000 - 2009 Garantias*/
	public void setiIF(int iIF) {
		this.iIF = iIF;
	}
  public void setFechaOperIni(String fecha_oper_ini){
    this.fecha_oper_ini = fecha_oper_ini;
  }
  public void setFechaOperFin(String fecha_oper_fin){
    this.fecha_oper_fin = fecha_oper_fin;
  }
  public void setSituacion(String cbo_situacion){
    this.cbo_situacion = cbo_situacion;
  }
  /*FODEA 000 - 2009 Garantias*/
	public String getQuerySolicitudes() {
		StringBuffer	sbQuery		= new StringBuffer();
		String 			sHint 		= " /*+index(es)*/ ";
		String 			sCondicion	= "";
		String 			sReferencia	= ",'ConsSolicIFGAbean:getQueryPorEstatus("+iIF+")' as PANTALLA ";
		
		if(iTipoOperacion!=0)
			sCondicion += " AND s.ic_tipo_operacion = ? ";
			
		if(iTipoOperacion==2)
			sCondicion += " AND s.ic_situacion != 7 ";

/*FODEA 000 - 2009 Garantias*/
		if(iIF != 0){
      sCondicion += " AND i.ic_if = ? ";
    }
		if(!fecha_oper_ini.equals("") && !fecha_oper_fin.equals("")){
      sCondicion += " AND es.df_fecha_hora BETWEEN TO_DATE(?, 'DD/MM/YYYY') AND TO_DATE(?, 'DD/MM/YYYY') + 1 ";
    }
		if(!cbo_situacion.equals("")){
      sCondicion += " AND es.ic_situacion = ? ";
    }
	 if(!folio.equals("")){
		sCondicion+=" and es.ic_folio = ? ";
	 }
	 if (!programa.equals("")){
		sCondicion+= " and es.cg_programa = ? ";
	 }
	 if (!enviadoSiag.equals("")){
		sCondicion+= " and es.cg_pperd_disp_post = ?";
	 }
/*FODEA 000 - 2009 Garantias*/
		sbQuery.append(
			" SELECT   "+sHint+" "   +
      " i.ic_if AS clave_if, i.cg_razon_social AS nombre_if,"+  /*FODEA 000 - 2009 Garantias*/
      " es.ic_usuario_facultado AS tipo_envio,"+  /*FODEA 000 - 2009 Garantias*/
			" TO_CHAR (es.df_fecha_hora, 'dd/mm/yyyy hh24:mi') AS fecha,"   +
			" TO_CHAR(es.ic_folio) as ic_folio, s.cg_descripcion AS situacion, es.in_registros_acep,"   +
			" es.in_registros_rech, es.in_registros, es.ic_situacion,"   +  /*FODEA 000 - 2009 Garantias*/
      " es.fn_impte_total as monto_enviado "+  /*FODEA 000 - 2009 Garantias*/
		", TO_CHAR(es.in_folio_reproceso) AS reproceso "+//FODEA 012-2009
			sReferencia+
			" , es.cg_programa as programa	"+//Fodea 024-2012
			"     FROM gti_estatus_solic es, gticat_situacion s, comcat_if i"   +
			"    WHERE es.ic_situacion = s.ic_situacion"   +
			"      AND es.ic_tipo_operacion = s.ic_tipo_operacion"   +
			"      AND es.ic_if_siag = i.ic_if_siag"   +
			sCondicion+
			" ORDER BY es.df_fecha_hora desc");
		System.out.println("sbQuery.toString()>>>>"+sbQuery.toString());
		return sbQuery.toString();
	} // getQuerySolicitudes()
	
	public String getPathFilePDF(String clave_if,String folio,String origen,String cvePerf,
	String strDirectorioTemp,HttpSession session,String strDirectorioPublicacion,String iNoCliente) {
	
		
		AccesoDB con = new AccesoDB();
		PreparedStatement	ps	= null;
		ResultSet rs	= null;
		String qrySentencia	= "";
		String tabla = "";
		CreaArchivo archivo = new CreaArchivo();
		String nombreArchivo = archivo.nombreArchivo()+".pdf";
		int i = 0;
		ComunesPDF pdfDoc = new ComunesPDF(1,strDirectorioTemp+nombreArchivo);
		String icIfSiag = "";
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
                Calendar tmpCalendar=Calendar.getInstance();
		
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
													((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
										 (String)session.getAttribute("sesExterno"),
										 
													(String) session.getAttribute("strNombre"),
														(String) session.getAttribute("strNombreUsuario"),
										 (String)session.getAttribute("strLogo"),
										 strDirectorioPublicacion);
		
		pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		
		try{
			con.conexionDB();
			
			
			qrySentencia =
				"select "+
				"  TO_CHAR(ES.ic_folio) as ic_folio "+
				" ,I.cg_razon_social as NOMBRE_IF"+
				" ,to_char(df_validacion_siag,'dd/mm/yyyy') as FECHA_PROCESO "+
				" ,to_char(df_autorizacion_siag,'dd/mm/yyyy') as FECHA_AUTORIZA "+
				" ,NVL(ES.in_registros_acep,0) as ACEP"+
				" ,NVL(ES.in_registros_rech,0) as RECH"+
				" ,NVL(ES.in_registros,0) as TOTAL"+
				" ,es.IC_IF_SIAG"+
				" ,es.ic_situacion"+
				" ,es.ig_anio_trim_calif"+
				" ,es.ig_trim_calif"+
				" ,nvl(es.CG_USUARIO_ACTUALIZA, ' ') as CG_USUARIO_ACTUALIZA"+
				" ,cs.cg_descripcion "+
				" from gti_estatus_solic ES"+
				" ,comcat_if I"+
				" ,gticat_situacion cs"+
				" where ES.ic_if_siag = I.ic_if_siag"+
				" and ES.IC_SITUACION = cs.IC_SITUACION"+
				" and ES.IC_TIPO_OPERACION = cs.IC_TIPO_OPERACION"+
				" and ES.ic_folio = ?"+
				" and I.ic_if = ?";
			
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,folio);
			if("4".equals(cvePerf)){
			ps.setString(2,clave_if);
			} else {
			ps.setString(2,iNoCliente);
			}
			rs = ps.executeQuery();
			if(rs.next()){
				int situacion = rs.getInt("ic_situacion");
				icIfSiag = rs.getString("IC_IF_SIAG");
				pdfDoc.setTable(1,100);
				if("CONSALTA".equals(origen)) {
					pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("RESULTADOS DE SOLICITUD DE ALTA DE GARANTIAS","celda04",ComunesPDF.CENTER);
				}else if("CONSSALCOM".equals(origen)){
					pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("RESULTADOS DE SOLICITUD DE CARGA DE SALDOS Y PAGO DE COMISIONES","celda04",ComunesPDF.CENTER);		
				}else if("CONSCALIF".equals(origen)){
					pdfDoc.addText("\nA�o: "+rs.getString("IG_ANIO_TRIM_CALIF")+"   Trimestre: "+rs.getString("IG_TRIM_CALIF")+"\n","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("RESULTADOS DE SOLICITUD DE CARGA DE CALIFICACION DE CARTERA","celda04",ComunesPDF.CENTER);		
				}else if("CONSREC".equals(origen)){
					pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("RESULTADOS DE SOLICITUD DE CARGA DE RECUPERACIONES","celda04",ComunesPDF.CENTER);		
				}
				else if("CONSREINSG".equals(origen)){
					pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("RESULTADOS DE SOLICITUD REINSTALACI�N DE GARANT�AS","celda04",ComunesPDF.CENTER);		
				}
				else{
					pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("RESULTADOS","celda04",ComunesPDF.CENTER);
				}
				pdfDoc.addTable();
		
				if(!"CONSSALCOM".equals(origen)){
					if("CONSREC".equals(origen) ||  "CONSREINSG".equals(origen)){
						String aux = situacion +" " +rs.getString("cg_descripcion") + "\n Fecha de Operaci�n en Firme:" + ((rs.getString("FECHA_PROCESO")==null)?"":rs.getString("FECHA_PROCESO")) +"\n"; 
						pdfDoc.setTable(2);    
						pdfDoc.setCell("USUARIO: "+rs.getString("CG_USUARIO_ACTUALIZA"),"formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.    
						pdfDoc.setCell(aux,"formas",ComunesPDF.RIGHT,1,1,0);//El parametro 0, es para que la celda no tenga borde.    
						pdfDoc.addTable();
					} else {
						if(rs.getString("FECHA_PROCESO") == null ){
							pdfDoc.addText("Fecha de Operaci�n en Firme: \n","formas",ComunesPDF.RIGHT);
						}else{
							pdfDoc.addText("Fecha de Operaci�n en Firme: "+rs.getString("FECHA_PROCESO")+"\n","formas",ComunesPDF.RIGHT);
						}
					}
				}else{
						if(situacion ==5){
							if(rs.getString("FECHA_AUTORIZA") == null ){
								pdfDoc.addText("Fecha de Operaci�n en Firme: \n","formas",ComunesPDF.RIGHT);
							}else{
								pdfDoc.addText("Fecha de Operaci�n en Firme: "+rs.getString("FECHA_AUTORIZA")+"\n","formas",ComunesPDF.RIGHT);
							}
						}else{
							if (rs.getString("FECHA_PROCESO") == null ){
								pdfDoc.addText("Fecha de C�lculo de Comisiones: \n","formas",ComunesPDF.RIGHT);
							}else{
								pdfDoc.addText("Fecha de C�lculo de Comisiones: "+rs.getString("FECHA_PROCESO")+"\n","formas",ComunesPDF.RIGHT);
							}
					  }
				}
				pdfDoc.setTable(5,95);
					pdfDoc.setCell("FOLIO DE LA OPERACION","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("INTERMEDIARIO FINANCIERO","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("REGISTROS SIN ERRORES","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("REGISTROS CON ERRORES","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("TOTAL DE REGISTROS PROCESADOS","celda01",ComunesPDF.CENTER);
		
					pdfDoc.setCell(rs.getString("IC_FOLIO"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(rs.getString("NOMBRE_IF"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("ACEP"),0),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("RECH"),0),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("TOTAL"),0),"formas",ComunesPDF.CENTER);			
		
				pdfDoc.addTable();
			}
			ps.close();	
		
			pdfDoc.addText("\n\n\n","formas",ComunesPDF.LEFT);	
			if("CONSALTA".equals(origen)) {
				qrySentencia =
					" select ic_programa"   +
					"  ,fn_porc_comision"   +
					"  ,in_registros_acep"   +
					"  ,in_registros_rech"   +
					"  ,in_registros"   +
					" from gti_det_alta_gtia GD"   +
					" , comcat_if I"   +
					" where GD.ic_if_siag = I.ic_if_siag" +
					" and GD.ic_folio = ?"+
					" and I.ic_if = ?";
				
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,folio);
				if("4".equals(cvePerf)){
				ps.setString(2,clave_if);
				} else {
				ps.setString(2,iNoCliente);
				}		
				rs = ps.executeQuery();
				while(rs.next()){
					if(i==0){
						pdfDoc.setTable(1,100);
						pdfDoc.setCell("DETALLE DE GARANTIAS POR PROGRAMA","celda04",ComunesPDF.CENTER);
						pdfDoc.addTable();
						pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
						pdfDoc.setTable(5,95);
						pdfDoc.setCell("CLAVE DEL PROGRAMA","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("PORCENTAJE DE COMISION","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("REGISTROS SIN ERRORES","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("REGISTROS CON ERRORES","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("TOTAL DE REGISTROS PROCESADOS","celda01",ComunesPDF.CENTER);
					}
			
					pdfDoc.setCell(rs.getString("IC_PROGRAMA")+" ","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("FN_PORC_COMISION"),4)+" ","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("IN_REGISTROS_ACEP"),0)+"","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("IN_REGISTROS_RECH"),0)+"","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("IN_REGISTROS"),0)+" ","formas",ComunesPDF.CENTER);
					i++;
				}	// while
				ps.close();
			}// ALTA
			else if("CONSSALCOM".equals(origen)) {
				// FECHA DE OPERACION EN FIRME ( COMISIONES POR MONEDA )
				qrySentencia =" SELECT " + 
                                "		df_validacion_siag, DF_FECHA_FIRME as FECHA_OPERACION " + 
                                " FROM GTI_DETALLE_SALDOS ds " + 
                                "  ,comcat_moneda m " + 
                                "  ,gti_estatus_solic " + 
                                "  WHERE ds.ic_moneda = m.ic_moneda(+) " + 
                                "  and gti_estatus_solic.ic_folio=ds.ic_folio " + 
                                "  and ds.ic_if_siag = ? " + 
                                "AND ds.ic_folio = ? " + 
                                "AND ds.cs_comision = 'S' " + 
                                "group by df_validacion_siag, DF_FECHA_FIRME ";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,icIfSiag);
				ps.setString(2,folio);
				rs = ps.executeQuery();
				if(rs.next()){
                                        
					if(rs.getString("FECHA_OPERACION") != null ){
                                                tmpCalendar.setTime(rs.getDate("FECHA_OPERACION"));
                                                pdfDoc.addText("Fecha de Operaci�n en Firme: "+Fecha.getFormatCalendar( tmpCalendar,"dd/MM/yyyy")+"","formas",ComunesPDF.RIGHT);						
                                        }else if(rs.getString("df_validacion_siag") != null){
                                                tmpCalendar.setTime(rs.getDate("df_validacion_siag"));
                                                pdfDoc.addText("Fecha de C�lculo de Comisi�n: "+Fecha.getFormatCalendar( tmpCalendar,"dd/MM/yyyy")+"","formas",ComunesPDF.RIGHT);
                                        }else{
                                                pdfDoc.addText("Fecha de Operaci�n en Firme: ","formas",ComunesPDF.RIGHT);
					}
				}
				rs.close();ps.close();
				
				// DETALLE DE COMISIONES POR MONEDA NACIONAL
				qrySentencia =" SELECT NVL(m.cd_nombre,'NA') AS cd_nombre," + 
                                "  sum(in_aceptados) as in_aceptados," + 
                                "  sum(in_rechazados) as in_rechazados, " + 
                                "  sum(in_tot_registros) as in_tot_registros, " + 
                                "  sum(fn_impte_autoriza) as fn_impte_autoriza, " + 
                                "  df_fecha_corte " + 
                                " FROM GTI_DETALLE_SALDOS ds , " + 
                                "  comcat_moneda m " + 
                                " WHERE ds.ic_moneda = m.ic_moneda(+) " + 
                                " AND ic_if_siag     = ? " + 
                                " AND ic_folio       = ? " + 
                                " AND ds.cs_comision = 'S' " + 
                                " AND m.ic_moneda    =1 " + 
                                " group by NVL(m.cd_nombre,'NA'), " + 
                                "  df_fecha_corte " + 
                                " ORDER BY df_fecha_corte";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,icIfSiag);
				ps.setString(2,folio);
				rs = ps.executeQuery();
                                i=0;
                                Double fn_impte_autoriza=new Double(0);
				while(rs.next()){
                                        Double tmp_fn_impte_autoriza=rs.getDouble("fn_impte_autoriza");
                                        fn_impte_autoriza=fn_impte_autoriza+tmp_fn_impte_autoriza;
					if(i==0){
                                                pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
                                                pdfDoc.setTable(1,100);
						pdfDoc.setCell("DETALLE DE COMISIONES POR "+rs.getString("cd_nombre"),"celda04",ComunesPDF.CENTER);
						pdfDoc.addTable();
						pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
						pdfDoc.setTable(5,95);
						pdfDoc.setCell("FECHA CORTE","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("REGISTROS SIN ERRORES","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("REGISTROS CON ERRORES","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("TOTAL DE REGISTROS","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("IMPORTE DE COMISI�N E IVA DE REGISTROS ACEPTADOS","celda01",ComunesPDF.CENTER);
					}
                                        tmpCalendar.setTime(rs.getDate("df_fecha_corte"));
					pdfDoc.setCell(Fecha.getFormatCalendar( tmpCalendar,"dd/MM/yyyy"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_aceptados"),0)+"","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_rechazados"),0)+"","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_tot_registros"),0)+"","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("fn_impte_autoriza"),2)+" ","formas",ComunesPDF.CENTER);
					i++;
				}	// while
				rs.close();ps.close();
				if(i>0){
				    pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.
				    pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.
				    pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.
				    pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.
				    pdfDoc.setCell(Comunes.formatoDecimal(fn_impte_autoriza,2)+"","celda01",ComunesPDF.CENTER);
					pdfDoc.addTable();
				}
			    // DETALLE DE COMISIONES POR DOLAR AMERICANO 
			    qrySentencia ="SELECT NVL(m.cd_nombre,'NA') AS cd_nombre," + 
                                    "  sum(in_aceptados) as in_aceptados," + 
                                    "  sum(in_rechazados) as in_rechazados, " + 
                                    "  sum(in_tot_registros) as in_tot_registros, " + 
                                    "  sum(fn_impte_autoriza) as fn_impte_autoriza, " + 
                                    "  df_fecha_corte " + 
			            " FROM GTI_DETALLE_SALDOS ds ," + 
			            "  comcat_moneda m" + 
			            " WHERE ds.ic_moneda = m.ic_moneda(+)" + 
			            " AND ic_if_siag     = ? " + 
			            " AND ic_folio       = ? " + 
			            " AND ds.cs_comision = 'S' " + 
			            " AND m.ic_moneda    =54 " + 
                                    " group by NVL(m.cd_nombre,'NA'), " + 
                                    "  df_fecha_corte " + 
                                    " ORDER BY df_fecha_corte";
			    ps = con.queryPrecompilado(qrySentencia);
			    ps.setString(1,icIfSiag);
			    ps.setString(2,folio);
			    rs = ps.executeQuery();
			    i=0;
			    fn_impte_autoriza=new Double(0);
			    while(rs.next()){
                                    Double tmp_fn_impte_autoriza=rs.getDouble("fn_impte_autoriza");
                                    fn_impte_autoriza=fn_impte_autoriza+tmp_fn_impte_autoriza;
			            if(i==0){
                                            pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
                                            pdfDoc.setTable(1,100);
			                    pdfDoc.setCell("DETALLE DE COMISIONES POR "+rs.getString("cd_nombre"),"celda04",ComunesPDF.CENTER);
			                    pdfDoc.addTable();
			                    pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
			                    pdfDoc.setTable(5,95);
			                    pdfDoc.setCell("FECHA CORTE","celda01",ComunesPDF.CENTER);
			                    pdfDoc.setCell("REGISTROS SIN ERRORES","celda01",ComunesPDF.CENTER);
			                    pdfDoc.setCell("REGISTROS CON ERRORES","celda01",ComunesPDF.CENTER);
			                    pdfDoc.setCell("TOTAL DE REGISTROS","celda01",ComunesPDF.CENTER);
			                    pdfDoc.setCell("IMPORTE DE COMISI�N E IVA DE REGISTROS ACEPTADOS","celda01",ComunesPDF.CENTER);
			            }
                                    tmpCalendar.setTime(rs.getDate("df_fecha_corte"));
                                    pdfDoc.setCell(Fecha.getFormatCalendar( tmpCalendar,"dd/MM/yyyy"),"formas",ComunesPDF.CENTER);
			            pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_aceptados"),0)+"","formas",ComunesPDF.CENTER);
			            pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_rechazados"),0)+"","formas",ComunesPDF.CENTER);
			            pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_tot_registros"),0)+"","formas",ComunesPDF.CENTER);
			            pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("fn_impte_autoriza"),2)+" ","formas",ComunesPDF.CENTER);
			            i++;
			    }       // while
			    rs.close();ps.close();
			    if(i>0){
                                    pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.
                                    pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.
                                    pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.
                                    pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.
                                    pdfDoc.setCell(Comunes.formatoDecimal(fn_impte_autoriza,2)+"","celda01",ComunesPDF.CENTER);
			            pdfDoc.addTable();
			    }

			    // DETALLE DE COMISIONES POR MONEDA N/A
			    qrySentencia ="SELECT NVL(m.cd_nombre,'NA') AS cd_nombre," + 
                                    "  sum(in_aceptados) as in_aceptados," + 
                                    "  sum(in_rechazados) as in_rechazados, " + 
                                    "  sum(in_tot_registros) as in_tot_registros, " + 
                                    "  sum(fn_impte_autoriza) as fn_impte_autoriza, " + 
                                    "  df_fecha_corte " + 
			            " FROM GTI_DETALLE_SALDOS ds ," + 
			            "  comcat_moneda m" + 
			            " WHERE ds.ic_moneda = m.ic_moneda(+)" + 
			            " AND ic_if_siag     = ? " + 
			            " AND ic_folio       = ? " + 
			            " AND ds.cs_comision = 'S' " + 
			            " AND (m.ic_moneda is null or m.ic_moneda not in(1,54)) " + 
                                    " group by NVL(m.cd_nombre,'NA'), " + 
                                    "  df_fecha_corte " + 
                                    " ORDER BY df_fecha_corte";
			    ps = con.queryPrecompilado(qrySentencia);
			    ps.setString(1,icIfSiag);
			    ps.setString(2,folio);
			    rs = ps.executeQuery();
			    i=0;
			    fn_impte_autoriza=new Double(0);
			    while(rs.next()){
                                    Double tmp_fn_impte_autoriza=rs.getDouble("fn_impte_autoriza");
                                    fn_impte_autoriza=fn_impte_autoriza+tmp_fn_impte_autoriza;
			            if(i==0){
                                            pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
			                    pdfDoc.setTable(1,100);
			                    pdfDoc.setCell("DETALLE DE COMISIONES "+rs.getString("cd_nombre"),"celda04",ComunesPDF.CENTER);
			                    pdfDoc.addTable();
			                    pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
			                    pdfDoc.setTable(5,95);
			                    pdfDoc.setCell("FECHA CORTE","celda01",ComunesPDF.CENTER);
			                    pdfDoc.setCell("REGISTROS SIN ERRORES","celda01",ComunesPDF.CENTER);
			                    pdfDoc.setCell("REGISTROS CON ERRORES","celda01",ComunesPDF.CENTER);
			                    pdfDoc.setCell("TOTAL DE REGISTROS","celda01",ComunesPDF.CENTER);
			                    pdfDoc.setCell("IMPORTE DE COMISI�N E IVA DE REGISTROS ACEPTADOS","celda01",ComunesPDF.CENTER);
			            }
                                    tmpCalendar.setTime(rs.getDate("df_fecha_corte"));
                                    pdfDoc.setCell(Fecha.getFormatCalendar( tmpCalendar,"dd/MM/yyyy"),"formas",ComunesPDF.CENTER);
			            pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_aceptados"),0)+"","formas",ComunesPDF.CENTER);
			            pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_rechazados"),0)+"","formas",ComunesPDF.CENTER);
			            pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_tot_registros"),0)+"","formas",ComunesPDF.CENTER);
			            pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("fn_impte_autoriza"),2)+" ","formas",ComunesPDF.CENTER);
			            i++;
			    }       // while
			    rs.close();ps.close();
			    if(i>0){
                                    pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.
                                    pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.
                                    pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.
                                    pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.
                                    pdfDoc.setCell(Comunes.formatoDecimal(fn_impte_autoriza,2)+"","celda01",ComunesPDF.CENTER);
                                    pdfDoc.addTable();
			    }

				// FECHA DE OPERACION EN FIRME ( SIN COMISIONES POR MONEDA )
				qrySentencia =
					"  SELECT "   +
					" 		DF_FECHA_FIRME AS FECHA_OPERACION "  +
					"  FROM GTI_DETALLE_SALDOS ds"   +
					"   ,comcat_moneda m"   +
					"   WHERE ds.ic_moneda = m.ic_moneda(+)"   +
					"   and ic_if_siag = ? "+
					" AND ic_folio = ? "  +
					" AND ds.cs_comision = 'N' "  +
					" order by decode(m.ic_moneda,1,1,54,2,3)";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,icIfSiag);
				ps.setString(2,folio);
				rs = ps.executeQuery();
				if(rs.next()){
					pdfDoc.addText("\n\n\n","formas",ComunesPDF.LEFT);
					if(rs.getString("FECHA_OPERACION") == null ){
						pdfDoc.addText("Fecha de Operaci�n en Firme: ","formas",ComunesPDF.RIGHT);
					}else{
                                                tmpCalendar.setTime(rs.getDate("FECHA_OPERACION"));
						pdfDoc.addText("Fecha de Operaci�n en Firme: "+Fecha.getFormatCalendar( tmpCalendar,"dd/MM/yyyy")+"","formas",ComunesPDF.RIGHT);
					}
				}
				rs.close();ps.close();
				
				// DETALLE SIN COMISIONES POR MONEDA NACIONAL
				i = 0;
                                qrySentencia = " SELECT NVL(m.cd_nombre,'NA') AS cd_nombre," + 
                                        "  sum(in_aceptados) as in_aceptados," + 
                                        "  sum(in_rechazados) as in_rechazados, " + 
                                        "  sum(in_tot_registros) as in_tot_registros, " + 
                                        "  sum(fn_impte_autoriza) as fn_impte_autoriza, " + 
                                        "  df_fecha_corte " + 
                                        " FROM GTI_DETALLE_SALDOS ds , " + 
                                        "  comcat_moneda m " + 
                                        " WHERE ds.ic_moneda = m.ic_moneda(+) " + 
                                        " AND ic_if_siag     = ? " + 
                                        " AND ic_folio       = ? " + 
                                        " AND ds.cs_comision = 'N' " + 
                                        " AND m.ic_moneda    =1 " + 
                                        " group by NVL(m.cd_nombre,'NA'), " + 
                                        "  df_fecha_corte " + 
                                        " ORDER BY df_fecha_corte";

				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,icIfSiag);
				ps.setString(2,folio);
				rs = ps.executeQuery();
                                fn_impte_autoriza=new Double(0);
				while(rs.next()){
				    Double tmp_fn_impte_autoriza=rs.getDouble("fn_impte_autoriza");
				    fn_impte_autoriza=fn_impte_autoriza+tmp_fn_impte_autoriza;
					if(i==0){
                                                pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
						pdfDoc.setTable(1,100);
						pdfDoc.setCell("DETALLE SIN COMISIONES POR "+rs.getString("cd_nombre"),"celda04",ComunesPDF.CENTER);
						pdfDoc.addTable();
						pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
						pdfDoc.setTable(5,95);
						pdfDoc.setCell("FECHA CORTE","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("REGISTROS SIN ERRORES","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("REGISTROS CON ERRORES","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("TOTAL DE REGISTROS","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("IMPORTE DE COMISI�N E IVA DE REGISTROS ACEPTADOS","celda01",ComunesPDF.CENTER);
					}
                                        tmpCalendar.setTime(rs.getDate("df_fecha_corte"));
                                        pdfDoc.setCell(Fecha.getFormatCalendar( tmpCalendar,"dd/MM/yyyy"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_aceptados"),0)+"","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_rechazados"),0)+"","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_tot_registros"),0)+"","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("fn_impte_autoriza"),2)+" ","formas",ComunesPDF.CENTER);
					i++;
				}	// while
				rs.close();ps.close();
                                if(i>0){
                                    pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.
                                    pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.
                                    pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.
                                    pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.
                                    pdfDoc.setCell(Comunes.formatoDecimal(fn_impte_autoriza,2)+"","celda01",ComunesPDF.CENTER);
			            pdfDoc.addTable();
                                }
			    // DETALLE SIN COMISIONES POR DOLAR AMERICANO
			    i = 0;
                            qrySentencia = " SELECT NVL(m.cd_nombre,'NA') AS cd_nombre, " + 
                                    "  sum(in_aceptados) as in_aceptados," + 
                                    "  sum(in_rechazados) as in_rechazados, " + 
                                    "  sum(in_tot_registros) as in_tot_registros, " + 
                                    "  sum(fn_impte_autoriza) as fn_impte_autoriza, " + 
                                    "  df_fecha_corte " + 
                                    " FROM GTI_DETALLE_SALDOS ds , " + 
                                    "  comcat_moneda m " + 
                                    " WHERE ds.ic_moneda = m.ic_moneda(+) " + 
                                    " AND ic_if_siag     = ? " + 
                                    " AND ic_folio       = ? " + 
                                    " AND ds.cs_comision = 'N' " + 
                                    " AND m.ic_moneda    =54 " + 
                                    " group by NVL(m.cd_nombre,'NA'), " + 
                                    "  df_fecha_corte " + 
                                    " ORDER BY df_fecha_corte";

			    ps = con.queryPrecompilado(qrySentencia);
			    ps.setString(1,icIfSiag);
			    ps.setString(2,folio);
			    rs = ps.executeQuery();
			    fn_impte_autoriza=new Double(0);
			    while(rs.next()){
			        Double tmp_fn_impte_autoriza=rs.getDouble("fn_impte_autoriza");
			        fn_impte_autoriza=fn_impte_autoriza+tmp_fn_impte_autoriza;
			            if(i==0){
                                            pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
			                    pdfDoc.setTable(1,100);
			                    pdfDoc.setCell("DETALLE SIN COMISIONES POR "+rs.getString("cd_nombre"),"celda04",ComunesPDF.CENTER);
			                    pdfDoc.addTable();
			                    pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
			                    pdfDoc.setTable(5,95);
			                    pdfDoc.setCell("FECHA CORTE","celda01",ComunesPDF.CENTER);
			                    pdfDoc.setCell("REGISTROS SIN ERRORES","celda01",ComunesPDF.CENTER);
			                    pdfDoc.setCell("REGISTROS CON ERRORES","celda01",ComunesPDF.CENTER);
			                    pdfDoc.setCell("TOTAL DE REGISTROS","celda01",ComunesPDF.CENTER);
			                    pdfDoc.setCell("IMPORTE DE COMISI�N E IVA DE REGISTROS ACEPTADOS","celda01",ComunesPDF.CENTER);
			            }
                                    tmpCalendar.setTime(rs.getDate("df_fecha_corte"));
                                    pdfDoc.setCell(Fecha.getFormatCalendar( tmpCalendar,"dd/MM/yyyy"),"formas",ComunesPDF.CENTER);
			            pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_aceptados"),0)+"","formas",ComunesPDF.CENTER);
			            pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_rechazados"),0)+"","formas",ComunesPDF.CENTER);
			            pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_tot_registros"),0)+"","formas",ComunesPDF.CENTER);
			            pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("fn_impte_autoriza"),2)+" ","formas",ComunesPDF.CENTER);
			            i++;
			    }       // while
			    rs.close();ps.close();
			    if(i>0){
			        pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.
			        pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.
			        pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.
			        pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.
			        pdfDoc.setCell(Comunes.formatoDecimal(fn_impte_autoriza,2)+"","celda01",ComunesPDF.CENTER);
			        pdfDoc.addTable();
			    }
			    // DETALLE SIN COMISIONES NA
			    i = 0;
			    qrySentencia =
			            "  SELECT nvl(m.cd_nombre,'NA') as cd_nombre,"   +
                                    "  sum(in_aceptados) as in_aceptados," + 
                                    "  sum(in_rechazados) as in_rechazados, " + 
                                    "  sum(in_tot_registros) as in_tot_registros, " + 
                                    "  sum(fn_impte_autoriza) as fn_impte_autoriza, " + 
                                    "  df_fecha_corte " + 
			            "  FROM GTI_DETALLE_SALDOS ds "   +
			            "   ,comcat_moneda m "   +
			            "   WHERE ds.ic_moneda = m.ic_moneda(+) "   +
			            "   and ic_if_siag = ? "+
			            " AND ic_folio = ? "  +
			            " AND ds.cs_comision = 'N' " +
                                    " AND (m.ic_moneda is null or m.ic_moneda not in(1,54))"  +
                                    " group by NVL(m.cd_nombre,'NA'), " + 
                                    "  df_fecha_corte " + 
                                    " ORDER BY df_fecha_corte";

			    ps = con.queryPrecompilado(qrySentencia);
			    ps.setString(1,icIfSiag);
			    ps.setString(2,folio);
			    rs = ps.executeQuery();
			    fn_impte_autoriza=new Double(0);
			    while(rs.next()){
			        Double tmp_fn_impte_autoriza=rs.getDouble("fn_impte_autoriza");
			        fn_impte_autoriza=fn_impte_autoriza+tmp_fn_impte_autoriza;
			            if(i==0){
                                            pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
			                    pdfDoc.setTable(1,100);
			                    pdfDoc.setCell("DETALLE SIN COMISIONES "+rs.getString("cd_nombre"),"celda04",ComunesPDF.CENTER);
			                    pdfDoc.addTable();
			                    pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
			                    pdfDoc.setTable(5,95);
			                    pdfDoc.setCell("FECHA CORTE","celda01",ComunesPDF.CENTER);
			                    pdfDoc.setCell("REGISTROS SIN ERRORES","celda01",ComunesPDF.CENTER);
			                    pdfDoc.setCell("REGISTROS CON ERRORES","celda01",ComunesPDF.CENTER);
			                    pdfDoc.setCell("TOTAL DE REGISTROS","celda01",ComunesPDF.CENTER);
			                    pdfDoc.setCell("IMPORTE DE COMISI�N E IVA DE REGISTROS ACEPTADOS","celda01",ComunesPDF.CENTER);
			            }
                                    tmpCalendar.setTime(rs.getDate("df_fecha_corte"));
                                    pdfDoc.setCell(Fecha.getFormatCalendar( tmpCalendar,"dd/MM/yyyy"),"formas",ComunesPDF.CENTER);
			            pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_aceptados"),0)+"","formas",ComunesPDF.CENTER);
			            pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_rechazados"),0)+"","formas",ComunesPDF.CENTER);
			            pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_tot_registros"),0)+"","formas",ComunesPDF.CENTER);
			            pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("fn_impte_autoriza"),2)+" ","formas",ComunesPDF.CENTER);
			            i++;
			    }       // while
			    rs.close();ps.close();
			    if(i>0){
			        pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.
			        pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.
			        pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.
			        pdfDoc.setCell("","formas",ComunesPDF.LEFT,1,1,0);//El parametro 0, es para que la celda no tenga borde.
			        pdfDoc.setCell(Comunes.formatoDecimal(fn_impte_autoriza,2)+"","celda01",ComunesPDF.CENTER);
			    }
				
			}//CONSSALCOM 
			else if("CONSREC".equals(origen) || "CONSREINSG".equals(origen)) {
				qrySentencia =
					"  SELECT nvl(m.cd_nombre,'NA') as cd_nombre,"   +
					"  		SUM(in_aceptados) AS in_aceptados,"   +
					"       SUM(in_rechazados) AS in_rechazados,"   +
					" 		SUM(in_tot_registros) AS in_tot_registros, "   +
					" 		SUM(fn_impte_deposito) AS fn_impte_deposito, "   +
					" 		SUM(fn_impte_calculado) AS fn_impte_calculado, "   +
					" 		SUM(fn_diferencia) AS fn_diferencia, "   +
					" 		SUM(nvl(fn_impte_complemento,0)) AS fn_impte_complemento, "   +
					" 		decode(m.ic_moneda,1,1,54,2,3) "   +
					"  FROM GTI_DETALLE_RECUPERACIONES d "   +
					"   ,comcat_moneda m "   +
					"   WHERE d.ic_moneda = m.ic_moneda(+) "   +
					"   and ic_if_siag = ? "+
					"   AND ic_folio = ?"  +
					"   GROUP BY nvl(m.cd_nombre,'NA'), decode(m.ic_moneda,1,1,54,2,3)"  +
					" order by decode(m.ic_moneda,1,1,54,2,3) ";
				ps = con.queryPrecompilado(qrySentencia);
				ps.setString(1,icIfSiag);
				ps.setString(2,folio);
				rs = ps.executeQuery();
				while(rs.next()){
					if(i==0){
						pdfDoc.setTable(1,100);
						if("CONSREC".equals(origen))
						pdfDoc.setCell("DETALLE DE RECUPERACIONES POR MONEDA","celda04",ComunesPDF.CENTER);
						else if("CONSREINSG".equals(origen))
						pdfDoc.setCell("DETALLE DE REINSTALACI�N DE GARANT�AS POR MONEDA","celda04",ComunesPDF.CENTER);
						pdfDoc.addTable();
						pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
						pdfDoc.setTable(8,95);
						pdfDoc.setCell("MONEDA","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("REGISTROS SIN ERRORES","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("REGISTROS CON ERRORES","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("TOTAL DE REGISTROS","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("IMPORTE DEPOSITO BANCO","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("IMPORTE CALCULADO\n(A-B+C+D)","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("DIFERENCIA","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("DEPOSITO DE LA DIFERENCIA","celda01",ComunesPDF.CENTER);
					}
			
					pdfDoc.setCell(rs.getString("cd_nombre")+"","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_aceptados"),0)+"","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_rechazados"),0)+"","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(rs.getInt("in_tot_registros"),0)+"","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("fn_impte_deposito"),2)+" ","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("fn_impte_calculado"),2)+" ","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("fn_diferencia"),2)+" ","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(rs.getDouble("fn_impte_complemento"),2)+" ","formas",ComunesPDF.CENTER);
					i++;
				}	// while
				rs.close();ps.close();
			}//CONSREC
			
			if(i>0)
				pdfDoc.addTable();
		
			pdfDoc.addText("\n\n\n","formas",ComunesPDF.LEFT);			
				
			if("CONSREC".equals(origen)) {
			}
				
			qrySentencia =
				"select cg_contenido"+
				" from gti_arch_resultado A"+
				" ,comcat_if I"+
				" where A.ic_if_siag = I.ic_if_siag"+
				" and A.ic_folio = ?"+
				" and I.ic_if = ?"+
				" order by A.ic_linea,A.ic_num_error";
			
			ps = con.queryPrecompilado(qrySentencia);
			ps.setString(1,folio);
			if("4".equals(cvePerf)){
			ps.setString(2,clave_if);
			} else {
			ps.setString(2,iNoCliente);
			}	
			
			rs = ps.executeQuery();
			i=0;
			while(rs.next()){
				if(i==0){
					pdfDoc.setTable(1,100);
					pdfDoc.setCell("ERRORES GENERADOS","celda04",ComunesPDF.CENTER);
					pdfDoc.addTable();		
					
					if("CONSREC".equals(origen) || "CONSREINSG".equals(origen)) {
						pdfDoc.addText("Donde:\n","formas",ComunesPDF.LEFT);
						pdfDoc.addText("A: (CAPITAL + INTERESES + MORATORIOS)\n","formas",ComunesPDF.LEFT);			
						if("CONSREC".equals(origen)){
							pdfDoc.addText("B: (GASTOS DE JUICIO)\n","formas",ComunesPDF.LEFT);}			
						else if("CONSREINSG".equals(origen)){
						pdfDoc.addText("B: (DEVOLUCI�N DE COMISI�N DE ANIVERSARIO + I.V.A. DE LA COMISI�N DE ANIVERSARIO)\n","formas",ComunesPDF.LEFT);}
						pdfDoc.addText("C: (INTERES + PENALIZACION)\n","formas",ComunesPDF.LEFT);
						pdfDoc.addText("D: (I.V.A.)\n\n","formas",ComunesPDF.LEFT);	
						//pdfDoc.addText("ERRORES GENERADOS POR EL PROCESO DE CARGA DE RECUPERACIONES AUTOMATICAS\n","formas",ComunesPDF.LEFT);
					}
				}
				pdfDoc.addText(rs.getString("cg_contenido")+"\n","formas",ComunesPDF.LEFT);
				i++;
			}
			ps.close();
		
			
			pdfDoc.endDocument();
				}catch(Exception e){
			e.printStackTrace();
			nombreArchivo="error";
		}finally{
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}
		return nombreArchivo;
}


	public void setPrograma(String programa) {
		this.programa = programa;
	}


	public String getPrograma() {
		return programa;
	}


	public void setFolio(String folio) {
		this.folio = folio;
	}


	public String getFolio() {
		return folio;
	}


	public void setEnviadoSiag(String enviadoSiag) {
		this.enviadoSiag = enviadoSiag;
	}


	public String getEnviadoSiag() {
		return enviadoSiag;
	}
	
}//ConsSolicIFGAbean