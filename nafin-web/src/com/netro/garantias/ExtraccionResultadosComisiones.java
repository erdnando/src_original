package com.netro.garantias;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.math.BigDecimal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**

	Esta clase se encarga de generar los archivos con los resultados de las comisiones del programa
	de la Sociedad Hipotecaria Federal.
	
	La pantalla donde se utiliza esta clase es la siguiente:
		ADMIN_IF_GARAN - PROGRAMA DE GARANT�A AUTOM�TICA � TRANSACCIONES � COMISIONES � CONSULTA DE RESULTADOS
	
	@author Jesus Salim Hernandez David
	@since  Fodea 006 - 2012 -- Garantias Programa SHF
	@date	  24/06/2012 11:12:59 p.m.
	
 */
public class ExtraccionResultadosComisiones {
	
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ExtraccionResultadosComisiones.class);
	
	/**
	 *	Crea un archivo TXT con el detalle de todos los registros de la carga masiva de comisiones.
    *
	 *	@throws AppException 
	 *	
	 *	@param directorio <tt>String</tt> con el path del directorio donde se guardara el archivo generado.
	 *	@param query   	<tt>HashMap</tt> con el query para realizar la consulta.
	 *	
	 *	@return <tt>String</tt> con el nombre del archivo generado.
	 */
	public static String generaArchivoTXTOrigen( String directorio, HashMap query )
		throws AppException {
			
		log.info("generaArchivoTXTOrigen(E)");
		
		FileOutputStream		out 				= null;
		BufferedWriter 		txt 				= null;
		CreaArchivo 			archivo 			= null;
		String					nombreArchivo 	= null;
		
		AccesoDB 				con 				= new AccesoDB();
		String 					querySentencia	= null;
		List						parametros		= null;
		HashMap 					registro 		= null;
		ArrayList 				lista				= new ArrayList();
		ResultSet 				rs 				= null;
		PreparedStatement 	ps 				= null;
 
		try {
 
			// Conectarse a la base de datos
			con.conexionDB();
			
			// Obtener query
			querySentencia = (String) 	query.get("text");
			ps = con.queryPrecompilado(querySentencia);
			// Definir los parametros
			int indice  = 1;
			parametros	= (List)		query.get("parameters");	
			for(int i=0;i<parametros.size();i++){
				Object o = (Object) parametros.get(i);
				if(        o instanceof String   ){
					ps.setString(indice++, (String)   o );
				} else if( o instanceof Long     ){
					ps.setLong(indice++,   ((Long)    o ).longValue() );
				} else if( o instanceof Integer  ){
					ps.setInt(indice++,    ((Integer) o ).intValue() );
				} else if( o instanceof BigDecimal ){
					ps.setBigDecimal(indice++, (BigDecimal) o );
				}
			}
			// Realizar consulta
			rs = ps.executeQuery();
 
			// Crear Archivo
			archivo 			= new CreaArchivo();
			nombreArchivo	= archivo.nombreArchivo()+".txt";
			out 				= new FileOutputStream(directorio+nombreArchivo);
			txt 				= new BufferedWriter(new OutputStreamWriter(out, "Cp1252"));

			int 	i 						= 0;
			long 	cuentaRegistros	= 0L;
			while( rs.next()) {
 
				if(cuentaRegistros > 0) txt.write("\r\n");
				txt.write((rs.getString("CG_CONTENIDO")	== null)?"":rs.getString("CG_CONTENIDO") ); 

				// Realizar flush si es conveniente
				if((i%250) == 0){
					i = 0;
					txt.flush();
				}
				i++;
				
				// Actualizar contador de registros
				cuentaRegistros++;
		
			}
 
			// Verificar si se encontraron registros
			if( cuentaRegistros == 0 ){
				txt.write("No hay datos"+"\n");
			}
			txt.flush();
			
		}catch(OutOfMemoryError om) { // Se acabo la memoria
			
			log.error("generaArchivoTXTOrigen(OutOfMemoryError)");
			log.error("generaArchivoTXTOrigen.directorio   = <" + directorio                        + ">");
			log.error("generaArchivoTXTOrigen.query    	  = <" + query                             + ">");
			log.error("generaArchivoTXTOrigen::Free Memory = <" + Runtime.getRuntime().freeMemory() + ">");
			log.error(om.getMessage());
			om.printStackTrace();
			
			throw new AppException("Se acab� la memoria.");
			
		} catch (Exception e) { // Ocurrio una excepcion
			
			log.error("generaArchivoTXTOrigen(Exception)");
			log.error("generaArchivoTXTOrigen.directorio  = <" + directorio  + ">");
			log.error("generaArchivoTXTOrigen.query    	 = <" + query       + ">");
			e.printStackTrace();
			throw new AppException("Ocurri� un error al generar el Archivo TXT.");
			
		} finally {
			
			if(rs  != null ){ try { rs.close(); }catch(Exception e){} }
			if(ps  != null ){ try { ps.close(); }catch(Exception e){} }
			
			// Cerrar archivo
			if(txt != null ){	try { txt.close();}catch(Exception e){} }
			
			// Cerrar conexion con la base de datos
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(true); // Debido a que se trata de una tabla remota
				con.cierraConexionDB();
			}
			log.info("generaArchivoTXTOrigen(S)");
		}
		
		
		return nombreArchivo;
	}
	
	/**
	 *
	 *	Crea un archivo TXT con el detalle de aquellos registros que presentaron errores en la carga
	 * masiva de comisiones.
	 *	@throws AppException 
	 *	
	 *	@param directorio <tt>String</tt> con el path del directorio donde se guardara el archivo generado.
	 *	@param query   	<tt>HashMap</tt> con el query para realizar la consulta.
	 *	
	 *	@return <tt>String</tt> con el nombre del archivo generado.
	 */
	public static String generaArchivoTXTErrores( String directorio, HashMap query )
		throws AppException{
			
		log.info("generaArchivoTXTErrores(E)");
		
		FileOutputStream		out 				= null;
		BufferedWriter 		txt 				= null;
		CreaArchivo 			archivo 			= null;
		String					nombreArchivo 	= null;
		
		AccesoDB 				con 				= new AccesoDB();
		String 					querySentencia	= null;
		List						parametros		= null;
		HashMap 					registro 		= null;
		ArrayList 				lista				= new ArrayList();
		ResultSet 				rs 				= null;
		PreparedStatement 	ps 				= null;
 
		try {
 
			// Conectarse a la base de datos
			con.conexionDB();
			
			// Obtener query
			querySentencia = (String) 	query.get("text");
			ps = con.queryPrecompilado(querySentencia);
			// Definir los parametros
			int indice  = 1;
			parametros	= (List)		query.get("parameters");	
			for(int i=0;i<parametros.size();i++){
				Object o = (Object) parametros.get(i);
				if(        o instanceof String     ){
					ps.setString(indice++, (String)   o );
				} else if( o instanceof Long       ){
					ps.setLong(indice++,   ((Long)    o ).longValue() );
				} else if( o instanceof Integer    ){
					ps.setInt(indice++,    ((Integer) o ).intValue() );
				} else if( o instanceof BigDecimal ){
					ps.setBigDecimal(indice++, (BigDecimal) o );
				}
			}
			// Realizar consulta
			rs = ps.executeQuery();
 
			// Crear Archivo
			archivo 			= new CreaArchivo();
			nombreArchivo	= archivo.nombreArchivo()+".txt";
			out 				= new FileOutputStream(directorio+nombreArchivo);
			txt 				= new BufferedWriter(new OutputStreamWriter(out, "Cp1252"));

			int 	i 						= 0;
			long 	cuentaRegistros	= 0L;
			while( rs.next()) {
 
				if(cuentaRegistros > 0) txt.write("\r\n");
				txt.write((rs.getString("CG_CONTENIDO")	== null)?"":rs.getString("CG_CONTENIDO") );

				// Realizar flush si es conveniente
				if((i%250) == 0){
					i = 0;
					txt.flush();
				}
				i++;
				
				// Actualizar contador de registros
				cuentaRegistros++;
		
			}
 
			// Verificar si se encontraron registros
			if( cuentaRegistros == 0 ){
				txt.write("No hay datos"+"\n");
			}
			txt.flush();
			
		}catch(OutOfMemoryError om) { // Se acabo la memoria
			
			log.error("generaArchivoTXTErrores(OutOfMemoryError)");
			log.error("generaArchivoTXTErrores.directorio   = <" + directorio                        + ">");
			log.error("generaArchivoTXTErrores.query    	   = <" + query                             + ">");
			log.error("generaArchivoTXTErrores::Free Memory = <" + Runtime.getRuntime().freeMemory() + ">");
			log.error(om.getMessage());
			om.printStackTrace();
			
			throw new AppException("Se acab� la memoria.");
			
		} catch (Exception e) { // Ocurrio una excepcion
			
			log.error("generaArchivoTXTErrores(Exception)");
			log.error("generaArchivoTXTErrores.directorio  = <" + directorio  + ">");
			log.error("generaArchivoTXTErrores.query    	  = <" + query       + ">");
			e.printStackTrace();
			throw new AppException("Ocurri� un error al generar el Archivo TXT.");
			
		} finally {
			
			if(rs  != null ){ try { rs.close(); }catch(Exception e){} }
			if(ps  != null ){ try { ps.close(); }catch(Exception e){} }
			
			// Cerrar archivo
			if(txt != null ){	try { txt.close();}catch(Exception e){} }
			
			// Cerrar conexion con la base de datos
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(true); // Debido a que se trata de una tabla remota
				con.cierraConexionDB();
			}
			log.info("generaArchivoTXTErrores(S)");
		}
		
		
		return nombreArchivo;
		
	}
 
	/**
	 *
	 *	Crea un archivo PDF con los resultados de la carga masiva de comisiones.
	 *
	 *	@throws AppException 
	 *	
	 *	@param directorio   				<tt>String</tt> con el path del directorio temporal donde se guardara el archivo generado.
	 *	@param query   	  				<tt>HashMap</tt> con el query para realizar la consulta.
	 * @param cabecera     				<tt>HashMap</tt> con la informacion de la cabcera del PDF.
	 * @param directorioPublicacion 	<tt>String</tt> con la ruta WEB del directorio de publicacion.
	 *	
	 *	@return <tt>String</tt> con el nombre del archivo generado.
	 */
	public static String generaArchivoPDFResultados( String directorio, String directorioPublicacion, HashMap queries, HashMap cabecera )
		throws AppException {

		log.info("generaArchivoPDFResultados(E)");
 
		AccesoDB 			con 				= new AccesoDB();
		PreparedStatement	ps					= null;
		ResultSet 			rs					= null;
		HashMap 				query 			= null;
		List					parametros		= null;
		String				queryString		= null;
		
		CreaArchivo 		archivo 			= new CreaArchivo();
		String 				nombreArchivo	= archivo.nombreArchivo()+".pdf";
		int 					i 					= 0;
		
		try {
 
			ComunesPDF pdfDoc 	= new ComunesPDF(1,directorio+nombreArchivo);

			String meses[] 		= { "Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre" };
			String fechaActual  	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    	= fechaActual.substring(0,2);
			String mesActual    	= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   	= fechaActual.substring(6,10);
			String horaActual  	= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
	
			String fechaOperacionFirme	      = "";
			String registrosSinErrores	      = "0";
			String registrosConErrores	      = "0";
			String totalRegistros		      = "0";
			String folioOperacion 				= "";
			String intermediarioFinanciero 	= "";	

			// Definir cabecera
			pdfDoc.encabezadoConImagenes(
				pdfDoc,
				(String) cabecera.get("strPais"),
				(String) cabecera.get("iNoNafinElectronico"),
				(String) cabecera.get("sesExterno"),					 
				(String) cabecera.get("strNombre"),
				(String) cabecera.get("strNombreUsuario"),
				(String) cabecera.get("strLogo"),
				directorioPublicacion
			);

			// Agregar fecha y localidad
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
	
			// Agregar titulo
			pdfDoc.addText("\n\n",											"formas",	ComunesPDF.CENTER);
			pdfDoc.addText("REPORTE DE RESULTADOS DE COMISIONES",	"formasB",	ComunesPDF.CENTER);
			
			// Conectarse a la Base de Datos
			con.conexionDB();
		
			// CONSULTAR RESULTADOS SOLICITUD
			query  			= (HashMap) queries.get("QUERY_RESULTADOS");
			// Obtener query Resultados Solicitud
			queryString	   = (String) 	query.get("text");
			ps = con.queryPrecompilado(queryString);
			// Definir los parametros
			int indice  	= 1;
			parametros		= (List) query.get("parameters");	
			for(int j=0;j<parametros.size();j++){
				Object o = (Object) parametros.get(j);
				if(        o instanceof String     ){
					ps.setString(indice++, (String)   o );
				} else if( o instanceof Long       ){
					ps.setLong(indice++,   ((Long)    o ).longValue() );
				} else if( o instanceof Integer    ){
					ps.setInt(indice++,    ((Integer) o ).intValue() );
				} else if( o instanceof BigDecimal ){
					ps.setBigDecimal(indice++, (BigDecimal) o );
				}
			}
			// Realizar consulta
			rs = ps.executeQuery();
			
			if(rs.next()){
 
				folioOperacion 			= (rs.getString("FOLIO_OPERACION")				== null)?"" :rs.getString("FOLIO_OPERACION");
				intermediarioFinanciero = (rs.getString("INTERMEDIARIO_FINANCIERO")	== null)?"" :rs.getString("INTERMEDIARIO_FINANCIERO");
				fechaOperacionFirme 		= (rs.getString("FECHA_OPERACION_FIRME")		== null)?"" :rs.getString("FECHA_OPERACION_FIRME");
				registrosSinErrores 		= (rs.getString("REGISTROS_SIN_ERRORES")		== null)?"0":rs.getString("REGISTROS_SIN_ERRORES");
				registrosConErrores 		= (rs.getString("REGISTROS_CON_ERRORES")		== null)?"0":rs.getString("REGISTROS_CON_ERRORES");
				totalRegistros 			= (rs.getString("TOTAL_REGISTROS")				== null)?"0":rs.getString("TOTAL_REGISTROS");
				
				// AGREGAR TITULO: RESULTADOS DE SOLICITUD DE CARGA DE PAGOS DE COMISIONES
				pdfDoc.setTable(1,100);
				pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
				pdfDoc.setCell("RESULTADOS DE SOLICITUD DE CARGA DE PAGOS DE COMISIONES","celda04",ComunesPDF.CENTER);
				pdfDoc.addTable();
		
				// AGREGAR FECHA DE OPERACION EN FIRME
				pdfDoc.addText("Fecha de Operaci�n en Firme: "+fechaOperacionFirme+"\n","formas",ComunesPDF.RIGHT);  
 
				// AGREGAR DETALLE DE RESULTADOS DE SOLICITUD DE CARGA DE PAGOS DE COMISIONES
				pdfDoc.setTable(5,95);
				
				pdfDoc.setCell("FOLIO DE LA OPERACION",                      "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("INTERMEDIARIO FINANCIERO",                   "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("REGISTROS SIN ERRORES",                      "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("REGISTROS CON ERRORES",                      "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("TOTAL DE REGISTROS PROCESADOS",              "celda01",ComunesPDF.CENTER);
		
				pdfDoc.setCell(folioOperacion,                     		 	 "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(intermediarioFinanciero,                      "formas", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(registrosSinErrores,0),"formas", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(registrosConErrores,0),"formas", ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(totalRegistros,		 0),"formas", ComunesPDF.CENTER);			
		
				pdfDoc.addTable();
				
			}
			ps.close();	
			rs.close();
			
			// Agregar espacio
			pdfDoc.addText("\n\n\n","formas",ComunesPDF.LEFT);	
			
			// CONSULTAR DETALLE COMISIONES
			query  			= (HashMap) queries.get("QUERY_DETALLE");
			// Obtener query Detalle Comisiones
			queryString	   = (String) 	query.get("text");
			
			ps = con.queryPrecompilado(queryString);
			// Definir los parametros
			indice  	= 1;
			parametros		= (List) query.get("parameters");	
			for(int j=0;j<parametros.size();j++){
				Object o = (Object) parametros.get(j);
				if(        o instanceof String     ){
					ps.setString(indice++, (String)   o );
				} else if( o instanceof Long       ){
					ps.setLong(indice++,   ((Long)    o ).longValue() );
				} else if( o instanceof Integer    ){
					ps.setInt(indice++,    ((Integer) o ).intValue() );
				} else if( o instanceof BigDecimal ){
					ps.setBigDecimal(indice++, (BigDecimal) o );
				}
			}
			// Realizar consulta
			rs = ps.executeQuery();
			
			i  = 0;
			while(rs.next()){
					
				if(i==0){
						
					pdfDoc.setTable(1,100);
					pdfDoc.setCell("DETALLE DE COMISIONES POR MONEDA",                           "celda04",ComunesPDF.CENTER);
					pdfDoc.addTable();
						
					pdfDoc.addText("\n",                                                         "formas", ComunesPDF.RIGHT );
						
					pdfDoc.setTable(5,95);
					pdfDoc.setCell("MONEDA",                                                     "celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("REGISTROS SIN ERRORES",                                      "celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("REGISTROS CON ERRORES",                                      "celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("TOTAL DE REGISTROS",                                         "celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("IMPORTE DE COMISI�N E IVA DE REGISTROS ACEPTADOS",           "celda01",ComunesPDF.CENTER);
						
				}
			
				String nombreMoneda 						= (rs.getString("NOMBRE_MONEDA")					== null)?"" :rs.getString("NOMBRE_MONEDA");
				String detalleRegistrosSinErrores	= (rs.getString("REGISTROS_SIN_ERRORES")		== null)?"0":rs.getString("REGISTROS_SIN_ERRORES");
				String detalleRegistrosConErrores	= (rs.getString("REGISTROS_CON_ERRORES")		== null)?"0":rs.getString("REGISTROS_CON_ERRORES");
				String detalleTotalRegistros			= (rs.getString("TOTAL_REGISTROS")				== null)?"0":rs.getString("TOTAL_REGISTROS");
				String importeComisionEIva				= (rs.getString("IMPORTE_COMISION_E_IVA")		== null)?"0":rs.getString("IMPORTE_COMISION_E_IVA");
				
				pdfDoc.setCell(nombreMoneda,                                    			"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(detalleRegistrosSinErrores, 0),   "formas",ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(detalleRegistrosConErrores, 0),   "formas",ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(detalleTotalRegistros,		0),   "formas",ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(importeComisionEIva,			2), 	"formas",ComunesPDF.CENTER);
				i++;
					
			}	// while
			rs.close();
			ps.close();
				
			if(i>0){
				pdfDoc.addTable();
			}
	
			// Agregar espacio
			pdfDoc.addText("\n\n\n","formas",ComunesPDF.LEFT);	
			
			// CONSULTAR DETALLE ERRORES 
			query  			= (HashMap) queries.get("QUERY_ERRORES");
			// Obtener query Detalle Errores 
			queryString	   = (String) 	query.get("text");
			ps = con.queryPrecompilado(queryString);
			// Definir los parametros
			indice  	= 1;
			parametros		= (List) query.get("parameters");	
			for(int j=0;j<parametros.size();j++){
				Object o = (Object) parametros.get(j);
				if(        o instanceof String     ){
					ps.setString(indice++, (String)   o );
				} else if( o instanceof Long       ){
					ps.setLong(indice++,   ((Long)    o ).longValue() );
				} else if( o instanceof Integer    ){
					ps.setInt(indice++,    ((Integer) o ).intValue() );
				} else if( o instanceof BigDecimal ){
					ps.setBigDecimal(indice++, (BigDecimal) o );
				}
			}
			// Realizar consulta
			rs = ps.executeQuery();
			
			i=0;
			while(rs.next()){
				
				if(i==0){
					
					// AGREGAR CABECERA CON EL DETALLE DE LOS ERRORES GENERADOS
					pdfDoc.setTable(1,100);
					pdfDoc.setCell("ERRORES GENERADOS",            "celda04", ComunesPDF.CENTER );
					pdfDoc.addTable();

				}
				
				String detalleError = (rs.getString("DETALLE_ERROR") == null)?"":rs.getString("DETALLE_ERROR"); 
				
				pdfDoc.addText(detalleError + "\n", "formasmen",  ComunesPDF.LEFT   );
				i++;
				
			}
			ps.close();
			rs.close();
 
			pdfDoc.endDocument();
 
		}catch(OutOfMemoryError om) { // Se acabo la memoria
			
			log.error("generaArchivoPDFResultados(OutOfMemoryError)");
			log.error("generaArchivoPDFResultados.directorio   = <" + directorio                        + ">");
			log.error("generaArchivoPDFResultados.queries      = <" + queries                           + ">");
			log.error("generaArchivoPDFResultados.cabecera     = <" + cabecera                          + ">");
			log.error("generaArchivoPDFResultados.query    	   = <" + query                             + ">");
			log.error("generaArchivoPDFResultados::Free Memory = <" + Runtime.getRuntime().freeMemory() + ">");
			log.error(om.getMessage());
			om.printStackTrace();
			
			throw new AppException("Se acab� la memoria.");
			
		} catch (Exception e) { // Ocurrio una excepcion
			
			log.error("generaArchivoPDFResultados(Exception)");
			log.error("generaArchivoPDFResultados.directorio   = <" + directorio + ">");
			log.error("generaArchivoPDFResultados.queries      = <" + queries    + ">");
			log.error("generaArchivoPDFResultados.cabecera     = <" + cabecera   + ">");
			log.error("generaArchivoPDFResultados.query    	   = <" + query      + ">");
			e.printStackTrace();
			
			throw new AppException("Ocurri� un error al generar el Archivo PDF.");
			
		} finally {
			
			if(rs  != null ){ try { rs.close(); }catch(Exception e){} }
			if(ps  != null ){ try { ps.close(); }catch(Exception e){} }
			
			// Cerrar conexion con la base de datos
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			log.info("generaArchivoPDFResultados(S)");
		}
		
		return nombreArchivo;
		
	}
	
}

