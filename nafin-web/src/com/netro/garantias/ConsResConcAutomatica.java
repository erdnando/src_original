package com.netro.garantias;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsResConcAutomatica implements IQueryGeneratorRegExtJS {
	
	private String cve_siag;
	private String cve_fiso;
	private String tipo_conc;
	private String cve_sit;
	private String fec_car_ini;
	private String fec_car_fin;
	

	public ConsResConcAutomatica() {
		//Constructor sin args.
	}
	
	private static final Log log = ServiceLocator.getInstance().getLog(ConsResConcAutomatica.class);//Variable para enviar mensajes al log.
	
	public String getAggregateCalculationQuery()	{
		
		StringBuffer 	qrySentencia 	= new StringBuffer();
		return qrySentencia.toString();
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds)  {
		StringBuffer qrySentencia = new StringBuffer();
		return qrySentencia.toString();
	}
	
	public String getDocumentQuery()	{
	
		StringBuffer qrySentencia = new StringBuffer();
		
		log.debug("getDocumentQuery)");
		return qrySentencia.toString();
	}
	
	public String getDocumentQueryFile() {
		this.variablesBind = new ArrayList();
		StringBuffer 	qrySentencia 	= new StringBuffer();
    	StringBuffer	condicion 		= new StringBuffer();
	
		try{
			qrySentencia.append(
						" SELECT p.ic_mes_conciliar as CVE_MES, p.ic_fiso as CVE_FISO, p.ic_if_siag as CVE_IF_SIAG, p.ic_tipo_conciliacion as TIPO_CONC, TO_CHAR (p.df_fecha_carga, 'DD/MM/YYYY HH:MI AM') as FEC_CARGA, ('0'||p.ic_situacion_conciliacion ||'-'|| c.cg_descripcion) as SITUACION, "+
						" p.ic_mes_conciliar as MES_CONC, p.in_num_conciliados as CONCILIADOS, p.in_num_no_conciliados as NO_CONC, "+
						" p.fn_monto_carga as MONTO_ENVIADO, p.in_tot_registros_portafolio_if as TOT_OPE, "+
						" nvl2(dbms_lob.getlength(p.BI_ARCHIVO_PORTAFOLIO_IF), 'S', 'N') as ORIGEN, "+
						" nvl2(dbms_lob.getlength(p.BI_ARCHIVO_REPORTE_RESULTADOS), 'S', 'N') as RESULTADOS, "+
						" nvl2(dbms_lob.getlength(p.BI_ARCHIVO_REPORTE_DETALLE), 'S', 'N') as DETALLE "+
						" FROM GTI_CONC_ARCHIVO_PORTAFOLIO p, GTICAT_SITUACION_CONCILIACION c "+
						" WHERE p.ic_fiso= ? and p.ic_if_siag = ? and p.ic_tipo_conciliacion = ? and p.ic_situacion_conciliacion = c.ic_situacion_conciliacion ");
						
						this.variablesBind.add(cve_fiso);
						this.variablesBind.add(cve_siag);
						this.variablesBind.add(tipo_conc);
						
						
			if(!cve_sit.equals("")){
				condicion.append(" AND p.ic_situacion_conciliacion = ? ");
					this.variablesBind.add(cve_sit);
			}else{
				condicion.append(" AND p.ic_situacion_conciliacion in (3, 4, 6, 7) ");
			}
			
			if(!"".equals(fec_car_ini)&&!"".equals(fec_car_fin)){
				condicion.append("    AND p.df_fecha_carga >= TO_DATE (?, 'dd/mm/yyyy') ");
				condicion.append("    AND p.df_fecha_carga < TRUNC (TO_DATE (?, 'dd/mm/yyyy') + 1) ");
				this.variablesBind.add(fec_car_ini);
				this.variablesBind.add(fec_car_fin);
			}
			
			qrySentencia.append(condicion.toString());

			
		}catch (Exception e){
			log.debug("ConsResConcAutomatica:: getDocumentQueryException "+e);
		}
	
		log.debug("getDocumentQueryFile(S) "+qrySentencia.toString());
		return qrySentencia.toString();
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String nombreArchivo = "";
		log.debug("crearCustomFile (E)");

		return nombreArchivo;
	}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		log.debug("crearPageCustomFile (E)");

		return nombreArchivo;
	}
	
	public List getConditions() {
		log.debug("*************getConditions=" + variablesBind);
		return variablesBind;
	}
	
	private ArrayList variablesBind = null;


	/**
	 * 
	 * GETTER'S & SETTER'S
	 */
	public void setTipo_conc(String tipo_conc) {
		this.tipo_conc = tipo_conc;
	}


	public String getTipo_conc() {
		return tipo_conc;
	}


	public void setCve_sit(String cve_sit) {
		this.cve_sit = cve_sit;
	}


	public String getCve_sit() {
		return cve_sit;
	}


	public void setFec_car_ini(String fec_car_ini) {
		this.fec_car_ini = fec_car_ini;
	}


	public String getFec_car_ini() {
		return fec_car_ini;
	}


	public void setFec_car_fin(String fec_car_fin) {
		this.fec_car_fin = fec_car_fin;
	}


	public String getFec_car_fin() {
		return fec_car_fin;
	}


	public void setCve_siag(String cve_siag) {
		this.cve_siag = cve_siag;
	}


	public String getCve_siag() {
		return cve_siag;
	}


	public void setCve_fiso(String cve_fiso) {
		this.cve_fiso = cve_fiso;
	}


	public String getCve_fiso() {
		return cve_fiso;
	}
	
}