package com.netro.garantias;

import com.netro.exception.NafinException;
import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsDesembolsoMasivo implements IQueryGeneratorReg , IQueryGeneratorRegExtJS{
	public ConsDesembolsoMasivo()  { 	}
//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsDesembolsoMasivo.class);
		
	StringBuffer 		qrySentencia;
	private List 		conditions;
	private String 	paginaOffset;
	private String 	paginaNo;
		
	private String intermediario;
	private String foperacionIni;
	private String foperacionFin;
	private String situacion;
	private String folio; 

			 
	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */

	public String getAggregateCalculationQuery() {
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		qrySentencia.append("  SELECT   /*+ index(es) index(i)  use_nl(es s i )*/ "); 
    qrySentencia.append("  count(1) total "); 	
		qrySentencia.append(" FROM gticat_situacion s, "); 
		qrySentencia.append("	     gti_estatus_solic es, "); 
		qrySentencia.append("		   comcat_if i "); 
		qrySentencia.append(" WHERE es.ic_situacion = s.ic_situacion "); 
		qrySentencia.append("	 AND es.ic_tipo_operacion = s.ic_tipo_operacion "); 
		qrySentencia.append("	 AND es.ic_if_siag = i.ic_if_siag  "); 		
		qrySentencia.append("	 AND es.ic_tipo_operacion = 6 ");
		 
		if(!intermediario.equals("")){ 		
			qrySentencia.append(" AND i.ic_if = ? ") ;
			conditions.add(intermediario);
		}
		
		if(!foperacionIni.equals("") &&  !foperacionFin.equals("") ){  		
			qrySentencia.append(" AND es.df_fecha_hora BETWEEN TO_DATE (?, 'DD/MM/YYYY') "); 
			qrySentencia.append(" AND TO_DATE (?, 'DD/MM/YYYY') + 1 "); 
			conditions.add(foperacionIni);
			conditions.add(foperacionFin);
		}	
		
			if(!situacion.equals("") ){ 			
			qrySentencia.append(" AND es.ic_situacion = ? ") ;
			conditions.add(situacion);
		}
		
		if(!folio.equals("")){ 		
			qrySentencia.append(" AND es.ic_folio = ? ") ;
			conditions.add(folio);
		}
		 
		qrySentencia.append(" ORDER BY es.df_fecha_hora DESC "); 
	
			log.debug("getAggregateCalculationQuery "+conditions.toString());
			log.debug("getAggregateCalculationQuery "+qrySentencia.toString());
			return qrySentencia.toString();
				
	}
	
	
	 /**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery(){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		
		qrySentencia.append("  SELECT    /*+ index(es) index(i)  use_nl(es s i )*/ ");
		qrySentencia.append("  i.ic_if AS clave_if ,");
	  qrySentencia.append("  i.cg_razon_social AS nombre_if, "); 
    qrySentencia.append("  TO_CHAR (es.df_fecha_hora, 'dd/mm/yyyy hh24:mi') AS fecha, "); 
    qrySentencia.append("  es.ic_folio as folio,  "); 
    qrySentencia.append("  s.cg_descripcion AS situacion, "); 
    qrySentencia.append("  es.fn_impte_total AS monto_enviado, "); 
		qrySentencia.append("	 es.in_registros_acep AS in_registros_acep,"); 
		qrySentencia.append("  es.in_registros_rech AS in_registros_rech,"); 
		qrySentencia.append("  es.in_registros AS in_registros,  "); 
		qrySentencia.append("  es.ic_situacion as ic_situacion , "); 		
		qrySentencia.append("  es.ic_if_siag as claveSiag ");		
		
		qrySentencia.append(" FROM gticat_situacion s, "); 
		qrySentencia.append("			 gti_estatus_solic es, "); 
		qrySentencia.append("			 comcat_if i "); 
		qrySentencia.append(" WHERE es.ic_situacion = s.ic_situacion "); 
		qrySentencia.append("	 AND es.ic_tipo_operacion = s.ic_tipo_operacion "); 
		qrySentencia.append("	 AND es.ic_if_siag = i.ic_if_siag  "); 
		qrySentencia.append("	 AND es.ic_tipo_operacion = 6 ");
		
		 
		if(!intermediario.equals("")){ 		
			qrySentencia.append(" AND i.ic_if = ? ") ;
			conditions.add(intermediario);
		}
		
		if(!foperacionIni.equals("") &&  !foperacionFin.equals("") ){  		
			qrySentencia.append(" AND es.df_fecha_hora BETWEEN TO_DATE (?, 'DD/MM/YYYY') "); 
			qrySentencia.append(" AND TO_DATE (?, 'DD/MM/YYYY') + 1 "); 
			conditions.add(foperacionIni);
			conditions.add(foperacionFin);
		}	
		
		if(!situacion.equals("") ){ 	
			qrySentencia.append(" AND es.ic_situacion = ? ") ;
			conditions.add(situacion);
		}
		
		if(!folio.equals("")){ 		
			qrySentencia.append(" AND es.ic_folio = ? ") ;
			conditions.add(folio);
		}
		 
		qrySentencia.append(" ORDER BY es.df_fecha_hora DESC ");
		
			
			log.debug("getDocumentQuery "+conditions.toString());
			log.debug("getDocumentQuery "+qrySentencia.toString());
			return qrySentencia.toString();
	}

	/**
	 * Obtiene el query necesario para mostrar la información completa de 
	 * una página a partir de las llaves primarias enviadas como parámetro
	 * @return Cadena con la consulta de SQL, para obtener la información
	 * 	completa de los registros con las llaves especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		qrySentencia.append("  SELECT /*+ index(es) index(i)  use_nl(es s i )*/ ");
		qrySentencia.append("  i.ic_if AS clave_if ,");
	  qrySentencia.append("  i.cg_razon_social AS nombre_if, "); 
    qrySentencia.append("  TO_CHAR (es.df_fecha_hora, 'dd/mm/yyyy hh24:mi') AS fecha, "); 
    qrySentencia.append("  es.ic_folio as folio,  "); 
    qrySentencia.append("  s.cg_descripcion AS situacion, "); 
    qrySentencia.append("  es.fn_impte_total AS monto_enviado, "); 
		qrySentencia.append("	 es.in_registros_acep AS in_registros_acep,"); 
		qrySentencia.append("  es.in_registros_rech AS in_registros_rech,"); 
		qrySentencia.append("  es.in_registros AS in_registros,  "); 
		qrySentencia.append("  es.ic_situacion as ic_situacion,  "); 
		qrySentencia.append("  es.ic_if_siag as claveSiag ");	
		
		qrySentencia.append(" FROM gticat_situacion s, "); 
		qrySentencia.append("			 gti_estatus_solic es, "); 
		qrySentencia.append("	     comcat_if i "); 
		qrySentencia.append(" WHERE es.ic_situacion = s.ic_situacion "); 
		qrySentencia.append("	 AND es.ic_tipo_operacion = s.ic_tipo_operacion "); 
		qrySentencia.append("	 AND es.ic_if_siag = i.ic_if_siag  "); 
		qrySentencia.append("	 AND es.ic_tipo_operacion = 6 ");
		
		if(!intermediario.equals("")){ 		
			qrySentencia.append(" AND i.ic_if = ? ") ;
			conditions.add(intermediario);
		}
		
		if(!foperacionIni.equals("") &&  !foperacionFin.equals("") ){  		
			qrySentencia.append(" AND es.df_fecha_hora BETWEEN TO_DATE (?, 'DD/MM/YYYY') "); 
			qrySentencia.append(" AND TO_DATE (?, 'DD/MM/YYYY') + 1 "); 
			conditions.add(foperacionIni);
			conditions.add(foperacionFin);
		}	
		
		if(!situacion.equals("") ){ 		
			qrySentencia.append(" AND es.ic_situacion = ? ") ;
			conditions.add(situacion);
		}
		
		if(!folio.equals("")){ 		
			qrySentencia.append(" AND es.ic_folio = ? ") ;
			conditions.add(folio);
		}
		 
		qrySentencia.append(" ORDER BY es.df_fecha_hora DESC ");
			
			
		log.debug("getDocumentSummaryQueryForIds "+conditions.toString());
		log.debug("getDocumentSummaryQueryForIds "+qrySentencia.toString());
		return qrySentencia.toString();
	}
	
	
	

	public String getDocumentQueryFile(){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();

		
	
			log.debug("getDocumentQueryFile "+conditions.toString());
			log.debug("getDocumentQueryFile "+qrySentencia.toString());
		return qrySentencia.toString();
	}//getDocumentQueryFile
	


/*****************************************************
	 GETTERS
*******************************************************/
 
	/**
	  Obtiene la lista de parámetros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
		
	
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}


public String getIntermediario() {
		return intermediario;
	}

	public void setIntermediario(String intermediario) {
		this.intermediario = intermediario;
	}

	public String getFoperacionIni() {
		return foperacionIni;
	}

	public void setFoperacionIni(String foperacionIni) {
		this.foperacionIni = foperacionIni;
	}

	public String getFoperacionFin() {
		return foperacionFin;
	}

	public void setFoperacionFin(String foperacionFin) {
		this.foperacionFin = foperacionFin;
	}

	public String getSituacion() {
		return situacion;
	}

	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

/**
 * MIGRACIÓN
 */
	public String descargarArchivos(String path, String opcion){
		String nombreArchivo = "";
		String linea = "";
		String espacio = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		
		AccesoDB 				   con			= new AccesoDB();
		PreparedStatement  ps 			= null;
		ResultSet 				 rs 			= null;
		StringBuffer       query 		= new StringBuffer();		
		try{
			con.conexionDB();
			String tabla ="";
			if(opcion.equals("origen")){
				tabla = "gti_contenido_arch";
				
				nombreArchivo = "ArchivoOrigen_"+Comunes.cadenaAleatoria(3)+".txt";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			}else if(opcion.equals("errores")){
				tabla = "gti_arch_rechazo";
				nombreArchivo = "ArchivoErrores_"+Comunes.cadenaAleatoria(3)+".txt";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			}
			query.append(" select cg_contenido ");
				query.append("  from "+tabla+" A");
				query.append(" ,comcat_if I ");
				query.append(" where A.ic_if_siag = I.ic_if_siag ");
				query.append(" and A.ic_folio = ? ");
				query.append(" and I.ic_if = ? ");
				query.append(" order by A.ic_linea ");
	
				log.debug("query Archivo Origen : " +query.toString());
			
				ps = con.queryPrecompilado(query.toString());	
				ps.setLong(1, Long.parseLong(folio));
				ps.setLong(2, Long.parseLong(intermediario));
				rs = ps.executeQuery();	
				
				
				long i =0;
				int hayRegsitros = 0;
				buffer = new BufferedWriter(writer);
				while (rs.next()) {
					String contenido = (rs.getString("CG_CONTENIDO")==null?"":rs.getString("CG_CONTENIDO").replace(',',' ') );
					buffer.write(contenido);
					buffer.write("\r\n"); 
								
					if((i%500L) == 0L) buffer.flush(); 
						i++;
						hayRegsitros++;
				}//termina while
				rs.close();	
				ps.close();	
				
				if(opcion.equals("errores")){
						buffer.write("Cantidad de Registros Rechazados : "+hayRegsitros );
				}
				
				
				if(hayRegsitros==0){
					buffer.write( " No Existe informacion. " );						
					buffer.write( "\r\n");							
				}				
					
				if(buffer 	!= null )  	buffer.close();
			
		}catch (Throwable e) {
			throw new AppException("Error al generar el archivo",e);
		}
		finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
		
		return nombreArchivo;
		
	
	}
	public String generaPDF(HttpServletRequest request,String rutaFisica, String rutaVirtual,String opcion, String claveSiag )	throws NafinException {				
		String 		nombreArchivo 	= null;
		CreaArchivo archivo 			= null;
		ComunesPDF 	pdfDoc			= null;
		
		log.info(" generaPDF  (E)");
		AccesoDB con          = new AccesoDB();	
		try{
				
			con.conexionDB();
		
			archivo			= new CreaArchivo();
			nombreArchivo	= archivo.nombreArchivo()+".pdf";
			pdfDoc			= new ComunesPDF(2,rutaFisica+nombreArchivo, "", false, true, true);
			
			//recupera los datos para generacion del Archivo
			
			String pais        		= (String)(request.getSession().getAttribute("strPais") == null?"":request.getSession().getAttribute("strPais"));
			String noCliente   		= (String)(request.getSession().getAttribute("iNoCliente") == null?" ":request.getSession().getAttribute("iNoCliente"));
			String strnombre   		= (String)(request.getSession().getAttribute("strNombre") == null?"":request.getSession().getAttribute("strNombre"));
			String nombreUsr   		= (String)(request.getSession().getAttribute("strNombreUsuario") == null?"":request.getSession().getAttribute("strNombreUsuario"));
			String logo      	 		= (String)(request.getSession().getAttribute("strLogo") == null?"":request.getSession().getAttribute("strLogo"));
			if(noCliente.equals("0")){
				noCliente = "";
			}
			
			PreparedStatement	ps  = null;
			ResultSet rs          = null;
			ResultSet rsArchi     = null;
			ResultSet rsContrInv  = null;
			StringBuffer sArchi   = new StringBuffer();			
			StringBuffer qrySentencia   = new StringBuffer();
			PreparedStatement	psf  = null;
			ResultSet rsFechaHonrada  = null;

			String meses[]        = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual    = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual      = fechaActual.substring(0,2);
			String mesActual      = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual     = fechaActual.substring(6,10);
			String horaActual     = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			ResultSet resultado = null;
			PreparedStatement	psresultado  = null;
			int i = 0;
			String  fechaHonrada = "",	moneda = "", fechap = "", fechad = "";
			
						
			qrySentencia   = new StringBuffer();
			qrySentencia.append(" SELECT E.IC_FOLIO, ");
			qrySentencia.append("	 i.CG_RAZON_SOCIAL, ");
			qrySentencia.append("	 E.IN_REGISTROS_ACEP, ");
			qrySentencia.append("	 E.IN_REGISTROS_RECH,  ");
			qrySentencia.append("	 E.IN_REGISTROS, ");
			qrySentencia.append("	 s.cg_descripcion  ");
			qrySentencia.append("	 FROM GTI_ESTATUS_SOLIC E, ");
			qrySentencia.append("	      gticat_situacion s,  ");
			qrySentencia.append("	      comcat_if i ");
			qrySentencia.append("	 WHERE e.ic_if_siag = i.ic_if_siag  ");
			qrySentencia.append("	 AND e.ic_situacion = s.ic_situacion ");
			qrySentencia.append("	 AND e.ic_tipo_operacion = s.ic_tipo_operacion  ");
			qrySentencia.append("	 AND E.IC_TIPO_OPERACION  = ? ");
			qrySentencia.append("	 AND e.ic_folio = ?   ");
			qrySentencia.append("	 AND i.ic_if = ? ");
			
			psresultado = con.queryPrecompilado(qrySentencia.toString());
			psresultado.setInt(1, 6); 
			psresultado.setString(2,	folio);
			psresultado.setInt(3,	Integer.parseInt(intermediario));
			resultado = psresultado.executeQuery();
		
		  log.debug("qrySentencia::"+qrySentencia.toString());
			
			qrySentencia   = new StringBuffer();
			qrySentencia.append( " SELECT  i.cg_razon_social, ");
			qrySentencia.append( "   SUM(CASE WHEN (D.cg_moneda ='PESOS')  THEN  d.IN_NUM_REGISTROS_ACEP END) AS aceptadosP,");
		  qrySentencia.append("    SUM(CASE WHEN (D.cg_moneda ='DOLARES') THEN  d.IN_NUM_REGISTROS_ACEP END) AS aceptadosD,");
		  qrySentencia.append("    SUM(CASE WHEN (D.cg_moneda ='PESOS')  THEN  d.IN_NUM_REGISTROS_RECH END) AS rechazadosP,");
		  qrySentencia.append("    SUM(CASE WHEN (D.cg_moneda ='DOLARES' ) THEN  d.IN_NUM_REGISTROS_RECH END) AS rechazadosD, ");
			qrySentencia.append( "   SUM(CASE WHEN (D.cg_moneda ='PESOS' )  THEN  d.IN_NUM_REGISTROS  END) AS totalregistrosP, ");
			qrySentencia.append("    SUM(CASE WHEN (D.cg_moneda ='DOLARES') THEN  d.IN_NUM_REGISTROS  END) AS totalregistrosD, ");
			qrySentencia.append("    SUM(CASE WHEN (D.cg_moneda ='PESOS')  THEN  d.fn_capital END) AS capitalP, ");
			qrySentencia.append("    SUM(CASE WHEN (D.cg_moneda ='DOLARES') THEN  d.fn_capital END) AS capitalD,");
			qrySentencia.append("    SUM(CASE WHEN (D.cg_moneda ='PESOS')  THEN  d.FN_INTERESES_ORDINARIOS END) AS intordinarioP, ");
			qrySentencia.append("    SUM(CASE WHEN (D.cg_moneda ='DOLARES') THEN  d.FN_INTERESES_ORDINARIOS END) AS intordinarioD,");
			qrySentencia.append("    SUM(CASE WHEN (D.cg_moneda ='PESOS')  THEN  d.FN_INTERESES_MORATORIOS END) AS intmoratorioP, ");
			qrySentencia.append("    SUM(CASE WHEN (D.cg_moneda ='DOLARES') THEN  d.FN_INTERESES_MORATORIOS END) AS intmoratorioD,");
			qrySentencia.append("    SUM(CASE WHEN (D.cg_moneda ='PESOS')  THEN  d.FN_COMISION_ANIVERSARIO END) AS comianiversarioP, ");
			qrySentencia.append("    SUM(CASE WHEN (D.cg_moneda ='DOLARES') THEN  d.FN_COMISION_ANIVERSARIO END) AS comianiversarioD,");
			qrySentencia.append("    SUM(CASE WHEN (D.cg_moneda ='PESOS')  THEN  d.FN_IVA_COMISION_ANIVERSARIO END) AS ivaniversarioP, ");
			qrySentencia.append("    SUM(CASE WHEN (D.cg_moneda ='DOLARES') THEN  d.FN_IVA_COMISION_ANIVERSARIO END) AS ivaniversarioD, ");
			qrySentencia.append("    SUM(CASE WHEN (D.cg_moneda ='PESOS')  THEN  nvl(d.fn_capital,0) +nvl(d.FN_INTERESES_ORDINARIOS,0) +nvl(d.FN_INTERESES_MORATORIOS,0)  END) AS totalResembolsoP, ");
			qrySentencia.append("    SUM(CASE WHEN (D.cg_moneda ='DOLARES') THEN  nvl(d.fn_capital,0) +nvl(d.FN_INTERESES_ORDINARIOS,0) +nvl(d.FN_INTERESES_MORATORIOS,0)  END) AS totalResembolsoD,  ");
			qrySentencia.append("    SUM(CASE WHEN (D.cg_moneda ='PESOS')  THEN  nvl(d.FN_COMISION_ANIVERSARIO,0) +nvl(d.FN_IVA_COMISION_ANIVERSARIO,0)  END) AS ComisionAniversarioP,   ");
			qrySentencia.append("    SUM(CASE WHEN (D.cg_moneda ='DOLARES') THEN  nvl(d.FN_COMISION_ANIVERSARIO,0) +nvl(d.FN_IVA_COMISION_ANIVERSARIO,0)  END) AS ComisionAniversarioD   ");
			qrySentencia.append(" FROM gti_detalle_desmasivos d, ");
			qrySentencia.append("    gti_estatus_solic e, ");
			qrySentencia.append("    comcat_if i ");
			qrySentencia.append(" WHERE e.ic_folio = d.ic_folio_operacion  ");
			qrySentencia.append(" AND e.ic_if_siag = i.ic_if_siag ");
			qrySentencia.append(" AND e.ic_tipo_operacion = ? ");
			qrySentencia.append(" AND e.ic_folio = ? ");
			qrySentencia.append(" AND i.ic_if = ? ");
			qrySentencia.append(" GROUP BY i.cg_razon_social");
			qrySentencia.append(" ORDER BY i.cg_razon_social ");
		 
			log.debug("qrySentencia::"+qrySentencia.toString());
		
			
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1, 6);
			ps.setString(2,	folio);
			ps.setInt(3,	Integer.parseInt(intermediario));
			rs = ps.executeQuery();
		
		  qrySentencia   = new StringBuffer();
			qrySentencia.append( " SELECT CG_CONTENIDO ");
			qrySentencia.append( " FROM gti_arch_resultado ");
			qrySentencia.append( " WHERE IC_IF_SIAG         = ? ");
			qrySentencia.append( " AND   IC_FOLIO           = ? ");
			qrySentencia.append( " ORDER BY IC_LINEA ");
		
			log.debug(qrySentencia);
			log.debug("claveSiag  "+claveSiag+" folio "+folio);
			
			ps = con.queryPrecompilado(qrySentencia.toString());
			ps.setInt(1,	Integer.parseInt(claveSiag));
			ps.setString(2,	folio);
			rsArchi = ps.executeQuery();
		
			while(rsArchi.next()){
				sArchi.append( ((rsArchi.getString("CG_CONTENIDO") == null)?"":rsArchi.getString("CG_CONTENIDO")+"\n") );
			}
			sArchi.append("\n ");
			rsArchi.close();
			ps.close();
			
			
			qrySentencia   = new StringBuffer();
			qrySentencia.append( "select s.ic_moneda as ic_moneda ,TO_CHAR(s.df_fecha_honrada,'dd/mm/yyyy') AS fecha  ");
			qrySentencia.append(							" from  gti_solicitud_pago s ");
			qrySentencia.append( " where  s.ic_folio = ?  ");
			qrySentencia.append( " GROUP BY s.ic_moneda,s.df_fecha_honrada ");
		
			log.debug("qrySentencia::"+qrySentencia.toString());
		
			psf = con.queryPrecompilado(qrySentencia.toString());
			psf.setString(1,	folio);	
			rsFechaHonrada = psf.executeQuery();
		
			while(rsFechaHonrada.next()){
				fechaHonrada = ((rsFechaHonrada.getString("fecha") == null)?"":rsFechaHonrada.getString("fecha")+"\n");
				moneda = ((rsFechaHonrada.getString("ic_moneda") == null)?"":rsFechaHonrada.getString("ic_moneda")+"\n");
			}
				
			rsFechaHonrada.close();
			psf.close();
		
		
		//Inicia la creacion del pDF		
																			 
				pdfDoc.setEncabezado(pais, noCliente, "", strnombre, nombreUsr, logo, rutaVirtual, "");		
				pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);	
			
				float widthtb0[] = {100f};
				pdfDoc.setTable(1, 100, widthtb0);
				pdfDoc.addTable();
				pdfDoc.addText("REPORTE DE RESULTADOS  ","formasG",ComunesPDF.CENTER);
				
			 if (resultado.next()){
		 
					String sIc_folio             = (resultado.getString("IC_FOLIO") == null)?"":resultado.getString("IC_FOLIO") ;
					String sIntermediario       = (resultado.getString("CG_RAZON_SOCIAL") == null)?"":resultado.getString("CG_RAZON_SOCIAL") ;
					String sRegSinError          = (resultado.getString("IN_REGISTROS_ACEP") == null)?"":resultado.getString("IN_REGISTROS_ACEP") ;
					String sRegConError          = (resultado.getString("IN_REGISTROS_RECH") == null)?"":resultado.getString("IN_REGISTROS_RECH") ;
					String sTotalRegistros       = (resultado.getString("IN_REGISTROS") == null)?"":resultado.getString("IN_REGISTROS") ;
					String sDescripSituacion       = (resultado.getString("cg_descripcion") == null)?"":resultado.getString("cg_descripcion") ;
		 
					float widthtb1[] = {100f};
					pdfDoc.setTable(1, 100, widthtb1);
					pdfDoc.addTable();
					pdfDoc.addText("Situacion de la Solicitud: "+sDescripSituacion+"\n","formas",ComunesPDF.LEFT);
				
					float widthtb2[] = {100f};    
					pdfDoc.setTable(1, 100, widthtb2);
					pdfDoc.setCell("RESULTADO DE SOLICITUD DE DESEMBOLSOS MASIVOS","celda01",ComunesPDF.CENTER,1,1,1);
					pdfDoc.addTable();
				
					float widthtb3[] = {16.6f,16.6f,16.6f};
					pdfDoc.setTable(3, 100, widthtb3);
					//row 1
					pdfDoc.setCell("Folio de Operación"+"\n ","formasrep",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sIc_folio+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
					pdfDoc.setCell(""+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
					pdfDoc.setCell("Intermediario Financiero"+"\n ","formasrep",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sIntermediario+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
					pdfDoc.setCell(""+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
					pdfDoc.setCell("Registros Sin Error "+"\n ","formasrep",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sRegSinError+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
					pdfDoc.setCell(""+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
					pdfDoc.setCell("Registros Con Error"+"\n ","formasrep",ComunesPDF.LEFT,1,1,0);
					pdfDoc.setCell(sRegConError+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);	
					pdfDoc.setCell(""+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
					pdfDoc.setCell("Total de Registros"+"\n ","formasrep",ComunesPDF.LEFT,1,1,0);    
					pdfDoc.setCell(sTotalRegistros+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
					pdfDoc.setCell(""+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
					pdfDoc.addTable();
			
			}
			resultado.close();
			psresultado.close();
			
			
			if (rs.next()){
				i++;
				String aceptadosP          = (rs.getString("aceptadosP") == null)?"":rs.getString("aceptadosP") ;
				String aceptadosD          = (rs.getString("aceptadosD") == null)?"":rs.getString("aceptadosD") ;
				String rechazadosP         = (rs.getString("rechazadosP") == null)?"":rs.getString("rechazadosP") ;
				String rechazadosD         = (rs.getString("rechazadosD") == null)?"":rs.getString("rechazadosD") ;
				String totalregistrosP     =(rs.getString("totalregistrosP") == null)?"":rs.getString("totalregistrosP") ;
				String totalregistrosD     =(rs.getString("totalregistrosD") == null)?"":rs.getString("totalregistrosD") ;  
				// String fecha               =  (rs.getString("fecha") == null)?"":rs.getString("fecha") ;
				String capitalP            = (rs.getString("capitalP") == null)?"":rs.getString("capitalP") ;  
				String capitalD            = (rs.getString("capitalD") == null)?"":rs.getString("capitalD") ;  
				String intordinarioP       =  (rs.getString("intordinarioP") == null)?"0":rs.getString("intordinarioP") ;
				String intordinarioD       =  (rs.getString("intordinarioD") == null)?"0":rs.getString("intordinarioD") ;
				String intmoratorioP       = (rs.getString("intmoratorioP") == null)?"0":rs.getString("intmoratorioP") ;
				String intmoratorioD       = (rs.getString("intmoratorioD") == null)?"0":rs.getString("intmoratorioD") ;
				String comianiversarioP    =(rs.getString("comianiversarioP") == null)?"0":rs.getString("comianiversarioP") ;
				String comianiversarioD    =(rs.getString("comianiversarioD") == null)?"0":rs.getString("comianiversarioD") ;
				String ivaniversarioP      = (rs.getString("ivaniversarioP") == null)?"0":rs.getString("ivaniversarioP") ;
				String ivaniversarioD      = (rs.getString("ivaniversarioD") == null)?"0":rs.getString("ivaniversarioD") ;
				String totalResembolsoP    = (rs.getString("totalResembolsoP") == null)?"0":rs.getString("totalResembolsoP") ;
				String totalResembolsoD    = (rs.getString("totalResembolsoD") == null)?"0":rs.getString("totalResembolsoD") ;
				String ComisionAniversarioP= (rs.getString("ComisionAniversarioP") == null)?"0":rs.getString("ComisionAniversarioP") ;
				String ComisionAniversarioD= (rs.getString("ComisionAniversarioD") == null)?"0":rs.getString("ComisionAniversarioD") ;
				double ImporteDesembolsoP  = Double.parseDouble((String)totalResembolsoP)-Double.parseDouble((String)ComisionAniversarioP);
				double ImporteDesembolsoD  = Double.parseDouble((String)totalResembolsoD)-Double.parseDouble((String)ComisionAniversarioD);
					 
				float widthtb4[] = {100f};    
				pdfDoc.setTable(1, 100, widthtb4);
				pdfDoc.setCell("DETALLE DE DESEMBOLSOS MASIVOS POR MONEDA","celda01",ComunesPDF.CENTER,1,1,1);
				pdfDoc.addTable();   
				
				float widthtb5[] = {16.6f,16.6f,16.6f};
				pdfDoc.setTable(3, 100, widthtb5);
				pdfDoc.setCell("Moneda"+"\n ","formasrep",ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell("Pesos "+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
				pdfDoc.setCell("Dolares"+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);   
				
				pdfDoc.setCell("Registros Aceptados ","formasrep",ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell(aceptadosP+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
				pdfDoc.setCell(aceptadosD+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
				
				pdfDoc.setCell("Registros Rechazados","formasrep",ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell(rechazadosP+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
				pdfDoc.setCell(rechazadosD+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
				
				pdfDoc.setCell("Total de Registros","formasrep",ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell(totalregistrosP+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
				pdfDoc.setCell(totalregistrosD+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
				
				pdfDoc.setCell("Fecha honrada","formasrep",ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell(fechaHonrada+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
				pdfDoc.setCell(fechaHonrada+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
				
				pdfDoc.setCell("Capital","formasrep",ComunesPDF.LEFT,1,1,0);   
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(capitalP,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(capitalD,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
			 
				pdfDoc.setCell("Interés ordinario ","formasrep",ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(intordinarioP,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(intordinarioD,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
				
				pdfDoc.setCell("Interés moratorio ","formasrep",ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(intmoratorioP,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(intmoratorioD,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
				 
				pdfDoc.setCell("Total Desembolso ","formasrep",ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalResembolsoP,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(totalResembolsoD,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
					
				pdfDoc.setCell("Comision Aniversario ","formasrep",ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(comianiversarioP,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(comianiversarioD,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
					
				pdfDoc.setCell("Iva de la Comision Aniversario ","formasrep",ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(ivaniversarioP,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(ivaniversarioD,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
			
				pdfDoc.setCell("Total Comision Aniversario ","formasrep",ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(ComisionAniversarioP,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(ComisionAniversarioD,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
			
				pdfDoc.setCell("Importe Neto a Desembolsar","formasrep",ComunesPDF.LEFT,1,1,0);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(ImporteDesembolsoP,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
				pdfDoc.setCell("$ "+Comunes.formatoDecimal(ImporteDesembolsoD,2)+"\n ","formasrep",ComunesPDF.RIGHT,1,1,0);
				pdfDoc.addTable();
			}
			rs.close();
			ps.close();
			
			float widthtb9[] = {100f};
			pdfDoc.setTable(1, 100, widthtb9);
			pdfDoc.setCell("Errores Generados","celda01",ComunesPDF.CENTER,1,1,1);	   
			pdfDoc.addTable();  
			
			
			float widthtb6[] = {100f};
			pdfDoc.setTable(1, 100, widthtb6);
			pdfDoc.setCell(sArchi.toString(),"formasrep",ComunesPDF.LEFT,1,1,0);
			pdfDoc.addTable();
			
			pdfDoc.endDocument();
								
			
			
			return nombreArchivo;
		}catch(Exception e){
			log.error("DesResultadoMasivo::generaPDF(Error)" +e); 
			e.printStackTrace();
			throw new NafinException("SIST0001");
		}finally	{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
			
				log.info(" generaPDF  (E)");
		}
  }//generaPDF
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
			
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
		public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
			
			
			
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
	
}