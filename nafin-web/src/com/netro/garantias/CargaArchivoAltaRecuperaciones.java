package com.netro.garantias;

import com.netro.model.catalogos.CatalogoSimple;
import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.Fecha;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**

	Esta clase se encarga de generar el archivo PDF del acuse de la carga masiva de recuperaciones.
		ADMIN_IF_GARAN - PROGRAMA DE GARANT�A AUTOM�TICA - TRANSACCIONES � RECUPERACI�N � CARGA ARCHIVO
	
	@author Jesus Salim Hernandez David
	@since  Fodea 004 - 2012 -- Migracion IF
	@date	  12/07/2012 11:57:37 a.m.
	
 */
public class CargaArchivoAltaRecuperaciones {
	
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(CargaArchivoAltaRecuperaciones.class);

	/**
		Crea un archivo PDF con la informaci�n del acuse de la carga
		@throws AppException 
		
		@param rg 												<tt>ResultadosGar</tt> con el resultado de la transmision de solicitudes.
		@param strDirectorioTemp   						<tt>String</tt> con la ruta del directorio temporal donde se creara el archivo PDF.
		@param cabecera       								<tt>HashMap</tt> con la informacion de la cabcera del PDF.
		@param strDirectorioPublicacion 					<tt>String</tt> con la ruta WEB del directorio de publicacion.
		@param totalRegistros							   <tt>String</tt> con el numero total de registros a transmitidos.
		@param montoTotal									   <tt>String</tt> con el monto total de la recuperaci�n de importes.
		@param loginUsuario  								<tt>String</tt> con el login del usuario firmado en el sistema.
		@param nombreUsuario   								<tt>String</tt> con el nombre del usuario firmado en el sistema.
 
		@return <tt>String</tt> con el nombre del archivo generado.
	*/	
	public static String generaAcuseArchivoPDF(
			ResultadosGar 	rg,
			String 			strDirectorioTemp,
			HashMap			cabecera,
			String 			strDirectorioPublicacion,
			String			totalRegistros,
			String 			montoTotal,
			String 			loginUsuario,
			String 			nombreUsuario
	) throws AppException{
 
		log.info("generaAcuseArchivoPDF(E)");
		
		String 		nombreArchivo 	= null;
		CreaArchivo archivo 			= null;
		ComunesPDF 	pdfDoc 			= null;
		
		try {
			
			archivo 						= new CreaArchivo();
			nombreArchivo 				= archivo.nombreArchivo()+".pdf";
	 
			// Obtener Descripci�n del Tipo de Operaci�n
			String 				tipoOperacion 	= "";
			// Preparar consulta
			CatalogoSimple 	cat 				= new CatalogoSimple();
			cat.setTabla("gticat_tipooperacion");
			cat.setCampoClave("ic_tipo_operacion");
			cat.setCampoDescripcion("cg_descripcion");
			List claves = new ArrayList();
			claves.add("4");	//4= Recuperaciones autom�ticas
			cat.setValoresCondicionIn(claves);
			// Consultar descripcion del Tipo de Operacion 4.
			List listaResultados = cat.getListaElementos();
			if (listaResultados!=null && listaResultados.size()>0) {
				tipoOperacion = Comunes.rellenaCeros(((ElementoCatalogo)listaResultados.get(0)).getClave(),2) + " " + ((ElementoCatalogo)listaResultados.get(0)).getDescripcion();
			}
		
			pdfDoc 						= new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
			
			String 	meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String 	fechaActual  	= Fecha.getFechaActual();
			String 	diaActual   	= fechaActual.substring(0,2);
			String 	mesActual    	= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String 	anioActual   	= fechaActual.substring(6,10);
			String 	horaActual  	= Fecha.getHoraActual("HH24:MI:SS");
 
			pdfDoc.encabezadoConImagenes(
						pdfDoc,
						(String) cabecera.get("strPais"),
						(String) cabecera.get("iNoNafinElectronico"),
						(String) cabecera.get("sesExterno"),
						(String) cabecera.get("strNombre"),
						(String) cabecera.get("strNombreUsuario"),
						(String) cabecera.get("strLogo"),
						strDirectorioPublicacion
					);
 
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
			pdfDoc.addText("Su solicitud fue recibida con el n�mero de folio: "+rg.getFolio(),"formas",ComunesPDF.CENTER);
			pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
			
			pdfDoc.setTable(2,70);
			
			pdfDoc.setCell("CIFRAS CONTROL",										"celda03",	ComunesPDF.CENTER,2,1,1);
			
			pdfDoc.setCell("No. total de registros",							"celda02",	ComunesPDF.CENTER);// No. total de registros
			pdfDoc.setCell(Comunes.formatoDecimal(totalRegistros,0),		"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.setCell("Monto total de los registros transmitidos",	"celda02",	ComunesPDF.CENTER);// Monto total de los registros transmitidos
			pdfDoc.setCell(Comunes.formatoDecimal(montoTotal,2),			"formas",	ComunesPDF.RIGHT);
 
			pdfDoc.setCell("Fecha de carga",										"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(rg.getFecha(),											"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.setCell("Hora de carga",										"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(rg.getHora(),											"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.setCell("Fecha valor",											"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(rg.getFechaValor(),									"formas",	ComunesPDF.RIGHT);
	
			pdfDoc.setCell("Usuario",												"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(loginUsuario+" - "+nombreUsuario,				"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.addTable();
			pdfDoc.endDocument();
		
		}catch(Exception e){
			
			log.error("generaAcuseArchivoPDF(Exception)");
			e.printStackTrace();
			log.error("generaAcuseArchivoPDF.rg                       = <" + rg                       + ">");
			log.error("generaAcuseArchivoPDF.strDirectorioTemp        = <" + strDirectorioTemp        + ">");
			log.error("generaAcuseArchivoPDF.cabecera                 = <" + cabecera                 + ">");
			log.error("generaAcuseArchivoPDF.strDirectorioPublicacion = <" + strDirectorioPublicacion + ">");
			log.error("generaAcuseArchivoPDF.totalRegistros           = <" + totalRegistros           + ">");
			log.error("generaAcuseArchivoPDF.montoTotal               = <" + montoTotal               + ">");
			log.error("generaAcuseArchivoPDF.loginUsuario             = <" + loginUsuario             + ">");
			log.error("generaAcuseArchivoPDF.nombreUsuario            = <" + nombreUsuario            + ">");
			
			throw new AppException("Ocurri� un error al generar el PDF con la informaci�n del acuse.");
			
		}
		log.info("generaAcuseArchivoPDF(S)");
		
		return nombreArchivo;	
	
	}
	
}