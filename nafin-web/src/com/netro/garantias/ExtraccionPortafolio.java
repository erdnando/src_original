package com.netro.garantias;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**

	Esta clase se encarga de generar los archivos de la consulta del portafolio, en la pantalla:
		ADMIN_IF_GARAN - PROGRAMA DE GARANT�A AUTOM�TICA - CARGA DE CALIFICACION DE CARTERA - OBTENCI�N PORTAFOLIO A CALIFICAR Y MON. DE AVANCE
	
	@author Jesus Salim Hernandez David
	@since  Fodea 004 - 2012 -- Migracion IF
	@date	  24/05/2012 10:43:00 a.m.
	
 */
public class ExtraccionPortafolio {

	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ExtraccionPortafolio.class);
	
	/**
		Realiza la extracci&oacute;n detallada y la guarda en un archivo PDF
		@throws AppException   
		
		@param directorio <tt>String</tt> con el path del directorio donde se guardara el archivo generado.
		@param cabecera   <tt>HashMap</tt> con la informacion de cabcera del archivo PDF.
		@param anio       <tt>String</tt> con el n&uacute;mero del a&ntilde;o.
		@param trimestre  <tt>String</tt> con el n&uacute;mero del trimestre.
		@param fechaCorte <tt>String</tt> con la fecha de corte en formato <tt>dd/mm/aaaa</tt>. 
		@param claveIF    <tt>String</tt> con la clave del IF como aparece en <tt>COMCAT_IF.IC_IF</tt>.
		
		@return <tt>String</tt> con el nombre del archivo generado.
	*/	
	public static String generaArchivoPDFExtraccionDetalle(String directorio, HashMap cabecera, String anio, String trimestre, String fechaCorte, String claveIF )
		throws AppException{
		
		log.info("generaArchivoPDFExtraccionDetalle(E)");
		
		String 		nombreArchivo 	= null;
		CreaArchivo archivo 			= null;
		ComunesPDF 	documentoPdf 	= null;
		
		try {
			
			// Obtener instancia del EJB de Garantias
			Garantias garantias = null;
			try {
						
				garantias 		= ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
							
			}catch(Exception e){			 
				e.printStackTrace();
				throw new AppException("Ocurri� un error al obtener instancia del EJB de Garant�as.");
			 
			}
	
			// Crear archivo PDF
			archivo 			= new CreaArchivo();
			nombreArchivo 	= archivo.nombreArchivo()+".pdf";
			//documentoPdf 	= new ComunesPDF(2,directorio+nombreArchivo, "", false, true, false);

			/*// Encabezado
			documentoPdf.setEncabezado(				(String)	cabecera.get("PAIS"),
																(String) cabecera.get("NUM_NAFIN_ELECTRONICO"),
																(String) cabecera.get("EXTERNO"),
																(String) cabecera.get("NOMBRE"),
																(String) cabecera.get("NOMBRE_USUARIO"),
																(String) cabecera.get("LOGO"),
																(String) cabecera.get("DIRECTORIO_PUBLICACION"),
																" ");		
                                                
                                                */
        // HttpSession session = request.getSession();
         nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
         documentoPdf = new ComunesPDF(2, directorio+nombreArchivo);
         
         String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
         String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
         String diaActual    = fechaActual.substring(0,2);
         String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
         String anioActual   = fechaActual.substring(6,10);
         String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
               
         documentoPdf.encabezadoConImagenes(documentoPdf,(String)	cabecera.get("PAIS"),
																(String) cabecera.get("NUM_NAFIN_ELECTRONICO"),
																(String) cabecera.get("EXTERNO"),
																(String) cabecera.get("NOMBRE"),
																(String) cabecera.get("NOMBRE_USUARIO"),
																(String) cabecera.get("LOGO"),
																(String) cabecera.get("DIRECTORIO_PUBLICACION"));	
               
        documentoPdf.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
         
			// Localidad, fecha y hora
			ArrayList registros = garantias.getDetallePortafolioACalificarPorRegistro(anio,trimestre,fechaCorte,claveIF);
			   
			if(registros != null && registros.size() > 0 ){                 
            documentoPdf.setTable(13,100);
            
				// Agregar cabecera de la tabla
				documentoPdf.setCell("A�o",																		"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setCell("Trimestre",																"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setCell("Fecha de Corte",															"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setCell("N�mero de Cr�dito",														"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setCell("RFC",  	  																	"celda01rep",ComunesPDF.CENTER,1,1,1);
				/*documentoPdf.setCell("Calificaci�n Deudor IF",												"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setCell("Calificaci�n Cubierta IF",											"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setCell("Calificaci�n Expuesta IF",											"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setCell("% Reserva Cubierto IF",												"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setCell("% Reserva Expuesto IF",												"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setCell("Reserva Cubierta IF",													"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setCell("Reserva Expuesta IF",													"celda01rep",ComunesPDF.CENTER,1,1,1);*/
				documentoPdf.setCell("Tipo de Cartera",														"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setCell("Probabilidad de Incumplimiento (PI)",								"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setCell("Severidad de la P�rdida (SP)",										"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setCell("Exposici�n al Incumplimiento (EI)",								"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setCell("Porcentaje de Reservas (%PE)",										"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setCell("Nivel de Riesgo",														"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setCell("Sistuaci�n de Recepci�n del Registro de Calificaci�n",		"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setCell("Fecha y Hora en la que se Recibi� la Informaci�n del IF",	"celda01rep",ComunesPDF.CENTER,1,1,1);
			
				// Llenar tabla
				int i = 0;
				for(i=0;i<registros.size();i++){  
            
            
					HashMap registro = (HashMap) registros.get(i);
					//for(int x=0; x<10; x++){            
                  documentoPdf.setCell((String)registro.get("A�O"),										"formasrep",ComunesPDF.CENTER,1,1,1);
                  documentoPdf.setCell((String)registro.get("TRIMESTRE"),								"formasrep",ComunesPDF.CENTER,1,1,1);
                  documentoPdf.setCell((String)registro.get("FECHA_CORTE"),							"formasrep",ComunesPDF.CENTER,1,1,1);
                  documentoPdf.setCell((String)registro.get("NUM_CREDITO"),							"formasrep",ComunesPDF.CENTER,1,1,1);
                  documentoPdf.setCell((String)registro.get("RFC"),										"formasrep",ComunesPDF.CENTER,1,1,1);
                  /*documentoPdf.setCell((String)registro.get("CALIFICACION_DEUDOR_IF"),			"formasrep",ComunesPDF.CENTER,1,1,1);
                  documentoPdf.setCell((String)registro.get("CALIFICACION_CUBIERTA_IF"),			"formasrep",ComunesPDF.CENTER,1,1,1);
                  documentoPdf.setCell((String)registro.get("CALIFICACION_EXPUESTA_IF"),			"formasrep",ComunesPDF.CENTER,1,1,1);
                  documentoPdf.setCell((String)registro.get("PORC_RESERVA_CUBIERTO_IF"),			"formasrep",ComunesPDF.CENTER,1,1,1);
                  documentoPdf.setCell((String)registro.get("PORC_RESERVA_EXPUESTO_IF"),			"formasrep",ComunesPDF.CENTER,1,1,1);
                  documentoPdf.setCell((String)registro.get("RESERVA_CUBIERTA_IF"),				"formasrep",ComunesPDF.CENTER,1,1,1);
                  documentoPdf.setCell((String)registro.get("RESERVA_EXPUESTA_IF"),			 	"formasrep",ComunesPDF.CENTER,1,1,1);*/
                  documentoPdf.setCell((String)registro.get("TIPO_CARTERA"),							"formasrep",ComunesPDF.CENTER,1,1,1);
                  documentoPdf.setCell((String)registro.get("PROB_INCUMP"),							"formasrep",ComunesPDF.CENTER,1,1,1);
                  documentoPdf.setCell((String)registro.get("SEV_PERDIDA"),							"formasrep",ComunesPDF.CENTER,1,1,1);
                  documentoPdf.setCell((String)registro.get("EXPOS_INCUMP"),							"formasrep",ComunesPDF.CENTER,1,1,1);
                  documentoPdf.setCell((String)registro.get("PORC_RESERV"),							"formasrep",ComunesPDF.CENTER,1,1,1);
                  documentoPdf.setCell((String)registro.get("NIVEL_RIESGO"),							"formasrep",ComunesPDF.CENTER,1,1,1);
                  documentoPdf.setCell((String)registro.get("SITUACION_RECEPCION_REGISTRO"), 	"formasrep",ComunesPDF.CENTER,1,1,1);
                  documentoPdf.setCell((String)registro.get("FECHA_RECEPCION_INFORMACION"),		"formasrep",ComunesPDF.CENTER,1,1,1);	
             //  }
				}
				documentoPdf.addTable();
				
				documentoPdf.addText(" ","formas",ComunesPDF.LEFT);
				documentoPdf.addText("Total: " + i + " Registro"+ ((i!=1)?"s.":"."),"formas",ComunesPDF.LEFT);
				
			}else{ // No hay registros
				documentoPdf.setTable(1,100,new float[]{100f});
				documentoPdf.setCell("\n    No se encontr� ning�n registro.\n ","celda01rep",ComunesPDF.LEFT,1,1,1);
				documentoPdf.addTable();
			}
			// Terminar documento
			documentoPdf.endDocument();
			
		}catch(Exception e){
			
			log.error("generaArchivoPDFExtraccionDetalle(Exception)");
			e.printStackTrace();
			log.error("directorio  = <" + directorio  + ">");
			log.error("cabecera    = <" + cabecera    + ">");
			log.error("anio        = <" + anio        + ">");
			log.error("trimestre   = <" + trimestre   + ">");
			log.error("fechaCorte  = <" + fechaCorte  + ">");
			log.error("claveIF     = <" + claveIF     + ">");
			throw new AppException("Ocurri� un error al generar el PDF con la informaci�n detallada.");
			
		}
		log.info("generaArchivoPDFExtraccionDetalle(S)");
		
		return nombreArchivo;	
		
	}
	
	/**
		Realiza la extracci&oacute;n detallada y la guarda en un archivo CSV
		@throws AppException 
		
		@param directorio <tt>String</tt> con el path del directorio donde se guardara el archivo generado.
		@param cabecera   <tt>HashMap</tt> con la informacion de cabcera del archivo PDF.
		@param anio       <tt>String</tt> con el n&uacute;mero del a&ntilde;o.
		@param trimestre  <tt>String</tt> con el n&uacute;mero del trimestre.
		@param fechaCorte <tt>String</tt> con la fecha de corte en formato <tt>dd/mm/aaaa</tt>. 
		@param claveIF    <tt>String</tt> con la clave del IF como aparece en <tt>COMCAT_IF.IC_IF</tt>.
		
		@return <tt>String</tt> con el nombre del archivo generado.
	*/	
	public static String generaArchivoCSV(String directorio, HashMap cabecera,String anio,String trimestre,String fechaCorte,String claveIF)
		throws AppException{
			
		log.info("generaArchivoCSV(E)");
		// Obtener instancia del EJB de Garantias
		Garantias garantias = null;
		try {

			garantias 		= ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
						
		}catch(Exception e){			 
			e.printStackTrace();
			throw new AppException("Ocurri� un error al obtener instancia del EJB de Garant�as.");
		 
		}		
		FileOutputStream		out 				= null;
		BufferedWriter 		csv 				= null;
		CreaArchivo 			archivo 			= null;
		String					nombreArchivo 	= null;
		
		AccesoDB 				con 				= new AccesoDB();
		String 					qrySentencia	= null;
		HashMap 					registro 		= null;
		ArrayList 				lista				= new ArrayList();
		ResultSet 				rs 				= null;
		PreparedStatement 	ps 				= null;
 
		try {
 
			// Crear Archivo
			archivo 			= new CreaArchivo();
			nombreArchivo	= archivo.nombreArchivo()+".csv";
			out 				= new FileOutputStream(directorio+nombreArchivo);
			csv 				= new BufferedWriter(new OutputStreamWriter(out, "Cp1252"));

			// Definir cabecera
			csv.write("A�o,Trimestre,Fecha de Corte,N�mero de Cr�dito,RFC,Tipo de Cartera,Probabilidad de Incumplimiento (PI),Severidad de la P�rdida (SP),Exposici�n al Incumplimiento (EI),Porcentaje de Reservas (%PE),Nivel de Riesgo,Sistuaci�n de Recepci�n del Registro de Calificaci�n,Fecha y Hora en la que se Recibi� la Informaci�n del IF"+"\n");
			ArrayList registros = garantias.getDetallePortafolioACalificarPorRegistro(anio,trimestre,fechaCorte,claveIF);
			
			int i = 0;
			for(i=0;i<registros.size();i++){ 
				registro = (HashMap) registros.get(i);
								
				csv.write((String)registro.get("A�O")+",");
				csv.write((String)registro.get("TRIMESTRE")+",");
				csv.write((String)registro.get("FECHA_CORTE")+",");
				csv.write("\"" + (String)registro.get("NUM_CREDITO")+"\",");
				csv.write((String)registro.get("RFC")+",");
				csv.write((String)registro.get("TIPO_CARTERA")+",");
				csv.write((String)registro.get("PROB_INCUMP")+",");
				csv.write((String)registro.get("SEV_PERDIDA")+",");
				csv.write((String)registro.get("EXPOS_INCUMP")+",");
				csv.write((String)registro.get("PORC_RESERV")+",");
				csv.write((String)registro.get("NIVEL_RIESGO")+",");
				/*csv.write("\"" + (rs.getString("CALIFICACION_DEUDOR_IF") 	== null?"": rs.getString("CALIFICACION_DEUDOR_IF"))	+"\",");
				csv.write("\"" + (rs.getString("CALIFICACION_CUBIERTA_IF") 	== null?"": rs.getString("CALIFICACION_CUBIERTA_IF"))	+"\",");
				csv.write("\"" + (rs.getString("CALIFICACION_EXPUESTA_IF") 	== null?"": rs.getString("CALIFICACION_EXPUESTA_IF"))	+"\",");
				csv.write("\"" + (Comunes.formatoDecimal((rs.getString("PORC_RESERVA_CUBIERTO_IF") == null || rs.getString("PORC_RESERVA_CUBIERTO_IF").equals("")?"0.00":rs.getString("PORC_RESERVA_CUBIERTO_IF")),2,true))	+"\",");
				csv.write("\"" + (Comunes.formatoDecimal((rs.getString("PORC_RESERVA_EXPUESTO_IF") == null || rs.getString("PORC_RESERVA_EXPUESTO_IF").equals("")?"0.00":rs.getString("PORC_RESERVA_EXPUESTO_IF")),2,true))	+"\",");
				csv.write("\"" + (Comunes.formatoDecimal((rs.getString("RESERVA_CUBIERTA_IF") 	  == null || rs.getString("RESERVA_CUBIERTA_IF").equals("")?"0.00":rs.getString("RESERVA_CUBIERTA_IF")),2,true))				+"\",");
				csv.write("\"" + (Comunes.formatoDecimal((rs.getString("RESERVA_EXPUESTA_IF")		  == null || rs.getString("RESERVA_EXPUESTA_IF").equals("")?"0.00":rs.getString("RESERVA_EXPUESTA_IF")),2,true))				+"\",");*/
				csv.write((String)registro.get("SITUACION_RECEPCION_REGISTRO")+",");
				csv.write((String)registro.get("FECHA_RECEPCION_INFORMACION")+"\n");
				
				if((i%200) == 0) csv.flush();
				//i++;
		
			}
 
			// Verificar si se encontraron registros
			if(i == 0){
				csv.write("No hay datos"+"\n");
			}
			
			// Obtener numero total de registros
			csv.write("\n");	
			csv.write("Total: " + i + " Registro"+ ((i!=1)?"s.":"."));
			
		}catch(OutOfMemoryError om) { // Se acabo la memoria
			
			log.error("generaArchivoCSV(OutOfMemoryError)");
			log.error(om.getMessage());
			log.error("Free Memory= "+Runtime.getRuntime().freeMemory());
			log.error("directorio  = <" + directorio  + ">");
			log.error("cabecera    = <" + cabecera    + ">");
			log.error("anio        = <" + anio        + ">");
			log.error("trimestre   = <" + trimestre   + ">");
			log.error("fechaCorte  = <" + fechaCorte  + ">");
			log.error("claveIF     = <" + claveIF     + ">");
			om.printStackTrace();
			throw new AppException("Se acab� la memoria.");
			
		} catch (Exception e) { // Ocurrio una excepcion
			
			log.error("generaArchivoCSV(Exception)");
			e.printStackTrace();
			log.error("directorio  = <" + directorio  + ">");
			log.error("cabecera    = <" + cabecera    + ">");
			log.error("anio        = <" + anio        + ">");
			log.error("trimestre   = <" + trimestre   + ">");
			log.error("fechaCorte  = <" + fechaCorte  + ">");
			log.error("claveIF     = <" + claveIF     + ">");
			throw new AppException("Ocurri� un error al generar el CSV con la informaci�n detallada.");
			
		} finally {
			
			if(rs!=null){	try { rs.close(); }catch(Exception e){} }
			if(ps!=null){	try { ps.close(); }catch(Exception e){} }
			
			// Cerrar archivo
			if(csv 	!= null )	try{csv.close();}catch(Exception e){};

		}
		
		log.info("generaArchivoCSV(S)");
		return nombreArchivo;
		
	}
	
	/**
		Realiza la extracci&oacute;n consolidada y la guarda en un archivo PDF
		@throws AppException
		
		@param directorio <tt>String</tt> con el path del directorio donde se guardara el archivo generado.
		@param cabecera   <tt>HashMap</tt> con la informacion de cabcera del archivo PDF.
		@param anio       <tt>String</tt> con el n&uacute;mero del a&ntilde;o.
		@param trimestre  <tt>String</tt> con el n&uacute;mero del trimestre.
		@param fechaCorte <tt>String</tt> con la fecha de corte en formato <tt>dd/mm/aaaa</tt>. 
		@param claveIF    <tt>String</tt> con la clave del IF como aparece en <tt>COMCAT_IF.IC_IF</tt>.
		
		@return <tt>String</tt> con el nombre del archivo generado.
	 */
	public static String generaArchivoPDFExtraccionConsolidada(String directorio, HashMap cabecera,String anio, String trimestre, String fechaCorte, String claveIF)
		throws AppException{
		
		log.info("generaArchivoPDFExtraccionConsolidada(E)");
		
		String 		nombreArchivo 	= null;
		CreaArchivo archivo 			= null;
		ComunesPDF 	documentoPdf 	= null;
		
		try {
			
			// Obtener instancia del EJB de Garantias
			Garantias garantias = null;
			try {

				garantias 		= ServiceLocator.getInstance().lookup("GarantiasEJB", Garantias.class);
							
			}catch(Exception e){
			 
				e.printStackTrace();
				throw new AppException("Ocurri� un error al obtener instancia del EJB de Garant�as.");
			 
			}
	
			// Crear archivo PDF
			archivo 			= new CreaArchivo();
			nombreArchivo 	= archivo.nombreArchivo()+".pdf";
			documentoPdf 	= new ComunesPDF(2,directorio+nombreArchivo);

			// Encabezado
			documentoPdf.encabezadoConImagenes(	 	documentoPdf,
													 			(String)	cabecera.get("PAIS"),
																(String) cabecera.get("NUM_NAFIN_ELECTRONICO"),
																(String) cabecera.get("EXTERNO"),
																(String) cabecera.get("NOMBRE"),
																(String) cabecera.get("NOMBRE_USUARIO"),
																(String) cabecera.get("LOGO"),
																(String) cabecera.get("DIRECTORIO_PUBLICACION")
														 );
			
			// Localidad, fecha y hora
			String meses[] 		= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaAct 		= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    	= fechaAct.substring(0,1).equals("0")?fechaAct.substring(1,2):fechaAct.substring(0,2);
			String mesActual    	= meses[Integer.parseInt(fechaAct.substring(3,5))-1];
			String anioActual   	= fechaAct.substring(6,10);
			String horaActual  	= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			documentoPdf.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+"                               "+horaActual+"             ","formas",ComunesPDF.RIGHT);
			documentoPdf.addText(" ","formas",ComunesPDF.RIGHT);
			documentoPdf.addText(" ","formas",ComunesPDF.RIGHT);
			
			// Extraer datos
			HashMap registro = garantias.getPortafolioACalificarPorRegistro(anio,trimestre,fechaCorte,claveIF);
			
			if(registro != null){
			 
				//documentoPdf.setTable(10,100,new float[]{10f,10f,10f,10f,10f,10f,10f,10f,10f,10f});
				documentoPdf.setLTable(10,100,new float[]{10f,10f,10f,10f,10f,10f,10f,10f,10f,10f});
			
				// Agregar cabecera de la tabla
				documentoPdf.setLCell("A�o",																								"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setLCell("Trimestre",																						"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setLCell("Fecha de corte",																				"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setLCell("N�mero de garant�as extra�das de la BD de SIAG",										"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setLCell("N�mero de garant�as recibidas",															"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setLCell("N�mero de garant�as no recibidas",														"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setLCell("Fecha en que se cerr� la calificaci�n",													"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setLCell("Acuse recibido a los resultados de la calificaci�n",								"celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setLCell("Indica si el portafolio de calificaci�n ha sido extraido por intermediario","celda01rep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setLCell("Situaci�n recepci�n",																			"celda01rep",ComunesPDF.CENTER,1,1,1);
			
				// Llenar tabla
				documentoPdf.setLCell((String)registro.get("A�O"),										"formasrep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setLCell((String)registro.get("TRIMESTRE"),								"formasrep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setLCell((String)registro.get("FECHA_CORTE"),							"formasrep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setLCell((String)registro.get("TOTAL_GARANTIAS"),						"formasrep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setLCell((String)registro.get("NUMERO_GARANTIAS_RECIBIDAS"),		"formasrep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setLCell((String)registro.get("NUMERO_GARANTIAS_NO_RECIBIDAS"),	"formasrep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setLCell((String)registro.get("FECHA_CIERRE"),							"formasrep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setLCell((String)registro.get("ACUSE"),									"formasrep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setLCell((String)registro.get("SITUACION_RECEPCION_PORTAFOLIO"),	"formasrep",ComunesPDF.CENTER,1,1,1);
				documentoPdf.setLCell((String)registro.get("SITUACION_RECEPCION"),				"formasrep",ComunesPDF.CENTER,1,1,1);
			
				documentoPdf.addLTable();
			}else{ // No hay registros
				documentoPdf.setLTable(1,100,new float[]{100f});
				documentoPdf.setLCell("\n    No se encontr� ning�n registro.\n ","celda01rep",ComunesPDF.LEFT,1,1,1);
				documentoPdf.addLTable();
			}
			// Terminar documento
			documentoPdf.endDocument();
			
		}catch(Exception e){
			
			log.error("generaArchivoPDFExtraccionConsolidada(Exception)");
			log.error("directorio  = <" + directorio  + ">");
			log.error("cabecera    = <" + cabecera    + ">");
			log.error("anio        = <" + anio        + ">");
			log.error("trimestre   = <" + trimestre   + ">");
			log.error("fechaCorte  = <" + fechaCorte  + ">");
			log.error("claveIF     = <" + claveIF     + ">");
			e.printStackTrace();
			throw new AppException("Ocurri� un error al generar el archivo PDF con la informacion consolidada.");
			
		}
		log.info("generaArchivoPDFExtraccionConsolidada(S)");
		
		return nombreArchivo;	
	}
	
}