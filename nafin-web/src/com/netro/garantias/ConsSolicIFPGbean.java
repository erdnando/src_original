package com.netro.garantias;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsSolicIFPGbean {

//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsSolicIFPGbean.class);
	
	private int iIF_SIAG = 0;  
  private String df_fecha_oper_de = "";  //fodea 030-2010-030 Desembolso Masivo Garant�as
  private String df_fecha_oper_a  = "";
  private String combsituacion ="";
  private String folioperacion ="";
  

	public ConsSolicIFPGbean() { }

	public void setiIF_SIAG(int iIF_SIAG) {
		this.iIF_SIAG = iIF_SIAG;
	}
   //fodea 030-2010-030 Desembolso Masivo Garant�as
  public void setdf_fecha_oper_de(String df_fecha_oper_de) {
		this.df_fecha_oper_de = df_fecha_oper_de;
	}
  
  public void setdf_fecha_oper_a(String df_fecha_oper_a) {
		this.df_fecha_oper_a = df_fecha_oper_a;
	}
  
  public void setfolioperacion(String folioperacion) {
		this.folioperacion = folioperacion;
	}
  
  public void setcombsituacion(String combsituacion) {
		this.combsituacion = combsituacion;
	}

	public String getQuerySolicitudes() {
    log.info("Inicia (E)");
    
		StringBuffer	sbQuery		= new StringBuffer();
		String 			sCondicion	= "";
		String 			sReferencia	= ",'ConsSolicIFPGbean:getQuerySolicitudes("+iIF_SIAG+")' as PANTALLA ";
  
		if(iIF_SIAG!=0)
			sCondicion += " AND es.ic_if_siag = ? ";
    
     //fodea 030-2010-030 Desembolso Masivo Garant�as  
   if(!df_fecha_oper_de.equals("") && !df_fecha_oper_a.equals("")){
      sCondicion += " AND es.df_fecha_hora >= TO_DATE (?, 'DD/MM/YYYY')  "+
                  " AND es.df_fecha_hora <= TO_DATE (?, 'DD/MM/YYYY')+1 ";                  
  }
   if(!folioperacion.equals("")){
      sCondicion += " AND es.ic_folio = ? ";
   }
   
   if(!combsituacion.equals("")){
      sCondicion += " AND es.ic_situacion = ? ";
   }  
  
		sbQuery.append(
			" SELECT   " +
			"   TO_CHAR (es.df_fecha_hora, 'dd/mm/yyyy hh24:mi') AS fecha, "   +
			"   TO_CHAR(es.ic_folio) as ic_folio, s.cg_descripcion AS situacion, es.ic_situacion, "   +
			"   es.ic_contragarante, sp.cc_garantia " +
			sReferencia+
			"     FROM gti_solicitud_pago sp, gticat_situacion s, gti_estatus_solic es"   +
			"    WHERE es.ic_situacion = s.ic_situacion"   +
			"      AND es.ic_tipo_operacion = s.ic_tipo_operacion"   +
			"      AND es.ic_folio   = sp.ic_folio"   +
			"      AND es.ic_if_siag = sp.ic_if_siag"   +
			"      AND es.ic_tipo_operacion = ? "   +
			sCondicion+
			" ORDER BY es.df_fecha_hora desc");
      
      log.debug("sbQuery:: "+sbQuery);
      log.debug("iIF_SIAG:: "+iIF_SIAG);
      log.debug("df_fecha_oper_de:: "+df_fecha_oper_de);
      log.debug("df_fecha_oper_a:: "+df_fecha_oper_a);
      log.debug("combsituacion:: "+combsituacion);
      log.debug("folioperacion:: "+folioperacion);      
  
      log.info("Termina (S)");
		return sbQuery.toString();
	} // getQuerySolicitudes()
	
	public Registros getSolicitudes(){
	
		AccesoDB 		con 				= new AccesoDB();
		String 			qrySentencia	= getQuerySolicitudes();
		List 				lVarBind			= new ArrayList();
		Registros		registros		= new Registros();
		try{
		lVarBind.add("3");
	if(iIF_SIAG!=0)
    lVarBind.add(iIF_SIAG+"");
     //fodea 030-2010-030 Desembolso Masivo Garant�as  
   if(!df_fecha_oper_de.equals("") && !df_fecha_oper_a.equals("")){
      lVarBind.add(df_fecha_oper_de);
		lVarBind.add(df_fecha_oper_a);
  }
   if(!folioperacion.equals("")){
     lVarBind.add(folioperacion);
   }
   
   if(!combsituacion.equals("")){
      lVarBind.add(combsituacion);
   }  
					con.conexionDB();
					registros = con.consultarDB(qrySentencia, lVarBind, false);
					
			
		} catch(Exception e) {
				log.info("getSolicitudes:(Exception) "+e);
				e.printStackTrace();
		} finally {
					if(con.hayConexionAbierta())
						con.cierraConexionDB();
						
		}
				return registros;
	
	}

}//ConsSolicIFPGbean
