package com.netro.garantias;

import com.netro.pdf.ComunesPDF;

import java.util.HashMap;

import netropology.utilerias.AppException;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**

	Esta clase se encarga de generar el archivo PDF del acuse de la carga masiva de comisiones.
		ADMIN_IF_GARAN - PROGRAMA DE GARANT�A AUTOM�TICA � TRANSACCIONES � COMISIONES � CARGA DE ARCHIVO
	
	@author Jesus Salim Hernandez David
	@since  Fodea 004 - 2012 -- Migracion IF
	@date	  25/06/2012 12:07:09 a.m.
	
 */
public class CargaArchivoComisiones {
	
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(CargaArchivoComisiones.class);

	/**
		Crea un archivo PDF con la informaci�n del acuse de la carga
		@throws AppException 
		
		@param rg 												<tt>ResultadosGar</tt> con el resultado de la "transmision" de la carga de comisiones.
		@param loginUsuario  								<tt>String</tt> con el login del usuario.
		@param cabecera       								<tt>HashMap</tt> con la informacion de la cabcera del PDF.
		@param strDirectorioTemp   						<tt>String</tt> con la ruta del directorio temporal donde se creara el archivo PDF.
		@param strDirectorioPublicacion 					<tt>String</tt> con la ruta WEB del directorio de publicacion. 
		@param numeroRegistros    							<tt>String</tt> con el numero de solicitudes transmitidas.
		@param montoTotalComisiones    					<tt>String</tt> con el monto total de las comisiones.
		@param acuseFirmaDigital	  						<tt>String</tt> con el numero de acuse de la firma digital.
		@param nombreMesCarga							   <tt>String</tt> con el nombre del mes y a�o de carga.
		@param nombreUsuario   								<tt>String</tt> con el nombre del usuario firmado en el sistema.
		
		@return <tt>String</tt> con el nombre del archivo generado.
	*/	
	public static String generaAcuseArchivoPDF(
			ResultadosGar 	rg, 
			String 			loginUsuario, 
			HashMap 			cabecera, 
			String 			strDirectorioTemp, 
			String 			strDirectorioPublicacion, 
			String 			numeroRegistros, 
			String 			montoTotalComisiones, 
			String  			acuseFirmaDigital,
			String 			nombreMesCarga,
			String 			nombreUsuario 
	) throws AppException {
 
		log.info("generaAcuseArchivoPDF(E)");
		
		String 		nombreArchivo 	= null;
		CreaArchivo archivo 			= null;
		ComunesPDF 	pdfDoc 			= null;
		
		try {
			
			archivo 					= new CreaArchivo();
			nombreArchivo 			= archivo.nombreArchivo()+".pdf";
	 
			// pdfDoc 				= new ComunesPDF(2,strDirectorioTemp+loginUsuario+"."+nombreArchivo);
			pdfDoc 					= new ComunesPDF(2,strDirectorioTemp+nombreArchivo);
			
			String meses[]	 		= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    	= fechaActual.substring(0,2);
			String mesActual   	= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   	= fechaActual.substring(6,10);
			String horaActual  	= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
			pdfDoc.encabezadoConImagenes(
				pdfDoc,
				(String)	cabecera.get("strPais"),
				(String) cabecera.get("iNoNafinElectronico"),
				(String)	cabecera.get("sesExterno"),
				(String) cabecera.get("strNombre"),
				(String) cabecera.get("strNombreUsuario"),
				(String)	cabecera.get("strLogo"),
				strDirectorioPublicacion
			);
	 
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
			pdfDoc.addText("Su solicitud fue recibida con el n�mero de folio: "+rg.getFolio(),"formas",ComunesPDF.CENTER);
			pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
			
			pdfDoc.setTable(2,70);
			
			/*
			pdfDoc.setCell("CIFRAS CONTROL",								"celda03",	ComunesPDF.CENTER,2,1,1);
			*/
			pdfDoc.setCell("Datos del Acuse",							"celda03",	ComunesPDF.CENTER,2,1,1);
			
			pdfDoc.setCell("Acuse firma digital",						"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(acuseFirmaDigital,							"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.setCell("Tipo de Operaci�n",							"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell("09 Comisiones",								"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.setCell("No. total de registros transmitidos",	"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(numeroRegistros,								"formas",	ComunesPDF.RIGHT);
 
			pdfDoc.setCell("Mes de carga",								"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(nombreMesCarga,								"formas",	ComunesPDF.RIGHT);
 
			pdfDoc.setCell("Fecha de carga",								"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(rg.getFecha(),									"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.setCell("Hora de carga",								"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(rg.getHora(),									"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.setCell("Fecha Valor",									"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(rg.getFechaValor(),							"formas",	ComunesPDF.RIGHT);
			
			pdfDoc.setCell("Usuario",										"celda02",	ComunesPDF.CENTER);
			pdfDoc.setCell(loginUsuario+" - "+nombreUsuario,	   "formas",	ComunesPDF.RIGHT);
			
			pdfDoc.addTable();
			pdfDoc.endDocument();
		
		}catch(Exception e){
			
			log.error("generaAcuseArchivoPDF(Exception)");
			e.printStackTrace();
			log.error("rg                               = <" + rg                                + ">");
			log.error("loginUsuario                     = <" + loginUsuario                      + ">");
			log.error("cabecera                         = <" + cabecera                          + ">");
			log.error("strDirectorioTemp                = <" + strDirectorioTemp                 + ">");
			log.error("strDirectorioPublicacion         = <" + strDirectorioPublicacion          + ">");
			log.error("numeroRegistros                  = <" + numeroRegistros                   + ">");
			log.error("montoTotalComisiones             = <" + montoTotalComisiones              + ">");
			log.error("acuseFirmaDigital                = <" + acuseFirmaDigital                 + ">");
			log.error("nombreMesCarga                   = <" + nombreMesCarga                    + ">");
			log.error("nombreUsuario                    = <" + nombreUsuario                     + ">"); 
			throw new AppException("Ocurri� un error al generar el PDF con la informaci�n del acuse.");
			
		} finally {
			
			log.info("generaAcuseArchivoPDF(S)");
			
		}
		
		return nombreArchivo;	
	
	}
	
}