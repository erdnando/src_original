package com.netro.garantias;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.math.BigDecimal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;

import org.apache.commons.logging.Log;

public class ConsSolicIFGAbeanExt implements IQueryGeneratorRegExtJS {

	private final static Log log = ServiceLocator.getInstance().getLog(ConsSolicIFGAbeanExt.class);

	StringBuffer qrySentencia = new StringBuffer("");
	StringBuffer	condicion  =new StringBuffer(); 		
	private List   conditions;
	
	/*FODEA 000 - 2009 Garantias*/
	private String iTipoOperacion = "0";
	private String iIF = "0";
	private String fecha_oper_ini = "";
	private String fecha_oper_fin = "";
	private String cbo_situacion = "";
	private String programa="";
	private String folio="";
	
	/*FODEA 000 - 2009 Garantias*/
	public ConsSolicIFGAbeanExt() { }


	// ------------------------------------------------------------------------------------------------------------------ //
	/**
	* Obtiene el query para obtener los totales de la consulta
	* @return Cadena con la consulta de SQL, para obtener los totales
	*/
	public String getAggregateCalculationQuery() { 
		return "";
	}
	
	
	/**
	* Obtiene el query para la CONSULTA
	* @return Cadena con la consulta de SQL
	*  LLAVES!!!
	*/
	public String getDocumentQuery() { 
		log.info("getDocumentQuery(E) :::...");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();		//binds
		condicion		= new StringBuffer();	//condiciones del query
		
		
		if ( !"0".equals(iTipoOperacion)) {
			condicion.append( " AND s.ic_tipo_operacion = ?" );  
			conditions.add(iTipoOperacion);
		}
		
		if ( "2".equals(iTipoOperacion)) {
			condicion.append( " AND s.ic_situacion != 7 " );  
		}
			
		if (iIF != null && !iIF.equals("") && !iIF.equals("0")) {
			condicion.append( "  AND i.ic_if = ? " );  
			conditions.add(iIF.toString());
		}
		
		if (!"".equals(fecha_oper_ini) && !"".equals(fecha_oper_fin)) {
			condicion.append( "AND es.df_fecha_hora BETWEEN TO_DATE(?, 'DD/MM/YYYY') AND TO_DATE(?, 'DD/MM/YYYY') + 1" );  
			conditions.add(fecha_oper_ini);
			conditions.add(fecha_oper_fin);
		}	
			
		if (!"".equals(cbo_situacion)) {
			condicion.append( "AND es.ic_situacion = ?" );  
			conditions.add(cbo_situacion);
		}
		
		if (!"".equals(folio)) {
			condicion.append( "AND es.ic_folio = ?" );  
			conditions.add(folio);
		}
		
		if (!"".equals(programa)) {
			condicion.append( "AND es.cg_programa = ?" );  
			conditions.add(programa);
		}
		
		qrySentencia.append( " SELECT es.ic_if_siag, es.ic_folio   " +
									" FROM gti_estatus_solic es, gticat_situacion s, comcat_if i "+			
									" WHERE es.ic_situacion = s.ic_situacion "+
									" 		AND es.ic_tipo_operacion = s.ic_tipo_operacion "+									
									" 		AND es.ic_if_siag = i.ic_if_siag "+ condicion +				 
									" ORDER BY es.df_fecha_hora DESC ");
		
		log.info("getDocumentQuery :::...  "+qrySentencia);
		log.info("conditions :::..."+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	
	/**
	* Obtiene el query para obtener la consulta
	* @return Cadena con la consulta de SQL, para obtener la consulta
	*/
	public String getDocumentSummaryQueryForIds(List pageIds) 
	{		
		log.info("getDocumentSummaryQueryForIds(E) :::...");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();		//binds
		condicion		= new StringBuffer();	//condiciones del query
		
		
		qrySentencia.append( " SELECT i.ic_if AS clave_if, i.cg_razon_social AS nombre_if, " +
								" 		es.ic_usuario_facultado AS tipo_envio, " +
								" 		TO_CHAR (es.df_fecha_hora, 'dd/mm/yyyy hh24:mi') AS fecha, " +
								" 		to_char(es.ic_folio) as ic_folio, s.cg_descripcion AS situacion, " +
								" 		es.in_registros_acep, " +
								"		es.in_registros_rech, es.in_registros, es.ic_situacion, " + 
								" 		es.fn_impte_total AS monto_enviado, " + 
								" 		to_char(es.in_folio_reproceso) AS reproceso, " +
								"  	'ConsSolicIFGAbean:getQueryPorEstatus(1)' AS pantalla, " +
								"     es.cg_programa AS programa  " +
								" FROM gti_estatus_solic es, gticat_situacion s, comcat_if i "+			
								" WHERE es.ic_situacion = s.ic_situacion "+
								" 		AND es.ic_tipo_operacion = s.ic_tipo_operacion "+
								" 		AND es.ic_if_siag = i.ic_if_siag  " );
						
			for(int i=0;i<pageIds.size();i++) {
				List lItem = (ArrayList)pageIds.get(i);
				if(i==0) {
					qrySentencia.append("  AND  ( "); 
				} else {
					qrySentencia.append("  OR  "); 
				}
				qrySentencia.append("  ( es.ic_if_siag = ? AND es.ic_folio = ?)"); 
					
				conditions.add(lItem.get(0).toString());
				conditions.add(lItem.get(1).toString());
			}		
			
			qrySentencia.append(" ) " +
			" ORDER BY es.df_fecha_hora DESC ");
		
		
		log.info("getDocumentSummaryQueryForIds :::...  "+qrySentencia);
		log.info("conditions :::..."+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	
	public String getDocumentQueryFile() {
		
		log.info("getDocumentQueryFile(E) :::...");
		
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();		//binds
		condicion		= new StringBuffer();	//condiciones del query
		
	
		if ( !"0".equals(iTipoOperacion)) {
			condicion.append( " AND s.ic_tipo_operacion = ?" );  
			conditions.add(iTipoOperacion);
		}
		
		if ( "2".equals(iTipoOperacion)) {
			condicion.append( " AND s.ic_situacion != 7 " );  
		}
			
		if ( iIF != null && !iIF.equals("") && !iIF.equals("0")) {
			condicion.append( "  AND i.ic_if = ? " );  
			conditions.add(iIF.toString());
		}
		
		if (!"".equals(fecha_oper_ini) && !"".equals(fecha_oper_fin)) {
			condicion.append( "AND es.df_fecha_hora BETWEEN TO_DATE(?, 'DD/MM/YYYY') AND TO_DATE(?, 'DD/MM/YYYY') + 1" );  
			conditions.add(fecha_oper_ini);
			conditions.add(fecha_oper_fin);
		}	
			
		if (!"".equals(cbo_situacion)) {
			condicion.append( "AND es.ic_situacion = ?" );  
			conditions.add(cbo_situacion);
		}
		
		if (!"".equals(folio)) {
			condicion.append( "AND es.ic_folio = ?" );  
			conditions.add(folio);
		}
		
		if (!"".equals(programa)) {
			condicion.append( "AND es.cg_programa = ?" );  
			conditions.add(programa);
		}
		
		qrySentencia.append( " SELECT  i.ic_if AS clave_if, i.cg_razon_social AS nombre_if,"+  /*FODEA 000 - 2009 Garantias*/
									" 	es.ic_usuario_facultado AS tipo_envio,"+  /*FODEA 000 - 2009 Garantias*/
									"  TO_CHAR (es.df_fecha_hora, 'dd/mm/yyyy hh24:mi') AS fecha,"   +
									"  to_char(es.ic_folio) as ic_folio, s.cg_descripcion AS situacion, es.in_registros_acep,"   +
									"  es.in_registros_rech, es.in_registros, es.ic_situacion,"   +  /*FODEA 000 - 2009 Garantias*/
									"  es.fn_impte_total as monto_enviado "+  /*FODEA 000 - 2009 Garantias*/
									"  , to_char(es.in_folio_reproceso) AS reproceso "+//FODEA 012-2009
									"  , es.cg_programa as programa	"+//Fodea 024-2012
									" FROM gti_estatus_solic es, gticat_situacion s, comcat_if i"   +
									" WHERE es.ic_situacion = s.ic_situacion"   +
									"   AND es.ic_tipo_operacion = s.ic_tipo_operacion"   +
									"   AND es.ic_if_siag = i.ic_if_siag"   +
									condicion+
									" ORDER BY es.df_fecha_hora desc");
									
		log.info("getDocumentQueryFile :::...  "+qrySentencia);
		log.info("conditions :::..."+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
		
	} 	
	
	
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo)
	{	
		String nombreArchivo = "";
		return  nombreArchivo;
	}
	
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	/*Este metodo se utiliza para crear archivos segun su tipo (PDF, CSV) SIN paginador*/
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		int total = 0;
		
		try {
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);
		
			contenidoArchivo = new StringBuffer();		
			contenidoArchivo.append("Nombre IF,	Fecha y Hora de Operaci�n,	Folio de la Operaci�n,	Situaci�n,	Usuario,	N�mero de operaciones aceptadas,	N�mero de operaciones con errores,	Total de Operaciones	\n");


			while (rs.next())	{	
			
				String nombreIF = (rs.getString("NOMBRE_IF") == null) ? "" : rs.getString("NOMBRE_IF");	
				String fechaHorario = (rs.getString("FECHA") == null) ? "" : rs.getString("FECHA");	
				String folioOpera = (rs.getString("IC_FOLIO") == null) ? "" : rs.getString("IC_FOLIO");	
				String situacion = (rs.getString("SITUACION") == null) ? "" : rs.getString("SITUACION");	
				String usuario = (rs.getString("TIPO_ENVIO") == null) ? "" : rs.getString("TIPO_ENVIO");	
				String operAceptadas = (rs.getString("IN_REGISTROS_ACEP") == null) ? "0" : rs.getString("IN_REGISTROS_ACEP");	
				String operErrores = (rs.getString("IN_REGISTROS_RECH") == null) ? "0" : rs.getString("IN_REGISTROS_RECH");	
				String totalOper = (rs.getString("IN_REGISTROS") == null) ? "0" : rs.getString("IN_REGISTROS");	 
							
				contenidoArchivo.append(nombreIF.replace(',',' ')+", "+
											fechaHorario.replace(',',' ')+", "+
											"'"+folioOpera.replace(',',' ')+"', "+
											situacion.replace(',',' ')+", "+
											"'"+usuario.replace(',',' ')+"', "+
											operAceptadas.replace(',',' ')+", "+											
											operErrores.replace(',',' ')+", "+
											totalOper.replace(',',' ')+"\n");
			
				total++;
				if(total==1000){					
					total=0;	
					buffer.write(contenidoArchivo.toString());
					contenidoArchivo = new StringBuffer();//Limpio  
				}
				
			}
			buffer.write(contenidoArchivo.toString());
			buffer.close();	
			contenidoArchivo = new StringBuffer();//Limpio    
			
			
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;	
	}
	
	
	
	public HashMap  getdatosPreAcuse( String folio ) throws AppException {
		System.out.println("getdatosPreAcuse(E)");
		
		boolean				lbSinError 		= true;
		AccesoDB 			con 		= new AccesoDB();
		PreparedStatement	ps;
		ResultSet			rs;				
		StringBuffer 	SQL = new StringBuffer();   
		List varBind = new ArrayList();
		HashMap	registros = new HashMap();
		int numReg = 0;
		BigDecimal monto_total = new BigDecimal(0);
		VectorTokenizer vt = new VectorTokenizer();
		Vector vct = null;
		try{
			con.conexionDB();
				
				SQL = new StringBuffer();   
				SQL.append( " select cg_contenido  " +
								" from gti_arch_rechazo " +
								" where ic_folio = ?   ");
				
				varBind = new ArrayList();	
				varBind.add(folio);
				System.out.println("SQL "+SQL);
				System.out.println("varBind "+varBind);
				ps = con.queryPrecompilado(SQL.toString(), varBind);
				rs = ps.executeQuery();
				while(rs!=null && rs.next()){

					//System.out.println("rs.getString(cg_contenido)"+rs.getString("cg_contenido"));
					vt = new VectorTokenizer(rs.getString("cg_contenido"),"@");
					vct = vt.getValuesVector();
					System.out.println("vct  "+vct.size());
					
					if( vct.size()>6){
						numReg++;
						//System.out.println("vct.get(6).toString()"+vct.get(6).toString());
						monto_total = monto_total.add(new BigDecimal(vct.get(6).toString()));
						//System.out.println("monto_total---"+monto_total);
					}
				}
				
				rs.close();
				ps.close();
							
				registros.put("NUM_REG", String.valueOf(numReg));
				registros.put("MONTO_TOTAL", monto_total.toPlainString());
				
				System.out.println("v" +registros);
				
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return registros;
	}
	
	/**
	 * Metodo para generar el archivo pdf del acuse de reprocesar Errores
	 * @return 
	 * @param path
	 * @param parametros
	 * @param request
	 */
	public String crearPdfAcuse( HttpServletRequest request, List parametros, String path   ) {
		String nombreArchivo = "";
		
			try{
			
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
				
				String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual		= fechaActual.substring(0,2);
				String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual		= fechaActual.substring(6,10);
				String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
				pdfDoc.addText("Su solicitud fue recibida con el n�mero de folio: "+folio,"formas",ComunesPDF.CENTER);
				pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
			
				String tot_registros=  parametros.get(0).toString();
				String monto_total=  parametros.get(1).toString();
				String mes_anioCarga=  parametros.get(2).toString();				
				String fecha=  parametros.get(3).toString();
				String hora=  parametros.get(4).toString();
				String strNombreUsuario=  parametros.get(5).toString();
										
			
				pdfDoc.setTable(2,70);			
				pdfDoc.setCell("CIFRAS CONTROL","celda03",ComunesPDF.CENTER,2,1,1);
			
				pdfDoc.setCell("No. total de regisrtos","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell(tot_registros,"formas",ComunesPDF.RIGHT);
			
				pdfDoc.setCell("Monto total de los registros transmitidos","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(monto_total,2),"formas",ComunesPDF.RIGHT);
			
				pdfDoc.setCell("Mes de carga","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell(mes_anioCarga,"formas",ComunesPDF.RIGHT);
			
				pdfDoc.setCell("Fecha de carga","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell(fecha,"formas",ComunesPDF.RIGHT);
			
				pdfDoc.setCell("Hora de carga","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell(hora,"formas",ComunesPDF.RIGHT);
			
				pdfDoc.setCell("Usuario","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell(strNombreUsuario,"formas",ComunesPDF.RIGHT);
			
				
				pdfDoc.addTable();
				pdfDoc.endDocument();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo",e);
			}finally{
				try{
				}catch(Exception e){}
			}
		return nombreArchivo;
	}
	
	
	
	// ------------------------------------------------------------------------------------------------------------------ //
	

 
	
	/*****************************************************
									SETTERS         
	*******************************************************/
	
  public void setiTipoOperacion(String iTipoOperacion) { this.iTipoOperacion = iTipoOperacion; }
  
	/*FODEA 000 - 2009 Garantias*/
  public void setiIF(String iIF) { this.iIF = iIF; }
	
  public void setFechaOperIni(String fecha_oper_ini){ this.fecha_oper_ini = fecha_oper_ini; }
  
  public void setFechaOperFin(String fecha_oper_fin){ this.fecha_oper_fin = fecha_oper_fin;  }
  
  public void setSituacion(String cbo_situacion){ this.cbo_situacion = cbo_situacion; }
  
  public void setPrograma(String programa) { this.programa = programa;	}
	
  public void setFolio(String folio) { this.folio = folio; }
  

	/*****************************************************
								GETTERS
	*******************************************************/

	public String getiIF() { return iIF; }

	public String getFechaOperIni() { return fecha_oper_ini; }
	
	public String getFechaOperFin() { return fecha_oper_fin; }
	
	public String getSituacion() { return cbo_situacion; }
	
	public String getPrograma() { return programa; }

	public String getFolio() { return folio;	}
	
	public List getConditions() {  return conditions;  }  
	
	
}//ConsSolicIFGAbeanExt