package com.netro.garantias;
import org.apache.commons.logging.Log;
import netropology.utilerias.*;

/**Fodea 030-2010-030 Consulta de Resultados Desembolsos Masivos
 * @autor Deysi Laura Hernández Contreras
 */
 
public class ConsResemMasivo { 

//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsResemMasivo.class);
	
	private int iIF_SIAG = 0;
  private String df_fecha_oper_de = ""; 
  private String df_fecha_oper_a  = "";
  private String combsituacion ="";
  private String folioperacion ="";
  

	public ConsResemMasivo() { }

	public void setiIF_SIAG(int iIF_SIAG) {
		this.iIF_SIAG = iIF_SIAG;
	}
  public void setdf_fecha_oper_de(String df_fecha_oper_de) {
		this.df_fecha_oper_de = df_fecha_oper_de;
	}
  
  public void setdf_fecha_oper_a(String df_fecha_oper_a) {
		this.df_fecha_oper_a = df_fecha_oper_a;
	}
  
  public void setfolioperacion(String folioperacion) {
		this.folioperacion = folioperacion;
	}
  
  public void setcombsituacion(String combsituacion) {
		this.combsituacion = combsituacion;
	}

	public String getQuerySolicitudes() {
    log.info("Inicia (E)");
    
		StringBuffer	sbQuery		= new StringBuffer();
		String 			sCondicion	= "";
		String 			sReferencia	= ",'ConsResemMasivo:getQuerySolicitudes("+iIF_SIAG+")' as PANTALLA ";
  
		if(iIF_SIAG!=0)
			sCondicion += " AND es.ic_if_siag = ? ";
    

   if(!df_fecha_oper_de.equals("") && !df_fecha_oper_a.equals("")){
      sCondicion += " AND es.df_fecha_hora >= TO_DATE (?, 'DD/MM/YYYY')  "+
                  " AND es.df_fecha_hora <= TO_DATE (?, 'DD/MM/YYYY')+1 "; 
                  
  }
   if(!folioperacion.equals("")){
      sCondicion += " AND es.ic_folio = ? ";
   }
   
   if(!combsituacion.equals("")){
      sCondicion += " AND es.ic_situacion = ? ";
   }  
  
		sbQuery.append(
			" SELECT   " +
			" TO_CHAR (es.df_fecha_hora, 'dd/mm/yyyy hh24:mi') AS fecha, "   +
			" TO_CHAR(es.ic_folio) as ic_folio, "+
      " s.cg_descripcion AS situacion, "+
      " es.ic_situacion, "   +
			" es.in_registros, "+
      " es.IN_REGISTROS_ACEP, "+
      " es.IN_REGISTROS_RECH, "+
      " es.fn_impte_total "+
			sReferencia+
			"FROM "+
      //" gti_solicitud_pago sp, "+
      " gticat_situacion s, "+
      " gti_estatus_solic es"   +
			" WHERE es.ic_situacion = s.ic_situacion"   +
			" AND es.ic_tipo_operacion = s.ic_tipo_operacion"   +
		//	" AND es.ic_folio   = sp.ic_folio"   +
//			" AND es.ic_if_siag = sp.ic_if_siag"   +
			" AND es.ic_tipo_operacion = 6 "   +
			sCondicion+
			" ORDER BY es.df_fecha_hora desc");
      
      log.debug("sbQuery:: "+sbQuery);
      log.debug("iIF_SIAG:: "+iIF_SIAG);
      log.debug("df_fecha_oper_de:: "+df_fecha_oper_de);
      log.debug("df_fecha_oper_a:: "+df_fecha_oper_a);
      log.debug("combsituacion:: "+combsituacion);
      log.debug("folioperacion:: "+folioperacion);      
  
      log.info("Termina (S)");
		return sbQuery.toString();
	} // getQuerySolicitudes()

}//ConsResemMasivo
