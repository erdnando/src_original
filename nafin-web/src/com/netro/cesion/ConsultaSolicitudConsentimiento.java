package com.netro.cesion;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;


public class ConsultaSolicitudConsentimiento implements IQueryGeneratorReg, IQueryGeneratorRegExtJS {	

	public ConsultaSolicitudConsentimiento(){}

	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaSolicitudConsentimiento.class);

	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer 		qrySentencia;
	private List 		conditions;
	private String 	paginaOffset;
	private String 	paginaNo;
	String[]		montos			= new String[2];
	String[]		doctos			= new String[2];
	String[]		numeros;
	
	private String directorio;
	public String noIF;
	public String banco;
	public String pyme;
	public String estatus;
	public String noContrato;
	public String fvigenciaIni;
	public String fvigenciaFin;
	public String fsolicitudIni;
	public String fsolicitudFin;
	public String campo1;
	public String campo2;
	public String epo;
	private String plazoContrato;
	private String claveTipoPlazo;
	private String loginUsuarioEpo;
	private String estatusProrroga;

	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */
		public String getAggregateCalculationQuery() {
			log.info("ConsultaSolicitudConsentimiento::getAggregateCalculationQuery(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();

			log.debug("..:: qrySentencia "+qrySentencia.toString());
			log.debug("..:: conditions: "+conditions);
			log.info("ConsultaSolicitudConsentimiento::getAggregateCalculationQuery(S)");
			return qrySentencia.toString();
		}
	
	
	 /**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
		public String getDocumentQuery() {
			log.info("ConsultaSolicitudConsentimiento::getDocumentQuery(E)");
			qrySentencia = new StringBuffer();
			conditions = new ArrayList();
	
			qrySentencia.append(" SELECT sol.ic_solicitud AS clave_solicitud");
			qrySentencia.append(" FROM cder_solicitud sol");
			qrySentencia.append(", comcat_epo e");
			qrySentencia.append(", comcat_if i");
			qrySentencia.append(", cdercat_clasificacion_epo cl");
			qrySentencia.append(", cdercat_tipo_contratacion con");
			qrySentencia.append(", cdercat_estatus_solic es");
			qrySentencia.append(", comcat_pyme p");
			qrySentencia.append(", cdercat_tipo_plazo stp");
			qrySentencia.append(" WHERE sol.ic_epo = e.ic_epo");
			qrySentencia.append(" AND sol.ic_if = i.ic_if ");
			qrySentencia.append(" AND sol.ic_clasificacion_epo = cl.ic_clasificacion_epo");
			qrySentencia.append(" AND sol.ic_tipo_contratacion = con.ic_tipo_contratacion");
			qrySentencia.append(" AND sol.ic_estatus_solic = es.ic_estatus_solic");
			qrySentencia.append(" AND sol.ic_pyme = p.ic_pyme");
			qrySentencia.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
			qrySentencia.append(" AND sol.ic_estatus_solic IN (?, ?, ?, ?, ?, ? , ?, ?, ? )");
			
			conditions.add(new Integer(2));
			conditions.add(new Integer(3));
			conditions.add(new Integer(4));
			conditions.add(new Integer(5));
			conditions.add(new Integer(6));
			conditions.add(new Integer(8));
			conditions.add(new Integer(7));
			conditions.add(new Integer(14));
			conditions.add(new Integer(9));
			//usuario epo
			if (loginUsuarioEpo != null && !"".equals(loginUsuarioEpo)) {
				qrySentencia.append(" AND SUBSTR(cl.cg_responsable, 1, 8) = ?");
				conditions.add(loginUsuarioEpo);
			}
			// EPO
			if ((!"".equals(epo) && epo != null)) {
				qrySentencia.append(" AND sol.ic_epo = ?");
				conditions.add(epo);
			}
			// IF
			if (!"".equals(noIF) && noIF != null) {
				qrySentencia.append(" AND sol.ic_if = ?");
				conditions.add(noIF);
			}
			// pyme
			if (!"".equals(pyme) && pyme != null) {
				qrySentencia.append(" AND sol.ic_pyme = ?");
				conditions.add(pyme);
			}
			// estatus
			if (!"".equals(estatus) && estatus != null) {
				qrySentencia.append(" AND sol.ic_estatus_solic = ?");
				conditions.add(estatus);				
			}
			if(!"".equals(estatusProrroga) && estatusProrroga != null   ) {
				qrySentencia.append(" AND sol.cs_estatus_prorroga = ? ");
				conditions.add(estatusProrroga);		
			}
			
			// noContrato
			if (!"".equals(noContrato) && noContrato != null) {
				qrySentencia.append(" AND sol.cg_no_contrato LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')");
				conditions.add(noContrato);
			}
			//Fecha de Vigencia de Contrato
			if ((!"".equals(fvigenciaIni) && fvigenciaIni != null) && (!"".equals(fvigenciaFin) && fvigenciaFin != null)) {
				qrySentencia.append(" AND sol.df_fecha_vigencia_Ini >=  to_date(?, 'dd/mm/yyyy')");
				qrySentencia.append(" AND sol.df_fecha_vigencia_Fin <= to_date(?, 'dd/mm/yyyy')");
				conditions.add(fvigenciaIni);
				conditions.add(fvigenciaFin);
			}
			//Fecha de Solicitud de Contrato
			if ((!"".equals(fsolicitudIni) && fsolicitudIni != null) && (!"".equals(fsolicitudFin) && fsolicitudFin != null)) {
				qrySentencia.append(" AND sol.df_fecha_Solicitud_Ini >= to_date(?, 'dd/mm/yyyy')");
				qrySentencia.append(" AND sol.df_fecha_Solicitud_Ini <= to_date(?, 'dd/mm/yyyy') + 1");
				conditions.add(fsolicitudIni);
				conditions.add(fsolicitudFin);
			}
			

			//plazo del contrato
			if (plazoContrato != null && !"".equals(plazoContrato)) {
				qrySentencia.append(" AND sol.ig_plazo = ?");
				conditions.add(new Integer(plazoContrato));
			}		
			//tipo de plazo del contrato
			if (claveTipoPlazo != null && !"".equals(claveTipoPlazo)) {
				qrySentencia.append(" AND sol.ic_tipo_plazo = ?");
				conditions.add(new Integer(claveTipoPlazo));
			}		
			
			qrySentencia.append(" ORDER BY sol.df_alta_solicitud DESC");
			
			log.debug("..:: qrySentencia: "+qrySentencia.toString());
			log.debug("..:: conditions: "+conditions);
			log.info("ConsultaSolicitudConsentimiento::getDocumentQuery(S)");
			return qrySentencia.toString();
		}

	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */
		public String getDocumentSummaryQueryForIds(List pageIds) {
			log.info("ConsultaSolicitudConsentimiento::getDocumentSummaryQueryForIds(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			
			qrySentencia.append(" SELECT sol.ic_solicitud AS clave_solicitud,");
			qrySentencia.append(" sol.ic_pyme AS clave_pyme, ");//FODEA 041 - 2010 ACF
			//qrySentencia.append(" p.CG_RAZON_SOCIAL AS Pyme, "); 
			qrySentencia.append(" p.CG_RAZON_SOCIAL||';'||sol.cg_empresas_representadas as Pyme, "); //FODEA-024-2014 MOD()
			qrySentencia.append(" p.CG_RFC  AS Rfc,"); 
			qrySentencia.append(" p.IC_PYME AS NoProveedor,");
			qrySentencia.append(" e.cg_razon_social AS Dependencia, ");
			qrySentencia.append(" i.cg_razon_social AS Cecionario,  ");
			qrySentencia.append(" sol.cg_no_contrato AS NoContrato,  ");
			qrySentencia.append(" con.cg_nombre AS Contratacion,  ");
			qrySentencia.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");
			qrySentencia.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");
			qrySentencia.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS plazo_contrato,");
			qrySentencia.append(" TO_CHAR(df_fecha_Solicitud_Ini,'DD/MM/YYYY')  AS fsolicitudIni,  ");
			qrySentencia.append(" cl.cg_area AS Clasificacion,  ");
			qrySentencia.append(" sol.cg_campo1 AS Campo1,  ");
			qrySentencia.append(" sol.cg_campo2 AS Campo2,  ");
			qrySentencia.append(" sol.cg_campo3 AS Campo3,  ");
			qrySentencia.append(" sol.cg_campo4 AS Campo4,  ");
			qrySentencia.append(" sol.cg_campo5 AS Campo5,  ");
			qrySentencia.append(" sol.cg_obj_contrato AS ObContrato,  ");
			qrySentencia.append(" cvp.cg_descripcion AS venanilla_pago,");
			qrySentencia.append(" sol.cg_sup_adm_resob AS sup_adm_resob,");
			qrySentencia.append(" sol.cg_numero_telefono AS numero_telefono,");
			qrySentencia.append(" TO_CHAR(sol.df_fecha_limite_firma,'DD/MM/YYYY') AS FLimite,  ");
			qrySentencia.append(" es.cg_descripcion AS Estatus , ");
			qrySentencia.append(" sol.ic_estatus_solic AS NoEstatus, ");
			qrySentencia.append(" NVL(TO_CHAR(sol.df_fecha_aceptacion,'DD/MM/YYYY'), 'N/A') AS FAceptacion,  ");	
			qrySentencia.append(" sol.ic_epo  AS noEpo ,  ");
			qrySentencia.append(" con.ic_tipo_contratacion AS tipoContratacion, " );
			qrySentencia.append(" sol.cg_comentarios  AS comentarios, ");									
			qrySentencia.append(" sol.ic_pyme AS clave_pyme,  ");
			qrySentencia.append(" TO_CHAR(sol.df_alta_solicitud,'DD/MM/YYYY') as fechasolicitud, ");
			qrySentencia.append(" TO_CHAR(sol.df_firma_contrato, 'dd/mm/yyyy') AS firma_contrato, ");
			qrySentencia.append(" sol.cs_estatus_prorroga AS estatus_prorroga ");
			qrySentencia.append("	,sol.cg_empresas_representadas as epresas ");
			qrySentencia.append(" , sol.IC_GRUPO_CESION  AS GRUPO_CESION ");
			
			qrySentencia.append(" FROM cder_solicitud sol");
			qrySentencia.append(", comcat_epo e");
			qrySentencia.append(", comcat_if i");
			qrySentencia.append(", cdercat_clasificacion_epo cl");
			qrySentencia.append(", cdercat_tipo_contratacion con");
			qrySentencia.append(", cdercat_estatus_solic es");
			qrySentencia.append(", comcat_pyme p");
			qrySentencia.append(", cdercat_tipo_plazo stp");
			qrySentencia.append(", cdercat_ventanilla_pago cvp");
			qrySentencia.append(" WHERE  sol.ic_epo = e.ic_epo");
			qrySentencia.append(" AND sol.ic_if = i.ic_if");
			qrySentencia.append(" AND sol.ic_clasificacion_epo = cl.ic_clasificacion_epo");
			qrySentencia.append(" AND sol.ic_tipo_contratacion = con.ic_tipo_contratacion");
			qrySentencia.append(" AND sol.ic_estatus_solic = es.ic_estatus_solic");
			qrySentencia.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
			qrySentencia.append(" AND sol.ic_pyme = p.ic_pyme");
			qrySentencia.append(" AND sol.ic_epo = cvp.ic_epo(+)");
			qrySentencia.append(" AND sol.ic_ventanilla_pago = cvp.ic_ventanilla_pago(+)");
			qrySentencia.append(" AND (");
			
			for (int i = 0; i < pageIds.size(); i++) { 
				List lItem = (ArrayList)pageIds.get(i);
				
				if(i > 0){qrySentencia.append("  OR  ");}
				
				qrySentencia.append("sol.ic_solicitud = ?");
				conditions.add(new Long(lItem.get(0).toString()));
			}
			
			qrySentencia.append(" ) ");
			qrySentencia.append(" ORDER BY sol.df_alta_solicitud DESC");
			
			log.debug("..:: qrySentencia: "+qrySentencia.toString());
			log.debug("..:: conditions: "+conditions);
			log.info("ConsultaSolicitudConsentimiento::getDocumentSummaryQueryForIds(S)");
			return qrySentencia.toString();
		}

		public String getDocumentQueryFile() {
			log.info("ConsultaSolicitudConsentimiento::getDocumentQueryFile(E)");
			qrySentencia = new StringBuffer();
			conditions = new ArrayList();



			qrySentencia.append(" SELECT sol.ic_solicitud AS clave_solicitud,");
			qrySentencia.append(" sol.ic_pyme AS clave_pyme, ");//FODEA 041 - 2010 ACF
			//qrySentencia.append(" p.CG_RAZON_SOCIAL AS Pyme, "); 
			qrySentencia.append(" p.CG_RAZON_SOCIAL||';'||sol.cg_empresas_representadas as Pyme, "); //FODEA-024-2014 MOD()
			qrySentencia.append(" p.CG_RFC  AS Rfc,"); 
			qrySentencia.append(" p.IC_PYME AS NoProveedor,");
			qrySentencia.append(" e.cg_razon_social AS Dependencia, ");
			qrySentencia.append(" i.cg_razon_social AS Cecionario,  ");
			qrySentencia.append(" sol.cg_no_contrato AS NoContrato,  ");
			qrySentencia.append(" con.cg_nombre AS Contratacion,  ");
			qrySentencia.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");
			qrySentencia.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");
			qrySentencia.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS plazo_contrato,");
			qrySentencia.append(" TO_CHAR(df_fecha_Solicitud_Ini,'DD/MM/YYYY')  AS fsolicitudIni,  ");
			qrySentencia.append(" cl.cg_area AS Clasificacion,  ");
			qrySentencia.append(" sol.cg_campo1 AS Campo1,  ");
			qrySentencia.append(" sol.cg_campo2 AS Campo2,  ");
			qrySentencia.append(" sol.cg_campo3 AS Campo3,  ");
			qrySentencia.append(" sol.cg_campo4 AS Campo4,  ");
			qrySentencia.append(" sol.cg_campo5 AS Campo5,  ");
			qrySentencia.append(" sol.cg_obj_contrato AS ObContrato,  ");
			qrySentencia.append(" cvp.cg_descripcion AS venanilla_pago,");
			qrySentencia.append(" sol.cg_sup_adm_resob AS sup_adm_resob,");
			qrySentencia.append(" sol.cg_numero_telefono AS numero_telefono,");
			qrySentencia.append(" TO_CHAR(sol.df_fecha_limite_firma,'DD/MM/YYYY') AS FLimite,  ");
			qrySentencia.append(" es.cg_descripcion AS Estatus , ");
			qrySentencia.append(" sol.ic_estatus_solic AS NoEstatus, ");
			qrySentencia.append(" TO_CHAR(sol.df_fecha_aceptacion,'DD/MM/YYYY') AS FAceptacion,  ");	
			qrySentencia.append(" sol.ic_epo  AS noEpo ,  ");
			qrySentencia.append(" con.ic_tipo_contratacion AS tipoContratacion, " );
			qrySentencia.append(" sol.cg_comentarios  AS comentarios, ");									
			qrySentencia.append(" sol.ic_pyme AS clave_pyme,  ");
			qrySentencia.append(" TO_CHAR(sol.df_firma_contrato, 'dd/mm/yyyy') AS firma_contrato, ");
			qrySentencia.append(" sol.cs_estatus_prorroga AS estatus_prorroga ");
			qrySentencia.append(" ,sol.cg_empresas_representadas as epresas");
		    qrySentencia.append(" ,sol.IC_GRUPO_CESION AS GRUPO_CESION ");
			qrySentencia.append(" FROM cder_solicitud sol");
			qrySentencia.append(", comcat_epo e");
			qrySentencia.append(", comcat_if i");
			qrySentencia.append(", cdercat_clasificacion_epo cl");
			qrySentencia.append(", cdercat_tipo_contratacion con");
			qrySentencia.append(", cdercat_estatus_solic es");
			qrySentencia.append(", comcat_pyme p");
			qrySentencia.append(", cdercat_tipo_plazo stp");
			qrySentencia.append(", cdercat_ventanilla_pago cvp");
			qrySentencia.append(" WHERE  sol.ic_epo = e.ic_epo");
			qrySentencia.append(" AND sol.ic_if = i.ic_if");
			qrySentencia.append(" AND sol.ic_clasificacion_epo = cl.ic_clasificacion_epo");
			qrySentencia.append(" AND sol.ic_tipo_contratacion = con.ic_tipo_contratacion");
			qrySentencia.append(" AND sol.ic_estatus_solic = es.ic_estatus_solic");
			qrySentencia.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
			qrySentencia.append(" AND sol.ic_pyme = p.ic_pyme");
			qrySentencia.append(" AND sol.ic_epo = cvp.ic_epo(+)");
			qrySentencia.append(" AND sol.ic_ventanilla_pago = cvp.ic_ventanilla_pago(+)");
			qrySentencia.append(" AND sol.ic_estatus_solic IN (?, ?, ?, ?, ?, ?, ?, ?, ?)");
			
			conditions.add(new Integer(2));
			conditions.add(new Integer(3));
			conditions.add(new Integer(4));
			conditions.add(new Integer(5));
			conditions.add(new Integer(6));
			conditions.add(new Integer(8));
			conditions.add(new Integer(7));
			conditions.add(new Integer(14));
			conditions.add(new Integer(9));
			//usuario epo
			if (loginUsuarioEpo != null && !"".equals(loginUsuarioEpo)) {
				qrySentencia.append(" AND SUBSTR(cl.cg_responsable, 1, 8) = ?");
				conditions.add(loginUsuarioEpo);
			}
			// EPO
			if ((!"".equals(epo) && epo != null)) {
				qrySentencia.append(" AND sol.ic_epo = ?");
				conditions.add(epo);
			}
			// IF
			if (!"".equals(noIF) && noIF != null) {
				qrySentencia.append(" AND sol.ic_if = ?");
				conditions.add(noIF);
			}
			// pyme
			if (!"".equals(pyme) && pyme != null) {
				qrySentencia.append(" AND sol.ic_pyme = ?");
				conditions.add(pyme);
			}			
			
			// estatus
			if (!"".equals(estatus) && estatus != null) {
				qrySentencia.append(" AND sol.ic_estatus_solic = ? ");
				conditions.add(estatus);				
			}
			if(!"".equals(estatusProrroga) && estatusProrroga != null   ) {
				qrySentencia.append(" AND sol.cs_estatus_prorroga = ? ");
				conditions.add(estatusProrroga);		
			}  
			
			// noContrato
			if (!"".equals(noContrato) && noContrato != null) {
				qrySentencia.append(" AND sol.cg_no_contrato LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%')");
				conditions.add(noContrato);
			}
			//Fecha de Vigencia de Contrato
			if ((!"".equals(fvigenciaIni) && fvigenciaIni != null) && (!"".equals(fvigenciaFin) && fvigenciaFin != null)) {
				qrySentencia.append(" AND sol.df_fecha_vigencia_Ini >=  to_date(?, 'dd/mm/yyyy')");
				qrySentencia.append(" AND sol.df_fecha_vigencia_Fin <= to_date(?, 'dd/mm/yyyy')");
				conditions.add(fvigenciaIni);
				conditions.add(fvigenciaFin);
			}
			//Fecha de Solicitud de Contrato
			if ((!"".equals(fsolicitudIni) && fsolicitudIni != null) && (!"".equals(fsolicitudFin) && fsolicitudFin != null)) {
				qrySentencia.append(" AND sol.df_fecha_Solicitud_Ini >= to_date(?, 'dd/mm/yyyy')");
				qrySentencia.append(" AND sol.df_fecha_Solicitud_Ini <= to_date(?, 'dd/mm/yyyy') + 1");
				conditions.add(fsolicitudIni);
				conditions.add(fsolicitudFin);
			}
			//plazo del contrato
			if (plazoContrato != null && !"".equals(plazoContrato)) {
				qrySentencia.append(" AND sol.ig_plazo = ?");
				conditions.add(new Integer(plazoContrato));
			}		
			//tipo de plazo del contrato
			if (claveTipoPlazo != null && !"".equals(claveTipoPlazo)) {
				qrySentencia.append(" AND sol.ic_tipo_plazo = ?");
				conditions.add(new Integer(claveTipoPlazo));
			}		
			
			qrySentencia.append(" ORDER BY sol.df_alta_solicitud DESC");
			
			log.debug("..:: qrySentencia: "+qrySentencia.toString());
			log.debug("..:: conditions: "+conditions);
			log.info("ConsultaSolicitudConsentimiento::getDocumentQueryFile(S)");
			return qrySentencia.toString();
		}//getDocumentQueryFile


public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
}

public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		StringBuffer linea = new StringBuffer(1024);
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		int columnasAd = 7;
		try {
		
			CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
			int indiceCamposAdicionales =0;
			String clasificacionEpo ="";
			HashMap camposAdicionalesParametrizados  = null;
			//obtencion de campos Adicionales
			String nombreClasificacionEpo = cesionBean.clasificacionEpo(epo);
			Vector lovDatos = cesionBean.getCamposAdicionales(epo, "5");
		   Map mpParams = cesionBean.getParametrosEpo(epo);
			String csProrrogaContrato = (String)mpParams.get("CDER_CS_PRORROGA_CONTRATO");
			
			if(tipo.equals("PDF")){
				HttpSession session = request.getSession();
				String tipoUsuario = (String)session.getAttribute("strTipoUsuario");
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				System.out.println("path "+path);
				System.out.println("nombreArchivo "+nombreArchivo);
				
					
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				pdfDoc.addText("Consulta Solicitud de Consentimiento ","formas",ComunesPDF.CENTER);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
			
				if(lovDatos.size() >0) columnasAd -=lovDatos.size();
				
				System.out.println("columnasAd "+columnasAd);
				
				pdfDoc.setTable(15, 100);			
				pdfDoc.setCell("Intermediario Financiero (Cesionario)","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Pyme (Cedente)","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("RFC","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No Proveedor","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Representante legal","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de solicitud Pyme ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No. Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Firma Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto / Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de Contratacion","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("F. Inicio Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("F. Final Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo del Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(nombreClasificacionEpo,"celda01",ComunesPDF.CENTER);				
				if (lovDatos !=null ) {
					for (int i=0; i<lovDatos.size(); i++) { 
						Vector lovRegistro = (Vector) lovDatos.get(i);
						String lsNombreCampo 	= (String) lovRegistro.get(1);
						pdfDoc.setCell(lsNombreCampo,"celda01",ComunesPDF.CENTER);					
					} //for
				}				
				pdfDoc.setCell("Ventanilla de Pago","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Supervisor/Administrador/Residente de Obra","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tel�fono","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Objeto del Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Comentarios","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Aceptaci�n  o Rechazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus Actual","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus Pr�rroga","celda01",ComunesPDF.CENTER);
				if(columnasAd>0){
					pdfDoc.setCell("   ","celda01",ComunesPDF.CENTER,++columnasAd);	
				}
		}else if(tipo.equals("CSV")) {
					
				contenidoArchivo.append("Intermediario Financiero (Cesionario)" + ",");		
				contenidoArchivo.append("Pyme (Cedente)"+ ",");		
				contenidoArchivo.append("RFC"+ ",");		
				contenidoArchivo.append("No Proveedor"+ ",");		
				contenidoArchivo.append("Representante legal"+ ",");		
				contenidoArchivo.append("Fecha de solicitud Pyme "+ ",");		
				contenidoArchivo.append("No. Contrato"+ ",");		
				contenidoArchivo.append("Firma Contrato"+ ",");		
				contenidoArchivo.append("Monto / Moneda"+ ",");		
				contenidoArchivo.append("Tipo de Contratacion"+ ",");		
				contenidoArchivo.append("F. Inicio Contrato"+ ",");		
				contenidoArchivo.append("F. Final Contrato"+ ",");		
				contenidoArchivo.append(" Plazo del Contrato"+ ",");		
				contenidoArchivo.append(nombreClasificacionEpo+ ",");		
				if (lovDatos !=null ) {
					for (int i=0; i<lovDatos.size(); i++) { 
						Vector lovRegistro = (Vector) lovDatos.get(i);
						String lsNombreCampo 	= (String) lovRegistro.get(1);						
						contenidoArchivo.append(lsNombreCampo+ ",");	
					} //for
				}				
				contenidoArchivo.append("Ventanilla de Pago"+ ",");	
				contenidoArchivo.append("Supervisor/Administrador/Residente de Obra"+ ",");	
				contenidoArchivo.append("Tel�fono"+ ",");	
				contenidoArchivo.append("Objeto del Contrato"+ ",");	
				contenidoArchivo.append("Comentarios"+ ",");	
				contenidoArchivo.append("Fecha de Aceptaci�n  o Rechazo"+ ",");	
				contenidoArchivo.append("Estatus Actual"+ ",");	
				contenidoArchivo.append("Estatus Pr�rroga"+ "\n");	
		}	
				
			while (rs.next()) {
                
				String cesionario = rs.getString("Cecionario");
				String pyme=(rs.getString("Pyme")==null)?" ":rs.getString("Pyme"); //FODEA-025-2014 MOD()
				String rfc = rs.getString("Rfc");
				String noProveedor = rs.getString("NoProveedor");
                
				///Se obtiene el representante Legal de la Pyme
				StringBuffer nombreContacto = new StringBuffer();
				UtilUsr utilUsr = new UtilUsr();
				boolean usuarioEncontrado = false;
				boolean perfilIndicado = false;
                
			    String grupoCesion = rs.getString("grupo_cesion");                
			    List lEmpresasGrupo =  cesionBean.getEmpresasXGrupoCder(grupoCesion);
			    //System.out.println("lEmpresasGrupo:"+lEmpresasGrupo);
			    for(int i=0;i<lEmpresasGrupo.size();i++) {
			        HashMap hmEmpresa = (HashMap)lEmpresasGrupo.get(i);
			        String str_representantes = (String)hmEmpresa.get("representantes");
			        str_representantes = str_representantes.replace(",", "; ");
			        if (i > 0) {
			            nombreContacto.append(" / ");
			        }
			        nombreContacto.append(str_representantes);
			    }                
                /*
				List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(noProveedor, "P");
				for (int i = 0; i < usuariosPorPerfil.size(); i++) {
					String loginUsuarioEpo = (String)usuariosPorPerfil.get(i);
						Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
							perfilIndicado = true;
							 if (i > 0) {
								nombreContacto.append(" / ");
							 }
							nombreContacto.append(usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno()+" ");
				}
                */
                
				String fSolicitudIni = (rs.getString("FSolicitudIni")==null)?" ":rs.getString("FSolicitudIni"); 				
				String noContrato =(rs.getString("NoContrato")==null)?" ":rs.getString("NoContrato"); 				
					
				StringBuffer monto_moneda = cesionBean.getMontoMoneda(rs.getString("clave_solicitud"));
				String monto_moneda2 = monto_moneda.toString().replaceAll(",", "");
					
				String contratacion = (rs.getString("Contratacion")==null)?" ":rs.getString("Contratacion");  
				String fechaIniContra=  (rs.getString("fecha_inicio_contrato")==null)?" ":rs.getString("fecha_inicio_contrato"); 
				String fechaFinContra =(rs.getString("fecha_fin_contrato")==null)?" ":rs.getString("fecha_fin_contrato");  
				String plazo_contraro = (rs.getString("plazo_contrato")==null)?" ":rs.getString("plazo_contrato");  
				String clasificacion = (rs.getString("Clasificacion")==null)?" ":rs.getString("Clasificacion"); 
						
				String campo1="", campo2="", campo3 ="", campo4="", campo5 ="";
		
				if (lovDatos !=null ) {
					for (int i=0; i<lovDatos.size(); i++) { 
						Vector	lovRegistro = (Vector) lovDatos.get(i);
						int 	liNumCampo 	= Integer.parseInt((String)lovRegistro.get(0));			
						if (liNumCampo== 1) { 
								campo1 = (rs.getString("Campo1")==null)?"":rs.getString("Campo1");
						} if (liNumCampo== 2) { 
							campo2 = (rs.getString("Campo2")==null)?"":rs.getString("Campo2");
						} if (liNumCampo== 3) { 
							campo3 =(rs.getString("Campo3")==null)?"":rs.getString("Campo3");
						} if (liNumCampo== 4) { 
							campo4 = (rs.getString("Campo4")==null)?"":rs.getString("Campo4");
						} if (liNumCampo== 5) { 
							campo5 = (rs.getString("Campo5")==null)?"":rs.getString("Campo5");
						} 		
					} // termina for 
				} //if 
				String firmaContrato =  (rs.getString("firma_contrato")==null)?" ":rs.getString("firma_contrato"); 
				String ventanilla =  (rs.getString("venanilla_pago")==null)?" ":rs.getString("venanilla_pago"); 
				String supadmini = (rs.getString("sup_adm_resob")==null)?" ":rs.getString("sup_adm_resob"); 
				String telefono = (rs.getString("numero_telefono")==null)?" ":rs.getString("numero_telefono"); 
				String obContrato = (rs.getString("ObContrato")==null)?" ":rs.getString("ObContrato");
				String comentarios = (rs.getString("comentarios")==null)?" ":rs.getString("comentarios"); 		 
				String fechaAceptacion = (rs.getString("FAceptacion")==null)?"NA":rs.getString("FAceptacion"); 				
				String estatus = (rs.getString("Estatus")==null)?" ":rs.getString("Estatus"); 
				String estatusProrroga = (rs.getString("estatus_prorroga")==null)?" ":rs.getString("estatus_prorroga");
				if ("N".equals(csProrrogaContrato)) {
					estatusProrroga = "N/A";
				} else if (estatusProrroga.equals("P")) {
					estatusProrroga = "Solicitud Pr�rroga ";
				} else if (estatusProrroga.equals("R")) {
					estatusProrroga = "Pr�rroga Rechazada";
				} else if (estatusProrroga.equals("A")) {
					estatusProrroga = "Pr�rroga Aceptada";
				} else {
					estatusProrroga = "N/A";
				}
				
				if(tipo.equals("PDF")) {
					pdfDoc.setCell(cesionario,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(pyme.replaceAll(";","\n"),"formas",ComunesPDF.CENTER);//FODEA-025-2014 MOD()
					pdfDoc.setCell(rfc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(noProveedor,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nombreContacto.toString(),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fSolicitudIni,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(noContrato,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(firmaContrato,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(monto_moneda2.replaceAll("<br/>", "\n"),"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(contratacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaIniContra,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaFinContra,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(plazo_contraro,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(clasificacion,"formas",ComunesPDF.CENTER);	
				
					if (lovDatos !=null ) {
						for (int i=0; i<lovDatos.size(); i++) { 
							Vector	lovRegistro = (Vector) lovDatos.get(i);
							int 	liNumCampo 	= Integer.parseInt((String)lovRegistro.get(0));			
							if (liNumCampo== 1) { 
									pdfDoc.setCell(campo1,"formas",ComunesPDF.CENTER);
							} if (liNumCampo== 2) { 
								pdfDoc.setCell(campo2,"formas",ComunesPDF.CENTER);
							} if (liNumCampo== 3) { 
								pdfDoc.setCell(campo3,"formas",ComunesPDF.CENTER);
							} if (liNumCampo== 4) { 
								pdfDoc.setCell(campo4,"formas",ComunesPDF.CENTER);
							} if (liNumCampo== 5) { 
								pdfDoc.setCell(campo5,"formas",ComunesPDF.CENTER);	
							} 		
						} // termina for 
					} //if 				
					pdfDoc.setCell(ventanilla,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(supadmini,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(telefono,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(obContrato,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(comentarios,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaAceptacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(estatus,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(estatusProrroga,"formas",ComunesPDF.CENTER);
					if(columnasAd>0){
						pdfDoc.setCell("   ","formas",ComunesPDF.CENTER,columnasAd);	
					}
				
				}//if(tipo.equals("PDF")) {
				
				if(tipo.equals("CSV")) {
					contenidoArchivo.append(cesionario.replaceAll(",", "")+ ",");	
					contenidoArchivo.append(pyme.replaceAll(",", "")+ ",");	
					contenidoArchivo.append(rfc.replaceAll(",", "")+ ",");	
					contenidoArchivo.append(noProveedor.replaceAll(",", "")+ ",");	
					contenidoArchivo.append(nombreContacto.toString().replaceAll(",", "")+ ",");	
					contenidoArchivo.append(fSolicitudIni+ ",");	
					contenidoArchivo.append(noContrato+ ",");	
					contenidoArchivo.append(firmaContrato+ ",");	
					contenidoArchivo.append(monto_moneda2.replaceAll(",", "").replaceAll("<br/>", "|")+ ",");	
					contenidoArchivo.append(contratacion.replaceAll(",", "")+ ",");	
					contenidoArchivo.append(fechaIniContra.replaceAll(",", "")+ ",");	
					contenidoArchivo.append(fechaFinContra.replaceAll(",", "")+ ",");	
					contenidoArchivo.append(plazo_contraro.replaceAll(",", "")+ ",");	
					contenidoArchivo.append(clasificacion.replaceAll(",", "")+ ",");	
					if (lovDatos !=null ) {
						for (int i=0; i<lovDatos.size(); i++) { 
							Vector	lovRegistro = (Vector) lovDatos.get(i);
							int 	liNumCampo 	= Integer.parseInt((String)lovRegistro.get(0));			
							if (liNumCampo== 1) { 
									contenidoArchivo.append("'"+campo1.replaceAll(",", "")+ ",");	
							} if (liNumCampo== 2) { 
								contenidoArchivo.append("'"+campo2.replaceAll(",", "")+ ",");	
							} if (liNumCampo== 3) { 
								contenidoArchivo.append(campo3.replaceAll(",", "")+ ",");	
							} if (liNumCampo== 4) { 
								contenidoArchivo.append(campo4.replaceAll(",", "")+ ",");	
							} if (liNumCampo== 5) { 
								contenidoArchivo.append(campo5.replaceAll(",", "")+ ",");	
							} 		
						} // termina for 
					} //if 
					
					contenidoArchivo.append(ventanilla.replaceAll(",", "")+ ",");	
					contenidoArchivo.append(supadmini.replaceAll(",", "")+ ",");	
					contenidoArchivo.append(telefono.replaceAll(",", "")+ ",");	
					contenidoArchivo.append(obContrato.replaceAll(",", "")+ ",");	
					contenidoArchivo.append(comentarios.replaceAll(",", "")+ ",");	
					contenidoArchivo.append(fechaAceptacion.replaceAll(",", "")+ ",");	
					contenidoArchivo.append(estatus.replaceAll(",", "")+ ",");	
					contenidoArchivo.append(estatusProrroga.replaceAll(",", "")+ "\n");
				}//if(tipo.equals("CSV")) 
				
			}//while (rs.next()) {
			rs.close();		
			
			if(tipo.equals("PDF")){
				pdfDoc.addTable();
				pdfDoc.endDocument();
			}
			if(tipo.equals("CSV")){
				creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
				nombreArchivo = creaArchivo.getNombre();
			}
            
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo", e);
		} finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
	
	
	return nombreArchivo;
	
}	
	
	
/*****************************************************
	 GETTERS*******************************************************/
 
	/**
	  Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	
	
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}



	public String getDirectorio() {
		return directorio;
	}

	public void setDirectorio(String directorio) {
		this.directorio = directorio;
	}

// get y set para la busqueda

public String getNoIF() {
		return noIF;
	}

	public void setNoIF(String noIF) {
		this.noIF = noIF;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getPyme() {
		return pyme;
	}

	public void setPyme(String pyme) {
		this.pyme = pyme;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getNoContrato() {
		return noContrato;
	}

	public void setNoContrato(String noContrato) {
		this.noContrato = noContrato;
	}

	public String getFvigenciaIni() {
		return fvigenciaIni;
	}

	public void setFvigenciaIni(String fvigenciaIni) {
		this.fvigenciaIni = fvigenciaIni;
	}

	public String getFvigenciaFin() {
		return fvigenciaFin;
	}

	public void setFvigenciaFin(String fvigenciaFin) {
		this.fvigenciaFin = fvigenciaFin;
	}

	public String getfsolicitudIni() {
		return fsolicitudIni;
	}

	public void setfsolicitudIni(String fsolicitudIni) {
		this.fsolicitudIni = fsolicitudIni;
	}

	public String getfsolicitudFin() {
		return fsolicitudFin;
	}

	public void setfsolicitudFin(String fsolicitudFin) {
		this.fsolicitudFin = fsolicitudFin;
	}

	public String getCampo1() {
		return campo1;
	}

	public void setCampo1(String campo1) {
		this.campo1 = campo1;
	}

	public String getCampo2() {
		return campo2;
	}

	public void setCampo2(String campo2) {
		this.campo2 = campo2;
	}

	public String getEpo() {
		return epo;
	}

	public void setEpo(String epo) {
		this.epo = epo;
	}
	

	public void setPlazoContrato(String plazoContrato) {this.plazoContrato = plazoContrato;}
	public void setClaveTipoPlazo(String claveTipoPlazo) {this.claveTipoPlazo = claveTipoPlazo;}
	public void setLoginUsuarioEpo(String loginUsuarioEpo) {this.loginUsuarioEpo = loginUsuarioEpo;}

	public String getPlazoContrato() {return this.plazoContrato;}
	public String getClaveTipoPlazo() {return this.claveTipoPlazo;}

	public String getEstatusProrroga() {
		return estatusProrroga;
	}

	public void setEstatusProrroga(String estatusProrroga) {
		this.estatusProrroga = estatusProrroga;
	}


	
}