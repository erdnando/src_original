package com.netro.cesion;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;


public class ConsExtincionContratoEPO implements IQueryGeneratorReg, IQueryGeneratorRegExtJS {

	
	private StringBuffer strSQL;
	private List conditions;
	private String paginaOffset;
	private String paginaNo;
	private String catPyme;
	private String noContrato;
	private String catMoneda;
	private String catContratacion;
	private String catIntermediario;
	
	private static final Log log = ServiceLocator.getInstance().getLog(ConsExtincionContratoEPO.class);//Variable para enviar mensajes al log.
	private String claveEpo;
	private String fechaInicio;
	private String fechaFin;

	//Constructor de la clase.
	public ConsExtincionContratoEPO() { }

	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E) ::..");
		strSQL = new StringBuffer();
		log.info("getAggregateCalculationQuery(S) ::..");
		return strSQL.toString();
	}
	
			
	 /**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery() {
		log.info("getDocumentQuery(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();
			
		strSQL.append(" SELECT sol.ic_solicitud AS clave_solicitud,");
		strSQL.append(" sol.ic_pyme AS clave_pyme,");
		strSQL.append(" sol.ic_epo AS clave_epo,");
		strSQL.append(" sol.ic_if AS clave_if,");
		strSQL.append(" cif.cg_razon_social AS nombre_if,");
		strSQL.append(" pym.cg_razon_social||';'||sol.CG_EMPRESAS_REPRESENTADAS AS nombre_pyme,");//OFODEA-024-2014 MOD()
		strSQL.append(" pym.cg_rfc AS rfc,");
		strSQL.append(" cpe.cg_pyme_epo_interno AS numero_proveedor,");
		strSQL.append(" TO_CHAR(sol.df_fecha_solicitud_ini, 'dd/mm/yyyy') AS fecha_sol_pyme,");
		strSQL.append(" sol.cg_no_contrato AS numero_contrato,");
		strSQL.append(" tcn.cg_nombre AS tipo_contratacion,");
		strSQL.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");//FODEA 016 - 2011 ACF
		strSQL.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");//FODEA 016 - 2011 ACF
		strSQL.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS plazo_contrato,");//FODEA 016 - 2011 ACF
		strSQL.append(" cep.cg_area AS clasificacion_epo,");
		strSQL.append(" sol.cg_campo1 AS campo_adicional_1,");
		strSQL.append(" sol.cg_campo2 AS campo_adicional_2,");
		strSQL.append(" sol.cg_campo3 AS campo_adicional_3,");
		strSQL.append(" sol.cg_campo4 AS campo_adicional_4,");
		strSQL.append(" sol.cg_campo5 AS campo_adicional_5,");
		strSQL.append(" sol.cg_obj_contrato AS objeto_contrato,");
		strSQL.append(" sol.cg_comentarios AS comentarios,");
		strSQL.append(" sol.fn_monto_credito AS monto_credito,");
		strSQL.append(" sol.cg_referencia AS numero_referencia,");
		strSQL.append(" TO_CHAR(sol.df_vencimiento_credito, 'dd/mm/yyyy') AS fecha_vencimiento,");
		//strSQL.append(" sol.cg_banco_deposito AS banco_deposito,");
		// strSQL.append(" sol.cg_cuenta AS numero_cuenta,");
		//strSQL.append(" sol.cg_cuenta_clabe AS clabe,");
		strSQL.append(" ex.CG_BANCO_DEPOSITO_EXT AS banco_deposito,");
		strSQL.append(" ex.CG_CUENTA_EXT AS numero_cuenta,");
		strSQL.append(" ex.CG_CUENTA_CLABE_EXT AS clabe,");
		
		strSQL.append(" sol.ic_estatus_solic AS clave_estatus,");
		strSQL.append(" sts.cg_descripcion AS estatus_solicitud, ");
		strSQL.append(" sol.cg_causas_rechazo_notif AS causas_rechazo_notif,");
		//strSQL.append(" sol.CG_OBSERV_RECHAZO AS causasRechazo, ");
			
		strSQL.append(" ex.CG_CAUSA_RECHAZO_EXT AS causasRechazo, ");
			
		strSQL.append("  TO_CHAR (ex.DF_EXTINCION_CONTRATO, 'DD/MM/YYYY') AS fextionContrato");
		strSQL.append(" , sol.CG_CORREOEXT as  correo ");
		strSQL.append(" , '' AS monto_moneda,'' AS  representa_legal ");
		
		strSQL.append(" , sol.IC_GRUPO_CESION  AS GRUPO_CESION  ");	   
		
		strSQL.append(" FROM cder_solicitud sol");
		strSQL.append(", comcat_epo epo");
		strSQL.append(", comcat_if cif");
		strSQL.append(", comcat_pyme pym");
		strSQL.append(", comrel_pyme_epo cpe");
		//strSQL.append(", comcat_moneda mon");
		strSQL.append(", cdercat_tipo_contratacion tcn");
		strSQL.append(", cdercat_clasificacion_epo cep");
		strSQL.append(", cdercat_estatus_solic sts");
		strSQL.append(", cdercat_tipo_plazo stp");
		strSQL.append(", CDER_EXTINCION_CONTRATO ex");		
			
		strSQL.append(" WHERE sol.ic_epo = epo.ic_epo");
		strSQL.append(" AND sol.ic_if = cif.ic_if");
		strSQL.append(" AND sol.ic_pyme = pym.ic_pyme");
		strSQL.append(" AND sol.ic_epo = cpe.ic_epo(+)");
		strSQL.append(" AND sol.ic_pyme = cpe.ic_pyme(+)");
		//strSQL.append(" AND sol.ic_moneda = mon.ic_moneda");
		strSQL.append(" AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion");
		strSQL.append(" AND sol.ic_epo = cep.ic_epo");
		strSQL.append(" AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo");
		strSQL.append(" AND sol.ic_estatus_solic = sts.ic_estatus_solic");
		strSQL.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
		strSQL.append(" AND sol.ic_solicitud = ex.ic_solicitud ");			
		strSQL.append(" and sol.ic_estatus_solic IN ( 20, 21, 23 ) ");
		//strSQL.append(" and sol.ic_estatus_solic =20 ");
			
		if(!claveEpo.equals("")){
			strSQL.append(" AND sol.ic_epo = ?");
			conditions.add(new Integer(claveEpo));
		}
		if(!catIntermediario.equals("")){
			strSQL.append(" AND sol.ic_if = ?");
			conditions.add(new Integer(catIntermediario));
		}
		if(!catPyme.equals("")){
			strSQL.append(" AND sol.ic_pyme = ?");
			conditions.add(new Long(catPyme));
		}
	
		if(!noContrato.equals("")){
			strSQL.append(" AND sol.cg_no_contrato = ?");
			conditions.add(noContrato);
		}
		
		if(!catContratacion.equals("")){
			strSQL.append(" AND sol.ic_tipo_contratacion = ?");
			conditions.add(catContratacion);
		}
		
		if(!fechaInicio.equals("") && !fechaFin.equals("")){
			strSQL.append(" AND sol.DF_FIRMA_CONTRATO >= TO_DATE(?, 'DD/MM/YYYY') AND sol.DF_FIRMA_CONTRATO < TO_DATE(?, 'DD/MM/YYYY')+1 ");
			conditions.add(fechaInicio);
			conditions.add(fechaFin);
		}
		
		if(!catMoneda.equals("")){
			strSQL.append(" AND sol.cs_multimoneda = ?");
      strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
      strSQL.append(" FROM cder_monto_x_solic mxs");
      strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
      strSQL.append(" AND mxs.ic_moneda = ?)");
	   	conditions.add("N");
			conditions.add(catMoneda);	
		}
		
		strSQL.append(" ORDER BY sol.df_alta_solicitud DESC");
			
		log.info("strSQL ::.."+strSQL.toString() );
		log.info("conditions ::.."+conditions.toString() );
		log.info("getDocumentQuery(S) ::..");
		return strSQL.toString();
	}


	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		log.info("getDocumentSummaryQueryForIds(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();
	
		
		strSQL.append(" SELECT sol.ic_solicitud AS clave_solicitud,");
		strSQL.append(" sol.ic_pyme AS clave_pyme,");
		strSQL.append(" sol.ic_epo AS clave_epo,");
		strSQL.append(" sol.ic_if AS clave_if,");
		strSQL.append(" cif.cg_razon_social AS nombre_if,");
		//strSQL.append(" pym.cg_razon_social||';'||sol.CG_EMPRESAS_REPRESENTADAS AS nombre_pyme,");//OFODEA-024-2014 MOD()
		strSQL.append(" pym.cg_razon_social AS nombre_pyme,");//OFODEA-024-2014 MOD()
		strSQL.append(" sol.CG_EMPRESAS_REPRESENTADAS AS empresas,");//OFODEA-024-2014 MOD()
		strSQL.append(" pym.cg_rfc AS rfc,");
		strSQL.append(" cpe.cg_pyme_epo_interno AS numero_proveedor,");
		strSQL.append(" TO_CHAR(sol.df_fecha_solicitud_ini, 'dd/mm/yyyy') AS fecha_sol_pyme,");
		strSQL.append(" sol.cg_no_contrato AS numero_contrato,");
		strSQL.append(" tcn.cg_nombre AS tipo_contratacion,");
		strSQL.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");//FODEA 016 - 2011 ACF
		strSQL.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");//FODEA 016 - 2011 ACF
		strSQL.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS plazo_contrato,");//FODEA 016 - 2011 ACF
		strSQL.append(" cep.cg_area AS clasificacion_epo,");
		strSQL.append(" sol.cg_campo1 AS campo_adicional_1,");
		strSQL.append(" sol.cg_campo2 AS campo_adicional_2,");
		strSQL.append(" sol.cg_campo3 AS campo_adicional_3,");
		strSQL.append(" sol.cg_campo4 AS campo_adicional_4,");
		strSQL.append(" sol.cg_campo5 AS campo_adicional_5,");
		strSQL.append(" sol.cg_obj_contrato AS objeto_contrato,");
		strSQL.append(" sol.cg_comentarios AS comentarios,");
		strSQL.append(" sol.fn_monto_credito AS monto_credito,");
		strSQL.append(" sol.cg_referencia AS numero_referencia,");
		strSQL.append(" TO_CHAR(sol.df_vencimiento_credito, 'dd/mm/yyyy') AS fecha_vencimiento,");
		//strSQL.append(" sol.cg_banco_deposito AS banco_deposito,");
		//strSQL.append(" sol.cg_cuenta AS numero_cuenta,");
		//strSQL.append(" sol.cg_cuenta_clabe AS clabe,");
		strSQL.append(" ex.CG_BANCO_DEPOSITO_EXT AS banco_deposito,");
		strSQL.append(" ex.CG_CUENTA_EXT AS numero_cuenta,");
		strSQL.append(" ex.CG_CUENTA_CLABE_EXT AS clabe,");
		strSQL.append(" sol.ic_estatus_solic AS clave_estatus,");
		strSQL.append(" sts.cg_descripcion AS estatus_solicitud, ");
		strSQL.append(" sol.cg_causas_rechazo_notif AS causas_rechazo_notif,");
		//strSQL.append(" sol.CG_OBSERV_RECHAZO AS causasRechazo, ");
		strSQL.append(" ex.CG_CAUSA_RECHAZO_EXT AS causasRechazo, ");
		strSQL.append("  TO_CHAR (ex.DF_EXTINCION_CONTRATO, 'DD/MM/YYYY') AS fextionContrato");
		strSQL.append(" , sol.CG_CORREOEXT as  correo ");	
		strSQL.append(" , '' AS monto_moneda,'' AS  representa_legal ");
		strSQL.append(" , sol.IC_GRUPO_CESION  AS GRUPO_CESION  ");	  
		
		strSQL.append(" FROM cder_solicitud sol");
		strSQL.append(", comcat_epo epo");
		strSQL.append(", comcat_if cif");
		strSQL.append(", comcat_pyme pym");
		strSQL.append(", comrel_pyme_epo cpe");
		//strSQL.append(", comcat_moneda mon");
		strSQL.append(", cdercat_tipo_contratacion tcn");
		strSQL.append(", cdercat_clasificacion_epo cep");
		strSQL.append(", cdercat_estatus_solic sts");
		strSQL.append(", cdercat_tipo_plazo stp");
		strSQL.append(", CDER_EXTINCION_CONTRATO ex");	
				
		strSQL.append(" WHERE sol.ic_epo = epo.ic_epo");
		strSQL.append(" AND sol.ic_if = cif.ic_if");
		strSQL.append(" AND sol.ic_pyme = pym.ic_pyme");
		strSQL.append(" AND sol.ic_epo = cpe.ic_epo(+)");
		strSQL.append(" AND sol.ic_pyme = cpe.ic_pyme(+)");
		//strSQL.append(" AND sol.ic_moneda = mon.ic_moneda");
		strSQL.append(" AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion");
		strSQL.append(" AND sol.ic_epo = cep.ic_epo");
		strSQL.append(" AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo");
		strSQL.append(" AND sol.ic_estatus_solic = sts.ic_estatus_solic");
		strSQL.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
		strSQL.append(" AND sol.ic_solicitud = ex.ic_solicitud ");			
		strSQL.append(" and sol.ic_estatus_solic IN ( 20, 21, 23 ) ");
		//strSQL.append(" and sol.ic_estatus_solic =20 ");
			
		strSQL.append(" AND (");
									
		for (int i = 0; i < pageIds.size(); i++) {
			List lItem = (ArrayList)pageIds.get(i);
			if(i > 0){
				strSQL.append("  OR  ");
			}
			strSQL.append("sol.ic_solicitud = ?");
			conditions.add(new Long(lItem.get(0).toString()));
		}
		strSQL.append(" ) ");
			
		log.debug("strSQL ::.."+strSQL.toString());
		log.debug("Conditions ::.."+conditions);
		
		
		log.info("getDocumentSummaryQueryForIds(S) ::..");
		return strSQL.toString();
	}


	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();

		strSQL.append(" SELECT sol.ic_solicitud AS clave_solicitud,");
		strSQL.append(" sol.ic_pyme AS clave_pyme,");
		strSQL.append(" sol.ic_epo AS clave_epo,");
		strSQL.append(" sol.ic_if AS clave_if,");
		strSQL.append(" cif.cg_razon_social AS nombre_if,");
		strSQL.append(" pym.cg_razon_social||';'||sol.CG_EMPRESAS_REPRESENTADAS AS nombre_pyme,");//OFODEA-024-2014 MOD()
		strSQL.append(" pym.cg_rfc AS rfc,");
		strSQL.append(" cpe.cg_pyme_epo_interno AS numero_proveedor,");
		strSQL.append(" TO_CHAR(sol.df_fecha_solicitud_ini, 'dd/mm/yyyy') AS fecha_sol_pyme,");
		strSQL.append(" sol.cg_no_contrato AS numero_contrato,");
		strSQL.append(" tcn.cg_nombre AS tipo_contratacion,");
		strSQL.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");//FODEA 016 - 2011 ACF
		strSQL.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");//FODEA 016 - 2011 ACF
		strSQL.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS plazo_contrato,");//FODEA 016 - 2011 ACF
		strSQL.append(" cep.cg_area AS clasificacion_epo,");
		strSQL.append(" sol.cg_campo1 AS campo_adicional_1,");
		strSQL.append(" sol.cg_campo2 AS campo_adicional_2,");
		strSQL.append(" sol.cg_campo3 AS campo_adicional_3,");
		strSQL.append(" sol.cg_campo4 AS campo_adicional_4,");
		strSQL.append(" sol.cg_campo5 AS campo_adicional_5,");
		strSQL.append(" sol.cg_obj_contrato AS objeto_contrato,");
		strSQL.append(" sol.cg_comentarios AS comentarios,");
		strSQL.append(" sol.fn_monto_credito AS monto_credito,");
		strSQL.append(" sol.cg_referencia AS numero_referencia,");
		strSQL.append(" TO_CHAR(sol.df_vencimiento_credito, 'dd/mm/yyyy') AS fecha_vencimiento,");
		//strSQL.append(" sol.cg_banco_deposito AS banco_deposito,");
		// strSQL.append(" sol.cg_cuenta AS numero_cuenta,");
		//strSQL.append(" sol.cg_cuenta_clabe AS clabe,");
		strSQL.append(" ex.CG_BANCO_DEPOSITO_EXT AS banco_deposito,");
		strSQL.append(" ex.CG_CUENTA_EXT AS numero_cuenta,");
		strSQL.append(" ex.CG_CUENTA_CLABE_EXT AS clabe,");
		
		strSQL.append(" sol.ic_estatus_solic AS clave_estatus,");
		strSQL.append(" sts.cg_descripcion AS estatus_solicitud, ");
		strSQL.append(" sol.cg_causas_rechazo_notif AS causas_rechazo_notif,");
		//strSQL.append(" sol.CG_OBSERV_RECHAZO AS causasRechazo, ");
			
		strSQL.append(" ex.CG_CAUSA_RECHAZO_EXT AS causasRechazo, ");
			
		strSQL.append("  TO_CHAR (ex.DF_EXTINCION_CONTRATO, 'DD/MM/YYYY') AS fextionContrato");
		strSQL.append(" , sol.CG_CORREOEXT as  correo ");
		strSQL.append(" , '' AS monto_moneda,'' AS  representa_legal ");
		strSQL.append(" , sol.IC_GRUPO_CESION  AS GRUPO_CESION  ");	   
		
		strSQL.append(" FROM cder_solicitud sol");
		strSQL.append(", comcat_epo epo");
		strSQL.append(", comcat_if cif");
		strSQL.append(", comcat_pyme pym");
		strSQL.append(", comrel_pyme_epo cpe");
		//strSQL.append(", comcat_moneda mon");
		strSQL.append(", cdercat_tipo_contratacion tcn");
		strSQL.append(", cdercat_clasificacion_epo cep");
		strSQL.append(", cdercat_estatus_solic sts");
		strSQL.append(", cdercat_tipo_plazo stp");
		strSQL.append(", CDER_EXTINCION_CONTRATO ex");		
			
		strSQL.append(" WHERE sol.ic_epo = epo.ic_epo");
		strSQL.append(" AND sol.ic_if = cif.ic_if");
		strSQL.append(" AND sol.ic_pyme = pym.ic_pyme");
		strSQL.append(" AND sol.ic_epo = cpe.ic_epo(+)");
		strSQL.append(" AND sol.ic_pyme = cpe.ic_pyme(+)");
		//strSQL.append(" AND sol.ic_moneda = mon.ic_moneda");
		strSQL.append(" AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion");
		strSQL.append(" AND sol.ic_epo = cep.ic_epo");
		strSQL.append(" AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo");
		strSQL.append(" AND sol.ic_estatus_solic = sts.ic_estatus_solic");
		strSQL.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
		strSQL.append(" AND sol.ic_solicitud = ex.ic_solicitud ");
		strSQL.append(" and sol.ic_estatus_solic IN ( 20, 21, 23 ) ");
		//strSQL.append(" and sol.ic_estatus_solic =20 ");

		if(!claveEpo.equals("")){
			strSQL.append(" AND sol.ic_epo = ?");
			conditions.add(new Integer(claveEpo));
		}
		if(!catIntermediario.equals("")){
			strSQL.append(" AND sol.ic_if = ?");
			conditions.add(new Integer(catIntermediario));
		}
		if(!catPyme.equals("")){
			strSQL.append(" AND sol.ic_pyme = ?");
			conditions.add(new Long(catPyme));
		}
	
		if(!noContrato.equals("")){
			strSQL.append(" AND sol.cg_no_contrato = ?");
			conditions.add(noContrato);
		}
		
		if(!catContratacion.equals("")){
			strSQL.append(" AND sol.ic_tipo_contratacion = ?");
			conditions.add(catContratacion);
		}
		
		if(!fechaInicio.equals("") && !fechaFin.equals("")){
			strSQL.append(" AND sol.DF_FIRMA_CONTRATO >= TO_DATE(?, 'DD/MM/YYYY') AND sol.DF_FIRMA_CONTRATO < TO_DATE(?, 'DD/MM/YYYY')+1 ");
			conditions.add(fechaInicio);
			conditions.add(fechaFin);
		}

		if(!catMoneda.equals("")){
			strSQL.append(" AND sol.cs_multimoneda = ?");
      strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
      strSQL.append(" FROM cder_monto_x_solic mxs");
      strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
      strSQL.append(" AND mxs.ic_moneda = ?)");
	   	conditions.add("N");
			conditions.add(catMoneda);	
		}
		
		strSQL.append(" ORDER BY sol.df_alta_solicitud DESC");

		log.info("getDocumentQueryFile(S) ::.. - -  "+strSQL.toString());
		return strSQL.toString();
	}//getDocumentQueryFile
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		String nombreArchivo = "";
		CreaArchivo creaArchivo = new CreaArchivo();
		ComunesPDF pdfDoc = new ComunesPDF();
		try {

			if(tipo.equals("PDF")) {

				CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);

				int hayRegistros = 0;
				String clasificacionEpo = cesionBean.clasificacionEpo(claveEpo);
				if(clasificacionEpo ==null){   clasificacionEpo=""; }
				java.util.HashMap campos_adicionales = cesionBean.getCamposAdicionalesParametrizados(claveEpo, "9");
				int indice	= Integer.parseInt((String)campos_adicionales.get("indice"));

				int columnas = 17;
				if(clasificacionEpo !=null ) columnas +=1;
				if(indice >0) columnas +=indice;

				HttpSession session = request.getSession();
				String tipoUsuario = (String)session.getAttribute("strTipoUsuario");
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				pdfDoc.addText("  Extinci�n de Contrato ","formas",ComunesPDF.CENTER);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

				pdfDoc.setLTable(columnas, 100);
				pdfDoc.setLCell("Intermediario Financiero (Cesionario)","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("PYME (Cedente)","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("RFC","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Representante Legal","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("No. Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto / Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de Contrataci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Inicio Contrato ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Final Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo del Contrato ","celda01",ComunesPDF.CENTER);
				if(clasificacionEpo !=null ){
					pdfDoc.setLCell(clasificacionEpo,"celda01",ComunesPDF.CENTER);
				}
				if(indice > 0){
					for(int y = 0; y < indice; y++){
						String nombreCampo = campos_adicionales.get("nombre_campo_"+y).toString();
						pdfDoc.setLCell(nombreCampo,"celda01",ComunesPDF.CENTER);
					}
				}
				pdfDoc.setLCell("Objeto del Contrato ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Extinci�n del Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Cuenta","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Cuenta CLABE","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Banco de Dep�sito","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Causa de Rechazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Estado","celda01",ComunesPDF.CENTER);

				while (reg.next()) {
					StringBuffer montosPorMoneda = cesionBean.getMontoMoneda(reg.getString("clave_solicitud"));
					//Se obtiene el representante Legal de la Pyme%>
					String noProveedor = reg.getString("clave_pyme");
					String nombrePyme =reg.getString("nombre_pyme");//FODEA-024-2014 MOD()
					String empresas =((reg.getString("empresas")==null)?"":reg.getString("empresas"));//FODEA-024-2014 MOD()
					String pymeEmpresas=nombrePyme+((!"".equals(empresas))?";"+empresas:"");
					
					StringBuffer nombreContacto = new StringBuffer();
					UtilUsr utilUsr = new UtilUsr();
					boolean usuarioEncontrado = false;
					boolean perfilIndicado = false;
					List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(noProveedor, "P");
					for (int y = 0; y < usuariosPorPerfil.size(); y++) {
						String loginUsuarioEpo = (String)usuariosPorPerfil.get(y);
						Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
						perfilIndicado = true;
						if(y>0){
							nombreContacto.append(" / ");
						}
						nombreContacto.append(usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno()+"  ");	
						nombreContacto.append("\n");
					}
					pdfDoc.setLCell(reg.getString("nombre_if"),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(pymeEmpresas.replaceAll(";","\n\n"),"formas",ComunesPDF.LEFT);//FODEA-024-2014 MOD()
					pdfDoc.setLCell(reg.getString("rfc"),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(nombreContacto.toString().trim(),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(reg.getString("numero_contrato"),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(montosPorMoneda.toString().replace(',',' ').replaceAll("<br/>", "\n"),"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(reg.getString("tipo_contratacion"),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(reg.getString("fecha_inicio_contrato"),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(reg.getString("fecha_fin_contrato"),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(reg.getString("plazo_contrato"),"formas",ComunesPDF.CENTER);
					if(!clasificacionEpo.equals("")){ 
						pdfDoc.setLCell(reg.getString("clasificacion_epo"),"formas",ComunesPDF.CENTER);
					}
					if(indice > 0){
						for(int j = 1; j < indice+1; j++){
							pdfDoc.setLCell(reg.getString("campo_adicional_"+j),"formas",ComunesPDF.CENTER);
						}
					}
					pdfDoc.setLCell(reg.getString("objeto_contrato"),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(reg.getString("fextionContrato"),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(reg.getString("numero_cuenta"),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(reg.getString("clabe"),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(reg.getString("banco_deposito"),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(reg.getString("causasrechazo"),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(reg.getString("estatus_solicitud"),"formas",ComunesPDF.CENTER);
					hayRegistros ++;
				}
				if (hayRegistros == 0){
					pdfDoc.addText(" ","formasMini",ComunesPDF.LEFT);
					pdfDoc.addText("No se Encontro Ning�n Registro","formas",ComunesPDF.LEFT);
				}else	{
					pdfDoc.addLTable();
					pdfDoc.endDocument();
				}
			}

		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		} finally {
		}
		return nombreArchivo;
	}
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String nombreArchivo = "";
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();

		try {
			CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
			int hayRegistros = 0;
			String clasificacionEpo = cesionBean.clasificacionEpo(claveEpo);
			if(clasificacionEpo ==null){   clasificacionEpo=""; }
			java.util.HashMap campos_adicionales = cesionBean.getCamposAdicionalesParametrizados(claveEpo, "9");
			int indice	= Integer.parseInt((String)campos_adicionales.get("indice"));
			
			contenidoArchivo.append("Intermediario Financiero (Cesionario), PYME (Cedente), RFC, Representante Legal, No. de Contrato, Monto / Moneda, Tipo de Contrataci�n, Fecha Inicio Contrato, Fecha Final Contrato, Plazo del Contrato, ");
			
			if(!clasificacionEpo.equals("")){
				contenidoArchivo.append(clasificacionEpo.replace(',',' ') +",");
			}
			if(indice > 0){
				for(int y = 0; y < indice; y++){
					String nombreCampo = campos_adicionales.get("nombre_campo_"+y).toString();
					contenidoArchivo.append(nombreCampo.replace(',',' ') +",");
				}
			}
			contenidoArchivo.append(" Objeto del Contrato, Fecha de Extinci�n del contrato,  Cuenta,  Cuenta CLABE, Banco de Deposito, Causar de Rechazo, Estatus ");
			contenidoArchivo.append("\n");

			while (rs.next()) {
				StringBuffer montosPorMoneda = cesionBean.getMontoMoneda(rs.getString("clave_solicitud"));
				//Se obtiene el representante Legal de la Pyme%>
				String noProveedor = rs.getString("clave_pyme");
				StringBuffer nombreContacto = new StringBuffer();
				//FODEA-024-2014 MOD()
				String nombrePyme=(rs.getString("nombre_pyme"))==null?"":rs.getString("nombre_pyme");
				String []nombrePymeAux=nombrePyme.split(";");
					if(nombrePymeAux.length == 1){
						nombrePyme = nombrePyme.replaceAll(";","");
					}else{
						nombrePyme = nombrePyme.substring(0,nombrePyme.length()-1);
					}
					//
				UtilUsr utilUsr = new UtilUsr();
				boolean usuarioEncontrado = false;
				boolean perfilIndicado = false;
				List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(noProveedor, "P");
				for (int y = 0; y < usuariosPorPerfil.size(); y++) {
					String loginUsuarioEpo = (String)usuariosPorPerfil.get(y);
					Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
					perfilIndicado = true;
					if(y>0){
						nombreContacto.append(" / ");
					}
					nombreContacto.append(usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno()+"  ");	
					//nombreContacto.append("\n");
				}
				contenidoArchivo.append(	((rs.getString("nombre_if"))==null?"":rs.getString("nombre_if")).replace(',',' ') +",");
				contenidoArchivo.append(	(nombrePyme).replace(',',' ') +",");
				contenidoArchivo.append(	((rs.getString("rfc"))==null?"":rs.getString("rfc")).replace(',',' ') +",");
				contenidoArchivo.append(nombreContacto.toString().trim()+",");
				contenidoArchivo.append(	((rs.getString("numero_contrato"))==null?"":rs.getString("numero_contrato")).replace(',',' ') +",");
				contenidoArchivo.append(montosPorMoneda.toString().replace(',',' ').replaceAll("<br/>", "|")+",");
				contenidoArchivo.append(	((rs.getString("tipo_contratacion"))==null?"":rs.getString("tipo_contratacion")).replace(',',' ') +",");
				contenidoArchivo.append(	((rs.getString("fecha_inicio_contrato"))==null?"":rs.getString("fecha_inicio_contrato")).replace(',',' ') +",");
				contenidoArchivo.append(	((rs.getString("fecha_fin_contrato"))==null?"":rs.getString("fecha_fin_contrato")).replace(',',' ') +",");
				contenidoArchivo.append(	((rs.getString("plazo_contrato"))==null?"":rs.getString("plazo_contrato")).replace(',',' ') +",");
				if(!clasificacionEpo.equals("")){
					contenidoArchivo.append(	((rs.getString("clasificacion_epo"))==null?"":rs.getString("clasificacion_epo")).replace(',',' ') +",");
				}
				if(indice > 0){
					for(int j = 1; j < indice+1; j++){
						String campoAdd= "campo_adicional_"+j;
						String campo = ((rs.getString(campoAdd))==null?"":rs.getString(campoAdd)).replace(',',' ') ;
						if(j==1||j==2){
							campo="'"+campo;
						}
						contenidoArchivo.append( campo +",");
					}
				}
				contenidoArchivo.append(	((rs.getString("objeto_contrato"))==null?"":rs.getString("objeto_contrato")).replace(',',' ') +",");
				contenidoArchivo.append(	((rs.getString("fextionContrato"))==null?"":rs.getString("fextionContrato")).replace(',',' ') +",");
				contenidoArchivo.append("'"+	((rs.getString("numero_cuenta"))==null?"":rs.getString("numero_cuenta")).replace(',',' ') +",");
				contenidoArchivo.append("'"+	((rs.getString("clabe"))==null?"":rs.getString("clabe")).replace(',',' ') +",");
				contenidoArchivo.append(	((rs.getString("banco_deposito"))==null?"":rs.getString("banco_deposito")).replace(',',' ') +",");
				contenidoArchivo.append(	((rs.getString("causasrechazo"))==null?"":rs.getString("causasrechazo")).replace(',',' ') +",");
				contenidoArchivo.append(	((rs.getString("estatus_solicitud"))==null?"":rs.getString("estatus_solicitud")).replace(',',' ') +",");
		 		contenidoArchivo.append("\r\n");
				hayRegistros ++;
			}//while (rs.next()) {
			if (rs!=null){rs.close();}
			if(hayRegistros==0){
				contenidoArchivo.append( " No se encontr� ning�n registro para los criter�os seleccionados. " );						
				contenidoArchivo.append( "\r\n");
			}
			creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
			nombreArchivo = creaArchivo.getNombre();
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		} finally {
		}
		return nombreArchivo;
	}

	//Getters
	public List getConditions() {	  return conditions;  }  
	public String getPaginaNo() {	return paginaNo;}
	public String getPaginaOffset() {return paginaOffset;}	
	public String getCatPyme() { 		return catPyme;  }
	public String getNoContrato() { 		return noContrato; 	}
	public String getCatMoneda() { 		return catMoneda; 	}
	public String getCatContratacion() { 		return catContratacion; 	}
	public String getCatIntermediario() { 		return catIntermediario; 	}
	public String getClaveEpo() { 	return claveEpo;  }
	
	
	
	//Setters
	public void setPaginaNo(String newPaginaNo) {this.paginaNo = newPaginaNo;  }
	public void setPaginaOffset(String paginaOffset) {this.paginaOffset=paginaOffset;}
	public void setCatContratacion(String catContratacion) { 		this.catContratacion = catContratacion; 	}
	public void setCatIntermediario(String catIntermediario) { 		this.catIntermediario = catIntermediario; 	}
	public void setCatMoneda(String catMoneda) { 		this.catMoneda = catMoneda; 	}
	public void setNoContrato(String noContrato) { 		this.noContrato = noContrato; 	}
	public void setCatPyme(String catPyme) { 		this.catPyme = catPyme; 	}
	public void setClaveEpo(String claveEpo) { 		this.claveEpo = claveEpo; 	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	
	
	
}