package com.netro.cesion;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AppException;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ContratosCedidosEPO implements IQueryGeneratorRegExtJS {

	private StringBuffer strSQL;
	private List conditions;
	private String claveIf;
	private String claveEpo;
	private String clavePyme;
	private String noContrato;
	
	private static final Log log = ServiceLocator.getInstance().getLog(ContratosCedidosEPO.class);//Variable para enviar mensajes al log.

	//Constructor de la clase.
	public ContratosCedidosEPO() { }

	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E) ::..");
		strSQL = new StringBuffer();
		log.info("getAggregateCalculationQuery(S) ::..");
		return strSQL.toString();
	}

	 /**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery() {
		log.info("getDocumentQuery(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();

		strSQL.append(
			" SELECT cc.cg_no_contrato as contrato "+
			" FROM cder_contrato_cedido cc "+
			", comcat_epo epo"+
			", comcat_if cif"+
			", comcat_pyme pym"+
			", comrel_pyme_epo cpe"+
			" WHERE cc.ic_epo = epo.ic_epo"+
			" AND cc.ic_if = cif.ic_if"+
			" AND cc.ic_pyme = pym.ic_pyme"+
			" AND cc.ic_epo = cpe.ic_epo(+)"+
			" AND cc.ic_pyme = cpe.ic_pyme(+)"+
			""
		);

		if(this.claveEpo != null	&&	!this.claveEpo.equals("")){
			strSQL.append(" AND cc.ic_epo = ?");
			conditions.add(new Integer(this.claveEpo));
		}
		if(this.claveIf != null	&&	!this.claveIf.equals("")){
			strSQL.append(" AND cc.ic_if = ?");
			conditions.add(new Integer(this.claveIf));
		}
		if(this.clavePyme != null	&&	!this.clavePyme.equals("")){
			strSQL.append(" AND cc.ic_pyme = ?");
			conditions.add(new Long(this.clavePyme));
		}
		if(this.noContrato != null	&&	!this.noContrato.equals("")){
			strSQL.append(" AND cc.cg_no_contrato = ?");
			conditions.add(this.noContrato);
		}
		strSQL.append(" ORDER BY cc.cg_no_contrato ");
			
		log.info("strSQL ::.."+strSQL.toString() );
		log.info("Conditions ::.."+conditions );
		log.info("getDocumentQuery(S) ::..");
		return strSQL.toString();
	}


	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		log.info("getDocumentSummaryQueryForIds(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();

		strSQL.append(
			" SELECT cc.cg_no_contrato AS contrato "+
			" ,cc.ic_epo "+
			" ,cc.ic_if "+
			" ,cc.ic_pyme "+
			" ,cif.cg_razon_social AS nombre_cesionario "+
			" ,pym.cg_razon_social AS nombre_pyme "+
			" ,'true' AS eliminar_contrato "+
			" ,TO_CHAR(cc.df_fecha_captura, 'dd/mm/yyyy') as fecha_captura"+
			" FROM cder_contrato_cedido cc "+
			", comcat_epo epo"+
			", comcat_if cif"+
			", comcat_pyme pym"+
			", comrel_pyme_epo cpe"+
			" WHERE cc.ic_epo = epo.ic_epo"+
			" AND cc.ic_if = cif.ic_if"+
			" AND cc.ic_pyme = pym.ic_pyme"+
			" AND cc.ic_epo = cpe.ic_epo(+)"+
			" AND cc.ic_pyme = cpe.ic_pyme(+)"+
			""
		);

		strSQL.append(" AND (");
									
		for (int i = 0; i < pageIds.size(); i++) {
			List lItem = (ArrayList)pageIds.get(i);
			if(i > 0){
				strSQL.append("  OR  ");
			}
			strSQL.append("cc.cg_no_contrato = ?");
			conditions.add(lItem.get(0));
		}
		strSQL.append(" ) ");
			
		log.debug("strSQL ::.."+strSQL.toString());
		log.debug("Conditions ::.."+conditions);

		log.info("getDocumentSummaryQueryForIds(S) ::..");
		return strSQL.toString();
	}


	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();

		strSQL.append(
			" SELECT cc.cg_no_contrato AS contrato "+
			" ,cc.ic_epo "+
			" ,cc.ic_if "+
			" ,cc.ic_pyme "+
			" ,cif.cg_razon_social AS nombre_cesionario "+
			" ,pym.cg_razon_social AS nombre_pyme "+
			" ,'true' AS eliminar_contrato "+
			" ,TO_CHAR(cc.df_fecha_captura, 'dd/mm/yyyy') as fecha_captura"+
			" FROM cder_contrato_cedido cc "+
			", comcat_epo epo"+
			", comcat_if cif"+
			", comcat_pyme pym"+
			", comrel_pyme_epo cpe"+
			" WHERE cc.ic_epo = epo.ic_epo"+
			" AND cc.ic_if = cif.ic_if"+
			" AND cc.ic_pyme = pym.ic_pyme"+
			" AND cc.ic_epo = cpe.ic_epo(+)"+
			" AND cc.ic_pyme = cpe.ic_pyme(+)"+
			""
		);

 		if(this.claveEpo != null	&&	!this.claveEpo.equals("")){
			strSQL.append(" AND cc.ic_epo = ?");
			conditions.add(new Integer(this.claveEpo));
		}
		if(this.claveIf != null	&&	!this.claveIf.equals("")){
			strSQL.append(" AND cc.ic_if = ?");
			conditions.add(new Integer(this.claveIf));
		}
		if(this.clavePyme != null	&&	!this.clavePyme.equals("")){
			strSQL.append(" AND cc.ic_pyme = ?");
			conditions.add(new Long(this.clavePyme));
		}
		if(this.noContrato != null	&&	!this.noContrato.equals("")){
			strSQL.append(" AND cc.cg_no_contrato = ?");
			conditions.add(this.noContrato);
		}
		strSQL.append(" ORDER BY cc.cg_no_contrato ");

		log.debug("strSQL ::.."+strSQL.toString());
		log.info("Conditions ::.."+conditions );
		log.info("getDocumentQueryFile(S) ::.. - -  "+strSQL.toString());
		return strSQL.toString();
	}//getDocumentQueryFile

	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		String nombreArchivo = "";
		return nombreArchivo;
	}
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String nombreArchivo = "";
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();

		try {
			int hayRegistros = 0;
			contenidoArchivo.append("No. Contrato, Nombre Cesionario, PYME (Cedente), ");
			contenidoArchivo.append("\n");

			while (rs.next()) {
				contenidoArchivo.append(	((rs.getString("contrato"))==null?"":rs.getString("contrato")).replace(',',' ') +",");
				contenidoArchivo.append(	((rs.getString("nombre_cesionario"))==null?"":rs.getString("nombre_cesionario")).replace(',',' ') +",");
				contenidoArchivo.append(	((rs.getString("nombre_pyme"))==null?"":rs.getString("nombre_pyme")).replace(',',' ') +",");
				contenidoArchivo.append("\r\n");
				hayRegistros ++;
			}
			if (rs!=null){rs.close();}
			if(hayRegistros==0){
				contenidoArchivo.append( " No se encontr� ning�n registro para los criter�os seleccionados. " );						
				contenidoArchivo.append( "\r\n");
			}
			creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
			nombreArchivo = creaArchivo.getNombre();
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo CSV", e);
		} finally {
		}
		return nombreArchivo;
	}

	//Getters
	public List getConditions()	{	return this.conditions;	}
	public String getClavePyme()	{	return this.clavePyme;	}
	public String getNoContrato()	{	return this.noContrato;	}
	public String getClaveIf()		{	return this.claveIf;		}
	public String getClaveEpo()	{	return this.claveEpo;	}

	//Setters
	public void setClaveCesionario(String clave_if)	{	this.claveIf = clave_if;		}
	public void setClaveEpo(String claveEpo) 			{	this.claveEpo = claveEpo;		}
	public void setClavePyme(String catPyme) 			{	this.clavePyme = catPyme;		}
	public void setClaveContrato(String noContrato) {	this.noContrato = noContrato;	}

}