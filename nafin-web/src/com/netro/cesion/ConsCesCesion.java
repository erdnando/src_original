package com.netro.cesion;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsCesCesion implements IQueryGeneratorRegExtJS {
	public ConsCesCesion() {
		//Constructor
	}
	
	private String cve_if;
	
	private static final Log log = ServiceLocator.getInstance().getLog(ConsCedCesion.class);//Variable para enviar mensajes al log.
	
	public String getAggregateCalculationQuery()	{
		
		StringBuffer 	qrySentencia 	= new StringBuffer();
		return qrySentencia.toString();
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds)  {
		StringBuffer qrySentencia = new StringBuffer();
		return qrySentencia.toString();
	}
	
	public String getDocumentQuery()	{
	
		StringBuffer qrySentencia = new StringBuffer();
		
		log.debug("getDocumentQuery)");
		return qrySentencia.toString();
	}
	
	public String getDocumentQueryFile() {
		this.variablesBind = new ArrayList();
		StringBuffer 	qrySentencia 	= new StringBuffer();
    	StringBuffer	condicion 		= new StringBuffer();
	
		try{
			qrySentencia.append(
						" SELECT c.CG_TIPO_REPRESENTANTE as TIPO_REP, c.CG_NOM_REPRESENTANTE as NOM_REP, c.CG_FIGURA_JURIDICA as FIG_JUR, "+
						" c.CG_ESCRITURA_PUBLICA as ESC_PUB, TO_CHAR(c.DF_ESCRITURA_PUBLICA, 'DD/MM/YYYY') as FEC_ESC_PUB, c.CG_NOMBRE_LICENCIADO as NOM_LIC, "+
						" c.IN_NUM_NOTARIO_PUBLICO as NOT_PUB, c.CG_ORIGEN_NOTARIO as CD_NOT, c.CG_REG_PUB_COMERCIO as REG_PUB, c.IN_NUM_FOLIO_MERCANTIL as FOLIO, c.CG_CUENTA_CLABE as CLABE, "+
						" c.CG_ESCRITURA_PUBLICA_REP as ESC_PUB_REP, TO_CHAR(c.DF_ESCRITURA_PUBLICA_REP, 'DD/MM/YYYY') as FEC_ESC_PUB_REP, c.CG_NOMBRE_LICENCIADO_REP as NOM_LIC_REP, "+
						" c.IN_NUM_NOTARIO_PUBLICO_REP as NOT_PUB_REP, c.CG_ORIGEN_NOTARIO_REP as CD_NOT_REP, CS_BANDERA_REP,CG_DOMICILIO_LEGAL, "+
						" nvl((dbms_lob.getlength(c.BI_PODERES_CESION)),0) as PODERES_CESION, "+
						" nvl((dbms_lob.getlength(c.BI_PODERES_EXTINCION)),0) as PODERES_EXTINCION, "+
						" CS_BANDERA_REP2 as bandera_rep2, CG_TIPO_REPRESENTANTE_REP2 as tipo_rep2, CG_NOM_REPRESENTANTE_REP2 as nom_rep2, cg_escritura_publica_rep2 as escritura_publica_rep2, "+
						" to_char(df_escritura_publica_rep2,'dd/mm/yyyy') as fecha_escritura_rep2, cg_nombre_licenciado_rep2 as nom_lic_rep2,in_num_notario_publico_rep2 as num_notario_rep2, "+
						" cg_origen_notario_rep2 as ciudad_notario_rep2, ic_if_rep2 as if_rep2"+
						" FROM comcat_cesionario c ");				
						
			if(!cve_if.equals("")){
				condicion.append("   WHERE c.ic_if = ? ");
					this.variablesBind.add(cve_if);
			}
			
			qrySentencia.append(condicion.toString());
			
		}catch (Exception e){
			log.debug("ConsCedCesion:: getDocumentQueryException "+e);
		}
	
		log.debug("getDocumentQueryFile(S) "+qrySentencia.toString());
		return qrySentencia.toString();
	}
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String nombreArchivo = "";
		
		return nombreArchivo;
	}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		log.debug("crearCustomFile (E)");

		return nombreArchivo;
	}
	
	public List getConditions() {
		log.debug("*************getConditions=" + variablesBind);
		return variablesBind;
	}
	
	private ArrayList variablesBind = null;

	public void setCve_if(String cve_if) {
		this.cve_if = cve_if;
	}

	public String getCve_if() {
		return cve_if;
	}

	
}