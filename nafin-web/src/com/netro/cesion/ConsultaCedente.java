package com.netro.cesion;

import netropology.utilerias.*;
import org.apache.commons.logging.Log;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class ConsultaCedente implements IQueryGeneratorRegExtJS{

	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaCedente.class);
	private List conditions;
	private String icEpo;
	private String icPyme;
	private String noContrato;
	private String fechaInicio;
	private String fechaFin;
	private String mensaje;

	public ConsultaCedente(){}

	/**
	 * Obtiene las llaves primarias
	 * @return sentencia sql
	 */
	public String getDocumentQuery(){
		return null;
	}

	/**
	 * Obtiene la lista de los Ids en turno para realizar la paginacion
	 * @return sentencia sql
	 * @param ids
	 */
	public String getDocumentSummaryQueryForIds(List ids){
		return null;
	}

	/**
	 * Obtiene los totales
	 * @return sentencia sql
	 */
	public String getAggregateCalculationQuery(){
		return null;
	}

	/**
	 * Obtiene la consulta para generar el archivo PDF o CSV sin utilizar paginaci�n
	 * @return sentencia sql
	 */
	public String getDocumentQueryFile(){
		log.info("getDocumentQueryFile(E)");

		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();

		if(!icPyme.equals("")){
			condicion.append(" AND N.IC_PYME  = ? ");
			conditions.add(icPyme);
		}
		if(!icEpo.equals("")){
			condicion.append(" AND S.IC_EPO  = ? ");
			conditions.add(icEpo);
		}
		if(!noContrato.equals("")){
			condicion.append(" AND S.CG_NO_CONTRATO  = ? ");
			conditions.add(noContrato);
		}
		if(!fechaInicio.equals("") && !fechaFin.equals("")){
			condicion.append(" AND N.DF_ESCRITURA_PUBLICA >= TO_DATE(?, 'DD/MM/YYYY') ");
			condicion.append(" AND N.DF_ESCRITURA_PUBLICA < TO_DATE(?, 'DD/MM/YYYY')+1 ");
			conditions.add(fechaInicio);
			conditions.add(fechaFin);
		}

		qrySentencia.append("SELECT S.IC_EPO, "                                                        );
		qrySentencia.append("N.IC_SOLICITUD, "                                                         );
		qrySentencia.append("N.IC_PYME, "                                                              );
		qrySentencia.append("E.CG_RAZON_SOCIAL AS NOMBRE_EPO, "                                        );
		qrySentencia.append("P.CG_RAZON_SOCIAL AS NOMBRE_PYME, "                                       );
		qrySentencia.append("S.CG_NO_CONTRATO AS NO_CONTRATO, "                                        );
		qrySentencia.append("CASE WHEN N.CG_TIPO_CEDENTE = 'P' THEN 'Proveedor' "                      );
		qrySentencia.append("WHEN N.CG_TIPO_CEDENTE = 'C' THEN 'Contratista' "                         );
		qrySentencia.append("ELSE '' END AS TIPO_CEDENTE, "                                            );
		qrySentencia.append("N.CG_FIGURA_JURIDICA AS FIGURA_JURIDICA, "                                );
		qrySentencia.append("N.CG_ESCRITURA_PUBLICA AS ESCRITURA_PUBLICA, "                            );
		qrySentencia.append("TO_CHAR(N.DF_ESCRITURA_PUBLICA,'DD/MM/YYYY') AS FECHA_ESCRITURA_PUBLICA, ");
		qrySentencia.append("N.CG_NOMBRE_LICENCIADO AS NOMBRE_LICENCIADO, "                            );
		qrySentencia.append("N.IN_NUM_NOTARIO_PUBLICO AS NOTARIO_PUBLICO, "                            );
		qrySentencia.append("N.CG_ORIGEN_NOTARIO AS ORIGEN_NOTARIO, "                                  );
		qrySentencia.append("N.CG_REG_PUB_COMERCIO AS REG_PUB_COMERCIO, "                              );
		qrySentencia.append("N.IN_NUM_FOLIO_MERCANTIL AS NUM_FOLIO_MERCANTIL, "                        );
		qrySentencia.append("N.CG_DOMICILIO_LEGAL AS DOMICILIO_LEGAL "                                 );
		qrySentencia.append("FROM CDER_SOLIC_PYME_NOTARIALES N, "                                      );
		qrySentencia.append("CDER_SOLICITUD S, "                                                       );
		qrySentencia.append("COMCAT_PYME P,COMCAT_EPO E "                                              );
		qrySentencia.append("WHERE N.IC_SOLICITUD = S.IC_SOLICITUD "                                   );
		qrySentencia.append("AND N.IC_PYME = P.IC_PYME "                                               );
		qrySentencia.append("AND S.IC_EPO = E.IC_EPO "                                                 );
		qrySentencia.append("AND S.IC_ESTATUS_SOLIC NOT IN (25) "                                      );
		qrySentencia.append(condicion.toString()                                                       );
		qrySentencia.append("ORDER BY E.CG_RAZON_SOCIAL, P.CG_RAZON_SOCIAL "                           );

		log.debug("Sentencia: " + qrySentencia.toString());
		log.debug("Condiciones: " + conditions.toString());
		log.info("getDocumentQueryFile(S)");

		return qrySentencia.toString();
	}

	/**
	 * Crea el archivo PDF o CSV seg�n el tipo que se le pasa como par�metro.
	 * Obtiene los datos de la consulta generada en el m�todo getDocumentQueryFile.
	 * @return nombre del archivo
	 * @param tipo
	 * @param path
	 * @param rsAPI
	 * @param request
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){

		log.info(" crearCustomFile(E)");

		String nombreArchivo = "";

		HttpSession session       = request.getSession();
		CreaArchivo creaArchivo   = new CreaArchivo();
		OutputStreamWriter writer = null;
		BufferedWriter buffer     = null;

		StringBuffer contenidoArchivo = new StringBuffer();

		String nombreEpo          = "";
		String nombrePyme         = "";
		String noContrato         = "";
		String tipoCedente        = "";
		String figuraJuridica     = "";
		String dfEscrituraPublica = "";
		String nombreLicenciado   = "";
		String origenNotario      = "";
		String regPubComercio     = "";
		String domicilioLegal     = "";

		int numFolioMercantil = 0;
		int notarioPublico    = 0;
		int escrituraPublica  = 0;
		int total             = 0;

		try {

			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);

			contenidoArchivo.append("EPO,"                      );
			contenidoArchivo.append("PYME,"                     );
			contenidoArchivo.append("No. Contrato,"             );
			contenidoArchivo.append("Tipo de CEDENTE,"          );
			contenidoArchivo.append("Figura Jur�dica,"          );
			contenidoArchivo.append("Escritura P�blica,"        );
			contenidoArchivo.append("Fecha Escritura P�blica,"  );
			contenidoArchivo.append("Nombre del Licenciado,"    );
			contenidoArchivo.append("N�m. Notario P�blico,"     );
			contenidoArchivo.append("Cd. Origen del Notario,"   );
			contenidoArchivo.append("Reg. P�blico de Comercio," );
			contenidoArchivo.append("Folio Mercantil,"          );
			contenidoArchivo.append("Domicilio Legal Empresa \n");

			while (rs.next()){
				nombreEpo          = (rs.getString("NOMBRE_EPO")              == null) ? "" : rs.getString("NOMBRE_EPO");
				nombrePyme         = (rs.getString("NOMBRE_PYME")             == null) ? "" : rs.getString("NOMBRE_PYME");
				noContrato         = (rs.getString("NO_CONTRATO")             == null) ? "" : rs.getString("NO_CONTRATO");
				tipoCedente        = (rs.getString("TIPO_CEDENTE")            == null) ? "" : rs.getString("TIPO_CEDENTE");
				figuraJuridica     = (rs.getString("FIGURA_JURIDICA")         == null) ? "" : rs.getString("FIGURA_JURIDICA");
				dfEscrituraPublica = (rs.getString("FECHA_ESCRITURA_PUBLICA") == null) ? "" : rs.getString("FECHA_ESCRITURA_PUBLICA");
				nombreLicenciado   = (rs.getString("NOMBRE_LICENCIADO")       == null) ? "" : rs.getString("NOMBRE_LICENCIADO");
				origenNotario      = (rs.getString("ORIGEN_NOTARIO")          == null) ? "" : rs.getString("ORIGEN_NOTARIO");
				regPubComercio     = (rs.getString("REG_PUB_COMERCIO")        == null) ? "" : rs.getString("REG_PUB_COMERCIO");
				domicilioLegal     = (rs.getString("DOMICILIO_LEGAL")         == null) ? "" : rs.getString("DOMICILIO_LEGAL");

				escrituraPublica  = (rs.getInt("ESCRITURA_PUBLICA")   == 0 ) ? 0 : rs.getInt("ESCRITURA_PUBLICA");
				notarioPublico    = (rs.getInt("NOTARIO_PUBLICO")     == 0 ) ? 0 : rs.getInt("NOTARIO_PUBLICO");
				numFolioMercantil = (rs.getInt("NUM_FOLIO_MERCANTIL") == 0 ) ? 0 : rs.getInt("NUM_FOLIO_MERCANTIL");

				contenidoArchivo.append(nombreEpo.replace(',',' ')+", "       );
				contenidoArchivo.append(nombrePyme.replace(',',' ')+", "      );
				contenidoArchivo.append(noContrato+", "                       );
				contenidoArchivo.append(tipoCedente.replace(',',' ')+", "     );
				contenidoArchivo.append(figuraJuridica.replace(',',' ')+", "  );
				contenidoArchivo.append(escrituraPublica+", "                 );
				contenidoArchivo.append(dfEscrituraPublica+", "               );
				contenidoArchivo.append(nombreLicenciado.replace(',',' ')+", ");
				contenidoArchivo.append(notarioPublico+", "                   );
				contenidoArchivo.append(origenNotario.replace(',',' ')+", "   );
				contenidoArchivo.append(regPubComercio.replace(',',' ')+", "  );
				contenidoArchivo.append(numFolioMercantil+", "                );
				contenidoArchivo.append(domicilioLegal.replace(',',' ')+"\n"  );

				total++;
				if(total==1000){
					total=0;
					buffer.write(contenidoArchivo.toString());
					contenidoArchivo = new StringBuffer();//Limpio
				}
			}

			buffer.write(contenidoArchivo.toString());
			buffer.close();
			contenidoArchivo = new StringBuffer();//Limpio

		} catch(Throwable e){
			throw new AppException("Error al generar el archivo csv ", e);
		} finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}

		log.info(" crearCustomFile(E)");
		return nombreArchivo;
	}

	/**
	 * Crea el archivo PDF utilizando paginaci�n
	 * @return 
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){
		return null;
	}

/*****************************************************
 *                GETTERS AND SETTERS                *
 *****************************************************/
 	public List getConditions(){
		return conditions;
	}

	public String getIcEpo() {
		return icEpo;
	}

	public void setIcEpo(String icEpo) {
		this.icEpo = icEpo;
	}

	public String getIcPyme() {
		return icPyme;
	}

	public void setIcPyme(String icPyme) {
		this.icPyme = icPyme;
	}

	public String getNoContrato() {
		return noContrato;
	}

	public void setNoContrato(String noContrato) {
		this.noContrato = noContrato;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

}