package com.netro.cesion;

import java.sql.*;
import netropology.utilerias.*;
import org.apache.commons.logging.Log;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

public class GruposCesionDerechosEmpresas implements IQueryGeneratorRegExtJS{

	private final static Log log = ServiceLocator.getInstance().getLog(GruposCesionDerechosEmpresas.class);
	private List conditions;
	private String icGrupoCesion;
	private String icPyme;
	private String nombrePyme;
	private String rfc;
	private String representanteLegal;
	private String contacto;
	private String nafinElectronico;

	public GruposCesionDerechosEmpresas(){}

	/**
	 * Obtiene las llaves primarias
	 * @return sentencia sql
	 */
	public String getDocumentQuery(){
		return null;
	}

	/**
	 * Obtiene la lista de los Ids en turno para realizar la paginacion
	 * @return sentencia sql
	 * @param ids
	 */
	public String getDocumentSummaryQueryForIds(List ids){
		return null;
	}

	/**
	 * Obtiene los totales
	 * @return sentencia sql
	 */
	public String getAggregateCalculationQuery(){
		return null;
	}
	
	/**
	 * Obtiene la consulta para generar el archivo PDF o CSV sin utilizar paginaci�n
	 * @return sentencia sql
	 */
	public String getDocumentQueryFile(){
		log.info("getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();

		qrySentencia.append("SELECT G.IC_GRUPO_CESION, "                    );
		qrySentencia.append("G.IC_PYME, "                                   );
		qrySentencia.append("P.CG_RAZON_SOCIAL AS NOMBRE_PYME, "            );
		qrySentencia.append("N.IC_NAFIN_ELECTRONICO AS NAFIN_ELECTRONICO, " );
		qrySentencia.append("P.CG_RFC AS RFC, "                             );
		qrySentencia.append("CASE G.CS_PYME_PRINCIPAL WHEN 'S' THEN 'true' ");
		qrySentencia.append("ELSE 'false' END AS PYME_PRINCIPAL "           );
		qrySentencia.append("FROM CDERREL_PYME_X_GRUPO G, "                 );
		qrySentencia.append("COMCAT_PYME P, COMREL_NAFIN N "                );
		qrySentencia.append("WHERE G.IC_PYME = P.IC_PYME "                  );
		qrySentencia.append("AND G.IC_PYME = N.IC_EPO_PYME_IF "             );
		qrySentencia.append("AND P.IC_PYME = N.IC_EPO_PYME_IF "             );
		qrySentencia.append("AND N.CG_TIPO = 'P' "                          );
		qrySentencia.append("AND IC_GRUPO_CESION = ? "                      );
		qrySentencia.append("ORDER BY G.IC_GRUPO_CESION, P.CG_RAZON_SOCIAL" );

		conditions.add(icGrupoCesion);

		log.debug("Sentencia: " + qrySentencia.toString());
		log.debug("Condiciones: " + conditions.toString());
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();
	}

	/**
	 * Crea el archivo PDF o CSV seg�n el tipo que se le pasa como par�metro.
	 * Obtiene los datos de la consulta generada en el m�todo getDocumentQueryFile.
	 * @return nombre del archivo
	 * @param tipo
	 * @param path
	 * @param rsAPI
	 * @param request
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rsAPI, String path, String tipo){
		return null;
	}

	/**
	 * Crea el archivo PDF utilizando paginaci�n
	 * @return 
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){
		return null;
	}

	/**
	 * Obtiene los datos del representante legal y del contacto de la pyme seleccionada.
	 * Los datos se guardan en variables de la clase y se obtienen por medio de sus getters.
	 * @return true si hay datos, false en caso contario.
	 */
	public boolean getDatosPyme(){

		boolean exito = false;;
		log.info("getDatosPyme(E)");

		AccesoDB con         = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs         = null;
		StringBuffer query   = new StringBuffer();
		
		query.append(" SELECT P.IC_PYME, P.CG_RAZON_SOCIAL, P.CG_RFC, ");
		query.append(" CG_NOMBREREP_LEGAL || ' ' || P.CG_APPAT_REP_LEGAL || ' ' || P.CG_APMAT_LEGAL AS REPRESENTANTE_LEGAL, ");
		query.append(" C.CG_NOMBRE || ' ' || C.CG_APPAT || ' ' || C.CG_APMAT AS CONTACTO, ");
		query.append(" N.IC_NAFIN_ELECTRONICO AS NAFIN_ELECTRONICO ");
		query.append(" FROM COMCAT_PYME P, COM_CONTACTO C, COMREL_NAFIN N ");
		query.append(" WHERE P.IC_PYME = C.IC_PYME ");
		query.append(" AND P.IC_PYME = N.IC_EPO_PYME_IF ");
		query.append(" AND C.IC_PYME = N.IC_EPO_PYME_IF ");
		query.append(" AND N.CG_TIPO = 'P' ");
		query.append(" AND P.IC_PYME = ? ");

		log.debug("Sentencia: " + query.toString());

		try{

			con.conexionDB();
			ps = con.queryPrecompilado(query.toString());
			ps.clearParameters();
			ps.setInt(1, Integer.parseInt(icPyme));
			rs = ps.executeQuery();
			if(rs.next()){
				this.icPyme             = "" + rs.getInt("IC_PYME");
				this.nombrePyme         = rs.getString("CG_RAZON_SOCIAL");
				this.rfc                = rs.getString("CG_RFC");
				this.representanteLegal = rs.getString("REPRESENTANTE_LEGAL");
				this.contacto           = rs.getString("CONTACTO");
				this.nafinElectronico   = rs.getString("NAFIN_ELECTRONICO");
				exito = true;
			}

		}catch(Exception e){
			exito = false;
			log.warn("GruposCesionDerechosEmpresas.getDatosPyme.Exception. " + e);
		}finally{
			try{
				if(rs!=null)
					rs.close();
				if(ps!=null)
					ps.close();
			}catch(SQLException ex){
				log.warn(ex);
			}
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}

		log.info("getDatosPyme(S)");
		return exito;
	}

/*****************************************************
 *                GETTERS AND SETTERS                *
 *****************************************************/
	public List getConditions(){
		return conditions;
	}
	
	public String getIcGrupoCesion(){
		return icGrupoCesion;
	}

	public void setIcGrupoCesion(String icGrupoCesion){
		this.icGrupoCesion = icGrupoCesion;
	}

	public String getIcPyme(){
		return icPyme;
	}

	public void setIcPyme(String icPyme){
		this.icPyme = icPyme;
	}

	public String getNombrePyme() {
		return nombrePyme;
	}

	public void setNombrePyme(String nombrePyme) {
		this.nombrePyme = nombrePyme;
	}

	public String getRfc() {
		return rfc;
	}

	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getRepresentanteLegal() {
		return representanteLegal;
	}

	public void setRepresentanteLegal(String representanteLegal) {
		this.representanteLegal = representanteLegal;
	}

	public String getContacto() {
		return contacto;
	}

	public void setContacto(String contacto) {
		this.contacto = contacto;
	}

	public String getNafinElectronico() {
		return nafinElectronico;
	}

	public void setNafinElectronico(String nafinElectronico) {
		this.nafinElectronico = nafinElectronico;
	}

}