package com.netro.cesion;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * consulta de Notificaciones de Cesion de Derechos
 * para el Perfil ADMIN IF
 * autor: Deysi Laura Hern�ndez Contreras
 */
public class ConsultaNotificacionesIF implements  IQueryGeneratorRegExtJS {	

	public ConsultaNotificacionesIF(){}

	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaNotificacionesIF.class);

	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer 		qrySentencia;  
	private List 		conditions;
	private String 	paginaOffset;
	private String 	paginaNo;
	private String clave_if;
	private String clave_contrat;
	private String clave_epo;
	private String clave_pyme;	
	private String num_contrato;
	private String clave_moneda;
	private String clave_plazo_contrato;
	private String plazo_contrato;
	private String clave_estatus;
	private String fecha_vigencia_ini;
	private String fecha_vigencia_fin;
	
	
	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */
		public String getAggregateCalculationQuery() {
			log.info("getAggregateCalculationQuery(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();

			log.debug("..:: qrySentencia "+qrySentencia.toString());
			log.debug("..:: conditions: "+conditions);
			log.info("getAggregateCalculationQuery(S)");
			return qrySentencia.toString();
		}
	
	
	 /**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
		public String getDocumentQuery() {
			log.info("getDocumentQuery(E)");
			qrySentencia = new StringBuffer();
			conditions = new ArrayList();
				
			
			
			log.debug("..:: qrySentencia: "+qrySentencia.toString());
			log.debug("..:: conditions: "+conditions);
			log.info("getDocumentQuery(S)");
			return qrySentencia.toString();
		}

	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */
		public String getDocumentSummaryQueryForIds(List pageIds) {
			log.info("getDocumentSummaryQueryForIds(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			
			
			
			
			log.debug("..:: qrySentencia: "+qrySentencia.toString());
			log.debug("..:: conditions: "+conditions);
			log.info("getDocumentSummaryQueryForIds(S)");
			return qrySentencia.toString();
		}

		public String getDocumentQueryFile() {
			log.info("getDocumentQueryFile(E)");
			qrySentencia = new StringBuffer();
			conditions = new ArrayList();

			qrySentencia.append(
									" SELECT sol.ic_solicitud AS CLAVE_SOLICITUD,  " +
									"        sol.ic_epo AS CLAVE_EPO," +
									"        sol.ic_pyme AS CLAVE_PYME," +
									"        sol.ic_if AS clave_if," +
									"        epo.cg_razon_social AS DEPENDENCIA,  " +
									//"        cp.cg_razon_social AS PYME_CEDENTE,  " +
									"        cp.cg_razon_social||';'||SOL.CG_EMPRESAS_REPRESENTADAS AS PYME_CEDENTE,  " +//FODEA-024-2014 MOD()
									"        cp.cg_rfc AS RFC, " +
									"        cpe.cg_pyme_epo_interno AS NO_PROVEEDOR,  " +
									"        TO_CHAR(sol.df_fecha_solicitud_ini, 'dd/mm/yyyy') AS FECHA_SOLICITUD, " +
									"        sol.cg_no_contrato AS NO_CONTRATO,  " +									
									"        tcn.cg_nombre AS TIPO_CONTRATACION,  " +
									"        TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy') AS F_INICIO_CONTRATO,  " +
									"        TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy') AS F_FINAL_CONTRATO,  " +
									"        cep.cg_area AS CLASIFICACION_EPO,  " +
									"        sol.cg_campo1 AS CAMPO_ADICIONAL_1,  " +
									"        sol.cg_campo2 AS CAMPO_ADICIONAL_2,  " +
									"        sol.cg_campo3 AS CAMPO_ADICIONAL_3,  " +
									"        sol.cg_campo4 AS CAMPO_ADICIONAL_4,  " +
									"        sol.cg_campo5 AS CAMPO_ADICIONAL_5,  " +
									"        sol.cg_obj_contrato AS OBJETO_CONTRATO, " +
									"        sol.cg_comentarios AS COMENTARIOS, " +//FODEA 041 - 2010 ACF
									"        sol.fn_monto_credito AS MONTO_CREDITO, " +
									"        sol.cg_referencia AS REFERENCIA_CREDITO, " +
									"        TO_CHAR(sol.df_vencimiento_credito, 'dd/mm/yyyy') AS FECHA_VENCIMIENTO_CREDITO, " +
									"        sol.cg_banco_deposito AS BANCO_DEPOSITO, " +
									"        sol.cg_cuenta AS CUENTA,  " +
									"        sol.cg_cuenta_clabe AS CUENTA_CABLE,  " +
									"        sts.cg_descripcion AS ESTATUS,  " +									 
									"        sol.ig_plazo ||'   ' || ctp.cg_descripcion  as PLAZO_CONTRATO, " +
									"       DECODE (sol.cg_observ_rechazo, null, 'N/A', sol.cg_observ_rechazo) as CAUSAS_RECHAZO, " +
									"			'' as MONTO_MONEDA, "+
									" TO_CHAR(sol.df_firma_contrato, 'dd/mm/yyyy') AS firma_contrato" +
									"   FROM cder_solicitud sol,  " +
									"        comcat_epo epo,  " +
									"        comcat_if cif,  " +
									"        comcat_pyme cp, " +								
									"        comrel_pyme_epo cpe,  " +
									"        cdercat_tipo_contratacion tcn,  " +
									"        cdercat_clasificacion_epo cep,  " +
									"        cdercat_estatus_solic sts, " +
									"        cdercat_tipo_plazo ctp " +
									"  WHERE sol.ic_epo = epo.ic_epo  " +
									"    AND sol.ic_if = cif.ic_if " +
									"    AND sol.ic_pyme = cp.ic_pyme  " +
									
									"    AND sol.ic_epo = cpe.ic_epo(+)" +
									"    AND sol.ic_pyme = cpe.ic_pyme(+)" +
									"    AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion  " +
									"    AND sol.ic_epo = cep.ic_epo  " +
									"    AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo  " +
									"    AND sol.ic_estatus_solic = sts.ic_estatus_solic " +
									"	  AND cif.ic_if = ? "+
									"	  AND sol.ic_estatus_solic IN (11,12,15,16) " + 
									 "   AND sol.ic_tipo_plazo = ctp.ic_tipo_plazo(+) "
								 );
			conditions.add(clave_if);									
			 if(!clave_contrat.equals("")){
				qrySentencia.append("   AND tcn.ic_tipo_contratacion = ? ");
				conditions.add(clave_contrat);
			}
			if(!clave_estatus.equals("")){
				qrySentencia.append("   AND sts.ic_estatus_solic = ? ");
				conditions.add(clave_estatus);
			}
			if(!clave_epo.equals("")){
				qrySentencia.append("   AND epo.ic_epo = ? ");
				conditions.add(clave_epo);
			}
			if(!clave_pyme.equals("")){
				qrySentencia.append("   AND cp.ic_pyme = ? " );
				conditions.add(clave_pyme);
			}
			if(!fecha_vigencia_ini.equals("") && !fecha_vigencia_fin.equals("")){
				qrySentencia.append(" AND sol.df_fecha_vigencia_ini >= TO_DATE(?, 'dd/mm/yyyy')");
				qrySentencia.append(" AND sol.df_fecha_vigencia_fin <= TO_DATE(?, 'dd/mm/yyyy')");
				conditions.add(fecha_vigencia_ini);
				conditions.add(fecha_vigencia_fin);
			}
			if(!num_contrato.equals("")){
				qrySentencia.append("   AND sol.cg_no_contrato = ? ");
				conditions.add(num_contrato);
			}
			if (clave_moneda != null && !"".equals(clave_moneda)) {
				if (clave_moneda.equals("0")) {
					qrySentencia.append(" AND sol.cs_multimoneda = ?");
               conditions.add("S");
            } else {
					qrySentencia.append(" AND sol.cs_multimoneda = ?");
               qrySentencia.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
               qrySentencia.append(" FROM cder_monto_x_solic mxs");
               qrySentencia.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
               qrySentencia.append(" AND mxs.ic_moneda = ?)");
               conditions.add("N");
               conditions.add(new Integer(clave_moneda));
            }
         }      
         if(!clave_plazo_contrato.equals("")){
				qrySentencia.append(" AND sol.ic_tipo_plazo = ? ");
            conditions.add(new Integer(clave_plazo_contrato));
         }
         if(!plazo_contrato.equals("")){
				qrySentencia.append(" AND sol.ig_plazo = ? ");
            conditions.add(new Integer(plazo_contrato));
         }
			qrySentencia.append(" ORDER BY sol.df_alta_solicitud DESC"); 
				
		
			log.debug("..:: qrySentencia: "+qrySentencia.toString());
			log.debug("..:: conditions: "+conditions);
			log.info("getDocumentQueryFile(S)");
			return qrySentencia.toString();
		}//getDocumentQueryFile


	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
	String nombreArchivo = "";
	
	return nombreArchivo;
	}

	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		ComunesPDF pdfDoc = new ComunesPDF();
		try {
		
			CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);

			if(tipo.equals("PDF") ) {
			
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString()+"", 
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),  
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
				pdfDoc.setTable(13,100);	
				pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Dependencia","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Pyme (Cedente)","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("RFC","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No. Proveedor","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Solicitud PyME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No. Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Firma Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto/Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de Contrataci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("F. Inicio Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("F. Final Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo del  Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);				
				pdfDoc.setCell("Clasificaci�n EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Objeto del Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Comentarios","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto del Cr�dito","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Referencia / No. Cr�dito","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Vencimiento Cr�dito","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Banco de Deposito","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Cuenta","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Cuenta CABLE","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Causas de Rechazo Notificaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
			}
						
			if(tipo.equals("CSV")) {
			
			contenidoArchivo.append("Dependencia, Pyme (Cedente), RFC, No. Proveedor, Fecha de Solicitud PyME," +
											"No. Contrato, Firma Contrato, Monto/Moneda, Tipo de Contrataci�n, F. Inicio Contrato, F. Final Contrato, "+
											"Plazo del  Contrato, Clasificaci�n EPO, Objeto del Contrato, Comentarios, "+
											" Monto del Cr�dito, Referencia / No. Cr�dito, Fecha de Vencimiento Cr�dito, Banco de Deposito, "+
											" Cuenta, Cuenta CABLE, Estatus, Causas de Rechazo Notificaci�n \n");
			}	
			
			while (rs.next())	{					
			
				String dependencia = (rs.getString("DEPENDENCIA") == null) ? "" : rs.getString("DEPENDENCIA");
				String pyme_cedente = (rs.getString("PYME_CEDENTE") == null) ? "" : rs.getString("PYME_CEDENTE");
				String rfc = (rs.getString("RFC") == null) ? "" : rs.getString("RFC");
				String no_proveedor = (rs.getString("NO_PROVEEDOR") == null) ? "" : rs.getString("NO_PROVEEDOR");
				String fecha_solicitud = (rs.getString("FECHA_SOLICITUD") == null) ? "" : rs.getString("FECHA_SOLICITUD");
				String no_contrato = (rs.getString("NO_CONTRATO") == null) ? "" : rs.getString("NO_CONTRATO");
				String tipo_contratacion = (rs.getString("TIPO_CONTRATACION") == null) ? "" : rs.getString("TIPO_CONTRATACION");
				String f_inicio_contrato = (rs.getString("F_INICIO_CONTRATO") == null) ? "" : rs.getString("F_INICIO_CONTRATO");
				String f_final_contrato = (rs.getString("F_FINAL_CONTRATO") == null) ? "" : rs.getString("F_FINAL_CONTRATO");
				String plazo_contrato = (rs.getString("PLAZO_CONTRATO") == null) ? "" : rs.getString("PLAZO_CONTRATO");
				String clasificacion_epo = (rs.getString("CLASIFICACION_EPO") == null) ? "" : rs.getString("CLASIFICACION_EPO");
				String objeto_contrato = (rs.getString("OBJETO_CONTRATO") == null) ? "" : rs.getString("OBJETO_CONTRATO");
				String comentarios = (rs.getString("COMENTARIOS") == null) ? "" : rs.getString("COMENTARIOS");
				String monto_credito = (rs.getString("MONTO_CREDITO") == null) ? "" : rs.getString("MONTO_CREDITO");
				String referencia_credito = (rs.getString("REFERENCIA_CREDITO") == null) ? "" : rs.getString("REFERENCIA_CREDITO");				
				String fecha_vencimiento_credito = (rs.getString("FECHA_VENCIMIENTO_CREDITO") == null) ? "" : rs.getString("FECHA_VENCIMIENTO_CREDITO");
				String banco_depposito = (rs.getString("BANCO_DEPOSITO") == null) ? "" : rs.getString("BANCO_DEPOSITO");
				String cuenta = (rs.getString("CUENTA") == null) ? "" : rs.getString("CUENTA");
				String cuenta_clabe = (rs.getString("CUENTA_CABLE") == null) ? "" : rs.getString("CUENTA_CABLE");
				String estatus = (rs.getString("ESTATUS") == null) ? "" : rs.getString("ESTATUS");
				String causas_rechazo = (rs.getString("CAUSAS_RECHAZO") == null) ? "" : rs.getString("CAUSAS_RECHAZO");
				String firmaContrato = (rs.getString("FIRMA_CONTRATO") == null) ? "" : rs.getString("FIRMA_CONTRATO");
				String 	clave_solicitud = (rs.getString("CLAVE_SOLICITUD") == null) ? "" : rs.getString("CLAVE_SOLICITUD");
				StringBuffer montosMonedaSol = cesionBean.getMontoMoneda(clave_solicitud);
				String monto_moneda  =montosMonedaSol.toString();
				
				if(tipo.equals("PDF") ) {				
					pdfDoc.setCell("A","fromas",ComunesPDF.CENTER);
					pdfDoc.setCell(dependencia,"fromas",ComunesPDF.CENTER);
					//FODEA-024-2014 MOD()
					String cedente[] = pyme_cedente.split(";");
						if(cedente.length == 1){
							pyme_cedente = pyme_cedente.replaceAll(";","");
						}else{
							pyme_cedente = pyme_cedente.replaceAll(";","\n\n");
						}
						//
					pdfDoc.setCell(pyme_cedente,"fromas",ComunesPDF.LEFT);
					pdfDoc.setCell(rfc,"fromas",ComunesPDF.LEFT);
					pdfDoc.setCell(no_proveedor,"fromas",ComunesPDF.LEFT);
					pdfDoc.setCell(fecha_solicitud,"fromas",ComunesPDF.CENTER);
					pdfDoc.setCell(no_contrato,"fromas",ComunesPDF.CENTER);
					pdfDoc.setCell(firmaContrato,"fromas",ComunesPDF.CENTER);
					pdfDoc.setCell(monto_moneda.replaceAll("<br/>", "\n").replaceAll(",", ""),"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(tipo_contratacion,"fromas",ComunesPDF.CENTER);
					pdfDoc.setCell(f_inicio_contrato,"fromas",ComunesPDF.CENTER);
					pdfDoc.setCell(f_final_contrato,"fromas",ComunesPDF.CENTER);
					pdfDoc.setCell(plazo_contrato,"fromas",ComunesPDF.CENTER);
					pdfDoc.setCell("B","fromas",ComunesPDF.CENTER);
					pdfDoc.setCell(clasificacion_epo,"fromas",ComunesPDF.CENTER);					
					pdfDoc.setCell(objeto_contrato,"fromas",ComunesPDF.CENTER);
					pdfDoc.setCell(comentarios,"fromas",ComunesPDF.CENTER);				
					pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(monto_credito), 2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(referencia_credito,"fromas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_vencimiento_credito,"fromas",ComunesPDF.CENTER);
					pdfDoc.setCell(banco_depposito,"fromas",ComunesPDF.CENTER);
					pdfDoc.setCell(cuenta,"fromas",ComunesPDF.CENTER);
					pdfDoc.setCell(cuenta_clabe,"fromas",ComunesPDF.CENTER);
					pdfDoc.setCell(estatus,"fromas",ComunesPDF.CENTER);
					pdfDoc.setCell(causas_rechazo,"fromas",ComunesPDF.CENTER);
					pdfDoc.setCell("","fromas",ComunesPDF.CENTER);
				}
				
				if(tipo.equals("CSV") ) {	
				
					contenidoArchivo.append(dependencia.replaceAll(",","")+","); 
					//FODEA-024-2014 MOD()
					String cedente[] = pyme_cedente.split(";");
						if(cedente.length == 1){
							pyme_cedente = pyme_cedente.replaceAll(";","");
						}else{
						
						}
						//
					contenidoArchivo.append(pyme_cedente.replaceAll(",","")+","); 
					contenidoArchivo.append(rfc.replaceAll(",","")+","); 
					contenidoArchivo.append(no_proveedor.replaceAll(",","")+","); 
					contenidoArchivo.append(fecha_solicitud.replaceAll(",","")+","); 
					contenidoArchivo.append(no_contrato.replaceAll(",","")+","); 
					contenidoArchivo.append(firmaContrato+","); 
					contenidoArchivo.append(monto_moneda.replaceAll("<br/>", "|").replaceAll(",", "")+",");
					contenidoArchivo.append(tipo_contratacion.replaceAll(",","")+","); 
					contenidoArchivo.append(f_inicio_contrato.replaceAll(",","")+","); 
					contenidoArchivo.append(f_final_contrato.replaceAll(",","")+","); 
					contenidoArchivo.append(plazo_contrato.replaceAll(",","")+","); 
					contenidoArchivo.append(clasificacion_epo.replaceAll(",","")+","); 
					contenidoArchivo.append(objeto_contrato.replaceAll(",","")+","); 
					contenidoArchivo.append(comentarios.replaceAll(",","")+","); 				
					contenidoArchivo.append("\"$"+Comunes.formatoDecimal(monto_credito,2,false)+"\",");						
					contenidoArchivo.append(referencia_credito.replaceAll(",","")+","); 
					contenidoArchivo.append(fecha_vencimiento_credito.replaceAll(",","")+","); 
					contenidoArchivo.append(banco_depposito.replaceAll(",","")+","); 
					contenidoArchivo.append("'"+cuenta.replaceAll(",","")+","); 
					contenidoArchivo.append("'"+cuenta_clabe.replaceAll(",","")+","); 
					contenidoArchivo.append(estatus.replaceAll(",","")+","); 
					contenidoArchivo.append(causas_rechazo.replaceAll(",","")+","); 
					contenidoArchivo.append("\n"); 				
				}			    
				
			}//while (rs.next())	{	
			if(tipo.equals("PDF") ) {
				pdfDoc.addTable();
				pdfDoc.endDocument();	
			}
		if(tipo.equals("CSV")) {
			creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
			nombreArchivo = creaArchivo.getNombre();
		}
			
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo", e);
		} finally {
			try {
				//reg.close();
				} catch(Exception e) {}
		}		
		return nombreArchivo;
	
	}	
	
	
/*****************************************************
	 GETTERS*******************************************************/
 
	/**
	  Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	
	
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getClave_if() {
		return clave_if;
	}

	public void setClave_if(String clave_if) {
		this.clave_if = clave_if;
	}

	public String getClave_contrat() {
		return clave_contrat;
	}

	public void setClave_contrat(String clave_contrat) {
		this.clave_contrat = clave_contrat;
	}

	public String getClave_epo() {
		return clave_epo;
	}

	public void setClave_epo(String clave_epo) {
		this.clave_epo = clave_epo;
	}

	public String getClave_pyme() {
		return clave_pyme;
	}

	public void setClave_pyme(String clave_pyme) {
		this.clave_pyme = clave_pyme;
	}
	
	public String getNum_contrato() {
		return num_contrato;
	}

	public void setNum_contrato(String num_contrato) {
		this.num_contrato = num_contrato;
	}

	public String getClave_moneda() {
		return clave_moneda;
	}

	public void setClave_moneda(String clave_moneda) {
		this.clave_moneda = clave_moneda;
	}

	public String getClave_plazo_contrato() { 
		return clave_plazo_contrato;
	}

	public void setClave_plazo_contrato(String clave_plazo_contrato) {
		this.clave_plazo_contrato = clave_plazo_contrato;
	}

	public String getPlazo_contrato() {
		return plazo_contrato;
	}

	public void setPlazo_contrato(String plazo_contrato) {
		this.plazo_contrato = plazo_contrato;
	}

	public String getClave_estatus() {
		return clave_estatus;
	}

	public void setClave_estatus(String clave_estatus) {
		this.clave_estatus = clave_estatus;
	}

	public String getFecha_vigencia_ini() {
		return fecha_vigencia_ini;
	}

	public void setFecha_vigencia_ini(String fecha_vigencia_ini) {
		this.fecha_vigencia_ini = fecha_vigencia_ini;
	}

	public String getFecha_vigencia_fin() {
		return fecha_vigencia_fin;
	}

	public void setFecha_vigencia_fin(String fecha_vigencia_fin) {
		this.fecha_vigencia_fin = fecha_vigencia_fin;
	}

	


}