package com.netro.cesion;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

/**
 * Admin IF Contrato de Cesi�n
 * autor:Deysi Laura Hern�ndez Contreras
 */

public class ConsContratoCesionIF implements IQueryGeneratorRegExtJS {   
      

//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsContratoCesionIF.class);
	private String 	paginaOffset;
	private String 	paginaNo; 
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String clave_pyme;
	private String clave_epo;
	private String numero_contrato;
	private String clave_moneda;
	private String clave_estatus_sol;
	private String clave_contratacion;
	private String fecha_vigencia_ini;
	private String fecha_vigencia_fin;
	private String plazoContrato;
	private String claveTipoPlazo;
	private String clave_if;
	private String clasificacionEpo;
	private String indiceCamposAdicionales;
	private String campo1;
	private String campo2;
	private String campo3;
	private String campo4;
	private String campo5;
	private String autIfOperativo;
	
	 
	public ConsContratoCesionIF() { }
	
	  
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer(); 
		conditions 		= new ArrayList();  
	
	
		qrySentencia.append(" SELECT sol.ic_solicitud AS CLAVE_SOLICITUD, "+
			" sol.ic_epo AS CLAVE_EPO,"+
			" sol.ic_pyme AS CLAVE_PYME,"+
			" sol.ic_tipo_contratacion AS CLAVE_TIPO_CONTRATACION,"+
			" epo.cg_razon_social AS DEPENDENCIA,"+
			" pym.cg_razon_social AS PYME_CEDENTE,"+
			" SOL.CG_EMPRESAS_REPRESENTADAS AS EMPRESAS,"+//FODEA-024-2014 MOD() 
			" pym.cg_rfc AS RFC,"+
			" cpe.cg_pyme_epo_interno AS PROVEEDOR,"+
			" ''  as REPRESENTANTE, " +
			" TO_CHAR(sol.df_fecha_solicitud_ini, 'dd/mm/yyyy') AS FECHA_SOLICITUD,"+
			" sol.cg_no_contrato AS NO_CONTRATO,"+
			" tcn.cg_nombre AS TIPO_CONTRATACION,"+
			" '' as MONTO_MONEDA, "+
			" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS FECHA_INI_CONTRA,"+
			" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS FECHA_FIN_CONTRA,"+
			" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || ctp.cg_descripcion) AS PLAZO_CONTRATO,"+
			" cep.cg_area AS CLASIFICACION_EPO,"+
			" TO_CHAR(sol.df_fecha_aceptacion, 'dd/mm/yyyy') AS FECHA_LIMITE,"+
			" sol.cg_campo1 AS CAMPO1,"+
			" sol.cg_campo2 AS CAMPO2,"+
			" sol.cg_campo3 AS CAMPO3,"+
			" sol.cg_campo4 AS CAMPO4,"+
			" sol.cg_campo5 AS CAMPO5,"+
			" cvp.cg_descripcion AS VENTANILLA_PAGO,"+
			" sol.cg_sup_adm_resob AS SUPERVISOR,"+
			" sol.cg_numero_telefono AS TELEFONO,"+
			" sol.cg_obj_contrato AS OBJETO_CONTRATO,"+
			" sol.cg_comentarios AS COMENTARIOS,"+
			" sol.fn_monto_credito AS MONTO_CREDITO,"+
			" sol.cg_referencia AS REFERENCIA_CREDITO,"+
			" TO_CHAR(sol.df_vencimiento_credito, 'dd/mm/yyyy') AS FECHA_VENCIMIENTO,"+
			" sol.cg_banco_deposito AS BANCO_DEPOSITO,"+
			" sol.cg_cuenta AS CUENTA,"+
			" sol.cg_cuenta_clabe AS CUENTA_CLABE,"+
			" sol.ic_estatus_solic AS IC_ESTATUS,"+
			" sts.cg_descripcion AS ESTATUS,"+
			" sol.cs_firmado_if AS FIRMADO_IF,"+
			" TO_CHAR(sol.df_firma_testigo1, 'dd/mm/yyyy') AS FECHA_FIRMA_1,"+
			" TO_CHAR(sol.df_firma_testigo2, 'dd/mm/yyyy') AS FECHA_FIRMA_2,"+
			" cda.cg_nombre_usuario as NOMBRE_USUARIO, "+ 
			" TO_CHAR(sol.df_alta_solicitud, 'dd/mm/yyyy') as fechasolicitud, "+
			" (select cg_razon_social from comcat_if where ic_if = ? ) as nombre_if, "+
			" TO_CHAR(sol.df_firma_contrato, 'dd/mm/yyyy') AS firma_contrato, "+				
			" CASE  WHEN sol.cc_acuse4 IS NOT NULL  THEN 'S' ELSE 'N' END AS representante1,  "+
			" CASE  WHEN sol.cc_acuse6 IS NOT NULL  THEN 'S' ELSE 'N'  END firmo_rep2, "+
			" sol.cg_causas_retorno as causas_retorno ,   "+
			" ces.ic_if_rep2  LOGIN_REPRESENTANTE2,   "+
			" sol.CS_AUTORIZO_IF_OPERATIVO  as CS_AUTORIZO_IF_OPERATIVO,  "+
			" SOL.IC_GRUPO_CESION   AS GRUPO_CESION "+ 
			
			" FROM cder_solicitud sol"+
			", comcat_epo epo"+
			", comcat_pyme pym"+
			", comrel_pyme_epo cpe"+
			", cdercat_tipo_contratacion tcn"+
			", cdercat_clasificacion_epo cep"+
			", cdercat_ventanilla_pago cvp"+
			", cdercat_estatus_solic sts"+
			", cder_acuse cda"  +    
			", cdercat_tipo_plazo ctp"+
			", comcat_cesionario ces "+
			" WHERE sol.ic_epo = epo.ic_epo"+
			" AND sol.ic_pyme = pym.ic_pyme"+
			" AND ces.ic_if = sol.ic_if "+
			" AND  (sol.ic_if = ? OR ces.ic_if_rep2 = ?) "+
			" AND sol.ic_epo = cpe.ic_epo(+)"+
			" AND sol.ic_pyme = cpe.ic_pyme(+)"+
			" AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion"+
			" AND sol.ic_epo = cep.ic_epo"+
			" AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo"+
			" AND sol.ic_estatus_solic = sts.ic_estatus_solic"+
			" AND sol.cc_acuse2 = cda.cc_acuse(+)"+ 
			" AND sol.ic_tipo_plazo = ctp.ic_tipo_plazo(+)"+
			" AND sol.ic_epo = cvp.ic_epo(+)"+
			" AND sol.ic_ventanilla_pago = cvp.ic_ventanilla_pago(+)");
			conditions.add(new Integer(clave_if));
		//	conditions.add(new Integer(clave_if));
			conditions.add(new Integer(clave_if));
			conditions.add(new Integer(clave_if));
			
			/*if(!clave_if.equals("")){
				qrySentencia.append(" AND sol.ic_if = ? ");
				conditions.add(new Integer(clave_if));	
			}*/
			
			if(!clave_epo.equals("")){
				qrySentencia.append( " AND sol.ic_epo = ?");
				conditions.add(new Integer(clave_epo));
			}
			
			if(!clave_pyme.equals("")){
				qrySentencia.append(" AND sol.ic_pyme = ? ");
				conditions.add(new Long(clave_pyme));
			}
			if(!numero_contrato.equals("")){
				qrySentencia.append(" AND sol.cg_no_contrato = ?");
				conditions.add(numero_contrato);
			}     
			if(!clave_estatus_sol.equals("")){
				//qrySentencia.append(" AND sol.ic_estatus_solic = ?");
				qrySentencia.append(" AND sol.ic_estatus_solic in(  ");
				String claveAux []=clave_estatus_sol.split(",");
				for(int i= 0 ; i<claveAux.length ; i++){
					if(i==(claveAux.length-1)){
						qrySentencia.append(" ? ) ");
					}else{
						qrySentencia.append(" ?, ");
					}
					conditions.add(new Integer(claveAux[i]));	
					
				}
				
			}else{
			   qrySentencia.append(" AND sol.ic_estatus_solic IN (?, ?, ?, ?, ?, ?, ? , ?, ?  )");
			   conditions.add(new Integer(7));
			   conditions.add(new Integer(9)); 
			   conditions.add(new Integer(10));
			   conditions.add(new Integer(11));          
			   conditions.add(new Integer(12));
			   conditions.add(new Integer(15));  
			   conditions.add(new Integer(16));  
			   conditions.add(new Integer(17));             
			   conditions.add(new Integer(25));       
			}
			if(!clave_contratacion.equals("")){
				qrySentencia.append(" AND sol.ic_tipo_contratacion = ?");
				conditions.add(new Integer(clave_contratacion));
			}
			if(!fecha_vigencia_ini.equals("") && !fecha_vigencia_fin.equals("")){
				qrySentencia.append(" AND sol.df_fecha_vigencia_ini >= TO_DATE(?, 'dd/mm/yyyy')" +
				" AND sol.df_fecha_vigencia_fin <= TO_DATE(?, 'dd/mm/yyyy')");
				conditions.add(fecha_vigencia_ini);
				conditions.add(fecha_vigencia_fin);
			}
			
			 if (clave_moneda != null && !"".equals(clave_moneda)) {
				if (clave_moneda.equals("0")) {
					qrySentencia.append("AND sol.cs_multimoneda = ? ");
					conditions.add("S");
				} else {
					qrySentencia.append( " AND sol.cs_multimoneda = ?"+
					" AND EXISTS (SELECT mxs.ic_monto_x_solic"+
					" FROM cder_monto_x_solic mxs" +
					" WHERE mxs.ic_solicitud = sol.ic_solicitud" +
					" AND mxs.ic_moneda = ?)");
					conditions.add("N");
					conditions.add(new Integer(clave_moneda));
				}
			}      
      if(!claveTipoPlazo.equals("")){
			qrySentencia.append( " AND sol.ic_tipo_plazo = ? " );
        conditions.add(new Integer(claveTipoPlazo));
      }
		
      if(!plazoContrato.equals("")){
      qrySentencia.append( " AND sol.ig_plazo = ? ");
        conditions.add(new Integer(plazoContrato));
      }
		if(autIfOperativo!=null&&autIfOperativo.equals("S")){
			qrySentencia.append( " AND sol.CS_AUTORIZO_IF_OPERATIVO = ? ");
        conditions.add(autIfOperativo);	
		}
		
		 qrySentencia.append(" ORDER BY sol.df_alta_solicitud DESC " );
	
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		try {
		
		     
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		ComunesPDF pdfDoc = new ComunesPDF();
	
		String dependencia  ="", pyme_cedente ="", rfc="", proveedor ="", representante ="", fecha_solicitud ="",
					 no_contrato ="", monto_moneda="", tipo_contratacion ="", fecha_ini_contra ="", fecha_fin_contra ="",
					 plazo_contrato ="", clasificacion_epo ="", fecha_limite ="",  campo01="", campo02 ="", campo03 ="", 
					 campo04 ="", campo05="",  ventanilla_pago  ="",  supervisor="", telefono ="", objeto_contrato ="", 
					 comentarios ="", monto_credito ="", referencia_credito ="", fecha_vencimiento ="", banco_deposito ="",
					 cuenta ="", cuenta_cable ="", estatus ="", fecha_firma_1  ="", fecha_firma_2 ="", clave_solicitud ="",
					 clave_pyme ="",firmaContrato="",causasRetorno = "",empresas = "";
			
		try {
				
			CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);

			if(tipo.equals("PDF") ) {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString()+"", 
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),  
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			   pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);
			}
			 
			pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);
		indiceCamposAdicionales="0";
			if(tipo.equals("PDF") ) {
				String mensaje  =	" Con base a lo dispuesto en el C�digo de Comercio por este conducto me permito notificar a esa Entidad el contrato de cesi�n de Derechos de Cobro que hemos formalizado con la empresa CEDENTE respecto al contrato que aqu� mismo se detalla \n " +
									" Por lo anterior, me permito solicitar a usted, se registre la Cesi�n de Derechos de Cobro de referencia a efecto de que esa Entidad, realice el(los) pago(s) derivados del Contrato aqu� se�alado a favor del INTERMEDIARIO FINANCIERO (CESIONARIO), en la inteligencia de que �ste, est� de acuerdo con lo establecido en el Convenio Marco de Cesi�n Electr�nica de Derechos  de Cobro. \n "+
									" Los datos de nuestra cuenta bancaria en la que se deben depositar los pagos derivados de esta operaci�n son los que aqu� se detallan. \n"+
								   " Sobre el particular y de conformidad con lo se�alado en la emisi�n de la conformidad para ceder los derechos de cobro emitida por esa Entidad, por este medio les notifico, la Cesi�n de Derechos de Cobro del Contrato antes mencionado, para todos los efectos legales a que haya lugar, adjunto, en forma electr�nica.\n ";
							 
			
				pdfDoc.setTable(1,80);
				pdfDoc.setCell(mensaje,"formas",ComunesPDF.JUSTIFIED);	
				pdfDoc.addTable();
				
				pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);				
				pdfDoc.setTable(16,100);								
				pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Dependencia","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Pyme (Cedente)", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" RFC","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Proveedor","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Representante legal","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de Solicitud PyME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" No. Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Firma Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto / Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Tipo de Contrataci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" F. Inicio Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" F. Final Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Plazo del Contrato","celda01",ComunesPDF.CENTER);		
				if(!clasificacionEpo.equals("")) {
				pdfDoc.setCell(clasificacionEpo,"celda01",ComunesPDF.CENTER);
				}				
				pdfDoc.setCell(" F. L�mite  para Notificaci�n ","celda01",ComunesPDF.CENTER);
				if(clasificacionEpo.equals("")) {
				pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);
				}							
				pdfDoc.setCell("B","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell(" Ventanilla de Pago","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Supervisor/Administrador /Residente de Obra","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Tel�fono","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Objeto del Contrato","celda01",ComunesPDF.CENTER);
			   pdfDoc.setCell(" Comentarios","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto del Cr�dito","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Referencia / No. Cr�dito","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha Vencimiento Cr�dito","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Banco de Dep�sito","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Cuenta","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Cuenta CLABE","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha Firma 1er Testigo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha Firma 2do Testigo","celda01",ComunesPDF.CENTER);		
				
			
				if(!indiceCamposAdicionales.equals("0")) {	
					pdfDoc.setCell("","celda01",ComunesPDF.CENTER);	
					int  nu = 0 - Integer.parseInt(indiceCamposAdicionales);
					int nuT =15-  Integer.parseInt(indiceCamposAdicionales);
					pdfDoc.setCell("C","celda01",ComunesPDF.CENTER); 
					if(!campo1.equals("")) { pdfDoc.setCell(campo1,"celda01",ComunesPDF.CENTER); 	}
					if(!campo2.equals("")) { pdfDoc.setCell(campo2,"celda01",ComunesPDF.CENTER); 	}
					if(!campo3.equals("")) { pdfDoc.setCell(campo3,"celda01",ComunesPDF.CENTER); 	}
					if(!campo4.equals("")) { pdfDoc.setCell(campo4,"celda01",ComunesPDF.CENTER); 	}
					if(!campo5.equals("")) { pdfDoc.setCell(campo5,"celda01",ComunesPDF.CENTER); 	}					
					pdfDoc.setCell("Causas de Retorno Pyme","celda01",ComunesPDF.CENTER);
					nuT= nuT-1;					
					pdfDoc.setCell("","celda01",ComunesPDF.CENTER, nuT);
					
				}else  {
					pdfDoc.setCell("Causas de Retorno Pyme","celda01",ComunesPDF.CENTER);
				}
			}						 
			if(tipo.equals("CSV") ) {
				contenidoArchivo.append(" Dependencia,  Pyme (Cedente),  RFC,  Proveedor, Representante legal,  Fecha de Solicitud PyME, No. Contrato, Firma Contrato,  Monto / Moneda,  Tipo de Contrataci�n , "+
												" F. Inicio Contrato,  F. Final Contrato,  Plazo del Contrato,  ");
				if(!clasificacionEpo.equals("")) { 	contenidoArchivo.append(clasificacionEpo +", ");  	}	
				contenidoArchivo.append(" F. L�mite  para Notificaci�n ,  ");
				if(!indiceCamposAdicionales.equals("0")) {	
					if(!campo1.equals("")) { contenidoArchivo.append(campo1.replaceAll(",","") +", ");	}
					if(!campo2.equals("")) { contenidoArchivo.append(campo2.replaceAll(",","") +", ");	}
					if(!campo3.equals("")) { contenidoArchivo.append(campo3.replaceAll(",","") +", ");	}
					if(!campo4.equals("")) { contenidoArchivo.append(campo4.replaceAll(",","") +", "); 	}
					if(!campo5.equals("")) { contenidoArchivo.append(campo5.replaceAll(",","") +", "); 	}					
				}
				contenidoArchivo.append(" Ventanilla de Pago ,  Supervisor/Administrador /Residente de Obra,  Tel�fono,  Objeto del Contrato, Comentarios, Monto del Cr�dito, "+
												" Referencia / No. Cr�dito,  Fecha Vencimiento Cr�dito,  Banco de Dep�sito, Cuenta,  Cuenta CLABE,  Estatus,  Fecha Firma 1er Testigo,   "+
												" Fecha Firma 2do Testigo  ,Causas retorno pyme ");
				contenidoArchivo.append("\n");
				
			}								
												
			while (rs.next())	{					
			
			
				dependencia = (rs.getString("DEPENDENCIA") == null) ? "" : rs.getString("DEPENDENCIA");
				pyme_cedente = (rs.getString("PYME_CEDENTE") == null) ? "" : rs.getString("PYME_CEDENTE");
				
				empresas  = (rs.getString("EMPRESAS") == null) ? "" : rs.getString("EMPRESAS");
				
				rfc = (rs.getString("RFC") == null) ? "" : rs.getString("RFC");
				proveedor = (rs.getString("PROVEEDOR") == null) ? "" : rs.getString("PROVEEDOR");
				fecha_solicitud = (rs.getString("FECHA_SOLICITUD") == null) ? "" : rs.getString("FECHA_SOLICITUD");
				no_contrato = (rs.getString("NO_CONTRATO") == null) ? "" : rs.getString("NO_CONTRATO");
				tipo_contratacion = (rs.getString("TIPO_CONTRATACION") == null) ? "" : rs.getString("TIPO_CONTRATACION");
				fecha_ini_contra = (rs.getString("FECHA_INI_CONTRA") == null) ? "" : rs.getString("FECHA_INI_CONTRA");				
				fecha_fin_contra = (rs.getString("FECHA_FIN_CONTRA") == null) ? "" : rs.getString("FECHA_FIN_CONTRA");				
				plazo_contrato = (rs.getString("PLAZO_CONTRATO") == null) ? "" : rs.getString("PLAZO_CONTRATO");				
				clasificacion_epo = (rs.getString("CLASIFICACION_EPO") == null) ? "" : rs.getString("CLASIFICACION_EPO");
				fecha_limite = (rs.getString("FECHA_LIMITE") == null) ? "" : rs.getString("FECHA_LIMITE");
				campo01 = (rs.getString("CAMPO1") == null) ? "" : rs.getString("CAMPO1");
				campo02 = (rs.getString("CAMPO2") == null) ? "" : rs.getString("CAMPO2");
				campo03 = (rs.getString("CAMPO3") == null) ? "" : rs.getString("CAMPO3");
				campo04 = (rs.getString("CAMPO4") == null) ? "" : rs.getString("CAMPO4");
				campo05 = (rs.getString("CAMPO5") == null) ? "" : rs.getString("CAMPO5");				
				ventanilla_pago = (rs.getString("VENTANILLA_PAGO") == null) ? "" : rs.getString("VENTANILLA_PAGO");
				supervisor = (rs.getString("SUPERVISOR") == null) ? "" : rs.getString("SUPERVISOR");
				telefono = (rs.getString("TELEFONO") == null) ? "" : rs.getString("TELEFONO");
				objeto_contrato = (rs.getString("OBJETO_CONTRATO") == null) ? "" : rs.getString("OBJETO_CONTRATO");
				comentarios = (rs.getString("COMENTARIOS") == null) ? "" : rs.getString("COMENTARIOS");
				monto_credito = (rs.getString("MONTO_CREDITO") == null) ? "" : rs.getString("MONTO_CREDITO");
				referencia_credito = (rs.getString("REFERENCIA_CREDITO") == null) ? "" : rs.getString("REFERENCIA_CREDITO");
				fecha_vencimiento = (rs.getString("FECHA_VENCIMIENTO") == null) ? "" : rs.getString("FECHA_VENCIMIENTO");
				banco_deposito = (rs.getString("BANCO_DEPOSITO") == null) ? "" : rs.getString("BANCO_DEPOSITO");
				cuenta = (rs.getString("CUENTA") == null) ? "" : rs.getString("CUENTA");
				cuenta_cable = (rs.getString("CUENTA_CLABE") == null) ? "" : rs.getString("CUENTA_CLABE");
				estatus = (rs.getString("ESTATUS") == null) ? "" : rs.getString("ESTATUS");
				fecha_firma_1 = (rs.getString("FECHA_FIRMA_1") == null) ? "" : rs.getString("FECHA_FIRMA_1");
				fecha_firma_2 = (rs.getString("FECHA_FIRMA_2") == null) ? "" : rs.getString("FECHA_FIRMA_2");				
				clave_solicitud = (rs.getString("CLAVE_SOLICITUD") == null) ? "" : rs.getString("CLAVE_SOLICITUD");
				clave_pyme = (rs.getString("CLAVE_PYME") == null) ? "" : rs.getString("CLAVE_PYME");
				firmaContrato = (rs.getString("FIRMA_CONTRATO") == null) ? "" : rs.getString("FIRMA_CONTRATO");
				causasRetorno = (rs.getString("CAUSAS_RETORNO") == null) ? "" : rs.getString("CAUSAS_RETORNO");
				if(causasRetorno.equals("")){
					causasRetorno="N/A";
				}
				StringBuffer montosMonedaSol = cesionBean.getMontoMoneda(clave_solicitud);
				monto_moneda  =montosMonedaSol.toString();
				
				//Se obtiene el representante legal de la Pyme
				StringBuffer nombreContacto = new StringBuffer();
				UtilUsr utilUsr = new UtilUsr();
				boolean usuarioEncontrado = false;
				boolean perfilIndicado = false;
				List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(clave_pyme, "P");
				for(int i=0;i<usuariosPorPerfil.size();i++){
					String loginUsuarioEpo = (String)usuariosPorPerfil.get(i);
					Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
					perfilIndicado = true;
					if(i>0){
						nombreContacto.append(" / ");
					}
					nombreContacto.append(usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno()+" ");
					nombreContacto.append("\n");
				}										
				representante = nombreContacto.toString();
				
				if(tipo.equals("PDF") ) {
					pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(dependencia,"formas",ComunesPDF.CENTER);
					//FODEA-024-2014 MOD()
					String pymeEmpresas ="";
					if(!"".equals(empresas)){
						pymeEmpresas = pyme_cedente+";"+empresas;
						pymeEmpresas = pymeEmpresas.replaceAll(";","\n");
					}else{
						pymeEmpresas = pyme_cedente;
					}
					pdfDoc.setCell(pymeEmpresas, "formas",ComunesPDF.LEFT);
					//
					pdfDoc.setCell(rfc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(proveedor,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(representante,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_solicitud,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(no_contrato,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(firmaContrato,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(monto_moneda.toString().replaceAll("<br/>", "\n").replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(tipo_contratacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_ini_contra,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_fin_contra,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(plazo_contrato,"formas",ComunesPDF.CENTER);		
					if(!clasificacionEpo.equals("")) {
					pdfDoc.setCell(clasificacion_epo,"formas",ComunesPDF.CENTER);
					}				
					pdfDoc.setCell(fecha_limite,"formas",ComunesPDF.CENTER);
					if(clasificacionEpo.equals("")) {
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
					}				
								
					pdfDoc.setCell("B","formas",ComunesPDF.CENTER); 
					pdfDoc.setCell(ventanilla_pago,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(supervisor,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(telefono,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(objeto_contrato,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(comentarios,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(monto_credito), 2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(referencia_credito,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_vencimiento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(banco_deposito,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cuenta,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cuenta_cable,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(estatus,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_firma_1,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_firma_2,"formas",ComunesPDF.CENTER);
					
					
					if(!indiceCamposAdicionales.equals("0")) {
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						int  nu = 5 - Integer.parseInt(indiceCamposAdicionales);
						int nuT =15-  Integer.parseInt(indiceCamposAdicionales);					
						pdfDoc.setCell("C","formas",ComunesPDF.CENTER); 
						if(!campo1.equals("")) { pdfDoc.setCell(campo01,"formas",ComunesPDF.CENTER); 	}
						if(!campo2.equals("")) { pdfDoc.setCell(campo02,"formas",ComunesPDF.CENTER); 	}
						if(!campo3.equals("")) { pdfDoc.setCell(campo03,"formas",ComunesPDF.CENTER); 	}
						if(!campo4.equals("")) { pdfDoc.setCell(campo04,"formas",ComunesPDF.CENTER); 	}
						if(!campo5.equals("")) { pdfDoc.setCell(campo5,"campo05",ComunesPDF.CENTER); 	}	
					
							pdfDoc.setCell(causasRetorno,"formas",ComunesPDF.CENTER);
							nuT = nuT-1;
					
						pdfDoc.setCell("","formas",ComunesPDF.CENTER, nuT);	
						
					}else{
					pdfDoc.setCell(causasRetorno,"formas",ComunesPDF.CENTER);							
					
					}
				}				
				
				if(tipo.equals("CSV") ) {
				//FODEA-024-2014 MOD()
					String pymeEmpresas ="";
					if(!"".equals(empresas)){
						pymeEmpresas = pyme_cedente+";"+empresas;
						pymeEmpresas = pymeEmpresas.replaceAll(","," ");
					}else{
						pymeEmpresas = pyme_cedente.replaceAll(","," ");
					}			
				contenidoArchivo.append(dependencia.replaceAll(",","")+","+
					pymeEmpresas.replaceAll(",","")+","+
					rfc.replaceAll(",","")+","+
					proveedor.replaceAll(",","")+","+
					representante.toString().replaceAll("\n", "").replaceAll(",", "")+","+
					fecha_solicitud+","+
					no_contrato+","+
					firmaContrato+","+
					monto_moneda.toString().replaceAll("<br/>", "|").replaceAll(",", "")+","+
					tipo_contratacion.replaceAll(",","")+","+
					fecha_ini_contra+","+
					fecha_fin_contra.replaceAll(",","")+","+
					plazo_contrato.replaceAll(",","")+",");
					
					if(!clasificacionEpo.equals("")) {
					contenidoArchivo.append(clasificacion_epo.replaceAll(",","")+",");				
					}				
					contenidoArchivo.append(fecha_limite+",");
														
					if(!indiceCamposAdicionales.equals("0")) {	
						if(!campo1.equals("")) { contenidoArchivo.append("'"+campo01.replaceAll(",","")+","); 	}
						if(!campo2.equals("")) { contenidoArchivo.append("'"+campo02.replaceAll(",","")+","); 	}
						if(!campo3.equals("")) { contenidoArchivo.append(campo03.replaceAll(",","")+","); 	}
						if(!campo4.equals("")) { contenidoArchivo.append(campo04.replaceAll(",","")+","); 	}
						if(!campo5.equals("")) { contenidoArchivo.append(campo05.replaceAll(",","")+","); 	}					
											
					}	
					
					contenidoArchivo.append(ventanilla_pago.replaceAll(",","")+","+
					supervisor.replaceAll(",","")+","+
					telefono.replaceAll(",","")+","+
					objeto_contrato.replaceAll(",","")+","+
					comentarios.replaceAll(",","")+","+
					"\"$"+Comunes.formatoDecimal(monto_credito,2,false)+"\","+
					referencia_credito.replaceAll(",","")+","+
					fecha_vencimiento.replaceAll(",","")+","+
					banco_deposito.replaceAll(",","")+","+
					"'"+cuenta.replaceAll(",","")+","+
					"'"+cuenta_cable.replaceAll(",","")+","+
					estatus.replaceAll(",","")+","+
					fecha_firma_1.replaceAll(",","")+","+
					fecha_firma_2.replaceAll(",","")+", "+
					causasRetorno.replaceAll(",","")+",");					
					
					contenidoArchivo.append("\n");			
				}
				
			}//while (rs.next())
			
			
			
			if(tipo.equals("PDF") ) {
				pdfDoc.addTable();
				pdfDoc.endDocument();	
			}
			if(tipo.equals("CSV") ) {
				creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
				nombreArchivo = creaArchivo.getNombre();
			}
		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
		
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getClave_pyme() {
		return clave_pyme;
	}

	public void setClave_pyme(String clave_pyme) {
		this.clave_pyme = clave_pyme;
	}

	public String getClave_epo() {
		return clave_epo;
	}

	public void setClave_epo(String clave_epo) {
		this.clave_epo = clave_epo;
	}

	public String getNumero_contrato() {
		return numero_contrato;
	}

	public void setNumero_contrato(String numero_contrato) {
		this.numero_contrato = numero_contrato;
	}

	public String getClave_moneda() {
		return clave_moneda;
	}

	public void setClave_moneda(String clave_moneda) {
		this.clave_moneda = clave_moneda;
	}

	public String getClave_estatus_sol() {
		return clave_estatus_sol;
	}

	public void setClave_estatus_sol(String clave_estatus_sol) {
		this.clave_estatus_sol = clave_estatus_sol;
	}

	public String getClave_contratacion() {
		return clave_contratacion;
	}

	public void setClave_contratacion(String clave_contratacion) {
		this.clave_contratacion = clave_contratacion;
	}

	public String getFecha_vigencia_ini() {
		return fecha_vigencia_ini;
	}

	public void setFecha_vigencia_ini(String fecha_vigencia_ini) {
		this.fecha_vigencia_ini = fecha_vigencia_ini;
	}

	public String getFecha_vigencia_fin() {
		return fecha_vigencia_fin;
	}

	public void setFecha_vigencia_fin(String fecha_vigencia_fin) {
		this.fecha_vigencia_fin = fecha_vigencia_fin;
	}

	public String getPlazoContrato() {
		return plazoContrato;
	}

	public void setPlazoContrato(String plazoContrato) {
		this.plazoContrato = plazoContrato;
	}

	public String getClaveTipoPlazo() {
		return claveTipoPlazo;
	}

	public void setClaveTipoPlazo(String claveTipoPlazo) {
		this.claveTipoPlazo = claveTipoPlazo;
	}

	public String getClave_if() {
		return clave_if;
	}

	public void setClave_if(String clave_if) {
		this.clave_if = clave_if;
	}

	public String getClasificacionEpo() {
		return clasificacionEpo;
	}

	public void setClasificacionEpo(String clasificacionEpo) {
		this.clasificacionEpo = clasificacionEpo;
	}

	public String getIndiceCamposAdicionales() {
		return indiceCamposAdicionales;
	}

	public void setIndiceCamposAdicionales(String indiceCamposAdicionales) {
		this.indiceCamposAdicionales = indiceCamposAdicionales;
	}

	public String getCampo1() {
		return campo1;
	}

	public void setCampo1(String campo1) {
		this.campo1 = campo1;
	}

	public String getCampo2() {
		return campo2;
	}

	public void setCampo2(String campo2) {
		this.campo2 = campo2;
	}

	public String getCampo3() {
		return campo3;
	}

	public void setCampo3(String campo3) {
		this.campo3 = campo3;
	}

	public String getCampo4() {
		return campo4;
	}

	public void setCampo4(String campo4) {
		this.campo4 = campo4;
	}

	public String getCampo5() {
		return campo5;
	}

	public void setCampo5(String campo5) {
		this.campo5 = campo5;
	}


	public void setAutIfOperativo(String autIfOperativo) {
		this.autIfOperativo = autIfOperativo;
	}


	public String getAutIfOperativo() {
		return autIfOperativo;
	}




	

}