package com.netro.cesion;

import java.util.HashMap;
import netropology.utilerias.*;
import org.apache.commons.logging.Log;
import java.util.ArrayList;
import java.util.List;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.*;

public class CapturaCedenteRepLegal implements IQueryGeneratorRegExtJS{

	private final static Log log = ServiceLocator.getInstance().getLog(CapturaCedenteRepLegal.class);
	private List conditions;
	private List representantes;
	private String icPyme;
	private String icSolicitud;
	private String mensaje;
	private String nombrePyme;

	public CapturaCedenteRepLegal(){}

	/**
	 * Obtiene las llaves primarias
	 * @return sentencia sql
	 */
	public String getDocumentQuery(){
		return null;
	}

	/**
	 * Obtiene la lista de los Ids en turno para realizar la paginacion
	 * @return sentencia sql
	 * @param ids
	 */
	public String getDocumentSummaryQueryForIds(List ids){
		return null;
	}

	/**
	 * Obtiene los totales
	 * @return sentencia sql
	 */
	public String getAggregateCalculationQuery(){
		return null;
	}

	/**
	 * Obtiene la consulta para generar el archivo PDF o CSV sin utilizar paginaci�n
	 * @return sentencia sql
	 */
	public String getDocumentQueryFile(){
		log.info("getDocumentQueryFile(E)");

		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();

		qrySentencia.append("SELECT IC_SOLICITUD, "                                   );
		qrySentencia.append("IC_PYME, "                                               );
		qrySentencia.append("IC_USUARIO, "                                            );
		qrySentencia.append("CG_TIPO_REPRESENTANTE, "                                 );
		qrySentencia.append("CG_NOMBRE_LICENCIADO, "                                  );
		qrySentencia.append("TO_CHAR(CG_ESCRITURA_PUBLICA) AS ESCRITURA_PUBLICA, "    );
		qrySentencia.append("TO_CHAR(DF_ESCRITURA_PUBLICA,'DD/MM/YYYY') "             );
		qrySentencia.append("AS FECHA_ESCRITURA_PUBLICA, "                            );
		qrySentencia.append("TO_CHAR(IN_NUM_NOTARIO_PUBLICO) AS NUM_NOTARIO_PUBLICO, ");
		qrySentencia.append("CG_ORIGEN_NOTARIO, "                                     );
		qrySentencia.append("'' as NOMBRE_REPRESENTANTE "                             );
		qrySentencia.append("FROM CDER_SOLIC_X_REPRESENTANTE "                        );
		qrySentencia.append("WHERE IC_SOLICITUD = ? "                                 );
		qrySentencia.append("AND IC_PYME = ?"                                         );

		// Se supone que estos campos son obligatorios, asi que nunca deben venir vacios
		conditions.add(icSolicitud);
		conditions.add(icPyme);

		log.debug("Sentencia: " + qrySentencia.toString());
		log.debug("Condiciones: " + conditions.toString());
		log.info("getDocumentQueryFile(S)");

		return qrySentencia.toString();
	}

	/**
	 * Crea el archivo PDF o CSV seg�n el tipo que se le pasa como par�metro.
	 * Obtiene los datos de la consulta generada en el m�todo getDocumentQueryFile.
	 * @return nombre del archivo
	 * @param tipo
	 * @param path
	 * @param rsAPI
	 * @param request
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){

		log.info(" crearCustomFile(E)");

		String nombreArchivo = "";

		netropology.utilerias.usuarios.Usuario usuario = null;
		netropology.utilerias.usuarios.UtilUsr utilUsr = new netropology.utilerias.usuarios.UtilUsr();

		HttpSession session       = request.getSession();
		CreaArchivo creaArchivo   = new CreaArchivo();
		OutputStreamWriter writer = null;
		BufferedWriter buffer     = null;

		StringBuffer contenidoArchivo = new StringBuffer();

		String nombreRepresentante = "";
		String tipoRepresentante   = "";
		String nombreLicenciado    = "";
		String dfEscrituraPublica  = "";
		String origenNotario       = "";
		String usuarioTmp          = "";

		int escrituraPublica = 0;
		int notarioPublico   = 0;
		int total            = 0;

		try {

			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);

			contenidoArchivo.append("PYME: " + nombrePyme.replace(',',' ')+ "\n");

			contenidoArchivo.append("Nombre del Representante,");
			contenidoArchivo.append("Tipo de Representante,"   );
			contenidoArchivo.append("Nombre Licenciado,"       );
			contenidoArchivo.append("Escritura P�blica,"       );
			contenidoArchivo.append("Fecha Escritura P�blica," );
			contenidoArchivo.append("No. Notario P�blico,"     );
			contenidoArchivo.append("Cd. Origen del Notario \n");

			while (rs.next()){
				usuarioTmp          = (rs.getString("IC_USUARIO")              == null) ? "" : rs.getString("IC_USUARIO");
				tipoRepresentante   = (rs.getString("CG_TIPO_REPRESENTANTE")   == null) ? "" : rs.getString("CG_TIPO_REPRESENTANTE");
				nombreLicenciado    = (rs.getString("CG_NOMBRE_LICENCIADO")    == null) ? "" : rs.getString("CG_NOMBRE_LICENCIADO");
				dfEscrituraPublica  = (rs.getString("FECHA_ESCRITURA_PUBLICA") == null) ? "" : rs.getString("FECHA_ESCRITURA_PUBLICA");
				origenNotario       = (rs.getString("CG_ORIGEN_NOTARIO")       == null) ? "" : rs.getString("CG_ORIGEN_NOTARIO");

				escrituraPublica = (rs.getInt("ESCRITURA_PUBLICA")   == 0 ) ? 0 : rs.getInt("ESCRITURA_PUBLICA");
				notarioPublico   = (rs.getInt("NUM_NOTARIO_PUBLICO") == 0 ) ? 0 : rs.getInt("NUM_NOTARIO_PUBLICO");

				

				// Obtengo el nombre del representante legal
				if(!usuarioTmp.equals("")){
					usuario = utilUsr.getUsuario(usuarioTmp);
					nombreRepresentante = usuario.getNombre() + " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno();
				} else{
					nombreRepresentante = "";
				}

				//Edito el campo tipo de representante:  L = Representante Legal, A = Apoderado
				if(tipoRepresentante.equals("A")){
					tipoRepresentante = "Apoderado";
				} else if(tipoRepresentante.equals("L")){
					tipoRepresentante = "Representante Legal";
				}

				contenidoArchivo.append(nombreRepresentante.replace(',',' ')+", ");
				contenidoArchivo.append(tipoRepresentante.replace(',',' ')+", "  );
				contenidoArchivo.append(nombreLicenciado.replace(',',' ')+", "   );
				if(escrituraPublica == 0){
					contenidoArchivo.append(" , ");
				} else{
					contenidoArchivo.append(escrituraPublica +", ");
				}
				contenidoArchivo.append(dfEscrituraPublica+", ");
				if(notarioPublico == 0){
					contenidoArchivo.append(" , ");
				} else{
					contenidoArchivo.append(notarioPublico +", ");
				}
				contenidoArchivo.append(origenNotario.replace(',',' ')+"\n");

				total++;
				if(total==1000){
					total=0;
					buffer.write(contenidoArchivo.toString());
					contenidoArchivo = new StringBuffer();//Limpio
				}
			}

			buffer.write(contenidoArchivo.toString());
			buffer.close();
			contenidoArchivo = new StringBuffer();//Limpio

		} catch(Throwable e){
			throw new AppException("Error al generar el archivo csv ", e);
		} finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}

		log.info(" crearCustomFile(E)");
		return nombreArchivo;
	}

	/**
	 * Crea el archivo PDF utilizando paginaci�n
	 * @return 
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){
		return null;
	}

/************************************************************
 *               M�TODOS DEL FODEA 23-2015                  *
 ************************************************************/
	public boolean updateRepresentanteLegal(){
		log.info("updateRepresentanteLegal(E)");
		boolean success = true;

		StringBuffer query   = new StringBuffer();
		AccesoDB con         = new AccesoDB();
		PreparedStatement ps = null;
		int regActualizados  = 0;

		HashMap mapRepresentante = new HashMap();
		String tipoRepresentante = "";
		String nombreLicenciado  = "";
		String escrituraPublica  = "";
		String fechaEscrituraPub = "";
		String numeroNotatio     = "";
		String origenNotario     = "";
		String solicitud         = "";
		String pyme              = "";
		String usuario           = "";

		query.append("UPDATE CDER_SOLIC_X_REPRESENTANTE SET "          );
		query.append("CG_TIPO_REPRESENTANTE = ?, "                     );
		query.append("CG_NOMBRE_LICENCIADO = ?, "                      );
		query.append("CG_ESCRITURA_PUBLICA = ?, "                      );
		query.append("DF_ESCRITURA_PUBLICA = TO_DATE(?,'DD/MM/YYYY'), ");
		query.append("IN_NUM_NOTARIO_PUBLICO = ?, "                    );
		query.append("CG_ORIGEN_NOTARIO = ? "                          );
		query.append("WHERE IC_SOLICITUD = ? "                         );
		query.append("AND IC_PYME = ? "                                );
		query.append("AND IC_USUARIO = ?"                              );

		log.debug("Sentencia: " + query.toString());

		try{

			if(representantes.size() > 0){

				con.conexionDB();
				ps = con.queryPrecompilado(query.toString());

				for(int i= 0; i < representantes.size(); i++){
					mapRepresentante = new HashMap();
					mapRepresentante = (HashMap) representantes.get(i);
					tipoRepresentante = (String)mapRepresentante.get("CG_TIPO_REPRESENTANTE");
					nombreLicenciado  = (String)mapRepresentante.get("CG_NOMBRE_LICENCIADO");
					escrituraPublica  = (String)mapRepresentante.get("ESCRITURA_PUBLICA");
					fechaEscrituraPub = (String)mapRepresentante.get("FECHA_ESCRITURA_PUBLICA");
					numeroNotatio     = (String)mapRepresentante.get("NUM_NOTARIO_PUBLICO");
					origenNotario     = (String)mapRepresentante.get("CG_ORIGEN_NOTARIO");
					solicitud         = (String)mapRepresentante.get("IC_SOLICITUD");
					pyme              = (String)mapRepresentante.get("IC_PYME");
					usuario           = (String)mapRepresentante.get("IC_USUARIO");

					// Edito la fecha
					if(fechaEscrituraPub.length() > 10){
						fechaEscrituraPub = fechaEscrituraPub.substring(8,10) + "/" + fechaEscrituraPub.substring(5,7) + "/" + fechaEscrituraPub.substring(0,4);
					}
					// Edito el tipo de representante
					if(tipoRepresentante.equals("Apoderado")){
						tipoRepresentante = "A";
					} else if(tipoRepresentante.equals("Representante Legal")){
						tipoRepresentante = "L";
					}

					log.debug("tipoRepresentante: <" + tipoRepresentante + ">");
					log.debug("nombreLicenciado: <" + nombreLicenciado + ">");
					log.debug("escrituraPublica: <" + escrituraPublica + ">");
					log.debug("fechaEscrituraPub: <" + fechaEscrituraPub + ">");
					log.debug("numeroNotatio: <" + numeroNotatio + ">");
					log.debug("origenNotario: <" + origenNotario + ">");
					log.debug("solicitud: <" + solicitud + ">");
					log.debug("pyme: <" + pyme + ">");
					log.debug("usuario: <" + usuario + ">");

					ps.clearParameters();
					ps.setString(1, tipoRepresentante);
					ps.setString(2, nombreLicenciado);
					ps.setInt(   3, Integer.parseInt(escrituraPublica));
					ps.setString(4, fechaEscrituraPub);
					ps.setInt(   5, Integer.parseInt(numeroNotatio));
					ps.setString(6, origenNotario);
					ps.setInt(   7, Integer.parseInt(solicitud));
					ps.setInt(   8, Integer.parseInt(pyme));
					ps.setInt(   9, Integer.parseInt(usuario));
					regActualizados = ps.executeUpdate();
					log.info("Se actualizaron " + regActualizados + " registros en la tabla CDER_SOLIC_X_REPRESENTANTE");

				}

			}

		}catch(Exception e){
			success = false;
			mensaje = "Error al actualizar los datos: " + e;
			log.warn("updateRepresentanteLegal.Exception. " + e);
		}finally{
			try{
				if(ps!=null)
					ps.close();
			}catch(SQLException ex){
				log.warn(ex);
			}
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(success);
				con.cierraConexionDB();
			}
		}
		log.info("updateRepresentanteLegal(S)");
		return success;
	}

/*****************************************************
 *                GETTERS AND SETTERS                *
 *****************************************************/
 	public List getConditions(){
		return conditions;
	}

 	public List getRepresentantes(){
		return representantes;
	}

	public void setRepresentantes(List representantes) {
		this.representantes = representantes;
	}

	public String getIcPyme() {
		return icPyme;
	}

	public void setIcPyme(String icPyme) {
		this.icPyme = icPyme;
	}

	public String getIcSolicitud() {
		return icSolicitud;
	}

	public void setIcSolicitud(String icSolicitud) {
		this.icSolicitud = icSolicitud;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getNombrePyme() {
		return nombrePyme;
	}

	public void setNombrePyme(String nombrePyme) {
		this.nombrePyme = nombrePyme;
	}

}