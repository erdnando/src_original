package com.netro.cesion;

import java.util.HashMap;
import netropology.utilerias.*;
import org.apache.commons.logging.Log;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import java.sql.*;

public class CapturaCedente implements IQueryGeneratorRegExtJS{

	private final static Log log = ServiceLocator.getInstance().getLog(CapturaCedente.class);
	private List conditions;
	private String icEpo;
	private String noContrato;
	private String icPyme;
	private String icSolicitud;
	private String tipoCedente;
	private String figuraJuridica;
	private String escrituraPublica;
	private String dfEscrituraPublica;
	private String nombreLicenciado;
	private String numNotarioPublico;
	private String origenNotario;
	private String numFolioMercantil;
	private String regPubComercio;
	private String domicilioLegal;
	private String estatus;
	private String mensaje;

	public CapturaCedente(){}

	/**
	 * Obtiene las llaves primarias
	 * @return sentencia sql
	 */
	public String getDocumentQuery(){
		return null;
	}

	/**
	 * Obtiene la lista de los Ids en turno para realizar la paginacion
	 * @return sentencia sql
	 * @param ids
	 */
	public String getDocumentSummaryQueryForIds(List ids){
		return null;
	}

	/**
	 * Obtiene los totales
	 * @return sentencia sql
	 */
	public String getAggregateCalculationQuery(){
		return null;
	}

	/**
	 * Obtiene la consulta para generar el archivo PDF o CSV sin utilizar paginaci�n
	 * @return sentencia sql
	 */
	public String getDocumentQueryFile(){
		log.info("getDocumentQueryFile(E)");

		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();

		if(!icEpo.equals("")){
			condicion.append("AND D.IC_EPO = ? ");
			conditions.add(icEpo);
		}
		if(!noContrato.equals("")){
			condicion.append("AND D.CG_NO_CONTRATO = ? ");
			conditions.add(noContrato);
		}
	
		qrySentencia.append("SELECT D.IC_SOLICITUD, "                                      );
		qrySentencia.append("D.CG_NO_CONTRATO, "                                           );
		qrySentencia.append("P.IC_PYME, "                                                  );
		qrySentencia.append("P.CG_RAZON_SOCIAL AS NOMBRE_PYME, "                           );
		qrySentencia.append("P.CG_RFC AS RFC, "                                            );
		qrySentencia.append("SUM(DECODE(R.CG_TIPO_REPRESENTANTE,NULL,1, 0)) AS REP_LEGAL, ");
		qrySentencia.append("(SELECT N.CG_TIPO_CEDENTE "                                   );
		qrySentencia.append("FROM CDER_SOLIC_PYME_NOTARIALES N "                           );
		qrySentencia.append("WHERE P.IC_PYME = N.IC_PYME "                                 );
		qrySentencia.append("AND N.IC_SOLICITUD = D.IC_SOLICITUD) AS NOTARIO, "            );
		qrySentencia.append("CASE WHEN  D.IC_ESTATUS_SOLIC IN(1, 2, 3, 13) THEN 'S' "      );
		qrySentencia.append("ELSE 'N' END AS EDITABLE "                                    );
		qrySentencia.append("FROM CDER_SOLICITUD D, "                                      );
		qrySentencia.append("CDER_SOLIC_X_REPRESENTANTE R, "                               );
		qrySentencia.append("COMCAT_PYME P "                                               );
		qrySentencia.append("WHERE D.IC_SOLICITUD = R.IC_SOLICITUD "                       );
		qrySentencia.append("AND R.IC_PYME = P.IC_PYME "                                   );
		qrySentencia.append("AND D.IC_ESTATUS_SOLIC NOT IN (25) "                          );
		qrySentencia.append(condicion.toString()                                           );
		qrySentencia.append("GROUP BY D.IC_SOLICITUD, D.CG_NO_CONTRATO, P.IC_PYME, "       );
		qrySentencia.append("P.CG_RAZON_SOCIAL,  P.CG_RFC,D.IC_ESTATUS_SOLIC "             );
		qrySentencia.append("ORDER BY P.IC_PYME"                                           );

		log.debug("Sentencia: " + qrySentencia.toString());
		log.debug("Condiciones: " + conditions.toString());
		log.info("getDocumentQueryFile(S)");

		return qrySentencia.toString();
	}

	/**
	 * Crea el archivo PDF o CSV seg�n el tipo que se le pasa como par�metro.
	 * Obtiene los datos de la consulta generada en el m�todo getDocumentQueryFile.
	 * @return nombre del archivo
	 * @param tipo
	 * @param path
	 * @param rsAPI
	 * @param request
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rsAPI, String path, String tipo){
		return null;
	}

	/**
	 * Crea el archivo PDF utilizando paginaci�n
	 * @return 
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){
		return null;
	}

/************************************************************
 *               M�TODOS DEL FODEA 23-2015                  *
 ************************************************************/
	/**
	 * Obtiene los datos constitutivos � CEDENTE.
	 * Los par�metros los obtiene de los m�todos getter y setter.
	 * @return HashMap con los datos de la pyme
	 */
	public HashMap getCderSolicPymeNotariales(){

		log.info("getCderSolicPymeNotariales(E)");

		HashMap datos        = new HashMap();
		StringBuffer query   = new StringBuffer();
		AccesoDB con         = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs         = null;

		query.append("SELECT IC_SOLICITUD, "                       );
		query.append("IC_PYME, "                                   );
		query.append("CG_TIPO_CEDENTE, "                           );
		query.append("CG_FIGURA_JURIDICA, "                        );
		query.append("CG_ESCRITURA_PUBLICA, "                      );
		query.append("TO_CHAR(DF_ESCRITURA_PUBLICA, 'DD/MM/YYYY') ");
		query.append("AS FECHA_ESCRITURA, "                        );
		query.append("CG_NOMBRE_LICENCIADO, "                      );
		query.append("IN_NUM_NOTARIO_PUBLICO, "                    );
		query.append("CG_ORIGEN_NOTARIO, "                         );
		query.append("IN_NUM_FOLIO_MERCANTIL, "                    );
		query.append("CG_REG_PUB_COMERCIO, "                       );
		query.append("CG_DOMICILIO_LEGAL "                         );
		query.append("FROM CDER_SOLIC_PYME_NOTARIALES "            );
		query.append("WHERE IC_PYME = ?"                           );
		query.append("AND IC_SOLICITUD = ?"                        );

		log.debug("Sentencia: " + query.toString());
		log.debug("ic_pyme: " + icPyme);
		log.debug("ic_solicitud: " + icSolicitud);

		try{

			con.conexionDB();
			ps = con.queryPrecompilado(query.toString());
			ps.clearParameters();
			ps.setInt(1, Integer.parseInt(icPyme));
			ps.setInt(2, Integer.parseInt(icSolicitud));
			rs = ps.executeQuery();

			if(rs.next()){
				datos.put("ic_solicitud",              ""+rs.getInt("IC_SOLICITUD")          );
				datos.put("ic_pyme",                   ""+rs.getInt("IC_PYME")               );
				datos.put("tipo_cedente",              rs.getString("CG_TIPO_CEDENTE")       );
				datos.put("figura_juridica",           rs.getString("CG_FIGURA_JURIDICA")    );
				datos.put("escritura_publica",         ""+rs.getInt("CG_ESCRITURA_PUBLICA")  );
				datos.put("fecha_escritura",           rs.getString("FECHA_ESCRITURA")       );
				datos.put("nombre_licenciado",         rs.getString("CG_NOMBRE_LICENCIADO")  );
				datos.put("no_notario_publico",        ""+rs.getInt("IN_NUM_NOTARIO_PUBLICO"));
				datos.put("ciudad_origen",             rs.getString("CG_ORIGEN_NOTARIO")     );
				datos.put("registro_publico_comercio", rs.getString("CG_REG_PUB_COMERCIO")   );
				datos.put("folio_mercantil",           ""+rs.getInt("IN_NUM_FOLIO_MERCANTIL"));
				datos.put("domicilio_empresa",         rs.getString("CG_DOMICILIO_LEGAL")    );
			} else{
				datos = null;
			}

		}catch(Exception e){
			datos = null;
			log.warn("getCderSolicPymeNotariales.Exception. " + e);
		}finally{
			try{
				if(rs!=null)
					rs.close();
				if(ps!=null)
					ps.close();
			}catch(SQLException ex){
				log.warn(ex);
			}
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
		}

		log.info("getCderSolicPymeNotariales(S)");
		return datos;
	}

	/**
	 * Inserta un nuevo registro con los datos de la pyme CEDENTE
	 * @return true si se insert� correctamente, false en otro caso.
	 */
	public boolean insertCderSolicPymeNotariales(){
		log.info("insertCderSolicPymeNotariales(E)");
		boolean success = true;

		StringBuffer query   = new StringBuffer();
		AccesoDB con         = new AccesoDB();
		PreparedStatement ps = null;
		int regActualizados  = 0;

		query.append("INSERT INTO CDER_SOLIC_PYME_NOTARIALES ("   );
		query.append("IC_SOLICITUD, "                             );
		query.append("IC_PYME, "                                  );
		query.append("CG_TIPO_CEDENTE, "                          );
		query.append("CG_FIGURA_JURIDICA, "                       );
		query.append("CG_ESCRITURA_PUBLICA, "                     );
		query.append("DF_ESCRITURA_PUBLICA, "                     );
		query.append("CG_NOMBRE_LICENCIADO, "                     );
		query.append("IN_NUM_NOTARIO_PUBLICO, "                   );
		query.append("CG_ORIGEN_NOTARIO, "                        );
		query.append("IN_NUM_FOLIO_MERCANTIL, "                   );
		query.append("CG_REG_PUB_COMERCIO, "                      );
		query.append("CG_DOMICILIO_LEGAL) "                       );
		query.append("VALUES(?, ?, ?, ?, ?, "                     );
		query.append("TO_DATE(?, 'DD/MM/YYYY'), ?, ?, ?, ?, ?, ?)");

		log.debug("Sentencia: " + query.toString());

		try{

			con.conexionDB();
			ps = con.queryPrecompilado(query.toString());
			ps.clearParameters();
			ps.setInt(    1, Integer.parseInt(icSolicitud));
			ps.setInt(    2, Integer.parseInt(icPyme));
			ps.setString( 3, tipoCedente);
			ps.setString( 4, figuraJuridica);
			ps.setInt(    5, Integer.parseInt(escrituraPublica));
			ps.setString( 6, dfEscrituraPublica);
			ps.setString( 7, nombreLicenciado);
			ps.setInt(    8, Integer.parseInt(numNotarioPublico));
			ps.setString( 9, origenNotario);
			ps.setInt(   10, Integer.parseInt(numFolioMercantil));
			ps.setString(11, regPubComercio);
			ps.setString(12, domicilioLegal);
			regActualizados = ps.executeUpdate();

			log.info("Se insertaron " + regActualizados + " registros en la tabla CDER_SOLIC_PYME_NOTARIALES");

		}catch(Exception e){
			success = false;
			mensaje = "Error al insertar los datos: " + e;
			log.warn("insertCderSolicPymeNotariales.Exception " + e);
		}finally{
			try{
				if(ps!=null)
					ps.close();
			}catch(SQLException ex){
				log.warn(ex);
			}
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(success);
				con.cierraConexionDB();
			}
		}
		log.info("insertCderSolicPymeNotariales(S)");
		return success;
	}

	/**
	 * Actualiza un registro con los datos de la pyme CEDENTE
	 * @return true si se actualiz� correctamente, false en otro caso.
	 */
	public boolean updateCderSolicPymeNotariales(){
		log.info("updateCderSolicPymeNotariales(E)");
		boolean success = true;

		StringBuffer query   = new StringBuffer();
		AccesoDB con         = new AccesoDB();
		PreparedStatement ps = null;
		int regActualizados  = 0;

		query.append("UPDATE CDER_SOLIC_PYME_NOTARIALES SET "          );
		query.append("CG_TIPO_CEDENTE = ?, "                           );
		query.append("CG_FIGURA_JURIDICA = ?, "                        );
		query.append("CG_ESCRITURA_PUBLICA = ?, "                      );
		query.append("DF_ESCRITURA_PUBLICA = TO_DATE(?,'DD/MM/YYYY'), ");
		query.append("CG_NOMBRE_LICENCIADO = ?, "                      );
		query.append("IN_NUM_NOTARIO_PUBLICO = ?, "                    );
		query.append("CG_ORIGEN_NOTARIO = ?, "                         );
		query.append("IN_NUM_FOLIO_MERCANTIL = ?, "                    );
		query.append("CG_REG_PUB_COMERCIO = ?, "                       );
		query.append("CG_DOMICILIO_LEGAL = ? "                         );
		query.append("WHERE IC_SOLICITUD = ? AND IC_PYME = ?"          );

		log.debug("Sentencia: " + query.toString());

		try{

			con.conexionDB();
			ps = con.queryPrecompilado(query.toString());
			ps.clearParameters();
			ps.setString( 1, tipoCedente);
			ps.setString( 2, figuraJuridica);
			ps.setInt(    3, Integer.parseInt(escrituraPublica));
			ps.setString( 4, dfEscrituraPublica);
			ps.setString( 5, nombreLicenciado);
			ps.setInt(    6, Integer.parseInt(numNotarioPublico));
			ps.setString( 7, origenNotario);
			ps.setInt(    8, Integer.parseInt(numFolioMercantil));
			ps.setString( 9, regPubComercio);
			ps.setString(10, domicilioLegal);
			ps.setInt(   11, Integer.parseInt(icSolicitud));
			ps.setInt(   12, Integer.parseInt(icPyme));
			regActualizados = ps.executeUpdate();

			log.info("Se actualizaron " + regActualizados + " registros en la tabla CDER_SOLIC_PYME_NOTARIALES");

		}catch(Exception e){
			success = false;
			mensaje = "Error al actualizar los datos: " + e;
			log.warn("updateCderSolicPymeNotariales.Exception. " + e);
		}finally{
			try{
				if(ps!=null)
					ps.close();
			}catch(SQLException ex){
				log.warn(ex);
			}
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(success);
				con.cierraConexionDB();
			}
		}
		log.info("updateCderSolicPymeNotariales(S)");
		return success;
	}

/*****************************************************
 *                GETTERS AND SETTERS                *
 *****************************************************/
 	public List getConditions(){
		return conditions;
	}

	public String getIcEpo() {
		return icEpo;
	}

	public void setIcEpo(String icEpo) {
		this.icEpo = icEpo;
	}

	public String getNoContrato() {
		return noContrato;
	}

	public void setNoContrato(String noContrato) {
		this.noContrato = noContrato;
	}

	public String getIcPyme() {
		return icPyme;
	}

	public void setIcPyme(String icPyme) {
		this.icPyme = icPyme;
	}

	public String getIcSolicitud() {
		return icSolicitud;
	}

	public void setIcSolicitud(String icSolicitud) {
		this.icSolicitud = icSolicitud;
	}

	public String getTipoCedente() {
		return tipoCedente;
	}

	public void setTipoCedente(String tipoCedente) {
		this.tipoCedente = tipoCedente;
	}

	public String getFiguraJuridica() {
		return figuraJuridica;
	}

	public void setFiguraJuridica(String figuraJuridica) {
		this.figuraJuridica = figuraJuridica;
	}

	public String getEscrituraPublica() {
		return escrituraPublica;
	}

	public void setEscrituraPublica(String escrituraPublica) {
		this.escrituraPublica = escrituraPublica;
	}

	public String getDfEscrituraPublica() {
		return dfEscrituraPublica;
	}

	public void setDfEscrituraPublica(String dfEscrituraPublica) {
		this.dfEscrituraPublica = dfEscrituraPublica;
	}

	public String getNombreLicenciado() {
		return nombreLicenciado;
	}

	public void setNombreLicenciado(String nombreLicenciado) {
		this.nombreLicenciado = nombreLicenciado;
	}

	public String getNumNotarioPublico() {
		return numNotarioPublico;
	}

	public void setNumNotarioPublico(String numNotarioPublico) {
		this.numNotarioPublico = numNotarioPublico;
	}

	public String getOrigenNotario() {
		return origenNotario;
	}

	public void setOrigenNotario(String origenNotario) {
		this.origenNotario = origenNotario;
	}

	public String getNumFolioMercantil() {
		return numFolioMercantil;
	}

	public void setNumFolioMercantil(String numFolioMercantil) {
		this.numFolioMercantil = numFolioMercantil;
	}

	public String getRegPubComercio() {
		return regPubComercio;
	}

	public void setRegPubComercio(String regPubComercio) {
		this.regPubComercio = regPubComercio;
	}

	public String getDomicilioLegal() {
		return domicilioLegal;
	}

	public void setDomicilioLegal(String domicilioLegal) {
		this.domicilioLegal = domicilioLegal;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

}