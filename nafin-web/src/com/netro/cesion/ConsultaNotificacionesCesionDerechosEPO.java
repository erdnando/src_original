package com.netro.cesion;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

public class ConsultaNotificacionesCesionDerechosEPO implements IQueryGeneratorRegExtJS {
	public ConsultaNotificacionesCesionDerechosEPO() {}
	
	private List conditions;
	StringBuffer strQuery;
	
	//Variable para enviar mensajes al log
	private static final Log log = ServiceLocator.getInstance().getLog(ConsultaNotificacionesCesionDerechosEPO.class);

	//Condiciones de la consulta
	private String loginUsuario;
	private String claveEpo;
	private String claveIf;
	private String clavePyme;
	private String claveEstatusSol;
	private String numeroContrato;
	private String fechaVigenciaIni;
	private String fechaVigenciaFin;
	private String fechaSolicitudIni;
	private String fechaSolicitudFin;
	private String plazoContrato;
	private String claveTipoPlazo;
	private List camposAdicionales;	
	private List tipoDato;
	public String getAggregateCalculationQuery(){
		return "";
	}
	public String getDocumentQuery(){
		conditions	= new ArrayList();
		strQuery		= new StringBuffer();
		return strQuery.toString();
	}
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions	= new ArrayList();
		strQuery		= new StringBuffer();
		return strQuery.toString();
	}
	public String getDocumentQueryFile(){
		conditions	= new ArrayList();
		strQuery		= new StringBuffer();
		
			strQuery.append(" SELECT sol.ic_solicitud AS clave_solicitud,");
			strQuery.append(" sol.ic_pyme AS clave_pyme,");
			strQuery.append(" sol.ic_epo AS clave_epo,");
			strQuery.append(" sol.ic_if AS clave_if,");
			strQuery.append("	'' AS monto_moneda,");
			strQuery.append(" '' AS representante_legal,");
			strQuery.append(" cif.cg_razon_social AS nombre_if,");
			strQuery.append(" pym.cg_razon_social||';'||sol.CG_EMPRESAS_REPRESENTADAS AS nombre_pyme,");//FODEA-024-2014 MOD()
			strQuery.append(" pym.cg_rfc AS rfc,");
			strQuery.append(" cpe.cg_pyme_epo_interno AS numero_proveedor,");
			strQuery.append(" TO_CHAR(sol.df_fecha_solicitud_ini, 'dd/mm/yyyy') AS fecha_sol_pyme,");
			strQuery.append(" sol.cg_no_contrato AS numero_contrato,");
			strQuery.append(" tcn.cg_nombre AS tipo_contratacion,");
			strQuery.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");
			strQuery.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");
			strQuery.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS plazo_contrato,");
			strQuery.append(" cep.cg_area AS clasificacion_epo,");
			strQuery.append(" sol.cg_campo1 AS campo_adicional_1,");
			strQuery.append(" sol.cg_campo2 AS campo_adicional_2,");
			strQuery.append(" sol.cg_campo3 AS campo_adicional_3,");
			strQuery.append(" sol.cg_campo4 AS campo_adicional_4,");
			strQuery.append(" sol.cg_campo5 AS campo_adicional_5,");
			strQuery.append(" cvp.cg_descripcion AS ventanilla_pago,");
			strQuery.append(" sol.cg_sup_adm_resob AS sup_adm_resob,");
			strQuery.append(" sol.cg_numero_telefono AS numero_telefono,");
			strQuery.append(" sol.cg_obj_contrato AS objeto_contrato,");
			strQuery.append(" sol.cg_comentarios AS comentarios,");
			strQuery.append(" sol.fn_monto_credito AS monto_credito,");
			strQuery.append(" sol.cg_referencia AS numero_referencia,");
			strQuery.append(" TO_CHAR(sol.df_vencimiento_credito, 'dd/mm/yyyy') AS fecha_vencimiento,");
			strQuery.append(" sol.cg_banco_deposito AS banco_deposito,");
			strQuery.append(" sol.cg_cuenta AS numero_cuenta,");
			strQuery.append(" sol.cg_cuenta_clabe AS clabe,");
			strQuery.append(" sol.ic_estatus_solic AS clave_estatus,");
			strQuery.append(" sts.cg_descripcion AS estatus_solicitud, ");
			strQuery.append(" sol.cc_acuse_redirec AS acuse_redirec,");
			strQuery.append(" sol.cc_acuse5 AS acuse5,");
			strQuery.append(" sol.cg_causas_rechazo_notif AS causas_rechazo_notif,");
			strQuery.append(" sol.CG_OBSERV_RECHAZO AS causasRechazo,");
			strQuery.append(" TO_CHAR(sol.df_firma_contrato, 'dd/mm/yyyy') AS firma_contrato"); 
			strQuery.append(" , sol.ic_grupo_cesion AS grupo_cesion "); 
			 
			strQuery.append(" FROM cder_solicitud sol");
			strQuery.append(", comcat_epo epo");
			strQuery.append(", comcat_if cif");
			strQuery.append(", comcat_pyme pym");
			strQuery.append(", comrel_pyme_epo cpe");
			strQuery.append(", cdercat_tipo_contratacion tcn");
			strQuery.append(", cdercat_clasificacion_epo cep");
			strQuery.append(", cdercat_estatus_solic sts");
			strQuery.append(", cdercat_tipo_plazo stp");
			strQuery.append(", cdercat_ventanilla_pago cvp");
			strQuery.append(" WHERE sol.ic_epo = epo.ic_epo");
			strQuery.append(" AND sol.ic_if = cif.ic_if");
			strQuery.append(" AND sol.ic_pyme = pym.ic_pyme");
			strQuery.append(" AND sol.ic_epo = cpe.ic_epo(+)");
			strQuery.append(" AND sol.ic_pyme = cpe.ic_pyme(+)");
			strQuery.append(" AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion");
			strQuery.append(" AND sol.ic_epo = cep.ic_epo");
			strQuery.append(" AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo");
			strQuery.append(" AND sol.ic_estatus_solic = sts.ic_estatus_solic");
			strQuery.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
			strQuery.append(" AND sol.ic_epo = cvp.ic_epo(+)");
			strQuery.append(" AND sol.ic_ventanilla_pago = cvp.ic_ventanilla_pago(+)");
			
			if (this.loginUsuario != null && !"".equals(this.loginUsuario)) {
				strQuery.append(" AND SUBSTR(cep.cg_responsable, 1, 8) = ?");
				conditions.add(this.loginUsuario);
			}

			if(!this.claveEpo.equals("")){
				strQuery.append(" AND sol.ic_epo = ?");
				conditions.add(new Integer(this.claveEpo));
			}
			if(!this.claveIf.equals("")){
				strQuery.append(" AND sol.ic_if = ?");
				conditions.add(new Integer(this.claveIf));
			}
			if(!this.clavePyme.equals("")){
				strQuery.append(" AND sol.ic_pyme = ?");
				conditions.add(new Long(this.clavePyme));
			}
			if(!this.claveEstatusSol.equals("")){
				strQuery.append(" AND sol.ic_estatus_solic = ?");
			    if(this.claveEstatusSol.equals("15"))
			        conditions.add(new Integer(11));
                else
                    conditions.add(new Integer(this.claveEstatusSol));
			}else{
				strQuery.append(" AND sol.ic_estatus_solic IN (?,?,?,?,?,?)");
				conditions.add(new Integer(9));
				conditions.add(new Integer(11));
				conditions.add(new Integer(12));
				conditions.add(new Integer(15));
				conditions.add(new Integer(16));
				conditions.add(new Integer(17));
			}
			if(!this.numeroContrato.equals("")){
				strQuery.append(" AND sol.cg_no_contrato = ?");
				conditions.add(this.numeroContrato);
			}
			if(!this.fechaVigenciaIni.equals("") && !this.fechaVigenciaFin.equals("")){
				strQuery.append(" AND sol.df_fecha_vigencia_ini >= TO_DATE(?, 'dd/mm/yyyy')");
				strQuery.append(" AND sol.df_fecha_vigencia_fin <= TO_DATE(?, 'dd/mm/yyyy')");
				conditions.add(this.fechaVigenciaIni);
				conditions.add(this.fechaVigenciaFin);
			}
			if(!this.fechaSolicitudIni.equals("") && !this.fechaSolicitudFin.equals("")){
				strQuery.append(" AND sol.df_fecha_solicitud_ini >= TO_DATE(?, 'dd/mm/yyyy')");
				strQuery.append(" AND sol.df_fecha_solicitud_ini <= TO_DATE(?, 'dd/mm/yyyy')");
				conditions.add(this.fechaSolicitudIni);
				conditions.add(this.fechaSolicitudFin);
			}
			/*if(!camposAdicionales.isEmpty()){
				for(int i = 0; i < camposAdicionales.size(); i++){
					String campoAdicional = (String)camposAdicionales.get(i);
					String tipo = (String)tipoDato.get(i);
					if(!campoAdicional.equals("")){
						strQuery.append(" AND sol.cg_campo"+ (i + 1) +" = ?");
						if(tipo.equals("N")){conditions.add(new Long(campoAdicional));}else{conditions.add(campoAdicional);}
					}
				}
			}*/
			
			if (this.plazoContrato != null && !"".equals(this.plazoContrato)) {
				strQuery.append(" AND sol.ig_plazo = ?");
				conditions.add(new Integer(this.plazoContrato));
			}		
	
			if (this.claveTipoPlazo != null && !"".equals(this.claveTipoPlazo)) {
				strQuery.append(" AND sol.ic_tipo_plazo = ?");
				conditions.add(new Integer(this.claveTipoPlazo));
			}
			
			strQuery.append(" ORDER BY sol.df_alta_solicitud DESC");
			
			
			log.debug("..:: strSQL : "+strQuery.toString());
			log.debug("..:: varBind : "+conditions.toString());
		
		return strQuery.toString();
	}
	/**
		* En este m�todo se debe realizar la implementaci�n de la generaci�n del archivo
		* con base en el resulset que se recibe como par�metro. El ResulSet es el resultado
		* de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
		* @param request HttpRequest empleado principalmente para obtener el objeto session
		* @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
		* @param path Ruta f�sica donde se generar� el archivo
		* @param tipo cadena que identifica el tipo o variante de archivo que va a generar.
		* @return Cadena con la ruta del archivo generado
	 */
	 public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
		String nombreArchivo = "";
		try{
			CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
			int cont = 1;
			
			if("PDF".equals(tipo)){
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual		= fechaActual.substring(0,2);
				String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
 				String anioActual		= fechaActual.substring(6,10);
				String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),
				(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + "del" + anioActual + "-----------------------------" +
				horaActual, "formas",ComunesPDF.RIGHT);
				
				HashMap camposAdicionales = cesionBean.getCamposAdicionalesParametrizados(this.claveEpo, "9");
				int indice = Integer.parseInt((String)camposAdicionales.get("indice"));
				//pdfDoc.setTable(23,100);
				float anchoCeldaA[] = {5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f, 5.88f};
				pdfDoc.setTable(19,100,anchoCeldaA);
				
				pdfDoc.setCell("A","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("Intermediario Financiero (Cesionario)","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Pyme (Cedente)","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("RFC","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No.Proveedor","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Representante legal","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de solicitud PYME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No.Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Firma Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto/Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de Contrataci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("F. Inicio Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("F. Final Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo del Contrato","celda01",ComunesPDF.CENTER);
				String clasificacionEpo = cesionBean.clasificacionEpo(this.claveEpo);
				pdfDoc.setCell(clasificacionEpo,"celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Ventanilla de Pago","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Supervisor/Administrador/Residente de Obra","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tel�fono","celda01",ComunesPDF.CENTER);
				
				pdfDoc.setCell("B", "celda02", ComunesPDF.CENTER);
				for(int i = 0; i<indice; i++){
					pdfDoc.setCell(camposAdicionales.get("nombre_campo_"+i) + "", "celda02", ComunesPDF.CENTER);
					cont+=1;
				}
				pdfDoc.setCell("Objeto del Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Comentarios","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto del Cr�dito","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("Referencia/No. Cr�dito","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Vencimiento Cr�dito","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("Banco de D�posito","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("Cuenta","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("Cuenta CLABE","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("Observaciones","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Causas de Rechazo de Notificaci�n","celda02",ComunesPDF.CENTER);
				for(int i = 0; i<=(5-cont); i++){
					pdfDoc.setCell("", "celda02", ComunesPDF.LEFT);
				}
				pdfDoc.setCell("", "celda02", ComunesPDF.LEFT);
				
				while (rs.next()){
					String NOMBRE_IF = (rs.getString("NOMBRE_IF") == null) ? "" : rs.getString("NOMBRE_IF");
					String NOMBRE_PYME = (rs.getString("NOMBRE_PYME") == null) ? "" : rs.getString("NOMBRE_PYME");
					String RFC = (rs.getString("RFC") == null) ? "" : rs.getString("RFC");
					String NUMERO_PROVEEDOR = (rs.getString("NUMERO_PROVEEDOR") == null) ? "" : rs.getString("NUMERO_PROVEEDOR");
					//Se obtiene el representante legal de la Pyme
					String noProveedor = (rs.getString("CLAVE_PYME")==null)?"":(rs.getString("CLAVE_PYME"));
					StringBuffer nombreContacto = new StringBuffer();
					UtilUsr utilUsr = new UtilUsr();
					boolean usuarioEncontrado = false;
					boolean perfilIndicado = false;
					List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(noProveedor, "P");
					for(int i=0;i<usuariosPorPerfil.size();i++){
						String loginUsuarioEpo = (String)usuariosPorPerfil.get(i);
						Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
						perfilIndicado = true;
						if(i>0){
							nombreContacto.append(" / ");
						}
						nombreContacto.append(usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno()+" ");
						nombreContacto.append("\n");
					}	
					String FECHA_SOL_PYME = (rs.getString("FECHA_SOL_PYME") == null) ? "" : rs.getString("FECHA_SOL_PYME");
					String NUMERO_CONTRATO = (rs.getString("NUMERO_CONTRATO") == null) ? "" : rs.getString("NUMERO_CONTRATO");
					StringBuffer montosMonedaSol = cesionBean.getMontoMoneda(rs.getString("CLAVE_SOLICITUD"));
					String TIPO_CONTRATACION = (rs.getString("TIPO_CONTRATACION") == null) ? "" : rs.getString("TIPO_CONTRATACION");
					String FECHA_INICIO_CONTRATO = (rs.getString("FECHA_INICIO_CONTRATO") == null) ? "" : rs.getString("FECHA_INICIO_CONTRATO");
					String FECHA_FIN_CONTRATO = (rs.getString("FECHA_FIN_CONTRATO") == null) ? "" : rs.getString("FECHA_FIN_CONTRATO");
					String PLAZO_CONTRATO = (rs.getString("PLAZO_CONTRATO") == null) ? "" : rs.getString("PLAZO_CONTRATO");
					String CLASIFICACION_EPO = (rs.getString("CLASIFICACION_EPO") == null) ? "" : rs.getString("CLASIFICACION_EPO");	
					String VENTANILLA_PAGO = (rs.getString("VENTANILLA_PAGO") == null) ? "" : rs.getString("VENTANILLA_PAGO");
					String SUP_ADM_RESOB = (rs.getString("SUP_ADM_RESOB") == null) ? "" : rs.getString("SUP_ADM_RESOB");
					String NUMERO_TELEFONO = (rs.getString("NUMERO_TELEFONO") == null) ? "" : rs.getString("NUMERO_TELEFONO");
					String OBJETO_CONTRATO = (rs.getString("OBJETO_CONTRATO") == null) ? "" : rs.getString("OBJETO_CONTRATO");
					String COMENTARIOS = (rs.getString("COMENTARIOS") == null) ? "" : rs.getString("COMENTARIOS");
					String MONTO_CREDITO = (rs.getString("MONTO_CREDITO") == null) ? "" : rs.getString("MONTO_CREDITO");
					String NUMERO_REFERENCIA = (rs.getString("NUMERO_REFERENCIA") == null) ? "" : rs.getString("NUMERO_REFERENCIA");
					String FECHA_VENCIMIENTO = (rs.getString("FECHA_VENCIMIENTO") == null) ? "" : rs.getString("FECHA_VENCIMIENTO");
					String BANCO_DEPOSITO = (rs.getString("BANCO_DEPOSITO") == null) ? "" : rs.getString("BANCO_DEPOSITO");
					String NUMERO_CUENTA = (rs.getString("NUMERO_CUENTA") == null) ? "" : rs.getString("NUMERO_CUENTA");
					String CLABE = (rs.getString("CLABE") == null) ? "" : rs.getString("CLABE");
					String OBSERVACIONES = (rs.getString("CAUSASRECHAZO") == null) ? "" : rs.getString("CAUSASRECHAZO");
					String ESTATUS_SOLICITUD = (rs.getString("ESTATUS_SOLICITUD") == null) ? "" : rs.getString("ESTATUS_SOLICITUD");
					String CAUSAS_RECHAZO_NOTIF = (rs.getString("CAUSAS_RECHAZO_NOTIF") == null) ? "" : rs.getString("CAUSAS_RECHAZO_NOTIF");	
					String FIRMA_CONTRATO = (rs.getString("FIRMA_CONTRATO") == null) ? "" : rs.getString("FIRMA_CONTRATO");	
					
					pdfDoc.setCell("A","formas", ComunesPDF.CENTER);
					pdfDoc.setCell(NOMBRE_IF, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(NOMBRE_PYME, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(RFC, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(NUMERO_PROVEEDOR, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(nombreContacto.toString(), "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(FECHA_SOL_PYME, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(NUMERO_CONTRATO, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(FIRMA_CONTRATO, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(montosMonedaSol.toString().replaceAll("<br/>","\n").replaceAll(",",""),"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(TIPO_CONTRATACION, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(FECHA_INICIO_CONTRATO, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(FECHA_FIN_CONTRATO, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(PLAZO_CONTRATO, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(CLASIFICACION_EPO, "formas", ComunesPDF.CENTER);
					
					pdfDoc.setCell(VENTANILLA_PAGO, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(SUP_ADM_RESOB, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(NUMERO_TELEFONO, "formas", ComunesPDF.CENTER);
					
					pdfDoc.setCell("B", "formas", ComunesPDF.CENTER);
					for(int j = 0; j<indice;j++){
						pdfDoc.setCell(rs.getString("CAMPO_ADICIONAL_"+(j + 1))==null?"":rs.getString("CAMPO_ADICIONAL_"+(j + 1)) + "", "formas", ComunesPDF.LEFT);
					}
					pdfDoc.setCell(OBJETO_CONTRATO, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(COMENTARIOS, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(MONTO_CREDITO,2), "formasrep", ComunesPDF.CENTER);
					pdfDoc.setCell(NUMERO_REFERENCIA, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(FECHA_VENCIMIENTO,"formas", ComunesPDF.CENTER);
					pdfDoc.setCell(BANCO_DEPOSITO, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(NUMERO_CUENTA, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(CLABE, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(OBSERVACIONES, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(ESTATUS_SOLICITUD, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(CAUSAS_RECHAZO_NOTIF, "formas", ComunesPDF.CENTER);
					for(int i = 0; i<=(5-cont); i++){
						pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
					}
				}
				pdfDoc.addTable();
				pdfDoc.endDocument(); 
			} else if(tipo.equals("CSV")){
				CreaArchivo creaArchivo = new CreaArchivo();
				StringBuffer contenidoArchivo = new StringBuffer();
				
				HashMap campos_adicionales_param = cesionBean.getCamposAdicionalesParametrizados(claveEpo, "9");
				int num_campos_adicionales = Integer.parseInt((String)campos_adicionales_param.get("indice"));
				
				String clasificacionEpo = cesionBean.clasificacionEpo(claveEpo);
	
				StringBuffer strBuff = new StringBuffer();			
				
				contenidoArchivo.append("Intermediario Financiero (Cesionario),");
				contenidoArchivo.append("Pyme (Cedente),");
				contenidoArchivo.append("RFC,");
				contenidoArchivo.append("No. Proveedor,");
				contenidoArchivo.append("Representante legal,");
				contenidoArchivo.append("Fecha de Solicitud PyME,");
				contenidoArchivo.append("No. Contrato,");
				contenidoArchivo.append("Firma Contrato,");
				contenidoArchivo.append("Monto / Moneda,");
				contenidoArchivo.append("Tipo de Contrataci�n,");
				contenidoArchivo.append("Fecha Inicio Contrato,");
				contenidoArchivo.append("Fecha Final Contrato,");
				contenidoArchivo.append("Plazo del Contrato,");
				if(clasificacionEpo != null && !clasificacionEpo.equals("")){
					contenidoArchivo.append(clasificacionEpo.toString().replaceAll(",","")+",");
				}
				for(int i = 0; i < num_campos_adicionales; i++){contenidoArchivo.append(campos_adicionales_param.get("nombre_campo_"+i) + ",");}
				contenidoArchivo.append("Ventanilla de Pago,");
				contenidoArchivo.append("Supervisor/Administrador/Residente de Obra,");
				contenidoArchivo.append("Tel�fono,");
				contenidoArchivo.append("Objeto del Contrato,");
				contenidoArchivo.append("Comentarios,");				
				contenidoArchivo.append("Monto del Cr�dito,");
				contenidoArchivo.append("Referencia / No. Cr�dito,");
				contenidoArchivo.append("Fecha Vencimiento Cr�dito,");
				contenidoArchivo.append("Banco de Dep�sito,");
				contenidoArchivo.append("Cuenta,");
				contenidoArchivo.append("Cuenta CLABE,");
				contenidoArchivo.append("Estatus");
				contenidoArchivo.append("\n");
				
				while(rs.next()){
					String NOMBRE_IF = (rs.getString("NOMBRE_IF") == null) ? "" : rs.getString("NOMBRE_IF");
					String NOMBRE_PYME = (rs.getString("NOMBRE_PYME") == null) ? "" : rs.getString("NOMBRE_PYME");
					String RFC = (rs.getString("RFC") == null) ? "" : rs.getString("RFC");
					String NUMERO_PROVEEDOR = (rs.getString("NUMERO_PROVEEDOR") == null) ? "" : rs.getString("NUMERO_PROVEEDOR");
					//Se obtiene el representante legal de la Pyme
					String noProveedor = (rs.getString("CLAVE_PYME")==null)?"":(rs.getString("CLAVE_PYME"));
					StringBuffer nombreContacto = new StringBuffer();
					UtilUsr utilUsr = new UtilUsr();
					boolean usuarioEncontrado = false;
					boolean perfilIndicado = false;
					List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(noProveedor, "P");
					for(int i=0;i<usuariosPorPerfil.size();i++){
						String loginUsuarioEpo = (String)usuariosPorPerfil.get(i);
						Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
						perfilIndicado = true;
						if(i>0){
							nombreContacto.append(" / ");
						}
						nombreContacto.append(usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno()+" ");
					}	
					String FECHA_SOL_PYME = (rs.getString("FECHA_SOL_PYME") == null) ? "" : rs.getString("FECHA_SOL_PYME");
					String NUMERO_CONTRATO = (rs.getString("NUMERO_CONTRATO") == null) ? "" : rs.getString("NUMERO_CONTRATO");
					StringBuffer montosMonedaSol = cesionBean.getMontoMoneda(rs.getString("CLAVE_SOLICITUD"));
					String TIPO_CONTRATACION = (rs.getString("TIPO_CONTRATACION") == null) ? "" : rs.getString("TIPO_CONTRATACION");
					String FECHA_INICIO_CONTRATO = (rs.getString("FECHA_INICIO_CONTRATO") == null) ? "" : rs.getString("FECHA_INICIO_CONTRATO");
					String FECHA_FIN_CONTRATO = (rs.getString("FECHA_FIN_CONTRATO") == null) ? "" : rs.getString("FECHA_FIN_CONTRATO");
					String PLAZO_CONTRATO = (rs.getString("PLAZO_CONTRATO") == null) ? "" : rs.getString("PLAZO_CONTRATO");
					String CLASIFICACION_EPO = (rs.getString("CLASIFICACION_EPO") == null) ? "" : rs.getString("CLASIFICACION_EPO");	
					String VENTANILLA_PAGO = (rs.getString("VENTANILLA_PAGO") == null) ? "" : rs.getString("VENTANILLA_PAGO");
					String SUP_ADM_RESOB = (rs.getString("SUP_ADM_RESOB") == null) ? "" : rs.getString("SUP_ADM_RESOB");
					String NUMERO_TELEFONO = (rs.getString("NUMERO_TELEFONO") == null) ? "" : rs.getString("NUMERO_TELEFONO");
					String OBJETO_CONTRATO = (rs.getString("OBJETO_CONTRATO") == null) ? "" : rs.getString("OBJETO_CONTRATO");
					String COMENTARIOS = (rs.getString("COMENTARIOS") == null) ? "" : rs.getString("COMENTARIOS");
					String MONTO_CREDITO = (rs.getString("MONTO_CREDITO") == null) ? "0" : rs.getString("MONTO_CREDITO");
					String NUMERO_REFERENCIA = (rs.getString("NUMERO_REFERENCIA") == null) ? "" : rs.getString("NUMERO_REFERENCIA");
					String FECHA_VENCIMIENTO = (rs.getString("FECHA_VENCIMIENTO") == null) ? "" : rs.getString("FECHA_VENCIMIENTO");
					String BANCO_DEPOSITO = (rs.getString("BANCO_DEPOSITO") == null) ? "" : rs.getString("BANCO_DEPOSITO");
					String NUMERO_CUENTA = (rs.getString("NUMERO_CUENTA") == null) ? "" : rs.getString("NUMERO_CUENTA");
					String CLABE = (rs.getString("CLABE") == null) ? "" : rs.getString("CLABE");
					String OBSERVACIONES = (rs.getString("CAUSASRECHAZO") == null) ? "" : rs.getString("CAUSASRECHAZO");
					String ESTATUS_SOLICITUD = (rs.getString("ESTATUS_SOLICITUD") == null) ? "" : rs.getString("ESTATUS_SOLICITUD");
					String CAUSAS_RECHAZO_NOTIF = (rs.getString("CAUSAS_RECHAZO_NOTIF") == null) ? "" : rs.getString("CAUSAS_RECHAZO_NOTIF");	
					String FIRMA_CONTRATO = (rs.getString("FIRMA_CONTRATO") == null) ? "" : rs.getString("FIRMA_CONTRATO");	
					
				//for(int i = 0; i < num_registros; i++){
					contenidoArchivo.append(NOMBRE_IF.replaceAll(",", "") + ",");
					contenidoArchivo.append(NOMBRE_PYME.replaceAll(",", "") + ",");
					contenidoArchivo.append(RFC.replaceAll(",", "") + ",");
					contenidoArchivo.append(NUMERO_PROVEEDOR.replaceAll(",", "") + ",");
					contenidoArchivo.append(nombreContacto.toString().replaceAll(",", "") + ",");				
					contenidoArchivo.append(FECHA_SOL_PYME.replaceAll(",", "") + ",");
					contenidoArchivo.append(NUMERO_CONTRATO.replaceAll(",", "") + ",");
					contenidoArchivo.append(FIRMA_CONTRATO + ",");
					
					contenidoArchivo.append(montosMonedaSol.toString().replaceAll("<br/>", "|").replaceAll(",", "") + ",");
					contenidoArchivo.append(TIPO_CONTRATACION + ",");
					contenidoArchivo.append(FECHA_INICIO_CONTRATO + ",");
					contenidoArchivo.append(FECHA_FIN_CONTRATO + ",");
					contenidoArchivo.append(PLAZO_CONTRATO + ",");
					if(clasificacionEpo != null && !clasificacionEpo.equals("")){
						contenidoArchivo.append(CLASIFICACION_EPO.replaceAll(",","") + ",");
					}
					for(int j = 0; j < num_campos_adicionales; j++){
						contenidoArchivo.append(rs.getString("CAMPO_ADICIONAL_"+(j + 1))==null?"-,":rs.getString("CAMPO_ADICIONAL_"+(j + 1)) + ",");
						cont +=1;
					}
					contenidoArchivo.append(VENTANILLA_PAGO.replaceAll(",", "") + ",");
					contenidoArchivo.append(SUP_ADM_RESOB.replaceAll(",", "") + ",");
					contenidoArchivo.append(NUMERO_TELEFONO.replaceAll(",", "") + ",");
					contenidoArchivo.append(OBJETO_CONTRATO.replaceAll(",", "") + ",");
					contenidoArchivo.append(COMENTARIOS.replaceAll(",", "") + ",");
					contenidoArchivo.append("$" + MONTO_CREDITO.replaceAll(",", "") + ",");
					contenidoArchivo.append(NUMERO_REFERENCIA.replaceAll(",", "") + ",");
					contenidoArchivo.append(FECHA_VENCIMIENTO.replaceAll(",", "") + ",");
					contenidoArchivo.append(BANCO_DEPOSITO.replaceAll(",", "") + ",");
					contenidoArchivo.append("'"+NUMERO_CUENTA.replaceAll(",", "") + ",");
					contenidoArchivo.append("'"+CLABE.replaceAll(",", "") + ",");
					contenidoArchivo.append(ESTATUS_SOLICITUD);
					contenidoArchivo.append("\n");
				//}
				}
				rs.close();
				creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
				nombreArchivo = creaArchivo.getNombre();
			}
		}catch(Throwable e){
			throw new AppException("Error al generar el archivo", e);
		}finally{
			try{
				rs.close();
			}catch(Exception e){}
		}
		return nombreArchivo;
	 }
	/** En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va a generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	 public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo){
		return "";
	 }
	 public List getConditions(){
		return conditions;
	 }
	public void setLoginUsuario(String loginUsuario) {
		this.loginUsuario = loginUsuario;
	}
	public String getLoginUsuario() {
		return loginUsuario;
	}
	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}
	public String getClaveEpo() {
		return claveEpo;
	}
	public void setClaveIf(String claveIf) {
		this.claveIf = claveIf;
	}
	public String getClaveIf() {
		return claveIf;
	}
	public void setClavePyme(String clavePyme) {
		this.clavePyme = clavePyme;
	}
	public String getClavePyme() {
		return clavePyme;
	}
	public void setClaveEstatusSol(String claveEstatusSol) {
		this.claveEstatusSol = claveEstatusSol;
	}
	public String getClaveEstatusSol() {
		return claveEstatusSol;
	}
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	public String getNumeroContrato() {
		return numeroContrato;
	}
	public void setFechaVigenciaIni(String fechaVigenciaIni) {
		this.fechaVigenciaIni = fechaVigenciaIni;
	}
	public String getFechaVigenciaIni() {
		return fechaVigenciaIni;
	}
	public void setFechaVigenciaFin(String fechaVigenciaFin) {
		this.fechaVigenciaFin = fechaVigenciaFin;
	}
	public String getFechaVigenciaFin() {
		return fechaVigenciaFin;
	}
	public void setFechaSolicitudIni(String fechaSolicitudIni) {
		this.fechaSolicitudIni = fechaSolicitudIni;
	}
	public String getFechaSolicitudIni() {
		return fechaSolicitudIni;
	}
	public void setFechaSolicitudFin(String fechaSolicitudFin) {
		this.fechaSolicitudFin = fechaSolicitudFin;
	}
	public String getFechaSolicitudFin() {
		return fechaSolicitudFin;
	}
	public void setPlazoContrato(String plazoContrato) {
		this.plazoContrato = plazoContrato;
	}
	public String getPlazoContrato() {
		return plazoContrato;
	}
	public void setClaveTipoPlazo(String claveTipoPlazo) {
		this.claveTipoPlazo = claveTipoPlazo;
	}
	public String getClaveTipoPlazo() {
		return claveTipoPlazo;
	}
	public void setCamposAdicionales(List camposAdicionales) {
		this.camposAdicionales = camposAdicionales;
	}
	public List getCamposAdicionales() {
		return camposAdicionales;
	}
	public void setTipoDato(List tipoDato) {
		this.tipoDato = tipoDato;
	}
	public List getTipoDato() {
		return tipoDato;
	}
}