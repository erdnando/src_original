package com.netro.cesion;

import java.util.HashMap;
import netropology.utilerias.*;
import org.apache.commons.logging.Log;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import java.sql.*;

public class GruposCesionDerechos implements IQueryGeneratorRegExtJS{

	private final static Log log = ServiceLocator.getInstance().getLog(GruposCesionDerechos.class);
	private List conditions;
	private List empresas;
	private String icGrupoCesion;
	private String icPyme;
	private String icEpo;
	private String nombreGrupo;
	private String noContrato;
	private String accion;
	private String usuario;

	public GruposCesionDerechos(){}

	/**
	 * Obtiene las llaves primarias
	 * @return sentencia sql
	 */
	public String getDocumentQuery(){

		log.info("getDocumentQuery(E)");

		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();

		qrySentencia.append(" SELECT IC_GRUPO_CESION");
		qrySentencia.append(" FROM CDER_GRUPO");

		log.debug("Sentencia: " + qrySentencia.toString());
		log.debug("Condiciones: " + conditions.toString());
		log.info("getDocumentQuery(S)");

		return qrySentencia.toString();
	}

	/**
	 * Obtiene la lista de los Ids en turno para realizar la paginacion
	 * @return sentencia sql
	 * @param ids
	 */
	public String getDocumentSummaryQueryForIds(List ids){

		log.info("getDocumentSummaryQueryForIds(E)");

		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();

		condicion.append(" AND C.IC_GRUPO_CESION IN (");
		for (int i = 0; i < ids.size(); i++) { 
			List lItem = (ArrayList)ids.get(i);
			if(i > 0){
				condicion.append(",");
			}
			condicion.append(" ? " );
			conditions.add(new Long(lItem.get(0).toString()));
		}
		condicion.append(") ");

		qrySentencia.append(" SELECT C.IC_GRUPO_CESION,");
		qrySentencia.append(" C.IC_EPO,");
		qrySentencia.append(" E.CG_RAZON_SOCIAL AS NOMBRE_EPO,");
		qrySentencia.append(" C.CG_NOMBRE_GRUPO,");
		qrySentencia.append(" C.CG_NO_CONTRATO,");
		qrySentencia.append(" C.CG_EXPEDIENTE_EFILE,");
		qrySentencia.append(" TO_CHAR(C.DF_ALTA,'DD/MM/YYYY HH:MI:SS PM') AS FECHA_REGISTRO");
		qrySentencia.append(" FROM CDER_GRUPO C, COMCAT_EPO E");
		qrySentencia.append(" WHERE 1 = 1");
		qrySentencia.append(" AND C.IC_EPO = E.IC_EPO");
		qrySentencia.append("" + condicion.toString());

		log.debug("Sentencia: " + qrySentencia.toString());
		log.debug("Condiciones: " + conditions.toString());
		log.info("getDocumentSummaryQueryForIds(S)");

		return qrySentencia.toString();
	}

	/**
	 * Obtiene los totales
	 * @return sentencia sql
	 */
	public String getAggregateCalculationQuery(){

		log.info("getAggregateCalculationQuery(E)");

		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();

		qrySentencia.append(" SELECT COUNT(IC_GRUPO_CESION) AS TOTAL");
		qrySentencia.append(" FROM CDER_GRUPO");

		log.debug("Sentencia: " + qrySentencia.toString());
		log.debug("Condiciones: " + conditions.toString());
		log.info("getAggregateCalculationQuery(S)");

		return qrySentencia.toString();

	}
	
	/**
	 * Obtiene la consulta para generar el archivo PDF o CSV sin utilizar paginaci�n
	 * @return sentencia sql
	 */
	public String getDocumentQueryFile(){

		log.info("getDocumentQueryFile(E)");

		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();

		qrySentencia.append(" SELECT C.IC_GRUPO_CESION,");
		qrySentencia.append(" C.IC_EPO,");
		qrySentencia.append(" C.CG_NOMBRE_GRUPO,");
		qrySentencia.append(" C.CG_NO_CONTRATO,");
		qrySentencia.append(" C.CG_EXPEDIENTE_EFILE,");
		qrySentencia.append(" TO_CHAR(C.DF_ALTA,'DD/MM/YYYY HH:MI:SS') AS FECHA_REGISTRO");
		qrySentencia.append(" FROM C.CDER_GRUPO");

		log.debug("Sentencia: " + qrySentencia.toString());
		log.debug("Condiciones: " + conditions.toString());
		log.info("getDocumentQueryFile(S)");

		return qrySentencia.toString();

	}

	/**
	 * Crea el archivo PDF o CSV seg�n el tipo que se le pasa como par�metro.
	 * Obtiene los datos de la consulta generada en el m�todo getDocumentQueryFile.
	 * @return nombre del archivo
	 * @param tipo
	 * @param path
	 * @param rsAPI
	 * @param request
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rsAPI, String path, String tipo){
		return null;
	}

	/**
	 * Crea el archivo PDF utilizando paginaci�n
	 * @return 
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){
		return null;
	}

/************************************************************
 *               M�TODOS DEL FODEA 23-2015                  *
 ************************************************************/
	/**
	 * Elimina un grupo de cesi�n de derechos, toma como par�metro la variable icGrupoCesion.
	 * Dentro del mismo m�todo se eliminan las empresas asociadas al grupo.
	 * @return El mensaje indica si se elimin� o no el grupo
	 */
	public String eliminaGrupo(){

		String mensaje = "";

		log.info("eliminaGrupo(E)");

		AccesoDB con         = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs         = null;
		StringBuffer queryS  = new StringBuffer();
		StringBuffer queryE  = new StringBuffer();
		StringBuffer queryG  = new StringBuffer();
		boolean success      = true;
		int regActualizados  = 0;
		int existe           = 0;
		boolean estatusGpo   = false;

		try{
			con.conexionDB();

			//Primero valido el estatus para saber si se puede eliminar
			GruposCesionDerechos grupos = new GruposCesionDerechos();
			estatusGpo = grupos.validaGrupo(Integer.parseInt(icGrupoCesion));

			if(estatusGpo == true){

				//Primero elimino las empresas asociadas a ese grupo
				queryE.append("DELETE FROM CDERREL_PYME_X_GRUPO ");
				queryE.append("WHERE IC_GRUPO_CESION = ? "       );
				log.debug("Sentencia SQL: " + queryE.toString());
				ps = con.queryPrecompilado(queryE.toString());
				ps.clearParameters();
				ps.setInt(1, Integer.parseInt(icGrupoCesion));
				regActualizados = ps.executeUpdate();
				ps.close();

				if(regActualizados > 0){
					//Si se eliminaron las empresas correctamente, elimino el grupo
					ps = null;
					queryG.append("DELETE FROM CDER_GRUPO "   );
					queryG.append("WHERE IC_GRUPO_CESION = ? ");
					log.debug("Sentencia SQL: " + queryG.toString());
					ps = con.queryPrecompilado(queryG.toString());
					ps.clearParameters();
					ps.setInt(1, Integer.parseInt(icGrupoCesion));
					regActualizados = ps.executeUpdate();
					ps.close();
					mensaje = "OK"; //El verdadero mensaje de �xito se muestra en el archivo js
				} else{
					success = false;
					mensaje = "No se pudo eliminar el grupo.";
				}

			} else{
				success = false;
				mensaje = "No se puede eliminar el grupo debido a que ya existe una solicitud de consentimiento.";
			}

		}catch(Exception e){
			success = false;
			mensaje = "Error al intentar eliminar el grupo. " + e;
			log.warn("eliminaGrupo.Exception. " + e);
		}finally{
			try{
				if(rs!=null)
					rs.close();
				if(ps!=null)
					ps.close();
			}catch(SQLException ex){
				log.warn(ex);
			}
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(success);
				con.cierraConexionDB();
			}
		}

		log.info("eliminaGrupo(S)");
		return mensaje;
	}

	/**
	 * Agrega un grupo nuevo o edita uno existente, incluyendo las pymes asociadas a dicho grupo.
	 * Antes de eso debe validar (en el caso de actualizar datos) si el grupo se puede editar o no.
	 * @return mensaje de OK o en su defecto el error
	 */
	public String actualizaGrupo(){
		String mensaje = "";
		log.info("actualizaGrupo(E)");

		AccesoDB con         = new AccesoDB();
		PreparedStatement ps = null;
		PreparedStatement psI= null;
		PreparedStatement psD= null;
		ResultSet rs         = null;
		StringBuffer queryI  = new StringBuffer();
		StringBuffer queryU  = new StringBuffer();
		String expedEfile    = "";
		int regActualizados  = 0;
		int nextVal          = 1;
		HashMap mapaEmpresa  = new HashMap();
		boolean success      = true;
		boolean estatusGpo   = false;
		try{
			con.conexionDB();

			if(accion.equals("INSERT")){

				// Para el insert, primero tengo que obtener el nextVal
				ps = con.queryPrecompilado("SELECT SEQ_CDER_GRUPO.NEXTVAL FROM DUAL");
				rs = ps.executeQuery();
				if(rs.next()){
					nextVal = rs.getInt(1);
				}
				rs.close();
				ps.close();

				// Se arma la variable del campo CG_EXPEDIENTE_EFILE
				expedEfile = noContrato + "-" + nextVal;

				/*
				 * inserto el grupo
				 */
				queryI.append("INSERT INTO CDER_GRUPO( ");
				queryI.append("IC_GRUPO_CESION, "        );
				queryI.append("IC_EPO, "                 );
				queryI.append("CG_NO_CONTRATO, "         );
				queryI.append("CG_EXPEDIENTE_EFILE, "    );
				queryI.append("IC_USUARIO)"              );
				queryI.append(" VALUES(?, ?, ?, ?, ?)"   );

				log.debug("Sentencia SQL: " + queryI.toString());

				ps = con.queryPrecompilado(queryI.toString());
				ps.clearParameters();
				ps.setInt(   1, nextVal                );
				ps.setInt(   2, Integer.parseInt(icEpo));
				ps.setString(3, noContrato             );
				ps.setString(4, expedEfile             );
				ps.setString(5, usuario                );
				regActualizados = ps.executeUpdate();
				ps.close();

			} else if(accion.equals("UPDATE")){

				// Se arma la variable del campo CG_EXPEDIENTE_EFILE
				expedEfile = noContrato + "-" + icGrupoCesion;
				/*
				* Actualizo el grupo
				*/
				queryU.append("UPDATE CDER_GRUPO "         );
				queryU.append("SET CG_NO_CONTRATO = ?, "   ); 
				queryU.append("CG_EXPEDIENTE_EFILE = ?, "  );
				queryU.append("DF_MODIFICACION = SYSDATE, ");
				queryU.append("IC_USUARIO = ? "            );
				queryU.append("WHERE IC_GRUPO_CESION = ? " );
				queryU.append("AND IC_EPO = ? "            );

				log.debug("Sentencia SQL: " + queryU.toString());

				ps = con.queryPrecompilado(queryU.toString());
				ps.clearParameters();
				ps.setString(1, noContrato);
				ps.setString(2, expedEfile);
				ps.setString(3, usuario);
				ps.setInt(   4, Integer.parseInt(icGrupoCesion));
				ps.setInt(   5, Integer.parseInt(icEpo));
				regActualizados = ps.executeUpdate();
				ps.close();

			}

			/*
			 * Si el grupo se insert� o actualiz� correctamente, se insertan las empresas
			 */
			if(regActualizados > 0){
				int icGrupo = 0;
				String icPymeTmp = "";
				String pymePrincipalTmp = "";
				psD = null;

				String queryInsert = "INSERT INTO CDERREL_PYME_X_GRUPO(IC_GRUPO_CESION, IC_PYME, CS_PYME_PRINCIPAL) VALUES(?, ?, ?)";
				String queryDelete = "DELETE CDERREL_PYME_X_GRUPO WHERE IC_GRUPO_CESION = ?";

				if(accion.equals("INSERT")){
					icGrupo = nextVal;
				} else if(accion.equals("UPDATE")){
					icGrupo = Integer.parseInt(icGrupoCesion);
					//Si ya existe el grupo se eliminan sus pymes y se vuelve a insertar todo el grid Empresas
					psD = con.queryPrecompilado(queryDelete);
					psD.clearParameters();
					psD.setInt(1, icGrupo);
					regActualizados = psD.executeUpdate();					
				}

				for(int i = 0; i < empresas.size(); i++){

					mapaEmpresa = new HashMap();
					mapaEmpresa = (HashMap) empresas.get(i);
					pymePrincipalTmp = "" + mapaEmpresa.get("PYME_PRINCIPAL");
					icPymeTmp = "" + mapaEmpresa.get("IC_PYME");
					psI = null;

					psI = con.queryPrecompilado(queryInsert);
					psI.clearParameters();
					psI.setInt(   1, icGrupo                   );
					psI.setInt(   2, Integer.parseInt(icPymeTmp));
					psI.setString(3, pymePrincipalTmp           );
					regActualizados = psI.executeUpdate();

				} // FIN DEL FOR

				/***** Si llego a este punto significa que todo sali� bien *****/
				mensaje = "OK";
				success = true;
				log.debug("Si llego a este punto significa que todo sali� bien");
				log.debug("mensaje: " + mensaje);
				log.debug("success: " + success);

			} else{
				success = false;
			}

		} catch(Exception e){
			success = false;
			mensaje = "Error al intentar actualizar el grupo. " + e;
			log.warn("actualizaGrupo.Exception. " + e);
		} finally{
			try{
				if(psD!=null)
					psD.close();
				if(psI!=null)
					psI.close();
			} catch(Exception e){}
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(success);
				con.cierraConexionDB();
			}
		}

		log.info("actualizaGrupo(S)");
		return mensaje;
	}

	/**
	 * Antes de eliminar o actualizar un grupo, este m�todo valida si el grupo ya
	 * tiene una solicitud. Si no tiene solicitud se puede eliminar y editar.
	 * Si ya tiene una solicitus, se verifica que su estatus sea 'Cancelada por NAFIN' (24)
	 * @return true: Se puede EDITAR/ELIMINAR, false: No se puede EDITAR/ELIMINAR. 
	 */
	public boolean validaGrupo(int id){

		log.info("validaGrupo(e)");
		AccesoDB con         = new AccesoDB();
		boolean estatus      = false;
		PreparedStatement ps = null;
		ResultSet rs         = null;
		String query         = "";
		int existe           = 0;
		int existeEstatus    = 0;

		try{
			con.conexionDB();

			query = "SELECT COUNT(IC_SOLICITUD) FROM CDER_SOLICITUD WHERE IC_GRUPO_CESION = ? ";
			log.debug("query: " + query);
			log.debug("icGrupoCesion: " + id);
			ps = con.queryPrecompilado(query.toString());
			ps.clearParameters();
			ps.setInt(1, id);
			rs = ps.executeQuery();
			if(rs.next()){
				existe = rs.getInt(1);
			}

			if(existe > 0){
				// El grupo si tiene solicitud, verifica el estatus
				query = "SELECT COUNT(IC_SOLICITUD) FROM CDER_SOLICITUD WHERE IC_GRUPO_CESION = ? AND IC_ESTATUS_SOLIC IN(24)";
				log.debug("query: " + query);
				log.debug("icGrupoCesion: " + id);
				ps = con.queryPrecompilado(query.toString());
				ps.clearParameters();
				ps.setInt(1, id);
				rs = ps.executeQuery();
				if(rs.next()){
					existeEstatus = rs.getInt(1);
				}
				if(existeEstatus > 0){
					// El grupo tiene estatus Cancelada por NAFIN, se puede editar
					estatus = true;
				} else{
					// El grupo tiene otro estatus, no se puede editar
					estatus = false;
				}
			} else{
				// El grupo no tiene solicitud, se puede editar
				estatus = true;
			}
		} catch(Exception e){
			log.warn("validaGrupo.Exception: " + e);
		} finally{
			try{
				if(ps!=null)
					ps.close();
			} catch(Exception e){}
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}

		log.info("validaGrupo(S)");
		return estatus;
	}

	/**
	 * Valida que el N�mero de Contrato y la Cadena Productiva especificadas en un grupo
	 * no puedan ser los mismos en otros grupos.
	 * @return true: Si ya existe en un grupo, false: otro caso
	 */
	public boolean validaContratosRepetidos(){

		log.info(" validaContratosRepetidos(e)");
		AccesoDB con         = new AccesoDB();
		boolean estatus      = false;
		PreparedStatement ps = null;
		ResultSet rs         = null;
		String query         = "";
		int existe           = 0;

		try{
			con.conexionDB();

			query = "SELECT COUNT(IC_GRUPO_CESION) FROM CDER_GRUPO WHERE IC_EPO = ? AND CG_NO_CONTRATO = ?";
			if(!this.icGrupoCesion.equals("")){
				query += " AND IC_GRUPO_CESION <> ?";
			}
			log.debug("query: " + query);
			log.debug("icEpo: " + this.icEpo);
			log.debug("noContrato: " + this.noContrato);
			log.debug("icGrupoCesion: " + this.icGrupoCesion);
			ps = con.queryPrecompilado(query.toString());
			ps.clearParameters();
			ps.setInt(1, Integer.parseInt(this.icEpo));
			ps.setString(2, this.noContrato);
			if(!this.icGrupoCesion.equals("")){
				ps.setInt(3, Integer.parseInt(this.icGrupoCesion));
			}
			rs = ps.executeQuery();
			if(rs.next()){
				existe = rs.getInt(1);
			}

			if(existe > 0){ // La combinaci�n ya est� asignada a por lo menos un grupo
				estatus = true;
			}
		} catch(Exception e){
			log.warn(" validaContratosRepetidos.Exception: " + e);
		} finally{
			try{
				if(ps!=null)
					ps.close();
			} catch(Exception e){}
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}

		log.info(" validaContratosRepetidos(S)");
		return estatus;
	}

/*****************************************************
 *                GETTERS AND SETTERS                *
 *****************************************************/
	public List getConditions(){
		return conditions;
	}

	public List getEmpresas(){
		return empresas;
	}

	public void setEmpresas(List empresas){
		this.empresas = empresas;
	}

	public String getIcGrupoCesion(){
		return icGrupoCesion;
	}

	public void setIcGrupoCesion(String icGrupoCesion){
		this.icGrupoCesion = icGrupoCesion;
	}

	public String getIcPyme(){
		return icPyme;
	}

	public void setIcPyme(String icPyme){
		this.icPyme = icPyme;
	}

	public String getIcEpo(){
		return icEpo;
	}

	public void setIcEpo(String icEpo){
		this.icEpo = icEpo;
	}

	public String getNombreGrupo(){
		return nombreGrupo;
	}

	public void setNombreGrupo(String nombreGrupo){
		this.nombreGrupo = nombreGrupo;
	}

	public String getNoContrato(){
		return noContrato;
	}

	public void setNoContrato(String noContrato){
		this.noContrato = noContrato;
	}

	public String getAccion(){
		return accion;
	}

	public void setAccion(String accion){
		this.accion = accion;
	}

	public String getUsuario(){
		return usuario;
	}

	public void setUsuario(String usuario){
		this.usuario = usuario;
	}

}