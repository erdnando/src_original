package com.netro.cesion;
import com.netro.pdf.ComunesPDF;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;
import java.util.*;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;
import org.apache.commons.logging.Log;

public class AutoCorsorcio implements IQueryGeneratorRegExtJS {
	public AutoCorsorcio() {    	}
	private static final Log log = ServiceLocator.getInstance().getLog(AutoCorsorcio.class);
	

	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");  
	private String 	paginaOffset;
	private String 	paginaNo;
	private String ic_solicitud;
	private String tipoVisor;
	private String titulo;
	private String tipoConsulta;
	private String id_grupo;
	
	
	public String getAggregateCalculationQuery()	{
		StringBuffer 	qrySentencia 	= new StringBuffer();
		return qrySentencia.toString();
	}
	
	
	public String getDocumentQuery()	{
		log.info("getDocumentQuery(E)");
		qrySentencia = new StringBuffer();
		conditions = new ArrayList();
		
		log.debug("..::strQuery getDocumentQuery: "+qrySentencia.toString());
		log.debug("..:: conditions : "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();
	}
	
	
	public String getDocumentSummaryQueryForIds(List pageIds)  {
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.debug(" strSQL ::.."+qrySentencia.toString());
		log.debug("Conditions ::.."+conditions);
		log.info("getDocumentSummaryQueryForIds(S) ::..");
		return qrySentencia.toString();
	}
	
	
	
	public String getDocumentQueryFile() {
		log.info(" getDocumentQueryFile(E)");
		conditions	= new ArrayList();
		qrySentencia		= new StringBuffer();
		
		if(tipoConsulta.equals("Consultar"))  {
		
			qrySentencia.append("SELECT  "+
				" p.CG_RAZON_SOCIAL  as  NOMBRE_PYME ,  "+
				" p.ic_pyme AS IC_PYME,  "+
				" r.IC_USUARIO  as ic_usuario,  "+
				
				"  '' as  NOMBRE_REPRESENTANTE_LEGAL,   "+
				" 	'' as  CORREO_ELECTRONICO,   "+				
				" to_char(r.df_envio_representante,'DD/MM/YYYY HH:MI:SS PM') as FECHA_AUTORIZACION ,   "+ 
            "  to_char(r.df_firma_representante,'DD/MM/YYYY HH:MI:SS PM' ) as FECHA_FIRMA    "+	 
		 
				" 	FROM  cder_solicitud  s,  "+
				"  CDER_SOLIC_X_REPRESENTANTE  r  , "+
				"  comcat_pyme p  "+ 
				
				"  WHERE  s.ic_solicitud = r.ic_solicitud  "+
				"  AND r.ic_pyme = p.ic_pyme  ");
			
			if(!"".equals(ic_solicitud) && ic_solicitud != null){
				qrySentencia.append("   AND s.ic_solicitud  = ? " );
				conditions.add(ic_solicitud);
			}	
		
		} else if(tipoConsulta.equals("Consultar_1")){
			
			qrySentencia.append("SELECT IC_USUARIO, s.IC_IF, s.IC_EPO, "+
			" i.CG_RAZON_SOCIAL AS NOMBRE_IF, "+
			"  e.CG_RAZON_SOCIAL AS NOMBRE_EPO,  " +
			//FIRMA REFRESENTANTES IF  
			" TO_CHAR(S.DF_FIRMA_IF, 'dd/mm/yyyy HH:MM:SS PM') AS DF_FIRMA_IF, "+
			" TO_CHAR(S.DF_FIRMA_IF2, 'dd/mm/yyyy HH:MM:SS PM') AS DF_FIRMA_IF2, "+
				//FIRMA DE LOS TETIGOS   
			" TO_CHAR(S.DF_FIRMA_TESTIGO1, 'dd/mm/yyyy HH:MM:SS PM') AS DF_FIRMA_TESTIGO1, "+
			" TO_CHAR(S.DF_FIRMA_TESTIGO2, 'dd/mm/yyyy HH:MM:SS PM') AS DF_FIRMA_TESTIGO2, "+
			" TO_CHAR(S.DF_AUTORIZO_IF_OPERATIVO, 'dd/mm/yyyy HH:MM:SS PM') AS DF_AUTORIZO_IF_OPERATIVO, "+  // IF OPERACION
			" TO_CHAR(S.DF_NOTIF_VENT, 'dd/mm/yyyy HH:MM:SS PM') AS DF_NOTIF_VENT, "+ // ADMIN EPO
			" TO_CHAR(S.DF_REDIRECCION, 'dd/mm/yyyy HH:MM:SS PM') AS DF_REDIRECCION, "+ // VENTANILLA
			//ACUSE
			" S.CG_USUARIO_AUT_IF_OPER, "+
			" S.CC_ACUSE4, "+
			" S.CC_ACUSE6, "+
			" S.CC_ACUSE_TESTIGO1, "+
			" S.CC_ACUSE_TESTIGO2, "+
			" S.CC_ACUSE5, "+
			" S.CC_ACUSE_REDIREC "+
			" FROM CDER_SOLICITUD s, comcat_epo e, comcat_if i "+
			"  where  s.ic_epo  = e.ic_epo  "+
			"  and s.ic_if = i.ic_if    "+
			"  and  IC_SOLICITUD  =   ? ");
		conditions.add(ic_solicitud);

		}else if(tipoConsulta.equals("Consultar_2"))  {
			qrySentencia.append("SELECT DISTINCT cp.ic_pyme as IC_PYME, cp.cg_razon_social as NOMBRE_PYME "); 
			qrySentencia.append("  FROM cder_grupo cg, cderrel_pyme_x_grupo cpg, comcat_pyme cp ");
			qrySentencia.append(" WHERE cg.ic_grupo_cesion = cpg.ic_grupo_cesion ");
			qrySentencia.append("   AND cpg.ic_pyme = cp.ic_pyme ");
			qrySentencia.append("   AND cg.ic_grupo_cesion = ? ");
			
			conditions.add(id_grupo);

		} else if(tipoConsulta.equals("Consultar_3")){

			qrySentencia.append("SELECT  "+
				" p.CG_RAZON_SOCIAL as NOMBRE_PYME, "+
				" p.ic_pyme AS IC_PYME, "+
				" r.IC_USUARIO as ic_usuario, "+
				" '' as NOMBRE_REPRESENTANTE_LEGAL, "+
				" '' as CORREO_ELECTRONICO, "+
				//" r.CG_FIRMA_REPRESENTANTE as FIRMA_CONVENIO_EXTINCION, "+
				" '' as FIRMA_CONVENIO_EXTINCION, "+
				" to_char(r.DF_EXTINCION_REP,'DD/MM/YYYY HH:MI:SS PM' ) as FECHA_FIRMA "+
				" FROM cder_solicitud s, "+
				" CDER_SOLIC_X_REPRESENTANTE r, "+
				" comcat_pyme p "+
				" WHERE s.ic_solicitud = r.ic_solicitud "+
				" AND r.ic_pyme = p.ic_pyme ");

			if(!"".equals(ic_solicitud) && ic_solicitud != null){
				qrySentencia.append(" AND s.ic_solicitud  = ? " );
				conditions.add(ic_solicitud);
			}

		}
		
		log.debug("..::getDocumentQuery: "+qrySentencia.toString());
		log.debug("..:: conditions: "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();
	}
	
	
	
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		log.info("crearPageCustomFile(E)");
		String nombreArchivo = "";
		
		return nombreArchivo;
	}
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet reg, String path, String tipo) {
		log.info("crearPageCustomFile(E)");
		CreaArchivo creaArchivo = new CreaArchivo();
		ComunesPDF pdfDoc = new ComunesPDF();
		HttpSession session = request.getSession();
		String nombreArchivo = "";
		
		try {
		
			CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class); 
	
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				
			if(tipoConsulta.equals("Consultar"))  {	
				pdfDoc.addText(titulo,"formas",ComunesPDF.LEFT);
				
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				
				
				if(tipoVisor.equals("Empresas")){
					pdfDoc.setLTable(3, 100);
				}else{
					pdfDoc.setLTable(5, 100);
				}
				pdfDoc.setLCell("Nombre Pyme","celda01",ComunesPDF.CENTER);	
				pdfDoc.setLCell("Nombre Representante Legal","celda01",ComunesPDF.CENTER);	
				pdfDoc.setLCell("Correo Electr�nico","celda01",ComunesPDF.CENTER);	
				
				if(tipoVisor.equals("Autorizacion_Consorcio")){
				
					pdfDoc.setLCell("Autorizaci�n Solicitud","celda01",ComunesPDF.CENTER);	
					pdfDoc.setLCell("Fecha de Autorizaci�n","celda01",ComunesPDF.CENTER);	
				
				}else  if(tipoVisor.equals("Contrato_cesion")){ 
				
					pdfDoc.setLCell("Firma Contrato","celda01",ComunesPDF.CENTER);	
					pdfDoc.setLCell("Fecha de Firma","celda01",ComunesPDF.CENTER);	
					
				}
				  
				while (reg.next()) {
				
					String nombrePyme = (reg.getString("NOMBRE_PYME")==null)?" ":reg.getString("NOMBRE_PYME");
					String nombreRepresentante = (reg.getString("NOMBRE_REPRESENTANTE_LEGAL")==null)?" ":reg.getString("NOMBRE_REPRESENTANTE_LEGAL");
					String correo = (reg.getString("CORREO_ELECTRONICO")==null)?" ":reg.getString("CORREO_ELECTRONICO");
					String fecha_solic = (reg.getString("FECHA_AUTORIZACION")==null)?"":reg.getString("FECHA_AUTORIZACION");
					String fecha_firma = (reg.getString("FECHA_FIRMA")==null)?"":reg.getString("FECHA_FIRMA");
					String  ic_usuario  = (reg.getString("IC_USUARIO") == null) ? "" : reg.getString("IC_USUARIO");
					 
					HashMap repre =  cesionBean.getRepresentantes(ic_usuario, ""); 
					  
					nombreRepresentante =  repre.get("NOMBRE_REPRESENTANTE_LEGAL").toString();
					correo =  repre.get("CORREO_ELECTRONICO").toString();
									
					pdfDoc.setLCell(nombrePyme,"formas",ComunesPDF.LEFT);	
					pdfDoc.setLCell(nombreRepresentante,"formas",ComunesPDF.LEFT);	
					pdfDoc.setLCell(correo,"formas",ComunesPDF.CENTER);	
					
					if(tipoVisor.equals("Autorizacion_Consorcio")){
					
						if(!fecha_solic.equals("")){
							pdfDoc.setLCell("AUTORIZADO","formas",ComunesPDF.CENTER);
						}else  {
							pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						}
						pdfDoc.setLCell(fecha_solic,"formas",ComunesPDF.CENTER);					
					
					
					}else if(tipoVisor.equals("Contrato_cesion")){
						
						if(!fecha_firma.equals("")){
							pdfDoc.setLCell("FIRMADO","formas",ComunesPDF.CENTER);
						}else  {
							pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						}
						pdfDoc.setLCell(fecha_firma,"formas",ComunesPDF.CENTER);					
					
						
					}
				}
			
			} else if(tipoConsulta.equals("Consultar_1")){

				HashMap representante = new HashMap();
			
				String NOMBRE_IF                = "";
				String NOMBRE_EPO               = "";
				String DF_FIRMA_IF              = "";
				String DF_FIRMA_IF2             = "";
				String DF_FIRMA_TESTIGO1        = "";
				String DF_FIRMA_TESTIGO2        = "";
				String DF_AUTORIZO_IF_OPERATIVO = "";
				String DF_NOTIF_VENT            = "";
				String DF_REDIRECCION           = "";
				String CC_ACUSE4                = "";
				String CC_ACUSE5                = "";
				String CC_ACUSE6                = "";
				String CC_ACUSE_TESTIGO1        = "";
				String CC_ACUSE_TESTIGO2        = "";
				String CC_ACUSE_REDIREC         = "";
				String CG_USUARIO_AUT_IF_OPER   = "";
		 
			if(reg.next()){
				 NOMBRE_IF               = (reg.getString("NOMBRE_IF")                == null) ? "" : reg.getString("NOMBRE_IF");
				 NOMBRE_EPO              = (reg.getString("NOMBRE_EPO")               == null) ? "" : reg.getString("NOMBRE_EPO");
				DF_FIRMA_IF              = (reg.getString("DF_FIRMA_IF")              == null) ? "" : reg.getString("DF_FIRMA_IF");
				DF_FIRMA_IF2             = (reg.getString("DF_FIRMA_IF2")             == null) ? "" : reg.getString("DF_FIRMA_IF2");
				DF_FIRMA_TESTIGO1        = (reg.getString("DF_FIRMA_TESTIGO1")        == null) ? "" : reg.getString("DF_FIRMA_TESTIGO1");
				DF_FIRMA_TESTIGO2        = (reg.getString("DF_FIRMA_TESTIGO2")        == null) ? "" : reg.getString("DF_FIRMA_TESTIGO2");
				DF_AUTORIZO_IF_OPERATIVO = (reg.getString("DF_AUTORIZO_IF_OPERATIVO") == null) ? "" : reg.getString("DF_AUTORIZO_IF_OPERATIVO");
				DF_NOTIF_VENT            = (reg.getString("DF_NOTIF_VENT")            == null) ? "" : reg.getString("DF_NOTIF_VENT");
				DF_REDIRECCION           = (reg.getString("DF_REDIRECCION")           == null) ? "" : reg.getString("DF_REDIRECCION");
				CC_ACUSE4                = (reg.getString("CC_ACUSE4")                == null) ? "" : reg.getString("CC_ACUSE4");
				CC_ACUSE5                = (reg.getString("CC_ACUSE5")                == null) ? "" : reg.getString("CC_ACUSE5");
				CC_ACUSE6                = (reg.getString("CC_ACUSE6")                == null) ? "" : reg.getString("CC_ACUSE6");
				CC_ACUSE_TESTIGO1        = (reg.getString("CC_ACUSE_TESTIGO1")        == null) ? "" : reg.getString("CC_ACUSE_TESTIGO1");
				CC_ACUSE_TESTIGO2        = (reg.getString("CC_ACUSE_TESTIGO2")        == null) ? "" : reg.getString("CC_ACUSE_TESTIGO2");
				CC_ACUSE_REDIREC         = (reg.getString("CC_ACUSE_REDIREC")         == null) ? "" : reg.getString("CC_ACUSE_REDIREC");
				CG_USUARIO_AUT_IF_OPER   = (reg.getString("CG_USUARIO_AUT_IF_OPER")   == null) ? "" : reg.getString("CG_USUARIO_AUT_IF_OPER");
			}

				pdfDoc.addText("","formas",ComunesPDF.LEFT);
				pdfDoc.addText("Firmas Contrato Cesi�n de Firmas","formas",ComunesPDF.LEFT);
				pdfDoc.addText("","formas",ComunesPDF.LEFT);

				pdfDoc.setLTable(6, 100);
				pdfDoc.setLCell("Raz�n Social","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Perfil Autorizador ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Nombre Representante Legal ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Correo Electr�nico ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Firma Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Firma ","celda01",ComunesPDF.CENTER);

				pdfDoc.setLCell(NOMBRE_IF,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell("IF OPERACION","formas",ComunesPDF.CENTER);
				if(!CG_USUARIO_AUT_IF_OPER.equals("")){
					representante = new HashMap();
					representante = cesionBean.getRepresentantes(CG_USUARIO_AUT_IF_OPER, "");
					pdfDoc.setLCell("" + representante.get("NOMBRE_REPRESENTANTE_LEGAL"),"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell("" + representante.get("CORREO_ELECTRONICO"),"formas",ComunesPDF.CENTER);
				} else{
					pdfDoc.setLCell("","formas",ComunesPDF.LEFT);
					pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
				}
				pdfDoc.setLCell(!"".equals(DF_AUTORIZO_IF_OPERATIVO)?"AUTORIZADO": "" ,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(DF_AUTORIZO_IF_OPERATIVO,"formas",ComunesPDF.CENTER);
				
				pdfDoc.setLCell(NOMBRE_IF,"formas",ComunesPDF.CENTER); 
				pdfDoc.setLCell("REPRESENTANTE IF 1","formas",ComunesPDF.CENTER);
				if(!CC_ACUSE4.equals("")){
					representante = new HashMap();
					representante = cesionBean.getRepresentantes(CC_ACUSE4);
					pdfDoc.setLCell("" + representante.get("NOMBRE_REPRESENTANTE_LEGAL"),"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell("" + representante.get("CORREO_ELECTRONICO"),"formas",ComunesPDF.CENTER);
				} else{
					pdfDoc.setLCell("","formas",ComunesPDF.LEFT);
					pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
				}
				pdfDoc.setLCell(!"".equals(DF_FIRMA_IF)?"FIRMADO": "" ,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(DF_FIRMA_IF,"formas",ComunesPDF.CENTER);
				
				pdfDoc.setLCell(NOMBRE_IF,"formas",ComunesPDF.CENTER); 
				pdfDoc.setLCell("REPRESENTANTE IF 2","formas",ComunesPDF.CENTER);

				if(!DF_FIRMA_IF2.equals("")){
					if(!CC_ACUSE6.equals("")){
						representante = new HashMap();
						representante = cesionBean.getRepresentantes(CC_ACUSE6);
						pdfDoc.setLCell("" + representante.get("NOMBRE_REPRESENTANTE_LEGAL"),"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell("" + representante.get("CORREO_ELECTRONICO"),"formas",ComunesPDF.CENTER);
					} else{
						pdfDoc.setLCell("","formas",ComunesPDF.LEFT);
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
					}
				} else{
					pdfDoc.setLCell("N/A","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("N/A","formas",ComunesPDF.CENTER);
				}
				pdfDoc.setLCell(!"".equals(DF_FIRMA_IF2)?"FIRMADO": "N/A" ,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(!"".equals(DF_FIRMA_IF2)?DF_FIRMA_IF2: "N/A","formas",ComunesPDF.CENTER);
				
				pdfDoc.setLCell(NOMBRE_IF,"formas",ComunesPDF.CENTER); 
				pdfDoc.setLCell("TESTIGO 1","formas",ComunesPDF.CENTER);
				if(!CC_ACUSE_TESTIGO1.equals("")){
					representante = new HashMap();
					representante = cesionBean.getRepresentantes(CC_ACUSE_TESTIGO1);
					pdfDoc.setLCell("" + representante.get("NOMBRE_REPRESENTANTE_LEGAL"),"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell("" + representante.get("CORREO_ELECTRONICO"),"formas",ComunesPDF.CENTER);
				} else{
					pdfDoc.setLCell("","formas",ComunesPDF.LEFT);
					pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
				}
				pdfDoc.setLCell(!"".equals(DF_FIRMA_TESTIGO1)?"FIRMADO": "" ,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(DF_FIRMA_TESTIGO1,"formas",ComunesPDF.CENTER);
				
				pdfDoc.setLCell(NOMBRE_IF,"formas",ComunesPDF.CENTER); 
				pdfDoc.setLCell("TESTIGO 2","formas",ComunesPDF.CENTER);
				if(!CC_ACUSE_TESTIGO2.equals("")){
					representante = new HashMap();
					representante = cesionBean.getRepresentantes(CC_ACUSE_TESTIGO2);
					pdfDoc.setLCell("" + representante.get("NOMBRE_REPRESENTANTE_LEGAL"),"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell("" + representante.get("CORREO_ELECTRONICO"),"formas",ComunesPDF.CENTER);
				} else{
					pdfDoc.setLCell("","formas",ComunesPDF.LEFT);
					pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
				}
				pdfDoc.setLCell(!"".equals(DF_FIRMA_TESTIGO2)?"FIRMADO": "" ,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(DF_FIRMA_TESTIGO2,"formas",ComunesPDF.CENTER);				
				
				pdfDoc.setLCell(NOMBRE_EPO,"formas",ComunesPDF.CENTER); 
				pdfDoc.setLCell("ADMIN EPO","formas",ComunesPDF.CENTER);
				if(!CC_ACUSE5.equals("")){
					representante = new HashMap();
					representante = cesionBean.getRepresentantes(CC_ACUSE5);
					pdfDoc.setLCell("" + representante.get("NOMBRE_REPRESENTANTE_LEGAL"),"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell("" + representante.get("CORREO_ELECTRONICO"),"formas",ComunesPDF.CENTER);
				} else{
					pdfDoc.setLCell("","formas",ComunesPDF.LEFT);
					pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
				}
				pdfDoc.setLCell(!"".equals(DF_FIRMA_TESTIGO2)?"NOTIFICADO": "" ,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(DF_NOTIF_VENT,"formas",ComunesPDF.CENTER);
				
				pdfDoc.setLCell(NOMBRE_EPO,"formas",ComunesPDF.CENTER); 
				pdfDoc.setLCell("VENTANILLA","formas",ComunesPDF.CENTER);
				if(!CC_ACUSE_REDIREC.equals("")){
					representante = new HashMap();
					representante = cesionBean.getRepresentantes(CC_ACUSE_REDIREC);
					pdfDoc.setLCell("" + representante.get("NOMBRE_REPRESENTANTE_LEGAL"),"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell("" + representante.get("CORREO_ELECTRONICO"),"formas",ComunesPDF.CENTER);
				} else{
					pdfDoc.setLCell("","formas",ComunesPDF.LEFT);
					pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
				}
				pdfDoc.setLCell(!"".equals(DF_FIRMA_TESTIGO2)?"REDIRECCIONADO": "" ,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(DF_REDIRECCION,"formas",ComunesPDF.CENTER);
			
			
			} else if(tipoConsulta.equals("Consultar_2")){
				pdfDoc.addText(titulo,"formas",ComunesPDF.LEFT);
				
				
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				
				pdfDoc.setLTable(3, 100);
				pdfDoc.setLCell("Nombre Pyme","celda01",ComunesPDF.CENTER);	
				pdfDoc.setLCell("Nombre Representante Legal","celda01",ComunesPDF.CENTER);	
				pdfDoc.setLCell("Correo Electr�nico","celda01",ComunesPDF.CENTER);	
				
				UtilUsr  utilUsr = new UtilUsr();
				while (reg.next()) {
				
					String  nombrePyme  = (reg.getString("NOMBRE_PYME") == null) ? "" : reg.getString("NOMBRE_PYME");
					String  ic_pyme  = (reg.getString("IC_PYME") == null) ? "" : reg.getString("IC_PYME");
					
					List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(ic_pyme, "P");
					String nombreRepPyme = "";
					for (int i = 0; i < usuariosPorPerfil.size(); i++) {
						String loginUsrPyme = (String)usuariosPorPerfil.get(i);
						Usuario usuario = utilUsr.getUsuario(loginUsrPyme);
						String mail = usuario.getEmail();
						nombreRepPyme = usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre();
						
						pdfDoc.setLCell(nombrePyme,"formas",ComunesPDF.LEFT);	
						pdfDoc.setLCell(nombreRepPyme,"formas",ComunesPDF.LEFT);	
						pdfDoc.setLCell(mail,"formas",ComunesPDF.CENTER);
					}
				}
		
			} else if(tipoConsulta.equals("Consultar_3")){	

				pdfDoc.addText(titulo,"formas",ComunesPDF.LEFT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

				pdfDoc.setLTable(5, 100);
				pdfDoc.setLCell("Nombre Pyme","celda01",ComunesPDF.CENTER);	
				pdfDoc.setLCell("Nombre Representante Legal","celda01",ComunesPDF.CENTER);	
				pdfDoc.setLCell("Correo Electr�nico","celda01",ComunesPDF.CENTER);	
				pdfDoc.setLCell("Firma Convenio Extinci�n","celda01",ComunesPDF.CENTER);	
				pdfDoc.setLCell("Fecha de Firma","celda01",ComunesPDF.CENTER);	

				while (reg.next()){

					String nombrePyme          = (reg.getString("NOMBRE_PYME")                ==null)?"": reg.getString("NOMBRE_PYME");
					String nombreRepresentante = (reg.getString("NOMBRE_REPRESENTANTE_LEGAL") ==null)?"": reg.getString("NOMBRE_REPRESENTANTE_LEGAL");
					String correo              = (reg.getString("CORREO_ELECTRONICO")         ==null)?"": reg.getString("CORREO_ELECTRONICO");
					String firma_convenio      = (reg.getString("FIRMA_CONVENIO_EXTINCION")   ==null)?"": reg.getString("FIRMA_CONVENIO_EXTINCION");
					String fecha_firma         = (reg.getString("FECHA_FIRMA")                ==null)?"": reg.getString("FECHA_FIRMA");
					String ic_usuario          = (reg.getString("IC_USUARIO")                 ==null)?"": reg.getString("IC_USUARIO");

					HashMap repre = cesionBean.getRepresentantes(ic_usuario, "");
					nombreRepresentante = repre.get("NOMBRE_REPRESENTANTE_LEGAL").toString();
					correo = repre.get("CORREO_ELECTRONICO").toString();

					if(!fecha_firma.equals("")){
						firma_convenio = "FIRMADO";
					} else{
						firma_convenio = "";
					}

					pdfDoc.setLCell(nombrePyme,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(nombreRepresentante,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(correo,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(firma_convenio,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_firma,"formas",ComunesPDF.CENTER);

			}
			
			}
			
			pdfDoc.addLTable();
			pdfDoc.endDocument();
	
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}
		
		log.info("crearCustomFile (S)");
		return nombreArchivo;
	 }
	
	public List getConditions() {  return conditions;  }  
	public String getPaginaNo() { return paginaNo; 	}
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getIc_solicitud() {
		return ic_solicitud;
	}

	public void setIc_solicitud(String ic_solicitud) {
		this.ic_solicitud = ic_solicitud;
	}

	public String getTipoVisor() {
		return tipoVisor;
	}

	public void setTipoVisor(String tipoVisor) {
		this.tipoVisor = tipoVisor;
	}

	public String getTitulo() {
		return titulo;
	} 

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getTipoConsulta() {
		return tipoConsulta;
	}

	public void setTipoConsulta(String tipoConsulta) { 
		this.tipoConsulta = tipoConsulta;
	}


	public void setId_grupo(String id_grupo) {
		this.id_grupo = id_grupo;
	}


	public String getId_grupo() {
		return id_grupo;
	}
	

	
}