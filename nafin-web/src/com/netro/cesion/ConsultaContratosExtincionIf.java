package com.netro.cesion;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;


public class ConsultaContratosExtincionIf implements IQueryGeneratorReg, IQueryGeneratorRegExtJS {

	private static final Log log = ServiceLocator.getInstance().getLog(ConsultaContratosExtincionIf.class);//Variable para enviar mensajes al log.
	private StringBuffer strSQL;
	private List conditions;
	private String paginaOffset;
	private String paginaNo;
	private String clavePyme;
	private String claveEpo;
	private String claveIfCesionario;
	private String numeroContrato;
	private String claveMoneda;
	private String claveTipoContratacion;
	private String clasificacionEpo;
	private String campo1;
	private String campo2;
	private String campo3;
	private String campo4;
	private String campo5;
	private String indiceCamposAdicionales;
	private String clave_solicitud;
	private String ic_estatus;
	private String acuse;
	private String fechaCarga;
	private String usuarioCarga;
	private String fechaInicio;
	private String fechaFin;
	private String tipoExtincion;
	String horaCarga;

	//Constructor de la clase.
	public ConsultaContratosExtincionIf() {}

	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E) ::..");
		strSQL = new StringBuffer();
		log.info("getAggregateCalculationQuery(S) ::..");
		return strSQL.toString();
	}
		
	 /**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery() {
		log.info("getDocumentQuery(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();

		strSQL.append(" SELECT sol.ic_solicitud AS clave_solicitud");
		strSQL.append(" FROM cder_solicitud sol");
		strSQL.append(" WHERE sol.ic_estatus_solic IN (?) ");

		if("L".equals(tipoExtincion)){ // Fodea 04-2016
			conditions.add(new Integer(19));//19.- Extinci�n Solicitada
		} else{
			conditions.add(new Integer(17));
		}

		if (clavePyme != null && !"".equals(clavePyme)) {
			strSQL.append(" AND sol.ic_pyme = ?");
			conditions.add(new Long(clavePyme));
		}

		if (claveEpo != null && !"".equals(claveEpo)) {
			strSQL.append(" AND sol.ic_epo = ?");
			conditions.add(new Integer(claveEpo));
		}

		if (claveIfCesionario != null && !"".equals(claveIfCesionario)) {
			strSQL.append(" AND sol.ic_if = ?");
			conditions.add(new Integer(claveIfCesionario));
		}

		if (numeroContrato != null && !"".equals(numeroContrato)) {
			strSQL.append(" AND sol.cg_no_contrato = ?");
			conditions.add(numeroContrato);
		}

		if(!fechaInicio.equals("") && !fechaFin.equals("")){
			strSQL.append(" AND sol.DF_FIRMA_CONTRATO >= TO_DATE(?, 'DD/MM/YYYY') AND sol.DF_FIRMA_CONTRATO < TO_DATE(?, 'DD/MM/YYYY')+1 ");
			conditions.add(fechaInicio);
			conditions.add(fechaFin);
		}

		if (claveMoneda != null && !"".equals(claveMoneda)) {
			if (claveMoneda.equals("0")) {
				strSQL.append(" AND sol.cs_multimoneda = ?");
				conditions.add("S");
			} else {
				strSQL.append(" AND sol.cs_multimoneda = ?");
				strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
				strSQL.append(" FROM cder_monto_x_solic mxs");
				strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
				strSQL.append(" AND mxs.ic_moneda = ?)");
				conditions.add("N");
				conditions.add(new Integer(claveMoneda));
			}
		}

		if (claveTipoContratacion != null && !"".equals(claveTipoContratacion)) {
			strSQL.append(" AND sol.ic_tipo_contratacion = ?");
			conditions.add(new Integer(claveTipoContratacion));
		}		

		strSQL.append(" ORDER BY sol.ic_solicitud");
		
		log.debug("..:: strSQL: "+strSQL.toString());
		log.debug("..:: conditions: "+conditions);
		
		log.info("getDocumentQuery(S) ::..");
		return strSQL.toString();
	}

	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		log.info("getDocumentSummaryQueryForIds(E) ::..");
		strSQL = new StringBuffer();  
		conditions = new ArrayList();

		strSQL.append(" SELECT sol.ic_solicitud AS clave_solicitud,"); 
		strSQL.append(" sol.ic_pyme AS clave_pyme,");
		strSQL.append(" sol.ic_epo AS clave_epo,");
		strSQL.append(" sol.ic_if AS clave_if,");
		strSQL.append(" sol.ic_tipo_contratacion AS clave_contratacion,");
		strSQL.append(" epo.cg_razon_social AS dependencia,");
		strSQL.append(" pyme.cg_razon_social AS nombre_cedente,");
		strSQL.append(" SOL.CG_EMPRESAS_REPRESENTADAS AS empresas,");//FODEA-024-2014 MOD(ADMIN IF G-1,G-3)
		strSQL.append(" pyme.cg_rfc AS rfc_cedente,");
		strSQL.append(" cif.cg_razon_social AS nombre_cesionario,");
		strSQL.append(" sol.cg_no_contrato AS numero_contrato,");
		strSQL.append(" con.cg_nombre AS tipo_contratacion,");
		strSQL.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");
		strSQL.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");
		strSQL.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS plazo_contrato,");
		strSQL.append(" cl.cg_area AS clasificacion_epo,");
		strSQL.append(" sol.cg_campo1 AS campo_adicional_1,");
		strSQL.append(" sol.cg_campo2 AS campo_adicional_2,");
		strSQL.append(" sol.cg_campo3 AS campo_adicional_3,");
		strSQL.append(" sol.cg_campo4 AS campo_adicional_4,");
		strSQL.append(" sol.cg_campo5 AS campo_adicional_5,");
		strSQL.append(" sol.cg_obj_contrato AS objeto_contrato,");
		strSQL.append(" sol.cg_comentarios AS comentarios,");
		//strSQL.append(" TO_CHAR(extc.df_extincion_contrato, 'dd/mm/yyyy') AS fecha_extincion,");
		strSQL.append(" DECODE (extc.df_extincion_contrato, NULL, TO_CHAR (Sysdate, 'dd/mm/yyyy'), TO_CHAR (extc.df_extincion_contrato, 'dd/mm/yyyy')) AS fecha_extincion,"); // Fodea 04-2016
		if("L".equals(tipoExtincion)){
			strSQL.append(" extc.cg_cuenta_ext AS numero_cuenta,");
			strSQL.append(" extc.cg_cuenta_clabe_ext AS cuenta_clabe,");
			strSQL.append(" extc.cg_banco_deposito_ext AS banco_deposito,");
		} else{
			strSQL.append(" sol.cg_campo1 AS numero_cuenta,");
			strSQL.append(" sol.cg_campo2 AS cuenta_clabe,");
			strSQL.append(" sol.cg_campo3 AS banco_deposito,");
		}
		strSQL.append(" DECODE (extc.cg_causa_rechazo_ext, null, 'N/A', extc.cg_causa_rechazo_ext) AS causas_rechazo,");
		strSQL.append(" DECODE(sol.ic_estatus_solic, 17, 'Por Solicitar', es.cg_descripcion) AS estatus_solicitud,");
		strSQL.append(" sol.ic_estatus_solic AS clave_estatus, ");
		strSQL.append(" '' AS REPRESENTATE_LEGAL, ");	
		strSQL.append(" '' AS MONTO_MONEDA, ");
		strSQL.append(" '' AS COLUMNA_ACCION,  ");	
		strSQL.append(" CC_ACUSE_EXT_IF AS CC_ACUSE_EXT_IF ,  ");	
		strSQL.append(" CC_ACUSE_EXT_IF2 AS CC_ACUSE_EXT_IF2 ");	 
		strSQL.append(" ,sol.IC_GRUPO_CESION  AS GRUPO_CESION  ");	 

		strSQL.append(" FROM cder_solicitud sol");
		strSQL.append(", comcat_epo epo");
		strSQL.append(", comcat_if cif");
		strSQL.append(", comcat_pyme pyme");
		strSQL.append(", cdercat_clasificacion_epo cl");
		strSQL.append(", cdercat_tipo_contratacion con");
		strSQL.append(", cdercat_estatus_solic es");
		strSQL.append(", cdercat_tipo_plazo stp");
		strSQL.append(", cder_extincion_contrato extc");
		strSQL.append(" WHERE sol.ic_epo = epo.ic_epo");
		strSQL.append(" AND sol.ic_if = cif.ic_if ");
		strSQL.append(" AND sol.ic_pyme = pyme.ic_pyme ");
		strSQL.append(" AND sol.ic_clasificacion_epo = cl.ic_clasificacion_epo(+)");
		strSQL.append(" AND sol.ic_tipo_contratacion = con.ic_tipo_contratacion");
		strSQL.append(" AND sol.ic_estatus_solic = es.ic_estatus_solic");
		strSQL.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
		strSQL.append(" AND sol.ic_solicitud = extc.ic_solicitud(+)");
		strSQL.append(" AND (");

		for (int i = 0; i < pageIds.size(); i++) {
			List lItem = (ArrayList)pageIds.get(i);

			if(i > 0){strSQL.append("  OR  ");}

			strSQL.append("sol.ic_solicitud = ?");
			conditions.add(new Long(lItem.get(0).toString()));
		}

		strSQL.append(" ) ");
		strSQL.append(" ORDER BY sol.df_alta_solicitud DESC");

		log.debug("..:: strSQL: "+strSQL.toString());
		log.debug("..:: conditions: "+conditions);

		log.info("getDocumentSummaryQueryForIds(S) ::..");
		return strSQL.toString();
	}

	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();
	
		strSQL.append(" SELECT sol.ic_solicitud AS clave_solicitud,"); 
		strSQL.append(" sol.ic_pyme AS clave_pyme,");
		strSQL.append(" sol.ic_epo AS clave_epo,");
		strSQL.append(" sol.ic_if AS clave_if,");
		strSQL.append(" sol.ic_tipo_contratacion AS clave_contratacion,");
		strSQL.append(" epo.cg_razon_social AS dependencia,");
		strSQL.append(" pyme.cg_razon_social AS nombre_cedente,");
		strSQL.append(" SOL.CG_EMPRESAS_REPRESENTADAS AS empresas,");//FODEA-024-2014 MOD(ADMIN IF G-2)
		strSQL.append(" pyme.cg_rfc AS rfc_cedente,");
		strSQL.append(" cif.cg_razon_social AS nombre_cesionario,");
		strSQL.append(" sol.cg_no_contrato AS numero_contrato,");
		strSQL.append(" con.cg_nombre AS tipo_contratacion,");
		strSQL.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");
		strSQL.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");
		strSQL.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS plazo_contrato,");
		strSQL.append(" cl.cg_area AS clasificacion_epo,");
		strSQL.append(" sol.cg_campo1 AS campo_adicional_1,");
		strSQL.append(" sol.cg_campo2 AS campo_adicional_2,");
		strSQL.append(" sol.cg_campo3 AS campo_adicional_3,");
		strSQL.append(" sol.cg_campo4 AS campo_adicional_4,");
		strSQL.append(" sol.cg_campo5 AS campo_adicional_5,");
		strSQL.append(" sol.cg_obj_contrato AS objeto_contrato,");
		strSQL.append(" sol.cg_comentarios AS comentarios,");
		//strSQL.append(" TO_CHAR(extc.df_extincion_contrato, 'dd/mm/yyyy') AS fecha_extincion,");
		strSQL.append(" DECODE (extc.df_extincion_contrato, NULL, TO_CHAR (Sysdate, 'dd/mm/yyyy'), TO_CHAR (extc.df_extincion_contrato, 'dd/mm/yyyy')) AS fecha_extincion,"); // Fodea 28-2015
		if("L".equals(tipoExtincion)){
			strSQL.append(" extc.cg_cuenta_ext AS numero_cuenta,");
			strSQL.append(" extc.cg_cuenta_clabe_ext AS cuenta_clabe,");
			strSQL.append(" extc.cg_banco_deposito_ext AS banco_deposito,");
		} else{
			strSQL.append(" sol.cg_campo1 AS numero_cuenta,");
			strSQL.append(" sol.cg_campo2 AS cuenta_clabe,");
			strSQL.append(" sol.cg_campo3 AS banco_deposito,");
		}
		strSQL.append(" DECODE (extc.cg_causa_rechazo_ext, null, 'N/A', extc.cg_causa_rechazo_ext) AS causas_rechazo,");		
		strSQL.append(" DECODE(sol.ic_estatus_solic, 17, 'Por Solicitar', es.cg_descripcion) AS estatus_solicitud,  ");		
		strSQL.append(" sol.ic_estatus_solic AS clave_estatus, ");
		strSQL.append(" '' AS REPRESENTATE_LEGAL, ");	
		strSQL.append(" '' AS MONTO_MONEDA, ");
		strSQL.append(" '' AS COLUMNA_ACCION ");		
		strSQL.append(" FROM cder_solicitud sol");
		strSQL.append(", comcat_epo epo");
		strSQL.append(", comcat_if cif");
		strSQL.append(", comcat_pyme pyme");
		strSQL.append(", cdercat_clasificacion_epo cl");
		strSQL.append(", cdercat_tipo_contratacion con");
		strSQL.append(", cdercat_estatus_solic es");
		strSQL.append(", cdercat_tipo_plazo stp");
		strSQL.append(", cder_extincion_contrato extc");
		strSQL.append(" WHERE sol.ic_epo = epo.ic_epo");
		strSQL.append(" AND sol.ic_if = cif.ic_if ");
		strSQL.append(" AND sol.ic_pyme = pyme.ic_pyme ");
		strSQL.append(" AND sol.ic_clasificacion_epo = cl.ic_clasificacion_epo(+)");
		strSQL.append(" AND sol.ic_tipo_contratacion = con.ic_tipo_contratacion");
		strSQL.append(" AND sol.ic_estatus_solic = es.ic_estatus_solic");
		strSQL.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
		strSQL.append(" AND sol.ic_solicitud = extc.ic_solicitud(+)"); 

		if ("".equals(ic_estatus)) {
		strSQL.append(" AND sol.ic_estatus_solic IN (?)");
			if("L".equals(tipoExtincion)){ // Fodea 04-2016
				conditions.add(new Integer(19)); //19.- Extinci�n Solicitada
			} else{
				conditions.add(new Integer(17));
			}
		}

		if (ic_estatus != null && !"".equals(ic_estatus)) {
			strSQL.append(" AND sol.ic_estatus_solic IN (?)");
			conditions.add(new Integer(ic_estatus));
		}

		if (clavePyme != null && !"".equals(clavePyme)) {
			strSQL.append(" AND sol.ic_pyme = ?");
			conditions.add(new Long(clavePyme));
		}

		if (claveEpo != null && !"".equals(claveEpo)) {
			strSQL.append(" AND sol.ic_epo = ?");
			conditions.add(new Integer(claveEpo));
		}

		if (claveIfCesionario != null && !"".equals(claveIfCesionario)) {
			strSQL.append(" AND sol.ic_if = ?");
			conditions.add(new Integer(claveIfCesionario));
		}

		if (numeroContrato != null && !"".equals(numeroContrato)) {
			strSQL.append(" AND sol.cg_no_contrato = ?");
			conditions.add(numeroContrato);
		}

		if (claveMoneda != null && !"".equals(claveMoneda)) {
			if (claveMoneda.equals("0")) {
				strSQL.append(" AND sol.cs_multimoneda = ?");
				conditions.add("S");
			} else {
				strSQL.append(" AND sol.cs_multimoneda = ?");
				strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
				strSQL.append(" FROM cder_monto_x_solic mxs");
				strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
				strSQL.append(" AND mxs.ic_moneda = ?)");
				conditions.add("N");
				conditions.add(new Integer(claveMoneda));
			}
		}

		if (claveTipoContratacion != null && !"".equals(claveTipoContratacion)) {
			strSQL.append(" AND sol.ic_tipo_contratacion = ?");
			conditions.add(new Integer(claveTipoContratacion));
		}		

		if (clave_solicitud != null && !"".equals(clave_solicitud)) {
			strSQL.append(" AND sol.ic_solicitud = ?");
			conditions.add(new Long(clave_solicitud));
		}
		
		if(!fechaInicio.equals("") && !fechaFin.equals("")){
			strSQL.append(" AND sol.DF_FIRMA_CONTRATO >= TO_DATE(?, 'DD/MM/YYYY') AND sol.DF_FIRMA_CONTRATO < TO_DATE(?, 'DD/MM/YYYY')+1 ");
			conditions.add(fechaInicio);
			conditions.add(fechaFin);
		}

		strSQL.append(" ORDER BY sol.ic_solicitud");

		log.info("strSQL(S) ::.."+strSQL);
		log.info("conditions(S) ::.."+conditions);

		log.info("getDocumentQuery(S) ::..");
		return strSQL.toString();
	}//getDocumentQueryFile  

	/***MIGRACION IF ***/
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		try {
		
			CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);

			if(tipo.equals("PDF") ) {
			
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString()+"", 
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),  
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
				int columnas=5- Integer.parseInt(indiceCamposAdicionales);
				
				pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);				
				pdfDoc.setLTable(13,100);
				pdfDoc.setLCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Dependencia","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Pyme (Cedente)","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("RFC","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Representante Legal","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("No. Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto / Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de Contrataci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Inicio Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Final Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo del Contrato","celda01",ComunesPDF.CENTER);
				if (clasificacionEpo != null && !clasificacionEpo.equals("")) {
					pdfDoc.setLCell(clasificacionEpo,"celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);
				}else {
					pdfDoc.setLCell("","celda01",ComunesPDF.CENTER,2);
					
				}
				
				pdfDoc.setLCell("B","celda01",ComunesPDF.CENTER);
				if (indiceCamposAdicionales != null && !indiceCamposAdicionales.equals("0")) {
					if (campo1 != null && !campo1.equals("")) { pdfDoc.setLCell(campo1,"celda01",ComunesPDF.CENTER); }
					if (campo2 != null && !campo2.equals("")) { pdfDoc.setLCell(campo2,"celda01",ComunesPDF.CENTER); }
					if (campo3 != null && !campo3.equals("")) { pdfDoc.setLCell(campo3,"celda01",ComunesPDF.CENTER); }
					if (campo4 != null && !campo4.equals("")) { pdfDoc.setLCell(campo4,"celda01",ComunesPDF.CENTER); }
					if (campo5 != null && !campo5.equals("")) { pdfDoc.setLCell(campo5,"celda01",ComunesPDF.CENTER); }
				}				
				pdfDoc.setLCell("Objeto del Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Extinci�n del Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Cuenta","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Cuenta CLABE","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Banco de Dep�sito","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Causa del Rechazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Estatus Actual","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell(" ","celda01",ComunesPDF.CENTER, columnas);
				
				while (rs.next())	{					
									
					String dependencia =(rs.getString("dependencia") == null) ? "" : rs.getString("dependencia");
					String nombre_cedente =(rs.getString("nombre_cedente") == null) ? "" : rs.getString("nombre_cedente");
					String empresas =(rs.getString("empresas") == null) ? "" : rs.getString("empresas");
					String rfc_cedente =(rs.getString("rfc_cedente") == null) ? "" : rs.getString("rfc_cedente");
					String numero_contrato =(rs.getString("numero_contrato") == null) ? "" : rs.getString("numero_contrato");
					String tipo_contratacion =(rs.getString("tipo_contratacion") == null) ? "" : rs.getString("tipo_contratacion");
					String fecha_inicio_contrato =(rs.getString("fecha_inicio_contrato") == null) ? "" : rs.getString("fecha_inicio_contrato");
					String fecha_fin_contrato =(rs.getString("fecha_fin_contrato") == null) ? "" : rs.getString("fecha_fin_contrato");
					String plazo_contrato =(rs.getString("plazo_contrato") == null) ? "" : rs.getString("plazo_contrato");
					String clasificacion_epo =(rs.getString("clasificacion_epo") == null) ? "" : rs.getString("clasificacion_epo");
					String campo_adicional_1 =(rs.getString("campo_adicional_1") == null) ? "" : rs.getString("campo_adicional_1");
					String campo_adicional_2 =(rs.getString("campo_adicional_2") == null) ? "" : rs.getString("campo_adicional_2");
					String campo_adicional_3 =(rs.getString("campo_adicional_3") == null) ? "" : rs.getString("campo_adicional_3");
					String campo_adicional_4 =(rs.getString("campo_adicional_4") == null) ? "" : rs.getString("campo_adicional_4");
					String campo_adicional_5 =(rs.getString("campo_adicional_5") == null) ? "" : rs.getString("campo_adicional_5");
					String fecha_extincion =(rs.getString("fecha_extincion") == null) ? "" : rs.getString("fecha_extincion");
					String numero_cuenta =(rs.getString("numero_cuenta") == null) ? "" : rs.getString("numero_cuenta");
					String cuenta_clabe =(rs.getString("cuenta_clabe") == null) ? "" : rs.getString("cuenta_clabe");
					String banco_deposito =(rs.getString("banco_deposito") == null) ? "" : rs.getString("banco_deposito");
					String causas_rechazo =(rs.getString("causas_rechazo") == null) ? "" : rs.getString("causas_rechazo");
					String estatus_solicitud =(rs.getString("estatus_solicitud") == null) ? "" : rs.getString("estatus_solicitud");
					String clave_solicitud =(rs.getString("clave_solicitud") == null) ? "" : rs.getString("clave_solicitud");
					String objeto_contrato =(rs.getString("objeto_contrato") == null) ? "" : rs.getString("objeto_contrato");
					StringBuffer montosMonedaSol = cesionBean.getMontoMoneda(clave_solicitud);
					
					//Se obtiene el representante legal de la Pyme
					String noProveedor = (rs.getString("clave_pyme")==null)?"":(rs.getString("clave_pyme"));
					StringBuffer nombreContacto = new StringBuffer();
					UtilUsr utilUsr = new UtilUsr();
					boolean usuarioEncontrado = false;
					boolean perfilIndicado = false;
					List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(noProveedor, "P");
					for(int i=0;i<usuariosPorPerfil.size();i++){
						String loginUsuarioEpo = (String)usuariosPorPerfil.get(i);
						Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
						perfilIndicado = true;
						if(i>0){
							nombreContacto.append(" / ");
						}
						nombreContacto.append(usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno()+" ");
						nombreContacto.append("\n");
					}
					String pymeEmpresas = nombre_cedente+((!"".equals(empresas))?";"+empresas:"");
					pdfDoc.setLCell("A","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(dependencia.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(pymeEmpresas.replaceAll(",", "").replaceAll(";","\n\n"),"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(rfc_cedente.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(nombreContacto.toString().replaceAll("\n", "").replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(numero_contrato.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(montosMonedaSol.toString().replaceAll("<br/>", "\n").replaceAll(",", ""),"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(tipo_contratacion.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_inicio_contrato.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_fin_contrato.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(plazo_contrato.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					if (clasificacionEpo != null && !clasificacionEpo.equals("")) {
						pdfDoc.setLCell(clasificacion_epo.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
					}else {
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER,2);
					}
					
					
					pdfDoc.setLCell("B","formas",ComunesPDF.CENTER);
					if (indiceCamposAdicionales != null && !indiceCamposAdicionales.equals("0")) {
						if (campo1 != null && !campo1.equals("")) { pdfDoc.setLCell(campo_adicional_1.replaceAll(",", ""),"formas",ComunesPDF.CENTER); }
						if (campo2 != null && !campo2.equals("")) { pdfDoc.setLCell(campo_adicional_2.replaceAll(",", ""),"formas",ComunesPDF.CENTER); }
						if (campo3 != null && !campo3.equals("")) { pdfDoc.setLCell(campo_adicional_3.replaceAll(",", ""),"formas",ComunesPDF.CENTER); }
						if (campo4 != null && !campo4.equals("")) { pdfDoc.setLCell(campo_adicional_4.replaceAll(",", ""),"formas",ComunesPDF.CENTER); }
						if (campo5 != null && !campo5.equals("")) { pdfDoc.setLCell(campo_adicional_5.replaceAll(",", ""),"formas",ComunesPDF.CENTER); }
					}
					
					pdfDoc.setLCell(objeto_contrato.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_extincion.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(numero_cuenta.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(cuenta_clabe.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(banco_deposito.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(causas_rechazo.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(estatus_solicitud.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("","formas",ComunesPDF.CENTER,columnas);   
							    
				}//while (rs.next())	{
				
				pdfDoc.addLTable();
				pdfDoc.endDocument();	
				
			}
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {

	  String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		int columnas=5- Integer.parseInt(indiceCamposAdicionales);
		
		try {
		
			CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);

		  	if(tipo.equals("CSV") ) {
				contenidoArchivo.append("Dependencia,");
				contenidoArchivo.append("Pyme (Cedente),");
				contenidoArchivo.append("RFC,");
				contenidoArchivo.append("Representante Legal,");
				contenidoArchivo.append("No. Contrato,");
				contenidoArchivo.append("Monto / Moneda,");
				contenidoArchivo.append("Tipo de Contrataci�n,");
				contenidoArchivo.append("Fecha Inicio Contrato,");
				contenidoArchivo.append("Fecha Final Contrato,");
				contenidoArchivo.append("Plazo del Contrato,");
				if (clasificacionEpo != null && !clasificacionEpo.equals("")) { 		contenidoArchivo.append(clasificacionEpo+","); 			}
				if (campo1 != null && !campo1.equals("")) { contenidoArchivo.append(campo1+","); }
				if (campo2 != null && !campo2.equals("")) { contenidoArchivo.append(campo2+","); }
				if (campo3 != null && !campo3.equals("")) { contenidoArchivo.append(campo3+","); }
				if (campo4 != null && !campo4.equals("")) { contenidoArchivo.append(campo4+","); }
				if (campo5 != null && !campo5.equals("")) { contenidoArchivo.append(campo5+","); }				
				contenidoArchivo.append("Objeto del Contrato,");
				contenidoArchivo.append("Fecha de Extinci�n del Contrato,");
				contenidoArchivo.append("Cuenta,");
				contenidoArchivo.append("Cuenta CLABE,");
				contenidoArchivo.append("Banco de Dep�sito,");
				contenidoArchivo.append("Causa del Rechazo,");
				contenidoArchivo.append("Estatus Actual\n");
			}	 
			
			if(tipo.equals("PDF") ) {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);			
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString()+"", 
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),  
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				
				pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);				
				pdfDoc.setLTable(13,100);
				pdfDoc.setLCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Dependencia","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Pyme (Cedente)","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("RFC","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Representante Legal","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("No. Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto / Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de Contrataci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Inicio Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Final Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo del Contrato","celda01",ComunesPDF.CENTER);
				if (clasificacionEpo != null && !clasificacionEpo.equals("")) {
					pdfDoc.setLCell(clasificacionEpo,"celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);
				}else {
					pdfDoc.setLCell("","celda01",ComunesPDF.CENTER,2);
					
				}
				
				pdfDoc.setLCell("B","celda01",ComunesPDF.CENTER);
				if (indiceCamposAdicionales != null && !indiceCamposAdicionales.equals("0")) {
					if (campo1 != null && !campo1.equals("")) { pdfDoc.setLCell(campo1,"celda01",ComunesPDF.CENTER); }
					if (campo2 != null && !campo2.equals("")) { pdfDoc.setLCell(campo2,"celda01",ComunesPDF.CENTER); }
					if (campo3 != null && !campo3.equals("")) { pdfDoc.setLCell(campo3,"celda01",ComunesPDF.CENTER); }
					if (campo4 != null && !campo4.equals("")) { pdfDoc.setLCell(campo4,"celda01",ComunesPDF.CENTER); }
					if (campo5 != null && !campo5.equals("")) { pdfDoc.setLCell(campo5,"celda01",ComunesPDF.CENTER); }
				}				
				pdfDoc.setLCell("Objeto del Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Extinci�n del Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Cuenta","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Cuenta CLABE","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Banco de Dep�sito","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Causa del Rechazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Estatus Actual","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell(" ","celda01",ComunesPDF.CENTER, columnas);
			}
				
			while (rs.next())	{					
				
				
				String dependencia =(rs.getString("dependencia") == null) ? "" : rs.getString("dependencia");
				String nombre_cedente =(rs.getString("nombre_cedente") == null) ? "" : rs.getString("nombre_cedente");
				String empresas =(rs.getString("empresas") == null) ? "" : rs.getString("empresas");
				//FODEA-024-2014 MOD()
					nombre_cedente=nombre_cedente+((!"".equals(empresas))?";"+empresas:"");
					
				String rfc_cedente =(rs.getString("rfc_cedente") == null) ? "" : rs.getString("rfc_cedente");
				String numero_contrato =(rs.getString("numero_contrato") == null) ? "" : rs.getString("numero_contrato");
				String tipo_contratacion =(rs.getString("tipo_contratacion") == null) ? "" : rs.getString("tipo_contratacion");
				String fecha_inicio_contrato =(rs.getString("fecha_inicio_contrato") == null) ? "" : rs.getString("fecha_inicio_contrato");
				String fecha_fin_contrato =(rs.getString("fecha_fin_contrato") == null) ? "" : rs.getString("fecha_fin_contrato");
				String plazo_contrato =(rs.getString("plazo_contrato") == null) ? "" : rs.getString("plazo_contrato");
				String clasificacion_epo =(rs.getString("clasificacion_epo") == null) ? "" : rs.getString("clasificacion_epo");
				String campo_adicional_1 =(rs.getString("campo_adicional_1") == null) ? "" : rs.getString("campo_adicional_1");
				String campo_adicional_2 =(rs.getString("campo_adicional_2") == null) ? "" : rs.getString("campo_adicional_2");
				String campo_adicional_3 =(rs.getString("campo_adicional_3") == null) ? "" : rs.getString("campo_adicional_3");
				String campo_adicional_4 =(rs.getString("campo_adicional_4") == null) ? "" : rs.getString("campo_adicional_4");
				String campo_adicional_5 =(rs.getString("campo_adicional_5") == null) ? "" : rs.getString("campo_adicional_5");
				String fecha_extincion =(rs.getString("fecha_extincion") == null) ? "" : rs.getString("fecha_extincion");
				String numero_cuenta =(rs.getString("numero_cuenta") == null) ? "" : rs.getString("numero_cuenta");
				String cuenta_clabe =(rs.getString("cuenta_clabe") == null) ? "" : rs.getString("cuenta_clabe");
				String banco_deposito =(rs.getString("banco_deposito") == null) ? "" : rs.getString("banco_deposito");
				String causas_rechazo =(rs.getString("causas_rechazo") == null) ? "" : rs.getString("causas_rechazo");
				String estatus_solicitud =(rs.getString("estatus_solicitud") == null) ? "" : rs.getString("estatus_solicitud");
				String clave_solicitud =(rs.getString("clave_solicitud") == null) ? "" : rs.getString("clave_solicitud");
				String objeto_contrato =(rs.getString("objeto_contrato") == null) ? "" : rs.getString("objeto_contrato");
				StringBuffer montosMonedaSol = cesionBean.getMontoMoneda(clave_solicitud);
					
				//Se obtiene el representante legal de la Pyme
				String noProveedor = (rs.getString("clave_pyme")==null)?"":(rs.getString("clave_pyme"));
				StringBuffer nombreContacto = new StringBuffer();
				UtilUsr utilUsr = new UtilUsr();
				boolean usuarioEncontrado = false;
				boolean perfilIndicado = false;
				List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(noProveedor, "P");
				for(int i=0;i<usuariosPorPerfil.size();i++){
					String loginUsuarioEpo = (String)usuariosPorPerfil.get(i);
					Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
					perfilIndicado = true;
					if(i>0){
						nombreContacto.append(" / ");
					}
					nombreContacto.append(usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno()+" ");
					nombreContacto.append("\n");
				}
				
				if(tipo.equals("CSV") ) {	
					contenidoArchivo.append(dependencia.replaceAll(",", "")+",");
					contenidoArchivo.append(nombre_cedente.replaceAll(",", "")+",");
					contenidoArchivo.append(rfc_cedente.replaceAll(",", "")+",");
					contenidoArchivo.append(nombreContacto.toString().replaceAll("\n", "").replaceAll(",", "")+",");
					contenidoArchivo.append(numero_contrato.replaceAll(",", "")+",");
					contenidoArchivo.append(montosMonedaSol.toString().replaceAll("<br/>", "|").replaceAll(",", "")+",");
					contenidoArchivo.append(tipo_contratacion.replaceAll(",", "")+",");
					contenidoArchivo.append(fecha_inicio_contrato.replaceAll(",", "")+",");
					contenidoArchivo.append(fecha_fin_contrato.replaceAll(",", "")+",");
					contenidoArchivo.append(plazo_contrato.replaceAll(",", "")+",");
					if (clasificacionEpo != null && !clasificacionEpo.equals("")) {
						contenidoArchivo.append(clasificacion_epo.replaceAll(",", "")+",");
					}
					if (campo1 != null && !campo1.equals("")) { contenidoArchivo.append("'"+campo_adicional_1.replaceAll(",", "")+","); }
					if (campo2 != null && !campo2.equals("")) { contenidoArchivo.append("'"+campo_adicional_2.replaceAll(",", "")+","); }
					if (campo3 != null && !campo3.equals("")) { contenidoArchivo.append(campo_adicional_3.replaceAll(",", "")+","); }
					if (campo4 != null && !campo4.equals("")) { contenidoArchivo.append(campo_adicional_4.replaceAll(",", "")+","); }
					if (campo5 != null && !campo5.equals("")) { contenidoArchivo.append(campo_adicional_5.replaceAll(",", "")+","); }
					contenidoArchivo.append(objeto_contrato.replaceAll(",", "")+",");
					contenidoArchivo.append(fecha_extincion.replaceAll(",", "")+",");
					contenidoArchivo.append("'"+numero_cuenta.replaceAll(",", "")+",");
					contenidoArchivo.append("'"+cuenta_clabe.replaceAll(",", "")+",");
					contenidoArchivo.append(banco_deposito.replaceAll(",", "")+",");
					contenidoArchivo.append(causas_rechazo.replaceAll(",", "")+",");
					contenidoArchivo.append(estatus_solicitud.replaceAll(",", "")+",");
					contenidoArchivo.append("\n");					
				}
				
				if(tipo.equals("PDF") ) {
					pdfDoc.setLCell("A","formas",ComunesPDF.CENTER); 
					pdfDoc.setLCell(dependencia.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(nombre_cedente.replaceAll(";", "\n\n"),"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(rfc_cedente.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(nombreContacto.toString().replaceAll("\n", "").replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(numero_contrato.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(montosMonedaSol.toString().replaceAll("<br/>", "\n").replaceAll(",", ""),"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(tipo_contratacion.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_inicio_contrato.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_fin_contrato.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(plazo_contrato.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					if (clasificacionEpo != null && !clasificacionEpo.equals("")) {
						pdfDoc.setLCell(clasificacion_epo.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
					}else {
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER,2);
					}
					pdfDoc.setLCell("B","formas",ComunesPDF.CENTER);
					if (indiceCamposAdicionales != null && !indiceCamposAdicionales.equals("0")) {
						if (campo1 != null && !campo1.equals("")) { pdfDoc.setLCell(campo_adicional_1.replaceAll(",", ""),"formas",ComunesPDF.CENTER); }
						if (campo2 != null && !campo2.equals("")) { pdfDoc.setLCell(campo_adicional_2.replaceAll(",", ""),"formas",ComunesPDF.CENTER); }
						if (campo3 != null && !campo3.equals("")) { pdfDoc.setLCell(campo_adicional_3.replaceAll(",", ""),"formas",ComunesPDF.CENTER); }
						if (campo4 != null && !campo4.equals("")) { pdfDoc.setLCell(campo_adicional_4.replaceAll(",", ""),"formas",ComunesPDF.CENTER); }
						if (campo5 != null && !campo5.equals("")) { pdfDoc.setLCell(campo_adicional_5.replaceAll(",", ""),"formas",ComunesPDF.CENTER); }
					}					
					pdfDoc.setLCell(objeto_contrato.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_extincion.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(numero_cuenta.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(cuenta_clabe.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(banco_deposito.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(causas_rechazo.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(estatus_solicitud.replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("","formas",ComunesPDF.CENTER,columnas);  
				
				}
			}//while (rs.next())	{  
			
			if(tipo.equals("CSV") ) {	
				creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
				nombreArchivo = creaArchivo.getNombre();
			}
			if(tipo.equals("PDF") ) {	  
				pdfDoc.addLTable();
				
				pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);	
				
				pdfDoc.setLTable(2,50);
				pdfDoc.setLCell("Cifras de Control ","celda01",ComunesPDF.CENTER,2);
				pdfDoc.setLCell("N�mero de Acuse","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell(acuse,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Carga","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell(fechaCarga,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell("Hora de Carga","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell(horaCarga,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell("Usuario de Captura","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell(usuarioCarga,"formas",ComunesPDF.CENTER);
				pdfDoc.addLTable();   
				
				pdfDoc.setLTable(1,80);
				pdfDoc.setLCell("Con base a lo dispuesto en el C�digo de Comercio por este conducto me permito notificar a esa Entidad "+
				"la Extinci�n del Contrato de Cesi�n de Derechos de Cobro que fue formalizado con la(s) empresa(s) "+
				"CEDENTE respecto al contrato que aqu� mismo se detalla.\n\n"+
				"Por lo anterior, me permito solicitar a usted, se registre la Extinci�n del Contrato de Cesi�n de "+
				"Derechos de Cobro de referencia a efecto de que esa Empresa, realice el(los) pago(s) derivado(s) del "+
				"Contrato aqu� se�alado a favor del CEDENTE.\n\n"+
				"Los datos de la Cuenta bancaria del CEDENTE en la que se deber�n depositar los pagos derivados de "+
				"esta operaci�n son los que aqu� se detallan.\n\n"+
				"Sobre el particular, por este medio les notifico, la Extinci�n del Contrato de Cesi�n de Derechos "+
				"de Cobro antes mencionado, para todos los efectos legales a que haya lugar.","formas",ComunesPDF.JUSTIFIED);
				pdfDoc.addLTable();				
				
				pdfDoc.endDocument();
			}
				
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	    
	//Getters
	public List getConditions() {  return conditions;  }  
	public String getPaginaNo() {return paginaNo;}
	public String getPaginaOffset() {return paginaOffset;}
	public String getClavePyme() {return this.clavePyme;}
	public String getClaveEpo() {return this.claveEpo;}
	public String getClaveIfCesionario() {return this.claveIfCesionario;}
	public String getNumeroContrato() {return this.numeroContrato;}
	public String getClaveMoneda() {return this.claveMoneda;}
	public String getClaveTipoContratacion() {return this.claveTipoContratacion;}
	
	//Setters
	public void setPaginaNo(String newPaginaNo) {this.paginaNo = newPaginaNo;  }
	public void setPaginaOffset(String paginaOffset) {this.paginaOffset=paginaOffset;}
	public void setClavePyme(String clavePyme) {this.clavePyme = clavePyme;}
	public void setClaveEpo(String claveEpo) {this.claveEpo = claveEpo;}
	public void setClaveIfCesionario(String claveIfCesionario) {this.claveIfCesionario = claveIfCesionario;}
	public void setNumeroContrato(String numeroContrato) {this.numeroContrato = numeroContrato;}
	public void setClaveMoneda(String claveMoneda) {this.claveMoneda = claveMoneda;}
	public void setClaveTipoContratacion(String claveTipoContratacion) {this.claveTipoContratacion = claveTipoContratacion;}

	public String getClasificacionEpo() {
		return clasificacionEpo;
	}

	public void setClasificacionEpo(String clasificacionEpo) {
		this.clasificacionEpo = clasificacionEpo;
	}

	public String getCampo1() {
		return campo1;
	}

	public void setCampo1(String campo1) {
		this.campo1 = campo1;
	}

	public String getCampo2() {
		return campo2;
	}

	public void setCampo2(String campo2) {
		this.campo2 = campo2;
	}

	public String getCampo3() {
		return campo3;
	}

	public void setCampo3(String campo3) {
		this.campo3 = campo3;
	}

	public String getCampo4() {
		return campo4;
	}

	public void setCampo4(String campo4) {
		this.campo4 = campo4;
	}

	public String getCampo5() {
		return campo5;
	}

	public void setCampo5(String campo5) {
		this.campo5 = campo5;
	}

	public String getIndiceCamposAdicionales() {
		return indiceCamposAdicionales;
	}

	public void setIndiceCamposAdicionales(String indiceCamposAdicionales) {
		this.indiceCamposAdicionales = indiceCamposAdicionales;
	}

	public String getClave_solicitud() {
		return clave_solicitud;
	}

	public void setClave_solicitud(String clave_solicitud) {
		this.clave_solicitud = clave_solicitud;
	}

	public String getIc_estatus() {
		return ic_estatus;  
	}

	public void setIc_estatus(String ic_estatus) {
		this.ic_estatus = ic_estatus;
	}

	public String getAcuse() {
		return acuse;
	}

	public void setAcuse(String acuse) {
		this.acuse = acuse;
	}

	public String getFechaCarga() {
		return fechaCarga;
	}

	public void setFechaCarga(String fechaCarga) {
		this.fechaCarga = fechaCarga;
	}

	public String getHoraCarga() {
		return horaCarga;
	}

	public void setHoraCarga(String horaCarga) {
		this.horaCarga = horaCarga;
	}

	public String getUsuarioCarga() {
		return usuarioCarga;
	}

	public void setUsuarioCarga(String usuarioCarga) {
		this.usuarioCarga = usuarioCarga;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) { 
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getTipoExtincion() {
		return tipoExtincion;
	}

	public void setTipoExtincion(String tipoExtincion) {
		this.tipoExtincion = tipoExtincion;
	}

}