package com.netro.cesion;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsCedCesion implements IQueryGeneratorRegExtJS {
	
	public ConsCedCesion() {
		//Constructor
	}
	
	private String cve_pyme;
	
	private static final Log log = ServiceLocator.getInstance().getLog(ConsCedCesion.class);//Variable para enviar mensajes al log.
	
	public String getAggregateCalculationQuery()	{
		
		StringBuffer 	qrySentencia 	= new StringBuffer();
		return qrySentencia.toString();
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds)  {
		StringBuffer qrySentencia = new StringBuffer();
		return qrySentencia.toString();
	}
	
	public String getDocumentQuery()	{
	
		StringBuffer qrySentencia = new StringBuffer();
		
		log.debug("getDocumentQuery)");
		return qrySentencia.toString();
	}
	
	public String getDocumentQueryFile() {
		this.variablesBind = new ArrayList();
		StringBuffer 	qrySentencia 	= new StringBuffer();
    	StringBuffer	condicion 		= new StringBuffer();
	     
		try{ 
			qrySentencia.append(
						" SELECT p.ic_pyme as IC_PYME, p.cg_razon_social as PYME, c.CG_TIPO_REPRESENTANTE as TIPO_REP, c.CG_TIPO_CEDENTE as TIPO_CED, c.CG_FIGURA_JURIDICA as FIG_JUR, "+
						" c.CG_ESCRITURA_PUBLICA as ESC_PUB, TO_CHAR(c.DF_ESCRITURA_PUBLICA, 'DD/MM/YYYY') as FEC_ESC_PUB, c.CG_NOMBRE_LICENCIADO as NOM_LIC, "+
						" c.IN_NUM_NOTARIO_PUBLICO as NOT_PUB, c.CG_ORIGEN_NOTARIO as CD_NOT, c.CG_REG_PUB_COMERCIO as REG_PUB, c.IN_NUM_FOLIO_MERCANTIL as FOLIO, "+
						" c.CG_ESCRITURA_PUBLICA_REP as ESC_PUB_REP, TO_CHAR(c.DF_ESCRITURA_PUBLICA_REP, 'DD/MM/YYYY') as FEC_ESC_PUB_REP, c.CG_NOMBRE_LICENCIADO_REP as NOM_LIC_REP, "+
						" c.IN_NUM_NOTARIO_PUBLICO_REP as NOT_PUB_REP, c.CG_ORIGEN_NOTARIO_REP as CD_NOT_REP, CS_BANDERA_REP, CG_DOMICILIO_LEGAL"+
						" FROM comcat_pyme p, comcat_cedente c "+ 
						" WHERE p.ic_pyme = c.ic_pyme ");     
						
						
			if(!cve_pyme.equals("")){
				condicion.append("   AND c.ic_pyme = ? ");
					this.variablesBind.add(cve_pyme);
			}
			
			qrySentencia.append(condicion.toString());
			
		}catch (Exception e){
			log.debug("ConsCedCesion:: getDocumentQueryException "+e);
		}
	
		log.debug("getDocumentQueryFile(S) "+qrySentencia.toString());
		return qrySentencia.toString();
	}
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String nombreArchivo = "";
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		CreaArchivo 	archivo 			= new CreaArchivo();
		
		int registros = 0;
		String pyme = "", rep= "", ced = "", figura= "", escritura="", fec_esc= "",  nom_lic = "", num_not= "", cd_not ="", reg_pub="", folio="";
		
		String escrituraRep="", fec_escRep= "",  nom_licRep = "", num_notRep= "", cd_notRep ="", domicilioLegal ="";
		if ("CSV".equals(tipo)) {
			try {
				
				while(rs.next()){
						if(registros==0) {
								contenidoArchivo.append(" Pyme"+ "," +
												  " Tipo de Representante"+ "," +
												  " Tipo de Cedente"+ "," +
												  " Figura Juridica"+ "," +
												  " Escritura P�blica"+ "," +
												  " Fecha Escritura P�blica"+ "," +
												  " Nombre del Licenciado"+ "," +
												  " N�m. Notario P�blico"+ "," +
												  " Cd. Origen del Notario"+ "," +
												  " Reg. P�blico de Comercio"+ "," +
												  " Folio Mercantil"+ "," +
												  " Domicilio Legal Empresa,"+ 
												  " Escritura P�blica Representante"+ "," +
												  " Fecha Escritura P�blica Representante"+ "," +
												  " Nombre del Licenciado Representante"+ "," +
												  " N�m. Notario P�blico Representante"+ "," +
												  " Cd. Origen del Notario Representante"+ "," +"\n");
							
						}
						registros++;
							pyme = (rs.getString("PYME") == null) ? "" : rs.getString("PYME");
							rep = (rs.getString("TIPO_REP") == null) ? "" : rs.getString("TIPO_REP");
							rep = rep.equals("L")?"Representante Legal":(rep.equals("A")?"Apoderado":"");
							ced= (rs.getString("TIPO_CED") == null) ? "" : rs.getString("TIPO_CED");
							ced = ced.equals("C")?"Contratista":(ced.equals("P")?"Proveedor":"");
							figura=(rs.getString("FIG_JUR") == null)?"":rs.getString("FIG_JUR");
							escritura= (rs.getString("ESC_PUB") == null) ? "" : rs.getString("ESC_PUB");
							fec_esc = (rs.getString("FEC_ESC_PUB") == null) ? "" : rs.getString("FEC_ESC_PUB");
							nom_lic= (rs.getString("NOM_LIC") == null) ? "" : rs.getString("NOM_LIC");
							num_not =(rs.getString("NOT_PUB") == null) ? "" : rs.getString("NOT_PUB");
							cd_not =(rs.getString("CD_NOT") == null) ? "" : rs.getString("CD_NOT");
							reg_pub =(rs.getString("REG_PUB") == null) ? "" : rs.getString("REG_PUB");
							folio =(rs.getString("FOLIO") == null) ? "" : rs.getString("FOLIO");
							
							escrituraRep= (rs.getString("ESC_PUB_REP") == null) ? "" : rs.getString("ESC_PUB_REP");
							fec_escRep = (rs.getString("FEC_ESC_PUB_REP") == null) ? "" : rs.getString("FEC_ESC_PUB_REP");
							nom_licRep= (rs.getString("NOM_LIC_REP") == null) ? "" : rs.getString("NOM_LIC_REP");
							num_notRep =(rs.getString("NOT_PUB_REP") == null) ? "" : rs.getString("NOT_PUB_REP");
							cd_notRep =(rs.getString("CD_NOT_REP") == null) ? "" : rs.getString("CD_NOT_REP");
							domicilioLegal =(rs.getString("CG_DOMICILIO_LEGAL") == null) ? "" : rs.getString("CG_DOMICILIO_LEGAL");

							contenidoArchivo.append( pyme.replaceAll(",","") +","+
                                     rep.replaceAll(",","") +","+
                                     ced.replaceAll(",","") +","+
                                     figura.replaceAll(",","") +","+
                                     escritura.replaceAll(",","") +","+
                                     fec_esc.replaceAll(",","") +","+
                                     nom_lic.replaceAll(",","") +","+
												 num_not.replaceAll(",","") +","+
												 cd_not.replaceAll(",","") +","+
												 reg_pub.replaceAll(",","") +","+
                                     folio.replaceAll(",","") +","+
												 domicilioLegal.replaceAll("","")+","+
												 escrituraRep.replaceAll(",","") +","+
                                     fec_escRep.replaceAll(",","") +","+
                                     nom_licRep.replaceAll(",","") +","+
												 num_notRep.replaceAll(",","") +","+
												 cd_notRep.replaceAll(",","") +","+"\n");
						
					}

					if(registros >= 1){
						if(!archivo.make(contenidoArchivo.toString(),path, ".csv")){
							nombreArchivo="ERROR";
						}else{
							nombreArchivo = archivo.nombre;
						}
					}
				
			} catch (Throwable e) {
					throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
			
		}else if("PDF".equals(tipo)){
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
	
				String meses[]	=	{"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	=	new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual	=	fechaActual.substring(0,2);
				String mesActual	=	meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual	=	fechaActual.substring(6,10);
				String horaActual	=	new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),
				(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + "-----------------------------" +
				horaActual, "formas", ComunesPDF.RIGHT);
				pdfDoc.addText("  ", "formas", ComunesPDF.CENTER);
				
				pdfDoc.setTable(10, 100);
				pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Pyme","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de Representante","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de Cedente","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Figura Juridica","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Escritura P�blica","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Escritura P�blica","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre del Licenciado","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�m. Notario P�blico","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Cd. origen del Notario","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Reg. P�blico de Comercio","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Folio Mercantil","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Domicilio Legal Empresa","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Escritura P�blica Representante","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Escritura P�blica Representante","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre del Licenciado Representante","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�m. Notario P�blico Representante","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Cd. origen del Notario Representante","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
				
				while (rs.next()) {
						pyme = (rs.getString("PYME") == null) ? "" : rs.getString("PYME");
						rep = (rs.getString("TIPO_REP") == null) ? "" : rs.getString("TIPO_REP");
						ced= (rs.getString("TIPO_CED") == null) ? "" : rs.getString("TIPO_CED");
						figura=(rs.getString("FIG_JUR") == null)?"":rs.getString("FIG_JUR");
						escritura= (rs.getString("ESC_PUB") == null) ? "" : rs.getString("ESC_PUB");
						fec_esc = (rs.getString("FEC_ESC_PUB") == null) ? "" : rs.getString("FEC_ESC_PUB");
						nom_lic= (rs.getString("NOM_LIC") == null) ? "" : rs.getString("NOM_LIC");
						num_not =(rs.getString("NOT_PUB") == null) ? "" : rs.getString("NOT_PUB");
						cd_not =(rs.getString("CD_NOT") == null) ? "" : rs.getString("CD_NOT");
						reg_pub =(rs.getString("REG_PUB") == null) ? "" : rs.getString("REG_PUB");
						folio =(rs.getString("FOLIO") == null) ? "" : rs.getString("FOLIO");
						
						escrituraRep= (rs.getString("ESC_PUB_REP") == null) ? "" : rs.getString("ESC_PUB_REP");
						fec_escRep = (rs.getString("FEC_ESC_PUB_REP") == null) ? "" : rs.getString("FEC_ESC_PUB_REP");
						nom_licRep= (rs.getString("NOM_LIC_REP") == null) ? "" : rs.getString("NOM_LIC_REP");
						num_notRep =(rs.getString("NOT_PUB_REP") == null) ? "" : rs.getString("NOT_PUB_REP");
						cd_notRep =(rs.getString("CD_NOT_REP") == null) ? "" : rs.getString("CD_NOT_REP");
						domicilioLegal =(rs.getString("CG_DOMICILIO_LEGAL") == null) ? "" : rs.getString("CG_DOMICILIO_LEGAL");
						
						pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(pyme,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(rep.equals("L")?"Representante Legal":(rep.equals("A")?"Apoderado":""),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(ced.equals("C")?"Contratista":(ced.equals("P")?"Proveedor":""),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(figura,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(escritura,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fec_esc,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nom_lic,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(num_not,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(cd_not,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(reg_pub,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(folio,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(domicilioLegal,"formas",ComunesPDF.CENTER);
						
						pdfDoc.setCell(escrituraRep,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fec_escRep,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nom_licRep,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(num_notRep,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(cd_notRep,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
				}
				
				pdfDoc.addTable();
				pdfDoc.endDocument();
			
			}catch (Throwable e) {
				throw new AppException("Error al generar el archivo",e);
			}
		}
		
		return nombreArchivo;
	}
	
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		log.debug("crearCustomFile (E)");

		return nombreArchivo;
	}
	
	public List getConditions() {
		log.debug("*************getConditions=" + variablesBind);
		return variablesBind;
	}
	private ArrayList variablesBind = null;


	public void setCve_pyme(String cve_pyme) {
		this.cve_pyme = cve_pyme;
	}


	public String getCve_pyme() {
		return cve_pyme;
	}
	
}