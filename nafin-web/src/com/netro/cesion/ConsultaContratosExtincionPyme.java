package com.netro.cesion;

import com.netro.pdf.ComunesPDF;

import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;
 
public class ConsultaContratosExtincionPyme implements IQueryGeneratorReg , IQueryGeneratorRegExtJS {

	private static final Log log = ServiceLocator.getInstance().getLog(ConsultaContratosExtincionPyme.class);//Variable para enviar mensajes al log.
	private StringBuffer strSQL;
	private List conditions;
	private String paginaOffset;
	private String paginaNo;
	private String clavePyme;
	private String claveEpo;
	private String claveIfCesionario;
	private String numeroContrato;
	private String claveMoneda;
	private String claveTipoContratacion;
	private String fechaInicio;
	private String fechaFin;

	//Constructor de la clase.
	public ConsultaContratosExtincionPyme() {}

	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E) ::..");
		strSQL = new StringBuffer();
		log.info("getAggregateCalculationQuery(S) ::..");
		return strSQL.toString();
	}
		
	 /**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery() {
		log.info("getDocumentQuery(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();

		strSQL.append(" SELECT DISTINCT sol.ic_solicitud AS clave_solicitud");
		strSQL.append(" FROM cder_solicitud sol, cder_solic_x_representante csr");
		strSQL.append(" WHERE sol.ic_solicitud = csr.ic_solicitud");
		strSQL.append(" AND sol.ic_estatus_solic IN (?, ?)");
		conditions.add(new Integer(17));//17.- Redireccionamiento Aplicado
		conditions.add(new Integer(18));//18.- Extinci�n Pre-Solicitada

		if (clavePyme != null && !"".equals(clavePyme)) {
			strSQL.append(" AND csr.ic_pyme = ?");
			conditions.add(new Long(clavePyme));
		}

		if (claveEpo != null && !"".equals(claveEpo)) {
			strSQL.append(" AND sol.ic_epo = ?");
			conditions.add(new Integer(claveEpo));
		}

		if (claveIfCesionario != null && !"".equals(claveIfCesionario)) {
			strSQL.append(" AND sol.ic_if = ?");
			conditions.add(new Integer(claveIfCesionario));
		}

		if (numeroContrato != null && !"".equals(numeroContrato)) {
			strSQL.append(" AND sol.cg_no_contrato = ?");
			conditions.add(numeroContrato);
		}

		if (claveMoneda != null && !"".equals(claveMoneda)){
			if (claveMoneda.equals("0")){
				strSQL.append(" AND sol.cs_multimoneda = ?");
				conditions.add("S");
			} else{
				strSQL.append(" AND sol.cs_multimoneda = ?");
				strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
				strSQL.append(" FROM cder_monto_x_solic mxs");
				strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
				strSQL.append(" AND mxs.ic_moneda = ?)");
				conditions.add("N");
				conditions.add(new Integer(claveMoneda));
			}
		}

		if(claveTipoContratacion != null && !"".equals(claveTipoContratacion)){
			strSQL.append(" AND sol.ic_tipo_contratacion = ?");
			conditions.add(new Integer(claveTipoContratacion));
		}		

		if(!fechaInicio.equals("") && !fechaFin.equals("")){
			strSQL.append(" AND sol.DF_FIRMA_CONTRATO >= TO_DATE(?, 'DD/MM/YYYY') AND sol.DF_FIRMA_CONTRATO < TO_DATE(?, 'DD/MM/YYYY')+1 ");
			conditions.add(fechaInicio);
			conditions.add(fechaFin);
		}

		strSQL.append(" ORDER BY sol.ic_solicitud");
		
		log.debug("..:: strSQL: "+strSQL.toString());
		log.debug("..:: conditions: "+conditions);
		
		log.info("getDocumentQuery(S) ::..");
		return strSQL.toString();
	}

	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		log.info("getDocumentQuery(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();

		strSQL.append(" SELECT sol.ic_solicitud AS clave_solicitud,");
    strSQL.append(" sol.ic_pyme AS clave_pyme,");
		strSQL.append(" sol.ic_epo AS clave_epo,");
		strSQL.append(" sol.ic_if AS clave_if,");
		strSQL.append(" sol.ic_tipo_contratacion AS clave_contratacion,");
		strSQL.append(" epo.cg_razon_social AS dependencia,");
		strSQL.append(" cif.cg_razon_social AS nombre_cesionario,");
		strSQL.append(" cpy.CG_RAZON_SOCIAL as cedente ,");//FODEA-024-2014 MOD()
		strSQL.append(" sol.cg_no_contrato AS numero_contrato,");
		strSQL.append(" con.cg_nombre AS tipo_contratacion,");
		strSQL.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");
		strSQL.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");
		strSQL.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS plazo_contrato,");
		strSQL.append(" cl.cg_area AS clasificacion_epo,");
		strSQL.append(" sol.cg_campo1 AS campo_adicional_1,");
		strSQL.append(" sol.cg_campo2 AS campo_adicional_2,");
		strSQL.append(" sol.cg_campo3 AS campo_adicional_3,");
		strSQL.append(" sol.cg_campo4 AS campo_adicional_4,");
		strSQL.append(" sol.cg_campo5 AS campo_adicional_5,");
		strSQL.append(" sol.cg_obj_contrato AS objeto_contrato,");
		strSQL.append(" sol.cg_comentarios AS comentarios,");
		//strSQL.append(" TO_CHAR(extc.df_extincion_contrato, 'dd/mm/yyyy') AS fecha_extincion,");
		strSQL.append(" DECODE (extc.df_extincion_contrato,NULL, TO_CHAR (Sysdate, 'dd/mm/yyyy'),   TO_CHAR (extc.df_extincion_contrato, 'dd/mm/yyyy')   ) AS fecha_extincion ,");
		strSQL.append(" sol.cg_campo1 AS numero_cuenta,");
		strSQL.append(" sol.cg_campo2 AS cuenta_clabe,");
		strSQL.append(" sol.cg_campo3 AS banco_deposito,");
		strSQL.append(" DECODE (extc.cg_causa_rechazo_ext, null, 'N/A', extc.cg_causa_rechazo_ext) AS causas_rechazo,");
		strSQL.append(" DECODE(sol.ic_estatus_solic, 17, 'Por Solicitar', es.cg_descripcion) AS estatus_solicitud,");
		strSQL.append(" sol.ic_estatus_solic AS clave_estatus,");
		strSQL.append(" cpy.CG_RAZON_SOCIAL ||';'|| sol.CG_EMPRESAS_REPRESENTADAS AS EMPRESAS,");
		/*strSQL.append("(");
		strSQL.append("SELECT SUM(DECODE(R.DF_EXTINCION_REP,NULL,1,0)) ");
		strSQL.append("FROM CDER_SOLICITUD D, CDER_SOLIC_X_REPRESENTANTE R " );
		strSQL.append("WHERE D.IC_SOLICITUD = R.IC_SOLICITUD "               );
		strSQL.append("AND D.IC_EPO = SOL.IC_EPO "                           );
		strSQL.append("AND D.IC_PYME = SOL.IC_PYME "                         );
		strSQL.append("AND D.IC_IF = SOL.IC_IF "                             );
		strSQL.append("AND D.IC_SOLICITUD = SOL.IC_SOLICITUD "               );
		strSQL.append(") AS FIRMAS_PENDIENTES, "                              );*/
		strSQL.append(" tb_num_rep.num_rep AS num_rep,");
		strSQL.append(" tb_num_rep.FIRMAS_PENDIENTES AS FIRMAS_PENDIENTES,");
		strSQL.append(" sol.ic_grupo_cesion AS IC_GRUPO ");
		strSQL.append(" , cpy.CG_RFC as RFC_PYME ");    
		
		strSQL.append(" FROM cder_solicitud sol");
		strSQL.append(", comcat_epo epo");
		strSQL.append(", comcat_if cif");
		strSQL.append(", cdercat_clasificacion_epo cl");
		strSQL.append(", cdercat_tipo_contratacion con");
		strSQL.append(", cdercat_estatus_solic es");
		strSQL.append(", cdercat_tipo_plazo stp");
		strSQL.append(", cder_extincion_contrato extc");
		strSQL.append(", comcat_pyme cpy");//FODEA-024-2014
		strSQL.append(",(SELECT   ic_solicitud, COUNT (*) num_rep, ");
		strSQL.append("         SUM(DECODE(DF_EXTINCION_REP,NULL,1,0)) AS FIRMAS_PENDIENTES ");
		strSQL.append("    FROM cder_solic_x_representante ");
		strSQL.append("GROUP BY ic_solicitud) tb_num_rep ");
		strSQL.append(" WHERE sol.ic_epo = epo.ic_epo");
		strSQL.append(" AND sol.ic_pyme = cpy.ic_pyme ");//FODEA-024-2014
		strSQL.append(" AND sol.ic_if = cif.ic_if ");
		strSQL.append(" AND sol.ic_solicitud = tb_num_rep.ic_solicitud ");
		strSQL.append(" AND sol.ic_clasificacion_epo = cl.ic_clasificacion_epo(+)");
		strSQL.append(" AND sol.ic_tipo_contratacion = con.ic_tipo_contratacion");
		strSQL.append(" AND sol.ic_estatus_solic = es.ic_estatus_solic");
		strSQL.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
		strSQL.append(" AND sol.ic_solicitud = extc.ic_solicitud(+)");
		strSQL.append(" AND (");
									
		for (int i = 0; i < pageIds.size(); i++) {
      List lItem = (ArrayList)pageIds.get(i);

      if(i > 0){strSQL.append("  OR  ");}

			strSQL.append("sol.ic_solicitud = ?");
      conditions.add(new Long(lItem.get(0).toString()));
		}

    strSQL.append(" ) ");
		strSQL.append(" ORDER BY sol.df_alta_solicitud DESC");
		
		log.debug("..:: strSQL: "+strSQL.toString());
		log.debug("..:: conditions: "+conditions);
		
		log.info("getDocumentQuery(S) ::..");
		return strSQL.toString();
	}
	
	public Registros getEmpresasRepresentadas(){
			log.info("getEmpresasRepresentadas(E) ::..");
		strSQL = new StringBuffer();
		Registros reg = new Registros();
		AccesoDB  con= new AccesoDB();
		conditions = new ArrayList();
				
		strSQL.append(" SELECT sol.ic_solicitud AS clave_solicitud,");
    strSQL.append(" sol.ic_pyme AS clave_pyme,");
		strSQL.append(" cpy.CG_RAZON_SOCIAL||';'||sol.CG_EMPRESAS_REPRESENTADAS as nombre_cedente ");//FODEA-024-2014 MOD()
		strSQL.append(" FROM cder_solicitud sol");
		strSQL.append(", comcat_epo epo");
		strSQL.append(", comcat_if cif");
		strSQL.append(", cdercat_clasificacion_epo cl");
		strSQL.append(", cdercat_tipo_contratacion con");
		strSQL.append(", cdercat_estatus_solic es");
		strSQL.append(", cdercat_tipo_plazo stp");
		strSQL.append(", cder_extincion_contrato extc");
		strSQL.append(", comcat_pyme cpy");//FODEA-024-2014
		strSQL.append(", cder_solic_x_representante csr ");
		strSQL.append(" WHERE sol.ic_epo = epo.ic_epo");
		strSQL.append(" AND sol.ic_solicitud = csr.ic_solicitud ");
		strSQL.append(" AND sol.ic_pyme = cpy.ic_pyme ");//FODEA-024-2014
		strSQL.append(" AND sol.ic_if = cif.ic_if ");
		strSQL.append(" AND sol.ic_clasificacion_epo = cl.ic_clasificacion_epo(+)");
		strSQL.append(" AND sol.ic_tipo_contratacion = con.ic_tipo_contratacion");
		strSQL.append(" AND sol.ic_estatus_solic = es.ic_estatus_solic");
		strSQL.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
		strSQL.append(" AND sol.ic_solicitud = extc.ic_solicitud(+)");
				
		strSQL.append(" AND sol.ic_estatus_solic IN (?, ?)");
		conditions.add(new Integer(17));//17.- Redireccionamiento Aplicado
		conditions.add(new Integer(18));//18.- Extinci�n Pre-Solicitada

		if (clavePyme != null && !"".equals(clavePyme)) {
			strSQL.append(" AND csr.ic_pyme = ?");
			conditions.add(new Long(clavePyme));
		}

		if (claveEpo != null && !"".equals(claveEpo)) {
			strSQL.append(" AND sol.ic_epo = ?");
			conditions.add(new Integer(claveEpo));
		}

		if (claveIfCesionario != null && !"".equals(claveIfCesionario)) {
			strSQL.append(" AND sol.ic_if = ?");
			conditions.add(new Integer(claveIfCesionario));
		}

		if (numeroContrato != null && !"".equals(numeroContrato)) {
			strSQL.append(" AND sol.cg_no_contrato = ?");
			conditions.add(numeroContrato);
		}

		if (claveMoneda != null && !"".equals(claveMoneda)) {
			if (claveMoneda.equals("0")) {
				strSQL.append(" AND sol.cs_multimoneda = ?");
				conditions.add("S");
			} else {
				strSQL.append(" AND sol.cs_multimoneda = ?");
				strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
				strSQL.append(" FROM cder_monto_x_solic mxs");
				strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
				strSQL.append(" AND mxs.ic_moneda = ?)");
				conditions.add("N");
				conditions.add(new Integer(claveMoneda));
			}
		}

		if (claveTipoContratacion != null && !"".equals(claveTipoContratacion)) {
			strSQL.append(" AND sol.ic_tipo_contratacion = ?");
			conditions.add(new Integer(claveTipoContratacion));
		}		

		strSQL.append(" ORDER BY sol.ic_solicitud");
		
		try{
			con.conexionDB();
			reg=con.consultarDB(strSQL.toString(),conditions);	
		} catch (Exception e) {
			log.error("getEmpresasRepresentadas(Error) ::..");
			throw new AppException("Ocurrio un error ", e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info("getMapaCesionario(S) ::..");
		}
		
		log.debug("..:: strSQL: "+strSQL.toString());
		log.debug("..:: conditions: "+conditions);
		
		log.info("getEmpresasRepresentadas(S) ::..");
		return reg;
	
	}
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		
		strSQL.append(" SELECT distinct(sol.ic_solicitud) AS clave_solicitud,");
    strSQL.append(" sol.ic_pyme AS clave_pyme,");
		strSQL.append(" sol.ic_epo AS clave_epo,");
		strSQL.append(" sol.ic_if AS clave_if,");
		strSQL.append(" sol.ic_tipo_contratacion AS clave_contratacion,");
		strSQL.append(" epo.cg_razon_social AS dependencia,");
		strSQL.append(" cif.cg_razon_social AS nombre_cesionario,");
		strSQL.append(" sol.cg_no_contrato AS numero_contrato,");
		strSQL.append(" con.cg_nombre AS tipo_contratacion,");
		strSQL.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");
		strSQL.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");
		strSQL.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS plazo_contrato,");
		strSQL.append(" cl.cg_area AS clasificacion_epo,");
		strSQL.append(" sol.cg_campo1 AS campo_adicional_1,");
		strSQL.append(" sol.cg_campo2 AS campo_adicional_2,");
		strSQL.append(" sol.cg_campo3 AS campo_adicional_3,");
		strSQL.append(" sol.cg_campo4 AS campo_adicional_4,");
		strSQL.append(" sol.cg_campo5 AS campo_adicional_5,");
		strSQL.append(" sol.cg_obj_contrato AS objeto_contrato,");
		strSQL.append(" sol.cg_comentarios AS comentarios,");
		strSQL.append(" TO_CHAR(extc.df_extincion_contrato, 'dd/mm/yyyy') AS fecha_extincion,");
		//strSQL.append(" DECODE (extc.df_extincion_contrato,NULL, TO_CHAR (Sysdate, 'dd/mm/yyyy'),   TO_CHAR (extc.df_extincion_contrato, 'dd/mm/yyyy')   ) AS fecha_extincion ,");
		strSQL.append(" sol.cg_campo1 AS numero_cuenta,");
		strSQL.append(" sol.cg_campo2 AS cuenta_clabe,");
		strSQL.append(" sol.cg_campo3 AS banco_deposito,");
		strSQL.append(" DECODE (extc.cg_causa_rechazo_ext, null, 'N/A', extc.cg_causa_rechazo_ext) AS causas_rechazo,");
		strSQL.append(" DECODE(sol.ic_estatus_solic, 17, 'Por Solicitar', es.cg_descripcion) AS estatus_solicitud,");
		strSQL.append(" sol.ic_estatus_solic AS clave_estatus, ");
		strSQL.append(" sol.ic_grupo_cesion AS IC_GRUPO");	  
		strSQL.append(" FROM cder_solicitud sol");
		strSQL.append(", comcat_epo epo");
		strSQL.append(", comcat_if cif");
		strSQL.append(", cdercat_clasificacion_epo cl");
		strSQL.append(", cdercat_tipo_contratacion con");
		strSQL.append(", cdercat_estatus_solic es");
		strSQL.append(", cdercat_tipo_plazo stp");
		strSQL.append(", cder_extincion_contrato extc");
		strSQL.append(", cder_solic_x_representante csr");
		strSQL.append(" WHERE sol.ic_epo = epo.ic_epo");
		strSQL.append(" AND sol.ic_solicitud = csr.ic_solicitud ");
		strSQL.append(" AND sol.ic_if = cif.ic_if ");
		strSQL.append(" AND sol.ic_clasificacion_epo = cl.ic_clasificacion_epo(+)");
		strSQL.append(" AND sol.ic_tipo_contratacion = con.ic_tipo_contratacion");
		strSQL.append(" AND sol.ic_estatus_solic = es.ic_estatus_solic");
		strSQL.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
		strSQL.append(" AND sol.ic_solicitud = extc.ic_solicitud(+)");
				
		strSQL.append(" AND sol.ic_estatus_solic IN (?, ?)");
		conditions.add(new Integer(17));//17.- Redireccionamiento Aplicado
		conditions.add(new Integer(18));//18.- Extinci�n Pre-Solicitada

		if (clavePyme != null && !"".equals(clavePyme)) {
			strSQL.append(" AND csr.ic_pyme = ?");
			conditions.add(new Long(clavePyme));
		}

		if (claveEpo != null && !"".equals(claveEpo)) {
			strSQL.append(" AND sol.ic_epo = ?");
			conditions.add(new Integer(claveEpo));
		}

		if (claveIfCesionario != null && !"".equals(claveIfCesionario)) {
			strSQL.append(" AND sol.ic_if = ?");
			conditions.add(new Integer(claveIfCesionario));
		}

		if (numeroContrato != null && !"".equals(numeroContrato)) {
			strSQL.append(" AND sol.cg_no_contrato = ?");
			conditions.add(numeroContrato);
		}

		if (claveMoneda != null && !"".equals(claveMoneda)) {
			if (claveMoneda.equals("0")) {
				strSQL.append(" AND sol.cs_multimoneda = ?");
				conditions.add("S");
			} else {
				strSQL.append(" AND sol.cs_multimoneda = ?");
				strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
				strSQL.append(" FROM cder_monto_x_solic mxs");
				strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
				strSQL.append(" AND mxs.ic_moneda = ?)");
				conditions.add("N");
				conditions.add(new Integer(claveMoneda));
			}
		}

		if (claveTipoContratacion != null && !"".equals(claveTipoContratacion)) {
			strSQL.append(" AND sol.ic_tipo_contratacion = ?");
			conditions.add(new Integer(claveTipoContratacion));
		}		

		if(!fechaInicio.equals("") && !fechaFin.equals("")){
			strSQL.append(" AND sol.DF_FIRMA_CONTRATO >= TO_DATE(?, 'DD/MM/YYYY') AND sol.DF_FIRMA_CONTRATO < TO_DATE(?, 'DD/MM/YYYY')+1 ");
			conditions.add(fechaInicio);
			conditions.add(fechaFin);
		}

		strSQL.append(" ORDER BY sol.ic_solicitud");
		
		log.info("strSQL  ::.."+strSQL);
		log.info("conditions  ::.."+conditions);
		
		log.info("getDocumentQueryFile(S) ::..");
		return strSQL.toString();
	}//getDocumentQueryFile
	
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
	
	StringBuffer linea = new StringBuffer(1024);
	OutputStreamWriter writer = null;

	String nombreArchivo = "";
	CreaArchivo creaArchivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer();
	ComunesPDF pdfDoc = new ComunesPDF();
	String cedente="";//FODEA-024-2014 MOD()
	Registros reg = this.getEmpresasRepresentadas();//FODEA-024-2014 MOD()
	
	try {
			
			CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
			int indiceCamposAdicionales =0;
			String clasificacionEpo =null;
			HashMap camposAdicionalesParametrizados  = null;			
			if( claveEpo != null){
				clasificacionEpo = cesionBean.clasificacionEpo(claveEpo);
				//obtencion de campos Adicionales
				camposAdicionalesParametrizados = cesionBean.getCamposAdicionalesParametrizados(claveEpo, "9");
				indiceCamposAdicionales = Integer.parseInt((String)camposAdicionalesParametrizados.get("indice"));
			}
			int columnas = 15;			
			if(clasificacionEpo !=null ) columnas +=1;
			if(indiceCamposAdicionales >0) columnas +=indiceCamposAdicionales;

			if(tipo.equals("PDF")) {				
				HttpSession session = request.getSession();
				String tipoUsuario = (String)session.getAttribute("strTipoUsuario");
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				//FODEA-024-2014
				int nSolicitudes = 0;
				while(reg.next()){
					cedente=(reg.getString("nombre_cedente") == null) ? "" : reg.getString("nombre_cedente");
				}
		
				cedente = cedente.replaceAll(";","\n");

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				//cedente,
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				pdfDoc.addText("  Extinci�n de Contrato ","formas",ComunesPDF.CENTER);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
			
					
				pdfDoc.setLTable(columnas, 100);	
				pdfDoc.setLCell("Dependencia","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Intermediario Financiero (Cesionario)","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("No. Contrato ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto / Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de Contrataci�n ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Inicio Contrato ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Final Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo del Contrato ","celda01",ComunesPDF.CENTER);
				if(clasificacionEpo !=null ){
				pdfDoc.setLCell(clasificacionEpo,"celda01",ComunesPDF.CENTER);
				}			
				for(int y = 0; y < indiceCamposAdicionales; y++){	
						String nombreCampo = ("Campo"+y);
						String desnombreCampo = camposAdicionalesParametrizados.get("nombre_campo_"+y).toString();
						pdfDoc.setLCell(desnombreCampo,"celda01",ComunesPDF.CENTER);
				}	
				pdfDoc.setLCell("Objeto del Contrato ","celda01",ComunesPDF.CENTER); 
				pdfDoc.setLCell("Fecha de Extinci�n del Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Cuenta","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Cuenta CLABE","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Banco de Dep�sito","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Causa del Rechazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Estado Actual ","celda01",ComunesPDF.CENTER);				
			}
			
			if(tipo.equals("CSV")) {
				contenidoArchivo.append(" Dependencia , ");
				contenidoArchivo.append(" Intermediario Financiero (Cesionario), ");
				contenidoArchivo.append(" No. Contrato, ");
				contenidoArchivo.append(" Monto / Moneda, ");
				contenidoArchivo.append(" Tipo de Contrataci�n , ");
				contenidoArchivo.append(" Fecha Inicio Contrato , "); 
				contenidoArchivo.append(" Fecha Final Contrato, ");
				contenidoArchivo.append(" Plazo delContrato , ");
				if(clasificacionEpo !=null){
					contenidoArchivo.append(clasificacionEpo+",");
				}					
				for(int y = 0; y < indiceCamposAdicionales; y++){	
						String nombreCampo = ("Campo"+y);
						String desnombreCampo = camposAdicionalesParametrizados.get("nombre_campo_"+y).toString();
						contenidoArchivo.append(desnombreCampo+",");
				}				
				contenidoArchivo.append(" Objeto del Contrato , ");
				contenidoArchivo.append(" Fecha de Extinci�n del Contrato , ");
				contenidoArchivo.append(" Cuenta , ");
				contenidoArchivo.append(" Cuenta CLABE , ");
				contenidoArchivo.append(" Banco de Dep�sito , ");
				contenidoArchivo.append(" Causa del Rechazo , ");
				contenidoArchivo.append(" Estado Actual , ");
				contenidoArchivo.append("\n");	
			}
				
			while (rs.next()) {
				
			String dependencia           = (rs.getString("dependencia")== null)?"":          rs.getString("dependencia").toString().replaceAll(",","");
			String nombre_cesionario     = (rs.getString("nombre_cesionario")== null)?"":    rs.getString("nombre_cesionario").toString().replaceAll(",","");
			String numero_contrato       = (rs.getString("numero_contrato")== null)?"":      rs.getString("numero_contrato").toString().replaceAll(",","");
			String tipo_contratacion     = (rs.getString("tipo_contratacion")== null)?"":    rs.getString("tipo_contratacion").toString().replaceAll(",","");
			String 	fecha_inicio_contrato = (rs.getString("fecha_inicio_contrato")== null)?"":rs.getString("fecha_inicio_contrato").toString().replaceAll(",","");
			String fecha_fin_contrato    = (rs.getString("fecha_fin_contrato")== null)?"":   rs.getString("fecha_fin_contrato").toString().replaceAll(",","");
			String plazo_contrato        = (rs.getString("plazo_contrato")== null)?"":       rs.getString("plazo_contrato").toString().replaceAll(",","");
			String clasificacion_epo     = (rs.getString("clasificacion_epo")== null)?"":    rs.getString("clasificacion_epo").toString().replaceAll(",","");
			String objeto_contrato       = (rs.getString("objeto_contrato")== null)?"":      rs.getString("objeto_contrato").toString().replaceAll(",","");
			String fecha_extincion       = (rs.getString("fecha_extincion")== null)?"":      rs.getString("fecha_extincion").toString().replaceAll(",","");
			String numero_cuenta         = (rs.getString("numero_cuenta")== null)?"":        rs.getString("numero_cuenta").toString().replaceAll(",","");
			String cuenta_clabe          = (rs.getString("cuenta_clabe")== null)?"":         rs.getString("cuenta_clabe").toString().replaceAll(",","");
			String estatus_solicitud     = (rs.getString("estatus_solicitud")== null)?"":    rs.getString("estatus_solicitud").toString().replaceAll(",","");
			String causas_rechazo        = (rs.getString("causas_rechazo")== null)?"":       rs.getString("causas_rechazo").toString().replaceAll(",","");
			String clave_solicitud       = (rs.getString("clave_solicitud")== null)?"":      rs.getString("clave_solicitud");
			String banco_deposito        = (rs.getString("banco_deposito")== null)?"":       rs.getString("banco_deposito").toString().replaceAll(",","");
			StringBuffer montosMonedaSo2 =  cesionBean.getMontoMoneda(clave_solicitud);				

			if(tipo.equals("CSV")) {
				contenidoArchivo.append(dependencia+",");
				contenidoArchivo.append(nombre_cesionario+",");
				contenidoArchivo.append(numero_contrato+",");
				contenidoArchivo.append(montosMonedaSo2.toString().replaceAll(",","").replaceAll("<br/>","|")+",");
				contenidoArchivo.append(tipo_contratacion+",");
				contenidoArchivo.append(fecha_inicio_contrato+",");
				contenidoArchivo.append(fecha_fin_contrato+",");
				contenidoArchivo.append(plazo_contrato+",");
				if(clasificacionEpo !=null){
				contenidoArchivo.append(clasificacion_epo+",");
				}
				for(int j = 0; j < indiceCamposAdicionales; j++){
					String temporal= j<2?"'":"";
					String campoAdicional ="CAMPO_ADICIONAL_"+(j + 1);	
					String descampoAdicional =(rs.getString(campoAdicional)== null)?"":rs.getString(campoAdicional).replaceAll(",","");	
					if(!campoAdicional.equals("") ){
					contenidoArchivo.append(temporal+descampoAdicional+" "+ ",");
					}						
				}	
				contenidoArchivo.append(objeto_contrato+",");	
				contenidoArchivo.append(fecha_extincion+",");	
				contenidoArchivo.append("'"+numero_cuenta+",");	
				contenidoArchivo.append("'"+cuenta_clabe+",");	
				contenidoArchivo.append(banco_deposito+",");	
				contenidoArchivo.append(causas_rechazo+", ");	
				contenidoArchivo.append(estatus_solicitud+",");						
				contenidoArchivo.append("\n");				
			}
				
			if(tipo.equals("PDF")) {		  
				
				pdfDoc.setLCell(dependencia,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(nombre_cesionario,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(numero_contrato,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(montosMonedaSo2.toString().replaceAll(",","").replaceAll("<br/>","\n"),"formas",ComunesPDF.LEFT);
				pdfDoc.setLCell(tipo_contratacion,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(fecha_inicio_contrato,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(fecha_fin_contrato,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(plazo_contrato,"formas",ComunesPDF.CENTER);
				if(clasificacionEpo !=null){
				pdfDoc.setLCell(clasificacion_epo,"formas",ComunesPDF.CENTER);
				}
				for(int j = 0; j < indiceCamposAdicionales; j++){
					String campoAdicional ="CAMPO_ADICIONAL_"+(j + 1);	
					String descampoAdicional =(rs.getString(campoAdicional)== null)?"":rs.getString(campoAdicional).replaceAll(",","")+ ",";	
					if(!campoAdicional.equals("") ){
					if(j==0||j==1){
					descampoAdicional="'"+descampoAdicional;
					}
					pdfDoc.setLCell(descampoAdicional,"formas",ComunesPDF.CENTER);
					}						
				}	
				pdfDoc.setLCell(objeto_contrato,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(fecha_extincion,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell("'"+numero_cuenta,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell("'"+cuenta_clabe,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(banco_deposito,"formas",ComunesPDF.CENTER);				
				pdfDoc.setLCell(causas_rechazo,"formas",ComunesPDF.CENTER);	
				pdfDoc.setLCell(estatus_solicitud,"formas",ComunesPDF.CENTER);
			}
			}//while (rs.next()) {
			
			if(tipo.equals("PDF")) {
				pdfDoc.addLTable();
				pdfDoc.endDocument();
			}else if(tipo.equals("CSV")) {				
				creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
				nombreArchivo = creaArchivo.getNombre();
			}
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo", e);
		} finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
		
		return nombreArchivo;
	}
	
	//Getters
	public List getConditions() {  return conditions;  }  
	public String getPaginaNo() {return paginaNo;}
	public String getPaginaOffset() {return paginaOffset;}
	public String getClavePyme() {return this.clavePyme;}
	public String getClaveEpo() {return this.claveEpo;}
	public String getClaveIfCesionario() {return this.claveIfCesionario;}
	public String getNumeroContrato() {return this.numeroContrato;}
	public String getClaveMoneda() {return this.claveMoneda;}
	public String getClaveTipoContratacion() {return this.claveTipoContratacion;}
	
	//Setters
	public void setPaginaNo(String newPaginaNo) {this.paginaNo = newPaginaNo;  }
	public void setPaginaOffset(String paginaOffset) {this.paginaOffset=paginaOffset;}
	public void setClavePyme(String clavePyme) {this.clavePyme = clavePyme;}
	public void setClaveEpo(String claveEpo) {this.claveEpo = claveEpo;}
	public void setClaveIfCesionario(String claveIfCesionario) {this.claveIfCesionario = claveIfCesionario;}
	public void setNumeroContrato(String numeroContrato) {this.numeroContrato = numeroContrato;}
	public void setClaveMoneda(String claveMoneda) {this.claveMoneda = claveMoneda;}
	public void setClaveTipoContratacion(String claveTipoContratacion) {this.claveTipoContratacion = claveTipoContratacion;}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

}