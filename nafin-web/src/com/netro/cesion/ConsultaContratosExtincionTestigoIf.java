package com.netro.cesion;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;


public class ConsultaContratosExtincionTestigoIf implements IQueryGeneratorReg, IQueryGeneratorRegExtJS {
	private StringBuffer strSQL;
	private List conditions;
	private String paginaOffset;
	private String paginaNo;
	private String clavePyme;
	private String claveEpo;
	private String claveIfCesionario;
	private String numeroContrato;
	private String claveMoneda;
	private String claveTipoContratacion;
	private static final Log log = ServiceLocator.getInstance().getLog(ConsultaContratosExtincionTestigoIf.class);//Variable para enviar mensajes al log.
	private String clave_solicitud;
	private String acuse;
	private String fechaCarga;
	private String HoraCarga;
	private String Usuario;
	private String estatus;
	private String fechaInicio;
	private String fechaFin;
	private String repIf2;
	

	/**
   * Constructor de la clase.
   */
	public ConsultaContratosExtincionTestigoIf() {}

	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E) ::..");
		strSQL = new StringBuffer();
		log.info("getAggregateCalculationQuery(S) ::..");
		return strSQL.toString();
	}
		
	 /**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery() {
		log.info("getDocumentQuery(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();

		strSQL.append(" SELECT sol.ic_solicitud AS clave_solicitud");
		strSQL.append(" FROM cder_solicitud sol");
    strSQL.append(", cder_extincion_contrato ext");
		strSQL.append(" WHERE sol.ic_estatus_solic IN (?)");
    strSQL.append(" AND ext.cc_acuse_ext_if IS NOT NULL");
    strSQL.append(" AND sol.ic_solicitud = ext.ic_solicitud(+)");
		if(repIf2.equals("S")){ // Si existe un segundo representante, la conslta debe solicitar su acuse
	 strSQL.append(" AND ext.cc_acuse_ext_if2 is not null "); 
		}/* else if(repIf2.equals("N")){ // Si solo existe un representante se solicita su acuse para saber que ya firm�
			strSQL.append(" AND ext.CC_ACUSE_EXT_IF is not null "); 
		}*/
		conditions.add(new Integer(19));//19.- Extinci�n Solicitada

		if (clavePyme != null && !"".equals(clavePyme)) {
			strSQL.append(" AND sol.ic_pyme = ?");
			conditions.add(new Long(clavePyme));
		}

		if (claveEpo != null && !"".equals(claveEpo)) {
			strSQL.append(" AND sol.ic_epo = ?");
			conditions.add(new Integer(claveEpo));
		}

		if (claveIfCesionario != null && !"".equals(claveIfCesionario)) {
			strSQL.append(" AND sol.ic_if = ?");
			conditions.add(new Integer(claveIfCesionario));
		}

		if (numeroContrato != null && !"".equals(numeroContrato)) {
			strSQL.append(" AND sol.cg_no_contrato = ?");
			conditions.add(numeroContrato);
		}

		if(!fechaInicio.equals("") && !fechaFin.equals("")){
			strSQL.append(" AND sol.DF_FIRMA_CONTRATO >= TO_DATE(?, 'DD/MM/YYYY') AND sol.DF_FIRMA_CONTRATO < TO_DATE(?, 'DD/MM/YYYY')+1 ");
			conditions.add(fechaInicio);
			conditions.add(fechaFin);
		}

		if (claveMoneda != null && !"".equals(claveMoneda)) {
			if (claveMoneda.equals("0")) {
				strSQL.append(" AND sol.cs_multimoneda = ?");
				conditions.add("S");
			} else {
				strSQL.append(" AND sol.cs_multimoneda = ?");
				strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
				strSQL.append(" FROM cder_monto_x_solic mxs");
				strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
				strSQL.append(" AND mxs.ic_moneda = ?)");
				conditions.add("N");
				conditions.add(new Integer(claveMoneda));
			}
		}

		if (claveTipoContratacion != null && !"".equals(claveTipoContratacion)) {
			strSQL.append(" AND sol.ic_tipo_contratacion = ?");
			conditions.add(new Integer(claveTipoContratacion));
		}	
				
		strSQL.append(" ORDER BY sol.ic_solicitud");
		
		log.debug("..:: strSQL: "+strSQL.toString());
		log.debug("..:: conditions: "+conditions);
		
		log.info("getDocumentQuery(S) ::..");
		return strSQL.toString();
	}

	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		log.info("getDocumentSummaryQueryForIds(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();

		strSQL.append(" SELECT sol.ic_solicitud AS clave_solicitud,"); 
		strSQL.append(" sol.ic_pyme AS clave_pyme,");
		strSQL.append(" sol.ic_epo AS clave_epo,");
		strSQL.append(" sol.ic_if AS clave_if,");
		strSQL.append(" sol.ic_tipo_contratacion AS clave_contratacion,");
		strSQL.append(" epo.cg_razon_social AS dependencia,");
		strSQL.append(" pyme.cg_razon_social||';'||sol.cg_empresas_representadas AS nombre_cedente,");//FODEA-024-2014 MOD()
		strSQL.append(" pyme.cg_rfc AS rfc_cedente,");
		strSQL.append(" cif.cg_razon_social AS nombre_cesionario,");
		strSQL.append(" sol.cg_no_contrato AS numero_contrato,");
		strSQL.append(" con.cg_nombre AS tipo_contratacion,");
		strSQL.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");
		strSQL.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");
		strSQL.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS plazo_contrato,");
		strSQL.append(" cl.cg_area AS clasificacion_epo,");
		strSQL.append(" sol.cg_campo1 AS campo_adicional_1,");
		strSQL.append(" sol.cg_campo2 AS campo_adicional_2,");
		strSQL.append(" sol.cg_campo3 AS campo_adicional_3,");
		strSQL.append(" sol.cg_campo4 AS campo_adicional_4,");
		strSQL.append(" sol.cg_campo5 AS campo_adicional_5,");
		strSQL.append(" sol.cg_obj_contrato AS objeto_contrato,");
		strSQL.append(" sol.cg_comentarios AS comentarios,");
		strSQL.append(" TO_CHAR(extc.df_extincion_contrato, 'dd/mm/yyyy') AS fecha_extincion,");
		strSQL.append(" extc.cg_cuenta_ext AS numero_cuenta,");
		strSQL.append(" extc.cg_cuenta_clabe_ext AS cuenta_clabe,");
		strSQL.append(" extc.cg_banco_deposito_ext AS banco_deposito,");
		strSQL.append(" DECODE(extc.cg_causa_rechazo_ext, null, 'N/A', extc.cg_causa_rechazo_ext) AS causas_rechazo,");
		strSQL.append(" DECODE(sol.ic_estatus_solic, 17, 'Por Solicitar', es.cg_descripcion) AS estatus_solicitud,");
	  strSQL.append(" DECODE(extc.cc_acuse_ext_if, null, 'N', 'S') AS aceptada_if,");
		strSQL.append(" sol.ic_estatus_solic AS clave_estatus, ");
		strSQL.append(" '' AS MONTO_MONEDA ,");
		strSQL.append(" '' AS REPRESENTATE_LEGAL, ");
		strSQL.append(" '' AS COLUMNA_ACCION ");
		strSQL.append(" , sol.IC_GRUPO_CESION  AS GRUPO_CESION  ");	  
		
		strSQL.append(" FROM cder_solicitud sol");
		strSQL.append(", comcat_epo epo");
		strSQL.append(", comcat_if cif");
		strSQL.append(", comcat_pyme pyme");
		strSQL.append(", cdercat_clasificacion_epo cl");
		strSQL.append(", cdercat_tipo_contratacion con");
		strSQL.append(", cdercat_estatus_solic es");
		strSQL.append(", cdercat_tipo_plazo stp");
		strSQL.append(", cder_extincion_contrato extc");
		strSQL.append(" WHERE sol.ic_epo = epo.ic_epo");
		strSQL.append(" AND sol.ic_if = cif.ic_if ");
		strSQL.append(" AND sol.ic_pyme = pyme.ic_pyme ");
		strSQL.append(" AND sol.ic_clasificacion_epo = cl.ic_clasificacion_epo(+)");
		strSQL.append(" AND sol.ic_tipo_contratacion = con.ic_tipo_contratacion");
		strSQL.append(" AND sol.ic_estatus_solic = es.ic_estatus_solic");
		strSQL.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
		strSQL.append(" AND sol.ic_solicitud = extc.ic_solicitud(+)");
		strSQL.append(" AND (");
									
		for (int i = 0; i < pageIds.size(); i++) {
      List lItem = (ArrayList)pageIds.get(i);

      if(i > 0){strSQL.append("  OR  ");}

			strSQL.append("sol.ic_solicitud = ?");
      conditions.add(new Long(lItem.get(0).toString()));
		}

    strSQL.append(" ) ");
		strSQL.append(" ORDER BY sol.df_alta_solicitud DESC");
		
		log.debug("..:: strSQL: "+strSQL.toString());
		log.debug("..:: conditions: "+conditions);
		
		log.info("getDocumentSummaryQueryForIds(S) ::..");
		return strSQL.toString();
	}
	

	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		
		strSQL.append(" SELECT sol.ic_solicitud AS clave_solicitud,"); 
		strSQL.append(" sol.ic_pyme AS clave_pyme,");
		strSQL.append(" sol.ic_epo AS clave_epo,");
		strSQL.append(" sol.ic_if AS clave_if,");
		strSQL.append(" sol.ic_tipo_contratacion AS clave_contratacion,");
		strSQL.append(" epo.cg_razon_social AS dependencia,");
		strSQL.append(" pyme.cg_razon_social||';'||sol.cg_empresas_representadas AS nombre_cedente,");//FODEA-024-2014 MOD()
		strSQL.append(" pyme.cg_rfc AS rfc_cedente,");
		strSQL.append(" cif.cg_razon_social AS nombre_cesionario,");
		strSQL.append(" sol.cg_no_contrato AS numero_contrato,");
		strSQL.append(" con.cg_nombre AS tipo_contratacion,");
		strSQL.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");
		strSQL.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");
		strSQL.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS plazo_contrato,");
		strSQL.append(" cl.cg_area AS clasificacion_epo,");
		strSQL.append(" sol.cg_campo1 AS campo_adicional_1,");
		strSQL.append(" sol.cg_campo2 AS campo_adicional_2,");
		strSQL.append(" sol.cg_campo3 AS campo_adicional_3,");
		strSQL.append(" sol.cg_campo4 AS campo_adicional_4,");
		strSQL.append(" sol.cg_campo5 AS campo_adicional_5,");
		strSQL.append(" sol.cg_obj_contrato AS objeto_contrato,");
		strSQL.append(" sol.cg_comentarios AS comentarios,");
		strSQL.append(" TO_CHAR(extc.df_extincion_contrato, 'dd/mm/yyyy') AS fecha_extincion,");
		strSQL.append(" extc.cg_cuenta_ext AS numero_cuenta,");
		strSQL.append(" extc.cg_cuenta_clabe_ext AS cuenta_clabe,");
		strSQL.append(" extc.cg_banco_deposito_ext AS banco_deposito,");
		strSQL.append(" DECODE(extc.cg_causa_rechazo_ext, null, 'N/A', extc.cg_causa_rechazo_ext) AS causas_rechazo,");
		strSQL.append(" DECODE(sol.ic_estatus_solic, 17, 'Por Solicitar', es.cg_descripcion) AS estatus_solicitud,");
	  strSQL.append(" DECODE(extc.cc_acuse_ext_if, null, 'N', 'S') AS aceptada_if,");
		strSQL.append(" sol.ic_estatus_solic AS clave_estatus, ");
		strSQL.append(" '' AS MONTO_MONEDA ,");
		strSQL.append(" '' AS REPRESENTATE_LEGAL, ");
		strSQL.append(" '' AS COLUMNA_ACCION ");
		strSQL.append(" FROM cder_solicitud sol");
		strSQL.append(", comcat_epo epo");
		strSQL.append(", comcat_if cif");
		strSQL.append(", comcat_pyme pyme");
		strSQL.append(", cdercat_clasificacion_epo cl");
		strSQL.append(", cdercat_tipo_contratacion con");
		strSQL.append(", cdercat_estatus_solic es");
		strSQL.append(", cdercat_tipo_plazo stp");
		strSQL.append(", cder_extincion_contrato extc");
		strSQL.append(" WHERE sol.ic_epo = epo.ic_epo");
		strSQL.append(" AND sol.ic_if = cif.ic_if ");
		strSQL.append(" AND sol.ic_pyme = pyme.ic_pyme ");
		strSQL.append(" AND sol.ic_clasificacion_epo = cl.ic_clasificacion_epo(+)");
		strSQL.append(" AND sol.ic_tipo_contratacion = con.ic_tipo_contratacion");
		strSQL.append(" AND sol.ic_estatus_solic = es.ic_estatus_solic");
		strSQL.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
		strSQL.append(" AND sol.ic_solicitud = extc.ic_solicitud(+)"); 
		//strSQL.append(" AND extc.cc_acuse_ext_if2 is not null "); 
		if(repIf2.equals("S")){ // Si existe un segundo representante, la conslta debe solicitar su acuse
		strSQL.append(" AND extc.cc_acuse_ext_if2 is not null "); 
		} else if(repIf2.equals("N")){ // Si solo existe un representante se solicita su acuse para saber que ya firm�
			strSQL.append(" AND extc.CC_ACUSE_EXT_IF is not null "); 
		}
		
		if ("".equals(estatus)) {
			strSQL.append(" AND sol.ic_estatus_solic IN (?)");			
			conditions.add(new Integer(19));//19.- Extinci�n Solicitada
		}
				
		if (clave_solicitud != null && !"".equals(clave_solicitud)) {
			strSQL.append(" AND sol.ic_solicitud = ? ");
			conditions.add(new Long(clave_solicitud));
		}
		

		if (clavePyme != null && !"".equals(clavePyme)) {
			strSQL.append(" AND sol.ic_pyme = ?");
			conditions.add(new Long(clavePyme));
		}

		if (claveEpo != null && !"".equals(claveEpo)) {
			strSQL.append(" AND sol.ic_epo = ?");
			conditions.add(new Integer(claveEpo));
		}

		if (claveIfCesionario != null && !"".equals(claveIfCesionario)) {
			strSQL.append(" AND sol.ic_if = ?");
			conditions.add(new Integer(claveIfCesionario));
		}

		if (numeroContrato != null && !"".equals(numeroContrato)) {
			strSQL.append(" AND sol.cg_no_contrato = ?");
			conditions.add(numeroContrato);
		}

		if(!fechaInicio.equals("") && !fechaFin.equals("")){
			strSQL.append(" AND sol.DF_FIRMA_CONTRATO >= TO_DATE(?, 'DD/MM/YYYY') AND sol.DF_FIRMA_CONTRATO < TO_DATE(?, 'DD/MM/YYYY')+1 ");
			conditions.add(fechaInicio);
			conditions.add(fechaFin);
		}

		if (claveMoneda != null && !"".equals(claveMoneda)) {
			if (claveMoneda.equals("0")) {
				strSQL.append(" AND sol.cs_multimoneda = ?");
				conditions.add("S");
			} else {
				strSQL.append(" AND sol.cs_multimoneda = ?");
				strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
				strSQL.append(" FROM cder_monto_x_solic mxs");
				strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
				strSQL.append(" AND mxs.ic_moneda = ?)");
				conditions.add("N");
				conditions.add(new Integer(claveMoneda));
			}
		}

		if (claveTipoContratacion != null && !"".equals(claveTipoContratacion)) {
			strSQL.append(" AND sol.ic_tipo_contratacion = ?");
			conditions.add(new Integer(claveTipoContratacion));
		}	
		

		strSQL.append(" ORDER BY sol.ic_solicitud");
		
		
		log.debug("..:: strSQL: "+strSQL.toString());
		log.debug("..:: conditions: "+conditions);
		log.info("getDocumentQueryFile(S) ::..");		
		
		return strSQL.toString();
	}//getDocumentQueryFile
	
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		ComunesPDF pdfDoc = new ComunesPDF();
		String   clasificacionEpo	 ="";
		int indiceCamposAdicionales =0;
		Vector lovDatos = new Vector(); 
		String dependencia ="", nombre_cedente="", rfc_cedente ="",  numero_contrato ="", clave_solicitud ="",
						clave_pyme="",monto_moneda ="", tipo_contratacion="",  fecha_inicio_contrato ="", fecha_fin_contrato ="",
						plazo_contrato ="", clasificacion_epo ="", objeto_contrato ="",fecha_extincion ="", numero_cuenta ="",
						cuenta_clabe="", banco_deposito ="", causas_rechazo ="", estatus_solicitud ="", 
						representante ="";						
		try {
		
			CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
			if(!claveEpo.equals(""))  {
				clasificacionEpo = cesionBean.clasificacionEpo(claveEpo);
				lovDatos = cesionBean.getCamposAdicionales(claveEpo, "5");	
				indiceCamposAdicionales = lovDatos.size();				
			}
			int tColum = 5 -indiceCamposAdicionales;
			
			if(tipo.equals("PDF") ) {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString()+"", 
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),  
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
				pdfDoc.setLTable(13,100);	
				pdfDoc.setLCell("A","celda01",ComunesPDF.CENTER);				
				pdfDoc.setLCell("Dependencia","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Pyme (Cedente) ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("RFC ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Representante Legal","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("No. Contrato ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto / Moneda	 ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de Contrataci�n ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Inicio Contrato ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Final Contrato ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo del Contrato ","celda01",ComunesPDF.CENTER);				
				if(!clasificacionEpo.equals("") ) {
					pdfDoc.setLCell(clasificacionEpo,"celda01",ComunesPDF.CENTER); 
					pdfDoc.setLCell("","celda01",ComunesPDF.CENTER); 
				}else {
					pdfDoc.setLCell("","celda01",ComunesPDF.CENTER,2);
				}
				pdfDoc.setLCell("B","celda01",ComunesPDF.CENTER);	
				if(indiceCamposAdicionales>0) {
					for(int i = 0; i < indiceCamposAdicionales; i++){
						List campos =(List)lovDatos.get(i);					
						pdfDoc.setLCell( campos.get(1).toString(),"celda01",ComunesPDF.CENTER);
					}	
				}				
				pdfDoc.setLCell("Objeto del Contrato ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Extinci�n del Contrato  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Cuenta ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Cuenta CLABE ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Banco de Dep�sito ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Causa del Rechazo ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Estatus Actual ","celda01",ComunesPDF.CENTER);
				if(tColum>0){
					pdfDoc.setLCell(" ","celda01",ComunesPDF.CENTER, tColum);
				}
			
				while (rs.next())	{					
			
					dependencia = (rs.getString("dependencia") == null) ? "" : rs.getString("dependencia");
					nombre_cedente = (rs.getString("nombre_cedente") == null) ? "" : rs.getString("nombre_cedente");
					//FODEA-024-2014 MOD()
					String cedente[]=nombre_cedente.split(";");
					if(cedente.length == 1){
						nombre_cedente = nombre_cedente.replaceAll(";","");
					}else{
						nombre_cedente = nombre_cedente.replaceAll(";","\n\n");
					}
					//
					rfc_cedente =(rs.getString("rfc_cedente") == null) ? "" : rs.getString("rfc_cedente");					
					numero_contrato =(rs.getString("numero_contrato") == null) ? "" : rs.getString("numero_contrato");
					tipo_contratacion =(rs.getString("tipo_contratacion") == null) ? "" : rs.getString("tipo_contratacion");
					fecha_inicio_contrato =(rs.getString("fecha_inicio_contrato") == null) ? "" : rs.getString("fecha_inicio_contrato");
					fecha_fin_contrato =(rs.getString("fecha_fin_contrato") == null) ? "" : rs.getString("fecha_fin_contrato");
					plazo_contrato =(rs.getString("plazo_contrato") == null) ? "" : rs.getString("plazo_contrato");
					clasificacion_epo =(rs.getString("clasificacion_epo") == null) ? "" : rs.getString("clasificacion_epo");
					objeto_contrato =(rs.getString("objeto_contrato") == null) ? "" : rs.getString("objeto_contrato");
					fecha_extincion =(rs.getString("fecha_extincion") == null) ? "" : rs.getString("fecha_extincion");
					numero_cuenta =(rs.getString("numero_cuenta") == null) ? "" : rs.getString("numero_cuenta");
					cuenta_clabe =(rs.getString("cuenta_clabe") == null) ? "" : rs.getString("cuenta_clabe");
					banco_deposito =(rs.getString("banco_deposito") == null) ? "" : rs.getString("banco_deposito");
					causas_rechazo =(rs.getString("causas_rechazo") == null) ? "" : rs.getString("causas_rechazo");
					estatus_solicitud =(rs.getString("estatus_solicitud") == null) ? "" : rs.getString("estatus_solicitud");
						
					clave_solicitud = (rs.getString("CLAVE_SOLICITUD") == null) ? "" : rs.getString("CLAVE_SOLICITUD");
					clave_pyme = (rs.getString("CLAVE_PYME") == null) ? "" : rs.getString("CLAVE_PYME");				
					StringBuffer montosMonedaSol = cesionBean.getMontoMoneda(clave_solicitud);
					monto_moneda  =montosMonedaSol.toString();
				
			
					//Se obtiene el representante legal de la Pyme
					StringBuffer nombreContacto = new StringBuffer();
					UtilUsr utilUsr = new UtilUsr();
					boolean usuarioEncontrado = false;
					boolean perfilIndicado = false;
					List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(clave_pyme, "P");
					for(int i=0;i<usuariosPorPerfil.size();i++){
						String loginUsuarioEpo = (String)usuariosPorPerfil.get(i);
						Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
						perfilIndicado = true;
						if(i>0){
							nombreContacto.append(" / ");
						}
						nombreContacto.append(usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno()+" ");
						nombreContacto.append("\n");
					}										
					representante = nombreContacto.toString();				
			
					pdfDoc.setLCell("A","formas",ComunesPDF.CENTER);				
					pdfDoc.setLCell(dependencia,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(nombre_cedente,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(rfc_cedente, "formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(representante.replaceAll("<br/>", "").replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(numero_contrato,"formas",ComunesPDF.CENTER);					
					pdfDoc.setLCell(monto_moneda.toString().replaceAll("<br/>", "\n").replaceAll(",", ""),"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(tipo_contratacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_inicio_contrato,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_fin_contrato,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(plazo_contrato,"formas",ComunesPDF.CENTER);	
					if(!clasificacionEpo.equals("") ) {
						pdfDoc.setLCell(clasificacion_epo,"formas",ComunesPDF.CENTER); 
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER); 
					}else {
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER,2);
					}					
					pdfDoc.setLCell("B","formas",ComunesPDF.CENTER);	
					if(indiceCamposAdicionales>0) {
						for(int i = 1; i <=indiceCamposAdicionales; i++){
							String campo = (rs.getString("campo_adicional_"+i) == null) ? "" : rs.getString("campo_adicional_"+i); 
							pdfDoc.setLCell(campo,"formas",ComunesPDF.CENTER);
						}	
					}						
					pdfDoc.setLCell(objeto_contrato,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_extincion,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(numero_cuenta,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(cuenta_clabe,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(banco_deposito,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(causas_rechazo,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(estatus_solicitud,"formas",ComunesPDF.CENTER);
					if(tColum>0){
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER, tColum);
					}
				
				}//while (rs.next())	{
				pdfDoc.addLTable();
				pdfDoc.endDocument();	
			}
			
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	//**
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		ComunesPDF pdfDoc = new ComunesPDF();
		String   clasificacionEpo	 ="";
		int indiceCamposAdicionales =0;
		Vector lovDatos = new Vector();
		String dependencia ="", nombre_cedente="", rfc_cedente ="",  numero_contrato ="", clave_solicitud ="",
						clave_pyme="",monto_moneda ="", tipo_contratacion="",  fecha_inicio_contrato ="", fecha_fin_contrato ="",
						plazo_contrato ="", clasificacion_epo ="", objeto_contrato ="",fecha_extincion ="", numero_cuenta ="",
						cuenta_clabe="", banco_deposito ="", causas_rechazo ="", estatus_solicitud ="", 
						representante ="";							
		try {
		
			CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
			if(!claveEpo.equals(""))  {
				clasificacionEpo = cesionBean.clasificacionEpo(claveEpo);
				lovDatos = cesionBean.getCamposAdicionales(claveEpo, "5");	
				indiceCamposAdicionales = lovDatos.size();				
			}
			int tColum = 5 -indiceCamposAdicionales;
			
			System.out.println("indiceCamposAdicionales>>>>>"+indiceCamposAdicionales);
			System.out.println("tColum>>>>>"+tColum);
			
			
			if(tipo.equals("CSV") ) {			
				contenidoArchivo.append("Dependencia, Pyme (Cedente),	RFC,	Representante Legal,	No. Contrato,	Monto / Moneda	, Tipo de Contrataci�n, 	Fecha Inicio Contrato,	Fecha Final Contrato,	Plazo del Contrato, ");
				if(!clasificacionEpo.equals("") ) { contenidoArchivo.append( clasificacionEpo+","); } 
				if(indiceCamposAdicionales>0) {
					for(int i = 0; i < indiceCamposAdicionales; i++){
							List campos =(List)lovDatos.get(i);					
						contenidoArchivo.append(	campos.get(1).toString()+",");	
					}	
				}
				contenidoArchivo.append(" Objeto del Contrato,	Fecha de Extinci�n del Contrato,	Cuenta,	Cuenta CLABE,	Banco de Dep�sito,	Causa del Rechazo,	Estatus Actual \n");
			}
			
			if(tipo.equals("PDF") ) {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString()+"", 
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),  
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
				pdfDoc.setLTable(13,100);	
				pdfDoc.setLCell("A","celda01",ComunesPDF.CENTER);				
				pdfDoc.setLCell("Dependencia","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Pyme (Cedente) ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("RFC ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Representante Legal","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("No. Contrato ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto / Moneda	 ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de Contrataci�n ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Inicio Contrato ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Final Contrato ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo del Contrato ","celda01",ComunesPDF.CENTER);				
				if(!clasificacionEpo.equals("") ) {
					pdfDoc.setLCell(clasificacionEpo,"celda01",ComunesPDF.CENTER); 
					pdfDoc.setLCell("","celda01",ComunesPDF.CENTER); 
				}else {
					pdfDoc.setLCell("","celda01",ComunesPDF.CENTER,2);
				}
				pdfDoc.setLCell("B","celda01",ComunesPDF.CENTER);	
				if(indiceCamposAdicionales>0) {
					for(int i = 0; i < indiceCamposAdicionales; i++){
						List campos =(List)lovDatos.get(i);					
						pdfDoc.setLCell( campos.get(1).toString(),"celda01",ComunesPDF.CENTER);
					}	
				}				
				pdfDoc.setLCell("Objeto del Contrato ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Extinci�n del Contrato  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Cuenta ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Cuenta CLABE ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Banco de Dep�sito ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Causa del Rechazo ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Estatus Actual ","celda01",ComunesPDF.CENTER);
				if(tColum>0){
					pdfDoc.setLCell(" ","celda01",ComunesPDF.CENTER, tColum);
				}
			
			}
			while (rs.next())	{					
			
				dependencia = (rs.getString("dependencia") == null) ? "" : rs.getString("dependencia");
				nombre_cedente = (rs.getString("nombre_cedente") == null) ? "" : rs.getString("nombre_cedente");
				rfc_cedente =(rs.getString("rfc_cedente") == null) ? "" : rs.getString("rfc_cedente");					
				numero_contrato =(rs.getString("numero_contrato") == null) ? "" : rs.getString("numero_contrato");
				tipo_contratacion =(rs.getString("tipo_contratacion") == null) ? "" : rs.getString("tipo_contratacion");
				fecha_inicio_contrato =(rs.getString("fecha_inicio_contrato") == null) ? "" : rs.getString("fecha_inicio_contrato");
				fecha_fin_contrato =(rs.getString("fecha_fin_contrato") == null) ? "" : rs.getString("fecha_fin_contrato");
				plazo_contrato =(rs.getString("plazo_contrato") == null) ? "" : rs.getString("plazo_contrato");
				clasificacion_epo =(rs.getString("clasificacion_epo") == null) ? "" : rs.getString("clasificacion_epo");
				objeto_contrato =(rs.getString("objeto_contrato") == null) ? "" : rs.getString("objeto_contrato");
				fecha_extincion =(rs.getString("fecha_extincion") == null) ? "" : rs.getString("fecha_extincion");
				numero_cuenta =(rs.getString("numero_cuenta") == null) ? "" : rs.getString("numero_cuenta");
				cuenta_clabe =(rs.getString("cuenta_clabe") == null) ? "" : rs.getString("cuenta_clabe");
				banco_deposito =(rs.getString("banco_deposito") == null) ? "" : rs.getString("banco_deposito");
				causas_rechazo =(rs.getString("causas_rechazo") == null) ? "" : rs.getString("causas_rechazo");
				estatus_solicitud =(rs.getString("estatus_solicitud") == null) ? "" : rs.getString("estatus_solicitud");
				clave_solicitud = (rs.getString("CLAVE_SOLICITUD") == null) ? "" : rs.getString("CLAVE_SOLICITUD");
				clave_pyme = (rs.getString("CLAVE_PYME") == null) ? "" : rs.getString("CLAVE_PYME");				
				//obtiene la moneda
				StringBuffer montosMonedaSol = cesionBean.getMontoMoneda(clave_solicitud);
				monto_moneda  =montosMonedaSol.toString();
				
				//Se obtiene el representante legal de la Pyme
				StringBuffer nombreContacto = new StringBuffer();
				UtilUsr utilUsr = new UtilUsr();
				boolean usuarioEncontrado = false;
				boolean perfilIndicado = false;
				List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(clave_pyme, "P");
				for(int i=0;i<usuariosPorPerfil.size();i++){
					String loginUsuarioEpo = (String)usuariosPorPerfil.get(i);
					Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
					perfilIndicado = true;
					if(i>0){
						nombreContacto.append(" / ");  
					}
					nombreContacto.append(usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno()+" ");
					nombreContacto.append("\n");
				}										
				representante = nombreContacto.toString();	
					
				if(tipo.equals("CSV") ) {
				//FODEA-024-2014 MOD()
					String cedente[]=nombre_cedente.split(";");
					if(cedente.length == 1){
						nombre_cedente = nombre_cedente.replaceAll(";","");
					}else{
						//pyme_cedente = pyme_cedente.replaceAll(";","\n\n");
					}
					//
					contenidoArchivo.append(dependencia.replaceAll(",","")+","+  
						nombre_cedente.replaceAll(",","")+","+
						rfc_cedente.replaceAll(",","")+","+
						representante.replaceAll("\n", "").replaceAll(",", "")+","+
						numero_contrato.replaceAll(",","")+","+
						monto_moneda.toString().replaceAll("<br/>", "|").replaceAll(",", "")+","+
						tipo_contratacion.replaceAll(",","")+","+
						fecha_inicio_contrato.replaceAll(",","")+","+
						fecha_fin_contrato.replaceAll(",","")+","+
						plazo_contrato.replaceAll(",","")+",");
					 if(!clasificacionEpo.equals("") ) {
						contenidoArchivo.append(clasificacion_epo.replaceAll(",","")+",");
					 }					
					if(indiceCamposAdicionales>0) {
						for(int i = 1; i <=indiceCamposAdicionales; i++){
						String campo = (rs.getString("campo_adicional_"+i) == null) ? "" : rs.getString("campo_adicional_"+i); 
						if(i==1||i==2){
							campo= "'"+ campo;
						}
						contenidoArchivo.append(campo.replaceAll(",","")+",");
						}	
					}	
					
					contenidoArchivo.append(objeto_contrato.replaceAll(",","")+","+
						fecha_extincion.replaceAll(",","")+","+"'"+ numero_cuenta.replaceAll(",","")+","+
						"'"+cuenta_clabe.replaceAll(",","")+","+
						banco_deposito.replaceAll(",","")+","+
						causas_rechazo.replaceAll(",","")+","+
						estatus_solicitud.replaceAll(",","")+"\n");
					
				}	
				
				if(tipo.equals("PDF") ) {
					pdfDoc.setLCell("A","formas",ComunesPDF.CENTER);				
					pdfDoc.setLCell(dependencia,"formas",ComunesPDF.CENTER);
					//FODEA-024-2014 MOD()
					String cedente[]=nombre_cedente.split(";");
					if(cedente.length == 1){
						nombre_cedente = nombre_cedente.replaceAll(";","");
					}else{
						nombre_cedente = nombre_cedente.replaceAll(";","\n\n");
					}
					//
					
					pdfDoc.setLCell(nombre_cedente,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(rfc_cedente, "formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(representante.replaceAll("<br/>", "").replaceAll(",", ""),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(numero_contrato,"formas",ComunesPDF.CENTER);					
					pdfDoc.setLCell(monto_moneda.toString().replaceAll("<br/>", "\n").replaceAll(",", ""),"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(tipo_contratacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_inicio_contrato,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_fin_contrato,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(plazo_contrato,"formas",ComunesPDF.CENTER);	
					if(!clasificacionEpo.equals("") ) {
						pdfDoc.setLCell(clasificacion_epo,"formas",ComunesPDF.CENTER); 
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER); 
					}else {
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER,2);
					}					
					pdfDoc.setLCell("B","formas",ComunesPDF.CENTER);	
					if(indiceCamposAdicionales>0) {
						for(int i = 1; i <=indiceCamposAdicionales; i++){
							String campo = (rs.getString("campo_adicional_"+i) == null) ? "" : rs.getString("campo_adicional_"+i); 
							pdfDoc.setLCell(campo+" ","formas",ComunesPDF.CENTER);
						}	
					}						
					pdfDoc.setLCell(objeto_contrato,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_extincion,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(numero_cuenta,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(cuenta_clabe,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(banco_deposito,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(causas_rechazo,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(estatus_solicitud,"formas",ComunesPDF.CENTER);
					if(tColum>0){
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER, tColum);
					}
				}
				
				
			}// while (rs.next())	{
				
				if(tipo.equals("CSV") ) {
					creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
					nombreArchivo = creaArchivo.getNombre();
				}
			
		
			
			if(tipo.equals("PDF") ) {
				
				pdfDoc.addLTable();
				
				if(!acuse.equals("")){
					pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);					
					pdfDoc.setLTable(2,50);
					pdfDoc.setLCell("Cifras de Control ","celda01",ComunesPDF.CENTER,2);
					pdfDoc.setLCell("N�mero de Acuse","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell(acuse,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("Fecha de Carga","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaCarga,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("Hora de Carga","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell(HoraCarga,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("Usuario de Captura","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell(Usuario,"formas",ComunesPDF.CENTER);
					pdfDoc.addLTable();   
				}
				
				pdfDoc.endDocument();
			}
		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
	
	//Getters
	public List getConditions() {  return conditions;  }  
	public String getPaginaNo() {return paginaNo;}
	public String getPaginaOffset() {return paginaOffset;}
	public String getClavePyme() {return this.clavePyme;}
	public String getClaveEpo() {return this.claveEpo;}
	public String getClaveIfCesionario() {return this.claveIfCesionario;}
	public String getNumeroContrato() {return this.numeroContrato;}
	public String getClaveMoneda() {return this.claveMoneda;}
	public String getClaveTipoContratacion() {return this.claveTipoContratacion;}
	
	//Setters
	public void setPaginaNo(String newPaginaNo) {this.paginaNo = newPaginaNo;  }
	public void setPaginaOffset(String paginaOffset) {this.paginaOffset=paginaOffset;}
	public void setClavePyme(String clavePyme) {this.clavePyme = clavePyme;}
	public void setClaveEpo(String claveEpo) {this.claveEpo = claveEpo;}
	public void setClaveIfCesionario(String claveIfCesionario) {this.claveIfCesionario = claveIfCesionario;}
	public void setNumeroContrato(String numeroContrato) {this.numeroContrato = numeroContrato;}
	public void setClaveMoneda(String claveMoneda) {this.claveMoneda = claveMoneda;}
	public void setClaveTipoContratacion(String claveTipoContratacion) {this.claveTipoContratacion = claveTipoContratacion;}

	public String getClave_solicitud() {
		return clave_solicitud;  
	}

	public void setClave_solicitud(String clave_solicitud) {
		this.clave_solicitud = clave_solicitud;
	}

	public String getAcuse() {
		return acuse;
	}

	public void setAcuse(String acuse) {
		this.acuse = acuse;
	}

	public String getFechaCarga() {
		return fechaCarga;
	}

	public void setFechaCarga(String fechaCarga) {
		this.fechaCarga = fechaCarga;
	}

	public String getHoraCarga() {
		return HoraCarga;
	}

	public void setHoraCarga(String HoraCarga) {
		this.HoraCarga = HoraCarga;
	}

	public String getUsuario() {
		return Usuario;
	}

	public void setUsuario(String Usuario) {
		this.Usuario = Usuario;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getRepIf2() {
		return repIf2;
	}

	public void setRepIf2(String repIf2) {
		this.repIf2 = repIf2;
	}

}