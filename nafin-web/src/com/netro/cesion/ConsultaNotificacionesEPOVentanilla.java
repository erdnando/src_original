package com.netro.cesion;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsultaNotificacionesEPOVentanilla implements  IQueryGeneratorRegExtJS {	

	public ConsultaNotificacionesEPOVentanilla(){}

	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaNotificacionesEPOVentanilla.class);

	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer 		qrySentencia;  
	private List 		conditions;
	private String 	paginaOffset;
	private String 	paginaNo;
	private String loginUsuario;
	private String claveEpo;
	private String claveIf;
	private String clavePyme;
	private String claveEstatusSol;
	private String numeroContrato;
	private String fechaVigenciaIni;
	private String fechaSolicitudIni;
	private String fechaVigenciaFin;
	private String fechaSolicitudFin;
	private String plazoContrato;
	private String claveTipoPlazo;
	
	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */
		public String getAggregateCalculationQuery() {
			log.info("getAggregateCalculationQuery(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();

			log.debug("..:: qrySentencia "+qrySentencia.toString());
			log.debug("..:: conditions: "+conditions);
			log.info("getAggregateCalculationQuery(S)");
			return qrySentencia.toString();
		}
	
	
	 /**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
		public String getDocumentQuery() {
			log.info("getDocumentQuery(E)");
			qrySentencia = new StringBuffer();
			conditions = new ArrayList();
				
			qrySentencia.append(" SELECT sol.ic_solicitud AS clave_solicitud ");			
			qrySentencia.append(" FROM cder_solicitud sol");
			qrySentencia.append(", comcat_epo epo");
			qrySentencia.append(", comcat_if cif");
			qrySentencia.append(", comcat_pyme pym");
			qrySentencia.append(", comrel_pyme_epo cpe");
			qrySentencia.append(", cdercat_tipo_contratacion tcn");
			qrySentencia.append(", cdercat_clasificacion_epo cep");
			qrySentencia.append(", cdercat_estatus_solic sts");
			qrySentencia.append(", cdercat_tipo_plazo stp");
			qrySentencia.append(", cdercat_ventanilla_pago cvp");
			qrySentencia.append(" WHERE sol.ic_epo = epo.ic_epo");
			qrySentencia.append(" AND sol.ic_if = cif.ic_if");
			qrySentencia.append(" AND sol.ic_pyme = pym.ic_pyme");
			qrySentencia.append(" AND sol.ic_epo = cpe.ic_epo(+)");
			qrySentencia.append(" AND sol.ic_pyme = cpe.ic_pyme(+)");
			qrySentencia.append(" AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion");
			qrySentencia.append(" AND sol.ic_epo = cep.ic_epo");
			qrySentencia.append(" AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo");
			qrySentencia.append(" AND sol.ic_estatus_solic = sts.ic_estatus_solic");
			qrySentencia.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
			qrySentencia.append(" AND sol.ic_epo = cvp.ic_epo(+)");
			qrySentencia.append(" AND sol.ic_ventanilla_pago = cvp.ic_ventanilla_pago(+)");
			
			if (loginUsuario != null && !"".equals(loginUsuario)) {
				qrySentencia.append(" AND SUBSTR(cep.cg_responsable, 1, 8) = ?");
				conditions.add(loginUsuario);
			}

			if(!claveEpo.equals("")){
				qrySentencia.append(" AND sol.ic_epo = ?");
				conditions.add(new Integer(claveEpo));
			}
			if(!claveIf.equals("")){
				qrySentencia.append(" AND sol.ic_if = ?");
				conditions.add(new Integer(claveIf));
			}
			if(!clavePyme.equals("")){
				qrySentencia.append(" AND sol.ic_pyme = ?");
				conditions.add(new Long(clavePyme));
			}
			if(!claveEstatusSol.equals("")){
				qrySentencia.append(" AND sol.ic_estatus_solic = ?");
				conditions.add(new Integer(claveEstatusSol));
			}else{
				qrySentencia.append(" AND sol.ic_estatus_solic IN (?,?,?,?,?,?)");
				conditions.add(new Integer(9));
				conditions.add(new Integer(11));
				conditions.add(new Integer(12));
				conditions.add(new Integer(15));
				conditions.add(new Integer(16));
				conditions.add(new Integer(17));
			}
			if(!numeroContrato.equals("")){
				qrySentencia.append(" AND sol.cg_no_contrato = ?");
				conditions.add(numeroContrato);
			}
			if(!fechaVigenciaIni.equals("") && !fechaVigenciaFin.equals("")){
				qrySentencia.append(" AND sol.df_fecha_vigencia_ini >= TO_DATE(?, 'dd/mm/yyyy')");
				qrySentencia.append(" AND sol.df_fecha_vigencia_fin <= TO_DATE(?, 'dd/mm/yyyy')");
				conditions.add(fechaVigenciaIni);
				conditions.add(fechaVigenciaFin);
			}
			if(!fechaSolicitudIni.equals("") && !fechaSolicitudFin.equals("")){
				qrySentencia.append(" AND sol.df_fecha_solicitud_ini >= TO_DATE(?, 'dd/mm/yyyy')");
				qrySentencia.append(" AND sol.df_fecha_solicitud_ini <= TO_DATE(?, 'dd/mm/yyyy')");
				conditions.add(fechaSolicitudIni);
				conditions.add(fechaSolicitudFin);
			}
					
			if (plazoContrato != null && !"".equals(plazoContrato)) {
				qrySentencia.append(" AND sol.ig_plazo = ?");
				conditions.add(new Integer(plazoContrato));
			}		
	
			if (claveTipoPlazo != null && !"".equals(claveTipoPlazo)) {
				qrySentencia.append(" AND sol.ic_tipo_plazo = ?");
				conditions.add(new Integer(claveTipoPlazo));
			}
			
			qrySentencia.append(" ORDER BY sol.df_alta_solicitud DESC");
			
			log.debug("..:: qrySentencia: "+qrySentencia.toString());
			log.debug("..:: conditions: "+conditions);
			log.info("getDocumentQuery(S)");
			return qrySentencia.toString();
		}

	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */
		public String getDocumentSummaryQueryForIds(List pageIds) {
			log.info("getDocumentSummaryQueryForIds(E)");
			qrySentencia 	= new StringBuffer();
			conditions 		= new ArrayList();
			
			
					qrySentencia.append(" SELECT sol.ic_solicitud AS CLAVE_SOLICITUD,");
			qrySentencia.append(" sol.ic_pyme AS CLAVE_PYME,");
			qrySentencia.append(" sol.ic_epo AS clave_epo,");
			qrySentencia.append(" sol.ic_if AS clave_if,");
			qrySentencia.append(" cif.cg_razon_social AS NOMBRE_CESINARIO,");
			//qrySentencia.append(" pym.cg_razon_social AS NOMBRE_PYME,");
			qrySentencia.append(" pym.cg_razon_social AS NOMBRE_PYME,");//FODEA-024-2014 MOD()
			qrySentencia.append(" pym.cg_rfc AS RFC,");
			qrySentencia.append(" cpe.cg_pyme_epo_interno AS NO_PROVEEDOR,");
			qrySentencia.append(" TO_CHAR(sol.df_fecha_solicitud_ini, 'dd/mm/yyyy') AS FECHA_SOLICITUD,");
			qrySentencia.append(" sol.cg_no_contrato AS NUMERO_CONTRATO,");
			qrySentencia.append(" tcn.cg_nombre AS TIPO_CONTRATACION,");
			qrySentencia.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS F_INI_CONTRATO,");
			qrySentencia.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS F_FIN_CONTRATO,");
			qrySentencia.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS PLAZO_CONTRATO,");
			qrySentencia.append(" cep.cg_area AS CLASFIFICACION_EPO,");
			qrySentencia.append(" sol.cg_campo1 AS CAMPO1,");
			qrySentencia.append(" sol.cg_campo2 AS CAMPO2,");
			qrySentencia.append(" sol.cg_campo3 AS CAMPO3,");
			qrySentencia.append(" sol.cg_campo4 AS CAMPO4,");
			qrySentencia.append(" sol.cg_campo5 AS CAMPO5,");
			qrySentencia.append(" cvp.cg_descripcion AS VENTANILLA_DE_PAGO,");
			qrySentencia.append(" sol.cg_sup_adm_resob AS SUPERVISOR,");
			qrySentencia.append(" sol.cg_numero_telefono AS TELEFONO,");
			qrySentencia.append(" sol.cg_obj_contrato AS OBJETO_CONTRATO,");
			qrySentencia.append(" sol.cg_comentarios AS COMENTARIOS,");
			qrySentencia.append(" sol.fn_monto_credito AS MONTO_CREDITO,");
			qrySentencia.append(" sol.cg_referencia AS REFERENCIA,");
			qrySentencia.append(" TO_CHAR(sol.df_vencimiento_credito, 'dd/mm/yyyy') AS F_VENCIMIENTO_CRE,");
			qrySentencia.append(" sol.cg_banco_deposito AS BANCO_DEPOSITO,");
			qrySentencia.append(" sol.cg_cuenta AS CUENTA,");
			qrySentencia.append(" sol.cg_cuenta_clabe AS CUENTA_CLABE,");
			qrySentencia.append(" sol.ic_estatus_solic AS IC_ESTATUS,");
			qrySentencia.append(" sts.cg_descripcion AS ESTATUS , ");
			qrySentencia.append(" sol.cg_causas_rechazo_notif AS CAUSA_RECHAZO_NOTIF,");
			qrySentencia.append(" sol.CG_OBSERV_RECHAZO AS CAUSA_RECHAZO, ");
			qrySentencia.append(" TO_CHAR(sol.df_firma_contrato, 'dd/mm/yyyy') AS firma_contrato"); 
			qrySentencia.append(" ,SOL.CG_EMPRESAS_REPRESENTADAS AS empresas");
			qrySentencia.append(" ,sol.IC_GRUPO_CESION  AS GRUPO_CESION  ");			
			
			qrySentencia.append(" FROM cder_solicitud sol");
			qrySentencia.append(", comcat_epo epo");
			qrySentencia.append(", comcat_if cif");
			qrySentencia.append(", comcat_pyme pym");
			qrySentencia.append(", comrel_pyme_epo cpe");
			qrySentencia.append(", cdercat_tipo_contratacion tcn");
			qrySentencia.append(", cdercat_clasificacion_epo cep");
			qrySentencia.append(", cdercat_estatus_solic sts");
			qrySentencia.append(", cdercat_tipo_plazo stp");
			qrySentencia.append(", cdercat_ventanilla_pago cvp");
			qrySentencia.append(" WHERE sol.ic_epo = epo.ic_epo");
			qrySentencia.append(" AND sol.ic_if = cif.ic_if");
			qrySentencia.append(" AND sol.ic_pyme = pym.ic_pyme");
			qrySentencia.append(" AND sol.ic_epo = cpe.ic_epo(+)");
			qrySentencia.append(" AND sol.ic_pyme = cpe.ic_pyme(+)");
			qrySentencia.append(" AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion");
			qrySentencia.append(" AND sol.ic_epo = cep.ic_epo");
			qrySentencia.append(" AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo");
			qrySentencia.append(" AND sol.ic_estatus_solic = sts.ic_estatus_solic");
			qrySentencia.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
			qrySentencia.append(" AND sol.ic_epo = cvp.ic_epo(+)");
			qrySentencia.append(" AND sol.ic_ventanilla_pago = cvp.ic_ventanilla_pago(+)");
			qrySentencia.append(" AND (");
			
			for (int i = 0; i < pageIds.size(); i++) {
				List lItem = (ArrayList)pageIds.get(i);
		
				if(i > 0){qrySentencia.append("  OR  ");}		
					qrySentencia.append("sol.ic_solicitud = ?");
					conditions.add(new Long(lItem.get(0).toString()));
				}

		qrySentencia.append(" ) ");	
		
			
			qrySentencia.append(" ORDER BY sol.df_alta_solicitud DESC");
			
			
			log.debug("..:: qrySentencia: "+qrySentencia.toString());
			log.debug("..:: conditions: "+conditions);
			log.info("getDocumentSummaryQueryForIds(S)");
			return qrySentencia.toString();
		}

		public String getDocumentQueryFile() {
			log.info("getDocumentQueryFile(E)");
			qrySentencia = new StringBuffer();
			conditions = new ArrayList();

			
			qrySentencia.append(" SELECT sol.ic_solicitud AS CLAVE_SOLICITUD,");
			qrySentencia.append(" sol.ic_pyme AS clave_pyme,");
			qrySentencia.append(" sol.ic_epo AS clave_epo,");
			qrySentencia.append(" sol.ic_if AS clave_if,");
			qrySentencia.append(" cif.cg_razon_social AS NOMBRE_CESINARIO,");
			//qrySentencia.append(" pym.cg_razon_social AS NOMBRE_PYME,");
			qrySentencia.append(" pym.cg_razon_social||';'||SOL.CG_EMPRESAS_REPRESENTADAS AS NOMBRE_PYME,");//FODEA-024-2014 MOD()
			qrySentencia.append(" pym.cg_rfc AS RFC,");
			qrySentencia.append(" cpe.cg_pyme_epo_interno AS NO_PROVEEDOR,");
			qrySentencia.append(" TO_CHAR(sol.df_fecha_solicitud_ini, 'dd/mm/yyyy') AS FECHA_SOLICITUD,");
			qrySentencia.append(" sol.cg_no_contrato AS NUMERO_CONTRATO,");
			qrySentencia.append(" tcn.cg_nombre AS TIPO_CONTRATACION,");
			qrySentencia.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS F_INI_CONTRATO,");
			qrySentencia.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS F_FIN_CONTRATO,");
			qrySentencia.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS PLAZO_CONTRATO,");
			qrySentencia.append(" cep.cg_area AS CLASFIFICACION_EPO,");
			qrySentencia.append(" sol.cg_campo1 AS CAMPO1,");
			qrySentencia.append(" sol.cg_campo2 AS CAMPO2,");
			qrySentencia.append(" sol.cg_campo3 AS CAMPO3,");
			qrySentencia.append(" sol.cg_campo4 AS CAMPO4,");
			qrySentencia.append(" sol.cg_campo5 AS CAMPO5,");
			qrySentencia.append(" cvp.cg_descripcion AS VENTANILLA_DE_PAGO,");
			qrySentencia.append(" sol.cg_sup_adm_resob AS SUPERVISOR,");
			qrySentencia.append(" sol.cg_numero_telefono AS TELEFONO,");
			qrySentencia.append(" sol.cg_obj_contrato AS OBJETO_CONTRATO,");
			qrySentencia.append(" sol.cg_comentarios AS COMENTARIOS,");
			qrySentencia.append(" sol.fn_monto_credito AS MONTO_CREDITO,");
			qrySentencia.append(" sol.cg_referencia AS REFERENCIA,");
			qrySentencia.append(" TO_CHAR(sol.df_vencimiento_credito, 'dd/mm/yyyy') AS F_VENCIMIENTO_CRE,");
			qrySentencia.append(" sol.cg_banco_deposito AS BANCO_DEPOSITO,");
			qrySentencia.append(" sol.cg_cuenta AS CUENTA,");
			qrySentencia.append(" sol.cg_cuenta_clabe AS CUENTA_CLABE,");
			qrySentencia.append(" sol.ic_estatus_solic AS IC_ESTATUS,");
			qrySentencia.append(" sts.cg_descripcion AS ESTATUS, ");
			qrySentencia.append(" sol.cg_causas_rechazo_notif AS CAUSA_RECHAZO_NOTIF,");
			qrySentencia.append(" sol.CG_OBSERV_RECHAZO AS CAUSA_RECHAZO,");
			qrySentencia.append(" TO_CHAR(sol.df_firma_contrato, 'dd/mm/yyyy') AS firma_contrato");
			qrySentencia.append(" ,sol.IC_GRUPO_CESION  AS GRUPO_CESION  ");	 
			
			qrySentencia.append(" FROM cder_solicitud sol");
			qrySentencia.append(", comcat_epo epo");
			qrySentencia.append(", comcat_if cif");
			qrySentencia.append(", comcat_pyme pym");
			qrySentencia.append(", comrel_pyme_epo cpe");
			qrySentencia.append(", cdercat_tipo_contratacion tcn");
			qrySentencia.append(", cdercat_clasificacion_epo cep");
			qrySentencia.append(", cdercat_estatus_solic sts");
			qrySentencia.append(", cdercat_tipo_plazo stp");
			qrySentencia.append(", cdercat_ventanilla_pago cvp");
			qrySentencia.append(" WHERE sol.ic_epo = epo.ic_epo");
			qrySentencia.append(" AND sol.ic_if = cif.ic_if");
			qrySentencia.append(" AND sol.ic_pyme = pym.ic_pyme");
			qrySentencia.append(" AND sol.ic_epo = cpe.ic_epo(+)");
			qrySentencia.append(" AND sol.ic_pyme = cpe.ic_pyme(+)");
			qrySentencia.append(" AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion");
			qrySentencia.append(" AND sol.ic_epo = cep.ic_epo");
			qrySentencia.append(" AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo");
			qrySentencia.append(" AND sol.ic_estatus_solic = sts.ic_estatus_solic");
			qrySentencia.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
			qrySentencia.append(" AND sol.ic_epo = cvp.ic_epo(+)");
			qrySentencia.append(" AND sol.ic_ventanilla_pago = cvp.ic_ventanilla_pago(+)");			
			
			if (loginUsuario != null && !"".equals(loginUsuario)) {
				qrySentencia.append(" AND SUBSTR(cep.cg_responsable, 1, 8) = ?");
				conditions.add(loginUsuario);
			}

			if(!claveEpo.equals("")){
				qrySentencia.append(" AND sol.ic_epo = ?");
				conditions.add(new Integer(claveEpo));
			}
			if(!claveIf.equals("")){
				qrySentencia.append(" AND sol.ic_if = ?");
				conditions.add(new Integer(claveIf));
			}
			if(!clavePyme.equals("")){
				qrySentencia.append(" AND sol.ic_pyme = ?");
				conditions.add(new Long(clavePyme));
			}
			if(!claveEstatusSol.equals("")){
				qrySentencia.append(" AND sol.ic_estatus_solic = ?");
				conditions.add(new Integer(claveEstatusSol));
			}else{
				qrySentencia.append(" AND sol.ic_estatus_solic IN (?,?,?,?,?,?)");
				conditions.add(new Integer(9));
				conditions.add(new Integer(11));
				conditions.add(new Integer(12));
				conditions.add(new Integer(15));
				conditions.add(new Integer(16));
				conditions.add(new Integer(17));
			}
			if(!numeroContrato.equals("")){
				qrySentencia.append(" AND sol.cg_no_contrato = ?");
				conditions.add(numeroContrato);
			}
			if(!fechaVigenciaIni.equals("") && !fechaVigenciaFin.equals("")){
				qrySentencia.append(" AND sol.df_fecha_vigencia_ini >= TO_DATE(?, 'dd/mm/yyyy')");
				qrySentencia.append(" AND sol.df_fecha_vigencia_fin <= TO_DATE(?, 'dd/mm/yyyy')");
				conditions.add(fechaVigenciaIni);
				conditions.add(fechaVigenciaFin);
			}
			if(!fechaSolicitudIni.equals("") && !fechaSolicitudFin.equals("")){
				qrySentencia.append(" AND sol.df_fecha_solicitud_ini >= TO_DATE(?, 'dd/mm/yyyy')");
				qrySentencia.append(" AND sol.df_fecha_solicitud_ini <= TO_DATE(?, 'dd/mm/yyyy')");
				conditions.add(fechaSolicitudIni);
				conditions.add(fechaSolicitudFin);
			}
					
			if (plazoContrato != null && !"".equals(plazoContrato)) {
				qrySentencia.append(" AND sol.ig_plazo = ?");
				conditions.add(new Integer(plazoContrato));
			}		
	
			if (claveTipoPlazo != null && !"".equals(claveTipoPlazo)) {
				qrySentencia.append(" AND sol.ic_tipo_plazo = ?");
				conditions.add(new Integer(claveTipoPlazo));
			}
			
			qrySentencia.append(" ORDER BY sol.df_alta_solicitud DESC");
			

		
			log.debug("..:: qrySentencia: "+qrySentencia.toString());
			log.debug("..:: conditions: "+conditions);
			log.info("getDocumentQueryFile(S)");
			return qrySentencia.toString();
		}//getDocumentQueryFile


	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
	
		StringBuffer linea = new StringBuffer(1024);
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {		
		
			CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
			int indiceCamposAdicionales =0;
			String nombreClasificacionEpo = cesionBean.clasificacionEpo(claveEpo);	
			Vector lovDatos = new Vector();
			 lovDatos = cesionBean.getCamposAdicionales(claveEpo, "5");
			indiceCamposAdicionales =	lovDatos.size();	
			
			HttpSession session = request.getSession();
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
			session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
	
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
			pdfDoc.addText("Consulta Solicitud de Consentimiento ","formas",ComunesPDF.CENTER);
			pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
			
		
			/*int columnas = 11;			
			if(!nombreClasificacionEpo.equals("")) columnas +=1;
			if(indiceCamposAdicionales >0) columnas +=indiceCamposAdicionales;			
			
			*/
			int columnas =13-indiceCamposAdicionales;			
			
			
			//integracion de Titulos
			pdfDoc.setTable(13, 100);			
			pdfDoc.setCell("Nombre Cesionario","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Pyme (Cedente)","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("RFC ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("No.Proveedor ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha de Solicitud PyME ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("No.Contrato ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Firma Contrato ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto / Moneda ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo de Contrataci�n ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("F. InicioContrato ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("F. FinalContrato ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Plazo del Contrato ","celda01",ComunesPDF.CENTER);			
			if(!nombreClasificacionEpo.equals(""))
				pdfDoc.setCell(nombreClasificacionEpo,"celda01",ComunesPDF.CENTER);	
				
			if (lovDatos !=null ) {
				for (int i=0; i<lovDatos.size(); i++) { 
					Vector lovRegistro = (Vector) lovDatos.get(i);
					String lsNombreCampo 	= (String) lovRegistro.get(1);
					pdfDoc.setCell(lsNombreCampo,"celda01",ComunesPDF.CENTER);					
				} //for
			}	
			pdfDoc.setCell("Ventanilla de Pago ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Supervisor/Administrador/Residente de Obra ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tel�fono ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Objeto del Contrato ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Comentarios ","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Monto delCr�dito ","celda01",ComunesPDF.CENTER);	
			pdfDoc.setCell("Referencia /No. Cr�dito ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Fecha Vencimiento Cr�dito ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Banco de Dep�sito ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Cuenta","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Cuenta CLABE","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);			
			pdfDoc.setCell("    ","celda01",ComunesPDF.CENTER, ++columnas);	
			
		
				
			while (reg.next()) {
			
				StringBuffer montosPorMoneda = cesionBean.getMontoMoneda(reg.getString("CLAVE_SOLICITUD"));		
				String nombre_cesinario =  (reg.getString("NOMBRE_CESINARIO")==null)?"":reg.getString("NOMBRE_CESINARIO");
				String nombre_pyme =  (reg.getString("NOMBRE_PYME")==null)?"":reg.getString("NOMBRE_PYME");
				String empresas =  (reg.getString("EMPRESAS")==null)?"":reg.getString("EMPRESAS");
				//FODEA-024-2014 MOD()
				String pymeEmpresas = "";
				if(!"".equals(empresas)){
					pymeEmpresas= nombre_pyme+";"+empresas;
					pymeEmpresas = pymeEmpresas.replaceAll(";","\n\n");
				}else{
					pymeEmpresas= nombre_pyme;
				}
				//				
				String rfc =  (reg.getString("RFC")==null)?"":reg.getString("RFC");
				String no_proveedor =  (reg.getString("NO_PROVEEDOR")==null)?"":reg.getString("NO_PROVEEDOR");
				String fecha_solicitud =  (reg.getString("FECHA_SOLICITUD")==null)?"":reg.getString("FECHA_SOLICITUD");
				String numero_contrato =  (reg.getString("NUMERO_CONTRATO")==null)?"":reg.getString("NUMERO_CONTRATO");
				String tipo_contratacion =  (reg.getString("TIPO_CONTRATACION")==null)?"":reg.getString("TIPO_CONTRATACION");
				String f_ini_contrato =  (reg.getString("F_INI_CONTRATO")==null)?"":reg.getString("F_INI_CONTRATO");
				String f_fin_contrato =  (reg.getString("F_FIN_CONTRATO")==null)?"":reg.getString("F_FIN_CONTRATO");
				String plazo_contrato =  (reg.getString("PLAZO_CONTRATO")==null)?"":reg.getString("PLAZO_CONTRATO");				
				String clasificacion_epo =  (reg.getString("CLASFIFICACION_EPO")==null)?"":reg.getString("CLASFIFICACION_EPO");
				String campo1 =  (reg.getString("CAMPO1")==null)?"":reg.getString("CAMPO1");
				String campo2 =  (reg.getString("CAMPO2")==null)?"":reg.getString("CAMPO2");
				String campo3 =  (reg.getString("CAMPO3")==null)?"":reg.getString("CAMPO3");
				String campo4 =  (reg.getString("CAMPO4")==null)?"":reg.getString("CAMPO4");
				String campo5 =  (reg.getString("CAMPO5")==null)?"":reg.getString("CAMPO5");			
				String ventanilla_de_pago =  (reg.getString("VENTANILLA_DE_PAGO")==null)?"":reg.getString("VENTANILLA_DE_PAGO");
				String supervisor =  (reg.getString("SUPERVISOR")==null)?"":reg.getString("SUPERVISOR");
				String telefono =  (reg.getString("TELEFONO")==null)?"":reg.getString("TELEFONO");
				String objeto_contrato =  (reg.getString("OBJETO_CONTRATO")==null)?"":reg.getString("OBJETO_CONTRATO");
				String comentarios =  (reg.getString("COMENTARIOS")==null)?"":reg.getString("COMENTARIOS");
				String monto_credito =  (reg.getString("MONTO_CREDITO")==null)?"":reg.getString("MONTO_CREDITO");
				String referencia =  (reg.getString("REFERENCIA")==null)?"":reg.getString("REFERENCIA");
				String f_vencimiento_cre =  (reg.getString("F_VENCIMIENTO_CRE")==null)?"":reg.getString("F_VENCIMIENTO_CRE");
				String banco_deposito =  (reg.getString("BANCO_DEPOSITO")==null)?"":reg.getString("BANCO_DEPOSITO");
				String cuenta =  (reg.getString("CUENTA")==null)?"":reg.getString("CUENTA");
				String cuenta_clabe =  (reg.getString("CUENTA_CLABE")==null)?"":reg.getString("CUENTA_CLABE");
				String estatus =  (reg.getString("ESTATUS")==null)?"":reg.getString("ESTATUS");
				String clave_solicitud =  (reg.getString("CLAVE_SOLICITUD")==null)?"":reg.getString("CLAVE_SOLICITUD");
				String ic_estatus =  (reg.getString("IC_ESTATUS")==null)?"":reg.getString("IC_ESTATUS");	
				String firmaContrato =  (reg.getString("FIRMA_CONTRATO")==null)?"":reg.getString("FIRMA_CONTRATO");	
			
				pdfDoc.setCell(nombre_cesinario,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(pymeEmpresas,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(rfc,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(no_proveedor,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fecha_solicitud,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(numero_contrato,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(firmaContrato,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(montosPorMoneda.toString().replaceAll("<br/>", "\n").replaceAll(",", ""),"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(tipo_contratacion,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(f_ini_contrato,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(f_fin_contrato,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(plazo_contrato,"formas",ComunesPDF.CENTER);
				if(!nombreClasificacionEpo.equals(""))
					pdfDoc.setCell(clasificacion_epo,"formas",ComunesPDF.CENTER);
				if (lovDatos !=null ) {
					for (int i=0; i<lovDatos.size(); i++) { 
						if(i==0) pdfDoc.setCell(campo1,"formas",ComunesPDF.CENTER);
						if(i==1) pdfDoc.setCell(campo2,"formas",ComunesPDF.CENTER);
						if(i==2) pdfDoc.setCell(campo3,"formas",ComunesPDF.CENTER);
						if(i==3) pdfDoc.setCell(campo4,"formas",ComunesPDF.CENTER);
						if(i==4) pdfDoc.setCell(campo5,"formas",ComunesPDF.CENTER);
					}
				}
				pdfDoc.setCell(ventanilla_de_pago,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(supervisor,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(telefono,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(objeto_contrato,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(comentarios,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(monto_credito,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(referencia,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(f_vencimiento_cre,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(banco_deposito,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(cuenta,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(cuenta_clabe,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(estatus,"formas",ComunesPDF.CENTER);				
				pdfDoc.setCell("    ","columnas",ComunesPDF.CENTER, columnas);
			}
			pdfDoc.addTable();
			pdfDoc.endDocument();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo", e);
		} finally {
			try {
				//reg.close();
			} catch(Exception e) {}
		}
	
	
	return nombreArchivo;
}

public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
	CreaArchivo creaArchivo = new CreaArchivo();
	StringBuffer contenidoArchivo = new StringBuffer();
	String nombreArchivo = "";
	try {
		
		CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
		int indiceCamposAdicionales =0;
		String nombreClasificacionEpo = cesionBean.clasificacionEpo(claveEpo);			
		Vector lovDatos = cesionBean.getCamposAdicionales(claveEpo, "5");
			
		
		contenidoArchivo.append("Nombre Cesionario"+",");
		contenidoArchivo.append("Pyme (Cedente)"+",");
		contenidoArchivo.append("RFC "+",");
		contenidoArchivo.append("No.Proveedor "+",");
		contenidoArchivo.append("Fecha de Solicitud PyME "+",");
		contenidoArchivo.append("No.Contrato "+",");
		contenidoArchivo.append("Firma Contrato"+",");
		contenidoArchivo.append("Monto / Moneda "+",");
		contenidoArchivo.append("Tipo de Contrataci�n "+",");
		contenidoArchivo.append("F. InicioContrato "+",");
		contenidoArchivo.append("F. FinalContrato "+",");
		contenidoArchivo.append("Plazo del Contrato "+",");			
		contenidoArchivo.append(nombreClasificacionEpo.toString()+",");	
		
			if (lovDatos !=null ) {
				for (int i=0; i<lovDatos.size(); i++) { 
					Vector lovRegistro = (Vector) lovDatos.get(i);
					String lsNombreCampo 	= (String) lovRegistro.get(1);
					contenidoArchivo.append(lsNombreCampo+",");					
				} //for
			}	
			contenidoArchivo.append("Ventanilla de Pago " +",");	
			contenidoArchivo.append("Supervisor/Administrador/Residente de Obra  "+",");	
			contenidoArchivo.append("Tel�fono "+",");	
			contenidoArchivo.append("Objeto del Contrato "+",");	
			contenidoArchivo.append("Comentarios "+",");	
			contenidoArchivo.append("Monto delCr�dito "+",");	
			contenidoArchivo.append("Referencia /No. Cr�dito "+",");	
			contenidoArchivo.append("Fecha Vencimiento Cr�dito "+",");	
			contenidoArchivo.append("Banco de Dep�sito "+",");	
			contenidoArchivo.append("Cuenta"+",");	
			contenidoArchivo.append("Cuenta CLABE"+",");	
			contenidoArchivo.append("Estatus"+",");	
			contenidoArchivo.append(" \n ");	
			
		while (rs.next()) {
			
				StringBuffer montosPorMoneda = cesionBean.getMontoMoneda(rs.getString("CLAVE_SOLICITUD"));		
				String nombre_cesinario =  (rs.getString("NOMBRE_CESINARIO")==null)?"":rs.getString("NOMBRE_CESINARIO");
				String nombre_pyme =  (rs.getString("NOMBRE_PYME")==null)?"":rs.getString("NOMBRE_PYME");
				//FODEA-024-2014 MOD()
				String cedenteAux[] = nombre_pyme.split(";");
				if(cedenteAux.length == 1){
					nombre_pyme = nombre_pyme.replaceAll(";","");
				}else{
					
				}
				//	
				String rfc =  (rs.getString("RFC")==null)?"":rs.getString("RFC");
				String no_proveedor =  (rs.getString("NO_PROVEEDOR")==null)?"":rs.getString("NO_PROVEEDOR");
				String fecha_solicitud =  (rs.getString("FECHA_SOLICITUD")==null)?"":rs.getString("FECHA_SOLICITUD");
				String numero_contrato =  (rs.getString("NUMERO_CONTRATO")==null)?"":rs.getString("NUMERO_CONTRATO");
				String tipo_contratacion =  (rs.getString("TIPO_CONTRATACION")==null)?"":rs.getString("TIPO_CONTRATACION");
				String f_ini_contrato =  (rs.getString("F_INI_CONTRATO")==null)?"":rs.getString("F_INI_CONTRATO");
				String f_fin_contrato =  (rs.getString("F_FIN_CONTRATO")==null)?"":rs.getString("F_FIN_CONTRATO");
				String plazo_contrato =  (rs.getString("PLAZO_CONTRATO")==null)?"":rs.getString("PLAZO_CONTRATO");				
				String clasificacion_epo =  (rs.getString("CLASFIFICACION_EPO")==null)?"":rs.getString("CLASFIFICACION_EPO");
				String campo1 =  (rs.getString("CAMPO1")==null)?"":rs.getString("CAMPO1");
				String campo2 =  (rs.getString("CAMPO2")==null)?"":rs.getString("CAMPO2");
				String campo3 =  (rs.getString("CAMPO3")==null)?"":rs.getString("CAMPO3");
				String campo4 =  (rs.getString("CAMPO4")==null)?"":rs.getString("CAMPO4");
				String campo5 =  (rs.getString("CAMPO5")==null)?"":rs.getString("CAMPO5");			
				String ventanilla_de_pago =  (rs.getString("VENTANILLA_DE_PAGO")==null)?"":rs.getString("VENTANILLA_DE_PAGO");
				String supervisor =  (rs.getString("SUPERVISOR")==null)?"":rs.getString("SUPERVISOR");
				String telefono =  (rs.getString("TELEFONO")==null)?"":rs.getString("TELEFONO");
				String objeto_contrato =  (rs.getString("OBJETO_CONTRATO")==null)?"":rs.getString("OBJETO_CONTRATO");
				String comentarios =  (rs.getString("COMENTARIOS")==null)?"":rs.getString("COMENTARIOS");
				String monto_credito =  (rs.getString("MONTO_CREDITO")==null)?"":rs.getString("MONTO_CREDITO");
				String referencia =  (rs.getString("REFERENCIA")==null)?"":rs.getString("REFERENCIA");
				String f_vencimiento_cre =  (rs.getString("F_VENCIMIENTO_CRE")==null)?"":rs.getString("F_VENCIMIENTO_CRE");
				String banco_deposito =  (rs.getString("BANCO_DEPOSITO")==null)?"":rs.getString("BANCO_DEPOSITO");
				String cuenta =  (rs.getString("CUENTA")==null)?"":rs.getString("CUENTA");
				String cuenta_clabe =  (rs.getString("CUENTA_CLABE")==null)?"":rs.getString("CUENTA_CLABE");
				String estatus =  (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");
				String clave_solicitud =  (rs.getString("CLAVE_SOLICITUD")==null)?"":rs.getString("CLAVE_SOLICITUD");
				String ic_estatus =  (rs.getString("IC_ESTATUS")==null)?"":rs.getString("IC_ESTATUS");	
				String firmaContrato=  (rs.getString("FIRMA_CONTRATO")==null)?"":rs.getString("FIRMA_CONTRATO");	
				contenidoArchivo.append(nombre_cesinario.replaceAll(",","")+",");	
				contenidoArchivo.append(nombre_pyme.replaceAll(",","")+",");	
				contenidoArchivo.append(rfc+",");	
				contenidoArchivo.append(no_proveedor+",");	
				contenidoArchivo.append(fecha_solicitud+",");	
				contenidoArchivo.append(numero_contrato+",");	
				contenidoArchivo.append(firmaContrato+",");	
				contenidoArchivo.append(montosPorMoneda.toString().replaceAll("<br/>", "|").replaceAll(",", "")+ ",");	
				contenidoArchivo.append(tipo_contratacion+",");	
				contenidoArchivo.append(f_ini_contrato+",");	
				contenidoArchivo.append(f_fin_contrato+",");	
				contenidoArchivo.append(plazo_contrato+",");	
				contenidoArchivo.append(clasificacion_epo+",");	
				if (lovDatos !=null ) {
					for (int i=0; i<lovDatos.size(); i++) { 
						if(i==0) contenidoArchivo.append("'"+campo1.replaceAll(",","")+",");	
						if(i==1) contenidoArchivo.append("'"+campo2.replaceAll(",","")+",");	
						if(i==2) contenidoArchivo.append(campo3.replaceAll(",","")+",");	
						if(i==3) contenidoArchivo.append(campo4.replaceAll(",","")+",");	
						if(i==4) contenidoArchivo.append(campo5.replaceAll(",","")+",");	
					}
				}
				contenidoArchivo.append(ventanilla_de_pago.replaceAll(",","")+",");	
				contenidoArchivo.append(supervisor.replaceAll(",","")+",");	
				contenidoArchivo.append(telefono+",");	
				contenidoArchivo.append(objeto_contrato.replaceAll(",","")+",");	
				contenidoArchivo.append(comentarios.replaceAll(",","")+",");	
				contenidoArchivo.append(monto_credito+",");	
				contenidoArchivo.append(referencia.replaceAll(",","")+",");	
				contenidoArchivo.append(f_vencimiento_cre+",");	
				contenidoArchivo.append(banco_deposito+",");	
				contenidoArchivo.append("'"+cuenta+",");	
				contenidoArchivo.append("'"+cuenta_clabe+",");	
				contenidoArchivo.append(estatus+",");	
				contenidoArchivo.append(" \n ");	
				
			}
		creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
		nombreArchivo = creaArchivo.getNombre();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo", e);
		} finally {
			try {
				//reg.close();
			} catch(Exception e) {}
		}		
	
	return nombreArchivo;
	
}	
	
	
/*****************************************************
	 GETTERS*******************************************************/
 
	/**
	  Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	
	
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getLoginUsuario() {
		return loginUsuario;
	}

	public void setLoginUsuario(String loginUsuario) {
		this.loginUsuario = loginUsuario;
	}

	public String getClaveEpo() {
		return claveEpo;
	}

	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}

	public String getClaveIf() {
		return claveIf;
	}

	public void setClaveIf(String claveIf) {
		this.claveIf = claveIf;
	}

	public String getClavePyme() {
		return clavePyme;
	}

	public void setClavePyme(String clavePyme) {
		this.clavePyme = clavePyme;
	}

	public String getClaveEstatusSol() {
		return claveEstatusSol;
	}

	public void setClaveEstatusSol(String claveEstatusSol) {
		this.claveEstatusSol = claveEstatusSol;
	}

	public String getNumeroContrato() {
		return numeroContrato;
	}

	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	public String getFechaVigenciaIni() {
		return fechaVigenciaIni;
	}

	public void setFechaVigenciaIni(String fechaVigenciaIni) {
		this.fechaVigenciaIni = fechaVigenciaIni;
	}

	public String getFechaSolicitudIni() {
		return fechaSolicitudIni;
	}

	public void setFechaSolicitudIni(String fechaSolicitudIni) {
		this.fechaSolicitudIni = fechaSolicitudIni;
	}

	public String getFechaVigenciaFin() {
		return fechaVigenciaFin;
	}

	public void setFechaVigenciaFin(String fechaVigenciaFin) {
		this.fechaVigenciaFin = fechaVigenciaFin;
	}

	public String getFechaSolicitudFin() {
		return fechaSolicitudFin;
	}

	public void setFechaSolicitudFin(String fechaSolicitudFin) {
		this.fechaSolicitudFin = fechaSolicitudFin;
	}

	public String getPlazoContrato() {
		return plazoContrato;
	}

	public void setPlazoContrato(String plazoContrato) {
		this.plazoContrato = plazoContrato;
	}

	public String getClaveTipoPlazo() {
		return claveTipoPlazo;
	}

	public void setClaveTipoPlazo(String claveTipoPlazo) {
		this.claveTipoPlazo = claveTipoPlazo;
	}



}