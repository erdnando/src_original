package com.netro.cesion;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 *  clase  para la consulta de Contrato de Cesion con perfil ADMIN TEST
 *  autor:Deysi Laura Hern�ndez Contreras
 **/
 
public class ConsContratoCesionIFTestigo implements IQueryGeneratorRegExtJS {


//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(ConsContratoCesionIFTestigo.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String claveIf;
	private String claveEpo;
	private String clavePyme;
	private String numeroContrato;
	private String claveEstatusSol;
	private String claveTipoContratacion;
	private String fechaVigenciaIni;
	private String fechaVigenciaFin;
	private String claveMoneda;
	private String clave_plazo_contrato;
	private String plazo_contrato;
	
	 
	public ConsContratoCesionIFTestigo() { }
	
	  
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
	
		qrySentencia.append(" SELECT sol.ic_solicitud AS CLAVE_SOLICITUD,");
		qrySentencia.append(" sol.ic_epo AS CLAVE_EPO,");
		qrySentencia.append(" sol.ic_pyme AS CLAVE_PYME,");
		qrySentencia.append(" sol.ic_tipo_contratacion AS CLAVE_TIPO_CONTRATACION,");
		qrySentencia.append(" epo.cg_razon_social AS DEPENDENCIA,");
		qrySentencia.append(" pym.cg_razon_social||';'||sol.cg_empresaS_representadas AS PYME_CEDENTE,");//FODEA-024-2014 MOD()
		qrySentencia.append(" pym.cg_rfc AS RFC,");
		qrySentencia.append(" cpe.cg_pyme_epo_interno AS NO_PROVEEDOR,");
		qrySentencia.append(" TO_CHAR(sol.df_fecha_solicitud_ini, 'dd/mm/yyyy') AS FECHA_SOLICITUD,");
		qrySentencia.append(" sol.cg_no_contrato AS NO_CONTRATO,");
		qrySentencia.append(" tcn.cg_nombre AS TIPO_CONTRATACION,");
		qrySentencia.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS FECHA_INI_CONTRA,");
		qrySentencia.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS FECHA_FIN_CONTRA,");
		qrySentencia.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || ctp.cg_descripcion) AS PLAZO_CONTRATO,");
		qrySentencia.append(" cep.cg_area AS CLASIFICACION_EPO,");
		qrySentencia.append(" cvp.cg_descripcion AS VENTANILLA_PAGO,");
		qrySentencia.append(" sol.cg_sup_adm_resob AS SUPERVISOR,");
		qrySentencia.append(" sol.cg_numero_telefono AS TELEFONO,");
		qrySentencia.append(" TO_CHAR(sol.df_fecha_aceptacion, 'dd/mm/yyyy') AS FECHA_LIMITE,");
		qrySentencia.append(" sol.cg_campo1 AS CAMPO1,");
		qrySentencia.append(" sol.cg_campo2 AS CAMPO2,");
		qrySentencia.append(" sol.cg_campo3 AS CAMPO3,");
		qrySentencia.append(" sol.cg_campo4 AS CAMPO4,");
		qrySentencia.append(" sol.cg_campo5 AS CAMPO5,");
		qrySentencia.append(" sol.cg_obj_contrato AS OBJETO_CONTRATO,");
		qrySentencia.append(" sol.cg_comentarios AS COMENTARIOS,");
		qrySentencia.append(" sol.fn_monto_credito AS MONTO_CREDITO,");
		qrySentencia.append(" sol.cg_referencia AS REFERENCIA_CREDITO,");
		qrySentencia.append(" TO_CHAR(sol.df_vencimiento_credito, 'dd/mm/yyyy') AS FECHA_VENCIMIENTO,");
		qrySentencia.append(" sol.cg_banco_deposito AS BANCO_DEPOSITO,");
		qrySentencia.append(" sol.cg_cuenta AS CUENTA,");
		qrySentencia.append(" sol.cg_cuenta_clabe AS CUENTA_CLABE,");
		qrySentencia.append(" sol.ic_estatus_solic AS IC_ESTATUS,");
		qrySentencia.append(" sts.cg_descripcion AS ESTATUS, "); 
		qrySentencia.append(" '' AS MONTO_MONEDA, "); 
		qrySentencia.append(" '' AS COLUMNA_ACCION, "); 		
		qrySentencia.append(" TO_CHAR(sol.df_alta_solicitud,'DD/MM/YYYY') as fechasolicitud, "); 	
		qrySentencia.append(" cif.cg_razon_social as cecionario, ");
		qrySentencia.append(" TO_CHAR(sol.df_firma_contrato, 'dd/mm/yyyy') AS firma_contrato");
		qrySentencia.append(" ,sol.IC_GRUPO_CESION  AS GRUPO_CESION  ");	
		
		qrySentencia.append(" FROM cder_solicitud sol");
		qrySentencia.append(", comcat_epo epo");
		qrySentencia.append(", comcat_if cif");
		qrySentencia.append(", comcat_pyme pym");
		qrySentencia.append(", comrel_pyme_epo cpe");
		qrySentencia.append(", cdercat_ventanilla_pago cvp");
		qrySentencia.append(", cdercat_tipo_contratacion tcn");
		qrySentencia.append(", cdercat_clasificacion_epo cep");
		qrySentencia.append(", cdercat_estatus_solic sts");
      qrySentencia.append(", cdercat_tipo_plazo ctp");
		qrySentencia.append(" WHERE sol.ic_epo = epo.ic_epo");
		qrySentencia.append(" AND sol.ic_if = cif.ic_if");
		qrySentencia.append(" AND sol.ic_pyme = pym.ic_pyme");
		qrySentencia.append(" AND sol.ic_epo = cpe.ic_epo(+)");
		qrySentencia.append(" AND sol.ic_pyme = cpe.ic_pyme(+)");
		qrySentencia.append(" AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion");
		qrySentencia.append(" AND sol.ic_epo = cep.ic_epo");
		qrySentencia.append(" AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo");
		qrySentencia.append(" AND sol.ic_estatus_solic = sts.ic_estatus_solic");      
		qrySentencia.append(" AND sol.cs_firmado_if = ?");
      qrySentencia.append(" AND sol.ic_tipo_plazo = ctp.ic_tipo_plazo(+)");
		qrySentencia.append(" AND sol.ic_epo = cvp.ic_epo(+)");
		qrySentencia.append(" AND sol.ic_ventanilla_pago = cvp.ic_ventanilla_pago(+)");
		conditions.add("S");
		
		if(!claveIf.equals("")){
			qrySentencia.append(" AND sol.ic_if = ?");
			conditions.add(new Integer(claveIf));
		}
		if(!claveEpo.equals("")){
			qrySentencia.append(" AND sol.ic_epo = ?");
			conditions.add(new Integer(claveEpo));
		}
		if(!clavePyme.equals("")){
			qrySentencia.append(" AND sol.ic_pyme = ?");
			conditions.add(new Long(clavePyme));
		}
		if(!numeroContrato.equals("")){
			qrySentencia.append(" AND sol.cg_no_contrato = ?");
			conditions.add(numeroContrato);
		}
		if(!claveEstatusSol.equals("")){
			qrySentencia.append(" AND sol.ic_estatus_solic = ?");
			conditions.add(new Integer(claveEstatusSol));
		}else{
			qrySentencia.append(" AND sol.ic_estatus_solic IN (?, ?)");
			conditions.add(new Integer(7));
			conditions.add(new Integer(9));
		}
		if(!claveTipoContratacion.equals("")){
			qrySentencia.append(" AND sol.cg_no_contrato = ?");
			conditions.add(new Integer(claveTipoContratacion));
		}
		if(!fechaVigenciaIni.equals("") && !fechaVigenciaFin.equals("")){
			qrySentencia.append(" AND sol.df_fecha_vigencia_ini >= TO_DATE(?, 'dd/mm/yyyy')");
			qrySentencia.append(" AND sol.df_fecha_vigencia_fin <= TO_DATE(?, 'dd/mm/yyyy')");
			conditions.add(fechaVigenciaIni);
			conditions.add(fechaVigenciaFin);
		}
		if (claveMoneda != null && !"".equals(claveMoneda)) {
			if (claveMoneda.equals("0")) {
				qrySentencia.append(" AND sol.cs_multimoneda = ?");
				conditions.add("S");
			} else {
				qrySentencia.append(" AND sol.cs_multimoneda = ?");
				qrySentencia.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
				qrySentencia.append(" FROM cder_monto_x_solic mxs");
				qrySentencia.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
				qrySentencia.append(" AND mxs.ic_moneda = ?)");
				conditions.add("N");
				conditions.add(new Integer(claveMoneda));
			}
      }      
      if(!clave_plazo_contrato.equals("")){
			qrySentencia.append(" AND sol.ic_tipo_plazo = ? ");
			conditions.add(new Integer(clave_plazo_contrato));
      }
      if(!plazo_contrato.equals("")){
			qrySentencia.append(" AND sol.ig_plazo = ? ");
			conditions.add(new Integer(plazo_contrato));
      }
      qrySentencia.append(" ORDER BY sol.df_alta_solicitud DESC"); 
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		

		try {
		
		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		ComunesPDF pdfDoc = new ComunesPDF();
		int indiceCamposAdicionales =0;	
		Vector lovDatos = new Vector(); 
		String  clasificacionEpo ="", dependencia ="", pyme_cedente ="", rfc ="",no_proveedor ="", fecha_solicitud ="", 	
					 no_contrato="", monto_moneda ="", tipo_contratacion ="", fecha_ini_contrato ="", 
					 fecha_fin_contrato ="", plazo_contrato ="", clasificacion_epo ="", fecha_limite ="", 
					 ventanilla_pago="",  supervisor ="", telefono ="", objeto_contrato ="", comentarios ="",
					 monto_credito="", referencia_credito="", fecha_vencimiento ="", banco_deposito ="",
					 cuenta="", cuenta_clabe="", estatus ="", clave_solicitud ="";
		
		try {
		
			CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
			if(!claveEpo.equals(""))  {
				clasificacionEpo = cesionBean.clasificacionEpo(claveEpo);
				lovDatos = cesionBean.getCamposAdicionales(claveEpo, "5");	
				indiceCamposAdicionales = lovDatos.size();				
			}
			int tColum = 5 -indiceCamposAdicionales;
			
			if(tipo.equals("PDF") ) {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString()+"", 
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),  
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
					
				pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);					
				pdfDoc.addText(" Contrato de Cesi�n ","formas",ComunesPDF.CENTER);
				pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);
							
				/*String mensaje  =	" Con base a lo dispuesto en el C�digo de Comercio por este conducto me permito notificar a esa Entidad el contrato de cesi�n de Derechos de Cobro que hemos formalizado con la empresa CEDENTE respecto al contrato que aqu� mismo se detalla \n " +
									" Por lo anterior, me permito solicitar a usted, se registre la Cesi�n de Derechos de Cobro de referencia a efecto de que esa Entidad, realice el(los) pago(s) derivados del Contrato aqu� se�alado a favor del INTERMEDIARIO FINANCIERO (CESIONARIO), en la inteligencia de que �ste, est� de acuerdo con lo establecido en el Convenio Marco de Cesi�n Electr�nica de Derechos  de Cobro. \n "+
									" Los datos de nuestra cuenta bancaria en la que se deben depositar los pagos derivados de esta operaci�n son los que aqu� se detallan. \n"+
								   " Sobre el particular y de conformidad con lo se�alado en la emisi�n de la conformidad para ceder los derechos de cobro emitida por esa Entidad, por este medio les notifico, la Cesi�n de Derechos de Cobro del Contrato antes mencionado, para todos los efectos legales a que haya lugar, adjunto, en forma electr�nica.\n ";
				*/
				//pdfDoc.setTable(1,80);
				//pdfDoc.setCell(mensaje,"formas",ComunesPDF.JUSTIFIED);	
				//pdfDoc.addTable();
				
				pdfDoc.addText("  ","formas",ComunesPDF.RIGHT);				
				pdfDoc.setTable(15,100);								
				pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Dependencia","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Pyme (Cedente)", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" RFC","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" No Proveedor","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de Solicitud PyME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" No. Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Firma Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto / Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Tipo de Contrataci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" F. Inicio Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" F. Final Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Plazo del Contrato","celda01",ComunesPDF.CENTER);		
				if(!clasificacionEpo.equals("")) {
					pdfDoc.setCell(clasificacionEpo,"celda01",ComunesPDF.CENTER);
				}
				pdfDoc.setCell("F. L�mite para Notificaci�n","celda01",ComunesPDF.CENTER);
				if(clasificacionEpo.equals("")) {
					pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);
				}
				
				pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);					
				pdfDoc.setCell(" Ventanilla de Pago","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Supervisor / Administrador / Residente de Obra","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Tel�fono","celda01", ComunesPDF.CENTER);
				pdfDoc.setCell(" Objeto del Contrato","celda01", ComunesPDF.CENTER);
				pdfDoc.setCell(" Comentarios","celda01", ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto del Cr�dito","celda01", ComunesPDF.CENTER);
				pdfDoc.setCell(" Referencia / No Cr�dito","celda01", ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha Vencimiento Cr�dito","celda01", ComunesPDF.CENTER);
				pdfDoc.setCell(" Banco de Dep�sito ","celda01", ComunesPDF.CENTER);
				pdfDoc.setCell(" Cuenta ","celda01", ComunesPDF.CENTER);
				pdfDoc.setCell(" Cuenta  CLABE","celda01", ComunesPDF.CENTER);
				pdfDoc.setCell(" Estatus ","celda01", ComunesPDF.CENTER);
				pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" ","celda01",ComunesPDF.CENTER);
			}
			
			if(tipo.equals("CSV") ) {
				contenidoArchivo.append(" Dependencia, Pyme (Cedente),  RFC, No Proveedor, Fecha de Solicitud PyME, No. Contrato, "+
												 " Firma Contrato, Monto / Moneda, Tipo de Contrataci�n, F. Inicio Contrato, F. Final Contrato, Plazo del Contrato, ");
				if(!clasificacionEpo.equals("")) {  contenidoArchivo.append(clasificacionEpo.replaceAll(",", "")+",");		}				
				contenidoArchivo.append(" F. L�mite para Notificaci�n,");
				if(indiceCamposAdicionales>0) {
					for(int i = 0; i < indiceCamposAdicionales; i++){
						List campos =(List)lovDatos.get(i);					
							contenidoArchivo.append( campos.get(1).toString().replaceAll(",", "")+",");
					}	
				}					
				contenidoArchivo.append(" Ventanilla de Pago, Supervisor / Administrador / Residente de Obra, Tel�fono, Objeto del Contrato,  Comentarios, "+
												" Monto del Cr�dito, Referencia / No Cr�dito,  Fecha Vencimiento Cr�dito, Banco de Dep�sito,  Cuenta , "+
												" Cuenta  CLABE,  Estatus \n");
			}
		
					 
			while (rs.next())	{					
			
				dependencia = (rs.getString("DEPENDENCIA") == null) ? "" : rs.getString("DEPENDENCIA");
				pyme_cedente = (rs.getString("PYME_CEDENTE") == null) ? "" : rs.getString("PYME_CEDENTE");
				rfc = (rs.getString("RFC") == null) ? "" : rs.getString("RFC");
				no_proveedor = (rs.getString("NO_PROVEEDOR") == null) ? "" : rs.getString("NO_PROVEEDOR");
				fecha_solicitud = (rs.getString("FECHA_SOLICITUD") == null) ? "" : rs.getString("FECHA_SOLICITUD");
				no_contrato = (rs.getString("NO_CONTRATO") == null) ? "" : rs.getString("NO_CONTRATO");
				tipo_contratacion = (rs.getString("TIPO_CONTRATACION") == null) ? "" : rs.getString("TIPO_CONTRATACION");
				fecha_ini_contrato = (rs.getString("FECHA_INI_CONTRA") == null) ? "" : rs.getString("FECHA_INI_CONTRA");
				fecha_fin_contrato = (rs.getString("FECHA_FIN_CONTRA") == null) ? "" : rs.getString("FECHA_FIN_CONTRA");
				plazo_contrato = (rs.getString("PLAZO_CONTRATO") == null) ? "" : rs.getString("PLAZO_CONTRATO");
				clasificacion_epo = (rs.getString("CLASIFICACION_EPO") == null) ? "" : rs.getString("CLASIFICACION_EPO");
				fecha_limite = (rs.getString("FECHA_LIMITE") == null) ? "" : rs.getString("FECHA_LIMITE");
				ventanilla_pago = (rs.getString("VENTANILLA_PAGO") == null) ? "" : rs.getString("VENTANILLA_PAGO");
				supervisor = (rs.getString("SUPERVISOR") == null) ? "" : rs.getString("SUPERVISOR");
				telefono = (rs.getString("TELEFONO") == null) ? "" : rs.getString("TELEFONO");
				objeto_contrato = (rs.getString("OBJETO_CONTRATO") == null) ? "" : rs.getString("OBJETO_CONTRATO");
				comentarios = (rs.getString("COMENTARIOS") == null) ? "" : rs.getString("COMENTARIOS");
				monto_credito = (rs.getString("MONTO_CREDITO") == null) ? "" : rs.getString("MONTO_CREDITO");
				referencia_credito = (rs.getString("REFERENCIA_CREDITO") == null) ? "" : rs.getString("REFERENCIA_CREDITO");
				fecha_vencimiento = (rs.getString("FECHA_VENCIMIENTO") == null) ? "" : rs.getString("FECHA_VENCIMIENTO");
				banco_deposito = (rs.getString("BANCO_DEPOSITO") == null) ? "" : rs.getString("BANCO_DEPOSITO");
				cuenta = (rs.getString("CUENTA") == null) ? "" : rs.getString("CUENTA");
				cuenta_clabe = (rs.getString("CUENTA_CLABE") == null) ? "" : rs.getString("CUENTA_CLABE");
				estatus = (rs.getString("ESTATUS") == null) ? "" : rs.getString("ESTATUS");
				clave_solicitud = (rs.getString("CLAVE_SOLICITUD") == null) ? "" : rs.getString("CLAVE_SOLICITUD");
				String firmaContrato= (rs.getString("FIRMA_CONTRATO") == null) ? "" : rs.getString("FIRMA_CONTRATO");
				
				StringBuffer montosMonedaSol = cesionBean.getMontoMoneda(clave_solicitud);
				monto_moneda= montosMonedaSol.toString().replaceAll(",", "");		
				
				if(tipo.equals("PDF") ) {
					pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(dependencia, "formas",ComunesPDF.CENTER);
					//FODEA-024-2014 MOD()
					String cedente[]=pyme_cedente.split(";");
					if(cedente.length == 1){
						pyme_cedente = pyme_cedente.replaceAll(";","");
					}else{
						pyme_cedente = pyme_cedente.replaceAll(";","\n\n");
					}
					//
					pdfDoc.setCell(pyme_cedente,  "formas",ComunesPDF.CENTER);
					pdfDoc.setCell(rfc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(no_proveedor,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_solicitud,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(no_contrato,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(firmaContrato,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(monto_moneda.replaceAll("<br/>", "\n"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(tipo_contratacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_ini_contrato,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_fin_contrato,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(plazo_contrato,"formas",ComunesPDF.CENTER);		
					if(!clasificacionEpo.equals("")) {
						pdfDoc.setCell(clasificacion_epo,"formas",ComunesPDF.CENTER);
					}
					pdfDoc.setCell(fecha_limite,"formas",ComunesPDF.CENTER);
					if(clasificacionEpo.equals("")) {
						pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
					}
					pdfDoc.setCell("B","formas",ComunesPDF.CENTER);					
					pdfDoc.setCell(ventanilla_pago,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(supervisor,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(telefono,"formas", ComunesPDF.CENTER);
					pdfDoc.setCell(objeto_contrato,"formas", ComunesPDF.CENTER);
					pdfDoc.setCell(comentarios,"formas", ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(String.valueOf(monto_credito), 2),"formas",ComunesPDF.RIGHT);
				
					pdfDoc.setCell(referencia_credito,"formas", ComunesPDF.CENTER);
					pdfDoc.setCell(fecha_vencimiento,"formas", ComunesPDF.CENTER);
					pdfDoc.setCell(banco_deposito, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(cuenta,"formas", ComunesPDF.CENTER);
					pdfDoc.setCell(cuenta_clabe,"formas", ComunesPDF.CENTER);
					pdfDoc.setCell(estatus, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(" ","formas",ComunesPDF.CENTER);
				}	
				
				if(tipo.equals("CSV") ) {
					//FODEA-024-2014 MOD()
					String cedente[]=pyme_cedente.split(";");
					if(cedente.length == 1){
						pyme_cedente = pyme_cedente.replaceAll(";","");
					}else{
						//pyme_cedente = pyme_cedente.replaceAll(";","\n\n");
					}
					//
					contenidoArchivo.append(dependencia.replaceAll(",", "")+","+
						pyme_cedente.replaceAll(",", "")+","+
						rfc.replaceAll(",", "")+","+
						no_proveedor.replaceAll(",", "")+","+
						fecha_solicitud+","+
						no_contrato+","+
						firmaContrato+","+
						monto_moneda.replaceAll("<br/>", "|")+","+
						tipo_contratacion.replaceAll(",", "")+","+
						fecha_ini_contrato+","+
						fecha_fin_contrato+","+
						plazo_contrato+","); 
					
					if(!clasificacionEpo.equals("")) {
						contenidoArchivo.append(clasificacion_epo.replaceAll(",", "")+",");
					}
					contenidoArchivo.append(fecha_limite+",");
									
					if(indiceCamposAdicionales>0) {
					for(int i = 1; i <=indiceCamposAdicionales; i++){
							String campo =	 (rs.getString("CAMPO"+i) == null) ? "" : rs.getString("CAMPO"+i);
							if(i==1||i==2){
								campo="'"+campo;
							}
							contenidoArchivo.append(campo.replaceAll(",", "")+",");
					}	
				}
				
					contenidoArchivo.append(ventanilla_pago.replaceAll(",", "")+","+
						supervisor.replaceAll(",", "")+","+
						telefono.replaceAll(",", "")+","+
						objeto_contrato.replaceAll(",", "")+","+
						comentarios.replaceAll(",", "")+","+
						"\"$"+Comunes.formatoDecimal(monto_credito,2,false)+"\","+
						referencia_credito.replaceAll(",", "")+","+
						fecha_vencimiento.replaceAll(",", "")+","+
						banco_deposito.replaceAll(",", "")+","+
						"'"+cuenta.replaceAll(",", "")+","+
						"'"+cuenta_clabe.replaceAll(",", "")+","+
						estatus.replaceAll(",", "")+"\n");
				}						
				
				
			}// while (rs.next())	{	
			
			
			if(tipo.equals("PDF") ) {
				pdfDoc.addTable();
				pdfDoc.endDocument();	
			}
			if(tipo.equals("CSV") ) {
				creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
				nombreArchivo = creaArchivo.getNombre();
			}
		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
		
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getClaveIf() {
		return claveIf;
	}

	public void setClaveIf(String claveIf) {
		this.claveIf = claveIf;
	}

	public String getClaveEpo() {
		return claveEpo;
	}

	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}

	public String getClavePyme() {
		return clavePyme;
	}

	public void setClavePyme(String clavePyme) {
		this.clavePyme = clavePyme;
	}

	public String getNumeroContrato() {
		return numeroContrato;
	}

	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}

	public String getClaveEstatusSol() {
		return claveEstatusSol;
	}

	public void setClaveEstatusSol(String claveEstatusSol) {
		this.claveEstatusSol = claveEstatusSol;
	}

	public String getClaveTipoContratacion() {
		return claveTipoContratacion;
	}

	public void setClaveTipoContratacion(String claveTipoContratacion) {
		this.claveTipoContratacion = claveTipoContratacion;
	}

	public String getFechaVigenciaIni() {
		return fechaVigenciaIni;
	}

	public void setFechaVigenciaIni(String fechaVigenciaIni) {
		this.fechaVigenciaIni = fechaVigenciaIni;
	}

	public String getFechaVigenciaFin() {
		return fechaVigenciaFin;
	}

	public void setFechaVigenciaFin(String fechaVigenciaFin) {
		this.fechaVigenciaFin = fechaVigenciaFin;
	}

	public String getClaveMoneda() {
		return claveMoneda;
	}

	public void setClaveMoneda(String claveMoneda) {
		this.claveMoneda = claveMoneda;
	}

	public String getClave_plazo_contrato() {
		return clave_plazo_contrato;
	}

	public void setClave_plazo_contrato(String clave_plazo_contrato) {
		this.clave_plazo_contrato = clave_plazo_contrato;
	}

	public String getPlazo_contrato() {
		return plazo_contrato;
	}

	public void setPlazo_contrato(String plazo_contrato) {
		this.plazo_contrato = plazo_contrato;
	}




	

}