package com.netro.cesion;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

public class ConsulSolicitudConsentimiento implements IQueryGeneratorReg, IQueryGeneratorRegExtJS {																											
																											
	public ConsulSolicitudConsentimiento() {}
	//Variable para enviar mensajes al log.
	private static final Log log = ServiceLocator.getInstance().getLog(ConsulSolicitudConsentimiento.class);
	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer 		strSQL;
	private List 		conditions;
	private String 	paginaOffset;
	private String 	paginaNo;
	String[]		montos			= new String[2];
	String[]		doctos			= new String[2];
	String[]		numeros; 
	
	private String directorio;
	
	private String noEpo;
	private String noIF;
	private String clasificacionEPO;
	private String noContrato;
	private String contratacion;
	private String fvigenciaIni;
	private String fvigenciaFin;
	private String campo1;
	private String campo2;
	private String campo3;
	private String campo4;
	private String campo5;
	private String contrato;
	private String solicitud;
	private String pyme;
	
	private String clavePyme;
	private String claveEpo;
	private String claveIfCesionario;
	private String numeroContrato;
	private String montoContrato;
	private String claveMoneda;
	private String claveTipoContratacion;
	private String fechaVigenciaContratoIni;
	private String fechaVigenciaContratoFin;
	private String claveEstatusSolicitud;
	private String csTipoMonto;
	private String plazoContrato;
	private String claveTipoPlazo;

	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E) ::..");
		strSQL = new StringBuffer();
		log.info("getAggregateCalculationQuery(S) ::..");
		return strSQL.toString();
	}
		
	 /**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery() {
		log.info("getDocumentQuery(E)::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();


		strSQL.append("SELECT   DISTINCT sol.ic_solicitud AS clave_solicitud ");
		strSQL.append("    FROM cder_solicitud sol, cder_solic_x_representante csr ");
		strSQL.append("   WHERE sol.ic_solicitud = csr.ic_solicitud ");
		if (claveEstatusSolicitud != null && !claveEstatusSolicitud.equals("")) {
			strSQL.append(" AND sol.ic_estatus_solic = ?");
			conditions.add(new Integer(claveEstatusSolicitud));
		} else {
			strSQL.append(" AND sol.ic_estatus_solic IN (?, ?, ?, ?, ?, ?)");
			conditions.add(new Integer(1));
			conditions.add(new Integer(13));
			conditions.add(new Integer(2));
			conditions.add(new Integer(5));
			conditions.add(new Integer(6));
			conditions.add(new Integer(8));
		}

		if (clavePyme != null && !"".equals(clavePyme)) {
			strSQL.append(" AND csr.ic_pyme = ? ");
			conditions.add(new Long(clavePyme));
		}

		if (claveEpo != null && !"".equals(claveEpo)) {
			strSQL.append(" AND sol.ic_epo = ?");
			conditions.add(new Integer(claveEpo));
		}

		if (claveIfCesionario != null && !"".equals(claveIfCesionario)) {
			strSQL.append(" AND sol.ic_if = ?");
			conditions.add(new Integer(claveIfCesionario));
		}

		if (numeroContrato != null && !"".equals(numeroContrato)) {
			strSQL.append(" AND sol.cg_no_contrato = ?");
			conditions.add(numeroContrato);
		}
		if (claveMoneda != null && !"".equals(claveMoneda)) {
			if (claveMoneda.equals("0")) {
				strSQL.append(" AND sol.cs_multimoneda = ?");
				conditions.add("S");
			} else {
				strSQL.append(" AND sol.cs_multimoneda = ?");
				strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
				strSQL.append(" FROM cder_monto_x_solic mxs");
				strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
				strSQL.append(" AND mxs.ic_moneda = ?)");
				conditions.add("N");
				conditions.add(new Integer(claveMoneda));
			}
		}

		if (csTipoMonto != null && !"".equals(csTipoMonto)) {
			if (csTipoMonto.equals("D")) {
				strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
				strSQL.append(" FROM cder_monto_x_solic mxs");
				strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
				strSQL.append(" AND mxs.fn_monto_moneda IS NOT NULL)");
				strSQL.append(" AND sol.cs_multimoneda = ?");
				conditions.add("N");
			} else if (csTipoMonto.equals("M")) {
				strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
				strSQL.append(" FROM cder_monto_x_solic mxs");
				strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
				strSQL.append(" AND mxs.fn_monto_moneda IS NULL)");
				strSQL.append(" AND sol.cs_multimoneda = ?");
				conditions.add("N");
			} else {
				strSQL.append(" AND (EXISTS (SELECT mxs.ic_monto_x_solic");
				strSQL.append(" FROM cder_monto_x_solic mxs");
				strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
				strSQL.append(" AND mxs.fn_monto_moneda IS NOT NULL)");
				strSQL.append(" OR EXISTS (SELECT mxs.ic_monto_x_solic");
				strSQL.append(" FROM cder_monto_x_solic mxs");
				strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
				strSQL.append(" AND mxs.fn_monto_moneda IS NULL))");
				strSQL.append(" AND sol.cs_multimoneda = ?");
				conditions.add("N");
			}
		}		

		if (plazoContrato != null && !"".equals(plazoContrato)) {
			strSQL.append(" AND sol.ig_plazo = ?");
			conditions.add(new Integer(plazoContrato));
		}		

		if (claveTipoPlazo != null && !"".equals(claveTipoPlazo)) {
			strSQL.append(" AND sol.ic_tipo_plazo = ?");
			conditions.add(new Integer(claveTipoPlazo));
		}		

		if (claveTipoContratacion != null && !"".equals(claveTipoContratacion)) {
			strSQL.append(" AND sol.ic_tipo_contratacion = ?");
			conditions.add(new Integer(claveTipoContratacion));
		}		

		if (fechaVigenciaContratoIni != null && !"".equals(fechaVigenciaContratoIni)) {
				strSQL.append(" AND sol.df_fecha_vigencia_ini >= TO_DATE(?, 'dd/mm/yyyy')");
				strSQL.append(" AND sol.df_fecha_vigencia_ini < TO_DATE(?, 'dd/mm/yyyy') + 1");
				conditions.add(fechaVigenciaContratoIni);
				conditions.add(fechaVigenciaContratoIni);
		}

		if (fechaVigenciaContratoFin != null && !"".equals(fechaVigenciaContratoFin)) {
				strSQL.append(" AND sol.df_fecha_vigencia_fin >= TO_DATE(?, 'dd/mm/yyyy')");
				strSQL.append(" AND sol.df_fecha_vigencia_fin < TO_DATE(?, 'dd/mm/yyyy') + 1");
				conditions.add(fechaVigenciaContratoFin);
				conditions.add(fechaVigenciaContratoFin);
		}		 

		strSQL.append(" ORDER BY sol.ic_solicitud");
		
		log.debug("..:: strSQL: "+strSQL.toString());
		log.debug("..:: conditions: "+conditions);
		log.info("getDocumentQuery(S) ::.");
		return strSQL.toString();
	}

	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){  
		log.info("getDocumentSummaryQueryForIds(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();

		strSQL.append(" SELECT sol.ic_solicitud as clave_solicitud,"); 
		strSQL.append(" sol.ic_epo as clave_epo,");
		strSQL.append(" sol.ic_if as clave_if,");
		strSQL.append(" sol.ic_pyme as clave_pyme,");
		strSQL.append(" '' as MONTO_MONEDA,");
		strSQL.append(" sol.ic_tipo_contratacion as clave_contratacion,");
		strSQL.append(" e.cg_razon_social as dependencia,");
		strSQL.append(" pyme.cg_razon_social||';'||sol.CG_EMPRESAS_REPRESENTADAS as nombre_cedente,");
		strSQL.append(" pyme.cg_rfc as rfc_cedente,");
		strSQL.append(" i.cg_razon_social as nombre_cesionario,");
		strSQL.append(" sol.cg_no_contrato as numero_contrato,");
		strSQL.append(" con.cg_nombre as tipo_contratacion,");
		strSQL.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");
		strSQL.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");
		strSQL.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS plazo_contrato,");
		strSQL.append(" cl.cg_area as clasificacion_epo,");
		strSQL.append(" sol.cg_campo1 as campo_adicional_1,");
		strSQL.append(" sol.cg_campo2 as campo_adicional_2,");
		strSQL.append(" sol.cg_campo3 as campo_adicional_3,");
		strSQL.append(" sol.cg_campo4 as campo_adicional_4,");
		strSQL.append(" sol.cg_campo5 as campo_adicional_5,");
		strSQL.append(" sol.cg_obj_contrato as objeto_contrato,");
		strSQL.append(" sol.cg_comentarios as comentarios,");
		strSQL.append(" DECODE(sol.ic_estatus_solic, 5, sol.cg_causas_rechazo,12, sol.cg_causas_rechazo, 'NA') AS causas_rechazo,");
		strSQL.append(" TO_CHAR(extc.df_extincion_contrato, 'dd/mm/yyyy') AS fecha_extincion,");
		strSQL.append(" extc.cg_cuenta_ext AS numero_cuenta,");
		strSQL.append(" extc.cg_cuenta_clabe_ext AS cuenta_clabe,");
		strSQL.append(" extc.cg_banco_deposito_ext AS banco_deposito,");
		strSQL.append(" DECODE (extc.cg_causa_rechazo_ext, null, 'N/A', extc.cg_causa_rechazo_ext) AS causas_rechazo_ext,");
		strSQL.append(" cvp.cg_descripcion AS venanilla_pago,");
		strSQL.append(" sol.cg_sup_adm_resob AS sup_adm_resob,");
		strSQL.append(" sol.cg_numero_telefono AS numero_telefono,");
		strSQL.append(" es.cg_descripcion as estatus_solicitud,");
		strSQL.append(" sol.ic_estatus_solic as clave_estatus,");
		strSQL.append(" TO_CHAR(sol.df_alta_solicitud, 'dd/mm/yyyy') as fechasolicitud,");
		strSQL.append(" TO_CHAR(sol.df_firma_contrato, 'dd/mm/yyyy') AS firma_contrato,");
		strSQL.append(" sol.ic_grupo_cesion AS ic_grupo_cesion,");
		strSQL.append(" tb_num_rep.num_rep AS num_rep,");
		strSQL.append(" tb_num_rep.num_envio_rep AS num_envio_rep,");
		strSQL.append(" cpg.IC_PYME as pyme_principal");
		strSQL.append(" , sol.CG_CAUSA_CANCELACION as MOTIVOS_CANCELACION");
		
		strSQL.append(" FROM cder_solicitud sol");
		strSQL.append(", comcat_epo e");
		strSQL.append(", comcat_if i");
		strSQL.append(", comcat_pyme pyme");
		strSQL.append(", cdercat_clasificacion_epo cl");
		strSQL.append(", cdercat_tipo_contratacion con");
		strSQL.append(", cdercat_estatus_solic es");
		strSQL.append(", cdercat_tipo_plazo stp");
		strSQL.append(", cder_extincion_contrato extc");
		strSQL.append(", cdercat_ventanilla_pago cvp");
		strSQL.append(", cder_grupo cg");
		strSQL.append(", cderrel_pyme_x_grupo cpg");
		strSQL.append(",(SELECT   ic_solicitud, COUNT (*) num_rep, ");
		strSQL.append("         SUM (DECODE (cc_acuse_envio_rep, NULL, 0, 1)) AS num_envio_rep ");
		strSQL.append("    FROM cder_solic_x_representante ");
		strSQL.append("GROUP BY ic_solicitud) tb_num_rep ");
		strSQL.append(" WHERE  sol.ic_epo = e.ic_epo");
		strSQL.append(" AND sol.ic_if = i.ic_if ");
		strSQL.append(" AND sol.ic_pyme = pyme.ic_pyme");
		strSQL.append(" AND sol.ic_clasificacion_epo = cl.ic_clasificacion_epo(+)");
		strSQL.append(" AND SOL.ic_tipo_contratacion = con.ic_tipo_contratacion");
		strSQL.append(" AND SOL.ic_estatus_solic = es.ic_estatus_solic");
		strSQL.append(" AND sol.ic_solicitud = tb_num_rep.ic_solicitud ");
		strSQL.append(" AND sol.IC_GRUPO_CESION = cg.IC_GRUPO_CESION(+) ");
		strSQL.append(" AND cg.IC_GRUPO_CESION = cpg.IC_GRUPO_CESION(+) ");
		strSQL.append(" AND cpg.CS_PYME_PRINCIPAL(+) = 'S' ");
		strSQL.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
		strSQL.append(" AND sol.ic_solicitud = extc.ic_solicitud(+)");
		strSQL.append(" AND sol.ic_epo = cvp.ic_epo(+)");
		strSQL.append(" AND sol.ic_ventanilla_pago = cvp.ic_ventanilla_pago(+)");
		strSQL.append(" AND (");
									
		for (int i = 0; i < pageIds.size(); i++) {
      List lItem = (ArrayList)pageIds.get(i);

      if(i > 0){strSQL.append("  OR  ");}

			strSQL.append("sol.ic_solicitud = ?");
      conditions.add(new Long(lItem.get(0).toString()));
		}

    strSQL.append(" ) ");
		strSQL.append(" ORDER BY sol.ic_solicitud");
		
		log.debug("..:: strSQL: "+strSQL.toString());
		log.debug("..:: conditions: "+conditions);
		
		log.info("getDocumentSummaryQueryForIds(S)");
		return strSQL.toString();
	}
	

	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();
				
		strSQL.append(" SELECT distinct sol.ic_solicitud as clave_solicitud,"); 
		strSQL.append(" sol.ic_epo as clave_epo,");
		strSQL.append(" sol.ic_if as clave_if,");
		strSQL.append(" sol.ic_pyme as clave_pyme,");
		strSQL.append(" '' AS MONTO_MONEDA,");
		strSQL.append(" sol.ic_tipo_contratacion as clave_contratacion,");
		strSQL.append(" e.cg_razon_social as dependencia,");
		strSQL.append(" pyme.cg_razon_social||';'||sol.CG_EMPRESAS_REPRESENTADAS as nombre_cedente,");//FODEA-024-2014 MOD()
		strSQL.append(" pyme.cg_rfc as rfc_cedente,");
		strSQL.append(" i.cg_razon_social as nombre_cesionario,");
		strSQL.append(" sol.cg_no_contrato as numero_contrato,");
		strSQL.append(" con.cg_nombre as tipo_contratacion,");
		strSQL.append(" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,");
		strSQL.append(" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,");
		strSQL.append(" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || stp.cg_descripcion) AS plazo_contrato,");
		strSQL.append(" cl.cg_area as clasificacion_epo,");
		strSQL.append(" sol.cg_campo1 as campo_adicional_1,");
		strSQL.append(" sol.cg_campo2 as campo_adicional_2,");
		strSQL.append(" sol.cg_campo3 as campo_adicional_3,");
		strSQL.append(" sol.cg_campo4 as campo_adicional_4,");
		strSQL.append(" sol.cg_campo5 as campo_adicional_5,");
		strSQL.append(" sol.cg_obj_contrato as objeto_contrato,");
		strSQL.append(" sol.cg_comentarios as comentarios,");
		strSQL.append(" DECODE(sol.ic_estatus_solic, 5, sol.cg_causas_rechazo, 'NA') AS causas_rechazo,");
		strSQL.append(" TO_CHAR(extc.df_extincion_contrato, 'dd/mm/yyyy') AS fecha_extincion,");
		strSQL.append(" extc.cg_cuenta_ext AS numero_cuenta,");
		strSQL.append(" extc.cg_cuenta_clabe_ext AS cuenta_clabe,");
		strSQL.append(" extc.cg_banco_deposito_ext AS banco_deposito,");
		strSQL.append(" DECODE (extc.cg_causa_rechazo_ext, null, 'N/A', extc.cg_causa_rechazo_ext) AS causas_rechazo_ext,");
		strSQL.append(" cvp.cg_descripcion AS venanilla_pago,");
		strSQL.append(" sol.cg_sup_adm_resob AS sup_adm_resob,");
		strSQL.append(" sol.cg_numero_telefono AS numero_telefono,");
		strSQL.append(" es.cg_descripcion as estatus_solicitud,");
		strSQL.append(" sol.ic_estatus_solic as clave_estatus,");
		strSQL.append(" sol.df_alta_solicitud as fecha_solicitud,");
		strSQL.append(" TO_CHAR(sol.df_firma_contrato, 'dd/mm/yyyy') AS firma_contrato");
		strSQL.append(" , sol.CG_CAUSA_CANCELACION as MOTIVOS_CANCELACION");
		
		strSQL.append(" FROM cder_solicitud sol");
		strSQL.append(", comcat_epo e");
		strSQL.append(", comcat_if i");
		strSQL.append(", comcat_pyme pyme");
		strSQL.append(", cdercat_clasificacion_epo cl");
		strSQL.append(", cdercat_tipo_contratacion con");
		strSQL.append(", cdercat_estatus_solic es");
		strSQL.append(", cdercat_tipo_plazo stp");
		strSQL.append(", cder_extincion_contrato extc");
		strSQL.append(", cdercat_ventanilla_pago cvp");
		strSQL.append(", cder_solic_x_representante csr");
	  strSQL.append(" WHERE  sol.ic_epo = e.ic_epo");
		strSQL.append(" AND sol.ic_if = i.ic_if ");
		strSQL.append(" AND sol.ic_pyme = pyme.ic_pyme");
		strSQL.append(" AND sol.ic_solicitud = csr.ic_solicitud ");
		strSQL.append(" AND sol.ic_clasificacion_epo = cl.ic_clasificacion_epo(+)");
		strSQL.append(" AND SOL.ic_tipo_contratacion = con.ic_tipo_contratacion");
		strSQL.append(" AND SOL.ic_estatus_solic = es.ic_estatus_solic");
		strSQL.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
		strSQL.append(" AND sol.ic_solicitud = extc.ic_solicitud(+)");
		strSQL.append(" AND sol.ic_epo = cvp.ic_epo(+)");
		strSQL.append(" AND sol.ic_ventanilla_pago = cvp.ic_ventanilla_pago(+)");
		
		
		if (claveEstatusSolicitud != null && !claveEstatusSolicitud.equals("")) {
			strSQL.append(" AND sol.ic_estatus_solic = ?");
			conditions.add(new Integer(claveEstatusSolicitud));
		} else {
			strSQL.append(" AND sol.ic_estatus_solic IN (?, ?, ?, ?, ?, ?)");
			conditions.add(new Integer(1));
			conditions.add(new Integer(13));
			conditions.add(new Integer(2));
			conditions.add(new Integer(5));
			conditions.add(new Integer(6));
			conditions.add(new Integer(8));
		}

		if (clavePyme != null && !"".equals(clavePyme)) {
			strSQL.append(" AND csr.ic_pyme = ?");
			conditions.add(new Long(clavePyme));
		}

		if (claveEpo != null && !"".equals(claveEpo)) {
			strSQL.append(" AND sol.ic_epo = ?");
			conditions.add(new Integer(claveEpo));
		}

		if (claveIfCesionario != null && !"".equals(claveIfCesionario)) {
			strSQL.append(" AND sol.ic_if = ?");
			conditions.add(new Integer(claveIfCesionario));
		}

		if (numeroContrato != null && !"".equals(numeroContrato)) {
			strSQL.append(" AND sol.cg_no_contrato = ?");
			conditions.add(numeroContrato);
		}
		if (claveMoneda != null && !"".equals(claveMoneda)) {
			if (claveMoneda.equals("0")) {
				strSQL.append(" AND sol.cs_multimoneda = ?");
				conditions.add("S");
			} else {
				strSQL.append(" AND sol.cs_multimoneda = ?");
				strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
				strSQL.append(" FROM cder_monto_x_solic mxs");
				strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
				strSQL.append(" AND mxs.ic_moneda = ?)");
				conditions.add("N");
				conditions.add(new Integer(claveMoneda));
			}
		}

		if (csTipoMonto != null && !"".equals(csTipoMonto)) {
			if (csTipoMonto.equals("D")) {
				strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
				strSQL.append(" FROM cder_monto_x_solic mxs");
				strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
				strSQL.append(" AND mxs.fn_monto_moneda IS NOT NULL)");
				strSQL.append(" AND sol.cs_multimoneda = ?");
				conditions.add("N");
			} else if (csTipoMonto.equals("M")) {
				strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
				strSQL.append(" FROM cder_monto_x_solic mxs");
				strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
				strSQL.append(" AND mxs.fn_monto_moneda IS NULL)");
				strSQL.append(" AND sol.cs_multimoneda = ?");
				conditions.add("N");
			} else {
				strSQL.append(" AND (EXISTS (SELECT mxs.ic_monto_x_solic");
				strSQL.append(" FROM cder_monto_x_solic mxs");
				strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
				strSQL.append(" AND mxs.fn_monto_moneda IS NOT NULL)");
				strSQL.append(" OR EXISTS (SELECT mxs.ic_monto_x_solic");
				strSQL.append(" FROM cder_monto_x_solic mxs");
				strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
				strSQL.append(" AND mxs.fn_monto_moneda IS NULL))");
				strSQL.append(" AND sol.cs_multimoneda = ?");
				conditions.add("N");
			}
		}		

		if (plazoContrato != null && !"".equals(plazoContrato)) {
			strSQL.append(" AND sol.ig_plazo = ?");
			conditions.add(new Integer(plazoContrato));
		}		

		if (claveTipoPlazo != null && !"".equals(claveTipoPlazo)) {
			strSQL.append(" AND sol.ic_tipo_plazo = ?");
			conditions.add(new Integer(claveTipoPlazo));
		}		

		if (claveTipoContratacion != null && !"".equals(claveTipoContratacion)) {
			strSQL.append(" AND sol.ic_tipo_contratacion = ?");
			conditions.add(new Integer(claveTipoContratacion));
		}		

		if (fechaVigenciaContratoIni != null && !"".equals(fechaVigenciaContratoIni)) {
				strSQL.append(" AND sol.df_fecha_vigencia_ini >= TO_DATE(?, 'dd/mm/yyyy')");
				strSQL.append(" AND sol.df_fecha_vigencia_ini < TO_DATE(?, 'dd/mm/yyyy') + 1");
				conditions.add(fechaVigenciaContratoIni);
		}

		if (fechaVigenciaContratoFin != null && !"".equals(fechaVigenciaContratoFin)) {
				strSQL.append(" AND sol.df_fecha_vigencia_fin >= TO_DATE(?, 'dd/mm/yyyy')");
				strSQL.append(" AND sol.df_fecha_vigencia_fin < TO_DATE(?, 'dd/mm/yyyy') + 1");
				conditions.add(fechaVigenciaContratoFin);
		}		 

		strSQL.append(" ORDER BY sol.ic_solicitud");
			
		
		log.debug("..:: strSQL: "+strSQL.toString());
		log.debug("..:: conditions: "+conditions);
		
		log.info("getDocumentQueryFile(S) ::..");
		return strSQL.toString();
	}//getDocumentQueryFile
	
	public Registros getEmpresasRepresentadas(){
			log.info("getEmpresasRepresentadas(E) ::..");
		strSQL = new StringBuffer();
		Registros reg = new Registros();
		AccesoDB  con= new AccesoDB();
		conditions = new ArrayList();
				
		strSQL.append(" SELECT sol.ic_solicitud as clave_solicitud,"); 
		strSQL.append(" e.cg_razon_social as dependencia,");
		strSQL.append(" pyme.cg_razon_social||';'||sol.CG_EMPRESAS_REPRESENTADAS as nombre_cedente");//FODEA-024-2014 MOD()
		strSQL.append(" FROM cder_solicitud sol");
		strSQL.append(", comcat_epo e");
		strSQL.append(", comcat_if i");
		strSQL.append(", comcat_pyme pyme");
		strSQL.append(", cdercat_clasificacion_epo cl");
		strSQL.append(", cdercat_tipo_contratacion con");
		strSQL.append(", cdercat_estatus_solic es");
		strSQL.append(", cdercat_tipo_plazo stp");
		strSQL.append(", cder_extincion_contrato extc");
		strSQL.append(", cdercat_ventanilla_pago cvp");
	  strSQL.append(" WHERE  sol.ic_epo = e.ic_epo");
		strSQL.append(" AND sol.ic_if = i.ic_if ");
		strSQL.append(" AND sol.ic_pyme = pyme.ic_pyme");
		strSQL.append(" AND sol.ic_clasificacion_epo = cl.ic_clasificacion_epo(+)");
		strSQL.append(" AND SOL.ic_tipo_contratacion = con.ic_tipo_contratacion");
		strSQL.append(" AND SOL.ic_estatus_solic = es.ic_estatus_solic");
		strSQL.append(" AND sol.ic_tipo_plazo = stp.ic_tipo_plazo(+)");
		strSQL.append(" AND sol.ic_solicitud = extc.ic_solicitud(+)");
		strSQL.append(" AND sol.ic_epo = cvp.ic_epo(+)");
		strSQL.append(" AND sol.ic_ventanilla_pago = cvp.ic_ventanilla_pago(+)");
		
		
		if (claveEstatusSolicitud != null && !claveEstatusSolicitud.equals("")) {
			strSQL.append(" AND sol.ic_estatus_solic = ?");
			conditions.add(new Integer(claveEstatusSolicitud));
		} else {
			strSQL.append(" AND sol.ic_estatus_solic IN (?, ?, ?, ?, ?, ?)");
			conditions.add(new Integer(1));
			conditions.add(new Integer(13));
			conditions.add(new Integer(2));
			conditions.add(new Integer(5));
			conditions.add(new Integer(6));
			conditions.add(new Integer(8));
		}

		if (clavePyme != null && !"".equals(clavePyme)) {
			strSQL.append(" AND sol.ic_pyme = ?");
			conditions.add(new Long(clavePyme));
		}

		if (claveEpo != null && !"".equals(claveEpo)) {
			strSQL.append(" AND sol.ic_epo = ?");
			conditions.add(new Integer(claveEpo));
		}

		if (claveIfCesionario != null && !"".equals(claveIfCesionario)) {
			strSQL.append(" AND sol.ic_if = ?");
			conditions.add(new Integer(claveIfCesionario));
		}

		if (numeroContrato != null && !"".equals(numeroContrato)) {
			strSQL.append(" AND sol.cg_no_contrato = ?");
			conditions.add(numeroContrato);
		}
		if (claveMoneda != null && !"".equals(claveMoneda)) {
			if (claveMoneda.equals("0")) {
				strSQL.append(" AND sol.cs_multimoneda = ?");
				conditions.add("S");
			} else {
				strSQL.append(" AND sol.cs_multimoneda = ?");
				strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
				strSQL.append(" FROM cder_monto_x_solic mxs");
				strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
				strSQL.append(" AND mxs.ic_moneda = ?)");
				conditions.add("N");
				conditions.add(new Integer(claveMoneda));
			}
		}

		if (csTipoMonto != null && !"".equals(csTipoMonto)) {
			if (csTipoMonto.equals("D")) {
				strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
				strSQL.append(" FROM cder_monto_x_solic mxs");
				strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
				strSQL.append(" AND mxs.fn_monto_moneda IS NOT NULL)");
				strSQL.append(" AND sol.cs_multimoneda = ?");
				conditions.add("N");
			} else if (csTipoMonto.equals("M")) {
				strSQL.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
				strSQL.append(" FROM cder_monto_x_solic mxs");
				strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
				strSQL.append(" AND mxs.fn_monto_moneda IS NULL)");
				strSQL.append(" AND sol.cs_multimoneda = ?");
				conditions.add("N");
			} else {
				strSQL.append(" AND (EXISTS (SELECT mxs.ic_monto_x_solic");
				strSQL.append(" FROM cder_monto_x_solic mxs");
				strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
				strSQL.append(" AND mxs.fn_monto_moneda IS NOT NULL)");
				strSQL.append(" OR EXISTS (SELECT mxs.ic_monto_x_solic");
				strSQL.append(" FROM cder_monto_x_solic mxs");
				strSQL.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
				strSQL.append(" AND mxs.fn_monto_moneda IS NULL))");
				strSQL.append(" AND sol.cs_multimoneda = ?");
				conditions.add("N");
			}
		}		

		if (plazoContrato != null && !"".equals(plazoContrato)) {
			strSQL.append(" AND sol.ig_plazo = ?");
			conditions.add(new Integer(plazoContrato));
		}		

		if (claveTipoPlazo != null && !"".equals(claveTipoPlazo)) {
			strSQL.append(" AND sol.ic_tipo_plazo = ?");
			conditions.add(new Integer(claveTipoPlazo));
		}		

		if (claveTipoContratacion != null && !"".equals(claveTipoContratacion)) {
			strSQL.append(" AND sol.ic_tipo_contratacion = ?");
			conditions.add(new Integer(claveTipoContratacion));
		}		

		if (fechaVigenciaContratoIni != null && !"".equals(fechaVigenciaContratoIni)) {
				strSQL.append(" AND sol.df_fecha_vigencia_ini >= TO_DATE(?, 'dd/mm/yyyy')");
				strSQL.append(" AND sol.df_fecha_vigencia_ini < TO_DATE(?, 'dd/mm/yyyy') + 1");
				conditions.add(fechaVigenciaContratoIni);
		}

		if (fechaVigenciaContratoFin != null && !"".equals(fechaVigenciaContratoFin)) {
				strSQL.append(" AND sol.df_fecha_vigencia_fin >= TO_DATE(?, 'dd/mm/yyyy')");
				strSQL.append(" AND sol.df_fecha_vigencia_fin < TO_DATE(?, 'dd/mm/yyyy') + 1");
				conditions.add(fechaVigenciaContratoFin);
		}		 

		strSQL.append(" ORDER BY sol.ic_solicitud");
		try{
			con.conexionDB();
			reg=con.consultarDB(strSQL.toString(),conditions);	
		} catch (Exception e) {
			log.error("getEmpresasRepresentadas(Error) ::..");
			throw new AppException("Ocurrio un error ", e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info("getMapaCesionario(S) ::..");
		}
		
		
		log.debug("..:: strSQL: "+strSQL.toString());
		log.debug("..:: conditions: "+conditions);
		
		log.info("getEmpresasRepresentadas(S) ::..");
		return reg;
	
	}

public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.info("crearCustomFile(E) ::..");
		
		StringBuffer linea = new StringBuffer(1024);
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		String Dependencia = "";
		String cedente="";//FODEA-024-2014 MOD()
		Registros reg = this.getEmpresasRepresentadas();
		try {
			
			CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
			int indiceCamposAdicionales =0;
			String clasificacionEpo ="";
			HashMap camposAdicionalesParametrizados  = null;
			
			if( claveEpo != null){
			
				 clasificacionEpo = cesionBean.clasificacionEpo(claveEpo);
				
				//obtencion de campos Adicionales
				 camposAdicionalesParametrizados = cesionBean.getCamposAdicionalesParametrizados(claveEpo, "9");
				 indiceCamposAdicionales = Integer.parseInt((String)camposAdicionalesParametrizados.get("indice"));
			
			}
			if(tipo.equals("PDF")) {
			
				HttpSession session = request.getSession();
				String tipoUsuario = (String)session.getAttribute("strTipoUsuario");
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				//FODEA-024-2014
				int nSolicitudes = 0;
				while(reg.next()){
					cedente=(reg.getString("nombre_cedente") == null) ? "" : reg.getString("nombre_cedente");
					nSolicitudes++;
				}
				if(nSolicitudes>1){
					cedente=(String) session.getAttribute("strNombre");
				}else{
					String cedenteAux[] = cedente.split(";");
					if(cedenteAux.length == 1){
						cedente = cedente.replaceAll(";","");
					}else{
						cedente = cedente.replaceAll(";","\n");
					}
				}
				
				
				//
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				cedente,//(String) session.getAttribute("strNombre"),//FODEA-024-2014
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				pdfDoc.addText("Consulta Solicitud de Consentimiento ","formas",ComunesPDF.CENTER);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				
			
				int columnas = 15;
				if(!clasificacionEpo.equals("")) columnas +=1;
				if(indiceCamposAdicionales >0) columnas +=indiceCamposAdicionales;
				
			
				pdfDoc.setTable(++columnas, 100);			
				pdfDoc.setCell("Dependencia","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Intermediario Financiero(Cesionario)","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No. Contrato ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Firma Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto / Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de Contrataci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Inicio Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Final Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo delContrato","celda01",ComunesPDF.CENTER);				
				
				if(!clasificacionEpo.equals("")){
				pdfDoc.setCell(clasificacionEpo,"celda01",ComunesPDF.CENTER);
				}
				
				for(int y = 0; y < indiceCamposAdicionales; y++){	
						String nombreCampo = ("Campo"+y);
						String desnombreCampo = camposAdicionalesParametrizados.get("nombre_campo_"+y).toString();
						pdfDoc.setCell(desnombreCampo,"celda01",ComunesPDF.CENTER);
				}			
								
				pdfDoc.setCell("Ventanilla de Pago","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Supervisor/Administrador/Residente de Obra","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tel�fono","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Objeto del Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Comentarios","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Causa del Rechazo","celda01",ComunesPDF.CENTER);
				while (rs.next()) {
					Dependencia=(rs.getString("DEPENDENCIA") == null) ? "" : rs.getString("DEPENDENCIA");
					pdfDoc.setCell((rs.getString("DEPENDENCIA") == null) ? "" : rs.getString("DEPENDENCIA"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("NOMBRE_CESIONARIO") == null) ? "" : rs.getString("NOMBRE_CESIONARIO"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("NUMERO_CONTRATO") == null) ? "" : rs.getString("NUMERO_CONTRATO"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("FIRMA_CONTRATO") == null) ? "" : rs.getString("FIRMA_CONTRATO"),"formas",ComunesPDF.CENTER);
					StringBuffer montosMonedaSol = cesionBean.getMontoMoneda(rs.getString("CLAVE_SOLICITUD"));
					pdfDoc.setCell(montosMonedaSol.toString().replaceAll(",", "").replaceAll("<br/>","\n"),"formas",ComunesPDF.LEFT);				
					pdfDoc.setCell((rs.getString("TIPO_CONTRATACION") == null) ? "" : rs.getString("TIPO_CONTRATACION"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("FECHA_INICIO_CONTRATO") == null) ? "" : rs.getString("FECHA_INICIO_CONTRATO"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("FECHA_FIN_CONTRATO") == null) ? "" : rs.getString("FECHA_FIN_CONTRATO"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("PLAZO_CONTRATO") == null) ? "" : rs.getString("PLAZO_CONTRATO"),"formas",ComunesPDF.CENTER);
					if (!clasificacionEpo.equals("")) {
					pdfDoc.setCell((rs.getString("CLASIFICACION_EPO") == null) ? "" : rs.getString("CLASIFICACION_EPO"),"formas",ComunesPDF.CENTER);
					}
					for(int j = 0; j < indiceCamposAdicionales; j++){
						String campoAdicional ="CAMPO_ADICIONAL_"+(j + 1);
						pdfDoc.setCell((rs.getString(campoAdicional) == null) ? "" : rs.getString(campoAdicional),"formas",ComunesPDF.CENTER);
					}
					pdfDoc.setCell((rs.getString("VENANILLA_PAGO") == null) ? "" : rs.getString("VENANILLA_PAGO"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("SUP_ADM_RESOB") == null) ? "" : rs.getString("SUP_ADM_RESOB"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("NUMERO_TELEFONO") == null) ? "" : rs.getString("NUMERO_TELEFONO"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("OBJETO_CONTRATO") == null) ? "" : rs.getString("OBJETO_CONTRATO"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("COMENTARIOS") == null) ? "" : rs.getString("COMENTARIOS"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("ESTATUS_SOLICITUD") == null) ? "" : rs.getString("ESTATUS_SOLICITUD"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((rs.getString("CAUSAS_RECHAZO") == null) ? "" : rs.getString("CAUSAS_RECHAZO"),"formas",ComunesPDF.CENTER);
					
				}
				//rs.close();  
				
				pdfDoc.addTable();
				StringBuffer strbuffLeyendaLegal = new StringBuffer();
				
				String nombresRepresentantesPyme ="", nombreEpo="";
				String clavePyme = (String)request.getSession().getAttribute("iNoCliente");
				UtilUsr utilUsr = new UtilUsr();
				List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(clavePyme, "P");
				
				for (int i = 0; i < usuariosPorPerfil.size(); i++) {
				String loginUsrPyme = (String)usuariosPorPerfil.get(i);
				Usuario usuario = utilUsr.getUsuario(loginUsrPyme);
				nombresRepresentantesPyme += " " + usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre() + ",";
				}

				/*
				float anchoCelda3[] = {100f};
				pdfDoc.setTable(1, 100, anchoCelda3);
				
				String texto=((nSolicitudes>1)?cedente.replaceAll("\n",",")+" y su(s) empresa(s) representada(s)":cedente.replaceAll("\n",","));
				
				strbuffLeyendaLegal.append("De conformidad con lo se�alado en el(los) Contrato(s) antes mencionado(s), solicitamos que la Empresa "+Dependencia+", tenga a bien otorgar su consentimiento"+
			"a la (las) empresa(s) "+cedente.replaceAll("\n",",")+", "+ " antes se�alada(s) para ceder a favor del INTERMEDIARIO FINANCIERO se�alado para estos efectos,  los derechos de cobro sobre la(s) "+
			" factura(s) por trabajos ejecutados, por el 100% (cien por ciento) del monto de dicho Contrato. \n\n"+
			
			"En virtud de lo anterior,  una vez que cuente con el consentimiento que por este medio les es solicitado, celebraremos el contrato de cesi�n de derechos "+
			" de cobro con el INTERMEDIARIO FINANCIERO, quien proceder� a notificar mediante el sistema NAFIN, en t�rminos de C�digo de Comercio."); 
				 
				pdfDoc.setCell(strbuffLeyendaLegal.toString(), "formas", ComunesPDF.JUSTIFIED, 1, 1, 1);
	pdfDoc.addTable();
				*/


				pdfDoc.endDocument();
				
			}else if(tipo.equals("CSV")) {
			
				CreaArchivo creaArchivo = new CreaArchivo();
				StringBuffer contenidoArchivo = new StringBuffer();
			
				contenidoArchivo.append("Dependencia,");
				contenidoArchivo.append("Intermediario Financiero(Cesionario),");
				contenidoArchivo.append("No. Contrato ,");
				contenidoArchivo.append("Firma Contrato ,");
				contenidoArchivo.append("Monto / Moneda,");
				contenidoArchivo.append("Tipo de Contrataci�n ,");
				contenidoArchivo.append("Fecha Inicio Contrato ,");
				contenidoArchivo.append("Fecha Final Contrato,");
				contenidoArchivo.append("Plazo delContrato ,");
				if(!clasificacionEpo.equals("")){
				contenidoArchivo.append(clasificacionEpo+",");
				}
			
				contenidoArchivo.append("Ventanilla de Pago,");
				contenidoArchivo.append("Supervisor/Administrador/Residente de Obra ,");
				contenidoArchivo.append("Tel�fono ,");
				contenidoArchivo.append("Objeto del Contrato ,");
				contenidoArchivo.append("Comentarios,");
				contenidoArchivo.append("Estatus,");
				contenidoArchivo.append("Causa del Rechazo,");
				for(int y = 0; y < indiceCamposAdicionales; y++){	
						String nombreCampo = ("Campo"+y);
				contenidoArchivo.append(camposAdicionalesParametrizados.get("nombre_campo_"+y)+"," );		
				}	
				contenidoArchivo.append("\n");
								
				while (rs.next()) {
				String vacio =" ";
						
					String 	CLASIFICACION_EPO = (rs.getString("CLASIFICACION_EPO")== null)?vacio:rs.getString("CLASIFICACION_EPO").toString().replaceAll(",","");
					String 	VENANILLA_PAGO = (rs.getString("VENANILLA_PAGO")== null)?vacio:rs.getString("VENANILLA_PAGO").toString().replaceAll(",","");
					String 	SUP_ADM_RESOB = (rs.getString("SUP_ADM_RESOB")== null)?vacio:rs.getString("SUP_ADM_RESOB").toString().replaceAll(",","");
					String 	NUMERO_TELEFONO = (rs.getString("NUMERO_TELEFONO")== null)?vacio:rs.getString("NUMERO_TELEFONO").toString().replaceAll(",","");
					String 	OBJETO_CONTRATO = (rs.getString("OBJETO_CONTRATO")== null)?vacio:rs.getString("OBJETO_CONTRATO").toString().replaceAll(",","");
					String 	COMENTARIOS = (rs.getString("COMENTARIOS")== null)?vacio:rs.getString("COMENTARIOS").toString().replaceAll(",","");
					String 	ESTATUS_SOLICITUD = (rs.getString("ESTATUS_SOLICITUD")== null)?vacio:rs.getString("ESTATUS_SOLICITUD").toString().replaceAll(",","");
					String 	CAUSAS_RECHAZO = (rs.getString("CAUSAS_RECHAZO")== null)?vacio:rs.getString("CAUSAS_RECHAZO").toString().replaceAll(",","");
					String 	DEPENDENCIA = (rs.getString("DEPENDENCIA")== null)?vacio:rs.getString("DEPENDENCIA").toString().replaceAll(",","");
					String 	NOMBRE_CESIONARIO = (rs.getString("NOMBRE_CESIONARIO")== null)?vacio:rs.getString("NOMBRE_CESIONARIO").toString().replaceAll(",","");
					String 	NUMERO_CONTRATO = (rs.getString("NUMERO_CONTRATO")== null)?vacio:rs.getString("NUMERO_CONTRATO").toString().replaceAll(",","");
					StringBuffer montosMonedaSol = cesionBean.getMontoMoneda(rs.getString("CLAVE_SOLICITUD"));
					String 	TIPO_CONTRATACION = (rs.getString("TIPO_CONTRATACION")== null)?vacio:rs.getString("TIPO_CONTRATACION").toString().replaceAll(",","");
					String 	FECHA_INICIO_CONTRATO = (rs.getString("FECHA_INICIO_CONTRATO")== null)?vacio:rs.getString("FECHA_INICIO_CONTRATO").toString().replaceAll(",","");
					String 	FECHA_FIN_CONTRATO = (rs.getString("FECHA_FIN_CONTRATO")== null)?vacio:rs.getString("FECHA_FIN_CONTRATO").toString().replaceAll(",","");
					String 	PLAZO_CONTRATO = (rs.getString("PLAZO_CONTRATO")== null)?vacio:rs.getString("PLAZO_CONTRATO").toString().replaceAll(",","");
					String 	FIRMA_CONTRATO = (rs.getString("FIRMA_CONTRATO")== null)?vacio:rs.getString("FIRMA_CONTRATO").toString();
					contenidoArchivo.append(DEPENDENCIA+ ",");
					contenidoArchivo.append(NOMBRE_CESIONARIO+ ",");
					contenidoArchivo.append(NUMERO_CONTRATO+ ",");
					contenidoArchivo.append(FIRMA_CONTRATO+ ",");
					
					contenidoArchivo.append(montosMonedaSol.toString().replaceAll("<br/>", "|").replaceAll(",", "")+ ",");					
					contenidoArchivo.append(TIPO_CONTRATACION+ ",");
					contenidoArchivo.append(FECHA_INICIO_CONTRATO+ ",");
					contenidoArchivo.append(FECHA_FIN_CONTRATO+",");
					contenidoArchivo.append(PLAZO_CONTRATO+ ",");						
					if (!clasificacionEpo.equals("")) {
						contenidoArchivo.append(CLASIFICACION_EPO+ ",");
					}				
					contenidoArchivo.append(VENANILLA_PAGO+ ",");					
					contenidoArchivo.append(SUP_ADM_RESOB+ ",");
					contenidoArchivo.append(NUMERO_TELEFONO+ ",");					
					contenidoArchivo.append(OBJETO_CONTRATO+ ",");
					contenidoArchivo.append(COMENTARIOS+ ",");
					contenidoArchivo.append(ESTATUS_SOLICITUD+ ",");
					contenidoArchivo.append(CAUSAS_RECHAZO+ ",");
					
					for(int j = 0; j < indiceCamposAdicionales; j++){
						String campoAdicional ="CAMPO_ADICIONAL_"+(j + 1);	
						String descampoAdicional =(rs.getString(campoAdicional)== null)?vacio:rs.getString(campoAdicional).replaceAll(",","")+ ",";	
						
						if(!descampoAdicional.equals("") ){
							if(j==0||j==1){
							descampoAdicional="'"+descampoAdicional;
							}
							contenidoArchivo.append(descampoAdicional);
						}						
					}					
					
					contenidoArchivo.append(",\n");
				}
				rs.close();
								
				creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
				
				nombreArchivo = creaArchivo.getNombre();
			}
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo", e);
		} finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
		log.info("crearCustomFile(S) ::..");	
		return nombreArchivo;
		
	}
	
	

/*****************************************************
	 GETTERS
*******************************************************/
 
	/**
	  Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	
	public String getClavePyme() {return this.clavePyme;}
	public String getClaveEpo() {return this.claveEpo;}
	public String getClaveIfCesionario() {return this.claveIfCesionario;}
	public String getNumeroContrato() {return this.numeroContrato;}
	public String getMontoContrato() {return this.montoContrato;}
	public String getClaveMoneda() {return this.claveMoneda;}
	public String getClaveTipoContratacion() {return this.claveTipoContratacion;}
	public String getFechaVigenciaContratoIni() {return this.fechaVigenciaContratoIni;}
	public String getFechaVigenciaContratoFin() {return this.fechaVigenciaContratoFin;}
	public String getClaveEstatusSolicitud() {return this.claveEstatusSolicitud;}
	public String getCsTipoMonto() {return this.csTipoMonto;}
	public String getPlazoContrato() {return this.plazoContrato;}
	public String getClaveTipoPlazo() {return this.claveTipoPlazo;}

	//GET
	public String getNoEpo() {	return noEpo;  }
	public String getNoIF() { 	return noIF; }
	public String getClasificacionEPO() { 	return clasificacionEPO; 	}
	public String getNoContrato() { 	return noContrato; 	}
	public String getContratacion() { 	return contratacion; }
	public String getFvigenciaIni() {  	return fvigenciaIni;  }
	public String getFvigenciaFin() {  return fvigenciaFin;  }
	public String getCampo1() {  return campo1;  }
	public String getCampo2() { 	return campo2;  }
	public String getCampo3() { return campo3;  }
	public String getCampo4() { return campo4;  }
	public String getCampo5() { 	return campo5;  	}
	public String getContrato() { return contrato;  }
	public String getSolicitud() { 		return solicitud; 	}
	
	
	 	
	
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public void setClavePyme(String clavePyme) {this.clavePyme = clavePyme;}
	public void setClaveEpo(String claveEpo) {this.claveEpo = claveEpo;}
	public void setClaveIfCesionario(String claveIfCesionario) {this.claveIfCesionario = claveIfCesionario;}
	public void setNumeroContrato(String numeroContrato) {this.numeroContrato = numeroContrato;}
	public void setMontoContrato(String montoContrato) {this.montoContrato = montoContrato;}
	public void setClaveMoneda(String claveMoneda) {this.claveMoneda = claveMoneda;}
	public void setClaveTipoContratacion(String claveTipoContratacion) {this.claveTipoContratacion = claveTipoContratacion;}
	public void setFechaVigenciaContratoIni(String fechaVigenciaContratoIni) {this.fechaVigenciaContratoIni = fechaVigenciaContratoIni;}
	public void setFechaVigenciaContratoFin(String fechaVigenciaContratoFin) {this.fechaVigenciaContratoFin = fechaVigenciaContratoFin;}
	public void setClaveEstatusSolicitud(String claveEstatusSolicitud) {this.claveEstatusSolicitud = claveEstatusSolicitud;}
	public void setCsTipoMonto(String csTipoMonto) {this.csTipoMonto = csTipoMonto;}
	public void setPlazoContrato(String plazoContrato) {this.plazoContrato = plazoContrato;}
	public void setClaveTipoPlazo(String claveTipoPlazo) {this.claveTipoPlazo = claveTipoPlazo;}

// SET	
	public void setNoEpo(String noEpo) {  this.noEpo = noEpo; 	} 
	public void setNoIF(String noIF) { 		this.noIF = noIF; }
	public void setClasificacionEPO(String clasificacionEPO) { 	this.clasificacionEPO = clasificacionEPO; 	}
	public void setNoContrato(String noContrato) { 	this.noContrato = noContrato; }
	public void setContratacion(String contratacion) { 	this.contratacion = contratacion;  }
	public void setFvigenciaIni(String fvigenciaIni) {   	this.fvigenciaIni = fvigenciaIni; 	}
	public void setFvigenciaFin(String fvigenciaFin) {  	this.fvigenciaFin = fvigenciaFin; 	}
	public void setCampo1(String campo1) { this.campo1 = campo1; }
	public void setCampo2(String campo2) {  this.campo2 = campo2;  }
	public void setCampo3(String campo3) { this.campo3 = campo3;  }
	public void setCampo4(String campo4) {  this.campo4 = campo4; 	}
	public void setCampo5(String campo5) {  this.campo5 = campo5;  	}
	public void setContrato(String contrato) { this.contrato = contrato; }
	public void setSolicitud(String solicitud) { 		this.solicitud = solicitud; 	}
	public String getDirectorio() {
		return directorio;
	}

	public void setDirectorio(String directorio) {
		this.directorio = directorio;
	}

public String getPyme() {
		return pyme;
	}

	public void setPyme(String pyme) {
		this.pyme = pyme;
	}

	
	
	

	
}