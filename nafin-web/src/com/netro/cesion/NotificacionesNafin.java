package com.netro.cesion;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

public class NotificacionesNafin implements IQueryGeneratorRegExtJS {
	public NotificacionesNafin() {}
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	
	StringBuffer strQuery = new StringBuffer("");
	
	private static final Log log = ServiceLocator.getInstance().getLog(NotificacionesNafin.class);

	//Condiciones de la consulta
	private String loginUsuario;
	private String claveEpo;
	private String claveIf;
	private String clavePyme;
	private String claveEstatusSol;
	private String numeroContrato;
	private String fechaVigenciaIni;
	private String fechaVigenciaFin;
	private String fechaSolicitudIni;
	private String fechaSolicitudFin;
	private String plazoContrato;
	private String claveTipoPlazo;
	private String claveContrat;
	private String campo1;
	private String campo2;
	private String campo3;
	private String campo4;
	private String campo5;
	private List tipoDato;
	private String claveMoneda;
	
	public String getAggregateCalculationQuery(){
	
		return "";
	}
		 /**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		strQuery = new StringBuffer();
		conditions = new ArrayList();
		strQuery.append(" SELECT sol.ic_solicitud AS clave_solicitud" +
							 " FROM cder_solicitud sol"+
							 ", cder_acuse ca "+
							 ", cder_acuse ca2"+
							 ", comcat_epo epo"+
							 ", comcat_if cif"+
							 ", comcat_pyme cp"+
							 ", cdercat_ventanilla_pago cvp"+
							 ", comrel_pyme_epo cpe"+
							 ", comrel_producto_epo cpepo"+
							 ", cdercat_tipo_contratacion tcn"+
							 ", cdercat_clasificacion_epo cep"+
							 ", cdercat_estatus_solic sts"+
							 ", (select ig_dias_pnotificacion from comrel_producto_epo where ic_epo = ? and ic_producto_nafin = ?) lim"+
							 ", cdercat_tipo_plazo ctp"+
							 " WHERE sol.ic_epo = epo.ic_epo"+
							 " AND sol.ic_if = cif.ic_if"+
							 " AND sol.ic_pyme = cp.ic_pyme"+
							 " AND sol.ic_epo = cpe.ic_epo(+)"+
							 " AND sol.ic_pyme = cpe.ic_pyme(+)"+
							 " AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion"+
		                " AND sol.ic_epo = cep.ic_epo"+
							 " AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo"+
							 " AND sol.ic_estatus_solic = sts.ic_estatus_solic"+
							 " AND sol.cc_acuse3 = ca.cc_acuse(+)"+
							 " AND sol.cc_acuse1 = ca2.cc_acuse(+)"+
							 " AND sol.ic_epo = cpepo.ic_epo"+
							 " AND cpepo.ic_producto_nafin = ?"+
							 " AND sol.ic_epo = cvp.ic_epo(+)"+
							 " AND sol.ic_ventanilla_pago = cvp.ic_ventanilla_pago(+)"+
							 " AND sol.ic_estatus_solic IN (11, 12, 15, 16)"+
							 " AND sol.ic_tipo_plazo = ctp.ic_tipo_plazo(+)");
		conditions.add(claveEpo);
		conditions.add("9");
		conditions.add("9");
		if(!"".equals(claveContrat) && claveContrat != null){
			strQuery.append("   AND tcn.ic_tipo_contratacion = ? ");
			conditions.add(claveContrat);
		}
			
		if(!"".equals(claveEstatusSol) && claveEstatusSol != null){
			strQuery.append("   AND sts.ic_estatus_solic = ? ");
			conditions.add(claveEstatusSol);
		}
		
		if(!"".equals(claveEpo) && claveEpo != null){
			strQuery.append("   AND epo.ic_epo = ? ");
			conditions.add(claveEpo);
		}
			
		if(!"".equals(clavePyme) && clavePyme != null){
			strQuery.append("   AND cp.ic_pyme = ? " );
			conditions.add(clavePyme);
		}
			
		if(!"".equals(claveIf) && claveIf != null){
			strQuery.append("   AND cif.ic_if = ? " );
			conditions.add(claveIf);
		}	
			
		if((!"".equals(fechaVigenciaIni) && fechaVigenciaIni != null) && (!"".equals(fechaVigenciaFin) && fechaVigenciaFin != null)){
			strQuery.append(" AND sol.df_fecha_vigencia_ini >= TO_DATE(?, 'dd/mm/yyyy')");
			strQuery.append(" AND sol.df_fecha_vigencia_fin <= TO_DATE(?, 'dd/mm/yyyy')");
			conditions.add(fechaVigenciaIni);
			conditions.add(fechaVigenciaFin);
		}
			
		if(!"".equals(numeroContrato) && numeroContrato != null){
			strQuery.append("   AND sol.cg_no_contrato = ? ");
			conditions.add(numeroContrato);
		}
		if (claveMoneda != null && !"".equals(claveMoneda)) {
			if (claveMoneda.equals("0")) {
				strQuery.append(" AND sol.cs_multimoneda = ?");
            conditions.add("S");
			} else {
				 strQuery.append(" AND sol.cs_multimoneda = ?");
            strQuery.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
            strQuery.append(" FROM cder_monto_x_solic mxs");
            strQuery.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
            strQuery.append(" AND mxs.ic_moneda = ?)");
            conditions.add("N");
            conditions.add(new Integer(claveMoneda));
         }
      }
		if(!"".equals(claveTipoPlazo) && claveTipoPlazo != null){
			strQuery.append(" AND sol.ic_tipo_plazo = ? ");
          conditions.add(new Integer(claveTipoPlazo));
      }
		if(!"".equals(plazoContrato) && plazoContrato != null){
			strQuery.append(" AND sol.ig_plazo = ? ");
         conditions.add(new Integer(plazoContrato));
      }
		strQuery.append(" ORDER BY sol.df_alta_solicitud DESC");
		log.debug("..:: strQuery getDocumentQuery: "+strQuery.toString());
		log.debug("..:: conditions : "+conditions);
		log.info("getDocumentQuery(S)");
		return strQuery.toString();
		
	}
		
	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds) {
		log.info("getDocumentSummaryQueryForIds(E)");
		strQuery 	= new StringBuffer();
		conditions 		= new ArrayList();
		strQuery.append(	" SELECT sol.ic_solicitud AS clave_solicitud,"+
								" sol.ic_epo AS clave_epo,"+
								" sol.ic_pyme AS clave_pyme,"+
								" sol.ic_if AS clave_if,"+
								" '' as mone,"+
								" '' as representante,"+
								"	epo.cg_razon_social AS dependencia,"+
								" cif.cg_razon_social AS cesionario,"+
								" /*cp.cg_razon_social AS pyme,*/"+
								"	cp.cg_razon_social||';'||sol.cg_empresas_representadas as pyme,"+ //FODEA-024-2014 MOD()
								" cp.cg_rfc AS RFC,"+
								" cpe.cg_pyme_epo_interno AS numero_proveedor,"+
								" TO_CHAR(sol.df_fecha_solicitud_ini, 'dd/mm/yyyy') AS fecha_solicitud,"+
								" sol.cg_no_contrato AS numero_contrato,"+
								" tcn.cg_nombre AS tipo_contratacion,"+
								" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,"+
								" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,"+
								" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || ctp.cg_descripcion) AS plazo_contrato,"+
								" cep.cg_area AS clasificacion_epo,"+
								" TO_CHAR(TO_DATE(sol.df_fecha_aceptacion, 'dd/mm/yyyy')+lim.ig_dias_pnotificacion) AS fecha_limite_notificacion,"+
								" sol.cg_campo1 AS campo_adicional_1,"+
								" sol.cg_campo2 AS campo_adicional_2,"+
								" sol.cg_campo3 AS campo_adicional_3,"+
								" sol.cg_campo4 AS campo_adicional_4,"+
								" sol.cg_campo5 AS campo_adicional_5,"+
								" cvp.cg_descripcion AS ventanilla_pago,"+
								" sol.cg_sup_adm_resob AS sup_adm_resob,"+
								" sol.cg_numero_telefono AS numero_telefono,"+
								" sol.cg_obj_contrato AS objeto_contrato,"+
								" sol.cg_comentarios AS comentarios,"+
								" sol.fn_monto_credito AS monto_credito,"+
								" sol.cg_referencia AS numero_referencia,"+
								" TO_CHAR(sol.df_vencimiento_credito, 'dd/mm/yyyy') AS fecha_vencimiento,"+
								" sol.cg_banco_deposito AS banco_deposito,"+
								" sol.cg_cuenta AS numero_cuenta,"+
								" sol.cg_cuenta_clabe AS clabe,"+
								" TO_CHAR(ca.df_operacion, 'dd/mm/yyyy') AS fecha_autrech_pyme,"+
								" TO_CHAR(ca2.df_operacion, 'dd/mm/yyyy') AS fecha_autrech_epo,"+
								" sts.cg_descripcion AS estatus,"+
								" epo.cg_clasificacion_epo AS nombre_clasificaion_epo,"+
								" sol.cg_observ_rechazo as observ_rechazo"+
								" FROM cder_solicitud sol"+
								", cder_acuse ca "+
								", cder_acuse ca2"+
								", comcat_epo epo"+
								", comcat_if cif"+
								", comcat_pyme cp"+
								", cdercat_ventanilla_pago cvp"+
								", comrel_pyme_epo cpe"+
								", comrel_producto_epo cpepo"+
								", cdercat_tipo_contratacion tcn"+
								", cdercat_clasificacion_epo cep"+
								", cdercat_estatus_solic sts"+
								", (select ig_dias_pnotificacion from comrel_producto_epo where ic_epo = ? and ic_producto_nafin = ?) lim"+
								", cdercat_tipo_plazo ctp"+
								" WHERE sol.ic_epo = epo.ic_epo"+
								" AND sol.ic_if = cif.ic_if"+
								" AND sol.ic_pyme = cp.ic_pyme"+
								" AND sol.ic_epo = cpe.ic_epo(+)"+
								" AND sol.ic_pyme = cpe.ic_pyme(+)"+
								" AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion"+
								" AND sol.ic_epo = cep.ic_epo"+
								" AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo"+
								" AND sol.ic_estatus_solic = sts.ic_estatus_solic"+
								" AND sol.cc_acuse3 = ca.cc_acuse(+)"+
								" AND sol.cc_acuse1 = ca2.cc_acuse(+)"+
								" AND sol.ic_epo = cpepo.ic_epo"+
								" AND cpepo.ic_producto_nafin = ?"+
								" AND sol.ic_epo = cvp.ic_epo(+)"+
								" AND sol.ic_ventanilla_pago = cvp.ic_ventanilla_pago(+)"+
								" AND sol.ic_estatus_solic IN (11, 12, 15, 16)"+
								" AND sol.ic_tipo_plazo = ctp.ic_tipo_plazo(+)");
		conditions.add(claveEpo);
		conditions.add("9");
		conditions.add("9");
		strQuery.append(" AND (");
		for (int i = 0; i < pageIds.size(); i++) { 
			List lItem = (ArrayList)pageIds.get(i);
			if(i > 0){strQuery.append("  OR  ");}
			strQuery.append("sol.ic_solicitud = ?");
			conditions.add(new Long(lItem.get(0).toString()));
		}
		strQuery.append(" ) ");
		strQuery.append(" ORDER BY sol.df_alta_solicitud DESC");
		log.debug("strSQL ::.."+strQuery.toString());
		log.debug("Conditions ::.."+conditions);
		log.info("getDocumentSummaryQueryForIds(S) ::..");
		return strQuery.toString();
	}

	public String getDocumentQueryFile(){
		log.info("getDocumentQueryFile(E)");
		conditions	= new ArrayList();
		strQuery		= new StringBuffer();
		strQuery.append(	" SELECT sol.ic_solicitud AS clave_solicitud,"+
								" sol.ic_epo AS clave_epo,"+
								" sol.ic_pyme AS clave_pyme,"+
								" sol.ic_if AS clave_if,"+
								" '' as mone,"+
								" '' as representante,"+
								"	epo.cg_razon_social AS dependencia,"+
								" cif.cg_razon_social AS cesionario,"+
								" /*cp.cg_razon_social AS pyme,*/"+
								"	cp.cg_razon_social||';'||sol.cg_empresas_representadas as pyme,"+ //FODEA-024-2014 MOD()
								" cp.cg_rfc AS RFC,"+
								" cpe.cg_pyme_epo_interno AS numero_proveedor,"+
								" TO_CHAR(sol.df_fecha_solicitud_ini, 'dd/mm/yyyy') AS fecha_solicitud,"+
								" sol.cg_no_contrato AS numero_contrato,"+
								" tcn.cg_nombre AS tipo_contratacion,"+
								" DECODE(sol.df_fecha_vigencia_ini, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy')) AS fecha_inicio_contrato,"+
								" DECODE(sol.df_fecha_vigencia_fin, null, 'N/A', TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy')) AS fecha_fin_contrato,"+
								" DECODE(sol.ig_plazo, null, 'N/A', sol.ig_plazo || ' ' || ctp.cg_descripcion) AS plazo_contrato,"+
								" cep.cg_area AS clasificacion_epo,"+
								" TO_CHAR(TO_DATE(sol.df_fecha_aceptacion, 'dd/mm/yyyy')+lim.ig_dias_pnotificacion) AS fecha_limite_notificacion,"+
								" sol.cg_campo1 AS campo_adicional_1,"+
								" sol.cg_campo2 AS campo_adicional_2,"+
								" sol.cg_campo3 AS campo_adicional_3,"+
								" sol.cg_campo4 AS campo_adicional_4,"+
								" sol.cg_campo5 AS campo_adicional_5,"+
								" cvp.cg_descripcion AS ventanilla_pago,"+
								" sol.cg_sup_adm_resob AS sup_adm_resob,"+
								" sol.cg_numero_telefono AS numero_telefono,"+
								" sol.cg_obj_contrato AS objeto_contrato,"+
								" sol.cg_comentarios AS comentarios,"+
								" sol.fn_monto_credito AS monto_credito,"+
								" sol.cg_referencia AS numero_referencia,"+
								" TO_CHAR(sol.df_vencimiento_credito, 'dd/mm/yyyy') AS fecha_vencimiento,"+
								" sol.cg_banco_deposito AS banco_deposito,"+
								" sol.cg_cuenta AS numero_cuenta,"+
								" sol.cg_cuenta_clabe AS clabe,"+
								" TO_CHAR(ca.df_operacion, 'dd/mm/yyyy') AS fecha_autrech_pyme,"+
								" TO_CHAR(ca2.df_operacion, 'dd/mm/yyyy') AS fecha_autrech_epo,"+
								" sts.cg_descripcion AS estatus,"+
								" epo.cg_clasificacion_epo AS nombre_clasificaion_epo,"+
								" sol.cg_observ_rechazo as observ_rechazo"+
								" FROM cder_solicitud sol"+
								", cder_acuse ca "+
								", cder_acuse ca2"+
								", comcat_epo epo"+
								", comcat_if cif"+
								", comcat_pyme cp"+
								", cdercat_ventanilla_pago cvp"+
								", comrel_pyme_epo cpe"+
								", comrel_producto_epo cpepo"+
								", cdercat_tipo_contratacion tcn"+
								", cdercat_clasificacion_epo cep"+
								", cdercat_estatus_solic sts"+
								", (select ig_dias_pnotificacion from comrel_producto_epo where ic_epo = ? and ic_producto_nafin = ?) lim"+
								", cdercat_tipo_plazo ctp"+
								" WHERE sol.ic_epo = epo.ic_epo"+
								" AND sol.ic_if = cif.ic_if"+
								" AND sol.ic_pyme = cp.ic_pyme"+
								" AND sol.ic_epo = cpe.ic_epo(+)"+
								" AND sol.ic_pyme = cpe.ic_pyme(+)"+
								" AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion"+
								" AND sol.ic_epo = cep.ic_epo"+
								" AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo"+
								" AND sol.ic_estatus_solic = sts.ic_estatus_solic"+
								" AND sol.cc_acuse3 = ca.cc_acuse(+)"+
								" AND sol.cc_acuse1 = ca2.cc_acuse(+)"+
								" AND sol.ic_epo = cpepo.ic_epo"+
								" AND cpepo.ic_producto_nafin = ? "+
								" AND sol.ic_epo = cvp.ic_epo(+)"+
								" AND sol.ic_ventanilla_pago = cvp.ic_ventanilla_pago(+)"+
								" AND sol.ic_estatus_solic IN (11, 12, 15, 16)"+
								" AND sol.ic_tipo_plazo = ctp.ic_tipo_plazo(+)");
		conditions.add(claveEpo);
		conditions.add("9");
		conditions.add("9");
		if (loginUsuario != null && !"".equals(loginUsuario)) {
			strQuery.append(" AND SUBSTR(cep.cg_responsable, 1, 8) = ?");
			conditions.add(loginUsuario);
		}
		if(!"".equals(claveContrat) && claveContrat != null){//ya
			strQuery.append("   AND tcn.ic_tipo_contratacion = ? ");
			conditions.add(claveContrat);
		}
		if(!"".equals(claveEstatusSol) && claveEstatusSol != null){//ya
			strQuery.append("   AND sts.ic_estatus_solic = ? ");
			conditions.add(claveEstatusSol);
		}
		if(!"".equals(claveEpo) && claveEpo != null){//ya
			strQuery.append("   AND epo.ic_epo = ? ");
			conditions.add(claveEpo);
		}
		if(!"".equals(clavePyme) && clavePyme != null){//ya
			strQuery.append("   AND cp.ic_pyme = ? " );
			conditions.add(clavePyme);
		}
		if(!"".equals(claveIf) && claveIf != null){//ya
			strQuery.append("   AND cif.ic_if = ? " );
			conditions.add(claveIf);
		}	
		if((!"".equals(fechaVigenciaIni) && fechaVigenciaIni != null) && (!"".equals(fechaVigenciaFin) && fechaVigenciaFin != null)){
			strQuery.append(" AND sol.df_fecha_vigencia_ini >= TO_DATE(?, 'dd/mm/yyyy')");//ya
			strQuery.append(" AND sol.df_fecha_vigencia_fin <= TO_DATE(?, 'dd/mm/yyyy')");
			conditions.add(fechaVigenciaIni);
			conditions.add(fechaVigenciaFin);
		}
		if(!"".equals(numeroContrato) && numeroContrato != null){//ya
			strQuery.append("   AND sol.cg_no_contrato = ? ");
			conditions.add(numeroContrato);
		}
		if (claveMoneda != null && !"".equals(claveMoneda)) {//ya
			if (claveMoneda.equals("0")) {
				strQuery.append(" AND sol.cs_multimoneda = ?");
            conditions.add("S");
         } else {
            strQuery.append(" AND sol.cs_multimoneda = ?");
            strQuery.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
            strQuery.append(" FROM cder_monto_x_solic mxs");
            strQuery.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
            strQuery.append(" AND mxs.ic_moneda = ?)");
            conditions.add("N");
            conditions.add(new Integer(claveMoneda));
         }
      }
		if(!"".equals(claveTipoPlazo) && claveTipoPlazo != null){
			strQuery.append(" AND sol.ic_tipo_plazo = ? ");
          conditions.add(new Integer(claveTipoPlazo));
      }
		if(!"".equals(plazoContrato) && plazoContrato != null){
         strQuery.append(" AND sol.ig_plazo = ? ");
			conditions.add(new Integer(plazoContrato));
      }
		strQuery.append(" ORDER BY sol.df_alta_solicitud DESC");
		log.debug("..:: strQuery getDocumentQuery: "+strQuery.toString());
		log.debug("..:: conditions: "+conditions);
		log.info("getDocumentQuery(S)");
		return strQuery.toString();
	}
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		log.info("crearPageCustomFile(E)");
		String nombreArchivo = "";
		CreaArchivo creaArchivo = new CreaArchivo();
		ComunesPDF pdfDoc = new ComunesPDF();
		try {

			if(tipo.equals("PDF")) {
				CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
				int indiceCamposAdicionales =0;
				String nombreClasificacionEpo = cesionBean.clasificacionEpo(claveEpo);
				Vector lovDatos = cesionBean.getCamposAdicionales(claveEpo, "5");
				int hayRegistros = 0;
				String clasificacionEpo = cesionBean.clasificacionEpo(claveEpo);
				if(clasificacionEpo ==null){   clasificacionEpo=""; }
				java.util.HashMap campos_adicionales = cesionBean.getCamposAdicionalesParametrizados(claveEpo, "9");
				int indice	= Integer.parseInt((String)campos_adicionales.get("indice"));
				int columnas = 16;
				if(clasificacionEpo !=null ) columnas +=1;
				if(indice >0) columnas +=indice;
				HttpSession session = request.getSession();
				String tipoUsuario = (String)session.getAttribute("strTipoUsuario");
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				pdfDoc.addText("  Extinci�n de Contrato ","formas",ComunesPDF.CENTER);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				columnas =13-indiceCamposAdicionales;
				pdfDoc.setTable(13, 100);
				pdfDoc.setCell("A","fromas",ComunesPDF.CENTER);
				pdfDoc.setCell("Dependencia","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Intermediario Financiero (Cesionario)","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Pyme(Cedente)","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("RFC","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No. Proveedor","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Representante legal","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de solocitud Pyme","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No. Contrato ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto/Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de Contrataci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("F. Inicio Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("F. Final Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("B","fromas",ComunesPDF.CENTER);
				pdfDoc.setCell("Plaza del Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Ciudad de Firma del Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("F. L�mite para Notificaci�n","celda01",ComunesPDF.CENTER);
				if (lovDatos !=null ) {
					for (int i=0; i<lovDatos.size(); i++) { 
						Vector lovRegistro = (Vector) lovDatos.get(i);
						String lsNombreCampo 	= (String) lovRegistro.get(1);
						pdfDoc.setCell(lsNombreCampo,"celda01",ComunesPDF.CENTER);
					} 					
				}
				
				pdfDoc.setCell("Ventanilla de Pago ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Supervisor/Administrador/Residente de Obra","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tel�fono","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Objeto del Contrato","celda01",ComunesPDF.CENTER);
			   if(lovDatos.size()<5){
					for(int j = (lovDatos.size()+1); j <=5 ; j++){ pdfDoc.setCell("  ","celda01",ComunesPDF.CENTER);   }
				}
				
				pdfDoc.setCell("C","fromas",ComunesPDF.CENTER);
				pdfDoc.setCell("Comentarios","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto del Cr�dito","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Referencia/No. Cr�dito ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Vencimiento Cr�dito","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Banco de Deposito","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Cuenta","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Cuenta CLABE","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("F. Aut. � Rechazo PyME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("F. Aut. � Rechazo EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Causas de Rechazo Notificaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("  ","celda01",ComunesPDF.CENTER);
				while (reg.next()) {//clave_pyme
					StringBuffer montosPorMoneda = cesionBean.getMontoMoneda(reg.getString("clave_solicitud"));
					String noProveedor = (reg.getString("clave_pyme")==null)?"":reg.getString("clave_pyme");
					
					campo1 =  (reg.getString("campo_adicional_1")==null)?"":reg.getString("campo_adicional_1");
					campo2 =  (reg.getString("campo_adicional_2")==null)?"":reg.getString("campo_adicional_2");
					campo3 =  (reg.getString("campo_adicional_3")==null)?"":reg.getString("campo_adicional_3");
					campo4 =  (reg.getString("campo_adicional_4")==null)?"":reg.getString("campo_adicional_4");
					campo5 =  (reg.getString("campo_adicional_5")==null)?"":reg.getString("campo_adicional_5");
					StringBuffer nombreContacto = new StringBuffer();
					UtilUsr utilUsr = new UtilUsr();
					boolean usuarioEncontrado = false;
					boolean perfilIndicado = false;
					List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(noProveedor, "P");
					for (int y = 0; y < usuariosPorPerfil.size(); y++) {
						String loginUsuarioEpo = (String)usuariosPorPerfil.get(y);
						Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
						perfilIndicado = true;
						if(y>0){
							nombreContacto.append(" / ");
						}
						nombreContacto.append(usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno()+"  ");	
						nombreContacto.append("\n");
					}
					//FODEA-024-2014 MOD()
					String cedente = ((reg.getString("pyme")==null)?"":reg.getString("pyme"));
					String cedenteAux[] = cedente.split(";");
					if(cedenteAux.length == 1){
						cedente = cedente.replaceAll(";","");
						reg.setObject("PYME",cedente);	
					}else{
						cedente = cedente.replaceAll(";","\n\n");
						reg.setObject("PYME",cedente);	
					}
					//
					
					pdfDoc.setCell("A","fromas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("dependencia")==null)?"":reg.getString("dependencia"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("cesionario")==null)?"":reg.getString("cesionario"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cedente,"formas",ComunesPDF.CENTER);//FODEA-024-2014 MOD()
					pdfDoc.setCell((reg.getString("RFC")==null)?"":reg.getString("RFC"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("numero_proveedor")==null)?"":reg.getString("numero_proveedor"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(nombreContacto.toString().trim(),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("fecha_solicitud")==null)?"":reg.getString("fecha_solicitud"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("numero_contrato")==null)?"":reg.getString("numero_contrato"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(montosPorMoneda.toString().replace(',',' ').replaceAll("<br/>", "\n"),"formas",ComunesPDF.LEFT);
					pdfDoc.setCell((reg.getString("tipo_contratacion")==null)?"":reg.getString("tipo_contratacion"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("fecha_inicio_contrato")==null)?"":reg.getString("fecha_inicio_contrato"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("fecha_fin_contrato")==null)?"":reg.getString("fecha_fin_contrato"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("B","fromas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("plazo_contrato")==null)?"":reg.getString("plazo_contrato"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("clasificacion_epo")==null)?"":reg.getString("clasificacion_epo"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("fecha_limite_notificacion")==null)?"":reg.getString("fecha_limite_notificacion"),"formas",ComunesPDF.CENTER);
					if (lovDatos !=null ) {
						for (int i=0; i<lovDatos.size(); i++) { 
							
							if(i==0) pdfDoc.setCell(campo1,"formas",ComunesPDF.CENTER);
							if(i==1) pdfDoc.setCell(campo2,"formas",ComunesPDF.CENTER);
							if(i==2) pdfDoc.setCell(campo3,"formas",ComunesPDF.CENTER);
							if(i==3) pdfDoc.setCell(campo4,"formas",ComunesPDF.CENTER);
							if(i==4) pdfDoc.setCell(campo5,"formas",ComunesPDF.CENTER);
						}
						
					} 
					pdfDoc.setCell((reg.getString("ventanilla_pago")==null)?"":reg.getString("ventanilla_pago"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("sup_adm_resob")==null)?"":reg.getString("sup_adm_resob"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("numero_telefono")==null)?"":reg.getString("numero_telefono"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("objeto_contrato")==null)?"":reg.getString("objeto_contrato"),"formas",ComunesPDF.CENTER);
					
				   if(lovDatos.size()<5){
						for(int j = (lovDatos.size()+1); j <=5 ; j++){ pdfDoc.setCell("  ","formas",ComunesPDF.CENTER); }
				   }
					
					pdfDoc.setCell("C","fromas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("comentarios")==null)?"":reg.getString("comentarios"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal((reg.getString("monto_credito")==null)?"":reg.getString("monto_credito"),2),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("numero_referencia")==null)?"":reg.getString("numero_referencia"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("fecha_vencimiento")==null)?"":reg.getString("fecha_vencimiento"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("banco_deposito")==null)?"":reg.getString("banco_deposito"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("numero_cuenta")==null)?"":reg.getString("numero_cuenta"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("clabe")==null)?"":reg.getString("clabe"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("fecha_autrech_pyme")==null)?"":reg.getString("fecha_autrech_pyme"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("fecha_autrech_epo")==null)?"":reg.getString("fecha_autrech_epo"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("estatus")==null)?"":reg.getString("estatus"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("observ_rechazo")==null)?"":reg.getString("observ_rechazo"),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("    ","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("    ","columnas",ComunesPDF.CENTER, columnas);
				
				}
					pdfDoc.addTable();
					pdfDoc.endDocument();
				
			}

		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo PDF", e);
		}
		log.info("crearPageCustomFile(S)");
		return nombreArchivo;
	}
	/**
		* En este m�todo se debe realizar la implementaci�n de la generaci�n del archivo
		* con base en el resulset que se recibe como par�metro. El ResulSet es el resultado
		* de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
		* @param request HttpRequest empleado principalmente para obtener el objeto session
		* @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
		* @param path Ruta f�sica donde se generar� el archivo
		* @param tipo cadena que identifica el tipo o variante de archivo que va a generar.
		* @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		log.info("crearPageCustomFile(E)");
		StringBuffer linea = new StringBuffer(1024);
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		int columnasAd = 6;
		String dependencia="", cesionario="";
		try {
			CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
			int indiceCamposAdicionales =0;
			String clasificacionEpo ="";
			HashMap campos_adicionales = cesionBean.getCamposAdicionalesParametrizados(claveEpo, "9");
			int indice	= Integer.parseInt((String)campos_adicionales.get("indice"));
			String nombreClasificacionEpo = cesionBean.clasificacionEpo(claveEpo);
			Vector lovDatos = cesionBean.getCamposAdicionales(claveEpo, "5");
			contenidoArchivo.append("Notificaciones Nafin"+"\n");
			contenidoArchivo.append("    "+"\n");
			contenidoArchivo.append("Dependencia, Intermediario Financiero (Cesionario),Pyme(Cedente),R.F.C,No. Proveedor, Representante legal,Fecha de solocitud PyME, No. Contrato, Monto/Moneda,Tipo de Contrataci�n,F. Inicio Contrato,F. Final Contrato,Plazo del Contrato");
			contenidoArchivo.append(",Ciudad de Firma del Contrato, F. L�mite para Notificaci�n,");	
			if (lovDatos !=null ) {
				for (int i=0; i<lovDatos.size(); i++) { 
					Vector lovRegistro = (Vector) lovDatos.get(i);
					String lsNombreCampo 	= (String) lovRegistro.get(1);
					contenidoArchivo.append(lsNombreCampo+ ",");	
				} 
			}				
			contenidoArchivo.append(" Ventanilla de Pago, Supervisor/Administrador/Residente de Obra, Tel�fono, Objeto del Contrato, Comentarios, Monto del Cr�dito, Referencia/No. Cr�dito, Fecha Vencimiento Cr�dito, Banco de Deposito,Cuenta, Cuenta CLABE, F. Aut. � Rechazo PyME, F. Aut. � Rechazo EPO, Estatus, Causas de Rechazo Notificaci�n"+"\n");
		
			while (rs.next()) {
				StringBuffer montosPorMoneda = cesionBean.getMontoMoneda(rs.getString("clave_solicitud"));
				dependencia = (rs.getString("dependencia")==null)?" ":rs.getString("dependencia");
				cesionario = (rs.getString("cesionario")==null)?" ":rs.getString("cesionario");
				String rfc =(rs.getString("RFC")==null)?" ":rs.getString("RFC");
				String pyme = (rs.getString("pyme")==null)?" ":rs.getString("pyme");
					//FODEA-024-2014 MOD()
					String cedenteAux[] = pyme.split(";");
					if(cedenteAux.length == 1){
						pyme = pyme.replaceAll(";","");
					}else{
						
					}
					//				
				String noProveedor = (rs.getString("numero_proveedor")==null)?"":rs.getString("numero_proveedor").trim();
				String numeroProveedor = (rs.getString("clave_pyme")==null)?"":rs.getString("clave_pyme").trim();
				StringBuffer nombreContacto = new StringBuffer();//obteniendo el representante legal
				UtilUsr utilUsr = new UtilUsr();
				boolean usuarioEncontrado = false;
				boolean perfilIndicado = false;
				List usuariosPorPerfil = utilUsr.getUsuariosxAfiliado(numeroProveedor, "P");
				for (int i = 0; i < usuariosPorPerfil.size(); i++) {
					String loginUsuarioEpo = (String)usuariosPorPerfil.get(i);
					Usuario usuarioEpo = utilUsr.getUsuario(loginUsuarioEpo);
					perfilIndicado = true;
					if (i > 0) {
						nombreContacto.append(" / ");
					}
					nombreContacto.append(usuarioEpo.getNombre()+" "+usuarioEpo.getApellidoMaterno()+" "+usuarioEpo.getApellidoPaterno()+" ");
					//nombreContacto.append("\n");//FODEA-024-2014 MOD(se comento la linea)
				}
				String fechaSolicitud = (rs.getString("fecha_solicitud")==null)?" ":rs.getString("fecha_solicitud"); 
				
				String numero_contrato = (rs.getString("numero_contrato")==null)?" ":rs.getString("numero_contrato");
				String tipo_contratacion= (rs.getString("tipo_contratacion")==null)?" ":rs.getString("tipo_contratacion");
				String fechaInicon = (rs.getString("fecha_inicio_contrato")==null)?" ":rs.getString("fecha_inicio_contrato");
				String  fechaFinCon = (rs.getString("fecha_fin_contrato")==null)?" ":rs.getString("fecha_fin_contrato");
				String  plazo_contrato =(rs.getString("plazo_contrato")==null)?" ":rs.getString("plazo_contrato");
				String  clasificacion = (rs.getString("clasificacion_epo")==null)?" ":rs.getString("clasificacion_epo");
				String  fecha_limite_notificacion = (rs.getString("fecha_limite_notificacion")==null)?" ":rs.getString("fecha_limite_notificacion");
				if (lovDatos !=null ) {
					for (int i=0; i<lovDatos.size(); i++) { 
						Vector	lovRegistro = (Vector) lovDatos.get(i);
						int 	liNumCampo 	= Integer.parseInt((String)lovRegistro.get(0));			
						if (liNumCampo== 1) { 
								 campo1 = (rs.getString("campo_adicional_1")==null)?"":rs.getString("campo_adicional_1");
						} if (liNumCampo== 2) { 
							 campo2 = (rs.getString("campo_adicional_2")==null)?"":rs.getString("campo_adicional_2");
						} if (liNumCampo== 3) { 
							 campo3 = (rs.getString("campo_adicional_3")==null)?"":rs.getString("campo_adicional_3");
						} if (liNumCampo== 4) { 
							 campo4 = (rs.getString("campo_adicional_4")==null)?"":rs.getString("campo_adicional_4");
						} if (liNumCampo== 5) { 
							 campo5 = (rs.getString("campo_adicional_5")==null)?"":rs.getString("campo_adicional_5");
						} 		
					} 
				} 
				String ventanilla_pago = (rs.getString("ventanilla_pago")==null)?" ":rs.getString("ventanilla_pago");
				String sup_adm_resob = (rs.getString("sup_adm_resob")==null)?" ":rs.getString("sup_adm_resob");
				String numero_telefono = (rs.getString("numero_telefono")==null)?" ":rs.getString("numero_telefono");
				String objeto_contrato= (rs.getString("objeto_contrato")==null)?" ":rs.getString("objeto_contrato");
				String comentarios = (rs.getString("comentarios")==null)?" ":rs.getString("comentarios");
				String monto_credito = (rs.getString("monto_credito")==null)?" ":rs.getString("monto_credito");
				String numero_referencia = (rs.getString("numero_referencia")==null)?" ":rs.getString("numero_referencia");
				String fecha_vencimiento = (rs.getString("fecha_vencimiento")==null)?"":rs.getString("fecha_vencimiento");
				String banco_deposito = (rs.getString("banco_deposito")==null)?" ":rs.getString("banco_deposito");
				String numero_cuenta = (rs.getString("numero_cuenta")==null)?" ":rs.getString("numero_cuenta");
				String clabe = (rs.getString("clabe")==null)?" ":rs.getString("clabe");
				String fechaPyme = (rs.getString("fecha_autrech_pyme")==null)?"":rs.getString("fecha_autrech_pyme");
				String fecha_limite = (rs.getString("fecha_limite_notificacion")==null)?"":rs.getString("fecha_limite_notificacion");
				String fechaEpo = (rs.getString("fecha_autrech_epo")==null)?"":rs.getString("fecha_autrech_epo");
				String estatus = (rs.getString("estatus")==null)?"":rs.getString("estatus");
				String nombre_clasificaion_epo = (rs.getString("nombre_clasificaion_epo")==null)?" ":rs.getString("nombre_clasificaion_epo");
				String observ_rechazo = (rs.getString("observ_rechazo")==null)?"":rs.getString("observ_rechazo");
				contenidoArchivo.append(dependencia.replaceAll(",", "")+",");
				contenidoArchivo.append(cesionario.replaceAll(",", "")+",");
				contenidoArchivo.append(pyme.replaceAll(",", "")+",");
				contenidoArchivo.append(rfc.replaceAll(",", "")+",");
				contenidoArchivo.append(noProveedor.replaceAll(",", "")+",");
				contenidoArchivo.append(nombreContacto.toString().trim()+",");
				contenidoArchivo.append(fechaSolicitud.replaceAll(",", "")+",");
				contenidoArchivo.append(numero_contrato.replaceAll(",", "")+",");
				contenidoArchivo.append(montosPorMoneda.toString().replace(',',' ').replaceAll("<br/>", "|")+",");
				contenidoArchivo.append(tipo_contratacion.replaceAll(",", "")+",");
				contenidoArchivo.append(fechaInicon.replaceAll(",", "")+",");
				contenidoArchivo.append(fechaFinCon.replaceAll(",", "")+",");
				contenidoArchivo.append(plazo_contrato.replaceAll(",", "")+",");
				contenidoArchivo.append(clasificacion.replaceAll(",", "")+",");
				contenidoArchivo.append(fecha_limite.replaceAll(",", "")+",");
				if (lovDatos !=null ) {
						for (int i=0; i<lovDatos.size(); i++) { 
							
							if(i==0) contenidoArchivo.append(campo1.replaceAll(",", "")+",");
							if(i==1) contenidoArchivo.append(campo2.replaceAll(",", "")+",");
							if(i==2) contenidoArchivo.append(campo3.replaceAll(",", "")+",");
							if(i==3) contenidoArchivo.append(campo4.replaceAll(",", "")+",");
							if(i==4) contenidoArchivo.append(campo5.replaceAll(",", "")+",");
						}
						
					} 
				contenidoArchivo.append(ventanilla_pago.replaceAll(",", "")+",");
				contenidoArchivo.append(sup_adm_resob.replaceAll(",", "")+",");
				contenidoArchivo.append(numero_telefono.replaceAll(",", "")+",");
				contenidoArchivo.append(objeto_contrato.replaceAll(",", "")+",");
				contenidoArchivo.append(comentarios.replaceAll(",", "")+",");
				contenidoArchivo.append("$ " +Comunes.formatoDecimal(monto_credito,2).replaceAll(",", "")+",");
				contenidoArchivo.append(numero_referencia.replaceAll(",", "")+",");
				contenidoArchivo.append(fecha_vencimiento.replaceAll(",", "")+",");
				contenidoArchivo.append(banco_deposito.replaceAll(",", "")+",");
				contenidoArchivo.append(numero_cuenta.replaceAll(",", "")+",");
				contenidoArchivo.append(clabe.replaceAll(",", "")+",");
				contenidoArchivo.append(fechaPyme.replaceAll(",", "")+",");
				contenidoArchivo.append(fechaEpo.replaceAll(",", "")+",");
				contenidoArchivo.append(estatus.replaceAll(",", "")+",");
				contenidoArchivo.append(observ_rechazo.replaceAll(",", "")+"\n");
			}
			creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
			nombreArchivo = creaArchivo.getNombre();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo", e);
		}
		log.info("crearCustomFile (S)");
		return nombreArchivo;
	 }
	 /**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  this.paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}
	public void setLoginUsuario(String loginUsuario) {
		this.loginUsuario = loginUsuario;
	}
	public String getLoginUsuario() {
		return loginUsuario;
	}
	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}
	public String getClaveEpo() {
		return claveEpo;
	}
	public void setClaveIf(String claveIf) {
		this.claveIf = claveIf;
	}
	public String getClaveIf() {
		return claveIf;
	}
	public void setClavePyme(String clavePyme) {
		this.clavePyme = clavePyme;
	}
	public String getClavePyme() {
		return clavePyme;
	}
	public void setClaveEstatusSol(String claveEstatusSol) {
		this.claveEstatusSol = claveEstatusSol;
	}
	public String getClaveEstatusSol() {
		return claveEstatusSol;
	}
	public void setNumeroContrato(String numeroContrato) {
		this.numeroContrato = numeroContrato;
	}
	public String getNumeroContrato() {
		return numeroContrato;
	}
	public void setFechaVigenciaIni(String fechaVigenciaIni) {
		this.fechaVigenciaIni = fechaVigenciaIni;
	}
	public String getFechaVigenciaIni() {
		return fechaVigenciaIni;
	}
	public void setFechaVigenciaFin(String fechaVigenciaFin) {
		this.fechaVigenciaFin = fechaVigenciaFin;
	}
	public String getFechaVigenciaFin() {
		return fechaVigenciaFin;
	}
	public void setFechaSolicitudIni(String fechaSolicitudIni) {
		this.fechaSolicitudIni = fechaSolicitudIni;
	}
	public String getFechaSolicitudIni() {
		return fechaSolicitudIni;
	}
	public void setFechaSolicitudFin(String fechaSolicitudFin) {
		this.fechaSolicitudFin = fechaSolicitudFin;
	}
	public String getFechaSolicitudFin() {
		return fechaSolicitudFin;
	}
	public void setPlazoContrato(String plazoContrato) {
		this.plazoContrato = plazoContrato;
	}
	public String getPlazoContrato() {
		return plazoContrato;
	}
	public void setClaveTipoPlazo(String claveTipoPlazo) {
		this.claveTipoPlazo = claveTipoPlazo;
	}
	public String getClaveTipoPlazo() {
		return claveTipoPlazo;
	}
	public String getClaveContrat() {
		return claveContrat;
	}
	public void setClaveContrat(String claveTipoPlazo) {
		this.claveContrat = claveTipoPlazo;
	}
	public void setClaveMoneda(String claveMoneda) {this.claveMoneda = claveMoneda;}
	public String getClaveMoneda() {return this.claveMoneda;}
	public void setTipoDato(List tipoDato) {
		this.tipoDato = tipoDato;
	}
	public List getTipoDato() {
		return tipoDato;
	}
}

