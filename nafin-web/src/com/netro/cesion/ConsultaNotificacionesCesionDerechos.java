package com.netro.cesion;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;
//import com.netro.cesion.CesionEJB.*;
//import com.netro.cesion.CesionEJBBean.*;
//import com.netro.cesion.CesionEJBHome.*;

/**
 * Esta clase obtiene la consulta realizada por el m�todo getNotificacionesPyme de CesionEJBBean del FODEA 037 - 2009 Cesi�n de Derechos,
 * para la Migraci�n de la pantalla de Notificaciones a ExtJS del Fodea 017 -2011.
 * Esta clase obtiene datos de las notificaciones de cesi�n de derechos en base a
 * la pyme, epo, Nombre de Cesionario (IF), n�mero de contrato, moneda, estatus, tipo de contrataci�n,
 * fecha de vigencia y plazo del contrato.
 *
 * @author Andrea Isabel Luna Aguill�n
 */

public class ConsultaNotificacionesCesionDerechos implements IQueryGeneratorRegExtJS{
	public ConsultaNotificacionesCesionDerechos() {}
	
	private List conditions;
	StringBuffer strQuery;
	//Variable para enviar mensajes al log
	private static final Log log = ServiceLocator.getInstance().getLog(ConsultaNotificacionesCesionDerechos.class);
	
	//Condiciones de la consulta
	private String clavePyme;
	private String claveEPO;
	private String claveIF;
	private String claveEstatus;
	private String claveContrat;
	private String claveMoneda;
	private String clavePlazoContrato;
	private String plazoContrato;
	private String numeroContrato;
	private String fechaVigenciaDesde;
	private String fechaVigenciaHasta;
	
	private int numSolicitudes;
	
	public String getAggregateCalculationQuery(){
		return "";
	}
	public String getDocumentQuery(){
		conditions	= new ArrayList();
		strQuery		= new StringBuffer();
		return strQuery.toString();
	}
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions	=	new ArrayList();
		strQuery	=	new StringBuffer();
		return strQuery.toString();
	}
	
		public Registros getEmpresasRepresentadas(){
			log.info("getEmpresasRepresentadas(E) ::..");
		Registros reg = new Registros();
		AccesoDB  con= new AccesoDB();
		conditions	= new ArrayList();
		strQuery		= new StringBuffer();
		
		strQuery.append(
									" SELECT sol.ic_solicitud AS clave_solicitud, " +
									" cp.cg_razon_social || ';'|| sol.cg_empresas_representadas AS CEDENTE"+
									"   FROM cder_solicitud sol, " +
									"        comcat_epo epo, " +
									"        comcat_if cif, " +
									"			comcat_pyme cp, " +
									//"        comcat_moneda mon, " +
									"        cdercat_tipo_contratacion tcn, " +
									"        cdercat_clasificacion_epo cep, " +
									"        cdercat_estatus_solic sts, " +
									"        cdercat_tipo_plazo ctp " +
									"  WHERE sol.ic_epo = epo.ic_epo " +
									"    AND sol.ic_if = cif.ic_if " +
									"    AND sol.ic_pyme = cp.ic_pyme " + 
									"    AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion " +
									"    AND sol.ic_epo = cep.ic_epo " +
									"    AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo " +
									"    AND sol.ic_estatus_solic = sts.ic_estatus_solic " +
									"    AND cp.ic_pyme = ? " +
									"   AND sol.ic_estatus_solic IN (11, 12, 15, 16, 17) " +
									"   AND sol.ic_tipo_plazo = ctp.ic_tipo_plazo(+) "
								 );
		conditions.add(clavePyme);
		if(!claveContrat.equals("")){
			strQuery.append("   AND tcn.ic_tipo_contratacion = ? ");
			conditions.add(claveContrat);
		}
		if(!claveEstatus.equals("")){
			strQuery.append("   AND sts.ic_estatus_solic = ? ");
			conditions.add(claveEstatus);
		}
		if(!claveEPO.equals("")){
			strQuery.append("   AND epo.ic_epo = ? ");
			conditions.add(claveEPO);
		}
		if(!claveIF.equals("")){
			strQuery.append("   AND cif.ic_if = ? " );
			conditions.add(claveIF);
		}
		if(!fechaVigenciaDesde.equals("") && !fechaVigenciaHasta.equals("")){
			strQuery.append(" AND sol.df_fecha_vigencia_ini >= TO_DATE(?, 'dd/mm/yyyy')");
			strQuery.append(" AND sol.df_fecha_vigencia_fin <= TO_DATE(?, 'dd/mm/yyyy')");
			conditions.add(fechaVigenciaDesde);
			conditions.add(fechaVigenciaHasta);
		}
		if(!numeroContrato.equals("")){
			strQuery.append("   AND sol.cg_no_contrato = ? ");
			conditions.add(numeroContrato);
		}						
		if (claveMoneda != null && !"".equals(claveMoneda)) {
         if (claveMoneda.equals("0")) {
				strQuery.append(" AND sol.cs_multimoneda = ?");
            conditions.add("S");
         } else {
            strQuery.append(" AND sol.cs_multimoneda = ?");
            strQuery.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
            strQuery.append(" FROM cder_monto_x_solic mxs");
            strQuery.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
            strQuery.append(" AND mxs.ic_moneda = ?)");
            conditions.add("N");
            conditions.add(new Integer(claveMoneda));
         }
      }      
      if(!clavePlazoContrato.equals("")){
			strQuery.append(" AND sol.ic_tipo_plazo = ? ");
			conditions.add(new Integer(clavePlazoContrato));
      }
      if(!plazoContrato.equals("")){
         strQuery.append(" AND sol.ig_plazo = ? ");
         conditions.add(new Integer(plazoContrato));
      }
		strQuery.append(" ORDER BY sol.df_alta_solicitud DESC"); 
		
		try{
			con.conexionDB();
			reg=con.consultarDB(strQuery.toString(),conditions);	
		} catch (Exception e) {
			log.error("getEmpresasRepresentadas(Error) ::..");
			throw new AppException("Ocurrio un error ", e);
		} finally {
			if (con.hayConexionAbierta()) {
			  con.cierraConexionDB();
			}
			log.info("getMapaCesionario(S) ::..");
		}
		
		
		log.debug("..:: strQuery: "+strQuery.toString());
		log.debug("..:: conditions: "+conditions);
		
		log.info("getEmpresasRepresentadas(S) ::..");
		return reg;
	
	}
	
	public String getDocumentQueryFile(){
		conditions	= new ArrayList();
		strQuery		= new StringBuffer();
		
		strQuery.append(
									" SELECT sol.ic_solicitud AS clave_solicitud, " +
									"        sol.ic_epo AS clave_epo," +
									"        sol.ic_pyme AS clave_pyme," +
									"        sol.ic_if AS clave_if," +
									"			'' AS monto_moneda," +
									"        epo.cg_razon_social AS dependencia, " +
									"        cif.cg_razon_social AS cesionario, " +
									"        sol.cg_no_contrato AS numero_contrato, " +
									//"        sol.fn_monto_contrato AS monto_contrato, " +
									//"        mon.cd_nombre AS moneda, " +
									"        tcn.cg_nombre AS tipo_contratacion, " +
									"        TO_CHAR(sol.df_fecha_vigencia_ini, 'dd/mm/yyyy') AS inicio_contrato, " +
									"        TO_CHAR(sol.df_fecha_vigencia_fin, 'dd/mm/yyyy') AS fin_contrato, " +
									"        cep.cg_area AS clasificacion_epo, " +
									"        sol.cg_campo1 AS camp_adic_1, " +
									"        sol.cg_campo2 AS camp_adic_2, " +
									"        sol.cg_campo3 AS camp_adic_3, " +
									"        sol.cg_campo4 AS camp_adic_4, " +
									"        sol.cg_campo5 AS camp_adic_5, " +
									"        sol.cg_obj_contrato AS objeto_contrato, " +
									"        sol.cg_comentarios AS comentarios, " +
									"        sol.cg_cuenta AS cuenta, " +
									"        sol.cg_cuenta_clabe AS cuenta_clabe, " +
									"        sts.cg_descripcion AS estatus, " + 
                  "        ctp.cg_descripcion as desc_plazo, " + 
                  "        sol.ig_plazo as plazo, " +
                  "        sol.cg_observ_rechazo as observ_rechazo, " +
						" TO_CHAR(sol.df_firma_contrato, 'dd/mm/yyyy') AS firma_contrato"+
									"   FROM cder_solicitud sol, " +
									"        comcat_epo epo, " +
									"        comcat_if cif, " +
									"			comcat_pyme cp, " +
									//"        comcat_moneda mon, " +
									"        cdercat_tipo_contratacion tcn, " +
									"        cdercat_clasificacion_epo cep, " +
									"        cdercat_estatus_solic sts, " +
									"        cdercat_tipo_plazo ctp " +
									"  WHERE sol.ic_epo = epo.ic_epo " +
									"    AND sol.ic_if = cif.ic_if " +
									"    AND sol.ic_pyme = cp.ic_pyme " + 
									//"    AND sol.ic_moneda = mon.ic_moneda " +
									"    AND sol.ic_tipo_contratacion = tcn.ic_tipo_contratacion " +
									"    AND sol.ic_epo = cep.ic_epo " +
									"    AND sol.ic_clasificacion_epo = cep.ic_clasificacion_epo " +
									"    AND sol.ic_estatus_solic = sts.ic_estatus_solic " +
									"    AND cp.ic_pyme = ? " +
									"   AND sol.ic_estatus_solic IN (11, 12, 15, 16, 17) " +
									"   AND sol.ic_tipo_plazo = ctp.ic_tipo_plazo(+) "
								 );
							
		conditions.add(clavePyme);
								 
		if(!claveContrat.equals("")){
			strQuery.append("   AND tcn.ic_tipo_contratacion = ? ");
			conditions.add(claveContrat);
		}
		if(!claveEstatus.equals("")){
			strQuery.append("   AND sts.ic_estatus_solic = ? ");
			conditions.add(claveEstatus);
		}
		if(!claveEPO.equals("")){
			strQuery.append("   AND epo.ic_epo = ? ");
			conditions.add(claveEPO);
		}
		if(!claveIF.equals("")){
			strQuery.append("   AND cif.ic_if = ? " );
			conditions.add(claveIF);
		}
		if(!fechaVigenciaDesde.equals("") && !fechaVigenciaHasta.equals("")){
			strQuery.append(" AND sol.df_fecha_vigencia_ini >= TO_DATE(?, 'dd/mm/yyyy')");
			strQuery.append(" AND sol.df_fecha_vigencia_fin <= TO_DATE(?, 'dd/mm/yyyy')");
			conditions.add(fechaVigenciaDesde);
			conditions.add(fechaVigenciaHasta);
		}
		if(!numeroContrato.equals("")){
			strQuery.append("   AND sol.cg_no_contrato = ? ");
			conditions.add(numeroContrato);
		}						
		if (claveMoneda != null && !"".equals(claveMoneda)) {
         if (claveMoneda.equals("0")) {
				strQuery.append(" AND sol.cs_multimoneda = ?");
            conditions.add("S");
         } else {
            strQuery.append(" AND sol.cs_multimoneda = ?");
            strQuery.append(" AND EXISTS (SELECT mxs.ic_monto_x_solic");
            strQuery.append(" FROM cder_monto_x_solic mxs");
            strQuery.append(" WHERE mxs.ic_solicitud = sol.ic_solicitud");
            strQuery.append(" AND mxs.ic_moneda = ?)");
            conditions.add("N");
            conditions.add(new Integer(claveMoneda));
         }
      }      
      if(!clavePlazoContrato.equals("")){
			strQuery.append(" AND sol.ic_tipo_plazo = ? ");
			conditions.add(new Integer(clavePlazoContrato));
      }
      if(!plazoContrato.equals("")){
         strQuery.append(" AND sol.ig_plazo = ? ");
         conditions.add(new Integer(plazoContrato));
      }
		strQuery.append(" ORDER BY sol.df_alta_solicitud DESC"); 
				 
		log.debug("strQuery ::: "+strQuery);
		log.debug("conditions ::: "+conditions);		
		
		return strQuery.toString();
	}
	/**
		* En este m�todo se debe realizar la implementaci�n de la generaci�n del archivo
		* con base en el resulset que se recibe como par�metro. El ResulSet es el resultado
		* de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
		* @param request HttpRequest empleado principalmente para obtener el objeto session
		* @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
		* @param path Ruta f�sica donde se generar� el archivo
		* @param tipo cadena que identifica el tipo o variante de archivo que va a generar.
		* @return Cadena con la ruta del archivo generado
	 */
	 	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo)  {
		String nombreArchivo = "";
		String cedente="";//FODEA-024-2014 MOD()
		Registros reg = this.getEmpresasRepresentadas();//FODEA-024-2014 MOD()
		try {		
			CesionEJB cesionBean = ServiceLocator.getInstance().lookup("CesionEJB", CesionEJB.class);
			
			if("PDF".equals(tipo)) {

				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[]	=	{"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	=	new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual	=	fechaActual.substring(0,2);
				String mesActual	=	meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual	=	fechaActual.substring(6,10);
				String horaActual	=	new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					//FODEA-024-2014
					int numSolicitudes = 0;
					Registros reg2 = new Registros();
					
					
					while(reg.next()){
					this.numSolicitudes++;
						cedente=(reg.getString("CEDENTE") == null) ? "" : reg.getString("CEDENTE");
						String cedenteAux[] = cedente.split(";");
						if(cedenteAux.length == 1){
							cedente = cedente.replaceAll(";","");
						}else{
							cedente = cedente.replaceAll(";","\n");
						}
					}
					//
					
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				((this.numSolicitudes==1)?cedente:(String)session.getAttribute("strNombre")),//cedente,//(String)session.getAttribute("strNombre"),//
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),
				(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + "-----------------------------" +
				horaActual, "formas", ComunesPDF.RIGHT);
				pdfDoc.setTable(16, 100);
				pdfDoc.setCell("Dependencia","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Intermediario Financiero (Cesionario)","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No. Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Firma Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto/Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de Contrataci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("F.Inicio Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("F.Final Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo del Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Clasificacion EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Objeto del Contrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Comentarios","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Cuenta","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Cuenta CLABE","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Causa de Rechazo de Notificaci�n","celda01",ComunesPDF.CENTER);
				
				while (rs.next()){
					String DEPENDENCIA	=	(rs.getString("dependencia") == null) ? "" : rs.getString("dependencia");
					String CESIONARIO	=	(rs.getString("cesionario") == null) ? "" : rs.getString("cesionario");
					String NUMERO_CONTRATO	=	(rs.getString("numero_contrato") == null) ? "" : rs.getString("numero_contrato");
					StringBuffer montosMonedaSol = cesionBean.getMontoMoneda(rs.getString("CLAVE_SOLICITUD"));				
					String TIPO_CONTRATACION = (rs.getString("tipo_contratacion") == null) ? "" : rs.getString("tipo_contratacion");
					String INICIO_CONTRATO = (rs.getString("inicio_contrato") == null) ? "" : rs.getString("inicio_contrato");
					String FIN_CONTRATO = (rs.getString("fin_contrato") == null) ? "" : rs.getString("fin_contrato");
					String PLAZO = (rs.getString("plazo") == null) ? "" : rs.getString("plazo");
					String DESC_PLAZO = (rs.getString("desc_plazo") == null) ? "" : rs.getString("desc_plazo");
					String CLASIFICACION_EPO = (rs.getString("clasificacion_epo") == null) ? "" : rs.getString("clasificacion_epo");
					String OBJETO_CONTRATO = (rs.getString("objeto_contrato") == null) ? "" : rs.getString("objeto_contrato");
					String COMENTARIOS = (rs.getString("comentarios") == null) ? "" : rs.getString("comentarios");
					String CUENTA = (rs.getString("cuenta") == null) ? "" : rs.getString("cuenta");
					String CUENTA_CLABE = (rs.getString("cuenta_clabe") == null) ? "" : rs.getString("cuenta_clabe");
					String ESTATUS = (rs.getString("estatus") == null) ? "" : rs.getString("estatus");
					String OBSERV_RECHAZO = (rs.getString("observ_rechazo") == null) ? "N/A" : rs.getString("observ_rechazo");
					String firmaContrato = (rs.getString("firma_contrato") == null) ? "" : rs.getString("firma_contrato");
					
					
					pdfDoc.setCell(DEPENDENCIA,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(CESIONARIO,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(NUMERO_CONTRATO,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(firmaContrato,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(montosMonedaSol.toString().replaceAll("<br/>", "\n"),"formas",ComunesPDF.LEFT);
					pdfDoc.setCell(TIPO_CONTRATACION, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(INICIO_CONTRATO,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(FIN_CONTRATO,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(PLAZO + " " + DESC_PLAZO,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(CLASIFICACION_EPO,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(OBJETO_CONTRATO, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(COMENTARIOS,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(CUENTA,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(CUENTA_CLABE,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(ESTATUS,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(OBSERV_RECHAZO, "formas", ComunesPDF.CENTER);
				}		
				pdfDoc.addTable();
				pdfDoc.endDocument();
			}
			else if(tipo.equals("CSV")) {
			
				CreaArchivo creaArchivo = new CreaArchivo();
				StringBuffer contenidoArchivo = new StringBuffer();
			
				contenidoArchivo.append("Dependencia,");
				contenidoArchivo.append("Intermediario Financiero (Cesionario),");
				contenidoArchivo.append("No. Contrato,");
				contenidoArchivo.append("Firma Contrato,");
				contenidoArchivo.append("Monto / Moneda,");
				contenidoArchivo.append("Tipo de Contrataci�n,");
				contenidoArchivo.append("Fecha Inicio Contrato,");
				contenidoArchivo.append("Fecha Final Contrato,");
				contenidoArchivo.append("Plazo del Contrato,");
				contenidoArchivo.append("Clasificacion EPO,");
				contenidoArchivo.append("Objeto del Contrato,");
				contenidoArchivo.append("Comentarios,");
				contenidoArchivo.append("Cuenta,");
				contenidoArchivo.append("Cuenta CLABE,");
				contenidoArchivo.append("Estatus,");
				contenidoArchivo.append("Causa del Rechazo de Notificaci�n,");
				contenidoArchivo.append("\n");
								
				while (rs.next()) {
				String vacio =" ";
						
					String DEPENDENCIA	=	(rs.getString("dependencia") == null) ? "" : rs.getString("dependencia");
					String CESIONARIO	=	(rs.getString("cesionario") == null) ? "" : rs.getString("cesionario");
					String NUMERO_CONTRATO	=	(rs.getString("numero_contrato") == null) ? "" : rs.getString("numero_contrato");
					StringBuffer montosMonedaSol = cesionBean.getMontoMoneda(rs.getString("CLAVE_SOLICITUD"));
					String TIPO_CONTRATACION = (rs.getString("tipo_contratacion") == null) ? "" : rs.getString("tipo_contratacion");
					String INICIO_CONTRATO = (rs.getString("inicio_contrato") == null) ? "" : rs.getString("inicio_contrato");
					String FIN_CONTRATO = (rs.getString("fin_contrato") == null) ? "" : rs.getString("fin_contrato");
					String PLAZO = (rs.getString("plazo") == null) ? "" : rs.getString("plazo");
					String DESC_PLAZO = (rs.getString("desc_plazo") == null) ? "" : rs.getString("desc_plazo");
					String CLASIFICACION_EPO = (rs.getString("clasificacion_epo") == null) ? "" : rs.getString("clasificacion_epo");
					String OBJETO_CONTRATO = (rs.getString("objeto_contrato") == null) ? "" : rs.getString("objeto_contrato");
					String COMENTARIOS = (rs.getString("comentarios") == null) ? "" : rs.getString("comentarios");
					String CUENTA = (rs.getString("cuenta") == null) ? "" : rs.getString("cuenta");
					String CUENTA_CLABE = (rs.getString("cuenta_clabe") == null) ? "" : rs.getString("cuenta_clabe");
					String ESTATUS = (rs.getString("estatus") == null) ? "" : rs.getString("estatus");
					String OBSERV_RECHAZO = (rs.getString("observ_rechazo") == null) ? "N/A" : rs.getString("observ_rechazo");
					String firmaContrato = (rs.getString("firma_contrato") == null) ? "" : rs.getString("firma_contrato");
					
					contenidoArchivo.append(DEPENDENCIA.replaceAll(","," ") + ",");
					contenidoArchivo.append(CESIONARIO.replaceAll(","," ") + ",");
					contenidoArchivo.append(NUMERO_CONTRATO+ ",");
					contenidoArchivo.append(firmaContrato+ ",");
					contenidoArchivo.append(montosMonedaSol.toString().replaceAll("<br/>", "|").replaceAll(",", "")+ ",");					
					contenidoArchivo.append(TIPO_CONTRATACION+ ",");
					contenidoArchivo.append(INICIO_CONTRATO+ ",");
					contenidoArchivo.append(FIN_CONTRATO+",");
					contenidoArchivo.append(PLAZO+" "+DESC_PLAZO+ ",");										
					contenidoArchivo.append(CLASIFICACION_EPO.replaceAll(","," ") + ",");					
					contenidoArchivo.append(OBJETO_CONTRATO+ ",");
					contenidoArchivo.append(COMENTARIOS+ ",");
					contenidoArchivo.append("'"+CUENTA+ ",");
					contenidoArchivo.append("'"+CUENTA_CLABE+ ",");
					contenidoArchivo.append(ESTATUS+ ",");
					contenidoArchivo.append(OBSERV_RECHAZO+ ",");					
					contenidoArchivo.append(",\n");
				}
				rs.close();				
				creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
				nombreArchivo = creaArchivo.getNombre();
			}
		}catch (Throwable e) {
			throw new AppException("Error al generar el archivo",e);
		}finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
		return nombreArchivo;
	}
	/** En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va a generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}	
	public List getConditions(){
		return conditions;
	}
	public void setClavePyme(String clavePyme){
		this.clavePyme = clavePyme;
	}
	public String getClavePyme(){
		return clavePyme;
	}
	public void setClaveEPO(String claveEPO){
		this.claveEPO = claveEPO;
	}
	public String getClaveEPO(){
		return claveEPO;
	}
	public void setClaveIF(String claveIF){
		this.claveIF = claveIF;
	}
	public String getClaveIF(){
		return claveIF;
	}
	public void setClaveEstatus(String claveEstatus){
		this.claveEstatus = claveEstatus;
	}
	public String getClaveEstatus(){
		return claveEstatus;
	}
	public void setClaveContrat(String claveContrat){
		this.claveContrat = claveContrat;
	}
	public String getClaveContrat(){
		return claveContrat;
	}
	public void setClaveMoneda(String claveMoneda){
		this.claveMoneda = claveMoneda;
	}
	public String getClaveModena(){
		return claveMoneda;
	}
	public void setNumeroContrato(String numeroContrato){
		this.numeroContrato = numeroContrato;
	}
	public String getNumeroContrato(){
		return numeroContrato;
	}
	public void setClavePlazoContrato(String clavePlazoContrato){
		this.clavePlazoContrato = clavePlazoContrato;
	}
	public String getClavePlazoContrato(){
		return clavePlazoContrato;
	}
	public void setPlazoContrato(String plazoContrato){
		this.plazoContrato = plazoContrato;
	}
	public String getPlazoContrato(){
		return plazoContrato;
	}
	public void setFechaVigenciaDesde(String fechaVigenciaDesde){
		this.fechaVigenciaDesde = fechaVigenciaDesde;
	}
	public String getFechaVigenciaDesde(){
		return fechaVigenciaDesde;
	}
	public void setFechaVigenciaHasta(String fechaVigenciaHasta){
		this.fechaVigenciaHasta = fechaVigenciaHasta;
	}
	public String getFechaVigenciaHasta(){
		return fechaVigenciaHasta;
	}


	public void setNumSolicitudes(int numSolicitudes) {
		this.numSolicitudes = numSolicitudes;
	}


	public int getNumSolicitudes() {
		return numSolicitudes;
	}
}