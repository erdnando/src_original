package com.netro.log;

import java.io.IOException;
import java.io.PrintWriter;

import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Level;

import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;

/**
 * Permite Ajustar la configuracion del Log4J en el servidor.
 * Esto a trav�s del URL.
 * Por ejemplo:
 * http://xxx.xxxxxxx.xxx/xxxxx/SetupLog4JServlet?logger=com.netro.ejb.afiliacion&logLevel=DEBUG
 * @author Gilberto Aparicio
 */
public class SetupLog4JServlet extends HttpServlet {
	
	
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out.println("Servlet de configuracion de Log4J<br/>");
		
		String logLevel 					= request.getParameter("logLevel");
		String reloadPropertiesFile 	= request.getParameter("reloadPropertiesFile");
		String logger 						= request.getParameter("logger");

		if (logLevel != null) {
			this.setLogLevelConParametro(out, logLevel, logger);
		} else if (reloadPropertiesFile != null) {
			System.out.println("Tratando de recargar el archivo log4j2.xml<br/>");
			// Note by jshernandez (07/04/2015 04:51:08 p.m.): 
			// Be aware that the next class is not part of the public API so it 
			// may break with any log4j2 minor release.
		   LoggerContext loggerContext = (LoggerContext) LogManager.getContext(false);
		   loggerContext.reconfigure();
		} else {
			out.println("No se especifico ninguno de los parametros: logLevel=(ALL, TRACE, DEBUG, INFO, WARN, ERROR, FATAL ) o reloadPropertiesFile<br/>");
			out.println("Ej. de uso: http://xxx.xxxxxxxx.xxx/xxxx/SetupLog4JServlet?logLevel=ERROR&logger=com.netro.ejb.parametrizacion<br/>");
		}
	}

	/**
	 * Establece el nivel de Log de acuerdo al nivel especificado a traves
	 * del parametro logLevel
	 * @param logger Categoria a modificar el nivel, p.e. com.netro
	 * @param logLevel Nivel de log: 
	 *    ALL, TRACE, DEBUG, INFO, WARN, ERROR, FATAL, OFF
	 * @param out PrintWriter para imprimir mensages a la pagina
    *
	 * Nota by jshernandez (07/04/2015 04:17:26 p.m.):
	 * 
    * El archivo de configuraci�n: log4j2.xml debe encontrarse dentro del
    * classpath de la aplicacion para que sea encontrado. En caso de que no
	 * se encuentre se carga la configuraci�n b�sica, con nivel de 
	 * mensajes = ERROR.
	 * 
	 * Para una correcta inicializaci�n/deinicializaci�n y tambi�n para evitar FUGA DE MEMORIA
	 * EN TODA APLICACI�N WEB que use log4j2, SE DEBER� INCLUIR LA LIBRER�A log4j-web*.jar
	 * en su classpath:
	 * 
	 * 		Using Log4j 2 in Web Applications
	 * 
	 * 		You must take particular care when using Log4j or any other logging framework 
	 *       within a Java EE web application. It's important for logging resources to be 
	 *       properly cleaned up (database connections closed, files closed, etc.) when the 
	 *       container shuts down or the web application is undeployed. Because of the nature 
	 *       of class loaders within web applications, Log4j resources cannot be cleaned up 
	 *       through normal means. Log4j must be "started" when the web application deploys 
	 *       and "shutdown" when the web application undeploys. How this works varies 
	 *       depending on whether your application is a Servlet 3.0 or newer or 
	 *       Servlet 2.5 web application.
	 *       
	 *       http://logging.apache.org/log4j/2.x/manual/webapp.html
	 *       
	 * Note by jshernandez (08/04/2015 12:44:58 p.m.): 
	 * 
	 * Be aware that some classes are not part of the public Log4j2 API, so it 
	 * may break with any minor release.
	 *
	 */
	private void setLogLevelConParametro(PrintWriter out, String logLevel, String strLogger) {

		LoggerContext 				ctx 				= (LoggerContext) LogManager.getContext(false);
		Configuration 				config 			= ctx.getConfiguration();
	
		Level level = null;
		level 		= Level.toLevel(logLevel, null);
		
	   if( level == null || "off".equalsIgnoreCase(logLevel) ){ // No se permitir� apagar el registro de mensajes
			out.println("Par�metro logLevel  de nivel '" + logLevel + "' no soportado<br/>");
			return;
		}
				
		Map<String,LoggerConfig> loggers = config.getLoggers();
		
		// Imprimir informaci�n de depuraci�n con los loggers que existen actualmente
		/*
		Iterator it = config.getLoggers().entrySet().iterator();
		while (it.hasNext()) {
		   Map.Entry pair = (Map.Entry)it.next();
		   out.println(pair.getKey() + " = " + pair.getValue()+"<br>");
		}
		*/
		
	   LoggerConfig loggerConfig = null;
		// Si el Logger no existe, crearlo con el nivel y paquetes indicados
	   if( !loggers.containsKey(strLogger) ) {
		   
		   loggerConfig = new LoggerConfig(strLogger, level, true);
			config.addLogger(strLogger, loggerConfig);
				
		// El logger s� existe: modificar log level
		} else {
		
			loggerConfig = config.getLoggerConfig(strLogger); 	
			loggerConfig.setLevel(level);

		}
		
		// Actualizar configuracion de los loggers
		ctx.updateLoggers(config);
		
		// Enviar respuesta de operaci�n exitosa
		out.println("Nivel de Log ha sido establecido a: " + logLevel + "<br/>");
		 
		
	}

}