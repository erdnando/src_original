package com.netro.cotizador;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/*******************************************************************************************
 * Fodea XX-2014                                                                           *
 * Descripci�n: Migraci�n de la pantalla Parametrizar Tesorer�a-Cat�logo de Riesgo a ExtJS *
 *              a ExtJS. La clase tiene m�todos que implementan la paginaci�n para generar *
 *              el grid                                                                    *
 * Elabor�: Agust�n Bautista Ruiz                                                          *
 * Fecha: 17/09/2014                                                                       *
 *******************************************************************************************/
public class ConsCatalogoRiesgos implements IQueryGeneratorRegExtJS{

	private static final Log log = ServiceLocator.getInstance().getLog(ConsCatalogoRiesgos.class);
	private List   conditions;
	private String idTipo;
	private String tipoRiesgo;

	public ConsCatalogoRiesgos(){}

	/**
	 * Obtiene las llaves primarias
	 * @return sentencia sql
	 */
	public String getDocumentQuery(){
		log.info(" getDocumentQuery(E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();

		/***** Armo la condici�n where *****/
		if(!idTipo.equals("")){
			condicion.append(" WHERE IDTIPOINTERMEDIARIO = ? ");
			conditions.add(idTipo);
		}
		if(!tipoRiesgo.equals("")){
			condicion.append(" WHERE CG_TIPO = ? ");
			conditions.add(tipoRiesgo);
		}

		qrySentencia.append(
			"SELECT IDTIPOINTERMEDIARIO, 'ConsCatalogoRiesgos::getDocumentQuery' " +
			"FROM TIPOINTERMEDIARIO " +
			condicion +
			"ORDER BY CG_TIPO DESC, IDTIPOINTERMEDIARIO"
		);

		log.debug("Sentencia(getDocumentQuery): " + qrySentencia.toString());
		log.debug("Condiciones(getDocumentQuery): " + conditions.toString());
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();
	}

	/**
	 * Obtiene los totales
	 * @return sentencia sql
	 */
	public String getAggregateCalculationQuery(){
		log.info(" getAggregateCalculationQuery (E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();

		/***** Armo la condici�n where *****/
		if(!idTipo.equals("")){
			condicion.append(" WHERE IDTIPOINTERMEDIARIO = ? ");
			conditions.add(idTipo);
		}
		if(!tipoRiesgo.equals("")){
			condicion.append(" WHERE CG_TIPO = ? ");
			conditions.add(tipoRiesgo);
		}

		qrySentencia.append(
			"SELECT COUNT(IDTIPOINTERMEDIARIO) AS total FROM TIPOINTERMEDIARIO " + condicion
		);

		log.debug("Sentencia(getAggregateCalculationQuery): " + qrySentencia.toString());
		log.debug("Condiciones(getAggregateCalculationQuery): " + conditions.toString());
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();
	}

	/**
	 * Obtiene la lista de los Ids en turno para realizar la paginacion
	 * @return sentencia sql
	 * @param ids
	 */
	public String getDocumentSummaryQueryForIds(List ids){ 
		log.info(" getDocumentSummaryQueryForIds(E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();

		/***** Armo la condici�n where *****/
		condicion.append(" WHERE IDTIPOINTERMEDIARIO IN(");
			for (int i = 0; i < ids.size(); i++) { 
				List lItem = (ArrayList)ids.get(i);
				if(i > 0){
					condicion.append(",");
				}
				condicion.append(" ? " );
				conditions.add(new Long(lItem.get(0).toString()));
			}
		condicion.append(") ");

		qrySentencia.append(
			"SELECT IDTIPOINTERMEDIARIO AS ID_RIESGO, " +
			"DESCRIPCION, " +
			"CG_TIPO AS TIPO_STR, " +
			"CASE CG_TIPO WHEN 'A' THEN 'Acreditado' " +
			"ELSE 'Intermediario' END AS TIPO " +
			"FROM TIPOINTERMEDIARIO " +
			condicion +
			"ORDER BY CG_TIPO DESC, IDTIPOINTERMEDIARIO"
		);

		log.debug("Sentencia(getDocumentSummaryQueryForIds): " + qrySentencia.toString());
		log.debug("Condiciones(getDocumentSummaryQueryForIds): " + conditions.toString());
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();
	}

	/**
	 * Realiza la consulta para generar el archivo csv
	 */
	public String getDocumentQueryFile(){ return null; }

	/**
	 * Metodo para generar un archivo CSV, de todos los registros
	 * @return nombre del archivo
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){ return null; }
	
	/**
	 * Metodo para generar un archivo PDF, utilizando paginaci�n
	 * @return 
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){ return null; }

/************************************************************************
 *									GETTERS AND SETTERS									*
 ************************************************************************/
 	public List getConditions(){
		return conditions; 
	}

	public String getIdTipo() {
		return idTipo;
	}

	public void setIdTipo(String idTipo) {
		this.idTipo = idTipo;
	}

	public String getTipoRiesgo() {
		return tipoRiesgo;
	}

	public void setTipoRiesgo(String tipoRiesgo) {
		this.tipoRiesgo = tipoRiesgo;
	}

}