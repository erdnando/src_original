package com.netro.cotizador;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class consAdmin implements  IQueryGeneratorRegExtJS {
	public consAdmin() {  }
	private List 	conditions;
	private String 	paginaOffset;
	private String 	paginaNo;
	StringBuffer 	strQuery;
	StringBuffer qrySentencia = new StringBuffer("");
	private static final Log log = ServiceLocator.getInstance().getLog(consAdmin.class);//Variable para enviar mensajes al log.
	
	private String cveMoneda;
	
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
 	}  
		 
	public String getDocumentQuery(){
		conditions = new ArrayList();	
		strQuery 		= new StringBuffer(); 
		log.debug("getDocumentQuery)"+strQuery.toString()); 
		log.debug("getDocumentQuery)"+conditions);
		return strQuery.toString();
 	}  
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
		strQuery = new StringBuffer(); 
		log.debug("getDocumentSummaryQueryForIds "+strQuery.toString()); 
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
		return strQuery.toString();
 	} 
					
	public String getDocumentQueryFile(){
		conditions = new ArrayList();   
		strQuery 		= new StringBuffer(); 
		StringBuffer condicion    = new StringBuffer();
		log.info("getDocumentQueryFile(E)");
		
		strQuery.append("select idcuadro,titmenu FROM CUADROS where idmoneda = " +cveMoneda );
		
		log.debug("getDocumentQueryFile "+strQuery.toString());
		log.info("getDocumentQueryFile(S)");
		return strQuery.toString();
		
 	} 
	/**
	 * En este método se debe realizar la implementación de la generación de archivo
	 * con base en el resultset que se recibe como parámetro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generará el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		return "";			
	}
	/**
	 * En este método se debe realizar la implementación de la generación de archivo
	 * con base en el objeto Registros que recibe como parámetro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generará el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		
		return "";
	}
	/**
	 * Función para obtener los datos de la tabla CUADROS  
	 * @return 
	 */
	 public List datosCuadro(String idCuadro)throws Exception{
		log.info("datosCuadro (E)");   
		AccesoDB 	con = new AccesoDB();
		PreparedStatement ps = null;
		ResultSet rs = null;  
		boolean		commit = true;
		strQuery 		= new StringBuffer();
		List 	datos  = new ArrayList();
		int regverepo = 0;
		try{
			con.conexionDB();
			strQuery.append(
				"select * FROM CUADROS where idcuadro = " + idCuadro
			);
			log.info("strQuery ::: "+strQuery.toString());
			ps = con.queryPrecompilado(strQuery.toString());
			rs = ps.executeQuery();
			if(rs.next()){
				datos.add((rs.getString("IDCUADRO")==null)?"":rs.getString("IDCUADRO"));	
				datos.add((rs.getString("IDMONEDA")==null)?"":rs.getString("IDMONEDA"));	
				datos.add((rs.getString("TITMENU")==null)?"":rs.getString("TITMENU"));
			
				datos.add((rs.getString("TITREPORTE")==null)?"":rs.getString("TITREPORTE"));	
				datos.add((rs.getString("SUBTITREP")==null)?"":rs.getString("SUBTITREP"));	
				datos.add((rs.getString("TXTCAB")==null)?"":rs.getString("TXTCAB"));
			
				datos.add((rs.getString("TXTPIE")==null)?"":rs.getString("TXTPIE"));	
				datos.add((rs.getString("ARCHIVO")==null)?"":rs.getString("ARCHIVO"));
			}
				
			rs.close();
			ps.close();	
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			log.error("Error  "+e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("datosCuadro (S)");
		}	
		return datos;
 	}
	 public String modificaCuadro(String idCuadro,String moneda,String tituloMenu,String tituloReporte,String subReporte,String cabecera,String pie)throws Exception{
		log.info("modificaCuadro (E)");   
		AccesoDB 	con = new AccesoDB();
		PreparedStatement ps = null;
		boolean salida=true;
		ResultSet rs = null;  
		boolean		commit = true;
		strQuery 		= new StringBuffer();
		String resultado = "";
		int regverepo = 0;
		try{
			con.conexionDB();
			strQuery.append(
				"update CUADROS set idmoneda = " + moneda + ", titmenu = '" + tituloMenu + "', titreporte = '" + tituloReporte + "', subtitrep = '" + subReporte + "', txtcab = '" + cabecera + "', txtpie = '" + pie + "' where idcuadro = "+ idCuadro);
			log.info("strQuery ::: "+strQuery.toString());
			con.ejecutaSQL(strQuery.toString());
			con.cerrarDB(true);	
		}catch(Exception e){
			salida = false;
		}	
		if(salida)
			resultado= "<BR>Datos modificados";
		else
        resultado ="<BR>No se modificaron los datos";
		return strQuery.toString()+"<BR>Salida :"+salida+resultado;
 	}
	

	public List getConditions() {
		return conditions;
	}
	public String getPaginaNo() { return paginaNo; 	}
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}
	

	public String getCveMoneda() {
		return cveMoneda;
	}

	public void setCveMoneda(String cveMoneda) {
		this.cveMoneda = cveMoneda;
	}
	
}