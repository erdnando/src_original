package com.netro.cotizador;

import com.netro.pdf.ComunesPDF;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorPS;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsBitacoraCurvas implements IQueryGeneratorPS, IQueryGeneratorRegExtJS {

	private int numList = 1;

	public ConsBitacoraCurvas() {  }

	public int getNumList(HttpServletRequest request){ 
		return numList;
	}

	public ArrayList getConditions(HttpServletRequest request){
    ArrayList conditions = new ArrayList(); 
    int cond = 1;   
		
		String idCurva 		= (request.getParameter("idCurva")==null)?"":request.getParameter("idCurva").trim();
		String moneda 		= (request.getParameter("moneda")==null)?"":request.getParameter("moneda").trim();
		String fechaCurva	= (request.getParameter("fechaCurva")==null)?"":request.getParameter("fechaCurva").trim();
		String fechaCurvaA	= (request.getParameter("fechaCurvaA")==null)?"":request.getParameter("fechaCurvaA").trim();
		String tipo			= (request.getParameter("tipo")==null)?"":request.getParameter("tipo").trim();

		if(tipo.equals("")){
			cond=2;
		}else {
			cond=1;
		}
		
		for(int i = 0; i < cond ; i++){
	
			if(!idCurva.equals(""))
				conditions.add(idCurva);
				
			if(!moneda.equals(""))
				conditions.add(moneda);
				
			if(!fechaCurva.equals("")&&!fechaCurvaA.equals("")){
				conditions.add(fechaCurva);
				conditions.add(fechaCurvaA);
			}
			
			if(!tipo.equals(""))
				conditions.add(tipo);
				
		
		}//fin for
		
    return conditions;
  }
	
	public String getAggregateCalculationQuery(HttpServletRequest request) {
			return "select 1 from bit_alta_curva ";
	}
	
	public String getDocumentSummaryQueryForIds(HttpServletRequest request, Collection ids) {
		StringBuffer qrySentencia = new StringBuffer();
		String tipo			= (request.getParameter("tipo")==null)?"":request.getParameter("tipo").trim();
		int i=0;
    	this.numList = 1;
    	
    	if(tipo.equals("C")){
        	
		qrySentencia.append(
			" SELECT   bc.cg_tipo || bc.idcurva || TO_CHAR (bc.dc_alta, 'ddmmyyyyhhmiss') AS llave,"   +
			"          TO_CHAR (bc.dc_alta, 'dd/mm/yyyy') AS dc_alta,"   +
			"          TO_CHAR (bc.dc_alta, 'hh24:mi:ss') AS hora, c.nombre as nombre, ' ' AS archivo,"   +
			"          bc.cg_nombre_usuario, m.cd_nombre, bc.cg_tipo as tipo"   +
			"     FROM bit_alta_curva bc, curva c, comcat_moneda m"   +
			"    WHERE bc.idcurva = c.idcurva"   +
			"      AND c.ic_moneda = m.ic_moneda(+)"   +
			"      AND bc.cg_tipo ||bc.idcurva || TO_CHAR (bc.dc_alta, 'ddmmyyyyhhmiss') IN ( " );   
			
				for (Iterator it = ids.iterator(); it.hasNext(); i++) {
		       	if(i>0) {
							qrySentencia.append(",");
						}
					qrySentencia.append(" ? ");
					it.next();
				}
		    	qrySentencia.append(")");
		    	
		 } //fin-if C
		 
		 if(tipo.equals("")){
		 	this.numList = 2; 
		 	qrySentencia.append(" UNION"  );
		 }	
		  if(tipo.equals("T")){	
			
			qrySentencia.append(
			" 	SELECT bc.cg_tipo || bc.idcurva || TO_CHAR (bc.dc_alta, 'ddmmyyyyhhmiss') AS llave,"   +
			"         TO_CHAR (bc.dc_alta, 'dd/mm/yyyy') AS dc_alta,"   +
			"         TO_CHAR (bc.dc_alta, 'hh24:mi:ss') AS hora, c.titmenu as nombre, c.archivo as archivo,"   +
			"         bc.cg_nombre_usuario, m.cd_nombre ,bc.cg_tipo as tipo"   +
			"    FROM bit_alta_curva bc, cuadros c, comcat_moneda m"   +
			"   WHERE bc.idcurva = c.idcuadro"   +
			"     AND c.idmoneda = m.ic_moneda(+)"   +
			"     AND bc.cg_tipo ||bc.idcurva || TO_CHAR (bc.dc_alta, 'ddmmyyyyhhmiss') IN ( "  );
			
			

		i=0;
		for (Iterator it = ids.iterator(); it.hasNext(); i++) {
       	if(i>0) {
					qrySentencia.append(",");
				}
			qrySentencia.append(" ? ");
			it.next();
		}
    	qrySentencia.append(")");	
		}//finif T
		
		qrySentencia.append("ORDER BY bc.dc_alta DESC");
		System.out.println("ConsBitacoraCurvas:getDocumentSummaryQueryForIds(1): El query queda de la siguiente manera:\n"+qrySentencia.toString());
		return qrySentencia.toString();
	}
	
	public String getDocumentQuery(HttpServletRequest request) {
    	
    	StringBuffer qrySentencia = new StringBuffer();
    	String sCondicion = "";
    	String sCondicion2 = "";
    	
		String idCurva 		= (request.getParameter("idCurva")==null)?"":request.getParameter("idCurva").trim();
		String moneda 		= (request.getParameter("moneda")==null)?"":request.getParameter("moneda").trim();
		String fechaCurva	= (request.getParameter("fechaCurva")==null)?"":request.getParameter("fechaCurva").trim();
		String fechaCurvaA	= (request.getParameter("fechaCurvaA")==null)?"":request.getParameter("fechaCurvaA").trim();
		String tipo			= (request.getParameter("tipo")==null)?"":request.getParameter("tipo").trim();
		String nombre="";

		if(!idCurva.equals("")){
			sCondicion += " and bc.idCurva  = ?";

		}
			
		if(!moneda.equals("")){
		
			sCondicion += " AND c.ic_moneda= ?";		
		}
			
		if(!fechaCurva.equals("")&&!fechaCurvaA.equals("")){
			sCondicion += " and bc.dc_alta >= to_date(?,'DD/MM/YYYY') and bc.dc_alta < to_date(?,'DD/MM/YYYY')+ 1";			
		}
		if(!tipo.equals("")){
			sCondicion += " and bc.cg_tipo= ?";		
		}
		
		
		if(!idCurva.equals("")){
			sCondicion2 += " and bc.idCurva  = ?";

		}
			
		if(!moneda.equals("")){
		
			sCondicion2 += " and c.idmoneda= ?";		
		}
			
		if(!fechaCurva.equals("")&&!fechaCurvaA.equals("")){
			sCondicion2 += " and bc.dc_alta >= to_date(?,'DD/MM/YYYY') and bc.dc_alta < to_date(?,'DD/MM/YYYY')+ 1";			
		}
		
		if(!tipo.equals("")){
			sCondicion2 += " and bc.cg_tipo= ?";		
		}
		

	System.out.println("idCurva:\n"+idCurva);
	System.out.println("moneda"+moneda);
	System.out.println("fechaCurva:\n"+fechaCurva);
	System.out.println("fechaCurvaA\n"+fechaCurvaA);
	System.out.println("tipo"+tipo);
	
	
		try {

		if(tipo.equals("C")){
			qrySentencia.append(
				" SELECT  bc.cg_tipo ||bc.idcurva || TO_CHAR (bc.dc_alta, 'ddmmyyyyhhmiss')"   +
				"   FROM bit_alta_curva bc, curva c "   +
				"  WHERE bc.idcurva = c.idcurva "+ sCondicion );
				
		}	//fin if C
		
		if(tipo.equals("")){
		 	
		 	qrySentencia.append(" UNION"  );
		 }	
		
		if(tipo.equals("T")){
			qrySentencia.append(	
				" SELECT bc.cg_tipo || bc.idcurva || TO_CHAR (bc.dc_alta, 'ddmmyyyyhhmiss')"   +
				"    FROM bit_alta_curva bc, cuadros c "   +
				"   WHERE bc.idcurva = c.idcuadro "+ sCondicion2 );
		}	//fin if T
				
			System.out.println("getDocumentQuery:(2)EL query para las claves es:\n"+qrySentencia.toString());
		} catch(Exception e){
			System.out.println("ConsBitacoraCurvas::getDocumentQueryException "+e);
		}
		return qrySentencia.toString();
	}
	
	public String getDocumentQueryFile(HttpServletRequest request) {
    	StringBuffer qrySentencia = new StringBuffer();
    	String sCondicion = "";
    	String sCondicion2 = "";
    	
		String idCurva 		= (request.getParameter("idCurva")==null)?"":request.getParameter("idCurva").trim();
		String moneda 		= (request.getParameter("moneda")==null)?"":request.getParameter("moneda").trim();
		String fechaCurva	= (request.getParameter("fechaCurva")==null)?"":request.getParameter("fechaCurva").trim();
		String fechaCurvaA	= (request.getParameter("fechaCurvaA")==null)?"":request.getParameter("fechaCurvaA").trim();
		String tipo			= (request.getParameter("tipo")==null)?"":request.getParameter("tipo").trim();


		if(!idCurva.equals(""))
			sCondicion += " AND bc.idCurva  = ?";
			
		if(!moneda.equals(""))
			sCondicion += " AND c.ic_moneda= ?";
			
		if(!fechaCurva.equals("")&&!fechaCurvaA.equals("")){
			sCondicion += " AND bc.dc_alta >= to_date(?,'DD/MM/YYYY') and bc.dc_alta < to_date(?,'DD/MM/YYYY')+ 1 ";			
		}
		if(!tipo.equals("")){
			sCondicion += " and bc.cg_tipo= ?";		
		}
		
		if(!idCurva.equals("")){
			sCondicion2 += " and bc.idCurva  = ?";

		}
			
		if(!moneda.equals("")){
			sCondicion2 += " and c.idmoneda= ?";		
		}
			
		if(!fechaCurva.equals("")&&!fechaCurvaA.equals("")){
			sCondicion2 += " and bc.dc_alta >= to_date(?,'DD/MM/YYYY') and bc.dc_alta < to_date(?,'DD/MM/YYYY')+ 1";			
		}
		
		if(!tipo.equals("")){
			sCondicion2 += " and bc.cg_tipo= ?";		
		}


	System.out.println("idCurva qf:\n"+idCurva);
	System.out.println("moneda qff"+moneda);
	System.out.println("fechaCurva qf:\n"+fechaCurva);
	System.out.println("fechaCurvaA qf\n"+fechaCurvaA);
	System.out.println("tipo"+tipo);
	
		try {
			
		if(tipo.equals("C")){	
			qrySentencia.append(
			" SELECT TO_CHAR (bc.dc_alta, 'dd/mm/yyyy') AS dc_alta,"   +
			"        TO_CHAR (bc.dc_alta, 'hh24:mi:ss') AS hora, c.nombre as nombre, ' ' as archivo,"   +
			"        bc.cg_nombre_usuario, m.cd_nombre , bc.cg_tipo as tipo"   +
			"   FROM bit_alta_curva bc, curva c, comcat_moneda m"   +
			"  WHERE bc.idcurva = c.idcurva"   +
			"    AND c.ic_moneda = m.ic_moneda(+)"   + sCondicion+
			"ORDER BY bc.dc_alta DESC");
		}
	
		if(tipo.equals("T")){	
			qrySentencia.append(
			" SELECT TO_CHAR (bc.dc_alta, 'dd/mm/yyyy') AS dc_alta,"   +
			"        TO_CHAR (bc.dc_alta, 'hh24:mi:ss') AS hora, c.titmenu as nombre, c.archivo as archivo,"   +
			"        bc.cg_nombre_usuario, m.cd_nombre , bc.cg_tipo as tipo"   +
			"   FROM bit_alta_curva bc, cuadros c, comcat_moneda m"   +
			"  WHERE bc.idcurva = c.idcuadro"   +
			"    AND c.IDMONEDA = m.ic_moneda(+)"   + sCondicion2+
			"ORDER BY bc.dc_alta DESC");
		}	
																
			System.out.println("ConsBitacoraCurvas:getDocumentQueryFile:(3) EL query para las claves es:\n"+qrySentencia.toString());
		} catch(Exception e){
			System.out.println("ConsBitacoraCurvas::getDocumentQueryFile::getDocumentQueryException "+e);
		}
		return qrySentencia.toString();

	}
	
	//----------------------MIGRACION FODEA-XX-2014-----------------------------
	
	//Variable para enviar mensajes al log.
	private static final Log log = ServiceLocator.getInstance().getLog(ConsBitacoraCurvas.class);
	
	//Atributos
	private   List 	conditions;
	private String claveMoneda;
	private String claveCurva;
	private String fechaCargaMin; 
	private String fechaCargaMax;
	private String tipoBit;
	
	StringBuffer qrySentencia = new StringBuffer("");
	
	public String getDocumentQuery(){
		log.info("ConsBitacoraCurvas.getDocumentQuery (E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		if(tipoBit.equals("C")){
			qrySentencia.append(
				" SELECT  'C_'||bc.cg_tipo ||bc.idcurva || TO_CHAR (bc.dc_alta, 'ddmmyyyyhhmiss')"   +
				"   FROM bit_alta_curva bc, curva c "   +
				"  WHERE bc.idcurva = c.idcurva " );
				
		}	//fin if C
		
		if(tipoBit.equals("")){
		 	
		 	qrySentencia.append(" UNION"  );
		 }	
		
		if(tipoBit.equals("T")){
			qrySentencia.append(	
				" SELECT 'T_'||bc.cg_tipo || bc.idcurva || TO_CHAR (bc.dc_alta, 'ddmmyyyyhhmiss')"   +
				"    FROM bit_alta_curva bc, cuadros c "   +
				"   WHERE bc.idcurva = c.idcuadro ");
		}	//fin if T
		
		if(!claveCurva.equals("")){
			qrySentencia.append("	and bc.idCurva  = ?	");
			conditions.add(claveCurva);
		}
		if(!claveMoneda.equals("")){
			if(tipoBit.equals("C")){
				qrySentencia.append(" AND c.ic_moneda= ? ");
			}else if(tipoBit.equals("T")){
				qrySentencia.append(" AND c.idmoneda= ? ");
			}
			conditions.add(claveMoneda);
		}
		if(!fechaCargaMin.equals("")&&!fechaCargaMax.equals("")){
			qrySentencia.append(" and bc.dc_alta >= to_date(?,'DD/MM/YYYY') and bc.dc_alta < to_date(?,'DD/MM/YYYY')+ 1 ");			
			conditions.add(fechaCargaMin);
			conditions.add(fechaCargaMax);
		}
		if(!tipoBit.equals("")){
			qrySentencia.append(" and bc.cg_tipo= ? ");		
			conditions.add(tipoBit);
		}
		
		
		
		log.info("getDocumentQuery.qrySentencia  "+qrySentencia);
		log.info("getDocumentQuery.conditions "+conditions);
		log.info("getDocumentQuery(S)");
		
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		StringBuffer qrySentenciaC 	= new StringBuffer();
		StringBuffer qrySentenciaT 	= new StringBuffer();
		conditions 		= new ArrayList();
	
			qrySentenciaC.append(
				" SELECT   bc.cg_tipo || bc.idcurva || TO_CHAR (bc.dc_alta, 'ddmmyyyyhhmiss') AS llave,"   +
				"          TO_CHAR (bc.dc_alta, 'dd/mm/yyyy') AS dc_alta,"   +
				"          TO_CHAR (bc.dc_alta, 'hh24:mi:ss') AS hora, c.nombre as nombre, '' AS archivo,"   +
				"          bc.cg_nombre_usuario, m.cd_nombre, bc.cg_tipo as tipo, 'CURVA'AS BANDERA "   +
				"     FROM bit_alta_curva bc, curva c, comcat_moneda m"   +
				"    WHERE bc.idcurva = c.idcurva"   +
				"      AND c.ic_moneda = m.ic_moneda(+)"   +
				"      AND 'C_'||bc.cg_tipo ||bc.idcurva || TO_CHAR (bc.dc_alta, 'ddmmyyyyhhmiss') IN ( " );   
			
			qrySentenciaT.append(
			" 	SELECT bc.cg_tipo || bc.idcurva || TO_CHAR (bc.dc_alta, 'ddmmyyyyhhmiss') AS llave,"   +
			"         TO_CHAR (bc.dc_alta, 'dd/mm/yyyy') AS dc_alta,"   +
			"         TO_CHAR (bc.dc_alta, 'hh24:mi:ss') AS hora, c.titmenu as nombre, c.archivo as archivo,"   +
			"         bc.cg_nombre_usuario, m.cd_nombre ,bc.cg_tipo as tipo, 'TABLA'AS BANDERA "   +
			"    FROM bit_alta_curva bc, cuadros c, comcat_moneda m"   +
			"   WHERE bc.idcurva = c.idcuadro"   +
			"     AND c.idmoneda = m.ic_moneda(+)"   +
			"     AND 'T_'||bc.cg_tipo ||bc.idcurva || TO_CHAR (bc.dc_alta, 'ddmmyyyyhhmiss') IN ( "  );
			
			
	
			for(int i = 0; i < pageIds.size(); i++){
			List lItem = (ArrayList)pageIds.get(i);
			if(i>0){ 
				qrySentenciaT.append(","); 
				qrySentenciaC.append(","); 
			}
			qrySentenciaT.append("?");
			qrySentenciaC.append("?");
			conditions.add(lItem.get(0));
			
		}
			qrySentenciaT.append(")");	
			qrySentenciaC.append(")");	
		
		if(tipoBit.equals("C")){
			qrySentencia = qrySentenciaC;
		}else if(tipoBit.equals("T")){
			qrySentencia = qrySentenciaT;
		} else{
			qrySentencia.append(qrySentenciaC.toString()+"	UNION	"+qrySentenciaT.toString());
			conditions.addAll(conditions);
		}
		
		qrySentencia.append("ORDER BY dc_alta DESC");
		System.out.println("ConsBitacoraCurvas:getDocumentSummaryQueryForIds(1): El query queda de la siguiente manera:\n"+qrySentencia.toString());
		System.out.println("Coditions "+conditions);
		return qrySentencia.toString();
	}
			
	public String getDocumentQueryFile(){
		log.info("getDocumentQueryFile(E)");
	
		qrySentencia = new StringBuffer("");
		StringBuffer sqlSentenciaTipoCreditoC = new StringBuffer("");
		StringBuffer sqlSentenciaTipoCreditoD = new StringBuffer("");
		StringBuffer condicion  = new StringBuffer("");
		conditions 		= new ArrayList();
		
		
		if(tipoBit.equals("C")){	
			qrySentencia.append(
			" SELECT TO_CHAR (bc.dc_alta, 'dd/mm/yyyy') AS dc_alta,"   +
			"        TO_CHAR (bc.dc_alta, 'hh24:mi:ss') AS hora, c.nombre as nombre, ' ' as archivo,"   +
			"        bc.cg_nombre_usuario, m.cd_nombre , bc.cg_tipo as tipo"   +
			"   FROM bit_alta_curva bc, curva c, comcat_moneda m"   +
			"  WHERE bc.idcurva = c.idcurva"   +
			"    AND c.ic_moneda = m.ic_moneda(+)"   );
		}
		if(tipoBit.equals("T")){	
			qrySentencia.append(
			" SELECT TO_CHAR (bc.dc_alta, 'dd/mm/yyyy') AS dc_alta,"   +
			"        TO_CHAR (bc.dc_alta, 'hh24:mi:ss') AS hora, c.titmenu as nombre, c.archivo as archivo,"   +
			"        bc.cg_nombre_usuario, m.cd_nombre , bc.cg_tipo as tipo"   +
			"   FROM bit_alta_curva bc, cuadros c, comcat_moneda m"   +
			"  WHERE bc.idcurva = c.idcuadro"   +
			"    AND c.IDMONEDA = m.ic_moneda(+)"  );
		}	
		System.out.println("curvainjdnj**claveCurva**"+claveCurva);   
		if(!claveCurva.equals("")){
			qrySentencia.append("	and bc.idCurva  = ?	");
			conditions.add(claveCurva);
		}
		if(!claveMoneda.equals("")){
			if(tipoBit.equals("C")){
				qrySentencia.append(" AND c.ic_moneda= ? ");
			}else if(tipoBit.equals("T")){
				qrySentencia.append(" AND c.idmoneda= ? ");
			}
			conditions.add(claveMoneda);
		}
		if(!fechaCargaMin.equals("")&&!fechaCargaMax.equals("")){
			qrySentencia.append(" and bc.dc_alta >= to_date(?,'DD/MM/YYYY') and bc.dc_alta < to_date(?,'DD/MM/YYYY')+ 1 ");			
			conditions.add(fechaCargaMin);
			conditions.add(fechaCargaMax);
		}
		if(!tipoBit.equals("")){
			qrySentencia.append(" and bc.cg_tipo= ? ");		
			conditions.add(tipoBit);
		}
		qrySentencia.append(" ORDER BY bc.dc_alta DESC ");
		
		log.info("sqlSentencia getDocumentQueryFile "+qrySentencia);
		log.info("conditions  "+conditions);
		log.info("getDocumentQueryFile(S)");
		
		return qrySentencia.toString();
	}
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		log.info("crearPageCustomFile(E)");
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try{
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
			pdfDoc.setTable(8, 100);
			pdfDoc.setCell("Fecha","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Hora","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Curva/Tabla","celda01",ComunesPDF.CENTER,4);
			pdfDoc.setCell("Nombre del Usuaio","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
			
			while (rs.next())	{
				String	fecha= (rs.getString("dc_alta")==null)?"":rs.getString("dc_alta");
				String	hora 			= (rs.getString("hora")==null)?"":rs.getString("hora");
				String	nombre = (rs.getString("nombre")==null)?"":rs.getString("nombre");
				String	nombreUsuario       = (rs.getString("cg_nombre_usuario")==null)?"":rs.getString("cg_nombre_usuario");
				String	archivo 		= (rs.getString("archivo")==null)?"":rs.getString("archivo");
				String	moneda = (rs.getString("cd_nombre")==null)?"":rs.getString("cd_nombre");
						int indice1 = nombre.indexOf(">")+1;
						int indice2 = nombre.indexOf("</");
						int tamanyo = nombre.length();
						if(indice1>0)
							nombre = nombre.substring(indice1, indice2);
				pdfDoc.setCell(fecha,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(hora,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(nombre,"formas",ComunesPDF.CENTER,4);
				pdfDoc.setCell(nombreUsuario,"formas",ComunesPDF.LEFT);
				//pdfDoc.setCell(archivo,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(moneda ,"formas",ComunesPDF.CENTER);
			}
			pdfDoc.addTable();
			pdfDoc.endDocument();
		}catch(Exception e){
			throw new AppException("Error al generar el archivo ", e);
		}
		
		log.info("crearPageCustomFile(S)");
		return nombreArchivo;	
	}
	
		public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		log.debug("crearCustomFile (E)");
		
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		ComunesPDF pdfDoc = new ComunesPDF();
			
		try {
				contenidoArchivo.append("Fecha,Hora, Curva, Nombre del Usuario,   Moneda \n"	);						
			
			while (rs.next())	{
				String	fecha= (rs.getString("dc_alta")==null)?"":rs.getString("dc_alta");
				String	hora 			= (rs.getString("hora")==null)?"":rs.getString("hora");
				String	nombre = (rs.getString("nombre")==null)?"":rs.getString("nombre");
				String	nombreUsuario       = (rs.getString("cg_nombre_usuario")==null)?"":rs.getString("cg_nombre_usuario");
				String	archivo 		= (rs.getString("archivo")==null)?"":rs.getString("archivo");
				String	moneda = (rs.getString("cd_nombre")==null)?"":rs.getString("cd_nombre");
				
				int indice1 = nombre.indexOf(">")+1;
				int indice2 = nombre.indexOf("</");
				int tamanyo = nombre.length();
				if(indice1>0)
					nombre = nombre.substring(indice1, indice2);
			
				contenidoArchivo.append(fecha	+","+
												hora.replaceAll(",","")+","+
												nombre.replaceAll(",","")+","+
												nombreUsuario.replaceAll(",","")+","+
												//nombreArchivo+","+
												moneda+",\n");  
			}
			creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
			nombreArchivo = creaArchivo.getNombre();
		} catch(Exception e) {
			e.printStackTrace();
			
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
	
		public String getAggregateCalculationQuery(){
		log.info("getAggregateCalculationQuery(E)");
		
		qrySentencia = new StringBuffer("");
		StringBuffer sqlSentenciaTipoCreditoC = new StringBuffer("");
		StringBuffer sqlSentenciaTipoCreditoD = new StringBuffer("");
		conditions = new ArrayList();
		
		
			
		log.info("sqlSentencia getAggregateCalculationQuery "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
	
		return qrySentencia.toString();
	}
	
	public JSONArray getDataCurvaTabla(){
		log.info("getDataCurvaTabla (E) ");
		AccesoDB con = new AccesoDB(); 
		ResultSet rs = null;	
		HashMap datos =  new HashMap();
		Registros registros  = new Registros();
		qrySentencia = new StringBuffer("");
		conditions = new ArrayList();
		HashMap catCurvaTabla = new HashMap();
		JSONArray registrosTC= new JSONArray();
		try{
			con.conexionDB();
			if(tipoBit.equals("C")){
				qrySentencia.append("Select idcurva as \"clave\",nombre as \"descripcion\"  From curva  where ic_moneda= "+claveMoneda);
			}
			if(tipoBit.equals("T")){
				qrySentencia.append("Select idcuadro as \"clave\",titmenu as \"descripcion\"  From cuadros  where idmoneda= "+claveMoneda);
			}
			
			rs=con.queryDB(qrySentencia.toString());
			while(rs.next()) {
				catCurvaTabla = new HashMap();
				catCurvaTabla.put("clave",rs.getString("CLAVE"));
				catCurvaTabla.put("descripcion",rs.getString("DESCRIPCION"));
				registrosTC.add(catCurvaTabla);
			} 
			
		}catch(Exception e){
			log.error("getDataCurvaTabla  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("getDataCurvaTabla (S) ");
		return registrosTC;
	}
	
	public void setConditions(List conditions) {
		this.conditions = conditions;
	}

	public List getConditions() {
		return conditions;
	}


	public void setClaveMoneda(String claveMoneda) {
		this.claveMoneda = claveMoneda;
	}


	public String getClaveMoneda() {
		return claveMoneda;
	}


	public void setClaveCurva(String claveCurva) {
		this.claveCurva = claveCurva;
	}


	public String getClaveCurva() {
		return claveCurva;
	}


	public void setFechaCargaMin(String fechaCargaMin) {
		this.fechaCargaMin = fechaCargaMin;
	}


	public String getFechaCargaMin() {
		return fechaCargaMin;
	}


	public void setFechaCargaMax(String fechaCargaMax) {
		this.fechaCargaMax = fechaCargaMax;
	}


	public String getFechaCargaMax() {
		return fechaCargaMax;
	}

	public void setTipoBit(String tipoBit) {
		this.tipoBit = tipoBit;
	}


	public String getTipoBit() {
		return tipoBit;
	}
	public JSONArray getCatalogoMoneda() throws Exception{   
		AccesoDB con = new AccesoDB();
		ResultSet rs = null; 
		Registros registros  = new Registros();
		StringBuffer consulta = new StringBuffer();
		//String idioma = (sesIdiomaUsuario.equals("EN") && "EPO".equals(sesTipoUsuario))?"_ing":"";
		String idioma ="";
		JSONArray registrosM = new JSONArray();
		HashMap catMoneda = new HashMap();
		log.info("getCatalogoMoneda(E)");
		try{
			con.conexionDB();
			consulta.append( "select distinct  m.ic_moneda , "+      
					"  m.cd_nombre  " + idioma +
					" from COMCAT_MONEDA M, COMCAT_TASA T "+
					" where M.ic_moneda = T.ic_moneda "+
					" and T.cs_disponible = 'S' and M.ic_moneda = 1 "+   
					" union "+
					" select distinct  m.ic_moneda ,  m.cd_nombre "+ idioma +
					" from COMCAT_MONEDA M, COMCAT_TASA T, COM_TIPO_CAMBIO TC "+
					" where M.ic_moneda = T.ic_moneda AND T.cs_disponible = 'S' "+
					" and M.ic_moneda = TC.ic_moneda  ORDER BY 2");
			log.debug("Sentencia: " + consulta.toString());
			rs  = con.queryDB(consulta.toString());
			while(rs.next()) {
				catMoneda = new HashMap();
				catMoneda.put("clave",rs.getString("ic_moneda"));
				catMoneda.put("descripcion",rs.getString("cd_nombre"));   
				registrosM.add(catMoneda);
			}   
			
		} catch(Exception e){
			log.warn("getIntermediarioException " + e);
		} finally{
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}

		log.info("getIntermediario(S)");
		return registrosM;
	}

}