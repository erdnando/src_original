package com.netro.cotizador;

import com.netro.pdf.ComunesPDF;
import netropology.utilerias.*;
import org.apache.commons.logging.Log;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import netropology.utilerias.ServiceLocator;

public class ConsCotizacionIfArchivos implements IQueryGeneratorRegExtJS{

	private final static Log log = ServiceLocator.getInstance().getLog(ConsCotizacionIfArchivos.class);
	private List    conditions;
	private List    regDetalleDisp;
	private List    regPlanPagos;
	private List    datosPagos;
	private String  numSolicitud;
	private String  aviso1;
	private String  aviso2;
	private String  aviso3;
	private String  aviso4;
	private String  aviso5;
	private String  aviso6;
	private HashMap datosConsulta;

// Las siguientes variables son para generar el pdf del archivo 22forma07Bext.jsp
	private String generaReporteFinal; // S: Genera el PDF del archivo 22forma07Bext.jsp
	private HashMap datosArchivo;
	private HashMap gridDisposiciones;
	private HashMap gridPlanPagos;
	
	
	

	public ConsCotizacionIfArchivos(){}
	public String getDocumentQuery(){ return null;}
	public String getAggregateCalculationQuery(){ return null;}
	public String getDocumentSummaryQueryForIds(List ids){ return null;}
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){ return null;}

	public String getDocumentQueryFile(){
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();
		log.info("getDocumentQueryFile (E)");
		qrySentencia.append("SELECT TO_CHAR(SYSDATE,'dd/mm/rrrr') FROM DUAL");
		log.info("getDocumentQueryFile (S)");
		return qrySentencia.toString();
	}

	/**
	 * Genera el archivo PDF
	 * @return nombreArchivo
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){

		log.info("crearPageCustomFile (E)");
		String nombreArchivo = "";

		if(tipo.equals("PDF")){
			if(this.getGeneraReporteFinal().equals("S")){
				nombreArchivo = this.generaArchivoReporteFinal(request, path);
			} else{
				nombreArchivo = this.generaArchivoConsulta(request, path);
			}
		} else if(tipo.equals("CSV")){
			nombreArchivo = this.generaArchivoPagos(path);
		}
		log.info("crearPageCustomFile (S)");
		return nombreArchivo;
	}

	public String generaArchivoConsulta(HttpServletRequest request, String path){

		log.info("String generaArchivoConsulta (E)");
		StringBuffer avisosTasaIndicativa = new StringBuffer();
		HttpSession session  = request.getSession();
		CreaArchivo crea     = new CreaArchivo();
		String nombreArchivo = "";
		String estatus       = "";
		String tipoOper      = "";
		String strTipoUsuario= "";
		String tmp = "";

		try{
			ComunesPDF pdfDoc = new ComunesPDF();
			StringBuffer contenidoArchivo = new StringBuffer();
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);

			estatus  = (String)this.datosConsulta.get("estatus");
			tipoOper = (String)this.datosConsulta.get("cot_tipo_oper"); 

			String meses[]     = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual   = fechaActual.substring(0,2);
			String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual  = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
			((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
			(String)session.getAttribute("sesExterno"),
			(String)session.getAttribute("strNombre"),
			(String)session.getAttribute("strNombreUsuario"),
			(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

			pdfDoc.setTable(2,45);
			pdfDoc.setCell("Datos de la cotizaci�n",  "celda01", ComunesPDF.LEFT, 2);
			pdfDoc.setCell("N�mero de Solicitud:", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell((String)this.datosConsulta.get("cot_numero_solicitud"), "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("Fecha de Solicitud:",  "formas", ComunesPDF.LEFT);
			pdfDoc.setCell((String)this.datosConsulta.get("cot_fecha_solicitud"),  "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("Hora de Solicitud:",   "formas", ComunesPDF.LEFT);
			pdfDoc.setCell((String)this.datosConsulta.get("cot_hora_solicitud"),   "formas", ComunesPDF.LEFT);
				
			if(!estatus.equals("1") || !estatus.equals("3") || !estatus.equals("4")){
				if(estatus.equals("8")){
					pdfDoc.setCell("Fecha de Cancelaci�n:","formas", ComunesPDF.LEFT);
					pdfDoc.setCell((String)this.datosConsulta.get("cot_fecha_cotizacion"), "formas", ComunesPDF.LEFT);
					pdfDoc.setCell("Hora de Cancelaci�n:", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell((String)this.datosConsulta.get("cot_hora_cotizacion"),  "formas", ComunesPDF.LEFT);
				} else{
					pdfDoc.setCell("Fecha de Cotizaci�n:", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell((String)this.datosConsulta.get("cot_fecha_cotizacion"), "formas", ComunesPDF.LEFT);
					pdfDoc.setCell("Hora de Cotizaci�n:",  "formas", ComunesPDF.LEFT);
					pdfDoc.setCell((String)this.datosConsulta.get("cot_hora_cotizacion"),  "formas", ComunesPDF.LEFT);
				}
			}

			pdfDoc.setCell("N�mero y Nombre de Usuario:", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell((String)this.datosConsulta.get("cot_usuario"),          "formas", ComunesPDF.LEFT);
			
			if(estatus.equals("7") || estatus.equals("8") || estatus.equals("9")){
				pdfDoc.setCell("N�mero de Recibo:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)this.datosConsulta.get("cot_num_referencia"),   "formas", ComunesPDF.LEFT);
				pdfDoc.setCell(this.getAviso1(), "formas", ComunesPDF.LEFT, 2);
			}

			pdfDoc.addTable();

			pdfDoc.addText("\n", "formas", ComunesPDF.LEFT);

			pdfDoc.setTable(2,60); //OK
			pdfDoc.setCell("Datos del Ejecutivo Nafin", "celda01", ComunesPDF.LEFT, 2);
			pdfDoc.setCell("Nombre del Ejecutivo:", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell((String)this.datosConsulta.get("cot_nombre_ejecutivo"), "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("Correo electr�nico:", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell((String)this.datosConsulta.get("cot_email"), "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("Tel�fono:", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell((String)this.datosConsulta.get("cot_telefono"), "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("Fax:", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell((String)this.datosConsulta.get("cot_fax"), "formas", ComunesPDF.LEFT);
			pdfDoc.addTable();

			pdfDoc.setTable(2,60);
			pdfDoc.setCell("Datos del Intermediario Financiero / Acreditado", "celda01", ComunesPDF.LEFT, 2);
			pdfDoc.setCell("Tipo de Operaci�n:", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell((String)this.datosConsulta.get("cot_tipo_oper"), "formas", ComunesPDF.LEFT);
				
			if(!tipoOper.equals("Primer Piso")){
				pdfDoc.setCell("Nombre del Intermediario:", "formas", ComunesPDF.LEFT);
				if(!((String)this.datosConsulta.get("cot_intermediario_ci")).equals("")){
					pdfDoc.setCell((String)this.datosConsulta.get("cot_intermediario_ci"), "formas", ComunesPDF.LEFT);
				} else{
					pdfDoc.setCell((String)this.datosConsulta.get("cot_intermediario_cs"), "formas", ComunesPDF.LEFT);
				}
				if(!strTipoUsuario.equals("IF") && !((String)this.datosConsulta.get("cot_riesgo_inter")).equals("")){
					pdfDoc.setCell("Riesgo del Intermediario:", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell((String)this.datosConsulta.get("cot_riesgo_inter"), "formas", ComunesPDF.LEFT);
				}
			}

			pdfDoc.addTable();

			pdfDoc.setTable(2,60);
			pdfDoc.setCell("Caracter�sticas de la Operaci�n", "celda01", ComunesPDF.LEFT, 2);

			if(!strTipoUsuario.equals("IF") && !((String)this.datosConsulta.get("cot_acreditado")).equals("")){
				pdfDoc.setCell("Nombre del Acreditado:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)this.datosConsulta.get("cot_acreditado"), "formas", ComunesPDF.LEFT);
				if(tipoOper.equals("Primer Piso")){
					pdfDoc.setCell("Riesgo del Acreditado:", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell((String)this.datosConsulta.get("cot_riesgo_acred"), "formas", ComunesPDF.LEFT);
				}
			}

			if(!((String)this.datosConsulta.get("cot_programa")).equals("")){
				pdfDoc.setCell("Nombre del programa o Acreditado Final:", "formas", ComunesPDF.LEFT);
				pdfDoc.setCell((String)this.datosConsulta.get("cot_programa"), "formas", ComunesPDF.LEFT);
			}

			pdfDoc.setCell("Monto total requerido:", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell((String)this.datosConsulta.get("cot_monto_total"), "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("Tipo de tasa requerida:", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell((String)this.datosConsulta.get("cot_tipo_tasa"), "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("N�mero de Disposiciones:", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell((String)this.datosConsulta.get("cot_num_disposiciones"), "formas", ComunesPDF.LEFT);
				
			try{
				int numero = Integer.parseInt((String)this.datosConsulta.get("cot_num_disposiciones"));
				if(numero > 1){
					pdfDoc.setCell("Disposiciones m�ltiples tasas de inter�s:", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell((String)this.datosConsulta.get("cot_mult_disposiciones"), "formas", ComunesPDF.LEFT);
				}
			} catch(Exception e){
				log.warn("Error al convertir la cadena a n�mero." + e);
			}

			pdfDoc.setCell("Plazo de la operaci�n:", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell((String)this.datosConsulta.get("cot_plazo_oper"), "formas", ComunesPDF.LEFT);
			pdfDoc.setCell("Esquema de recuperaci�n del capital:", "formas", ComunesPDF.LEFT);
			pdfDoc.setCell((String)this.datosConsulta.get("cot_esquema_recuperacion"), "formas", ComunesPDF.LEFT);

			if(!((String)this.datosConsulta.get("cot_ic_esquema_recup")).equals("1")){
				if(((String)this.datosConsulta.get("cot_ic_esquema_recup")).equals("2") || ((String)this.datosConsulta.get("cot_ic_esquema_recup")).equals("3")){
					pdfDoc.setCell("Periodicidad del pago de inter�s:", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell((String)this.datosConsulta.get("cot_periodo_ppi"), "formas", ComunesPDF.LEFT);
				}
				if(!((String)this.datosConsulta.get("cot_ic_esquema_recup")).equals("3")){
					if(((String)this.datosConsulta.get("cot_ic_esquema_recup")).equals("2") || ((String)this.datosConsulta.get("cot_ic_esquema_recup")).equals("4")){
						pdfDoc.setCell("Periodicidad de Capital:", "formas", ComunesPDF.LEFT);
						pdfDoc.setCell((String)this.datosConsulta.get("cot_periodo_ppc"), "formas", ComunesPDF.LEFT);
					}
				}
				if(((String)this.datosConsulta.get("cot_ic_esquema_recup")).equals("2") && !((String)this.datosConsulta.get("cot_primer_fecha")).equals("")){
					pdfDoc.setCell("Primer fecha de Pago de Capital:", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell((String)this.datosConsulta.get("cot_primer_fecha"), "formas", ComunesPDF.LEFT);
					pdfDoc.setCell("Pen�ltima fecha de pago de Capital:", "formas", ComunesPDF.LEFT);
					pdfDoc.setCell((String)this.datosConsulta.get("cot_penultima_fecha"), "formas", ComunesPDF.LEFT);
				}
			}

			pdfDoc.addTable();

			/***** Se arma la Tabla Detalle Disposiciones *****/
			if(this.regDetalleDisp.size() > 0){
				pdfDoc.setTable(4, 60);
				pdfDoc.setCell("Detalle de Disposiciones", "celda01", ComunesPDF.LEFT, 4);
				pdfDoc.setCell("N�mero de Disposici�n",    "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Monto",                    "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Disposici�n",     "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Vencimiento",     "celda01", ComunesPDF.CENTER);

				HashMap mapaDetalleDisp = new HashMap();
				for(int i = 0; i < this.regDetalleDisp.size(); i++){
					mapaDetalleDisp = (HashMap)this.regDetalleDisp.get(i);
					pdfDoc.setCell((String)mapaDetalleDisp.get("NUM_DISPOSICION"),   "formas", ComunesPDF.CENTER);
					pdfDoc.setCell((String)mapaDetalleDisp.get("MONTO"),             "formas", ComunesPDF.CENTER);
					pdfDoc.setCell((String)mapaDetalleDisp.get("FECHA_DISPOSICION"), "formas", ComunesPDF.CENTER);
					pdfDoc.setCell((String)mapaDetalleDisp.get("FECHA_VENCIMIENTO"), "formas", ComunesPDF.CENTER);
				}
				pdfDoc.addTable();
			}

			/********** Se arma la Tabla Plan de Pagos Capital **********/
			if(this.regPlanPagos.size() > 0){
				pdfDoc.setTable(4, 60);
				pdfDoc.setCell("Plan de Pagos Capital", "celda01", ComunesPDF.CENTER, 4);
				pdfDoc.setCell("N�mero de Pago",        "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Pago",         "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Pagar",         "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("",                      "celda01", ComunesPDF.CENTER);

				HashMap mapaPlanPago = new HashMap();
				for(int i = 0; i < this.regPlanPagos.size(); i++){
					mapaPlanPago =(HashMap)this.regPlanPagos.get(i);
					pdfDoc.setCell((String)mapaPlanPago.get("NUM_PAGO"),    "formas", ComunesPDF.CENTER);
					pdfDoc.setCell((String)mapaPlanPago.get("FECHA_PAGO"),  "formas", ComunesPDF.CENTER);
					pdfDoc.setCell((String)mapaPlanPago.get("MONTO_PAGAR"), "formas", ComunesPDF.CENTER);
					pdfDoc.setCell("",                                      "formas", ComunesPDF.CENTER);
				}
				if(estatus.equals("2") || estatus.equals("5")){
					if(strTipoUsuario.equals("IF") && ((String)this.datosConsulta.get("cot_tipo_cotizacion")).equals("I")){
						pdfDoc.setCell(this.getAviso2(), "formas", ComunesPDF.LEFT, 4);
					} else if(strTipoUsuario.equals("IF") && ((String)this.datosConsulta.get("cot_tipo_cotizacion")).equals("N")){
						pdfDoc.setCell(this.getAviso3(), "formas", ComunesPDF.LEFT, 4);
					}
				}
				pdfDoc.addTable();
			}

			pdfDoc.setTable(2,60);
			pdfDoc.setCell("TASA INDICATIVA", "celda01", ComunesPDF.LEFT, 2);
			if(!((String)this.datosConsulta.get("cot_ig_tasa_md")).equals("")){
				pdfDoc.setCell("Tasa Indicativa con oferta mismo d�a:", "formas",  ComunesPDF.LEFT);
				tmp = (String)this.datosConsulta.get("cot_tasa_indicativa") + " " +  (String)this.datosConsulta.get("cot_ig_tasa_md");
				pdfDoc.setCell(tmp, "formas",  ComunesPDF.LEFT);
				avisosTasaIndicativa.append((String)this.getAviso4());

				if(!((String)this.datosConsulta.get("cot_ig_tasa_spot")).equals("")){
					pdfDoc.setCell("Tasa Indicativa con oferta 48 horas:", "formas",  ComunesPDF.LEFT);
					tmp = (String)this.datosConsulta.get("cot_tasa_indicativa") + " " +  (String)this.datosConsulta.get("cot_ig_tasa_spot");
					pdfDoc.setCell(tmp, "formas",  ComunesPDF.LEFT);
					avisosTasaIndicativa.append((String)this.getAviso5() + "\n");
				}
				avisosTasaIndicativa.append((String)this.getAviso6());
				pdfDoc.setCell(avisosTasaIndicativa.toString(), "formas",  ComunesPDF.LEFT, 2);
			}

			if(estatus.equals("7") || estatus.equals("8") || estatus.equals("9")){
				pdfDoc.setCell("Tasa confirmada:",                               "formas",  ComunesPDF.LEFT);
				if(((String)this.datosConsulta.get("cot_aceptado_md")).equals("S")){
					tmp = (String)this.datosConsulta.get("cot_tasa_indicativa") + " " +  (String)this.datosConsulta.get("cot_ig_tasa_md");
				} else{
					tmp = (String)this.datosConsulta.get("cot_tasa_indicativa") + " " +  (String)this.datosConsulta.get("cot_ig_tasa_spot");
				}
				pdfDoc.setCell(tmp, "formas", ComunesPDF.LEFT);
				tmp = "";
				if(estatus.equals("8")){
					pdfDoc.setCell("Fecha y Hora de Cancelaci�n", "formas",  ComunesPDF.LEFT);
					tmp = (String)this.datosConsulta.get("cot_fecha_cotizacion ") + " " +  (String)this.datosConsulta.get("cot_hora_cotizacion");
					pdfDoc.setCell(tmp, "formas", ComunesPDF.LEFT);
				} else{
					pdfDoc.setCell("Fecha y Hora de Confirmaci�n", "formas",  ComunesPDF.LEFT);
					tmp = (String)this.datosConsulta.get("cot_df_Aceptacion ");
					pdfDoc.setCell(tmp, "formas", ComunesPDF.LEFT);
				}
			}
			pdfDoc.addTable();

			if(!((String)this.datosConsulta.get("cot_observaciones")).equals("")){
				pdfDoc.setTable(1,60);
				pdfDoc.setCell("Observaciones", "celda01",  ComunesPDF.LEFT);
				pdfDoc.setCell((String)this.datosConsulta.get("cot_observaciones"), "formas", ComunesPDF.LEFT);
				pdfDoc.addTable();
			}

			pdfDoc.endDocument();

		} catch(Throwable e){
			throw new AppException("Error al generar el archivo PDF. ", e);
		}

		log.info("generaArchivoConsulta (S)");
		return nombreArchivo;

	}

	public String generaArchivoReporteFinal(HttpServletRequest request, String path){
		
		log.info("generaArchivoReporteFinal (E)");
		HttpSession session  = request.getSession();
		CreaArchivo crea     = new CreaArchivo();
		String nombreArchivo = "";
		String tmp = "";

		try{
			ComunesPDF pdfDoc = new ComunesPDF();
			StringBuffer contenidoArchivo = new StringBuffer();
			nombreArchivo = (String)this.datosConsulta.get("numero_solicitud") + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);

			String meses[]     = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual   = fechaActual.substring(0,2);
			String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual  = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
			((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
			(String)session.getAttribute("sesExterno"),
			(String)session.getAttribute("strNombre"),
			(String)session.getAttribute("strNombreUsuario"),
			(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);


			pdfDoc.setTable(2,60);
			pdfDoc.setCell("Cotizador de Cr�ditos",                                "celda01", ComunesPDF.LEFT, 2);
			pdfDoc.setCell("Intermediario / Acreditado:",                          "formas",  ComunesPDF.LEFT   );
			pdfDoc.setCell((String)this.datosConsulta.get("cg_acreditado"),        "formas",  ComunesPDF.LEFT   );
			pdfDoc.setCell("Datos del Ejecutivo Nafin",                            "celda01", ComunesPDF.LEFT, 2);
			pdfDoc.setCell("Nombre del Ejecutivo:",                                "formas",  ComunesPDF.LEFT   );
			pdfDoc.setCell((String)this.datosConsulta.get("nombre_ejecutivo"),     "formas",  ComunesPDF.LEFT   );
			pdfDoc.setCell("Caracter�sticas de la Operaci�n ",                     "celda01", ComunesPDF.LEFT, 2);

			tmp = (String)this.datosConsulta.get("cg_programa");
			if(!tmp.equals("")){
				pdfDoc.setCell("Nombre del programa o acreditado Final:",           "formas",  ComunesPDF.LEFT   );
				pdfDoc.setCell((String)this.datosConsulta.get("cg_programa"),       "formas",  ComunesPDF.LEFT   );
			}

			pdfDoc.setCell("Moneda:",                                              "formas",  ComunesPDF.LEFT   );
			pdfDoc.setCell((String)this.datosConsulta.get("nombre_moneda"),        "formas",  ComunesPDF.LEFT   );
			pdfDoc.setCell("Monto total requerido:",                               "formas",  ComunesPDF.LEFT   );
			pdfDoc.setCell((String)this.datosConsulta.get("fn_monto"),             "formas",  ComunesPDF.LEFT   );
			pdfDoc.setCell("Tipo de tasa requerida:",                              "formas",  ComunesPDF.LEFT   );
			pdfDoc.setCell((String)this.datosConsulta.get("ic_tipo_tasa"),         "formas",  ComunesPDF.LEFT   );
			pdfDoc.setCell("N�mero de Disposiciones:",                             "formas",  ComunesPDF.LEFT   );
			pdfDoc.setCell((String)this.datosConsulta.get("in_num_disp"),          "formas",  ComunesPDF.LEFT   );

			tmp = (String)this.datosConsulta.get("in_num_disp");
			if(Integer.parseInt(tmp) > 1){
				pdfDoc.setCell("Disposiciones m�ltiples. Tasa de Inter�s:",         "formas",  ComunesPDF.LEFT   );
				pdfDoc.setCell((String)this.datosConsulta.get("tasa_unica"),        "formas",  ComunesPDF.LEFT   );
			}

			pdfDoc.setCell("Plazo de la operaci�n:",                               "formas",  ComunesPDF.LEFT   );
			pdfDoc.setCell((String)this.datosConsulta.get("ig_plazo"),             "formas",  ComunesPDF.LEFT   );
			pdfDoc.setCell("Esquema de recuperaci�n del capital:",                 "formas",  ComunesPDF.LEFT   );
			pdfDoc.setCell((String)this.datosConsulta.get("esquema_recup"),        "formas",  ComunesPDF.LEFT   );

			tmp = (String)this.datosConsulta.get("ic_esquema_recup");
			int esquemaRec = Integer.parseInt(tmp);

			if(esquemaRec != 1){
				if(esquemaRec == 2 || esquemaRec == 3){
					pdfDoc.setCell("Periodicidad del pago de Inter�s:",                       "formas",  ComunesPDF.LEFT   );
					pdfDoc.setCell((String)this.datosConsulta.get("periodo_tasa_interes"),    "formas",  ComunesPDF.LEFT   );
				}
				if(esquemaRec != 3){
					if(esquemaRec == 2 || esquemaRec == 4){
						pdfDoc.setCell("Periodicidad de Capital:",                             "formas",  ComunesPDF.LEFT   );
						pdfDoc.setCell((String)this.datosConsulta.get("periodo_tasa_capital"), "formas",  ComunesPDF.LEFT   );
					}
					tmp = (String)this.datosConsulta.get("pfpc");
					if(esquemaRec == 2 && !tmp.equals("")){
						pdfDoc.setCell("Primer fecha de pago de Capital:",                     "formas",  ComunesPDF.LEFT   );
						pdfDoc.setCell((String)this.datosConsulta.get("pfpc"),                 "formas",  ComunesPDF.LEFT   );
						pdfDoc.setCell("Pen�ltima fecha de pago de Capital:",                  "formas",  ComunesPDF.LEFT   );
						pdfDoc.setCell((String)this.datosConsulta.get("pufpc"),                "formas",  ComunesPDF.LEFT   );
					}
				}
			}

			pdfDoc.addTable();

			/***** Se arma la Tabla Detalle Disposiciones *****/
			if(this.regDetalleDisp.size() > 0){
				pdfDoc.setTable(4,60);
				pdfDoc.setCell("Detalle Disposiciones",                             "celda01", ComunesPDF.LEFT, 4);
				pdfDoc.setCell("N�mero de Disposici�n",                             "celda01", ComunesPDF.CENTER );
				pdfDoc.setCell("Monto",                                             "celda01", ComunesPDF.CENTER );
				pdfDoc.setCell("Fecha de Disposici�n",                              "celda01", ComunesPDF.CENTER );
				pdfDoc.setCell("Fecha de Vencimiento",                              "celda01", ComunesPDF.CENTER );

				HashMap mapaDetalleDisp = new HashMap();
				for(int i = 0; i < this.regDetalleDisp.size(); i++){
					mapaDetalleDisp = (HashMap)this.regDetalleDisp.get(i);
					pdfDoc.setCell((String)mapaDetalleDisp.get("NUM_DISPOSICION"),   "formas",  ComunesPDF.CENTER );
					pdfDoc.setCell((String)mapaDetalleDisp.get("MONTO"),             "formas",  ComunesPDF.CENTER );
					pdfDoc.setCell((String)mapaDetalleDisp.get("FECHA_DISPOSICION"), "formas",  ComunesPDF.CENTER );
					pdfDoc.setCell((String)mapaDetalleDisp.get("FECHA_VENCIMIENTO"), "formas",  ComunesPDF.CENTER );
				}
				pdfDoc.addTable();
			}

			/********** Se arma la Tabla Plan de Pagos Capital **********/
			if(this.regPlanPagos.size() > 0){
				pdfDoc.setTable(4,60);
				pdfDoc.setCell("Plan de Pagos Capital",                             "celda01", ComunesPDF.LEFT, 4);
				pdfDoc.setCell("N�mero de Pago",                                    "celda01", ComunesPDF.CENTER );
				pdfDoc.setCell("Fecha de Pago",                                     "celda01", ComunesPDF.CENTER );
				pdfDoc.setCell("Monto a Pagar",                                     "celda01", ComunesPDF.CENTER );
				pdfDoc.setCell(" ",                                                 "celda01", ComunesPDF.CENTER );
				HashMap mapaPlanPago = new HashMap();
				for(int i = 0; i < this.regPlanPagos.size(); i++){
					mapaPlanPago =(HashMap)this.regPlanPagos.get(i);
					pdfDoc.setCell((String)mapaPlanPago.get("NUM_PAGO"),             "formas",  ComunesPDF.CENTER );
					pdfDoc.setCell((String)mapaPlanPago.get("FECHA_PAGO"),           "formas",  ComunesPDF.CENTER );
					pdfDoc.setCell((String)mapaPlanPago.get("MONTO_PAGAR"),          "formas",  ComunesPDF.CENTER );
					pdfDoc.setCell("",                                               "formas",  ComunesPDF.CENTER );
				}
				pdfDoc.addTable();
			}

			pdfDoc.setTable(3,60);
			pdfDoc.setCell("Datos exclusivos de Tesorer�a",                        "celda01", ComunesPDF.LEFT, 3);
			pdfDoc.setCell("",                                                     "formas",  ComunesPDF.CENTER );
			pdfDoc.setCell("D�as:",                                                "celda01", ComunesPDF.CENTER );
			pdfDoc.setCell("A�os:",                                                "celda01", ComunesPDF.CENTER );
			pdfDoc.setCell("Plazo promedio de vida:",                              "formas",  ComunesPDF.LEFT   );
			pdfDoc.setCell((String)this.datosConsulta.get("ppv"),                  "formas",  ComunesPDF.CENTER );
			pdfDoc.setCell((String)this.datosConsulta.get("ppv_360"),              "formas",  ComunesPDF.CENTER );
			pdfDoc.setCell("Duraci�n:",                                            "formas",  ComunesPDF.LEFT   );
			pdfDoc.setCell((String)this.datosConsulta.get("duracion"),             "formas",  ComunesPDF.CENTER );
			pdfDoc.setCell((String)this.datosConsulta.get("duracion_360"),         "formas",  ComunesPDF.CENTER );
			pdfDoc.addTable();

			pdfDoc.setTable(2,60);
			pdfDoc.setCell("\n",                                                     "formas",  ComunesPDF.LEFT, 2);
			pdfDoc.setCell("Costo de Fondeo sin Acarreo:",                         "formas",  ComunesPDF.LEFT   );
			pdfDoc.setCell((String)this.datosConsulta.get("tre"),                  "formas",  ComunesPDF.LEFT   );
			pdfDoc.setCell("Costo de Fondeo con Acarreo:",                         "formas",  ComunesPDF.LEFT   );
			pdfDoc.setCell((String)this.datosConsulta.get("tre_con_acarreo"),      "formas",  ComunesPDF.LEFT   );
			pdfDoc.setCell("Impuesto:",                                            "formas",  ComunesPDF.LEFT   );
			pdfDoc.setCell((String)this.datosConsulta.get("impuesto_usd"),         "formas",  ComunesPDF.LEFT   );
			pdfDoc.setCell("Costo All-in:",                                        "formas",  ComunesPDF.LEFT   );
			pdfDoc.setCell((String)this.datosConsulta.get("costo_all_in"),         "formas",  ComunesPDF.LEFT   );
			pdfDoc.setCell("Factores Totales:",                                    "formas",  ComunesPDF.LEFT   );
			pdfDoc.setCell((String)this.datosConsulta.get("factores_riesgo_usd"),  "formas",  ComunesPDF.LEFT   );
			pdfDoc.setCell("TASA DE CREDITO:",                                     "formas",  ComunesPDF.LEFT   );
			pdfDoc.setCell((String)this.datosConsulta.get("tasa_credito"),         "formas",  ComunesPDF.LEFT   );
			pdfDoc.addTable();
			pdfDoc.endDocument();

		} catch(Throwable e){
			throw new AppException("Error al generar el archivo PDF. ", e);
		}

		log.info("generaArchivoReporteFinal (S)");
		return nombreArchivo;

	}
	/**
	 * Genera el acrchivo CSV con los pagos por solicitud.
	 * @return nombreArchivo
	 */
	public String generaArchivoPagos(String path){
		String nombreArchivo = "";
		StringBuffer contenidoArchivo = new StringBuffer();
		CreaArchivo creaArchivo = new CreaArchivo();
		log.info("generaArchivoPagos(E)");
		try{

			contenidoArchivo.append("Plan de Pagos Capital - Solicitud N�mero: ");
			contenidoArchivo.append("" + getNumSolicitud());
			contenidoArchivo.append(",\n,\n");
				
			contenidoArchivo.append("N�mero de Pago,");
			contenidoArchivo.append("Fecha de Pago," );
			contenidoArchivo.append("Monto a pagar," );
			contenidoArchivo.append("\n"             );

			for(int i = 0; i < datosPagos.size(); i++){
				List alColumnas = (ArrayList)datosPagos.get(i);
				contenidoArchivo.append(alColumnas.get(0).toString().replace(',',' ') +",");
				contenidoArchivo.append(alColumnas.get(1).toString().replace(',',' ') +",");
				contenidoArchivo.append(alColumnas.get(2).toString().replace(',',' ') +",");
				contenidoArchivo.append("\n");
			}
			if(!creaArchivo.make(contenidoArchivo.toString(), path, ".csv")){
				nombreArchivo = "ERROR";
			}	else{
				nombreArchivo = creaArchivo.nombre;
			}
		} catch(Throwable e){
			throw new AppException("Error al generar el archivo CSV. ", e);
		}
		log.info("generaArchivoPagos(S)");
		return nombreArchivo;
	}

/************************************************************************
 *                          GETTERS AND SETTERS                         *
 ************************************************************************/
	public List getConditions() {
		return conditions;
	}

	public HashMap getDatosConsulta() {
		return datosConsulta;
	}

	public void setDatosConsulta(HashMap datosConsulta) {
		this.datosConsulta = datosConsulta;
	}

	public List getRegDetalleDisp() {
		return regDetalleDisp;
	}

	public void setRegDetalleDisp(List regDetalleDisp) {
		this.regDetalleDisp = regDetalleDisp;
	}

	public List getRegPlanPagos() {
		return regPlanPagos;
	}

	public void setRegPlanPagos(List regPlanPagos) {
		this.regPlanPagos = regPlanPagos;
	}

	public List getDatosPagos() {
		return datosPagos;
	}

	public void setDatosPagos(List datosPagos) {
		this.datosPagos = datosPagos;
	}

	public String getAviso1() {
		return "En caso de que la disposici�n de los recursos no se lleve a cabo "   +
		"total o parcialmente en la fecha estipulada, Nacional Financiera aplicar� " + 
		"el cobro de una comisi�n sobre los montos no dispuestos, conforme a las "   + 
		"pol�ticas de cr�dito aplicables a este tipo de operaciones.";
	}

	public String getAviso2() {
		return "COTIZACION INDICATIVA, NO ES POSIBLE TOMAR EN FIRME LA OPERACION, FAVOR DE CONSULTAR A SU EJECUTIVO.";
	}

	public String getAviso3() {
		return "Para realizar la toma en firme favor de ponerse en contacto con su ejecutivo.";
	}

	public String getAviso4() {
		return "Nota:\nTasa con oferta Mismo D�a: El Intermediario Financiero " +
		"tiene la opci�n de tomarla en firme el mismo d�a que haya recibido la " +
		"cotizaci�n indicativa a m�s tardar a las 12:30 horas.\n";
	}

	public String getAviso5() {
		return "Tasa con oferta 48 Horas: El Intermediario Financiero tiene la "   +
		"opci�n de tomarla en firme a m�s tardar dos d�as h�biles despu�s de que " +
		"la Tesorer�a haya proporcionado la cotizaci�n indicativa antes de las "   +
		"12:30 horas del segundo d�a h�bil.";
	}

	public String getAviso6() {
		return "Las cotizaciones indicativas proporcionadas por la Tesorer�a de " +
		"Nacional Financiera son de uso reservado para el intermediario solicitante " +
		"y ser� responsabilidad de dicho intermediario el mal uso de la informaci�n proporcionada.";
	}

	public String getNumSolicitud() {
		return numSolicitud;
	}

	public void setNumSolicitud(String numSolicitud) {
		this.numSolicitud = numSolicitud;
	}

	public String getGeneraReporteFinal() {
		return generaReporteFinal;
	}

	public void setGeneraReporteFinal(String generaReporteFinal) {
		this.generaReporteFinal = generaReporteFinal;
	}

	public HashMap getGridDisposiciones() {
		return gridDisposiciones;
	}

	public void setGridDisposiciones(HashMap gridDisposiciones) {
		this.gridDisposiciones = gridDisposiciones;
	}

	public HashMap getGridPlanPagos() {
		return gridPlanPagos;
	}

	public void setGridPlanPagos(HashMap gridPlanPagos) {
		this.gridPlanPagos = gridPlanPagos;
	}

	public HashMap getDatosArchivo() {
		return datosArchivo;
	}

	public void setDatosArchivo(HashMap datosArchivo) {
		this.datosArchivo = datosArchivo;
	}

}