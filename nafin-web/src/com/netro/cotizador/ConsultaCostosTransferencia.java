package com.netro.cotizador;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.negocio.InformacionGeneralBean;

import org.apache.commons.logging.Log;

public class ConsultaCostosTransferencia implements IQueryGeneratorRegExtJS  {


	private static final Log log = ServiceLocator.getInstance().getLog(ConsultaCostosTransferencia.class);//Variable para enviar mensajes al log.
	private List conditions;
	
	private String idCurva;
	private String nombre;
	private String fechaCurva;
	private String fechaCurvaA;
	private String nombreImagen;
	private String fecha;
	private String moneda;
	private String titulo;
	
	
	public ConsultaCostosTransferencia() {
	}
	
	public String getAggregateCalculationQuery() {
		// No tiene totales.
		return "";
	}
	public String getDocumentQuery(){  // para las llaves primarias		
		log.info(":: getDocumentQuery(E)");
		StringBuffer qrySentencia = new StringBuffer();
    	conditions = new ArrayList();
		try {
			if(/*!"".equals(fechaCurva) ||*/ !"".equals(idCurva)){
				conditions.add(idCurva);
				if("".equals(fechaCurva)){
					Registros reg = this.fechaCurva();
					while(reg.next()){
						fechaCurva = reg.getString("FECHA");
					}
				}
				conditions.add(fechaCurva);
			}else{
				log.error(":: getDocumentQuery(ERROR)");
				throw new AppException("ERROR::Error en los parametros recibidos=>[ fechasCurva || idCurva ]");
			}
		
			qrySentencia.append(" SELECT DISTINCT a.plazo||b.IC_MONEDA||b.idcurva||a.fecha as pkid, plazo	as plazo	\n"+
									"	  FROM datoscurva a, curva b	\n"+
									"	 WHERE b.IDCURVA = ?	\n"+
									"		AND fecha = ?	\n"+
									"		AND a.idcurva = b.idcurva	\n"+
									" ORDER BY a.plazo ASC       ");
			log.info("::qrySentancua( "+qrySentencia.toString()+" )");
			log.info("::conditions[ "+conditions+" ]");
		} catch(Exception e){
			e.printStackTrace();
			log.error("::getDocumentQuery(ERROR)=>[ "+e.getMessage()+" ]");
		}
      
      log.info(":: getDocumentQuery(S)");
		return qrySentencia.toString();
 	}
	
	public String getDocumentSummaryQueryForIds(List pageIds){ // paginacion 			
		log.info(" :: getDocumentSummaryQueryForIds(E)");
		conditions = new ArrayList();
		StringBuffer qrySentencia = new StringBuffer();
    	
		
		
			qrySentencia.append(" SELECT DISTINCT a.plazo, a.tasa, b.nombre, fecha	\n"+
							//consulta
							" FROM datoscurva a, curva b	\n"+
							" WHERE a.idcurva = b.idcurva	\n"+							
							"    and a.plazo||b.IC_MONEDA||b.idcurva||a.fecha in (");
		
      for(int i = 0; i < pageIds.size(); i++){
          List lItem = (ArrayList)pageIds.get(i);
          if(i>0){ qrySentencia.append(","); }
      	 qrySentencia.append(" ? ");
			 log.info(lItem.get(0).toString()+"\n");
			 conditions.add(lItem.get(0).toString());
        }
    	qrySentencia.append(")");
		qrySentencia.append(" ORDER BY  fecha, a.plazo ASC ");
		
		log.info(" :: qrySentencia ("+qrySentencia.toString()+" )");
		log.info(" :: conditions ["+conditions+" ]");
		log.info(" :: getDocumentSummaryQueryForIds(E)");
		
		return qrySentencia.toString();
		
 	}  
	public String getDocumentQueryFile(){ // para todos los registros				
		log.info(ConsultaCostosTransferencia.class.getName()+" :: getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();
    	conditions = new ArrayList();
			
		try {
			if(/*!"".equals(fechaCurva) ||*/ !"".equals(idCurva)){
				conditions.add(idCurva);
				if("".equals(fechaCurva)){
					Registros reg = this.fechaCurva();
					while(reg.next()){
						fechaCurva = reg.getString(0);
					}
				}
				conditions.add(fechaCurva);
			}else{
				log.error(":: getDocumentQuery(ERROR)");
				throw new AppException("ERROR::Error en los parametros recibidos=>[ fechasCurva || idCurva ]");
			}
		
			qrySentencia.append(
							" SELECT distinct a.plazo, a.tasa, b.nombre,fecha FROM datoscurva a, curva b "+
							" WHERE b.idcurva= ?	AND fecha= ?  AND a.idcurva=b.idcurva ORDER BY fecha,a.plazo ASC 	");
			log.info(ConsultaCostosTransferencia.class.getName()+" :: qrySentencia( "+qrySentencia.toString()+" )");
		} catch(Exception e){
			e.printStackTrace();
			log.error(ConsultaCostosTransferencia.class.getName()+" :: getDocumentQueryFile(ERROR)=>[ "+e.getMessage()+" ]");
		}
		return qrySentencia.toString();
 	}     
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
				
			if ("CSV".equals(tipo)) {
				try {
				
					nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
					writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
					buffer = new BufferedWriter(writer);
				
					contenidoArchivo.append("Plazo, Tasa \n");
				
					int registros = 0;	
					while(rs.next()){												
						String plazo = rs.getString("PLAZO")==null?"":rs.getString("PLAZO");
						String tasa = rs.getString("TASA")==null?"":rs.getString("TASA");										
						tasa = InformacionGeneralBean.formatoNumeros((Double.parseDouble(tasa))*100, "#0.000000");
						contenidoArchivo.append( plazo + "," + tasa + "\n");
					
						registros++;
						if(registros==1000){					
							registros=0;	
							buffer.write(contenidoArchivo.toString());
							contenidoArchivo = new StringBuffer();//Limpio  
						}
				
					}
					
					buffer.write(contenidoArchivo.toString());
					buffer.close();	
					contenidoArchivo = new StringBuffer();//Limpio    
				 
				} catch (Throwable e) {	
					e.printStackTrace();
						throw new AppException("Error al generar el archivo", e);
				} finally {
					try {
						rs.close();
					} catch(Exception e) {}
				}
			}else if ("PDF".equals(tipo)) {
				try {
					HttpSession session = request.getSession();
					
					ComunesPDF pdfDoc = new ComunesPDF();
					nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
					pdfDoc = new ComunesPDF(2, path + nombreArchivo);
		
		
					String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
					String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
					String diaActual    = fechaActual.substring(0,2);
					String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
					String anioActual   = fechaActual.substring(6,10);
					String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
					pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
					pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
					
					pdfDoc.addText("	 ","formas",ComunesPDF.CENTER);
					 
					pdfDoc.addText("Costos de Transferencia ","formas",ComunesPDF.CENTER);
				
					pdfDoc.addText("	 ","formas",ComunesPDF.CENTER);
					
					pdfDoc.setTable(2, 50);					
					pdfDoc.setCell(titulo,"celda01",ComunesPDF.CENTER,2);		
					pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tasa","celda01",ComunesPDF.CENTER);
													
					while (rs.next()) {
						String plazo = rs.getString("PLAZO")==null?"":rs.getString("PLAZO");
						String tasa = rs.getString("TASA")==null?"":rs.getString("TASA");
						tasa = InformacionGeneralBean.formatoNumeros((Double.parseDouble(tasa))*100, "#0.000000");
						pdfDoc.setCell(plazo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(tasa+" %","formas",ComunesPDF.CENTER);
					}
					
					pdfDoc.addTable();
					pdfDoc.endDocument();  
	
	
				} catch(Throwable e) {
				e.printStackTrace();
					throw new AppException("Error al generar el archivo", e);
				} finally {
					try {
						rs.close();
					} catch(Exception e) {}
				}
			}
		return nombreArchivo;	
	}	
	
	//para formar  csv o pdf por pagina  15 registros	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		
		if ("PDF".equals(tipo)) {   
			int TOTAL_CAMPOS = 18;
			int nRow = 0, numMon = 0, numDL = 0;
			double totSaldoInsMon = 0.0;
			double totAmortizMon = 0.0;
			double totInteresMon = 0.0;
			double totDescFopMon = 0.0;
			double totDescFinapeMon	= 0.0;
			double totVentoMon = 0.0;
			double totAdeudoTotMon = 0.0;
			double totSaldoInsNvoMon = 0.0;
			double totExigibleMon = 0.0;
			double totSaldoInsDL = 0.0;
			double totAmortizDL = 0.0;
			double totInteresDL = 0.0;
			double totDescFopDL = 0.0;
			double totDescFinapeDL	= 0.0;
			double totVentoDL = 0.0;
			double totAdeudoTotDL = 0.0;
			double totSaldoInsNvoDL = 0.0;
			double totExigibleDL = 0.0;
			
			
			
			try {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			 	
				  
				/*pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				((session.getAttribute("strLogo")==null)?"nafin":session.getAttribute("strLogo").toString()),
			(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				*/		
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				

				while (rs.next()) {
					Vector vecDatos = new Vector();	   
					for(int i=1; i<=TOTAL_CAMPOS; i++) {
						vecDatos.add(rs.getString(i));
					}
					
					
					if(rs.getString(3).equals("54")){
						numDL++;
						totSaldoInsDL		+= Double.valueOf(rs.getString(8)).doubleValue();
						totAmortizDL		+= Double.valueOf(rs.getString(9)).doubleValue();
						totInteresDL		+= Double.valueOf(rs.getString(10)).doubleValue();
						totDescFopDL		+= Double.valueOf(rs.getString(11)).doubleValue();
						totDescFinapeDL	+= Double.valueOf(rs.getString(12)).doubleValue();
						totVentoDL			+= Double.valueOf(rs.getString(13)).doubleValue();
						totAdeudoTotDL		+= Double.valueOf(rs.getString(14)).doubleValue();
						totSaldoInsNvoDL	+= Double.valueOf(rs.getString(15)).doubleValue();
						totExigibleDL		+= Double.valueOf(rs.getString("fg_totalexigible")).doubleValue();
					} else {
						numMon++;
						totSaldoInsMon		+= Double.valueOf(rs.getString(8)).doubleValue();
						totAmortizMon		+= Double.valueOf(rs.getString(9)).doubleValue();
						totInteresMon		+= Double.valueOf(rs.getString(10)).doubleValue();
						totDescFopMon		+= Double.valueOf(rs.getString(11)).doubleValue();
						totDescFinapeMon	+= Double.valueOf(rs.getString(12)).doubleValue();
						totVentoMon			+= Double.valueOf(rs.getString(13)).doubleValue();
						totAdeudoTotMon	+= Double.valueOf(rs.getString(14)).doubleValue();
						totSaldoInsNvoMon	+= Double.valueOf(rs.getString(15)).doubleValue();
						totExigibleMon		+= Double.valueOf(rs.getString("fg_totalexigible")).doubleValue();
					}
					
					
					nRow++;
				}
				
				
				pdfDoc.addTable();
				pdfDoc.endDocument();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					//rs.close();
				} catch(Exception e) {}
			}
		}		
		return nombreArchivo;
	}
	public Registros fechaCurva(){
		log.info(ConsultaCostosTransferencia.class.getName()+" :: fechaCurva(E)");
		AccesoDB con = new AccesoDB(); 
		Registros registros  = new Registros();
		StringBuffer qrySentencia = new StringBuffer("");
		conditions = new ArrayList();
		try{
			con.conexionDB();
				qrySentencia.append("SELECT TO_CHAR (MAX (TO_DATE (fecha, 'yyyymmdd')), 'yyyymmdd') as fecha \n"+
										"	FROM datoscurva a, curva b \n"+
										"	 WHERE b.idcurva = ? AND a.idcurva = b.idcurva");
										conditions.add(idCurva);
			registros=con.consultarDB(qrySentencia.toString(),conditions);
			
		}catch(Exception e){
			log.error("getDataCurvaTabla  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("getDataCurvaTabla (S) ");
		return registros;
	}
	public List getConditions() {
		return conditions;
	}


	public void setIdCurva(String idCurva) {
		this.idCurva = idCurva;
	}


	public String getIdCurva() {
		return idCurva;
	}


	public void setFechaCurva(String fechaCurva) {
		this.fechaCurva = fechaCurva;
	}


	public String getFechaCurva() {
		return fechaCurva;
	}


	public void setFechaCurvaA(String fechaCurvaA) {
		this.fechaCurvaA = fechaCurvaA;
	}


	public String getFechaCurvaA() {
		return fechaCurvaA;
	}


	public void setNombreImagen(String nombreImagen) {
		this.nombreImagen = nombreImagen;
	}


	public String getNombreImagen() {
		return nombreImagen;
	}


	public void setFecha(String fecha) {
		this.fecha = fecha;
	}


	public String getFecha() {
		return fecha;
	}


	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}


	public String getMoneda() {
		return moneda;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getNombre() {
		return nombre;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

}