package com.netro.cotizador;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import net.sf.json.JSONArray;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/*******************************************************************************************
 * Fodea:       04-2015                                                                    *
 * Descripci�n: Migraci�n de la pantalla Tasas de credito Nafin- Tablas de precios a ExtJS *
 * Elabor�:     Agust�n Bautista Ruiz                                                      *
 * Fecha:       22/09/2014                                                                 *
 *******************************************************************************************/
public class ConsTablaPrecios{

	private static final Log log = ServiceLocator.getInstance().getLog(ConsTablaPrecios.class);
	private int    idCuadro;
	private int    opcion;
	private int    numeroColumnas;
	private String usuario;
	private String fecha;
	private String nombreArchivo;
	private String strDirTrabajo;
	private String strIdTipoIntermediario;
	private String tipoIntermediario;

	public ConsTablaPrecios(){}

	/**
	 * Lee el archivo de la tabla y regresa los datos en una lista
	 * @return lista con los renglones del archivo
	 */
	public InputStream leeArchivo(){

		log.info("leeArchivo(E)");

		InputStream  inStream     = null;

		AccesoDB          con = new AccesoDB();
		ResultSet         rs  = null; 
		PreparedStatement ps  = null;

		try{
			con.conexionDB();
			String query = " SELECT BI_ARCHIVO FROM CUADROS WHERE IDCUADRO = ? ";
			log.debug("Sentencia: " + query   );
			log.debug("idCuadro: "  + idCuadro);
			ps = con.queryPrecompilado(query);
			ps.setInt(1,idCuadro);
			rs = ps.executeQuery();
			if (rs.next()){
				Blob blob = rs.getBlob("bi_archivo");
				inStream = blob.getBinaryStream();
			}
			rs.close();
			ps.close();

		} catch(Exception e){
			log.warn("leeArchivoException " + e);
		} finally{
			try { rs.close(); } catch (SQLException e){}
			try { ps.close(); } catch (SQLException e){}
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		log.info("leeArchivo(S)");
		return inStream;
	}

	/**
	 * Extrae los riesgos y el id de usuario a partir de su login
	 * @throws java.lang.Exception
	 * @return HashMap
	 */
	public HashMap<String, String> getIntermediario() throws Exception{

		AccesoDB con = new AccesoDB();
		ResultSet rs = null; 

		HashMap<String, String> mapa     = new HashMap<String, String>();
		StringBuffer            consulta = new StringBuffer();

		log.info("getIntermediario(E)");

		try{
			con.conexionDB();
			consulta.append("SELECT IC_USUARIO, RIESGO FROM RIESGOUSR WHERE IC_USUARIO = '" + this.usuario + "'");
			log.debug("Sentencia: " + consulta.toString());
			rs = con.queryDB(consulta.toString());
			if (rs.next()){
				mapa.put("IC_USUARIO", rs.getString("IC_USUARIO"));
				mapa.put("RIESGO",     rs.getString("RIESGO")    );
			}
		} catch(Exception e){
			log.warn("getIntermediarioException " + e);
		} finally{
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}

		log.info("getIntermediario(S)");
		return mapa;

	}

	/**
	 * Obtiene los datos a mostrar en el reporte
	 * @throws java.lang.Exception
	 * @return HashMap
	 */
	public HashMap<String, String> getDatosReporte() throws Exception{

		AccesoDB con = new AccesoDB();
		ResultSet rs = null;

		HashMap<String, String> mapa     = new HashMap<String, String>();
		StringBuffer            consulta = new StringBuffer();

		log.info("getDatosReporte(E)");

		try{
			con.conexionDB();
			consulta.append("SELECT * FROM CUADROS WHERE IDCUADRO = " + this.idCuadro);
			log.debug("Sentencia: " + consulta.toString());
			rs = con.queryDB(consulta.toString());
			if (rs.next()){
				mapa.put("IDCUADRO",   rs.getString("IDCUADRO")  );
				mapa.put("TITMENU",    rs.getString("TITMENU")   );
				mapa.put("TITREPORTE", rs.getString("TITREPORTE"));
				mapa.put("SUBTITREP",  rs.getString("SUBTITREP") );
				mapa.put("TXTCAB",     rs.getString("TXTCAB")    );
				mapa.put("TXTPIE",     rs.getString("TXTPIE")    );
				mapa.put("ARCHIVO",    rs.getString("ARCHIVO")   );
			}
		} catch(Exception e){
			log.warn("getDatosReporteException " + e);
		} finally{
			if (con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}

		log.info("getDatosReporte(S)");
		return mapa;

	}

	/**
	 * Obtiene la fecha del servidor que es la fecha de consulta de la pantalla
	 * @throws java.lang.Exception
	 * @return fecha
	 */
	public String getFecha() throws Exception{
		AccesoDB con = new AccesoDB();
		ResultSet rs = null; 
		StringBuffer consulta = new StringBuffer();
		log.info("getFecha(E)");
		try{
			con.conexionDB();
			consulta.append("SELECT TO_CHAR(SYSDATE,'dd/mm/rrrr') FROM DUAL");
			log.debug("Sentencia: " + consulta.toString());
			rs = con.queryDB(consulta.toString());
			if (rs.next()){
				fecha = rs.getString(1);
			} else{
				fecha = "";
			}
		} catch(Exception e){
			log.warn("getFechaException " + e);
		} finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		log.info("getFecha(S)");
		return fecha;
	}

	/**
	 * Obtiene el tipo de intermediario cuando lee el archivo
	 * @throws java.lang.Exception
	 * @return tipoIntermediario
	 * @param var2
	 */
	public String getTipoIntermediario(String var2) throws Exception{
		AccesoDB con = new AccesoDB();
		ResultSet rs = null; 
		StringBuffer consulta = new StringBuffer();
		String descripcion = null;
		log.info("getTipoIntermediario(E)");
		try{
			con.conexionDB();
			consulta.append("SELECT * FROM TIPOINTERMEDIARIO WHERE IDTIPOINTERMEDIARIO = " + var2 );
			log.debug("Sentencia: " + consulta.toString());
			rs = con.queryDB(consulta.toString());
			if (rs.next()){
				descripcion = rs.getString("DESCRIPCION").trim();
			}
			if(descripcion == null){
				System.err.println("La consulta no arroj� datos");
			}
		} catch(Exception e){
			log.warn("getTipoIntermediarioException " + e);
		} finally{
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		log.info("getTipoIntermediario(S)");
		return descripcion;
	}

	public JSONArray getDatosArchivo() throws Exception{

		log.info("getDatosArchivo(E)");
		AccesoDB con = new AccesoDB();
		int contador      = 0;
		int numCols       = 0;
		int numRenglon    = 0;
		int paraNotas     = 0;
		String tokenFinal = "";
		String renglon    = "";
		String var        = "";
		String var2       = "";
		String auxi       = "";
		String cstrg      = "";
		String notas      = "";
		String decide     = ",0,";
		boolean flag      = true;
		boolean flagUser  = true;
		HashMap mapa      = new HashMap();
		JSONArray lista   = new JSONArray();

		try{
			con.conexionDB();
			PreparedStatement ps = null;
			ResultSet rs = null;
			
			String query = " SELECT BI_ARCHIVO FROM CUADROS WHERE IDCUADRO = ? ";
			
			ps = con.queryPrecompilado(query);
			ps.setInt(1,idCuadro);
			rs = ps.executeQuery();
			
			InputStream inStream = null;
		    if(rs.next()){			
				Blob blob = rs.getBlob("bi_archivo");
				inStream = blob.getBinaryStream();				
			}
			rs.close();
			ps.close();
			
			BufferedReader bDatos   = new BufferedReader(new InputStreamReader(inStream,"ISO-8859-1"));

			if((renglon = bDatos.readLine()) != null){
				StringTokenizer datos = new StringTokenizer(renglon, ",");
				while(datos.hasMoreElements()){
					var = datos.nextToken();
					if(var.length()>0) {
						var2 = strIdTipoIntermediario;
						/***** Se eliminan las comillas *****/
						StringTokenizer temporal = new StringTokenizer(var, "\"");
						while(temporal.hasMoreElements()){
							if(temporal.nextToken().equals(var2))
								decide += contador + ",";
							contador++;
						}
					}
				}
			}
			contador = 0;
			while((renglon = bDatos.readLine()) != null){
				StringTokenizer datos2 = new StringTokenizer(renglon, ",");
				mapa = new HashMap();
				numRenglon ++;
				while(datos2.hasMoreElements()){
					var = datos2.nextToken();
					if(var.length()>0) {
						cstrg = "," + contador + ",";
						if(decide.indexOf(cstrg) >- 1){
							if (paraNotas == 4){
								notas = notas + var + ",";
							} else{
								StringTokenizer caso = new StringTokenizer(var, "\"");
								while(caso.hasMoreElements()){
									var2 = caso.nextToken();
								}
								mapa.put("CAMPO_" + contador, var2);
								if(numRenglon == 1)
									numCols++;
							}
						}
						contador ++;
					}
				} //Termina un rengl�n
				if (paraNotas != 4 &&  mapa.size()>0){ //Cuando paraNotas == 5, no guarda nada en el hashmap, por eso no se agrega a la lista
					lista.add(mapa);
				}
				this.setNumeroColumnas(numCols);
				contador = 0;
				paraNotas ++;
			} // Termina toda la tabla
			bDatos.close();
			inStream.close();
		} catch(Exception e){
			lista = null;
			log.warn("getDatosArchivoException. " +  e);
		}finally
		{
			if(con.hayConexionAbierta())
			{
				con.cierraConexionDB();
			}
		}
		log.info("getDatosArchivo(S)");
		return lista;
	}

	public JSONArray getDatosArchivoBis() throws Exception{

		log.info("getDatosArchivoBis(E)");
		AccesoDB con = new AccesoDB();
		int contador      = 0;
		int numCols       = 0;
		int numRenglon    = 0;
		int paraNotas     = 0;
		String tokenFinal = "";
		String renglon    = "";
		String var        = "";
		String var2       = "";
		String auxi       = "";
		String cstrg      = "";
		String notas      = "";
		String decide     = ",0,";
		boolean flag      = true;
		boolean flagUser  = true;
		HashMap mapa      = new HashMap();
		JSONArray lista   = new JSONArray();
		try{
			
			con.conexionDB();
			PreparedStatement ps = null;
			ResultSet rs = null;
			
			String query = " SELECT BI_ARCHIVO FROM CUADROS WHERE IDCUADRO = ? ";
			
			ps = con.queryPrecompilado(query);
			ps.setInt(1,idCuadro);
			rs = ps.executeQuery();
			
			InputStream inStream = null;
			if(rs.next()){
				Blob blob = rs.getBlob("bi_archivo");
				inStream = blob.getBinaryStream();
			}
			rs.close();
			ps.close();
						
			BufferedReader bDatos   = new BufferedReader(new InputStreamReader(inStream,"ISO-8859-1"));
				/*
			if(this.opcion == 1){
				if((renglon = bDatos.readLine()) != null){
					StringTokenizer datos = new StringTokenizer(renglon, ",");
					while(datos.hasMoreElements()){
						var = datos.nextToken();
						var2 = this.strIdTipoIntermediario;
						log.debug("var2: " + var2);
						//***** Se eliminan las comillas ****
						StringTokenizer temporal = new StringTokenizer(var, "\"");
						while(temporal.hasMoreElements()){
							tokenFinal = temporal.nextToken();
							if (var2 != null ) {							
								StringTokenizer creditos = new StringTokenizer(var2, ",");							
								while(creditos.hasMoreElements()){
									auxi = creditos.nextToken();
									if( tokenFinal.equals(auxi) )
										decide += contador + ",";
								}
							}
							contador ++;
						}
					}
				}
			}
			*/
			
			contador = 0;
			while((renglon = bDatos.readLine()) != null){
				StringTokenizer datos2 = new StringTokenizer(renglon, ",");				
				mapa = new HashMap();
				numRenglon ++;
				while(datos2.hasMoreElements()){
					var = datos2.nextToken().trim();						
					if(var.length()>0) {
						cstrg = "," + contador + ",";
						
						if(this.opcion == 1){  
						
							if(decide.indexOf(cstrg)>-1){
								if (paraNotas == 5){
									notas = notas + var + ",";
								} else{
									/*Se eliminan las comillas*/
									StringTokenizer caso = new StringTokenizer(var, "\"");
									while(caso.hasMoreElements()){
										var2 = caso.nextToken();
									}
									if(paraNotas == 0){
										if (flagUser){
											mapa.put("CAMPO_" + contador, "");
											flagUser = false;
											if(numRenglon == 1)
												numCols ++;
										}
										String descripcion = this.getTipoIntermediario(var2);
										if(!"".equals(descripcion)){
											mapa.put("CAMPO_" + contador, descripcion);
											if(numRenglon == 1)
												numCols ++;
										}
									} else{
										mapa.put("CAMPO_" + contador, var2);
										if(numRenglon == 1)
											numCols ++;
									}
								}
							}
									
						} else{ //Opcion 2
												
							if (paraNotas == 5){
								notas = notas + var + ",";
							} else{
								StringTokenizer caso = new StringTokenizer(var, "\"");
								while(caso.hasMoreElements()){
									var2 = caso.nextToken();
								}									
								if(paraNotas == 0){
									if (flagUser){
										mapa.put("CAMPO_" + contador, "");
										flagUser = false;
										if(numRenglon == 1)
											numCols ++;
									}
									String descripcion = this.getTipoIntermediario(var2);
									if(!"".equals(descripcion)){
										mapa.put("CAMPO_" + contador, descripcion);
										if(numRenglon == 1)
											numCols ++;
									}
								} else{
									mapa.put("CAMPO_" + contador, var2);
								}
							}
						}
						contador ++;
					}
				} //Termina un rengl�n
				
				if (paraNotas != 5  && mapa.size()>0){ //Cuando paraNotas == 5, no guarda nada en el hashmap, por eso no se agrega a la lista
					lista.add(mapa);
				}
				this.setNumeroColumnas(numCols); //Obtengo el numero de columnas para saber cuantas voy a mostrar en el grid
				contador = 0;
				paraNotas ++;
			} // Termina toda la tabla
			bDatos.close();
			inStream.close();
		} catch(Exception e){
			lista = null;
			log.error("getDatosArchivoBisException. ",  e);
		}finally
		{
			if(con.hayConexionAbierta())
			{
				con.cierraConexionDB();
			}
		}
		log.info("getDatosArchivoBis(S)");
		return lista;

	}

/************************************************************
 *                   GETTERS ANS SETTERS                    *
 ************************************************************/
	public int getIdCuadro() {
		return idCuadro;
	}

	public void setIdCuadro(int idCuadro) {
		this.idCuadro = idCuadro;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getNombreArchivo() {
		return nombreArchivo;
	}

	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}

	public String getStrDirTrabajo() {
		return strDirTrabajo;
	}

	public void setStrDirTrabajo(String strDirTrabajo) {
		this.strDirTrabajo = strDirTrabajo;
	}

	public String getStrIdTipoIntermediario() {
		return strIdTipoIntermediario;
	}

	public void setStrIdTipoIntermediario(String strIdTipoIntermediario) {
		this.strIdTipoIntermediario = strIdTipoIntermediario;
	}

	public int getOpcion() {
		return opcion;
	}

	public void setOpcion(int opcion) {
		this.opcion = opcion;
	}

	public int getNumeroColumnas() {
		return numeroColumnas;
	}

	public void setNumeroColumnas(int numeroColumnas) {
		this.numeroColumnas = numeroColumnas;
	}

}