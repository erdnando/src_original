package com.netro.cotizador;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import org.apache.commons.logging.Log;

public class ConsSolCotiTasaCredNafin implements IQueryGeneratorRegExtJS{

	public ConsSolCotiTasaCredNafin() {}

//-------------------- C�DIGO DE LA VERSI�N MIGRADA INICIO ----------------------------
	private final static Log log = ServiceLocator.getInstance().getLog(ConsSolCotiTasaCredNafin.class);

	private List   conditions;
	private String fechaSolicitudIni;
	private String fechaSolicitudFin;
	private String noSolicitud;
	private String icIf;
	private String fechaEstatusIni;
	private String fechaEstatusFin;
	private String usuario;
	private String icMoneda;
	private String montoCreditoIni;
	private String montoCreditoFin;
	private String icEjecutivo;
	private String icEstatus;

	/**
	 * Obtiene las llaves primarias
	 * @return sentencia sql
	 */
	public String getDocumentQuery(){

		log.info("getDocumentQuery(E)");
		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();
		
		qrySentencia.append("SELECT DISTINCT CS.IC_SOLIC_ESP                                                    \n");
		qrySentencia.append("           FROM COT_SOLIC_ESP CS,                                                  \n");
		qrySentencia.append("                COTCAT_EJECUTIVO CE,                                               \n");
		qrySentencia.append("                COTCAT_ESTATUS E,                                                  \n");
		qrySentencia.append("                COMCAT_MONEDA CM,                                                  \n");
		qrySentencia.append("                COMCAT_IF CI                                                       \n");
		qrySentencia.append("          WHERE CS.IC_EJECUTIVO = CE.IC_EJECUTIVO                                  \n");
		qrySentencia.append("            AND CS.IC_ESTATUS = E.IC_ESTATUS                                       \n");
		qrySentencia.append("            AND CS.IC_MONEDA = CM.IC_MONEDA                                        \n");
		qrySentencia.append("            AND CI.IC_IF(+) = CS.IC_IF                                             \n");

		if(!this.usuario.equals("")){
			qrySentencia.append("            AND (   CE.IC_USUARIO = ?                                           \n");
			qrySentencia.append("                 OR CE.IC_AREA IN (SELECT IC_AREA                               \n");
			qrySentencia.append("                                     FROM COTCAT_EJECUTIVO                      \n");
			qrySentencia.append("                                    WHERE IC_USUARIO = ? AND CS_DIRECTOR = 'S') \n");
			qrySentencia.append("                )                                                               \n");
			conditions.add(this.usuario);
			conditions.add(this.usuario);
		}
		if(!this.fechaSolicitudIni.equals("") && !this.fechaSolicitudFin.equals("")){
			qrySentencia.append("            AND CS.DF_SOLICITUD BETWEEN TO_DATE (?, 'DD/MM/YYYY')               \n");
			qrySentencia.append("                                    AND   TO_DATE (?, 'DD/MM/YYYY') + 1         \n");
			conditions.add(this.fechaSolicitudIni);
			conditions.add(this.fechaSolicitudFin);
		}
		if(!this.noSolicitud.equals("")){
			qrySentencia.append("            AND (   TO_CHAR (CS.DF_SOLICITUD, 'YYYY')                           \n");
			qrySentencia.append("                 || TO_CHAR (CS.DF_SOLICITUD, 'MM')                             \n");
			qrySentencia.append("                 || '-'                                                         \n");
			qrySentencia.append("                 || CS.IG_NUM_SOLIC                                             \n");
			qrySentencia.append("                ) = ?                                                           \n");
			conditions.add(this.noSolicitud);
		}
		if(!this.icMoneda.equals("")){
			qrySentencia.append("            AND CS.IC_MONEDA = ?                                                \n");
			conditions.add(this.icMoneda);
		}
		if(!this.montoCreditoIni.equals("") && !this.montoCreditoFin.equals("")){
			qrySentencia.append("            AND CS.FN_MONTO BETWEEN ? AND ?                                     \n");
			conditions.add(this.montoCreditoIni);
			conditions.add(this.montoCreditoFin);
		}
		if(!this.icIf.equals("")){
			qrySentencia.append("            AND CS.IC_IF = ?                                                    \n");
			conditions.add(this.icIf);
		}
		if(!this.icEstatus.equals("")){
			qrySentencia.append("            AND CS.IC_ESTATUS = ?                                               \n");
			conditions.add(this.icEstatus);
		} else{
			qrySentencia.append("            AND CS.IC_ESTATUS IN (7, 8, 9, 10)                                  \n");
		}
		if(!this.fechaEstatusIni.equals("") && !this.fechaEstatusFin.equals("")){
			qrySentencia.append("            AND CS.DF_MODIF_ESTATUS BETWEEN TO_DATE (?,                         \n");
			qrySentencia.append("                                                     'DD/MM/YYYY'               \n");
			qrySentencia.append("                                                    )                           \n");
			qrySentencia.append("                                        AND   TO_DATE (?,                       \n");
			qrySentencia.append("                                                       'DD/MM/YYYY'             \n");
			qrySentencia.append("                                                      ) + 1                     \n");
			conditions.add(this.fechaEstatusIni);
			conditions.add(this.fechaEstatusFin);
		}
		if(!this.usuario.equals("")){
			qrySentencia.append("            AND CS.IC_USUARIO_MODIF_ESTATUS = ?                                 \n");
			conditions.add(this.usuario);
		}

		qrySentencia.append("       ORDER BY CS.IC_SOLIC_ESP"                                                      );

		log.info("qrySentencia: \n" + qrySentencia);
		log.info("conditions: " + conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();

	}

	/**
	 * Obtiene la lista de los Ids en turno para realizar la paginacion
	 * @return sentencia sql
	 * @param ids
	 */
	public String getDocumentSummaryQueryForIds(List ids){

		log.info("getDocumentSummaryQueryForIds(E)");
		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();

		qrySentencia.append("SELECT DISTINCT CS.IC_SOLIC_ESP AS IC_SOLIC_ESP,                              \n");
		qrySentencia.append("                TO_CHAR (CS.DF_SOLICITUD, 'DD/MM/YYYY') AS FECHA_SOLICITUD,   \n");
		qrySentencia.append("                TO_CHAR (CS.DF_COTIZACION,                                    \n");
		qrySentencia.append("                         'DD/MM/YYYY  HH24:MM:SS'                             \n");
		qrySentencia.append("                        ) AS FECHA_COTIZACION,                                \n");
		qrySentencia.append("                (   TO_CHAR (CS.DF_SOLICITUD, 'YYYY')                         \n");
		qrySentencia.append("                 || TO_CHAR (CS.DF_SOLICITUD, 'MM')                           \n");
		qrySentencia.append("                 || '-'                                                       \n");
		qrySentencia.append("                 || CS.IG_NUM_SOLIC                                           \n");
		qrySentencia.append("                ) AS NUMERO_SOLICITUD,                                        \n");
		qrySentencia.append("                DECODE (CS.IC_IF,                                             \n");
		qrySentencia.append("                        NULL, CS.CG_RAZON_SOCIAL_IF,                          \n");
		qrySentencia.append("                        CI.CG_RAZON_SOCIAL                                    \n");
		qrySentencia.append("                       ) AS INTERMEDIARIO,                                    \n");
		qrySentencia.append("                CM.CD_NOMBRE AS MONEDA_SOLICITUD,                             \n");
		qrySentencia.append("                (CE.CG_NOMBRE || ' ' || CE.CG_APPATERNO || ' '                \n");
		qrySentencia.append("                 || CE.CG_APMATERNO                                           \n");
		qrySentencia.append("                ) AS EJECUTIVO,                                               \n");
		qrySentencia.append("                CS.FN_MONTO AS MONTO, CS.IG_PLAZO AS PLAZO,                   \n");
		qrySentencia.append("                DECODE (CS.CS_ACEPTADO_MD,                                    \n");
		qrySentencia.append("                        'S', CS.IG_TASA_MD,                                   \n");
		qrySentencia.append("                        CS.IG_TASA_SPOT                                       \n");
		qrySentencia.append("                       ) AS TASA,                                             \n");
		qrySentencia.append("                CS.IC_ESTATUS AS IC_ESTATUS, CS.CG_OBSERVACIONES,             \n");
		qrySentencia.append("                CER.CG_DESCRIPCION AS ESQUEMA_RECUP,                          \n");
		qrySentencia.append("                CP.IC_SOLIC_ESP AS NUMERO_DE_PAGO, CS.IC_MONEDA,              \n");
		qrySentencia.append("                CS.IC_TIPO_TASA,                                              \n");
		qrySentencia.append("                TO_CHAR (CS.DF_ACEPTACION,                                    \n");
		qrySentencia.append("                         'DD/MM/YYYY  HH24:MM:SS'                             \n");
		qrySentencia.append("                        ) AS FECHA_HORA_SOLICITUD,                            \n");
		qrySentencia.append("                E.CG_DESCRIPCION AS RS_ESTATUS, CS.CG_UT_PPC, CS.CG_UT_PPI,   \n");
		qrySentencia.append("                CS.IC_ESQUEMA_RECUP, CS.CG_OBSERVACIONES_SIRAC,               \n");
		qrySentencia.append("                CSP.IC_SOLIC_ESP AS TABLA_PORTAL,                             \n");
		qrySentencia.append("                TO_CHAR (CS.DF_MODIF_ESTATUS,                                 \n");
		qrySentencia.append("                         'DD/MM/YYYY'                                         \n");
		qrySentencia.append("                        ) AS FECHACAMBIOESTATUS,                              \n");
		qrySentencia.append("                   CS.IC_USUARIO_MODIF_ESTATUS                                \n");
		qrySentencia.append("                || ' - '                                                      \n");
		qrySentencia.append("                || CS.CG_NOMUSER_MOD_ESTATUS AS USUARIOCAMBIOESTATUS,         \n");
		qrySentencia.append("                '' AS IC_PLAN_DE_PAGOS,                                       \n");
		qrySentencia.append("                '' AS DESCTASAIND,                                            \n");
		qrySentencia.append("                'false' AS CAMBIA_OPERADO                                     \n");
		qrySentencia.append("           FROM COT_SOLIC_ESP CS,                                             \n");
		qrySentencia.append("                COTCAT_EJECUTIVO CE,                                          \n");
		qrySentencia.append("                COTCAT_ESTATUS E,                                             \n");
		qrySentencia.append("                COMCAT_MONEDA CM,                                             \n");
		qrySentencia.append("                COMCAT_IF CI,                                                 \n");
		qrySentencia.append("                COT_PAGO CP,                                                  \n");
		qrySentencia.append("                COTCAT_ESQUEMA_RECUP CER,                                     \n");
		qrySentencia.append("                (SELECT IC_SOLIC_ESP                                          \n");
		qrySentencia.append("                   FROM COM_SOLIC_PORTAL                                      \n");
		qrySentencia.append("                  WHERE IC_SOLIC_ESP IS NOT NULL AND IC_ESTATUS_SOLIC = 2) CSP\n");
		qrySentencia.append("          WHERE CS.IC_EJECUTIVO = CE.IC_EJECUTIVO                             \n");
		qrySentencia.append("            AND CS.IC_ESTATUS = E.IC_ESTATUS                                  \n");
		qrySentencia.append("            AND CS.IC_MONEDA = CM.IC_MONEDA                                   \n");
		qrySentencia.append("            AND CI.IC_IF(+) = CS.IC_IF                                        \n");
		qrySentencia.append("            AND CS.IC_ESQUEMA_RECUP = CER.IC_ESQUEMA_RECUP                    \n");
		qrySentencia.append("            AND CS.IC_SOLIC_ESP = CP.IC_SOLIC_ESP(+)                          \n");
		qrySentencia.append("            AND CS.IC_SOLIC_ESP = CSP.IC_SOLIC_ESP(+)                         \n");
		qrySentencia.append("            AND CS.IC_SOLIC_ESP IN (                                          \n");
		for (int i = 0; i < ids.size(); i++) { 
			List lItem = (ArrayList)ids.get(i);
			if(i > 0){
				qrySentencia.append(",");
			}
			qrySentencia.append(" ? " );
			conditions.add(new Long(lItem.get(0).toString()));
		}

    	qrySentencia.append("                                   \n)                                        \n");

		qrySentencia.append("       ORDER BY CS.IC_SOLIC_ESP");

		log.info("qrySentencia: \n" + qrySentencia);
		log.info("conditions: " + conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();

	}

	/**
	 * Obtiene los totales
	 * @return sentencia sql
	 */
	public String getAggregateCalculationQuery(){

		log.info("getAggregateCalculationQuery(E)");
		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();

		qrySentencia.append("SELECT COUNT (DISTINCT CS.IC_SOLIC_ESP)                                            \n");
		qrySentencia.append("           FROM COT_SOLIC_ESP CS,                                                  \n");
		qrySentencia.append("                COTCAT_EJECUTIVO CE,                                               \n");
		qrySentencia.append("                COTCAT_ESTATUS E,                                                  \n");
		qrySentencia.append("                COMCAT_MONEDA CM,                                                  \n");
		qrySentencia.append("                COMCAT_IF CI                                                       \n");
		qrySentencia.append("          WHERE CS.IC_EJECUTIVO = CE.IC_EJECUTIVO                                  \n");
		qrySentencia.append("            AND CS.IC_ESTATUS = E.IC_ESTATUS                                       \n");
		qrySentencia.append("            AND CS.IC_MONEDA = CM.IC_MONEDA                                        \n");
		qrySentencia.append("            AND CI.IC_IF(+) = CS.IC_IF                                             \n");

		if(!this.usuario.equals("")){
			qrySentencia.append("            AND (   CE.IC_USUARIO = ?                                           \n");
			qrySentencia.append("                 OR CE.IC_AREA IN (SELECT IC_AREA                               \n");
			qrySentencia.append("                                     FROM COTCAT_EJECUTIVO                      \n");
			qrySentencia.append("                                    WHERE IC_USUARIO = ? AND CS_DIRECTOR = 'S') \n");
			qrySentencia.append("                )                                                               \n");
			conditions.add(this.usuario);
			conditions.add(this.usuario);
		}
		if(!this.fechaSolicitudIni.equals("") && !this.fechaSolicitudFin.equals("")){
			qrySentencia.append("            AND CS.DF_SOLICITUD BETWEEN TO_DATE (?, 'DD/MM/YYYY')               \n");
			qrySentencia.append("                                    AND   TO_DATE (?, 'DD/MM/YYYY') + 1         \n");
			conditions.add(this.fechaSolicitudIni);
			conditions.add(this.fechaSolicitudFin);
		}
		if(!this.noSolicitud.equals("")){
			qrySentencia.append("            AND (   TO_CHAR (CS.DF_SOLICITUD, 'YYYY')                           \n");
			qrySentencia.append("                 || TO_CHAR (CS.DF_SOLICITUD, 'MM')                             \n");
			qrySentencia.append("                 || '-'                                                         \n");
			qrySentencia.append("                 || CS.IG_NUM_SOLIC                                             \n");
			qrySentencia.append("                ) = ?                                                           \n");
			conditions.add(this.noSolicitud);
		}
		if(!this.icMoneda.equals("")){
			qrySentencia.append("            AND CS.IC_MONEDA = ?                                                \n");
			conditions.add(this.icMoneda);
		}
		if(!this.montoCreditoIni.equals("") && !this.montoCreditoFin.equals("")){
			qrySentencia.append("            AND CS.FN_MONTO BETWEEN ? AND ?                                     \n");
			conditions.add(this.montoCreditoIni);
			conditions.add(this.montoCreditoFin);
		}
		if(!this.icIf.equals("")){
			qrySentencia.append("            AND CS.IC_IF = ?                                                    \n");
			conditions.add(this.icIf);
		}
		if(!this.icEstatus.equals("")){
			qrySentencia.append("            AND CS.IC_ESTATUS = ?                                               \n");
			conditions.add(this.icEstatus);
		} else{
			qrySentencia.append("            AND CS.IC_ESTATUS IN (7, 8, 9, 10)                                  \n");
		}
		if(!this.fechaEstatusIni.equals("") && !this.fechaEstatusFin.equals("")){
			qrySentencia.append("            AND CS.DF_MODIF_ESTATUS BETWEEN TO_DATE (?,                         \n");
			qrySentencia.append("                                                     'DD/MM/YYYY'               \n");
			qrySentencia.append("                                                    )                           \n");
			qrySentencia.append("                                        AND   TO_DATE (?,                       \n");
			qrySentencia.append("                                                       'DD/MM/YYYY'             \n");
			qrySentencia.append("                                                      ) + 1                     \n");
			conditions.add(this.fechaEstatusIni);
			conditions.add(this.fechaEstatusFin);
		}
		if(!this.usuario.equals("")){
			qrySentencia.append("            AND CS.IC_USUARIO_MODIF_ESTATUS = ?                                 \n");
			conditions.add(this.usuario);
		}

		qrySentencia.append("       ORDER BY CS.IC_SOLIC_ESP"                                                      );

		log.info("qrySentencia: \n" + qrySentencia);
		log.info("conditions: " + conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();

	}

	/**
	 * Obtiene la consulta para generar el archivo PDF o CSV sin utilizar paginaci�n
	 * @return sentencia sql
	 */
	public String getDocumentQueryFile(){

		log.info("getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();

		log.info("qrySentencia: \n" + qrySentencia);
		log.info("conditions: " + conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();

	}

	/**
	 * Crea el archivo PDF o CSV seg�n el tipo que se le pasa como par�metro.
	 * Obtiene los datos de la consulta generada en el m�todo getDocumentQueryFile.
	 * @param request
	 * @param rs
	 * @param path
	 * @param tipo
	 * @return nombreArchivo
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String directorioTemporal, String tipo){
		return null;
	}

	/**
	 * Crea el archivo PDF utilizando paginaci�n
	 * @param request
	 * @param reg
	 * @param path
	 * @param tipo
	 * @return nombreArchivo
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo){
		return null;
	}

/*****************************************************
 *                GETTERS AND SETTERS                *
 *****************************************************/
	public List getConditions() {
		return conditions;
	}

	public String getFechaSolicitudIni() {
		return fechaSolicitudIni;
	}

	public void setFechaSolicitudIni(String fechaSolicitudIni) {
		this.fechaSolicitudIni = fechaSolicitudIni;
	}

	public String getFechaSolicitudFin() {
		return fechaSolicitudFin;
	}

	public void setFechaSolicitudFin(String fechaSolicitudFin) {
		this.fechaSolicitudFin = fechaSolicitudFin;
	}

	public String getNoSolicitud() {
		return noSolicitud;
	}

	public void setNoSolicitud(String noSolicitud) {
		this.noSolicitud = noSolicitud;
	}

	public String getIcIf() {
		return icIf;
	}

	public void setIcIf(String icIf) {
		this.icIf = icIf;
	}

	public String getFechaEstatusIni() {
		return fechaEstatusIni;
	}

	public void setFechaEstatusIni(String fechaEstatusIni) {
		this.fechaEstatusIni = fechaEstatusIni;
	}

	public String getFechaEstatusFin() {
		return fechaEstatusFin;
	}

	public void setFechaEstatusFin(String fechaEstatusFin) {
		this.fechaEstatusFin = fechaEstatusFin;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getIcMoneda() {
		return icMoneda;
	}

	public void setIcMoneda(String icMoneda) {
		this.icMoneda = icMoneda;
	}

	public String getMontoCreditoIni() {
		return montoCreditoIni;
	}

	public void setMontoCreditoIni(String montoCreditoIni) {
		this.montoCreditoIni = montoCreditoIni;
	}

	public String getMontoCreditoFin() {
		return montoCreditoFin;
	}

	public void setMontoCreditoFin(String montoCreditoFin) {
		this.montoCreditoFin = montoCreditoFin;
	}

	public String getIcEjecutivo() {
		return icEjecutivo;
	}

	public void setIcEjecutivo(String icEjecutivo) {
		this.icEjecutivo = icEjecutivo;
	}

	public String getIcEstatus() {
		return icEstatus;
	}

	public void setIcEstatus(String icEstatus) {
		this.icEstatus = icEstatus;
	}

}