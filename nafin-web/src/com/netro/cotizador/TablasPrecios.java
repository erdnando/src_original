package com.netro.cotizador;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/********************************************************************************************
 * Fodea:       04-2015                                                                     *
 * Descripci�n: Migraci�n de la pantalla Tasas de credito Nafin- Tablas de precios a ExtJS. *
 *              La clase genera los archivos pdf y csv. No realiza consultas ya que obtiene *
 *              los datos del form de la pantalla                                           *
 * Elabor�:     Agust�n Bautista Ruiz                                                       *
 * Fecha:       29/09/2014                                                                  *
 ********************************************************************************************/
public class TablasPrecios implements IQueryGeneratorRegExtJS{

	private static final Log log = ServiceLocator.getInstance().getLog(TablasPrecios.class);
	private JSONArray arrayGrid;
	private List      conditions;
	private String    tituloReporte;
	private String    subtituloReporte;
	private String    fechaConsulta;
	private String    cabeceraReporte;
	private String    pieReporte;
	private boolean   muestraGrid;
	private int       numColumnas;

	public TablasPrecios(){}
	public String getDocumentQuery(){ return null; }
	public String getAggregateCalculationQuery(){ return null; }
	public String getDocumentSummaryQueryForIds(List ids){ return null; }
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){ return null; }

	public String getDocumentQueryFile(){
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();
		log.info("getDocumentQueryFile (E)");
		qrySentencia.append("SELECT TO_CHAR(SYSDATE,'dd/mm/rrrr') FROM DUAL");
		log.info("getDocumentQueryFile (S)");
		return qrySentencia.toString(); 
	}

	/**
	 * Genera el archivo PDF o CSV seg�n el tipo solicitado
	 * @return nombreArchivo
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){

		log.info("crearCustomFile (E)");

		String nombreArchivo = "";
		HttpSession session  = request.getSession();

		if(tipo.equals("PDF")){

			ComunesPDF pdfDoc             = new ComunesPDF();
			CreaArchivo creaArchivo       = new CreaArchivo();
			OutputStreamWriter writer     = null;
			BufferedWriter buffer         = null;

			try{
				Comunes comunes = new Comunes();
				nombreArchivo   = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc          = new ComunesPDF(2, path + nombreArchivo);

				String meses[]     = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText("\n\n","formas",ComunesPDF.RIGHT);

				pdfDoc.addText(this.reemplazarTags(this.tituloReporte),    "formasB", ComunesPDF.LEFT);
				pdfDoc.addText(this.reemplazarTags(this.subtituloReporte), "formasB", ComunesPDF.LEFT);
				pdfDoc.addText(                    this.fechaConsulta,     "formas",  ComunesPDF.LEFT);
				pdfDoc.addText("\n\n",                                     "formas",  ComunesPDF.LEFT);
				pdfDoc.addText(this.reemplazarTags(this.cabeceraReporte),  "formas",  ComunesPDF.LEFT);
				pdfDoc.addText("\n\n",                                     "formas",  ComunesPDF.LEFT);

				if(this.muestraGrid == true){
					String campo = "";
					pdfDoc.setLTable(this.numColumnas, 100);
					for(int i=0; i<arrayGrid.size(); i++){ // ESte for cuenta las filas de la tabla
						JSONObject auxiliar = arrayGrid.getJSONObject(i);
						for(int j = 0; j < auxiliar.size(); j++){ // Este for cuenta las columnas
							campo = (auxiliar.getString("CAMPO_" + j)).equals("null")?"" : auxiliar.getString("CAMPO_" + j);
							if(j == 0){
								pdfDoc.setLCell(campo, "formas", ComunesPDF.LEFT);
							} else{
								pdfDoc.setLCell(campo, "formas", ComunesPDF.CENTER);
							}
						}
					}
					pdfDoc.addLTable();
				}

				pdfDoc.addText("\n\n", "formas", ComunesPDF.LEFT);
				pdfDoc.addText(this.reemplazarTags(this.pieReporte), "formas", ComunesPDF.LEFT);

				pdfDoc.endDocument();

			} catch(Throwable e){
				throw new AppException("Error al generar el archivo PDF. ", e);
			}

		} else if(tipo.equals("CSV")){

			try{
				OutputStreamWriter writer = null;
				BufferedWriter buffer = null;
				StringBuffer contenidoArchivo = new StringBuffer();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				int total = 0;

				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
				buffer = new BufferedWriter(writer);

				contenidoArchivo = new StringBuffer();
				contenidoArchivo.append(this.reemplazarTags(this.tituloReporte).replaceAll(",","") + "\n");
				contenidoArchivo.append(this.reemplazarTags(this.subtituloReporte).replaceAll(",","") + "\n");
				contenidoArchivo.append(this.fechaConsulta + "\n,\n");
				contenidoArchivo.append(this.reemplazarTags(this.cabeceraReporte).replaceAll(",","") + "\n,\n");
				if(this.muestraGrid == true){
					String campo = "";
					for(int i=0; i<arrayGrid.size(); i++){ // Este for cuenta las filas
						JSONObject auxiliar = arrayGrid.getJSONObject(i);
						for(int j = 0; j < auxiliar.size(); j++){ // Este for cuenta las columnas
							campo = (auxiliar.getString("CAMPO_" + j)).equals("null")?"" : auxiliar.getString("CAMPO_" + j);
							if(j == (auxiliar.size()-1 ) ){
								contenidoArchivo.append(campo + "\n");
							} else{
								contenidoArchivo.append(campo + ",");
							}
							
						}
					}
				}

				contenidoArchivo.append("\n" + this.reemplazarTags(this.pieReporte).replaceAll(",",""));
				buffer.write(contenidoArchivo.toString());
				buffer.close();
				contenidoArchivo = new StringBuffer();//Limpio

			} catch(Throwable e){
				throw new AppException("Error al generar el archivo CSV. ", e);
			}

		}

		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}
	
	/**
	 * Reemplaza los tags html de los string que forman el documento, dejando solo texto plano
	 * @return cadenaNueva
	 * @param cadena
	 */
	public String reemplazarTags(String cadena){
	
		boolean guarda = true;
		String cadenaNueva = "";
		char c;
		for (int n = 0; n < cadena.length ()-1; n++){
			c = cadena.charAt (n);
			if(c == '<'){
				guarda = false;
			} else if(c == '>'){
				guarda = true;
			}
			if(guarda == true){
				cadenaNueva += c;
			}
		}
		cadenaNueva = cadenaNueva.replaceAll(">","");
		return cadenaNueva;
	}

/************************************************************
 *                  GETTERS ANS SETTERS                     *
 ************************************************************/
	public List getConditions(){
		return conditions; 
	}

	public String getTituloReporte() {
		return tituloReporte;
	}

	public void setTituloReporte(String tituloReporte) {
		this.tituloReporte = tituloReporte;
	}

	public String getSubtituloReporte() {
		return subtituloReporte;
	}

	public void setSubtituloReporte(String subtituloReporte) {
		this.subtituloReporte = subtituloReporte;
	}

	public String getFechaConsulta() {
		return fechaConsulta;
	}

	public void setFechaConsulta(String fechaConsulta) {
		this.fechaConsulta = fechaConsulta;
	}

	public String getCabeceraReporte() {
		return cabeceraReporte;
	}

	public void setCabeceraReporte(String cabeceraReporte) {
		this.cabeceraReporte = cabeceraReporte;
	}

	public String getPieReporte() {
		return pieReporte;
	}

	public void setPieReporte(String pieReporte) {
		this.pieReporte = pieReporte;
	}

	public int getNumColumnas() {
		return numColumnas;
	}

	public void setNumColumnas(int numColumnas) {
		this.numColumnas = numColumnas;
	}

	public JSONArray agetArrayGrid() {
		return arrayGrid;
	}

	public void setArrayGrid(JSONArray arrayGrid) {
		this.arrayGrid = arrayGrid;
	}

	public boolean getMuestraGrid() {
		return muestraGrid;
	}

	public void setMuestraGrid(boolean muestraGrid) {
		this.muestraGrid = muestraGrid;
	}

}