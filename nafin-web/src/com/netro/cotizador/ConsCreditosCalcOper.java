package com.netro.cotizador;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import com.netro.pdf.ComunesPDF;
import java.util.HashMap;

import netropology.utilerias.IQueryGeneratorPS;

import org.apache.commons.logging.Log;

public class ConsCreditosCalcOper implements IQueryGeneratorRegExtJS{

	private final static Log log = ServiceLocator.getInstance().getLog(ConsCreditosCalcOper.class);

	private List   conditions;
	private List   gridEstatus;
	private List   gridCotizacion;
	private List   gridCalculadora;
	private String fechaSolicitudIni;
	private String fechaSolicitudFin;
	private String noSolicitud;
	private String icIf;
	private String fechaEstatusIni;
	private String fechaEstatusFin;
	private String usuario;
	private String icMoneda;
	private String montoCreditoIni;
	private String montoCreditoFin;
	private String icEjecutivo;
	private String icEstatus;

	/**
	 * Obtiene las llaves primarias
	 * @return sentencia sql
	 */
	public String getDocumentQuery(){

		log.info("getDocumentQuery(E)");
		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();
		
		qrySentencia.append("SELECT NOREFERENCIA                     \n");
		qrySentencia.append("  FROM CREDITO CRE, STATUSCOTIZACION EC \n");
		qrySentencia.append(" WHERE CRE.IDSTATUS = EC.IDSTATUS       \n");

		if(!this.noSolicitud.equals("") && this.noSolicitud.indexOf("-") >= 0){
			this.noSolicitud = "0";
		}

		if(!this.icEstatus.equals("")){
			if(this.icEstatus.equals("7")){
				this.icEstatus = "2";
			} else if(this.icEstatus.equals("8")){
				this.icEstatus = "3";
			} else if(this.icEstatus.equals("9")){
				this.icEstatus = "4";
			}
		}

		if(!this.noSolicitud.equals("")){
			if(!this.icMoneda.equals("") && this.icMoneda.equals("54")){
				qrySentencia.append(" AND CRE.NOREFERENCIA IS NULL\n");
			} else{
				qrySentencia.append(" AND CRE.NOREFERENCIA = ?\n");
				conditions.add(this.noSolicitud);
			}
		}

		if(!this.fechaSolicitudIni.equals("") && !this.fechaSolicitudFin.equals("")){
			qrySentencia.append(" AND CRE.FECHACOTIZACION BETWEEN TO_DATE (?,'DD/MM/YYYY') AND TO_DATE(?,'DD/MM/YYYY')\n"); 
			conditions.add(this.fechaSolicitudIni);
			conditions.add(this.fechaSolicitudFin);
		}

		if(!this.icMoneda.equals("") && this.icMoneda.equals("54")){
			qrySentencia.append(" AND CRE.NOREFERENCIA IS NULL\n");
		}

		if(!this.montoCreditoIni.equals("") && !this.montoCreditoFin.equals("")){
			qrySentencia.append(" AND CRE.MONTO BETWEEN ? AND ?\n");
			conditions.add(this.montoCreditoIni);
			conditions.add(this.montoCreditoFin);
		}
		if(!this.icEstatus.equals("")){
			qrySentencia.append(" AND CRE.IDSTATUS = ?\n");
			conditions.add(this.icEstatus);
		} else{
			qrySentencia.append(" AND CRE.IDSTATUS IN (2,3,4)\n");
		}
		if(!this.fechaEstatusIni.equals("") && !this.fechaEstatusFin.equals("")){
			qrySentencia.append(" AND CRE.IDSTATUS = ?\n");
			conditions.add("0");
		}
		if(!this.usuario.equals("")){
			qrySentencia.append(" AND CRE.IDSTATUS = ?\n");
			conditions.add("0");
		}

		log.info("qrySentencia: \n" + qrySentencia);
		log.info("conditions: " + conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();

	}

	/**
	 * Obtiene la lista de los Ids en turno para realizar la paginacion
	 * @return sentencia sql
	 * @param ids
	 */
	public String getDocumentSummaryQueryForIds(List ids){

		log.info("getDocumentSummaryQueryForIds(E)");
		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();

		qrySentencia.append("SELECT   TO_CHAR (FECHACOTIZACION, 'DD/MM/YYYY') AS FECHA_COTIZACION,           \n");
		qrySentencia.append("         TO_CHAR (FECHADISPOSICION, 'DD/MM/YYYY') AS FECHA_TF,                  \n");
		qrySentencia.append("         NOREFERENCIA AS NO_REFERENCIA, INTERMEDIARIO AS NOMBRE_IF,             \n");
		qrySentencia.append("         'MONEDA NACIONAL' AS NOMBRE_MONEDA, E.EJECUTIVO,                       \n");
		qrySentencia.append("         TO_CHAR(MONTO,'FM999999999.99') AS MONTO,                              \n");
		qrySentencia.append("         PLAZO || ' ' || UT.DESCRIPCION AS PLAZO,                               \n");
		qrySentencia.append("         CASE                                                                   \n");
		qrySentencia.append("            WHEN PERIODOSGRACIA IS NOT NULL                                     \n");
		qrySentencia.append("               THEN 'Tipo Renta'                                                \n");
		qrySentencia.append("            WHEN PERIODOGRACIACAPITAL IS NOT NULL                               \n");
		qrySentencia.append("               THEN 'Tradicional'                                               \n");
		qrySentencia.append("            ELSE 'Cupon Cero'                                                   \n");
		qrySentencia.append("         END AS ESQUEMA,                                                        \n");
		qrySentencia.append("         TASANETACONVIGENCIA * 100 AS TASA, EC.DESCRIPCION AS ESTATUS,          \n");
		qrySentencia.append("         CG_OBSERVACIONES_SIRAC AS OBSERVACIONES, CRE.IDSTATUS AS ID_STATUS,    \n");
		qrySentencia.append("         'false' AS CAMBIA_OPERADO                                              \n");
		qrySentencia.append("    FROM CREDITO CRE,                                                           \n");
		qrySentencia.append("         UNIDADTIEMPO UT,                                                       \n");
		qrySentencia.append("         STATUSCOTIZACION EC,                                                   \n");
		qrySentencia.append("         (SELECT    NVL (VE.CG_NOMBRE, '')                                      \n");
		qrySentencia.append("                 || ' '                                                         \n");
		qrySentencia.append("                 || NVL (VE.CG_APPATERNO, '')                                   \n");
		qrySentencia.append("                 || ' '                                                         \n");
		qrySentencia.append("                 || NVL (VE.CG_APMATERNO, '') AS EJECUTIVO,                     \n");
		qrySentencia.append("                 VE.CG_MAIL AS CORREO_ELECTRONICO, VE.CG_TELEFONO AS TELEFONO,  \n");
		qrySentencia.append("                 VE.CG_FAX AS FAX, VI.CG_RAZON_SOCIAL                           \n");
		qrySentencia.append("            FROM COTCAT_EJECUTIVO VE, COTREL_IF_EJECUTIVO VIE, COMCAT_IF VI     \n");
		qrySentencia.append("           WHERE VI.IC_IF = VIE.IC_IF AND VE.IC_EJECUTIVO = VIE.IC_EJECUTIVO) E \n");
		qrySentencia.append("   WHERE CRE.IDUNIDADTIEMPO = UT.IDUNIDADTIEMPO                                 \n");
		qrySentencia.append("     AND CRE.IDSTATUS = EC.IDSTATUS                                             \n");
		qrySentencia.append("     AND CRE.INTERMEDIARIO = E.CG_RAZON_SOCIAL(+)                               \n");
		qrySentencia.append("     AND CRE.NOREFERENCIA IN (                                                  \n");

		for (int i = 0; i < ids.size(); i++) { 
			List lItem = (ArrayList)ids.get(i);
			if(i > 0){
				qrySentencia.append(",");
			}
			qrySentencia.append(" ? " );
			conditions.add(new Long(lItem.get(0).toString()));
		}

    	qrySentencia.append("                           \n)                                                  \n");

		qrySentencia.append("ORDER BY CRE.NOREFERENCIA");

		log.info("qrySentencia: \n" + qrySentencia);
		log.info("conditions: " + conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();

	}

	/**
	 * Obtiene los totales
	 * @return sentencia sql
	 */
	public String getAggregateCalculationQuery(){

		log.info("getAggregateCalculationQuery(E)");
		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();

		qrySentencia.append("SELECT COUNT (NOREFERENCIA)             \n");
		qrySentencia.append("  FROM CREDITO CRE, STATUSCOTIZACION EC \n");
		qrySentencia.append(" WHERE CRE.IDSTATUS = EC.IDSTATUS       \n");

		if(!this.noSolicitud.equals("") && this.noSolicitud.indexOf("-") >= 0){
			this.noSolicitud = "0";
		}

		if(!this.icEstatus.equals("")){
			if(this.icEstatus.equals("7")){
				this.icEstatus = "2";
			} else if(this.icEstatus.equals("8")){
				this.icEstatus = "3";
			} else if(this.icEstatus.equals("9")){
				this.icEstatus = "4";
			}
		}

	   if(!this.noSolicitud.equals("")){
	      if(!this.icMoneda.equals("") && this.icMoneda.equals("54")){
	         qrySentencia.append(" AND CRE.NOREFERENCIA IS NULL\n");
	      } else{
	         qrySentencia.append(" AND CRE.NOREFERENCIA = ?\n");
	         conditions.add(this.noSolicitud);
	      }
	   }
		if(!this.fechaSolicitudIni.equals("") && !this.fechaSolicitudFin.equals("")){
			qrySentencia.append(" AND CRE.FECHACOTIZACION BETWEEN TO_DATE (?,'DD/MM/YYYY') AND TO_DATE(?,'DD/MM/YYYY')\n"); 
			conditions.add(this.fechaSolicitudIni);
			conditions.add(this.fechaSolicitudFin);
		}
		if(!this.icMoneda.equals("") && this.icMoneda.equals("54")){
			qrySentencia.append(" AND CRE.NOREFERENCIA IS NULL\n");
		}
		if(!this.montoCreditoIni.equals("") && !this.montoCreditoFin.equals("")){
			qrySentencia.append(" AND CRE.MONTO BETWEEN ? AND ?\n");
			conditions.add(this.montoCreditoIni);
			conditions.add(this.montoCreditoFin);
		}
		if(!this.icEstatus.equals("")){
			qrySentencia.append(" AND CRE.IDSTATUS = ?\n");
			conditions.add(this.icEstatus);
		} else{
			qrySentencia.append(" AND CRE.IDSTATUS IN (2,3,4)\n");
		}
		if(!this.fechaEstatusIni.equals("") && !this.fechaEstatusFin.equals("")){
			qrySentencia.append(" AND CRE.IDSTATUS = ?\n");
			conditions.add("0");
		}
		if(!this.usuario.equals("")){
			qrySentencia.append(" AND CRE.IDSTATUS = ?\n");
			conditions.add("0");
		}

		log.info("qrySentencia: \n" + qrySentencia);
		log.info("conditions: " + conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();

	}

	/**
	 * Obtiene la consulta para generar el archivo PDF o CSV sin utilizar paginaci�n
	 * @return sentencia sql
	 */
	public String getDocumentQueryFile(){

		log.info("getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();

		qrySentencia.append("SELECT COUNT(*) FROM DUAL");

		log.info("qrySentencia: \n" + qrySentencia);
		log.info("conditions: " + conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();

	}

	/**
	 * Crea el archivo PDF o CSV seg�n el tipo que se le pasa como par�metro.
	 * @param request
	 * @param rs
	 * @param path
	 * @param tipo
	 * @return nombreArchivo
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){

		log.info("crearCustomFile(E)");

		String      nombreArchivo = "";
		HttpSession session       = request.getSession();
		ComunesPDF  pdfDoc        = new ComunesPDF();
		HashMap     mapa          = new HashMap();

		try{

			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);

			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual   = fechaActual.substring(0,2);
			String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual  = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

			if(gridEstatus != null && !gridEstatus.isEmpty()){
				pdfDoc.setLTable(2,60);
				pdfDoc.setLCell("La operaci�n se llev� a cabo con �xito", "celda01", ComunesPDF.CENTER, 2);
				for(int i=0; i<gridEstatus.size(); i++){
					mapa = new HashMap();
					mapa = (HashMap) gridEstatus.get(i);
					pdfDoc.setLCell((String)mapa.get("ETIQUETA"),      "formas", ComunesPDF.LEFT);
					pdfDoc.setLCell((String)mapa.get("DESCRIPCION"),   "formas", ComunesPDF.LEFT);
				}
				pdfDoc.addLTable();
			}

			if(gridCotizacion != null && !gridCotizacion.isEmpty()){
				pdfDoc.setLTable(12,100);
				pdfDoc.setLCell("COTIZACI�N ESPEC�FICA",          "celda01", ComunesPDF.LEFT, 12);
				pdfDoc.setLCell("Fecha de Solicitud",             "celda01", ComunesPDF.CENTER  );
				pdfDoc.setLCell("Fecha y hora de Toma en Firme",  "celda01", ComunesPDF.CENTER  );
				pdfDoc.setLCell("N�mero de Solicitud",            "celda01", ComunesPDF.CENTER  );
				pdfDoc.setLCell("Intermediario financiero",       "celda01", ComunesPDF.CENTER  );
				pdfDoc.setLCell("Moneda",                         "celda01", ComunesPDF.CENTER  );
				pdfDoc.setLCell("Ejecutivo",                      "celda01", ComunesPDF.CENTER  );
				pdfDoc.setLCell("Monto Total Requerido",          "celda01", ComunesPDF.CENTER  ); 
				pdfDoc.setLCell("Plazo de la Operaci�n",          "celda01", ComunesPDF.CENTER  );
				pdfDoc.setLCell("Esquema de Recuperaci�n",        "celda01", ComunesPDF.CENTER  );
				pdfDoc.setLCell("Tasa Confirmada",                "celda01", ComunesPDF.CENTER  );
				pdfDoc.setLCell("Estatus",                        "celda01", ComunesPDF.CENTER  );
				pdfDoc.setLCell("Observaciones",                  "celda01", ComunesPDF.CENTER  );

				for(int i=0; i<gridCotizacion.size(); i++){
					mapa = new HashMap();
					mapa = (HashMap) gridCotizacion.get(i);
					pdfDoc.setLCell((String)mapa.get("FECHA_SOLICITUD"),      "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell((String)mapa.get("FECHA_HORA_SOLICITUD"), "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell((String)mapa.get("NUMERO_SOLICITUD"),     "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell((String)mapa.get("INTERMEDIARIO"),        "formas", ComunesPDF.LEFT  );
					pdfDoc.setLCell((String)mapa.get("MONEDA_SOLICITUD"),     "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell((String)mapa.get("EJECUTIVO"),            "formas", ComunesPDF.LEFT  );
					pdfDoc.setLCell((String)mapa.get("MONTO"),                "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell((String)mapa.get("PLAZO"),                "formas", ComunesPDF.LEFT  );
					pdfDoc.setLCell((String)mapa.get("ESQUEMA_RECUP"),        "formas", ComunesPDF.LEFT  );
					pdfDoc.setLCell((String)mapa.get("DESCTASAIND"),          "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell((String)mapa.get("RS_ESTATUS"),           "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell((String)mapa.get("CG_OBSERVACIONES"),     "formas", ComunesPDF.LEFT  );
				}
				pdfDoc.addLTable();
			}

			if(gridCalculadora != null && !gridCalculadora.isEmpty()){
				//pdfDoc.newPage();
				pdfDoc.setLTable(12,100);
				pdfDoc.setLCell("CALCULADORA",              "celda01", ComunesPDF.LEFT, 12);
				pdfDoc.setLCell("Fecha de Cotizaci�n",      "celda01", ComunesPDF.CENTER  );
				pdfDoc.setLCell("Fecha de Toma en Firme",   "celda01", ComunesPDF.CENTER  );
				pdfDoc.setLCell("N�mero de Referencia",     "celda01", ComunesPDF.CENTER  );
				pdfDoc.setLCell("Intermediario Financiero", "celda01", ComunesPDF.CENTER  );
				pdfDoc.setLCell("Moneda",                   "celda01", ComunesPDF.CENTER  );
				pdfDoc.setLCell("Ejecutivo",                "celda01", ComunesPDF.CENTER  );
				pdfDoc.setLCell("Monto Total Requerido",    "celda01", ComunesPDF.CENTER  ); 
				pdfDoc.setLCell("Plazo de la Operaci�n",    "celda01", ComunesPDF.CENTER  );
				pdfDoc.setLCell("Esquema de Recuperaci�n",  "celda01", ComunesPDF.CENTER  );
				pdfDoc.setLCell("Tasa Confirmada",          "celda01", ComunesPDF.CENTER  );
				pdfDoc.setLCell("Estatus",                  "celda01", ComunesPDF.CENTER  );
				pdfDoc.setLCell("Observaciones",            "celda01", ComunesPDF.CENTER  );

				for(int i=0; i<gridCalculadora.size(); i++){
					mapa = new HashMap();
					mapa = (HashMap) gridCalculadora.get(i);
					pdfDoc.setLCell((String)mapa.get("FECHA_COTIZACION"), "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell((String)mapa.get("FECHA_TF"),         "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell((String)mapa.get("NO_REFERENCIA"),    "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell((String)mapa.get("NOMBRE_IF"),        "formas", ComunesPDF.LEFT  );
					pdfDoc.setLCell((String)mapa.get("NOMBRE_MONEDA"),    "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell((String)mapa.get("EJECUTIVO"),        "formas", ComunesPDF.LEFT  );
					pdfDoc.setLCell((String)mapa.get("MONTO"),            "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell((String)mapa.get("PLAZO"),            "formas", ComunesPDF.LEFT  );
					pdfDoc.setLCell((String)mapa.get("ESQUEMA"),          "formas", ComunesPDF.LEFT  );
					pdfDoc.setLCell((String)mapa.get("TASA"),             "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell((String)mapa.get("ESTATUS"),          "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell((String)mapa.get("OBSERVACIONES"),    "formas", ComunesPDF.LEFT  );
				}
				pdfDoc.addLTable();
			}
						
			pdfDoc.endDocument();

		} catch(Throwable e){
			throw new AppException("Error al generar el archivo ", e);
		}

		log.info("crearCustomFile(S)");
		return nombreArchivo;
	}

	/**
	 * Crea el archivo PDF utilizando paginaci�n
	 * @param request
	 * @param reg
	 * @param path
	 * @param tipo
	 * @return nombreArchivo
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo){
		return null;
	}

/*****************************************************
 *                GETTERS AND SETTERS                *
 *****************************************************/
	public List getConditions() {
		return conditions;
	}

	public List getGridEstatus() {
		return gridEstatus;
	}

	public void setGridEstatus(List gridEstatus) {
		this.gridEstatus = gridEstatus;
	}

	public List getGridCotizacion() {
		return gridCotizacion;
	}

	public void setGridCotizacion(List gridCotizacion) {
		this.gridCotizacion = gridCotizacion;
	}

	public List getGridCalculadora() {
		return gridCalculadora;
	}

	public void setGridCalculadora(List gridCalculadora) {
		this.gridCalculadora = gridCalculadora;
	}

	public String getFechaSolicitudIni() {
		return fechaSolicitudIni;
	}

	public void setFechaSolicitudIni(String fechaSolicitudIni) {
		this.fechaSolicitudIni = fechaSolicitudIni;
	}

	public String getFechaSolicitudFin() {
		return fechaSolicitudFin;
	}

	public void setFechaSolicitudFin(String fechaSolicitudFin) {
		this.fechaSolicitudFin = fechaSolicitudFin;
	}

	public String getNoSolicitud() {
		return noSolicitud;
	}

	public void setNoSolicitud(String noSolicitud) {
		this.noSolicitud = noSolicitud;
	}

	public String getIcIf() {
		return icIf;
	}

	public void setIcIf(String icIf) {
		this.icIf = icIf;
	}

	public String getFechaEstatusIni() {
		return fechaEstatusIni;
	}

	public void setFechaEstatusIni(String fechaEstatusIni) {
		this.fechaEstatusIni = fechaEstatusIni;
	}

	public String getFechaEstatusFin() {
		return fechaEstatusFin;
	}

	public void setFechaEstatusFin(String fechaEstatusFin) {
		this.fechaEstatusFin = fechaEstatusFin;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getIcMoneda() {
		return icMoneda;
	}

	public void setIcMoneda(String icMoneda) {
		this.icMoneda = icMoneda;
	}

	public String getMontoCreditoIni() {
		return montoCreditoIni;
	}

	public void setMontoCreditoIni(String montoCreditoIni) {
		this.montoCreditoIni = montoCreditoIni;
	}

	public String getMontoCreditoFin() {
		return montoCreditoFin;
	}

	public void setMontoCreditoFin(String montoCreditoFin) {
		this.montoCreditoFin = montoCreditoFin;
	}

	public String getIcEjecutivo() {
		return icEjecutivo;
	}

	public void setIcEjecutivo(String icEjecutivo) {
		this.icEjecutivo = icEjecutivo;
	}

	public String getIcEstatus() {
		return icEstatus;
	}

	public void setIcEstatus(String icEstatus) {
		this.icEstatus = icEstatus;
	}

}