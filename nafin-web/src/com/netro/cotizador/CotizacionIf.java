package com.netro.cotizador;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorRegExtJS;

public class CotizacionIf implements IQueryGenerator, IQueryGeneratorRegExtJS {

	public CotizacionIf() {  }
	
	public String getAggregateCalculationQuery(HttpServletRequest request) {
		// No tiene totales.
		return "select 1 from dual";
	}
	
	public String getDocumentSummaryQueryForIds(HttpServletRequest request, Collection ids) {
		StringBuffer qrySentencia = new StringBuffer();
    	
    	String ic_usuario = (request.getParameter("ic_usuario")==null)?"":request.getParameter("ic_usuario").trim();
    	String usuario_nafin = (request.getParameter("usuario_nafin")==null)?"":request.getParameter("usuario_nafin").trim();
	   /*usuario_nafin ==> para pantalla Nafin-Tasa de Credito Nafin-Solicitud Cotizacion - Consulta Solicitud*/
	   /* usuario_nafin permitira obtener La descripcion de los IFS que se despliegan en el layout, aun que no se haya seleccionado Un ejecutivo del Combo, 
	   esto es solo para el caso de la consultas del usuario Nafin, no se utiliza
	   como un filtro, mas bien es una bandera que le idicara a la clase de la paginacion
	   CotizacionIf.java que se trata de una consulta de el usuario nafin, para que forme el query de consulta que incluya el campo de 
	   descripcion de IF.*/
    	
		qrySentencia.append(
							" SELECT distinct cs.ic_solic_esp as ic_solic_esp,TO_CHAR(cs.df_solicitud,'DD/MM/YYYY') AS fecha_solicitud, "+
							"		 TO_CHAR(cs.df_cotizacion,'DD/MM/YYYY  HH24:MI:SS') AS fecha_cotizacion, "+
							"        (TO_CHAR(cs.df_solicitud,'YYYY') || TO_CHAR(cs.df_solicitud,'MM') "+
							//"|| '-S' || cs.ic_solic_esp  "+
							"         || '-' || cs.ig_num_solic "+							
							"        ) AS numero_solicitud, "+
							"        cm.cd_nombre AS moneda, "+
							"        (ce.cg_nombre || ' ' || ce.cg_appaterno || ' ' || ce.cg_apmaterno "+
							"        ) AS ejecutivo, "+
							"        cs.fn_monto AS monto, cs.ig_plazo AS plazo, "+
							"        e.cg_descripcion AS estatus, cs.ig_tasa_md tasa_mismo_dia, "+
							"        cs.ig_tasa_spot AS tasa_spot, cs.ig_num_referencia AS num_referencia, "+
							"        cs.cg_causa_rechazo AS causa_rechazo, "+
							"        cs.cg_observaciones_sirac AS obser_oper, "+
							"		 cs.ic_estatus as ic_estatus, "+
							"		 DECODE(cs.cs_aceptado_md,'S',cs.Ig_tasa_md,cs.ig_tasa_spot) as tasa_confirmada "+
							((!ic_usuario.equals("")||usuario_nafin.equals("S"))?" ,DECODE(cs.ic_if,null,cs.CG_RAZON_SOCIAL_IF,ci.CG_RAZON_SOCIAL) as CG_RAZON_SOCIAL ":"")+//Para pantalla [Ejecutivo | Nafin]-Tasa de Credito Nafin-Solicitud Cotizacion - Consulta Solicitud
							" 	,cs.ic_moneda"+
							"		,cs.ic_tipo_tasa"+
							"  ,case "+
					    "     when ie.cg_solicitar_aut = 'T' then 'SI' "+
					    "     when ie.cg_solicitar_aut = 'N' then 'NO' "+
					    "     when ie.cg_solicitar_aut = 'M' and cs.fn_monto >= ie.fn_monto_mayor_a then 'SI' "+
					    "     else 'NO' "+
					    "   end as solicitar_aut "+
							"		,decode(cs.CG_TIPO_COTIZACION,'I','Indicativa','F','En Firme','') as tipoCotiza "+
							"   ,cs.cg_ut_ppc"+
							"   ,cs.cg_ut_ppi"+
							"   ,cs.ic_esquema_recup"+	
							"	,cs.cs_linea "+					
							"   FROM cot_solic_esp cs, "+
							"        cotcat_ejecutivo ce, "+
							"        cotcat_estatus e, "+
							"        comcat_moneda cm, "+
							"				 cotrel_if_ejecutivo ie"+
							((!ic_usuario.equals("")||usuario_nafin.equals("S"))?" ,comcat_if ci ":"")+//Para pantalla [Ejecutivo | Nafin]-Tasa de Credito Nafin-Solicitud Cotizacion - Consulta Solicitud
							"  WHERE cs.ic_ejecutivo = ce.ic_ejecutivo "+
							"    AND cs.ic_estatus = e.ic_estatus "+
							"    AND cs.ic_moneda = cm.ic_moneda "+
							" 	 AND cs.ic_if = ie.ic_if(+)" +
							((!ic_usuario.equals("")||usuario_nafin.equals("S"))?" AND ci.IC_IF(+) = cs.IC_IF ":"")+//Para pantalla [Ejecutivo | Nafin]-Tasa de Credito Nafin-Solicitud Cotizacion - Consulta Solicitud
							"    AND cs.IC_SOLIC_ESP in (");
		int i=0;
		for (Iterator it = ids.iterator(); it.hasNext(); i++) {
        	if(i>0) {
				qrySentencia.append(",");
			}
			qrySentencia.append(" "+it.next().toString()+" ");
		}
    	qrySentencia.append(")");
		qrySentencia.append(" ORDER BY cs.ic_solic_esp ");
		System.out.println("CotizacionIf:getDocumentSummaryQueryForIds: El query queda de la siguiente manera:\n"+qrySentencia.toString());
		return qrySentencia.toString();
	}
	
	public String getDocumentQuery(HttpServletRequest request) {
    	
    	StringBuffer qrySentencia = new StringBuffer();
    	String sCondicion = "";
    	
		String fechaInicio = (request.getParameter("fechaInicio")==null)?"":request.getParameter("fechaInicio").trim();
		String fechaFinal = (request.getParameter("fechaFinal")==null)?"":request.getParameter("fechaFinal").trim();
		String numeroSolicitud = (request.getParameter("numeroSolicitud")==null)?"":request.getParameter("numeroSolicitud").trim();
		String moneda = (request.getParameter("moneda")==null)?"":request.getParameter("moneda").trim();
		String montoInicio = (request.getParameter("montoInicio")==null)?"":request.getParameter("montoInicio").trim();
		String montoFinal = (request.getParameter("montoFinal")==null)?"":request.getParameter("montoFinal").trim();
		String estatus = (request.getParameter("estatus")==null)?"":request.getParameter("estatus").trim();
		String ic_if = (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if").trim();
		String ic_usuario = (request.getParameter("ic_usuario")==null)?"":request.getParameter("ic_usuario").trim();
		String origen		= (request.getParameter("origen")==null)?"":request.getParameter("origen").trim();
				
		if(!ic_usuario.equals("")){//Para pantalla Ejecutivo-Tasa de Credito Nafin-Solicitud Cotizacion - Consulta Solicitud
			sCondicion += " and (ce.ic_usuario = '"+ic_usuario+"' or ce.ic_area in(select ic_area from cotcat_ejecutivo where ic_usuario = '"+ic_usuario+"' and cs_director = 'S'))";
		}		
		if(!fechaInicio.equals(""))
			sCondicion += " AND cs.df_solicitud BETWEEN TO_DATE ('" + fechaInicio + "','DD/MM/YYYY') AND TO_DATE('" + fechaFinal + "','DD/MM/YYYY')+1 ";
		/*if(!numeroSolicitud.equals(""))
			sCondicion += " AND (TO_CHAR(cs.df_solicitud,'YYYY') || TO_CHAR(cs.df_solicitud,'MM') || '-S' || cs.ic_solic_esp ) = '" + numeroSolicitud + "'";*/
		if(!numeroSolicitud.equals(""))
			sCondicion += " AND (TO_CHAR(cs.df_solicitud,'YYYY') || TO_CHAR(cs.df_solicitud,'MM') || '-' || cs.ig_num_solic ) = '" + numeroSolicitud + "'";						
		if(!moneda.equals(""))
			sCondicion += " AND cs.ic_moneda = " + moneda ;
		if(!montoInicio.equals(""))
			sCondicion += " AND cs.fn_monto BETWEEN " +montoInicio+ " AND "+montoFinal;
		if(!estatus.equals(""))
			sCondicion += " AND cs.ic_estatus in("+estatus+")";
		if(!ic_if.equals(""))
			sCondicion += " AND cs.ic_if = "+ic_if;
		if("CALCULADORAPESOSDOLARES".equals(origen)){
			sCondicion += " and ((cs.ic_moneda = 1 and cs.ic_tipo_tasa = 1) or (cs.ic_moneda = 54 and cs.ic_tipo_tasa in(1,2)))";
		}
		
		
		try {
			qrySentencia.append(" SELECT cs.IC_SOLIC_ESP "+
								"   FROM cot_solic_esp cs, "+
								"        cotcat_ejecutivo ce, "+
								"        cotcat_estatus e, "+
								"        comcat_moneda cm "+
								(!ic_usuario.equals("")?" ,comcat_if ci ":"")+//Para pantalla Ejecutivo-Tasa de Credito Nafin-Solicitud Cotizacion - Consulta Solicitud
								"  WHERE cs.ic_ejecutivo = ce.ic_ejecutivo "+
								"    AND cs.ic_estatus = e.ic_estatus "+
								(!ic_usuario.equals("")?" AND ci.ic_if(+)=cs.ic_if ":"")+////Para pantalla Ejecutivo-Tasa de Credito Nafin-Solicitud Cotizacion - Consulta Solicitud
								"    AND cs.ic_moneda = cm.ic_moneda "+sCondicion+" ORDER BY cs.ic_solic_esp ");
			System.out.println("getDocumentQuery:EL query para las claves es:\n"+qrySentencia.toString());
		} catch(Exception e){
			System.out.println("CotizacionIf::getDocumentQueryException "+e);
		}
		return qrySentencia.toString();
	}
	
	public String getDocumentQueryFile(HttpServletRequest request) {
    	StringBuffer qrySentencia = new StringBuffer();
    	String sCondicion = "";
    	
		String fechaInicio = (request.getParameter("fechaInicio")==null)?"":request.getParameter("fechaInicio").trim();
		String fechaFinal = (request.getParameter("fechaFinal")==null)?"":request.getParameter("fechaFinal").trim();
		String numeroSolicitud = (request.getParameter("numeroSolicitud")==null)?"":request.getParameter("numeroSolicitud").trim();
		String moneda = (request.getParameter("moneda")==null)?"":request.getParameter("moneda").trim();
		String montoInicio = (request.getParameter("montoInicio")==null)?"":request.getParameter("montoInicio").trim();
		String montoFinal = (request.getParameter("montoFinal")==null)?"":request.getParameter("montoFinal").trim();
		String estatus = (request.getParameter("estatus")==null)?"":request.getParameter("estatus").trim();
		String ic_if = (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if").trim();
  	String ic_usuario = (request.getParameter("ic_usuario")==null)?"":request.getParameter("ic_usuario").trim();
   	String usuario_nafin = (request.getParameter("usuario_nafin")==null)?"":request.getParameter("usuario_nafin").trim();
		String origen		= (request.getParameter("origen")==null)?"":request.getParameter("origen").trim();


		if(!ic_usuario.equals("")){//Para pantalla Ejecutivo-Tasa de Credito Nafin-Solicitud Cotizacion - Consulta Solicitud
			sCondicion += " and (ce.ic_usuario = '"+ic_usuario+"' or ce.ic_area in(select ic_area from cotcat_ejecutivo where ic_usuario = '"+ic_usuario+"' and cs_director = 'S'))";
		}		
			
		if(!fechaInicio.equals(""))
			sCondicion += " AND cs.df_solicitud BETWEEN TO_DATE ('" + fechaInicio + "','DD/MM/YYYY') AND TO_DATE('" + fechaFinal + "','DD/MM/YYYY')+1 ";
		/*if(!numeroSolicitud.equals(""))
			sCondicion += " AND (TO_CHAR(cs.df_solicitud,'YYYY') || TO_CHAR(cs.df_solicitud,'MM') || '-S' || cs.ic_solic_esp ) = '" + numeroSolicitud + "'";*/
		if(!numeroSolicitud.equals(""))
			sCondicion += " AND (TO_CHAR(cs.df_solicitud,'YYYY') || TO_CHAR(cs.df_solicitud,'MM') || '-' || cs.ig_num_solic ) = '" + numeroSolicitud + "'";			
		if(!moneda.equals(""))
			sCondicion += " AND cs.ic_moneda = " + moneda ;
		if(!montoInicio.equals(""))
			sCondicion += " AND cs.fn_monto BETWEEN " +montoInicio+ " AND "+montoFinal;
		if(!estatus.equals(""))
			sCondicion += " AND cs.ic_estatus in("+estatus+")";
		if(!ic_if.equals(""))
			sCondicion += " AND cs.ic_if = "+ic_if;
		if("CALCULADORAPESOSDOLARES".equals(origen)){
			sCondicion += " and ((cs.ic_moneda = 1 and cs.ic_tipo_tasa = 1) or (cs.ic_moneda = 54 and cs.ic_tipo_tasa in(1,2)))";
		}

			
		try {
			qrySentencia.append(
							" SELECT distinct cs.ic_solic_esp as ic_solic_esp,TO_CHAR(cs.df_solicitud,'DD/MM/YYYY') AS fecha_solicitud, "+
							"		 TO_CHAR(cs.df_cotizacion,'DD/MM/YYYY  HH24:MI:SS') AS fecha_cotizacion, "+
							"        (TO_CHAR(cs.df_solicitud,'YYYY') || TO_CHAR(cs.df_solicitud,'MM') "+
							//"|| '-S' || cs.ic_solic_esp  "+
							"         || '-' || cs.ig_num_solic "+							
							"        ) AS numero_solicitud, "+
							"        cm.cd_nombre AS moneda, "+
							"        (ce.cg_nombre || ' ' || ce.cg_appaterno || ' ' || ce.cg_apmaterno "+
							"        ) AS ejecutivo, "+
							"        cs.fn_monto AS monto, cs.ig_plazo AS plazo, "+
							"        e.cg_descripcion AS estatus, cs.ig_tasa_md tasa_mismo_dia, "+
							"        cs.ig_tasa_spot AS tasa_spot, cs.ig_num_referencia AS num_referencia, "+
							"        cs.cg_causa_rechazo AS causa_rechazo, "+
							"        cs.cg_observaciones_sirac AS obser_oper, "+
							"		 cs.ic_estatus as ic_estatus, "+
							"		 DECODE(cs.cs_aceptado_md,'S',cs.Ig_tasa_md,cs.ig_tasa_spot) as tasa_confirmada "+
							((!ic_usuario.equals("")||usuario_nafin.equals("S"))?" ,DECODE(cs.ic_if,null,cs.CG_RAZON_SOCIAL_IF,ci.CG_RAZON_SOCIAL) as CG_RAZON_SOCIAL ":"")+//Para pantalla [Ejecutivo | Nafin]-Tasa de Credito Nafin-Solicitud Cotizacion - Consulta Solicitud
							" 	,cs.ic_moneda"+
							"		,cs.ic_tipo_tasa"+
							"  ,case "+
						    "     when ie.cg_solicitar_aut = 'T' then 'SI' "+
						    "     when ie.cg_solicitar_aut = 'N' then 'NO' "+
						    "     when ie.cg_solicitar_aut = 'M' and cs.fn_monto >= ie.fn_monto_mayor_a then 'SI' "+
						    "     else 'NO' "+
					    	"   end as solicitar_aut "+
							"		,decode(cs.CG_TIPO_COTIZACION,'I','Indicativa','F','En Firme','') as tipoCotiza "+
							"   ,cs.cg_ut_ppc"+
							"   ,cs.cg_ut_ppi"+
							"   ,cs.ic_esquema_recup"+	
							"	,cs.cs_linea "+	
							" 	,TO_CHAR (cs.df_solicitud, 'HH24:MI') AS hora_solicitud"   +
							" 	,TO_CHAR (cs.df_cotizacion,'HH24:MI') AS hora_cotizacion"   +
							" 	,TO_CHAR (cs.df_aceptacion,'HH24:MI') AS hora_enfirme"  +							
							"   FROM cot_solic_esp cs, "+
							"        cotcat_ejecutivo ce, "+
							"        cotcat_estatus e, "+
							"        comcat_moneda cm, "+
							"				 cotrel_if_ejecutivo ie"+
							((!ic_usuario.equals("")||usuario_nafin.equals("S"))?" ,comcat_if ci ":"")+//Para pantalla [Ejecutivo | Nafin]-Tasa de Credito Nafin-Solicitud Cotizacion - Consulta Solicitud
							"  WHERE cs.ic_ejecutivo = ce.ic_ejecutivo "+
							"    AND cs.ic_estatus = e.ic_estatus "+
							"    AND cs.ic_moneda = cm.ic_moneda "+
							" 	 AND cs.ic_if = ie.ic_if(+)" +
							((!ic_usuario.equals("")||usuario_nafin.equals("S"))?" AND ci.IC_IF(+) = cs.IC_IF ":"")+//Para pantalla [Ejecutivo | Nafin]-Tasa de Credito Nafin-Solicitud Cotizacion - Consulta Solicitud
							"    AND cs.ic_moneda = cm.ic_moneda "+sCondicion+" ORDER BY cs.ic_solic_esp ");
			System.out.println("CotizacionIf:getDocumentQueryFile: EL query para las claves es:\n"+qrySentencia.toString());
		} catch(Exception e){
			System.out.println("CotizacionIf::getDocumentQueryFile::getDocumentQueryException "+e);
		}
		return qrySentencia.toString();

	}


	 /*****************************************************************************
	*										MIGRACION												 *
	*										garellano												 *
	*                         	 19/junio/2012										 		 *  
	****************************************************************************/
	public  StringBuffer strQuery;
	private List conditions = new ArrayList(); 
	private String fechaInicio;
	private String fechaFinal;
	private	String numeroSolicitud;
	private	String moneda;
	private String montoInicio;
	private	String montoFinal;
	private	String estatus;
	private	String ic_if;
	private	String ic_usuario;
	private	String origen;
  private String usuario_nafin;
	
	/**     
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */
	public String getAggregateCalculationQuery() { // Obtiene los totales 
		// No tiene totales.
		return "select 1 from dual";
 	}  
	/**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */	 
	public String getDocumentQuery(){  // para las llaves primarias		
		StringBuffer qrySentencia = new StringBuffer();
    	String sCondicion = "";
      
      if(!ic_usuario.equals("")){//Para pantalla Ejecutivo-Tasa de Credito Nafin-Solicitud Cotizacion - Consulta Solicitud
			sCondicion += " and (ce.ic_usuario = '"+ic_usuario+"' or ce.ic_area in(select ic_area from cotcat_ejecutivo where ic_usuario = '"+ic_usuario+"' and cs_director = 'S'))";
		}		
		if(!fechaInicio.equals(""))
			sCondicion += " AND cs.df_solicitud BETWEEN TO_DATE ('" + fechaInicio + "','DD/MM/YYYY') AND TO_DATE('" + fechaFinal + "','DD/MM/YYYY')+1 ";
		/*if(!numeroSolicitud.equals(""))
			sCondicion += " AND (TO_CHAR(cs.df_solicitud,'YYYY') || TO_CHAR(cs.df_solicitud,'MM') || '-S' || cs.ic_solic_esp ) = '" + numeroSolicitud + "'";*/
		if(!numeroSolicitud.equals(""))
			sCondicion += " AND (TO_CHAR(cs.df_solicitud,'YYYY') || TO_CHAR(cs.df_solicitud,'MM') || '-' || cs.ig_num_solic ) = '" + numeroSolicitud + "'";						
		if(!moneda.equals(""))
			sCondicion += " AND cs.ic_moneda = " + moneda ;
		if(!montoInicio.equals("")){
			sCondicion += " AND cs.fn_monto BETWEEN " +montoInicio+ " AND "+montoFinal;
         
      }
		if(!estatus.equals(""))
			sCondicion += " AND cs.ic_estatus in("+estatus+")";
		if(!ic_if.equals(""))
			sCondicion += " AND cs.ic_if = "+ic_if;
		if("CALCULADORAPESOSDOLARES".equals(origen)){
			sCondicion += " and ((cs.ic_moneda = 1 and cs.ic_tipo_tasa = 1) or (cs.ic_moneda = 54 and cs.ic_tipo_tasa in(1,2)))";
		}
		
		
		try {
			qrySentencia.append(" SELECT cs.IC_SOLIC_ESP "+
								"   FROM cot_solic_esp cs, "+
								"        cotcat_ejecutivo ce, "+
								"        cotcat_estatus e, "+
								"        comcat_moneda cm "+
								(!ic_usuario.equals("")?" ,comcat_if ci ":"")+//Para pantalla Ejecutivo-Tasa de Credito Nafin-Solicitud Cotizacion - Consulta Solicitud
								"  WHERE cs.ic_ejecutivo = ce.ic_ejecutivo "+
								"    AND cs.ic_estatus = e.ic_estatus "+
								(!ic_usuario.equals("")?" AND ci.ic_if(+)=cs.ic_if ":"")+////Para pantalla Ejecutivo-Tasa de Credito Nafin-Solicitud Cotizacion - Consulta Solicitud
								"    AND cs.ic_moneda = cm.ic_moneda "+sCondicion+" ORDER BY cs.ic_solic_esp ");
			System.out.println("getDocumentQuery:EL query para las claves es:\n"+qrySentencia.toString());
		} catch(Exception e){
			System.out.println("CotizacionIf::getDocumentQueryException "+e);
		}
      
      System.err.println(qrySentencia);
      
		return qrySentencia.toString();
 	}  
	   
	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */	  		
	public String getDocumentSummaryQueryForIds(List pageIds){ // paginacion 			
		StringBuffer qrySentencia = new StringBuffer();

	/*usuario_nafin ==> para pantalla Nafin-Tasa de Credito Nafin-Solicitud Cotizacion - Consulta Solicitud*/
	   /* usuario_nafin permitira obtener La descripcion de los IFS que se despliegan en el layout, aun que no se haya seleccionado Un ejecutivo del Combo, 
	   esto es solo para el caso de la consultas del usuario Nafin, no se utiliza
	   como un filtro, mas bien es una bandera que le idicara a la clase de la paginacion
	   CotizacionIf.java que se trata de una consulta de el usuario nafin, para que forme el query de consulta que incluya el campo de 
	   descripcion de IF.*/
    	
		qrySentencia.append(
							" SELECT distinct cs.ic_solic_esp as ic_solic_esp,TO_CHAR(cs.df_solicitud,'DD/MM/YYYY') AS fecha_solicitud, "+
							"		 TO_CHAR(cs.df_cotizacion,'DD/MM/YYYY  HH24:MI:SS') AS fecha_cotizacion, "+
							"        (TO_CHAR(cs.df_solicitud,'YYYY') || TO_CHAR(cs.df_solicitud,'MM') "+
							//"|| '-S' || cs.ic_solic_esp  "+
							"         || '-' || cs.ig_num_solic "+							
							"        ) AS numero_solicitud, "+
							"        cm.cd_nombre AS moneda, "+
							"        (ce.cg_nombre || ' ' || ce.cg_appaterno || ' ' || ce.cg_apmaterno "+
							"        ) AS ejecutivo, "+
							"        cs.fn_monto AS monto, cs.ig_plazo AS plazo, "+
							"        e.cg_descripcion AS estatus, cs.ig_tasa_md tasa_mismo_dia, "+
							"        cs.ig_tasa_spot AS tasa_spot, cs.ig_num_referencia AS num_referencia, "+
							"        cs.cg_causa_rechazo AS causa_rechazo, "+
							"        cs.cg_observaciones_sirac AS obser_oper, "+
							"		 cs.ic_estatus as ic_estatus, "+
							"		 DECODE(cs.cs_aceptado_md,'S',cs.Ig_tasa_md,cs.ig_tasa_spot) as tasa_confirmada "+
							((!ic_usuario.equals("")||usuario_nafin.equals("S"))?" ,DECODE(cs.ic_if,null,cs.CG_RAZON_SOCIAL_IF,ci.CG_RAZON_SOCIAL) as CG_RAZON_SOCIAL ":"")+//Para pantalla [Ejecutivo | Nafin]-Tasa de Credito Nafin-Solicitud Cotizacion - Consulta Solicitud
							" 	,cs.ic_moneda"+
							"		,cs.ic_tipo_tasa"+
							"  ,case "+
					    "     when ie.cg_solicitar_aut = 'T' then 'SI' "+
					    "     when ie.cg_solicitar_aut = 'N' then 'NO' "+
					    "     when ie.cg_solicitar_aut = 'M' and cs.fn_monto >= ie.fn_monto_mayor_a then 'SI' "+
					    "     else 'NO' "+
					    "   end as solicitar_aut "+
							"		,decode(cs.CG_TIPO_COTIZACION,'I','Indicativa','F','En Firme','') as tipoCotiza "+
							"   ,cs.cg_ut_ppc"+
							"   ,cs.cg_ut_ppi"+
							"   ,cs.ic_esquema_recup"+	
							"	,cs.cs_linea "+					
							"   FROM cot_solic_esp cs, "+
							"        cotcat_ejecutivo ce, "+
							"        cotcat_estatus e, "+
							"        comcat_moneda cm, "+
							"				 cotrel_if_ejecutivo ie"+
							((!ic_usuario.equals("")||usuario_nafin.equals("S"))?" ,comcat_if ci ":"")+//Para pantalla [Ejecutivo | Nafin]-Tasa de Credito Nafin-Solicitud Cotizacion - Consulta Solicitud
							"  WHERE cs.ic_ejecutivo = ce.ic_ejecutivo "+
							"    AND cs.ic_estatus = e.ic_estatus "+
							"    AND cs.ic_moneda = cm.ic_moneda "+
							" 	 AND cs.ic_if = ie.ic_if(+)" +
							((!ic_usuario.equals("")||usuario_nafin.equals("S"))?" AND ci.IC_IF(+) = cs.IC_IF ":"")+//Para pantalla [Ejecutivo | Nafin]-Tasa de Credito Nafin-Solicitud Cotizacion - Consulta Solicitud
							"    AND cs.IC_SOLIC_ESP in (");
    
      for(int i = 0; i < pageIds.size(); i++){
          List lItem = (ArrayList)pageIds.get(i);
          if(i>0){ qrySentencia.append(","); }
          qrySentencia.append("'"+lItem.get(0).toString()+"'");
        }

    	qrySentencia.append(")");
		qrySentencia.append(" ORDER BY cs.ic_solic_esp ");
		System.out.println("CotizacionIf:getDocumentSummaryQueryForIds: El query queda de la siguiente manera:\n"+qrySentencia.toString());
		return qrySentencia.toString();
		
 	}  
	
	public String getDocumentQueryFile(){ // para todos los registros				
		StringBuffer qrySentencia = new StringBuffer();
    	String sCondicion = "";
      if(!ic_usuario.equals("")){//Para pantalla Ejecutivo-Tasa de Credito Nafin-Solicitud Cotizacion - Consulta Solicitud
			sCondicion += " and (ce.ic_usuario = '"+ic_usuario+"' or ce.ic_area in(select ic_area from cotcat_ejecutivo where ic_usuario = '"+ic_usuario+"' and cs_director = 'S'))";
		}		
			
		if(!fechaInicio.equals(""))
			sCondicion += " AND cs.df_solicitud BETWEEN TO_DATE ('" + fechaInicio + "','DD/MM/YYYY') AND TO_DATE('" + fechaFinal + "','DD/MM/YYYY')+1 ";
		/*if(!numeroSolicitud.equals(""))
			sCondicion += " AND (TO_CHAR(cs.df_solicitud,'YYYY') || TO_CHAR(cs.df_solicitud,'MM') || '-S' || cs.ic_solic_esp ) = '" + numeroSolicitud + "'";*/
		if(!numeroSolicitud.equals(""))
			sCondicion += " AND (TO_CHAR(cs.df_solicitud,'YYYY') || TO_CHAR(cs.df_solicitud,'MM') || '-' || cs.ig_num_solic ) = '" + numeroSolicitud + "'";			
		if(!moneda.equals(""))
			sCondicion += " AND cs.ic_moneda = " + moneda ;
		if(!montoInicio.equals(""))
			sCondicion += " AND cs.fn_monto BETWEEN " +montoInicio+ " AND "+montoFinal;
		if(!estatus.equals(""))
			sCondicion += " AND cs.ic_estatus in("+estatus+")";
		if(!ic_if.equals(""))
			sCondicion += " AND cs.ic_if = "+ic_if;
		if("CALCULADORAPESOSDOLARES".equals(origen)){
			sCondicion += " and ((cs.ic_moneda = 1 and cs.ic_tipo_tasa = 1) or (cs.ic_moneda = 54 and cs.ic_tipo_tasa in(1,2)))";
		}

			
		try {
			qrySentencia.append(
							" SELECT distinct cs.ic_solic_esp as ic_solic_esp,TO_CHAR(cs.df_solicitud,'DD/MM/YYYY') AS fecha_solicitud, "+
							"		 TO_CHAR(cs.df_cotizacion,'DD/MM/YYYY  HH24:MI:SS') AS fecha_cotizacion, "+
							"        (TO_CHAR(cs.df_solicitud,'YYYY') || TO_CHAR(cs.df_solicitud,'MM') "+
							//"|| '-S' || cs.ic_solic_esp  "+
							"         || '-' || cs.ig_num_solic "+							
							"        ) AS numero_solicitud, "+
							"        cm.cd_nombre AS moneda, "+
							"        (ce.cg_nombre || ' ' || ce.cg_appaterno || ' ' || ce.cg_apmaterno "+
							"        ) AS ejecutivo, "+
							"        cs.fn_monto AS monto, cs.ig_plazo AS plazo, "+
							"        e.cg_descripcion AS estatus, cs.ig_tasa_md tasa_mismo_dia, "+
							"        cs.ig_tasa_spot AS tasa_spot, cs.ig_num_referencia AS num_referencia, "+
							"        cs.cg_causa_rechazo AS causa_rechazo, "+
							"        cs.cg_observaciones_sirac AS obser_oper, "+
							"		 cs.ic_estatus as ic_estatus, "+
							"		 DECODE(cs.cs_aceptado_md,'S',cs.Ig_tasa_md,cs.ig_tasa_spot) as tasa_confirmada "+
							((!ic_usuario.equals("")||usuario_nafin.equals("S"))?" ,DECODE(cs.ic_if,null,cs.CG_RAZON_SOCIAL_IF,ci.CG_RAZON_SOCIAL) as CG_RAZON_SOCIAL ":"")+//Para pantalla [Ejecutivo | Nafin]-Tasa de Credito Nafin-Solicitud Cotizacion - Consulta Solicitud
							" 	,cs.ic_moneda"+
							"		,cs.ic_tipo_tasa"+
							"  ,case "+
						    "     when ie.cg_solicitar_aut = 'T' then 'SI' "+
						    "     when ie.cg_solicitar_aut = 'N' then 'NO' "+
						    "     when ie.cg_solicitar_aut = 'M' and cs.fn_monto >= ie.fn_monto_mayor_a then 'SI' "+
						    "     else 'NO' "+
					    	"   end as solicitar_aut "+
							"		,decode(cs.CG_TIPO_COTIZACION,'I','Indicativa','F','En Firme','') as tipoCotiza "+
							"   ,cs.cg_ut_ppc"+
							"   ,cs.cg_ut_ppi"+
							"   ,cs.ic_esquema_recup"+	
							"	,cs.cs_linea "+	
							" 	,TO_CHAR (cs.df_solicitud, 'HH24:MI') AS hora_solicitud"   +
							" 	,TO_CHAR (cs.df_cotizacion,'HH24:MI') AS hora_cotizacion"   +
							" 	,TO_CHAR (cs.df_aceptacion,'HH24:MI') AS hora_enfirme"  +							
							"   FROM cot_solic_esp cs, "+
							"        cotcat_ejecutivo ce, "+
							"        cotcat_estatus e, "+
							"        comcat_moneda cm, "+
							"				 cotrel_if_ejecutivo ie"+
							((!ic_usuario.equals("")||usuario_nafin.equals("S"))?" ,comcat_if ci ":"")+//Para pantalla [Ejecutivo | Nafin]-Tasa de Credito Nafin-Solicitud Cotizacion - Consulta Solicitud
							"  WHERE cs.ic_ejecutivo = ce.ic_ejecutivo "+
							"    AND cs.ic_estatus = e.ic_estatus "+
							"    AND cs.ic_moneda = cm.ic_moneda "+
							" 	 AND cs.ic_if = ie.ic_if(+)" +
							((!ic_usuario.equals("")||usuario_nafin.equals("S"))?" AND ci.IC_IF(+) = cs.IC_IF ":"")+//Para pantalla [Ejecutivo | Nafin]-Tasa de Credito Nafin-Solicitud Cotizacion - Consulta Solicitud
							"    AND cs.ic_moneda = cm.ic_moneda "+sCondicion+" ORDER BY cs.ic_solic_esp ");
			System.out.println("CotizacionIf:getDocumentQueryFile: EL query para las claves es:\n"+qrySentencia.toString());
		} catch(Exception e){
			System.out.println("CotizacionIf::getDocumentQueryFile::getDocumentQueryException "+e);
		}
		return qrySentencia.toString();
 	}     
	         
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	///para formar el csv o pdf de los todos los registros
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";  
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		CreaArchivo 	archivo 			= new CreaArchivo();

			if ("XLS".equals(tipo)) {
				try {
					int registros = 0;	
					while(rs.next()){
						if(registros==0) { 
							contenidoArchivo.append("Nombre del proveedor, Numero del proveedor, Numero de Transferencia, Fecha de pago, Montonto total de la transferencia, Moneda, Fecha de la factura, Monto de la factura, Fecha de operacion");
						}
						registros++;
						String nomproveedor = rs.getString("nomproveedor")==null?"":rs.getString("nomproveedor");
						String numproveedor = rs.getString("numproveedor")==null?"":rs.getString("numproveedor");
						String numtrans = rs.getString("numtrans")==null?"":rs.getString("numtrans");
						String fechapago = rs.getString("fechapago")==null?"":rs.getString("fechapago");
						String montocheque = rs.getString("montocheque")==null?"":rs.getString("montocheque");
						String moneda = rs.getString("moneda")==null?"":rs.getString("moneda");
						String fechafactura = rs.getString("fechafactura")==null?"":rs.getString("fechafactura");
						String montofactura = rs.getString("montofactura")==null?"":rs.getString("montofactura");
						String fechaoperacion = rs.getString("fechaoperacion")==null?"":rs.getString("fechaoperacion");
							
						contenidoArchivo.append("\n"+
							nomproveedor.replaceAll(",","") + "," +
							numproveedor + "," +
							numtrans + "," +
							fechapago.replaceAll("-","/") + "," +
							"$ " +  Comunes.formatoDecimal(montocheque, 2, false) + "," +
							moneda + "," +
							fechafactura.replaceAll("-","/") + "," +
							"$ " + Comunes.formatoDecimal(montofactura, 2, false) + "," +
							fechaoperacion.replaceAll("-","/") + ",");
					}
					
					if(registros >= 1){
						if(!archivo.make(contenidoArchivo.toString(),path, ".csv")){
							nombreArchivo="ERROR";
						}else{
							nombreArchivo = archivo.nombre;
						}
					}	 
				} catch (Throwable e) {
						throw new AppException("Error al generar el archivo", e);
				} finally {
					try {
						rs.close();
					} catch(Exception e) {}
				}
			}else if ("PDF".equals(tipo)) {
				try {
					HttpSession session = request.getSession();
					nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
					ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					
					String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
					String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
					String diaActual    = fechaActual.substring(0,2);
					String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
					String anioActual   = fechaActual.substring(6,10);
					String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
							
					pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					session.getAttribute("iNoNafinElectronico").toString(),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
							
					pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
					pdfDoc.setTable(9, 80);
					pdfDoc.setCell("Nombre del Proveedor","celda01",ComunesPDF.LEFT);
					pdfDoc.setCell("Numero del Proveedor","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Numero de Transferencia","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Pagado","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Total de la Transferencia","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de la Factura","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto de la Factura","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Operacion","celda01",ComunesPDF.CENTER);
													
					while (rs.next()) {
						String nombreproveedor = rs.getString("nomproveedor")==null?"":rs.getString("nomproveedor");
						String numeroproveedor = rs.getString("numproveedor")==null?"":rs.getString("numproveedor");
						String numtrans = rs.getString("numtrans")==null?"":rs.getString("numtrans");
						String fechapago = rs.getString("fechapago")==null?"":rs.getString("fechapago");
						String montocheque = rs.getString("montocheque")==null?"":rs.getString("montocheque");
						String moneda = rs.getString("moneda")==null?"":rs.getString("moneda");
						String fechafactura = rs.getString("fechafactura")==null?"":rs.getString("fechafactura");
						String montofactura = rs.getString("montofactura")==null?"":rs.getString("montofactura");
						String fechaoperacion = rs.getString("fechaoperacion")==null?"":rs.getString("fechaoperacion");
						 
						pdfDoc.setCell(nombreproveedor,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(numeroproveedor,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(numtrans,"formas",ComunesPDF.CENTER);		
						pdfDoc.setCell(fechapago,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(montocheque, 2, false),"formas",ComunesPDF.CENTER);		
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechafactura,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ " + Comunes.formatoDecimal(montocheque, 2, false),"formas",ComunesPDF.CENTER);	
						pdfDoc.setCell(fechaoperacion,"formas",ComunesPDF.CENTER);				
					}
					pdfDoc.addTable();
					pdfDoc.endDocument();
				} catch(Throwable e) {
					throw new AppException("Error al generar el archivo", e);
				} finally {
					try {
						rs.close();
					} catch(Exception e) {}
				}
			}
		return nombreArchivo;	
	}	
	
	//para formar  csv o pdf por pagina  15 registros	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		StringBuffer contenidoArchivo = new StringBuffer();
		CreaArchivo archivo 	= new CreaArchivo();
		
		if ("PDF".equals(tipo)) {   
			int TOTAL_CAMPOS = 18;
			int nRow = 0, numMon = 0, numDL = 0;
			double totSaldoInsMon = 0.0;
			double totAmortizMon = 0.0;
			double totInteresMon = 0.0;
			double totDescFopMon = 0.0;
			double totDescFinapeMon	= 0.0;
			double totVentoMon = 0.0;
			double totAdeudoTotMon = 0.0;
			double totSaldoInsNvoMon = 0.0;
			double totExigibleMon = 0.0;
			double totSaldoInsDL = 0.0;
			double totAmortizDL = 0.0;
			double totInteresDL = 0.0;
			double totDescFopDL = 0.0;
			double totDescFinapeDL	= 0.0;
			double totVentoDL = 0.0;
			double totAdeudoTotDL = 0.0;
			double totSaldoInsNvoDL = 0.0;
			double totExigibleDL = 0.0;
			int numCols = 0;
			float anchos[] = null;
			
			
			
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
						
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				

				while (rs.next()) {
					Vector vecDatos = new Vector();	   
					for(int i=1; i<=TOTAL_CAMPOS; i++) {
						vecDatos.add(rs.getString(i));
					}
					
					String numeroDocumento = rs.getString("numero_documento")==null?"":rs.getString("numero_documento");//FODEA 015 - 2009 ACF
					if(rs.getString(3).equals("54")){
						numDL++;
						totSaldoInsDL		+= Double.valueOf(rs.getString(8)).doubleValue();
						totAmortizDL		+= Double.valueOf(rs.getString(9)).doubleValue();
						totInteresDL		+= Double.valueOf(rs.getString(10)).doubleValue();
						totDescFopDL		+= Double.valueOf(rs.getString(11)).doubleValue();
						totDescFinapeDL	+= Double.valueOf(rs.getString(12)).doubleValue();
						totVentoDL			+= Double.valueOf(rs.getString(13)).doubleValue();
						totAdeudoTotDL		+= Double.valueOf(rs.getString(14)).doubleValue();
						totSaldoInsNvoDL	+= Double.valueOf(rs.getString(15)).doubleValue();
						totExigibleDL		+= Double.valueOf(rs.getString("fg_totalexigible")).doubleValue();
					} else {
						numMon++;
						totSaldoInsMon		+= Double.valueOf(rs.getString(8)).doubleValue();
						totAmortizMon		+= Double.valueOf(rs.getString(9)).doubleValue();
						totInteresMon		+= Double.valueOf(rs.getString(10)).doubleValue();
						totDescFopMon		+= Double.valueOf(rs.getString(11)).doubleValue();
						totDescFinapeMon	+= Double.valueOf(rs.getString(12)).doubleValue();
						totVentoMon			+= Double.valueOf(rs.getString(13)).doubleValue();
						totAdeudoTotMon	+= Double.valueOf(rs.getString(14)).doubleValue();
						totSaldoInsNvoMon	+= Double.valueOf(rs.getString(15)).doubleValue();
						totExigibleMon		+= Double.valueOf(rs.getString("fg_totalexigible")).doubleValue();
					}
					
					
					nRow++;
				}
				
				
				pdfDoc.addTable();
				pdfDoc.endDocument();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					//rs.close();
				} catch(Exception e) {}
			}
		}		
		return nombreArchivo;
	}
	
	public List getConditions() {
		return conditions;
	}


  public void setFechaInicio(String fechaInicio) {
    this.fechaInicio = fechaInicio;
  }


  public void setFechaFinal(String fechaFinal) {
    this.fechaFinal = fechaFinal;
  }


  public void setNumeroSolicitud(String numeroSolicitud) {
    this.numeroSolicitud = numeroSolicitud;
  }


  public void setMoneda(String moneda) {
    this.moneda = moneda;
  }


  public void setMontoInicio(String montoInicio) {
    this.montoInicio = montoInicio;
  }


  public void setMontoFinal(String montoFinal) {
    this.montoFinal = montoFinal;
  }


  public void setEstatus(String estatus) {
    this.estatus = estatus;
  }


  public void setIc_if(String ic_if) {
    this.ic_if = ic_if;
  }


  public void setIc_usuario(String ic_usuario) {
    this.ic_usuario = ic_usuario;
  }


  public void setOrigen(String origen) {
    this.origen = origen;
  }


  public void setUsuario_nafin(String usuario_nafin) {
    this.usuario_nafin = usuario_nafin;
  }
}