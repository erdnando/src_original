package com.netro.cotizador;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import netropology.utilerias.AppException;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import com.netro.pdf.ComunesPDF;
import java.util.HashMap;
import java.io.Writer;
import netropology.utilerias.Fecha;
import netropology.utilerias.usuarios.UtilUsr;
import netropology.utilerias.usuarios.Usuario;

import org.apache.commons.logging.Log;

public class CreditosConfirmadosArchivos implements IQueryGeneratorRegExtJS{

	private final static Log log = ServiceLocator.getInstance().getLog(CreditosConfirmadosArchivos.class);
	private List   conditions;
	private String mensaje1;
	private String mensaje2;
	private String mensaje3;
	private String mensaje4; // Observaciones
	private JSONArray listaDatosCotizacion;
	private JSONArray listaEjecutivoNafin;
	private JSONArray listaIntermediario;
	private JSONArray listaOperacion;
	private JSONArray listaDetalleDisp;
	private JSONArray listaPlanPagos;
	private JSONArray listaTasaIndicativa;
	private JSONArray listaTasaConfirmada;

	public String getDocumentQuery(){
		return null;
	}
	public String getAggregateCalculationQuery(){
		return null;
	}
	public String getDocumentSummaryQueryForIds(List ids){
		return null;
	}
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){
		return null;
	}

	public String getDocumentQueryFile(){
		StringBuffer qrySentencia = new StringBuffer();
		conditions = new ArrayList();
		qrySentencia.append("SELECT TO_CHAR(SYSDATE,'dd/mm/rrrr') FROM DUAL");
		return qrySentencia.toString();
	}


	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){

		log.info("crearCustomFile(E)");

		String nombreArchivo = "";
		ComunesPDF pdfDoc = new ComunesPDF();
		HttpSession session  = request.getSession();

		try{

			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);

			String meses[]     = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual   = fechaActual.substring(0,2);
			String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual  = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

			if(this.listaDatosCotizacion.size() > 0){
				pdfDoc.setTable(2, 65);
				pdfDoc.setCell("Datos de la Cotizaci�n", "celda01", ComunesPDF.LEFT, 2);
				for(int i = 0; i < this.listaDatosCotizacion.size(); i++){
					JSONObject auxiliar = this.listaDatosCotizacion.getJSONObject(i);
					pdfDoc.setCell((String)auxiliar.getString("ETIQUETA"),    "formas", ComunesPDF.LEFT);
					pdfDoc.setCell((String)auxiliar.getString("DESCRIPCION"), "formas", ComunesPDF.LEFT);
				}
				if(!"".equals(this.mensaje1)){
					pdfDoc.setCell(this.mensaje1, "formas", ComunesPDF.CENTER, 2);
				}
				pdfDoc.addTable();
			}
			if(this.listaEjecutivoNafin.size() > 0){
				pdfDoc.setTable(2, 80);
				pdfDoc.setCell("Datos del Ejecutivo Nafin", "celda01", ComunesPDF.LEFT, 2);
				for(int i = 0; i < this.listaEjecutivoNafin.size(); i++){
					JSONObject auxiliar = this.listaEjecutivoNafin.getJSONObject(i);
					pdfDoc.setCell((String)auxiliar.getString("ETIQUETA"),    "formas", ComunesPDF.LEFT);
					pdfDoc.setCell((String)auxiliar.getString("DESCRIPCION"), "formas", ComunesPDF.LEFT);
				}
				pdfDoc.addTable();
			}
			if(this.listaIntermediario.size() > 0){
				pdfDoc.setTable(2, 80);
				pdfDoc.setCell("Datos del Intermediario Financiero/Acreditado", "celda01", ComunesPDF.LEFT, 2);
				for(int i = 0; i < this.listaIntermediario.size(); i++){
					JSONObject auxiliar = this.listaIntermediario.getJSONObject(i);
					pdfDoc.setCell((String)auxiliar.getString("ETIQUETA"),    "formas", ComunesPDF.LEFT);
					pdfDoc.setCell((String)auxiliar.getString("DESCRIPCION"), "formas", ComunesPDF.LEFT);
				}
				pdfDoc.addTable();
			}
			if(this.listaOperacion.size() > 0){
				pdfDoc.setTable(2, 80);
				pdfDoc.setCell("Caracter�sticas de la Operaci�n", "celda01", ComunesPDF.LEFT, 2);
				for(int i = 0; i < this.listaOperacion.size(); i++){
					JSONObject auxiliar = this.listaOperacion.getJSONObject(i);
					pdfDoc.setCell((String)auxiliar.getString("ETIQUETA"),    "formas", ComunesPDF.LEFT);
					pdfDoc.setCell((String)auxiliar.getString("DESCRIPCION"), "formas", ComunesPDF.LEFT);
				}
				pdfDoc.addTable();
			}
			if(this.listaDetalleDisp.size() > 0){
				pdfDoc.setTable(4, 80);
				pdfDoc.setCell("Detalle de disposiciones", "celda01", ComunesPDF.LEFT, 4);
				pdfDoc.setCell("N�mero de disposici�n",    "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Monto",                    "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de disposici�n",     "celda01", ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de vencimiento",     "celda01", ComunesPDF.CENTER);
				for(int i = 0; i < this.listaDetalleDisp.size(); i++){
					JSONObject auxiliar = this.listaDetalleDisp.getJSONObject(i);
					pdfDoc.setCell((String)auxiliar.getString("NUMERO_DISPOSICION"), "formas", ComunesPDF.CENTER);
					pdfDoc.setCell((String)auxiliar.getString("MONTO"),              "formas", ComunesPDF.CENTER);
					pdfDoc.setCell((String)auxiliar.getString("FECHA_DISPOSICION"),  "formas", ComunesPDF.CENTER);
					pdfDoc.setCell((String)auxiliar.getString("FECHA_VENCIMIENTO"),  "formas", ComunesPDF.CENTER);
				}
				pdfDoc.addTable();
			}
			if(this.listaPlanPagos.size() > 0){
				pdfDoc.setTable(4, 80);
				pdfDoc.setCell("Plan de Pagos Capital", "celda01", ComunesPDF.LEFT, 4);
				pdfDoc.setCell("N�mero de pago",        "celda01", ComunesPDF.CENTER );
				pdfDoc.setCell("Fecha de pago",         "celda01", ComunesPDF.CENTER );
				pdfDoc.setCell("Monto a pagar",         "celda01", ComunesPDF.CENTER );
				pdfDoc.setCell(" ",     "celda01", ComunesPDF.CENTER);
				for(int i = 0; i < this.listaPlanPagos.size(); i++){
					JSONObject auxiliar = this.listaPlanPagos.getJSONObject(i);
					pdfDoc.setCell((String)auxiliar.getString("NUMERO_PAGO"), "formas", ComunesPDF.CENTER);
					pdfDoc.setCell((String)auxiliar.getString("FECHA_PAGO"),  "formas", ComunesPDF.CENTER);
					pdfDoc.setCell((String)auxiliar.getString("MONTO_PAGAR"), "formas", ComunesPDF.CENTER);
					pdfDoc.setCell((String)auxiliar.getString("VACIO"),       "formas", ComunesPDF.CENTER);
				}
				pdfDoc.addTable();
			}
			if(!"".equals(this.mensaje2)){
				pdfDoc.setTable(1, 80);
				pdfDoc.setCell(this.mensaje2, "formas", ComunesPDF.CENTER);
				pdfDoc.addTable();
			}
			if(this.listaTasaIndicativa.size() > 0){
				pdfDoc.setTable(2, 80);
				pdfDoc.setCell("Tasa indicativa", "celda01", ComunesPDF.LEFT, 2);
				for(int i = 0; i < this.listaTasaIndicativa.size(); i++){
					JSONObject auxiliar = this.listaTasaIndicativa.getJSONObject(i);
					pdfDoc.setCell((String)auxiliar.getString("ETIQUETA"),    "formas", ComunesPDF.LEFT);
					pdfDoc.setCell((String)auxiliar.getString("DESCRIPCION"), "formas", ComunesPDF.LEFT);
				}
				pdfDoc.addTable();
			}
			if(this.listaTasaConfirmada.size() > 0){
				pdfDoc.setTable(2, 80);
				for(int i = 0; i < this.listaTasaConfirmada.size(); i++){
					JSONObject auxiliar = this.listaTasaConfirmada.getJSONObject(i);
					if(i == 0){
						String tmp = (String)auxiliar.getString("ETIQUETA");
						tmp = tmp.replaceAll("<b>","").replaceAll("</b>","");
						pdfDoc.setCell(tmp, "celda01", ComunesPDF.CENTER);
						tmp = (String)auxiliar.getString("DESCRIPCION");
						tmp = tmp.replaceAll("<b>","").replaceAll("</b>","");
						pdfDoc.setCell(tmp, "celda01", ComunesPDF.CENTER);
					} else{
						pdfDoc.setCell((String)auxiliar.getString("ETIQUETA"),    "formas", ComunesPDF.CENTER);
						pdfDoc.setCell((String)auxiliar.getString("DESCRIPCION"), "formas", ComunesPDF.CENTER);
					}
				}
				pdfDoc.addTable();
			}
			if(!"".equals(this.mensaje3)){
				pdfDoc.setTable(1, 80);
				pdfDoc.setCell(this.mensaje3, "formas", ComunesPDF.CENTER);
				pdfDoc.addTable();
			}
			if(!"".equals(this.mensaje4)){
				String tmp = this.mensaje4;
				tmp =  tmp.substring(26,this.mensaje4.length());
				pdfDoc.setTable(1, 80);
				pdfDoc.setCell("Observaciones", "celda01", ComunesPDF.LEFT  );
				pdfDoc.setCell(tmp,             "formas",  ComunesPDF.CENTER);
				pdfDoc.addTable();
			}

			pdfDoc.endDocument();

		} catch(Throwable e){
			throw new AppException("Error al generar el archivo ", e);
		}

		log.info("crearCustomFile(S)");
		return nombreArchivo;
	}

/*****************************************************
 *                GETTERS AND SETTERS                *
 *****************************************************/
	public List getConditions() {
		return conditions;
	}

	public String getMensaje1() {
		return mensaje1;
	}

	public void setMensaje1(String mensaje1) {
		this.mensaje1 = mensaje1;
	}

	public String getMensaje2() {
		return mensaje2;
	}

	public void setMensaje2(String mensaje2) {
		this.mensaje2 = mensaje2;
	}
	public String getMensaje3() {
		return mensaje3;
	}

	public void setMensaje3(String mensaje3) {
		this.mensaje3 = mensaje3;
	}
	public String getMensaje4() {
		return mensaje4;
	}

	public void setMensaje4(String mensaje4) {
		this.mensaje4 = mensaje4;
	}

	public JSONArray getListaDatosCotizacion() {
		return listaDatosCotizacion;
	}

	public void setListaDatosCotizacion(JSONArray listaDatosCotizacion) {
		this.listaDatosCotizacion = listaDatosCotizacion;
	}

	public JSONArray getListaEjecutivoNafin() {
		return listaEjecutivoNafin;
	}

	public void setListaEjecutivoNafin(JSONArray listaEjecutivoNafin) {
		this.listaEjecutivoNafin = listaEjecutivoNafin;
	}

	public JSONArray getListaIntermediario() {
		return listaIntermediario;
	}

	public void setListaIntermediario(JSONArray listaIntermediario) {
		this.listaIntermediario = listaIntermediario;
	}

	public JSONArray getListaOperacion() {
		return listaOperacion;
	}

	public void setListaOperacion(JSONArray listaOperacion) {
		this.listaOperacion = listaOperacion;
	}

	public JSONArray getListaDetalleDisp() {
		return listaDetalleDisp;
	}

	public void setListaDetalleDisp(JSONArray listaDetalleDisp) {
		this.listaDetalleDisp = listaDetalleDisp;
	}

	public JSONArray getListaPlanPagos() {
		return listaPlanPagos;
	}

	public void setListaPlanPagos(JSONArray listaPlanPagos) {
		this.listaPlanPagos = listaPlanPagos;
	}

	public JSONArray getListaTasaIndicativa() {
		return listaTasaIndicativa;
	}

	public void setListaTasaIndicativa(JSONArray listaTasaIndicativa) {
		this.listaTasaIndicativa = listaTasaIndicativa;
	}

	public JSONArray getListaTasaConfirmada() {
		return listaTasaConfirmada;
	}

	public void setListaTasaConfirmada(JSONArray listaTasaConfirmada) {
		this.listaTasaConfirmada = listaTasaConfirmada;
	}

}