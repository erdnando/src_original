package com.netro.cotizador;

import com.netro.exception.NafinException;
import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsultaSolicitudes  implements IQueryGeneratorRegExtJS{
	
 /**
  * Declaracion de variables
  */	
  private final static Log log = ServiceLocator.getInstance().getLog(ConsultaSolicitudes.class);
  private List 		  conditions;
  private StringBuffer qrySentencia 	= new StringBuffer();
  
  /**
	* Variables para pantalla: Tasas de Credito Nafin - Solicitud de Cotizacion - Consulta de Solicitudes
	*/
  private String fechaSolicitudInicial;
  private String fechaSolicitudFinal;
  private String moneda;
  private String numeroSolicitud;
  private String montoCreditoInicial;
  private String montoCreditoFinal;
  private String intermediarioFinanciero;
  private String estatus;
  private String personaResponsable;
  
  /**
	* Variables para pantalla: Tasas de Credito Nafin - Parametrizar Tesoreria - asignar Factores de Riesgo
	*/
	private String intermediario;
	private String tipoIntermedario;
	

	/**
	* Constructor by Default
	*/
	public ConsultaSolicitudes() { 	}
	
	/**
	* Obtiene solo Moneda Nacional y Dolar Americano 
	* @return Objeto de tipo Registros
	*/
	public Registros catalogoMoneda(){
		log.info("catalogoMoneda(E)");
		
		Registros registros  = new Registros();
		AccesoDB  		con 	= new AccesoDB(); 
		qrySentencia 			= new StringBuffer();
		
		try{
			con.conexionDB();
			qrySentencia.append(	"SELECT DISTINCT m.ic_moneda AS clave, m.cd_nombre AS descripcion \n"+
						"   FROM comcat_moneda m \n"+
						"   WHERE ic_moneda IN (1, 54)	\n  ");
						
			registros = con.consultarDB(qrySentencia.toString());
			con.cierraConexionDB();		
		} catch (Exception e) {
			log.error("catalogoMoneda  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 		
		log.info("\n catalogoMoneda :\n \n "+qrySentencia.toString());
		log.info("catalogoMoneda (S) ");
		return registros;									
	}
	
	/**
	* Obtiene elementos para crear un Catalogo Intermediario 
	* @return Objeto de tipo Registros
	*/
	public Registros catalogoIntermediario(){
		log.info("catalogoIntermediario(E)");
		
		Registros registros  = new Registros();
		AccesoDB  		con 	= new AccesoDB(); 
		qrySentencia 			= new StringBuffer();
		
		try{
			con.conexionDB();
			qrySentencia.append(	"SELECT   i.ic_if AS clave, i.cg_razon_social AS descripcion \n"+
						"   FROM comcat_if i \n"+
						"   WHERE idtipointermediario IS NOT NULL	\n "+
						"  ORDER BY cg_razon_social \n");
						
			registros = con.consultarDB(qrySentencia.toString());
			con.cierraConexionDB();		
		} catch (Exception e) {
			log.error("catalogoIntermediario  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 		
		log.info("\n catalogoIntermediario :\n \n "+qrySentencia.toString());
		log.info("catalogoIntermediario (S) ");
		return registros;									
	}
	/**
	* Obtiene elementos para crear un Catalogo Intermediario 
	* @return Objeto de tipo Registros
	*/
	public Registros catalogoIntermediarioDos(){
		log.info("catalogoIntermediario(E)");
		
		Registros registros  = new Registros();
		AccesoDB  		con 	= new AccesoDB(); 
		qrySentencia 			= new StringBuffer();
		
		try{
			con.conexionDB();
			qrySentencia.append(	"SELECT   i.ic_if AS clave, i.cg_razon_social AS descripcion \n"+
						"   FROM comcat_if i \n"+
						"  ORDER BY cg_razon_social \n");
						
			registros = con.consultarDB(qrySentencia.toString());
			con.cierraConexionDB();		
		} catch (Exception e) {
			log.error("catalogoIntermediario  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 		
		log.info("\n catalogoIntermediario :\n \n "+qrySentencia.toString());
		log.info("catalogoIntermediario (S) ");
		return registros;									
	}
	/**
	* Obtiene elementos para crear un Catalogo Intermediario 
	* @return Objeto de tipo Registros
	*/
	public Registros tipoIntermediario(){
		log.info("catalogoIntermediario(E)");
		
		Registros registros  = new Registros();
		AccesoDB  		con 	= new AccesoDB(); 
		qrySentencia 			= new StringBuffer();
		
		try{
			con.conexionDB();
			qrySentencia.append(	"SELECT idtipointermediario AS clave, descripcion \n"+
						"   FROM tipointermediario \n"+
						"  WHERE cg_tipo = 'I' \n");
						
			registros = con.consultarDB(qrySentencia.toString());
			con.cierraConexionDB();		
		} catch (Exception e) {
			log.error("catalogoIntermediario  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 		
		log.info("\n catalogoIntermediario :\n \n "+qrySentencia.toString());
		log.info("catalogoIntermediario (S) ");
		return registros;									
	}
	/**
	* Obtiene elementos para crear un Catalogo Estatus   
	* @return Objeto de tipo Registros
	*/
	public Registros catalogoEstatus(){
		log.info("catalogoEstatus(E)");
		
		Registros registros  = new Registros();
		AccesoDB  		con 	= new AccesoDB(); 
		qrySentencia 			= new StringBuffer();
		
		try{
			con.conexionDB();
			qrySentencia.append(	"SELECT   ic_estatus AS clave, cg_descripcion AS descripcion \n"+
						"   FROM cotcat_estatus \n"+
						"   ORDER BY 2 \n");
						
			registros = con.consultarDB(qrySentencia.toString());
			con.cierraConexionDB();		
		} catch (Exception e) {
			log.error("catalogoEstatus  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 		
		log.info("\n catalogoEstatus :\n \n "+qrySentencia.toString());
		log.info("catalogoEstatus (S) ");
		return registros;									
	}
	
	
	/**
	* Obtiene elementos para crear un Catalogo Persona  
	* @return Objeto de tipo Registros
	*/
	public Registros catalogoPersona(){
		log.info("catalogoPersona(E)");
		
		Registros registros  = new Registros();
		AccesoDB  		con 	= new AccesoDB(); 
		qrySentencia 			= new StringBuffer();

		try{
			con.conexionDB();
			qrySentencia.append(	"SELECT   ic_usuario as clave, ( cg_nombre||' '||cg_appaterno||' '|| cg_apmaterno) as descripcion \n"+
						"   FROM cotcat_ejecutivo \n"+
						"   ORDER BY 2 \n");
						
			registros = con.consultarDB(qrySentencia.toString());
			con.cierraConexionDB();		
		} catch (Exception e) {
			log.error("catalogoPersona  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 		
		log.info("\n catalogoPersona :\n \n "+qrySentencia.toString());
		log.info("catalogoPersona (S) ");
		return registros;									
	}
	
	/**
	* Obtiene elementos para crear un Catalogo Area  
	* @return Objeto de tipo Registros
	*/
	public Registros catalogoArea(){
		log.info("catalogoArea(E)");
		
		Registros registros  = new Registros();
		AccesoDB  		con 	= new AccesoDB(); 
		qrySentencia 			= new StringBuffer();

		try{
			con.conexionDB();
			qrySentencia.append(	
			"SELECT   ic_area AS clave, cg_descripcion AS descripcion \n"+
			" FROM cotcat_area \n"+
			" ORDER BY 2 \n");
						
			registros = con.consultarDB(qrySentencia.toString());
			con.cierraConexionDB();		
		} catch (Exception e) {
			log.error("catalogoArea  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 		
		log.info("\n catalogoArea :\n \n "+qrySentencia.toString());
		log.info("catalogoArea (S) ");
		return registros;									
	}
	
	/**
	* Obtiene elementos a mostrar en Pantalla:  Tasas de credito Nafin - Parametrizar Tesoreria - Asignar Facotres de Riesgo  
	* @return Objeto de tipo Registros
	*/
	public Registros obtenerAsignacion(){
		log.info("obtenerAsignacion(E)");
		
		Registros registros  = new Registros();
		AccesoDB  		con 	= new AccesoDB(); 
		qrySentencia 			= new StringBuffer();

		try{
			con.conexionDB();
			qrySentencia.append(	
			"SELECT   i.ic_if as clave, i.cg_razon_social as descripcion, i.idtipointermediario as clave_inter, ti.descripcion as desc_inter \n"+
						" FROM comcat_if i, tipointermediario ti \n"+
						" WHERE i.idtipointermediario = ti.idtipointermediario \n");
			if (intermediario.equals("")){
			} else {
				qrySentencia.append(" and i.ic_if = "+intermediario);
			}
			if (tipoIntermedario.equals("")){
			} else {
			//	qrySentencia.append(" and i.idtipointermediario = "+tipoIntermedario);
			}	
			qrySentencia.append("   ORDER BY i.cg_razon_social \n");
						
			registros = con.consultarDB(qrySentencia.toString());
			con.cierraConexionDB();		
		} catch (Exception e) {
			log.error("obtenerAsignacion  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 		
		log.info("\n obtenerAsignacion :\n \n "+qrySentencia.toString());
		log.info("obtenerAsignacion (S) ");
		return registros;									
	}
	
	/**
	* Actualiza el campo tipo de intermediario de la tabla de Intermediario
	* Pantalla:  Tasas de credito Nafin - Parametrizar Tesoreria - Asignar Facotres de Riesgo  
	* @return true si se actualizo correctamente
	*/
	public boolean guardar(){
		log.info("guardar(E)");
		boolean guardar = false;
		
		AccesoDB  		con 	= new AccesoDB(); 
		String qrySentencias 	= "";
		try{
			con.conexionDB();
			PreparedStatement	ps	= null;
			ResultSet 			rs	= null;
					qrySentencias = 
						" update comcat_if "+
						" set idtipointermediario = ?"+
						" where ic_if = ?";
					ps = con.queryPrecompilado(qrySentencias);
					ps.setString(1,tipoIntermedario);
					ps.setString(2,intermediario);
					ps.execute();
		guardar =true;
		ps.close();
		} catch (Exception e) {
			con.terminaTransaccion(false);
			log.error("guardar  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
		} 		
		log.info("\n guardar :\n \n "+qrySentencias);
		log.info("guardar (S) ");
		return guardar;									
	}
	
	/**
	* Elimina el intermediario de la busqueda
	* Pantalla:  Tasas de credito Nafin - Parametrizar Tesoreria - Asignar Facotres de Riesgo  
	* @return true si se elimino correctamente
	*/
	public boolean eliminar(){
		log.info("eliminar(E)");
		boolean eliminar = false;
		
		AccesoDB  		con 	= new AccesoDB(); 
		qrySentencia 			= new StringBuffer();
		
		String qrySentencias 	= "";
		try{
			con.conexionDB();
			PreparedStatement	ps	= null;
			ResultSet 			rs	= null;
			qrySentencias= 
					" update comcat_if "+
					" set idtipointermediario = null"+
					" where ic_if = ?";
				ps = con.queryPrecompilado(qrySentencias);
				ps.setString(1,intermediario);
				ps.execute();	
			eliminar =true;
			ps.close();
		} catch (Exception e) {
			con.terminaTransaccion(false);
			log.error("eliminar  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();
			}
		} 		
		log.info("\n eliminar :\n \n "+qrySentencias);
		log.info("eliminar (S) ");
		return eliminar;									
	}
	
	/**
	 * 
	 * @return 
	 */
	public String getDescTasaIndicativa(String moneda,String plaxo,String tasa,String esquema, String ppc, String ppi){
		String tasaInd = "";
		if("1".equals(tasa)){	//fija MN y USD		
			tasaInd = "";
		}else if("1".equals(moneda)){	// variable y protegida MN
			tasaInd = "TIIE 28 d�as + ";
		}else if("54".equals(moneda)){	//variable USD
			float plazo = 0;
			if(esquema.equals("1"))	//cupon cero, se toma el plazo de la operacion por ser al vencimiento
				plazo = Float.parseFloat(plaxo);
			else if(esquema.equals("4"))		//tipo renta, se toma la periodicidad de pago de capital porque es la misma que la de interes, la cual no se captura
				plazo = Float.parseFloat(ppc);
			else
				plazo = Float.parseFloat(ppi);	// tradicional y plan de pagos se toma la periodicidad de pago de interes capturada

			if(plazo <= 35f)
				tasaInd = "Libor 1m + ";
			else if(plazo <= 95f)
				tasaInd = "Libor 3m + ";
			else if(plazo <= 185f)
				tasaInd = "Libor 6m + ";
			else if(plazo <= 365f)
				tasaInd = "Libor 12m + ";
			else
				tasaInd = "";	//si excede el plazo no se cotiza o se da tasa fija
		}
		return tasaInd;	
	}
	
	/**
	 * 
	 * @throws NafinException
	 * @return 
	 */
	public String techoTasaProtegida() throws NafinException{
		log.info(":techoTasaProtegida (E)");
		String qry = "select ig_techo_tasa_p from parametrossistema where ic_moneda = 1";
		AccesoDB con = new AccesoDB();
		ResultSet rs = null;
		String techo = " con techo de ";
		try{
			con.conexionDB();
			rs = con.queryDB(qry);
			if(rs.next()){
				techo += rs.getDouble("ig_techo_tasa_p")+" %";
			}
			rs.close();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			log.info(":techoTasaProtegida (S)");
			if(con.hayConexionAbierta()){
				con.cierraConexionDB();
			}
		}
		return techo;
	}
	
	
	/**
	* Este m�todo debe regresar un query con el que se obtienen los
	* identificadores unicos de los registros a mostrar en la consulta
	* @return Cadena con el query armado de acuerdo a los parametros recibidos
	*/
	public String getDocumentQuery(){
		log.info("getDocumentQuery(E)");
		qrySentencia = new StringBuffer();
		conditions 	 = new ArrayList();
		qrySentencia.append(
		" SELECT   cs.ic_solic_esp \n"+
		"  FROM cot_solic_esp cs, \n"+
		"	cotcat_ejecutivo ce, \n"+
		"	cotcat_estatus e, \n"+
		"	comcat_moneda cm \n");
		
		if (intermediarioFinanciero.equals("")){
		} else {
			qrySentencia.append(" ,comcat_if ci \n");
		}
		
		qrySentencia.append(" WHERE cs.ic_ejecutivo = ce.ic_ejecutivo \n AND cs.ic_estatus = e.ic_estatus \n");
		
		if (intermediarioFinanciero.equals("")){	
		} else {
			qrySentencia.append(" AND ci.ic_if(+) = cs.ic_if \n");
		}
		
		qrySentencia.append(	" AND cs.ic_moneda = cm.ic_moneda \n");		
		
		if (personaResponsable.equals("")){
		} else {
			qrySentencia.append(
			" AND ( ce.ic_usuario = '"+personaResponsable+"' \n"+
			" OR ce.ic_area IN ( SELECT ic_area FROM cotcat_ejecutivo 	\n"+
			" WHERE ic_usuario = '"+personaResponsable+"' \n"+
			" AND cs_director = 'S' )) \n");
		}
		
		if (fechaSolicitudInicial.equals("")){
		} else {
			qrySentencia.append(
			" AND cs.df_solicitud BETWEEN TO_DATE ('"+fechaSolicitudInicial+"', 'DD/MM/YYYY') \n"+
			" AND TO_DATE ('"+fechaSolicitudFinal+"', 'DD/MM/YYYY') + 1 \n");
		} 
		
		if (numeroSolicitud.equals("")){
		} else {
			qrySentencia.append(
			"  AND (   TO_CHAR (cs.df_solicitud, 'YYYY') \n"+
			" 	|| TO_CHAR (cs.df_solicitud, 'MM') \n"+
			" 	|| '-' || cs.ig_num_solic \n"+
			"	) = '"+numeroSolicitud+"' \n");
		}
		
		if(moneda.equals("")){
		} else {
			qrySentencia.append( " AND cs.ic_moneda = ? 	\n");
			conditions.add(moneda);
		}
		
		if(montoCreditoInicial.equals("")){
		} else {
			qrySentencia.append( " AND cs.fn_monto BETWEEN ? AND ? \n");
			conditions.add(montoCreditoInicial);
			conditions.add(montoCreditoFinal);
		}
		
		if(estatus.equals("")){
		} else {
			qrySentencia.append(" AND cs.ic_estatus IN ( ? ) 	\n");
			conditions.add(estatus);
		}
		
		if (intermediarioFinanciero.equals("")){
		} else {
			qrySentencia.append(" AND cs.ic_if = ? 	\n");
			conditions.add(intermediarioFinanciero);
		}
		
		qrySentencia.append(" ORDER BY cs.ic_solic_esp \n");
		
		log.info("qry: \n"+qrySentencia);
		log.info("Binds: \n"+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();
  }
	
	
	/**
	* Este m�todo debe regresar un query con el que se obtienen los
	* datos a mostrar en la consulta basandose en los identificadores unicos
	* especificados en las lista de ids
	* @param ids Lista de los identificadoes unicos. El tama�o de la lista
	* 	recibida ser� de acuerdo a la configuraci�n de registros x p�gina
	* @return Cadena con el query armado de acuerdo a los parametros recibidos
	*/
	public String getDocumentSummaryQueryForIds(List ids){
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		
		qrySentencia.append(
		"SELECT DISTINCT cs.ic_solic_esp AS ic_solic_esp,	\n"+
		"	TO_CHAR (cs.df_solicitud, 'DD/MM/YYYY') AS fecha_solicitud, \n"+
		"	TO_CHAR (cs.df_cotizacion, 'DD/MM/YYYY  HH24:MI:SS'	\n"+
		"	 ) AS fecha_cotizacion,	\n"+
		"	(   TO_CHAR (cs.df_solicitud, 'YYYY')	\n"+
		"	 || TO_CHAR (cs.df_solicitud, 'MM') 	\n"+
		"	 || '-'  || cs.ig_num_solic  ) AS numero_solicitud,	\n"+
		"	cm.cd_nombre AS moneda,	\n"+
		"	(ce.cg_nombre || ' ' || ce.cg_appaterno || ' '	\n"+
		"	 || ce.cg_apmaterno ) AS ejecutivo,	\n"+
		"	cs.fn_monto AS monto, cs.ig_plazo AS plazo,	\n"+
		"	e.cg_descripcion AS estatus, cs.ig_tasa_md tasa_mismo_dia,	\n"+
		"	cs.ig_tasa_spot AS tasa_spot,	\n"+
		"	cs.ig_num_referencia AS num_referencia,	\n"+
		"	cs.cg_causa_rechazo AS causa_rechazo,	\n"+
		"	cs.cg_observaciones_sirac AS obser_oper,	\n"+
		"	cs.ic_estatus AS ic_estatus, 	\n"+
		"	DECODE (cs.cs_aceptado_md, 'S', cs.ig_tasa_md,	\n"+
		"	cs.ig_tasa_spot  ) AS tasa_confirmada,	\n"+
		"  DECODE (cs.ic_if,	NULL, cs.cg_razon_social_if, \n"+
		"	 ci.cg_razon_social ) AS cg_razon_social,	\n"+
		"   cs.ic_moneda, cs.ic_tipo_tasa, 	\n"+
		"  CASE	\n"+
		"	WHEN ie.cg_solicitar_aut = 'T' THEN 'SI' \n"+
		"	WHEN ie.cg_solicitar_aut = 'N' THEN 'NO' \n"+
		"	WHEN ie.cg_solicitar_aut = 'M' \n"+
		"	 AND cs.fn_monto >= ie.fn_monto_mayor_a  THEN 'SI' ELSE 'NO' \n"+
		"	END AS solicitar_aut, \n"+
		"	DECODE (cs.cg_tipo_cotizacion, \n"+
		"	'I', 'Indicativa', \n"+
		"	'F', 'En Firme', \n"+
		"	'' ) AS tipocotiza, \n"+
		"	cs.cg_ut_ppc, cs.cg_ut_ppi, \n "+
		"  cs.ic_esquema_recup, cs.cs_linea, \n"+
		"	'' as Plan_Pago, '' as Consultar \n "+
		"	FROM cot_solic_esp cs, \n"+
		"	 cotcat_ejecutivo ce,	\n"+
		"	 cotcat_estatus e,	\n"+
		"	 comcat_moneda cm,	\n"+
		"	 cotrel_if_ejecutivo ie,	\n"+
		"	 comcat_if ci	\n"+
		" WHERE cs.ic_ejecutivo = ce.ic_ejecutivo	\n"+
		"  AND cs.ic_estatus = e.ic_estatus \n"+
		"	AND cs.ic_moneda = cm.ic_moneda \n"+
		"	AND cs.ic_if = ie.ic_if(+) \n"+
		"  AND ci.ic_if(+) = cs.ic_if	\n"+
		"	AND cs.ic_solic_esp IN (");
		//lista pKeys para almacenar los parametros in de la consulta totales parciales
		List pKeys = new ArrayList();
		for(int i = 0; i < ids.size(); i++){
			List lItem = (ArrayList)ids.get(i);
			if(i>0){ qrySentencia.append(","); }
			qrySentencia.append("?");
			conditions.add(lItem.get(0));
			pKeys.add(ids.get(i));			
		}
		
		qrySentencia.append(") order by cs.ic_solic_esp ");
		log.info("qry:\n  "+qrySentencia);
		log.info("Binds:\n "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}


	/**
	* Este m�todo debe regresar un query con el que se obtienen totales de la
	* consulta.
	* @return Cadena con el query armado de acuerdo a los parametros recibidos
	*/
	public String getAggregateCalculationQuery(){
	return "";
	}
	
	
	/**
	* Este m�todo debe regresar una Lista de parametros que ser�n usados
	* como valor de las variables BIND de los queries generados.
	* @return Lista con los valores a usar en las variables bind
	*/
	public List getConditions(){
	return conditions;
	}
	
	
	/**
	* Este m�todo debe regresar un query que obtendr� todos los registros
	* resultantes de la b�squeda, con la finalidad de generar un archivo.
	* @return Cadena con el query armado de acuerdo a los parametros recibidos
	*/
	public String getDocumentQueryFile(){
	log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		
		qrySentencia.append(
		"SELECT DISTINCT cs.ic_solic_esp AS ic_solic_esp,	\n"+
		"	TO_CHAR (cs.df_solicitud, 'DD/MM/YYYY') AS fecha_solicitud, \n"+
		"  TO_CHAR (cs.df_solicitud, 'HH24:MI:SS') AS hora_solicitud, \n"+
		"	TO_CHAR (cs.df_cotizacion, 'DD/MM/YYYY  HH24:MI:SS') AS fecha_cotizacion,	\n"+
		"	TO_CHAR (cs.df_cotizacion, 'HH24:MI:SS') AS hora_cotizacion, 	\n"+
		"  TO_CHAR (cs.df_aceptacion, 'HH24:MI') AS hora_enfirme,\n"+
		"	(   TO_CHAR (cs.df_solicitud, 'YYYY')	\n"+
		"	 || TO_CHAR (cs.df_solicitud, 'MM') 	\n"+
		"	 || '-'  || cs.ig_num_solic  ) AS numero_solicitud,	\n"+
		"	cm.cd_nombre AS moneda,	\n"+
		"	(ce.cg_nombre || ' ' || ce.cg_appaterno || ' '	\n"+
		"	 || ce.cg_apmaterno ) AS ejecutivo,	\n"+
		"	cs.fn_monto AS monto, cs.ig_plazo AS plazo,	\n"+
		"	e.cg_descripcion AS estatus, cs.ig_tasa_md tasa_mismo_dia,	\n"+
		"	cs.ig_tasa_spot AS tasa_spot,	\n"+
		"	cs.ig_num_referencia AS num_referencia,	\n"+
		"	cs.cg_causa_rechazo AS causa_rechazo,	\n"+
		"	cs.cg_observaciones_sirac AS obser_oper,	\n"+
		"	cs.ic_estatus AS ic_estatus, 	\n"+
		"	DECODE (cs.cs_aceptado_md, 'S', cs.ig_tasa_md,	\n"+
		"	cs.ig_tasa_spot  ) AS tasa_confirmada,	\n"+
		"  DECODE (cs.ic_if,	NULL, cs.cg_razon_social_if, \n"+
		"	 ci.cg_razon_social ) AS cg_razon_social,	\n"+
		"   cs.ic_moneda, cs.ic_tipo_tasa, 	\n"+
		"  CASE	\n"+
		"	WHEN ie.cg_solicitar_aut = 'T' THEN 'SI' \n"+
		"	WHEN ie.cg_solicitar_aut = 'N' THEN 'NO' \n"+
		"	WHEN ie.cg_solicitar_aut = 'M' \n"+
		"	 AND cs.fn_monto >= ie.fn_monto_mayor_a  THEN 'SI' ELSE 'NO' \n"+
		"	END AS solicitar_aut, \n"+
		"	DECODE (cs.cg_tipo_cotizacion, \n"+
		"	'I', 'Indicativa', \n"+
		"	'F', 'En Firme', \n"+
		"	'' ) AS tipocotiza, \n"+
		"	cs.cg_ut_ppc, cs.cg_ut_ppi, \n "+
		"  cs.ic_esquema_recup, cs.cs_linea, \n"+
		"	'' as Plan_Pago, '' as Consultar \n "+
		"  FROM cot_solic_esp cs, \n"+
		" cotcat_ejecutivo ce, \n"+
		" cotcat_estatus e, \n"+
		" cotrel_if_ejecutivo ie, \n"+
		" comcat_moneda cm \n");
		
		if (intermediarioFinanciero.equals("")){
			qrySentencia.append(" ,comcat_if ci \n");
		} else {
			qrySentencia.append(" ,comcat_if ci \n");
		}		
		qrySentencia.append(" WHERE cs.ic_ejecutivo = ce.ic_ejecutivo \n AND cs.ic_estatus = e.ic_estatus \n");
		if (intermediarioFinanciero.equals("")){	
		} else {
			qrySentencia.append(" AND ci.ic_if(+) = cs.ic_if \n");
		}
		qrySentencia.append(	" AND cs.ic_moneda = cm.ic_moneda \n");		
		if (personaResponsable.equals("")){
		} else {
			qrySentencia.append(
			" AND ( ce.ic_usuario = '"+personaResponsable+"' \n"+
			" OR ce.ic_area IN ( SELECT ic_area FROM cotcat_ejecutivo 	\n"+
			" WHERE ic_usuario = '"+personaResponsable+"' \n"+
			" AND cs_director = 'S' )) \n");
		}		
		if (fechaSolicitudInicial.equals("")){
		} else {
			qrySentencia.append(
			" AND cs.df_solicitud BETWEEN TO_DATE ('"+fechaSolicitudInicial+"', 'DD/MM/YYYY') \n"+
			" AND TO_DATE ('"+fechaSolicitudFinal+"', 'DD/MM/YYYY') + 1 \n");
		} 		
		if (numeroSolicitud.equals("")){
		} else {
			qrySentencia.append(
			"  AND (   TO_CHAR (cs.df_solicitud, 'YYYY') \n"+
			" 	|| TO_CHAR (cs.df_solicitud, 'MM') \n"+
			" 	|| '-' || cs.ig_num_solic \n"+
			"	) = '"+numeroSolicitud+"' \n");
		}		
		if(moneda.equals("")){
		} else {
			qrySentencia.append( " AND cs.ic_moneda = ? 	\n");
			conditions.add(moneda);
		}		
		if(montoCreditoInicial.equals("")){
		} else {
			qrySentencia.append( " AND cs.fn_monto BETWEEN ? AND ? \n");
			conditions.add(montoCreditoInicial);
			conditions.add(montoCreditoFinal);
		}		
		if(estatus.equals("")){
		} else {
			qrySentencia.append(" AND cs.ic_estatus IN ( ? ) 	\n");
			conditions.add(estatus);
		}		
		if (intermediarioFinanciero.equals("")){
			qrySentencia.append("  AND ci.ic_if(+) = cs.ic_if	\n");
		} else {
			qrySentencia.append(" AND cs.ic_if = ? 	\n");
			conditions.add(intermediarioFinanciero);
		}		
		qrySentencia.append(" AND cs.ic_if = ie.ic_if(+) ORDER BY cs.ic_solic_esp \n");
		
		log.info("qry: \n"+qrySentencia);
		log.info("Binds: \n"+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();

	}
	
	
	/**
	* En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	* con base en el resultset que se recibe como par�metro. El ResulSet es el
	* resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	* @param request HttpRequest empleado principalmente para obtener el objeto session
	* @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	* @param path Ruta donde se generar� el archivo
	* @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	* @return Cadena con la ruta del archivo generado
	*/
	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo){
	log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();		
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		
		try {
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);
		
			contenidoArchivo = new StringBuffer();	
			contenidoArchivo.append("Fecha de Solicitud,N�mero de Solicitud,Requiere autorizaci�n del ejecutivo,Intermediario financiero,"+
			"Moneda, Ejecutivo,Monto Total Requerido,Plazo de la Operaci�n,Estatus,Cotizada en L�nea,Tasa mismo d�a,Tasa 48 horas,"+
			"Tasa Confirmada,Tipo de Cotizaci�n,Causas de rechazo,Observaciones de Operaciones,Hora Solicitud,Hora Cotizaci�n,Hora Toma en Firme, \n\r");

			
		while (rs.next() )	{ 
				String fechaSolicitud  = (rs.getString("FECHA_SOLICITUD") == null) ? "" : rs.getString("FECHA_SOLICITUD");
				String numeroSolicitud = (rs.getString("NUMERO_SOLICITUD") == null) ? "" : rs.getString("NUMERO_SOLICITUD");
				String autorizacion	  = (rs.getString("SOLICITAR_AUT") == null) ? "" : rs.getString("SOLICITAR_AUT");
				String intermediario	  = (rs.getString("CG_RAZON_SOCIAL") == null) ? "" : rs.getString("CG_RAZON_SOCIAL");
				String moneda			  = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
				String ejecutivo		  = (rs.getString("EJECUTIVO") == null) ? "" : rs.getString("EJECUTIVO");
				String montoTotalReq	  = (rs.getString("MONTO") == null) ? "" : rs.getString("MONTO");
				String plazoOperacion  = (rs.getString("PLAZO") == null) ? "" : rs.getString("PLAZO");
				String estatus  		  = (rs.getString("ESTATUS") == null) ? "" : rs.getString("ESTATUS");
				String cotizadaLinea	  = (rs.getString("CS_LINEA") == null) ? "" : rs.getString("CS_LINEA");
				String horaSolicitud	  = (rs.getString("HORA_SOLICITUD") == null) ? "" : rs.getString("HORA_SOLICITUD");	
				String horaCotizacion  = (rs.getString("HORA_COTIZACION") == null) ? "" : rs.getString("HORA_COTIZACION");	
				String horaEnFirme	  = (rs.getString("HORA_ENFIRME") == null) ? "" : rs.getString("HORA_ENFIRME");	
				if(cotizadaLinea.equals("N")) { 
					cotizadaLinea="No";
				} else {		}
				
				String icMoneda		  = (rs.getString("IC_MONEDA") == null) ? "" : rs.getString("IC_MONEDA");
				String icTipoTasa		  = (rs.getString("IC_TIPO_TASA") == null) ? "" : rs.getString("IC_TIPO_TASA");
				String icEsquemaRec	  = (rs.getString("IC_ESQUEMA_RECUP") == null) ? "" : rs.getString("IC_ESQUEMA_RECUP");
				String cgUtPpc			  = (rs.getString("CG_UT_PPC") == null) ? "" : rs.getString("CG_UT_PPC");
				String cgUtPpi			  = (rs.getString("CG_UT_PPI") == null) ? "" : rs.getString("CG_UT_PPI");
				String icSolicEsp		  = (rs.getString("IC_SOLIC_ESP") == null) ? "" : rs.getString("IC_SOLIC_ESP");
				String icPlanPagos="";
				if(icEsquemaRec.equals("3")){
					icPlanPagos=icSolicEsp;
				}

				String resTasaMismoDia = "", resTasa48 = "", restasaConfirm = "";
				String icEstatus = (rs.getString("IC_ESTATUS") == null) ? "" : rs.getString("IC_ESTATUS");
				String tasaMismoDia = (rs.getString("TASA_MISMO_DIA") == null) ? "" : rs.getString("TASA_MISMO_DIA");

			   SolCotEspBean solCotEsp  = new SolCotEspBean();
				
				Solicitud sol = new Solicitud();

				String descTasaInd = sol.getDescTasaIndicativa();
				String tec = "";
				if ("3".equals(rs.getString("ic_tipo_tasa"))) {
					tec = solCotEsp.techoTasaProtegida();
				}
				String valor=   (rs.getString("TASA_MISMO_DIA") == null) ? "" : rs.getString("TASA_MISMO_DIA");
				String valorS=  (rs.getString("TASA_SPOT") == null) ? "" : rs.getString("TASA_SPOT");
				String tasaConfirmada  = (rs.getString("TASA_CONFIRMADA") == null) ? "" : rs.getString("TASA_CONFIRMADA");
			
				try {
					
					String remplazado = valor.replace('.', ',');
					String remplazadoS = valorS.replace('.', ',');
					String[] valorArr = remplazado.split(",");
					String[] valorArrS = remplazadoS.split(",");
					String entero = "", enteroS = "";
					String decimal = "", decimalS = "";
					for (int z = 0; z < valorArr.length; z++) {
						if (z == 0)
							entero = valorArr[z];
						if (z == 1)
							decimal = valorArr[z];
					}
					for (int z = 0; z < valorArrS.length; z++) {
						if (z == 0)
							enteroS = valorArrS[z];
						if (z == 1)
							decimalS = valorArrS[z];
					}
					if (decimal.length() == 1) {
						StringBuffer newDec = new StringBuffer(decimal);
						newDec.append("0");
						entero = entero + "." + newDec.toString();
						resTasaMismoDia = entero;
					} else if (decimal.length() == 2) {
						entero = entero + "." + decimal;
						resTasaMismoDia = entero;
					}
					if (decimalS.length() == 1) {
						StringBuffer newDecS = new StringBuffer(decimalS);
						newDecS.append("0");
						enteroS = enteroS + "." + newDecS.toString();
						resTasa48 = enteroS;
					} else if (decimalS.length() == 2) {
						enteroS = enteroS + "." + decimalS;
						resTasa48 = enteroS;
					}

				} catch (Exception e) {
					System.err.println("error: " + e);
				}
				if (!valor.equals("") && !icEstatus.equals("3")) {
					String res = descTasaInd + valor + tec;
					resTasaMismoDia = res;
				}
				if (!valorS.equals("") && !icEstatus.equals("3")) {
					String res = descTasaInd + valorS + tec;
					resTasa48 = res;
				}
				if (icEstatus.equals("7") || icEstatus.equals("9")) {
					String res = descTasaInd + tasaConfirmada + tec;
					restasaConfirm = res;
				}
				
				
				String tipoCotizacion  = (rs.getString("TIPOCOTIZA") == null) ? "" : rs.getString("TIPOCOTIZA");
				String causaRechazo    = (rs.getString("CAUSA_RECHAZO") == null) ? "" : rs.getString("CAUSA_RECHAZO");
				String observaciones   = (rs.getString("OBSER_OPER") == null) ? "" : rs.getString("OBSER_OPER");
				
				contenidoArchivo.append(
				fechaSolicitud.replace(',',' ')+","+
				numeroSolicitud.replace(',',' ')+","+
				autorizacion.replace(',',' ')+","+
				intermediario.replace(',',' ')+","+
				moneda.replace(',',' ')+","+
				ejecutivo.replace(',',' ')+","+
				montoTotalReq.replace(',',' ')+","+
				plazoOperacion.replace(',',' ')+","+
				estatus.replace(',',' ')+","+
				cotizadaLinea.replace(',',' ')+","+
				resTasaMismoDia.replace(',',' ')+","+
				resTasa48.replace(',',' ')+","+
				restasaConfirm.replace(',',' ')+","+
				tipoCotizacion.replace(',',' ')+","+
				causaRechazo.replace(',',' ')+","+
				observaciones.replace(',',' ')+","+
				horaSolicitud+","+horaCotizacion+","+horaEnFirme+
				"\n");				
				
			}//while(rs.next()){
				
			buffer.write(contenidoArchivo.toString());
			buffer.close();	
				
		creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
		nombreArchivo = creaArchivo.getNombre();
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;	
	}
	
	
	/**
	* En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	* con base en el objeto Registros que recibe como par�metro.
	* @param request HttpRequest empleado principalmente para obtener el objeto session
	* @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	* @param path Ruta donde se generar� el archivo
	* @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	* @return Cadena con la ruta del archivo generado
	*/
	public String crearPageCustomFile(HttpServletRequest request, Registros rs, String path, String tipo){
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			pdfDoc.setTable(16,100);
			pdfDoc.setCell("Fecha de Solicitud","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("N�mero de Solicitud","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Requiere autorizaci�n del ejecutivo","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Intermediario financiero","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Ejecutivo","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Monto Total Requerido","celda01",ComunesPDF.CENTER);			
			pdfDoc.setCell("Plazo de la Operaci�n","celda01",ComunesPDF.CENTER); 
			pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER); 
			pdfDoc.setCell("Cotizada en L�nea","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tasa mismo d�a","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tasa 48 horas","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tasa Confirmada","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Tipo de Cotizaci�n","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Causas de rechazo","celda01",ComunesPDF.CENTER);
			pdfDoc.setCell("Observaciones de Operaciones","celda01",ComunesPDF.CENTER);
			
			while (rs.next())	{
				String fechaSolicitud  = (rs.getString("FECHA_SOLICITUD") == null) ? "" : rs.getString("FECHA_SOLICITUD");
				String numeroSolicitud = (rs.getString("NUMERO_SOLICITUD") == null) ? "" : rs.getString("NUMERO_SOLICITUD");
				String autorizacion	  = (rs.getString("SOLICITAR_AUT") == null) ? "" : rs.getString("SOLICITAR_AUT");
				String intermediario	  = (rs.getString("CG_RAZON_SOCIAL") == null) ? "" : rs.getString("CG_RAZON_SOCIAL");
				String moneda			  = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
				String ejecutivo		  = (rs.getString("EJECUTIVO") == null) ? "" : rs.getString("EJECUTIVO");
				String montoTotalReq	  = (rs.getString("MONTO") == null) ? "" : rs.getString("MONTO");
				String plazoOperacion  = (rs.getString("PLAZO") == null) ? "" : rs.getString("PLAZO");
				String estatus  		  = (rs.getString("ESTATUS") == null) ? "" : rs.getString("ESTATUS");
				String cotizadaLinea	  = (rs.getString("CS_LINEA") == null) ? "" : rs.getString("CS_LINEA");
				if(cotizadaLinea.equals("N")) {
					cotizadaLinea="No";
				} else {		}
				
				String icMoneda		  = (rs.getString("IC_MONEDA") == null) ? "" : rs.getString("IC_MONEDA");
				String icTipoTasa		  = (rs.getString("IC_TIPO_TASA") == null) ? "" : rs.getString("IC_TIPO_TASA");
				String icEsquemaRec	  = (rs.getString("IC_ESQUEMA_RECUP") == null) ? "" : rs.getString("IC_ESQUEMA_RECUP");
				String cgUtPpc			  = (rs.getString("CG_UT_PPC") == null) ? "" : rs.getString("CG_UT_PPC");
				String cgUtPpi			  = (rs.getString("CG_UT_PPI") == null) ? "" : rs.getString("CG_UT_PPI");
				String icSolicEsp		  = (rs.getString("IC_SOLIC_ESP") == null) ? "" : rs.getString("IC_SOLIC_ESP");
				String icPlanPagos="";
				if(icEsquemaRec.equals("3")){
					icPlanPagos=icSolicEsp;
				}
	
			   
			   SolCotEspBean solCotEsp  = new SolCotEspBean();
			   
			   Solicitud sol = new Solicitud();

			   String descTasaInd = sol.getDescTasaIndicativa();
			   String tec = "", resTasaMismoDia ="", resTasa48 ="", restasaConfirm =""; 
				
				
			   if ("3".equals(rs.getString("ic_tipo_tasa"))) {
			      tec = solCotEsp.techoTasaProtegida();
			   }
			   try {
			      String valor = rs.getString("TASA_MISMO_DIA");
			      String valorS = rs.getString("TASA_SPOT");
			      String remplazado = valor.replace('.', ',');
			      String remplazadoS = valorS.replace('.', ',');
			      String[] valorArr = remplazado.split(",");
			      String[] valorArrS = remplazadoS.split(",");
			      String entero = "", enteroS = "";
			      String decimal = "", decimalS = "";
			      for (int z = 0; z < valorArr.length; z++) {
			         if (z == 0)
			            entero = valorArr[z];
			         if (z == 1)
			            decimal = valorArr[z];
			      }
			      for (int z = 0; z < valorArrS.length; z++) {
			         if (z == 0)
			            enteroS = valorArrS[z];
			         if (z == 1)
			            decimalS = valorArrS[z];
			      }
			      if (decimal.length() == 1) {
			         StringBuffer newDec = new StringBuffer(decimal);
			         newDec.append("0");
			         entero = entero + "." + newDec.toString();
			         resTasaMismoDia = entero;
			      } else if (decimal.length() == 2) {
			         entero = entero + "." + decimal;
			         resTasaMismoDia = entero;
			      }
			      if (decimalS.length() == 1) {
			         StringBuffer newDecS = new StringBuffer(decimalS);
			         newDecS.append("0");
			         enteroS = enteroS + "." + newDecS.toString();
			         resTasa48 = enteroS;
			      } else if (decimalS.length() == 2) {
			         enteroS = enteroS + "." + decimalS;
			         resTasa48 = enteroS;
			      }

			   } catch (Exception e) {
			      System.err.println("error: " + e);
			   }
			   if (!rs.getString("TASA_MISMO_DIA").equals("") && !rs.getString("IC_ESTATUS").equals("3")) {
			      String res = descTasaInd + rs.getString("TASA_MISMO_DIA") + tec;
			      resTasaMismoDia = res;
			   }
			   if (!rs.getString("TASA_SPOT").equals("") && !rs.getString("IC_ESTATUS").equals("3")) {
			      String res = descTasaInd + rs.getString("TASA_SPOT") + tec;
			      resTasa48 = res;
			   }
			   if (rs.getString("IC_ESTATUS").equals("7") || rs.getString("IC_ESTATUS").equals("9")) {
			      String res = descTasaInd + rs.getString("TASA_CONFIRMADA") + tec;
			      restasaConfirm = res;
			   }
				
				String tipoCotizacion  = (rs.getString("TIPOCOTIZA") == null) ? "" : rs.getString("TIPOCOTIZA");
				String causaRechazo    = (rs.getString("CAUSA_RECHAZO") == null) ? "" : rs.getString("CAUSA_RECHAZO");
				String observaciones   = (rs.getString("OBSER_OPER") == null) ? "" : rs.getString("OBSER_OPER");
				
				pdfDoc.setCell(fechaSolicitud,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(numeroSolicitud,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(autorizacion,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(intermediario,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(ejecutivo,"formas",ComunesPDF.CENTER);				
				pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTotalReq,2,true),"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(plazoOperacion+" d�as","formas",ComunesPDF.CENTER);				
				pdfDoc.setCell(estatus,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(cotizadaLinea,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(resTasaMismoDia,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(resTasa48,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(restasaConfirm,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(tipoCotizacion,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(causaRechazo,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(observaciones,"formas",ComunesPDF.CENTER);
				
			}//fin while
			
			pdfDoc.addTable();
			pdfDoc.endDocument();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
			} catch(Exception e) {}
		}
		return nombreArchivo;
	}
	
	public String file( String path,String nombreArchivo, String tipo,String pais,String iNoNafElec, String sesExter, String strNom, String strNomUsr, String logo, String dirPub ){
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
	
		try {
			
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

			pdfDoc.encabezadoConImagenes(pdfDoc,pais,
				iNoNafElec,sesExter,strNom,strNomUsr,logo,dirPub);	
			
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
		
			
			pdfDoc.endDocument();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
			} catch(Exception e) {}
		}
		return nombreArchivo;
	} 




	public void setFechaSolicitudInicial(String fechaSolicitudInicial) {
		this.fechaSolicitudInicial = fechaSolicitudInicial;
	}


	public String getFechaSolicitudInicial() {
		return fechaSolicitudInicial;
	}


	public void setFechaSolicitudFinal(String fechaSolicitudFinal) {
		this.fechaSolicitudFinal = fechaSolicitudFinal;
	}


	public String getFechaSolicitudFinal() {
		return fechaSolicitudFinal;
	}


	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}


	public String getMoneda() {
		return moneda;
	}


	public void setNumeroSolicitud(String numeroSolicitud) {
		this.numeroSolicitud = numeroSolicitud;
	}


	public String getNumeroSolicitud() {
		return numeroSolicitud;
	}


	public void setMontoCreditoInicial(String montoCreaditoInicial) {
		this.montoCreditoInicial = montoCreaditoInicial;
	}


	public String getMontoCreditoInicial() {
		return montoCreditoInicial;
	}


	public void setMontoCreditoFinal(String montoCreditoFinal) {
		this.montoCreditoFinal = montoCreditoFinal;
	}


	public String getMontoCreditoFinal() {
		return montoCreditoFinal;
	}


	public void setIntermediarioFinanciero(String intemediarioFinanciero) {
		this.intermediarioFinanciero = intemediarioFinanciero;
	}


	public String getIntermediarioFinanciero() {
		return intermediarioFinanciero;
	}


	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}


	public String getEstatus() {
		return estatus;
	}


	public void setPersonaResponsable(String personaResponsable) {
		this.personaResponsable = personaResponsable;
	}


	public String getPersonaResponsable() {
		return personaResponsable;
	}


	public void setIntermediario(String intermediario) {
		this.intermediario = intermediario;
	}


	public String getIntermediario() {
		return intermediario;
	}


	public void setTipoIntermedario(String tipoIntermedario) {
		this.tipoIntermedario = tipoIntermedario;
	}


	public String getTipoIntermedario() {
		return tipoIntermedario;
	}
  
}