package com.netro.cotizador;

import com.netro.pdf.ComunesPDF;
import netropology.utilerias.*;
import org.apache.commons.logging.Log;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import netropology.utilerias.ServiceLocator;

public class ConsCotizacionIf implements IQueryGeneratorRegExtJS{

	private final static Log log = ServiceLocator.getInstance().getLog(ConsCotizacionIf.class);
	private List   conditions;
	private String fechaInicio;
	private String fechaFinal;
	private String numeroSolicitud;
	private String moneda;
	private String montoInicio;
	private String montoFinal;
	private String estatus;
	private String icIf;
	private String icUsuario;
	private String usuarioNafin;
	private String origen;
	private String ic_moneda;
	private String ig_plazo;
	private String ic_tipo_tasa;
	private String ic_esquema_recup;
	private String cg_ut_ppc;
	private String cg_ut_ppi;

	public ConsCotizacionIf(){}
	public String getDocumentQueryFile(){ return null;}
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){ return null;}

	/**
	 * Obtiene las llaves primarias
	 * @return 
	 */
	public String getDocumentQuery(){

		log.info("getDocumentQuery (E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();

		if(!icUsuario.equals("")){
			condicion.append("AND ( CE.IC_USUARIO = ? OR CE.IC_AREA IN ( SELECT IC_AREA FROM COTCAT_EJECUTIVO WHERE IC_USUARIO = ? AND CS_DIRECTOR = 'S') ) ");
			conditions.add(icUsuario);
			conditions.add(icUsuario);
		}
		if(!fechaInicio.equals("")){
			condicion.append("AND CS.DF_SOLICITUD BETWEEN TO_DATE (?, 'DD/MM/YYYY') AND TO_DATE (?, 'DD/MM/YYYY') + 1 ");
			conditions.add(fechaInicio);
			conditions.add(fechaFinal);
		}
		if(!numeroSolicitud.equals("")){
			condicion.append("AND (TO_CHAR(CS.DF_SOLICITUD,'YYYY') || TO_CHAR(CS.DF_SOLICITUD,'MM') || '-S' || CS.IC_SOLIC_ESP) = ? ");
			conditions.add(numeroSolicitud);
		}
		if(!moneda.equals("")){
			condicion.append("AND CS.IC_MONEDA = ? ");
			conditions.add(moneda);
		}
		if(!montoInicio.equals("")){
			condicion.append("AND CS.FN_MONTO BETWEEN ? AND ? ");
			conditions.add(montoFinal);
			conditions.add(montoInicio);
		}
		if(!estatus.equals("")){
			condicion.append("AND CS.IC_ESTATUS IN(" + this.estatus + ") ");
		}
		if(!icIf.equals("")){
			condicion.append(" AND CS.IC_IF = ? ");
			conditions.add(icIf);
		}
		if(origen.equals("CALCULADORAPESOSDOLARES")){
			condicion.append("AND ((CS.IC_MONEDA = 1 AND CS.IC_TIPO_TASA = 1) OR (CS.IC_MONEDA = 54 AND CS.IC_TIPO_TASA IN (1, 2))) ");
		}

		qrySentencia.append(
			"SELECT CS.IC_SOLIC_ESP "                  +
			"FROM COT_SOLIC_ESP CS, "                  +
			"COTCAT_EJECUTIVO CE, "                    +
			"COTCAT_ESTATUS E, "                       +
			"COMCAT_MONEDA CM, "                       +
			"COMCAT_IF CI "                            +
			"WHERE CS.IC_EJECUTIVO = CE.IC_EJECUTIVO " +
			"AND CS.IC_ESTATUS = E.IC_ESTATUS "        +
			"AND CI.IC_IF(+) = CS.IC_IF "              +
			"AND CS.IC_MONEDA = CM.IC_MONEDA "         +
			condicion.toString()                       +
			"ORDER BY CS.IC_SOLIC_ESP "
		);

		log.info("Sentencia(getDocumentQuery): "   + qrySentencia.toString());
		log.info("Condiciones(getDocumentQuery): " + conditions.toString());
		log.info("getDocumentQuery (S)");
		return qrySentencia.toString();

	}

	/**
	 * Obtiene los totales
	 * @return 
	 */
	public String getAggregateCalculationQuery(){

		log.info("getAggregateCalculationQuery (E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();

		if(!icUsuario.equals("")){
			condicion.append("AND ( CE.IC_USUARIO = ? OR CE.IC_AREA IN ( SELECT IC_AREA FROM COTCAT_EJECUTIVO WHERE IC_USUARIO = ? AND CS_DIRECTOR = 'S') ) ");
			conditions.add(icUsuario);
			conditions.add(icUsuario);
		}
		if(!fechaInicio.equals("")){
			condicion.append("AND CS.DF_SOLICITUD BETWEEN TO_DATE (?, 'DD/MM/YYYY') AND TO_DATE (?, 'DD/MM/YYYY') + 1 ");
			conditions.add(fechaInicio);
			conditions.add(fechaFinal);
		}
		if(!numeroSolicitud.equals("")){
			condicion.append("AND (TO_CHAR(CS.DF_SOLICITUD,'YYYY') || TO_CHAR(CS.DF_SOLICITUD,'MM') || '-S' || CS.IC_SOLIC_ESP) = ? ");
			conditions.add(numeroSolicitud);
		}
		if(!moneda.equals("")){
			condicion.append("AND CS.IC_MONEDA = ? ");
			conditions.add(moneda);
		}
		if(!montoInicio.equals("")){
			condicion.append("AND CS.FN_MONTO BETWEEN ? AND ? ");
			conditions.add(montoFinal);
			conditions.add(montoInicio);
		}
		if(!estatus.equals("")){
			condicion.append("AND CS.IC_ESTATUS IN(" + this.estatus + ") ");
		}
		if(!icIf.equals("")){
			condicion.append(" AND CS.IC_IF = ? ");
			conditions.add(icIf);
		}
		if(origen.equals("CALCULADORAPESOSDOLARES")){
			condicion.append("AND ((CS.IC_MONEDA = 1 AND CS.IC_TIPO_TASA = 1) OR (CS.IC_MONEDA = 54 AND CS.IC_TIPO_TASA IN (1, 2))) ");
		}

		qrySentencia.append(
			"SELECT CS.IC_SOLIC_ESP "                  +
			"FROM COT_SOLIC_ESP CS, "                  +
			"COTCAT_EJECUTIVO CE, "                    +
			"COTCAT_ESTATUS E, "                       +
			"COMCAT_MONEDA CM, "                       +
			"COMCAT_IF CI "                            +
			"WHERE CS.IC_EJECUTIVO = CE.IC_EJECUTIVO " +
			"AND CS.IC_ESTATUS = E.IC_ESTATUS "        +
			"AND CI.IC_IF(+) = CS.IC_IF "              +
			"AND CS.IC_MONEDA = CM.IC_MONEDA "         +
			condicion.toString()                       +
			"ORDER BY CS.IC_SOLIC_ESP "
		);

		log.info("Sentencia(getAggregateCalculationQuery): "   + qrySentencia.toString());
		log.info("Condiciones(getAggregateCalculationQuery): " + conditions.toString());
		log.info("getAggregateCalculationQuery (S)");
		return qrySentencia.toString();

	}

	/**
	 * Obtiene los datos por paginaci�n utilizando las llaves primarias
	 * @return 
	 * @param pageIds
	 */
	public String getDocumentSummaryQueryForIds(List ids){

		log.info("getDocumentSummaryQueryForIds (E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();

		condicion.append("AND CS.IC_SOLIC_ESP IN (");
		for (int i = 0; i < ids.size(); i++){
			List lItem = (ArrayList)ids.get(i);
			if(i > 0){
				condicion.append(",");
			}
			condicion.append(" ? " );
			conditions.add(new Long(lItem.get(0).toString()));
		}
		condicion.append(") ");

		qrySentencia.append(
			"SELECT DISTINCT CS.IC_SOLIC_ESP AS IC_SOLIC_ESP, "                                                                      +
			"TO_CHAR (CS.DF_SOLICITUD, 'DD/MM/YYYY') AS FECHA_SOLICITUD, "                                                           +
			//"TO_CHAR (CS.DF_COTIZACION, 'DD/MM/YYYY HH24:MI:SS' ) AS FECHA_COTIZACION, "                                             +
			"TO_CHAR (CS.DF_COTIZACION, 'DD/MM/YYYY' ) AS FECHA_COTIZACION, "                                                        +
			"(TO_CHAR (CS.DF_SOLICITUD, 'YYYY') || TO_CHAR (CS.DF_SOLICITUD, 'MM') || '-' || CS.IG_NUM_SOLIC) AS NUMERO_SOLICITUD, " +
			"CM.CD_NOMBRE AS MONEDA, "                                                                                               +
			"(CE.CG_NOMBRE || ' ' || CE.CG_APPATERNO || ' ' || CE.CG_APMATERNO) AS EJECUTIVO, "                                      +
			"CS.FN_MONTO AS MONTO, "                                                                                                 +
			"CS.IG_PLAZO AS PLAZO, "                                                                                                 +
			"E.CG_DESCRIPCION AS ESTATUS, "                                                                                          +
			"CS.IG_TASA_MD TASA_MISMO_DIA, "                                                                                         +
			"CS.IG_TASA_SPOT AS TASA_SPOT, "                                                                                         +
			"CS.IG_NUM_REFERENCIA AS NUM_REFERENCIA, "                                                                               +
			"CS.CG_CAUSA_RECHAZO AS CAUSA_RECHAZO, "                                                                                 +
			"CS.CG_OBSERVACIONES_SIRAC AS OBSER_OPER, "                                                                              +
			"CS.IC_ESTATUS AS IC_ESTATUS, "                                                                                          +
			"DECODE (CS.CS_ACEPTADO_MD, 'S', CS.IG_TASA_MD, CS.IG_TASA_SPOT ) AS TASA_CONFIRMADA, "                                  +
			"DECODE (CS.IC_IF, NULL, CS.CG_RAZON_SOCIAL_IF, CI.CG_RAZON_SOCIAL ) AS CG_RAZON_SOCIAL, "                               +
			"CS.IC_MONEDA, "                                                                                                         +
			"CS.IC_TIPO_TASA, "                                                                                                      +
			"CASE WHEN IE.CG_SOLICITAR_AUT = 'T' THEN 'SI' "                                                                         +
			"WHEN IE.CG_SOLICITAR_AUT = 'N' THEN 'NO' "                                                                              +
			"WHEN IE.CG_SOLICITAR_AUT = 'M' AND CS.FN_MONTO >= IE.FN_MONTO_MAYOR_A THEN 'SI' "                                       +
			"ELSE 'NO' END AS SOLICITAR_AUT, "                                                                                       +
			"DECODE (CS.CG_TIPO_COTIZACION, 'I', 'INDICATIVA', 'F', 'EN FIRME', '' ) AS TIPOCOTIZA, "                                +
			"CS.CG_UT_PPC, "                                                                                                         +
			"CS.CG_UT_PPI, "                                                                                                         +
			"CS.IC_ESQUEMA_RECUP, "                                                                                                  +
			"CS.CS_LINEA "                                                                                                           +
			"FROM COT_SOLIC_ESP CS, "                                                                                                +
			"COTCAT_EJECUTIVO CE, "                                                                                                  +
			"COTCAT_ESTATUS E, "                                                                                                     +
			"COMCAT_MONEDA CM, "                                                                                                     +
			"COTREL_IF_EJECUTIVO IE, "                                                                                               +
			"COMCAT_IF CI "                                                                                                          +
			"WHERE CS.IC_EJECUTIVO = CE.IC_EJECUTIVO "                                                                               +
			"AND CS.IC_ESTATUS = E.IC_ESTATUS "                                                                                      +
			"AND CS.IC_MONEDA = CM.IC_MONEDA "                                                                                       +
			"AND CS.IC_IF = IE.IC_IF(+) "                                                                                            +
			"AND CI.IC_IF(+) = CS.IC_IF "                                                                                            +
			condicion.toString()                                                                                                     +
			//"AND CS.IC_SOLIC_ESP IN (6551, 6595, 6596, 6597, 6779, 6780, 6803, 6804, 6844) " +
			"ORDER BY CS.IC_SOLIC_ESP "
		);

		log.info("Sentencia (getDocumentSummaryQueryForIds): "   + qrySentencia.toString());
		log.info("Condiciones(getDocumentSummaryQueryForIds): " + conditions.toString());
		log.info(" getDocumentSummaryQueryForIds (S)");
		return qrySentencia.toString();

	}

	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){

		log.info("crearPageCustomFile (E)");
		HttpSession session     = request.getSession();
		CreaArchivo creaArchivo = new CreaArchivo();
		String nombreArchivo    = "";
		String fechaSolicitud   = "";
		String numSolicitud     = "";
		String solicitarAut     = "";
		String razonSocial      = "";
		String moneda           = "";
		String ejecutivo        = "";
		String monto            = "";
		String plazo            = "";
		String estatus          = "";
		String tasaMismoDia     = "";
		String tasaSpot         = "";
		String tasaConfirm      = "";
		String tipoCotiza       = "";
		String causaRechazo     = "";
		String planPago         = "";
		String obserOper        = "";
		String cotizadaLinea    = "";
		String ic_plan_de_pagos = "";
		String descTasaInd      = "";

		if(tipo.equals("PDF")){
			try{

				ComunesPDF pdfDoc             = new ComunesPDF();
				StringBuffer contenidoArchivo = new StringBuffer();

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				String meses[]     = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

				pdfDoc.setTable(16,100);
				pdfDoc.setCell("Fecha de Solicitud",                  "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de Solicitud",                 "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Requiere autorizaci�n del ejecutivo", "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Intermediario financiero",            "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda",                              "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Ejecutivo",                           "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Total Requerido",               "celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Plazo de la Operaci�n",               "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus",                             "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Cotizada en L�nea",                   "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa mismo dia",                      "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa 48 horas",                       "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa Confirmada",                     "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de Cotizacion",                  "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Causas de rechazo",                   "celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Observaciones de Operaciones",        "celda01",ComunesPDF.CENTER);

				while (rs.next()){

					fechaSolicitud = (rs.getString("FECHA_SOLICITUD") == null) ? "" : rs.getString("FECHA_SOLICITUD");
					numSolicitud   = (rs.getString("NUMERO_SOLICITUD")== null) ? "" : rs.getString("NUMERO_SOLICITUD");
					solicitarAut   = (rs.getString("SOLICITAR_AUT")   == null) ? "" : rs.getString("SOLICITAR_AUT");
					razonSocial    = (rs.getString("CG_RAZON_SOCIAL") == null) ? "" : rs.getString("CG_RAZON_SOCIAL");
					moneda         = (rs.getString("MONEDA")          == null) ? "" : rs.getString("MONEDA");
					ejecutivo      = (rs.getString("EJECUTIVO")       == null) ? "" : rs.getString("EJECUTIVO");
					monto          = (rs.getString("MONTO")           == null) ? "" : rs.getString("MONTO");
					plazo          = (rs.getString("PLAZO")           == null) ? "" : rs.getString("PLAZO");
					estatus        = (rs.getString("ESTATUS")         == null) ? "" : rs.getString("ESTATUS");
					tasaMismoDia   = (rs.getString("TASA_MISMO_DIA")  == null) ? "" : rs.getString("TASA_MISMO_DIA");
					tasaSpot       = (rs.getString("TASA_SPOT")       == null) ? "" : rs.getString("TASA_SPOT");
					tasaConfirm    = (rs.getString("TASA_CONFIRMADA") == null) ? "" : rs.getString("TASA_CONFIRMADA");
					tipoCotiza     = (rs.getString("TIPOCOTIZA")      == null) ? "" : rs.getString("TIPOCOTIZA");
					causaRechazo   = (rs.getString("CAUSA_RECHAZO")   == null) ? "" : rs.getString("CAUSA_RECHAZO");
					obserOper      = (rs.getString("OBSER_OPER")      == null) ? "" : rs.getString("OBSER_OPER");
					cotizadaLinea  = (rs.getString("CS_LINEA")        == null) ? "" : rs.getString("CS_LINEA").trim();

					/***** Cotizaci�n en linea *****/
					if(cotizadaLinea.equals("S")){
						cotizadaLinea = "SI";
					} else{
						cotizadaLinea = "NO";
					}

					/***** Formato de cantidades *****/
					if(!monto.equals("")){
						monto = "$" + Comunes.formatoDecimal(monto, 2);
					}

					/***** Formato a los campos de porcentajes *****/
					this.setIc_moneda(rs.getString("IC_MONEDA"));
					this.setIg_plazo(plazo);
					this.setIc_tipo_tasa(rs.getString("IC_TIPO_TASA"));
					this.setIc_esquema_recup(rs.getString("IC_ESQUEMA_RECUP"));
					this.setCg_ut_ppc(rs.getString("CG_UT_PPC"));
					this.setCg_ut_ppi(rs.getString("CG_UT_PPI"));
					if((rs.getString("IC_ESQUEMA_RECUP")).equals("3"))
						ic_plan_de_pagos = rs.getString("IC_SOLIC_ESP");
					descTasaInd = this.getDescTasaIndicativa();

					if(!(rs.getString("TASA_MISMO_DIA")).equals("") && !(rs.getString("IC_ESTATUS")).equals("3")){
						tasaMismoDia = descTasaInd + " " + rs.getString("TASA_MISMO_DIA") + " %";
					} else{
						tasaMismoDia = "";
					}
					if(!(rs.getString("TASA_SPOT")).equals("") && !(rs.getString("IC_ESTATUS")).equals("3")){
						tasaSpot = descTasaInd + " " + rs.getString("TASA_SPOT") + " %";
					} else{
						tasaSpot = "";
					}
					if((rs.getString("IC_ESTATUS")).equals("7") || (rs.getString("IC_ESTATUS")).equals("9")){
						tasaConfirm = descTasaInd + " " + rs.getString("TASA_CONFIRMADA") + " %";
					} else{
						tasaConfirm = "";
					}
					if(!(rs.getString("PLAZO")).equals("")){
						plazo = rs.getString("PLAZO") + " d�as";
					} else{
						plazo = "";
					}

					pdfDoc.setCell(fechaSolicitud, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(numSolicitud,   "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(solicitarAut,   "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(razonSocial,    "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(moneda,         "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(ejecutivo,      "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(monto,          "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(plazo,          "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(estatus,        "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(cotizadaLinea,  "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(tasaMismoDia,   "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(tasaSpot,       "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(tasaConfirm,    "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(tipoCotiza,     "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(causaRechazo,   "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(obserOper,      "formas", ComunesPDF.CENTER);
				}

				pdfDoc.addTable();
				pdfDoc.endDocument();

			} catch(Throwable e){
				throw new AppException("Error al generar el archivo PDF. ", e);
			}
		} else if(tipo.equals("CSV")){
			try{

				StringBuffer contenidoArchivo = new StringBuffer();
				contenidoArchivo.append("Fecha de Solicitud,");
				contenidoArchivo.append("N�mero de Solicitud,");
				contenidoArchivo.append("Requiere autorizaci�n del ejecutivo,");
				contenidoArchivo.append("Intermediario financiero,");
				contenidoArchivo.append("Moneda,");
				contenidoArchivo.append("Ejecutivo,");
				contenidoArchivo.append("Monto Total Requerido,");
				contenidoArchivo.append("Plazo de la Operaci�n,");
				contenidoArchivo.append("Estatus,");
				contenidoArchivo.append("Cotizada en L�nea,");
				contenidoArchivo.append("Tasa mismo dia,");
				contenidoArchivo.append("Tasa 48 horas,");
				contenidoArchivo.append("Tasa Confirmada,");
				contenidoArchivo.append("Tipo de Cotizacion,");
				contenidoArchivo.append("Causas de rechazo,");
				contenidoArchivo.append("Observaciones de Operaciones,");
				contenidoArchivo.append("\n");

				while(rs.next()){

					fechaSolicitud = (rs.getString("FECHA_SOLICITUD") == null) ? "" : rs.getString("FECHA_SOLICITUD");
					numSolicitud   = (rs.getString("NUMERO_SOLICITUD")== null) ? "" : rs.getString("NUMERO_SOLICITUD");
					solicitarAut   = (rs.getString("SOLICITAR_AUT")   == null) ? "" : rs.getString("SOLICITAR_AUT");
					razonSocial    = (rs.getString("CG_RAZON_SOCIAL") == null) ? "" : rs.getString("CG_RAZON_SOCIAL");
					moneda         = (rs.getString("MONEDA")          == null) ? "" : rs.getString("MONEDA");
					ejecutivo      = (rs.getString("EJECUTIVO")       == null) ? "" : rs.getString("EJECUTIVO");
					monto          = (rs.getString("MONTO")           == null) ? "" : rs.getString("MONTO");
					plazo          = (rs.getString("PLAZO")           == null) ? "" : rs.getString("PLAZO");
					estatus        = (rs.getString("ESTATUS")         == null) ? "" : rs.getString("ESTATUS");
					tasaMismoDia   = (rs.getString("TASA_MISMO_DIA")  == null) ? "" : rs.getString("TASA_MISMO_DIA");
					tasaSpot       = (rs.getString("TASA_SPOT")       == null) ? "" : rs.getString("TASA_SPOT");
					tasaConfirm    = (rs.getString("TASA_CONFIRMADA") == null) ? "" : rs.getString("TASA_CONFIRMADA");
					tipoCotiza     = (rs.getString("TIPOCOTIZA")      == null) ? "" : rs.getString("TIPOCOTIZA");
					causaRechazo   = (rs.getString("CAUSA_RECHAZO")   == null) ? "" : rs.getString("CAUSA_RECHAZO");
					obserOper      = (rs.getString("OBSER_OPER")      == null) ? "" : rs.getString("OBSER_OPER");
					cotizadaLinea  = (rs.getString("CS_LINEA")        == null) ? "" : rs.getString("CS_LINEA").trim();

					/***** Cotizaci�n en linea *****/
					if(cotizadaLinea.equals("S")){
						cotizadaLinea = "SI";
					} else{
						cotizadaLinea = "NO";
					}

					/***** Formato de cantidades *****/
					if(!monto.equals("")){
						monto = "$" + Comunes.formatoDecimal(monto, 2);
					}

					/***** Formato a los campos de porcentajes *****/
					this.setIc_moneda(rs.getString("IC_MONEDA"));
					this.setIg_plazo(plazo);
					this.setIc_tipo_tasa(rs.getString("IC_TIPO_TASA"));
					this.setIc_esquema_recup(rs.getString("IC_ESQUEMA_RECUP"));
					this.setCg_ut_ppc(rs.getString("CG_UT_PPC"));
					this.setCg_ut_ppi(rs.getString("CG_UT_PPI"));
					if((rs.getString("IC_ESQUEMA_RECUP")).equals("3"))
						ic_plan_de_pagos = rs.getString("IC_SOLIC_ESP");
					descTasaInd = this.getDescTasaIndicativa();

					if(!(rs.getString("TASA_MISMO_DIA")).equals("") && !(rs.getString("IC_ESTATUS")).equals("3")){
						tasaMismoDia = descTasaInd + " " + rs.getString("TASA_MISMO_DIA") + " %";
					} else{
						tasaMismoDia = "";
					}
					if(!(rs.getString("TASA_SPOT")).equals("") && !(rs.getString("IC_ESTATUS")).equals("3")){
						tasaSpot = descTasaInd + " " + rs.getString("TASA_SPOT") + " %";
					} else{
						tasaSpot = "";
					}
					if((rs.getString("IC_ESTATUS")).equals("7") || (rs.getString("IC_ESTATUS")).equals("9")){
						tasaConfirm = descTasaInd + " " + rs.getString("TASA_CONFIRMADA") + " %";
					} else{
						tasaConfirm = "";
					}
					if(!(rs.getString("PLAZO")).equals("")){
						plazo = rs.getString("PLAZO") + " d�as";
					} else{
						plazo = "";
					}

					contenidoArchivo.append(fechaSolicitud.replace(',',' ') +",");
					contenidoArchivo.append(numSolicitud.replace(',',' ')   +",");
					contenidoArchivo.append(solicitarAut.replace(',',' ')   +",");
					contenidoArchivo.append(razonSocial.replace(',',' ')    +",");
					contenidoArchivo.append(moneda.replace(',',' ')         +",");
					contenidoArchivo.append(ejecutivo.replace(',',' ')      +",");
					contenidoArchivo.append(monto.replace(',',' ')          +",");
					contenidoArchivo.append(plazo.replace(',',' ')          +",");
					contenidoArchivo.append(estatus.replace(',',' ')        +",");
					contenidoArchivo.append(cotizadaLinea.replace(',',' ')  +",");
					contenidoArchivo.append(tasaMismoDia.replace(',',' ')   +",");
					contenidoArchivo.append(tasaSpot.replace(',',' ')       +",");
					contenidoArchivo.append(tasaConfirm.replace(',',' ')    +",");
					contenidoArchivo.append(tipoCotiza.replace(',',' ')     +",");
					contenidoArchivo.append(causaRechazo.replace(',',' ')   +",");
					contenidoArchivo.append(obserOper.replace(',',' ')      +",");
					contenidoArchivo.append("\n");
				}
				if(!creaArchivo.make(contenidoArchivo.toString(), path, ".csv")){
					nombreArchivo = "ERROR";
				}	else{
					nombreArchivo = creaArchivo.nombre;
				}
			} catch(Throwable e){
				throw new AppException("Error al generar el archivo CSV. ", e);
			}
		}

		log.info("crearPageCustomFile (S)");
		return nombreArchivo;
	}

	public String getDescTasaIndicativa(){
		String tasaInd = "";
		if("1".equals(this.ic_tipo_tasa)){ //fija MN y USD
			tasaInd = "";
		}else if("1".equals(this.ic_moneda)){ // variable y protegida MN
			tasaInd = "TIIE 28 d�as + ";
		}else if("54".equals(this.ic_moneda)){ //variable USD
			float plazo = 0;
			if(this.ic_esquema_recup.equals("1")) //cupon cero, se toma el plazo de la operacion por ser al vencimiento
				plazo = Float.parseFloat(this.ig_plazo);
			else if(this.ic_esquema_recup.equals("4")) //tipo renta, se toma la periodicidad de pago de capital porque es la misma que la de interes, la cual no se captura
				plazo = Float.parseFloat(this.cg_ut_ppc);
			else
				plazo = Float.parseFloat(this.cg_ut_ppi); // tradicional y plan de pagos se toma la periodicidad de pago de interes capturada

			if(plazo <= 35f)
				tasaInd = "Libor 1m + ";
			else if(plazo <= 95f)
				tasaInd = "Libor 3m + ";
			else if(plazo <= 185f)
				tasaInd = "Libor 6m + ";
			else if(plazo <= 365f)
				tasaInd = "Libor 12m + ";
			else
				tasaInd = ""; //si excede el plazo no se cotiza o se da tasa fija
		}
		return tasaInd;
	}

/************************************************************************
 *                          GETTERS AND SETTERS                         *
 ************************************************************************/
	public List getConditions() {
		return conditions;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFinal() {
		return fechaFinal;
	}

	public void setFechaFinal(String fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

	public String getNumeroSolicitud() {
		return numeroSolicitud;
	}

	public void setNumeroSolicitud(String numeroSolicitud) {
		this.numeroSolicitud = numeroSolicitud;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getMontoInicio() {
		return montoInicio;
	}

	public void setMontoInicio(String montoInicio) {
		this.montoInicio = montoInicio;
	}

	public String getMontoFinal() {
		return montoFinal;
	}

	public void setMontoFinal(String montoFinal) {
		this.montoFinal = montoFinal;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getIcIf() {
		return icIf;
	}

	public void setIcIf(String icIf) {
		this.icIf = icIf;
	}

	public String getIcUsuario() {
		return icUsuario;
	}

	public void setIcUsuario(String icUsuario) {
		this.icUsuario = icUsuario;
	}

	public String getUsuarioNafin() {
		return usuarioNafin;
	}

	public void setUsuarioNafin(String usuarioNafin) {
		this.usuarioNafin = usuarioNafin;
	}

	public String getOrigen() {
		return origen;
	}

	public void setOrigen(String origen) {
		this.origen = origen;
	}

	public String getIc_moneda() {
		return ic_moneda;
	}

	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}

	public String getIg_plazo() {
		return ig_plazo;
	}

	public void setIg_plazo(String ig_plazo) {
		this.ig_plazo = ig_plazo;
	}

	public String getIc_tipo_tasa() {
		return ic_tipo_tasa;
	}

	public void setIc_tipo_tasa(String ic_tipo_tasa) {
		this.ic_tipo_tasa = ic_tipo_tasa;
	}

	public String getIc_esquema_recup() {
		return ic_esquema_recup;
	}

	public void setIc_esquema_recup(String ic_esquema_recup) {
		this.ic_esquema_recup = ic_esquema_recup;
	}

	public String getCg_ut_ppc() {
		return cg_ut_ppc;
	}

	public void setCg_ut_ppc(String cg_ut_ppc) {
		this.cg_ut_ppc = cg_ut_ppc;
	}

	public String getCg_ut_ppi() {
		return cg_ut_ppi;
	}

	public void setCg_ut_ppi(String cg_ut_ppi) {
		this.cg_ut_ppi = cg_ut_ppi;
	}

}