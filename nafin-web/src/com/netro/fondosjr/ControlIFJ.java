package com.netro.fondosjr;

import com.netro.fondojr.FondoJunior;
import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ControlIFJ implements IQueryGeneratorRegExtJS {
	private final static Log log = ServiceLocator.getInstance().getLog(ControlIFJ.class);
	
	private String fechaMovimiento;
	private String clavePrograma;
	private String nombrePrograma;
	
	private List 	conditions;
	
	private String 	paginaOffset;
	private String 	paginaNo;
	
	public ControlIFJ() {
	}
	/**
	* Obtiene el query para obtener las llaves primarias de la consulta
	* @return Cadena con la consulta de SQL, para obtener llaves primarias
	*/
	public String getDocumentQuery(){
		log.info("getDocumentQuery(E) ::..");
		String whereAnd = "";
		StringBuffer strSQL = new StringBuffer();
		
		return strSQL.toString();
	}//getDocumentQuery
		/**
	* Obtiene el query necesario para mostrar la informaci�n completa de 
	* una p�gina a partir de las llaves primarias enviadas como par�metro
	* @return Cadena con la consulta de SQL, para obtener la informaci�n 
	* completa de los registros con las llaves especificadas
	*/
	public String getDocumentSummaryQueryForIds(List pageIds){
		log.info("getDocumentSummaryQueryForIds(E) ::..");
		StringBuffer strSQL = new StringBuffer();
		
		
		log.debug("..:: strSQL: " + strSQL.toString());
		log.debug("..:: conditions: " + conditions);
		
		log.info("getDocumentSummaryQueryForIds(S)");
		return strSQL.toString();
	}//getDocumentSummaryQueryForIds	
		/**
	* Obtiene el query para obtener los totales de la consulta
	* @return Cadena con la consulta de SQL, para obtener los totales
	*/
	public String getAggregateCalculationQuery(){
		log.info("getAggregateCalculationQuery(E) ::..");
		StringBuffer strSQL = new StringBuffer();
		conditions = new ArrayList();
		
		
		log.info("getAggregateCalculationQuery(S) ::..");
		return strSQL.toString();
	}//getAggregateCalculationQuery
	public String getDocumentQueryFile(){
		log.info("getDocumentQueryFile(E)");
		String whereAnd = "";
		StringBuffer strSQL = new StringBuffer();
		conditions = new ArrayList();
		
		strSQL.append("Select c.Transaccion,suma,numero from  \n"+
							"(  \n"+
							"    SELECT  \n"+
							"        'Aportaciones' as Transaccion,  \n"+
							"        SUM (fg_capital) AS suma,   \n"+
							"        COUNT (*) AS numero  \n"+
							"    FROM com_mov_gral_fondo_jr m, comcat_oper_fondo_jr c  \n"+
							"    WHERE m.ig_oper_fondo_jr = c.ig_oper_fondo_jr  \n"+
							"       AND c.cg_operacion IN ('suma', 'Suma', 'SUMA')  \n"+
							"       AND c.cg_descripcion NOT IN ('intereses', 'INTERESES', 'Intereses')  \n"+
							"       AND m.ic_programa_fondojr = 3  \n");
		if(!"".equals(fechaMovimiento)){
			strSQL.append("       AND m.df_fec_valor < TO_DATE (?, 'dd/mm/yyyy') + 1  \n");
			conditions.add(fechaMovimiento);
		}					
		strSQL.append(	" UNION    \n"+
							"       SELECT 'Cancelaciones'as Transaccion,SUM (fg_capital) AS suma, COUNT (*) AS numero  \n"+
							"      FROM com_mov_gral_fondo_jr m, comcat_oper_fondo_jr c  \n"+
							"     WHERE m.ig_oper_fondo_jr = c.ig_oper_fondo_jr  \n"+
							"       AND c.cg_operacion IN ('resta', 'Resta', 'RESTA')  \n"+
							"       AND m.ic_programa_fondojr = 3  \n");
		if(!"".equals(fechaMovimiento)){
			strSQL.append("       AND m.df_fec_valor < TO_DATE (?, 'dd/mm/yyyy') + 1  \n");
			conditions.add(fechaMovimiento);
		}					
		strSQL.append(" UNION  \n"+
							"     SELECT 'Interes' as Transaccion, SUM (fg_capital) AS suma, COUNT (*) AS numero  \n"+
							"      FROM com_mov_gral_fondo_jr m, comcat_oper_fondo_jr c  \n"+
							"     WHERE m.ig_oper_fondo_jr = c.ig_oper_fondo_jr  \n"+
							"       AND c.cg_operacion IN ('suma', 'Suma', 'SUMA')  \n"+
							"       AND c.cg_descripcion IN ('intereses', 'INTERESES', 'Intereses')  \n"+
							"       AND m.ic_programa_fondojr = 3  \n");
		if(!"".equals(fechaMovimiento)){
			strSQL.append("       AND m.df_fec_valor < TO_DATE (?, 'dd/mm/yyyy') + 1   \n");
			conditions.add(fechaMovimiento);
		}					
		strSQL.append(" UNION  \n"+
							"      SELECT 'Desembolsos' as Transaccion,SUM (fg_totalvencimiento) AS suma, COUNT (*) AS numero  \n"+
							"      FROM com_pagos_fide_val  \n"+
							"     WHERE cg_estatus = 'RF'  \n"+
							"       AND cs_validacion = 'A'  \n"+
							"       AND ic_programa_fondojr = 3  \n");
		if(!"".equals(fechaMovimiento)){
			strSQL.append("       AND df_validacion < TO_DATE (?, 'dd/mm/yyyy') + 1   \n");
			conditions.add(fechaMovimiento);
		}					
		strSQL.append( " UNION  \n"+
							"    SELECT 'Reembolsos' as Transaccion,SUM (fg_totalvencimiento) AS suma, COUNT (*) AS numero  \n"+
							"      FROM com_pagos_fide_val  \n"+
							"     WHERE cg_estatus = 'R'  \n"+
							"       AND cs_validacion = 'A'  \n"+
							"       AND ic_programa_fondojr = 3  \n");
		if(!"".equals(fechaMovimiento)){
			strSQL.append("       AND df_validacion < TO_DATE (?, 'dd/mm/yyyy') + 1   \n");
			conditions.add(fechaMovimiento);
		}					
		strSQL.append(	" UNION  \n"+
							"    SELECT 'No pagados'as Transaccion,SUM (fg_totalvencimiento) AS suma, COUNT (*) numero  \n"+
							"      FROM com_pagos_fide_val  \n"+
							"     WHERE cg_estatus = 'NP'  \n"+
							"       AND cs_validacion = 'A'  \n"+
							"       AND ic_programa_fondojr = 3  \n");
		if(!"".equals(fechaMovimiento)){
			strSQL.append("       AND com_fechaprobablepago < TO_DATE (?, 'dd/mm/yyyy') + 1   \n");
			conditions.add(fechaMovimiento);
		}					
		strSQL.append(	" UNION  \n"+
							"    SELECT 'Prepagos'as Transacciones,SUM (fg_totalvencimiento) AS suma, COUNT (*) AS numero  \n"+
							"      FROM com_pagos_fide_val  \n"+
							"     WHERE cg_estatus = 'T'  \n"+
							"       AND cs_validacion = 'A'  \n"+
							"       AND ic_programa_fondojr = 3  \n");
		if(!"".equals(fechaMovimiento)){
			strSQL.append("       AND com_fechaprobablepago < TO_DATE (?, 'dd/mm/yyyy') + 1   \n");
			conditions.add(fechaMovimiento);
		}					
		strSQL.append(	" ) C  \n");	
		System.err.println("query: "+strSQL.toString());	

		return strSQL.toString();
	}//getDocumentQueryFile
	
		public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		try {
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		}
		return nombreArchivo;
					
	}
	
	/**
	 * se imprimen todos los registros de la consulta 
	 * @return 
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		//String nombreArchivo = "";
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		try {
			if (tipo != null && tipo.equals("PDF")) {
				List resultado = new ArrayList();	
				double saldoDisponible;
				int registros=0;
				String aportaciones ="", cancelaciones="", intereses="", desembolsos="", reembolsos="", noPagados ="", prepagos= "";
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				if(fechaMovimiento.equals("")){ fechaMovimiento = fechaActual; }			
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formasG",ComunesPDF.CENTER);
				pdfDoc.addText(" ","formasG",ComunesPDF.CENTER);
				pdfDoc.addText(nombrePrograma,"formasG",ComunesPDF.CENTER);
				pdfDoc.addText(" ","formasG",ComunesPDF.CENTER);	
				pdfDoc.addText(" ","formasG",ComunesPDF.CENTER);
				pdfDoc.addText("Control Fondo JR ","formasG",ComunesPDF.CENTER);
				pdfDoc.addText(" ","formasG",ComunesPDF.CENTER);
				pdfDoc.addText("Fecha de Movimiento Hasta: "+fechaMovimiento,"formasG",ComunesPDF.CENTER);
				pdfDoc.addText(" ","formasG",ComunesPDF.CENTER);
				
				
				pdfDoc.setTable(3, 50);
				pdfDoc.setCell("Transacci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Total Transacci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("No Registros ","celda01",ComunesPDF.CENTER);
				FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);
		
			
					resultado = fondoJunior.ControlFondoJR(fechaMovimiento, clavePrograma);
				
				if(resultado.size()>0) {
					for(int x=0; x<resultado.size(); x++) {	 
						List datos = (ArrayList)resultado.get(x);	
						log.debug("datos --"+datos);
						pdfDoc.setCell((String)datos.get(0),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal((String)datos.get(1),2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(Comunes.formatoDecimal( (String)datos.get(2),0),"formas",ComunesPDF.CENTER);	
	
						String informacion = (String)datos.get(0);
	
						registros +=Integer.parseInt( (String)datos.get(2));
							
						if(informacion.equals("Aportaciones")){ 	 aportaciones = (String)datos.get(1);  }
						if(informacion.equals("Cancelaciones")){  cancelaciones = (String)datos.get(1);  }
						if(informacion.equals("Intereses")){  intereses = (String)datos.get(1);  }
						if(informacion.equals("Desembolsos")){  desembolsos = (String)datos.get(1);  }
						if(informacion.equals("Reembolsos")){  reembolsos = (String)datos.get(1);  }
						if(informacion.equals("No Pagados")){  noPagados = (String)datos.get(1);  }
						if(informacion.equals("Prepagos")){  prepagos = (String)datos.get(1);  }
							
					}
				}
					
				if(aportaciones.equals("")){ aportaciones ="0"; }
				if(cancelaciones.equals("")){ cancelaciones ="0"; }
				if(intereses.equals("")){ intereses ="0"; }
				if(desembolsos.equals("")){ desembolsos ="0"; }
				if(reembolsos.equals("")){ reembolsos ="0"; }
				if(noPagados.equals("")){ noPagados ="0"; }
				if(prepagos.equals("")){ prepagos ="0"; }
				
				//saldoDisponible = Double.parseDouble((String)aportaciones)-Double.parseDouble((String)cancelaciones)+Double.parseDouble((String)intereses)-Double.parseDouble((String)desembolsos)+Double.parseDouble((String)reembolsos)-Double.parseDouble((String)noPagados);
				saldoDisponible = Double.parseDouble((String)aportaciones)-Double.parseDouble((String)cancelaciones)+Double.parseDouble((String)intereses)+Double.parseDouble((String)desembolsos)+Double.parseDouble((String)reembolsos)-Double.parseDouble((String)noPagados)+Double.parseDouble((String)prepagos);
				String saldoDisponibleT = Double.toString (saldoDisponible);
				pdfDoc.setCell("Saldo Disponible ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(saldoDisponibleT,2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell(Comunes.formatoDecimal(String.valueOf(registros),0),"formas",ComunesPDF.CENTER);
			
				pdfDoc.addTable();
				pdfDoc.endDocument();
				
			} 
			if (tipo != null && tipo.equals("CSV")) {
				nombreArchivo = Comunes.cadenaAleatoria(16)+".csv";
				List resultado = new ArrayList();	
				FileOutputStream out  = new FileOutputStream(path+nombreArchivo);
				BufferedWriter csv  = new BufferedWriter(new OutputStreamWriter(out, "Cp1252"));
				File 		file 								= null;
				double saldoDisponible;
				int registros=0;
				String aportaciones ="", cancelaciones="",	 intereses="", desembolsos="",	 reembolsos="", noPagados ="", prepagos = "";
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				if(fechaMovimiento.equals("")){ fechaMovimiento = fechaActual; }

				FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);
				
				resultado = fondoJunior.ControlFondoJR(fechaMovimiento, clavePrograma);
				csv.write("Programa:,"+nombrePrograma+"\n" );
				csv.write(",\n" );
				csv.write("Fecha de Movimiento Hasta: "+fechaMovimiento+"\n" );
				csv.write(",\n" );
				
				csv.write("Transacci�n,Total Transacci�n, No Registros \n" );
				if(resultado.size()>0) {
					for(int x=0; x<resultado.size(); x++) {	 
						List datos = (ArrayList)resultado.get(x);
							csv.write((String)datos.get(0)+",");
							csv.write("\"$"+Comunes.formatoDecimal((String)datos.get(1),2,true)+"\",");						
							csv.write("\""+Comunes.formatoDecimal((String)datos.get(2),0,true)+"\"\n");
						
							String informacion = (String)datos.get(0);
					
							registros +=Integer.parseInt( (String)datos.get(2));
							
							if(informacion.equals("Aportaciones")){ 	 aportaciones = (String)datos.get(1);  }
							if(informacion.equals("Cancelaciones")){  cancelaciones = (String)datos.get(1);  }
							if(informacion.equals("Intereses")){  intereses = (String)datos.get(1);  }
							if(informacion.equals("Desembolsos")){  desembolsos = (String)datos.get(1);  }
							if(informacion.equals("Reembolsos")){  reembolsos = (String)datos.get(1);  }
							if(informacion.equals("No Pagados")){  noPagados = (String)datos.get(1);  }
							if(informacion.equals("Prepagos")){  prepagos = (String)datos.get(1);  }
								
					}
				}

				if(aportaciones.equals("")){ aportaciones ="0"; }
				if(cancelaciones.equals("")){ cancelaciones ="0"; }
				if(intereses.equals("")){ intereses ="0"; }
				if(desembolsos.equals("")){ desembolsos ="0"; }
				if(reembolsos.equals("")){ reembolsos ="0"; }
				if(noPagados.equals("")){ noPagados ="0"; }
				if(prepagos.equals("")){ prepagos ="0"; }
		
				//saldoDisponible = Double.parseDouble((String)aportaciones)-Double.parseDouble((String)cancelaciones)+Double.parseDouble((String)intereses)-Double.parseDouble((String)desembolsos)+Double.parseDouble((String)reembolsos)-Double.parseDouble((String)noPagados);
				saldoDisponible = Double.parseDouble((String)aportaciones)-Double.parseDouble((String)cancelaciones)+Double.parseDouble((String)intereses)+Double.parseDouble((String)desembolsos)+Double.parseDouble((String)reembolsos)-Double.parseDouble((String)noPagados)+Double.parseDouble((String)prepagos);
				String saldoDisponibleT = Double.toString (saldoDisponible);
				
				csv.write("Saldo Disponible"+",");					
				csv.write("\"$"+Comunes.formatoDecimal(saldoDisponibleT,2,true)+"\",");
				csv.write("\""+Comunes.formatoDecimal(registros,0,true)+"\"\n");
								
				if(csv 	!= null )  csv.close();
		
				file = new File(path + nombreArchivo);		
			}
			
					
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
			/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	
	public void setFechaMovimiento(String fechaMovimiento) {
		this.fechaMovimiento = fechaMovimiento;
	}


	public String getFechaMovimiento() {
		return fechaMovimiento;
	}


	public void setClavePrograma(String clavePrograma) {
		this.clavePrograma = clavePrograma;
	}


	public String getClavePrograma() {
		return clavePrograma;
	}

	public void setNombrePrograma(String nombrePrograma) {
		this.nombrePrograma = nombrePrograma;
	}


	public String getNombrePrograma() {
		return nombrePrograma;
	}


}