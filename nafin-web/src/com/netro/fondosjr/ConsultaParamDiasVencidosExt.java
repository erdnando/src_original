package com.netro.fondosjr;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsultaParamDiasVencidosExt implements IQueryGeneratorRegExtJS {

	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaParamDiasVencidosExt.class);
	 
  private String fechaVenDesde="";
	private String fechaVenA="";
	private String diasVencimiento="";
	private String detalle="";
	private String programa="";
	private String fechaModificacion="";
	private String usuarioModifico="";
	private String operacion="";
	private String modificar="";
	private String accion="";
  private List   conditions;
  StringBuffer qrySentencia = new StringBuffer("");
  
  public ConsultaParamDiasVencidosExt() {  }
  
  	/**
	 * Este m�todo debe regresar un query con el que se obtienen los 
	 * identificadores unicos de los registros a mostrar en la consulta
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public  String getDocumentQuery(){
    qrySentencia 	= new StringBuffer();
    conditions 		= new ArrayList();
    
		 System.out.println("fechaVenDesde  "+fechaVenDesde);
		 System.out.println("fechaVenA  "+fechaVenA);
		 System.out.println("diasVencimiento  "+diasVencimiento);				 
			
	 	qrySentencia.append(" SELECT ");
		qrySentencia.append(" iC_NUM_OPERACION as nOperacion, ");
		qrySentencia.append(" TO_CHAR (DF_FECHA_VMTO_INI, 'dd/mm/yyyy') AS fec_ven_ini,");
		qrySentencia.append("	TO_CHAR (DF_FECHA_VMTO_FIN , 'dd/mm/yyyy') AS fec_ven_fin, ");
	  qrySentencia.append("	IG_DIAS_VENCIMIENTO as diasVencimiento,");
		qrySentencia.append("	CG_DETALLE  AS detalle, ");
		qrySentencia.append("	CG_USER_MODIFICA  as nomUsuario,  ");
		qrySentencia.append("	TO_CHAR (DF_ULT_MODIFICA, 'dd/mm/yyyy') AS fec_modificacion ");
		qrySentencia.append("	from COM_CRED_PARAM_DIAS_VENC ");
		qrySentencia.append("	where IC_PROGRAMA_FONDOJR = "+programa);
						
			if (fechaVenDesde!=null && !fechaVenDesde.equals("")) {
				qrySentencia.append(" AND df_fecha_vmto_ini >= TO_DATE( ?,'DD/MM/YYYY')  ");			
				conditions.add(fechaVenDesde);
			}
			if (fechaVenA!=null && !fechaVenA.equals("")) {
				qrySentencia.append(" AND DF_FECHA_VMTO_FIN <= TO_DATE( ?,'DD/MM/YYYY') ");				
				conditions.add(fechaVenA);
			}
			
			if (operacion!=null && !operacion.equals("")) {
			 qrySentencia.append(" AND IC_NUM_OPERACION =?");
				conditions.add(operacion);
			}
			
			if (diasVencimiento!=null && !diasVencimiento.equals("")) {
			 qrySentencia.append(" AND IG_DIAS_VENCIMIENTO =?");
				conditions.add(diasVencimiento);
			}
	
		qrySentencia.append(" 	order by ic_num_operacion asc ");
	
		log.debug("getDocumentQuery "+conditions.toString());
		log.debug("getDocumentQuery "+qrySentencia.toString());
		return qrySentencia.toString();
  }
  
  
	/**
	 * Este m�todo debe regresar un query con el que se obtienen totales de la
	 * consulta.
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public  String getAggregateCalculationQuery(){
    qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();

		qrySentencia.append(" SELECT  count(*)  ");
		qrySentencia.append("	from COM_CRED_PARAM_DIAS_VENC ");
		qrySentencia.append("	where IC_PROGRAMA_FONDOJR = "+programa);	
			
			if (fechaVenDesde!=null && !fechaVenDesde.equals("")) {
				qrySentencia.append(" AND df_fecha_vmto_ini >= TO_DATE( ?,'DD/MM/YYYY')  ");			
				conditions.add(fechaVenDesde);
			}
			if (fechaVenA!=null && !fechaVenA.equals("")) {
				qrySentencia.append(" AND DF_FECHA_VMTO_FIN <= TO_DATE( ?,'DD/MM/YYYY') ");				
				conditions.add(fechaVenA);
			}
			
			if (operacion!=null && !operacion.equals("")) {
			 qrySentencia.append(" AND IC_NUM_OPERACION =?");
				conditions.add(operacion);
			}
			
			if (diasVencimiento!=null && !diasVencimiento.equals("")) {
			 qrySentencia.append(" AND IG_DIAS_VENCIMIENTO =?");
				conditions.add(diasVencimiento);
			}
		
		 qrySentencia.append(" 	order by ic_num_operacion asc ");
		
		log.debug("getAggregateCalculationQuery "+conditions.toString());
		log.debug("getAggregateCalculationQuery "+qrySentencia.toString());
		return qrySentencia.toString();
  }
  
	/**
	 * Este m�todo debe regresar una Lista de parametros que ser�n usados
	 * como valor de las variables BIND de los queries generados.
	 * @return Lista con los valores a usar en las variables bind
	 */
	public  List getConditions(){
  return conditions;
  }
  
  /**
	 * Este m�todo debe regresar un query con el que se obtienen los 
	 * datos a mostrar en la consulta basandose en los identificadores unicos 
	 * especificados en las lista de ids
	 * @param ids Lista de los identificadoes unicos. El tama�o de la lista 
	 * 	recibida ser� de acuerdo a la configuraci�n de registros x p�gina
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public  String getDocumentSummaryQueryForIds(List ids){
    qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();		
		
		qrySentencia.append(" SELECT ");
		qrySentencia.append(" iC_NUM_OPERACION as nOperacion, ");
		qrySentencia.append(" TO_CHAR (DF_FECHA_VMTO_INI, 'dd/mm/yyyy') AS fec_ven_ini,");
		qrySentencia.append("	TO_CHAR (DF_FECHA_VMTO_FIN , 'dd/mm/yyyy') AS fec_ven_fin, ");
	  qrySentencia.append("	IG_DIAS_VENCIMIENTO as diasVencimiento,");
		qrySentencia.append("	CG_DETALLE  AS detalle, ");
		qrySentencia.append("	CG_USER_MODIFICA  as nomUsuario,  ");
		qrySentencia.append("	TO_CHAR (DF_ULT_MODIFICA, 'dd/mm/yyyy') AS fec_modificacion ");
		qrySentencia.append("	from COM_CRED_PARAM_DIAS_VENC ");
		qrySentencia.append("	where IC_PROGRAMA_FONDOJR = "+programa);
			
			if (fechaVenDesde!=null && !fechaVenDesde.equals("")) {
				qrySentencia.append(" AND df_fecha_vmto_ini >= TO_DATE( ?,'DD/MM/YYYY')  ");			
				conditions.add(fechaVenDesde);
			}
			if (fechaVenA!=null && !fechaVenA.equals("")) {
				qrySentencia.append(" AND DF_FECHA_VMTO_FIN <= TO_DATE( ?,'DD/MM/YYYY') ");				
				conditions.add(fechaVenA);
			}
			
			if (operacion!=null && !operacion.equals("")) {
			 qrySentencia.append(" AND IC_NUM_OPERACION =?");
				conditions.add(operacion);
			}
			
			if (diasVencimiento!=null && !diasVencimiento.equals("")) {
			 qrySentencia.append(" AND IG_DIAS_VENCIMIENTO =?");
				conditions.add(diasVencimiento);
			}		
			
			if(accion.equals("")){
				qrySentencia.append(" and  ic_num_operacion in ( ");			 
				List   pKeys = new ArrayList();
				for(int i = 0; i < ids.size(); i++){
					List lItem = (ArrayList)ids.get(i);
					if(i>0){
					qrySentencia.append(",?");
					} else{
					qrySentencia.append("?");
					}
					conditions.add(lItem.get(0));
					pKeys.add(ids.get(i));			
				}
				qrySentencia.append(") ");
			}
		
		qrySentencia.append(" 	order by ic_num_operacion asc ");
		
		log.debug("getDocumentSummaryQueryForIds "+conditions.toString());
		log.debug("getDocumentSummaryQueryForIds "+qrySentencia.toString());
		return qrySentencia.toString();
  }
  
	/**
	 * Este m�todo debe regresar un query que obtendr� todos los registros
	 * resultantes de la b�squeda, con la finalidad de generar un archivo.
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	public  String getDocumentQueryFile(){
  
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();

		qrySentencia.append(" SELECT ");
		qrySentencia.append(" iC_NUM_OPERACION as nOperacion, ");
		qrySentencia.append(" TO_CHAR (DF_FECHA_VMTO_INI, 'dd/mm/yyyy') AS fec_ven_ini,");
		qrySentencia.append("	TO_CHAR (DF_FECHA_VMTO_FIN , 'dd/mm/yyyy') AS fec_ven_fin, ");
	  qrySentencia.append("	IG_DIAS_VENCIMIENTO as diasVencimiento,");
		qrySentencia.append("	CG_DETALLE  AS detalle, ");
		qrySentencia.append("	CG_USER_MODIFICA  as nomUsuario,  ");
		qrySentencia.append("	TO_CHAR (DF_ULT_MODIFICA, 'dd/mm/yyyy') AS fec_modificacion ");
		qrySentencia.append("	from COM_CRED_PARAM_DIAS_VENC ");
		qrySentencia.append("	where IC_PROGRAMA_FONDOJR = "+programa);
	
					
			if (fechaVenDesde!=null && !fechaVenDesde.equals("")) {
				qrySentencia.append(" AND df_fecha_vmto_ini >= TO_DATE( ?,'DD/MM/YYYY')  ");			
				conditions.add(fechaVenDesde);
			}
			if (fechaVenA!=null && !fechaVenA.equals("")) {
				qrySentencia.append(" AND DF_FECHA_VMTO_FIN <= TO_DATE( ?,'DD/MM/YYYY') ");				
				conditions.add(fechaVenA);
			}
			
			if (operacion!=null && !operacion.equals("")) {
			 qrySentencia.append(" AND IC_NUM_OPERACION =?");
				conditions.add(operacion);
			}
			
			if (diasVencimiento!=null && !diasVencimiento.equals("")) {
			 qrySentencia.append(" AND IG_DIAS_VENCIMIENTO =?");
				conditions.add(diasVencimiento);
			}		
	
		
		qrySentencia.append(" 	order by ic_num_operacion asc ");
		
		log.debug("getDocumentQueryFile "+conditions.toString());
		log.debug("getDocumentQueryFile "+qrySentencia.toString());
		return qrySentencia.toString();
  }
  
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public  String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
    log.debug("crearCustomFile (E)");
    String nombreArchivo ="";
    HttpSession session = request.getSession();
    CreaArchivo creaArchivo = new CreaArchivo();
    StringBuffer contenidoArchivo = new StringBuffer();
      
    OutputStreamWriter writer = null;
    BufferedWriter buffer = null;
    int total = 0;
    try {
		nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
		writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
		buffer = new BufferedWriter(writer);
			
		contenidoArchivo = new StringBuffer();	
    contenidoArchivo.append("Consulta de par�metros para la aplicaci�n del l�mite de vencimiento\n");
		contenidoArchivo.append( " , Fecha de Vencimiento\n");
		contenidoArchivo.append( "No. Operaci�n, Desde, Hasta, D�as limite de Vencimiento, Usuario �ltima modificaci�n, Fecha �ltima modificaci�n\n");
    
		while (rs.next())	{
			String noOperacion 	= (rs.getString("NOPERACION") 						== null) ? "" : rs.getString("NOPERACION");
			String fechaVenIni 	= (rs.getString("FEC_VEN_INI") 	== null) ? "" : rs.getString("FEC_VEN_INI");
			String fechaVenFin 	= (rs.getString("FEC_VEN_FIN") 					== null) ? "" : rs.getString("FEC_VEN_FIN");				
			String diasLimite 	= (rs.getString("DIASVENCIMIENTO") 	== null) ? "" : rs.getString("DIASVENCIMIENTO");
			String usuario  	 	= (rs.getString("NOMUSUARIO") 					== null) ? "" : rs.getString("NOMUSUARIO");				
			String fechaModif		= (rs.getString("FEC_MODIFICACION")== null) ? "" : rs.getString("FEC_MODIFICACION");
			
			contenidoArchivo.append(
			"'"+noOperacion.replace	(',',' ')+","+
			fechaVenIni.replace	(',',' ')+","+		
			fechaVenFin.replace	(',',' ')+","+
			diasLimite.replace	(',',' ')+","+
			usuario.replace		  (',',' ')+","+
			fechaModif.replace	(',',' ')+","+ "\n");
		}
			
		creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
		nombreArchivo = creaArchivo.getNombre();
		
		}catch (Exception e){
			throw new AppException("Error al generar el archivo ", e);
		}
	log.debug("crearCustomFile (S)");
	return nombreArchivo;   
  }
  
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo){
    String nombreArchivo = "";
    HttpSession session = request.getSession();	
    ComunesPDF pdfDoc = new ComunesPDF();
    CreaArchivo creaArchivo = new CreaArchivo();
    StringBuffer contenidoArchivo = new StringBuffer();
    
    try {
		nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
		pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
		String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String diaActual    = fechaActual.substring(0,2);
		String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		String anioActual   = fechaActual.substring(6,10);
		String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
		pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
		(String)session.getAttribute("sesExterno"),
		(String) session.getAttribute("strNombre"),
		(String) session.getAttribute("strNombreUsuario"),
		(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
		pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
		pdfDoc.setTable(6,90);
			
		pdfDoc.setCell("No. Operaci�n"								,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de Vencimiento Desde"	  ,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha de Vencimiento Hasta"		,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("D�as limite de Vencimiento"   ,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Usuario �ltima modificaci�n"	,"celda01",ComunesPDF.CENTER);
		pdfDoc.setCell("Fecha �ltima modificaci�n"		,"celda01",ComunesPDF.CENTER);
			
		while(reg.next()){
			String noOperacion= (reg.getString("NOPERACION") 					== null) ? "" : reg.getString("NOPERACION");
			String fecVenIni 	= (reg.getString("FEC_VEN_INI") 	== null) ? "" : reg.getString("FEC_VEN_INI");
			String fecVenFin 	= (reg.getString("FEC_VEN_FIN") 		== null) ? "" : reg.getString("FEC_VEN_FIN");
			String diasVencim = (reg.getString("DIASVENCIMIENTO") 		== null) ? "" : reg.getString("DIASVENCIMIENTO");
			String usuario 		= (reg.getString("NOMUSUARIO") 			== null) ? "" : reg.getString("NOMUSUARIO");
			String fecMod 		= (reg.getString("FEC_MODIFICACION") == null) ? "" : reg.getString("FEC_MODIFICACION");

			pdfDoc.setCell(noOperacion,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fecVenIni	,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fecVenFin		,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(diasVencim	,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(usuario		,"formas",ComunesPDF.CENTER);
			pdfDoc.setCell(fecMod		,"formas",ComunesPDF.CENTER);
		}
		pdfDoc.addTable();
		pdfDoc.endDocument();
	} catch(Exception e){
		throw new AppException("Error al generar el archivo PDF ", e);
	}
	return nombreArchivo;  
  }


  public void setFechaVenDesde(String fechaVenDesde)  {
    this.fechaVenDesde = fechaVenDesde;
  }


  public String getFechaVenDesde()  {
    return fechaVenDesde;
  }


  public void setFechaVenA(String fechaVenA)  {
    this.fechaVenA = fechaVenA;
  }


  public String getFechaVenA()  {
    return fechaVenA;
  }


  public void setDiasVencimiento(String diasVencimiento)  {
    this.diasVencimiento = diasVencimiento;
  }


  public String getDiasVencimiento()  {
    return diasVencimiento;
  }


  public void setDetalle(String detalle)  {
    this.detalle = detalle;
  }


  public String getDetalle()  {
    return detalle;
  }


  public void setPrograma(String programa)  {
    this.programa = programa;
  }


  public String getPrograma()  {
    return programa;
  }


  public void setFechaModificacion(String fechaModificacion)  {
    this.fechaModificacion = fechaModificacion;
  }


  public String getFechaModificacion()  {
    return fechaModificacion;
  }


  public void setUsuarioModifico(String usuarioModifico)  {
    this.usuarioModifico = usuarioModifico;
  }


  public String getUsuarioModifico()  {
    return usuarioModifico;
  }


  public void setOperacion(String operacion)  {
    this.operacion = operacion;
  }


  public String getOperacion()  {
    return operacion;
  }


  public void setModificar(String modificar)  {
    this.modificar = modificar;
  }


  public String getModificar()  {
    return modificar;
  }


	public void setAccion(String accion) {
		this.accion = accion;
	}


	public String getAccion() {
		return accion;
	}
  
}