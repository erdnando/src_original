package com.netro.fondosjr;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsultaMonitorFondoJR implements IQueryGeneratorReg, IQueryGeneratorRegExtJS {
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaMonitorFondoJR.class);
	
	/**
	 * Constructor de la clase.
	 */
	public ConsultaMonitorFondoJR(){}
	
	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer 	strSQL;
	private List 	conditions;
	private String paginaOffset;
	private String paginaNo;
	private String strDirectorioPublicacion;
	private String cvePrograma;
	private String fec_venc_ini;
	private String fec_venc_fin;
	private String estatusCarga;		//FODEA 026-2010 Rebos
	private String chkDesembolso;
	private String descPrograma;

	/**
	* Obtiene el query para obtener los totales de la consulta
	* @return Cadena con la consulta de SQL, para obtener los totales
	*/
	public String getAggregateCalculationQuery(){
		log.info("getAggregateCalculationQuery(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		log.info("getAggregateCalculationQuery(S) ::..");
		return strSQL.toString();
	}//getAggregateCalculationQuery
	
	/**
	* Obtiene el query para obtener las llaves primarias de la consulta
	* @return Cadena con la consulta de SQL, para obtener llaves primarias
	*/
	public String getDocumentQuery(){
		log.info("getDocumentQuery(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();			
		
		strSQL.append("SELECT  tmonitor.orden , " );
		strSQL.append("		tmonitor.estatus , " );
		strSQL.append("		tmonitor.fecha_venc , " );
		strSQL.append("        tmonitor.folio " );
		strSQL.append("FROM ( " );
		
		if( "V".equals(estatusCarga) || "B".equals(estatusCarga) ){
		
			strSQL.append(" 			SELECT 1 orden, " );
			strSQL.append(" 			DECODE(cg_tipo_registro,'Reembolsos al dia','BR', " );
			strSQL.append("    									'Pagados y No Pagados','BP', " );
			strSQL.append("    									'Recuperaciones al dia','BRC', " );
			strSQL.append("    									'Prepagos al dia','BT')estatus, " );
			strSQL.append("        DECODE(com_fechaprobablepago,null, " );
			strSQL.append("               		TO_CHAR (DF_FECHA_BIT, 'DD/MM/YYYY'), " );
			strSQL.append("               		TO_CHAR (com_fechaprobablepago, 'DD/MM/YYYY')) fecha_venc, ");
			strSQL.append("         TO_CHAR (df_registro_fide, 'DD/MM/YYYY HH:MI:SS') ultimo_reg, " );
			strSQL.append("         ig_total_reg total_reg, " );
			strSQL.append("         fg_monto_total monto_tot, " );
			strSQL.append("         nvl(ig_folio,0) folio,  " );
			strSQL.append("         cg_estatus_carga estatus_proc, " );
			strSQL.append("         cg_tipo_registro tipo_reg,          " );
			strSQL.append("         cg_usuario usuario  " );
			strSQL.append("    FROM bit_monitor_fondojr " );
			strSQL.append("		WHERE ic_programa_fondojr = ? ");		
			if(fec_venc_ini!=null && !fec_venc_ini.equals("") && fec_venc_fin!=null && !fec_venc_fin.equals("")){
				strSQL.append(" AND  ((COM_FECHAPROBABLEPAGO >= TO_DATE(?,'DD/MM/YYYY') " );
				strSQL.append(" AND COM_FECHAPROBABLEPAGO <= TO_DATE(?,'DD/MM/YYYY')) OR (COM_FECHAPROBABLEPAGO IS NULL)) ");
			}
		}//if( "V".equals(estatusCarga) || "B".equals(estatusCarga) )
		
		if( "N".equals(estatusCarga) ){
		
			//strSQL.append("UNION ALL " );
			strSQL.append("SELECT   2 orden, " );
			strSQL.append("		 'P' estatus, " );
			strSQL.append("		 TO_CHAR (pf.com_fechaprobablepago, 'DD/MM/YYYY') fecha_venc, " );
			strSQL.append("         TO_CHAR (MAX (pf.df_registro_fide),'DD/MM/YYYY hh:mi:ss') ultimo_reg, " );
			strSQL.append("         COUNT (pf.com_fechaprobablepago) total_reg, " );
			strSQL.append("         SUM (pf.fg_totalvencimiento) monto_tot,  " );
			strSQL.append("         0  folio, " );
			strSQL.append("         'N' estatus_proc, " );
			strSQL.append("         null tipo_reg, " );
			strSQL.append("         null usuario                  " );
			strSQL.append("    FROM com_pagos_fide pf " );
			strSQL.append("   WHERE pf.cg_estatus IN ('P', 'NP') ");
			strSQL.append("	 	AND pf.ic_programa_fondojr = ? " );
			if(fec_venc_ini!=null && !fec_venc_ini.equals("") && fec_venc_fin!=null && !fec_venc_fin.equals("")){
				strSQL.append(" AND PF.COM_FECHAPROBABLEPAGO >= TO_DATE(?,'DD/MM/YYYY') " );
				strSQL.append(" AND PF.COM_FECHAPROBABLEPAGO <= TO_DATE(?,'DD/MM/YYYY') ");
			}
			strSQL.append("GROUP BY pf.com_fechaprobablepago, pf.ig_folio " );
			strSQL.append("UNION ALL " );
			strSQL.append("SELECT   3 orden, " );
			strSQL.append("		 pf.cg_estatus estatus, " );
			strSQL.append("		 TO_CHAR (SYSDATE, 'DD/MM/YYYY') fecha_venc, " );
			strSQL.append("         TO_CHAR (MAX (pf.df_registro_fide), 'DD/MM/YYYY HH:MI:SS') ultimo_reg, " );
			strSQL.append("         COUNT (pf.cg_estatus) total_reg,  " );
			strSQL.append("         SUM (pf.fg_totalvencimiento) monto_tot, " );
			strSQL.append("         0 folio, " );
			strSQL.append("         decode(pf.cg_estatus_proc,'B',pf.cg_estatus_proc,'N') estatus_proc, " );
			strSQL.append("         null tipo_reg, " );
			strSQL.append("         null usuario " );
			strSQL.append("    FROM com_pagos_fide pf " );
			strSQL.append("   WHERE pf.cg_estatus IN ('R', 'T') " );
			strSQL.append("		AND pf.ic_programa_fondojr = ? ");
			strSQL.append("     AND 'RC' != " );
			strSQL.append("            fn_es_recuperacion_fondojr (pf.com_fechaprobablepago, " );
			strSQL.append("                                        pf.ig_prestamo, " );
			strSQL.append("                                        pf.ig_disposicion, " );
			strSQL.append("                                        pf.fg_totalvencimiento " );
			strSQL.append("                                       ) " );
			strSQL.append("GROUP BY pf.cg_estatus, decode(pf.cg_estatus_proc,'B',pf.cg_estatus_proc,'N')  " );
			strSQL.append("UNION ALL " );
			strSQL.append("SELECT   4 orden, " );
			strSQL.append("		 DECODE(cp.cg_estatus ,'R','RC') estatus, " );
			strSQL.append("		 TO_CHAR (SYSDATE, 'dd/mm/yyyy') fecha_venc, " );
			strSQL.append("         TO_CHAR (MAX (cp.df_registro_fide),'dd/mm/yyyy hh:mi:ss') ultimo_reg, " );
			strSQL.append("         COUNT (cp.cg_estatus) total_reg, " );
			strSQL.append("         SUM (cp.fg_totalvencimiento) monto_tot, " );
			strSQL.append("         0 folio,           " );
			strSQL.append("         cp.cg_estatus_proc estatus_proc, " );
			strSQL.append("         null tipo_reg, " );
			strSQL.append("         null usuario " );
			strSQL.append("    FROM com_pagos_fide cp " );
			strSQL.append("   WHERE cp.cg_estatus = 'R' " );
			strSQL.append("		AND cp.ic_programa_fondojr = ? ");
			strSQL.append("     AND 'RC' = " );
			strSQL.append("            fn_es_recuperacion_fondojr (cp.com_fechaprobablepago, " );
			strSQL.append("                                        cp.ig_prestamo, " );
			strSQL.append("                                        cp.ig_disposicion, " );
			strSQL.append("                                        cp.fg_totalvencimiento " );
			strSQL.append("                                       ) " );
			strSQL.append("GROUP BY cp.cg_estatus, cp.cg_estatus_proc " );
		}//if( "N".equals(estatusCarga) )
		
		if( "NPR".equals(estatusCarga) ) {//R:NP Rechazados -- F027-2011 FVR
			strSQL.append("SELECT   5 orden, " );
			strSQL.append("		 'NPR' estatus, " );
			strSQL.append("		 TO_CHAR (sysdate, 'DD/MM/YYYY') fecha_venc, " );
			strSQL.append("         TO_CHAR (MAX (pf.df_registro_fide),'DD/MM/YYYY hh:mi:ss') ultimo_reg, " );
			strSQL.append("         COUNT (pf.com_fechaprobablepago) total_reg, " );
			strSQL.append("         SUM (pf.fg_totalvencimiento) monto_tot,  " );
			strSQL.append("         0  folio, " );
			strSQL.append("         'NPR' estatus_proc, " );
			strSQL.append("         null tipo_reg, " );
			strSQL.append("         null usuario                  " );
			strSQL.append("    FROM com_cred_np_rechazados pf " );
			strSQL.append("   WHERE pf.cg_estatus IN ('NPR') ");
			strSQL.append("	 	AND pf.ic_programa_fondojr = ? " );
			if(!"S".equals(chkDesembolso))
				strSQL.append("	 	AND pf.CG_DESEMBOLSO = ? " );
			strSQL.append("GROUP BY pf.cg_estatus " );
		}//if( "R".equals(estatusCarga) )
		
		strSQL.append(") tmonitor " );		
		
		//FODEA 026-2010 Rebos 
		if(estatusCarga!=null && !estatusCarga.equals("")){
			if("N".equals(estatusCarga)){
				strSQL.append("where estatus_proc in (?,?,?) " );  		
			}else{
				strSQL.append("where estatus_proc = ?" );  		
			}
		}
		
		strSQL.append("order by tmonitor.orden, tmonitor.fecha_venc ");		
		
		if( "V".equals(estatusCarga) || "B".equals(estatusCarga) ){		
			conditions.add(new Long(cvePrograma));
			if(fec_venc_ini!=null && !fec_venc_ini.equals("") && fec_venc_fin!=null && !fec_venc_fin.equals("")){
				conditions.add(fec_venc_ini);
				conditions.add(fec_venc_fin);
			}
		}//if( "V".equals(estatusCarga) || "B".equals(estatusCarga) )
		
		if( "N".equals(estatusCarga) ){
			conditions.add(new Long(cvePrograma));
			if(fec_venc_ini!=null && !fec_venc_ini.equals("") && fec_venc_fin!=null && !fec_venc_fin.equals("")){
				conditions.add(fec_venc_ini);
				conditions.add(fec_venc_fin);
			}
			
			conditions.add(new Long(cvePrograma));
			conditions.add(new Long(cvePrograma));
		}//if( "N".equals(estatusCarga) )
		
		if( "NPR".equals(estatusCarga) ) {//R:NP Rechazados -- F027-2011 FVR
			conditions.add(new Long(cvePrograma));
			if(!"S".equals(chkDesembolso))
				conditions.add("N");
		}
		//FODEA 026-2010 Rebos 
		if(estatusCarga!=null && !estatusCarga.equals("")){
			if("N".equals(estatusCarga)){
				conditions.add(estatusCarga);
				conditions.add("R");
				conditions.add("P");
			}else{
				conditions.add(estatusCarga);
			}
			
		}
		
		log.debug("..:: strSQL: " + strSQL.toString());
		log.debug("..:: conditions: " + conditions);
		
		log.info("getDocumentQuery(S) ::..");
		return strSQL.toString();
	}//getDocumentQuery
	
	/**
	* Obtiene el query necesario para mostrar la información completa de 
	* una página a partir de las llaves primarias enviadas como parámetro
	* @return Cadena con la consulta de SQL, para obtener la información 
	* completa de los registros con las llaves especificadas
	*/
	public String getDocumentSummaryQueryForIds(List pageIds){
		log.info("getDocumentSummaryQueryForIds(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		
		strSQL.append("SELECT  tmonitor.orden as morden, " );
		strSQL.append("		  tmonitor.estatus as mestatus, " );
		strSQL.append("		  tmonitor.fecha_venc as mfecha_venc, " );
		strSQL.append("        tmonitor.ultimo_reg as multimo_reg, " );
		strSQL.append("        tmonitor.total_reg as mtotal_reg, " );
		strSQL.append("        tmonitor.monto_tot as mmonto_tot, " );
		strSQL.append("        tmonitor.folio as mfolio, " );
		strSQL.append("        tmonitor.estatus_proc as mestatus_proc, " );
		strSQL.append("        tmonitor.tipo_reg as mtipo_reg, " );
		strSQL.append("        tmonitor.usuario as musuario " );
		strSQL.append("FROM ( " );
		
		if( "V".equals(estatusCarga) || "B".equals(estatusCarga) ){
			strSQL.append(" 			SELECT 1 orden, " );
			strSQL.append(" 			DECODE(cg_tipo_registro,'Reembolsos al dia','BR', " );
			strSQL.append("    									'Pagados y No Pagados','BP', " );
			strSQL.append("    									'Recuperaciones al dia','BRC', " );
			strSQL.append("    									'Prepagos al dia','BT')estatus, " );
			strSQL.append("        DECODE(com_fechaprobablepago,null, " );
			strSQL.append("               		TO_CHAR (DF_FECHA_BIT, 'DD/MM/YYYY'), " );
			strSQL.append("               		TO_CHAR (com_fechaprobablepago, 'DD/MM/YYYY')) fecha_venc, ");
			strSQL.append("         TO_CHAR (df_registro_fide, 'DD/MM/YYYY HH:MI:SS') ultimo_reg, " );
			strSQL.append("         ig_total_reg total_reg, " );
			strSQL.append("         fg_monto_total monto_tot, " );
			strSQL.append("         nvl(ig_folio,0) folio,  " );
			strSQL.append("         cg_estatus_carga estatus_proc, " );
			strSQL.append("         cg_tipo_registro tipo_reg,          " );
			strSQL.append("         cg_usuario usuario  " );
			strSQL.append("    FROM bit_monitor_fondojr " );
			strSQL.append("		WHERE ic_programa_fondojr = ? ");
			if(fec_venc_ini!=null && !fec_venc_ini.equals("") && fec_venc_fin!=null && !fec_venc_fin.equals("")){
				strSQL.append(" AND  ((COM_FECHAPROBABLEPAGO >= TO_DATE(?,'DD/MM/YYYY') " );
				strSQL.append(" AND COM_FECHAPROBABLEPAGO <= TO_DATE(?,'DD/MM/YYYY')) OR (COM_FECHAPROBABLEPAGO IS NULL)) ");
			}
		}//if( "V".equals(estatusCarga) || "B".equals(estatusCarga) ){
		
		if( "N".equals(estatusCarga) ){
			//strSQL.append("UNION ALL " );
			strSQL.append("SELECT   2 orden, " );
			strSQL.append("		 'P' estatus, " );
			strSQL.append("		 TO_CHAR (pf.com_fechaprobablepago, 'DD/MM/YYYY') fecha_venc, " );
			strSQL.append("         TO_CHAR (MAX (pf.df_registro_fide),'DD/MM/YYYY hh:mi:ss') ultimo_reg, " );
			strSQL.append("         COUNT (pf.com_fechaprobablepago) total_reg, " );
			strSQL.append("         SUM (pf.fg_totalvencimiento) monto_tot,  " );
			strSQL.append("         0  folio, " );
			strSQL.append("         'N' estatus_proc, " );
			strSQL.append("         null tipo_reg, " );
			strSQL.append("         null usuario                  " );
			strSQL.append("    FROM com_pagos_fide pf " );
			strSQL.append("   WHERE pf.cg_estatus IN ('P', 'NP') ");
			strSQL.append("	 	AND pf.ic_programa_fondojr = ? " );
			if(fec_venc_ini!=null && !fec_venc_ini.equals("") && fec_venc_fin!=null && !fec_venc_fin.equals("")){
				strSQL.append(" AND PF.COM_FECHAPROBABLEPAGO >= TO_DATE(?,'DD/MM/YYYY') " );
				strSQL.append(" AND PF.COM_FECHAPROBABLEPAGO <= TO_DATE(?,'DD/MM/YYYY') ");
			}
			strSQL.append("GROUP BY pf.com_fechaprobablepago, pf.ig_folio " );
			strSQL.append("UNION ALL " );
			strSQL.append("SELECT   3 orden, " );
			strSQL.append("		 pf.cg_estatus estatus, " );
			strSQL.append("		 TO_CHAR (SYSDATE, 'DD/MM/YYYY') fecha_venc, " );
			strSQL.append("         TO_CHAR (MAX (pf.df_registro_fide), 'DD/MM/YYYY HH:MI:SS') ultimo_reg, " );
			strSQL.append("         COUNT (pf.cg_estatus) total_reg,  " );
			strSQL.append("         SUM (pf.fg_totalvencimiento) monto_tot, " );
			strSQL.append("         0 folio, " );
			strSQL.append("         decode(pf.cg_estatus_proc,'B',pf.cg_estatus_proc,'N') estatus_proc, " );
			strSQL.append("         null tipo_reg, " );
			strSQL.append("         null usuario " );
			strSQL.append("    FROM com_pagos_fide pf " );
			strSQL.append("   WHERE pf.cg_estatus IN ('R', 'T') " );
			strSQL.append("		AND pf.ic_programa_fondojr = ? ");
			strSQL.append("     AND 'RC' != " );
			strSQL.append("            fn_es_recuperacion_fondojr (pf.com_fechaprobablepago, " );
			strSQL.append("                                        pf.ig_prestamo, " );
			strSQL.append("                                        pf.ig_disposicion, " );
			strSQL.append("                                        pf.fg_totalvencimiento " );
			strSQL.append("                                       ) " );
			strSQL.append("GROUP BY pf.cg_estatus, decode(pf.cg_estatus_proc,'B',pf.cg_estatus_proc,'N')  " );
			strSQL.append("UNION ALL " );
			strSQL.append("SELECT   4 orden, " );
			strSQL.append("		 DECODE(cp.cg_estatus ,'R','RC') estatus, " );
			strSQL.append("		 TO_CHAR (SYSDATE, 'dd/mm/yyyy') fecha_venc, " );
			strSQL.append("         TO_CHAR (MAX (cp.df_registro_fide),'dd/mm/yyyy hh:mi:ss') ultimo_reg, " );
			strSQL.append("         COUNT (cp.cg_estatus) total_reg, " );
			strSQL.append("         SUM (cp.fg_totalvencimiento) monto_tot, " );
			strSQL.append("         0 folio,           " );
			strSQL.append("         cp.cg_estatus_proc estatus_proc, " );
			strSQL.append("         null tipo_reg, " );
			strSQL.append("         null usuario " );
			strSQL.append("    FROM com_pagos_fide cp " );
			strSQL.append("   WHERE cp.cg_estatus = 'R' " );
			strSQL.append("		AND cp.ic_programa_fondojr = ? ");
			strSQL.append("     AND 'RC' = " );
			strSQL.append("            fn_es_recuperacion_fondojr (cp.com_fechaprobablepago, " );
			strSQL.append("                                        cp.ig_prestamo, " );
			strSQL.append("                                        cp.ig_disposicion, " );
			strSQL.append("                                        cp.fg_totalvencimiento " );
			strSQL.append("                                       ) " );
			strSQL.append("GROUP BY cp.cg_estatus, cp.cg_estatus_proc " );	
		}
		
		if( "NPR".equals(estatusCarga) ){
			strSQL.append("SELECT   5 orden, " );
			strSQL.append("		 'NPR' estatus, " );
			strSQL.append("		 TO_CHAR (sysdate, 'DD/MM/YYYY') fecha_venc, " );
			strSQL.append("         TO_CHAR (MAX (pf.df_registro_fide),'DD/MM/YYYY hh:mi:ss') ultimo_reg, " );
			strSQL.append("         COUNT (pf.com_fechaprobablepago) total_reg, " );
			strSQL.append("         SUM (pf.fg_totalvencimiento) monto_tot,  " );
			strSQL.append("         0  folio, " );
			strSQL.append("         'NPR' estatus_proc, " );
			strSQL.append("         null tipo_reg, " );
			strSQL.append("         null usuario                  " );
			strSQL.append("    FROM com_cred_np_rechazados pf " );
			strSQL.append("   WHERE pf.cg_estatus IN ('NPR') ");
			strSQL.append("	 	AND pf.ic_programa_fondojr = ? " );
			if(!"S".equals(chkDesembolso))
				strSQL.append("	 	AND pf.CG_DESEMBOLSO = ? " );
			strSQL.append("GROUP BY pf.cg_estatus " );
		}
		
		strSQL.append(") tmonitor " );
		
		
		if( "V".equals(estatusCarga) || "B".equals(estatusCarga) ){
			conditions.add(new Long(cvePrograma));
			if(fec_venc_ini!=null && !fec_venc_ini.equals("") && fec_venc_fin!=null && !fec_venc_fin.equals("")){
				conditions.add(fec_venc_ini);
				conditions.add(fec_venc_fin);
			}
		}
		
		if( "N".equals(estatusCarga) ){
			conditions.add(new Long(cvePrograma));
			if(fec_venc_ini!=null && !fec_venc_ini.equals("") && fec_venc_fin!=null && !fec_venc_fin.equals("")){
				conditions.add(fec_venc_ini);
				conditions.add(fec_venc_fin);
			}
			conditions.add(new Long(cvePrograma));
			conditions.add(new Long(cvePrograma));
		}
		
		if( "NPR".equals(estatusCarga) ){
			conditions.add(new Long(cvePrograma));
			if(!"S".equals(chkDesembolso))
				conditions.add("N");
		}
		
		strSQL.append(" WHERE (");
		
		for(int i = 0; i < pageIds.size(); i++){
      List lItem = (ArrayList)pageIds.get(i);
			
      if(i > 0){strSQL.append("  OR  ");}
      
			strSQL.append(" (tmonitor.orden = ? and tmonitor.estatus = ? and tmonitor.fecha_venc = ? and tmonitor.folio = ? ) ");
		
			conditions.add(new Long(lItem.get(0).toString()));
			conditions.add(lItem.get(1).toString());
			conditions.add(lItem.get(2).toString());
			conditions.add(new Long(lItem.get(3).toString()));
		}
    strSQL.append(" ) ");

		//FODEA 026-2010 Rebos 
		if(estatusCarga!=null && !estatusCarga.equals("")){
			if("N".equals(estatusCarga)){
				strSQL.append("and estatus_proc in (?,?,?) " );   			
				conditions.add(estatusCarga);
				conditions.add("R");
				conditions.add("P");
			}else{
				strSQL.append("and estatus_proc = ?" );   
				conditions.add(estatusCarga);
			}
			
		}

		strSQL.append("order by tmonitor.orden, tmonitor.fecha_venc ");
		
		log.info("strSQL ::.." + strSQL);
		
		log.debug("..:: strSQL: " + strSQL.toString());
		log.debug("..:: conditions: " + conditions);
		
		log.info("getDocumentSummaryQueryForIds(S)");
		return strSQL.toString();
	}//getDocumentSummaryQueryForIds	
	
	public String getDocumentQueryFile(){
		log.info("getDocumentQueryFile(E)");
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		
		strSQL = new StringBuffer();
		conditions = new ArrayList();			
		
		strSQL.append("SELECT   " );
		strSQL.append("		 DECODE(tmonitor.orden,'1',tmonitor.tipo_reg,'2',tmonitor.fecha_venc,tmonitor.estatus) AS Fecha_de_Vencimiento_Nafin, " );
		strSQL.append("        tmonitor.ultimo_reg as Fecha_Ultimo_Reg_Env_por_FIDE, " );
		strSQL.append("        tmonitor.folio as Folio_de_Operacion, " );
		strSQL.append("        tmonitor.total_reg as Total_de_Registros, " );
		strSQL.append("        tmonitor.monto_tot as Monto " );
		strSQL.append("FROM ( " );
		
		if( "V".equals(estatusCarga) || "B".equals(estatusCarga) ){
		
			strSQL.append(" 			SELECT 1 orden, " );
			strSQL.append(" 			DECODE(cg_tipo_registro,'Reembolsos al dia','BR', " );
			strSQL.append("    									'Pagados y No Pagados','BP', " );
			strSQL.append("    									'Recuperaciones al dia','BRC', " );
			strSQL.append("    									'Prepagos al dia','BT')estatus, " );
			strSQL.append("        DECODE(com_fechaprobablepago,null, " );
			strSQL.append("               		TO_CHAR (DF_FECHA_BIT, 'DD/MM/YYYY'), " );
			strSQL.append("               		TO_CHAR (com_fechaprobablepago, 'DD/MM/YYYY')) fecha_venc, ");
			strSQL.append("         TO_CHAR (df_registro_fide, 'DD/MM/YYYY HH:MI:SS') ultimo_reg, " );
			strSQL.append("         ig_total_reg total_reg, " );
			strSQL.append("         fg_monto_total monto_tot, " );
			strSQL.append("         nvl(ig_folio,0) folio,  " );
			strSQL.append("         cg_estatus_carga estatus_proc, " );
			strSQL.append("         cg_tipo_registro tipo_reg,          " );
			strSQL.append("         cg_usuario usuario  " );
			strSQL.append("    FROM bit_monitor_fondojr " );
			strSQL.append("		WHERE ic_programa_fondojr = ? ");		
			if(fec_venc_ini!=null && !fec_venc_ini.equals("") && fec_venc_fin!=null && !fec_venc_fin.equals("")){
				strSQL.append(" AND  ((COM_FECHAPROBABLEPAGO >= TO_DATE(?,'DD/MM/YYYY') " );
				strSQL.append(" AND COM_FECHAPROBABLEPAGO <= TO_DATE(?,'DD/MM/YYYY')) OR (COM_FECHAPROBABLEPAGO IS NULL)) ");
			}
		}//if( "V".equals(estatusCarga) || "B".equals(estatusCarga) )
		
		if( "N".equals(estatusCarga) ){
		
			//strSQL.append("UNION ALL " );
			strSQL.append("SELECT   2 orden, " );
			strSQL.append("		 'P' estatus, " );
			strSQL.append("		 TO_CHAR (pf.com_fechaprobablepago, 'DD/MM/YYYY') fecha_venc, " );
			strSQL.append("         TO_CHAR (MAX (pf.df_registro_fide),'DD/MM/YYYY hh:mi:ss') ultimo_reg, " );
			strSQL.append("         COUNT (pf.com_fechaprobablepago) total_reg, " );
			strSQL.append("         SUM (pf.fg_totalvencimiento) monto_tot,  " );
			strSQL.append("         0  folio, " );
			strSQL.append("         pf.cg_estatus_proc estatus_proc, " );
			strSQL.append("         null tipo_reg, " );
			strSQL.append("         null usuario                  " );
			strSQL.append("    FROM com_pagos_fide pf " );
			strSQL.append("   WHERE pf.cg_estatus IN ('P', 'NP') ");
			strSQL.append("	 	AND pf.ic_programa_fondojr = ? " );
			if(fec_venc_ini!=null && !fec_venc_ini.equals("") && fec_venc_fin!=null && !fec_venc_fin.equals("")){
				strSQL.append(" AND PF.COM_FECHAPROBABLEPAGO >= TO_DATE(?,'DD/MM/YYYY') " );
				strSQL.append(" AND PF.COM_FECHAPROBABLEPAGO <= TO_DATE(?,'DD/MM/YYYY') ");
			}
			strSQL.append("GROUP BY pf.com_fechaprobablepago, pf.ig_folio, pf.cg_estatus_proc " );
			strSQL.append("UNION ALL " );
			strSQL.append("SELECT   3 orden, " );
			strSQL.append("		 decode(pf.cg_estatus,'R', 'Reembolsos al dia', 'T','Prepagos al dia' ) as estatus, " );
			strSQL.append("		 TO_CHAR (SYSDATE, 'DD/MM/YYYY') fecha_venc, " );
			strSQL.append("         TO_CHAR (MAX (pf.df_registro_fide), 'DD/MM/YYYY HH:MI:SS') ultimo_reg, " );
			strSQL.append("         COUNT (pf.cg_estatus) total_reg,  " );
			strSQL.append("         SUM (pf.fg_totalvencimiento) monto_tot, " );
			strSQL.append("         0 folio, " );
			strSQL.append("         pf.cg_estatus_proc estatus_proc, " );
			strSQL.append("         null tipo_reg, " );
			strSQL.append("         null usuario " );
			strSQL.append("    FROM com_pagos_fide pf " );
			strSQL.append("   WHERE pf.cg_estatus IN ('R', 'T') " );
			strSQL.append("		AND pf.ic_programa_fondojr = ? ");
			strSQL.append("     AND 'RC' != " );
			strSQL.append("            fn_es_recuperacion_fondojr (pf.com_fechaprobablepago, " );
			strSQL.append("                                        pf.ig_prestamo, " );
			strSQL.append("                                        pf.ig_disposicion, " );
			strSQL.append("                                        pf.fg_totalvencimiento " );
			strSQL.append("                                       ) " );
			strSQL.append("GROUP BY decode(pf.cg_estatus,'R', 'Reembolsos al dia', 'T','Prepagos al dia' ), pf.cg_estatus_proc  " );
			strSQL.append("UNION ALL " );
			strSQL.append("SELECT   4 orden, " );
			strSQL.append("		 DECODE(DECODE(cp.cg_estatus ,'R','RC'),'RC','Recuperaciones al dia') estatus, " );
			strSQL.append("		 TO_CHAR (SYSDATE, 'dd/mm/yyyy') fecha_venc, " );
			strSQL.append("         TO_CHAR (MAX (cp.df_registro_fide),'dd/mm/yyyy hh:mi:ss') ultimo_reg, " );
			strSQL.append("         COUNT (cp.cg_estatus) total_reg, " );
			strSQL.append("         SUM (cp.fg_totalvencimiento) monto_tot, " );
			strSQL.append("         0 folio,           " );
			strSQL.append("         cp.cg_estatus_proc estatus_proc, " );
			strSQL.append("         null tipo_reg, " );
			strSQL.append("         null usuario " );
			strSQL.append("    FROM com_pagos_fide cp " );
			strSQL.append("   WHERE cp.cg_estatus = 'R' " );
			strSQL.append("		AND cp.ic_programa_fondojr = ? ");
			strSQL.append("     AND 'RC' = " );
			strSQL.append("            fn_es_recuperacion_fondojr (cp.com_fechaprobablepago, " );
			strSQL.append("                                        cp.ig_prestamo, " );
			strSQL.append("                                        cp.ig_disposicion, " );
			strSQL.append("                                        cp.fg_totalvencimiento " );
			strSQL.append("                                       ) " );
			strSQL.append("GROUP BY DECODE(DECODE(cp.cg_estatus ,'R','RC'),'RC','Recuperaciones al dia'), cp.cg_estatus_proc " );
		}//if( "N".equals(estatusCarga) )
		
		if( "NPR".equals(estatusCarga) ) {//R:NP Rechazados -- F027-2011 FVR
			strSQL.append("SELECT   5 orden, " );
			strSQL.append("		 'NP Rechazados al dia' estatus, " );
			strSQL.append("		 TO_CHAR (sysdate, 'DD/MM/YYYY') fecha_venc, " );
			strSQL.append("         TO_CHAR (MAX (pf.df_registro_fide),'DD/MM/YYYY hh:mi:ss') ultimo_reg, " );
			strSQL.append("         COUNT (pf.com_fechaprobablepago) total_reg, " );
			strSQL.append("         SUM (pf.fg_totalvencimiento) monto_tot,  " );
			strSQL.append("         0  folio, " );
			strSQL.append("         'NPR' estatus_proc, " );
			strSQL.append("         null tipo_reg, " );
			strSQL.append("         null usuario                  " );
			strSQL.append("    FROM com_cred_np_rechazados pf " );
			strSQL.append("   WHERE pf.cg_estatus IN ('NPR') ");
			strSQL.append("	 	AND pf.ic_programa_fondojr = ? " );
			strSQL.append("GROUP BY pf.cg_estatus " );
		}//if( "R".equals(estatusCarga) )
		
		strSQL.append(") tmonitor " );		
		
		//FODEA 026-2010 Rebos 
		if(estatusCarga!=null && !estatusCarga.equals("")){
		strSQL.append("where estatus_proc = ?" );  		
		}
		
		strSQL.append("order by DECODE(tmonitor.estatus,'BRC',tmonitor.tipo_reg, DECODE(tmonitor.estatus,'BT',tmonitor.tipo_reg,tmonitor.fecha_venc)) ");		
		
		if( "V".equals(estatusCarga) || "B".equals(estatusCarga) ){		
			conditions.add(new Long(cvePrograma));
			if(fec_venc_ini!=null && !fec_venc_ini.equals("") && fec_venc_fin!=null && !fec_venc_fin.equals("")){
				conditions.add(fec_venc_ini);
				conditions.add(fec_venc_fin);
			}
		}//if( "V".equals(estatusCarga) || "B".equals(estatusCarga) )
		
		if( "N".equals(estatusCarga) ){
			conditions.add(new Long(cvePrograma));
			if(fec_venc_ini!=null && !fec_venc_ini.equals("") && fec_venc_fin!=null && !fec_venc_fin.equals("")){
				conditions.add(fec_venc_ini);
				conditions.add(fec_venc_fin);
			}
			
			conditions.add(new Long(cvePrograma));
			conditions.add(new Long(cvePrograma));
		}//if( "N".equals(estatusCarga) )
		
		if( "NPR".equals(estatusCarga) ) {
			conditions.add(new Long(cvePrograma));
		}
		
		if(estatusCarga!=null && !estatusCarga.equals("")){
			conditions.add(estatusCarga);
		}
		
		log.debug("..:: strSQL: " + strSQL.toString());
		log.debug("..:: conditions: " + conditions);
		
		
		log.info("getDocumentQueryFile(S)");
		return strSQL.toString();
	}//getDocumentQueryFile
	
	
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		

		try {
		
		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		ComunesPDF pdfDoc = new ComunesPDF();
	
			
		try {
		
			if(tipo.equals("CSV") ) {
		
				contenidoArchivo.append("Programa:,"+descPrograma+"\n");
				contenidoArchivo.append("Fecha de Vencimiento Nafin,");
				contenidoArchivo.append("Fecha y Hora del Ultimo Registro Enviado por FIDE,");
				contenidoArchivo.append("Folio de Operacion,");
				contenidoArchivo.append("Total de Registros,");
				contenidoArchivo.append("Monto");
				contenidoArchivo.append("\n");
				while(rs.next()){ 
					String fecha_Ven = (rs.getString("MFECHA_VENC") == null) ? "" : rs.getString("MFECHA_VENC");
					String multimoReg = (rs.getString("MULTIMO_REG") == null) ? "" : rs.getString("MULTIMO_REG");
					String mFolio = (rs.getString("MFOLIO") == null) ? "" : rs.getString("MFOLIO");
					String total_reg = (rs.getString("MTOTAL_REG") == null) ? "" : rs.getString("MTOTAL_REG");
					String montoTotal = (rs.getString("MMONTO_TOT") == null) ? "" : rs.getString("MMONTO_TOT");
				
					contenidoArchivo.append(fecha_Ven +",");
					contenidoArchivo.append(multimoReg +",");
					contenidoArchivo.append(mFolio +",");
					contenidoArchivo.append(total_reg +",");
					contenidoArchivo.append(montoTotal +",");
					contenidoArchivo.append("\n");
				}
				creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
				nombreArchivo = creaArchivo.getNombre();
			}
		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
	 
	//GETTERS
  /**
	 * Obtiene la lista de parámetros a usar como variables bind en las condiciones de la consulta
	 * @return Lista con los parametros de las condiciones
	 */
	public List getConditions(){return conditions;}
  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() {return paginaNo;}
	
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset(){return paginaOffset;}
	
	//Parametros del formulario
	public String getStrDirectorioPublicacion() {return strDirectorioPublicacion;}
	public String getCvePrograma() {return cvePrograma;}
	public String getFec_venc_ini() {return fec_venc_ini;}
	public String getFec_venc_fin() {return fec_venc_fin;}
	public String getEstatusCarga() {return estatusCarga;}
	
	
	//SETTERS
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo){paginaNo = newPaginaNo;}
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset){this.paginaOffset=paginaOffset;}
		
	//Parametros del formulario
	public void setStrDirectorioPublicacion(String strDirectorioPublicacion) {this.strDirectorioPublicacion = strDirectorioPublicacion;}
	public void setCvePrograma(String cvePrograma) {this.cvePrograma = cvePrograma;}
	public void setFec_venc_ini(String fec_venc_ini) {this.fec_venc_ini = fec_venc_ini;}
	public void setFec_venc_fin(String fec_venc_fin) {this.fec_venc_fin = fec_venc_fin;}
	public void setEstatusCarga(String estatusCarga) {this.estatusCarga = estatusCarga;}


	public void setChkDesembolso(String chkDesembolso) {
		this.chkDesembolso = chkDesembolso;
	}


	public String getChkDesembolso() {
		return chkDesembolso;
	}

	public String getDescPrograma() {
		return descPrograma;
	}

	public void setDescPrograma(String descPrograma) {
		this.descPrograma = descPrograma;
	}
	
}