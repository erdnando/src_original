package com.netro.fondosjr;

import com.netro.exception.NafinException;
import com.netro.pdf.ComunesPDF;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.VectorTokenizer;

import org.apache.commons.logging.Log;

/******************************************************************************************
 * Fodea			04-2015																							*
 * Descripci�n: Migraci�n de la pantalla Administraci�n-Dispersion-Resumen IFs				*
 *					a ExtJS. Realiza la consulta para llenar el grid y generar los archivos.	*
 * Elabor�:		Agust�n Bautista Ruiz																		*
 * Fecha:		12/08/2014																						*
 ******************************************************************************************/
public class ReembolsoPorFiniquito implements IQueryGeneratorRegExtJS{

	private static final Log log = ServiceLocator.getInstance().getLog(ReembolsoPorFiniquito.class);	
	private List	conditions;
	private String cvePrograma;
	private int		procesoCarga;
	private int		cantidadDesembolsos;
	private String strCantidadDesembolsos;
	private String totalDesembolsos;
	private String strTotalDesembolsos;
	private String strAviso;
	private String strPrograma;
	private String tipoArchivo;
	private String numReembolsos;
	private String acuseCarga;
	private String folioCarga;
	private String fechaCarga;
	private String horaCarga;
	private String nombreUsuario;
	
	public ReembolsoPorFiniquito() {}
	public String getDocumentQuery(){ return null; }
	public String getAggregateCalculationQuery(){ return null; }
	public String getDocumentSummaryQueryForIds(List ids){ return null; }
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){ return null; }	
	
	/**
	 * La consulta no requiere paginaci�n, y es unicamente para generar el pdf, por el momento
	 * @return qrySentencia
	 */
	public String getDocumentQueryFile(){ 
			
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();
		log.info("getDocumentQueryFile (E)");	
		try{
			if(this.tipoArchivo.equals("ACUSE")){
				qrySentencia.append(
					" SELECT TO_CHAR(cpf.df_registro_fide, 'dd/mm/yyyy') fecha_movimiento, " +
					" cpf.ig_prestamo numero_prestamo, " 												 +
					" cpf.ig_cliente num_cliente_sirac, " 												 +
					" cpf.cg_nombrecliente nombre_cliente, " 											 +
					" cpf.fg_totalvencimiento monto_reembolso, " 									 +
					" cop.cg_descripcion origen, " 														 +
					" crf.cg_observaciones observaciones, " 											 +
					" cpf.ig_disposicion disposicion " 													 +
					" FROM com_pagos_fide_val cpf, " 													 +
					" comcat_origen_programa cop, " 														 +
					" com_tmp_reemb_x_finiquito crf " 													 +
					" WHERE cpf.ig_origen = cop.ig_origen_programa " 								 +
					" AND cpf.ig_prestamo = crf.ig_prestamo " 										 +
					" AND cpf.ig_proc_desembolso = crf.ic_reemb_x_finiquito " 					 +
					" AND crf.ic_reemb_x_finiquito = ? " 												 +
					" AND cpf.cg_estatus = 'RF' " 														 +
					" AND cpf.ic_programa_fondojr = ? " 												 +
					//" and cpf.ig_disposicion  in ( "+disposiciones +") " +//Fodea 027-2011
					" ORDER BY numero_prestamo, disposicion "
				);
				conditions.add(	new Integer(this.procesoCarga));
				conditions.add(	new String(this.cvePrograma)	);
		
			} else if(this.tipoArchivo.equals("PREACUSE")){
				qrySentencia.append(
						"SELECT   TO_CHAR (cpf.df_registro_fide, 'dd/mm/yyyy') fecha_movimiento, "			+
						"         cpf.ig_prestamo numero_prestamo, cpf.ig_cliente num_cliente_sirac, "	+
						"         cpf.cg_nombrecliente nombre_cliente, " 											+		
						"         cpf.fg_totalvencimiento monto_reembolso, cop.cg_descripcion origen, "	+
						"         crf.cg_observaciones observaciones, cpf.ig_disposicion disposicion "	+
						"    FROM com_pagos_fide_val cpf, "																+
						"         comcat_origen_programa cop, "														+
						"         com_tmp_reemb_x_finiquito crf " 													+
						"   WHERE cpf.ig_origen = cop.ig_origen_programa " 										+
						"     AND cpf.ig_prestamo = crf.ig_prestamo " 												+
						"     AND crf.ic_reemb_x_finiquito = ? " 														+
						"     AND cpf.cg_estatus = 'NP' " 																+
						"     AND cpf.cs_validacion = 'A' " 															+
						"     AND cpf.ic_programa_fondojr = ? " 														+
						"     AND NOT EXISTS ( " 																			+
						"            SELECT cpfi.ig_disposicion, cpfi.ig_prestamo " 							+
						"              FROM com_pagos_fide_val cpfi, " 												+
						"                   comcat_origen_programa copi, " 										+
						"                   com_tmp_reemb_x_finiquito crfi " 										+
						"             WHERE cpfi.ig_origen = copi.ig_origen_programa " 						+
						"               AND cpfi.ig_prestamo = crfi.ig_prestamo " 								+
						"               AND crfi.ic_reemb_x_finiquito = ? " 										+
						"               AND cpfi.ic_programa_fondojr = ? " 										+
						"               AND cg_estatus <> 'NP' " 														+
						"               AND cpf.cs_validacion <> 'R' " 												+
						"               AND cpfi.ig_prestamo = cpf.ig_prestamo " 								+
						"               AND cpfi.ig_disposicion = cpf.ig_disposicion) " 						+
						"ORDER BY numero_prestamo, disposicion"
				);
					
				conditions.add(	new Integer(this.procesoCarga));
				conditions.add(	new String(this.cvePrograma)	);
				conditions.add(	new Integer(this.procesoCarga));
				conditions.add(	new String(this.cvePrograma)	);			
			}
			
			log.info("Sentencia: "   + qrySentencia.toString());		
			log.info("Condiciones: " + conditions.toString());	
			
		}catch(Exception e){
			log.warn("getDocumentQueryFileException "+e);
		}
		log.info("getDocumentQueryFile (S)");
		
		return qrySentencia.toString(); 
	}	

	/**
	 * Genera el archivo PDF
	 * @return nombre del archivo
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 * @return 
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){ 
	
		log.info("crearCustomFile (E)");
		
		String nombreArchivo = "";
		String fecha			= "";
		String origen		 	= "";		
		String observacion 	= "";
		String cliente		 	= "";
		String deesembolso 	= "";
		int prestamo		 	= 0;
		int clienteSirac 	 	= 0;
		int amortizacion	 	= 0;
		HttpSession session  = request.getSession();
		
		if(tipo.equals("PDF")){
			
			ComunesPDF pdfDoc    			= new ComunesPDF();
			StringBuffer contenidoArchivo = new StringBuffer();
			CreaArchivo creaArchivo 		= new CreaArchivo();
			OutputStreamWriter writer		= null;
			BufferedWriter buffer 			= null;
	
			try {
				Comunes comunes		= new Comunes();
				nombreArchivo 			= Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc 					= new ComunesPDF(2, path + nombreArchivo);
			
				String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    	= fechaActual.substring(0,2);
				String mesActual    	= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   	= fechaActual.substring(6,10);
				String horaActual  	= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText( "	","formas",ComunesPDF.RIGHT);
				
				pdfDoc.addText( "Programa: " + this.strPrograma, 	  "formas", ComunesPDF.LEFT);
				
				if(this.tipoArchivo.equals("PREACUSE")){
				
					pdfDoc.addText( "Preacuse Desembolsos", "titulo", ComunesPDF.CENTER);
					pdfDoc.setTable(2,50);
					pdfDoc.setCell(this.getStrCantidadDesembolsos(),	"formas", ComunesPDF.LEFT			);
					pdfDoc.setCell(""+this.cantidadDesembolsos,			"formas", ComunesPDF.RIGHT			);
					pdfDoc.setCell(this.getStrTotalDesembolsos(),		"formas", ComunesPDF.LEFT			);
					pdfDoc.setCell(this.totalDesembolsos,					"formas", ComunesPDF.RIGHT			);
					pdfDoc.setCell(this.getStrAviso(),						"formas", ComunesPDF.LEFT, 2, 1	);
					pdfDoc.addTable();				
					
				} else if(this.tipoArchivo.equals("ACUSE")){
				
					pdfDoc.addText("\n\n", "formas", ComunesPDF.CENTER);
					pdfDoc.addText("La autentificaci�n se llev� a cabo con �xito \n Recibo: " + this.acuseCarga, "titulo", ComunesPDF.CENTER);				
					pdfDoc.setTable(2,50);
					pdfDoc.setCell("No. Total de Amortizaciones Desembolsadas",			"formas", ComunesPDF.RIGHT		);
					pdfDoc.setCell(""+this.cantidadDesembolsos,								"formas", ComunesPDF.LEFT		);
					pdfDoc.setCell("Monto Total de Amortizaciones Desembolsadas",		"formas", ComunesPDF.RIGHT		);
					pdfDoc.setCell("$" + this.totalDesembolsos,								"formas", ComunesPDF.LEFT		);
					pdfDoc.setCell("N�mero de Acuse",											"formas", ComunesPDF.RIGHT		);
					pdfDoc.setCell(this.folioCarga,												"formas", ComunesPDF.LEFT		);					
					pdfDoc.setCell("Fecha de Carga",												"formas", ComunesPDF.RIGHT		);
					pdfDoc.setCell(this.fechaCarga,												"formas", ComunesPDF.LEFT		);		
					pdfDoc.setCell("Hora de Carga",												"formas", ComunesPDF.RIGHT		);
					pdfDoc.setCell(this.horaCarga,												"formas", ComunesPDF.LEFT		);		
					pdfDoc.setCell("Usuario",														"formas", ComunesPDF.RIGHT		);
					pdfDoc.setCell(this.nombreUsuario,											"formas", ComunesPDF.LEFT		);					
					pdfDoc.setCell(this.getStrAviso(),											"formas", ComunesPDF.CENTER, 2);
					pdfDoc.addTable();				
				
				}

				pdfDoc.addText("\n\n","formas",ComunesPDF.CENTER);
				
				pdfDoc.setTable(8,80);
				pdfDoc.setCell("Fecha de Movimiento",		"celda01", ComunesPDF.CENTER		);
				pdfDoc.setCell("Pr�stamo",						"celda01", ComunesPDF.CENTER		);
				pdfDoc.setCell("N�mero de Cliente SIRAC",	"celda01", ComunesPDF.CENTER		);
				pdfDoc.setCell("Cliente",						"celda01", ComunesPDF.CENTER		);
				pdfDoc.setCell("Monto del Desembolso",		"celda01", ComunesPDF.CENTER		);
				pdfDoc.setCell("No. Amortizaci�n",			"celda01", ComunesPDF.CENTER		);
				pdfDoc.setCell("Origen",						"celda01", ComunesPDF.CENTER		);
				pdfDoc.setCell("Observaciones",				"celda01", ComunesPDF.CENTER		);
				
				while (rs.next())	{		
					fecha				= (rs.getString("fecha_movimiento")	== null) ? ""	: rs.getString("fecha_movimiento");
					origen			= (rs.getString("origen") 				== null) ? ""	: rs.getString("origen");
					observacion		= (rs.getString("observaciones")		== null) ? ""	: rs.getString("observaciones");
					cliente			= (rs.getString("nombre_cliente")	== null) ? ""	: rs.getString("nombre_cliente");
					deesembolso		= (rs.getString("monto_reembolso")	== null) ? ""	: rs.getString("monto_reembolso");
					prestamo			= (rs.getInt("numero_prestamo")		== 0	 ) ? 0	: rs.getInt("numero_prestamo");
					clienteSirac	= (rs.getInt("num_cliente_sirac")	== 0	 ) ? 0	: rs.getInt("num_cliente_sirac");
					amortizacion	= (rs.getInt("disposicion")			== 0	 ) ? 0	: rs.getInt("disposicion");
					
					pdfDoc.setCell(fecha,												"formas",	ComunesPDF.CENTER);			
					pdfDoc.setCell(""+prestamo,										"formas",	ComunesPDF.CENTER);
					pdfDoc.setCell(""+clienteSirac,									"formas",	ComunesPDF.CENTER);
					pdfDoc.setCell(cliente,												"formas",	ComunesPDF.LEFT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(deesembolso,2),"formas",	ComunesPDF.RIGHT);
					pdfDoc.setCell(""+amortizacion,									"formas",	ComunesPDF.CENTER);
					pdfDoc.setCell(origen,												"formas",	ComunesPDF.CENTER);
					pdfDoc.setCell(observacion,										"formas",	ComunesPDF.LEFT);
					
				}
				pdfDoc.setCell(this.getStrTotalDesembolsos1(),    "formas", ComunesPDF.RIGHT, 4, 1);	
				pdfDoc.setCell(this.totalDesembolsos,             "formas", ComunesPDF.LEFT,  4, 1);
				pdfDoc.setCell(this.getStrCantidadDesembolsos1(), "formas", ComunesPDF.RIGHT, 4, 1);
				pdfDoc.setCell(""+this.cantidadDesembolsos,       "formas", ComunesPDF.LEFT,  4, 1);
					
				pdfDoc.addTable();	
					
				pdfDoc.endDocument();	
				
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo ", e);
			} finally { try { /*rs.close();*/ } catch(Exception e) {} }		
		
		} else if(tipo.equals("CSV")){
			try{
				OutputStreamWriter writer = null;
				BufferedWriter buffer = null;
				StringBuffer contenidoArchivo = new StringBuffer();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				int total = 0;
				
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
				buffer = new BufferedWriter(writer);		
				
				contenidoArchivo = new StringBuffer();	
				contenidoArchivo.append("\nLa autentificaci�n se llev� a cabo con �xito \n Recibo: "+this.acuseCarga+"\n,\n");
				contenidoArchivo.append("Programa:,");
				contenidoArchivo.append(this.strPrograma + "\n");
				contenidoArchivo.append("No. Total de Amortizaciones Desembolsadas,");
				contenidoArchivo.append(this.cantidadDesembolsos + "\n");
				contenidoArchivo.append("Monto Total de Amortizaciones Desembolsadas,");
				contenidoArchivo.append("$" + this.totalDesembolsos + "\n");
				contenidoArchivo.append("N�mero de Acuse,");
				contenidoArchivo.append(this.folioCarga + "\n");
				contenidoArchivo.append("Fecha de Carga,");
				contenidoArchivo.append(this.fechaCarga + "\n");
				contenidoArchivo.append("Hora de Carga,");
				contenidoArchivo.append(this.horaCarga + "\n");
				contenidoArchivo.append("Usuario,");
				contenidoArchivo.append(this.nombreUsuario + "\n");
				contenidoArchivo.append(this.getStrAviso().replace(',',' ')+"\n,\n");				

				contenidoArchivo.append("Fecha de Movimiento,");
				contenidoArchivo.append("Prestamo,");
				contenidoArchivo.append("N�mero de Cliente SIRAC,");
				contenidoArchivo.append("Cliente,");
				contenidoArchivo.append("Monto del Desembolso,");
				contenidoArchivo.append("No. Amortizacion,");
				contenidoArchivo.append("Origen,");
				contenidoArchivo.append("Observaciones\n");

				while (rs.next()){
					fecha				= (rs.getString("fecha_movimiento")	== null) ? ""	: rs.getString("fecha_movimiento");
					origen			= (rs.getString("origen") 				== null) ? ""	: rs.getString("origen");
					observacion		= (rs.getString("observaciones")		== null) ? ""	: rs.getString("observaciones");
					cliente			= (rs.getString("nombre_cliente")	== null) ? ""	: rs.getString("nombre_cliente");
					deesembolso		= (rs.getString("monto_reembolso")	== null) ? ""	: rs.getString("monto_reembolso");
					prestamo			= (rs.getInt("numero_prestamo")		== 0	 ) ? 0	: rs.getInt("numero_prestamo");
					clienteSirac	= (rs.getInt("num_cliente_sirac")	== 0	 ) ? 0	: rs.getInt("num_cliente_sirac");
					amortizacion	= (rs.getInt("disposicion")			== 0	 ) ? 0	: rs.getInt("disposicion");
					
					contenidoArchivo.append(fecha + ",");
					contenidoArchivo.append(prestamo+ ",");
					contenidoArchivo.append(clienteSirac + ",");
					contenidoArchivo.append(cliente.replace(',',' ') + ",");
					contenidoArchivo.append("$" + deesembolso + ",");
					contenidoArchivo.append(amortizacion + ",");
					contenidoArchivo.append(origen + ",");
					contenidoArchivo.append(observacion.replace(',',' ') + "\n");

				}
		  
          contenidoArchivo.append("Total Monto por Desembolsos,");
          contenidoArchivo.append("$" + this.totalDesembolsos + "\n");
          contenidoArchivo.append("Total Desembolsos" + ",");
          contenidoArchivo.append(this.cantidadDesembolsos + "\n");		  
			if(total==1000){					
				total=0;	
				buffer.write(contenidoArchivo.toString());
				contenidoArchivo = new StringBuffer();//Limpio  
			}			  
			buffer.write(contenidoArchivo.toString());
			buffer.close();	
			contenidoArchivo = new StringBuffer();//Limpio   
			
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo ", e);
			} finally { try { /*rs.close();*/ } catch(Exception e) {} }	
		
		
		}

		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}
			
	/**
	 * Este m�todo es por si el anterior es ocupado por otro proceso.
	 * En vez de recibir un objeto InputStream, recibe uno tipo FileReader, que es el que utilizo
	 * en el archivo 33reembolsosPorFiniquito01ext.data.jsp
	 * @throws com.netro.exception.NafinException
	 * @return 
	 * @param cvePrograma
	 * @param nombreUsr
	 * @param cgUsuario
	 * @param nombreArchivo
	 * @param inputStream
	 */
	public String cargaTmpDesembolsosBatch(FileReader lineas, String nombreArchivo, String cgUsuario, String nombreUsr, String cvePrograma) throws NafinException{
		log.info("cargaTmpDesembolsosBatch (E)");
		AccesoDB con			= new AccesoDB();
		PreparedStatement ps	= null;
		ResultSet rs			= null;
		boolean commit			= true;
		String query			= "";
		
		VectorTokenizer vtd	= null;
		Vector vecdet			= null;
		String linea			= "";
		String numProcResumen= "";
		String folioBatch		= "";
		
		String numeroProceso			= "";
		String ig_prestamo			= "";
		String ig_cliente				= "";
		String fg_totalvencimiento	= "";
		String observaciones			= "";
		int contReg						= 0;

		try{
			con.conexionDB();
			BufferedReader breader = new BufferedReader(lineas);
		
			query = "SELECT SEQ_COMTMP_DESEMBOLSOS_BATCH.NEXTVAL numProc FROM DUAL";
			ps = con.queryPrecompilado(query);
			rs = ps.executeQuery();
			if(rs!=null && rs.next()){
				numeroProceso = rs.getString("numProc");
			}
			rs.close();
			ps.close();
			
			query =	"INSERT INTO comtmp_desembolsos_batch " +
						" (ic_desembolsos_batch, ig_linea, ig_prestamo, ig_cliente, fn_monto_reembolso, cg_observaciones) " +
						" VALUES (?, ?, ?, ?, ?, ?) ";
			String linea1 = "";
			while ((linea1=breader.readLine())!=null){
				if(linea1!=null && !"".equals(linea1)){
					contReg ++;
					vtd		= new VectorTokenizer(linea1,"|");
					vecdet	= vtd.getValuesVector();
					
					ig_prestamo				=(vecdet.size()>=1)?vecdet.get(0).toString().trim():"";
					ig_cliente				=(vecdet.size()>=2)?vecdet.get(1).toString().trim().toUpperCase():"";
					fg_totalvencimiento	=(vecdet.size()>=3)?vecdet.get(2).toString().trim().toUpperCase():"";
					observaciones			=(vecdet.size()>=4)?vecdet.get(3).toString().trim().toUpperCase():"";
					
					ig_prestamo				=Comunes.quitaComitasSimplesyDobles(ig_prestamo);
					ig_cliente				=Comunes.quitaComitasSimplesyDobles(ig_cliente);
					fg_totalvencimiento	=Comunes.quitaComitasSimplesyDobles(fg_totalvencimiento);
					observaciones			=Comunes.quitaComitasSimplesyDobles(observaciones);
					
					ig_prestamo				=((ig_prestamo.length()>12)?ig_prestamo.substring(0,13):ig_prestamo);
					ig_cliente				=((ig_cliente.length()>12)?ig_cliente.substring(0,13):ig_cliente);
					fg_totalvencimiento	=((fg_totalvencimiento.length()>20)?fg_totalvencimiento.substring(0,21):fg_totalvencimiento);
					observaciones			=((observaciones.length()>200)?observaciones.substring(0,201):observaciones);
					
					ps = con.queryPrecompilado(query);
					ps.setLong(	 1, Long.parseLong(numeroProceso));
					ps.setInt(	 2, contReg);
					ps.setString(3, ig_prestamo);
					ps.setString(4, ig_cliente);
					ps.setString(5, fg_totalvencimiento);
					ps.setString(6, observaciones);
					
					ps.executeUpdate();
					ps.close();
				}
				
			}
			//inputStream.close();
			
			if(contReg>0){
				
				query = "SELECT seq_com_resumen_desem_batch.NEXTVAL numProc FROM DUAL";
				ps = con.queryPrecompilado(query);
				rs = ps.executeQuery();
				if(rs!=null && rs.next()){
					numProcResumen = rs.getString("numProc");
				}
				rs.close();
				ps.close();
				
				//Se genera folio de la carga batch
				java.util.Date date = new java.util.Date();
				String fechaFolio = (new SimpleDateFormat ("ddMMyyyy")).format(date);
				folioBatch = fechaFolio+numProcResumen;
				
				//Se inserta resumen de la carga batch antes de que sea procesada
				query = "INSERT INTO com_resumen_desembolsos_batch " +
					"            (ic_resumen_desem_batch, ic_desembolsos_batch, cg_folio_batch, " +
					"             cg_nombre_archivo, ig_reg_total, ig_reg_pend, cg_usuario, " +
					"             cg_nombre_usr, ic_programa_fondojr) " +
					"     VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?) ";
					
				ps = con.queryPrecompilado(query);
				ps.setLong(		1, Long.parseLong(numProcResumen));
				ps.setLong(		2, Long.parseLong(numeroProceso));
				ps.setString(	3, folioBatch);
				ps.setString(	4, nombreArchivo);
				ps.setInt(		5, contReg);
				ps.setInt(		6, contReg);
				ps.setString(	7, cgUsuario);
				ps.setString(	8, nombreUsr);
				ps.setLong(		9, Long.parseLong(cvePrograma));
				
				ps.executeUpdate();
				ps.close();
			}
			
			return folioBatch;
		}catch(Throwable e){
			log.warn("cargaTmpDesembolsosBatch (Error)");
			commit = false;
			throw new AppException("Error al realizar carga temporal de desembolsos batch",e);
		}finally {
			if(con.hayConexionAbierta()){
				con.terminaTransaccion(commit);
				con.cierraConexionDB();
			}
			log.info("cargaTmpDesembolsosBatch (S)");
		}
   
	}	
	
/************************************************************
 *								GETTERS ANS SETTERS						*
 ************************************************************/	
	public List getConditions(){
		return conditions; 
	}	

	public int getCantidadDesembolsos() {
		return cantidadDesembolsos;
	}

	public void setCantidadDesembolsos(int cantidadDesembolsos) {
		this.cantidadDesembolsos = cantidadDesembolsos;
	}

	public String getTotalDesembolsos() {
		return totalDesembolsos;
	}

	public void setTotalDesembolsos(String totalDesembolsos) {
		this.totalDesembolsos = totalDesembolsos;
	}

	public int getProcesoCarga() {
		return procesoCarga;
	}

	public void setProcesoCarga(int procesoCarga) {
		this.procesoCarga = procesoCarga;
	}

	public String getCvePrograma() {
		return cvePrograma;
	}

	public void setCvePrograma(String cvePrograma) {
		this.cvePrograma = cvePrograma;
	}

	public String getStrTotalDesembolsos() {
		return "Monto Total de Amortizaciones a Desembolsar";
	}

	public String getStrTotalDesembolsos1() {
		return "Total Monto por Desembolsos";
	}

	public String getStrCantidadDesembolsos() {
		return "No. Total de Amortizaciones a Desembolsar";
	}

	public String getStrCantidadDesembolsos1() {
		return "Total Desembolsos";
	}

	public String getStrAviso() {
		return "Al transmitirse este MENSAJE DE DATOS, "+
		"usted esta bajo su responsabilidad haciendo un "+
		"movimiento contable al fondo, aceptando de esta "+
		"forma la responsabilidad de los movimientos registrados "+
		"del mismo, dicha transmisi�n tendr� validez para todos "+
		"los efectos legales.";
	}

	public String getStrPrograma() {
		return strPrograma;
	}

	public void setStrPrograma(String strPrograma) {
		this.strPrograma = strPrograma;
	}

	public String getTipoArchivo() {
		return tipoArchivo;
	}

	public void setTipoArchivo(String tipoArchivo) {
		this.tipoArchivo = tipoArchivo;
	}

	public String getNumReembolsos() {
		return numReembolsos;
	}

	public void setNumReembolsos(String numReembolsos) {
		this.numReembolsos = numReembolsos;
	}

	public String getAcuseCarga() {
		return acuseCarga;
	}

	public void setAcuseCarga(String acuseCarga) {
		this.acuseCarga = acuseCarga;
	}

	public String getFolioCarga() {
		return folioCarga;
	}

	public void setFolioCarga(String folioCarga) {
		this.folioCarga = folioCarga;
	}

	public String getFechaCarga() {
		return fechaCarga;
	}

	public void setFechaCarga(String fechaCarga) {
		this.fechaCarga = fechaCarga;
	}

	public String getHoraCarga() {
		return horaCarga;
	}

	public void setHoraCarga(String horaCarga) {
		this.horaCarga = horaCarga;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
		
}