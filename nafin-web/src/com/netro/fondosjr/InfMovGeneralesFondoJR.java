package com.netro.fondosjr;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;


public class InfMovGeneralesFondoJR implements IQueryGeneratorReg, IQueryGeneratorRegExtJS  {
	public InfMovGeneralesFondoJR()
	{
	}
	
	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer 		qrySentencia;
	private List 		conditions;  
	private String 	paginaOffset;
	private String 	paginaNo;
	String[]		montos			= new String[2];
	String[]		doctos			= new String[2];
	String[]		numeros;
	
	private String fechaValor;
	private String importe;
	private String referencia;
	private String observaciones;
	private String operacion;
	public String directorio;
	public String proceso;



	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */

	public String getAggregateCalculationQuery() {
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
	  
		qrySentencia.append("SELECT count(1) total, 'ConsultaGeneral::getAggregateCalculationQuery' origen " +
									" FROM  COMTMP_MOV_GRAL_FONDO_JR  mov, COMCAT_OPER_FONDO_JR cat"); 
									
	
						
		qrySentencia.append(" WHERE  mov.IG_OPER_FONDO_JR =  cat.IG_OPER_FONDO_JR and  IG_NO_PROCESO = ? ") ;
	
		conditions.add(proceso);
		/*
			if((!"".equals(operacion) && operacion!=null))
			{
				qrySentencia.append(" AND  mov.IG_OPER_FONDO_JR = ? ") ;
				conditions.add(operacion);
			}
			
		
		//importe
	
		if((!"".equals(importe) && importe!=null))
		{
			qrySentencia.append(" AND mov.FG_CAPITAL = ? ") ;
			conditions.add(importe);
		}
										
	
		//referencia
		if((!"".equals(referencia) && referencia!=null))
		{
			//qrySentencia.append(" AND mov.CG_REFERENCIA = ? ") ;
			qrySentencia.append("	AND mov.CG_REFERENCIA LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') ");
			conditions.add((referencia));
		}
		
		//observaciones
		if((!"".equals(observaciones) && observaciones!=null))
		{
			//qrySentencia.append(" AND mov.CG_OBSERVACIONES = ? ") ;
			qrySentencia.append("	AND mov.CG_OBSERVACIONES LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') ");
		
			conditions.add((observaciones));
		}
		
			//fecha
			if((!"".equals(fechaValor) && fechaValor!=null))
			{
				//qrySentencia.append(" AND mov.DF_FEC_VALOR = ? ") ;
				qrySentencia.append(" AND mov.DF_FEC_VALOR = TO_DATE (?, 'dd/mm/yyyy') ");
				conditions.add(fechaValor);
			}
	*/
			System.out.println("Paginador :: TipoOperacion::"+ operacion+ "importe::"+ importe+ "referencia::"+referencia);		
			System.out.println("paginador :: observaciones :: " + observaciones + "  fecha valor :: " + fechaValor);
				
			System.out.println("getAggregateCalculationQuery "+conditions.toString());
			System.out.println("getAggregateCalculationQuery "+qrySentencia.toString());
	
		return qrySentencia.toString();
		
	}
	
	
	 /**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery(){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
	
		
		qrySentencia.append(" SELECT mov.IG_MOV_GRAL as MOVIMIENTO, mov.IG_FOLIO AS FOLIO, TO_CHAR(mov.DF_FEC_MOVIMIENTO, 'DD/MM/YYYY') as FECHA_MOVIMIENTO, "+
	                    " TO_CHAR(mov.DF_FEC_VALOR, 'DD/MM/YYYY') AS FECHA_VALOR, mov.FG_CAPITAL AS IMPORTE, "+
							  " mov.CG_REFERENCIA AS REFERENCIA, cat.cg_operacion AS OPERACION, mov.CG_OBSERVACIONES AS OBSERVACIONES , IG_NO_PROCESO as PROCESO ");
							
	
		
	     qrySentencia.append(" FROM  COMTMP_MOV_GRAL_FONDO_JR  mov, COMCAT_OPER_FONDO_JR cat ");

		qrySentencia.append(" WHERE  mov.IG_OPER_FONDO_JR =  cat.IG_OPER_FONDO_JR and  IG_NO_PROCESO = ? ") ;
	
		conditions.add(proceso);
		/*
		
		if((!"".equals(operacion) && operacion!=null))
			{
				qrySentencia.append(" AND  mov.IG_OPER_FONDO_JR = ? ") ;
				conditions.add(operacion);
			}
			
		//importe
	
		if((!"".equals(importe) && importe!=null))
		{
			qrySentencia.append(" AND mov.FG_CAPITAL = ? ") ;
			conditions.add(importe);
		}
										
	
		//referencia
		if((!"".equals(referencia) && referencia!=null))
		{
			//qrySentencia.append(" AND mov.CG_REFERENCIA = ? ") ;
			qrySentencia.append("	AND mov.CG_REFERENCIA LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') ");
		
			conditions.add((referencia));
		}
		
		//observaciones
		if((!"".equals(observaciones) && observaciones!=null))
		{
			//qrySentencia.append(" AND mov.CG_OBSERVACIONES = ? ") ;
			qrySentencia.append("	AND mov.CG_OBSERVACIONES LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') ");
		
			conditions.add((observaciones));
		}
		
			//fecha
			if((!"".equals(fechaValor) && fechaValor!=null))
			{
				//qrySentencia.append(" AND mov.DF_FEC_VALOR = ? ") ;
				qrySentencia.append(" AND mov.DF_FEC_VALOR = TO_DATE (?, 'dd/mm/yyyy') ");
				conditions.add(fechaValor);
			}
	*/
			System.out.println("Paginador :: TipoOperacion::"+ operacion+ "importe::"+ importe+ "referencia::"+referencia);		
			System.out.println("paginador :: observaciones :: " + observaciones + "  fecha valor :: " + fechaValor);
				
			System.out.println("getDocumentQuery "+conditions.toString());
			System.out.println("getDocumentQuery "+qrySentencia.toString());
	

			return qrySentencia.toString();
	}

	/**
	 * Obtiene el query necesario para mostrar la información completa de 
	 * una página a partir de las llaves primarias enviadas como parámetro
	 * @return Cadena con la consulta de SQL, para obtener la información
	 * 	completa de los registros con las llaves especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
			
	
		
		qrySentencia.append(" SELECT mov.IG_MOV_GRAL as MOVIMIENTO, mov.IG_FOLIO AS FOLIO, TO_CHAR(mov.DF_FEC_MOVIMIENTO, 'DD/MM/YYYY') as FECHA_MOVIMIENTO, "+
	                    " TO_CHAR(mov.DF_FEC_VALOR, 'DD/MM/YYYY') AS FECHA_VALOR, mov.FG_CAPITAL AS IMPORTE, "+
							   " mov.CG_REFERENCIA AS REFERENCIA, cat.cg_operacion AS OPERACION, mov.CG_OBSERVACIONES AS OBSERVACIONES,  IG_NO_PROCESO as PROCESO");
							
	
		
		qrySentencia.append(" FROM  COMTMP_MOV_GRAL_FONDO_JR  mov, COMCAT_OPER_FONDO_JR cat ");

		qrySentencia.append(" WHERE  mov.IG_OPER_FONDO_JR =  cat.IG_OPER_FONDO_JR and  IG_NO_PROCESO = ? ") ;
	
		conditions.add(proceso);
		/*
		
			if((!"".equals(operacion) && operacion!=null))
			{
				qrySentencia.append(" AND mov.IG_OPER_FONDO_JR = ? ") ;
				conditions.add(operacion);
			}
	
		//importe
	
		if((!"".equals(importe) && importe!=null))
		{
			qrySentencia.append(" AND mov.FG_CAPITAL = ? ") ;
			conditions.add(importe);
		}
										
	
		//referencia
		if((!"".equals(referencia) && referencia!=null))
		{
			//qrySentencia.append(" AND mov.CG_REFERENCIA = ? ") ;
			qrySentencia.append("	AND mov.CG_REFERENCIA LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') ");
		
			conditions.add((referencia));
		}
		
		//observaciones
		if((!"".equals(observaciones) && observaciones!=null))
		{
			//qrySentencia.append(" AND mov.CG_OBSERVACIONES = ? ") ;
			qrySentencia.append("	AND mov.CG_OBSERVACIONES LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') ");
		
			conditions.add((observaciones));
		}
		
			//fecha
			if((!"".equals(fechaValor) && fechaValor!=null))
			{
				//qrySentencia.append(" AND mov.DF_FEC_VALOR = ? ") ;
				qrySentencia.append(" AND mov.DF_FEC_VALOR = TO_DATE (?, 'dd/mm/yyyy') ");
				conditions.add(fechaValor);
			}
	 	*/
		
			System.out.println("Paginador :: TipoOperacion::"+ operacion+ "importe::"+ importe+ "referencia::"+referencia);		
			System.out.println("paginador :: observaciones :: " + observaciones + "  fecha valor :: " + fechaValor);
				
			System.out.println("getDocumentSummaryQueryForIds "+conditions.toString());
			System.out.println("getDocumentSummaryQueryForIds "+qrySentencia.toString());
	
		qrySentencia.append(" ORDER BY mov.IG_MOV_GRAL ") ;
		
		return qrySentencia.toString();
	}
	

	public String getDocumentQueryFile(){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();

		qrySentencia.append(" SELECT mov.IG_MOV_GRAL as MOVIMIENTO, mov.IG_FOLIO AS FOLIO, TO_CHAR(mov.DF_FEC_MOVIMIENTO, 'DD/MM/YYYY') as FECHA_MOVIMIENTO, "+
	                    " TO_CHAR(mov.DF_FEC_VALOR, 'DD/MM/YYYY') AS FECHA_VALOR, mov.FG_CAPITAL AS IMPORTE, "+
							  " mov.CG_REFERENCIA AS REFERENCIA, cat.cg_operacion AS OPERACION, mov.CG_OBSERVACIONES AS OBSERVACIONES, IG_NO_PROCESO as PROCESO");
							
		qrySentencia.append(" FROM  COMTMP_MOV_GRAL_FONDO_JR  mov, COMCAT_OPER_FONDO_JR cat ");
		
		qrySentencia.append(" WHERE  mov.IG_OPER_FONDO_JR =  cat.IG_OPER_FONDO_JR and  IG_NO_PROCESO = ? ") ;
	
		conditions.add(proceso);
		/*
		
		if((!"".equals(operacion) && operacion!=null))
		{
			qrySentencia.append(" AND  mov.IG_OPER_FONDO_JR = ? ") ;
			conditions.add(operacion);
		}
		
		//importe
	
		if((!"".equals(importe) && importe!=null))
		{
			qrySentencia.append(" AND mov.FG_CAPITAL = ? ") ;
			conditions.add(importe);
		}
	
		//referencia
		if((!"".equals(referencia) && referencia!=null))
		{
			//qrySentencia.append(" AND mov.CG_REFERENCIA = ? ") ;
			qrySentencia.append("	AND mov.CG_REFERENCIA LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') ");
		
			conditions.add((referencia));
		}
		
		//observaciones
		if((!"".equals(observaciones) && observaciones!=null))
		{
			//qrySentencia.append(" AND mov.CG_OBSERVACIONES = ? ") ;
			qrySentencia.append("	AND mov.CG_OBSERVACIONES LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') ");
		
			conditions.add((observaciones));
		}
		
		//fecha
			if((!"".equals(fechaValor) && fechaValor!=null))
			{
				//qrySentencia.append(" AND mov.DF_FEC_VALOR = ? ") ;
				qrySentencia.append(" AND mov.DF_FEC_VALOR = TO_DATE (?, 'dd/mm/yyyy') ");
				conditions.add(fechaValor);
			}
	*/
			System.out.println("Paginador :: TipoOperacion::"+ operacion+ "importe::"+ importe+ "referencia::"+referencia);		
			System.out.println("paginador :: observaciones :: " + observaciones + "  fecha valor :: " + fechaValor);
				
			System.out.println("getAggregateCalculationQuery "+conditions.toString());
			System.out.println("getAggregateCalculationQuery "+qrySentencia.toString());
		
		return qrySentencia.toString();   
	}//getDocumentQueryFile
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rg, String path, String tipo) {
		String nombreArchivo = "";
		String linea = "";
		
		return nombreArchivo;
	}
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String nombreArchivo = "";
		String linea = "";
		
		return nombreArchivo;
	}


/*****************************************************
	 GETTERS
*******************************************************/
 
	/**
	  Obtiene la lista de parámetros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	

	
	//GET
public String getFechaValor()		 {return fechaValor;}
public String getImporte()			 {return importe;}
public String getReferencia()		 {return referencia;}
public String getObservaciones()	 {return observaciones;}
public String getOperacion()		 {return operacion;}
	
	
	 	
	
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

   
// SET	
public void setFechaValor(String fechaValor)		 { this.fechaValor = fechaValor; }
public void setImporte(String importe)		          { this.importe = importe;  }
public void setReferencia(String referencia)		    { this.referencia = referencia; }
public void setObservaciones(String observaciones)	 { this.observaciones = observaciones; }
public void setOperacion(String operacion)		    { this.operacion = operacion; }

	public String getDirectorio() {
		return directorio;
	}

	public void setDirectorio(String directorio) {
		this.directorio = directorio;
	}

	public String getProceso() {
		return proceso;
	}

	public void setProceso(String proceso) {
		this.proceso = proceso;
	}




	
}