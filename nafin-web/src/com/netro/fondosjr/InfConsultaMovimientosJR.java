package com.netro.fondosjr;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class InfConsultaMovimientosJR implements IQueryGeneratorReg, IQueryGeneratorRegExtJS {
	public InfConsultaMovimientosJR()
	{
	}
//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(InfConsultaMovimientosJR.class);
		
	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer 		qrySentencia;
	private List 		conditions;
	private String 	paginaOffset;
	private String 	paginaNo;
	String[]		montos			= new String[2];
	String[]		doctos			= new String[2];
	String[]		numeros;
	
	private String fechaValor;
	private String operacion;
  public String clavePrograma;


			 
	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */

	public String getAggregateCalculationQuery() {
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
	  
		qrySentencia.append("SELECT count(1) total, 'ConsultaGeneral::getAggregateCalculationQuery' origen " +
								  " FROM  COM_MOV_GRAL_FONDO_JR  mov, COMCAT_OPER_FONDO_JR cat"); 
			
		
			qrySentencia.append(" WHERE  mov.IG_OPER_FONDO_JR =  cat.IG_OPER_FONDO_JR ") ;
			
		
		
			if((!"".equals(operacion) && operacion!=null))
			{
				qrySentencia.append(" and  mov.IG_OPER_FONDO_JR =  ? ") ;
				conditions.add(operacion);
			}
	
	
			//fecha
			if((!"".equals(fechaValor) && fechaValor!=null))
			{
				//qrySentencia.append(" AND mov.DF_FEC_VALOR = ? ") ;				
				qrySentencia.append(" AND mov.DF_FEC_VALOR = TO_DATE (?, 'dd/mm/yyyy') ");
				conditions.add(fechaValor);
			}
			
      if(!"".equals(clavePrograma) && clavePrograma != null ) 	{					
				qrySentencia.append(" AND mov.IC_PROGRAMA_FONDOJR = ? ");
				conditions.add(clavePrograma);
			}
      
      qrySentencia.append(" ORDER BY mov.IG_MOV_GRAL ") ;
      
			log.debug("paginador :: fecha :: " + fechaValor);
			log.debug("Paginador :: TipoOperacion::"+ operacion);
			
			log.debug("getAggregateCalculationQuery "+conditions.toString());
			log.debug("getAggregateCalculationQuery "+qrySentencia.toString());
			return qrySentencia.toString();
			
			
			
	}
	
	
	 /**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery(){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
	
		
		qrySentencia.append(" SELECT mov.IG_MOV_GRAL as MOVIMIENTO, mov.IG_FOLIO AS FOLIO, TO_CHAR(mov.DF_FEC_MOVIMIENTO, 'DD/MM/YYYY') as FECHA_MOVIMIENTO, "+
	                    " TO_CHAR(mov.DF_FEC_VALOR, 'DD/MM/YYYY') AS FECHA_VALOR, mov.FG_CAPITAL AS IMPORTE, "+
							  " mov.CG_REFERENCIA AS REFERENCIA, cat.cg_operacion AS OPERACION, mov.CG_OBSERVACIONES AS OBSERVACIONES");
		
		
		qrySentencia.append(" FROM  COM_MOV_GRAL_FONDO_JR  mov, COMCAT_OPER_FONDO_JR cat ");

		
		
		qrySentencia.append(" WHERE  mov.IG_OPER_FONDO_JR =  cat.IG_OPER_FONDO_JR ") ;
			
		
		if((!"".equals(operacion) && operacion!=null))
			{
				qrySentencia.append(" AND   mov.IG_OPER_FONDO_JR =  ? ") ;
				conditions.add(operacion);
			}
		
		
		//fecha
			if((!"".equals(fechaValor) && fechaValor!=null))
			{
				//qrySentencia.append(" AND mov.DF_FEC_VALOR = ? ") ;
				qrySentencia.append(" AND mov.DF_FEC_VALOR = TO_DATE (?, 'dd/mm/yyyy') ");
					
				conditions.add(fechaValor);
			}
      
        if(!"".equals(clavePrograma)  && clavePrograma != null ) 	{						
				qrySentencia.append(" AND mov.IC_PROGRAMA_FONDOJR = ? ");
				conditions.add(clavePrograma);
			}
      
			
      qrySentencia.append(" ORDER BY mov.IG_MOV_GRAL ") ;
      
			log.debug("paginador :: fecha :: " + fechaValor);
			log.debug("Paginador :: TipoOperacion::"+ operacion);
			
			log.debug("getDocumentQuery "+conditions.toString());
			log.debug("getDocumentQuery "+qrySentencia.toString());
			return qrySentencia.toString();
	}

	/**
	 * Obtiene el query necesario para mostrar la información completa de 
	 * una página a partir de las llaves primarias enviadas como parámetro
	 * @return Cadena con la consulta de SQL, para obtener la información
	 * 	completa de los registros con las llaves especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
			
	
		
		qrySentencia.append(" SELECT mov.IG_MOV_GRAL as MOVIMIENTO, mov.IG_FOLIO AS FOLIO, TO_CHAR(mov.DF_FEC_MOVIMIENTO, 'DD/MM/YYYY') as FECHA_MOVIMIENTO, "+
	                    " TO_CHAR(mov.DF_FEC_VALOR, 'DD/MM/YYYY') AS FECHA_VALOR, mov.FG_CAPITAL AS IMPORTE, "+
		  					  " mov.CG_REFERENCIA AS REFERENCIA, cat.cg_operacion AS OPERACION, mov.CG_OBSERVACIONES AS OBSERVACIONES");
		
	
		
		qrySentencia.append(" FROM  COM_MOV_GRAL_FONDO_JR  mov, COMCAT_OPER_FONDO_JR cat ");

		   qrySentencia.append(" WHERE  mov.IG_OPER_FONDO_JR =  cat.IG_OPER_FONDO_JR ") ;
		
		
		
			if((!"".equals(operacion) && operacion!=null))
			{
					qrySentencia.append(" AND  mov.IG_OPER_FONDO_JR =  ? ") ;
					
				conditions.add(operacion);
			}
	
		//fecha
			if((!"".equals(fechaValor) && fechaValor!=null))
			{
				//qrySentencia.append(" AND mov.DF_FEC_VALOR = ? ") ;
				qrySentencia.append(" AND mov.DF_FEC_VALOR = TO_DATE (?, 'dd/mm/yyyy') ");
					
				conditions.add(fechaValor);
			}
	
       if(!"".equals(clavePrograma)  && clavePrograma != null ) 	{						
				qrySentencia.append(" AND mov.IC_PROGRAMA_FONDOJR = ? ");
				conditions.add(clavePrograma);
			}
      
			
		
			qrySentencia.append(" AND (");
		
			for(int i=0;i<pageIds.size();i++) {
			List lItem = (ArrayList)pageIds.get(i);
				if(i>0) {
						qrySentencia.append("  OR  "); 
				}
				qrySentencia.append(" (IG_MOV_GRAL = ? ");
					conditions.add(new Long(lItem.get(0).toString()));			
				qrySentencia.append(" ) ");
			}
		qrySentencia.append(") " );

		
		qrySentencia.append(" ORDER BY mov.IG_MOV_GRAL ") ;
		
		log.debug("paginador :: fecha :: " + fechaValor);
		log.debug("Paginador :: TipoOperacion::"+ operacion);
			
		log.debug("getDocumentSummaryQueryForIds "+conditions.toString());
		log.debug("getDocumentSummaryQueryForIds "+qrySentencia.toString());
		return qrySentencia.toString();
	}
	
	
	

	public String getDocumentQueryFile(){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();

	qrySentencia.append(" SELECT mov.IG_MOV_GRAL as MOVIMIENTO, mov.IG_FOLIO AS FOLIO, TO_CHAR(mov.DF_FEC_MOVIMIENTO, 'DD/MM/YYYY') as FECHA_MOVIMIENTO, "+
	                    " TO_CHAR(mov.DF_FEC_VALOR, 'DD/MM/YYYY') AS FECHA_VALOR, mov.FG_CAPITAL AS IMPORTE, "+
							  " mov.CG_REFERENCIA AS REFERENCIA, cat.cg_operacion AS OPERACION, mov.CG_OBSERVACIONES AS OBSERVACIONES");
		
		
		qrySentencia.append(" FROM  COM_MOV_GRAL_FONDO_JR  mov, COMCAT_OPER_FONDO_JR cat ");

			qrySentencia.append(" WHERE  mov.IG_OPER_FONDO_JR =  cat.IG_OPER_FONDO_JR ") ;
			
		
		if((!"".equals(operacion) && operacion!=null))
			{
				qrySentencia.append(" AND  mov.IG_OPER_FONDO_JR =  ? ") ;
				conditions.add(operacion);
			}
		
			
			//fecha
			if((!"".equals(fechaValor) && fechaValor!=null))
			{
				//qrySentencia.append(" AND mov.DF_FEC_VALOR = ? ") ;
				qrySentencia.append(" AND mov.DF_FEC_VALOR = TO_DATE (?, 'dd/mm/yyyy') ");
				
				conditions.add(fechaValor);
			}
			
       if(!"".equals(clavePrograma)  && clavePrograma != null ) 	{						
				qrySentencia.append(" AND mov.IC_PROGRAMA_FONDOJR = ? ");
				conditions.add(clavePrograma);
			}
      
      qrySentencia.append(" ORDER BY mov.IG_MOV_GRAL ") ;
      
			log.debug("paginador :: fecha :: " + fechaValor);
			log.debug("Paginador :: TipoOperacion::"+ operacion);
      log.debug("Paginador :: clavePrograma::"+ clavePrograma);
			log.debug("getDocumentQueryFile "+conditions.toString());
			log.debug("getDocumentQueryFile "+qrySentencia.toString());
		return qrySentencia.toString();
	}//getDocumentQueryFile
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rg, String path, String tipo) {
		String nombreArchivo = "";
		String linea = "";
		OutputStreamWriter writer = null;   
		BufferedWriter buffer = null;
		if("PDF".equals(tipo)){
			try{
				HttpSession session = request.getSession();
				
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
				String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual		= fechaActual.substring(0,2);
				String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual		= fechaActual.substring(6,10);
				String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				pdfDoc.encabezadoConImagenesPersonalizado(pdfDoc,(String)session.getAttribute("strPais"),
														((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
														(String)session.getAttribute("sesExterno"),								 
														(String) session.getAttribute("strNombre"),    
														(String) session.getAttribute("strNombreUsuario"),
														"00archivos/15cadenas/15archcadenas/logos/" + (String)session.getAttribute("strLogo"),
														"00utils/gif/SE.gif",
														(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				
				pdfDoc.addText("Consulta de Movimientos JR\n","formas",ComunesPDF.CENTER);
				pdfDoc.addText("Programa:      "+(String) session.getAttribute("descProgramaFondoJR"),"formas",ComunesPDF.CENTER);	
				pdfDoc.setTable(7, 100);
				
				pdfDoc.setCell("FECHA DE MOVIMIENTO","celda01rep",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell("FECHA VALOR","celda01rep",ComunesPDF.CENTER,1,1,1); 	
				pdfDoc.setCell("REFERENCIA","celda01rep",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell("IMPORTE","celda01rep",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell("MOVIMIENTO","celda01rep",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell("OPERACION","celda01rep",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell("OBSERVACIONES","celda01rep",ComunesPDF.CENTER,1,1,1);
				
				while (rg.next()){
					String fechaMovimiento1		= (rg.getString("FECHA_MOVIMIENTO") == null) ? "" : rg.getString("FECHA_MOVIMIENTO");
					String fechaValor1 					= (rg.getString("FECHA_VALOR") == null) ? "" : rg.getString("FECHA_VALOR");
					String referencia						= (rg.getString("REFERENCIA") == null) ? "" : rg.getString("REFERENCIA");
					String importe			= (rg.getString("IMPORTE") == null) ? "" : rg.getString("IMPORTE");
					String movimiento	= (rg.getString("MOVIMIENTO") == null) ? "" : rg.getString("MOVIMIENTO");
					String operacion1						= (rg.getString("OPERACION") == null) ? "" : rg.getString("OPERACION");
					String observaciones					= (rg.getString("OBSERVACIONES") == null) ? "" : rg.getString("OBSERVACIONES");
					
					pdfDoc.setCell(fechaMovimiento1,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaValor1,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(referencia,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$ "+Comunes.formatoDecimal(importe,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(movimiento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(operacion1,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(observaciones,"formas",ComunesPDF.CENTER);
					
				}
				
				pdfDoc.addTable();
				pdfDoc.endDocument();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo",e);
			}finally{
				try{
				}catch(Exception e){}
			}
		}	
		return nombreArchivo;
	}
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String nombreArchivo = "";
		String linea = "";
		OutputStreamWriter writer = null;   
		BufferedWriter buffer = null;
		
		return nombreArchivo;
	}

/*****************************************************
	 GETTERS
*******************************************************/
 
	/**
	  Obtiene la lista de parámetros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	

	
	//GET
public String getFechaValor()		 {return fechaValor;}
public String getOperacion()		 {return operacion;}
	
	
	 	
	   
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}


// SET	
public void setFechaValor(String fechaValor)		 { this.fechaValor = fechaValor; }
public void setOperacion(String operacion)		    { this.operacion = operacion; }

  public String getClavePrograma() {
    return clavePrograma;
  }

  public void setClavePrograma(String clavePrograma) {
    this.clavePrograma = clavePrograma;
  }




	
}