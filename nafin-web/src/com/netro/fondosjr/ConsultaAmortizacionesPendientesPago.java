package com.netro.fondosjr;

import com.netro.fondojr.FondoJunior;
import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.CQueryHelperRegExtJS;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsultaAmortizacionesPendientesPago implements IQueryGeneratorReg, IQueryGeneratorRegExtJS {
  
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaAmortizacionesPendientesPago.class);
	
	/**
	 * Constructor de la clase.
	 */
	public ConsultaAmortizacionesPendientesPago(){}
	
	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer 		strSQL;
	private List 		conditions;
	private String 	paginaOffset;
	private String 	paginaNo;
	
	// Parametros de la forma
	private String 	diaMinimo;
	private String 	diaMaximo;
	private ArrayList numerosPrestamo;
	private String 	clavePrograma;
	
	// Variables auxiliares de los parametros de la forma
	private boolean 	hayNumerosDePrestamo = false;
	private boolean	hayDiaMinimo			= false;
	private boolean	hayDiaMaximo			= false;	 
 
	/**
	* Obtiene el query para obtener los totales de la consulta
	* @return Cadena con la consulta de SQL, para obtener los totales
	*/
	
	public String getAggregateCalculationQuery(){
		
		log.info("getAggregateCalculationQuery(E)");
		
		strSQL 		= new StringBuffer();
		conditions 	= new ArrayList();
		
		StringBuffer 	variablesBind			= new StringBuffer();
		if(hayNumerosDePrestamo){
			for(int i=0;i<numerosPrestamo.size();i++){
				if(i>0) variablesBind.append(",");
				variablesBind.append("?");
			}
		}
		
		// Construir query
		strSQL.append(
			"	SELECT"  +
			"	SUM(D.FG_AMORTIZACION) 			AS CAPITAL,"  +
			"	SUM(D.FG_INTERES) 				AS INTERESES,"  +
			"	SUM(D.FG_TOTALVENCIMIENTO) 	AS TOTAL_A_PAGAR"  +
			"	FROM"  +
			"	COM_CRED_PEND_REEMBOLSO_FIDE D"  +
			"	WHERE"  +
			"	D.IG_PRESTAMO IN ("  +
			"	SELECT"  +
			"   C.IG_PRESTAMO"  +
			"	FROM"  +
			"	("  +
			"		SELECT"  +
			"       /*+ ordered index(PEND_REEMBOLSO IN_COM_CRE_PEN_REM_FIDE_01_NUK ) use_nl(PEND_REEMBOLSO PAGOS_FIDE) */ 		"  +
			"				PEND_REEMBOLSO.IG_PRESTAMO,"  +
			"				("  +
			"					CASE WHEN"  +
			"						TO_NUMBER(TRUNC(SYSDATE) - MIN(PEND_REEMBOLSO.COM_FECHAPROBABLEPAGO)) < 0 THEN 0 	"  +
			"					ELSE"  +
			"						TO_NUMBER(TRUNC(SYSDATE) - MIN(PEND_REEMBOLSO.COM_FECHAPROBABLEPAGO))  	"  +
			"					END"  +
			"				) AS DIAS_TRANSCURRIDOS_VENC"  +
			"		FROM"  +
			"			COM_CRED_PEND_REEMBOLSO_FIDE 	PEND_REEMBOLSO,"  +
			"			COM_PAGOS_FIDE_VAL 				PAGOS_FIDE"  + 
			"		WHERE"  +
			"			PEND_REEMBOLSO.CG_ESTATUS 					= 'NP'"  +
			"			AND PEND_REEMBOLSO.IG_PRESTAMO 				= PAGOS_FIDE.IG_PRESTAMO"  +
			"        AND PAGOS_FIDE.PREPAGO_VALIDACION  = 'N'"  +//F06-2014
			"			AND PAGOS_FIDE.IC_PROGRAMA_FONDOJR			= ?");
		strSQL.append(hayNumerosDePrestamo?" AND PEND_REEMBOLSO.IG_PRESTAMO IN ("+variablesBind.toString()+") ":"");
		strSQL.append(
			"		GROUP BY"  +
			"			PEND_REEMBOLSO.IG_PRESTAMO"  +
			"	) C 																						" 
		);
		if(hayDiaMinimo && hayDiaMaximo){
			strSQL.append( " WHERE C.DIAS_TRANSCURRIDOS_VENC >= ? AND C.DIAS_TRANSCURRIDOS_VENC <= ? ");
		}else if(hayDiaMinimo && !hayDiaMaximo){
			strSQL.append( " WHERE C.DIAS_TRANSCURRIDOS_VENC >= ? ");
		}else if(!hayDiaMinimo && hayDiaMaximo){
			strSQL.append( " WHERE C.DIAS_TRANSCURRIDOS_VENC <= ? ");
		}
		strSQL.append(
			") 																							"
		);
		
		// Construir condiciones
		conditions.add(new Integer(clavePrograma));
		
		if(hayNumerosDePrestamo){
			for(int i=0;i<numerosPrestamo.size();i++){
				conditions.add((String)numerosPrestamo.get(i));
			}
		}
		
		if(hayDiaMinimo && hayDiaMaximo){
			conditions.add(new Integer(diaMinimo));
			conditions.add(new Integer(diaMaximo));
		}else if(hayDiaMinimo && !hayDiaMaximo){
			conditions.add(new Integer(diaMinimo));
		}else if(!hayDiaMinimo && hayDiaMaximo){ 
			conditions.add(new Integer(diaMaximo));
		}
 
		log.debug("strSQL     = <" + strSQL.toString() + ">");
		log.debug("conditions = <" + conditions        + ">");
		
		log.info("getAggregateCalculationQuery(S)");
		
		return strSQL.toString();
		
	}//getAggregateCalculationQuery
	
	/**
	* Obtiene el query para obtener las llaves primarias de la consulta
	* @return Cadena con la consulta de SQL, para obtener llaves primarias
	*/
	public String getDocumentQuery(){
		
		log.info("getDocumentQuery(E)");
		
		strSQL 		= new StringBuffer();
		conditions 	= new ArrayList();
 
		StringBuffer 	variablesBind			= new StringBuffer();
		if(hayNumerosDePrestamo){
			for(int i=0;i<numerosPrestamo.size();i++){
				if(i>0) variablesBind.append(",");
				variablesBind.append("?");
			}
		}
		
		// Construir query
		strSQL.append(
			" SELECT"  +
			"   C.IG_PRESTAMO,"  +
			"   C.DIAS_TRANSCURRIDOS_VENC,"  +
			"	 C.FECHA_AMORT_MAS_ANTIGUA"  +
			"   FROM"  +
			"	("  +
			"		SELECT"  +
			"	/*+ ordered index(PEND_REEMBOLSO IN_COM_CRE_PEN_REM_FIDE_01_NUK ) use_nl(PEND_REEMBOLSO PAGOS_FIDE) */ 	"  +
			"	PEND_REEMBOLSO.IG_PRESTAMO,"  +
			"	("  +
			"	CASE WHEN"  +
			"	TO_NUMBER(TRUNC(SYSDATE) - MIN(PEND_REEMBOLSO.COM_FECHAPROBABLEPAGO)) < 0 THEN 0 	"  +
			"	ELSE"  +
			"	TO_NUMBER(TRUNC(SYSDATE) - MIN(PEND_REEMBOLSO.COM_FECHAPROBABLEPAGO))	"  +
			"	END"  +
			"	) AS DIAS_TRANSCURRIDOS_VENC,"  +
			"	MIN(PEND_REEMBOLSO.COM_FECHAPROBABLEPAGO) AS FECHA_AMORT_MAS_ANTIGUA "  +
			"	FROM"  +
			"	COM_CRED_PEND_REEMBOLSO_FIDE 	PEND_REEMBOLSO,"  +
			"	COM_PAGOS_FIDE_VAL PAGOS_FIDE "  +
			"	WHERE "+
			"	PEND_REEMBOLSO.CG_ESTATUS = 'NP' "  +
			"	AND PEND_REEMBOLSO.IG_PRESTAMO= PAGOS_FIDE.IG_PRESTAMO "  +
			"	AND PAGOS_FIDE.PREPAGO_VALIDACION  = 'N' "  +//F06-2014
			"	AND PAGOS_FIDE.IC_PROGRAMA_FONDOJR= ? "
		);  
		strSQL.append(hayNumerosDePrestamo?" AND PEND_REEMBOLSO.IG_PRESTAMO IN ("+variablesBind.toString()+") ":"");
		strSQL.append(
			"		GROUP BY "  +
			"			PEND_REEMBOLSO.IG_PRESTAMO "  +
			"	) C ");
		if(hayDiaMinimo && hayDiaMaximo){
			strSQL.append( " WHERE C.DIAS_TRANSCURRIDOS_VENC >= ? AND C.DIAS_TRANSCURRIDOS_VENC <= ? ");
		}else if(hayDiaMinimo && !hayDiaMaximo){
			strSQL.append( " WHERE C.DIAS_TRANSCURRIDOS_VENC >= ? ");
		}else if(!hayDiaMinimo && hayDiaMaximo){
			strSQL.append( " WHERE C.DIAS_TRANSCURRIDOS_VENC <= ? ");
		}
		strSQL.append(
			"ORDER BY C.FECHA_AMORT_MAS_ANTIGUA, C.IG_PRESTAMO "  	
		);
		
		// Construir condiciones
		conditions.add(new Integer(clavePrograma));
		
		if(hayNumerosDePrestamo){
			for(int i=0;i<numerosPrestamo.size();i++){
				conditions.add((String)numerosPrestamo.get(i));
			}
		}
		
		if(hayDiaMinimo && hayDiaMaximo){
			conditions.add(new Integer(diaMinimo));
			conditions.add(new Integer(diaMaximo));
		}else if(hayDiaMinimo && !hayDiaMaximo){
			conditions.add(new Integer(diaMinimo));
		}else if(!hayDiaMinimo && hayDiaMaximo){
			conditions.add(new Integer(diaMaximo));
		}
					
		log.debug("strSQL     =  <" + strSQL.toString() + ">");
		log.debug("conditions =  <" + conditions        + ">");
		
		log.info("getDocumentQuery(S)");
		
		return strSQL.toString();
		
	}//getDocumentQuery
	 
	/**
	* Obtiene el query necesario para mostrar la informaci�n completa de 
	* una p�gina a partir de las llaves primarias enviadas como par�metro
	* @return Cadena con la consulta de SQL, para obtener la informaci�n 
	* completa de los registros con las llaves especificadas
	*/
	public String getDocumentSummaryQueryForIds(List pageIds){
		
		log.info("getDocumentSummaryQueryForIds(E)");
		
		strSQL 		= new StringBuffer();
		conditions 	= new ArrayList();

		boolean			hayPageIds				= pageIds != null && pageIds.size() > 0?true:false;	
		StringBuffer 	variablesBind			= new StringBuffer();
		if(hayPageIds){
			for(int i=0;i<pageIds.size();i++){
				if(i>0) variablesBind.append(",");
				variablesBind.append("?");
			}
		}
		
		// Construir query
		strSQL.append(
			" SELECT"  +
			"	D.FECHA_AMORTIZACION_NAFIN,"  +
			"	D.PRESTAMO,"  +
			"	D.CLIENTE,"  +
			"	("  + 
			"		SELECT"  +
			"			COUNT(1)"  +
			"		FROM"  +
			"			COM_CRED_PEND_REEMBOLSO_FIDE A"  +
			"		WHERE"  +
			"			A.CG_ESTATUS             = 'NP' 				"  +
			"		AND A.IG_PRESTAMO           = D.PRESTAMO 		"  +
			"		AND A.COM_FECHAPROBABLEPAGO < TRUNC(SYSDATE) "	+				
			"	) AS NUM_CUOTAS_A_PAGAR,"  +
			"	D.DIAS_TRANSCURRIDOS_VENC,"  +
			"	D.CAPITAL,"  +
			"	D.INTERESES,"  +
			"	D.TOTAL_A_PAGAR,"  +
			"	D.ORIGEN,"  +
			"	D.FECHA_AMORT_MAS_ANTIGUA"  +
			" FROM ("
		);
		strSQL.append(
			" SELECT"  +
			"   TO_CHAR(MIN(PEND_REEMBOLSO.COM_FECHAPROBABLEPAGO),'DD/MM/YYYY') 	AS FECHA_AMORTIZACION_NAFIN, 		"  +
			"   PEND_REEMBOLSO.IG_PRESTAMO AS PRESTAMO,"  +
			"	 ("  +
			"		SELECT /*+ index(FIDE_VAL IN_COM_PAGOS_FIDE_VAL_03_NUK)*/"  +
			"			FIDE_VAL.CG_NOMBRECLIENTE"  +
			"		FROM"  +
			"			COM_PAGOS_FIDE_VAL FIDE_VAL"  +
			"		WHERE"  +
			"			FIDE_VAL.IG_CLIENTE = PEND_REEMBOLSO.IG_CLIENTE"  +
			"        AND FIDE_VAL.PREPAGO_VALIDACION  = 'N'"  +//F06-2014
			"			AND ROWNUM = 1"  +	
			"	 ) AS CLIENTE,"  +
			"   ("  +
			"		CASE WHEN"  +
			"      	TO_NUMBER(TRUNC(SYSDATE) - MIN(PEND_REEMBOLSO.COM_FECHAPROBABLEPAGO)) < 0 THEN 0 	"  +
			"		ELSE"  +
			"      	TO_NUMBER(TRUNC(SYSDATE) - MIN(PEND_REEMBOLSO.COM_FECHAPROBABLEPAGO))	"  +
			"		END"  +
			"	) AS DIAS_TRANSCURRIDOS_VENC,"  +
			"   SUM(PEND_REEMBOLSO.FG_AMORTIZACION) 		AS CAPITAL,"  +
			"   SUM(PEND_REEMBOLSO.FG_INTERES) 				AS INTERESES,"  +
			"   SUM(PEND_REEMBOLSO.FG_TOTALVENCIMIENTO) 	AS TOTAL_A_PAGAR,"  +
			"   PEND_REEMBOLSO.IG_ORIGEN 						AS ORIGEN,"  +
			"	MIN(PEND_REEMBOLSO.COM_FECHAPROBABLEPAGO) AS FECHA_AMORT_MAS_ANTIGUA"  +
			" FROM"  +
			"   COM_CRED_PEND_REEMBOLSO_FIDE PEND_REEMBOLSO"  +
			" WHERE"  +
			"   PEND_REEMBOLSO.CG_ESTATUS = 'NP'");
		strSQL.append(hayPageIds?" AND PEND_REEMBOLSO.IG_PRESTAMO IN ( "+ variablesBind.toString()+" ) ":"");
		strSQL.append(
			" GROUP BY"  +
			"	PEND_REEMBOLSO.IG_PRESTAMO,"  +
			"	PEND_REEMBOLSO.IG_CLIENTE,"  +
			"	PEND_REEMBOLSO.IG_ORIGEN" 
		);
		strSQL.append(
			") D  																									"  +
			"ORDER BY FECHA_AMORT_MAS_ANTIGUA, PRESTAMO	 												"
		);
		
		// Agregar parametros de consulta
		if(hayPageIds){
			for(int i=0;i<pageIds.size();i++){
				List 		lItem 				= (ArrayList) pageIds.get(i);
				String 	numeroPrestamo 	= lItem.get(0).toString();
				conditions.add(new Long(numeroPrestamo));
			}
		}				 
		
		log.debug("strSQL     = <" + strSQL.toString() + ">");
		log.debug("conditions = <" + conditions        + ">");
		
		log.info("getDocumentSummaryQueryForIds(S)");
		
		return strSQL.toString();
		
	}//getDocumentSummaryQueryForIds	
	
	public String getDocumentQueryFile(){
		
		log.info("getDocumentQueryFile(E)");
		
		strSQL 		= new StringBuffer();
		conditions 	= new ArrayList();
 
		StringBuffer 	variablesBind			= new StringBuffer();
		if(hayNumerosDePrestamo){
			for(int i=0;i<numerosPrestamo.size();i++){
				if(i>0) variablesBind.append(",");
				variablesBind.append("?");
			}
		}
		
		strSQL.append(
			"	SELECT"  +
			"	D.FECHA_AMORTIZACION_NAFIN,"  +
			"	D.PRESTAMO,"  +
			"	D.CLIENTE,"  +
			"	("  + 
			"		SELECT"  +
			"			COUNT(1)"  +
			"		FROM"  +
			"			COM_CRED_PEND_REEMBOLSO_FIDE A"  +
			"		WHERE"  +
			"			A.CG_ESTATUS             = 'NP'"  +
			"		AND A.IG_PRESTAMO           = D.PRESTAMO 		"  +
			"		AND A.COM_FECHAPROBABLEPAGO < TRUNC(SYSDATE) "	+				
			"	) AS NUM_CUOTAS_A_PAGAR,"  +
			"	D.DIAS_TRANSCURRIDOS_VENC,"  +
			"	D.CAPITAL,"  +
			"	D.INTERESES,"  +
			"	D.TOTAL_A_PAGAR,"  +
			"	D.ORIGEN,"  +
			"	D.FECHA_AMORT_MAS_ANTIGUA"  +
			" FROM ("
		);
		strSQL.append(
			"	SELECT"  +
			"   TO_CHAR(MIN(PEND_REEMBOLSO.COM_FECHAPROBABLEPAGO),'DD/MM/YYYY') 	AS FECHA_AMORTIZACION_NAFIN, 		"  +
			"   PEND_REEMBOLSO.IG_PRESTAMO 						AS PRESTAMO,"  +
			"	 ("  +
			"		SELECT /*+ index(FIDE_VAL IN_COM_PAGOS_FIDE_VAL_03_NUK)*/"  +
			"			FIDE_VAL.CG_NOMBRECLIENTE"  +
			"		FROM"  +
			"			COM_PAGOS_FIDE_VAL FIDE_VAL"  +
			"		WHERE"  +
			"			FIDE_VAL.IG_CLIENTE = PEND_REEMBOLSO.IG_CLIENTE"  +
			"        AND FIDE_VAL.PREPAGO_VALIDACION  = 'N'"  + //F06-2014
			"			AND ROWNUM = 1"  +	
			"	 ) AS CLIENTE,"  +
			"   ("  +
			"		CASE WHEN"  +
			"      	TO_NUMBER(TRUNC(SYSDATE) - MIN(PEND_REEMBOLSO.COM_FECHAPROBABLEPAGO)) < 0 THEN 0 	"  +
			"		ELSE"  +
			"      	TO_NUMBER(TRUNC(SYSDATE) - MIN(PEND_REEMBOLSO.COM_FECHAPROBABLEPAGO))	"  +
			"		END"  +
			"	) AS DIAS_TRANSCURRIDOS_VENC,"  +
			"   SUM(PEND_REEMBOLSO.FG_AMORTIZACION) 		AS CAPITAL,"  +
			"   SUM(PEND_REEMBOLSO.FG_INTERES) 				AS INTERESES,"  +
			"   SUM(PEND_REEMBOLSO.FG_TOTALVENCIMIENTO) 	AS TOTAL_A_PAGAR,"  +
			"   PEND_REEMBOLSO.IG_ORIGEN 						AS ORIGEN,"  +
			"	MIN(PEND_REEMBOLSO.COM_FECHAPROBABLEPAGO) AS FECHA_AMORT_MAS_ANTIGUA"  +
			"	FROM"  +
			"   COM_CRED_PEND_REEMBOLSO_FIDE PEND_REEMBOLSO"  +
			"	WHERE"  +
			"   PEND_REEMBOLSO.CG_ESTATUS = 'NP' AND"  +
			"	PEND_REEMBOLSO.IG_PRESTAMO IN ("  +
			"	SELECT"  +
			"					C.IG_PRESTAMO"  +
			"				FROM"  +
			"					("  +
			"						SELECT"  +
			"								/*+ ordered index(PEND_REEMBOLSO IN_COM_CRE_PEN_REM_FIDE_01_NUK ) use_nl(PEND_REEMBOLSO PAGOS_FIDE) */  "  +
			"								PEND_REEMBOLSO.IG_PRESTAMO,"  +
			"								("  +
			"									CASE WHEN"  +
			"										TO_NUMBER(TRUNC(SYSDATE) - MIN(PEND_REEMBOLSO.COM_FECHAPROBABLEPAGO)) < 0 THEN 0 	"  +
			"									ELSE"  +
			"										TO_NUMBER(TRUNC(SYSDATE) - MIN(PEND_REEMBOLSO.COM_FECHAPROBABLEPAGO))	"  +
			"									END"  +
			"								) AS DIAS_TRANSCURRIDOS_VENC"  +
			"						FROM"  +
			"							COM_CRED_PEND_REEMBOLSO_FIDE 	PEND_REEMBOLSO,"  +
			"							COM_PAGOS_FIDE_VAL 				PAGOS_FIDE"  + 
			"						WHERE"  +
			"							PEND_REEMBOLSO.CG_ESTATUS 					= 'NP'AND	"  +
			"							PEND_REEMBOLSO.IG_PRESTAMO 				= PAGOS_FIDE.IG_PRESTAMO				AND	"  +
			"							PAGOS_FIDE.IC_PROGRAMA_FONDOJR			= ?" +
			"                    AND PAGOS_FIDE.PREPAGO_VALIDACION      = 'N'" //F06-2014
		);
		strSQL.append(hayNumerosDePrestamo?" AND PEND_REEMBOLSO.IG_PRESTAMO IN ("+variablesBind.toString()+") ":"");
		strSQL.append(
			"						GROUP BY"  +
			"							PEND_REEMBOLSO.IG_PRESTAMO"  +
			"					) C 																					");
		if(hayDiaMinimo && hayDiaMaximo){
			strSQL.append( " WHERE C.DIAS_TRANSCURRIDOS_VENC >= ? AND C.DIAS_TRANSCURRIDOS_VENC <= ? ");
		}else if(hayDiaMinimo && !hayDiaMaximo){
			strSQL.append( " WHERE C.DIAS_TRANSCURRIDOS_VENC >= ? ");
		}else if(!hayDiaMinimo && hayDiaMaximo){
			strSQL.append( " WHERE C.DIAS_TRANSCURRIDOS_VENC <= ? ");
		}
		strSQL.append(		
			"	 ) 																									"  +
			"GROUP BY  																								"  +
			"	PEND_REEMBOLSO.IG_PRESTAMO, 																	"  +
			"	PEND_REEMBOLSO.IG_CLIENTE,  																	"  +
			"	PEND_REEMBOLSO.IG_ORIGEN 																		"  			
		);
		strSQL.append(
			") D                                                                             "  +
			"ORDER BY FECHA_AMORT_MAS_ANTIGUA, PRESTAMO 													"
		);
		
		
		// Construir condiciones
		conditions.add(new Integer(clavePrograma));
		
		if(hayNumerosDePrestamo){
			for(int i=0;i<numerosPrestamo.size();i++){
				conditions.add((String)numerosPrestamo.get(i));
			}
		}
		
		if(hayDiaMinimo && hayDiaMaximo){
			conditions.add(new Integer(diaMinimo));
			conditions.add(new Integer(diaMaximo));
		}else if(hayDiaMinimo && !hayDiaMaximo){
			conditions.add(new Integer(diaMinimo));
		}else if(!hayDiaMinimo && hayDiaMaximo){
			conditions.add(new Integer(diaMaximo));
		}
		
		log.debug("strSQL      = <" + strSQL.toString() + ">");
		log.debug("conditions  = <" + conditions        + ">");
		
		log.info("getDocumentQueryFile(S)");
		
		return strSQL.toString();
		
	}//getDocumentQueryFile
	
	/**
	 * Funciones para la nueva version de la pantalla 
	 */
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rg, String path, String tipo) {
		String nombreArchivo = "";
		String linea = "";
		OutputStreamWriter writer = null;   
		BufferedWriter buffer = null;
		if("PDF".equals(tipo)){
			try{
				HttpSession session = request.getSession();
				BigDecimal totalPaginaCapital 	 = new BigDecimal("0.00");
				BigDecimal totalPaginaIntereses 	 = new BigDecimal("0.00");
				BigDecimal totalPaginaTotalAPagar = new BigDecimal("0.00");
				HashMap		totalPorPagina = new HashMap();
				FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);
				HashMap catalogoOrigenPrograma 		= fondoJunior.getCatalogoOrigenPrograma();

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
				String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual		= fechaActual.substring(0,2);
				String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual		= fechaActual.substring(6,10);
				String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				( session.getAttribute("iNoNafinElectronico") == null?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText((String) session.getAttribute("descProgramaFondoJR"),"formas",ComunesPDF.CENTER);
				pdfDoc.addText("Amortizaciones Pendientes de Pago","formasG",ComunesPDF.CENTER);
				pdfDoc.addText(" ","formas",ComunesPDF.CENTER);   
				pdfDoc.setTable(9, 100);
				pdfDoc.setCell("Fecha Amortizaci�n Nafin","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Pr�stamo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Cliente","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�m. Cuota a Pagar","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("D�as Transcurridos de Vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Capital","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Total a Pagar","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Origen","celda01",ComunesPDF.CENTER);
				while (rg.next()){
					String fechaAmortizacion		= (rg.getString("FECHA_AMORTIZACION_NAFIN") == null) ? "" : rg.getString("FECHA_AMORTIZACION_NAFIN");
					String prestamo 					= (rg.getString("PRESTAMO") == null) ? "" : rg.getString("PRESTAMO");
					String cliente						= (rg.getString("CLIENTE") == null) ? "" : rg.getString("CLIENTE");
					String numCuotasAPagar			= (rg.getString("NUM_CUOTAS_A_PAGAR") == null) ? "" : rg.getString("NUM_CUOTAS_A_PAGAR");
					String diasTranscurridosVenc	= (rg.getString("DIAS_TRANSCURRIDOS_VENC") == null) ? "" : rg.getString("DIAS_TRANSCURRIDOS_VENC");
					String capital						= (rg.getString("CAPITAL") == null) ? "" : rg.getString("CAPITAL");
					String intereses					= (rg.getString("INTERESES") == null) ? "" : rg.getString("INTERESES");
					String totalAPagar				= (rg.getString("TOTAL_A_PAGAR") == null) ? "" : rg.getString("TOTAL_A_PAGAR");
					String origen						= (rg.getString("ORIGEN") == 	null) ? "" : rg.getString("ORIGEN");
					String auxOrigen = fondoJunior.getDescripcionOrigenPrograma(origen,catalogoOrigenPrograma);

					totalPaginaCapital 		= totalPaginaCapital.add(			new BigDecimal(rg.getString("CAPITAL"))			);
					totalPaginaIntereses 	= totalPaginaIntereses.add(		new BigDecimal(rg.getString("INTERESES"))		);
					totalPaginaTotalAPagar 	= totalPaginaTotalAPagar.add(		new BigDecimal(rg.getString("TOTAL_A_PAGAR"))	);
					pdfDoc.setCell(fechaAmortizacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(prestamo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cliente,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(numCuotasAPagar,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(diasTranscurridosVenc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+capital,"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+intereses,"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+totalAPagar,"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(auxOrigen,"formas",ComunesPDF.CENTER);
					
					
				}
				/*totalPorPagina.put("CAPITAL",			totalPaginaCapital.toPlainString()			);
				totalPorPagina.put("INTERESES",		totalPaginaIntereses.toPlainString()		);
				totalPorPagina.put("TOTAL_A_PAGAR",	totalPaginaTotalAPagar.toPlainString()	);
				pdfDoc.setCell("Total:","formas", ComunesPDF.LEFT,5);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(totalPorPagina.get("CAPITAL")     	   ,2,true), 	"formas", ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(totalPorPagina.get("INTERESES")        ,2,true), 	"formas", ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(totalPorPagina.get("TOTAL_A_PAGAR")    ,2,true), 	"formas", ComunesPDF.RIGHT);
				pdfDoc.setCell(" ","formas", ComunesPDF.CENTER);*/
				/*CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS();
				Registros regi = queryHelper.getResultCount(request);
				if(regi.next()){
					pdfDoc.setCell("Total: ", 																					"formas", ComunesPDF.LEFT,5);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(regi.getString("CAPITAL")     	  ,2,true), 		"formas", ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(regi.getString("INTERESES")        ,2,true), 		"formas", ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(regi.getString("TOTAL_A_PAGAR")    ,2,true), 		"formas", ComunesPDF.CENTER);
					pdfDoc.setCell(" ", 																							   "formas", ComunesPDF.CENTER);
				}*/
				pdfDoc.addTable();
				pdfDoc.endDocument();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo",e);
			}finally{
				try{
				}catch(Exception e){}
			}
		}	
		return nombreArchivo;
	}
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";
		String nombreArchivo = "";
		String espacio = "";
		OutputStreamWriter writer = null;  
		BufferedWriter buffer = null;
     	try {
	
			FondoJunior fondoJunior = ServiceLocator.getInstance().lookup("FondoJuniorEJB", FondoJunior.class);
			HashMap catalogoOrigenPrograma 		= fondoJunior.getCatalogoOrigenPrograma();

			if(tipo.equals("CSV")) {
				HttpSession session = request.getSession();
				String gsCveUsuario = (String) session.getAttribute("descProgramaFondoJR");
				linea = "Programa:"+","+gsCveUsuario+"\r\n"+"Fecha Amortizaci�n Nafin ,Pr�stamo,Cliente,N�m. Cuota a Pagar,D�as Transcurridos de Vencimiento,Capital,Inter�s,Total a Pagar,Origen\n";
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
				buffer = new BufferedWriter(writer);
				buffer.write(linea);
				while (rs.next()) {
					String fechaAmortizacion		= (rs.getString("FECHA_AMORTIZACION_NAFIN") == null) ? "" : rs.getString("FECHA_AMORTIZACION_NAFIN");
					String prestamo 					= (rs.getString("PRESTAMO") == null) ? "" : rs.getString("PRESTAMO");
					String cliente						= (rs.getString("CLIENTE") == null) ? "" : rs.getString("CLIENTE");
					String numCuotasAPagar			= (rs.getString("NUM_CUOTAS_A_PAGAR") == null) ? "" : rs.getString("NUM_CUOTAS_A_PAGAR");
					String diasTranscurridosVenc	= (rs.getString("DIAS_TRANSCURRIDOS_VENC") == null) ? "" : rs.getString("DIAS_TRANSCURRIDOS_VENC");
					String capital						= (rs.getString("CAPITAL") == null) ? "" : rs.getString("CAPITAL");
					String intereses					= (rs.getString("INTERESES") == null) ? "" : rs.getString("INTERESES");
					String totalAPagar				= (rs.getString("TOTAL_A_PAGAR") == null) ? "" : rs.getString("TOTAL_A_PAGAR");
					String origen						= (rs.getString("ORIGEN") == 	null) ? "" : rs.getString("ORIGEN");
					String auxOrigen = fondoJunior.getDescripcionOrigenPrograma(origen,catalogoOrigenPrograma);
					linea = fechaAmortizacion.replace(',',' ') + ", " + 
								prestamo.replace(',',' ') + ", " +
								cliente.replace(',',' ') + ", " +
								numCuotasAPagar.replace(',',' ') + ", " +
								diasTranscurridosVenc.replace(',',' ') + ", " +
								capital.replace(',',' ') + ", " +
								intereses.replace(',',' ') + ", " +
								totalAPagar.replace(',',' ') + ", " +
								auxOrigen.replace(',',' ') + "\n";
					buffer.write(linea);
				}
				CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS();
				Registros regi = queryHelper.getResultCount(request);
				if(regi.next()){
					linea="Total: "+","+","+","+","+","+regi.getString("CAPITAL")+","+regi.getString("INTERESES")+","+regi.getString("TOTAL_A_PAGAR")+"\n";
					buffer.write(linea);
				}
				buffer.close();
			}
		 }catch (Throwable e) {
			throw new AppException("Error al generar el archivo",e);
		}
		finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
		
		return nombreArchivo;
	}
	/**
	 * Termino de las funciones para la nueva version  
	 */
	
	
	
	//GETTERS
  /**
	 * Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 * @return Lista con los parametros de las condiciones
	 */
	public List getConditions(){return conditions;}
  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() {return paginaNo;}
	
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset(){return paginaOffset;}
	
	//Parametros del formulario 
 	public String getDiaMinimo(){
		return this.diaMinimo;
	}

	public String getDiaMaximo(){
		return this.diaMaximo;
	}

	public ArrayList getNumeroPrestamo(){
		return this.numerosPrestamo;
	}
 
	public String getClavePrograma(){
		return this.clavePrograma;
	}

	
	//SETTERS
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo){paginaNo = newPaginaNo;}
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset){this.paginaOffset=paginaOffset;}
		
	//Parametros del formulario
	public void setDiaMinimo(String diaMinimo){
		hayDiaMinimo	= diaMinimo != null && !diaMinimo.trim().equals("")?true:false;
		this.diaMinimo = diaMinimo;
	}

	public void setDiaMaximo(String diaMaximo){
		hayDiaMaximo	= diaMaximo != null && !diaMaximo.trim().equals("")?true:false; 
		this.diaMaximo = diaMaximo;
	}

	public void setNumeroPrestamo(ArrayList numerosPrestamo){
		hayNumerosDePrestamo = (numerosPrestamo != null && numerosPrestamo.size() > 0)?true:false; 
		this.numerosPrestamo = numerosPrestamo;
	}
	
	public void setClavePrograma(String clavePrograma){
		this.clavePrograma = clavePrograma;
	}
 
}
