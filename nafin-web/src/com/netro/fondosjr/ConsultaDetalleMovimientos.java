package com.netro.fondosjr;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsultaDetalleMovimientos implements IQueryGeneratorReg ,IQueryGeneratorRegExtJS{ 
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaDetalleMovimientos.class);
	
	/**
	 * Constructor de la clase.
	 */
	public ConsultaDetalleMovimientos(){}
	
	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer 	strSQL;
	private List 	conditions;
	private String paginaOffset;
	private String paginaNo;
	private String strDirectorioPublicacion;
	private String clavePrograma;
	private String fecVencIni;
	private String fecVencFin;
	private String fecOperIni;
	private String fecOperFin;
	private String fecFideIni;
	private String fecFideFin;
	private String diasDesde;
	private String diasHasta;		

	/**
	* Obtiene el query para obtener los totales de la consulta
	* @return Cadena con la consulta de SQL, para obtener los totales
	*/
	public String getAggregateCalculationQuery(){
		log.info("getAggregateCalculationQuery(E) ::..");
		
		strSQL = new StringBuffer();
		conditions = new ArrayList();

		// F012 - 2011
		boolean hayDiasDeVencimientoEspecificados = false;
		if( (diasDesde != null && !diasDesde.equals("")) || (diasHasta != null && !diasHasta.equals("")) ){
			hayDiasDeVencimientoEspecificados = true;
		}
		
		strSQL.append(" SELECT SUM(total_capital) total_capital,");
		strSQL.append(" SUM(total_interes) total_interes,");
		strSQL.append(" SUM(total_saldo) total_saldo,");
		strSQL.append(" SUM(total_pagado) total_pagado,");
		strSQL.append(" SUM(total_no_pagado) total_no_pagado,");
		strSQL.append(" SUM(total_reembolso) total_reembolso,");
		strSQL.append(" SUM(total_reembolso_fini) total_reembolso_fini,");
		strSQL.append(" SUM(total_reembolso_tot) total_reembolso_tot,");
		strSQL.append(" (SUM (total_otros_mov_sum) + SUM (total_otros_mov_res) ) saldo_otros_mov " );
		//strSQL.append(" (SUM(total_saldo) - SUM(saldo_inicial)) saldo_otros_mov");
		//strSQL.append(" (SUM(total_saldo)  - SUM(saldo_inicial) - SUM(total_pagado) - SUM(total_no_pagado) - SUM(total_reembolso) - SUM(total_reembolso_fini) - SUM(total_reembolso_tot)) saldo_otros_mov");
		strSQL.append(" FROM (");
		strSQL.append(" SELECT SUM(total_capital) total_capital,");
		strSQL.append(" SUM(total_interes) total_interes,");
		strSQL.append(" SUM(total_saldo) total_saldo,");
		strSQL.append(" SUM(total_pagado) total_pagado,");
		strSQL.append(" SUM(total_no_pagado) total_no_pagado,");
		strSQL.append(" SUM(total_reembolso) total_reembolso,");
		strSQL.append(" SUM(total_reembolso_fini) total_reembolso_fini,");
		strSQL.append(" SUM(total_reembolso_tot) total_reembolso_tot,");
		strSQL.append(" SUM (total_otros_mov_sum) total_otros_mov_sum, ");
		strSQL.append(" SUM (total_otros_mov_res) total_otros_mov_res ");
		strSQL.append(" FROM (");
		strSQL.append(" SELECT SUM(NVL(amort, 0)) total_capital,");
		strSQL.append(" SUM(NVL(interes, 0)) total_interes,");
		strSQL.append(" SUM(NVL(saldo, 0)) total_saldo,");
		strSQL.append(" DECODE(estatus, 'P', SUM(saldo), 0) total_pagado,");
		strSQL.append(" DECODE(estatus, 'NP', SUM(saldo), 0) total_no_pagado,");
		strSQL.append(" DECODE(estatus, 'R', SUM(saldo), 0) total_reembolso,");
		strSQL.append(" DECODE(estatus, 'RF', SUM(saldo), 0) total_reembolso_fini,");
		strSQL.append(" DECODE(estatus, 'T', SUM(saldo), 0) total_reembolso_tot, ");
		strSQL.append(" DECODE (estatus,'SUMA', SUM (saldo), 0) total_otros_mov_sum, ");
		strSQL.append(" DECODE (estatus,'RESTA', SUM (saldo),0) total_otros_mov_res ");
		strSQL.append(" FROM (");
		strSQL.append(" SELECT ROWNUM ic_registro_tablota,");
		strSQL.append(" tablota.fec_venc fec_venc,");
		strSQL.append(" tablota.fec_fide fec_fide,");
		strSQL.append(" tablota.amort amort,");
		strSQL.append(" tablota.interes interes,");
		strSQL.append(" tablota.saldo saldo,");
		strSQL.append(" tablota.estatus estatus,");
		strSQL.append(" tablota.observaciones observaciones,");
		strSQL.append(" tablota.fec_val fec_val,");
		strSQL.append(" tablota.usuario usuario,");
		strSQL.append(" tablota.prestamo prestamo,");
		strSQL.append(" tablota.num_cliente num_cliente,");
		strSQL.append(" tablota.fec_cliente fec_cliente,");
		strSQL.append(" tablota.num_cuota num_cuota,");
		strSQL.append(" tablota.monto_reembolso,");
		strSQL.append(" tablota.ic_programa_fondojr");
		strSQL.append(" FROM (");
		//ESTA SECCION DEL QUERY OBTIENE EL SALDO INICAL DEL FONDO
		//strSQL.append(" SELECT NULL fec_venc,");
		strSQL.append(" SELECT '" + this.getFecVencIni() + "' fec_venc,");
		strSQL.append(" NULL fec_fide,");
		strSQL.append(" TO_NUMBER(NULL) amort,");
		strSQL.append(" TO_NUMBER(NULL) interes,");
		strSQL.append(" cmv.fg_capital saldo,");
		strSQL.append(" DECODE (cof.cg_operacion, 'Suma', 'INICIAL', 'INICIAL') estatus,");
		strSQL.append(" cof.cg_descripcion observaciones,");
		strSQL.append(" NULL fec_val,");
		strSQL.append(" cmv.cg_nousuario usuario,");
		strSQL.append(" TO_NUMBER(NULL) prestamo,");
		strSQL.append(" TO_NUMBER(NULL) num_cliente,");
		strSQL.append(" NULL fec_cliente,");
		strSQL.append(" TO_NUMBER(NULL) num_cuota,");
		strSQL.append(" TO_NUMBER(NULL) monto_reembolso,");
		strSQL.append(" cof.ic_programa_fondojr");
		if(hayDiasDeVencimientoEspecificados){ // F012-2011
			strSQL.append(", TO_NUMBER(NULL) as dias_transcurridos_vencimiento "); 
		}
		strSQL.append(" FROM comcat_oper_fondo_jr cof, com_mov_gral_fondo_jr cmv");
		strSQL.append(" WHERE cof.ig_oper_fondo_jr = cmv.ig_oper_fondo_jr");
		strSQL.append(" AND UPPER (cof.cg_descripcion) = 'SALDO INICIAL'");
		strSQL.append(" AND cof.ic_programa_fondojr = cmv.ic_programa_fondojr");
		strSQL.append(" AND cof.ic_programa_fondojr = ?");
		strSQL.append(" AND ROWNUM = 1");
		//A PARTIR DE AQUI ES LA CONSULTA NORMAL
		strSQL.append(" UNION ALL");
		strSQL.append(" SELECT /*+index(cpv in_com_pagos_fide_val_01_nuk)*/");
		strSQL.append(" TO_CHAR (cpv.com_fechaprobablepago, 'dd/mm/yyyy') fec_venc,");
		strSQL.append(" TO_CHAR (cpv.df_registro_fide, 'dd/mm/yyyy') fec_fide,");
		strSQL.append(" cpv.fg_amortizacion amort,");
		strSQL.append(" cpv.fg_interes interes,");
		strSQL.append(" cpv.fg_totalvencimiento saldo,");
		strSQL.append(" cpv.cg_estatus estatus,");
		strSQL.append(" cpv.cg_observaciones observaciones,");
		strSQL.append(" TO_CHAR (cpv.df_validacion , 'dd/mm/yyyy') fec_val,");
		strSQL.append(" cpv.cg_usuario usuario,");
		strSQL.append(" cpv.ig_prestamo prestamo,");
		strSQL.append(" cpv.IG_CLIENTE num_cliente,");
		strSQL.append(" TO_CHAR (cpv.DF_PAGO_CLIENTE,'dd/mm/yyyy') fec_cliente,");
		strSQL.append(" cpv.IG_DISPOSICION num_cuota,");
		strSQL.append(" TO_NUMBER (NULL) monto_reembolso,");
		strSQL.append(" cpv.ic_programa_fondojr");
		if(hayDiasDeVencimientoEspecificados){ // F012-2011
			strSQL.append(", TO_NUMBER(NULL) as dias_transcurridos_vencimiento "); 
		}
		strSQL.append(" FROM com_pagos_fide_val cpv");
	 
		if(hayDiasDeVencimientoEspecificados){ //Fodea 027-2011 
			strSQL.append(" WHERE (cpv.cg_estatus = 'P' OR cpv.cg_estatus = 'T' OR cpv.cg_estatus = 'RC')");
		}else{
			strSQL.append(" WHERE (cpv.cg_estatus = 'P' OR cpv.cg_estatus = 'T' OR cpv.cg_estatus = 'RC' OR cpv.cg_estatus = 'R')");
		}
		strSQL.append(" AND cpv.cs_validacion = 'A'");
		strSQL.append(" UNION ALL");
		strSQL.append(" SELECT /*+ use_nl(cpv rxf) index(cpv in_com_pagos_fide_val_01_nuk) index(rxf cp_com_reemb_finiquito_nuk)*/");
		strSQL.append(" DISTINCT TO_CHAR(rxf.df_reembolso, 'dd/mm/yyyy') fec_venc,");
		strSQL.append(" NULL fec_fide,");
		strSQL.append(" SUM(cpv.fg_amortizacion) amort,");
		strSQL.append(" SUM(cpv.fg_interes) interes,");
		strSQL.append(" SUM(cpv.fg_totalvencimiento) saldo,");
		strSQL.append(" cpv.cg_estatus estatus,");
		strSQL.append(" cpv.cg_observaciones observaciones,");
		strSQL.append(" NULL fec_val,");
		strSQL.append(" cpv.cg_usuario usuario,");
		strSQL.append(" cpv.ig_prestamo prestamo, cpv.ig_cliente num_cliente,");
		strSQL.append(" NULL fec_cliente,");
		strSQL.append(" TO_NUMBER (NULL) num_cuota,");
		strSQL.append(" rxf.fn_monto_reembolso monto_reembolso,");
		strSQL.append(" cpv.ic_programa_fondojr");
		if(hayDiasDeVencimientoEspecificados){ // F012-2011
			strSQL.append(", TO_NUMBER(NULL) as dias_transcurridos_vencimiento "); 
		}
		strSQL.append(" FROM com_pagos_fide_val cpv, com_reembolsos_finiquito rxf");
		strSQL.append(" WHERE cpv.ig_prestamo = rxf.ig_prestamo");
		strSQL.append(" AND cpv.cg_estatus = 'RF'");
		strSQL.append(" AND cpv.cs_validacion = 'A'");  
		strSQL.append(" AND NOT EXISTS (");
		strSQL.append(" SELECT 1");
		strSQL.append(" FROM com_pagos_fide_val");
		strSQL.append(" WHERE cg_estatus = 'RC'");
		strSQL.append(" AND com_fechaprobablepago = cpv.com_fechaprobablepago");
		strSQL.append(" AND ig_prestamo = cpv.ig_prestamo");
		strSQL.append(" AND ig_cliente = cpv.ig_cliente");
		strSQL.append(" AND ig_disposicion = cpv.ig_disposicion");
		strSQL.append(" AND fg_totalvencimiento = cpv.fg_totalvencimiento)");
		strSQL.append(" GROUP BY rxf.df_reembolso,");
		strSQL.append(" rxf.fn_monto_reembolso,");
		strSQL.append(" cpv.cg_estatus,");
		strSQL.append(" cpv.cg_usuario,");
		strSQL.append(" cpv.ig_prestamo,");
		strSQL.append(" cpv.ig_cliente,");
		strSQL.append(" cpv.cg_observaciones,");
		strSQL.append(" cpv.ic_programa_fondojr");
		strSQL.append(" UNION ALL");
		strSQL.append(" SELECT /*+ index(cpv in_com_pagos_fide_val_01_nuk)*/");
		strSQL.append(" TO_CHAR (cpv.com_fechaprobablepago, 'dd/mm/yyyy') fec_venc,");
		strSQL.append(" TO_CHAR (cpv.df_registro_fide, 'dd/mm/yyyy') fec_fide,");
		strSQL.append(" cpv.fg_amortizacion amort,");
		strSQL.append(" cpv.fg_interes interes,");
		strSQL.append(" cpv.fg_totalvencimiento saldo,");
		strSQL.append(" cpv.cg_estatus estatus,");
		strSQL.append(" cpv.cg_observaciones observaciones,");
		strSQL.append(" TO_CHAR (cpv.df_validacion , 'dd/mm/yyyy') fec_val,");
		strSQL.append(" cpv.cg_usuario usuario,");
		strSQL.append(" cpv.ig_prestamo prestamo,");
		strSQL.append(" cpv.ig_cliente num_cliente,");  
		strSQL.append(" TO_CHAR (cpv.DF_PAGO_CLIENTE,'dd/mm/yyyy') fec_cliente,");
		strSQL.append(" cpv.ig_disposicion num_cuota,");
		strSQL.append(" TO_NUMBER (NULL) monto_reembolso,");
		strSQL.append(" cpv.ic_programa_fondojr");
		if(hayDiasDeVencimientoEspecificados){ // F012-2011
			strSQL.append(",		( ");
			strSQL.append("      	SELECT ");
			strSQL.append("         	DECODE(  ");
			strSQL.append("					cpv.cg_estatus,  ");
			strSQL.append("					'NP', ");
			strSQL.append("					DECODE(   ");
			strSQL.append("						( ");
			strSQL.append("							SELECT /*+ index(cpv2 CP_COM_PAGOS_FIDE_VAL_PK)*/ ");
			strSQL.append("								COUNT(1) ");
			strSQL.append("							FROM    ");
			strSQL.append("								COM_PAGOS_FIDE_VAL CPV2 ");
			strSQL.append("							WHERE   ");
			strSQL.append("								CPV2.COM_FECHAPROBABLEPAGO     = cpv.com_fechaprobablepago ");
			strSQL.append("								AND CPV2.IG_PRESTAMO           = cpv.ig_prestamo ");
			strSQL.append("								AND CPV2.IG_DISPOSICION        = cpv.ig_disposicion ");
			strSQL.append("								AND CPV2.FG_TOTALVENCIMIENTO   = cpv.fg_totalvencimiento  ");
			strSQL.append("								AND cpv2.ic_programa_fondojr = cpv.ic_programa_fondojr ");
		
		if(hayDiasDeVencimientoEspecificados){ //Fodea 027-2011 
			strSQL.append("								AND CPV2.CG_ESTATUS IN ('RC','RF') ");
		}else{
			strSQL.append("								AND CPV2.CG_ESTATUS IN ('R','RC','RF') ");
		}
			strSQL.append("						), ");
			strSQL.append("						0, ");
			strSQL.append("						( ");
			strSQL.append("							CASE  WHEN  ");
			strSQL.append("								cpv.com_fechaprobablepago < trunc(sysdate+1)  ");
			strSQL.append("							then  ");
			strSQL.append("								(trunc(sysdate) - cpv.com_fechaprobablepago)  ");
			strSQL.append("							else  ");
			strSQL.append("								cpv.com_fechaprobablepago-cpv.com_fechaprobablepago  ");
			strSQL.append("							end ");
			strSQL.append("						) ");
			strSQL.append("             	");
			strSQL.append("					) ");
			strSQL.append("         		,NULL ");
			strSQL.append("				)  ");
			strSQL.append("      	FROM ");
			strSQL.append("         	COM_CRED_PARAM_DIAS_VENC CCP ");
			strSQL.append("      	WHERE ");
			strSQL.append("				CCP.IC_PROGRAMA_FONDOJR 	=	CPV.IC_PROGRAMA_FONDOJR 		AND 	");
			strSQL.append("         	CPV.COM_FECHAPROBABLEPAGO 	>= TRUNC(CCP.DF_FECHA_VMTO_INI) 	AND 	");
			strSQL.append("      		CPV.COM_FECHAPROBABLEPAGO 	< 	TRUNC(CCP.DF_FECHA_VMTO_FIN+1) 		");
			strSQL.append("   	) AS DIAS_TRANSCURRIDOS_VENCIMIENTO ");
		}
		strSQL.append(" FROM com_pagos_fide_val cpv");
		strSQL.append(" WHERE cpv.cg_estatus = 'NP'");
		strSQL.append(" AND cpv.cs_validacion = 'A'");
		strSQL.append(" UNION ALL");
		strSQL.append(" SELECT /*+ index(cof cp_comcat_oper_fondo_jr_pk) index(cmv in_com_mov_gral_fdo_jr_01_nuk)*/");
		strSQL.append(" TO_CHAR (cmv.df_fec_movimiento, 'dd/mm/yyyy') AS fec_venc,");
		strSQL.append(" NULL AS fec_fide,");
		strSQL.append(" TO_NUMBER (NULL) AS amort,");
		strSQL.append(" TO_NUMBER (NULL) AS interes,");
		strSQL.append(" cmv.fg_capital AS saldo,");
		strSQL.append(" Upper(cof.cg_operacion) AS estatus,");
		strSQL.append(" cof.cg_descripcion AS observaciones,");
		strSQL.append(" NULL AS fec_val,");
		strSQL.append(" cmv.cg_nousuario AS usuario,");
		strSQL.append(" TO_NUMBER (NULL) AS prestamo,");
		strSQL.append(" TO_NUMBER (NULL) AS num_cliente,");
		strSQL.append(" NULL AS fec_cliente,");
		strSQL.append(" TO_NUMBER (NULL) AS num_cuota,");
		strSQL.append(" TO_NUMBER (NULL) monto_reembolso,");
		strSQL.append(" cof.ic_programa_fondojr");
		if(hayDiasDeVencimientoEspecificados){ // F012-2011
			strSQL.append(", TO_NUMBER(NULL) as dias_transcurridos_vencimiento "); 
		}
		strSQL.append(" FROM comcat_oper_fondo_jr cof, com_mov_gral_fondo_jr cmv");
		strSQL.append(" WHERE cof.ig_oper_fondo_jr = cmv.ig_oper_fondo_jr");
		strSQL.append(" AND cof.ic_programa_fondojr = cmv.ic_programa_fondojr");
		strSQL.append(" AND UPPER (cof.cg_descripcion) != 'SALDO INICIAL'");
		strSQL.append(" ) tablota");

		strSQL.append(" WHERE tablota.ic_programa_fondojr = ? ");
		conditions.add(new Integer(clavePrograma));
		conditions.add(new Integer(clavePrograma));
		
		if (fecVencIni!=null && !fecVencIni.equals("")) {
			strSQL.append(" AND TO_DATE(tablota.fec_venc,'dd/mm/yyyy') >= TRUNC(TO_DATE(?,'dd/mm/yyyy'))");
			conditions.add(fecVencIni);
		}
		if (fecVencFin!=null && !fecVencFin.equals("")) {
			strSQL.append(" AND TO_DATE(tablota.fec_venc,'dd/mm/yyyy') < TRUNC(TO_DATE(?,'dd/mm/yyyy') + 1)");
			conditions.add(fecVencFin);
		}
		if (fecOperIni!=null && !fecOperIni.equals("")) {
			strSQL.append(" AND TO_DATE(tablota.fec_val,'dd/mm/yyyy') >= TRUNC(TO_DATE(?,'dd/mm/yyyy'))");
			conditions.add(fecOperIni);
		}
		if (fecOperFin!=null && !fecOperFin.equals("")) {
			strSQL.append(" AND TO_DATE(tablota.fec_val,'dd/mm/yyyy') < TRUNC(TO_DATE(?,'dd/mm/yyyy') + 1)");
			conditions.add(fecOperFin);
		}
		if (fecFideIni!=null && !fecFideIni.equals("")) {
			strSQL.append(" AND TO_DATE(tablota.fec_fide,'dd/mm/yyyy') >= TRUNC(TO_DATE(?,'dd/mm/yyyy'))");
			conditions.add(fecFideIni);
		}
		if (fecFideFin!=null && !fecFideFin.equals("")) {
			strSQL.append(" AND TO_DATE(tablota.fec_fide,'dd/mm/yyyy') < TRUNC(TO_DATE(?,'dd/mm/yyyy') + 1)");
			conditions.add(fecFideIni);
		}
		
		if(hayDiasDeVencimientoEspecificados){ // F012-2011
			strSQL.append(" AND	tablota.DIAS_TRANSCURRIDOS_VENCIMIENTO >= ? AND tablota.DIAS_TRANSCURRIDOS_VENCIMIENTO <= ? "); 
			conditions.add(new Integer(diasDesde));
			conditions.add(new Integer(diasHasta));
		}
		
		strSQL.append(" ) tab_temp");
		strSQL.append(" GROUP BY estatus) tab_tmp) tab_tmp1 ");
		

		log.debug("..:: strSQL: " + strSQL.toString());
		log.debug("..:: conditions: " + conditions);

		log.info("getAggregateCalculationQuery(S) ::..");
		System.err.println("strSQL(getAggregateCalculationQuery) = <"+strSQL.toString()+">");// Debug info
		return strSQL.toString();
	}//getAggregateCalculationQuery
	
	/**
	* Obtiene el query para obtener las llaves primarias de la consulta
	* @return Cadena con la consulta de SQL, para obtener llaves primarias
	*/
	public String getDocumentQuery(){
		log.info("getDocumentQuery(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		
		// F012 - 2011
		boolean hayDiasDeVencimientoEspecificados = false;
		if( (diasDesde != null && !diasDesde.equals("")) || (diasHasta != null && !diasHasta.equals("")) ){
			hayDiasDeVencimientoEspecificados = true;
		}
 
		strSQL.append(" SELECT ROWNUM ic_registro_tablota");
		strSQL.append(" FROM (");
		//ESTA SECCION DEL QUERY OBTIENE EL SALDO INICAL DEL FONDO
		//strSQL.append(" SELECT NULL fec_venc,");
		strSQL.append(" SELECT '" + this.getFecVencIni() + "' fec_venc,");
		strSQL.append(" NULL fec_fide,");
		strSQL.append(" TO_NUMBER(NULL) amort,");
		strSQL.append(" TO_NUMBER(NULL) interes,");
		strSQL.append(" cmv.fg_capital saldo,");
		strSQL.append(" DECODE (cof.cg_operacion, 'Suma', 'INICIAL', 'INICIAL') estatus,");
		strSQL.append(" cof.cg_descripcion observaciones,");
		strSQL.append(" NULL fec_val,");
		strSQL.append(" cmv.cg_nousuario usuario,");
		strSQL.append(" TO_NUMBER(NULL) prestamo,");
		strSQL.append(" TO_NUMBER(NULL) num_cliente,");
		strSQL.append(" NULL fec_cliente,");
		strSQL.append(" TO_NUMBER(NULL) num_cuota,");
		strSQL.append(" TO_NUMBER(NULL) monto_reembolso,");
		strSQL.append(" cof.ic_programa_fondojr ");
		if(hayDiasDeVencimientoEspecificados){ // F012-2011
			strSQL.append(", TO_NUMBER(NULL) as dias_transcurridos_vencimiento "); 
		}
		strSQL.append(" FROM comcat_oper_fondo_jr cof, com_mov_gral_fondo_jr cmv");
		strSQL.append(" WHERE cof.ig_oper_fondo_jr = cmv.ig_oper_fondo_jr");
		strSQL.append(" AND UPPER (cof.cg_descripcion) = 'SALDO INICIAL'");
		strSQL.append(" AND cof.ic_programa_fondojr = cmv.ic_programa_fondojr");
		strSQL.append(" AND cof.ic_programa_fondojr = ?");
		strSQL.append(" AND ROWNUM = 1");
		//A PARTIR DE AQUI ES LA CONSULTA NORMAL
		if(!hayDiasDeVencimientoEspecificados){ // F012-2011
			strSQL.append(" UNION ALL");
			strSQL.append(" SELECT /*+index(cpv in_com_pagos_fide_val_01_nuk)*/");
			strSQL.append(" TO_CHAR (cpv.com_fechaprobablepago, 'dd/mm/yyyy') fec_venc,");
			strSQL.append(" TO_CHAR (cpv.df_registro_fide, 'dd/mm/yyyy') fec_fide,");
			strSQL.append(" cpv.fg_amortizacion amort,");
			strSQL.append(" cpv.fg_interes interes,");
			strSQL.append(" cpv.fg_totalvencimiento saldo,");
			strSQL.append(" cpv.cg_estatus estatus,");
			strSQL.append(" cpv.cg_observaciones observaciones,");
			strSQL.append(" TO_CHAR (cpv.df_validacion , 'dd/mm/yyyy') fec_val,");
			strSQL.append(" cpv.cg_usuario usuario,");
			strSQL.append(" cpv.ig_prestamo prestamo,");
			strSQL.append(" cpv.IG_CLIENTE num_cliente,");
			strSQL.append(" TO_CHAR (cpv.DF_PAGO_CLIENTE,'dd/mm/yyyy') fec_cliente,");
			strSQL.append(" cpv.IG_DISPOSICION num_cuota,");
			strSQL.append(" TO_NUMBER (NULL) monto_reembolso,");
			strSQL.append(" cpv.ic_programa_fondojr ");
			if(hayDiasDeVencimientoEspecificados){ // F012-2011
				strSQL.append(", TO_NUMBER(NULL) as dias_transcurridos_vencimiento ");
			}
			strSQL.append(" FROM com_pagos_fide_val cpv");
			
			if(hayDiasDeVencimientoEspecificados){ // F012-2011				
				strSQL.append(" WHERE (cpv.cg_estatus = 'P' OR cpv.cg_estatus = 'T' OR cpv.cg_estatus = 'RC')");
			}else{
			strSQL.append(" WHERE (cpv.cg_estatus = 'P' OR cpv.cg_estatus = 'T' OR cpv.cg_estatus = 'RC' OR cpv.cg_estatus = 'R')");
				
			}
			strSQL.append(" AND cpv.cs_validacion = 'A'");
			
			strSQL.append(" UNION ALL");
			strSQL.append(" SELECT /*+ use_nl(cpv rxf) index(cpv in_com_pagos_fide_val_01_nuk) index(rxf cp_com_reemb_finiquito_nuk)*/");
			strSQL.append(" DISTINCT TO_CHAR(rxf.df_reembolso, 'dd/mm/yyyy') fec_venc,");
			strSQL.append(" NULL fec_fide,");
			strSQL.append(" SUM(cpv.fg_amortizacion) amort,");
			strSQL.append(" SUM(cpv.fg_interes) interes,");
			strSQL.append(" SUM(cpv.fg_totalvencimiento) saldo,");
			strSQL.append(" cpv.cg_estatus estatus,");
			strSQL.append(" cpv.cg_observaciones observaciones,");
			strSQL.append(" NULL fec_val,");
			strSQL.append(" cpv.cg_usuario usuario,");
			strSQL.append(" cpv.ig_prestamo prestamo, cpv.ig_cliente num_cliente,");
			strSQL.append(" NULL fec_cliente,");
			strSQL.append(" TO_NUMBER (NULL) num_cuota,");
			strSQL.append(" rxf.fn_monto_reembolso monto_reembolso,");
			strSQL.append(" cpv.ic_programa_fondojr ");
			if(hayDiasDeVencimientoEspecificados){ // F012-2011
				strSQL.append(", TO_NUMBER(NULL) as dias_transcurridos_vencimiento ");
			}
			strSQL.append(" FROM com_pagos_fide_val cpv, com_reembolsos_finiquito rxf");
			strSQL.append(" WHERE cpv.ig_prestamo = rxf.ig_prestamo");
			strSQL.append(" AND cpv.cg_estatus = 'RF'");
			strSQL.append(" AND cpv.cs_validacion = 'A'");  
			strSQL.append(" AND NOT EXISTS (");
			strSQL.append(" SELECT 1");
			strSQL.append(" FROM com_pagos_fide_val");
			strSQL.append(" WHERE cg_estatus = 'RC'");
			strSQL.append(" AND com_fechaprobablepago = cpv.com_fechaprobablepago");
			strSQL.append(" AND ig_prestamo = cpv.ig_prestamo");
			strSQL.append(" AND ig_cliente = cpv.ig_cliente");
			strSQL.append(" AND ig_disposicion = cpv.ig_disposicion");
			strSQL.append(" AND fg_totalvencimiento = cpv.fg_totalvencimiento)");
			strSQL.append(" GROUP BY rxf.df_reembolso,");
			strSQL.append(" rxf.fn_monto_reembolso,");
			strSQL.append(" cpv.cg_estatus,");
			strSQL.append(" cpv.cg_usuario,");
			strSQL.append(" cpv.ig_prestamo,");
			strSQL.append(" cpv.ig_cliente,");
			strSQL.append(" cpv.cg_observaciones,");
			strSQL.append(" cpv.ic_programa_fondojr");
		}
		
		strSQL.append(" UNION ALL");
		strSQL.append(" SELECT /*+ index(cpv in_com_pagos_fide_val_01_nuk)*/");
		strSQL.append(" TO_CHAR (cpv.com_fechaprobablepago, 'dd/mm/yyyy') fec_venc,");
		strSQL.append(" TO_CHAR (cpv.df_registro_fide, 'dd/mm/yyyy') fec_fide,");
		strSQL.append(" cpv.fg_amortizacion amort,");
		strSQL.append(" cpv.fg_interes interes,");
		strSQL.append(" cpv.fg_totalvencimiento saldo,");
		strSQL.append(" cpv.cg_estatus estatus,");
		strSQL.append(" cpv.cg_observaciones observaciones,");
		strSQL.append(" TO_CHAR (cpv.df_validacion , 'dd/mm/yyyy') fec_val,");
		strSQL.append(" cpv.cg_usuario usuario,");
		strSQL.append(" cpv.ig_prestamo prestamo,");
		strSQL.append(" cpv.ig_cliente num_cliente,");  
		strSQL.append(" TO_CHAR (cpv.DF_PAGO_CLIENTE,'dd/mm/yyyy') fec_cliente,");
		strSQL.append(" cpv.ig_disposicion num_cuota,");
		strSQL.append(" TO_NUMBER (NULL) monto_reembolso,");
		strSQL.append(" cpv.ic_programa_fondojr ");
		if(hayDiasDeVencimientoEspecificados){ // F012-2011
			strSQL.append(",		( ");
			strSQL.append("      	SELECT ");
			strSQL.append("         	DECODE(  ");
			strSQL.append("					cpv.cg_estatus,  ");
			strSQL.append("					'NP', ");
			strSQL.append("					DECODE(   ");
			strSQL.append("						( ");
			strSQL.append("							SELECT /*+ index(cpv2 CP_COM_PAGOS_FIDE_VAL_PK)*/ ");
			strSQL.append("								COUNT(1) ");
			strSQL.append("							FROM    ");
			strSQL.append("								COM_PAGOS_FIDE_VAL CPV2 ");
			strSQL.append("							WHERE   ");
			strSQL.append("								CPV2.COM_FECHAPROBABLEPAGO     = cpv.com_fechaprobablepago ");
			strSQL.append("								AND CPV2.IG_PRESTAMO           = cpv.ig_prestamo ");
			strSQL.append("								AND CPV2.IG_DISPOSICION        = cpv.ig_disposicion ");
			strSQL.append("								AND CPV2.FG_TOTALVENCIMIENTO   = cpv.fg_totalvencimiento  ");
			strSQL.append("								AND cpv2.ic_programa_fondojr = cpv.ic_programa_fondojr ");
			
			if(hayDiasDeVencimientoEspecificados){ // F012-2011			
				strSQL.append("								AND CPV2.CG_ESTATUS IN ('RC','RF') ");
			}else{
				strSQL.append("								AND CPV2.CG_ESTATUS IN ('R','RC','RF') ");				
			}
			strSQL.append("						), ");
			strSQL.append("						0, ");
			strSQL.append("						( ");
			strSQL.append("							CASE  WHEN  ");
			strSQL.append("								cpv.com_fechaprobablepago < trunc(sysdate+1)  ");
			strSQL.append("							then  ");
			strSQL.append("								(trunc(sysdate) - cpv.com_fechaprobablepago)  ");
			strSQL.append("							else  ");
			strSQL.append("								cpv.com_fechaprobablepago-cpv.com_fechaprobablepago  ");
			strSQL.append("							end ");
			strSQL.append("						) ");
			strSQL.append("             	");
			strSQL.append("					) ");
			strSQL.append("         		,NULL ");
			strSQL.append("				)  ");
			strSQL.append("      	FROM ");
			strSQL.append("         	COM_CRED_PARAM_DIAS_VENC CCP ");
			strSQL.append("      	WHERE ");
			strSQL.append("				CCP.IC_PROGRAMA_FONDOJR 	=	CPV.IC_PROGRAMA_FONDOJR 		AND 	");
			strSQL.append("         	CPV.COM_FECHAPROBABLEPAGO 	>= TRUNC(CCP.DF_FECHA_VMTO_INI) 	AND 	");
			strSQL.append("      		CPV.COM_FECHAPROBABLEPAGO 	< 	TRUNC(CCP.DF_FECHA_VMTO_FIN+1) 		");
			strSQL.append("   	) AS DIAS_TRANSCURRIDOS_VENCIMIENTO ");
		}
		strSQL.append(" FROM com_pagos_fide_val cpv");
		strSQL.append(" WHERE cpv.cg_estatus = 'NP'");
		strSQL.append(" AND cpv.cs_validacion = 'A'");
		
		if(!hayDiasDeVencimientoEspecificados){ // F012-2011
			strSQL.append(" UNION ALL");
			strSQL.append(" SELECT /*+ index(cof cp_comcat_oper_fondo_jr_pk) index(cmv in_com_mov_gral_fdo_jr_01_nuk)*/");
			strSQL.append(" TO_CHAR (cmv.df_fec_movimiento, 'dd/mm/yyyy') AS fec_venc,");
			strSQL.append(" NULL AS fec_fide,");
			strSQL.append(" TO_NUMBER (NULL) AS amort,");
			strSQL.append(" TO_NUMBER (NULL) AS interes,");
			strSQL.append(" cmv.fg_capital AS saldo,");
			strSQL.append(" cof.cg_operacion AS estatus,");
			strSQL.append(" cof.cg_descripcion AS observaciones,");
			strSQL.append(" NULL AS fec_val,");
			strSQL.append(" cmv.cg_nousuario AS usuario,");
			strSQL.append(" TO_NUMBER (NULL) AS prestamo,");
			strSQL.append(" TO_NUMBER (NULL) AS num_cliente,");
			strSQL.append(" NULL AS fec_cliente,");
			strSQL.append(" TO_NUMBER (NULL) AS num_cuota,");
			strSQL.append(" TO_NUMBER (NULL) monto_reembolso,");
			strSQL.append(" cof.ic_programa_fondojr ");
			if(hayDiasDeVencimientoEspecificados){ // F012-2011
				strSQL.append(", TO_NUMBER(NULL) as dias_transcurridos_vencimiento "); 
			}
			strSQL.append(" FROM comcat_oper_fondo_jr cof, com_mov_gral_fondo_jr cmv");
			strSQL.append(" WHERE cof.ig_oper_fondo_jr = cmv.ig_oper_fondo_jr");
			strSQL.append(" AND cof.ic_programa_fondojr = cmv.ic_programa_fondojr");
			strSQL.append(" AND UPPER (cof.cg_descripcion) != 'SALDO INICIAL'");
		}
		strSQL.append(" ) tablota");
		strSQL.append(" WHERE tablota.ic_programa_fondojr = ? ");
		conditions.add(new Integer(clavePrograma));
		conditions.add(new Integer(clavePrograma));
			
		if (fecVencIni!=null && !fecVencIni.equals("")) {
			strSQL.append(" AND TO_DATE(tablota.fec_venc,'dd/mm/yyyy') >= TRUNC(TO_DATE(?,'dd/mm/yyyy'))");
			conditions.add(fecVencIni);
		}
		if (fecVencFin!=null && !fecVencFin.equals("")) {
			strSQL.append(" AND TO_DATE(tablota.fec_venc,'dd/mm/yyyy') < TRUNC(TO_DATE(?,'dd/mm/yyyy') + 1)");
			conditions.add(fecVencFin);
		}
		if (fecOperIni!=null && !fecOperIni.equals("")) {
			strSQL.append(" AND TO_DATE(tablota.fec_val,'dd/mm/yyyy') >= TRUNC(TO_DATE(?,'dd/mm/yyyy'))");
			conditions.add(fecOperIni);
		}
		if (fecOperFin!=null && !fecOperFin.equals("")) {
			strSQL.append(" AND TO_DATE(tablota.fec_val,'dd/mm/yyyy') < TRUNC(TO_DATE(?,'dd/mm/yyyy') + 1)");
			conditions.add(fecOperFin);
		}
		if (fecFideIni!=null && !fecFideIni.equals("")) {
			strSQL.append(" AND TO_DATE(tablota.fec_fide,'dd/mm/yyyy') >= TRUNC(TO_DATE(?,'dd/mm/yyyy'))");
			conditions.add(fecFideIni);
		}
		if (fecFideFin!=null && !fecFideFin.equals("")) {
			strSQL.append(" AND TO_DATE(tablota.fec_fide,'dd/mm/yyyy') < TRUNC(TO_DATE(?,'dd/mm/yyyy') + 1)");
			conditions.add(fecFideIni);
		}
			
		if(hayDiasDeVencimientoEspecificados){ // F012-2011
			strSQL.append(" AND	tablota.DIAS_TRANSCURRIDOS_VENCIMIENTO >= ? AND tablota.DIAS_TRANSCURRIDOS_VENCIMIENTO <= ? "); 
			conditions.add(new Integer(diasDesde));
			conditions.add(new Integer(diasHasta));
		}
		
		log.debug("..:: strSQL: " + strSQL.toString());
		log.debug("..:: conditions: " + conditions);
		
		log.info("getDocumentQuery(S) ::..");
		System.err.println("strSQL(getDocumentQuery) = <"+strSQL.toString()+">");// Debug info
		return strSQL.toString();
	}//getDocumentQuery
	
	
	/***MIGRACION IF ***/
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}

	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		return "";
	}
	
	/**
	* Obtiene el query necesario para mostrar la información completa de 
	* una página a partir de las llaves primarias enviadas como parámetro
	* @return Cadena con la consulta de SQL, para obtener la información 
	* completa de los registros con las llaves especificadas
	*/
	public String getDocumentSummaryQueryForIds(List pageIds){
		log.info("getDocumentSummaryQueryForIds(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		
		// F012 - 2011
		boolean hayDiasDeVencimientoEspecificados = false;
		if( (diasDesde != null && !diasDesde.equals("")) || (diasHasta != null && !diasHasta.equals("")) ){
			hayDiasDeVencimientoEspecificados = true;
		}
		
		strSQL.append(" SELECT ic_registro_tablota,");
		strSQL.append(" fec_venc fec_venc,");
		strSQL.append(" fec_fide fec_fide,");
		strSQL.append(" amort amort,");
		strSQL.append(" interes interes,");
		strSQL.append(" saldo saldo,");
		strSQL.append(" estatus estatus,");
		strSQL.append(" observaciones observaciones,");
		strSQL.append(" fec_val fec_val,");
		strSQL.append(" usuario usuario,");
		strSQL.append(" prestamo prestamo,");
		strSQL.append(" num_cliente num_cliente,");
		strSQL.append(" fec_cliente fec_cliente,");
		strSQL.append(" num_cuota num_cuota,");
		strSQL.append(" monto_reembolso,");
		strSQL.append(" ic_programa_fondojr");
		strSQL.append(", dias_transcurridos_vencimiento "); 	// F012-2011 
		strSQL.append(", dias_limite "); 							// F012-2011
		strSQL.append(" FROM (");
		strSQL.append(" SELECT ROWNUM ic_registro_tablota,");
		strSQL.append(" tablota.fec_venc fec_venc,");
		strSQL.append(" tablota.fec_fide fec_fide,");
		strSQL.append(" tablota.amort amort,");
		strSQL.append(" tablota.interes interes,");
		strSQL.append(" tablota.saldo saldo,");
		strSQL.append(" tablota.estatus estatus,");
		strSQL.append(" tablota.observaciones observaciones,");
		strSQL.append(" tablota.fec_val fec_val,");
		strSQL.append(" tablota.usuario usuario,");
		strSQL.append(" tablota.prestamo prestamo,");
		strSQL.append(" tablota.num_cliente num_cliente,");
		strSQL.append(" tablota.fec_cliente fec_cliente,");
		strSQL.append(" tablota.num_cuota num_cuota,");
		strSQL.append(" tablota.monto_reembolso,");
		strSQL.append(" tablota.ic_programa_fondojr");
		strSQL.append(", tablota.dias_transcurridos_vencimiento "); // F012-2011
		strSQL.append(", tablota.dias_limite ");							// F012-2011
		strSQL.append(" FROM (");
		//ESTA SECCION DEL QUERY OBTIENE EL SALDO INICAL DEL FONDO
		//strSQL.append(" SELECT NULL fec_venc,");
		strSQL.append(" SELECT '" + this.getFecVencIni() + "' fec_venc,");
		strSQL.append(" NULL fec_fide,");
		strSQL.append(" TO_NUMBER(NULL) amort,");
		strSQL.append(" TO_NUMBER(NULL) interes,");
		strSQL.append(" cmv.fg_capital saldo,");
		strSQL.append(" DECODE (cof.cg_operacion, 'Suma', 'INICIAL', 'INICIAL') estatus,");
		strSQL.append(" cof.cg_descripcion observaciones,");
		strSQL.append(" NULL fec_val,");
		strSQL.append(" cmv.cg_nousuario usuario,");
		strSQL.append(" TO_NUMBER(NULL) prestamo,");
		strSQL.append(" TO_NUMBER(NULL) num_cliente,");
		strSQL.append(" NULL fec_cliente,");
		strSQL.append(" TO_NUMBER(NULL) num_cuota,");
		strSQL.append(" TO_NUMBER(NULL) monto_reembolso,");
		strSQL.append(" cof.ic_programa_fondojr");
		strSQL.append(", TO_NUMBER(NULL) as dias_transcurridos_vencimiento "); // F012-2011
		strSQL.append(", TO_NUMBER(NULL) as dias_limite "); // F012-2011
		strSQL.append(" FROM comcat_oper_fondo_jr cof, com_mov_gral_fondo_jr cmv");
		strSQL.append(" WHERE cof.ig_oper_fondo_jr = cmv.ig_oper_fondo_jr");
		strSQL.append(" AND UPPER (cof.cg_descripcion) = 'SALDO INICIAL'");
		strSQL.append(" AND cof.ic_programa_fondojr = cmv.ic_programa_fondojr");
		strSQL.append(" AND cof.ic_programa_fondojr = ?");
		strSQL.append(" AND ROWNUM = 1");
		//A PARTIR DE AQUI ES LA CONSULTA NORMAL
		strSQL.append(" UNION ALL");
		strSQL.append(" SELECT /*+index(cpv in_com_pagos_fide_val_01_nuk)*/");
		strSQL.append(" TO_CHAR (cpv.com_fechaprobablepago, 'dd/mm/yyyy') fec_venc,");
		strSQL.append(" TO_CHAR (cpv.df_registro_fide, 'dd/mm/yyyy') fec_fide,");
		strSQL.append(" cpv.fg_amortizacion amort,");
		strSQL.append(" cpv.fg_interes interes,");
		strSQL.append(" cpv.fg_totalvencimiento saldo,");
		strSQL.append(" cpv.cg_estatus estatus,");
		strSQL.append(" cpv.cg_observaciones observaciones,");
		strSQL.append(" TO_CHAR (cpv.df_validacion , 'dd/mm/yyyy') fec_val,");
		strSQL.append(" cpv.cg_usuario usuario,");
		strSQL.append(" cpv.ig_prestamo prestamo,");
		strSQL.append(" cpv.IG_CLIENTE num_cliente,");
		strSQL.append(" TO_CHAR (cpv.DF_PAGO_CLIENTE,'dd/mm/yyyy') fec_cliente,");
		strSQL.append(" cpv.IG_DISPOSICION num_cuota,");
		strSQL.append(" TO_NUMBER (NULL) monto_reembolso,");
		strSQL.append(" cpv.ic_programa_fondojr");
		strSQL.append(", TO_NUMBER(NULL) as dias_transcurridos_vencimiento "); // F012-2011
		strSQL.append(", TO_NUMBER(NULL) as dias_limite "); // F012-2011
		strSQL.append(" FROM com_pagos_fide_val cpv");
		
		if(hayDiasDeVencimientoEspecificados){ // 027-2011			
			strSQL.append(" WHERE (cpv.cg_estatus = 'P' OR cpv.cg_estatus = 'T' OR cpv.cg_estatus = 'RC')");
		}else{
			strSQL.append(" WHERE (cpv.cg_estatus = 'P' OR cpv.cg_estatus = 'T' OR cpv.cg_estatus = 'RC' OR cpv.cg_estatus = 'R')");
		}
		strSQL.append(" AND cpv.cs_validacion = 'A'");
		strSQL.append(" UNION ALL");
		strSQL.append(" SELECT /*+ use_nl(cpv rxf) index(cpv in_com_pagos_fide_val_01_nuk) index(rxf cp_com_reemb_finiquito_nuk)*/");
		strSQL.append(" DISTINCT TO_CHAR(rxf.df_reembolso, 'dd/mm/yyyy') fec_venc,");
		strSQL.append(" NULL fec_fide,");
		strSQL.append(" SUM(cpv.fg_amortizacion) amort,");
		strSQL.append(" SUM(cpv.fg_interes) interes,");
		strSQL.append(" SUM(cpv.fg_totalvencimiento) saldo,");
		strSQL.append(" cpv.cg_estatus estatus,");
		strSQL.append(" cpv.cg_observaciones observaciones,");
		strSQL.append(" NULL fec_val,");
		strSQL.append(" cpv.cg_usuario usuario,");
		strSQL.append(" cpv.ig_prestamo prestamo, cpv.ig_cliente num_cliente,");
		strSQL.append(" NULL fec_cliente,");
		strSQL.append(" TO_NUMBER (NULL) num_cuota,");
		strSQL.append(" rxf.fn_monto_reembolso monto_reembolso,");
		strSQL.append(" cpv.ic_programa_fondojr");
		strSQL.append(", TO_NUMBER(NULL) as dias_transcurridos_vencimiento "); // F012-2011
		strSQL.append(", TO_NUMBER(NULL) as dias_limite "); // F012-2011
		strSQL.append(" FROM com_pagos_fide_val cpv, com_reembolsos_finiquito rxf");
		strSQL.append(" WHERE cpv.ig_prestamo = rxf.ig_prestamo");
		strSQL.append(" AND cpv.cg_estatus = 'RF'");
		strSQL.append(" AND cpv.cs_validacion = 'A'");  
		strSQL.append(" AND NOT EXISTS (");
		strSQL.append(" SELECT 1");
		strSQL.append(" FROM com_pagos_fide_val");
		strSQL.append(" WHERE cg_estatus = 'RC'");
		strSQL.append(" AND com_fechaprobablepago = cpv.com_fechaprobablepago");
		strSQL.append(" AND ig_prestamo = cpv.ig_prestamo");
		strSQL.append(" AND ig_cliente = cpv.ig_cliente");
		strSQL.append(" AND ig_disposicion = cpv.ig_disposicion");
		strSQL.append(" AND fg_totalvencimiento = cpv.fg_totalvencimiento)");
		strSQL.append(" GROUP BY rxf.df_reembolso,");
		strSQL.append(" rxf.fn_monto_reembolso,");
		strSQL.append(" cpv.cg_estatus,");
		strSQL.append(" cpv.cg_usuario,");
		strSQL.append(" cpv.ig_prestamo,");
		strSQL.append(" cpv.ig_cliente,");
		strSQL.append(" cpv.cg_observaciones,");
		strSQL.append(" cpv.ic_programa_fondojr");
		strSQL.append(" UNION ALL");
		strSQL.append(" SELECT /*+ index(cpv in_com_pagos_fide_val_01_nuk)*/");
		strSQL.append(" TO_CHAR (cpv.com_fechaprobablepago, 'dd/mm/yyyy') fec_venc,");
		strSQL.append(" TO_CHAR (cpv.df_registro_fide, 'dd/mm/yyyy') fec_fide,");
		strSQL.append(" cpv.fg_amortizacion amort,");
		strSQL.append(" cpv.fg_interes interes,");
		strSQL.append(" cpv.fg_totalvencimiento saldo,");
		strSQL.append(" cpv.cg_estatus estatus,");
		strSQL.append(" cpv.cg_observaciones observaciones,");
		strSQL.append(" TO_CHAR (cpv.df_validacion , 'dd/mm/yyyy') fec_val,");
		strSQL.append(" cpv.cg_usuario usuario,");
		strSQL.append(" cpv.ig_prestamo prestamo,");
		strSQL.append(" cpv.ig_cliente num_cliente,");  
		strSQL.append(" TO_CHAR (cpv.DF_PAGO_CLIENTE,'dd/mm/yyyy') fec_cliente,");
		strSQL.append(" cpv.ig_disposicion num_cuota,");
		strSQL.append(" TO_NUMBER (NULL) monto_reembolso,");
		strSQL.append(" cpv.ic_programa_fondojr");
		strSQL.append(",		( "); // F012-2011
		strSQL.append("      	SELECT ");
		strSQL.append("         	DECODE(  ");
		strSQL.append("					cpv.cg_estatus,  ");
		strSQL.append("					'NP', ");
		strSQL.append("					DECODE(   ");
		strSQL.append("						( ");
		strSQL.append("							SELECT /*+ index(cpv2 CP_COM_PAGOS_FIDE_VAL_PK)*/ ");
		strSQL.append("								COUNT(1) ");
		strSQL.append("							FROM    ");
		strSQL.append("								COM_PAGOS_FIDE_VAL CPV2 ");
		strSQL.append("							WHERE   ");
		strSQL.append("								CPV2.COM_FECHAPROBABLEPAGO     = cpv.com_fechaprobablepago ");
		strSQL.append("								AND CPV2.IG_PRESTAMO           = cpv.ig_prestamo ");
		strSQL.append("								AND CPV2.IG_DISPOSICION        = cpv.ig_disposicion ");
		strSQL.append("								AND CPV2.FG_TOTALVENCIMIENTO   = cpv.fg_totalvencimiento  ");
		strSQL.append("								AND cpv2.ic_programa_fondojr = cpv.ic_programa_fondojr ");
			if(hayDiasDeVencimientoEspecificados){ // 027-2011		
				strSQL.append("								AND CPV2.CG_ESTATUS IN ('RC','RF') ");				
			}else{
				strSQL.append("								AND CPV2.CG_ESTATUS IN ('R','RC','RF') ");
			}
		strSQL.append("						), ");
		strSQL.append("						0, ");
		strSQL.append("						( ");
		strSQL.append("							CASE  WHEN  ");
		strSQL.append("								cpv.com_fechaprobablepago < trunc(sysdate+1)  ");
		strSQL.append("							then  ");
		strSQL.append("								(trunc(sysdate) - cpv.com_fechaprobablepago)  ");
		strSQL.append("							else  ");
		strSQL.append("								cpv.com_fechaprobablepago-cpv.com_fechaprobablepago  ");
		strSQL.append("							end ");
		strSQL.append("						) ");
		strSQL.append("             	");
		strSQL.append("					) ");
		strSQL.append("         		,NULL ");
		strSQL.append("				)  ");
		strSQL.append("      	FROM ");
		strSQL.append("         	COM_CRED_PARAM_DIAS_VENC CCP ");
		strSQL.append("      	WHERE ");
		strSQL.append("				CCP.IC_PROGRAMA_FONDOJR 	=	CPV.IC_PROGRAMA_FONDOJR 		AND 	");
		strSQL.append("         	CPV.COM_FECHAPROBABLEPAGO 	>= TRUNC(CCP.DF_FECHA_VMTO_INI) 	AND 	");
		strSQL.append("      		CPV.COM_FECHAPROBABLEPAGO 	< 	TRUNC(CCP.DF_FECHA_VMTO_FIN+1) 		");
		strSQL.append("   	) AS DIAS_TRANSCURRIDOS_VENCIMIENTO, ");
		strSQL.append("    	( "); // F012-2011
		strSQL.append("       	SELECT  "); 
		strSQL.append("         	CRED_PARAM.IG_DIAS_VENCIMIENTO  "); 
		strSQL.append("       	FROM  "); 
		strSQL.append("         	COM_CRED_PARAM_DIAS_VENC CRED_PARAM "); 
		strSQL.append("       	WHERE  "); 
		strSQL.append("				CRED_PARAM.IC_PROGRAMA_FONDOJR 	=	CPV.IC_PROGRAMA_FONDOJR 					AND 	");
		strSQL.append("         	CPV.COM_FECHAPROBABLEPAGO  		>= TRUNC(CRED_PARAM.DF_FECHA_VMTO_INI) 	AND  	"); 
		strSQL.append("         	CPV.COM_FECHAPROBABLEPAGO 			< 	TRUNC(CRED_PARAM.DF_FECHA_VMTO_FIN+1) 			"); 
		strSQL.append("    	) AS dias_limite ");
		strSQL.append(" FROM com_pagos_fide_val cpv");
		strSQL.append(" WHERE cpv.cg_estatus = 'NP'");
		strSQL.append(" AND cpv.cs_validacion = 'A'");
		strSQL.append(" UNION ALL");
		strSQL.append(" SELECT /*+ index(cof cp_comcat_oper_fondo_jr_pk) index(cmv in_com_mov_gral_fdo_jr_01_nuk)*/");
		strSQL.append(" TO_CHAR (cmv.df_fec_movimiento, 'dd/mm/yyyy') AS fec_venc,");
		strSQL.append(" NULL AS fec_fide,");
		strSQL.append(" TO_NUMBER (NULL) AS amort,");
		strSQL.append(" TO_NUMBER (NULL) AS interes,");
		strSQL.append(" cmv.fg_capital AS saldo,");
		strSQL.append(" cof.cg_operacion AS estatus,");
		strSQL.append(" cof.cg_descripcion AS observaciones,");
		strSQL.append(" NULL AS fec_val,");
		strSQL.append(" cmv.cg_nousuario AS usuario,");
		strSQL.append(" TO_NUMBER (NULL) AS prestamo,");
		strSQL.append(" TO_NUMBER (NULL) AS num_cliente,");
		strSQL.append(" NULL AS fec_cliente,");
		strSQL.append(" TO_NUMBER (NULL) AS num_cuota,");
		strSQL.append(" TO_NUMBER (NULL) monto_reembolso,");
		strSQL.append(" cof.ic_programa_fondojr");
		strSQL.append(", TO_NUMBER(NULL) as dias_transcurridos_vencimiento "); //F012-2011
		strSQL.append(", TO_NUMBER(NULL) as dias_limite "); // F012-2011
		strSQL.append(" FROM comcat_oper_fondo_jr cof, com_mov_gral_fondo_jr cmv");
		strSQL.append(" WHERE cof.ig_oper_fondo_jr = cmv.ig_oper_fondo_jr");
		strSQL.append(" AND cof.ic_programa_fondojr = cmv.ic_programa_fondojr");
		strSQL.append(" AND UPPER (cof.cg_descripcion) != 'SALDO INICIAL'");
		strSQL.append(" ) tablota");

		strSQL.append(" WHERE tablota.ic_programa_fondojr = ? ");
		conditions.add(new Integer(clavePrograma));
		conditions.add(new Integer(clavePrograma));
		
		if (fecVencIni!=null && !fecVencIni.equals("")) {
			strSQL.append(" AND TO_DATE(tablota.fec_venc,'dd/mm/yyyy') >= TRUNC(TO_DATE(?,'dd/mm/yyyy'))");
			conditions.add(fecVencIni);
		}
		if (fecVencFin!=null && !fecVencFin.equals("")) {
			strSQL.append(" AND TO_DATE(tablota.fec_venc,'dd/mm/yyyy') < TRUNC(TO_DATE(?,'dd/mm/yyyy') + 1)");
			conditions.add(fecVencFin);
		}
		if (fecOperIni!=null && !fecOperIni.equals("")) {
			strSQL.append(" AND TO_DATE(tablota.fec_val,'dd/mm/yyyy') >= TRUNC(TO_DATE(?,'dd/mm/yyyy'))");
			conditions.add(fecOperIni);
		}
		if (fecOperFin!=null && !fecOperFin.equals("")) {
			strSQL.append(" AND TO_DATE(tablota.fec_val,'dd/mm/yyyy') < TRUNC(TO_DATE(?,'dd/mm/yyyy') + 1)");
			conditions.add(fecOperFin);
		}
		if (fecFideIni!=null && !fecFideIni.equals("")) {
			strSQL.append(" AND TO_DATE(tablota.fec_fide,'dd/mm/yyyy') >= TRUNC(TO_DATE(?,'dd/mm/yyyy'))");
			conditions.add(fecFideIni);
		}
		if (fecFideFin!=null && !fecFideFin.equals("")) {
			strSQL.append(" AND TO_DATE(tablota.fec_fide,'dd/mm/yyyy') < TRUNC(TO_DATE(?,'dd/mm/yyyy') + 1)");
			conditions.add(fecFideIni);
		}
		
		if(hayDiasDeVencimientoEspecificados){ // F012-2011
			strSQL.append(" AND	tablota.dias_transcurridos_vencimiento >= ? AND tablota.dias_transcurridos_vencimiento <= ? "); 
			conditions.add(new Integer(diasDesde));
			conditions.add(new Integer(diasHasta));
		}
		
		strSQL.append(" ) tab_temp");
		strSQL.append(" WHERE ic_programa_fondojr = ? ");
		conditions.add(new Integer(clavePrograma));
		
		strSQL.append(" AND (");
		
		for(int i = 0; i < pageIds.size(); i++){
      List lItem = (ArrayList)pageIds.get(i);
			
      if(i > 0){strSQL.append("  OR  ");}
      
			strSQL.append("ic_registro_tablota = ?");
			conditions.add(new Long(lItem.get(0).toString()));
		}
    strSQL.append(" ) ");

		strSQL.append(" ORDER BY fec_venc ");
		
		log.debug("..:: strSQL: " + strSQL.toString());
		log.debug("..:: conditions: " + conditions);
		
		log.info("getDocumentSummaryQueryForIds(S)");
		System.err.println("strSQL(getDocumentSummaryQueryForIds) = <"+strSQL.toString()+">");// Debug info
		return strSQL.toString();
	}//getDocumentSummaryQueryForIds	
	
	public String getDocumentQueryFile(){
		log.info("getDocumentQueryFile(E)");
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		log.info("getDocumentQueryFile(S)");
		return strSQL.toString();
	}//getDocumentQueryFile
	
	//GETTERS
  /**
	 * Obtiene la lista de parámetros a usar como variables bind en las condiciones de la consulta
	 * @return Lista con los parametros de las condiciones
	 */
	public List getConditions(){return conditions;}
  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() {return paginaNo;}
	
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset(){return paginaOffset;}
	
	//Parametros del formulario
	public String getStrDirectorioPublicacion() {return strDirectorioPublicacion;}
	public String getClavePrograma() {return clavePrograma;}
	public String getFecVencIni() {return fecVencIni;}
	public String getFecVencFin() {return fecVencFin;}
	public String getFecOperIni() {return fecOperIni;}
	public String getFecOperFin() {return fecOperFin;}
	public String getFecFideIni() {return fecFideIni;}
	public String getFecFideFin() {return fecFideFin;}
	public String getDiasDesde()	{return diasDesde; }
	public String getDiasHasta()	{return diasHasta; }
	
	//SETTERS
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo){paginaNo = newPaginaNo;}
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset){this.paginaOffset=paginaOffset;}
		
	//Parametros del formulario
	public void setStrDirectorioPublicacion(String strDirectorioPublicacion) {this.strDirectorioPublicacion = strDirectorioPublicacion;}
	public void setClavePrograma(String clavePrograma) {this.clavePrograma = clavePrograma;}
	public void setFecVencIni(String fecVencIni) {this.fecVencIni = fecVencIni;}
	public void setFecVencFin(String fecVencFin) {this.fecVencFin = fecVencFin;}
	public void setFecOperIni(String fecOperIni) {this.fecOperIni = fecOperIni;}
	public void setFecOperFin(String fecOperFin) {this.fecOperFin = fecOperFin;}
	public void setFecFideIni(String fecFideIni) {this.fecFideIni = fecFideIni;}
	public void setFecFideFin(String fecFideFin) {this.fecFideFin = fecFideFin;}
	public void setDiasDesde(String diasDesde)	{ this.diasDesde = diasDesde; }
	public void setDiasHasta(String diasHasta)	{ this.diasHasta = diasHasta;	}
	
}