package com.netro.fondosjr;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;

public class InfCatOperaciones implements IQueryGeneratorReg, IQueryGeneratorRegExtJS  {
	public InfCatOperaciones()
	{
	}
	
	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer 		qrySentencia;
	private List 		conditions;
	private String 	paginaOffset;
	private String 	paginaNo;
	
	

	private String nombre;
	private String tipoOperacion;
  public String clavePrograma;



	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */

	public String getAggregateCalculationQuery() {
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();		
   
	  System.out.println("clavePrograma"+clavePrograma);
    
		qrySentencia.append(	" SELECT ig_oper_fondo_jr NUMERO, "+
									" cg_descripcion DESCRIPCION, " +
									" cg_operacion OPERACION " +
									" FROM comcat_oper_fondo_jr ");
		
		
		  if(!"".equals(clavePrograma) && clavePrograma != null ) 	{					
				qrySentencia.append(" where IC_PROGRAMA_FONDOJR = ? ");
				conditions.add(clavePrograma);
			}
      
    /*
		 if((!"".equals(tipoOperacion) && tipoOperacion!=null))
		{
			qrySentencia.append(" Where  IG_OPER_FONDO_JR =  ? ") ;
			conditions.add(tipoOperacion);
			}
			
		if((!"".equals(nombre) && nombre!=null))
			{
				//qrySentencia.append(" and  cg_descripcion =  ? ") ;
				qrySentencia.append("	Where cg_descripcion LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') ");
				conditions.add(nombre);
			}	
		*/
		System.out.println("getAggregateCalculationQuery "+conditions.toString());
		System.out.println("getAggregateCalculationQuery "+qrySentencia.toString());
		
		return qrySentencia.toString();
		
	}
	
	
	 /**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery(){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
			
		qrySentencia.append(	" SELECT ig_oper_fondo_jr NUMERO, "+
									" cg_descripcion DESCRIPCION, " +
									" cg_operacion OPERACION " +
									" FROM comcat_oper_fondo_jr ");

			if(!"".equals(clavePrograma) && clavePrograma != null ) 	{					
				qrySentencia.append(" where IC_PROGRAMA_FONDOJR = ? ");
				conditions.add(clavePrograma);
			}
      /*
			if((!"".equals(tipoOperacion) && tipoOperacion!=null))
		{
			qrySentencia.append(" Where  IG_OPER_FONDO_JR =  ? ") ;
			conditions.add(tipoOperacion);
			}
			
		if((!"".equals(nombre) && nombre!=null))
			{
				//qrySentencia.append(" and  cg_descripcion =  ? ") ;
				qrySentencia.append("	Where cg_descripcion LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') ");
				conditions.add(nombre);
			}	
		*/
		System.out.println("getDocumentQuery "+conditions.toString());
		System.out.println("getDocumentQuery "+qrySentencia.toString());
		
			return qrySentencia.toString();
	}

	/**
	 * Obtiene el query necesario para mostrar la información completa de 
	 * una página a partir de las llaves primarias enviadas como parámetro
	 * @return Cadena con la consulta de SQL, para obtener la información
	 * 	completa de los registros con las llaves especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
			
		
		qrySentencia.append(	" SELECT ig_oper_fondo_jr NUMERO, "+
									" cg_descripcion DESCRIPCION, " +
									" cg_operacion OPERACION " +
									" FROM comcat_oper_fondo_jr ");
									
		  if(!"".equals(clavePrograma) && clavePrograma != null ) 	{					
				qrySentencia.append(" where IC_PROGRAMA_FONDOJR = ? ");
				conditions.add(clavePrograma);
			}
      
    	/*						
		if((!"".equals(tipoOperacion) && tipoOperacion!=null))
		{
			qrySentencia.append(" where  IG_OPER_FONDO_JR =  ? ") ;
			conditions.add(tipoOperacion);
			}
			
		if((!"".equals(nombre) && nombre!=null))
			{
				//qrySentencia.append(" and  cg_descripcion =  ? ") ;
				qrySentencia.append("	Where cg_descripcion LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') ");
				conditions.add(nombre);
			}	
		*/
		
		
		System.out.println("getDocumentSummaryQueryForIds "+conditions.toString());
		System.out.println("getDocumentSummaryQueryForIds "+qrySentencia.toString());
		
		return qrySentencia.toString();
	}
	
	
	

	public String getDocumentQueryFile(){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();

		qrySentencia.append(	" SELECT ig_oper_fondo_jr NUMERO, "+
									" cg_descripcion DESCRIPCION, " +
									" cg_operacion OPERACION " +
									" FROM comcat_oper_fondo_jr ");

		  if(!"".equals(clavePrograma) && clavePrograma != null ) 	{					
				qrySentencia.append(" where IC_PROGRAMA_FONDOJR = ? ");
				conditions.add(clavePrograma);
			}
      /*
		if((!"".equals(tipoOperacion) && tipoOperacion!=null))
		{
			qrySentencia.append(" where  IG_OPER_FONDO_JR =  ? ") ;
			conditions.add(tipoOperacion);
			}
			
		if((!"".equals(nombre) && nombre!=null))
			{
				//qrySentencia.append(" and  cg_descripcion =  ? ") ;
				qrySentencia.append("	Where cg_descripcion LIKE NVL (REPLACE (UPPER (?), '*', '%'), '%') ");
				conditions.add(nombre);
			}	
			*/
		
		System.out.println("getDocumentQueryFile "+conditions.toString());
		System.out.println("getDocumentQueryFile "+qrySentencia.toString());
		
		return qrySentencia.toString();
	}//getDocumentQueryFile
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rg, String path, String tipo) {
		String nombreArchivo = "";
		String linea = "";
		
		return nombreArchivo;
	}
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String nombreArchivo = "";
		String linea = "";
		
		return nombreArchivo;
	}

/*****************************************************
	 GETTERS
*******************************************************/
 
	/**
	  Obtiene la lista de parámetros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	

	
	//GET
public String getNombre()		          {return nombre;}
public String gettipoOperacion()			 {return tipoOperacion;}

	
	 	
	
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}


// SET	
public void setNombre(String nombre)		 { this.nombre = nombre; }
public void setTipoOperacion(String tipoOperacion)		 { this.tipoOperacion = tipoOperacion;  }

  public String getClavePrograma() {
    return clavePrograma;
  }

  public void setClavePrograma(String clavePrograma) {
    this.clavePrograma = clavePrograma;
  }


	
}