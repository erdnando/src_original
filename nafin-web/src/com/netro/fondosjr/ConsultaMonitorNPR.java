package com.netro.fondosjr;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsultaMonitorNPR implements IQueryGeneratorReg, IQueryGeneratorRegExtJS {
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaMonitorNPR.class);
	
	/**
	 * Constructor de la clase.
	 */
	public ConsultaMonitorNPR(){}
	
	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer 	strSQL;
	private List 	conditions;
	private String paginaOffset;
	private String paginaNo;
	private String strDirectorioPublicacion;
	private String cvePrograma;
	private String fec_venc_ini;
	private String fec_venc_fin;
	private String chkDesembolso;
	private String descPrograma;
		

	/**
	* Obtiene el query para obtener los totales de la consulta
	* @return Cadena con la consulta de SQL, para obtener los totales
	*/
	public String getAggregateCalculationQuery(){
		log.info("getAggregateCalculationQuery(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		log.info("getAggregateCalculationQuery(S) ::..");
		return strSQL.toString();
	}//getAggregateCalculationQuery
	
	/**
	* Obtiene el query para obtener las llaves primarias de la consulta
	* @return Cadena con la consulta de SQL, para obtener llaves primarias
	*/
	public String getDocumentQuery(){
		log.info("getDocumentQuery(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		
		strSQL.append("SELECT  tmonitor.orden as morden, " );
		strSQL.append("		tmonitor.estatus as mestatus, " );
		strSQL.append("		tmonitor.fecha_venc as mfecha_venc " );
		strSQL.append("FROM ( " );
		strSQL.append("SELECT   '1' orden, " );
		strSQL.append("		 	pf.cg_estatus estatus, " );
		strSQL.append("		 	TO_CHAR (pf.com_fechaprobablepago, 'DD/MM/YYYY') fecha_venc, " );
		strSQL.append("         TO_CHAR (MAX (pf.df_registro_fide), 'DD/MM/YYYY HH:MI:SS') ultimo_reg, " );
		strSQL.append("         COUNT (pf.cg_estatus) total_reg,  " );
		strSQL.append("         SUM (pf.fg_totalvencimiento) monto_tot, " );
		strSQL.append("         null folio, " );
		strSQL.append("         'NPR' estatus_proc, " );
		strSQL.append("         null tipo_reg, " );
		strSQL.append("         null usuario " );
		strSQL.append("    FROM com_cred_np_rechazados pf " );
		strSQL.append("   WHERE pf.cg_estatus IN ('NPR') " );
		strSQL.append("		AND pf.ic_programa_fondojr = ? ");
		if(!"S".equals(chkDesembolso))
			strSQL.append("		AND pf.cg_desembolso = ? ");
		if(fec_venc_ini!=null && !fec_venc_ini.equals("") && fec_venc_fin!=null && !fec_venc_fin.equals("")){
			strSQL.append(" AND PF.COM_FECHAPROBABLEPAGO >= TO_DATE(?,'DD/MM/YYYY') ");
			strSQL.append(" AND PF.COM_FECHAPROBABLEPAGO <= TO_DATE(?,'DD/MM/YYYY') ");
		}
		strSQL.append("GROUP BY pf.com_fechaprobablepago, pf.cg_estatus " );
		strSQL.append(") tmonitor " );
		strSQL.append("order by tmonitor.orden, tmonitor.fecha_venc ");
		
		
		conditions.add(new Long(cvePrograma));
		if(!"S".equals(chkDesembolso))
			conditions.add("N");
		if(fec_venc_ini!=null && !fec_venc_ini.equals("") && fec_venc_fin!=null && !fec_venc_fin.equals("")){
			conditions.add(fec_venc_ini);
			conditions.add(fec_venc_fin);
		}

		log.debug("..:: strSQL: " + strSQL.toString());
		log.debug("..:: conditions: " + conditions);
		
		log.info("getDocumentQuery(S) ::..");
		return strSQL.toString();
	}//getDocumentQuery
	
	/**
	* Obtiene el query necesario para mostrar la información completa de 
	* una página a partir de las llaves primarias enviadas como parámetro
	* @return Cadena con la consulta de SQL, para obtener la información 
	* completa de los registros con las llaves especificadas
	*/
	public String getDocumentSummaryQueryForIds(List pageIds){
		log.info("getDocumentSummaryQueryForIds(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		
		strSQL.append("SELECT  tmonitor.orden as morden, " );
		strSQL.append("		tmonitor.estatus as mestatus, " );
		strSQL.append("		tmonitor.fecha_venc as mfecha_venc, " );
		strSQL.append("        tmonitor.ultimo_reg as multimo_reg, " );
		strSQL.append("        tmonitor.total_reg as mtotal_reg, " );
		strSQL.append("        tmonitor.monto_tot as mmonto_tot, " );
		strSQL.append("        tmonitor.folio as mfolio, " );
		strSQL.append("        tmonitor.estatus_proc as mestatus_proc, " );
		strSQL.append("        tmonitor.tipo_reg as mtipo_reg, " );
		strSQL.append("        tmonitor.usuario as musuario " );
		strSQL.append("FROM ( " );
		strSQL.append("SELECT   '1' orden, " );
		strSQL.append("		 	pf.cg_estatus estatus, " );
		strSQL.append("		 	TO_CHAR (pf.com_fechaprobablepago, 'DD/MM/YYYY') fecha_venc, " );
		strSQL.append("         TO_CHAR (MAX (pf.df_registro_fide), 'DD/MM/YYYY HH:MI:SS') ultimo_reg, " );
		strSQL.append("         COUNT (pf.cg_estatus) total_reg,  " );
		strSQL.append("         SUM (pf.fg_totalvencimiento) monto_tot, " );
		strSQL.append("         null folio, " );
		strSQL.append("         'NPR' estatus_proc, " );
		strSQL.append("         null tipo_reg, " );
		strSQL.append("         null usuario " );
		strSQL.append("    FROM com_cred_np_rechazados pf " );
		strSQL.append("   WHERE pf.cg_estatus IN ('NPR') " );
		strSQL.append("		AND pf.ic_programa_fondojr = ? ");
		if(!"S".equals(chkDesembolso))
			strSQL.append("		AND pf.cg_desembolso = ? ");
		if(fec_venc_ini!=null && !fec_venc_ini.equals("") && fec_venc_fin!=null && !fec_venc_fin.equals("")){
			strSQL.append(" AND PF.COM_FECHAPROBABLEPAGO >= TO_DATE(?,'DD/MM/YYYY') ");
			strSQL.append(" AND PF.COM_FECHAPROBABLEPAGO <= TO_DATE(?,'DD/MM/YYYY') ");
		}
		strSQL.append("GROUP BY pf.com_fechaprobablepago, pf.cg_estatus " );
		strSQL.append(") tmonitor " );
		
		
		conditions.add(new Long(cvePrograma));
		if(!"S".equals(chkDesembolso))
			conditions.add("N");
		if(fec_venc_ini!=null && !fec_venc_ini.equals("") && fec_venc_fin!=null && !fec_venc_fin.equals("")){
			conditions.add(fec_venc_ini);
			conditions.add(fec_venc_fin);
		}

		
		
		strSQL.append(" WHERE (");
		
		for(int i = 0; i < pageIds.size(); i++){
      List lItem = (ArrayList)pageIds.get(i);
			
      if(i > 0){strSQL.append("  OR  ");}
      
			strSQL.append(" (tmonitor.orden = ? and tmonitor.estatus = ? and tmonitor.fecha_venc = ? ) ");
		
			conditions.add(new Long(lItem.get(0).toString()));
			conditions.add(lItem.get(1).toString());
			conditions.add(lItem.get(2).toString());			
		}
		strSQL.append(" ) ");

		strSQL.append("order by tmonitor.orden, tmonitor.fecha_venc ");
		
		log.debug("..:: strSQL: " + strSQL.toString());
		log.debug("..:: conditions: " + conditions);
		
		log.info("getDocumentSummaryQueryForIds(S)");
		return strSQL.toString();
	}//getDocumentSummaryQueryForIds	
	
	public String getDocumentQueryFile(){
		log.info("getDocumentQueryFile(E)");
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		
			strSQL.append("SELECT  tmonitor.orden as morden, " );
		strSQL.append("		tmonitor.estatus as mestatus, " );
		strSQL.append("		tmonitor.fecha_venc as mfecha_venc, " );
		strSQL.append("        tmonitor.ultimo_reg as multimo_reg, " );
		strSQL.append("        tmonitor.total_reg as mtotal_reg, " );
		strSQL.append("        tmonitor.monto_tot as mmonto_tot, " );
		strSQL.append("        tmonitor.folio as mfolio, " );
		strSQL.append("        tmonitor.estatus_proc as mestatus_proc, " );
		strSQL.append("        tmonitor.tipo_reg as mtipo_reg, " );
		strSQL.append("        tmonitor.usuario as musuario " );
		strSQL.append("FROM ( " );
		strSQL.append("SELECT   '1' orden, " );
		strSQL.append("		 	pf.cg_estatus estatus, " );
		strSQL.append("		 	TO_CHAR (pf.com_fechaprobablepago, 'DD/MM/YYYY') fecha_venc, " );
		strSQL.append("         TO_CHAR (MAX (pf.df_registro_fide), 'DD/MM/YYYY HH:MI:SS') ultimo_reg, " );
		strSQL.append("         COUNT (pf.cg_estatus) total_reg,  " );
		strSQL.append("         SUM (pf.fg_totalvencimiento) monto_tot, " );
		strSQL.append("         null folio, " );
		strSQL.append("         'NPR' estatus_proc, " );
		strSQL.append("         null tipo_reg, " );
		strSQL.append("         null usuario " );
		strSQL.append("    FROM com_cred_np_rechazados pf " );
		strSQL.append("   WHERE pf.cg_estatus IN ('NPR') " );
		strSQL.append("		AND pf.ic_programa_fondojr = ? ");
		if(!"S".equals(chkDesembolso))
			strSQL.append("		AND pf.cg_desembolso = ? ");
		if(fec_venc_ini!=null && !fec_venc_ini.equals("") && fec_venc_fin!=null && !fec_venc_fin.equals("")){
			strSQL.append(" AND PF.COM_FECHAPROBABLEPAGO >= TO_DATE(?,'DD/MM/YYYY') ");
			strSQL.append(" AND PF.COM_FECHAPROBABLEPAGO <= TO_DATE(?,'DD/MM/YYYY') ");
		}
		strSQL.append("GROUP BY pf.com_fechaprobablepago, pf.cg_estatus " );
		strSQL.append(") tmonitor " );
		strSQL.append("order by tmonitor.orden, tmonitor.fecha_venc ");
		
		conditions.add(new Long(cvePrograma));
		if(!"S".equals(chkDesembolso))
			conditions.add("N");
		if(fec_venc_ini!=null && !fec_venc_ini.equals("") && fec_venc_fin!=null && !fec_venc_fin.equals("")){
			conditions.add(fec_venc_ini);
			conditions.add(fec_venc_fin);
		}
				
		log.info("getDocumentQueryFile(S)");
		return strSQL.toString();
	}//getDocumentQueryFile
	
	
	/**
	 * se imprimen todos los registros de la consulta 
	 * @return 
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();  
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer(); 
		try {
		
			contenidoArchivo.append("Programa:,"+descPrograma+"\n");
			contenidoArchivo.append("Fecha de Vencimiento Nafin,");
			contenidoArchivo.append("NP Rechazados,");
			contenidoArchivo.append("Monto");
			contenidoArchivo.append("\n");
		
			while(rs.next()){
			
				String fecha_ven = (rs.getString("MFECHA_VENC")==null)?" ":rs.getString("MFECHA_VENC"); 
				String total_reg = (rs.getString("MTOTAL_REG")==null)?" ":rs.getString("MTOTAL_REG"); 
				String monto = (rs.getString("MMONTO_TOT")==null)?" ":rs.getString("MMONTO_TOT"); 
				contenidoArchivo.append(fecha_ven.replaceAll(",", "")+ ",");	
				contenidoArchivo.append(total_reg.replaceAll(",", "")+ ",");	
				contenidoArchivo.append(monto.replaceAll(",", "")+ "\n");	
			}
			creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
			nombreArchivo = creaArchivo.getNombre();
					
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		

		try {
		
		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	//GETTERS
  /**
	 * Obtiene la lista de parámetros a usar como variables bind en las condiciones de la consulta
	 * @return Lista con los parametros de las condiciones
	 */
	public List getConditions(){return conditions;}
  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() {return paginaNo;}
	
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset(){return paginaOffset;}
	
	//Parametros del formulario
	public String getStrDirectorioPublicacion() {return strDirectorioPublicacion;}
	public String getCvePrograma() {return cvePrograma;}
	public String getFec_venc_ini() {return fec_venc_ini;}
	public String getFec_venc_fin() {return fec_venc_fin;}
	
	
	//SETTERS 
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo){paginaNo = newPaginaNo;}
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset){this.paginaOffset=paginaOffset;}
		
	//Parametros del formulario
	public void setStrDirectorioPublicacion(String strDirectorioPublicacion) {this.strDirectorioPublicacion = strDirectorioPublicacion;}
	public void setCvePrograma(String cvePrograma) {this.cvePrograma = cvePrograma;}
	public void setFec_venc_ini(String fec_venc_ini) {this.fec_venc_ini = fec_venc_ini;}
	public void setFec_venc_fin(String fec_venc_fin) {this.fec_venc_fin = fec_venc_fin;}


	public void setChkDesembolso(String chkDesembolso) {
		this.chkDesembolso = chkDesembolso;
	}


	public String getChkDesembolso() {
		return chkDesembolso;
	}

	public String getDescPrograma() {
		return descPrograma;
	}

	public void setDescPrograma(String descPrograma) {
		this.descPrograma = descPrograma;
	}
	
}