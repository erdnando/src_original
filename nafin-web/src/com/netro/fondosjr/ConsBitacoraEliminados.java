package com.netro.fondosjr;

import com.netro.pdf.ComunesPDF;
import com.netro.xls.ComunesXLS;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsBitacoraEliminados implements IQueryGeneratorReg, IQueryGeneratorRegExtJS {
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ConsBitacoraEliminados.class);
	
	/**
	 * Constructor de la clase.
	 */
	public ConsBitacoraEliminados(){}
	
	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer 	strSQL;
	private List 	conditions;
	private String paginaOffset;
	private String paginaNo;
	private String strDirectorioPublicacion;
	private String cvePrograma;
	private String fec_venc_ini;
	private String fec_venc_fin;
	private String chkDesembolso;
	private String descPrograma;
		

	/**
	* Obtiene el query para obtener los totales de la consulta
	* @return Cadena con la consulta de SQL, para obtener los totales
	*/
	public String getAggregateCalculationQuery(){
		log.info("getAggregateCalculationQuery(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		log.info("getAggregateCalculationQuery(S) ::..");
		return strSQL.toString();
	}//getAggregateCalculationQuery
	
	/**
	* Obtiene el query para obtener las llaves primarias de la consulta
	* @return Cadena con la consulta de SQL, para obtener llaves primarias
	*/
	public String getDocumentQuery(){
		log.info("getDocumentQuery(E) ::..");
		String whereAnd = "";
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		
		strSQL.append(" SELECT ic_bit_monitor ");
		strSQL.append("   FROM bit_monitor_fjr_eliminados ");
		if(fec_venc_ini!=null && !fec_venc_ini.equals("")){
			whereAnd = whereAnd.equals("")?" WHERE ": " AND ";
			strSQL.append(whereAnd+" com_fechaprobablepago >= TO_DATE(?,'DD/MM/YYYY') ");
		}
		
		if(fec_venc_fin!=null && !fec_venc_fin.equals("")){
			whereAnd = whereAnd.equals("")?" WHERE ": " AND ";
			strSQL.append(whereAnd+" com_fechaprobablepago <= TO_DATE(?,'DD/MM/YYYY') ");
		}

		strSQL.append("order by ic_bit_monitor ");
		
		if(fec_venc_ini!=null && !fec_venc_ini.equals("")){
			conditions.add(fec_venc_ini);
		}
		
		if(fec_venc_fin!=null && !fec_venc_fin.equals("")){
			conditions.add(fec_venc_fin);
		}

		log.debug("..:: strSQL: " + strSQL.toString());
		log.debug("..:: conditions: " + conditions);
		
		log.info("getDocumentQuery(S) ::..");
		return strSQL.toString();
	}//getDocumentQuery
	
	/**
	* Obtiene el query necesario para mostrar la información completa de 
	* una página a partir de las llaves primarias enviadas como parámetro
	* @return Cadena con la consulta de SQL, para obtener la información 
	* completa de los registros con las llaves especificadas
	*/
	public String getDocumentSummaryQueryForIds(List pageIds){
		log.info("getDocumentSummaryQueryForIds(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		
		strSQL.append(" SELECT ic_bit_monitor, to_char(com_fechaprobablepago,'dd/mm/yyyy') com_fechaprobablepago, ig_prestamo, ig_cliente, ");
		strSQL.append("        ig_disposicion, fg_amortizacion, fg_interes, fg_totalvencimiento, ");
		strSQL.append("        to_char(df_pago_cliente,'dd/mm/yyyy') df_pago_cliente, cg_estatus, ig_origen, to_char(df_periodofin,'dd/mm/yyyy') df_periodofin, ");
		strSQL.append("        to_char(df_registro_fide,'dd/mm/yyyy') df_registro_fide, ig_folio, cg_estatus_proc, cg_error, ");
		strSQL.append("        cg_modalidadpago, ic_moneda, cg_moneda, ic_financiera, cg_razon_social, ");
		strSQL.append("        ig_baseoperacion, cg_descbaseoper, cg_nombrecliente, ig_sucursal, ");
		strSQL.append("        ig_proyecto, ig_contrato, ig_numelectronico, cg_bursatilizado, ");
		strSQL.append("        ig_subaplicacion, ig_cuotasemit, ig_cuotasporvenc, ig_totalcuotas, ");
		strSQL.append("        cg_aniomodalidad, ig_tipoestrato, to_char(df_fechaopera,'dd/mm/yyyy') df_fechaopera, ig_sancionado, ");
		strSQL.append("        ig_frecuenciacapital, ig_frecuenciainteres, ig_tasabase, ");
		strSQL.append("        cg_esquematasas, cg_relmat_1, fg_spread_1, cg_relmat_2, fg_spread_2, ");
		strSQL.append("        cg_relmat_3, fg_spread_3, cg_relmat_4, fg_margen, ig_tipogarantia, ");
		strSQL.append("        fg_porcdescfop, fg_totdescfop, fg_porcdescfinape, fg_totdescfinape, ");
		strSQL.append("        fg_fcrecimiento, df_periodoinic, ig_dias, fg_comision, fg_porcgarantia, ");
		strSQL.append("        fg_porccomision, fg_sdoinsoluto, fg_intcobradoxanticip, fg_pagotrad, ");
		strSQL.append("        fg_subsidio, fg_totalexigible, fg_capitalvencido, fg_interesvencido, ");
		strSQL.append("        fg_totalcarven, fg_adeudototal, fg_finadicotorgado, fg_finadicrecup, ");
		strSQL.append("        fg_sdoinsnvo, fg_sdoinsbase, ig_numero_docto, ic_programa_fondojr, ");
		strSQL.append("        cs_tipo_info, cg_causas, cg_usuario, to_char(df_fecha_bit,'dd/mm/yyyy') df_fecha_bit ");
		strSQL.append("   FROM bit_monitor_fjr_eliminados ");

		strSQL.append(" WHERE (");
		
		for(int i = 0; i < pageIds.size(); i++){
			List lItem = (ArrayList)pageIds.get(i);
				
			if(i > 0){
				strSQL.append("  OR  ");
			}
			strSQL.append(" (ic_bit_monitor = ? ) ");
			conditions.add(new Long(lItem.get(0).toString()));		
		}
		
		strSQL.append(" ) ");
		strSQL.append("order by ic_bit_monitor ");
		
		log.debug("..:: strSQL: " + strSQL.toString());
		log.debug("..:: conditions: " + conditions);
		
		log.info("getDocumentSummaryQueryForIds(S)");
		return strSQL.toString();
	}//getDocumentSummaryQueryForIds	
	
	public String getDocumentQueryFile(){
		log.info("getDocumentQueryFile(E)");
		String whereAnd = "";
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		
		strSQL.append(" SELECT ic_bit_monitor, to_char(com_fechaprobablepago,'dd/mm/yyyy') com_fechaprobablepago, ig_prestamo, ig_cliente, ");
		strSQL.append("        ig_disposicion, fg_amortizacion, fg_interes, fg_totalvencimiento, ");
		strSQL.append("        to_char(df_pago_cliente,'dd/mm/yyyy') df_pago_cliente, cg_estatus, ig_origen, to_char(df_periodofin,'dd/mm/yyyy') df_periodofin, ");
		strSQL.append("        to_char(df_registro_fide,'dd/mm/yyyy') df_registro_fide, ig_folio, cg_estatus_proc, cg_error, ");
		strSQL.append("        cg_modalidadpago, ic_moneda, cg_moneda, ic_financiera, cg_razon_social, ");
		strSQL.append("        ig_baseoperacion, cg_descbaseoper, cg_nombrecliente, ig_sucursal, ");
		strSQL.append("        ig_proyecto, ig_contrato, ig_numelectronico, cg_bursatilizado, ");
		strSQL.append("        ig_subaplicacion, ig_cuotasemit, ig_cuotasporvenc, ig_totalcuotas, ");
		strSQL.append("        cg_aniomodalidad, ig_tipoestrato, to_char(df_fechaopera,'dd/mm/yyyy') df_fechaopera, ig_sancionado, ");
		strSQL.append("        ig_frecuenciacapital, ig_frecuenciainteres, ig_tasabase, ");
		strSQL.append("        cg_esquematasas, cg_relmat_1, fg_spread_1, cg_relmat_2, fg_spread_2, ");
		strSQL.append("        cg_relmat_3, fg_spread_3, cg_relmat_4, fg_margen, ig_tipogarantia, ");
		strSQL.append("        fg_porcdescfop, fg_totdescfop, fg_porcdescfinape, fg_totdescfinape, ");
		strSQL.append("        fg_fcrecimiento, df_periodoinic, ig_dias, fg_comision, fg_porcgarantia, ");
		strSQL.append("        fg_porccomision, fg_sdoinsoluto, fg_intcobradoxanticip, fg_pagotrad, ");
		strSQL.append("        fg_subsidio, fg_totalexigible, fg_capitalvencido, fg_interesvencido, ");
		strSQL.append("        fg_totalcarven, fg_adeudototal, fg_finadicotorgado, fg_finadicrecup, ");
		strSQL.append("        fg_sdoinsnvo, fg_sdoinsbase, ig_numero_docto, ic_programa_fondojr, ");
		strSQL.append("        cs_tipo_info, cg_causas, cg_usuario, to_char(df_fecha_bit,'dd/mm/yyyy') df_fecha_bit ");
		strSQL.append("   FROM bit_monitor_fjr_eliminados ");
		
		if(fec_venc_ini!=null && !fec_venc_ini.equals("")){
			whereAnd = whereAnd.equals("")?" WHERE ": " AND ";
			strSQL.append(whereAnd+" com_fechaprobablepago >= TO_DATE(?,'DD/MM/YYYY') ");
		}
		
		if(fec_venc_fin!=null && !fec_venc_fin.equals("")){
			whereAnd = whereAnd.equals("")?" WHERE ": " AND ";
			strSQL.append(whereAnd+" com_fechaprobablepago <= TO_DATE(?,'DD/MM/YYYY') ");
		}

		strSQL.append("order by ic_bit_monitor ");
		
		if(fec_venc_ini!=null && !fec_venc_ini.equals("")){
			conditions.add(fec_venc_ini);
		}
		
		if(fec_venc_fin!=null && !fec_venc_fin.equals("")){
			conditions.add(fec_venc_fin);
		}

		return strSQL.toString();
	}//getDocumentQueryFile
	
	
	/**
	 * se imprimen todos los registros de la consulta 
	 * @return 
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		//String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();  
		ComunesXLS xls = null;
		CreaArchivo creaArchivo = new CreaArchivo();
		String	nombreArchivo	= creaArchivo.nombreArchivo()+".xls";
		xls = new ComunesXLS(path+nombreArchivo);
		StringBuffer contenidoArchivo = new StringBuffer(); 
		try {
		
			contenidoArchivo.append("Programa:,"+descPrograma+"\n");
		
			xls.setTabla(12);
			xls.setCelda("BITACORA PRESTAMOS ELIMINADOS", "formas", ComunesXLS.CENTER, 12);
			xls.setCelda(" ", "celda01", ComunesXLS.CENTER, 12);
			
			xls.setCelda("Fecha de Vencimiento Nafin", "celda01", ComunesXLS.CENTER, 1);
			xls.setCelda("fecha operacion", "celda01", ComunesXLS.CENTER, 1);
			xls.setCelda("Fecha pago cliente FIDE", "celda01", ComunesXLS.CENTER, 1);
			xls.setCelda("Préstamo", "celda01", ComunesXLS.CENTER, 1);
			xls.setCelda("Num. Cliente Sirac", "celda01", ComunesXLS.CENTER, 1);
			xls.setCelda("Cliente", "celda01", ComunesXLS.CENTER, 1);
			xls.setCelda("Num. Cuota a Pagar", "celda01", ComunesXLS.CENTER, 1);
			xls.setCelda("Capital más Interés", "celda01", ComunesXLS.CENTER, 1);
			xls.setCelda("Nombre del Validador", "celda01", ComunesXLS.CENTER, 1);
			xls.setCelda("Fecha de Validación", "celda01", ComunesXLS.CENTER, 1);
			xls.setCelda("Error", "celda01", ComunesXLS.CENTER, 1);
			xls.setCelda("Causas de Eliminación", "celda01", ComunesXLS.CENTER, 1);		
			while(rs.next()){
			
				String com_fechaprobablepago = (rs.getString("com_fechaprobablepago")==null)?" ":rs.getString("com_fechaprobablepago"); 
				String df_fechaopera = (rs.getString("df_fechaopera")==null)?" ":rs.getString("df_fechaopera"); 
				String df_pago_cliente = (rs.getString("df_pago_cliente")==null)?" ":rs.getString("df_pago_cliente"); 
				String ig_prestamo = (rs.getString("ig_prestamo")==null)?" ":rs.getString("ig_prestamo"); 
				String ig_cliente = (rs.getString("ig_cliente")==null)?" ":rs.getString("ig_cliente"); 
				String cg_nombrecliente = (rs.getString("cg_nombrecliente")==null)?" ":rs.getString("cg_nombrecliente"); 
				String ig_disposicion = (rs.getString("ig_disposicion")==null)?" ":rs.getString("ig_disposicion"); 
				String fg_totalvencimiento = (rs.getString("fg_totalvencimiento")==null)?" ":rs.getString("fg_totalvencimiento"); 
				String cg_usuario = (rs.getString("cg_usuario")==null)?" ":rs.getString("cg_usuario"); 
				String df_fecha_bit = (rs.getString("df_fecha_bit")==null)?" ":rs.getString("df_fecha_bit"); 
				String cg_error = (rs.getString("cg_error")==null)?" ":rs.getString("cg_error"); 
				String cg_causas = (rs.getString("cg_causas")==null)?" ":rs.getString("cg_causas"); 
				
				
				xls.setCelda(com_fechaprobablepago, "formas", ComunesXLS.LEFT, 1);
				xls.setCelda(df_fechaopera, "formas", ComunesXLS.LEFT, 1);
				xls.setCelda(df_pago_cliente, "formas", ComunesXLS.LEFT, 1);
				xls.setCelda(ig_prestamo, "formas", ComunesXLS.LEFT, 1);
				xls.setCelda(ig_cliente, "formas", ComunesXLS.LEFT, 1);
				xls.setCelda(cg_nombrecliente, "formas", ComunesXLS.LEFT, 1);
				xls.setCelda(ig_disposicion, "formas", ComunesXLS.LEFT, 1);
				xls.setCelda(fg_totalvencimiento, "formas", ComunesXLS.LEFT, 1);
				xls.setCelda(cg_usuario, "formas", ComunesXLS.LEFT, 1);
				xls.setCelda(df_fecha_bit, "formas", ComunesXLS.LEFT, 1);
				xls.setCelda(cg_error, "formas", ComunesXLS.LEFT, 1);
				xls.setCelda(cg_causas, "formas", ComunesXLS.LEFT, 1);

			}
			
			xls.cierraTabla();
			xls.cierraXLS();
			
			nombreArchivo = creaArchivo.getNombre()+".xls";
					
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		

		try {
		
		
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	//GETTERS
  /**
	 * Obtiene la lista de parámetros a usar como variables bind en las condiciones de la consulta
	 * @return Lista con los parametros de las condiciones
	 */
	public List getConditions(){return conditions;}
  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() {return paginaNo;}
	
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset(){return paginaOffset;}
	
	//Parametros del formulario
	public String getStrDirectorioPublicacion() {return strDirectorioPublicacion;}
	public String getCvePrograma() {return cvePrograma;}
	public String getFec_venc_ini() {return fec_venc_ini;}
	public String getFec_venc_fin() {return fec_venc_fin;}
	
	
	//SETTERS 
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo){paginaNo = newPaginaNo;}
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset){this.paginaOffset=paginaOffset;}
		
	//Parametros del formulario
	public void setStrDirectorioPublicacion(String strDirectorioPublicacion) {this.strDirectorioPublicacion = strDirectorioPublicacion;}
	public void setCvePrograma(String cvePrograma) {this.cvePrograma = cvePrograma;}
	public void setFec_venc_ini(String fec_venc_ini) {this.fec_venc_ini = fec_venc_ini;}
	public void setFec_venc_fin(String fec_venc_fin) {this.fec_venc_fin = fec_venc_fin;}


	public void setDescPrograma(String descPrograma) {
		this.descPrograma = descPrograma;
	}


	public String getDescPrograma() {
		return descPrograma;
	}

	
}