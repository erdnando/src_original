package com.netro.fondosjr;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsultaHistoricaMovimientosFondoJr implements IQueryGeneratorReg,IQueryGeneratorRegExtJS{
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ConsultaHistoricaMovimientosFondoJr.class);
	 
	/**
	 * Constructor de la clase.
	 */
	public ConsultaHistoricaMovimientosFondoJr(){}
	
	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer 	strSQL;
	private List 	conditions;
	private String paginaOffset;
	private String paginaNo;
	private String strDirectorioPublicacion;
	private String clavePrograma;
	private String fecVencIni;
	private String fecVencFin;
	private String fecOperIni;
	private String fecOperFin;
	private String fecFideIni;
	private String fecFideFin;	
	private String estatus;
	private String estatusVal;
	private String prestamo;
	private String numSirac;
	private String nombrePyme;
	private String orden;
	private String diasDesde;
	private String diasHasta;	
 
	/**
	* Obtiene el query para obtener los totales de la consulta
	* @return Cadena con la consulta de SQL, para obtener los totales
	*/
	public String getAggregateCalculationQuery(){
		
		log.info("getAggregateCalculationQuery(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		
		// F012 - 2011
		boolean hayDiasDeVencimientoEspecificados = false;
		if( (diasDesde != null && !diasDesde.equals("")) || (diasHasta != null && !diasHasta.equals("")) ){
			hayDiasDeVencimientoEspecificados = true;
		}
 
		if(!hayDiasDeVencimientoEspecificados){
			
			strSQL.append(" SELECT /*+use_nl(cpv prg)*/");
			strSQL.append(" SUM(cpv.fg_amortizacion) AS total_amort,");
			strSQL.append(" SUM(cpv.fg_interes) AS total_interes,");
			strSQL.append(" SUM(cpv.fg_totalvencimiento) AS total_saldo");
			strSQL.append(" FROM com_pagos_fide_val cpv");
			strSQL.append(", comcat_origen_programa prg");
			strSQL.append(" WHERE cpv.ig_origen = prg.ig_origen_programa");
			strSQL.append(" AND (cpv.cg_estatus = ? OR cpv.cg_estatus = ? OR cpv.cg_estatus = ? OR cpv.cg_estatus = ? OR cpv.cg_estatus = ? OR cpv.cg_estatus = ?)");
	
			conditions.add("P");
			conditions.add("NP");
			conditions.add("T");
			conditions.add("RC");
			conditions.add("RF");
			conditions.add("R");
	
			if (clavePrograma != null && !clavePrograma.equals("")) {
				strSQL.append(" AND cpv.ic_programa_fondojr = ?");
				conditions.add(new Integer(clavePrograma));
			}
			
			if (fecVencIni != null && !fecVencIni.equals("")) {
				strSQL.append(" AND cpv.com_fechaprobablepago >= TRUNC(TO_DATE(?, 'dd/mm/yyyy'))");
				conditions.add(fecVencIni);
				
			}
			
			if (fecVencFin != null && !fecVencFin.equals("")) {
				strSQL.append(" AND cpv.com_fechaprobablepago < TRUNC(TO_DATE(?, 'dd/mm/yyyy')) + 1");
				conditions.add(fecVencFin);
			}
				
			if (fecOperIni != null && !fecOperIni.equals("")) {
				strSQL.append(" AND cpv.df_registro_fide >= TRUNC(TO_DATE(?, 'dd/mm/yyyy'))");
				conditions.add(fecOperIni);
			}
			
			if (fecOperFin != null && !fecOperFin.equals("")) {
				strSQL.append(" AND cpv.df_registro_fide < TRUNC(TO_DATE(?, 'dd/mm/yyyy')) + 1");
				conditions.add(fecOperFin);
			}
			
			if (fecFideIni != null && !fecFideIni.equals("")) {
				strSQL.append(" AND cpv.df_pago_cliente >= TRUNC(TO_DATE(?, 'dd/mm/yyyy'))");
				conditions.add(fecFideIni);
			}
			
			if (fecFideFin != null && !fecFideFin.equals("")) {
				strSQL.append(" AND cpv.df_pago_cliente < TRUNC(TO_DATE(?, 'dd/mm/yyyy')) + 1");
				conditions.add(fecFideIni);
			}
			
			if (estatus != null && !estatus.equals("")) {
				strSQL.append(" AND cpv.cg_estatus = ?");
				conditions.add(estatus);
			}
			
			if (estatusVal != null && !estatusVal.equals("")) {
				strSQL.append(" AND cpv.cs_validacion = ?");
				conditions.add(estatusVal);
			}
			
			if (numSirac != null && !numSirac.equals("")) {
				strSQL.append(" AND cpv.ig_cliente = ?");
				conditions.add(new Long(numSirac));
			}
			
			if (prestamo != null && !prestamo.equals("")) {
				strSQL.append(" AND cpv.ig_prestamo = ?");
				conditions.add(new Long(prestamo));
			}
 
		}else{ // F012 - 2011
			
			strSQL.append(" SELECT ");
			strSQL.append(" 	SUM(fg_amortizacion)     AS total_amort, ");
			strSQL.append(" 	SUM(fg_interes)          AS total_interes, ");
			strSQL.append(" 	SUM(fg_totalvencimiento) AS total_saldo ");
			strSQL.append(" FROM ");
			strSQL.append(" (");
			strSQL.append(" 	SELECT /*+use_nl(cpv prg)*/");
			strSQL.append(" 		cpv.fg_amortizacion,");
			strSQL.append(" 		cpv.fg_interes,");
			strSQL.append(" 		cpv.fg_totalvencimiento,");
			strSQL.append("		( ");
			strSQL.append("      	SELECT ");
			strSQL.append("         	DECODE(  ");
			strSQL.append("					cpv.cg_estatus,  ");
			strSQL.append("					'NP', ");
			strSQL.append("					DECODE(   ");
			strSQL.append("						( ");
			strSQL.append("							SELECT /*+index(cpv2 CP_COM_PAGOS_FIDE_VAL_PK)*/ ");
			strSQL.append("								COUNT(1) ");
			strSQL.append("							FROM    ");
			strSQL.append("								COM_PAGOS_FIDE_VAL CPV2 ");
			strSQL.append("							WHERE   ");
			strSQL.append("								CPV2.COM_FECHAPROBABLEPAGO     = cpv.com_fechaprobablepago ");
			strSQL.append("								AND CPV2.IG_PRESTAMO           = cpv.ig_prestamo ");
			strSQL.append("								AND CPV2.IG_DISPOSICION        = cpv.ig_disposicion ");
			strSQL.append("								AND CPV2.FG_TOTALVENCIMIENTO   = cpv.fg_totalvencimiento  ");
			if (clavePrograma != null && !clavePrograma.equals("")) {//AJUSTE F011-2011
				strSQL.append("							AND cpv2.ic_programa_fondojr = ? ");
				conditions.add(new Integer(clavePrograma));
			}
			strSQL.append("								AND CPV2.CG_ESTATUS IN ('R','RC','RF') ");
			strSQL.append("						), ");
			strSQL.append("						0, ");
			strSQL.append("						( ");
			strSQL.append("							CASE  WHEN  ");
			strSQL.append("								cpv.com_fechaprobablepago < trunc(sysdate+1)  ");
			strSQL.append("							then  ");
			strSQL.append("								(trunc(sysdate) - cpv.com_fechaprobablepago)  ");
			strSQL.append("							else  ");
			strSQL.append("								cpv.com_fechaprobablepago-cpv.com_fechaprobablepago  ");
			strSQL.append("							end ");
			strSQL.append("						) ");
			strSQL.append("             	");
			strSQL.append("					) ");
			strSQL.append("         		,NULL ");
			strSQL.append("				)  ");
			strSQL.append("      	FROM ");
			strSQL.append("         	COM_CRED_PARAM_DIAS_VENC CCP ");
			strSQL.append("      	WHERE ");
			strSQL.append("         	CPV.COM_FECHAPROBABLEPAGO 	>= TRUNC(CCP.DF_FECHA_VMTO_INI) ");
			strSQL.append("      		AND CPV.COM_FECHAPROBABLEPAGO < 	TRUNC(CCP.DF_FECHA_VMTO_FIN+1) ");
			strSQL.append("       		AND CPV.CS_VALIDACION = 'A' ");
			if (clavePrograma != null && !clavePrograma.equals("")) {//AJUSTE F011-2011
				strSQL.append("			AND ccp.ic_programa_fondojr = ? ");
				conditions.add(new Integer(clavePrograma));
			}
			strSQL.append("   	) AS DIAS_TRANSCURRIDOS_VENCIMIENTO ");
			strSQL.append(" FROM com_pagos_fide_val cpv");
			strSQL.append(", comcat_origen_programa prg");
			strSQL.append(" WHERE cpv.ig_origen = prg.ig_origen_programa");
			strSQL.append(" AND cpv.cg_estatus = ? ");
			//strSQL.append(" AND (cpv.cg_estatus = ? OR cpv.cg_estatus = ? OR cpv.cg_estatus = ? OR cpv.cg_estatus = ? OR cpv.cg_estatus = ? OR cpv.cg_estatus = ?)");
	
			conditions.add("NP");
			/*conditions.add("P");
			conditions.add("T");
			conditions.add("RC");
			conditions.add("RF");
			conditions.add("R");*/
			
			if (clavePrograma != null && !clavePrograma.equals("")) {
				strSQL.append(" AND cpv.ic_programa_fondojr = ?");
				conditions.add(new Integer(clavePrograma));
			}
			
			if (fecVencIni != null && !fecVencIni.equals("")) {
				strSQL.append(" AND cpv.com_fechaprobablepago >= TRUNC(TO_DATE(?, 'dd/mm/yyyy'))");
				conditions.add(fecVencIni);
			}
			
			if (fecVencFin != null && !fecVencFin.equals("")) {
				strSQL.append(" AND cpv.com_fechaprobablepago < TRUNC(TO_DATE(?, 'dd/mm/yyyy')) + 1");
				conditions.add(fecVencFin);
			}
				
			if (fecOperIni != null && !fecOperIni.equals("")) {
				strSQL.append(" AND cpv.df_registro_fide >= TRUNC(TO_DATE(?, 'dd/mm/yyyy'))");
				conditions.add(fecOperIni);
			}
			
			if (fecOperFin != null && !fecOperFin.equals("")) {
				strSQL.append(" AND cpv.df_registro_fide < TRUNC(TO_DATE(?, 'dd/mm/yyyy')) + 1");
				conditions.add(fecOperFin);
			}
			
			if (fecFideIni != null && !fecFideIni.equals("")) {
				strSQL.append(" AND cpv.df_pago_cliente >= TRUNC(TO_DATE(?, 'dd/mm/yyyy'))");
				conditions.add(fecFideIni);
			}
			
			if (fecFideFin != null && !fecFideFin.equals("")) {
				strSQL.append(" AND cpv.df_pago_cliente < TRUNC(TO_DATE(?, 'dd/mm/yyyy')) + 1");
				conditions.add(fecFideIni);
			}
			
			if (estatus != null && !estatus.equals("")) {
				strSQL.append(" AND cpv.cg_estatus = ?");
				conditions.add(estatus);
			}
			
			//if (estatusVal != null && !estatusVal.equals("")) {
			strSQL.append(" AND cpv.cs_validacion = ? ");
			conditions.add("A");
			//}
			
			if (numSirac != null && !numSirac.equals("")) {
				strSQL.append(" AND cpv.ig_cliente = ?");
				conditions.add(new Long(numSirac));
			}
			
			if (prestamo != null && !prestamo.equals("")) {
				strSQL.append(" AND cpv.ig_prestamo = ?");
				conditions.add(new Long(prestamo));
			}
			
			strSQL.append(" ) A ");
			strSQL.append(" WHERE ");
			strSQL.append("	A.DIAS_TRANSCURRIDOS_VENCIMIENTO >= ? AND A.DIAS_TRANSCURRIDOS_VENCIMIENTO <= ? "); 
			conditions.add(new Integer(diasDesde));
			conditions.add(new Integer(diasHasta));
			
		}
		
		log.debug("..:: strSQL: " + strSQL.toString());
		log.debug("..:: conditions: " + conditions);
		
		log.info("getAggregateCalculationQuery(S) ::..");
		return strSQL.toString();
	}//getAggregateCalculationQuery
	
	/**
	* Obtiene el query para obtener las llaves primarias de la consulta
	* @return Cadena con la consulta de SQL, para obtener llaves primarias
	*/
	public String getDocumentQuery(){
		log.info("getDocumentQuery(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();

		// F012 - 2011
		boolean hayDiasDeVencimientoEspecificados = false;
		if( (diasDesde != null && !diasDesde.equals("")) || (diasHasta != null && !diasHasta.equals("")) ){
			hayDiasDeVencimientoEspecificados = true;
		}
		
		if(!hayDiasDeVencimientoEspecificados){
			
			strSQL.append(" SELECT /*+use_nl(cpv prg)*/ DISTINCT TO_CHAR(cpv.com_fechaprobablepago, 'dd/mm/yyyy') as com_fechaprobablepago,");
			strSQL.append(" cpv.ig_prestamo,");
			strSQL.append(" cpv.ig_cliente,");
			strSQL.append(" cpv.ig_disposicion,");
			strSQL.append(" cpv.cg_estatus,  ");
			strSQL.append(" cpv.com_fechaprobablepago as com_fechaprobablepago2");			
			strSQL.append(" FROM com_pagos_fide_val cpv");
			strSQL.append(", comcat_origen_programa prg");
			strSQL.append(" WHERE cpv.ig_origen = prg.ig_origen_programa");
			strSQL.append(" AND (cpv.cg_estatus = ? OR cpv.cg_estatus = ? OR cpv.cg_estatus = ? OR cpv.cg_estatus = ? OR cpv.cg_estatus = ? OR cpv.cg_estatus = ?)");
	
			conditions.add("P");
			conditions.add("NP");
			conditions.add("T");
			conditions.add("RC");
			conditions.add("RF");
			conditions.add("R");
			
			if (clavePrograma != null && !clavePrograma.equals("")) {
				strSQL.append(" AND cpv.ic_programa_fondojr = ?");
				conditions.add(new Integer(clavePrograma));
			}
			
			if (fecVencIni != null && !fecVencIni.equals("")) {
				strSQL.append(" AND cpv.com_fechaprobablepago >= TRUNC(TO_DATE(?, 'dd/mm/yyyy'))");
				conditions.add(fecVencIni);
			}
			
			if (fecVencFin != null && !fecVencFin.equals("")) {
				strSQL.append(" AND cpv.com_fechaprobablepago < TRUNC(TO_DATE(?, 'dd/mm/yyyy')) + 1");
				conditions.add(fecVencFin);
			}
				
			if (fecOperIni != null && !fecOperIni.equals("")) {
				strSQL.append(" AND cpv.df_registro_fide >= TRUNC(TO_DATE(?, 'dd/mm/yyyy'))");
				conditions.add(fecOperIni);
			}
			
			if (fecOperFin != null && !fecOperFin.equals("")) {
				strSQL.append(" AND cpv.df_registro_fide < TRUNC(TO_DATE(?, 'dd/mm/yyyy')) + 1");
				conditions.add(fecOperFin);
			}
			
			if (fecFideIni != null && !fecFideIni.equals("")) {
				strSQL.append(" AND cpv.df_pago_cliente >= TRUNC(TO_DATE(?, 'dd/mm/yyyy'))");
				conditions.add(fecFideIni);
			}
			
			if (fecFideFin != null && !fecFideFin.equals("")) {
				strSQL.append(" AND cpv.df_pago_cliente < TRUNC(TO_DATE(?, 'dd/mm/yyyy')) + 1");
				conditions.add(fecFideIni);
			}
			
			if (estatus != null && !estatus.equals("")) {
				strSQL.append(" AND cpv.cg_estatus = ?");
				conditions.add(estatus);
			}
			
			if (estatusVal != null && !estatusVal.equals("")) {
				strSQL.append(" AND cpv.cs_validacion = ?");
				conditions.add(estatusVal);
			}
			
			if (numSirac != null && !numSirac.equals("")) {
				strSQL.append(" AND cpv.ig_cliente = ?");
				conditions.add(new Long(numSirac));
			}
			
			if (prestamo != null && !prestamo.equals("")) {
				strSQL.append(" AND cpv.ig_prestamo = ?");
				conditions.add(new Long(prestamo));
			} 

		if ( (!diasDesde.equals("") && !diasHasta.equals("")) ||  !prestamo.equals("") || !numSirac.equals("") ) {				
			//strSQL.append(" GROUP BY IG_PRESTAMO, ig_disposicion, com_fechaprobablepago, ig_cliente, cg_estatus ");
			strSQL.append(" order by IG_PRESTAMO, com_fechaprobablepago2 ASC ");
		}

		}else{ // F012 - 2011
			
			strSQL.append(" SELECT " );
			//strSQL.append(" * " );
			strSQL.append("  com_fechaprobablepago, IG_PRESTAMO, IG_CLIENTE, IG_DISPOSICION, CG_ESTATUS, DIAS_TRANSCURRIDOS_VENCIMIENTO " );
			strSQL.append(" , com_fechaprobablepago2 FROM " );
			strSQL.append(" ( " );
	
			strSQL.append(" SELECT /*+use_nl(cpv prg)*/ DISTINCT cpv.com_fechaprobablepago as com_fechaprobablepago2, TO_CHAR(cpv.com_fechaprobablepago, 'dd/mm/yyyy') as com_fechaprobablepago,");
			strSQL.append(" cpv.ig_prestamo,");
			strSQL.append(" cpv.ig_cliente,");
			strSQL.append(" cpv.ig_disposicion,");
			strSQL.append(" cpv.cg_estatus,");
			strSQL.append("		( ");
			strSQL.append("      	SELECT ");
			strSQL.append("         	DECODE(  ");
			strSQL.append("					cpv.cg_estatus,  ");
			strSQL.append("					'NP', ");
			strSQL.append("					DECODE(   ");
			strSQL.append("						( ");
			strSQL.append("							SELECT /*+index(cpv2 CP_COM_PAGOS_FIDE_VAL_PK)*/ ");
			strSQL.append("								COUNT(1) ");
			strSQL.append("							FROM    ");
			strSQL.append("								COM_PAGOS_FIDE_VAL CPV2 ");
			strSQL.append("							WHERE   ");
			strSQL.append("								CPV2.COM_FECHAPROBABLEPAGO     = cpv.com_fechaprobablepago ");
			strSQL.append("								AND CPV2.IG_PRESTAMO           = cpv.ig_prestamo ");
			strSQL.append("								AND CPV2.IG_DISPOSICION        = cpv.ig_disposicion ");
			strSQL.append("								AND CPV2.FG_TOTALVENCIMIENTO   = cpv.fg_totalvencimiento  ");
			if (clavePrograma != null && !clavePrograma.equals("")) {//AJUSTE F011-2011
				strSQL.append("							AND cpv2.ic_programa_fondojr = ? ");
				conditions.add(new Integer(clavePrograma));
			}
			strSQL.append("								AND CPV2.CG_ESTATUS IN ('R','RC','RF') ");
			strSQL.append("						), ");
			strSQL.append("						0, ");
			strSQL.append("						( ");
			strSQL.append("							CASE  WHEN  ");
			strSQL.append("								cpv.com_fechaprobablepago < trunc(sysdate+1)  ");
			strSQL.append("							then  ");
			strSQL.append("								(trunc(sysdate) - cpv.com_fechaprobablepago)  ");
			strSQL.append("							else  ");
			strSQL.append("								cpv.com_fechaprobablepago-cpv.com_fechaprobablepago  ");
			strSQL.append("							end ");
			strSQL.append("						) ");
			strSQL.append("             	");
			strSQL.append("					) ");
			strSQL.append("         		,NULL ");
			strSQL.append("				)  ");
			strSQL.append("      	FROM ");
			strSQL.append("         	COM_CRED_PARAM_DIAS_VENC CCP ");
			strSQL.append("      	WHERE ");
			strSQL.append("         	CPV.COM_FECHAPROBABLEPAGO 	>= TRUNC(CCP.DF_FECHA_VMTO_INI) ");
			strSQL.append("      		AND CPV.COM_FECHAPROBABLEPAGO < 	TRUNC(CCP.DF_FECHA_VMTO_FIN+1) ");
			strSQL.append("       		AND CPV.CS_VALIDACION = 'A' ");
			if (clavePrograma != null && !clavePrograma.equals("")) {//AJUSTE F011-2011
				strSQL.append("      	AND ccp.ic_programa_fondojr = ? ");
				conditions.add(new Integer(clavePrograma));
			}
			strSQL.append("   	) AS DIAS_TRANSCURRIDOS_VENCIMIENTO ");
			strSQL.append(" FROM com_pagos_fide_val cpv");
			strSQL.append(", comcat_origen_programa prg");
			strSQL.append(" WHERE cpv.ig_origen = prg.ig_origen_programa");
			strSQL.append(" AND cpv.cg_estatus = ? ");
			//strSQL.append(" AND (cpv.cg_estatus = ? OR cpv.cg_estatus = ? OR cpv.cg_estatus = ? OR cpv.cg_estatus = ? OR cpv.cg_estatus = ? OR cpv.cg_estatus = ?)");
	
			conditions.add("NP");
			/*conditions.add("P");
			conditions.add("T");
			conditions.add("RC");
			conditions.add("RF");
			conditions.add("R");*/
			
			if (clavePrograma != null && !clavePrograma.equals("")) {
				strSQL.append(" AND cpv.ic_programa_fondojr = ?");
				conditions.add(new Integer(clavePrograma));
			}
			
			if (fecVencIni != null && !fecVencIni.equals("")) {
				strSQL.append(" AND cpv.com_fechaprobablepago >= TRUNC(TO_DATE(?, 'dd/mm/yyyy'))");
				conditions.add(fecVencIni);
			}
			
			if (fecVencFin != null && !fecVencFin.equals("")) {
				strSQL.append(" AND cpv.com_fechaprobablepago < TRUNC(TO_DATE(?, 'dd/mm/yyyy')) + 1");
				conditions.add(fecVencFin);
			}
				
			if (fecOperIni != null && !fecOperIni.equals("")) {
				strSQL.append(" AND cpv.df_registro_fide >= TRUNC(TO_DATE(?, 'dd/mm/yyyy'))");
				conditions.add(fecOperIni);
			}
			
			if (fecOperFin != null && !fecOperFin.equals("")) {
				strSQL.append(" AND cpv.df_registro_fide < TRUNC(TO_DATE(?, 'dd/mm/yyyy')) + 1");
				conditions.add(fecOperFin);
			}
			
			if (fecFideIni != null && !fecFideIni.equals("")) {
				strSQL.append(" AND cpv.df_pago_cliente >= TRUNC(TO_DATE(?, 'dd/mm/yyyy'))");
				conditions.add(fecFideIni);
			}
			
			if (fecFideFin != null && !fecFideFin.equals("")) {
				strSQL.append(" AND cpv.df_pago_cliente < TRUNC(TO_DATE(?, 'dd/mm/yyyy')) + 1");
				conditions.add(fecFideIni);
			}
			
			if (estatus != null && !estatus.equals("")) {
				strSQL.append(" AND cpv.cg_estatus = ?");
				conditions.add(estatus);
			}
			
			//if (estatusVal != null && !estatusVal.equals("")) {
			strSQL.append(" AND cpv.cs_validacion = ? ");
			conditions.add("A");
			//}
			
			if (numSirac != null && !numSirac.equals("")) {
				strSQL.append(" AND cpv.ig_cliente = ?");
				conditions.add(new Long(numSirac));
			}
			
			if (prestamo != null && !prestamo.equals("")) {
				strSQL.append(" AND cpv.ig_prestamo = ?");
				conditions.add(new Long(prestamo));
			}
			
			strSQL.append(" ) A "); 
			strSQL.append(" WHERE ");
			strSQL.append("	A.DIAS_TRANSCURRIDOS_VENCIMIENTO >= ? AND A.DIAS_TRANSCURRIDOS_VENCIMIENTO <= ? "); 
			conditions.add(new Integer(diasDesde));
			conditions.add(new Integer(diasHasta));
				
				if ( (!diasDesde.equals("") && !diasHasta.equals("")) ||  !prestamo.equals("") || !numSirac.equals("") ) {				
				strSQL.append("  order by IG_PRESTAMO, com_fechaprobablepago2  ASC ");
				
				}		
		} 
				
		log.debug("..:: strSQL: " + strSQL.toString());
		log.debug("..:: conditions: " + conditions);
		
		log.info("getDocumentQuery(S) ::..");
		
		return strSQL.toString();
	}//getDocumentQuery
	 
	/**
	* Obtiene el query necesario para mostrar la informaci�n completa de 
	* una p�gina a partir de las llaves primarias enviadas como par�metro
	* @return Cadena con la consulta de SQL, para obtener la informaci�n 
	* completa de los registros con las llaves especificadas
	*/
	public String getDocumentSummaryQueryForIds(List pageIds){
		log.info("getDocumentSummaryQueryForIds(E) ::..");
		strSQL = new StringBuffer();
		conditions = new ArrayList();

		// F012 - 2011
		boolean hayDiasDeVencimientoEspecificados = false;
		if( (diasDesde != null && !diasDesde.equals("")) || (diasHasta != null && !diasHasta.equals("")) ){
			hayDiasDeVencimientoEspecificados = true;
		}
		
		strSQL.append(" SELECT /*+use_nl(cpv prg)*/ DISTINCT  TO_CHAR(cpv.com_fechaprobablepago, 'dd/mm/yyyy') AS fec_venc,");
		strSQL.append(" TO_CHAR(cpv.df_registro_fide, 'dd/mm/yyyy') AS fec_fide,");
		strSQL.append(" cpv.fg_amortizacion AS amort,");
		strSQL.append(" cpv.fg_interes AS interes,");
		strSQL.append(" cpv.fg_totalvencimiento AS saldo,");
		strSQL.append(" DECODE(cpv.cg_estatus, 'P', 'Pagado', 'NP', 'No Pagado', 'R', 'Reembolso', 'RF', 'Desembolso', 'RC', 'Recuperacion de Fondo', 'T', 'Prepagos Totales') AS estatus,");
		strSQL.append(" cpv.cg_observaciones AS observaciones,");
		strSQL.append(" TO_CHAR (cpv.df_validacion, 'dd/mm/yyyy') AS fec_val,");
		strSQL.append(" cpv.cg_usuario AS usuario,");
		strSQL.append(" cpv.ig_prestamo AS prestamo,");
		strSQL.append(" cpv.ig_cliente AS num_cliente,");
		strSQL.append(" TO_CHAR (cpv.df_pago_cliente, 'dd/mm/yyyy') AS fec_cliente,");
		strSQL.append(" cpv.ig_disposicion AS num_cuota,");
		strSQL.append(" DECODE (cpv.cs_validacion, 'P', 'En Proceso', 'A', 'Validado', 'R', 'Rechazado', 'En Proceso') AS estatus_val,");
		strSQL.append(" cpv.cg_nombrecliente AS nombrecliente,");
		strSQL.append(" prg.cg_descripcion AS origen_desc,");
		strSQL.append(" cpv.com_fechaprobablepago AS fec_venc2,");	
		strSQL.append(" cpv.PREPAGO_VALIDACION AS PREPAGO_VALIDACION ,");	 
		
		strSQL.append(" ( "); 
		strSQL.append("       SELECT "); 
		strSQL.append("          DECODE(  "); 
		strSQL.append(" 				cpv.cg_estatus,  "); 
		strSQL.append(" 				'NP', "); 
		strSQL.append(" 				DECODE(   "); 
		strSQL.append(" 					( "); 
		strSQL.append(" 						SELECT /*+index(cpv2 CP_COM_PAGOS_FIDE_VAL_PK)*/ "); 
		strSQL.append(" 							COUNT(1) "); 
		strSQL.append(" 						FROM    "); 
		strSQL.append(" 							COM_PAGOS_FIDE_VAL CPV2 "); 
		strSQL.append(" 						WHERE   "); 
		strSQL.append(" 							CPV2.COM_FECHAPROBABLEPAGO     = cpv.com_fechaprobablepago "); 
		strSQL.append(" 							AND CPV2.IG_PRESTAMO           = cpv.ig_prestamo "); 
		strSQL.append(" 							AND CPV2.IG_DISPOSICION        = cpv.ig_disposicion "); 
		strSQL.append(" 							AND CPV2.FG_TOTALVENCIMIENTO   = cpv.fg_totalvencimiento  "); 
		if (clavePrograma != null && !clavePrograma.equals("")) {//AJUSTE F011-2011
			strSQL.append("						AND cpv2.ic_programa_fondojr = ? ");//fvr 012-2011
			conditions.add(new Integer(clavePrograma));
		}
		strSQL.append(" 							AND CPV2.CG_ESTATUS IN ('R','RC','RF') "); 
		strSQL.append(" 					), "); 
		strSQL.append(" 					0, "); 
		strSQL.append(" 					( "); 
		strSQL.append(" 						CASE  WHEN  "); 
		strSQL.append(" 							cpv.com_fechaprobablepago < trunc(sysdate+1)  "); 
		strSQL.append(" 						then  "); 
		strSQL.append(" 							(trunc(sysdate) - cpv.com_fechaprobablepago)  "); 
		strSQL.append(" 						else  "); 
		strSQL.append(" 							cpv.com_fechaprobablepago-cpv.com_fechaprobablepago  "); 
		strSQL.append(" 							end "); 
		strSQL.append(" 					) "); 
		strSQL.append("              "); 
		strSQL.append(" 				) "); 
		strSQL.append("          	,NULL "); 
		strSQL.append(" 			)  "); 
		strSQL.append("       FROM "); 
		strSQL.append("          COM_CRED_PARAM_DIAS_VENC CCP "); 
		strSQL.append("       WHERE "); 
		strSQL.append("          	CPV.COM_FECHAPROBABLEPAGO 	>= TRUNC(CCP.DF_FECHA_VMTO_INI) "); 
		strSQL.append("       		AND CPV.COM_FECHAPROBABLEPAGO < 	TRUNC(CCP.DF_FECHA_VMTO_FIN+1) "); 
		strSQL.append("       		AND CPV.CS_VALIDACION = 'A' ");
		if (clavePrograma != null && !clavePrograma.equals("")) {//AJUSTE F011-2011
			strSQL.append("    AND ccp.ic_programa_fondojr = ? ");
			conditions.add(new Integer(clavePrograma));
		}
		strSQL.append("    ) AS DIAS_TRANSCURRIDOS_VENCIMIENTO, "); 
		strSQL.append("    ( "); 
		strSQL.append("       SELECT  "); 
		strSQL.append("          IG_DIAS_VENCIMIENTO  "); 
		strSQL.append("       FROM  "); 
		strSQL.append("          COM_CRED_PARAM_DIAS_VENC "); 
		strSQL.append("       WHERE  "); 
		strSQL.append("          CPV.COM_FECHAPROBABLEPAGO     >= TRUNC(DF_FECHA_VMTO_INI) "); 
		strSQL.append("          AND CPV.COM_FECHAPROBABLEPAGO < TRUNC(DF_FECHA_VMTO_FIN+1) "); 
		strSQL.append("       	 AND CPV.CS_VALIDACION = 'A' ");
		if (clavePrograma != null && !clavePrograma.equals("")) {//AJUSTE F011-2011
			strSQL.append("    AND ic_programa_fondojr = ? ");
			conditions.add(new Integer(clavePrograma));
		}
		strSQL.append("    ) AS DIAS_LIMITE, ");
		strSQL.append("	 TO_CHAR(CPV.COM_FECHAPROBABLEPAGO,'DD/MM/YYYY') AS FECHAPROBABLEPAGO, ");
		strSQL.append("	 cpv.cg_estatus as clave_estatus ");
		strSQL.append(" FROM com_pagos_fide_val cpv");
		strSQL.append(", comcat_origen_programa prg");
		strSQL.append(" WHERE cpv.ig_origen = prg.ig_origen_programa");
			
		if(hayDiasDeVencimientoEspecificados){
			strSQL.append(" AND cpv.cs_validacion = ? ");
			conditions.add("A");
		}else{
			if (estatusVal != null && !estatusVal.equals("")) {
				strSQL.append(" AND cpv.cs_validacion = ?");
				conditions.add(estatusVal);
			}
		}
			
		strSQL.append(" AND (");
			
		for(int i = 0; i < pageIds.size(); i++){
			List lItem = (ArrayList)pageIds.get(i);
				
			if(i > 0){strSQL.append("  OR  ");}
			
			strSQL.append("(cpv.com_fechaprobablepago = TRUNC(TO_DATE(?, 'dd/mm/yyyy'))");
			strSQL.append(" AND cpv.ig_prestamo = ?");
			strSQL.append(" AND cpv.ig_cliente = ?");
			strSQL.append(" AND cpv.ig_disposicion = ?");
			strSQL.append(" AND cpv.cg_estatus = ?)");
			conditions.add(lItem.get(0).toString());
			conditions.add(new Long(lItem.get(1).toString()));
			conditions.add(new Long(lItem.get(2).toString()));
			conditions.add(new Long(lItem.get(3).toString()));
			conditions.add(lItem.get(4).toString());
		}
		
		strSQL.append(" ) ");
	
	if ( (diasDesde.equals("") && diasHasta.equals("")) ||  prestamo.equals("") || numSirac.equals("") ) {		
		if (orden != null && !orden.equals("")) {
			if (orden.equals("E")) {
					//strSQL.append(" ORDER BY cpv.cg_estatus");
					strSQL.append(" ORDER BY 6");
			} else if (orden.equals("V")) {
					//strSQL.append(" ORDER BY cpv.com_fechaprobablepago");
					strSQL.append(" ORDER BY 1");
			}	else if (orden.equals("F")) {
					//strSQL.append(" ORDER BY cpv.df_registro_fide");
					strSQL.append(" ORDER BY 2");
			}
		}
		 
	}
			if ( (!diasDesde.equals("") && !diasHasta.equals("")) ||  !prestamo.equals("") || !numSirac.equals("") ) {				
		
		 		strSQL.append("  order by IG_PRESTAMO, fec_venc2, clave_estatus  ASC  ");
			
			}					 
		 
		log.debug("..:: strSQL: " + strSQL.toString());
		log.debug("..:: conditions: " + conditions);
		
		log.info("getDocumentSummaryQueryForIds(S)");
		
		return strSQL.toString();
	}//getDocumentSummaryQueryForIds	
	
	public String getDocumentQueryFile(){
		strSQL = new StringBuffer();
		conditions = new ArrayList();
		
		log.info("getDocumentQueryFile(E)");
    
		// F012 - 2011
		boolean hayDiasDeVencimientoEspecificados = false;
		if( (diasDesde != null && !diasDesde.equals("")) || (diasHasta != null && !diasHasta.equals("")) ){
			hayDiasDeVencimientoEspecificados = true;
		}
 
		strSQL.append(" SELECT ");
		strSQL.append(" 	FECHA_AMORTIZACION_NAFIN, ");
		strSQL.append(" 	FECHA_OPERACION, ");
		strSQL.append(" 	FECHA_PAGO_CLIENTE_FIDE, ");
		strSQL.append(" 	PRESTAMO, ");
		strSQL.append(" 	NUM_CLIENTE_SIRAC, ");
		strSQL.append(" 	CLIENTE, ");
		strSQL.append(" 	NUM_CUOTA_A_PAGAR, ");
		strSQL.append("   DECODE  (PREPAGO_VALIDACION,  'N',  DECODE( CLAVE_ESTATUS, 'NP', TO_CHAR(DECODE(DIAS_TRANSCURRIDOS_VENCIMIENTO,NULL,0,DIAS_TRANSCURRIDOS_VENCIMIENTO)), 'N/A') , 'S', 'N/A')    AS DIAS_TRANSCURRIDOS_VENCIMIENTO, ");		
		strSQL.append(" 	CAPITAL, ");
		strSQL.append(" 	INTERES, ");
		strSQL.append(" 	TOTAL_A_PAGAR, ");
		strSQL.append(" 	ESTATUS_DEL_REGISTRO, ");
		strSQL.append(" 	ORIGEN, ");
		strSQL.append(" 	ESTATUS_VALIDACION  ");
		
		strSQL.append(" FROM ( "); 
		strSQL.append(" SELECT /*+use_nl(cpv prg)*/  DISTINCT TO_CHAR(cpv.com_fechaprobablepago, 'dd/mm/yyyy') AS fecha_amortizacion_nafin,");
		strSQL.append(" TO_CHAR(cpv.df_registro_fide, 'dd/mm/yyyy') AS fecha_operacion,");
		strSQL.append(" TO_CHAR (cpv.df_pago_cliente, 'dd/mm/yyyy') AS fecha_pago_cliente_fide,");
		strSQL.append(" cpv.ig_prestamo AS prestamo,");
		strSQL.append(" cpv.ig_cliente AS num_cliente_sirac,");
		strSQL.append(" cpv.cg_nombrecliente AS cliente,");
		strSQL.append(" cpv.ig_disposicion AS num_cuota_a_pagar,");
		strSQL.append(" cpv.com_fechaprobablepago as  FECHA_AMORTIZACION_NAFIN2,");
		strSQL.append(" cpv.PREPAGO_VALIDACION AS PREPAGO_VALIDACION ,");	  
		
		strSQL.append(" ( "); 
		strSQL.append("  SELECT "); 
		strSQL.append("  DECODE(  "); 
		strSQL.append("  cpv.cg_estatus,  "); 
		strSQL.append("  'NP', "); 
		strSQL.append(" 	DECODE(   "); 
		strSQL.append(" 	( "); 
		strSQL.append(" 	SELECT /*+index(cpv2 CP_COM_PAGOS_FIDE_VAL_PK)*/ "); 
		strSQL.append(" 	COUNT(1) "); 
		strSQL.append(" 	FROM    "); 
		strSQL.append(" 	COM_PAGOS_FIDE_VAL CPV2 "); 
		strSQL.append(" 	WHERE   "); 
		strSQL.append(" 	CPV2.COM_FECHAPROBABLEPAGO     = cpv.com_fechaprobablepago "); 
		strSQL.append(" 	AND CPV2.IG_PRESTAMO           = cpv.ig_prestamo "); 
		strSQL.append(" 	AND CPV2.IG_DISPOSICION        = cpv.ig_disposicion "); 
		strSQL.append(" 	AND CPV2.FG_TOTALVENCIMIENTO   = cpv.fg_totalvencimiento  "); 
		if (clavePrograma != null && !clavePrograma.equals("")) {//AJUSTE F011-2011
			strSQL.append("      	AND cpv2.ic_programa_fondojr = ? ");
			conditions.add(new Integer(clavePrograma));
		}
		strSQL.append(" 							AND CPV2.CG_ESTATUS IN ('R','RC','RF') "); 
		strSQL.append(" 					), "); 
		strSQL.append(" 					0, "); 
		strSQL.append(" 					( "); 
		strSQL.append(" 						CASE  WHEN  "); 
		strSQL.append(" 							cpv.com_fechaprobablepago < trunc(sysdate+1)  "); 
		strSQL.append(" 						then  "); 
		strSQL.append(" 							(trunc(sysdate) - cpv.com_fechaprobablepago)  "); 
		strSQL.append(" 						else  "); 
		strSQL.append(" 							cpv.com_fechaprobablepago-cpv.com_fechaprobablepago  "); 
		strSQL.append(" 							end "); 
		strSQL.append(" 					) "); 
		strSQL.append("              "); 
		strSQL.append(" 				) "); 
		strSQL.append("          	,NULL "); 
		strSQL.append(" 			)  "); 
		strSQL.append("       FROM "); 
		strSQL.append("          COM_CRED_PARAM_DIAS_VENC CCP "); 
		strSQL.append("       WHERE "); 
		strSQL.append("          	CPV.COM_FECHAPROBABLEPAGO 	>= TRUNC(CCP.DF_FECHA_VMTO_INI) "); 
		strSQL.append("       		AND CPV.COM_FECHAPROBABLEPAGO < 	TRUNC(CCP.DF_FECHA_VMTO_FIN+1) ");
		strSQL.append("       		AND CPV.CS_VALIDACION = 'A' ");
		if (clavePrograma != null && !clavePrograma.equals("")) {
			strSQL.append("      	AND ccp.ic_programa_fondojr = ? ");
			conditions.add(new Integer(clavePrograma));
		}
		strSQL.append("    ) AS DIAS_TRANSCURRIDOS_VENCIMIENTO, ");
		strSQL.append(" cpv.fg_amortizacion AS capital,");
		strSQL.append(" cpv.fg_interes AS interes,");
		strSQL.append(" cpv.fg_totalvencimiento AS total_a_pagar,");
		strSQL.append(" DECODE(cpv.cg_estatus, 'P', 'Pagado', 'NP', 'No Pagado', 'R', 'Reembolso', 'RF', 'Desembolso', 'RC', 'Recuperacion de Fondo', 'T', 'Prepagos Totales') AS estatus_del_registro,");
		strSQL.append(" cpv.cg_estatus  AS estatus_registro1,");
		strSQL.append(" prg.cg_descripcion AS origen,");
		strSQL.append(" DECODE (cpv.cs_validacion, 'P', 'En Proceso', 'A', 'Validado', 'R', 'Rechazado', 'En Proceso') AS estatus_validacion,");
		strSQL.append(" CPV.CG_ESTATUS as CLAVE_ESTATUS ");
		strSQL.append(" FROM com_pagos_fide_val cpv");
		strSQL.append(", comcat_origen_programa prg");
		strSQL.append(" WHERE cpv.ig_origen = prg.ig_origen_programa");
		
		if(hayDiasDeVencimientoEspecificados){
			
			strSQL.append(" AND cpv.cg_estatus = ? ");
			conditions.add("NP");
			
		}else{
			
			strSQL.append(" AND (cpv.cg_estatus = ? OR cpv.cg_estatus = ? OR cpv.cg_estatus = ? OR cpv.cg_estatus = ? OR cpv.cg_estatus = ? OR cpv.cg_estatus = ?)");
			
			conditions.add("P");
			conditions.add("NP");
			conditions.add("T");
			conditions.add("RC");
			conditions.add("RF");
			conditions.add("R");
			
		}
	

			
		if (clavePrograma != null && !clavePrograma.equals("")) {
			strSQL.append(" AND cpv.ic_programa_fondojr = ?");
			conditions.add(new Integer(clavePrograma));
		}
			
		if (fecVencIni != null && !fecVencIni.equals("")) {
			strSQL.append(" AND cpv.com_fechaprobablepago >= TRUNC(TO_DATE(?, 'dd/mm/yyyy'))");
			conditions.add(fecVencIni);
		}
			
		if (fecVencFin != null && !fecVencFin.equals("")) {
			strSQL.append(" AND cpv.com_fechaprobablepago < TRUNC(TO_DATE(?, 'dd/mm/yyyy')) + 1");
			conditions.add(fecVencFin);
		}
				
		if (fecOperIni != null && !fecOperIni.equals("")) {
			strSQL.append(" AND cpv.df_registro_fide >= TRUNC(TO_DATE(?, 'dd/mm/yyyy'))");
			conditions.add(fecOperIni);
		}
			
		if (fecOperFin != null && !fecOperFin.equals("")) {
			strSQL.append(" AND cpv.df_registro_fide < TRUNC(TO_DATE(?, 'dd/mm/yyyy')) + 1");
			conditions.add(fecOperFin);
		}
			
		if (fecFideIni != null && !fecFideIni.equals("")) {
			strSQL.append(" AND cpv.df_pago_cliente >= TRUNC(TO_DATE(?, 'dd/mm/yyyy'))");
			conditions.add(fecFideIni);
		}
			
		if (fecFideFin != null && !fecFideFin.equals("")) {
			strSQL.append(" AND cpv.df_pago_cliente < TRUNC(TO_DATE(?, 'dd/mm/yyyy')) + 1");
			conditions.add(fecFideIni);
		}
			
		if (estatus != null && !estatus.equals("")) {
			strSQL.append(" AND cpv.cg_estatus = ?");
			conditions.add(estatus);
		}
		
		if(hayDiasDeVencimientoEspecificados){
			strSQL.append(" AND cpv.cs_validacion = ? ");
			conditions.add("A");
		}else{
			if (estatusVal != null && !estatusVal.equals("")) {
				strSQL.append(" AND cpv.cs_validacion = ?");
				conditions.add(estatusVal);
			}
		}
		
		if (numSirac != null && !numSirac.equals("")) {
			strSQL.append(" AND cpv.ig_cliente = ?");
			conditions.add(new Long(numSirac));
		}
			
		if (prestamo != null && !prestamo.equals("")) {
			strSQL.append(" AND cpv.ig_prestamo = ?");
			conditions.add(new Long(prestamo));
		}
			
		strSQL.append(" ) A "); 
			
		if(hayDiasDeVencimientoEspecificados){
				strSQL.append(" WHERE ");
				strSQL.append("	A.DIAS_TRANSCURRIDOS_VENCIMIENTO >= ? AND A.DIAS_TRANSCURRIDOS_VENCIMIENTO <= ? "); 
				conditions.add(new Integer(diasDesde));
				conditions.add(new Integer(diasHasta));
		}
 	if ( (diasDesde.equals("") && diasHasta.equals("")) ||  prestamo.equals("") || numSirac.equals("") ) {				
	
		if (orden != null && !orden.equals("")) {
			if (orden.equals("E")) {
				//strSQL.append(" ORDER BY cpv.cg_estatus");
				strSQL.append(" ORDER BY 6");
			} else if (orden.equals("V")) {
				//strSQL.append(" ORDER BY cpv.com_fechaprobablepago");
				strSQL.append(" ORDER BY 1");
			}	else if (orden.equals("F")) {
				//strSQL.append(" ORDER BY cpv.df_registro_fide");
				strSQL.append(" ORDER BY 2");
			}
		}

	}		if ( (!diasDesde.equals("") && !diasHasta.equals("")) ||  !prestamo.equals("") || !numSirac.equals("") ) {				
		
		 strSQL.append(" order by  PRESTAMO, fecha_amortizacion_nafin2 , estatus_registro1 ASC "); 
		}		
		log.debug("..:: strSQL: " + strSQL.toString());
		log.debug("..:: conditions: " + conditions);
		//System.err.println("strSQL = <"+strSQL.toString()+">");
		log.info("getDocumentQueryFile(S)");
		return strSQL.toString();
	}//getDocumentQueryFile
	
	
	////////////////////////////////--*** IQueryGeneratorRegExtJS ***--////////////////////////////////
	
	//public abstract String getDocumentQuery();
	
	
	
	//public abstract String getDocumentSummaryQueryForIds(List ids);
	
	
	
	//public abstract List getConditions();
	/**
	 * Este m�todo debe regresar un query que obtendr� todos los registros
	 * resultantes de la b�squeda, con la finalidad de generar un archivo.
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	//public abstract String getDocumentQueryFile();
	
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo)
	{	
		/*Este metodo se utiliza para crear archivos segun su tipo (PDF, CSV) sin paginador*/
		/**
		 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
		 * con base en el objeto Registros que recibe como par�metro.
		 * @param request HttpRequest empleado principalmente para obtener el objeto session
		 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
		 * @param path Ruta donde se generar� el archivo
		 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
		 * @return Cadena con la ruta del archivo generado
		 */
		 
		String nombreArchivo = "";


		if("CSV".equals(tipo))
		{
				String linea = "";
				OutputStreamWriter writer = null;
				BufferedWriter buffer = null;
				StringBuffer 	contenidoArchivo 	= new StringBuffer();
				CreaArchivo 	archivo 			= new CreaArchivo();
				try {
					nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
					writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
					buffer = new BufferedWriter(writer);
					buffer.write(linea);
				
					//Encabezado
					linea=					
						"Fecha Amortizaci�n Nafin, Fecha Operaci�n,Fecha Pago,Pr�stamo,N�m. Cliente SIRAC, Cliente, " + 
						"N�m. Cuota a Pagar, D�as Transcurridos de Vencimiento,Capital,Interes,Total a Pagar,Estatus del Registro," +
						"Origen,Estatus de Validaci�n,\n";
					buffer.write(linea);
					
					while (rs.next()) 
					{
						
					String fec1 = (rs.getString("fecha_amortizacion_nafin") == null) ? "" 			: rs.getString("fecha_amortizacion_nafin");
					String fec2 = (rs.getString("fecha_operacion") == null) ? "" 						: rs.getString("fecha_operacion");
					String fec3 = (rs.getString("fecha_pago_cliente_fide") == null) ? "" 			: rs.getString("fecha_pago_cliente_fide");
					String pres = (rs.getString("prestamo") == null) ? "" 								: rs.getString("prestamo");
					String numc = (rs.getString("num_cliente_sirac") == null) ? "" 					: rs.getString("num_cliente_sirac");
					String name = (rs.getString("cliente") == null) ? "" 									: rs.getString("cliente");
					String cuot = (rs.getString("num_cuota_a_pagar") == null) ? "" 					: rs.getString("num_cuota_a_pagar");
					String dias = (rs.getString("DIAS_TRANSCURRIDOS_VENCIMIENTO") == null) ? "" 	: rs.getString("DIAS_TRANSCURRIDOS_VENCIMIENTO");//pendiente
					String capi = (rs.getString("capital") == null) ? "" 									: rs.getString("capital");
					String inte = (rs.getString("interes") == null) ? "" 									: rs.getString("interes");
					String sald = (rs.getString("total_a_pagar") == null) ? "" 							: rs.getString("total_a_pagar");
					String esta = (rs.getString("estatus_del_registro") == null) ? "" 				: rs.getString("estatus_del_registro");
					String odes = (rs.getString("origen") == null) ? "" 									: rs.getString("origen");
					String eval = (rs.getString("estatus_validacion") == null) ? "" 					: rs.getString("estatus_validacion");
					

					linea =	fec1+","+ fec2+","+fec3+","+pres+","+numc+","+name.replaceAll(",","")+","+cuot+","+dias+"," +capi+","+inte+","+
								sald+","+esta+","+odes+","+eval+"\n";
					
					buffer.write(linea);
					}
					buffer.close();
				} catch (Throwable e) 
				{
					throw new AppException("Error al generar el archivo ", e);
				}  finally {
						try {
							rs.close();
						} catch(Exception e) {}
				}
		}
		
			return  nombreArchivo;
	}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo)
	{
		/*Este metodo se utiliza para crear archivos segun su tipo (pdf, csv) CON PAGINADOR*/
		//return "";
		String nombreArchivo = "";
		if("PDF".equals(tipo)){
			try{
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
				
				String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual		= fechaActual.substring(0,2);
				String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual		= fechaActual.substring(6,10);
				String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
							
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
                                pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				////////////////////////////////////////////////////////////////////
				
				pdfDoc.setTable(14, 100); //numero de columnas y el ancho que ocupara la tabla en el documento
				
				pdfDoc.setCell("Fecha Amortizaci�n Nafin","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Operaci�n","celda01",ComunesPDF.LEFT);
				pdfDoc.setCell("Fecha Pago Cliente FIDE","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Pr�stamo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�m. Cliente SIRAC","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Cliente","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�m. Cuota a Pagar","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("D�as Transcurridos de Vencimiento","celda01",ComunesPDF.CENTER);//pendiente
				pdfDoc.setCell("Capital","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Interes","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Total a Pagar","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus del Registro","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Origen","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus de Validaci�n","celda01",ComunesPDF.CENTER);
				
				
				
				while (reg.next())
				{
					String fec1 = (reg.getString("FEC_VENC") == null) ? "" 			: reg.getString("FEC_VENC");
					String fec2 = (reg.getString("FEC_FIDE") == null) ? "" 			: reg.getString("FEC_FIDE");
					String fec3 = (reg.getString("FEC_CLIENTE") == null) ? "" 		: reg.getString("FEC_CLIENTE");
					String pres = (reg.getString("PRESTAMO") == null) ? "" 			: reg.getString("PRESTAMO");
					String numc = (reg.getString("NUM_CLIENTE") == null) ? "" 		: reg.getString("NUM_CLIENTE");
					String name = (reg.getString("NOMBRECLIENTE") == null) ? "" 	: reg.getString("NOMBRECLIENTE");
					String cuot = (reg.getString("NUM_CUOTA") == null) ? "" 			: reg.getString("NUM_CUOTA");
					String dias = ("NP".equals(reg.getString("CLAVE_ESTATUS"))?reg.getString("DIAS_TRANSCURRIDOS_VENCIMIENTO"):"N/A");
					
					String limi = (reg.getString("DIAS_LIMITE") == null) ? "" 			: reg.getString("DIAS_LIMITE");//pendiente
					//String esta = (reg.getString("CLAVE_ESTATUS") == null) ? "" 		: reg.getString("CLAVE_ESTATUS");//pendiente
					String fech = (reg.getString("FECHAPROBABLEPAGO") == null) ? "" 	: reg.getString("FECHAPROBABLEPAGO");//pendiente
					
					String capi = (reg.getString("AMORT") == null) ? "" 				: reg.getString("AMORT");
					String inte = (reg.getString("INTERES") == null) ? "" 			: reg.getString("INTERES");
					String sald = (reg.getString("SALDO") == null) ? "" 				: reg.getString("SALDO");
					String esta = (reg.getString("ESTATUS") == null) ? "" 			: reg.getString("ESTATUS");
					String odes = (reg.getString("ORIGEN_DESC") == null) ? "" 		: reg.getString("ORIGEN_DESC");
					String eval = (reg.getString("ESTATUS_VAL") == null) ? "" 		: reg.getString("ESTATUS_VAL");
					
					pdfDoc.setCell(fec1,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fec2,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fec3,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(pres,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(numc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(name,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cuot,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(dias,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(capi,2) ,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(inte,2) ,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(sald,2) ,"formas",ComunesPDF.CENTER);
					
					pdfDoc.setCell(esta,"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(odes,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(eval,"formas",ComunesPDF.CENTER);
				
				}
				
				/*
				 * para crear los encabezados de otra manera
				 * List lEncabezados = new ArrayList();
				 * lEncabezados.add("");
				 * pdfDoc.setTable(lEncabezados, "formasmenB", 100);
				 * */
				
				pdfDoc.addTable();
				pdfDoc.endDocument();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo",e);
			}finally{
				try{}	catch(Exception e){}
			}
		}
		
		
			return  nombreArchivo;
		
	}
	//////////////////////////////--*** END IQueryGeneratorRegExtJS ***--//////////////////////////////
	//GETTERS
  /**
	 * Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 * @return Lista con los parametros de las condiciones
	 */
	public List getConditions(){return conditions;}
  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() {return paginaNo;}
	
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset(){return paginaOffset;}
	
	//Parametros del formulario
	public String getStrDirectorioPublicacion() {return strDirectorioPublicacion;}
	public String getClavePrograma() {return clavePrograma;}
	public String getFecVencIni() {return fecVencIni;}
	public String getFecVencFin() {return fecVencFin;}
	public String getFecOperIni() {return fecOperIni;}
	public String getFecOperFin() {return fecOperFin;}
	public String getFecFideIni() {return fecFideIni;}
	public String getFecFideFin() {return fecFideFin;}
	public String getPrestamo() {return prestamo;}
	public String getEstatus() {return estatus;}
	public String getEstatusVal() {return estatusVal;}
	public String getNumSirac() {return numSirac;}
	public String getNombrePyme() {return nombrePyme;}
	public String getOrden() {return orden;}
	public String getDiasDesde(){	return this.diasDesde; }
	public String getDiasHasta(){	return this.diasHasta; }
 
	//SETTERS
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo){paginaNo = newPaginaNo;}
   
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset){this.paginaOffset=paginaOffset;}
		
	//Parametros del formulario
	public void setStrDirectorioPublicacion(String strDirectorioPublicacion) {this.strDirectorioPublicacion = strDirectorioPublicacion;}
	public void setClavePrograma(String clavePrograma) {this.clavePrograma = clavePrograma;}
	public void setFecVencIni(String fecVencIni) {this.fecVencIni = fecVencIni;}
	public void setFecVencFin(String fecVencFin) {this.fecVencFin = fecVencFin;}
	public void setFecOperIni(String fecOperIni) {this.fecOperIni = fecOperIni;}
	public void setFecOperFin(String fecOperFin) {this.fecOperFin = fecOperFin;}
	public void setFecFideIni(String fecFideIni) {this.fecFideIni = fecFideIni;}
	public void setFecFideFin(String fecFideFin) {this.fecFideFin = fecFideFin;}
	public void setPrestamo(String prestamo) { this.prestamo = prestamo;}
	public void setEstatus(String estatus) {this.estatus = estatus;}
	public void setEstatusVal(String estatusVal){	this.estatusVal = estatusVal;}
	public void setNumSirac(String numSirac){ this.numSirac = numSirac;}
	public void setNombrePyme(String nombrePyme){	this.nombrePyme = nombrePyme;}
	public void setOrden(String orden){	this.orden = orden;}
	public void setDiasDesde(String diasDesde){ this.diasDesde = diasDesde; }
	public void setDiasHasta(String diasHasta){ this.diasHasta = diasHasta;	}
  
}