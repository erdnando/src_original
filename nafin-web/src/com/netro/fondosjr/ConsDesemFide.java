package com.netro.fondosjr;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStreamWriter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsDesemFide implements  IQueryGeneratorRegExtJS {	

	
	public ConsDesemFide() {}   
	
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ConsDesemFide.class);  
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");  
	private String 	paginaOffset;
	private String 	paginaNo;
	private String folio;
	private String fechaIni;
	private String fechaFin;
	private String fechaHonradaIni;
	private String fechaHonradaFin;
	

  public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
			
			
		qrySentencia.append(" SELECT  "+
			 "	distinct  a.CC_ACUSE  as ACUSE  " +	
			 
			 " FROM  COM_ACUSE_DESEMBOLSO_FJR  a, COM_DESEMBOLSO_FJR  d  " +
			 " WHERE a.CC_ACUSE  =   d.CC_ACUSE    " );
			  
		  
      if (!"".equals(folio) )  {
			qrySentencia.append( " and a.CC_ACUSE  = ? ");
			 conditions.add(folio);
		}
		if ( !"".equals(fechaIni ) &&   !"".equals(fechaFin ) )   {
			qrySentencia.append(" and a.DF_FECHAHORA_CARGA >= TO_DATE ( ?, 'dd/mm/yyyy') ");
			qrySentencia.append(" and a.DF_FECHAHORA_CARGA < TO_DATE (?, 'dd/mm/yyyy') + 1 " );
			conditions.add(fechaIni);
			conditions.add(fechaFin);
		}
			
		if ( !"".equals(fechaHonradaIni ) &&   !"".equals(fechaHonradaFin ) )   {
			qrySentencia.append(" and d.DF_FECHAHONRADA >= TO_DATE ( ?, 'dd/mm/yyyy') ");
			qrySentencia.append(" and d.DF_FECHAHONRADA < TO_DATE (?, 'dd/mm/yyyy') + 1 " );
			conditions.add(fechaHonradaIni);
			conditions.add(fechaHonradaFin);
		}
				
					
	   log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
			qrySentencia.append(" SELECT  "+
			 "	distinct TO_CHAR(a.DF_FECHAHORA_CARGA,'dd/mm/yyyy HH24:mi:ss')   as FECHA_HORA  ,  "+
			 " a.CG_USUARIO as USUARIO ,  "+
			 " a.CC_ACUSE  as ACUSE  , " +
			 " a.df_fechahora_carga  ,  " +
			 " TO_CHAR(D.DF_FECHAHONRADA,'dd/mm/yyyy')   as FECHA_HONRADA  " +  
			 " FROM  COM_ACUSE_DESEMBOLSO_FJR  a, COM_DESEMBOLSO_FJR  d  " +
			 " WHERE a.CC_ACUSE  =   d.CC_ACUSE    " );
			  
		  
      if (!"".equals(folio) )  {
			qrySentencia.append( " and a.CC_ACUSE  = ? ");
			 conditions.add(folio);
		}
		if ( !"".equals(fechaIni ) &&   !"".equals(fechaFin ) )   {
			qrySentencia.append(" and a.DF_FECHAHORA_CARGA >= TO_DATE ( ?, 'dd/mm/yyyy') ");
			qrySentencia.append(" and a.DF_FECHAHORA_CARGA < TO_DATE (?, 'dd/mm/yyyy') + 1 " );
			conditions.add(fechaIni);
			conditions.add(fechaFin);
		}
			
		if ( !"".equals(fechaHonradaIni ) &&   !"".equals(fechaHonradaFin ) )   {
			qrySentencia.append(" and d.DF_FECHAHONRADA >= TO_DATE ( ?, 'dd/mm/yyyy') ");
			qrySentencia.append(" and d.DF_FECHAHONRADA < TO_DATE (?, 'dd/mm/yyyy') + 1 " );
			conditions.add(fechaHonradaIni);
			conditions.add(fechaHonradaFin);
		}
				
			
		qrySentencia.append(" AND (");
			
			for (int i = 0; i < pageIds.size(); i++) { 
				List lItem = (ArrayList)pageIds.get(i);
				
				if(i > 0){qrySentencia.append("  OR  ");}
				
				qrySentencia.append(" a.CC_ACUSE  =  ? " );
				conditions.add(new Long(lItem.get(0).toString()));
			}
			
			qrySentencia.append(" ) ");
			
			
			qrySentencia.append(" ORDER BY a.df_fechahora_carga desc  ");	
		
				
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
	
	
		qrySentencia.append(" SELECT  "+
			 "	distinct TO_CHAR(a.DF_FECHAHORA_CARGA,'dd/mm/yyyy HH24:mi:ss')   as FECHA_HORA  ,  "+
			 " a.CG_USUARIO as USUARIO ,  "+
			 " a.CC_ACUSE  as ACUSE  , " +		
			 " TO_CHAR(D.DF_FECHAHONRADA,'dd/mm/yyyy')   as FECHA_HONRADA ,    " +   
			 "  a.df_fechahora_carga  " +
			 " FROM  COM_ACUSE_DESEMBOLSO_FJR  a, COM_DESEMBOLSO_FJR  d  " +
			 " WHERE a.CC_ACUSE  =   d.CC_ACUSE    " );
			  
		  
      if (!"".equals(folio) )  {
			qrySentencia.append( " and a.CC_ACUSE  = ? ");
			 conditions.add(folio);
		}
		if ( !"".equals(fechaIni ) &&   !"".equals(fechaFin ) )   {
			qrySentencia.append(" and a.DF_FECHAHORA_CARGA >= TO_DATE ( ?, 'dd/mm/yyyy') ");
			qrySentencia.append(" and a.DF_FECHAHORA_CARGA < TO_DATE (?, 'dd/mm/yyyy') + 1 " );
			conditions.add(fechaIni);
			conditions.add(fechaFin);
		}
			
		if ( !"".equals(fechaHonradaIni ) &&   !"".equals(fechaHonradaFin ) )   {
			qrySentencia.append(" and d.DF_FECHAHONRADA >= TO_DATE ( ?, 'dd/mm/yyyy') ");
			qrySentencia.append(" and d.DF_FECHAHONRADA < TO_DATE (?, 'dd/mm/yyyy') + 1 " );
			conditions.add(fechaHonradaIni);
			conditions.add(fechaHonradaFin);
		}
					
       qrySentencia.append(" ORDER BY a.df_fechahora_carga desc  ");	
			 
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		

		try {
				
			
		
				
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {			
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	/**
	 *   
	 * @return 
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
		
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";	
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		int total = 0;
		
		try {
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);
		
			contenidoArchivo = new StringBuffer();		
			contenidoArchivo.append(" Fecha y Hora de Carga , Fecha Honrada , Usuario , Folio    \n");	
			
			while (rs.next())	{	
			
				String fechaHora = (rs.getString("FECHA_HORA") == null) ? "" : rs.getString("FECHA_HORA");
				String  fechaHonrada= (rs.getString("FECHA_HONRADA") == null) ? "" : rs.getString("FECHA_HONRADA");
				String  usuario= (rs.getString("USUARIO") == null) ? "" : rs.getString("USUARIO");
				String  acuse= (rs.getString("ACUSE") == null) ? "" : rs.getString("ACUSE");
			
				contenidoArchivo.append(fechaHora.replace(',',' ')+", "+
											fechaHonrada.replace(',',' ')+", "+
											usuario.replace(',',' ')+", "+
											acuse.replace(',',' ')+" \n");	
											
				total++;
				if(total==1000){					
					total=0;	
					buffer.write(contenidoArchivo.toString());
					contenidoArchivo = new StringBuffer();//Limpio  
				}
			
			
			} // Wile 
			
			buffer.write(contenidoArchivo.toString());
			buffer.close();	
			contenidoArchivo = new StringBuffer();//Limpio    
			
			
			
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}

	
	
	public String consDescargaArchivos(String rutaDestino, String folio, String tipoArchivo, String extension )  {
		log.info("consDescargaArchivos(E) ::..");
	
		AccesoDB con = new AccesoDB();
		String nombreArchivoTmp = null;
		String columna  = "";
		List  lVarBind		= new ArrayList();	
		if(tipoArchivo.equals("Acuse"))  {  		columna =  "BI_ACUSE";   }
		if(tipoArchivo.equals("Desembolso"))  {  	columna =  "BI_DESEMBOLSOS";      }
			
		try {
			con.conexionDB();
		
			String strSQL = 
				" SELECT "+columna+
				" FROM  COM_ACUSE_DESEMBOLSO_FJR "+
				" WHERE CC_ACUSE = ? ";		
			lVarBind.add(folio);
			
			//log.info("strSQL == "+strSQL);	log.info("lVarBind == "+lVarBind);
						
			PreparedStatement ps = con.queryPrecompilado(strSQL,lVarBind );
			ResultSet rs = ps.executeQuery();
			
			if (rs.next()) {	
			
				InputStream inStream = rs.getBinaryStream(columna);
			
				CreaArchivo creaArchivo = new CreaArchivo();
				if (!creaArchivo.make(inStream, rutaDestino, "."+extension)) {
					throw new AppException("Error al generar el archivo en " + rutaDestino);
				}
				nombreArchivoTmp = creaArchivo.getNombre();
				inStream.close();
			}
			rs.close();
			ps.close();

		
		} catch(Throwable e) {
			log.error("consArchCotizacion(ERROR) ::..");
			throw new AppException("Error Inesperado", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(true);
				con.cierraConexionDB();				
			}	
			log.info("consArchCotizacion(S) ::..");
		}
		return nombreArchivoTmp;
	}
		
	
	public List getConditions() {  return conditions;  }  
	public String getPaginaNo() { return paginaNo; 	}
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getFechaIni() {
		return fechaIni;
	}

	public void setFechaIni(String fechaIni) {
		this.fechaIni = fechaIni;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getFechaHonradaIni() {
		return fechaHonradaIni;
	}

	public void setFechaHonradaIni(String fechaHonradaIni) {
		this.fechaHonradaIni = fechaHonradaIni;
	}

	public String getFechaHonradaFin() {
		return fechaHonradaFin;
	}

	public void setFechaHonradaFin(String fechaHonradaFin) {
		this.fechaHonradaFin = fechaHonradaFin;
	}


}