package com.netro.fondosjr;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class InfAcuseMoviGenerales implements IQueryGeneratorRegExtJS  {
	public InfAcuseMoviGenerales()
	{
	}
	
	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer 		qrySentencia;
	private List 		conditions;
	private String 	paginaOffset;
	private String 	paginaNo;
	private static final Log log = ServiceLocator.getInstance().getLog(InfAcuseMoviGenerales.class);//Variable para enviar mensajes al log.

	StringBuffer 	strQuery;

	private String proceso;
	private String numAcuse;
	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
 	} 
	 /**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery(){
		conditions = new ArrayList();	
		strQuery 		= new StringBuffer(); 
		log.debug("getDocumentQuery)"+strQuery.toString()); 
		log.debug("getDocumentQuery)"+conditions);
		return strQuery.toString();
 	} 

	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
		strQuery = new StringBuffer(); 
		log.debug("getDocumentSummaryQueryForIds "+strQuery.toString()); 
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
		return strQuery.toString();
 	} 
public String getDocumentQueryFile(){
		conditions = new ArrayList();   
		strQuery 		= new StringBuffer(); 
		StringBuffer condicion    = new StringBuffer();
		log.info("getDocumentQueryFile(E)");
		strQuery.append(
				" SELECT mov.IG_MOV_GRAL as NUMERO, " +
										" TO_CHAR(mov.DF_FEC_MOVIMIENTO, 'DD/MM/YYYY') AS FECHA_MOVIMIENTO, "+
										" TO_CHAR(mov.DF_FEC_VALOR, 'DD/MM/YYYY') AS FECHA_VALOR, "+
										" mov.FG_CAPITAL AS IMPORTE, "+
										" mov.CG_REFERENCIA AS REFERENCIA, "+
										" cat.cg_operacion AS OPERACION, "+
										" mov.CG_OBSERVACIONES AS OBSERVACIONES "+
										" FROM COM_MOV_GRAL_FONDO_JR mov , COMCAT_OPER_FONDO_JR cat "+
										" WHERE  mov.IG_OPER_FONDO_JR = cat.IG_OPER_FONDO_JR  AND IG_NO_PROCESO = "+proceso);
		log.debug("getDocumentQueryFile "+strQuery.toString());
		log.info("getDocumentQueryFile(S)");
		return strQuery.toString();
 	} 
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		
		return "";
	}
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		log.debug("::crearCustomFile(E)");
		String linea = "";  
		String nombreArchivo = "";
		int numeroRegistros = 0;
		double totalAdeudo = 0.0;
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		CreaArchivo 	archivo 			= new CreaArchivo();
		if ("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				/*Este metodo tiene la posibilidad de personalizar las imagenes del lado izquierdo y derecho*/
				pdfDoc.encabezadoConImagenesPersonalizado(pdfDoc,(String)session.getAttribute("strPais"),
														((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
														(String)session.getAttribute("sesExterno"),								 
														(String) session.getAttribute("strNombre"),
														(String) session.getAttribute("strNombreUsuario"),
														"00archivos/15cadenas/15archcadenas/logos/" + (String)session.getAttribute("strLogo"),
														"00utils/gif/SE.gif",
														(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				
				pdfDoc.addText("Consulta de Movimientos JR\n","formas",ComunesPDF.CENTER);
				pdfDoc.addText("Programa:      "+(String) session.getAttribute("descProgramaFondoJR"),"formas",ComunesPDF.CENTER);
				pdfDoc.addText("N�mero de recibo   "+numAcuse,"formas",ComunesPDF.CENTER);
				pdfDoc.setTable(7,100,new float[]{10.5f,10.5f,10f,11.5f,10f,11.5f,10f,});
				pdfDoc.setCell("FECHA DE MOVIMIENTO","celda01rep",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell("FECHA VALOR","celda01rep",ComunesPDF.CENTER,1,1,1); 	
				pdfDoc.setCell("REFERENCIA","celda01rep",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell("IMPORTE","celda01rep",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell("MOVIMIENTO","celda01rep",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell("OPERACI�N","celda01rep",ComunesPDF.CENTER,1,1,1);
				pdfDoc.setCell("OBSERVACIONES","celda01rep",ComunesPDF.CENTER,1,1,1);
				while (rs.next()) {
					numeroRegistros++;
					String NUMERO = rs.getString("NUMERO")==null?"":rs.getString("NUMERO");
					String FECHA_MOVIMIENTO = rs.getString("FECHA_MOVIMIENTO")==null?"":rs.getString("FECHA_MOVIMIENTO");
					String FECHA_VALOR = rs.getString("FECHA_VALOR")==null?"":rs.getString("FECHA_VALOR");
					String IMPORTE = rs.getString("IMPORTE")==null?"":rs.getString("IMPORTE");
					String REFERENCIA = rs.getString("REFERENCIA")==null?"":rs.getString("REFERENCIA");
					String OPERACION = rs.getString("OPERACION")==null?"":rs.getString("OPERACION");
					String OBSERVACIONES = rs.getString("OBSERVACIONES")==null?"":rs.getString("OBSERVACIONES");
					
					pdfDoc.setCell(FECHA_MOVIMIENTO,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(FECHA_VALOR,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(REFERENCIA,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(IMPORTE,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(NUMERO,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(OPERACION,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(OBSERVACIONES,"formas",ComunesPDF.CENTER);
					
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();
			} catch(Throwable e) {
					throw new AppException("Error al generar el archivo", e);
				}
		}
		log.info(":: crearCustomFile(S)");
		return nombreArchivo;			
	}
/*****************************************************
	 GETTERS
*******************************************************/
 
	/**
	  Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	

	
	//GET
public String getProceso()		          {return proceso;}
public String getNumAcuse()		          {return numAcuse;}
	
	 	
	
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina   
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

// SET	
public void setProceso(String proceso)		 { this.proceso = proceso; }
public void setNumAcuse(String numAcuse)		 { this.numAcuse = numAcuse; }



	
}