package com.netro.admcontenido;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class SeccionContenido  implements java.io.Serializable {
	private String claveSeccion;
	private String claveOpcion;
	private String descripcion;
	private String titulo;

	private static final Log log = ServiceLocator.getInstance().getLog(SeccionContenido.class);
	
	public SeccionContenido()  {
	}
	
	/**
	 * Obtiene el contenido de la seccion especificada con setClaveSeccion(String)
	 * @return 
	 */
	public Registros getContenidoSeccion() {
		log.info("getContenidoSeccion(E)");
		Integer iClaveSeccion =  null;
		try {
			if (this.claveSeccion == null || this.claveSeccion.equals("")) {
				throw new Exception("La clave de seccion es requerida");
			}
			
			iClaveSeccion = new Integer(this.claveSeccion);
		} catch(Exception e) {
			throw new AppException("Error al obtener el contenido de Seccion", e);
		}
		
		String strSQL = 
				" SELECT ic_seccion, ic_opcion, cg_descripcion, cg_titulo, cs_nuevo " +
				" FROM admcon_seccion_cont " +
				" WHERE ic_seccion = ? " +
				" ORDER BY cs_nuevo DESC, df_modificacion DESC ";
		
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			List params = new ArrayList();
			params.add(iClaveSeccion);
			Registros reg = con.consultarDB(strSQL, params);
			return reg;
		} catch(Exception e) {
			throw new AppException("Error al obtener el contenido de Seccion", e);
		} finally {
			log.info("getContenidoSeccion(S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
	}

	/**
	 * Obtiene el contenido detalle de la seccion especificada con setClaveSeccion(String)
	 * @return 
	 */
	public Registros getContenidoSeccionDetalle() {
		Integer iClaveSeccion = null;
		Integer iClaveOpcion = null;
		try {
			if (this.claveSeccion == null || this.claveSeccion.equals("")) {
				throw new Exception("La clave de seccion es requerida");
			}
			if (this.claveOpcion == null || this.claveOpcion.equals("")) {
				throw new Exception("La clave de opcion es requerida");
			}
			
			iClaveSeccion = new Integer(this.claveSeccion);
			iClaveOpcion  = new Integer(this.claveOpcion);
		} catch(Exception e) {
			throw new AppException("Error al obtener el contenido detalle de Seccion", e);
		}
		
		String strSQL = 
				" SELECT cg_descripcion " +
				" FROM admcon_seccion_cont_det " +
				" WHERE ic_seccion = ? " +
				" AND ic_opcion = ? " +
				" ORDER BY ig_orden ";
		
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			List params = new ArrayList();
			params.add(iClaveSeccion);
			params.add(iClaveOpcion);
			Registros reg = con.consultarDB(strSQL, params);
			return reg;
		} catch(Exception e) {
			throw new AppException("Error al obtener el contenido detalle de Seccion", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		
	}


	public String getClaveSeccion() {
		return claveSeccion;
	}

	public void setClaveSeccion(String claveSeccion) {
		this.claveSeccion = claveSeccion;
	}

	public String getClaveOpcion() {
		return claveOpcion;
	}

	public void setClaveOpcion(String claveOpcion) {
		this.claveOpcion = claveOpcion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	
}