package com.netro.admcontenido;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Esta clase representa la imagen parametrizada para una seccion del home.
 * (admcon_seccion)
 * @author Gilberto Aparicio 24/04/2012
 *
 */
public class ImagenSeccion implements java.io.Serializable {

	private static final Log log = ServiceLocator.getInstance().getLog(ImagenSeccion.class);
	
	public ImagenSeccion() {
	}
	
	/**
	 * Establece la fecha de alta o modificacion del dise�o
	 * @param fechaModificacion fecha de modificacion.
	 */
	public void setFechaModificacion(java.util.Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	
	/**
	 * Obtiene la fecha de alta o modificacion del dise�o
	 * @return (objeto Date) fecha de modificacion.
	 */
	public java.util.Date getFechaModificacion() {
		return this.fechaModificacion;
	}
	
	/**
	 * Obtiene la informaci�n de la imagen de cada seccion
	 * 
	 * @return Lista con la informaci�n de los dise�os (objetos ImagenSeccion).
	 * 		Si no hay imagenes, regresa una lista vacia, nunca null.
	 */
	public static List getInfoImagenesSecciones() {
		log.info("getInfoImagenesSecciones(E)");
		List imagenesSecciones = new ArrayList();

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		
		try {
			con.conexionDB();
			String strSQL = 
					" SELECT ic_seccion, cg_nombre, " +
					" df_modificacion " +
					" FROM admcon_seccion " +
					" ORDER BY ic_seccion ";
			ps = con.queryPrecompilado(strSQL);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				ImagenSeccion imagen = new ImagenSeccion();
				imagen.setClaveSeccion(rs.getString("ic_seccion"));
				imagen.setNombreSeccion(rs.getString("cg_nombre"));
				imagen.setFechaModificacion(rs.getTimestamp("df_modificacion"));
				imagenesSecciones.add(imagen);
			}
			rs.close();
			return imagenesSecciones;
		} catch(Exception e) {
			throw new AppException("Error al obtener la informacion de las secciones de la pantalla principal", e);
		} finally {
			log.info("getInfoImagenesSecciones(S)");
			if (con.hayConexionAbierta()) {
				try {
					if (ps != null) {
						ps.close();
					}
				} catch(Exception e) {
					System.out.println(e.getMessage());
				}
				con.cierraConexionDB();
			}
		}
	}
	
	
	/**
	 * Guarda la version de BD en el Sistema de Archivos.
	 * @param rutaSeccion Ruta donde se deber� guardar la imagen obtenida de BD
	 */
	public void getImagenBDaFS(String rutaSeccion) {
		log.info("getImagenBDaFS(E)");
		int iClaveSeccion = 0;
		try {
			if (this.claveSeccion == null || this.claveSeccion.equals("")) {
				throw new Exception("La clave de seccion es requerida");
			}
			
			iClaveSeccion = Integer.parseInt(this.claveSeccion);
		} catch(Exception e) {
			throw new AppException("Error al guardar en FS imagen de seccion de BD", e);
		}

		AccesoDB con = new AccesoDB();
		String strSQL = 
				" SELECT bi_imagen, dbms_lob.getlength(bi_imagen) AS image_size, " +
				" df_modificacion " +
				" FROM admcon_seccion " +
				" WHERE ic_seccion = ? ";
		try {
			con.conexionDB();
			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setInt(1, iClaveSeccion);
			ResultSet rs = ps.executeQuery();
			rs.next();
			java.util.Date fechaArchivo = rs.getTimestamp("df_modificacion");
			int filesize = rs.getInt("image_size");
			InputStream inStream = rs.getBinaryStream("bi_imagen");
			rs.close();
			ps.close();

			if (filesize >0) {
				OutputStream outstream = new BufferedOutputStream(new FileOutputStream(rutaSeccion + this.claveSeccion + ".jpg"));
				int i = 0;
				byte arrByte[] = new byte[16384];
				while((i = inStream.read(arrByte, 0, arrByte.length)) != -1) {
					outstream.write(arrByte, 0, i);
				}
				outstream.close();
	
				//Cuando hay fechaArchivo esa es la que le pone al archivo creado
				if (fechaArchivo != null) {
					File archivo = new File(rutaSeccion + this.claveSeccion + ".jpg");
					if (archivo.exists()){
						archivo.setLastModified(fechaArchivo.getTime());
					}
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
			throw new AppException("Error al obtener la imagen de seccion de Base de Datos", e);
		} finally {
			log.info("getImagenBDaFS(S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}

	}

	/**
	 * Establece la clave de la seccion del home
	 * @param claveSeccion Valor de la clave de la secci�n.
	 */
	public void setClaveSeccion(String claveSeccion) {
		this.claveSeccion = claveSeccion;
	}


	public String getClaveSeccion() {
		return claveSeccion;
	}
	
	public String toString() {
		return 
				"Clave Seccion=" + this.claveSeccion + "\n" +
				"Fecha Modificacion=" + this.fechaModificacion;
	}
	
	/**
	 * Establece el nombre de la seccion
	 * @param nombreSeccion
	 */
	public void setNombreSeccion(String nombreSeccion) {
		this.nombreSeccion = nombreSeccion;
	}


	public String getNombreSeccion() {
		return nombreSeccion;
	}
	
	private java.util.Date fechaModificacion;
	private String claveSeccion;
	private String nombreSeccion;


}