package com.netro.admcontenido;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class Link  implements java.io.Serializable {
	private String claveLink;
	private String nombre;
	private String url;

	private static final Log log = ServiceLocator.getInstance().getLog(Link.class);

	public Link() {
	}

	public String getClaveLink() {
		return claveLink;
	}

	public void setClaveLink(String claveLink) {
		this.claveLink = claveLink;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public Registros getLinks() {
		log.info("getLinks(E)");
		String strSQL = 
				" SELECT cg_nombre, cg_url " +
				" FROM admcon_link " +
				" ORDER BY ic_link ";
		
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
		Registros reg = con.consultarDB(strSQL);
			return reg;
		} catch(Exception e) {
			throw new AppException("Error al obtener los links", e);
		} finally {
			log.info("getLinks(S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
	}
	
}