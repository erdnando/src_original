package com.netro.admcontenido;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class Banner implements java.io.Serializable {

	private static final Log log = ServiceLocator.getInstance().getLog(Banner.class);
	private String claveBanner;
	private String nombreBanner;
	private Date fechaModificacion;
	
	public Banner() {
	}

	/**
	 * Determina si es necesario obtener de BD las imagenes de los banners
	 * @param rutaBanners Ruta fisica donde se almacenan las imagenes de los banners
	 * @return 
	 */
	public static boolean sincronizarBannersRequerido(String rutaBanner) {
		log.info("sincronizarBannersRequerido(E)");

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		
		try {
			con.conexionDB();

			String strSQL = 
					" SELECT ic_banner " +
					" FROM admcon_banner " +
					" WHERE cs_activo = ? " +
					" ORDER BY df_modificacion DESC ";
			ps = con.queryPrecompilado(strSQL);
			ps.setString(1,"S");
			ResultSet rsBanner = ps.executeQuery();
			String idBanner = "1";
			if (rsBanner.next()) {
				//Solo se toma el primero, que es el de mas reciente modificacion
				idBanner = rsBanner.getString("ic_banner");
			}
			rsBanner.close();
			ps.close();

			strSQL = 
					" SELECT max(df_modificacion) as df_modificacion " +
					" FROM admcon_banner " +
					" WHERE cs_activo = ? ";
			
			File archivoImgBanner = new File(rutaBanner + "banner" + idBanner + "s.jpg");
			boolean sincronizarImagenes = false;
			if (archivoImgBanner.exists()) {
				java.util.Date fechaFS = new java.util.Date(archivoImgBanner.lastModified());

				ps = con.queryPrecompilado(strSQL);
				ps.setString(1,"S");
				ResultSet rs = ps.executeQuery();
				if (rs.next()) {

					//Fecha registrada en el sistema de archivos(FS) es menor que la BD se debe actualizar las imagenes
					//Solo se compara la fecha del banner1.jpg, dado que todos los archivos de banner tienen la misma
					//fecha y hora, ya que al ser extraidos de la BD y colocados en el FS, se les ajusta la fecha
					if(fechaFS.compareTo(rs.getTimestamp("df_modificacion")) != 0 ) {
						sincronizarImagenes = true;
					} else {
						sincronizarImagenes = false;
					}
				} else {
					sincronizarImagenes = false;
				}
				rs.close();
				ps.close();
			} else {
				sincronizarImagenes = true;
			}
			return sincronizarImagenes;
		} catch(Exception e) {
			throw new AppException("Error al obtener la informacion de los banners de la pantalla principal", e);
		} finally {
			log.info("sincronizarBannersRequerido(S)");
			if (con.hayConexionAbierta()) {
				try {
					if (ps != null) {
						ps.close();
					}
				} catch(Exception e) {
					log.error("Error al cerrar statement", e);
				}
				con.cierraConexionDB();
			}
		}
	}
	
	/**
	 * Obtiene la informaci�n de la imagen de los banners parametrizados
	 * 
	 * @return Lista con la informaci�n de los dise�os (objetos Banner).
	 * 		Si no hay imagenes, regresa una lista vacia, nunca null.
	 */
/*	public static List getInfoImagenesBanners() {
		log.info("getInfoImagenesBanners(E)");
		List imagenesInfo = new ArrayList();

		AccesoDB con = new AccesoDB();
		PreparedStatement ps = null;
		
		try {
			con.conexionDB();
			String strSQL = 
					" SELECT ic_banner, cg_nombre, " +
					" df_modificacion " +
					" FROM admcon_banner " +
					" WHERE cs_activo = ? " +
					" ORDER BY df_modificacion ";
			ps = con.queryPrecompilado(strSQL);
			ps.setString(1,"S");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Banner banner = new Banner();
				banner.setClaveBanner(rs.getString("ic_banner"));
				banner.setNombreBanner(rs.getString("cg_nombre"));
				banner.setFechaModificacion(rs.getTimestamp("df_modificacion"));
				
				imagenesInfo.add(banner);
			}
			rs.close();
			return imagenesInfo;
		} catch(Exception e) {
			throw new AppException("Error al obtener la informacion de los banners de la pantalla principal", e);
		} finally {
			log.info("getInfoImagenesBanners(S)");
			if (con.hayConexionAbierta()) {
				try {
					if (ps != null) {
						ps.close();
					}
				} catch(Exception e) {
					System.out.println(e.getMessage());
				}
				con.cierraConexionDB();
			}
		}
	}
*/
	
	/**
	 * Guarda la version de BD en el Sistema de Archivos.
	 * @param rutaBanner Ruta donde se deber� guardar la imagen obtenida de BD
	 */
	public static void getImagenesBDaFS(String rutaBanner) {
		log.info("getImagenesBDaFS(E)");
/*		int iClaveBanner = 0;
		try {
			if (this.claveBanner == null || this.claveBanner.equals("")) {
				throw new Exception("La clave de banner es requerida");
			}
			
			iClaveBanner = Integer.parseInt(this.claveBanner);
		} catch(Exception e) {
			throw new AppException("Error al guardar en FS imagen de seccion de BD", e);
		}
*/
		AccesoDB con = new AccesoDB();
		
		String strSQLFecha = 
				" SELECT max(df_modificacion) as df_modificacion " +
				" FROM admcon_banner " +
				" WHERE cs_activo = ? ";
			

		String strSQL = 
				" SELECT ic_banner, cg_nombre, bi_imagen, dbms_lob.getlength(bi_imagen) AS image_size, " +
				" bi_imagen_miniatura, dbms_lob.getlength(bi_imagen_miniatura) AS image_size_miniatura " +
				" FROM admcon_banner " +
				" WHERE cs_activo = ? " +
				" ORDER by df_modificacion DESC ";
		try {
			con.conexionDB();

			PreparedStatement ps = con.queryPrecompilado(strSQLFecha);
			ps.setString(1,"S");
			java.util.Date fechaArchivo = new java.util.Date();
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				fechaArchivo = rs.getTimestamp("df_modificacion");
			}
			rs.close();
			ps.close();


			ps = con.queryPrecompilado(strSQL);
			ps.setString(1, "S");
			rs = ps.executeQuery();
			
			int numReg = 0;
			while(rs.next()) {
				numReg = rs.getInt("ic_banner");
				int filesize = rs.getInt("image_size");
				InputStream inStream = rs.getBinaryStream("bi_imagen");

				int filesizeMin = rs.getInt("image_size_miniatura");
				InputStream inStreamMin = rs.getBinaryStream("bi_imagen_miniatura");
				
				String rutaNombreComun = rutaBanner + "banner" + numReg;
				
				if (filesize >0) {
					OutputStream outstream = new BufferedOutputStream(new FileOutputStream(rutaNombreComun + "l.jpg"));
					int i = 0;
					byte arrByte[] = new byte[16384];
					while((i = inStream.read(arrByte, 0, arrByte.length)) != -1) {
						outstream.write(arrByte, 0, i);
					}
					outstream.close();
		
					//Cuando hay fechaArchivo esa es la que le pone al archivo creado
					if (fechaArchivo != null) {
						File archivo = new File(rutaNombreComun + "l.jpg");
						if (archivo.exists()){
							archivo.setLastModified(fechaArchivo.getTime());
						}
					}
				}

				if (filesizeMin >0) {
					OutputStream outstream = new BufferedOutputStream(new FileOutputStream(rutaNombreComun + "s.jpg"));
					int i = 0;
					byte arrByte[] = new byte[16384];
					while((i = inStreamMin.read(arrByte, 0, arrByte.length)) != -1) {
						outstream.write(arrByte, 0, i);
					}
					outstream.close();
		
					//Cuando hay fechaArchivo esa es la que le pone al archivo creado
					if (fechaArchivo != null) {
						File archivo = new File(rutaNombreComun + "s.jpg");
						if (archivo.exists()){
							archivo.setLastModified(fechaArchivo.getTime());
						}
					}
				}
			}
			rs.close();
			ps.close();
		} catch(Exception e) {
			e.printStackTrace();
			throw new AppException("Error al obtener las imagenes de banners de Base de Datos", e);
		} finally {
			log.info("getImagenesBDaFS(S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}

	}

	/**
	 * Obtiene la informaci�n de los banner a mostrar
	 */
	public static Map getBannersInfo() {
		log.info("getBannersInfo(E)");

		Map nombreBanners = new HashMap();
		AccesoDB con = new AccesoDB();
		
		String strSQL = 
				" SELECT ic_banner, cg_nombre " +
				" FROM admcon_banner " +
				" WHERE cs_activo = ? " +
				" ORDER by df_modificacion DESC ";
		try {
			con.conexionDB();
			
			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setString(1, "S");
			ResultSet rs = ps.executeQuery();
			
			int numReg = 0;
			while(rs.next()) {
				numReg++;
				
				Banner banner = new Banner();
				banner.setClaveBanner(rs.getString("ic_banner"));
				banner.setNombreBanner(rs.getString("cg_nombre"));
				
				nombreBanners.put(new Integer(numReg), banner);
			}
			rs.close();
			ps.close();
			return nombreBanners;
		} catch(Exception e) {
			e.printStackTrace();
			throw new AppException("Error al obtener la informacion de los banners", e);
		} finally {
			log.info("getBannersInfo(S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}

	}


	/**
	 * Obtiene la informaci�n detallada del banner especificado
	 */
	public Registros getBanner() {
		log.info("getBanner(E)");

		Integer iClaveBanner = null;
		try {
			if (this.claveBanner == null || this.claveBanner.equals("")) {
				throw new Exception("La clave de banner es requerida");
			}
			
			iClaveBanner = new Integer(this.claveBanner);
		} catch(Exception e) {
			throw new AppException("Error al obtener el banner " + this.claveBanner, e);
		}

		AccesoDB con = new AccesoDB();
		
		String strSQL = 
				"SELECT cg_titulo, cg_texto " +
				"FROM admcon_banner_det " +
				"WHERE ic_banner = ? " +
				"ORDER BY ig_posicion ";
		try {
			con.conexionDB();
			List params = new ArrayList();
			params.add(iClaveBanner);
			Registros reg = con.consultarDB(strSQL, params);
			return reg;
		} catch(Exception e) {
			e.printStackTrace();
			throw new AppException("Error al obtener la informacion del banner " + 
					this.claveBanner, e);
		} finally {
			log.info("getBanner(S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}

	}


	public String getClaveBanner() {
		return claveBanner;
	}

	public void setClaveBanner(String claveBanner) {
		this.claveBanner = claveBanner;
	}

	public String getNombreBanner() {
		return nombreBanner;
	}

	public void setNombreBanner(String nombreBanner) {
		this.nombreBanner = nombreBanner;
	}

	public Date getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	
	
}