package com.netro.appinit;

import com.netro.admcontenido.Banner;
import com.netro.admcontenido.ImagenSeccion;
import com.netro.cadenas.DisenoEpo;
import com.netro.cadenas.DisenoIF;

import java.io.File;
import java.io.IOException;

import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.PosixFilePermission;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.MensajeParam;
import netropology.utilerias.Menu;
import netropology.utilerias.ParametrosGlobalesApp;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.lang.SystemUtils;
import org.apache.commons.logging.Log;


public class InicializacionContextListener implements ServletContextListener {
    
    //Variable para enviar mensajes al log.
    private final static Log log = ServiceLocator.getInstance().getLog(InicializacionContextListener.class);
    
	private static final PosixFilePermission[] _ARRAY_PERMS = new PosixFilePermission[] {PosixFilePermission.OWNER_READ, 
			PosixFilePermission.OWNER_WRITE, PosixFilePermission.OWNER_EXECUTE, PosixFilePermission.GROUP_READ};
	private static final Set<PosixFilePermission> PERMS = new HashSet<PosixFilePermission>(Arrays.asList(_ARRAY_PERMS));

	@Override
	public void contextInitialized(ServletContextEvent event) {

		List disenoEpos = null;
		List imagenesSecciones = null;
		List imagenesBanners = null;
		List disenoIFs = null;
		List conveniosIFs = null;
		try {
			ServletContext application = event.getServletContext();
			String  strDirectorioPublicacion = application.getRealPath("/") + "/";
			
		   if (SystemUtils.IS_OS_UNIX) {
			   String rutaProcesosExternos = ParametrosGlobalesApp.getInstance().getParametro("RUTA_ABSOLUTA_PROCESOS_EXTERNOS");
				String contenidoDirApp = 
						"#Contenido generado por la aplicacion de N@E." + (new java.util.Date()) + "\n" +
						"DIRAPP=" + strDirectorioPublicacion;
				//Files.write(Paths.get(rutaProcesosExternos + "/_DIRAPP.sh"), contenidoDirApp.getBytes());
			  // this.copiarArchivosShell(strDirectorioPublicacion + "WEB-INF/procesos_java/", rutaProcesosExternos + "/procesos_java/");
			  // this.copiarArchivosShell(strDirectorioPublicacion + "WEB-INF/procesos/", rutaProcesosExternos + "/procesos/");
			}
			
			com.netro.seguridadbean.Seguridad beanSeguridad = ServiceLocator.getInstance().lookup("SeguridadEJB", com.netro.seguridadbean.Seguridad.class);
	
			beanSeguridad.crearDatos();
			
			Menu.generarMenus(false);	//false = no forzar regeneraci�n de menus

			//Carga de Mensajes parametrizados por idioma
			application.setAttribute("mensaje_param", new MensajeParam());
			//Carga de Mensajes parametrizados por idioma


			AccesoDB con = new AccesoDB();
			String urlHttp = "";

			try {
				con.conexionDB();
				String strSQL = 
						" SELECT " +
						" cg_url_http " +
						" FROM com_param_gral " +
						" WHERE ic_param_gral = ? ";
				PreparedStatement ps = con.queryPrecompilado(strSQL);
				ps.setInt(1, 1);
				ResultSet rs = ps.executeQuery();
				rs.next();
				urlHttp = (rs.getString("cg_url_http")==null)?"":rs.getString("cg_url_http");
				
				rs.close();
				ps.close();

			} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
			}

			
			
			//---------------- Carga de atributos de aplicacion -------------

			//Ruta fisica del directorio de la aplicacion. p.e. /oracle/ias9i/j2ee/OC4J_Nafin/applications/nafin/nafin-web/
			application.setAttribute("strDirectorioPublicacion", strDirectorioPublicacion);
			//URL del servidor. Sirve para obtener los XMLs via HTTP. p.e. http://wasdes2.nafin.com:85
			application.setAttribute("URL_HTTP",urlHttp);

			//---------------------------------------------------------------
			String ruta = application.getRealPath("/") +  "/";
			//---------------------------------------------------------------

			
			imagenesSecciones = ImagenSeccion.getInfoImagenesSecciones();
			
			

			Iterator it = imagenesSecciones.iterator();
            Map<Integer, String> mapSecciones = new HashMap<>();
			while (it.hasNext()) {
				ImagenSeccion imagenSeccion = (ImagenSeccion)it.next();
				String claveSeccion = imagenSeccion.getClaveSeccion();
			    mapSecciones.put(Integer.parseInt(claveSeccion),imagenSeccion.getNombreSeccion());
				String rutaSeccion = ruta + "home/img/menu/";
				File archivoImgSeccion = new File(rutaSeccion + claveSeccion + ".jpg");
				boolean sincronizarImagenes = false;
				if (archivoImgSeccion.exists()) {
					java.util.Date fechaFS = new java.util.Date(archivoImgSeccion.lastModified());
					//Fecha registrada en el sistema de archivos(FS) es menor que la BD se debe actualizar las imagenes
					if(fechaFS.compareTo(imagenSeccion.getFechaModificacion()) < 0 ) {
						sincronizarImagenes = true;
					} else {
						sincronizarImagenes = false;
					}
				} else {
					sincronizarImagenes = true;
				}

				if (sincronizarImagenes) {
					log.trace("Sincronizando Imagen Seccion: " + claveSeccion);
					imagenSeccion.getImagenBDaFS(rutaSeccion);
				}
			}
		    application.setAttribute("mapSecciones", mapSecciones);


			//------------------------  IMAGENES BANNER HOME ------------------------------
			String rutaBanner = ruta + "home/img/Banners/";
			if (Banner.sincronizarBannersRequerido(rutaBanner)) { //Si es necesario sincronizar las imagenes de banners de BD con el FS
				Banner.getImagenesBDaFS(rutaBanner);
			}
			Map banners = Banner.getBannersInfo();
			application.setAttribute("appBanners",banners);
			
/*			imagenesBanners = Banner.getInfoImagenesBanners();
			
			it = imagenesBanners.iterator();
			Map nombreBanners = new HashMap();
			int numReg = 0;
			while (it.hasNext()) {
				numReg++;
				Banner banner = (Banner)it.next();
				String claveBanner = banner.getClaveBanner();
				nombreBanners.put(new Integer(numReg), banner.getNombreBanner());
				String rutaBanner = ruta + "home/img/Banners/";
				File archivoImgBanner = new File(rutaBanner + "banner" + numReg + ".jpg");
				boolean sincronizarImagenes = false;
				if (archivoImgBanner.exists()) {
					java.util.Date fechaFS = new java.util.Date(archivoImgBanner.lastModified());
					//Fecha registrada en el sistema de archivos(FS) es menor que la BD se debe actualizar las imagenes
					if(fechaFS.compareTo(banner.getFechaModificacion()) < 0 ) {
						sincronizarImagenes = true;
					} else {
						sincronizarImagenes = false;
					}
				} else {
					sincronizarImagenes = true;
				}

				if (sincronizarImagenes) {
					System.out.println("Sincronizando Imagen Banner: " + numReg);
					banner.getImagenBDaFS(rutaBanner);
				}
			}
			application.setAttribute("appNombreBanners",nombreBanners);
*/		
			
			//---------------------------------------------------------------
            // se incorpora la condici�n del mecanismo login para que en desarrollo no tarde demasiado en levantar la aplicaci�n		
            String mecanismo = ParametrosGlobalesApp.getInstance().getParametro("MECANISMO_LOGIN");           
            if (!"DUMMY".equals(mecanismo)) {

                disenoEpos = DisenoEpo.getInfoDisenosEpos();

                it = disenoEpos.iterator();
                while (it.hasNext()) {
                    DisenoEpo diseno = (DisenoEpo) it.next();
                    String claveEpo = diseno.getClaveEpo();
                    String rutaEpo = ruta + "00utils/css/" + claveEpo + "/";
                    String imgEpo = rutaEpo + "imgs/";
                    if (diseno.getBorrado()) {
                        log.trace("Eliminando dise�o EPO " + claveEpo);
                        diseno.eliminarFS(ruta);
                        File fRuta = new File(rutaEpo);
                        Comunes.deleteDirectory(fRuta);
                    } else {
                        File archivoLogo =
                            new File(ruta + "00archivos/15cadenas/15archcadenas/logos/" + claveEpo + ".gif");
                        boolean sincronizarImagenes = false;
                        if (archivoLogo.exists()) {
                            java.util.Date fechaFS = new java.util.Date(archivoLogo.lastModified());
                            //Fecha registrada en el sistema de archivos(FS) es menor que la BD se debe actualizar las imagenes
                            if (fechaFS.compareTo(diseno.getFechaModificacion()) < 0) {
                                sincronizarImagenes = true;
                            } else {
                                sincronizarImagenes = false;
                            }
                        } else {
                            sincronizarImagenes = true;
                        }

                        if (sincronizarImagenes) {
                            log.trace("Sincronizando dise�o EPO: " + claveEpo);
                            Comunes.crearDirectorios(rutaEpo);
                            Comunes.crearDirectorios(imgEpo);
                            diseno.descomprimirArchivosDeBD(ruta, "FINAL");
                            File fRuta = new File(rutaEpo);
                            if (fRuta != null && fRuta.exists()) {
                                boolean siHayArchivos = false;
                                File[] files = fRuta.listFiles();
                                for (int i = 0; i < files.length; i++) {
                                    if (files[i].isDirectory()) {
                                        continue;
                                    } else {
                                        siHayArchivos = true;
                                        break;
                                    }
                                }
                                if (!siHayArchivos) {
                                    Comunes.deleteDirectory(fRuta);
                                }
                            }
                        }
                    }
                }

            }
                        
                        
			///ACTUALIZACI�N DE IMAGENES DE IF�S
			
			disenoIFs = DisenoIF.getInfoDisenosIfs();
			Iterator itIF = disenoIFs.iterator();
			while(itIF.hasNext()){
				DisenoIF disenoI = (DisenoIF)itIF.next();
				String claveIF = disenoI.getClaveIF();
				File archivoLogo = new File(ruta+"00archivos/if/logos/"+claveIF+".gif");
				boolean sincronizarImagenes = false;
				if (archivoLogo.exists()) {
					java.util.Date fechaFS = new java.util.Date(archivoLogo.lastModified());
					//Fecha registrada en el sistema de archivos(FS) es menor que la BD se debe actualizar las imagenes
					if(fechaFS.compareTo(disenoI.getFechaModificacion()) < 0 ) {
						sincronizarImagenes = true;
					} else {
						sincronizarImagenes = false;
					}
				} else {
					sincronizarImagenes = true;
				}
				if (sincronizarImagenes) {
					log.trace("Sincronizando dise�o if: " + claveIF);
					disenoI.descomprimirArchivosDeBD(ruta,"GIF");
				}
			}//fin while
			
			///ACTUALIZACI�N DE CONVENIOS DE IF�S
			
			conveniosIFs = DisenoIF.getInfoconveniosIfs();
			Iterator convIF = conveniosIFs.iterator();
			while(convIF.hasNext()){
				DisenoIF convenio = (DisenoIF)convIF.next();
				String claveIF = convenio.getClaveIF();
				String claveEPO = convenio.getClaveEPO();
				File archivoconvenio = new File(ruta+"00archivos/if/conv/"+claveIF+"_"+claveEPO+".pdf");
				boolean sincronizarImagenes = false;
				if (archivoconvenio.exists()) {
					java.util.Date fechaFS = new java.util.Date(archivoconvenio.lastModified());
					//Fecha registrada en el sistema de archivos(FS) es menor que la BD se debe actualizar las imagenes
					if(fechaFS.compareTo(convenio.getFechaModificacion()) < 0 ) {
						sincronizarImagenes = true;
					} else {
						sincronizarImagenes = false;
					}
				} else {
					sincronizarImagenes = true;
				}
				if (sincronizarImagenes) {
					log.trace("Sincronizando convenio if: " + claveIF);
					log.trace("Sincronizando convenio epo: " + claveEPO);
					convenio.descomprimirArchivosDeBD(ruta,"PDF");
				}
			}//fin while
			
			
			try {
				con.conexionDB();
				String strSQL = 
						" SELECT " +
						" cg_texto " +
						" FROM admcon_texto_logo " +
						" WHERE cg_texto IS NOT NULL ";
				Registros regTextoLogo = con.consultarDB(strSQL);
				application.setAttribute("regTextoLogo",regTextoLogo);
				
				
				strSQL = 
						" SELECT cg_nombre " +
						" FROM admcon_seccion " +
						" ORDER BY ic_seccion ";
						
				Registros regSecciones = con.consultarDB(strSQL);
				application.setAttribute("regSecciones",regSecciones);
				
				//////////////////FALTA codigo para sincronizar imagenes de las secciones....en home/img/menu
				
				/**********************Manuales por Perfil ***************************/
				 
				netropology.utilerias.ManualesPerfilEpo manuales = new netropology.utilerias.ManualesPerfilEpo();
				manuales.consultarImagenTodos(strDirectorioPublicacion);
				
						
			} finally {
				if (con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
			}

		} catch (Throwable e) {
			e.printStackTrace();
			throw new AppException("Error al inicializar la aplicacion.", e);
		}
	}
	
	/**
	 * Copia los archivos .sh y .sql a la ruta especificada y establece los permisos de ejecucion al propietario
	 * @param rutaDir Ruta del directorio de donde se copian los archivos
	 */
	private void copiarArchivosShell(String desdeRutaDir, final String aRutaDir) throws IOException{
		Path start = Paths.get(desdeRutaDir);
		Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
					//System.out.println(file);
					if (file.toString().toLowerCase().endsWith(".sh") || file.toString().toLowerCase().endsWith(".sql")) {
						String contents = new String(Files.readAllBytes(file));  //Esto se puede hacer porque los archivos *.sh son peque�os y por lo tanto no representan un problema de memoria
						Path destino = Paths.get(aRutaDir + "/" + file.getFileName());
						Files.write(destino, contents.replace("\r", "").getBytes());
						if (file.toString().toLowerCase().endsWith(".sh")) {
							Files.setPosixFilePermissions(destino, InicializacionContextListener.PERMS);
						}
					}
					return FileVisitResult.CONTINUE;
				}
		});
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {}
}