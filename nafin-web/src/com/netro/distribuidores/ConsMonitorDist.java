package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsMonitorDist implements IQueryGeneratorRegExtJS {
public ConsMonitorDist(){}

	private List 	conditions;
	StringBuffer strQuery = new StringBuffer();
	private static final Log log = ServiceLocator.getInstance().getLog(ConsMonitorDist.class);//Variable para enviar mensajes al log.
	
	private String fecha_sol;
	private String ic_epo;
	private String tipoSolic;
	private String Eestatus;
	private String ic_moneda;
	private String ic_producto_nafin;
	private String ic_pyme;
	private String ic_if;

	public String getAggregateCalculationQuery() {
		return "";
 	} 
	
	public String getDocumentQuery(){
		conditions = new ArrayList();	
		strQuery 		= new StringBuffer(); 
		log.debug("getDocumentQuery)"+strQuery.toString()); 
		log.debug("getDocumentQuery)"+conditions);
		return strQuery.toString();
 	}  
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
		strQuery = new StringBuffer(); 
		log.debug("getDocumentSummaryQueryForIds "+strQuery.toString()); 
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
		return strQuery.toString();
 	}
	
	public String getDocumentQueryFile(){
		conditions = new ArrayList();
		strQuery = new StringBuffer(); 
		String		condicion 		= "";
		String		condicion2 		= "";
		String lsOrden = "";
		
				strQuery.append(
					" SELECT cg_tipo_solicitud,"+   
					" DECODE (lc.cg_tipo_solicitud, 'I', 'Inicial', 'R', 'Renovacion', 'Ampliacion') AS tipo_solicitud,"   +
					"          lc.ic_linea_credito,"   +
					"          TO_CHAR (lc.df_captura, 'dd/mm/yyyy') AS fecha_captura,"   +
					"          el.cd_descripcion AS estatus, lc.ic_estatus_linea,"   +
					"          lc.fn_monto_autorizado, p.in_plazo_dias AS plazo,"   +
					"          TO_CHAR (lc.df_vencimiento, 'dd/mm/yyyy') AS fecha_ven,"   +
					"          TO_CHAR (lc.df_vencimiento, 'DD-MON-YY') AS fecha_venc,"   +
					"          f.cd_nombre AS banco_servicio, lc.cg_numero_cuenta,"   +
					"          lc.cs_aceptacion_pyme, lc.cg_causa_rechazo,"   +
					"          i.cg_razon_social AS nombre_if, py.in_numero_sirac, ve.epo_rel,"   +
					"          e.cg_razon_social AS nombre_epo,"   +
					"          TO_CHAR (lc.df_autorizacion_if, 'dd/mm/yyyy') AS df_autorizacion_if,"   +
					"          TO_CHAR (lc.df_autorizacion_if, 'DD-MON-YY') AS df_autorizacion_ifc,"   +
					"          lc.fn_saldo_linea_total, lc.fn_monto_autorizado_total,"   +
					"          ve.ic_if AS ic_if, pn.ic_nombre AS producto,"   +
					"          TO_CHAR (lc.df_captura, 'dd/mm/yyyy') AS df_captura,"   +
					"          TO_CHAR (lc.df_limite_disposicion, 'dd/mm/yyyy') AS df_disposicion,"   +
					"          TO_CHAR (lc.df_captura, 'DD-MON-YY') AS df_capturac,"   +
					"          TO_CHAR (lc.df_limite_disposicion, 'DD-MON-YY') AS df_disposicionc,"   +
					"          cm.cd_nombre AS moneda, NVL (lc.fn_saldo_linea, 0) AS remanente,"   +
					"          NVL (lc.fn_monto_autorizado - lc.fn_saldo_linea, 0) AS ejercido,"   +
					"          lc.cg_numero_cuenta_if, tci.cd_descripcion AS tipo_cobro,"   +
					"          lc.ic_producto_nafin AS cve_producto, lc.cg_cuenta_clabe,"   +
					"          (SELECT SUM (fn_monto_autorizado_total)"   +
					"          		FROM com_linea_credito lc2"   +
					"            	WHERE cg_tipo_solicitud <> 'A'"   +
					"              AND df_vencimiento_adicional > SYSDATE"   +
					"           AND ic_if NOT IN (lc.ic_if)"   +
					"           AND lc.ic_pyme = lc2.ic_pyme) AS lineas_otros,"   +
					"          	lc.ic_moneda, TO_CHAR (lc.df_cambio, 'dd/mm/yyyy') AS fecha_cambio,"   +
					"          	i.ig_tipo_piso AS tipo_piso, ta.cd_descripcion AS tipo_amort,"   +
					"          	lc.fn_monto_liberado AS monto_liberado, lc.cg_linea_tipo AS tipo_linea,"   +
					"          	lc.ic_tabla_amort, ta.ic_tabla_amort,"   +
					"          	TO_CHAR (lc.df_autorizacion, 'dd/mm/yyyy') AS df_autorizacion,"   +
					"          	TO_CHAR (lc.df_autorizacion, 'DD-MON-YY') AS df_autorizacionc"   +
					"     FROM com_linea_credito lc,"   +
					"          comcat_estatus_linea el,"   +
					"          comcat_if i,"   +
					"          comcat_tipo_cobro_interes tci,"   +
					"          comrel_pyme_epo_x_producto pp,"   +
					"          comcat_financiera f,"   +
					"          comcat_plazo p,"   +
					"          comcat_epo e,"   +
					"          comrel_producto_epo pe,"   +
					"          comrel_if_epo_x_producto iep,"   +
					"          comcat_pyme py,"   +
					"          comcat_producto_nafin pn,"   +
					"          comcat_tabla_amort ta,"   +
					"          comcat_moneda cm,"   +
					"          (SELECT   COUNT (pexp.ic_epo) AS epo_rel, pexp.ic_pyme,"   +
					"                    iexp.ic_if AS ic_if, pexp.ic_producto_nafin AS producto"   +
					"               FROM comrel_pyme_epo_x_producto pexp,"   +
					"                    comrel_if_epo_x_producto iexp"   +
					"              WHERE pexp.cs_habilitado IN ('S', 'H')"   +
					"                AND pexp.ic_epo = iexp.ic_epo"   );
				
					
					
					if(ic_producto_nafin != null && !"0".equals(ic_producto_nafin)){					
						strQuery.append(" AND pexp.ic_producto_nafin = ?");
						conditions.add(this.ic_producto_nafin);
					}
					else {
						strQuery.append(" AND pexp.ic_producto_nafin in (2,5)  ");
					}
			
					strQuery.append(" AND iexp.cs_habilitado IN ('S')"   +
						"                AND iexp.ic_producto_nafin = pexp.ic_producto_nafin"   +
						"           GROUP BY pexp.ic_pyme, iexp.ic_if, pexp.ic_producto_nafin) ve"   +
						"    WHERE lc.ic_pyme = ?"  );
					conditions.add(this.ic_pyme);
					
					strQuery.append("      AND pp.ic_pyme = lc.ic_pyme"   +
					"      AND pp.ic_epo = lc.ic_epo"   +
					"      AND py.ic_pyme = lc.ic_pyme"   +
					"      AND el.ic_estatus_linea = lc.ic_estatus_linea"   +
					"      AND lc.ic_financiera = f.ic_financiera(+)"   +
					"      AND lc.ic_plazo = p.ic_plazo(+)"   +
					"      AND lc.ic_epo = e.ic_epo"   +
					"      AND e.cs_habilitado = 'S'"   +
					"      AND e.ic_epo = pe.ic_epo"   +
					"      AND lc.ic_producto_nafin = pp.ic_producto_nafin"   +
					"      AND pp.ic_producto_nafin = pn.ic_producto_nafin"   +
					"      AND pp.ic_producto_nafin = pe.ic_producto_nafin"   +
					"      AND pp.ic_producto_nafin = iep.ic_producto_nafin"   +
					"      AND lc.ic_moneda = cm.ic_moneda"   +
					"      AND pe.cs_habilitado = 'S'"   +
					"      AND i.cs_habilitado = 'S'"   +
					"      AND i.ic_if = iep.ic_if"   +
					"      AND e.ic_epo = iep.ic_epo"   +
					"      AND iep.cs_habilitado = 'S'"   +
					"      AND i.ic_if = lc.ic_if"   +
					"      AND lc.ic_pyme = ve.ic_pyme(+)"   +
					"      AND lc.ic_if = ve.ic_if(+)"   +
					"      AND lc.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes(+)"   +
					"      AND lc.ic_producto_nafin = ve.producto"   +
					"      AND lc.ic_tabla_amort = ta.ic_tabla_amort(+)"  
				
				);
		
				if(ic_producto_nafin != null && !"0".equals(ic_producto_nafin)){
					strQuery.append("   AND lc.ic_producto_nafin = ? ");
					conditions.add(this.ic_producto_nafin);
				}
				else {
					strQuery.append("   AND lc.ic_producto_nafin in (2,5)");
				}
			
				if(fecha_sol != null && !"".equals(fecha_sol)){
					strQuery.append(" AND to_date(to_char(lc.df_captura,'dd/mm/yyyy'),'dd/mm/yyyy') <= to_date(?,'dd/mm/yyyy')");
					conditions.add(this.fecha_sol);
				}
				
				else{
					strQuery.append(" AND lc.df_captura between add_months(sysdate,-3) and sysdate");
				}
				
				if(ic_epo != null && !"".equals(ic_epo)){
					strQuery.append(" AND pp.ic_epo = ?");
					conditions.add(this.ic_epo);
				}
				
				if( tipoSolic != null && !"".equals(tipoSolic)){
					strQuery.append(" AND lc.cg_tipo_solicitud = ?");
					conditions.add(this.tipoSolic);			
				}
				
				if( Eestatus != null &&  !"".equals(Eestatus)){
						strQuery.append("  AND lc.ic_estatus_linea = ?");
						conditions.add(this.Eestatus);	
				}
				
				if(ic_moneda != null && !"".equals(ic_moneda)){
						strQuery.append("   AND  lc.ic_moneda = ? ");
						conditions.add(this.ic_moneda);
				}
				
				if(ic_if != null && !"".equals(ic_if)){
					strQuery.append(" AND lc.ic_if = ?");
					conditions.add(this.ic_if);
							
					if (ic_producto_nafin != null && "5".equals(ic_producto_nafin))
						lsOrden = " order by lc.IC_PRODUCTO_NAFIN DESC,lc.ic_linea_credito";
					else
						lsOrden = " order by lc.IC_PRODUCTO_NAFIN,lc.ic_linea_credito";
				}		
		
		
		log.debug("getDocumentQueryFile "+strQuery.toString());
		log.debug("getDocumentQueryFile)"+conditions);
		log.debug("getDocumentQueryFile)"+conditions);
		return strQuery.toString();
	}

	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {

		String nombreArchivo = "";

		if ("PDF".equals(tipo)) {
			try{

				HttpSession session = request.getSession();
				String tipoUsuario = (String)session.getAttribute("strTipoUsuario");
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";

				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
				if(!tipoUsuario.equals("NAFIN") ){
					pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					session.getAttribute("iNoNafinElectronico").toString(),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				}else{
					pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"), " ",
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				}

				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				pdfDoc.addText("Resumen de Pr�stamos ","formas",ComunesPDF.CENTER);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

				pdfDoc.setLTable(12, 100);
				pdfDoc.setLCell("Tipo de Solicitud","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Nombre del IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Captura","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de Cobro de interes","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Autorizado","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Banco de Servicio","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Cuenta Propia Num. Cuenta","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("IF Num. Cuenta","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Causa Rechazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setLHeaders();
				double montoTotalMN = 0;
				double montoTotalDL = 0;
				double montoAutorizado = 0;
				int numRegistrosMN = 0;
				int numRegistrosDL = 0;
				while (rs.next()) {
					String tipoSolicitud    = (rs.getString("TIPO_SOLICITUD")      == null) ? "N/A" : rs.getString("TIPO_SOLICITUD");
					String nombreIf         = (rs.getString("NOMBRE_IF")           == null) ? "N/A" : rs.getString("NOMBRE_IF");
					String moneda           = (rs.getString("MONEDA")              == null) ? "N/A" : rs.getString("MONEDA");
					String fechaCaptura     = (rs.getString("FECHA_CAPTURA")       == null) ? "N/A" : rs.getString("FECHA_CAPTURA");
					String estatus          = (rs.getString("ESTATUS")             == null) ? "N/A" : rs.getString("ESTATUS");
					String tipoCobro        = (rs.getString("TIPO_COBRO")          == null) ? "N/A" : rs.getString("TIPO_COBRO");
					String fechaVencimiento = (rs.getString("FECHA_VEN")           == null) ? "N/A" : rs.getString("FECHA_VEN");
					String bancoServicio    = (rs.getString("BANCO_SERVICIO")      == null) ? "N/A" : rs.getString("BANCO_SERVICIO");
					String cuentaPropia     = (rs.getString("CG_NUMERO_CUENTA")    == null) ? "N/A" : rs.getString("CG_NUMERO_CUENTA");
				   String cuentaIF         = (rs.getString("CG_NUMERO_CUENTA_IF") == null) ? "N/A" : rs.getString("CG_NUMERO_CUENTA_IF");
					String causaRechazo     = (rs.getString("CG_CAUSA_RECHAZO")    == null) ? "N/A" : rs.getString("CG_CAUSA_RECHAZO");
					String ic_moneda        = (rs.getString("IC_MONEDA")           == null) ? ""    : rs.getString("IC_MONEDA");
					montoAutorizado         = Double.parseDouble((rs.getString("FN_MONTO_AUTORIZADO")== null)?"":rs.getString("FN_MONTO_AUTORIZADO"));
					if("1".equals(ic_moneda)){
						numRegistrosMN ++;
						montoTotalMN = montoTotalMN + montoAutorizado;
					}else{
						numRegistrosDL ++;
						montoTotalDL = montoTotalDL + montoAutorizado;
					}
					pdfDoc.setLCell(tipoSolicitud,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(nombreIf,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaCaptura,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(estatus,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipoCobro,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoAutorizado,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(bancoServicio,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(cuentaPropia,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(cuentaIF,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(causaRechazo,"formas",ComunesPDF.CENTER);

				}
				pdfDoc.addLTable();
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
				pdfDoc.setLTable(12, 100);
				pdfDoc.setLHeaders();
				if(numRegistrosMN>0){
					pdfDoc.setLCell("Total de Documentos en Moneda Nacional","celda01",ComunesPDF.RIGHT,6);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoTotalMN,2),"formas",ComunesPDF.RIGHT,6);
				}
				if(numRegistrosDL>0){
					pdfDoc.setLCell("Total de Documentos en Dolares","celda01",ComunesPDF.RIGHT,6);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoTotalDL,2),"formas",ComunesPDF.RIGHT,6);
				}
				pdfDoc.addLTable();
				pdfDoc.endDocument();
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo", e);
		} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		}
		return nombreArchivo;
	}


	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}	
	
	public void setFecha_sol(String fecha_sol) {
		this.fecha_sol = fecha_sol;
	}


	public String getFecha_sol() {
		return fecha_sol;
	}


	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}


	public String getIc_epo() {
		return ic_epo;
	}


	public void setTipoSolic(String tipoSolic) {
		this.tipoSolic = tipoSolic;
	}


	public String getTipoSolic() {
		return tipoSolic;
	}


	public void setEestatus(String Eestatus) {
		this.Eestatus = Eestatus;
	}


	public String getEestatus() {
		return Eestatus;
	}


	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}


	public String getIc_moneda() {
		return ic_moneda;
	}


	public void setIc_producto_nafin(String ic_producto_nafin) {
		this.ic_producto_nafin = ic_producto_nafin;
	}


	public String getIc_producto_nafin() {
		return ic_producto_nafin;
	}


	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}


	public String getIc_pyme() {
		return ic_pyme;
	}


	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}


	public String getIc_if() {
		return ic_if;
	}
	public List getConditions() {
		return conditions;
	}
}

