package com.netro.distribuidores;

import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class RepDoctoSolicxEstatusDistBean  {

	private int estatusDocto;
	private String estatusSolic;
	private String claveDocto;

	//Constructor 
	public RepDoctoSolicxEstatusDistBean() {}
		
	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(RepDoctoSolicxEstatusDistBean.class);
	
	private String campos[]={
			"  PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"+
			" 	,D.IG_NUMERO_DOCTO"+
			" 	,D.CC_ACUSE"+
			"	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"+
			"	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"+
			"	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"+
			"	,D.IG_PLAZO_DOCTO"+
			"	,M.CD_NOMBRE AS MONEDA"+
			"	,D.FN_MONTO"+
					"	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
					"	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
			"	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"+
			"	,decode(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) AS FN_PORC_DESCUENTO"+
			"	,TF.CD_DESCRIPCION AS MODO_PLAZO"+
			"	,ED.CD_DESCRIPCION AS ESTATUS"+
			"	,M.ic_moneda"+
			"	,D.ic_documento"+
			"	,E.cg_razon_social as NOMBRE_EPO"+
			"	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
			" ,d.CG_VENTACARTERA as CG_VENTACARTERA ",//Para 2D
			
			"  PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"+
			"  	,D.IG_NUMERO_DOCTO"+
			"  	,D.CC_ACUSE"+
			" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"+
			" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"+
			" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"+
			" 	,D.IG_PLAZO_DOCTO"+
			" 	,M.CD_NOMBRE AS MONEDA"+
			" 	,D.FN_MONTO"+
			" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
			" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
			" 	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"+
			"	,decode(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) AS FN_PORC_DESCUENTO"+
			" 	,TF.CD_DESCRIPCION AS MODO_PLAZO"+
			" 	,ED.CD_DESCRIPCION AS ESTATUS"+
			" 	,M.ic_moneda"+
			" 	,D.ic_documento"+
			" 	,E.cg_razon_social as NOMBRE_EPO"+
			" 	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
			" 	,I.cg_razon_social as NOMBRE_IF"   +
			" 	,decode(PP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"   +
			" 	,DS.fn_importe_recibir as MONTO_CREDITO"   +
			" 	,D.ig_plazo_credito"   +
			" 	,TCI.cd_descripcion as TIPO_COBRO_INT"   +
			"  	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) as REFERENCIA_INT"+
			" 	,DS.FN_VALOR_TASA as VALOR_TASA_INT"   +
			" 	,DS.fn_importe_interes as MONTO_TASA_INT"   +
			"	,to_char(DS.df_fecha_seleccion,'dd/mm/yyyy') as FECHA_SELECCION "+
			"	,to_char(D.df_fecha_venc_credito,'dd/mm/yyyy') as FECHA_VENC_CREDITO"+
			"	,LC.ic_moneda as MONEDA_LINEA"+
			" ,d.CG_VENTACARTERA as CG_VENTACARTERA ",//Para 3D
					
			"  PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"   +
			"  	,D.IG_NUMERO_DOCTO"   +
			"  	,D.CC_ACUSE"   +
			" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"   +
			" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"   +
			" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"   +
			" 	,D.IG_PLAZO_DOCTO"   +
			" 	,M.CD_NOMBRE AS MONEDA"   +
			" 	,D.FN_MONTO"   +
			" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
			" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
			" 	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"   +
			"	,decode(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) AS FN_PORC_DESCUENTO"+
			" 	,TF.CD_DESCRIPCION AS MODO_PLAZO"   +
			" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
			" 	,M.ic_moneda"   +
			" 	,D.ic_documento"   +
			" 	,E.cg_razon_social as NOMBRE_EPO"   +
			" 	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"   +
			" 	,I.cg_razon_social as NOMBRE_IF"   +
			" 	,decode(PP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"   +
			" 	,DS.fn_importe_recibir as MONTO_CREDITO"   +
			" 	,D.ig_plazo_credito"   +
			" 	,TCI.cd_descripcion as TIPO_COBRO_INT"   +
			"  	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) as REFERENCIA_INT"+
			" 	,DS.FN_VALOR_TASA as VALOR_TASA_INT"   +
			" 	,DS.fn_importe_interes as MONTO_TASA_INT"   +
			"	,to_char(DS.df_fecha_seleccion,'dd/mm/yyyy') as FECHA_SELECCION "+
			"	,to_char(D.df_fecha_venc_credito,'dd/mm/yyyy') as FECHA_VENC_CREDITO"+
			"	,to_char(CA.df_fecha_hora,'dd/mm/yyyy') as FECHA_OPER_IF"+
			"	,LC.ic_moneda as MONEDA_LINEA"+
			" ,d.CG_VENTACARTERA as CG_VENTACARTERA ",//Para 1C	
			
			"  PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"   +
			"  	,D.IG_NUMERO_DOCTO"   +
			"  	,D.CC_ACUSE"   +
			" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"   +
			" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"   +
			" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"   +
			" 	,D.IG_PLAZO_DOCTO"   +
			" 	,M.CD_NOMBRE AS MONEDA"   +
			" 	,D.FN_MONTO"   +
			" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
			" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
			" 	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"   +
			"	,decode(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) AS FN_PORC_DESCUENTO"+
			" 	,TF.CD_DESCRIPCION AS MODO_PLAZO"   +
			" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
			" 	,M.ic_moneda"   +
			" 	,D.ic_documento"   +
			" 	,E.cg_razon_social as NOMBRE_EPO"   +
			" 	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"   +
			" 	,I.cg_razon_social as NOMBRE_IF"   +
			" 	,decode(PP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"   +
			" 	,DS.fn_importe_recibir as MONTO_CREDITO"   +
			" 	,D.ig_plazo_credito"   +
			" 	,TCI.cd_descripcion as TIPO_COBRO_INT"   +
			"  	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) as REFERENCIA_INT"+
			" 	,DS.FN_VALOR_TASA as VALOR_TASA_INT"   +
			" 	,DS.fn_importe_interes as MONTO_TASA_INT"   +
			"	,to_char(DS.df_fecha_seleccion,'dd/mm/yyyy') as FECHA_SELECCION "+
			"	,to_char(D.df_fecha_venc_credito,'dd/mm/yyyy') as FECHA_VENC_CREDITO"+
			"	,to_char(CA.df_fecha_hora,'dd/mm/yyyy') as FECHA_OPER_IF"+
			"	,LC.ic_moneda as MONEDA_LINEA"+
			" ,d.CG_VENTACARTERA as CG_VENTACARTERA ",//Para 2C
			
			"  PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"   +
			"  	,D.IG_NUMERO_DOCTO"   +
			"  	,D.CC_ACUSE"   +
			" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"   +
			" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"   +
			" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"   +
			" 	,D.IG_PLAZO_DOCTO"   +
			" 	,M.CD_NOMBRE AS MONEDA"   +
			" 	,D.FN_MONTO"   +
			" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
			" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
			" 	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"   +
			"	,decode(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) AS FN_PORC_DESCUENTO"+
			" 	,TF.CD_DESCRIPCION AS MODO_PLAZO"   +
			" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
			" 	,M.ic_moneda"   +
			" 	,D.ic_documento"   +
			" 	,E.cg_razon_social as NOMBRE_EPO"   +
			" 	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"   +
			" 	,I.cg_razon_social as NOMBRE_IF"   +
			" 	,decode(PP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"   +
			" 	,DS.fn_importe_recibir as MONTO_CREDITO"   +
			" 	,D.ig_plazo_credito"   +
			" 	,TCI.cd_descripcion as TIPO_COBRO_INT"   +
			"  	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) as REFERENCIA_INT"+
			" 	,DS.FN_VALOR_TASA as VALOR_TASA_INT"   +
			" 	,DS.fn_importe_interes as MONTO_TASA_INT"   +
			"	,to_char(DS.df_fecha_seleccion,'dd/mm/yyyy') as FECHA_SELECCION "+
			"	,to_char(D.df_fecha_venc_credito,'dd/mm/yyyy') as FECHA_VENC_CREDITO "+
			"	,to_char(CA.df_fecha_hora,'dd/mm/yyyy') as FECHA_OPER_IF "+
			"	,LC.ic_moneda as MONEDA_LINEA"+
			" ,d.CG_VENTACARTERA as CG_VENTACARTERA ",///PARA 3C
			
			"  PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"   +
			"  	,D.IG_NUMERO_DOCTO"   +
			"  	,D.CC_ACUSE"   +
			" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"   +
			" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"   +
			" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"   +
			" 	,D.IG_PLAZO_DOCTO"   +
			" 	,M.CD_NOMBRE AS MONEDA"   +
			" 	,D.FN_MONTO"   +
			" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
			" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
			" 	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"   +
			"	,decode(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) AS FN_PORC_DESCUENTO"+
			" 	,TF.CD_DESCRIPCION AS MODO_PLAZO"   +
			" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
			" 	,M.ic_moneda"   +
			" 	,D.ic_documento"   +
			" 	,E.cg_razon_social as NOMBRE_EPO"   +
			" 	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"   +
			" 	,I.cg_razon_social as NOMBRE_IF"   +
			" 	,decode(PP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"   +
			" 	,DS.fn_importe_recibir as MONTO_CREDITO"   +
			" 	,D.ig_plazo_credito"   +
			" 	,TCI.cd_descripcion as TIPO_COBRO_INT"   +
			"  	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) as REFERENCIA_INT"+
			" 	,DS.FN_VALOR_TASA as VALOR_TASA_INT"   +
			" 	,DS.fn_importe_interes as MONTO_TASA_INT"   +
			"	,to_char(DS.df_fecha_seleccion,'dd/mm/yyyy') as FECHA_SELECCION "+
			"	,to_char(D.df_fecha_venc_credito,'dd/mm/yyyy') as FECHA_VENC_CREDITO "+
			"	,to_char(CA.df_fecha_hora,'dd/mm/yyyy') as FECHA_OPER_IF "+
			"	,LC.ic_moneda as MONEDA_LINEA"+
			" ,d.CG_VENTACARTERA as CG_VENTACARTERA ",//PARA 4C
			
			"  PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"   +
			"  	,D.IG_NUMERO_DOCTO"   +
			"  	,D.CC_ACUSE"   +
			" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"   +
			" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"   +
			" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"   +
			" 	,D.IG_PLAZO_DOCTO"   +
			" 	,M.CD_NOMBRE AS MONEDA"   +
			" 	,D.FN_MONTO"   +
			" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
			" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
			" 	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"   +
			"	,decode(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) AS FN_PORC_DESCUENTO"+
			" 	,TF.CD_DESCRIPCION AS MODO_PLAZO"   +
			" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
			" 	,M.ic_moneda"   +
			" 	,D.ic_documento"   +
			" 	,E.cg_razon_social as NOMBRE_EPO"   +
			" 	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"   +
			" 	,I.cg_razon_social as NOMBRE_IF"   +
			" 	,decode(PP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"   +
			" 	,DS.fn_importe_recibir as MONTO_CREDITO"   +
			" 	,D.ig_plazo_credito"   +
			" 	,TCI.cd_descripcion as TIPO_COBRO_INT"   +
			"  	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) as REFERENCIA_INT"+
			" 	,DS.FN_VALOR_TASA as VALOR_TASA_INT"   +
			" 	,DS.fn_importe_interes as MONTO_TASA_INT"   +
			" 	,CE.CG_NUMERO_PAGO"   +
			"	,to_char(DS.df_fecha_seleccion,'dd/mm/yyyy') as FECHA_SELECCION "+
			"	,to_char(D.df_fecha_venc_credito,'dd/mm/yyyy') as FECHA_VENC_CREDITO "+
			"	,to_char(CA.df_fecha_hora,'dd/mm/yyyy') as FECHA_OPER_IF "+
			"	,LC.ic_moneda as MONEDA_LINEA"+
			" ,d.CG_VENTACARTERA as CG_VENTACARTERA ",//PARA 11D
					
			"  PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"   +
			"  	,D.IG_NUMERO_DOCTO"   +
			"  	,D.CC_ACUSE"   +
			" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"   +
			" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"   +
			" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"   +
			" 	,D.IG_PLAZO_DOCTO"   +
			" 	,M.CD_NOMBRE AS MONEDA"   +
			" 	,D.FN_MONTO"   +
			" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
			" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
			" 	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"   +
			"	,decode(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) AS FN_PORC_DESCUENTO"+
			" 	,TF.CD_DESCRIPCION AS MODO_PLAZO"   +
			" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
			" 	,M.ic_moneda"   +
			" 	,D.ic_documento"   +
			" 	,E.cg_razon_social as NOMBRE_EPO"   +
			" 	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"   +
			" 	,I.cg_razon_social as NOMBRE_IF"   +
			" 	,decode(PP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"   +
			" 	,DS.fn_importe_recibir as MONTO_CREDITO"   +
			" 	,D.ig_plazo_credito"   +
			" 	,TCI.cd_descripcion as TIPO_COBRO_INT"   +
			"  	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) as REFERENCIA_INT"+
			" 	,DS.FN_VALOR_TASA as VALOR_TASA_INT"   +
			" 	,DS.fn_importe_interes as MONTO_TASA_INT"   +
			" 	,CE.CG_NUMERO_PAGO"   +
			"	,to_char(DS.df_fecha_seleccion,'dd/mm/yyyy') as FECHA_SELECCION "+
			"	,to_char(D.df_fecha_venc_credito,'dd/mm/yyyy') as FECHA_VENC_CREDITO "+
			"	,to_char(CA.df_fecha_hora,'dd/mm/yyyy') as FECHA_OPER_IF "+
			"	,LC.ic_moneda as MONEDA_LINEA"+
			" ,d.CG_VENTACARTERA as CG_VENTACARTERA ",//PARA 22D
			
			" PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"+
			" 	,D.IG_NUMERO_DOCTO"+
			" 	,D.CC_ACUSE"+
			"	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"+
			"	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"+
			"	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"+
			"	,D.IG_PLAZO_DOCTO"+
			"	,M.CD_NOMBRE AS MONEDA"+
			"	,D.FN_MONTO"+
					"	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
					"	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
			"	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"+
			"	,decode(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) AS FN_PORC_DESCUENTO"+
			"	,TF.CD_DESCRIPCION AS MODO_PLAZO"+
			"	,ED.CD_DESCRIPCION AS ESTATUS"+
			"	,M.ic_moneda"+
			"	,D.ic_documento"+
			"	,E.cg_razon_social as NOMBRE_EPO"+
			"	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
			" ,d.CG_VENTACARTERA as CG_VENTACARTERA ",//PARA 9D
			
			"  PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"+
			" 	,D.IG_NUMERO_DOCTO"+
			" 	,D.CC_ACUSE"+
			"	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"+
			"	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"+
			"	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"+
			"	,D.IG_PLAZO_DOCTO"+
			"	,M.CD_NOMBRE AS MONEDA"+
			"	,D.FN_MONTO"+
					"	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
					"	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
			"	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"+
			"	,decode(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) AS FN_PORC_DESCUENTO"+
			"	,TF.CD_DESCRIPCION AS MODO_PLAZO"+
			"	,ED.CD_DESCRIPCION AS ESTATUS"+
			"	,M.ic_moneda"+
			"	,D.ic_documento"+
			"	,E.cg_razon_social as NOMBRE_EPO"+
			"	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
			"	,CE.ct_cambio_motivo "+
			" ,d.CG_VENTACARTERA as CG_VENTACARTERA ",//PARA 5D
			
			"  PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"   +
			"  	,D.IG_NUMERO_DOCTO"   +
			"  	,D.CC_ACUSE"   +
			" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"   +
			" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"   +
			" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"   +
			" 	,D.IG_PLAZO_DOCTO"   +
			" 	,M.CD_NOMBRE AS MONEDA"   +
			" 	,D.FN_MONTO"   +
			" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
			" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
			" 	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"   +
			"	,decode(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) AS FN_PORC_DESCUENTO"+
			" 	,TF.CD_DESCRIPCION AS MODO_PLAZO"   +
			" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
			" 	,M.ic_moneda"   +
			" 	,D.ic_documento"   +
			" 	,E.cg_razon_social as NOMBRE_EPO"   +
			" 	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"   +
			" 	,I.cg_razon_social as NOMBRE_IF"   +
			" 	,decode(PP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"   +
			" 	,DS.fn_importe_recibir as MONTO_CREDITO"   +
			" 	,D.ig_plazo_credito"   +
			" 	,TCI.cd_descripcion as TIPO_COBRO_INT"   +
			"  	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) as REFERENCIA_INT"+
			" 	,DS.FN_VALOR_TASA as VALOR_TASA_INT"   +
			" 	,DS.fn_importe_interes as MONTO_TASA_INT"   +
			" 	,CE.ct_cambio_motivo"   +
			"	,to_char(DS.df_fecha_seleccion,'dd/mm/yyyy') as FECHA_SELECCION "+
			"	,to_char(D.df_fecha_venc_credito,'dd/mm/yyyy') as FECHA_VENC_CREDITO"+
			"	,to_char(CE.dc_fecha_cambio,'dd/mm/yyyy') as FECHA_CAMBIO"+
			"	,LC.ic_moneda as MONEDA_LINEA"+
			" ,d.CG_VENTACARTERA as CG_VENTACARTERA ",//PARA 20D
			
			"  PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"+
			" 	,D.IG_NUMERO_DOCTO"+
			" 	,D.CC_ACUSE"+
			"	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"+
			"	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"+
			"	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"+
			"	,D.IG_PLAZO_DOCTO"+
			"	,M.CD_NOMBRE AS MONEDA"+
			"	,D.FN_MONTO"+
					"	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
					"	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
			"	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"+
			"	,decode(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) AS FN_PORC_DESCUENTO"+
			"	,TF.CD_DESCRIPCION AS MODO_PLAZO"+
			"	,ED.CD_DESCRIPCION AS ESTATUS"+
			"	,M.ic_moneda"+
			"	,D.ic_documento"+
			"	,E.cg_razon_social as NOMBRE_EPO"+
			"	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
			" ,d.CG_VENTACARTERA as CG_VENTACARTERA ",//PARA 1D			
      
     " py.cg_razon_social AS nombre_dist, " +
     " d.ig_numero_docto, "+
     " d.cc_acuse, " +
     " TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision, " +
     " TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga, " +
     " TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc, "+
     " d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto, "+
     " DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), 'N', 'Sin conversion', 'P', 'Dolar-Peso', '') AS tipo_conversion, "+
     "  DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), 'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'), '1'  ) AS tipo_cambio, "+
     "  NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento, "+
     " DECODE (d.ic_tipo_financiamiento,  1, NVL (d.fn_porc_descuento, 0), DECODE (d.ic_tipo_financiamiento, 3, NVL (d.fn_porc_descuento, 0), 0  )  ) AS fn_porc_descuento, "+
     " tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus, "+
     " m.ic_moneda, d.ic_documento, e.cg_razon_social AS nombre_epo, "+
     "  DECODE (NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza),  'D', 'Delegada',  'N', 'No Delegada'  ) AS tipo_cobranza, "+
     "  DECODE ( ed.ic_estatus_docto, 32, i2.cg_razon_social, i.cg_razon_social ) AS nombre_if, "+
     " DECODE (pp.cg_tipo_credito, 'D', 'Descuento y/o Factoraje',  'C', 'Credito en Cuenta Corriente'  ) AS tipo_credito, "+
     " ds.fn_importe_recibir AS monto_credito, d.ig_plazo_credito, "+
     " tci.cd_descripcion AS tipo_cobro_int, "+
     "  DECODE (ds.cg_tipo_tasa, 'P', 'Preferencial', 'N', 'Negociada',  ct.cd_nombre || ' ' || ds.cg_rel_mat || ' ' || ds.fn_puntos  ) AS referencia_int,  "+
     "  ds.fn_valor_tasa AS valor_tasa_int, "+
     "  ds.fn_importe_interes AS monto_tasa_int, "+
     " TO_CHAR (ds.df_fecha_seleccion, 'dd/mm/yyyy') AS fecha_seleccion, "+
     " TO_CHAR (d.df_fecha_venc_credito, 'dd/mm/yyyy') AS fecha_venc_credito, "+
     " lc.ic_moneda AS moneda_linea, d.cg_ventacartera AS cg_ventacartera, "+
     " ds.FN_PORC_COMISION_APLI AS comision_aplicable, " +
     " bin.cg_codigo_bin,  bin.cg_descripcion as bin, "+
     " ddp.ic_orden_pago_tc  AS orden_enviado, "+
     " ddp.cg_transaccion_id AS id_operacion, " +
     " ( ddp.cg_codigo_resp || ' ' || ddp.cg_codigo_desc ) AS codigo_autoriza, "+
     " ddp.df_fecha_autorizacion AS fecha_registro, ddp.cg_num_tarjeta as num_tarjeta "
     //32D
      
			
	};

	private String tablas[]={
			"  DIS_DOCUMENTO D"+
			" ,COMCAT_PYME PY"+
			" ,COMCAT_MONEDA M"+
			" ,COMREL_PRODUCTO_EPO PE"+
			" ,COMCAT_PRODUCTO_NAFIN PN"+
			" ,COM_TIPO_CAMBIO TC"+
			" ,COMCAT_TIPO_FINANCIAMIENTO TF"+
			" ,COMCAT_ESTATUS_DOCTO ED"+
			" ,COMCAT_EPO E",//para 2D 0
			
			"  DIS_DOCUMENTO D"+
			"  ,COMCAT_PYME PY"+
			"  ,COMCAT_MONEDA M"+
			"  ,COMREL_PRODUCTO_EPO PE"+
			"  ,COMCAT_PRODUCTO_NAFIN PN"+
			"  ,COM_TIPO_CAMBIO TC"+
			"  ,COMCAT_TIPO_FINANCIAMIENTO TF"+
			"  ,COMCAT_ESTATUS_DOCTO ED"+
			"  ,COMCAT_EPO E"+
			"  ,(select ic_linea_credito,ic_if,ic_tipo_cobro_interes ,'C' as cg_tipo_credito,ic_moneda from com_linea_credito where ic_producto_nafin = 4 "   +
			"    union all"   +
			"    select ic_linea_credito_dm as ic_linea_credito,ic_if,ic_tipo_cobro_interes,'D' as cg_tipo_credito,ic_moneda from dis_linea_credito_dm where ic_producto_nafin = 4 ) LC"   +
			"  ,DIS_DOCTO_SELECCIONADO DS"   +
			"  ,COMCAT_IF I"   +
			"  ,COMREL_PYME_EPO_X_PRODUCTO PP"   +
			"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
			"  ,COMCAT_TASA CT",//Para 3D	1
			
			"  DIS_DOCUMENTO D"   +
			"  ,COMCAT_PYME PY"   +
			"  ,COMCAT_MONEDA M"   +
			"  ,COMREL_PRODUCTO_EPO PE"   +
			"  ,COMCAT_PRODUCTO_NAFIN PN"   +
			"  ,COM_TIPO_CAMBIO TC"   +
			"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
			"  ,COMCAT_ESTATUS_DOCTO ED"   +
			"  ,(select ic_linea_credito,ic_if,ic_tipo_cobro_interes ,'C' as cg_tipo_credito,ic_moneda from com_linea_credito where ic_producto_nafin = 4"   +
			"    union all"   +
			"    select ic_linea_credito_dm as ic_linea_credito,ic_if,ic_tipo_cobro_interes,'D' as cg_tipo_credito,ic_moneda from dis_linea_credito_dm where ic_producto_nafin = 4) LC"   +
			"  ,COMCAT_EPO E"   +
			"  ,DIS_DOCTO_SELECCIONADO DS"   +
			"  ,COMCAT_IF I"   +
			"  ,COMREL_PYME_EPO_X_PRODUCTO PP"   +
			"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
			"  ,COMCAT_TASA CT"   +
			"  ,DIS_SOLICITUD S"   +
			"  ,COM_ACUSE3 CA" ,//PAra 1C 2
			
			"  DIS_DOCUMENTO D"   +
			"  ,COMCAT_PYME PY"   +
			"  ,COMCAT_MONEDA M"   +
			"  ,COMREL_PRODUCTO_EPO PE"   +
			"  ,COMCAT_PRODUCTO_NAFIN PN"   +
			"  ,COM_TIPO_CAMBIO TC"   +
			"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
			"  ,COMCAT_ESTATUS_DOCTO ED"   +
			"  ,(select ic_linea_credito,ic_if,ic_tipo_cobro_interes ,'C' as cg_tipo_credito,ic_moneda from com_linea_credito where ic_producto_nafin = 4"   +
			"    union all"   +
			"    select ic_linea_credito_dm as ic_linea_credito,ic_if,ic_tipo_cobro_interes,'D' as cg_tipo_credito,ic_moneda from dis_linea_credito_dm where ic_producto_nafin = 4) LC"   +
			"  ,COMCAT_EPO E"   +
			"  ,DIS_DOCTO_SELECCIONADO DS"   +
			"  ,COMCAT_IF I"   +
			"  ,COMREL_PYME_EPO_X_PRODUCTO PP"   +
			"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
			"  ,COMCAT_TASA CT"   +
			"  ,DIS_SOLICITUD S"   +
			"  ,COM_ACUSE3 CA",//Para 2C  3
			
			"  DIS_DOCUMENTO D"   +
			"  ,COMCAT_PYME PY"   +
			"  ,COMCAT_MONEDA M"   +
			"  ,COMREL_PRODUCTO_EPO PE"   +
			"  ,COMCAT_PRODUCTO_NAFIN PN"   +
			"  ,COM_TIPO_CAMBIO TC"   +
			"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
			"  ,COMCAT_ESTATUS_DOCTO ED"   +
			"  ,(select ic_linea_credito,ic_if,ic_tipo_cobro_interes ,'C' as cg_tipo_credito,ic_moneda from com_linea_credito where ic_producto_nafin = 4"   +
			"    union all"   +
			"    select ic_linea_credito_dm as ic_linea_credito,ic_if,ic_tipo_cobro_interes,'D' as cg_tipo_credito,ic_moneda from dis_linea_credito_dm where ic_producto_nafin = 4) LC"   +
			"  ,COMCAT_EPO E"   +
			"  ,DIS_DOCTO_SELECCIONADO DS"   +
			"  ,COMCAT_IF I"   +
			"  ,COMREL_PYME_EPO_X_PRODUCTO PP"   +
			"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
			"  ,COMCAT_TASA CT"   +
			"  ,DIS_SOLICITUD S"   +
			"  ,COM_ACUSE3 CA",//PARA 3C	4
			
			"  DIS_DOCUMENTO D"   +
			"  ,COMCAT_PYME PY"   +
			"  ,COMCAT_MONEDA M"   +
			"  ,COMREL_PRODUCTO_EPO PE"   +
			"  ,COMCAT_PRODUCTO_NAFIN PN"   +
			"  ,COM_TIPO_CAMBIO TC"   +
			"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
			"  ,COMCAT_ESTATUS_DOCTO ED"   +
			"  ,(select ic_linea_credito,ic_if,ic_tipo_cobro_interes ,'C' as cg_tipo_credito,ic_moneda from com_linea_credito where ic_producto_nafin = 4"   +
			"    union all"   +
			"    select ic_linea_credito_dm as ic_linea_credito,ic_if,ic_tipo_cobro_interes,'D' as cg_tipo_credito,ic_moneda from dis_linea_credito_dm where ic_producto_nafin = 4) LC"   +
			"  ,COMCAT_EPO E"   +
			"  ,DIS_DOCTO_SELECCIONADO DS"   +
			"  ,COMCAT_IF I"   +
			"  ,COMREL_PYME_EPO_X_PRODUCTO PP"   +
			"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
			"  ,COMCAT_TASA CT"   +
			"  ,DIS_SOLICITUD S"   +
			"  ,COM_ACUSE3 CA",//PARA 4C
			
			"  DIS_DOCUMENTO D"   +
			"  ,COMCAT_PYME PY"   +
			"  ,COMCAT_MONEDA M"   +
			"  ,COMREL_PRODUCTO_EPO PE"   +
			"  ,COMCAT_PRODUCTO_NAFIN PN"   +
			"  ,COM_TIPO_CAMBIO TC"   +
			"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
			"  ,COMCAT_ESTATUS_DOCTO ED"   +
			"  ,(select ic_linea_credito,ic_if,ic_tipo_cobro_interes ,'C' as cg_tipo_credito,ic_moneda from com_linea_credito where ic_producto_nafin = 4"   +
			"    union all"   +
			"    select ic_linea_credito_dm as ic_linea_credito,ic_if,ic_tipo_cobro_interes,'D' as cg_tipo_credito,ic_moneda from dis_linea_credito_dm where ic_producto_nafin = 4) LC"   +
			"  ,COMCAT_EPO E"   +
			"  ,DIS_DOCTO_SELECCIONADO DS"   +
			"  ,COMCAT_IF I"   +
			"  ,COMREL_PYME_EPO_X_PRODUCTO PP"   +
			"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
			"  ,COMCAT_TASA CT"   +
			"  ,DIS_CAMBIO_ESTATUS CE"+
			"  ,DIS_SOLICITUD S"+
			"  ,COM_ACUSE3 CA",//para 11D
			
			"  DIS_DOCUMENTO D"   +
			"  ,COMCAT_PYME PY"   +
			"  ,COMCAT_MONEDA M"   +
			"  ,COMREL_PRODUCTO_EPO PE"   +
			"  ,COMCAT_PRODUCTO_NAFIN PN"   +
			"  ,COM_TIPO_CAMBIO TC"   +
			"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
			"  ,COMCAT_ESTATUS_DOCTO ED"   +
			"  ,(select ic_linea_credito,ic_if,ic_tipo_cobro_interes ,'C' as cg_tipo_credito,ic_moneda from com_linea_credito where ic_producto_nafin = 4"   +
			"    union all"   +
			"    select ic_linea_credito_dm as ic_linea_credito,ic_if,ic_tipo_cobro_interes,'D' as cg_tipo_credito,ic_moneda from dis_linea_credito_dm where ic_producto_nafin = 4) LC"   +
			"  ,COMCAT_EPO E"   +
			"  ,DIS_DOCTO_SELECCIONADO DS"   +
			"  ,COMCAT_IF I"   +
			"  ,COMREL_PYME_EPO_X_PRODUCTO PP"   +
			"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
			"  ,COMCAT_TASA CT"   +
			"  ,DIS_CAMBIO_ESTATUS CE"   +
			"  ,DIS_SOLICITUD S"+
			"  ,COM_ACUSE3 CA",//PARA 22D
			
			" DIS_DOCUMENTO D"+
			" ,COMCAT_PYME PY"+
			" ,COMCAT_MONEDA M"+
			" ,COMREL_PRODUCTO_EPO PE"+
			" ,COMCAT_PRODUCTO_NAFIN PN"+
			" ,COM_TIPO_CAMBIO TC"+
			" ,COMCAT_TIPO_FINANCIAMIENTO TF"+
			" ,COMCAT_ESTATUS_DOCTO ED"+
			" ,COMCAT_EPO E"+
			" ,DIS_CAMBIO_ESTATUS CE",//PARA 9D
			
			"  DIS_DOCUMENTO D"+
			" ,COMCAT_PYME PY"+
			" ,COMCAT_MONEDA M"+
			" ,COMREL_PRODUCTO_EPO PE"+
			" ,COMCAT_PRODUCTO_NAFIN PN"+
			" ,COM_TIPO_CAMBIO TC"+
			" ,COMCAT_TIPO_FINANCIAMIENTO TF"+
			" ,COMCAT_ESTATUS_DOCTO ED"+
			" ,COMCAT_EPO E"+
			" ,DIS_CAMBIO_ESTATUS CE",//PARA 5D
			
			"  DIS_DOCUMENTO D"   +
			"  ,COMCAT_PYME PY"   +
			"  ,COMCAT_MONEDA M"   +
			"  ,COMREL_PRODUCTO_EPO PE"   +
			"  ,COMCAT_PRODUCTO_NAFIN PN"   +
			"  ,COM_TIPO_CAMBIO TC"   +
			"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
			"  ,COMCAT_ESTATUS_DOCTO ED"   +
			"  ,(select ic_linea_credito,ic_if,ic_tipo_cobro_interes ,'C' as cg_tipo_credito,ic_moneda from com_linea_credito where ic_producto_nafin = 4"   +
			"    union all"   +
			"    select ic_linea_credito_dm as ic_linea_credito,ic_if,ic_tipo_cobro_interes,'D' as cg_tipo_credito,ic_moneda from dis_linea_credito_dm where ic_producto_nafin = 4) LC"   +
			"  ,COMCAT_EPO E"   +
			"  ,DIS_DOCTO_SELECCIONADO DS"   +
			"  ,COMCAT_IF I"   +
			"  ,COMREL_PYME_EPO_X_PRODUCTO PP"   +
			"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
			"  ,COMCAT_TASA CT"   +
			"  ,DIS_CAMBIO_ESTATUS CE",//PARA 20D
			
			" DIS_DOCUMENTO D"+
			" ,COMCAT_PYME PY"+
			" ,COMCAT_MONEDA M"+
			" ,COMREL_PRODUCTO_EPO PE"+
			" ,COMCAT_PRODUCTO_NAFIN PN"+
			" ,COM_TIPO_CAMBIO TC"+
			" ,COMCAT_TIPO_FINANCIAMIENTO TF"+
			" ,COMCAT_ESTATUS_DOCTO ED"+
			" ,COMCAT_EPO E", //PARA 1D		
      
      "  DIS_DOCUMENTO D"   +
			"  ,COMCAT_PYME PY"   +
			"  ,COMCAT_MONEDA M"   +
			"  ,COMREL_PRODUCTO_EPO PE"   +
			"  ,COMCAT_PRODUCTO_NAFIN PN"   +
			"  ,COM_TIPO_CAMBIO TC"   +
			"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
			"  ,COMCAT_ESTATUS_DOCTO ED"   +
     "   ,COMCAT_EPO E"   +
			"  ,(select ic_linea_credito,ic_if,ic_tipo_cobro_interes ,'C' as cg_tipo_credito,ic_moneda from com_linea_credito where ic_producto_nafin = 4"   +
			"    union all"   +
			"    select ic_linea_credito_dm as ic_linea_credito,ic_if,ic_tipo_cobro_interes,'D' as cg_tipo_credito,ic_moneda from dis_linea_credito_dm where ic_producto_nafin = 4) LC"   +
			"  ,DIS_DOCTO_SELECCIONADO DS"   +
			"  ,COMCAT_IF I, COMCAT_IF i2"   +
			"  ,COMREL_PYME_EPO_X_PRODUCTO PP"   +
			"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
			"  ,COMCAT_TASA CT " +
      "  ,com_bins_if bin "+
      "  ,dis_doctos_pago_tc ddp "
//			"  ,DIS_CAMBIO_ESTATUS CE"+
	//		"  ,DIS_SOLICITUD S"+
		//	"  ,COM_ACUSE3 CA"//para 32D
	};
	private String condiciones[]={
/*0-2D*/		" D.IC_PYME = PY.IC_PYME"+
		" AND D.IC_MONEDA = M.IC_MONEDA"+
		" AND PN.IC_PRODUCTO_NAFIN = 4"+
		" AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
		" AND PE.IC_EPO = D.IC_EPO"+
		" AND E.IC_EPO = D.IC_EPO"+
		" AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
			  " AND M.ic_moneda = TC.ic_moneda" +
		" AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
		" AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"+
		" AND D.IC_ESTATUS_DOCTO = 2" +
		" AND TRUNC(D.DF_CARGA) = TRUNC(SYSDATE)",///Para 2D
		
/*1 -3D*/		"  D.IC_PYME = PY.IC_PYME"   +
		"  AND DS.IC_DOCUMENTO = D.IC_DOCUMENTO"   +
		"  AND D.IC_MONEDA = M.IC_MONEDA"   +
		"  AND PN.IC_PRODUCTO_NAFIN = 4"   +
		"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
		"  AND PE.IC_EPO = D.IC_EPO"   +
		"  AND I.IC_IF = LC.IC_IF"   +
		"  AND E.IC_EPO = D.IC_EPO"   +
		"  AND PP.IC_EPO = E.IC_EPO"   +
		"  AND PP.IC_PYME = PY.IC_PYME"   +
		"  AND PP.IC_PRODUCTO_NAFIN = 4"   +
		"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
		"  AND M.ic_moneda = TC.ic_moneda"   +
		"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
		"  AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"   +
		"  AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES"   +
		"  AND CT.IC_TASA = DS.IC_TASA"   +
		"  AND D.IC_ESTATUS_DOCTO = ? /*3 0r 24*/"   +
		"  AND PP.CG_TIPO_CREDITO = LC.CG_TIPO_CREDITO"   +
		"  AND DECODE(PP.CG_TIPO_CREDITO,'D',D.IC_LINEA_CREDITO_DM,'C',D.IC_LINEA_CREDITO) = LC.IC_LINEA_CREDITO",//  +
		//"  AND TRUNC(DS.DF_FECHA_SELECCION) = TRUNC(SYSDATE)",//Para 3D
		
		"  D.IC_PYME = PY.IC_PYME"   +
		"  AND DS.IC_DOCUMENTO = D.IC_DOCUMENTO"   +
		"  AND S.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
		"  AND CA.CC_ACUSE = S.CC_ACUSE"   +
		"  AND D.IC_MONEDA = M.IC_MONEDA"   +
		"  AND PN.IC_PRODUCTO_NAFIN = 4"   +
		"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
		"  AND PE.IC_EPO = D.IC_EPO"   +
		"  AND I.IC_IF = LC.IC_IF"   +
		"  AND E.IC_EPO = D.IC_EPO"   +
		"  AND PP.IC_EPO = E.IC_EPO"   +
		"  AND PP.IC_PYME = PY.IC_PYME"   +
		"  AND PP.IC_PRODUCTO_NAFIN = 4"   +
		"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
		"  AND M.ic_moneda = TC.ic_moneda"   +
		"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
		"  AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"   +
		"  AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES"   +
		"  AND CT.IC_TASA = DS.IC_TASA"   +
		"  AND D.IC_ESTATUS_DOCTO in(3,4)"   +
		"  AND S.IC_ESTATUS_SOLIC = 1"   +
		"  AND PP.CG_TIPO_CREDITO = LC.CG_TIPO_CREDITO"   +
		"  AND DECODE(PP.CG_TIPO_CREDITO,'D',D.IC_LINEA_CREDITO_DM,'C',D.IC_LINEA_CREDITO) = LC.IC_LINEA_CREDITO"   +
		"  AND TRUNC(CA.DF_FECHA_HORA) = TRUNC(SYSDATE)",//Para 1C
		
		"  D.IC_PYME = PY.IC_PYME"   +
		"  AND DS.IC_DOCUMENTO = D.IC_DOCUMENTO"   +
		"  AND S.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
		"  AND D.IC_MONEDA = M.IC_MONEDA"   +
		"  AND PN.IC_PRODUCTO_NAFIN = 4"   +
		"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
		"  AND PE.IC_EPO = D.IC_EPO"   +
		"  AND I.IC_IF = LC.IC_IF"   +
		"  AND E.IC_EPO = D.IC_EPO"   +
		"  AND CA.CC_ACUSE = S.CC_ACUSE"   +
		"  AND PP.IC_EPO = E.IC_EPO"   +
		"  AND PP.IC_PYME = PY.IC_PYME"   +
		"  AND PP.IC_PRODUCTO_NAFIN = 4"   +
		"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
		"  AND M.ic_moneda = TC.ic_moneda"   +
		"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
		"  AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"   +
		"  AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES"   +
		"  AND CT.IC_TASA = DS.IC_TASA"   +
		"  AND D.IC_ESTATUS_DOCTO in(3,4)"   +
		"  AND S.IC_ESTATUS_SOLIC = 2"   +
		"  AND PP.CG_TIPO_CREDITO = LC.CG_TIPO_CREDITO"   +
		"  AND DECODE(PP.CG_TIPO_CREDITO,'D',D.IC_LINEA_CREDITO_DM,'C',D.IC_LINEA_CREDITO) = LC.IC_LINEA_CREDITO"   +
		"  AND TRUNC(CA.DF_FECHA_HORA) = TRUNC(SYSDATE)",//PARA 2C
		
		"  D.IC_PYME = PY.IC_PYME"   +
		"  AND DS.IC_DOCUMENTO = D.IC_DOCUMENTO"   +
		"  AND S.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
		"  AND D.IC_MONEDA = M.IC_MONEDA"   +
		"  AND PN.IC_PRODUCTO_NAFIN = 4"   +
		"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
		"  AND PE.IC_EPO = D.IC_EPO"   +
		"  AND I.IC_IF = LC.IC_IF"   +
		"  AND S.CC_ACUSE=CA.CC_ACUSE"+
		"  AND E.IC_EPO = D.IC_EPO"   +
		"  AND PP.IC_EPO = E.IC_EPO"   +
		"  AND PP.IC_PYME = PY.IC_PYME"   +
		"  AND PP.IC_PRODUCTO_NAFIN = 4"   +
		"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
		"  AND M.ic_moneda = TC.ic_moneda"   +
		"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
		"  AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"   +
		"  AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES"   +
		"  AND CT.IC_TASA = DS.IC_TASA"   +
		"  AND D.IC_ESTATUS_DOCTO in(4)"   +
		"  AND S.IC_ESTATUS_SOLIC = 3"   +
		"  AND PP.CG_TIPO_CREDITO = LC.CG_TIPO_CREDITO"   +
		"  AND DECODE(PP.CG_TIPO_CREDITO,'D',D.IC_LINEA_CREDITO_DM,'C',D.IC_LINEA_CREDITO) = LC.IC_LINEA_CREDITO"   +
		"  AND TRUNC(S.DF_OPERACION) = TRUNC(SYSDATE)",//para 3C
		
		" 	D.IC_PYME = PY.IC_PYME"   +
		"  AND DS.IC_DOCUMENTO = D.IC_DOCUMENTO"   +
		"  AND S.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
		"  AND D.IC_MONEDA = M.IC_MONEDA"   +
		"  AND PN.IC_PRODUCTO_NAFIN = 4"   +
		"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
		"  AND PE.IC_EPO = D.IC_EPO"   +
		"  AND I.IC_IF = LC.IC_IF"   +
		"  AND S.CC_ACUSE=CA.CC_ACUSE"+
		"  AND E.IC_EPO = D.IC_EPO"   +
		"  AND PP.IC_EPO = E.IC_EPO"   +
		"  AND PP.IC_PYME = PY.IC_PYME"   +
		"  AND PP.IC_PRODUCTO_NAFIN = 4"   +
		"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
		"  AND M.ic_moneda = TC.ic_moneda"   +
		"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
		"  AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"   +
		"  AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES"   +
		"  AND CT.IC_TASA = DS.IC_TASA"   +
		//ESL preguntar dis_documento que estatus sino quitar
		"  AND D.IC_ESTATUS_DOCTO in(4)"   +
		//ESL "  AND S.IC_ESTATUS_SOLIC = 3"   +
		"  AND S.IC_ESTATUS_SOLIC = 4"   +
		"  AND PP.CG_TIPO_CREDITO = LC.CG_TIPO_CREDITO"   +
		"  AND DECODE(PP.CG_TIPO_CREDITO,'D',D.IC_LINEA_CREDITO_DM,'C',D.IC_LINEA_CREDITO) = LC.IC_LINEA_CREDITO"  +
		"  AND TRUNC(S.DF_OPERACION) = TRUNC(SYSDATE)",//PARA 4C
		
		"  D.IC_PYME = PY.IC_PYME"   +
		"  AND DS.IC_DOCUMENTO = D.IC_DOCUMENTO"   +
		"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"+
		"  AND S.CC_ACUSE = CA.CC_ACUSE"+
		"  AND D.IC_MONEDA = M.IC_MONEDA"   +
		"  AND PN.IC_PRODUCTO_NAFIN = 4"   +
		"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
		"  AND PE.IC_EPO = D.IC_EPO"   +
		"  AND I.IC_IF = LC.IC_IF"   +
    "  AND E.IC_EPO = D.IC_EPO"   +
		"  AND PP.IC_EPO = E.IC_EPO"   +
		"  AND PP.IC_PYME = PY.IC_PYME"   +
		"  AND PP.IC_PRODUCTO_NAFIN = 4"   +
		"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
		"  AND M.ic_moneda = TC.ic_moneda"   +
		"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
		"  AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"   +
		"  AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES"   +
		"  AND CT.IC_TASA = DS.IC_TASA"   +
		"  AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"   +
		"  AND CE.IC_CAMBIO_ESTATUS in(19,20)"   +
		"  AND D.IC_ESTATUS_DOCTO = 11"   +
		"  AND PP.CG_TIPO_CREDITO = LC.CG_TIPO_CREDITO"   +
		"  AND DECODE(PP.CG_TIPO_CREDITO,'D',D.IC_LINEA_CREDITO_DM,'C',D.IC_LINEA_CREDITO) = LC.IC_LINEA_CREDITO"   +
		"  AND CE.DC_FECHA_CAMBIO IN(SELECT MAX(DC_FECHA_CAMBIO) FROM DIS_CAMBIO_ESTATUS WHERE IC_CAMBIO_ESTATUS in(19,20) AND IC_DOCUMENTO = D.IC_DOCUMENTO)" +
		"  AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE)" ,//PARA D11
		
		"   D.IC_PYME = PY.IC_PYME"   +
		"  AND DS.IC_DOCUMENTO = D.IC_DOCUMENTO"   +
		"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"+
		"  AND S.CC_ACUSE = CA.CC_ACUSE"+
		"  AND D.IC_MONEDA = M.IC_MONEDA"   +
		"  AND PN.IC_PRODUCTO_NAFIN = 4"   +
		"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
		"  AND PE.IC_EPO = D.IC_EPO"   +
		"  AND I.IC_IF = LC.IC_IF"   +
		"  AND E.IC_EPO = D.IC_EPO"   +
		"  AND PP.IC_EPO = E.IC_EPO"   +
		"  AND PP.IC_PYME = PY.IC_PYME"   +
		"  AND PP.IC_PRODUCTO_NAFIN = 4"   +
		"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
		"  AND M.ic_moneda = TC.ic_moneda"   +
		"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
		"  AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"   +
		"  AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES"   +
		"  AND CT.IC_TASA = DS.IC_TASA"   +
		"  AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"   +
		"  AND CE.IC_CAMBIO_ESTATUS = 18"   +
		"  AND D.IC_ESTATUS_DOCTO = 22"   +
		"  AND PP.CG_TIPO_CREDITO = LC.CG_TIPO_CREDITO"   +
		"  AND DECODE(PP.CG_TIPO_CREDITO,'D',D.IC_LINEA_CREDITO_DM,'C',D.IC_LINEA_CREDITO) = LC.IC_LINEA_CREDITO "   +
		"  AND CE.DC_FECHA_CAMBIO IN(SELECT MAX(DC_FECHA_CAMBIO) FROM DIS_CAMBIO_ESTATUS WHERE IC_CAMBIO_ESTATUS = 18 AND IC_DOCUMENTO = D.IC_DOCUMENTO)"+
		"  AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE)"  , //PARA D22

	
		"  D.IC_PYME = PY.IC_PYME"+
		" AND D.IC_MONEDA = M.IC_MONEDA"+
		" AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"+
		" AND PN.IC_PRODUCTO_NAFIN = 4"+
		" AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
		" AND PE.IC_EPO = D.IC_EPO"+
		" AND E.IC_EPO = D.IC_EPO"+
		" AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
			  " AND M.ic_moneda = TC.ic_moneda" +
		" AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
		" AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"+
		" AND D.IC_ESTATUS_DOCTO = 9"+
		" AND CE.IC_CAMBIO_ESTATUS = 22"+
		" AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE)",//PARA 9D

		" D.IC_PYME = PY.IC_PYME"+
		" AND D.IC_MONEDA = M.IC_MONEDA"+
		" AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"+
		" AND PN.IC_PRODUCTO_NAFIN = 4"+
		" AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
		" AND PE.IC_EPO = D.IC_EPO"+
		" AND E.IC_EPO = D.IC_EPO"+
		" AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
			  " AND M.ic_moneda = TC.ic_moneda" +
		" AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
		" AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"+
		" AND D.IC_ESTATUS_DOCTO = 5"+
		" AND CE.IC_CAMBIO_ESTATUS = 4",//+
		//" AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE)",//PARA 5D
		
		"  D.IC_PYME = PY.IC_PYME"   +
		"  AND DS.IC_DOCUMENTO = D.IC_DOCUMENTO"   +
		"  AND D.IC_MONEDA = M.IC_MONEDA"   +
		"  AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"   +
		"  AND PN.IC_PRODUCTO_NAFIN = 4"   +
		"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
		"  AND PE.IC_EPO = D.IC_EPO"   +
		"  AND I.IC_IF = LC.IC_IF"   +
		"  AND E.IC_EPO = D.IC_EPO"   +
		"  AND PP.IC_EPO = E.IC_EPO"   +
		"  AND PP.IC_PYME = PY.IC_PYME"   +
		"  AND PP.IC_PRODUCTO_NAFIN = 4"   +
		"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
		"  AND M.ic_moneda = TC.ic_moneda"   +
		"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
		"  AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"   +
		"  AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES"   +
		"  AND CT.IC_TASA = DS.IC_TASA"   +
		"  AND D.IC_ESTATUS_DOCTO = 20"   +
		"  AND CE.IC_CAMBIO_ESTATUS = 14"   +
		"  AND PP.CG_TIPO_CREDITO = LC.CG_TIPO_CREDITO"   +
		"  AND DECODE(PP.CG_TIPO_CREDITO,'D',D.IC_LINEA_CREDITO_DM,'C',D.IC_LINEA_CREDITO) = LC.IC_LINEA_CREDITO"   +
		"  AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE)",//PARA 20D
		
		" D.IC_PYME = PY.IC_PYME"+
		" AND D.IC_MONEDA = M.IC_MONEDA"+
		" AND PN.IC_PRODUCTO_NAFIN = 4"+
		" AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
		" AND PE.IC_EPO = D.IC_EPO"+
		" AND E.IC_EPO = D.IC_EPO"+
		" AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
			  " AND M.ic_moneda = TC.ic_moneda" +
		" AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
		" AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"+
		" AND D.IC_ESTATUS_DOCTO = 1"+
		" AND TRUNC(D.DF_CARGA) = TRUNC(SYSDATE)" ,//PARA 1D
    
    "  D.IC_PYME = PY.IC_PYME"   +
		"  AND DS.IC_DOCUMENTO = D.IC_DOCUMENTO"   +
//		"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"+
//		"  AND S.CC_ACUSE = CA.CC_ACUSE"+
		"  AND D.IC_MONEDA = M.IC_MONEDA"   +
		"  AND PN.IC_PRODUCTO_NAFIN = 4"   +
		"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
		"  AND PE.IC_EPO = D.IC_EPO"   +
		"  AND I.IC_IF = LC.IC_IF"   +    
    "  AND E.IC_EPO = D.IC_EPO"   +
		"  AND PP.IC_EPO = E.IC_EPO"   +
		"  AND PP.IC_PYME = PY.IC_PYME"   +
		"  AND PP.IC_PRODUCTO_NAFIN = 4"   +
		"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
		"  AND M.ic_moneda = TC.ic_moneda"   +
		"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
		"  AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"   +
		"  AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES"   +
		"  AND CT.IC_TASA = DS.IC_TASA"   +
//		"  AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"   +
//		"  AND CE.IC_CAMBIO_ESTATUS in(19,20)"   +
		"  AND D.IC_ESTATUS_DOCTO = ?"   +
		"  AND PP.CG_TIPO_CREDITO = LC.CG_TIPO_CREDITO"   +
		"  AND DECODE(PP.CG_TIPO_CREDITO,'D',D.IC_LINEA_CREDITO_DM,'C',D.IC_LINEA_CREDITO) = LC.IC_LINEA_CREDITO " +
    "  AND d.ic_bins = bin.ic_bins(+) "+
    "  AND bin.ic_if = i2.ic_if(+) " +
  " AND d.ic_orden_pago_tc = ddp.ic_orden_pago_tc(+)" +
//	"  AND CE.DC_FECHA_CAMBIO IN(SELECT MAX(DC_FECHA_CAMBIO) FROM DIS_CAMBIO_ESTATUS WHERE IC_CAMBIO_ESTATUS in(19,20) AND IC_DOCUMENTO = D.IC_DOCUMENTO)",//+
 " AND TRUNC(ddp.df_fecha_autorizacion) = TRUNC(SYSDATE)" //PARA D32
	};
	
	private String camposTotales[]={//
		"	m.cd_nombre AS moneda, COUNT (*) AS total_doctos,	"+
      "  SUM (d.fn_monto) AS fn_monto_total,	"+
      "  SUM (ds.fn_importe_recibir) AS monto_credito_total,	"+
      "  (  SUM (d.fn_monto) - (  (  SUM (d.fn_monto)	"+
		"		* SUM (DECODE (d.ic_tipo_financiamiento,	"+
      "                         1, NVL (d.fn_porc_descuento, 0),	"+
      "                         0	"+
      "                        )	"+
      "                )	"+
      "         )	"+
      "       / 100	"+
      "      )	"+
      "   ) AS monto_valuado_total	",
		
		"	m.cd_nombre AS moneda, COUNT (*) AS total_doctos,	"+
      "  SUM (d.fn_monto) AS fn_monto_total,	"+
    //  "  /*SUM (ds.fn_importe_recibir) AS monto_credito_total,	*/"+
      "  (  SUM (d.fn_monto) - (  (  SUM (d.fn_monto)	"+
		"		* SUM (DECODE (d.ic_tipo_financiamiento,	"+
      "                         1, NVL (d.fn_porc_descuento, 0),	"+
      "                         0	"+
      "                        )	"+
      "                )	"+
      "         )	"+
      "       / 100	"+
      "      )	"+
      "   ) AS monto_valuado_total,	"+
		"	m.ic_moneda",
    
    "	m.cd_nombre AS moneda, COUNT (*) AS total_doctos,	"+
      "  SUM (d.fn_monto) AS fn_monto_total,	"+
    //  "  /*SUM (ds.fn_importe_recibir) AS monto_credito_total,	*/"+
      "  (  SUM (d.fn_monto) - (  (  SUM (d.fn_monto)	"+
		"		* SUM (DECODE (d.ic_tipo_financiamiento,	"+
      "                         1, NVL (d.fn_porc_descuento, 0),	"+
      "                         0	"+
      "                        )	"+
      "                )	"+
      "         )	"+
      "       / 100	"+
      "      )	"+
      "   ) AS monto_valuado_total,	"+
		"	m.ic_moneda,	"+
    // "  ( (SUM (d.fn_monto)) - (SUM(d.fn_monto *  ( select cb.fn_comision  from  com_bins_if cb join  comcat_if ci   on  cb.ic_if = ci.ic_if where  cb.fn_comision = bin.fn_comision )  )) ) AS monto_valuado_total_mio	"
    // "  (  (SUM (d.fn_monto))  - (SUM ( ( SELECT cb.fn_comision  FROM com_bins_if cb JOIN comcat_if ci ON cb.ic_if = ci.ic_if WHERE cb.fn_comision = bin.fn_comision ) ) ) ) AS monto_valuado_total_mio "
    //" (  (SUM (d.fn_monto)) - SUM ( d.fn_monto * (DECODE (pe.fn_tasa_descuento, '', NVL (pe.fn_tasa_descuento, 0), pe.fn_tasa_descuento  )/100) )  ) AS monto_valuado_total_mio"
    " ( SUM  (  ( d.fn_monto - (   ( d.fn_monto * DECODE( d.ic_tipo_financiamiento,1 , NVL (d.fn_porc_descuento, 0),DECODE (d.ic_tipo_financiamiento, 3, NVL (d.fn_porc_descuento, 0), 0  ) )  )/100 )  ) - ( ( ( d.fn_monto - (   ( d.fn_monto * DECODE( d.ic_tipo_financiamiento,1 , NVL (d.fn_porc_descuento, 0),DECODE (d.ic_tipo_financiamiento, 3, NVL (d.fn_porc_descuento, 0), 0  ) )  )/100 )  )  * ( DECODE ( ds.fn_porc_comision_apli,'',NVL(ds.fn_porc_comision_apli,0),ds.fn_porc_comision_apli  )  ) )/100 )  ) )AS MONTO_VALUADO_TOTAL_MIO"
	};
	/**
	 * 
	 * @return el query de los totales por estatus
	 */
	public String getTotalesPorEstatus(){
		log.info("getTotalesPorEstatus(E)");
		StringBuffer qrySentencia = new StringBuffer("");
		try{
			switch (estatusDocto){
				case 1:
					if(estatusSolic.equals("C")){
						qrySentencia.append("SELECT "+camposTotales[0]+ " FROM " +tablas[2]+" WHERE "+condiciones[2]);			
					}else if(estatusSolic.equals("D")){
						qrySentencia.append("SELECT "+camposTotales[1]+ " FROM " +tablas[11]+" WHERE "+condiciones[11]);			
					}
					break;
				case 2:
					if(estatusSolic.equals("C")){
						qrySentencia.append("SELECT "+camposTotales[0]+ " FROM " +tablas[3]+" WHERE "+condiciones[3]);			
					}else if(estatusSolic.equals("D")){
						qrySentencia.append("SELECT "+camposTotales[1]+ " FROM " +tablas[0]+" WHERE "+condiciones[0]);			
					}
					break;
				case 3:
				/*MOD +(*/case 24://)
					if(estatusSolic.equals("C")){
						qrySentencia.append("SELECT "+camposTotales[0]+ " FROM " +tablas[4]+" WHERE "+condiciones[4]);			
					}else if(estatusSolic.equals("D")){
						qrySentencia.append("SELECT "+camposTotales[0]+ " FROM " +tablas[1]+" WHERE "+condiciones[1]);			
					}
					break;
				case 4://C
					if(estatusSolic.equals("C")){
						qrySentencia.append("SELECT "+camposTotales[0]+ " FROM " +tablas[5]+" WHERE "+condiciones[5]);			
					}
					break;
				case 5://D
					if(estatusSolic.equals("D")){
						qrySentencia.append("SELECT "+camposTotales[1]+ " FROM " +tablas[9]+" WHERE "+condiciones[9]);			
					}
					break;
				case 9://D
					if(estatusSolic.equals("D")){
						qrySentencia.append("SELECT "+camposTotales[1]+ " FROM " +tablas[8]+" WHERE "+condiciones[8]);			
					}
					break;
				case 11: //D
					if(estatusSolic.equals("D")){
						qrySentencia.append("SELECT "+camposTotales[0]+ " FROM " +tablas[6]+" WHERE "+condiciones[6]);			
					}
					break;
				case 20://D
					if(estatusSolic.equals("D")){
						qrySentencia.append("SELECT "+camposTotales[0]+ " FROM " +tablas[10]+" WHERE "+condiciones[10]);
					}
					break;
				case 22://D
					if(estatusSolic.equals("D")){
						qrySentencia.append("SELECT "+camposTotales[0]+ " FROM " +tablas[7]+" WHERE "+condiciones[7]);			
					}
					break;
        case 32://D
					if(estatusSolic.equals("D")){
						qrySentencia.append("SELECT "+camposTotales[2]+ " FROM " +tablas[12]+" WHERE "+condiciones[12]);			
					}
					break;
			}
			
			qrySentencia.append(" GROUP BY m.cd_nombre, m.ic_moneda ");
		} catch(Exception e) {
			log.error("RepDoctoSolicxEstatusDistBean::RepDoctoSolicxEstatusDistBean Exception "+e);
			e.printStackTrace();
		} 	
		log.info("qrySentenciaTOTALES "+ qrySentencia);
		
		log.info("getTotalesPorEstatus(S)");
		
		return qrySentencia.toString();
	
	}
	/**
	 * 
	 * @return el 	query con la consulta de estatus
	 */
	public String getQueryPorEstatus() {
		log.info("getQueryPorEstatus(E)");
		StringBuffer qrySentencia = new StringBuffer("");
		
		try{
			switch (estatusDocto){
				case 1:
					if(estatusSolic.equals("C")){
						qrySentencia.append("SELECT "+campos[2]+ " FROM " +tablas[2]+" WHERE "+condiciones[2]);			
					}else if(estatusSolic.equals("D")){
						qrySentencia.append("SELECT "+campos[11]+ " FROM " +tablas[11]+" WHERE "+condiciones[11]);			
					}
					break;
				case 2:
					if(estatusSolic.equals("C")){
						qrySentencia.append("SELECT "+campos[3]+ " FROM " +tablas[3]+" WHERE "+condiciones[3]);			
					}else if(estatusSolic.equals("D")){
						qrySentencia.append("SELECT "+campos[0]+ " FROM " +tablas[0]+" WHERE "+condiciones[0]);			
					}
					break;
				case 3:
				/*MOD +(*/case 24://)
					if(estatusSolic.equals("C")){
						qrySentencia.append("SELECT "+campos[4]+ " FROM " +tablas[4]+" WHERE "+condiciones[4]);			
					}else if(estatusSolic.equals("D")){
						qrySentencia.append("SELECT "+campos[1]+ " FROM " +tablas[1]+" WHERE "+condiciones[1]);			
					}
					break;
				case 4://C
					if(estatusSolic.equals("C")){
						qrySentencia.append("SELECT "+campos[5]+ " FROM " +tablas[5]+" WHERE "+condiciones[5]);			
					}
					break;
				case 5://D
					if(estatusSolic.equals("D")){
						qrySentencia.append("SELECT "+campos[9]+ " FROM " +tablas[9]+" WHERE "+condiciones[9]);			
					}
					break;
				case 9://D
					if(estatusSolic.equals("D")){
						qrySentencia.append("SELECT "+campos[8]+ " FROM " +tablas[8]+" WHERE "+condiciones[8]);			
					}
					break;
				case 11: //D
					if(estatusSolic.equals("D")){
						qrySentencia.append("SELECT "+campos[6]+ " FROM " +tablas[6]+" WHERE "+condiciones[6]);			
					}
					break;
				case 20://D
					if(estatusSolic.equals("D")){
						qrySentencia.append("SELECT "+campos[10]+ " FROM " +tablas[10]+" WHERE "+condiciones[10]);
					}
					break;
				case 22://D
					if(estatusSolic.equals("D")){
						qrySentencia.append("SELECT "+campos[7]+ " FROM " +tablas[7]+" WHERE "+condiciones[7]);			
					}
					break;
        case 32: //D
					if(estatusSolic.equals("D")){
						qrySentencia.append("SELECT "+campos[12]+ " FROM " +tablas[12]+" WHERE "+condiciones[12]);			
					}
					break;
			}
			//Se agraga la siguiente condicion a la consulta para solo consultar resultados del dia actual
			//qrySentencia.append(" AND TRUNC(D.DF_CARGA) = TRUNC(SYSDATE) ");
		} catch(Exception e) {
			log.error("RepDoctoSolicxEstatusDistBean::RepDoctoSolicxEstatusDistBean Exception "+e);
			e.printStackTrace();
		} 	
		log.info("qrySentencio "+ qrySentencia);
		log.info("getQueryPorEstatus(S)");
		return qrySentencia.toString();
	}

	
	/**
	 * 
	 * @return el query con la consulta del detalle del documento
	 */
	public String getDetallesDocumento(){
			log.info("getDetallesDocumento(E)");
		StringBuffer qrySentencia = new StringBuffer("");
		qrySentencia.append(" SELECT distinct TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fech_cambio, "+
								"	 TO_CHAR (ce.df_fecha_emision_anterior, 'dd/mm/yyyy') AS fech_emi_ant, "+
								"	 ce.ct_cambio_motivo,  "+
								"	 ce.fn_monto_anterior,  "+
								"	 ce.fn_monto_nuevo, "+
								"	 TO_CHAR (ce.df_fecha_emision_nueva, 'dd/mm/yyyy') AS fech_emi_new, "+
								"	 TO_CHAR (ce.df_fecha_venc_anterior, 'dd/mm/yyyy') AS fech_venc_ant, "+
								"	 TO_CHAR (ce.df_fecha_venc_nueva, 'dd/mm/yyyy') AS fech_venc_new, "+
								"	 cce.cd_descripcion, "+
								"	 TFA.cd_descripcion as modo_plazo_anterior, "+
								"	 TFN.cd_descripcion as modo_plazo_nuevo "+
							"	FROM dis_cambio_estatus ce, comcat_cambio_estatus cce  "+
							"		  ,comcat_tipo_financiamiento TFA,comcat_tipo_financiamiento TFN "+
							"	WHERE ic_documento = ?	"+
							"			  AND cce.ic_cambio_estatus = ce.ic_cambio_estatus 	"+
							"			  AND ce.ic_tipo_finan_ant = TFA.ic_tipo_financiamiento(+)	"+
							"			  AND ce.ic_tipo_finan_nuevo = TFN.ic_tipo_financiamiento(+)")	;
		
		log.info("getDetallesDocumento(E)");
		return qrySentencia.toString();
	}
	//setter�s y getter�s
	public void setEstatusDocto(int estatusDocto) {
		this.estatusDocto = estatusDocto;
	}


	public int getEstatusDocto() {
		return estatusDocto;
	}


	public void setEstatusSolic(String estatusSolic) {
		this.estatusSolic = estatusSolic;
	}


	public String getEstatusSolic() {
		return estatusSolic;
	}


	public void setClaveDocto(String claveDocto) {
		this.claveDocto = claveDocto;
	}


	public String getClaveDocto() {
		return claveDocto;
	}


	
}