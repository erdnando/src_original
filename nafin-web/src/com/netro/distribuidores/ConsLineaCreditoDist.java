package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsLineaCreditoDist implements IQueryGeneratorRegExtJS {
  public ConsLineaCreditoDist() { }

	private List conditions;
	StringBuffer strQuery;
  
  String claveIf = "";
  String clavePyme = "";
  String fechaSolCred = "";
  
  private static final Log log = ServiceLocator.getInstance().getLog(ConsLineaCreditoDist.class);//Variable para enviar mensajes al log.  
  
  public String getAggregateCalculationQuery() {
    return "";
  }

  public String getDocumentQuery() {

     return "";
  }
  public String getDocumentSummaryQueryForIds(List pageIds) {
    return "";
  }

  public String getDocumentQueryFile(){
		conditions	= new ArrayList();
		strQuery		= new StringBuffer();
		strQuery.append(
			"SELECT lc.ic_linea_credito_dm AS folio, 'Descuento y/o Factoraje' AS tipocredito, "+
			"       cif.cg_razon_social AS nom_if, "+
			"       TO_CHAR (ac4.df_acuse, 'DD/MM/YYYY') AS fecha_auto, "+
			"       mon.cd_nombre AS moneda, lc.fn_monto_autorizado_total, "+
			"       lc.fn_saldo_total, "+
			"       TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') AS fecha_venc, "+
			"       el.cd_descripcion AS estatus, "+
			"       TO_CHAR (lc.df_cambio, 'dd/mm/yyyy') AS fecha_cambio, "+
			"       mon.ic_moneda "+
			"  FROM dis_linea_credito_dm lc, "+
			"       comcat_if cif, "+
			"       dis_acuse4 ac4, "+
			"       comcat_estatus_linea el, "+
			"       comcat_moneda mon "+
			" WHERE lc.ic_if = cif.ic_if "+
			"   AND lc.ic_estatus_linea = el.ic_estatus_linea "+
			"   AND lc.cc_acuse = ac4.cc_acuse "+
			"   AND lc.ic_moneda = mon.ic_moneda "+
			"   AND lc.cg_tipo_solicitud = ? "+
			"   AND lc.ic_producto_nafin = ? "+
			"   AND lc.ic_if = ? "+
			"   AND lc.ic_epo = ? ");

          conditions.add("I");
			 conditions.add(new Integer(4));
          conditions.add(claveIf);
			 conditions.add(clavePyme);
        if (fechaSolCred != null && !fechaSolCred.equals("")) {
          strQuery.append( " AND TRUNC (ac4.df_acuse) <= TRUNC(TO_DATE(?,'dd/mm/yyyy')) " );
          conditions.add(fechaSolCred);
        }
		  strQuery.append( " ORDER BY 1 " );
	log.debug("getDocumentQuery)"+strQuery.toString());
	log.debug("getDocumentQuery)"+conditions);
	return strQuery.toString();
	}
  	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
    
		if ("CSV".equals(tipo)) {
		try {
		int 		totalDoctosMN=0;
		double	totalMontoSMN=0;
		double	totalMontoAMN=0;
		int		totalDoctosUSD=0;
		double	totalMontoSUSD=0;
		double	totalMontoAUSD=0;
		nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
		writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
		buffer = new BufferedWriter(writer);
		 
		int numRegistros = 0;
		String	rs_folio = "",
					rs_tipoCred = "",
					rs_If = "",
					rs_dfAuto = "",
					rs_moneda = "",
					rs_mtoAuto = "",
					rs_SaldoDisp = "",
					rs_dfVenc = "",
					rs_estatus = "",
					rs_FechaCamb = "",
					rs_ic_moneda = "";
			while (rs.next()) {
				numRegistros++;
				rs_folio 	 	= (rs.getString(1)==null)?"":rs.getString(1);
				rs_tipoCred 	= (rs.getString(2)==null)?"":rs.getString(2);
				rs_If   		= (rs.getString(3)==null)?"":rs.getString(3);
				rs_dfAuto 	 	= (rs.getString(4)==null)?"":rs.getString(4);
				rs_moneda		= (rs.getString(5)==null)?"":rs.getString(5);
				rs_mtoAuto	 	= (rs.getString(6)==null)?"":rs.getString(6);
				rs_SaldoDisp	= (rs.getString(7)==null)?"":rs.getString(7);
				rs_dfVenc	 	= (rs.getString(8)==null)?"":rs.getString(8);
				rs_estatus	 	= (rs.getString(9)==null)?"":rs.getString(9);
				rs_FechaCamb	= (rs.getString(10)==null)?"":rs.getString(10);
				rs_ic_moneda	= (rs.getString(11)==null)?"":rs.getString(11);
				String cgTipoCredito = rs_tipoCred.substring(0,1);
				if("1".equals(rs_ic_moneda)){
					totalDoctosMN ++;
					totalMontoSMN += Double.parseDouble(rs_SaldoDisp);
					totalMontoAMN +=Double.parseDouble(rs_mtoAuto);
				}else{
					totalDoctosUSD ++;
					totalMontoSUSD += Double.parseDouble(rs_SaldoDisp);
					totalMontoAUSD += Double.parseDouble(rs_mtoAuto);
				}
				
				if (numRegistros == 1){
					linea = "\nNombre IF,Folio,Monto autorizado,Saldo disponible,Moneda,Fecha de Autorizaci�n,Fecha vencimiento,Estatus Actual,Fecha de Cambio de Estatus";
				  buffer.write(linea);
				}
				linea = "\n"+rs_If.replace(',',' ')+","+rs_folio+","+rs_mtoAuto+","+rs_SaldoDisp+","+rs_moneda+","+rs_dfAuto+","+rs_dfVenc+","+rs_estatus+","+rs_FechaCamb;
				buffer.write(linea);
			}
			if(totalDoctosMN>0){
				linea ="\nTotal M.N.,"+totalDoctosMN+","+totalMontoAMN+","+totalMontoSMN;
				buffer.write(linea);
			}
			if(totalDoctosUSD>0){
				linea ="\nTotal Dolares,"+totalDoctosUSD+","+totalMontoAUSD+","+totalMontoSUSD;
				buffer.write(linea);
			}
			buffer.close();
		}catch (Throwable e) {
		 throw new AppException("Error al generar el archivo", e);
		} finally {
		 try {
			rs.close();
		 } catch(Exception e) {}
		}
		
    }else if ("PDF".equals(tipo)) {
		try {
			HttpSession session = request.getSession();

			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			int 		nRow = 0;
			int 		totalDoctosMN=0;
			double	totalMontoSMN=0;
			double	totalMontoAMN=0;
			int		totalDoctosUSD=0;
			double	totalMontoSUSD=0;
			double	totalMontoAUSD=0;
			ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
			while (rs.next()) {
				if(nRow == 0){
					String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
					String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
					String diaActual    = fechaActual.substring(0,2);
					String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
					String anioActual   = fechaActual.substring(6,10);
					String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						 
					  pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
						 session.getAttribute("iNoNafinElectronico").toString(),
						 (String)session.getAttribute("sesExterno"),
						 (String) session.getAttribute("strNombre"),
						 (String) session.getAttribute("strNombreUsuario"),
						 (String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
						 
					pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
					pdfDoc.addText(" ","formas", ComunesPDF.CENTER);
					pdfDoc.setTable(2, 50);
					pdfDoc.setCell("Informaci�n de l�neas de Cr�dito", "formasmenB",ComunesPDF.CENTER,2);
					pdfDoc.setCell("IF:", "formasmen");
					pdfDoc.setCell(rs.getString("NOM_IF"), "formasmen");
					pdfDoc.setCell("Estados de solicitudes de cr�dito al:", "formasmen");
					pdfDoc.setCell(this.fechaSolCred, "formasmen");
					pdfDoc.addTable();
					pdfDoc.setTable(9, 100);
					pdfDoc.setCell("Nombre IF", "formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Folio", "formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Autorizado", "formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Saldo disponible", "formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda", "formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Autorizaci�n", "formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de vencimiento", "formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Estatus Actual", "formasmenB",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Cambio de Estatus", "formasmenB",ComunesPDF.CENTER);
				}
				pdfDoc.setCell((rs.getString("NOM_IF")==null)?"":rs.getString("NOM_IF"), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell((rs.getString("FOLIO")==null)?"":rs.getString("FOLIO"), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell((rs.getString("FN_MONTO_AUTORIZADO_TOTAL")==null)?"":rs.getString("FN_MONTO_AUTORIZADO_TOTAL"), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell((rs.getString("FN_SALDO_TOTAL")==null)?"":rs.getString("FN_SALDO_TOTAL"), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell((rs.getString("MONEDA")==null)?"":rs.getString("MONEDA"), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell((rs.getString("FECHA_AUTO")==null)?"":rs.getString("FECHA_AUTO"), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell((rs.getString("FECHA_VENC")==null)?"":rs.getString("FECHA_VENC"), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell((rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS"), "formasmen", ComunesPDF.CENTER);
				pdfDoc.setCell((rs.getString("FECHA_CAMBIO")==null)?"":rs.getString("FECHA_CAMBIO"), "formasmen", ComunesPDF.CENTER);

				if("1".equals(rs.getString("IC_MONEDA"))){
					totalDoctosMN ++;
					totalMontoSMN += Double.parseDouble(rs.getString("FN_SALDO_TOTAL"));
					totalMontoAMN +=Double.parseDouble(rs.getString("FN_MONTO_AUTORIZADO_TOTAL"));
				}else{
					totalDoctosUSD ++;
					totalMontoSUSD += Double.parseDouble(rs.getString("FN_SALDO_TOTAL"));
					totalMontoAUSD += Double.parseDouble(rs.getString("FN_MONTO_AUTORIZADO_TOTAL"));
				}
				nRow++;
			}if (nRow > 0){
				if(totalDoctosMN>0){
					pdfDoc.setCell("Total M.N.", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(totalDoctosMN,0), "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(totalMontoAMN,2), "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(totalMontoSMN,2), "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setCell("", "formasmenB", ComunesPDF.CENTER,5);
				}
				if(totalDoctosUSD>0){
					pdfDoc.setCell("Dolares Americanos", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(totalDoctosUSD,0), "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(totalMontoAUSD,2), "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(totalMontoSUSD,2), "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setCell("", "formasmenB", ComunesPDF.CENTER,5);
				}
			}else {
				pdfDoc.addText(" No se encontro ning�n registro ","formas", ComunesPDF.CENTER);
			}
			pdfDoc.addTable();
			pdfDoc.endDocument();

		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo", e);
		} finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
	}
	return nombreArchivo;
  }
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		String nombreArchivo = "";
		if ("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();
	
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				int 		nRow = 0;
				int 		totalDoctosMN=0;
				double	totalMontoSMN=0;
				double	totalMontoAMN=0;
				int		totalDoctosUSD=0;
				double	totalMontoSUSD=0;
				double	totalMontoAUSD=0;
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				while (reg.next()) {
					if(nRow == 0){
						String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
						String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
						String diaActual    = fechaActual.substring(0,2);
						String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
						String anioActual   = fechaActual.substring(6,10);
						String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
							 
						  pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
							 session.getAttribute("iNoNafinElectronico").toString(),
							 (String)session.getAttribute("sesExterno"),
							 (String) session.getAttribute("strNombre"),
							 (String) session.getAttribute("strNombreUsuario"),
							 (String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
							 
						pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
						pdfDoc.addText(" ","formas", ComunesPDF.CENTER);
						pdfDoc.setTable(10, 100);
						pdfDoc.setCell("Historico de L�neas", "formasmenB",ComunesPDF.CENTER,10);
						pdfDoc.setCell("HISTORICO", "formasmenB",ComunesPDF.CENTER,6);
						pdfDoc.setCell("ACTUAL", "formasmenB",ComunesPDF.CENTER,4);
						pdfDoc.setCell("Folio", "formasmenB",ComunesPDF.CENTER);
						pdfDoc.setCell("Tipo de Cr�dito", "formasmenB",ComunesPDF.CENTER);
						pdfDoc.setCell("Moneda", "formasmenB",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha de Autorizaci�n", "formasmenB",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha de vencimiento", "formasmenB",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto Autorizado", "formasmenB",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha de Autorizaci�n", "formasmenB",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha de vencimiento", "formasmenB",ComunesPDF.CENTER);
						pdfDoc.setCell("Monto Autorizado", "formasmenB",ComunesPDF.CENTER);						
						pdfDoc.setCell("Tipo de Solicitud", "formasmenB",ComunesPDF.CENTER);
					}
					pdfDoc.setCell((reg.getString("FOLIO")==null)?"":reg.getString("FOLIO"), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("TIPOCREDITO")==null)?"":reg.getString("TIPOCREDITO"), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("MONEDA")==null)?"":reg.getString("MONEDA"), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("FECHA_AUTO_HIS")==null)?"":reg.getString("FECHA_AUTO_HIS"), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("FECHA_VENC_HIS")==null)?"":reg.getString("FECHA_VENC_HIS"), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("MONTO_AUTO_HIS")==null)?"":"$ "+reg.getString("MONTO_AUTO_HIS"), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("FECHA_AUTO_ACT")==null)?"":reg.getString("FECHA_AUTO_ACT"), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("FECHA_VENC_ACT")==null)?"":reg.getString("FECHA_VENC_ACT"), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("MONTO_AUTO_ACT")==null)?"":"$ "+reg.getString("MONTO_AUTO_ACT"), "formasmen", ComunesPDF.CENTER);
					pdfDoc.setCell((reg.getString("TIPO_SOL")==null)?"":reg.getString("TIPO_SOL"), "formasmen", ComunesPDF.CENTER);
					nRow++;
				}if (nRow <= 0){
					pdfDoc.addText(" No se encontro ning�n registro ","formas", ComunesPDF.CENTER);
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();
	
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
			}
		}
	return nombreArchivo;
	}

	public List getConditions() {
		return conditions;
	}

	public void setClaveIf(String ic_if) {
		this.claveIf = ic_if;
	}

	public void setClavePyme(String clavePyme) {
		this.clavePyme = clavePyme;
	}

	public String getClavePyme() {
		return clavePyme;
	}

	public void setFechaSolCred(String fecha) {
		this.fechaSolCred = fecha;
	}

}