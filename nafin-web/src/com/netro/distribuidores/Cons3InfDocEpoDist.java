package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class Cons3InfDocEpoDist implements IQueryGeneratorRegExtJS {
  public Cons3InfDocEpoDist() { }

	private List conditions;
	StringBuffer strQuery;
  
  String claveEpo = "";
  String clavePyme = "";
  String cgTipoCredito = "";
  
  private static final Log log = ServiceLocator.getInstance().getLog(Cons3InfDocEpoDist.class);//Variable para enviar mensajes al log.  
  
  public String getAggregateCalculationQuery() {
    return "";
  }

  public String getDocumentQuery() {

     return "";
  }
  public String getDocumentSummaryQueryForIds(List pageIds) {
    return "";
  }

  public String getDocumentQueryFile(){
   if (cgTipoCredito != null && !cgTipoCredito.equals("")) {
      
		conditions	= new ArrayList();
		strQuery		= new StringBuffer();

      if ("D".equals(cgTipoCredito)){
      
        strQuery.append(" SELECT (E.CG_RAZON_SOCIAL || ' - Tipo de Cr�dito: Descuento y/o Factoraje' || ' - Modalidad de Plazo: ' || VTF.CD_DESCRIPCION) AS NOMBRE_EPO_CONCAT"	+
				"	,E.CG_RAZON_SOCIAL as NOMBRE_EPO"	+
            "  ,'Descuento y/o Factoraje' as TIPO_CREDITO"	+
            "  ,VTF.CD_DESCRIPCION AS MODO_PLAZO "	+
            "  ,I.CG_RAZON_SOCIAL AS NOMBRE_IF"	+
            "  ,M.CD_NOMBRE AS MONEDA"	+
            "  ,TCI.CD_DESCRIPCION AS ESQUEMA_INTERES"	+
            " FROM COMCAT_EPO E"	+
            "  ,COMREL_PYME_EPO_X_PRODUCTO PEP"	+
            "  ,(SELECT TF.CD_DESCRIPCION,ETF.IC_EPO FROM COMREL_EPO_TIPO_FINANCIAMIENTO ETF,COMCAT_TIPO_FINANCIAMIENTO TF WHERE ETF.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO AND ETF.IC_PRODUCTO_NAFIN = ?) VTF"	+
            "  ,DIS_LINEA_CREDITO_DM LC"	+
            "  ,COMCAT_IF I "	+
            "  ,COMCAT_MONEDA M"	+
            "  ,COMCAT_TIPO_COBRO_INTERES TCI"	+
            " WHERE PEP.IC_EPO = E.IC_EPO"	+
            "  AND PEP.IC_PRODUCTO_NAFIN = ?"	+
            "  AND PEP.CG_TIPO_CREDITO = ?"	+
            "  AND E.IC_EPO = VTF.IC_EPO(+)"	+
            "  AND LC.IC_EPO = E.IC_EPO"	+
            "  AND TRUNC(LC.DF_VENCIMIENTO_ADICIONAL) > TRUNC(SYSDATE) "	+
            "  AND LC.IC_IF = I.IC_IF"	+
            "  AND LC.IC_MONEDA = M.IC_MONEDA"	+
            "  AND LC.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES");
          conditions.add(new Integer(4));
          conditions.add(new Integer(4));
          conditions.add("D" );
        if (claveEpo != null && !claveEpo.equals("")) {
          strQuery.append( " AND PEP.ic_epo = ?" );
          conditions.add(new Integer(claveEpo) );
        }
        if (clavePyme != null && !clavePyme.equals("")) {
          strQuery.append( " AND PEP.ic_pyme = ?" );
          conditions.add(new Integer(clavePyme) );
        }
        
      }else if("C".equals(cgTipoCredito)){
      
        strQuery.append(" SELECT (E.CG_RAZON_SOCIAL || ' - Tipo de Cr�dito: Credito en Cuenta Corriente' || ' - Modalidad de Plazo: ' || VTF.CD_DESCRIPCION) AS NOMBRE_EPO_CONCAT"	+
				"	,E.CG_RAZON_SOCIAL as NOMBRE_EPO"	+
            "  ,'Credito en Cuenta Corriente' as TIPO_CREDITO"	+
            "  ,VTF.CD_DESCRIPCION AS MODO_PLAZO "	+
            "  ,I.CG_RAZON_SOCIAL AS NOMBRE_IF"	+
            "  ,M.CD_NOMBRE AS MONEDA"	+
            "  ,TCI.CD_DESCRIPCION AS ESQUEMA_INTERES"	+
            "  ,to_char(LC.df_autorizacion_nafin,'dd/mm/yyyy') as FECHA_AUTORIZACION"	+
            "  ,CP.in_plazo_dias"	+
            "  ,to_char(df_vencimiento_adicional,'dd/mm/yyyy') as FECHA_VENC"	+
            "  ,LC.fn_monto_autorizado_total"	+
            "  ,LC.cg_tipo_solicitud||nvl(PY.in_numero_sirac,'')||LC.ic_linea_credito as NUMERO_LINEA"	+
            " FROM COMCAT_EPO E"	+
            "  ,COMREL_PYME_EPO_X_PRODUCTO PEP"	+
            "  ,(SELECT TF.CD_DESCRIPCION,ETF.IC_EPO FROM COMREL_EPO_TIPO_FINANCIAMIENTO ETF,COMCAT_TIPO_FINANCIAMIENTO TF WHERE ETF.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO AND ETF.IC_PRODUCTO_NAFIN = ?) VTF"	+
            "  ,COM_LINEA_CREDITO LC"	+
            "  ,COMCAT_IF I "	+
            "  ,COMCAT_MONEDA M"	+
            "  ,COMCAT_TIPO_COBRO_INTERES TCI"	+
            "  ,COMCAT_PLAZO CP"	+
            "  ,COMCAT_PYME PY"	+
            " WHERE PEP.IC_EPO = E.IC_EPO"	+
            "  AND PEP.IC_PRODUCTO_NAFIN = ?"	+
            "  AND PEP.CG_TIPO_CREDITO = ?"	+
            "  AND E.IC_EPO = VTF.IC_EPO(+)"	+
            "  AND LC.IC_PYME = PEP.IC_PYME"	+
            "  AND TRUNC(LC.DF_VENCIMIENTO_ADICIONAL) > TRUNC(SYSDATE) "	+
            "  AND LC.IC_IF = I.IC_IF"	+
            "  AND LC.IC_MONEDA = M.IC_MONEDA"	+
            "  AND LC.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES"	+
            "  AND LC.IC_ESTATUS_LINEA = ?"	+
            "  AND LC.IC_PLAZO = CP.IC_PLAZO"	+
            "  AND PY.IC_PYME = PEP.IC_PYME");
          conditions.add(new Integer(4));
          conditions.add(new Integer(4));
          conditions.add("C" );
          conditions.add(new Integer(5));
        if (claveEpo != null && !claveEpo.equals("")) {
          strQuery.append( " AND PEP.ic_epo = ?" );
          conditions.add(new Integer(claveEpo) );
        }
        if (clavePyme != null && !clavePyme.equals("")) {
          strQuery.append( " AND PEP.ic_pyme = ?" );
          conditions.add(new Integer(clavePyme) );
        }
      }
    }
	log.debug("getDocumentQuery)"+strQuery.toString());
	log.debug("getDocumentQuery)"+conditions);
	return strQuery.toString();
	}
  	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
    
    if ("CSV".equals(tipo)) {
      	try {
          nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
          writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
          buffer = new BufferedWriter(writer);
          
          int numRegistros = 0;
          String nombreEPO = "";
          String tipoCredito = "";
          String modoPlazo = "";
          while (rs.next()) {
            numRegistros++;
                  nombreEPO		= (rs.getString("NOMBRE_EPO")==null)?"":rs.getString("NOMBRE_EPO");
                tipoCredito		= (rs.getString("TIPO_CREDITO")==null)?"":rs.getString("TIPO_CREDITO");
                  modoPlazo		= (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
            String nombreIF		= (rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF");
            String moneda			= (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
            String tipoCobroInt	= (rs.getString("ESQUEMA_INTERES")==null)?"":rs.getString("ESQUEMA_INTERES");
          
            if (numRegistros == 1){
              linea = "EPO,"+nombreEPO+"\nTipo de cr�dito,"+tipoCredito+"\nModalidad de plazo,"+modoPlazo+"\n"+
              "IF Relacionada,Moneda,Esquema de Cobro a Interes\n";
              buffer.write(linea);
            }
            linea = nombreIF.replace(',',' ')+","+moneda+","+tipoCobroInt+"\n";
            buffer.write(linea);
          }
          buffer.close();
        }catch (Throwable e) {
          throw new AppException("Error al generar el archivo", e);
        } finally {
          try {
            rs.close();
          } catch(Exception e) {}
        }
        
    }else if ("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				int nRow = 0;
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				while (rs.next()) {
      		if(nRow == 0){
            String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
            String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
            String diaActual    = fechaActual.substring(0,2);
            String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
            String anioActual   = fechaActual.substring(6,10);
            String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
                
              pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
                session.getAttribute("iNoNafinElectronico").toString(),
                (String)session.getAttribute("sesExterno"),
                (String) session.getAttribute("strNombre"),
                (String) session.getAttribute("strNombreUsuario"),
                (String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
                
            pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
            pdfDoc.addText(" ","formas", ComunesPDF.CENTER);
            pdfDoc.addText("EPO:																									"+(rs.getString("NOMBRE_EPO")), "formas", ComunesPDF.LEFT);
            pdfDoc.addText("Tipo de cr�dito:									"+(rs.getString("TIPO_CREDITO")), "formas", ComunesPDF.LEFT);
            pdfDoc.addText("Modalidad de plazo:		"+(rs.getString("MODO_PLAZO")), "formas", ComunesPDF.LEFT);
            pdfDoc.setTable(3, 100);
            pdfDoc.setCell("IF Relacionado", "formasmenB", ComunesPDF.CENTER);
            pdfDoc.setCell("Moneda", "formasmenB", ComunesPDF.CENTER);
            pdfDoc.setCell("Esquema de cobro a intereses", "formasmenB", ComunesPDF.CENTER);
          }
        pdfDoc.setCell(rs.getString("NOMBRE_IF"), "formasmen", ComunesPDF.CENTER);
        pdfDoc.setCell(rs.getString("MONEDA"), "formasmen", ComunesPDF.CENTER);
        pdfDoc.setCell(rs.getString("ESQUEMA_INTERES"), "formasmen", ComunesPDF.CENTER);
        nRow++;
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		}else if ("CSV_b".equals(tipo)) {
      	try {
          nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
          writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
          buffer = new BufferedWriter(writer);
          
          int numRegistros = 0;
          String nombreEPO = "";
          String tipoCredito = "";
          String modoPlazo = "";
          while (rs.next()) {
            numRegistros++;
                  nombreEPO		= (rs.getString("NOMBRE_EPO")==null)?"":rs.getString("NOMBRE_EPO");
                tipoCredito		= (rs.getString("TIPO_CREDITO")==null)?"":rs.getString("TIPO_CREDITO");
                  modoPlazo		= (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
            String nombreIF		= (rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF");
            String moneda			= (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
            String tipoCobroInt	      = (rs.getString("ESQUEMA_INTERES")==null)?"":rs.getString("ESQUEMA_INTERES");
            String fechaAutorizacion	= (rs.getString("FECHA_AUTORIZACION")==null)?"":rs.getString("FECHA_AUTORIZACION");
            String plazo				      = (rs.getString("IN_PLAZO_DIAS")==null)?"":rs.getString("IN_PLAZO_DIAS");
            String fechaVencimiento 	= (rs.getString("FECHA_VENC")==null)?"":rs.getString("FECHA_VENC");
            double montoAutorizado 	  = rs.getDouble("FN_MONTO_AUTORIZADO_TOTAL");
            String numeroLinea			  = (rs.getString("NUMERO_LINEA")==null)?"":rs.getString("NUMERO_LINEA");
          
            if (numRegistros == 1){
              linea = "EPO,"+nombreEPO+"\nTipo de cr�dito,"+tipoCredito+"\nModalidad de plazo,"+modoPlazo+"\n"+
                   "IF Relacionado,Num. l�nea cr�dito,Moneda,Fecha autorizaci�n,Plazo,Fecha vencimiento,Monto l�nea,Esquema de cobro a intereses\n";
              buffer.write(linea);
            }
            linea = nombreIF.replace(',',' ')+","+numeroLinea+","+moneda+","+fechaAutorizacion+","+plazo+","+fechaVencimiento+",$"+Comunes.formatoDecimal(montoAutorizado,2,false)+","+tipoCobroInt+"\n";
            buffer.write(linea);
          }
          buffer.close();
        }catch (Throwable e) {
          throw new AppException("Error al generar el archivo", e);
        } finally {
          try {
            rs.close();
          } catch(Exception e) {}
        }
        
    }else if ("PDF_b".equals(tipo)) {
			try {
				HttpSession session = request.getSession();

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				int nRow = 0;
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				while (rs.next()) {
      		if(nRow == 0){
            String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
            String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
            String diaActual    = fechaActual.substring(0,2);
            String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
            String anioActual   = fechaActual.substring(6,10);
            String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
                
              pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
                session.getAttribute("iNoNafinElectronico").toString(),
                (String)session.getAttribute("sesExterno"),
                (String) session.getAttribute("strNombre"),
                (String) session.getAttribute("strNombreUsuario"),
                (String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
                
            pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
            pdfDoc.addText(" ","formas", ComunesPDF.CENTER);
            pdfDoc.addText("EPO:																									"+(rs.getString("NOMBRE_EPO")), "formas", ComunesPDF.LEFT);
            pdfDoc.addText("Tipo de cr�dito:									"+(rs.getString("TIPO_CREDITO")), "formas", ComunesPDF.LEFT);
            pdfDoc.addText("Modalidad de plazo:		"+(rs.getString("MODO_PLAZO")), "formas", ComunesPDF.LEFT);
            pdfDoc.setTable(8, 100);
            pdfDoc.setCell("IF Relacionado", "formasmenB", ComunesPDF.CENTER);
            pdfDoc.setCell("Num. L�nea cr�dito", "formasmenB", ComunesPDF.CENTER);
            pdfDoc.setCell("Moneda", "formasmenB", ComunesPDF.CENTER);
            pdfDoc.setCell("Fecha autorizaci�n", "formasmenB", ComunesPDF.CENTER);
            pdfDoc.setCell("Plazo", "formasmenB", ComunesPDF.CENTER);
            pdfDoc.setCell("Fecha vencimiento", "formasmenB", ComunesPDF.CENTER);
            pdfDoc.setCell("Monto l�nea", "formasmenB", ComunesPDF.CENTER);
            pdfDoc.setCell("Esquema de cobro a intereses", "formasmenB", ComunesPDF.CENTER);
          }
        pdfDoc.setCell(rs.getString("NOMBRE_IF"), "formasmen", ComunesPDF.CENTER);
        pdfDoc.setCell(rs.getString("NUMERO_LINEA"), "formasmen", ComunesPDF.CENTER);
        pdfDoc.setCell(rs.getString("MONEDA"), "formasmen", ComunesPDF.CENTER);
        pdfDoc.setCell(rs.getString("FECHA_AUTORIZACION"), "formasmen", ComunesPDF.CENTER);
        pdfDoc.setCell(rs.getString("IN_PLAZO_DIAS"), "formasmen", ComunesPDF.CENTER);
        pdfDoc.setCell(rs.getString("FECHA_VENC"), "formasmen", ComunesPDF.CENTER);
        pdfDoc.setCell("$ "+Comunes.formatoDecimal(rs.getString("FN_MONTO_AUTORIZADO_TOTAL"),2), "formasmen", ComunesPDF.CENTER);
        pdfDoc.setCell(rs.getString("ESQUEMA_INTERES"), "formasmen", ComunesPDF.CENTER);
        nRow++;
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		}
    return nombreArchivo;
  }
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}

	public List getConditions() {
		return conditions;
	}

	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}

	public String getClaveEpo() {
		return claveEpo;
	}

	public void setClavePyme(String clavePyme) {
		this.clavePyme = clavePyme;
	}

	public String getClavePyme() {
		return clavePyme;
	}

	public void setCgTipoCredito(String cgTipoCredito) {
		this.cgTipoCredito = cgTipoCredito;
	}

	public String getCgTipoCredito() {
		return cgTipoCredito;
	} 

}