package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import org.apache.commons.logging.Log;

public class ConsInfDocPymeDist implements IQueryGenerator, IQueryGeneratorRegExtJS {

  public ConsInfDocPymeDist() {  }
	public String getAggregateCalculationQuery(HttpServletRequest request) {

		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		StringBuffer qrySentencia	= new StringBuffer();
		StringBuffer qryDM			= new StringBuffer();
		StringBuffer qryCCC			= new StringBuffer();
		StringBuffer qryFdR			= new StringBuffer();
		StringBuffer condicion		= new StringBuffer();

		String	ic_pyme					= request.getSession().getAttribute("iNoCliente").toString();
		String	ic_epo					=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String	ic_if					=	(request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String	ic_estatus_docto		=	(request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto");
		String 	ig_numero_docto			=	(request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String 	fecha_seleccion_de		=	(request.getParameter("fecha_seleccion_de")==null)?"":request.getParameter("fecha_seleccion_de");
		String 	fecha_seleccion_a		=	(request.getParameter("fecha_seleccion_a")==null)?"":request.getParameter("fecha_seleccion_a");
		String 	cc_acuse				=	(request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String 	monto_credito_de		=	(request.getParameter("monto_credito_de")==null)?"":request.getParameter("monto_credito_de");
		String 	monto_credito_a			=	(request.getParameter("monto_credito_a")==null)?"":request.getParameter("monto_credito_a");
		String 	fecha_emision_de		=	(request.getParameter("fecha_emision_de")==null)?"":request.getParameter("fecha_emision_de");
		String 	fecha_emision_a			=	(request.getParameter("fecha_emision_a")==null)?"":request.getParameter("fecha_emision_a");
		String 	fecha_vto_de			=	(request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String 	fecha_vto_a				=	(request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String 	fecha_vto_credito_de	=	(request.getParameter("fecha_vto_credito_de")==null)?"":request.getParameter("fecha_vto_credito_de");
		String 	fecha_vto_credito_a 	=	(request.getParameter("fecha_vto_credito_a")==null)?"":request.getParameter("fecha_vto_credito_a");
		String 	ic_tipo_cobro_int		=	(request.getParameter("ic_tipo_cobro_int")==null)?"":request.getParameter("ic_tipo_cobro_int");
		String	ic_moneda				=	(request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String 	fn_monto_de				=	(request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String 	fn_monto_a				=	(request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String 	monto_con_descuento		=	(request.getParameter("monto_con_descuento")==null)?"":"checked";
		String	solo_cambio				=	(request.getParameter("solo_cambio")==null)?"":request.getParameter("solo_cambio");
		String	modo_plazo				=	(request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
		String	tipo_credito			=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String	cgTipoConversion		=	(request.getParameter("tipo_conversion")==null)?"":request.getParameter("tipo_conversion");
		String	ic_documento			=	(request.getParameter("ic_documento")==null)?"":request.getParameter("ic_documento");
		String 	fecha_publicacion_de	=	(request.getParameter("fecha_publicacion_de")==null)?fechaHoy:request.getParameter("fecha_publicacion_de");
		String 	fecha_publicacion_a		=	(request.getParameter("fecha_publicacion_a")==null)?fechaHoy:request.getParameter("fecha_publicacion_a");
		String tipoCreditoXepo		=	(request.getParameter("tipoCreditoXepo")==null)?"":request.getParameter("tipoCreditoXepo");
		String tipo_pago = (request.getParameter("tipo_pago")==null)?"":request.getParameter("tipo_pago"); // F009-2015

		try {
			if(!"".equals(ic_epo)&&ic_epo!=null)
				condicion.append(" AND D.IC_EPO = "+ic_epo);
			if("2".equals(ic_estatus_docto)||"5".equals(ic_estatus_docto)||"9".equals(ic_estatus_docto)||"1".equals(ic_estatus_docto)||"4".equals(ic_estatus_docto)||"23".equals(ic_estatus_docto))
				condicion.append(" AND D.IC_ESTATUS_DOCTO ="+ic_estatus_docto);
			if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
				condicion.append(" and D.ig_numero_docto = '"+ig_numero_docto+"'");
			if(!"".equals(cc_acuse)&& cc_acuse!=null)
				condicion.append(" and D.cc_acuse = '"+cc_acuse+"'");
			if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null)
				condicion.append(" and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date('"+fecha_vto_credito_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_credito_a+"','dd/mm/yyyy'))");		
			if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null)
				condicion.append(" AND LC.ic_tipo_cobro_interes = "+ic_tipo_cobro_int);
			if(!"".equals(ic_moneda)&&ic_moneda!=null)
				condicion.append(" and D.ic_moneda = "+ic_moneda);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
				condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
				condicion.append(" and D.fn_monto between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(solo_cambio)&& solo_cambio!=null)
				condicion.append(" and D.ic_documento in (select ic_documento from dis_cambio_estatus)");
			if(!"".equals(modo_plazo)&& modo_plazo!=null)
				condicion.append(" and D.ic_tipo_financiamiento = "+modo_plazo);
			if(!"".equals(ic_documento)&&ic_documento!=null)
				condicion.append(" AND D.IC_DOCUMENTO = "+ic_documento);
			if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
				condicion.append(" and D.df_carga >= to_date('"+fecha_publicacion_de+"','dd/mm/yyyy') and D.df_carga < to_date('"+fecha_publicacion_a+"','dd/mm/yyyy') + 1");
			if("".equals(ic_estatus_docto))
				condicion.append(" and D.ic_estatus_docto in(1,2,3,4,5,9,11,20,22,24,32)");//Mod +(24)
			if(!"".equals(ic_estatus_docto)&&!"2".equals(ic_estatus_docto)&&!"5".equals(ic_estatus_docto)&&!"9".equals(ic_estatus_docto)&&!"1".equals(ic_estatus_docto)&&!"4".equals(ic_estatus_docto)&&!"24".equals(ic_estatus_docto)&&!"32".equals(ic_estatus_docto))//MOD +(&&!"24".equals(ic_estatus_docto))	
				condicion.append(" and D.ic_documento is null");	
			if(!"".equals(monto_credito_de)&& monto_credito_de!=null&&!"".equals(monto_credito_a)&& monto_credito_a!=null)
				condicion.append(" and D.ic_documento is null");
			if(!tipo_pago.equals("")){
				condicion.append(" AND D.IG_TIPO_PAGO = " + tipo_pago);
			}

			qryDM.append("   SELECT/*+ use_nl(d,m,pe,tc,lc,epo) index(d cp_dis_documento_pk) index(m cp_comcat_moneda_pk)*/"   +
					"   M.ic_moneda as moneda, m.cd_nombre as nommoneda,d.fn_monto as monto"   +
					"   FROM DIS_DOCUMENTO D"   +
					"   ,COMCAT_MONEDA M"   +
					"   ,COMREL_PRODUCTO_EPO PE"   +
					"   ,COM_TIPO_CAMBIO TC"   +
					"   ,DIS_LINEA_CREDITO_DM LC"   +
					"   ,COMCAT_EPO EPO"   +
					"   WHERE D.IC_MONEDA = M.IC_MONEDA"   +
					"   AND PE.IC_EPO = D.IC_EPO"   +
					"   AND D.IC_EPO = EPO.IC_EPO"   +
					"   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"   AND M.ic_moneda = TC.ic_moneda"   +
					"   AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM (+)"   +
					"   AND PE.IC_PRODUCTO_NAFIN = 4"   +
					"   AND EPO.CS_HABILITADO = 'S'"   +
					" 	and d.IC_TIPO_FINANCIAMIENTO is not  null "+
					" AND D.IC_pyme = "+ic_pyme+
					condicion.toString());

		qryCCC.append("   SELECT/*+ use_nl(d,m,pe,tc,lc,epo) index(d cp_dis_documento_pk) index(m cp_comcat_moneda_pk)*/"   +
					"   M.ic_moneda as moneda, m.cd_nombre as nommoneda,d.fn_monto as monto"   +
					"   FROM DIS_DOCUMENTO D"   +
					"   ,COMCAT_MONEDA M"   +
					"   ,COMREL_PRODUCTO_EPO PE"   +
					"   ,COM_TIPO_CAMBIO TC"   +
					"   ,COM_LINEA_CREDITO LC"   +
					"   ,COMCAT_EPO EPO"   +
					"   WHERE D.IC_MONEDA = M.IC_MONEDA"   +
					"   AND PE.IC_EPO = D.IC_EPO"   +
					"   AND D.IC_EPO = EPO.IC_EPO"   +
					"   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"   AND M.ic_moneda = TC.ic_moneda"   +
					"   AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO (+)"   +
					" and d.ic_linea_credito_dm is null "+
					"   AND PE.IC_PRODUCTO_NAFIN = 4"  +
					"   AND EPO.CS_HABILITADO = 'S'"  +
					" 	and d.IC_TIPO_FINANCIAMIENTO is not  null "+
					" AND D.IC_pyme = "+ic_pyme+
					condicion.toString());
					
			qryFdR.append("   SELECT/*+ use_nl(d,m,pe,tc,lc,epo) index(d cp_dis_documento_pk) index(m cp_comcat_moneda_pk)*/"   +
					"   M.ic_moneda as moneda, m.cd_nombre as nommoneda,d.fn_monto as monto"   +
					"   FROM DIS_DOCUMENTO D"   +
					"   ,COMCAT_MONEDA M"   +
					"   ,COMREL_PRODUCTO_EPO PE"   +
					"   ,COM_TIPO_CAMBIO TC"   +
					"   ,COM_LINEA_CREDITO LC"   +
					"   ,COMCAT_EPO EPO"   +
					"   WHERE D.IC_MONEDA = M.IC_MONEDA"   +
					"   AND PE.IC_EPO = D.IC_EPO"   +
					"   AND D.IC_EPO = EPO.IC_EPO"   +
					"   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"   AND M.ic_moneda = TC.ic_moneda"   +
					"   AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO (+)"   +
					" and d.ic_linea_credito_dm is null "+
					"   AND PE.IC_PRODUCTO_NAFIN = 4"  +
					"   AND EPO.CS_HABILITADO = 'S'"  +
					" 	and d.IC_TIPO_FINANCIAMIENTO is null "+
					" AND D.IC_pyme = "+ic_pyme+
					condicion.toString());
					
			String qryAux = "";	
			if("D".equals(tipo_credito))
				qryAux = qryDM.toString();
			else if("C".equals(tipo_credito))
				qryAux = qryCCC.toString();
			else  if("".equals(tipo_credito)  && tipoCreditoXepo.equals("F"))   
				qryAux = qryFdR.toString();
			else 
				qryAux = qryDM.toString()+ " UNION ALL "+qryCCC.toString();

			qrySentencia.append(
				"SELECT moneda, nommoneda, COUNT (1), SUM (monto), 'ConsInfDocPymeDist::getAggregateCalculationQuery'"+
				"  FROM ("+qryAux.toString()+")"+
				"GROUP BY  moneda, nommoneda ORDER BY moneda ");

		}catch(Exception e){
			System.out.println("ConsInfDocPymeDist::getAggregateCalculationQuery "+e);
		}
		return qrySentencia.toString();
	}

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids) {
		int i=0;
		StringBuffer qrySentencia	= new StringBuffer();
		StringBuffer qryDM			= new StringBuffer();
		StringBuffer qryCCC			= new StringBuffer();
		StringBuffer condicion		= new StringBuffer();
		StringBuffer qryFdR			= new StringBuffer();

		String	ic_pyme				= request.getSession().getAttribute("iNoCliente").toString();
		String tipo_credito		=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String tipoCreditoXepo		=	(request.getParameter("tipoCreditoXepo")==null)?"":request.getParameter("tipoCreditoXepo");
		String tipo_pago = (request.getParameter("tipo_pago")==null)?"":request.getParameter("tipo_pago"); // F009-2015

		condicion.append(" AND d.ic_documento in (");
		i=0;
		for (Iterator it = ids.iterator(); it.hasNext();i++){
        	if(i>0){
				condicion.append(",");
			}
			condicion.append(" "+it.next().toString()+" ");
		}
		condicion.append(") ");

			qryDM.append("  SELECT /*+ "   +
					"      use_nl(d,e,m,pe,pn,tc,tf,ed,lc)"   +
					"      index(d cp_dis_documento_pk) "   +
					"      index(e cp_comcat_epo_pk) "   +
					"      index(m cp_comcat_moneda_pk) "   +
					"      index(pe cp_comrel_producto_epo_pk)"   +
					"      index(tc cp_com_tipo_cambio_pk)"   +
					"      index(tf cp_comcat_tipo_finan_pk)"   +
					"      index(ed cp_comcat_estatus_docto_pk)"   +
					"      index(lc cp_dis_linea_credito_dm_pk)*/"   +
					"     E.CG_RAZON_SOCIAL AS NOMBRE_EPO"   +
					"  	,D.IG_NUMERO_DOCTO"   +
					"  	,D.CC_ACUSE"   +
					" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"   +
					" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"   +
					" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"   +
					" 	,D.IG_PLAZO_DOCTO"   +
					" 	,M.CD_NOMBRE AS MONEDA"   +
					" 	,D.FN_MONTO"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
					" 	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"   +
					" 	,DECODE(D.IC_TIPO_FINANCIAMIENTO,2,0,NVL(D.FN_PORC_DESCUENTO,0)) AS FN_PORC_DESCUENTO"   +
					" 	,TF.CD_DESCRIPCION AS MODO_PLAZO"   +
					" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
					" 	,M.ic_moneda"   +
					" 	,D.ic_documento"   +
					" 	,LC.ic_moneda as MONEDA_LINEA, D.IC_ESTATUS_DOCTO,clas.cg_descripcion as CATEGORIA "   +
					" ,d.CG_VENTACARTERA as CG_VENTACARTERA, D.IG_TIPO_PAGO "+
					"  FROM DIS_DOCUMENTO D"   +
					"  ,COMCAT_EPO E"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  ,COMCAT_ESTATUS_DOCTO ED"   +
					"  ,DIS_LINEA_CREDITO_DM LC,comrel_clasificacion clas "+
					"  WHERE D.IC_EPO = E.IC_EPO"   +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND PE.IC_EPO = D.IC_EPO"   +
					"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"  AND M.ic_moneda = TC.ic_moneda"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"   +
					"  AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM (+)"   +
					"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"  AND D.ic_clasificacion = clas.ic_clasificacion (+) "+
					"  AND PN.IC_PRODUCTO_NAFIN = 4"  +
					"  AND D.IC_pyme = "+ic_pyme +
					condicion.toString());

		qryCCC.append("  SELECT /*+ "   +
					"      use_nl(d,e,m,pe,pn,tc,tf,ed,lc)"   +
					"      index(d cp_dis_documento_pk) "   +
					"      index(e cp_comcat_epo_pk) "   +
					"      index(m cp_comcat_moneda_pk) "   +
					"      index(pe cp_comrel_producto_epo_pk)"   +
					"      index(tc cp_com_tipo_cambio_pk)"   +
					"      index(tf cp_comcat_tipo_finan_pk)"   +
					"      index(ed cp_comcat_estatus_docto_pk)"   +
					"      index(lc cp_dis_linea_credito_dm_pk)*/"   +
					" 	  E.CG_RAZON_SOCIAL AS NOMBRE_EPO"   +
					"  	,D.IG_NUMERO_DOCTO"   +
					"  	,D.CC_ACUSE"   +
					" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"   +
					" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"   +
					" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"   +
					" 	,D.IG_PLAZO_DOCTO"   +
					" 	,M.CD_NOMBRE AS MONEDA"   +
					" 	,D.FN_MONTO"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
					" 	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"   +
					" 	,DECODE(D.IC_TIPO_FINANCIAMIENTO,2,0,NVL(D.FN_PORC_DESCUENTO,0)) AS FN_PORC_DESCUENTO"   +
					" 	,TF.CD_DESCRIPCION AS MODO_PLAZO"   +
					" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
					" 	,M.ic_moneda"   +
					" 	,D.ic_documento"   +
					" 	,nvl(LC.ic_moneda,'') as MONEDA_LINEA, D.IC_ESTATUS_DOCTO,clas.cg_descripcion as CATEGORIA "   +
					"   ,d.CG_VENTACARTERA as CG_VENTACARTERA, D.IG_TIPO_PAGO "+
					"  FROM DIS_DOCUMENTO D"   +
					"  ,COMCAT_EPO E"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  ,COMCAT_ESTATUS_DOCTO ED"   +
					"  ,COM_LINEA_CREDITO LC,comrel_clasificacion clas "+
					"  WHERE D.IC_EPO = E.IC_EPO"   +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND PN.IC_PRODUCTO_NAFIN = 4"   +
					"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"  AND PE.IC_EPO = D.IC_EPO"   +
					"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"  AND M.ic_moneda = TC.ic_moneda"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND D.ic_clasificacion = clas.ic_clasificacion (+) "+
					"  AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"   +
					"  AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO (+)"   +
					" and d.ic_linea_credito_dm is null "+
					"  AND D.IC_pyme = "+ic_pyme +
					condicion.toString());
					
					
				qryFdR.append("  SELECT /*+ "   +
					"      use_nl(d,e,m,pe,pn,tc,tf,ed,lc)"   +
					"      index(d cp_dis_documento_pk) "   +
					"      index(e cp_comcat_epo_pk) "   +
					"      index(m cp_comcat_moneda_pk) "   +
					"      index(pe cp_comrel_producto_epo_pk)"   +
					"      index(tc cp_com_tipo_cambio_pk)"   +
					"      index(tf cp_comcat_tipo_finan_pk)"   +
					"      index(ed cp_comcat_estatus_docto_pk)"   +
					"      index(lc cp_dis_linea_credito_dm_pk)*/"   +
					" 	  E.CG_RAZON_SOCIAL AS NOMBRE_EPO"   +
					"  	,D.IG_NUMERO_DOCTO"   +
					"  	,D.CC_ACUSE"   +
					" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"   +
					" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"   +
					" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"   +
					" 	,D.IG_PLAZO_DOCTO"   +
					" 	,M.CD_NOMBRE AS MONEDA"   +
					" 	,D.FN_MONTO"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
					" 	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"   +
					" 	,DECODE(D.IC_TIPO_FINANCIAMIENTO,2,0,NVL(D.FN_PORC_DESCUENTO,0)) AS FN_PORC_DESCUENTO"   +
					" 	,'NA' AS MODO_PLAZO"   +
					" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
					" 	,M.ic_moneda"   +
					" 	,D.ic_documento"   +
					" 	,nvl(LC.ic_moneda,'') as MONEDA_LINEA, D.IC_ESTATUS_DOCTO,clas.cg_descripcion as CATEGORIA "   +
					"   ,d.CG_VENTACARTERA as CG_VENTACARTERA, D.IG_TIPO_PAGO "+
					"  FROM DIS_DOCUMENTO D"   +
					"  ,COMCAT_EPO E"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  ,COMCAT_ESTATUS_DOCTO ED"   +
					"  ,COM_LINEA_CREDITO LC,comrel_clasificacion clas "+
					"  WHERE D.IC_EPO = E.IC_EPO"   +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND PN.IC_PRODUCTO_NAFIN = 4"   +
					"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"  AND PE.IC_EPO = D.IC_EPO"   +
					"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"  AND M.ic_moneda = TC.ic_moneda"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO(+)"   +
					"  AND D.ic_clasificacion = clas.ic_clasificacion (+) "+
					"  AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"   +
					"  AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO (+)"   +
					" and d.ic_linea_credito_dm is null "+
					"  AND D.IC_pyme = "+ic_pyme +
					" and d.IC_TIPO_FINANCIAMIENTO is null "+
					condicion.toString());
					
					
			if("D".equals(tipo_credito))
				qrySentencia = qryDM;
			else if("C".equals(tipo_credito))
				qrySentencia = qryCCC;
			else  if("".equals(tipo_credito)  && tipoCreditoXepo.equals("F"))   
				qrySentencia = qryFdR;
			else
				qrySentencia.append(qryDM.toString() + " UNION ALL "+ qryCCC.toString());

		System.out.println("el query queda de la siguiente manera "+qrySentencia.toString());
		return qrySentencia.toString();
	}
	
	public String getDocumentQuery(HttpServletRequest request) {

		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
    	StringBuffer qrySentencia	= new StringBuffer();
    	StringBuffer qryDM			= new StringBuffer();
    	StringBuffer qryCCC			= new StringBuffer();
		StringBuffer qryFdR			= new StringBuffer();
    	StringBuffer condicion		= new StringBuffer();

		String	ic_pyme				= request.getSession().getAttribute("iNoCliente").toString();

		String	ic_epo					=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String	ic_if					=	(request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String	ic_estatus_docto		=	(request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto");
		String 	ig_numero_docto			=	(request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String 	fecha_seleccion_de		=	(request.getParameter("fecha_seleccion_de")==null)?"":request.getParameter("fecha_seleccion_de");
		String 	fecha_seleccion_a		=	(request.getParameter("fecha_seleccion_a")==null)?"":request.getParameter("fecha_seleccion_a");
		String 	cc_acuse				=	(request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String 	monto_credito_de		=	(request.getParameter("monto_credito_de")==null)?"":request.getParameter("monto_credito_de");
		String 	monto_credito_a			=	(request.getParameter("monto_credito_a")==null)?"":request.getParameter("monto_credito_a");
		String 	fecha_emision_de		=	(request.getParameter("fecha_emision_de")==null)?"":request.getParameter("fecha_emision_de");
		String 	fecha_emision_a			=	(request.getParameter("fecha_emision_a")==null)?"":request.getParameter("fecha_emision_a");
		String 	fecha_vto_de			=	(request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String 	fecha_vto_a				=	(request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String 	fecha_vto_credito_de	=	(request.getParameter("fecha_vto_credito_de")==null)?"":request.getParameter("fecha_vto_credito_de");
		String 	fecha_vto_credito_a 	=	(request.getParameter("fecha_vto_credito_a")==null)?"":request.getParameter("fecha_vto_credito_a");
		String 	ic_tipo_cobro_int		=	(request.getParameter("ic_tipo_cobro_int")==null)?"":request.getParameter("ic_tipo_cobro_int");
		String	ic_moneda				=	(request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String 	fn_monto_de				=	(request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String 	fn_monto_a				=	(request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String 	monto_con_descuento		=	(request.getParameter("monto_con_descuento")==null)?"":"checked";
		String	solo_cambio				=	(request.getParameter("solo_cambio")==null)?"":request.getParameter("solo_cambio");
		String	modo_plazo				=	(request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
		String	tipo_credito			=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String	cgTipoConversion		=	(request.getParameter("tipo_conversion")==null)?"":request.getParameter("tipo_conversion");
		String	ic_documento			=	(request.getParameter("ic_documento")==null)?"":request.getParameter("ic_documento");
		String 	fecha_publicacion_de	=	(request.getParameter("fecha_publicacion_de")==null)?fechaHoy:request.getParameter("fecha_publicacion_de");
		String 	fecha_publicacion_a		=	(request.getParameter("fecha_publicacion_a")==null)?fechaHoy:request.getParameter("fecha_publicacion_a");
		String tipoCreditoXepo		=	(request.getParameter("tipoCreditoXepo")==null)?"":request.getParameter("tipoCreditoXepo");
		String tipo_pago = (request.getParameter("tipo_pago")==null)?"":request.getParameter("tipo_pago"); // F009-2015

		try {
			if(!"".equals(ic_epo)&&ic_epo!=null)
				condicion.append(" AND D.IC_EPO = "+ic_epo);
			if("2".equals(ic_estatus_docto)||"5".equals(ic_estatus_docto)||"9".equals(ic_estatus_docto)||"1".equals(ic_estatus_docto)||"4".equals(ic_estatus_docto)||"24".equals(ic_estatus_docto)||"32".equals(ic_estatus_docto))
				condicion.append(" AND D.IC_ESTATUS_DOCTO ="+ic_estatus_docto);
			if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
				condicion.append(" and D.ig_numero_docto = '"+ig_numero_docto+"'");
			if(!"".equals(cc_acuse)&& cc_acuse!=null)
				condicion.append(" and D.cc_acuse = '"+cc_acuse+"'");
			if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null)
				condicion.append(" and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date('"+fecha_vto_credito_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_credito_a+"','dd/mm/yyyy'))");		
			if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null)
				condicion.append(" AND LC.ic_tipo_cobro_interes = "+ic_tipo_cobro_int);
			if(!"".equals(ic_moneda)&&ic_moneda!=null)
				condicion.append(" and D.ic_moneda = "+ic_moneda);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
				condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
				condicion.append(" and D.fn_monto between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(solo_cambio)&& solo_cambio!=null)
				condicion.append(" and D.ic_documento in (select ic_documento from dis_cambio_estatus)");
			if(!"".equals(modo_plazo)&& modo_plazo!=null)
				condicion.append(" and D.ic_tipo_financiamiento = "+modo_plazo);
			if(!"".equals(ic_documento)&&ic_documento!=null)
				condicion.append(" AND D.IC_DOCUMENTO = "+ic_documento);
			if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
				condicion.append(" and D.df_carga >= to_date('"+fecha_publicacion_de+"','dd/mm/yyyy') and D.df_carga < to_date('"+fecha_publicacion_a+"','dd/mm/yyyy') + 1");
			if("".equals(ic_estatus_docto)) {
				condicion.append(" and D.ic_estatus_docto in(1,2,3,4,5,9,11,20,22,24,32)");
			} else if(!"2".equals(ic_estatus_docto)&&!"5".equals(ic_estatus_docto)&&!"9".equals(ic_estatus_docto)&&!"1".equals(ic_estatus_docto)&&!"4".equals(ic_estatus_docto)&&!"24".equals(ic_estatus_docto)&&!"32".equals(ic_estatus_docto)){
				condicion.append(" and D.ic_estatus_docto is null /*1*/");
			}
			//if(!"".equals(monto_credito_de)&& monto_credito_de!=null&&!"".equals(monto_credito_a)&& monto_credito_a!=null)
			//	condicion.append(" and D.ic_documento is null");
			if(!tipo_pago.equals("")){
				condicion.append(" AND D.IG_TIPO_PAGO = " + tipo_pago);
			}

			qryDM.append(
					"   SELECT /*+index(d) use_nl(d pe tc lc epo)*/"   +
					"    D.IC_DOCUMENTO"   +
					"   FROM DIS_DOCUMENTO D"   +
					"   ,COMREL_PRODUCTO_EPO PE"   +
					"   ,COM_TIPO_CAMBIO TC"   +
					"   ,DIS_LINEA_CREDITO_DM LC"   +
					"   ,COMCAT_EPO EPO"   +
					"   WHERE PE.IC_EPO = D.IC_EPO"   +
					"   AND D.IC_EPO = EPO.IC_EPO"   +
					"   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = D.IC_MONEDA)"   +
					"   AND D.ic_moneda = TC.ic_moneda"   +
					"   AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM (+)"   +
					"   AND PE.IC_PRODUCTO_NAFIN = 4"   +
					"   AND EPO.CS_HABILITADO = 'S' "   +
					" 	and d.IC_TIPO_FINANCIAMIENTO is not  null "+
					"   AND D.IC_pyme = "+ic_pyme  +
					condicion.toString());

		qryCCC.append("   SELECT /*+index(d) use_nl(d pe tc lc epo)*/"   +
					"    D.IC_DOCUMENTO"   +
					"   FROM  DIS_DOCUMENTO D"   +
					"   ,COMREL_PRODUCTO_EPO PE"   +
					"   ,COM_TIPO_CAMBIO TC"   +
					"   ,COM_LINEA_CREDITO LC"   +
					"   ,COMCAT_EPO EPO"   +
					"   WHERE PE.IC_EPO = D.IC_EPO"   +
					"   AND D.IC_EPO = EPO.IC_EPO"   +
					"   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = D.IC_MONEDA)"   +
					"   AND D.ic_moneda = TC.ic_moneda"   +
					"   AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO (+)"   +
					" and d.ic_linea_credito_dm is null "+
					"   AND PE.IC_PRODUCTO_NAFIN = 4"   +
					"   AND EPO.CS_HABILITADO = 'S'"   +
					" 	and d.IC_TIPO_FINANCIAMIENTO is not  null "+
					"  AND D.IC_pyme = "+ic_pyme+
					condicion.toString());
					
			qryFdR.append("   SELECT /*+index(d) use_nl(d pe tc lc epo)*/"   +
					"    D.IC_DOCUMENTO"   +
					"   FROM  DIS_DOCUMENTO D"   +
					"   ,COMREL_PRODUCTO_EPO PE"   +
					"   ,COM_TIPO_CAMBIO TC"   +
					"   ,COM_LINEA_CREDITO LC"   +
					"   ,COMCAT_EPO EPO"   +
					"   WHERE PE.IC_EPO = D.IC_EPO"   +
					"   AND D.IC_EPO = EPO.IC_EPO"   +
					"   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = D.IC_MONEDA)"   +
					"   AND D.ic_moneda = TC.ic_moneda"   +
					"   AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO (+)"   +
					" and d.ic_linea_credito_dm is null "+
					"   AND PE.IC_PRODUCTO_NAFIN = 4"   +
					"   AND EPO.CS_HABILITADO = 'S'"   +
					" and d.IC_TIPO_FINANCIAMIENTO is null "+
					"  AND D.IC_pyme = "+ic_pyme+
					condicion.toString());
			
			  
			if("D".equals(tipo_credito))
				qrySentencia = qryDM;
			else if("C".equals(tipo_credito))
				qrySentencia = qryCCC;
			else  if("".equals(tipo_credito)  && tipoCreditoXepo.equals("F"))   
				qrySentencia = qryFdR;
			else
				qrySentencia.append(qryDM.toString() + " UNION ALL "+ qryCCC.toString() );
			
			System.out.println("EL query de la llave primaria: "+qrySentencia.toString());
		}catch(Exception e){
			System.out.println("ConsInfDocPymeDist::getDocumentQueryException "+e);
		}
		return qrySentencia.toString();
	}
	
	public String getDocumentQueryFile(HttpServletRequest request) {

		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
    	StringBuffer qrySentencia	= new StringBuffer();
    	StringBuffer qryDM			= new StringBuffer();
    	StringBuffer qryCCC			= new StringBuffer();
		StringBuffer qryFdR			= new StringBuffer();
    	StringBuffer condicion		= new StringBuffer();
      
		String	ic_pyme				= request.getSession().getAttribute("iNoCliente").toString();

		String	ic_epo				=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String	ic_if					=	(request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String	ic_estatus_docto	=	(request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto");
		String 	ig_numero_docto	=	(request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String 	fecha_seleccion_de=	(request.getParameter("fecha_seleccion_de")==null)?"":request.getParameter("fecha_seleccion_de");
		String 	fecha_seleccion_a	=	(request.getParameter("fecha_seleccion_a")==null)?"":request.getParameter("fecha_seleccion_a");
		String 	cc_acuse				=	(request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String 	monto_credito_de	=	(request.getParameter("monto_credito_de")==null)?"":request.getParameter("monto_credito_de");
		String 	monto_credito_a		=	(request.getParameter("monto_credito_a")==null)?"":request.getParameter("monto_credito_a");
		String 	fecha_emision_de		=	(request.getParameter("fecha_emision_de")==null)?"":request.getParameter("fecha_emision_de");
		String 	fecha_emision_a			=	(request.getParameter("fecha_emision_a")==null)?"":request.getParameter("fecha_emision_a");
		String 	fecha_vto_de			=	(request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String 	fecha_vto_a				=	(request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String 	fecha_vto_credito_de	=	(request.getParameter("fecha_vto_credito_de")==null)?"":request.getParameter("fecha_vto_credito_de");
		String 	fecha_vto_credito_a 	=	(request.getParameter("fecha_vto_credito_a")==null)?"":request.getParameter("fecha_vto_credito_a");
		String 	ic_tipo_cobro_int		=	(request.getParameter("ic_tipo_cobro_int")==null)?"":request.getParameter("ic_tipo_cobro_int");
		String	ic_moneda				=	(request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String 	fn_monto_de				=	(request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String 	fn_monto_a				=	(request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String 	monto_con_descuento		=	(request.getParameter("monto_con_descuento")==null)?"":"checked";
		String	solo_cambio				=	(request.getParameter("solo_cambio")==null)?"":request.getParameter("solo_cambio");
		String	modo_plazo				=	(request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
		String	tipo_credito			=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String	cgTipoConversion		=	(request.getParameter("tipo_conversion")==null)?"":request.getParameter("tipo_conversion");
		String	ic_documento			=	(request.getParameter("ic_documento")==null)?"":request.getParameter("ic_documento");
		String 	fecha_publicacion_de	=	(request.getParameter("fecha_publicacion_de")==null)?fechaHoy:request.getParameter("fecha_publicacion_de");
		String 	fecha_publicacion_a		=	(request.getParameter("fecha_publicacion_a")==null)?fechaHoy:request.getParameter("fecha_publicacion_a");
		String tipoCreditoXepo		=	(request.getParameter("tipoCreditoXepo")==null)?"":request.getParameter("tipoCreditoXepo");
		String tipo_pago = (request.getParameter("tipo_pago")==null)?"":request.getParameter("tipo_pago"); // F009-2015

		try {

			if(!"".equals(ic_epo)&&ic_epo!=null)
				condicion.append(" AND D.IC_EPO = "+ic_epo);
			if("2".equals(ic_estatus_docto)||"5".equals(ic_estatus_docto)||"9".equals(ic_estatus_docto)||"1".equals(ic_estatus_docto)||"4".equals(ic_estatus_docto)||"24".equals(ic_estatus_docto)||"32".equals(ic_estatus_docto))
				condicion.append(" AND D.IC_ESTATUS_DOCTO ="+ic_estatus_docto);
			if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
				condicion.append(" and D.ig_numero_docto = '"+ig_numero_docto+"'");
			if(!"".equals(cc_acuse)&& cc_acuse!=null)
				condicion.append(" and D.cc_acuse = '"+cc_acuse+"'");
			if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null)
				condicion.append(" and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date('"+fecha_vto_credito_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_credito_a+"','dd/mm/yyyy'))");		
			if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null)
				condicion.append(" AND LC.ic_tipo_cobro_interes = "+ic_tipo_cobro_int);
			if(!"".equals(ic_moneda)&&ic_moneda!=null)
				condicion.append(" and D.ic_moneda = "+ic_moneda);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
				condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
				condicion.append(" and D.fn_monto between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(solo_cambio)&& solo_cambio!=null)
				condicion.append(" and D.ic_documento in (select ic_documento from dis_cambio_estatus)");
			if(!"".equals(modo_plazo)&& modo_plazo!=null)
				condicion.append(" and D.ic_tipo_financiamiento = "+modo_plazo);
			if(!"".equals(ic_documento)&&ic_documento!=null)
				condicion.append(" AND D.IC_DOCUMENTO = "+ic_documento);
			if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
				condicion.append(" and D.df_carga >= to_date('"+fecha_publicacion_de+"','dd/mm/yyyy') and D.df_carga < to_date('"+fecha_publicacion_a+"','dd/mm/yyyy') + 1");
			if("".equals(ic_estatus_docto)) {
				condicion.append(" and D.ic_estatus_docto in(1,2,3,4,5,9,11,20,22,24,32)");
			} else if(!"2".equals(ic_estatus_docto)&&!"5".equals(ic_estatus_docto)&&!"9".equals(ic_estatus_docto)&&!"1".equals(ic_estatus_docto)&&!"4".equals(ic_estatus_docto)&&!"24".equals(ic_estatus_docto)&&!"32".equals(ic_estatus_docto)){
				condicion.append(" and D.ic_estatus_docto is null /*1*/");
			}
			if(!tipo_pago.equals("")){
				condicion.append(" AND D.IG_TIPO_PAGO = " + tipo_pago);
			}

			qryDM.append("  SELECT /*+ "   +
					"      use_nl(d,e,m,pe,pn,tc,tf,ed,lc)"   +
					"      index(d cp_dis_documento_pk) "   +
					"      index(e cp_comcat_epo_pk) "   +
					"      index(m cp_comcat_moneda_pk) "   +
					"      index(pe cp_comrel_producto_epo_pk)"   +
					"      index(tc cp_com_tipo_cambio_pk)"   +
					"      index(tf cp_comcat_tipo_finan_pk)"   +
					"      index(ed cp_comcat_estatus_docto_pk)"   +
					"      index(lc cp_dis_linea_credito_dm_pk)*/"   +
					"     E.CG_RAZON_SOCIAL AS NOMBRE_EPO"   +
					"  	,D.IG_NUMERO_DOCTO"   +
					"  	,D.CC_ACUSE"   +
					" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"   +
					" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"   +
					" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"   +
					" 	,D.IG_PLAZO_DOCTO"   +
					" 	,M.CD_NOMBRE AS MONEDA"   +
					" 	,D.FN_MONTO"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
					" 	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"   +
					" 	,DECODE(D.IC_TIPO_FINANCIAMIENTO,2,0,NVL(D.FN_PORC_DESCUENTO,0)) AS FN_PORC_DESCUENTO"   +
					" 	,TF.CD_DESCRIPCION AS MODO_PLAZO"   +
					" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
					" 	,M.ic_moneda"   +
					" 	,D.ic_documento"   +
					" 	,LC.ic_moneda as MONEDA_LINEA, D.IC_ESTATUS_DOCTO,clas.cg_descripcion as CATEGORIA "   +
					" ,d.CG_VENTACARTERA as CG_VENTACARTERA, D.IG_TIPO_PAGO "+
					"  FROM DIS_DOCUMENTO D"   +
					"  ,COMCAT_EPO E"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  ,COMCAT_ESTATUS_DOCTO ED"   +
					"  ,DIS_LINEA_CREDITO_DM LC,comrel_clasificacion clas "+
					"  WHERE D.IC_EPO = E.IC_EPO"   +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND PE.IC_EPO = D.IC_EPO"   +
					"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"  AND M.ic_moneda = TC.ic_moneda"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"   +
					"  AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM (+)"   +
					"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"  AND D.ic_clasificacion = clas.ic_clasificacion (+) "+
					"  AND PN.IC_PRODUCTO_NAFIN = 4"  +
					"  AND E.CS_HABILITADO = 'S'"  +
					"  AND D.IC_pyme = "+ic_pyme  +
					condicion.toString());

		qryCCC.append("  SELECT /*+ "   +
					"      use_nl(d,e,m,pe,pn,tc,tf,ed,lc)"   +
					"      index(d cp_dis_documento_pk) "   +
					"      index(e cp_comcat_epo_pk) "   +
					"      index(m cp_comcat_moneda_pk) "   +
					"      index(pe cp_comrel_producto_epo_pk)"   +
					"      index(tc cp_com_tipo_cambio_pk)"   +
					"      index(tf cp_comcat_tipo_finan_pk)"   +
					"      index(ed cp_comcat_estatus_docto_pk)"   +
					"      index(lc cp_dis_linea_credito_dm_pk)*/"   +
					" 	  E.CG_RAZON_SOCIAL AS NOMBRE_EPO"   +
					"  	,D.IG_NUMERO_DOCTO"   +
					"  	,D.CC_ACUSE"   +
					" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"   +
					" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"   +
					" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"   +
					" 	,D.IG_PLAZO_DOCTO"   +
					" 	,M.CD_NOMBRE AS MONEDA"   +
					" 	,D.FN_MONTO"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
					" 	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"   +
					" 	,DECODE(D.IC_TIPO_FINANCIAMIENTO,2,0,NVL(D.FN_PORC_DESCUENTO,0)) AS FN_PORC_DESCUENTO"   +
					" 	,TF.CD_DESCRIPCION AS MODO_PLAZO"   +
					" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
					" 	,M.ic_moneda"   +
					" 	,D.ic_documento"   +
					" 	,nvl(LC.ic_moneda,'') as MONEDA_LINEA, D.IC_ESTATUS_DOCTO,clas.cg_descripcion as CATEGORIA "   +
					"   ,d.CG_VENTACARTERA as CG_VENTACARTERA, D.IG_TIPO_PAGO "+
					"  FROM DIS_DOCUMENTO D"   +
					"  ,COMCAT_EPO E"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  ,COMCAT_ESTATUS_DOCTO ED"   +
					"  ,COM_LINEA_CREDITO LC,comrel_clasificacion clas "+
					"  WHERE D.IC_EPO = E.IC_EPO"   +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND PN.IC_PRODUCTO_NAFIN = 4"   +
					"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"  AND PE.IC_EPO = D.IC_EPO"   +
					"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"  AND M.ic_moneda = TC.ic_moneda"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND D.ic_clasificacion = clas.ic_clasificacion (+) "+
					"  AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"   +
					"  AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO (+)"   +
					" and d.ic_linea_credito_dm is null "+
					"  AND E.CS_HABILITADO = 'S'"  +
					"  AND D.IC_pyme = "+ic_pyme  +
					condicion.toString());
					
					
		qryFdR.append("  SELECT /*+ "   +
					"      use_nl(d,e,m,pe,pn,tc,tf,ed,lc)"   +
					"      index(d cp_dis_documento_pk) "   +
					"      index(e cp_comcat_epo_pk) "   +
					"      index(m cp_comcat_moneda_pk) "   +
					"      index(pe cp_comrel_producto_epo_pk)"   +
					"      index(tc cp_com_tipo_cambio_pk)"   +
					"      index(tf cp_comcat_tipo_finan_pk)"   +
					"      index(ed cp_comcat_estatus_docto_pk)"   +
					"      index(lc cp_dis_linea_credito_dm_pk)*/"   +
					" 	  E.CG_RAZON_SOCIAL AS NOMBRE_EPO"   +
					"  	,D.IG_NUMERO_DOCTO"   +
					"  	,D.CC_ACUSE"   +
					" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"   +
					" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"   +
					" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"   +
					" 	,D.IG_PLAZO_DOCTO"   +
					" 	,M.CD_NOMBRE AS MONEDA"   +
					" 	,D.FN_MONTO"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
					" 	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"   +
					" 	,DECODE(D.IC_TIPO_FINANCIAMIENTO,2,0,NVL(D.FN_PORC_DESCUENTO,0)) AS FN_PORC_DESCUENTO"   +
					" 	,'NA' AS MODO_PLAZO"   +
					" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
					" 	,M.ic_moneda"   +
					" 	,D.ic_documento"   +
					" 	,nvl(LC.ic_moneda,'') as MONEDA_LINEA, D.IC_ESTATUS_DOCTO,clas.cg_descripcion as CATEGORIA "   +
					"   ,d.CG_VENTACARTERA as CG_VENTACARTERA, D.IG_TIPO_PAGO "+
					"  FROM DIS_DOCUMENTO D"   +
					"  ,COMCAT_EPO E"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  ,COMCAT_ESTATUS_DOCTO ED"   +
					"  ,COM_LINEA_CREDITO LC,comrel_clasificacion clas "+
					"  WHERE D.IC_EPO = E.IC_EPO"   +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND PN.IC_PRODUCTO_NAFIN = 4"   +
					"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"  AND PE.IC_EPO = D.IC_EPO"   +
					"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"  AND M.ic_moneda = TC.ic_moneda"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO(+)"   +
					"  AND D.ic_clasificacion = clas.ic_clasificacion (+) "+
					"  AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"   +
					"  AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO (+)"   +
					" and d.ic_linea_credito_dm is null "+
					"  AND E.CS_HABILITADO = 'S'"  +
					" and d.IC_TIPO_FINANCIAMIENTO is null "+
					"  AND D.IC_pyme = "+ic_pyme  +
					condicion.toString());
					
			if("D".equals(tipo_credito))
				qrySentencia = qryDM;
			else if("C".equals(tipo_credito))
				qrySentencia = qryCCC;
			else  if("".equals(tipo_credito)  && tipoCreditoXepo.equals("F"))   
				qrySentencia = qryFdR;
			else
				qrySentencia.append(qryDM.toString() + " UNION ALL "+ qryCCC.toString());

				
			System.out.println("EL query queda as� : "+qrySentencia.toString());
		}catch(Exception e){
			System.out.println("ConsInfDocPymeDist::getDocumentQueryFileException "+e);
		}
		return qrySentencia.toString();
	}

/*******************************************************************************
 *          Migraci�n. Implementa IQueryGeneratorRegExtJS.java                 *
 *******************************************************************************/
	private static final Log log = ServiceLocator.getInstance().getLog(ConsInfDocPymeDist.class);
	private List conditions;
	private String ic_moneda;
	private String ic_pyme;
	private String ic_epo;
	private String ic_if;
	private String ic_estatus_docto;
	private String ig_numero_docto;
	private String fecha_seleccion_de;
	private String fecha_seleccion_a;
	private String cc_acuse;
	private String monto_credito_de;
	private String monto_credito_a;
	private String fecha_emision_de;
	private String fecha_emision_a;
	private String fecha_vto_de;
	private String fecha_vto_a;
	private String fecha_vto_credito_de;
	private String fecha_vto_credito_a;
	private String ic_tipo_cobro_int;
	private String c_moneda;
	private String fn_monto_de;
	private String fn_monto_a;
	private String monto_con_descuento;
	private String solo_cambio;
	private String modo_plazo;
	private String tipo_credito;
	private String cgTipoConversion;
	private String ic_documento;
	private String fecha_publicacion_de;
	private String fecha_publicacion_a;
	private String tipoCreditoXepo;
	private String tipoConversion;
	private String tipo_pago;
	private String publicaDoctosFinanciables;

	public String getDocumentQuery() {
		return null;
	}

	public String getDocumentSummaryQueryForIds(List ids) {
		return null;
	}

	public String getAggregateCalculationQuery() {
		return null;
	}

/**
 * Se forma la consulta para generar los archivos pdf y csv
 * @return qrySentencia
 */
	public String getDocumentQueryFile() {

		log.info("getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer qryDM = new StringBuffer();
		StringBuffer qryCCC = new StringBuffer();
		StringBuffer qryFdR = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		conditions = new ArrayList();

		if(!"".equals(ic_epo)&&ic_epo!=null){
			condicion.append(" AND D.IC_EPO = ?");
			conditions.add(ic_epo);
		}
		if("2".equals(ic_estatus_docto)||"5".equals(ic_estatus_docto)||"9".equals(ic_estatus_docto)||"1".equals(ic_estatus_docto)||"4".equals(ic_estatus_docto)||"24".equals(ic_estatus_docto)||"32".equals(ic_estatus_docto)){
			condicion.append(" AND D.IC_ESTATUS_DOCTO = ?");
			conditions.add(ic_estatus_docto);
		}
		if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null){
			condicion.append(" AND D.ig_numero_docto = ?");
			conditions.add(ig_numero_docto);
		}
		if(!"".equals(cc_acuse)&& cc_acuse!=null){
			condicion.append(" AND D.cc_acuse = ?");
			conditions.add(cc_acuse);
		}
		if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null){
			condicion.append(" and trunc(D.df_fecha_emision) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");
			conditions.add(fecha_emision_de);
			conditions.add(fecha_emision_a);
		}
		if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null){
			condicion.append(" and trunc(D.df_fecha_venc) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");
			conditions.add(fecha_vto_de);
			conditions.add(fecha_vto_a);
		}
		if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null){
			condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");
			conditions.add(fecha_vto_credito_de);
			conditions.add(fecha_vto_credito_a);
		}
		if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null){
			condicion.append(" AND LC.ic_tipo_cobro_interes = ?");
			conditions.add(ic_tipo_cobro_int);
		}
		if(!"".equals(getIc_moneda()) && getIc_moneda() != null){
			condicion.append(" AND D.ic_moneda = ?");
			conditions.add(getIc_moneda());
		}
		if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento)){
			condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between ? and ?");
			conditions.add(fn_monto_de);
			conditions.add(fn_monto_a);
		}
		if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento)){
			condicion.append(" and D.fn_monto between ? and ?");
			conditions.add(fn_monto_de);
			conditions.add(fn_monto_a);
		}
		if(!"".equals(solo_cambio)&& solo_cambio!=null){
			condicion.append(" and D.ic_documento in (select ic_documento from dis_cambio_estatus)");
		}
		if(!"".equals(modo_plazo)&& modo_plazo!=null){
			condicion.append(" AND D.ic_tipo_financiamiento = ?");
			conditions.add(modo_plazo);
		}
		if(!"".equals(ic_documento)&&ic_documento!=null){
			condicion.append(" AND D.IC_DOCUMENTO = ?");
			conditions.add(ic_documento);
		}
		if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null){
			condicion.append(" and D.df_carga >= to_date(?,'dd/mm/yyyy') and D.df_carga < to_date(?,'dd/mm/yyyy') + 1");
			conditions.add(fecha_publicacion_de);
			conditions.add(fecha_publicacion_a);
		}
		if("".equals(ic_estatus_docto)){
			condicion.append(" and D.ic_estatus_docto in(1,2,3,4,5,9,11,20,22,24,32)");
		} else if(!"2".equals(ic_estatus_docto)&&!"5".equals(ic_estatus_docto)&&!"9".equals(ic_estatus_docto)&&!"1".equals(ic_estatus_docto)&&!"4".equals(ic_estatus_docto)&&!"24".equals(ic_estatus_docto)&&!"32".equals(ic_estatus_docto)){
		condicion.append(" and D.ic_estatus_docto is null /*1*/");
		}
		if(!"".equals(tipo_pago)){
			condicion.append(" AND D.IG_TIPO_PAGO = ?");
			conditions.add(tipo_pago);
		}

		qryDM.append("SELECT /*+ ");
		qryDM.append("       use_nl(d,e,m,pe,pn,tc,tf,ed,lc)");
		qryDM.append("       index(d cp_dis_documento_pk) ");
		qryDM.append("       index(e cp_comcat_epo_pk) ");
		qryDM.append("       index(m cp_comcat_moneda_pk) ");
		qryDM.append("       index(pe cp_comrel_producto_epo_pk)");
		qryDM.append("       index(tc cp_com_tipo_cambio_pk)");
		qryDM.append("       index(tf cp_comcat_tipo_finan_pk)");
		qryDM.append("       index(ed cp_comcat_estatus_docto_pk)");
		qryDM.append("       index(lc cp_dis_linea_credito_dm_pk)*/");
		qryDM.append("       E.CG_RAZON_SOCIAL AS NOMBRE_EPO");
		qryDM.append("       ,D.IG_NUMERO_DOCTO");
		qryDM.append("       ,D.CC_ACUSE");
		qryDM.append("       ,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION");
		qryDM.append("       ,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA");
		qryDM.append("       ,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC");
		qryDM.append("       ,D.IG_PLAZO_DOCTO");
		qryDM.append("       ,M.CD_NOMBRE AS MONEDA");
		qryDM.append("       ,D.FN_MONTO");
		qryDM.append("       ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION");
		qryDM.append("       ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO");
		qryDM.append("       ,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO");
		qryDM.append("       ,DECODE(D.IC_TIPO_FINANCIAMIENTO,2,0,NVL(D.FN_PORC_DESCUENTO,0)) AS FN_PORC_DESCUENTO");
		qryDM.append("       ,TF.CD_DESCRIPCION AS MODO_PLAZO");
		qryDM.append("       ,ED.CD_DESCRIPCION AS ESTATUS");
		qryDM.append("       ,M.ic_moneda");
		qryDM.append("       ,D.ic_documento");
		qryDM.append("       ,LC.ic_moneda as MONEDA_LINEA, D.IC_ESTATUS_DOCTO,clas.cg_descripcion as CATEGORIA ");
		qryDM.append("       ,d.CG_VENTACARTERA as CG_VENTACARTERA, D.IG_TIPO_PAGO ");
		qryDM.append("  FROM DIS_DOCUMENTO D");
		qryDM.append("       ,COMCAT_EPO E");
		qryDM.append("       ,COMCAT_MONEDA M");
		qryDM.append("       ,COMREL_PRODUCTO_EPO PE");
		qryDM.append("       ,COMCAT_PRODUCTO_NAFIN PN");
		qryDM.append("       ,COM_TIPO_CAMBIO TC");
		qryDM.append("       ,COMCAT_TIPO_FINANCIAMIENTO TF");
		qryDM.append("       ,COMCAT_ESTATUS_DOCTO ED");
		qryDM.append("       ,DIS_LINEA_CREDITO_DM LC,comrel_clasificacion clas ");
		qryDM.append(" WHERE D.IC_EPO = E.IC_EPO");
		qryDM.append("   AND D.IC_MONEDA = M.IC_MONEDA");
		qryDM.append("   AND PE.IC_EPO = D.IC_EPO");
		qryDM.append("   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)");
		qryDM.append("   AND M.ic_moneda = TC.ic_moneda");
		qryDM.append("   AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO");
		qryDM.append("   AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO");
		qryDM.append("   AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM (+)");
		qryDM.append("   AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN");
		qryDM.append("   AND D.ic_clasificacion = clas.ic_clasificacion (+) ");
		qryDM.append("   AND PN.IC_PRODUCTO_NAFIN = 4" );
		qryDM.append("   AND E.CS_HABILITADO = 'S'" );
		qryDM.append("   AND D.IC_pyme = "+ic_pyme );
		qryDM.append(condicion.toString());

		qryCCC.append("SELECT /*+ ");
		qryCCC.append("       use_nl(d,e,m,pe,pn,tc,tf,ed,lc)");
		qryCCC.append("       index(d cp_dis_documento_pk) ");
		qryCCC.append("       index(e cp_comcat_epo_pk) ");
		qryCCC.append("       index(m cp_comcat_moneda_pk) ");
		qryCCC.append("       index(pe cp_comrel_producto_epo_pk)");
		qryCCC.append("       index(tc cp_com_tipo_cambio_pk)");
		qryCCC.append("       index(tf cp_comcat_tipo_finan_pk)");
		qryCCC.append("       index(ed cp_comcat_estatus_docto_pk)");
		qryCCC.append("       index(lc cp_dis_linea_credito_dm_pk)*/");
		qryCCC.append("       E.CG_RAZON_SOCIAL AS NOMBRE_EPO");
		qryCCC.append("       ,D.IG_NUMERO_DOCTO");
		qryCCC.append("       ,D.CC_ACUSE");
		qryCCC.append("       ,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION");
		qryCCC.append("       ,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA");
		qryCCC.append("       ,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC");
		qryCCC.append("       ,D.IG_PLAZO_DOCTO");
		qryCCC.append("       ,M.CD_NOMBRE AS MONEDA");
		qryCCC.append("       ,D.FN_MONTO");
		qryCCC.append("       ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION");
		qryCCC.append("       ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO");
		qryCCC.append("       ,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO");
		qryCCC.append("       ,DECODE(D.IC_TIPO_FINANCIAMIENTO,2,0,NVL(D.FN_PORC_DESCUENTO,0)) AS FN_PORC_DESCUENTO");
		qryCCC.append("       ,TF.CD_DESCRIPCION AS MODO_PLAZO");
		qryCCC.append("       ,ED.CD_DESCRIPCION AS ESTATUS");
		qryCCC.append("       ,M.ic_moneda");
		qryCCC.append("       ,D.ic_documento");
		qryCCC.append("       ,nvl(LC.ic_moneda,'') as MONEDA_LINEA, D.IC_ESTATUS_DOCTO,clas.cg_descripcion as CATEGORIA ");
		qryCCC.append("       ,d.CG_VENTACARTERA as CG_VENTACARTERA, D.IG_TIPO_PAGO ");
		qryCCC.append("  FROM DIS_DOCUMENTO D");
		qryCCC.append("       ,COMCAT_EPO E");
		qryCCC.append("       ,COMCAT_MONEDA M");
		qryCCC.append("       ,COMREL_PRODUCTO_EPO PE");
		qryCCC.append("       ,COMCAT_PRODUCTO_NAFIN PN");
		qryCCC.append("       ,COM_TIPO_CAMBIO TC");
		qryCCC.append("       ,COMCAT_TIPO_FINANCIAMIENTO TF");
		qryCCC.append("       ,COMCAT_ESTATUS_DOCTO ED");
		qryCCC.append("       ,COM_LINEA_CREDITO LC,comrel_clasificacion clas ");
		qryCCC.append(" WHERE D.IC_EPO = E.IC_EPO");
		qryCCC.append("   AND D.IC_MONEDA = M.IC_MONEDA");
		qryCCC.append("   AND PN.IC_PRODUCTO_NAFIN = 4");
		qryCCC.append("   AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN");
		qryCCC.append("   AND PE.IC_EPO = D.IC_EPO");
		qryCCC.append("   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)");
		qryCCC.append("   AND M.ic_moneda = TC.ic_moneda");
		qryCCC.append("   AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO");
		qryCCC.append("   AND D.ic_clasificacion = clas.ic_clasificacion (+) ");
		qryCCC.append("   AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO");
		qryCCC.append("   AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO (+)");
		qryCCC.append("   and d.ic_linea_credito_dm is null ");
		qryCCC.append("   AND E.CS_HABILITADO = 'S'" );
		qryCCC.append("   AND D.IC_pyme = "+ic_pyme );
		qryCCC.append(condicion.toString());

		qryFdR.append("SELECT /*+ ");
		qryFdR.append("       use_nl(d,e,m,pe,pn,tc,tf,ed,lc)");
		qryFdR.append("       index(d cp_dis_documento_pk) ");
		qryFdR.append("       index(e cp_comcat_epo_pk) ");
		qryFdR.append("       index(m cp_comcat_moneda_pk) ");
		qryFdR.append("       index(pe cp_comrel_producto_epo_pk)");
		qryFdR.append("       index(tc cp_com_tipo_cambio_pk)");
		qryFdR.append("       index(tf cp_comcat_tipo_finan_pk)");
		qryFdR.append("       index(ed cp_comcat_estatus_docto_pk)");
		qryFdR.append("       index(lc cp_dis_linea_credito_dm_pk)*/");
		qryFdR.append("       E.CG_RAZON_SOCIAL AS NOMBRE_EPO");
		qryFdR.append("       ,D.IG_NUMERO_DOCTO");
		qryFdR.append("       ,D.CC_ACUSE");
		qryFdR.append("       ,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION");
		qryFdR.append("       ,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA");
		qryFdR.append("       ,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC");
		qryFdR.append("       ,D.IG_PLAZO_DOCTO");
		qryFdR.append("       ,M.CD_NOMBRE AS MONEDA");
		qryFdR.append("       ,D.FN_MONTO");
		qryFdR.append("       ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION");
		qryFdR.append("       ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO");
		qryFdR.append("       ,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO");
		qryFdR.append("       ,DECODE(D.IC_TIPO_FINANCIAMIENTO,2,0,NVL(D.FN_PORC_DESCUENTO,0)) AS FN_PORC_DESCUENTO");
		qryFdR.append("       ,'NA' AS MODO_PLAZO");
		qryFdR.append("       ,ED.CD_DESCRIPCION AS ESTATUS");
		qryFdR.append("       ,M.ic_moneda");
		qryFdR.append("       ,D.ic_documento");
		qryFdR.append("       ,nvl(LC.ic_moneda,'') as MONEDA_LINEA, D.IC_ESTATUS_DOCTO,clas.cg_descripcion as CATEGORIA ");
		qryFdR.append("       ,d.CG_VENTACARTERA as CG_VENTACARTERA, D.IG_TIPO_PAGO ");
		qryFdR.append("  FROM DIS_DOCUMENTO D");
		qryFdR.append("       ,COMCAT_EPO E");
		qryFdR.append("       ,COMCAT_MONEDA M");
		qryFdR.append("       ,COMREL_PRODUCTO_EPO PE");
		qryFdR.append("       ,COMCAT_PRODUCTO_NAFIN PN");
		qryFdR.append("       ,COM_TIPO_CAMBIO TC");
		qryFdR.append("       ,COMCAT_TIPO_FINANCIAMIENTO TF");
		qryFdR.append("       ,COMCAT_ESTATUS_DOCTO ED");
		qryFdR.append("       ,COM_LINEA_CREDITO LC,comrel_clasificacion clas ");
		qryFdR.append(" WHERE D.IC_EPO = E.IC_EPO");
		qryFdR.append("   AND D.IC_MONEDA = M.IC_MONEDA");
		qryFdR.append("   AND PN.IC_PRODUCTO_NAFIN = 4");
		qryFdR.append("   AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN");
		qryFdR.append("   AND PE.IC_EPO = D.IC_EPO");
		qryFdR.append("   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)");
		qryFdR.append("   AND M.ic_moneda = TC.ic_moneda");
		qryFdR.append("   AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO(+)");
		qryFdR.append("   AND D.ic_clasificacion = clas.ic_clasificacion (+) ");
		qryFdR.append("   AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO");
		qryFdR.append("   AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO (+)");
		qryFdR.append("   and d.ic_linea_credito_dm is null ");
		qryFdR.append("   AND E.CS_HABILITADO = 'S'" );
		qryFdR.append("   and d.IC_TIPO_FINANCIAMIENTO is null ");
		qryFdR.append("   AND D.IC_pyme = "+ic_pyme );
		qryFdR.append(condicion.toString());

		if("D".equals(tipo_credito))
			qrySentencia = qryDM;
		else if("C".equals(tipo_credito))
			qrySentencia = qryCCC;
		else if("".equals(tipo_credito)  && tipoCreditoXepo.equals("F"))
			qrySentencia = qryFdR;
		else
			qrySentencia.append(qryDM.toString() + " UNION ALL "+ qryCCC.toString());

		log.info("qrySentencia: " + qrySentencia.toString());
		log.info("conditions: " + conditions.toString());
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();
	}

	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo) {

		log.info("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();

		String epo               = "";
		String numDocto          = "";
		String numAcuseCarga     = "";
		String fechaEmision      = "";
		String fechaPublicacion  = "";
		String fechaVencimiento  = "";
		String plazoDocto        = "";
		String moneda            = "";
		String icMoneda          = "";
		String categoria         = "";
		String plazoDescuento    = "";
		String porcDescuento     = "";
		String modoPlazo         = "";
		String estatus           = "";
		String icDocumento       = "";
		String monedaLinea       = "";
		String bandeVentaCartera = "";
		String tipoPago          = "";
		int nRow                 = 0;
		double montoValuado      = 0;
		double monto             = 0;
		double tipoCambio        = 0;
		double montoDescontar    = 0;

		if(tipo.equals("PDF")){
			try {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

				while (rs.next()){
					if(nRow == 0){
						int numCols = (!"".equals(getTipoConversion())?18:15);
						if(publicaDoctosFinanciables.equals("S")){
							numCols++;
						}
						pdfDoc.setLTable(numCols,100);
						pdfDoc.setLCell("Datos del Documento Inicial","celda01",ComunesPDF.CENTER,numCols);
						pdfDoc.setLCell("EPO","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("No. Docto Inicial","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("No. Acuse Carga","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha Emisi�n","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha Pub.","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha Venc.","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Plazo Docto","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Categor�a","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Plazo Descuento D�as","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("% Descuento","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto % Descuento","celda01",ComunesPDF.CENTER);
						if(!"".equals(getTipoConversion())){
							pdfDoc.setLCell("Tipo Conv.","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Tipo Cambio","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Monto Valuado","celda01",ComunesPDF.CENTER);
						}
						pdfDoc.setLCell("Modalidad de Plazo","celda01",ComunesPDF.CENTER);
						if(publicaDoctosFinanciables.equals("S")){
							pdfDoc.setLCell("Tipo de pago","celda01",ComunesPDF.CENTER);
						}
						pdfDoc.setLCell("Estatus","celda01",ComunesPDF.CENTER);
						pdfDoc.setLHeaders();
						nRow++;
					}
					String rs_estatus = (rs.getString("ic_estatus_docto")==null)?"":rs.getString("ic_estatus_docto");
					epo               = (rs.getString("NOMBRE_EPO")==null)?"":rs.getString("NOMBRE_EPO");
					numDocto          = (rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO");
					numAcuseCarga     = (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
					fechaEmision      = (rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION");
					fechaPublicacion  = (rs.getString("DF_CARGA")==null)?"":rs.getString("DF_CARGA");
					fechaVencimiento  = (rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC");
					plazoDocto        = (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");
					moneda            = (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
					monto             = Double.parseDouble(rs.getString("FN_MONTO"));
					tipoConversion    = (rs.getString("TIPO_CONVERSION") == null) ? "" : rs.getString("TIPO_CONVERSION");
					tipoCambio        = Double.parseDouble(rs.getString("TIPO_CAMBIO"));
					plazoDescuento    = (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
					if(!"2".equals(rs_estatus)){
						porcDescuento = "0";
					}else{
						porcDescuento  = (rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO");
					}
					montoDescontar    = monto*Double.parseDouble(porcDescuento)/100;
					montoValuado      = (monto-montoDescontar)*tipoCambio;
					modoPlazo         = (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
					estatus           = (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");
					icMoneda          = (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");
					icDocumento       = (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
					monedaLinea       = (rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA");
					categoria         = (rs.getString("CATEGORIA")==null)?"":rs.getString("CATEGORIA");
					bandeVentaCartera = (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
					if (bandeVentaCartera.equals("S") ) {
						modoPlazo ="";
					}
					tipoPago = (rs.getString("IG_TIPO_PAGO")==null)?"":rs.getString("IG_TIPO_PAGO");
					if(tipoPago.equals("1")){
						tipoPago = "Financiamiento con intereses";
					} else if(tipo_pago.equals("2")){
						tipoPago = "Meses sin intereses";
					}
					pdfDoc.setLCell(epo.replace(',',' '),"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(numDocto,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(numAcuseCarga,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaEmision,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaPublicacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(plazoDocto,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(moneda,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(categoria,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(plazoDescuento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(porcDescuento+'%',"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoDescontar,2),"formas",ComunesPDF.RIGHT);

					if(!"".equals(getTipoConversion())){
						pdfDoc.setLCell(((tipoCambio!=1&&!icMoneda.equals(monedaLinea)) ? getTipoConversion() : ""),"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell("$"+((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?Comunes.formatoDecimal(tipoCambio,2,false):""),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?Comunes.formatoDecimal(montoValuado,2,false):""),"formas",ComunesPDF.RIGHT);
					}
					pdfDoc.setLCell(modoPlazo,"formas",ComunesPDF.LEFT);
					if(publicaDoctosFinanciables.equals("S")){
						pdfDoc.setLCell(tipoPago,"formas",ComunesPDF.LEFT);
					}
					pdfDoc.setLCell(estatus,"formas",ComunesPDF.LEFT);
				}

				pdfDoc.addLTable();
				pdfDoc.endDocument();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF. ", e);
			}
		}
		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}

	public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo) {
		return null;
	}
/*******************************************************************************
 *                       GETTERS AND SETTERS                                   *
 *******************************************************************************/
	public String getIc_pyme() {
		return ic_pyme;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}

	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getIc_estatus_docto() {
		return ic_estatus_docto;
	}

	public void setIc_estatus_docto(String ic_estatus_docto) {
		this.ic_estatus_docto = ic_estatus_docto;
	}

	public String getIg_numero_docto() {
		return ig_numero_docto;
	}

	public void setIg_numero_docto(String ig_numero_docto) {
		this.ig_numero_docto = ig_numero_docto;
	}

	public String getFecha_seleccion_de() {
		return fecha_seleccion_de;
	}

	public void setFecha_seleccion_de(String fecha_seleccion_de) {
		this.fecha_seleccion_de = fecha_seleccion_de;
	}

	public String getFecha_seleccion_a() {
		return fecha_seleccion_a;
	}

	public void setFecha_seleccion_a(String fecha_seleccion_a) {
		this.fecha_seleccion_a = fecha_seleccion_a;
	}

	public String getCc_acuse() {
		return cc_acuse;
	}

	public void setCc_acuse(String cc_acuse) {
		this.cc_acuse = cc_acuse;
	}

	public String getMonto_credito_de() {
		return monto_credito_de;
	}

	public void setMonto_credito_de(String monto_credito_de) {
		this.monto_credito_de = monto_credito_de;
	}

	public String getMonto_credito_a() {
		return monto_credito_a;
	}

	public void setMonto_credito_a(String monto_credito_a) {
		this.monto_credito_a = monto_credito_a;
	}

	public String getFecha_emision_de() {
		return fecha_emision_de;
	}

	public void setFecha_emision_de(String fecha_emision_de) {
		this.fecha_emision_de = fecha_emision_de;
	}

	public String getFecha_emision_a() {
		return fecha_emision_a;
	}

	public void setFecha_emision_a(String fecha_emision_a) {
		this.fecha_emision_a = fecha_emision_a;
	}

	public String getFecha_vto_de() {
		return fecha_vto_de;
	}

	public void setFecha_vto_de(String fecha_vto_de) {
		this.fecha_vto_de = fecha_vto_de;
	}

	public String getFecha_vto_a() {
		return fecha_vto_a;
	}

	public void setFecha_vto_a(String fecha_vto_a) {
		this.fecha_vto_a = fecha_vto_a;
	}

	public String getFecha_vto_credito_de() {
		return fecha_vto_credito_de;
	}

	public void setFecha_vto_credito_de(String fecha_vto_credito_de) {
		this.fecha_vto_credito_de = fecha_vto_credito_de;
	}

	public String getFecha_vto_credito_a() {
		return fecha_vto_credito_a;
	}

	public void setFecha_vto_credito_a(String fecha_vto_credito_a) {
		this.fecha_vto_credito_a = fecha_vto_credito_a;
	}

	public String getIc_tipo_cobro_int() {
		return ic_tipo_cobro_int;
	}

	public void setIc_tipo_cobro_int(String ic_tipo_cobro_int) {
		this.ic_tipo_cobro_int = ic_tipo_cobro_int;
	}

	public String getC_moneda() {
		return c_moneda;
	}

	public void setC_moneda(String c_moneda) {
		this.c_moneda = c_moneda;
	}

	public String getFn_monto_de() {
		return fn_monto_de;
	}

	public void setFn_monto_de(String fn_monto_de) {
		this.fn_monto_de = fn_monto_de;
	}

	public String getFn_monto_a() {
		return fn_monto_a;
	}

	public void setFn_monto_a(String fn_monto_a) {
		this.fn_monto_a = fn_monto_a;
	}

	public String getMonto_con_descuento() {
		return monto_con_descuento;
	}

	public void setMonto_con_descuento(String monto_con_descuento) {
		this.monto_con_descuento = monto_con_descuento;
	}

	public String getSolo_cambio() {
		return solo_cambio;
	}

	public void setSolo_cambio(String solo_cambio) {
		this.solo_cambio = solo_cambio;
	}

	public String getModo_plazo() {
		return modo_plazo;
	}

	public void setModo_plazo(String modo_plazo) {
		this.modo_plazo = modo_plazo;
	}

	public String getTipo_credito() {
		return tipo_credito;
	}

	public void setTipo_credito(String tipo_credito) {
		this.tipo_credito = tipo_credito;
	}

	public String getCgTipoConversion() {
		return cgTipoConversion;
	}

	public void setCgTipoConversion(String cgTipoConversion) {
		this.cgTipoConversion = cgTipoConversion;
	}

	public String getIc_documento() {
		return ic_documento;
	}

	public void setIc_documento(String ic_documento) {
		this.ic_documento = ic_documento;
	}

	public String getFecha_publicacion_de() {
		return fecha_publicacion_de;
	}

	public void setFecha_publicacion_de(String fecha_publicacion_de) {
		this.fecha_publicacion_de = fecha_publicacion_de;
	}

	public String getFecha_publicacion_a() {
		return fecha_publicacion_a;
	}

	public void setFecha_publicacion_a(String fecha_publicacion_a) {
		this.fecha_publicacion_a = fecha_publicacion_a;
	}

	public String getTipoCreditoXepo() {
		return tipoCreditoXepo;
	}

	public void setTipoCreditoXepo(String tipoCreditoXepo) {
		this.tipoCreditoXepo = tipoCreditoXepo;
	}

	public List getConditions(){
		return conditions; 
	}

	public String getIc_moneda() {
		return ic_moneda;
	}

	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}

	public String getTipoConversion() {
		return tipoConversion;
	}

	public void setTipoConversion(String tipoConversion) {
		this.tipoConversion = tipoConversion;
	}

	public String getTipo_pago() {
		return tipo_pago;
	}

	public void setTipo_pago(String tipo_pago) {
		this.tipo_pago = tipo_pago;
	}

	public String getPublicaDoctosFinanciables() {
		return publicaDoctosFinanciables;
	}

	public void setPublicaDoctosFinanciables(String publicaDoctosFinanciables) {
		this.publicaDoctosFinanciables = publicaDoctosFinanciables;
	}
}
