package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsNoOperNafinDist implements IQueryGenerator, IQueryGeneratorRegExtJS {
	public ConsNoOperNafinDist(){}
	private String paginaOffset;
	private String paginaNo;
	private List   conditions;
	StringBuffer   strQuery = new StringBuffer("");
	private static final Log log = ServiceLocator.getInstance().getLog(ConsNoOperNafinDist.class);
	private String loginUsuario;
	private String claveEpo;
	private String clavePyme;
	private String tipoCredito;
	private String modePlazo;
	private String icCambioEstatus;
	private String dcFechaCambioDe;
	private String dcFechaCambioA;
	private String claveIf;
	private String numeroDocto;
	private String fechaVtoDe;
	private String fechaVtoA;
	private String fechaPublicacionDe;
	private String fechaPublicacionA;
	private String moneda;
	private String montoDe;
	private String montoA;
	private List tipoDato;
	private String montoDescuento;
	private String tipoConsulta;
	private String ic_documento;

	public String getAggregateCalculationQuery(HttpServletRequest request) {
		log.info("getAggregateCalculationQuery(E)  ");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer qrySentenciaDM = new StringBuffer();
		StringBuffer qrySentenciaCCC = new StringBuffer();
		StringBuffer qryAux = new StringBuffer();
		StringBuffer condicion    = new StringBuffer();
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		String ic_epo =	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String ic_pyme = (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String tipo_credito = (request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String modo_plazo = (request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
		String ic_cambio_estatus = (request.getParameter("ic_cambio_estatus")==null)?"":request.getParameter("ic_cambio_estatus");
		String dc_fecha_cambio_de =	(request.getParameter("dc_fecha_cambio_de")==null)?fechaHoy:request.getParameter("dc_fecha_cambio_de");
		String dc_fecha_cambio_a = (request.getParameter("dc_fecha_cambio_a")==null)?fechaHoy:request.getParameter("dc_fecha_cambio_a");
		String ic_if = (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String ig_numero_docto = (request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String fecha_vto_de = (request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String fecha_vto_a = (request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String fecha_publicacion_de = (request.getParameter("fecha_publicacion_de")==null)?"":request.getParameter("fecha_publicacion_de");
		String fecha_publicacion_a = (request.getParameter("fecha_publicacion_a")==null)?"":request.getParameter("fecha_publicacion_a");
		String ic_moneda = (request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String fn_monto_de = (request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String fn_monto_a = (request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String monto_con_descuento = (request.getParameter("monto_con_descuento")==null)?"":"checked";
		if(!"".equals(ic_epo)&&ic_epo!=null)
			condicion.append(" AND D.IC_EPO = "+ic_epo);
		if(!"".equals(ic_pyme)&&ic_pyme!=null)
			condicion.append(" AND D.IC_PYME = "+ic_pyme);
		if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
			condicion.append(" and D.ig_numero_docto = '"+ig_numero_docto+"'");
		if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
			condicion.append(" and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
		if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
			condicion.append(" and trunc(D.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))");
		if(!"".equals(ic_moneda)&&ic_moneda!=null)
			condicion.append(" and D.ic_moneda = "+ic_moneda);
		if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
			condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de+" and "+fn_monto_a);
		if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
			condicion.append(" and D.fn_monto between "+fn_monto_de+" and "+fn_monto_a);
		if(!"".equals(modo_plazo)&& modo_plazo!=null)
			condicion.append(" and TF.ic_tipo_financiamiento = "+modo_plazo);
		if(!"".equals(dc_fecha_cambio_de)&&dc_fecha_cambio_de!=null&&!"".equals(dc_fecha_cambio_a)&& dc_fecha_cambio_a!=null)
			condicion.append(" and trunc(CE.dc_fecha_cambio) between trunc(to_date('"+dc_fecha_cambio_de+"','dd/mm/yyyy')) and trunc(to_date('"+dc_fecha_cambio_a+"','dd/mm/yyyy'))");
		if(!"".equals(ic_if)&&ic_if!=null)
			condicion.append(" AND LC.IC_IF = "+ic_if);
		if(!"".equals(ic_cambio_estatus)&& ic_cambio_estatus!=null)
			condicion.append(" and CE.ic_cambio_estatus = "+ic_cambio_estatus);
		else
			condicion.append(" and CE.ic_cambio_estatus in(4,2,24)");
		qrySentenciaDM.append(
				" SELECT  " +
				" 	m.ic_moneda, m.cd_nombre AS moneda, COUNT(*) as numRegistros, SUM(d.fn_monto) as monto, " +
				" 	SUM(DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), " +
				" 			'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') * d.fn_monto) as montoValuado " +
				" FROM dis_cambio_estatus ce, " +
				" 	dis_documento d, " +
				" 	dis_linea_credito_dm lc, " +
				" 	comrel_producto_epo pe, " +
				" 	com_tipo_cambio tc, " +
				" 	comcat_epo epo, " +
				" 	comcat_tipo_financiamiento tf, " +
				" 	comcat_producto_nafin pn, " +
				" 	comcat_moneda m " +
				" WHERE  " +
				" 	d.ic_moneda = m.ic_moneda " +
				" 	AND pe.ic_producto_nafin = pn.ic_producto_nafin " +
				" 	AND d.ic_epo = pe.ic_epo " +
				" 	AND pe.ic_epo = epo.ic_epo " +				
				" 	AND d.ic_documento = ce.ic_documento " +
				" 	AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento " +
				" 	AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm " +
				" 	AND tc.dc_fecha IN (SELECT MAX (dc_fecha) " +
				" 			FROM com_tipo_cambio " +
				" 			WHERE ic_moneda = d.ic_moneda) " +
				" 	AND ce.dc_fecha_cambio IN (SELECT /*+index(dce)*/ MAX (dc_fecha_cambio) FROM dis_cambio_estatus dce WHERE ic_documento = d.ic_documento) " +
				" 	AND tc.ic_moneda = m.ic_moneda " +
				" 	AND pe.ic_producto_nafin = 4   " + condicion +
				" 	AND epo.cs_habilitado = 'S'   " + 				
				" GROUP BY m.ic_moneda, m.cd_nombre ");
		
		qrySentenciaCCC.append(
				" SELECT  " +
				" 	m.ic_moneda, m.cd_nombre AS moneda, count(*) as numRegistros, SUM(d.fn_monto) as monto, " +
				" 	SUM(DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), " +
				" 			'P', DECODE (d.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') * d.fn_monto) as montoValuado " +
				" FROM dis_documento d, " +
				" 	dis_cambio_estatus ce, " +
				" 	comrel_producto_epo pe, " +
				" 	com_linea_credito lc, " +
				" 	com_tipo_cambio tc, " +
				" 	comcat_epo epo, " +
				" 	comcat_tipo_financiamiento tf, " +
				" 	comcat_moneda m, " +
				" 	comcat_producto_nafin pn " +
				" WHERE  " +
				" 	d.ic_moneda = m.ic_moneda " +
				" 	AND d.ic_epo = pe.ic_epo " +
				" 	AND pe.ic_epo = epo.ic_epo " +
				" 	AND d.ic_documento = ce.ic_documento " +
				" 	AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento " +
				" 	AND d.ic_linea_credito (+) = lc.ic_linea_credito " +
				" 	AND pe.ic_producto_nafin = pn.ic_producto_nafin " +
				" 	AND tc.dc_fecha IN (SELECT MAX (dc_fecha) " +
				" 			FROM com_tipo_cambio " +
				" 			WHERE ic_moneda = d.ic_moneda) " +
				" 	AND ce.dc_fecha_cambio IN (SELECT MAX (dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_documento = d.ic_documento) " +
				" 	AND tc.ic_moneda = m.ic_moneda " +
				" 	AND pe.ic_producto_nafin = 4 " + condicion +
				" 	AND epo.cs_habilitado = 'S' " + 
				" GROUP BY m.ic_moneda, m.cd_nombre ");
		if ("D".equals(tipo_credito)) {
			qryAux.append(qrySentenciaDM);
		} else if ("C".equals(tipo_credito)) {
			qryAux.append(qrySentenciaCCC);
		} else {
			qryAux.append(qrySentenciaDM + " union all  " + qrySentenciaCCC);
		}
		qrySentencia.append(
				" SELECT ic_moneda, moneda, SUM(numRegistros)," +
				" 	SUM(monto), SUM(montoValuado), 'ConsNoOperNafinDist::getAggregateCalculationQuery' " +
				" FROM (" + qryAux + ")" +
				" GROUP BY ic_moneda, moneda ");
		log.debug("getAggregateCalculationQuery:  "+qrySentencia.toString());
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();
	}

	/**
	 *
	 */
	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids) {
		log.info("getDocumentSummaryQueryForIds(E)");
		int i=0;
		StringBuffer qrySentencia	= new StringBuffer();
		StringBuffer qrySentenciaDM	= new StringBuffer();
		StringBuffer qrySentenciaCCC	= new StringBuffer();
		StringBuffer qryAux	= new StringBuffer();
    	StringBuffer condicion		= new StringBuffer();
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		String tipo_credito = (request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		condicion.append(" AND ce.ic_documento||'_'||TO_CHAR (ce.dc_fecha_cambio, 'ddmmyyyyhhmiss') in (");
		i=0;
		for (Iterator it = ids.iterator(); it.hasNext();i++){
        	if(i>0){
				condicion.append(",");
			}
			condicion.append(" '"+it.next().toString()+"' ");
		}
		condicion.append(") ");
		qrySentenciaDM.append(
				" SELECT " +
				" 	py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse, " +
				" 	TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision, " +
				" 	TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga, " +
				" 	TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc, " +
				" 	d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto, " +
				" 	DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', '', " +
				" 			'P', 'Dolar-Peso','') AS tipo_conversion, " +
				" 	DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), " +
				" 			'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio, " +
				" 	NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento, " +
				" 	decode(d.ic_tipo_financiamiento,1,NVL (d.fn_porc_descuento, 0),0) AS fn_porc_descuento, " +
				" 	tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus, " +
				" 	m.ic_moneda, d.ic_documento, e.cg_razon_social AS nombre_epo, " +
				" 	DECODE (NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza),'D', 'Delegada', " +
				" 	'N', 'No Delegada') AS tipo_cobranza, " +
				" 	i.cg_razon_social AS nombre_if, " +
				" 	TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fecha_cambio, " +
				" 	cce.cd_descripcion AS tipo_cambio_estatus, " +
				" 	lc.ic_moneda as moneda_linea , ce.ct_cambio_motivo as causa, " +
				" 	'ConsNoOperNafinDist::getDocumentSummaryQueryForIds' " +
				" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				" FROM dis_cambio_estatus ce, " +
				" 	dis_documento d, " +
				" 	dis_linea_credito_dm lc, " +
				" 	comcat_pyme py, " +
				" 	comrel_producto_epo pe, " +
				" 	com_tipo_cambio tc, " +
				" 	comcat_epo e, " +
				" 	comcat_if i, " +
				" 	comcat_tipo_financiamiento tf, " +
				" 	comcat_producto_nafin pn, " +
				" 	comcat_estatus_docto ed, " +
				" 	comcat_moneda m, " +
				" 	comcat_cambio_estatus cce " +
				" WHERE d.ic_pyme = py.ic_pyme " +
				" 	AND d.ic_moneda = m.ic_moneda " +
				" 	AND d.ic_epo = pe.ic_epo " +
				" 	AND d.ic_epo = e.ic_epo " +
				" 	AND d.ic_documento = ce.ic_documento " +
				" 	AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento " +
				" 	AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm " +
				" 	AND d.ic_estatus_docto = ed.ic_estatus_docto " +
				" 	AND lc.ic_if = i.ic_if " +
				" 	AND pe.ic_producto_nafin = pn.ic_producto_nafin " +
				" 	AND cce.ic_cambio_estatus = ce.ic_cambio_estatus " +
				" 	AND tc.dc_fecha IN (SELECT MAX (dc_fecha) " +
				" 			FROM com_tipo_cambio " +
				" 			WHERE ic_moneda = m.ic_moneda) " +
				" 	AND ce.dc_fecha_cambio IN (SELECT MAX (dc_fecha_cambio)  " +
				" 			FROM dis_cambio_estatus  " +
				" 			WHERE ic_documento = d.ic_documento) " +
				" 	AND tc.ic_moneda = m.ic_moneda " +
				" 	AND pe.ic_producto_nafin = 4  " + condicion);
		qrySentenciaCCC.append(
				" SELECT /*+ index ( ce in_dis_cambio_estatus_01_nuk) index ( d IN_DIS_DOCUMENTO_04_NUK) index (in_com_linea_credito_02_nuk) */ " +
				" 	py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse, " +
				" 	TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision, " +
				" 	TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga, " +
				" 	TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc, " +
				" 	d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto, " +
				" 	DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', '', " +
				" 			'P', 'Dolar-Peso','') AS tipo_conversion, " +
				" 	DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), " +
				" 			'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio, " +
				" 	NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento, " +
				" 	decode(d.ic_tipo_financiamiento,1,NVL (d.fn_porc_descuento, 0),0) AS fn_porc_descuento, " +
				" 	tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus, " +
				" 	m.ic_moneda, d.ic_documento, e.cg_razon_social AS nombre_epo, " +
				" 	DECODE (NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza),'D', 'Delegada', " +
				" 			'N', 'No Delegada') AS tipo_cobranza, " +
				" 	i.cg_razon_social AS nombre_if, " +
				" 	TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fecha_cambio, " +
				" 	cce.cd_descripcion AS tipo_cambio_estatus, " +
				" 	nvl(lc.ic_moneda,'') as moneda_linea, ce.ct_cambio_motivo as causa, " +
				" 	'ConsNoOperNafinDist::getDocumentSummaryQueryForIds' " +
				" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				" FROM dis_documento d, " +
				" 	dis_cambio_estatus ce, " +
				" 	comrel_producto_epo pe, " +
				" 	com_linea_credito lc, " +
				" 	com_tipo_cambio tc, " +
				" 	comcat_pyme py, " +
				" 	comcat_epo e, " +
				" 	comcat_if i, " +
				" 	comcat_tipo_financiamiento tf, " +
				" 	comcat_estatus_docto ed, " +
				" 	comcat_moneda m, " +
				" 	comcat_producto_nafin pn, " +
				" 	comcat_cambio_estatus cce " +
				" WHERE d.ic_pyme = py.ic_pyme " +
				" 	AND d.ic_moneda = m.ic_moneda " +
				" 	AND d.ic_epo = pe.ic_epo " +
				" 	AND d.ic_epo = e.ic_epo " +
				" 	AND d.ic_documento = ce.ic_documento " +
				" 	AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento " +
				" 	AND d.ic_linea_credito (+) = lc.ic_linea_credito " +
				" 	AND d.ic_estatus_docto = ed.ic_estatus_docto " +
				" 	AND lc.ic_if = i.ic_if " +
				" 	AND pe.ic_producto_nafin = pn.ic_producto_nafin " +
				" 	AND ce.ic_cambio_estatus = cce.ic_cambio_estatus " +
				" 	AND tc.dc_fecha IN (SELECT MAX (dc_fecha) " +
				" 			FROM com_tipo_cambio " +
				" 			WHERE ic_moneda = m.ic_moneda) " +
				" 	AND ce.dc_fecha_cambio IN (SELECT MAX (dc_fecha_cambio)  " +
				" 			FROM dis_cambio_estatus  " +
				" 			WHERE ic_documento = d.ic_documento) " +
				" 	AND tc.ic_moneda = m.ic_moneda " +
				" 	AND pe.ic_producto_nafin = 4 " + condicion);
		if ("D".equals(tipo_credito)) {
			qryAux.append(qrySentenciaDM);
		} else if ("C".equals(tipo_credito)) {
			qryAux.append(qrySentenciaCCC);
		} else {
			qryAux.append(qrySentenciaDM + " union all  " +
					 qrySentenciaCCC);
		}
		qrySentencia = qryAux;
		qrySentencia.append("ORDER BY nombre_dist ");
		log.debug("getDocumentSummaryQueryForIds :"+qrySentencia.toString());
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();
		
	}

	/**
	 *
	 */
	public String getDocumentQuery(HttpServletRequest request) {
		log.info("getDocumentQuery(E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer qrySentenciaDM = new StringBuffer();
		StringBuffer qrySentenciaCCC = new StringBuffer();
		StringBuffer qryAux = new StringBuffer();
		StringBuffer condicion    = new StringBuffer();
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		String ic_epo =	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String ic_pyme = (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String tipo_credito = (request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String modo_plazo = (request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
		String ic_cambio_estatus = (request.getParameter("ic_cambio_estatus")==null)?"":request.getParameter("ic_cambio_estatus");
		String dc_fecha_cambio_de =	(request.getParameter("dc_fecha_cambio_de")==null)?fechaHoy:request.getParameter("dc_fecha_cambio_de");
		String dc_fecha_cambio_a = (request.getParameter("dc_fecha_cambio_a")==null)?fechaHoy:request.getParameter("dc_fecha_cambio_a");
		String ic_if = (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String ig_numero_docto = (request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String fecha_vto_de = (request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String fecha_vto_a = (request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String fecha_publicacion_de = (request.getParameter("fecha_publicacion_de")==null)?"":request.getParameter("fecha_publicacion_de");
		String fecha_publicacion_a = (request.getParameter("fecha_publicacion_a")==null)?"":request.getParameter("fecha_publicacion_a");
		String ic_moneda = (request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String fn_monto_de = (request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String fn_monto_a = (request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String monto_con_descuento = (request.getParameter("monto_con_descuento")==null)?"":"checked";
		if(!"".equals(ic_epo)&&ic_epo!=null)
			condicion.append(" AND D.IC_EPO = "+ic_epo);
		if(!"".equals(ic_pyme)&&ic_pyme!=null)
			condicion.append(" AND D.IC_PYME = "+ic_pyme);
		if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
			condicion.append(" and D.ig_numero_docto = '"+ig_numero_docto+"'");
		if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
			condicion.append(" and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
		if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
			condicion.append(" and trunc(D.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))");
		if(!"".equals(ic_moneda)&&ic_moneda!=null)
			condicion.append(" and D.ic_moneda = "+ic_moneda);
		if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
			condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de+" and "+fn_monto_a);
		if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
			condicion.append(" and D.fn_monto between "+fn_monto_de+" and "+fn_monto_a);
		if(!"".equals(modo_plazo)&& modo_plazo!=null)
			condicion.append(" and TF.ic_tipo_financiamiento = "+modo_plazo);
		if(!"".equals(dc_fecha_cambio_de)&&dc_fecha_cambio_de!=null&&!"".equals(dc_fecha_cambio_a)&& dc_fecha_cambio_a!=null)
			condicion.append(" and trunc(CE.dc_fecha_cambio) between trunc(to_date('"+dc_fecha_cambio_de+"','dd/mm/yyyy')) and trunc(to_date('"+dc_fecha_cambio_a+"','dd/mm/yyyy'))");
		if(!"".equals(ic_if)&&ic_if!=null)
			condicion.append(" AND LC.IC_IF = "+ic_if);
		if(!"".equals(ic_cambio_estatus)&& ic_cambio_estatus!=null)
			condicion.append(" and CE.ic_cambio_estatus = "+ic_cambio_estatus);
		else
			condicion.append(" and CE.ic_cambio_estatus in(4,2,24)");
		qrySentenciaDM.append(
				" SELECT /*+ index (lc in_dis_documento_02_nuk) index ( ce in_dis_cambio_estatus_01_nuk) index ( d IN_DIS_DOCUMENTO_04_NUK) */ " +
				" 	ce.ic_documento || '_' || TO_CHAR(ce.dc_fecha_cambio, 'ddmmyyyyhhmiss'), " +
				" 	'ConsNoOperNafinDist::getDocumentQuery' " +
				" FROM dis_cambio_estatus ce, " +
				" 	dis_documento d, " +
				" 	dis_linea_credito_dm lc, " +
				" 	comcat_pyme py, " +
				" 	comrel_producto_epo pe, " +
				" 	com_tipo_cambio tc, " +
				" 	comcat_epo e, " +
				" 	comcat_if i, " +
				" 	comcat_tipo_financiamiento tf, " +
				" 	comcat_producto_nafin pn, " +
				" 	comcat_estatus_docto ed, " +
				" 	comcat_moneda m, " +
				" 	comcat_cambio_estatus cce " +
				" WHERE d.ic_pyme = py.ic_pyme " +
				" 	AND d.ic_moneda = m.ic_moneda " +
				" 	AND d.ic_epo = pe.ic_epo " +
				" 	AND d.ic_epo = e.ic_epo " +
				" 	AND d.ic_documento = ce.ic_documento " +
				" 	AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento " +
				" 	AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm " +
				" 	AND d.ic_estatus_docto = ed.ic_estatus_docto " +
				" 	AND lc.ic_if = i.ic_if " +
				" 	AND pe.ic_producto_nafin = pn.ic_producto_nafin " +
				" 	AND cce.ic_cambio_estatus = ce.ic_cambio_estatus " +
				" 	AND tc.dc_fecha IN (SELECT MAX (dc_fecha) " +
				" 			FROM com_tipo_cambio " +
				" 			WHERE ic_moneda = m.ic_moneda) " +
				" 	AND ce.dc_fecha_cambio IN (SELECT MAX (dc_fecha_cambio) " +
				" 			FROM dis_cambio_estatus WHERE ic_documento = d.ic_documento) " +
				" 	AND tc.ic_moneda = m.ic_moneda " +
				" 	AND e.cs_habilitado = 'S' " +
				" 	AND pe.ic_producto_nafin = 4 " + condicion);
		qrySentenciaCCC.append(
				" SELECT /*+ index ( ce in_dis_cambio_estatus_01_nuk) index ( d IN_DIS_DOCUMENTO_04_NUK) index (in_com_linea_credito_02_nuk) */ " +
				" 	ce.ic_documento || '_' || TO_CHAR(ce.dc_fecha_cambio, 'ddmmyyyyhhmiss'), " +
				" 	'ConsNoOperNafinDist::getDocumentQuery' " +
				" FROM dis_documento d, " +
				" 	dis_cambio_estatus ce, " +
				" 	comrel_producto_epo pe, " +
				" 	com_linea_credito lc, " +
				" 	com_tipo_cambio tc, " +
				" 	comcat_pyme py, " +
				" 	comcat_epo e, " +
				" 	comcat_if i, " +
				" 	comcat_tipo_financiamiento tf, " +
				" 	comcat_estatus_docto ed, " +
				" 	comcat_moneda m, " +
				" 	comcat_producto_nafin pn, " +
				" 	comcat_cambio_estatus cce " +
				" WHERE d.ic_pyme = py.ic_pyme " +
				" 	AND d.ic_moneda = m.ic_moneda " +
				" 	AND d.ic_epo = pe.ic_epo " +
				" 	AND d.ic_epo = e.ic_epo " +
				" 	AND d.ic_documento = ce.ic_documento " +
				" 	AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento " +
				" 	AND d.ic_linea_credito (+) = lc.ic_linea_credito " +
				" 	AND d.ic_estatus_docto = ed.ic_estatus_docto " +
				" 	AND lc.ic_if = i.ic_if " +
				" 	AND pe.ic_producto_nafin = pn.ic_producto_nafin " +
				" 	AND ce.ic_cambio_estatus = cce.ic_cambio_estatus " +
				" 	AND tc.dc_fecha IN (SELECT MAX (dc_fecha) " +
				" 			FROM com_tipo_cambio " +
				" 			WHERE ic_moneda = m.ic_moneda) " +
				" 	AND ce.dc_fecha_cambio IN (SELECT MAX (dc_fecha_cambio) " +
				" 			FROM dis_cambio_estatus WHERE ic_documento = d.ic_documento) " +
				" 	AND tc.ic_moneda = m.ic_moneda " +
				" 	AND e.cs_habilitado = 'S' " +
				" 	AND pe.ic_producto_nafin = 4 " + condicion);
		if ("D".equals(tipo_credito)) {
			qryAux.append(qrySentenciaDM);
		} else if ("C".equals(tipo_credito)) {
			qryAux.append(qrySentenciaCCC);
		} else {
			qryAux.append(qrySentenciaDM + " union all  " +
					 qrySentenciaCCC);
		}
		qrySentencia = qryAux;
		log.debug("getDocumentQuery: "+qrySentencia.toString());
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();
	}

	/**
	 *
	 */
	public String getDocumentQueryFile(HttpServletRequest request) {
		log.debug("getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer qrySentenciaDM = new StringBuffer();
		StringBuffer qrySentenciaCCC = new StringBuffer();
		StringBuffer qryAux = new StringBuffer();
		StringBuffer condicion    = new StringBuffer();
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		String ic_epo =	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String ic_pyme = (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String tipo_credito = (request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String modo_plazo = (request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
		String ic_cambio_estatus = (request.getParameter("ic_cambio_estatus")==null)?"":request.getParameter("ic_cambio_estatus");
		String dc_fecha_cambio_de =	(request.getParameter("dc_fecha_cambio_de")==null)?fechaHoy:request.getParameter("dc_fecha_cambio_de");
		String dc_fecha_cambio_a = (request.getParameter("dc_fecha_cambio_a")==null)?fechaHoy:request.getParameter("dc_fecha_cambio_a");
		String ic_if = (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String ig_numero_docto = (request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String fecha_vto_de = (request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String fecha_vto_a = (request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String fecha_publicacion_de = (request.getParameter("fecha_publicacion_de")==null)?"":request.getParameter("fecha_publicacion_de");
		String fecha_publicacion_a = (request.getParameter("fecha_publicacion_a")==null)?"":request.getParameter("fecha_publicacion_a");
		String ic_moneda = (request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String fn_monto_de = (request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String fn_monto_a = (request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String monto_con_descuento = (request.getParameter("monto_con_descuento")==null)?"":"checked";
		if(!"".equals(ic_epo)&&ic_epo!=null)
			condicion.append(" AND D.IC_EPO = "+ic_epo);
		if(!"".equals(ic_pyme)&&ic_pyme!=null)
			condicion.append(" AND D.IC_PYME = "+ic_pyme);
		if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
			condicion.append(" and D.ig_numero_docto = '"+ig_numero_docto+"'");
		if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
			condicion.append(" and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
		if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
			condicion.append(" and trunc(D.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))");
		if(!"".equals(ic_moneda)&&ic_moneda!=null)
			condicion.append(" and D.ic_moneda = "+ic_moneda);
		if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
			condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de+" and "+fn_monto_a);
		if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
			condicion.append(" and D.fn_monto between "+fn_monto_de+" and "+fn_monto_a);
		if(!"".equals(modo_plazo)&& modo_plazo!=null)
			condicion.append(" and TF.ic_tipo_financiamiento = "+modo_plazo);
		if(!"".equals(dc_fecha_cambio_de)&&dc_fecha_cambio_de!=null&&!"".equals(dc_fecha_cambio_a)&& dc_fecha_cambio_a!=null)
			condicion.append(" and trunc(CE.dc_fecha_cambio) between trunc(to_date('"+dc_fecha_cambio_de+"','dd/mm/yyyy')) and trunc(to_date('"+dc_fecha_cambio_a+"','dd/mm/yyyy'))");
		if(!"".equals(ic_if)&&ic_if!=null)
			condicion.append(" AND LC.IC_IF = "+ic_if);
		if(!"".equals(ic_cambio_estatus)&& ic_cambio_estatus!=null)
			condicion.append(" and CE.ic_cambio_estatus = "+ic_cambio_estatus);
		else
			condicion.append(" and CE.ic_cambio_estatus in(4,2,24)");
		qrySentenciaDM.append(
				" SELECT  " +
				" 	py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse, " +
				" 	TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision, " +
				" 	TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga, " +
				" 	TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc, " +
				" 	d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto, " +
				" 	DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', '', " +
				" 			'P', 'Dolar-Peso','') AS tipo_conversion, " +
				" 	DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), " +
				" 			'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio, " +
				" 	NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento, " +
				" 	decode(d.ic_tipo_financiamiento,1,NVL (d.fn_porc_descuento, 0),0) AS fn_porc_descuento, " +
				" 	tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus, " +
				" 	m.ic_moneda, d.ic_documento, e.cg_razon_social AS nombre_epo, " +
				" 	DECODE (NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza),'D', 'Delegada', " +
				" 	'N', 'No Delegada') AS tipo_cobranza, " +
				" 	i.cg_razon_social AS nombre_if, " +
				" 	TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fecha_cambio, " +
				" 	cce.cd_descripcion AS tipo_cambio_estatus, " +
				" 	lc.ic_moneda as moneda_linea , ce.ct_cambio_motivo as causa, " +
				" 	'ConsNoOperNafinDist::getDocumentQueryFile' " +
				" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				" FROM dis_cambio_estatus ce, " +
				" 	dis_documento d, " +
				" 	dis_linea_credito_dm lc, " +
				" 	comcat_pyme py, " +
				" 	comrel_producto_epo pe, " +
				" 	com_tipo_cambio tc, " +
				" 	comcat_epo e, " +
				" 	comcat_if i, " +
				" 	comcat_tipo_financiamiento tf, " +
				" 	comcat_producto_nafin pn, " +
				" 	comcat_estatus_docto ed, " +
				" 	comcat_moneda m, " +
				" 	comcat_cambio_estatus cce " +
				" WHERE d.ic_pyme = py.ic_pyme " +
				" 	AND d.ic_moneda = m.ic_moneda " +
				" 	AND d.ic_epo = pe.ic_epo " +
				" 	AND d.ic_epo = e.ic_epo " +
				" 	AND d.ic_documento = ce.ic_documento " +
				" 	AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento " +
				" 	AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm " +
				" 	AND d.ic_estatus_docto = ed.ic_estatus_docto " +
				" 	AND lc.ic_if = i.ic_if " +
				" 	AND pe.ic_producto_nafin = pn.ic_producto_nafin " +
				" 	AND cce.ic_cambio_estatus = ce.ic_cambio_estatus " +
				" 	AND tc.dc_fecha IN (SELECT MAX (dc_fecha) " +
				" 			FROM com_tipo_cambio " +
				" 			WHERE ic_moneda = m.ic_moneda) " +
				" 	AND ce.dc_fecha_cambio IN (SELECT MAX (dc_fecha_cambio)  " +
				" 			FROM dis_cambio_estatus  " +
				" 			WHERE ic_documento = d.ic_documento) " +
				" 	AND tc.ic_moneda = m.ic_moneda " +
				" 	AND e.cs_habilitado = 'S' " +
				" 	AND pe.ic_producto_nafin = 4  " + condicion);
		qrySentenciaCCC.append(
				" SELECT " +
				" 	py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse, " +
				" 	TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision, " +
				" 	TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga, " +
				" 	TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc, " +
				" 	d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto, " +
				" 	DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', '', " +
				" 			'P', 'Dolar-Peso','') AS tipo_conversion, " +
				" 	DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), " +
				" 			'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio, " +
				" 	NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento, " +
				" 	decode(d.ic_tipo_financiamiento,1,NVL (d.fn_porc_descuento, 0),0) AS fn_porc_descuento, " +
				" 	tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus, " +
				" 	m.ic_moneda, d.ic_documento, e.cg_razon_social AS nombre_epo, " +
				" 	DECODE (NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza),'D', 'Delegada', " +
				" 			'N', 'No Delegada') AS tipo_cobranza, " +
				" 	i.cg_razon_social AS nombre_if, " +
				" 	TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fecha_cambio, " +
				" 	cce.cd_descripcion AS tipo_cambio_estatus, " +
				" 	nvl(lc.ic_moneda,'') as moneda_linea, ce.ct_cambio_motivo as causa, " +
				" 	'ConsNoOperNafinDist::getDocumentQueryFile' " +
				" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				" FROM dis_documento d, " +
				" 	dis_cambio_estatus ce, " +
				" 	comrel_producto_epo pe, " +
				" 	com_linea_credito lc, " +
				" 	com_tipo_cambio tc, " +
				" 	comcat_pyme py, " +
				" 	comcat_epo e, " +
				" 	comcat_if i, " +
				" 	comcat_tipo_financiamiento tf, " +
				" 	comcat_estatus_docto ed, " +
				" 	comcat_moneda m, " +
				" 	comcat_producto_nafin pn, " +
				" 	comcat_cambio_estatus cce " +
				" WHERE d.ic_pyme = py.ic_pyme " +
				" 	AND d.ic_moneda = m.ic_moneda " +
				" 	AND d.ic_epo = pe.ic_epo " +
				" 	AND d.ic_epo = e.ic_epo " +
				" 	AND d.ic_documento = ce.ic_documento " +
				" 	AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento " +
				" 	AND d.ic_linea_credito (+) = lc.ic_linea_credito " +
				" 	AND d.ic_estatus_docto = ed.ic_estatus_docto " +
				" 	AND lc.ic_if = i.ic_if " +
				" 	AND pe.ic_producto_nafin = pn.ic_producto_nafin " +
				" 	AND ce.ic_cambio_estatus = cce.ic_cambio_estatus " +
				" 	AND tc.dc_fecha IN (SELECT MAX (dc_fecha) " +
				" 			FROM com_tipo_cambio " +
				" 			WHERE ic_moneda = m.ic_moneda) " +
				" 	AND ce.dc_fecha_cambio IN (SELECT MAX (dc_fecha_cambio)  " +
				" 			FROM dis_cambio_estatus  " +
				" 			WHERE ic_documento = d.ic_documento) " +
				" 	AND tc.ic_moneda = m.ic_moneda " +
				" 	AND e.cs_habilitado = 'S' " +
				" 	AND pe.ic_producto_nafin = 4 " + condicion);

		if ("D".equals(tipo_credito)) {
			qryAux.append(qrySentenciaDM);
		} else if ("C".equals(tipo_credito)) {
			qryAux.append(qrySentenciaCCC);
		} else {
			qryAux.append(qrySentenciaDM + " union all  " +
					 qrySentenciaCCC);
		}
		qrySentencia = qryAux;
		qrySentencia.append("ORDER BY nombre_dist ");
		log.info("getDocumentQueryFile : "+qrySentencia.toString());
		log.debug("getDocumentQueryFile(S)");
		return qrySentencia.toString();
	}

	/*******************************************************************************
	 *          Migraci�n. Implementa IQueryGeneratorRegExtJS.java                 *
	 *******************************************************************************/
	 public String getAggregateCalculationQuery(){
		log.info("getAggregateCalculationQuery(E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer qrySentenciaDM = new StringBuffer();
		StringBuffer qrySentenciaCCC = new StringBuffer();
		StringBuffer qryAux = new StringBuffer();
		StringBuffer condicion    = new StringBuffer();
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		if(!"".equals(claveEpo)&&claveEpo!=null)
			condicion.append(" AND D.IC_EPO = "+claveEpo);
		if(!"".equals( clavePyme)&& clavePyme!=null)
			condicion.append(" AND D.IC_PYME = "+ clavePyme);
		if(!"".equals(numeroDocto)&& numeroDocto!=null)
			condicion.append(" and D.ig_numero_docto = '"+numeroDocto+"'");
		if(!"".equals(fechaVtoDe)&& fechaVtoDe!=null&&!"".equals(fechaVtoA)&& fechaVtoA!=null)
			condicion.append(" and trunc(D.df_fecha_venc) between trunc(to_date('"+fechaVtoDe+"','dd/mm/yyyy')) and trunc(to_date('"+fechaVtoA+"','dd/mm/yyyy'))");
		if(!"".equals(fechaPublicacionDe)&& fechaPublicacionDe!=null&&!"".equals(fechaPublicacionA)&& fechaPublicacionA!=null)
			condicion.append(" and trunc(D.df_carga) between trunc(to_date('"+fechaPublicacionDe+"','dd/mm/yyyy')) and trunc(to_date('"+fechaPublicacionA+"','dd/mm/yyyy'))");
		if(!"".equals(moneda)&&moneda!=null)
			condicion.append(" and D.ic_moneda = "+moneda);
		if(!"".equals(montoDe)&& montoDe!=null&&!"".equals(montoA)&& montoA!=null&&!"".equals(montoDescuento))
			condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between "+montoDe+" and "+montoA);
		if(!"".equals(montoDe)&& montoDe!=null&&!"".equals(montoA)&& montoA!=null&&"".equals(montoDescuento))
			condicion.append(" and D.fn_monto between "+montoDe+" and "+montoA);
		if(!"".equals(modePlazo)&& modePlazo!=null)
			condicion.append(" and TF.ic_tipo_financiamiento = "+modePlazo);
		if(!"".equals(dcFechaCambioDe)&&dcFechaCambioDe!=null&&!"".equals(dcFechaCambioA)&& dcFechaCambioA!=null)
			condicion.append(" and trunc(CE.dc_fecha_cambio) between trunc(to_date('"+dcFechaCambioDe+"','dd/mm/yyyy')) and trunc(to_date('"+dcFechaCambioA+"','dd/mm/yyyy'))");
		if(!"".equals(claveIf)&&claveIf!=null)
			condicion.append(" AND LC.IC_IF = "+claveIf);
		if(!"".equals(icCambioEstatus)&& icCambioEstatus!=null)
			condicion.append(" and CE.ic_cambio_estatus = "+icCambioEstatus);
		else
			condicion.append(" and CE.ic_cambio_estatus in(4,2,24)");
		qrySentenciaDM.append(
				" SELECT  " +
				" 	m.ic_moneda, m.cd_nombre AS moneda, COUNT(*) as numRegistros, SUM(d.fn_monto) as monto, " +
				" 	SUM(DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), " +
				" 			'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') * d.fn_monto) as montoValuado " +
				" FROM dis_cambio_estatus ce, " +
				" 	dis_documento d, " +
				" 	dis_linea_credito_dm lc, " +
				" 	comrel_producto_epo pe, " +
				" 	com_tipo_cambio tc, " +
				" 	comcat_epo epo, " +
				" 	comcat_tipo_financiamiento tf, " +
				" 	comcat_producto_nafin pn, " +
				" 	comcat_moneda m " +
				" WHERE  " +
				" 	d.ic_moneda = m.ic_moneda " +
				" 	AND pe.ic_producto_nafin = pn.ic_producto_nafin " +
				" 	AND d.ic_epo = pe.ic_epo " +
				" 	AND pe.ic_epo = epo.ic_epo " +				
				" 	AND d.ic_documento = ce.ic_documento " +   
				" 	AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento " +
				" 	AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm " +
				" 	AND tc.dc_fecha IN (SELECT MAX (dc_fecha) " +
				" 			FROM com_tipo_cambio " +
				" 			WHERE ic_moneda = d.ic_moneda) " +
				" 	AND ce.dc_fecha_cambio IN (SELECT /*+index(dce)*/ MAX (dc_fecha_cambio) FROM dis_cambio_estatus dce WHERE ic_documento = d.ic_documento) " +
				" 	AND tc.ic_moneda = m.ic_moneda " +
				" 	AND pe.ic_producto_nafin = 4   " + condicion +
				" 	AND epo.cs_habilitado = 'S'   " + 				
				" GROUP BY m.ic_moneda, m.cd_nombre ");
		
		qrySentenciaCCC.append(   
				" SELECT  " +  
				" 	m.ic_moneda, m.cd_nombre AS moneda, count(*) as numRegistros, SUM(d.fn_monto) as monto, " +
				" 	SUM(DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), " +
				" 			'P', DECODE (d.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') * d.fn_monto) as montoValuado " +
				" FROM dis_documento d, " +
				" 	dis_cambio_estatus ce, " +
				" 	comrel_producto_epo pe, " +
				" 	com_linea_credito lc, " +
				" 	com_tipo_cambio tc, " +
				" 	comcat_epo epo, " +
				" 	comcat_tipo_financiamiento tf, " +
				" 	comcat_moneda m, " +
				" 	comcat_producto_nafin pn " +
				" WHERE  " +
				" 	d.ic_moneda = m.ic_moneda " +
				" 	AND d.ic_epo = pe.ic_epo " +
				" 	AND pe.ic_epo = epo.ic_epo " +
				" 	AND d.ic_documento = ce.ic_documento " +
				" 	AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento " +
				" 	AND d.ic_linea_credito (+) = lc.ic_linea_credito " +
				" 	AND pe.ic_producto_nafin = pn.ic_producto_nafin " +
				" 	AND tc.dc_fecha IN (SELECT MAX (dc_fecha) " +
				" 			FROM com_tipo_cambio " +
				" 			WHERE ic_moneda = d.ic_moneda) " +
				" 	AND ce.dc_fecha_cambio IN (SELECT MAX (dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_documento = d.ic_documento) " +
				" 	AND tc.ic_moneda = m.ic_moneda " +
				" 	AND pe.ic_producto_nafin = 4 " + condicion +
				" 	AND epo.cs_habilitado = 'S' " + 
				" GROUP BY m.ic_moneda, m.cd_nombre ");
		if ("D".equals(tipoCredito)) {
			qryAux.append(qrySentenciaDM);
		} else if ("C".equals(tipoCredito)) {
			qryAux.append(qrySentenciaCCC);
		} else {
			qryAux.append(qrySentenciaDM + " union all  " +
					 qrySentenciaCCC);
		}
		qrySentencia.append(
				" SELECT ic_moneda, moneda, SUM(numRegistros)," +
				" 	SUM(monto), SUM(montoValuado), 'ConsNoOperNafinDist::getAggregateCalculationQuery' " +
				" FROM (" + qryAux + ")" +
				" GROUP BY ic_moneda, moneda ");
		log.debug("getAggregateCalculationQuery: "+qrySentencia.toString());
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();
	}

 /**
 * Obtiene el query para obtener las llaves primarias de la consulta
 * @return Cadena con la consulta de SQL, para obtener llaves primarias
 */
	public String getDocumentQuery() {
		log.info("getDocumentQuery(E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer qrySentenciaDM = new StringBuffer();
		StringBuffer qrySentenciaCCC = new StringBuffer();
		StringBuffer qryAux = new StringBuffer();
		StringBuffer condicion    = new StringBuffer();
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		if(!"".equals(claveEpo)&&claveEpo!=null)
			condicion.append(" AND D.IC_EPO = "+claveEpo);
		if(!"".equals( clavePyme)&& clavePyme!=null)
			condicion.append(" AND D.IC_PYME = "+ clavePyme);
		if(!"".equals(numeroDocto)&& numeroDocto!=null)
			condicion.append(" and D.ig_numero_docto = '"+numeroDocto+"'");
		if(!"".equals(fechaVtoDe)&& fechaVtoDe!=null&&!"".equals(fechaVtoA)&& fechaVtoA!=null)
			condicion.append(" and trunc(D.df_fecha_venc) between trunc(to_date('"+fechaVtoDe+"','dd/mm/yyyy')) and trunc(to_date('"+fechaVtoA+"','dd/mm/yyyy'))");
		if(!"".equals(fechaPublicacionDe)&& fechaPublicacionDe!=null&&!"".equals(fechaPublicacionA)&& fechaPublicacionA!=null)
			condicion.append(" and trunc(D.df_carga) between trunc(to_date('"+fechaPublicacionDe+"','dd/mm/yyyy')) and trunc(to_date('"+fechaPublicacionA+"','dd/mm/yyyy'))");
		if(!"".equals(moneda)&&moneda!=null)
			condicion.append(" and D.ic_moneda = "+moneda);
		if(!"".equals(montoDe)&& montoDe!=null&&!"".equals(montoA)&& montoA!=null&&!"".equals(montoDescuento))
			condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between "+montoDe+" and "+montoA);
		if(!"".equals(montoDe)&& montoDe!=null&&!"".equals(montoA)&& montoA!=null&&"".equals(montoDescuento))
			condicion.append(" and D.fn_monto between "+montoDe+" and "+montoA);
		if(!"".equals(modePlazo)&& modePlazo!=null)
			condicion.append(" and TF.ic_tipo_financiamiento = "+modePlazo);
		if(!"".equals(dcFechaCambioDe)&&dcFechaCambioDe!=null&&!"".equals(dcFechaCambioA)&& dcFechaCambioA!=null)
			condicion.append(" and trunc(CE.dc_fecha_cambio) between trunc(to_date('"+dcFechaCambioDe+"','dd/mm/yyyy')) and trunc(to_date('"+dcFechaCambioA+"','dd/mm/yyyy'))");
		if(!"".equals(claveIf)&&claveIf!=null)
			condicion.append(" AND LC.IC_IF = "+claveIf);
		if(!"".equals(icCambioEstatus)&& icCambioEstatus!=null)
			condicion.append(" and CE.ic_cambio_estatus = "+icCambioEstatus);
		else
			condicion.append(" and CE.ic_cambio_estatus in(4,2,24)");

		qrySentenciaDM.append(
				" SELECT /*+ index (lc in_dis_documento_02_nuk) index ( ce in_dis_cambio_estatus_01_nuk) index ( d IN_DIS_DOCUMENTO_04_NUK) */ " +
				" 	ce.ic_documento || '_' || TO_CHAR(ce.dc_fecha_cambio, 'ddmmyyyyhhmiss'), " +
				" 	'ConsNoOperNafinDist::getDocumentQuery' " +
				" FROM dis_cambio_estatus ce, " +
				" 	dis_documento d, " +
				" 	dis_linea_credito_dm lc, " +
				" 	comcat_pyme py, " +
				" 	comrel_producto_epo pe, " +
				" 	com_tipo_cambio tc, " +
				" 	comcat_epo e, " +
				" 	comcat_if i, " +
				" 	comcat_tipo_financiamiento tf, " +
				" 	comcat_producto_nafin pn, " +
				" 	comcat_estatus_docto ed, " +
				" 	comcat_moneda m, " +
				" 	comcat_cambio_estatus cce " +
				" WHERE d.ic_pyme = py.ic_pyme " +
				" 	AND d.ic_moneda = m.ic_moneda " +
				" 	AND d.ic_epo = pe.ic_epo " +
				" 	AND d.ic_epo = e.ic_epo " +
				" 	AND d.ic_documento = ce.ic_documento " +
				" 	AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento " +
				" 	AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm " +
				" 	AND d.ic_estatus_docto = ed.ic_estatus_docto " +
				" 	AND lc.ic_if = i.ic_if " +
				" 	AND pe.ic_producto_nafin = pn.ic_producto_nafin " +
				" 	AND cce.ic_cambio_estatus = ce.ic_cambio_estatus " +
				" 	AND tc.dc_fecha IN (SELECT MAX (dc_fecha) " +
				" 			FROM com_tipo_cambio " +
				" 			WHERE ic_moneda = m.ic_moneda) " +
				" 	AND ce.dc_fecha_cambio IN (SELECT MAX (dc_fecha_cambio) " +
				" 			FROM dis_cambio_estatus WHERE ic_documento = d.ic_documento) " +
				" 	AND tc.ic_moneda = m.ic_moneda " +
				" 	AND e.cs_habilitado = 'S' " +
				" 	AND pe.ic_producto_nafin = 4 " + condicion.toString());
		
		qrySentenciaCCC.append(
				" SELECT /*+ index ( ce in_dis_cambio_estatus_01_nuk) index ( d IN_DIS_DOCUMENTO_04_NUK) index (in_com_linea_credito_02_nuk) */ " +
				" 	ce.ic_documento || '_' || TO_CHAR(ce.dc_fecha_cambio, 'ddmmyyyyhhmiss'), " +
				" 	'ConsNoOperNafinDist::getDocumentQuery' " +
				" FROM dis_documento d, " +
				" 	dis_cambio_estatus ce, " +
				" 	comrel_producto_epo pe, " +
				" 	com_linea_credito lc, " +
				" 	com_tipo_cambio tc, " +
				" 	comcat_pyme py, " +
				" 	comcat_epo e, " +
				" 	comcat_if i, " +
				" 	comcat_tipo_financiamiento tf, " +
				" 	comcat_estatus_docto ed, " +
				" 	comcat_moneda m, " +
				" 	comcat_producto_nafin pn, " +
				" 	comcat_cambio_estatus cce " +
				" WHERE d.ic_pyme = py.ic_pyme " +
				" 	AND d.ic_moneda = m.ic_moneda " +
				" 	AND d.ic_epo = pe.ic_epo " +
				" 	AND d.ic_epo = e.ic_epo " +
				" 	AND d.ic_documento = ce.ic_documento " +
				" 	AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento " +
				" 	AND d.ic_linea_credito (+) = lc.ic_linea_credito " +
				" 	AND d.ic_estatus_docto = ed.ic_estatus_docto " +
				" 	AND lc.ic_if = i.ic_if " +
				" 	AND pe.ic_producto_nafin = pn.ic_producto_nafin " +
				" 	AND ce.ic_cambio_estatus = cce.ic_cambio_estatus " +
				" 	AND tc.dc_fecha IN (SELECT MAX (dc_fecha) " +
				" 			FROM com_tipo_cambio " +
				" 			WHERE ic_moneda = m.ic_moneda) " +
				" 	AND ce.dc_fecha_cambio IN (SELECT MAX (dc_fecha_cambio) " +
				" 			FROM dis_cambio_estatus WHERE ic_documento = d.ic_documento) " +
				" 	AND tc.ic_moneda = m.ic_moneda " +
				" 	AND e.cs_habilitado = 'S' " +
				" 	AND pe.ic_producto_nafin = 4 " + condicion.toString());
		if ("D".equals(tipoCredito)) {
			qryAux.append(qrySentenciaDM);
		} else if ("C".equals(tipoCredito)) {
			qryAux.append(qrySentenciaCCC);
		} else {
			qryAux.append(qrySentenciaDM + " union all  " +
					 qrySentenciaCCC);
		}
		qrySentencia = qryAux;
		log.info("getDocumentQuery: "+qrySentencia.toString());
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();

	}

/**
 * Obtiene el query necesario para mostrar la informaci�n completa de 
 * una p�gina a partir de las llaves primarias enviadas como par�metro
 * @return Cadena con la consulta de SQL, para obtener la informaci�n
 * 	completa de los registros con las llaves especificadas
 */
	public String getDocumentSummaryQueryForIds(List pageIds) {
		log.info("getDocumentSummaryQueryForIds(E)");
		StringBuffer qrySentencia	= new StringBuffer();
		StringBuffer qrySentenciaDM	= new StringBuffer();
		StringBuffer qrySentenciaCCC	= new StringBuffer();
		StringBuffer qryAux	= new StringBuffer();
    	StringBuffer condicion		= new StringBuffer();
		conditions 		= new ArrayList();
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		condicion.append(" AND ce.ic_documento||'_'||TO_CHAR (ce.dc_fecha_cambio, 'ddmmyyyyhhmiss') in (");
		conditions 		= new ArrayList();
		for (int i = 0; i < pageIds.size(); i++){
			List lItem = (ArrayList)pageIds.get(i);
			condicion.append(" '"+lItem.get(0).toString()+"' ");
			condicion.append(",");
		}
		condicion.deleteCharAt(condicion.length()-1);
		qrySentenciaDM.append(
				" SELECT " +
				" 	py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse, " +
				" 	TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision, " +
				" 	TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga, " +
				" 	TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc, " +
				" 	d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto, " +
				" 	DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', '', " +
				" 			'P', 'Dolar-Peso','') AS tipo_conversion, " +
				" 	DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), " +
				" 			'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio, " +
				" 	NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento, " +
				" 	decode(d.ic_tipo_financiamiento,1,NVL (d.fn_porc_descuento, 0),0) AS fn_porc_descuento, " +
				" 	tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus, " +
				" 	m.ic_moneda, d.ic_documento, e.cg_razon_social AS nombre_epo, " +
				" 	DECODE (NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza),'D', 'Delegada', " +
				" 	'N', 'No Delegada') AS tipo_cobranza, " +
				" 	i.cg_razon_social AS nombre_if, " +
				" 	TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fecha_cambio, " +
				" 	cce.cd_descripcion AS tipo_cambio_estatus, " +
				" 	lc.ic_moneda as moneda_linea , ce.ct_cambio_motivo as causa, " +
				" 	'ConsNoOperNafinDist::getDocumentSummaryQueryForIds' " +
				" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				" FROM dis_cambio_estatus ce, " +
				" 	dis_documento d, " +
				" 	dis_linea_credito_dm lc, " +
				" 	comcat_pyme py, " +
				" 	comrel_producto_epo pe, " +
				" 	com_tipo_cambio tc, " +
				" 	comcat_epo e, " +
				" 	comcat_if i, " +
				" 	comcat_tipo_financiamiento tf, " +
				" 	comcat_producto_nafin pn, " +
				" 	comcat_estatus_docto ed, " +
				" 	comcat_moneda m, " +
				" 	comcat_cambio_estatus cce " +
				" WHERE d.ic_pyme = py.ic_pyme " +
				" 	AND d.ic_moneda = m.ic_moneda " +
				" 	AND d.ic_epo = pe.ic_epo " +
				" 	AND d.ic_epo = e.ic_epo " +
				" 	AND d.ic_documento = ce.ic_documento " +
				" 	AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento " +
				" 	AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm " +
				" 	AND d.ic_estatus_docto = ed.ic_estatus_docto " +
				" 	AND lc.ic_if = i.ic_if " +
				" 	AND pe.ic_producto_nafin = pn.ic_producto_nafin " +
				" 	AND cce.ic_cambio_estatus = ce.ic_cambio_estatus " +
				" 	AND tc.dc_fecha IN (SELECT MAX (dc_fecha) " +
				" 			FROM com_tipo_cambio " +
				" 			WHERE ic_moneda = m.ic_moneda) " +
				" 	AND ce.dc_fecha_cambio IN (SELECT MAX (dc_fecha_cambio)  " +
				" 			FROM dis_cambio_estatus  " +
				" 			WHERE ic_documento = d.ic_documento) " +
				" 	AND tc.ic_moneda = m.ic_moneda " +
				" 	AND pe.ic_producto_nafin = 4  " + condicion.toString()+")");
		
		qrySentenciaCCC.append(
				" SELECT  " +
				" 	py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse, " +
				" 	TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision, " +
				" 	TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga, " +
				" 	TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc, " +
				" 	d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto, " +
				" 	DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', '', " +
				" 			'P', 'Dolar-Peso','') AS tipo_conversion, " +
				" 	DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), " +
				" 			'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio, " +
				" 	NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento, " +
				" 	decode(d.ic_tipo_financiamiento,1,NVL (d.fn_porc_descuento, 0),0) AS fn_porc_descuento, " +
				" 	tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus, " +
				" 	m.ic_moneda, d.ic_documento, e.cg_razon_social AS nombre_epo, " +
				" 	DECODE (NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza),'D', 'Delegada', " +
				" 			'N', 'No Delegada') AS tipo_cobranza, " +
				" 	i.cg_razon_social AS nombre_if, " +
				" 	TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fecha_cambio, " +
				" 	cce.cd_descripcion AS tipo_cambio_estatus, " +
				" 	nvl(lc.ic_moneda,'') as moneda_linea, ce.ct_cambio_motivo as causa, " +
				" 	'ConsNoOperNafinDist::getDocumentSummaryQueryForIds' " +
				" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				" FROM dis_documento d, " +
				" 	dis_cambio_estatus ce, " +
				" 	comrel_producto_epo pe, " +
				" 	com_linea_credito lc, " +
				" 	com_tipo_cambio tc, " +
				" 	comcat_pyme py, " +
				" 	comcat_epo e, " +
				" 	comcat_if i, " +
				" 	comcat_tipo_financiamiento tf, " +
				" 	comcat_estatus_docto ed, " +
				" 	comcat_moneda m, " +
				" 	comcat_producto_nafin pn, " +
				" 	comcat_cambio_estatus cce " +
				" WHERE d.ic_pyme = py.ic_pyme " +
				" 	AND d.ic_moneda = m.ic_moneda " +
				" 	AND d.ic_epo = pe.ic_epo " +
				" 	AND d.ic_epo = e.ic_epo " +
				" 	AND d.ic_documento = ce.ic_documento " +
				" 	AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento " +
				" 	AND d.ic_linea_credito (+) = lc.ic_linea_credito " +
				" 	AND d.ic_estatus_docto = ed.ic_estatus_docto " +
				" 	AND lc.ic_if = i.ic_if " +
				" 	AND pe.ic_producto_nafin = pn.ic_producto_nafin " +
				" 	AND ce.ic_cambio_estatus = cce.ic_cambio_estatus " +
				" 	AND tc.dc_fecha IN (SELECT MAX (dc_fecha) " +
				" 			FROM com_tipo_cambio " +
				" 			WHERE ic_moneda = m.ic_moneda) " +
				" 	AND ce.dc_fecha_cambio IN (SELECT MAX (dc_fecha_cambio)  " +
				" 			FROM dis_cambio_estatus  " +
				" 			WHERE ic_documento = d.ic_documento) " +
				" 	AND tc.ic_moneda = m.ic_moneda " +
				" 	AND pe.ic_producto_nafin = 4 " + condicion.toString()+")");

		if ("D".equals(tipoCredito)) {
			qryAux.append(qrySentenciaDM);
		} else if ("C".equals(tipoCredito)) {
			qryAux.append(qrySentenciaCCC);
		} else {
			qryAux.append(qrySentenciaDM + " union all  " +
					 qrySentenciaCCC);
		}
		qrySentencia = qryAux;
		qrySentencia.append("ORDER BY nombre_dist ");
		log.debug("getDocumentSummaryQueryForIds: "+qrySentencia.toString());
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();
	}

	public String getDocumentQueryFile(){
		log.info("getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer qrySentenciaDM = new StringBuffer();
		StringBuffer qrySentenciaCCC = new StringBuffer();
		StringBuffer qryAux = new StringBuffer();
		StringBuffer condicion    = new StringBuffer();
		conditions 		= new ArrayList();
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		if("1".equals(tipoConsulta)) {
			if(!"".equals(claveEpo)&&claveEpo!=null)
				condicion.append(" AND D.IC_EPO = "+claveEpo);
			if(!"".equals( clavePyme)&& clavePyme!=null)
				condicion.append(" AND D.IC_PYME = "+ clavePyme);
			if(!"".equals(numeroDocto)&& numeroDocto!=null)
				condicion.append(" and D.ig_numero_docto = '"+numeroDocto+"'");
			if(!"".equals(fechaVtoDe)&& fechaVtoDe!=null&&!"".equals(fechaVtoA)&& fechaVtoA!=null)
				condicion.append(" and trunc(D.df_fecha_venc) between trunc(to_date('"+fechaVtoDe+"','dd/mm/yyyy')) and trunc(to_date('"+fechaVtoA+"','dd/mm/yyyy'))");
			if(!"".equals(fechaPublicacionDe)&& fechaPublicacionDe!=null&&!"".equals(fechaPublicacionA)&& fechaPublicacionA!=null)
				condicion.append(" and trunc(D.df_carga) between trunc(to_date('"+fechaPublicacionDe+"','dd/mm/yyyy')) and trunc(to_date('"+fechaPublicacionA+"','dd/mm/yyyy'))");
			if(!"".equals(moneda)&&moneda!=null)
				condicion.append(" and D.ic_moneda = "+moneda);
			if(!"".equals(montoDe)&& montoDe!=null&&!"".equals(montoA)&& montoA!=null&&!"".equals(montoDescuento))
				condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between "+montoDe+" and "+montoA);
			if(!"".equals(montoDe)&& montoDe!=null&&!"".equals(montoA)&& montoA!=null&&"".equals(montoDescuento))
				condicion.append(" and D.fn_monto between "+montoDe+" and "+montoA);
			if(!"".equals(modePlazo)&& modePlazo!=null)
					condicion.append(" and TF.ic_tipo_financiamiento = "+modePlazo);
			if(!"".equals(dcFechaCambioDe)&&dcFechaCambioDe!=null&&!"".equals(dcFechaCambioA)&& dcFechaCambioA!=null)
				condicion.append(" and trunc(CE.dc_fecha_cambio) between trunc(to_date('"+dcFechaCambioDe+"','dd/mm/yyyy')) and trunc(to_date('"+dcFechaCambioA+"','dd/mm/yyyy'))");
			if(!"".equals(claveIf)&&claveIf!=null)
				condicion.append(" AND LC.IC_IF = "+claveIf);
			if(!"".equals(icCambioEstatus)&& icCambioEstatus!=null)
				condicion.append(" and CE.ic_cambio_estatus = "+icCambioEstatus);
			else
				condicion.append(" and CE.ic_cambio_estatus in(4,2,24)");

			qrySentenciaDM.append(
					" SELECT  " +
					" 	py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse, " +
					" 	TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision, " +
					" 	TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga, " +
					" 	TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc, " +
					" 	d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto, " +
					" 	DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', '', " +
					" 			'P', 'Dolar-Peso','') AS tipo_conversion, " +
					" 	DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), " +
					" 			'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio, " +
					" 	NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento, " +
					" 	decode(d.ic_tipo_financiamiento,1,NVL (d.fn_porc_descuento, 0),0) AS fn_porc_descuento, " +
					" 	tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus, " +
					" 	m.ic_moneda, d.ic_documento, e.cg_razon_social AS nombre_epo, " +
					" 	DECODE (NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza),'D', 'Delegada', " +
					" 	'N', 'No Delegada') AS tipo_cobranza, " +
					" 	i.cg_razon_social AS nombre_if, " +
					" 	TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fecha_cambio, " +
					" 	cce.cd_descripcion AS tipo_cambio_estatus, " +
					" 	lc.ic_moneda as moneda_linea , ce.ct_cambio_motivo as causa, " +
					" 	'ConsNoOperNafinDist::getDocumentQueryFile' " +
					" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
					" FROM dis_cambio_estatus ce, " +
					" 	dis_documento d, " +
					" 	dis_linea_credito_dm lc, " +
					" 	comcat_pyme py, " +
					" 	comrel_producto_epo pe, " +
					" 	com_tipo_cambio tc, " +
					" 	comcat_epo e, " +
					" 	comcat_if i, " +
					" 	comcat_tipo_financiamiento tf, " +
					" 	comcat_producto_nafin pn, " +
					" 	comcat_estatus_docto ed, " +
					" 	comcat_moneda m, " +
					" 	comcat_cambio_estatus cce " +
					" WHERE d.ic_pyme = py.ic_pyme " +
					" 	AND d.ic_moneda = m.ic_moneda " +
					" 	AND d.ic_epo = pe.ic_epo " +
					" 	AND d.ic_epo = e.ic_epo " +
					" 	AND d.ic_documento = ce.ic_documento " +
					" 	AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento " +
					" 	AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm " +
					" 	AND d.ic_estatus_docto = ed.ic_estatus_docto " +
					" 	AND lc.ic_if = i.ic_if " +
					" 	AND pe.ic_producto_nafin = pn.ic_producto_nafin " +
					" 	AND cce.ic_cambio_estatus = ce.ic_cambio_estatus " +
					" 	AND tc.dc_fecha IN (SELECT MAX (dc_fecha) " +
					" 			FROM com_tipo_cambio " +
					" 			WHERE ic_moneda = m.ic_moneda) " +
					" 	AND ce.dc_fecha_cambio IN (SELECT MAX (dc_fecha_cambio)  " +
					" 			FROM dis_cambio_estatus  " +
					" 			WHERE ic_documento = d.ic_documento) " +
					" 	AND tc.ic_moneda = m.ic_moneda " +
					" 	AND e.cs_habilitado = 'S' " +
					" 	AND pe.ic_producto_nafin = 4  " + condicion.toString());
		
			qrySentenciaCCC.append(
					" SELECT  " +
					" 	py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse, " +
					" 	TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision, " +
					" 	TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga, " +
					" 	TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc, " +
					" 	d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto, " +
					" 	DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', '', " +
					" 			'P', 'Dolar-Peso','') AS tipo_conversion, " +
					" 	DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), " +
					" 			'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio, " +
					" 	NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento, " +
					" 	decode(d.ic_tipo_financiamiento,1,NVL (d.fn_porc_descuento, 0),0) AS fn_porc_descuento, " +
					" 	tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus, " +
					" 	m.ic_moneda, d.ic_documento, e.cg_razon_social AS nombre_epo, " +
					" 	DECODE (NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza),'D', 'Delegada', " +
					" 			'N', 'No Delegada') AS tipo_cobranza, " +
					" 	i.cg_razon_social AS nombre_if, " +
					" 	TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fecha_cambio, " +
					" 	cce.cd_descripcion AS tipo_cambio_estatus, " +
					" 	nvl(lc.ic_moneda,'') as moneda_linea, ce.ct_cambio_motivo as causa, " +
					" 	'ConsNoOperNafinDist::getDocumentQueryFile' " +
					" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
					" FROM dis_documento d, " +
					" 	dis_cambio_estatus ce, " +
					" 	comrel_producto_epo pe, " +
					" 	com_linea_credito lc, " +
					" 	com_tipo_cambio tc, " +
					" 	comcat_pyme py, " +
					" 	comcat_epo e, " +
					" 	comcat_if i, " +
					" 	comcat_tipo_financiamiento tf, " +
					" 	comcat_estatus_docto ed, " +
					" 	comcat_moneda m, " +
					" 	comcat_producto_nafin pn, " +
					" 	comcat_cambio_estatus cce " +
					" WHERE d.ic_pyme = py.ic_pyme " +
					" 	AND d.ic_moneda = m.ic_moneda " +
					" 	AND d.ic_epo = pe.ic_epo " +
					" 	AND d.ic_epo = e.ic_epo " +
					" 	AND d.ic_documento = ce.ic_documento " +
					" 	AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento " +
					" 	AND d.ic_linea_credito (+) = lc.ic_linea_credito " +
					" 	AND d.ic_estatus_docto = ed.ic_estatus_docto " +
					" 	AND lc.ic_if = i.ic_if " +
					" 	AND pe.ic_producto_nafin = pn.ic_producto_nafin " +
					" 	AND ce.ic_cambio_estatus = cce.ic_cambio_estatus " +
					" 	AND tc.dc_fecha IN (SELECT MAX (dc_fecha) " +
					" 			FROM com_tipo_cambio " +
					" 			WHERE ic_moneda = m.ic_moneda) " +
					" 	AND ce.dc_fecha_cambio IN (SELECT MAX (dc_fecha_cambio)  " +
					" 			FROM dis_cambio_estatus  " +
					" 			WHERE ic_documento = d.ic_documento) " +
					" 	AND tc.ic_moneda = m.ic_moneda " +
					" 	AND e.cs_habilitado = 'S' " +
					" 	AND pe.ic_producto_nafin = 4 " + condicion.toString());
			if ("D".equals(tipoCredito)) {
				qryAux.append(qrySentenciaDM);
			} else if ("C".equals(tipoCredito)) {
				qryAux.append(qrySentenciaCCC);
			} else {
				qryAux.append(qrySentenciaDM + " union all  " + qrySentenciaCCC);
			}
			qrySentencia = qryAux;
			qrySentencia.append("ORDER BY nombre_dist ");
		}else	if("Detalle".equals(tipoConsulta)) {
			qrySentencia.append(" SELECT DISTINCT TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS FECHA_CAMBIO, "+
					" cce.cd_descripcion as CAMBIO_ESTATUS ,       "+           
					" TO_CHAR (ce.df_fecha_emision_anterior, 'dd/mm/yyyy' ) As F_EMISION_ANTERIOR ,  "+
					" TO_CHAR (ce.df_fecha_emision_nueva, 'dd/mm/yyyy'  ) AS F_EMISION_NUEVA ,  "+
					" ce.fn_monto_anterior  as MONTO_ANTERIOR  ,  "+
					" ce.fn_monto_nuevo as MONTO_NUEVO ,   "+
					" TO_CHAR (ce.df_fecha_venc_anterior, 'dd/mm/yyyy'  ) AS F_VENCIMIENTO_ANTERIOR,  "+
					" TO_CHAR (ce.df_fecha_venc_nueva, 'dd/mm/yyyy' ) AS F_VENCIMIENTO_NUEVO,      "+               
					" tfa.cd_descripcion AS MODALIDAD_ANTERIOR  ,  "+
					" tfn.cd_descripcion AS MODALIDAD_NUEVO  "+
					" FROM dis_cambio_estatus ce, "+
					" comcat_cambio_estatus cce, "+
					" comcat_tipo_financiamiento tfa, "+
					" comcat_tipo_financiamiento tfn "+
					" WHERE cce.ic_cambio_estatus = ce.ic_cambio_estatus "+
					"  AND ce.ic_tipo_finan_ant = tfa.ic_tipo_financiamiento(+)  "+
					"  AND ce.ic_tipo_finan_nuevo = tfn.ic_tipo_financiamiento(+) ");
				 
			if(!"".equals(ic_documento) ){
				qrySentencia.append(" and  ic_documento = ? ");
				conditions.add(ic_documento);
			}
			
		}
		
		log.debug("getDocumentQueryFile:"+qrySentencia.toString());
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();
	}
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		log.info("crearPageCustomFile(E)");
		if("PDF".equals(tipo)){
			try{
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
				int columnas = 13;
				String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual		= fechaActual.substring(0,2);
				String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual		= fechaActual.substring(6,10);
				String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.setLTable(13, 100);
				pdfDoc.setLCell("A","fromas",ComunesPDF.CENTER);
				pdfDoc.setLCell("EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Distribuidor","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("N�mero de documento inicial","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Num. acuse","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de emisi�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de publicaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo docto.","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo para descuento en dias","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("% de descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("B","fromas",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto % de descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo conv.","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo cambio","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto valuado en pesos","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Modalidad de plazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de cobranza","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de cambio de estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de cambio de estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Causa","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);
				while (rs.next()){
					String fechaEmision =(rs.getString("df_fecha_emision")==null)?" ":rs.getString("df_fecha_emision");
					String nombreDist =(rs.getString("nombre_dist")==null)?" ":rs.getString("ig_numero_docto");
					String ccAcuse =(rs.getString("cc_acuse")==null)?" ":rs.getString("cc_acuse");
					String igNumeroDocto =(rs.getString("ig_numero_docto")==null)?" ":rs.getString("nombre_dist");
					String dfCarga = (rs.getString("df_carga")==null)?" ":rs.getString("df_carga");
					String dfFechaVenc = (rs.getString("df_fecha_venc")==null)?" ":rs.getString("df_fecha_venc");
					String igPlazoDocto = (rs.getString("ig_plazo_docto")==null)?" ":rs.getString("ig_plazo_docto");
					String moneda = (rs.getString("moneda")==null)?" ":rs.getString("moneda");
					String fnMonto = (rs.getString("fn_monto")==null)?" ":rs.getString("fn_monto");
					String monedaLinea = (rs.getString("moneda_linea")==null)?" ":rs.getString("moneda_linea");
					String tipoConversion = (rs.getString("tipo_conversion")==null)?" ":rs.getString("tipo_conversion");
					String tipoCambio = (rs.getString("tipo_cambio")==null)?" ":rs.getString("tipo_cambio");
					String igPlazoDescuento = (rs.getString("ig_plazo_descuento")==null)?" ":rs.getString("ig_plazo_descuento");
					String fnPorcDescuento = (rs.getString("fn_porc_descuento")==null)?" ":rs.getString("fn_porc_descuento");
					String montoDescontar		=String.valueOf((Double.parseDouble(fnMonto))*Double.parseDouble(fnPorcDescuento)/100);
					String modoPlazo = (rs.getString("modo_plazo")==null)?" ":rs.getString("modo_plazo");
					String estatus = (rs.getString("estatus")==null)?" ":rs.getString("estatus");
					String icMoneda = (rs.getString("ic_moneda")==null)?" ":rs.getString("ic_moneda");
					String aux1 = (icMoneda.equals(monedaLinea)||Integer.parseInt(tipoCambio)==1)?"":tipoConversion;
					String aux2=(icMoneda.equals(monedaLinea)||Integer.parseInt(tipoCambio)==1)?"":""+tipoCambio;
					String icDocumento = (rs.getString("ic_documento")==null)?" ":rs.getString("ic_documento");
					String nombreEPO = (rs.getString("nombre_epo")==null)?" ":rs.getString("nombre_epo");
					String tipoCobranza = (rs.getString("tipo_cobranza")==null)?" ":rs.getString("tipo_cobranza");
					String nombreIF = (rs.getString("nombre_if")==null)?" ":rs.getString("nombre_if");
					String fechaCambio = (rs.getString("fecha_cambio")==null)?" ":rs.getString("fecha_cambio");
					String tipoCambioEstatus = (rs.getString("tipo_cambio_estatus")==null)?" ":rs.getString("tipo_cambio_estatus");
					String causa = (rs.getString("causa")==null)?" ":rs.getString("causa");
					String montoValuado		= String.valueOf(Double.parseDouble(fnMonto)*Double.parseDouble(tipoCambio));
					String auxMontoValuado = (icMoneda.equals(monedaLinea)||Integer.parseInt(tipoCambio)==1)?" ":Comunes.formatoDecimal(montoValuado,2);
					pdfDoc.setLCell("A","fromas",ComunesPDF.CENTER);
					pdfDoc.setLCell(nombreEPO,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(igNumeroDocto,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(nombreDist,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(ccAcuse,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaEmision,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(dfFechaVenc,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(dfCarga,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(igPlazoDocto,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(fnMonto,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(igPlazoDescuento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fnPorcDescuento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("B","fromas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoDescontar,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(aux1,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(aux2,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(auxMontoValuado,"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(modoPlazo,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipoCobranza,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(estatus,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(nombreIF,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipoCambioEstatus,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaCambio,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(causa,"formas",ComunesPDF.JUSTIFIED);
					pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
				//	pdfDoc.setLCell("","columnas",ComunesPDF.CENTER, columnas);
				}
				pdfDoc.addLTable();
				pdfDoc.endDocument();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo",e);
			}
		}
		log.info("crearPageCustomFile(S)");
		return nombreArchivo;
	}
/**
 * En este m�todo se debe realizar la implementaci�n de la generaci�n del archivo
 * con base en el resulset que se recibe como par�metro. El ResulSet es el resultado
 * de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
 * @param request HttpRequest empleado principalmente para obtener el objeto session
 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
 * @param path Ruta f�sica donde se generar� el archivo
 * @param tipo cadena que identifica el tipo o variante de archivo que va a generar.
 * @return Cadena con la ruta del archivo generado
*/
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){

		log.info("crearCustomFile(E)");

		StringBuffer linea = new StringBuffer(1024);
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		int total = 0;

		String causaAux="";
		int tama�o=0;
		if(tipo.equals("CSV")){
			try {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
				buffer = new BufferedWriter(writer);
				int indiceCamposAdicionales =0;
				contenidoArchivo = new StringBuffer();	
				contenidoArchivo.append("Epo, Distribuidor,N�mero de documento inicial.,Num. Acuse,Fecha de emisi�n, Fecha de vencimiento,Fecha de publicaci�n, Plazo docto, Moneda,Monto,Plazo para descuento en dias,% de descuento,Monto % de descuento");
				contenidoArchivo.append(",Tipo conv., Tipo cambio,Monto valuado en pesos,Modalidad de Plazo,Tipo de cobranza,Estatus,IF,Tipo de Cambio de Estatus, Fecha de Cambio de Estatus,Causa"+"\n");
				while (rs.next()) {
					String fechaEmision      = (rs.getString("df_fecha_emision")    ==null)?" ":rs.getString("df_fecha_emision");
					String nombreDist        = (rs.getString("nombre_dist")         ==null)?" ":rs.getString("ig_numero_docto");
					String ccAcuse           = (rs.getString("cc_acuse")            ==null)?" ":rs.getString("cc_acuse");
					String igNumeroDocto     = (rs.getString("ig_numero_docto")     ==null)?" ":rs.getString("nombre_dist");
					String dfCarga           = (rs.getString("df_carga")            ==null)?" ":rs.getString("df_carga");
					String dfFechaVenc       = (rs.getString("df_fecha_venc")       ==null)?" ":rs.getString("df_fecha_venc");
					String igPlazoDocto      = (rs.getString("ig_plazo_docto")      ==null)?" ":rs.getString("ig_plazo_docto");
					String moneda            = (rs.getString("moneda")              ==null)?" ":rs.getString("moneda");
					String fnMonto           = (rs.getString("fn_monto")            ==null)?" ":rs.getString("fn_monto");
					String monedaLinea       = (rs.getString("moneda_linea")        ==null)?" ":rs.getString("moneda_linea");
					String tipoConversion    = (rs.getString("tipo_conversion")     ==null)?" ":rs.getString("tipo_conversion");
					String tipoCambio        = (rs.getString("tipo_cambio")         ==null)?" ":rs.getString("tipo_cambio");
					String igPlazoDescuento  = (rs.getString("ig_plazo_descuento")  ==null)?" ":rs.getString("ig_plazo_descuento");
					String fnPorcDescuento   = (rs.getString("fn_porc_descuento")   ==null)?" ":rs.getString("fn_porc_descuento");
					String modoPlazo         = (rs.getString("modo_plazo")          ==null)?" ":rs.getString("modo_plazo");
					String estatus           = (rs.getString("estatus")             ==null)?" ":rs.getString("estatus");
					String icMoneda          = (rs.getString("ic_moneda")           ==null)?" ":rs.getString("ic_moneda");
					String icDocumento       = (rs.getString("ic_documento")        ==null)?" ":rs.getString("ic_documento");
					String nombreEPO         = (rs.getString("nombre_epo")          ==null)?" ":rs.getString("nombre_epo");
					String tipoCobranza      = (rs.getString("tipo_cobranza")       ==null)?" ":rs.getString("tipo_cobranza");
					String nombreIF          = (rs.getString("nombre_if")           ==null)?" ":rs.getString("nombre_if");
					String fechaCambio       = (rs.getString("fecha_cambio")        ==null)?" ":rs.getString("fecha_cambio");
					String tipoCambioEstatus = (rs.getString("tipo_cambio_estatus") ==null)?" ":rs.getString("tipo_cambio_estatus");
					String causa             = (rs.getString("causa")               ==null)?" ":rs.getString("causa");
					String aux1              = (icMoneda.equals(monedaLinea)||Integer.parseInt(tipoCambio)==1)?"":tipoConversion;
					String aux2              = (icMoneda.equals(monedaLinea)||Integer.parseInt(tipoCambio)==1)?"":""+tipoCambio;
					String montoValuado      = String.valueOf(Double.parseDouble(fnMonto)*Double.parseDouble(tipoCambio));
					String montoDescontar    = String.valueOf((Double.parseDouble(fnMonto))*Double.parseDouble(fnPorcDescuento)/100);
					String auxMontoValuado   = (icMoneda.equals(monedaLinea)||Integer.parseInt(tipoCambio)==1)?" ":Comunes.formatoDecimal(montoValuado,2);

					contenidoArchivo.append(nombreEPO.replace(',',' ')+",");
					contenidoArchivo.append(igNumeroDocto.replace(',',' ')+",");
					contenidoArchivo.append(nombreDist.replace(',',' ')+",");
					contenidoArchivo.append(ccAcuse.replace(',',' ')+",");
					contenidoArchivo.append(fechaEmision.replace(',',' ')+",");
					contenidoArchivo.append(dfFechaVenc.toString().trim()+",");
					contenidoArchivo.append(dfCarga.replace(',',' ')+",");
					contenidoArchivo.append(igPlazoDocto.replace(',',' ')+",");
					contenidoArchivo.append(moneda.replace(',',' ')+",");  
					contenidoArchivo.append(fnMonto.replace(',',' ')+",");
					contenidoArchivo.append(igPlazoDescuento.replace(',',' ')+",");
					contenidoArchivo.append(fnPorcDescuento.replace(',',' ')+",");
					contenidoArchivo.append(montoDescontar.replace(',',' ')+",");
					contenidoArchivo.append(aux1.replace(',',' ')+",");
					contenidoArchivo.append(aux2.replace(',',' ')+",");
					contenidoArchivo.append(auxMontoValuado.replace(',',' ')+",");
					contenidoArchivo.append(modoPlazo.replace(',',' ')+",");
					contenidoArchivo.append(tipoCobranza.replace(',',' ')+",");
					contenidoArchivo.append(estatus.replace(',',' ')+",");
					contenidoArchivo.append(nombreIF.replace(',',' ')+",");
					contenidoArchivo.append(tipoCambioEstatus.replace(',',' ')+",");
					contenidoArchivo.append(fechaCambio.replace(',',' ')+",");
					contenidoArchivo.append(causa.replaceAll("\n","")+"\n");
					total++;
					if(total==1000){
						total=0;
						buffer.write(contenidoArchivo.toString());
						contenidoArchivo = new StringBuffer();//Limpio  
					}
				}
				buffer.write(contenidoArchivo.toString());
				buffer.close();	
				contenidoArchivo = new StringBuffer();//Limpio
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV. ", e);
			}
			
		} else if(tipo.equals("PDF")){

			try{
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
				int columnas = 13;
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);

				pdfDoc.setLTable(13, 100);
				pdfDoc.setLCell("A","fromas",ComunesPDF.CENTER);
				pdfDoc.setLCell("EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Distribuidor","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("N�mero de documento inicial","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Num. acuse","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de emisi�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de publicaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo docto.","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo para descuento en dias","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("% de descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("B","fromas",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto % de descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo conv.","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo cambio","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto valuado en pesos","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Modalidad de plazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de cobranza","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de cambio de estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de cambio de estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Causa","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);
				pdfDoc.setLHeaders(); // Esta l�nea es para que muestre los encabezados en todas las p�ginas

				while (rs.next()){
					String fechaEmision      = (rs.getString("df_fecha_emision")    ==null)?" ":rs.getString("df_fecha_emision");
					String nombreDist        = (rs.getString("nombre_dist")         ==null)?" ":rs.getString("ig_numero_docto");
					String ccAcuse           = (rs.getString("cc_acuse")            ==null)?" ":rs.getString("cc_acuse");
					String igNumeroDocto     = (rs.getString("ig_numero_docto")     ==null)?" ":rs.getString("nombre_dist");
					String dfCarga           = (rs.getString("df_carga")            ==null)?" ":rs.getString("df_carga");
					String dfFechaVenc       = (rs.getString("df_fecha_venc")       ==null)?" ":rs.getString("df_fecha_venc");
					String igPlazoDocto      = (rs.getString("ig_plazo_docto")      ==null)?" ":rs.getString("ig_plazo_docto");
					String moneda            = (rs.getString("moneda")              ==null)?" ":rs.getString("moneda");
					String fnMonto           = (rs.getString("fn_monto")            ==null)?" ":rs.getString("fn_monto");
					String monedaLinea       = (rs.getString("moneda_linea")        ==null)?" ":rs.getString("moneda_linea");
					String tipoConversion    = (rs.getString("tipo_conversion")     ==null)?" ":rs.getString("tipo_conversion");
					String tipoCambio        = (rs.getString("tipo_cambio")         ==null)?" ":rs.getString("tipo_cambio");
					String igPlazoDescuento  = (rs.getString("ig_plazo_descuento")  ==null)?" ":rs.getString("ig_plazo_descuento");
					String fnPorcDescuento   = (rs.getString("fn_porc_descuento")   ==null)?" ":rs.getString("fn_porc_descuento");
					String montoDescontar    = String.valueOf((Double.parseDouble(fnMonto))*Double.parseDouble(fnPorcDescuento)/100);
					String modoPlazo         = (rs.getString("modo_plazo")          ==null)?" ":rs.getString("modo_plazo");
					String estatus           = (rs.getString("estatus")             ==null)?" ":rs.getString("estatus");
					String icMoneda          = (rs.getString("ic_moneda")           ==null)?" ":rs.getString("ic_moneda");
					String aux1              = (icMoneda.equals(monedaLinea)||Integer.parseInt(tipoCambio)==1)?"":tipoConversion;
					String aux2              = (icMoneda.equals(monedaLinea)||Integer.parseInt(tipoCambio)==1)?"":""+tipoCambio;
					String icDocumento       = (rs.getString("ic_documento")        ==null)?" ":rs.getString("ic_documento");
					String nombreEPO         = (rs.getString("nombre_epo")          ==null)?" ":rs.getString("nombre_epo");
					String tipoCobranza      = (rs.getString("tipo_cobranza")       ==null)?" ":rs.getString("tipo_cobranza");
					String nombreIF          = (rs.getString("nombre_if")           ==null)?" ":rs.getString("nombre_if");
					String fechaCambio       = (rs.getString("fecha_cambio")        ==null)?" ":rs.getString("fecha_cambio");
					String tipoCambioEstatus = (rs.getString("tipo_cambio_estatus") ==null)?" ":rs.getString("tipo_cambio_estatus");
					String causa             = (rs.getString("causa")               ==null)?" ":rs.getString("causa");
					String montoValuado      = String.valueOf(Double.parseDouble(fnMonto)*Double.parseDouble(tipoCambio));
					String auxMontoValuado   = (icMoneda.equals(monedaLinea)||Integer.parseInt(tipoCambio)==1)?" ":Comunes.formatoDecimal(montoValuado,2);

					pdfDoc.setLCell("A","fromas",ComunesPDF.CENTER);
					pdfDoc.setLCell(nombreEPO,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(igNumeroDocto,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(nombreDist,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(ccAcuse,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaEmision,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(dfFechaVenc,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(dfCarga,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(igPlazoDocto,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(fnMonto,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(igPlazoDescuento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fnPorcDescuento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("B","fromas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoDescontar,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(aux1,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(aux2,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(auxMontoValuado,"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(modoPlazo,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipoCobranza,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(estatus,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(nombreIF,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipoCambioEstatus,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaCambio,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(causa,"formas",ComunesPDF.JUSTIFIED);
					pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
					//pdfDoc.setLCell("","columnas",ComunesPDF.CENTER, columnas);
				}

				pdfDoc.addLTable();
				pdfDoc.endDocument();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF. ", e);
			}

		}

		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}

	 /**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  this.paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}
	public void setLoginUsuario(String loginUsuario) {
		this.loginUsuario = loginUsuario;
	}
	public String getLoginUsuario() {
		return loginUsuario;
	}
	 public void setClaveEpo(String claveEpo) { 
		this.claveEpo = claveEpo;
	}
	public String getClaveEpo() {
		return claveEpo;
	}
	 public void setClavePyme(String clavePyme) {
		this.clavePyme = clavePyme;
	}
	public String getClavePyme() {
		return claveEpo;
	}
	 public void setTipoCredito(String tipoCredito) {
		this.tipoCredito = tipoCredito;
	}
	public String getTipoCredito() {
		return tipoCredito;
	}
	
	
	public void setModePlazo(String modePlazo) {
		this.modePlazo =modePlazo;
	}
	public String getModePlazo() {
		return modePlazo;
	}
	public void setIcCambioEstatus(String icCambioEstatus) {
		this.icCambioEstatus = icCambioEstatus;
	}
	public String getIcCambioEstatus() {
		return icCambioEstatus;
	}
	public void setDcFechaCambioDe(String dcFechaCambioDe) {
		this.dcFechaCambioDe = dcFechaCambioDe;
	}
	public String getDcFechaCambioDe() {
		return dcFechaCambioDe;
	}
	public void setDcFechaCambioA(String dcFechaCambioA) {
		this.dcFechaCambioA = dcFechaCambioA;
	}
	public String getDcFechaCambioA() {
		return dcFechaCambioA;
	}
	public void setClaveIf(String claveIf) {
		this.claveIf = claveIf;
	}
	public String getClaveIf() {
		return claveIf;
	}
	public void setNumeroDocto(String numeroDocto) {
		this.numeroDocto = numeroDocto;
	}
	public String getNumeroDocto() {
		return numeroDocto;
	}
	
	public void setFechaVtoDe(String fechaVtoDe) {
		this.fechaVtoDe = fechaVtoDe;
	}
	public String getFechaVtoDe() {
		return fechaVtoDe;
	}
	public void setFechaVtoA(String fechaVtoA) {
		this.fechaVtoA = fechaVtoA;
	}
	public String getFechaVtoA() {
		return fechaVtoA;
	}
	public void setFechaPublicacionDe(String fechaPublicacionDe) {
		this.fechaPublicacionDe = fechaPublicacionDe;
	}
	public String getFechaPublicacionDe() {
		return fechaPublicacionDe;
	}
	public void setFechaPublicacionA(String fechaPublicacionA) {
		this.fechaPublicacionA = fechaPublicacionA;
	}
	public String getFechaPublicacionA() {
		return fechaPublicacionA;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMontoDe(String montoDe) {
		this.montoDe = montoDe;
	}
	public String getMontoDe() {
		return montoDe;
	}
	public void setMontoA(String montoA) {
		this.montoA = montoA;
	}
	public String getMontoA() {
		return montoA;
	}
	public void setMontoDescuento(String montoDescuento) {
		this.montoDescuento = montoDescuento;
	}
	public String getMontoDescuento() {
		return montoDescuento;
	}
	public String getTipoConsulta() {
		return tipoConsulta;
	}

	public void setTipoConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}
	public String getIc_documento() {
		return ic_documento;
	}

	public void setIc_documento(String ic_documento) {
		this.ic_documento = ic_documento;
	}

}