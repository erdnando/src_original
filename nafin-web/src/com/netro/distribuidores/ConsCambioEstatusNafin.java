package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsCambioEstatusNafin implements IQueryGeneratorRegExtJS{

	private static final Log log = ServiceLocator.getInstance().getLog(ConsCambioEstatusNafin.class);
	private List conditions;
	private String opcionEstatus; //Determina cual de todas las consultas se ejecutar�
	private String tituloTabla; //Obtiene el t�tulo que llevar� la tabla de acuerdo a la opci�n seleccionada en el combo box
	String qrySentenciaDM; //Es para hacer una consulta con union 
	String qrySentenciaCCC; //Es para hacer una consulta con union
	StringBuffer sentencia = new StringBuffer(); //Hace ua union de qrySentenciaDM con qrySentenciaCCC
	/***************************************************************************
	 * 				Variables para las consultas de los archivos                *
	 ***************************************************************************/
	String epo, distribuidor, numDocto, numAcuseCarga, fechaEmision, fechaVencimiento, plazoDocto, moneda, tipoConversion, 
			 plazoDescuento, porcDescuento, modoPlazo, icMoneda, icDocumento, tipoCobranza, tipoCredito, numAcuse, fechaPublicacion, 
			 fechaCambio, causa, bandeVentaCartera, estatus, intermediario, plazoCredito, tipoCobroInt, referenciaTasaInt,  
			 fechaVencCredito, antFechaEmision, antFechaVto, antModoPlazo;		 
	double monto, tipoCambio, montoValuado, montoDescontar, montoCredito, valorTasaInt, montoTasaInt, antMonto, montoCapitalInt;	

	public ConsCambioEstatusNafin() {}
	
	public String getDocumentQuery(){ return null; }

	public String getDocumentSummaryQueryForIds(List ids){ return null; }
	
	public String getAggregateCalculationQuery(){ return null; }
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){	 
		return null;
	}
	
	/**
	 * Obtiene las consultas tanto para mostrarlas en el grid como para imprimir 
	 * los archivos, la consulta que tomar� depende de la opci�n "estatus"
	 * @return cadena con la consulta seleccionada
	 */
	public String getDocumentQueryFile(){
			
		String qrySentencia = "";
		conditions = new ArrayList();
		log.info("***** getDocumentQueryFile (E) *****");	
		try{
			if(opcionEstatus.equals("2")){
				qrySentencia = this.consultaSeleccionadoPymeANegociable(opcionEstatus);
			} else if(opcionEstatus.equals("21")){
				qrySentencia = this.consultaModificar();
			} else if(opcionEstatus.equals("4")){ 
				qrySentencia = this.consultaNegociableABaja();
			} else if(opcionEstatus.equals("22")){
				qrySentencia = this.consultaNegociableAVencidoSinOperar();
			} else if(opcionEstatus.equals("24")){
				qrySentencia = this.consultaNoNegociableABaja();
			} else if(opcionEstatus.equals("23")){
				qrySentencia = this.consultaSeleccionadoPymeANegociable(opcionEstatus);
			} else if(opcionEstatus.equals("34")){
				qrySentencia = this.consultaSeleccionadoPymeANegociable(opcionEstatus);
			}
		}catch(Exception e){
			log.warn("ConsCambioEstatusNafin::getDocumentQueryFileException "+e);
		}
		log.info("***** getDocumentQueryFile(S) *****");
		return qrySentencia;
	}
	
	/**
	 * Manda a llamar a los m�todos generaPDF o generaCSV de acuerdo al tipo de 
	 * documento que se solicite
	 * @return nombre del archivo
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
		log.debug("********** crearCustomFile (E)**********");
		String nombreArchivo ="";
		if(tipo.equals("PDF")){
			nombreArchivo = this.generaArchivoPDF(request, rs, path);
		} else if(tipo.equals("CSV")){
			nombreArchivo = this.generaArchivoCSV(request, rs, path);
		}
		log.debug("******** crearCustomFile (S)");
		return nombreArchivo;		
		}
	
	/**
	 * El siguiente m�todo recibe los par�metros de crearCustomFile para generar el archivo pdf
	 * @return el nombre del archivo (pdf)
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String generaArchivoPDF(HttpServletRequest request, java.sql.ResultSet rs, String path){
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;	

		try {
			Comunes comunes = new Comunes();
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
			((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
			(String)session.getAttribute("sesExterno"),
			(String) session.getAttribute("strNombre"),
			(String) session.getAttribute("strNombreUsuario"),
			(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			pdfDoc.addText(" 		","formas",ComunesPDF.RIGHT);
			pdfDoc.addText(tituloTabla,"formas",ComunesPDF.LEFT);
		
			if(opcionEstatus.equals("4")  || opcionEstatus.equals("22") || opcionEstatus.equals("24")){
				pdfDoc.setTable(19,100);
				pdfDoc.setCell("EPO ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Distribuidor ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de Acuse de Carga ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de documento inicial ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de emisi�n ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de publicaci�n ","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Plazo docto ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo para descuento en d�as ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("% de descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto % de descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo conv ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo cambio ","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Monto valuado en pesos ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Modalidad de plazo ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Cambio ","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Causa ","celda01",ComunesPDF.CENTER);
				
				while (rs.next())	{		
					epo = (rs.getString("NOMBRE_EPO")==null)?"":rs.getString("NOMBRE_EPO");
					distribuidor = (rs.getString("NOMBRE_DIST")==null)?"":rs.getString("NOMBRE_DIST");
					numDocto = (rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO");
					numAcuseCarga = (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
					fechaEmision = (rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION");
					fechaVencimiento = (rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC");
					plazoDocto = (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");
					moneda = (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
					monto = rs.getDouble("FN_MONTO");
					tipoConversion = (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");
					tipoCambio = rs.getDouble("TIPO_CAMBIO");
					plazoDescuento = (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
					porcDescuento = (rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO");
					modoPlazo = (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
					icMoneda	= (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");
					icDocumento = (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
					tipoCobranza = (rs.getString("TIPO_COBRANZA")==null)?"":rs.getString("TIPO_COBRANZA");
					tipoCredito = (rs.getString("TIPO_CREDITO")==null)?"":rs.getString("TIPO_CREDITO");
					numAcuse = (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
					fechaPublicacion = (rs.getString("FECHA_PUBLICACION")==null)?"":rs.getString("FECHA_PUBLICACION");
					fechaCambio = (rs.getString("FECHA_CAMBIO")==null)?"":rs.getString("FECHA_CAMBIO");
					causa	= (rs.getString("CAUSA")==null)?"":rs.getString("CAUSA");
					bandeVentaCartera	= (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
					montoValuado = monto*tipoCambio;
					montoDescontar = monto*Double.parseDouble(porcDescuento)/100;
					if(bandeVentaCartera.equals("S")){
						modoPlazo = "";
					}
					
					pdfDoc.setCell(epo,"formas",ComunesPDF.CENTER);			
					pdfDoc.setCell(distribuidor,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(numAcuse,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(numDocto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaEmision,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaPublicacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(plazoDocto,"formas",ComunesPDF.CENTER);	
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.RIGHT);			
					pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(plazoDescuento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(porcDescuento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(montoDescontar,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(tipoConversion,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(("1".equals(icMoneda))?"1":"$"+Comunes.formatoDecimal(tipoCambio,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoValuado,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(modoPlazo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaCambio,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(causa,"formas",ComunesPDF.CENTER);
				}//Fin del while
				pdfDoc.addTable();
			}	//Fin del if opcionEstatus == 4 || opcionEstatus == 22 || opcionEstatus == 24
			if(opcionEstatus.equals("2")){
				pdfDoc.setTable(27,100);
				pdfDoc.setCell("EPO ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Distribuidor ","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("N�mero de Acuse de Carga ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de documento inicial ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de emisi�n ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de publicaci�n ","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Plazo docto ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo para descuento en d�as ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("% de descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto % de descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo conv ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo cambio ","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Monto valuado en pesos ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Modalidad de plazo ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de documento final ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("IF ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Referencia tasa de inter�s ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Valor tasa de inter�s ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto de Intereses ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Total de Capital e Inter�s ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Rechazo ","celda01",ComunesPDF.CENTER);
				
				while (rs.next())	{		
					epo = (rs.getString("NOMBRE_EPO")==null)?"":rs.getString("NOMBRE_EPO");
					distribuidor = (rs.getString("NOMBRE_DIST")==null)?"":rs.getString("NOMBRE_DIST");
					numDocto = (rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO");
					numAcuseCarga = (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
					fechaEmision = (rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION");
					fechaVencimiento = (rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC");
					plazoDocto = (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");
					moneda = (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
					monto = rs.getDouble("FN_MONTO");
					tipoConversion = (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");
					tipoCambio = rs.getDouble("TIPO_CAMBIO");
					plazoDescuento = (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
					porcDescuento = (rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO");
					modoPlazo = (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
					estatus = (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");
					icMoneda = (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");
					icDocumento	= (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
					tipoCobranza = (rs.getString("TIPO_COBRANZA")==null)?"":rs.getString("TIPO_COBRANZA");
					icDocumento = (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
					intermediario = (rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF");
					tipoCredito = (rs.getString("TIPO_CREDITO")==null)?"":rs.getString("TIPO_CREDITO");
					montoCredito = rs.getDouble("MONTO_CREDITO");
					plazoCredito = (rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO");
					tipoCobroInt = (rs.getString("TIPO_COBRO_INT")==null)?"":rs.getString("TIPO_COBRO_INT");
					referenciaTasaInt	= (rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT");
					valorTasaInt = rs.getDouble("VALOR_TASA_INT");
					montoTasaInt = rs.getDouble("MONTO_TASA_INT");
					numAcuse = (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
					fechaPublicacion = (rs.getString("FECHA_PUBLICACION")==null)?"":rs.getString("FECHA_PUBLICACION");
					fechaVencCredito = (rs.getString("FECHA_VENC_CREDITO")==null)?"":rs.getString("FECHA_VENC_CREDITO");
					montoCapitalInt = montoCredito + montoTasaInt;
					fechaCambio = (rs.getString("FECHA_CAMBIO")==null)?"":rs.getString("FECHA_CAMBIO");
					bandeVentaCartera = (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
					montoValuado = monto*tipoCambio;
					montoDescontar = monto*Double.parseDouble(porcDescuento)/100;
					if(bandeVentaCartera.equals("S")){
						modoPlazo = "";
					}

					pdfDoc.setCell(epo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(distribuidor,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(numAcuse,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(numDocto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaEmision,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaPublicacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(plazoDocto,"formas",ComunesPDF.CENTER);	
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(plazoDescuento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(porcDescuento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(montoDescontar,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(tipoConversion,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(("1".equals(icMoneda))?"1":"$;"+Comunes.formatoDecimal(tipoCambio,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoValuado,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(modoPlazo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(icDocumento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoCredito,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(plazoCredito,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaVencCredito,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(intermediario,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(referenciaTasaInt,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(""+valorTasaInt,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTasaInt,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoCapitalInt,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(fechaCambio,"formas",ComunesPDF.CENTER);
				}//Fin del while
				pdfDoc.addTable();
			}	//Fin del if opcionEstatus == 2		
			//agregado
			if(opcionEstatus.equals("23") || opcionEstatus.equals("34")){
				pdfDoc.setTable(26,100);
				pdfDoc.setCell("EPO ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Distribuidor ","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("N�mero de Acuse de carga ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de documento inicial ","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Fecha de emisi�n ","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Fecha de vencimiento ","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Fecha de publicaci�n ","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Plazo de documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo para descuento en d�as ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje de descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto porcentaje de descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Modalidad de plazo ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de documento final ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de cr�dito ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("IF ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto documento Final ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de vencimiento ","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Referencia de tasa de inter�s ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Valor Tasa de inter�s ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto de Inter�s ","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Monto Total de capital","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de rechazo ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Causa ","celda01",ComunesPDF.CENTER);
				while (rs.next())	{		
					epo = (rs.getString("NOMBRE_EPO")==null)?"":rs.getString("NOMBRE_EPO");
					distribuidor = (rs.getString("NOMBRE_DIST")==null)?"":rs.getString("NOMBRE_DIST");
					numDocto = (rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO");
					numAcuseCarga = (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
					fechaEmision = (rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION");
					fechaVencimiento = (rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC");
					plazoDocto = (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");
					moneda = (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
					monto = rs.getDouble("FN_MONTO");
					tipoConversion = (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");
					tipoCambio = rs.getDouble("TIPO_CAMBIO");
					plazoDescuento = (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
					porcDescuento = (rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO");
					modoPlazo = (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
					estatus = (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");
					icMoneda = (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");
					icDocumento	= (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
					tipoCobranza = (rs.getString("TIPO_COBRANZA")==null)?"":rs.getString("TIPO_COBRANZA");
					icDocumento = (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
					intermediario = (rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF");
					tipoCredito = (rs.getString("TIPO_CREDITO")==null)?"":rs.getString("TIPO_CREDITO");
					montoCredito = rs.getDouble("MONTO_CREDITO");
					plazoCredito = (rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO");
					tipoCobroInt = (rs.getString("TIPO_COBRO_INT")==null)?"":rs.getString("TIPO_COBRO_INT");
					referenciaTasaInt	= (rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT");
					valorTasaInt = rs.getDouble("VALOR_TASA_INT");
					montoTasaInt = rs.getDouble("MONTO_TASA_INT");
					numAcuse = (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
					fechaPublicacion = (rs.getString("FECHA_PUBLICACION")==null)?"":rs.getString("FECHA_PUBLICACION");
					fechaVencCredito = (rs.getString("FECHA_VENC_CREDITO")==null)?"":rs.getString("FECHA_VENC_CREDITO");
					montoCapitalInt = montoCredito + montoTasaInt;
					fechaCambio = (rs.getString("FECHA_CAMBIO")==null)?"":rs.getString("FECHA_CAMBIO");
					bandeVentaCartera = (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
					causa = (rs.getString("CAUSA")==null)?"":rs.getString("CAUSA");
					montoValuado = monto*tipoCambio;
					montoDescontar = monto*Double.parseDouble(porcDescuento)/100;
					if(bandeVentaCartera.equals("S")){
						modoPlazo = "";
					}//pdfDoc.setCell("% "+porcDescuento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(epo,"formas",ComunesPDF.CENTER); 
					pdfDoc.setCell(distribuidor,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(numAcuse,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(numDocto,"formas",ComunesPDF.CENTER); 
					pdfDoc.setCell(fechaEmision,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaPublicacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(plazoDocto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(plazoDescuento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(porcDescuento,2)+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(montoDescontar,2)+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(modoPlazo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(icDocumento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(tipoCredito,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(intermediario,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(plazoCredito,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaVencCredito,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(referenciaTasaInt,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(valorTasaInt+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTasaInt,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoCapitalInt,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(fechaCambio,"formas",ComunesPDF.CENTER); 
					pdfDoc.setCell(causa,"formas",ComunesPDF.CENTER);
				}//Fin del while
				pdfDoc.addTable();
			}	//Fin del if opcionEstatus == 23, 34		
			//agregado
			if(opcionEstatus.equals("21")){
				pdfDoc.setTable(22,100);
				pdfDoc.setCell("EPO ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Distribuidor ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de Acuse de Carga ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de documento inicial ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de emisi�n ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de publicaci�n ","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Plazo docto ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto ","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo para descuento en d�as ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("% de descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto % de descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo conv ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo cambio ","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Monto valuado en pesos ","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Modalidad de plazo ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de cambio ","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Anterior fecha de emisi�n ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Anterior fecha de vencimiento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Anterior monto del documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Anterior modalidad de plazo ","celda01",ComunesPDF.CENTER);
				
				while (rs.next())	{		
					epo = (rs.getString("NOMBRE_EPO")==null)?"":rs.getString("NOMBRE_EPO");
					distribuidor = (rs.getString("NOMBRE_DIST")==null)?"":rs.getString("NOMBRE_DIST");
					numDocto = (rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO");
					numAcuseCarga = (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
					fechaEmision = (rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION");
					fechaVencimiento = (rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC");
					plazoDocto = (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");
					moneda = (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
					monto = rs.getDouble("FN_MONTO");
					tipoConversion = (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");
					tipoCambio = rs.getDouble("TIPO_CAMBIO");
					plazoDescuento = (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
					porcDescuento = (rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO");
					modoPlazo = (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
					icMoneda = (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");
					icDocumento = (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
					tipoCredito = (rs.getString("TIPO_CREDITO")==null)?"":rs.getString("TIPO_CREDITO");
					numAcuse = (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
					fechaPublicacion = (rs.getString("FECHA_PUBLICACION")==null)?"":rs.getString("FECHA_PUBLICACION");
					fechaCambio = (rs.getString("FECHA_CAMBIO")==null)?"":rs.getString("FECHA_CAMBIO");
					antFechaEmision = (rs.getString("FECHA_EMISION_ANT")==null)?"":rs.getString("FECHA_EMISION_ANT");
					antFechaVto = (rs.getString("FECHA_VENC_ANT")==null)?"":rs.getString("FECHA_VENC_ANT");
					antMonto = rs.getDouble("FN_MONTO_ANTERIOR");
					antModoPlazo = (rs.getString("MODO_PLAZO_ANT")==null)?"":rs.getString("MODO_PLAZO_ANT");
					bandeVentaCartera	= (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
					montoValuado = monto*tipoCambio;
					montoDescontar = monto*Double.parseDouble(porcDescuento)/100;
					if(bandeVentaCartera.equals("S")){
						modoPlazo = "";
					}	
			
					pdfDoc.setCell(epo,"formas",ComunesPDF.CENTER);			
					pdfDoc.setCell(distribuidor,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(numAcuse,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(numDocto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaEmision,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaPublicacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(plazoDocto,"formas",ComunesPDF.CENTER);	
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.RIGHT);			
					pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(plazoDescuento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(porcDescuento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(montoDescontar,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(tipoConversion,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(("1".equals(icMoneda))?"1":"$"+Comunes.formatoDecimal(tipoCambio,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoValuado,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(modoPlazo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaCambio,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(antFechaEmision,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(antFechaVto,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell((antMonto==0)?"":"$"+Comunes.formatoDecimal(antMonto,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(antModoPlazo,"formas",ComunesPDF.CENTER);	
				}//Fin del while			
				pdfDoc.addTable();
			}	//Fin del if opcionEstatus == 21		
			
			pdfDoc.endDocument();	
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		}
		return nombreArchivo;
	}
		
	/**
	 * El siguiente m�todo recibe los par�metros de crearCustomFile para generar el archivo csv
	 * @return el nombre del archivo (csv)
	 * @param path
	 * @param rs
	 * @param request
	 */
	public String generaArchivoCSV(HttpServletRequest request, java.sql.ResultSet rs, String path){

		String nombreArchivo ="";
		HttpSession session = request.getSession();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		int total = 0;
		String antMontoStr;

		try {
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);
			
			contenidoArchivo = new StringBuffer();	
			contenidoArchivo.append(tituloTabla + "\n");
			
			if(opcionEstatus.equals("4") || opcionEstatus.equals("22") || opcionEstatus.equals("24")){
				contenidoArchivo.append("EPO, Distribuidor, N�mero de Acuse de Carga, N�mero de documento inicial, Fecha de emisi�n,"+ 
												"Fecha de vencimiento, Fecha de publicaci�n, Plazo docto, Monto, Moneda, Plazo para descuento en d�as,"+
												"% de descuento, Monto % de descuento, Tipo conv., Tipo cambio, Monto valuado en pesos, Modalidad de plazo,"+
												"Fecha de Cambio, Causa \n");				
				while (rs.next())	{		
					epo = (rs.getString("NOMBRE_EPO")==null)?"":rs.getString("NOMBRE_EPO");
					distribuidor = (rs.getString("NOMBRE_DIST")==null)?"":rs.getString("NOMBRE_DIST");
					numDocto = (rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO");
					numAcuseCarga = (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
					fechaEmision = (rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION");
					fechaVencimiento = (rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC");
					plazoDocto = (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");
					moneda = (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
					monto = rs.getDouble("FN_MONTO");
					tipoConversion = (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");
					tipoCambio = rs.getDouble("TIPO_CAMBIO");
					plazoDescuento = (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
					porcDescuento = (rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO");
					modoPlazo = (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
					icMoneda	= (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");
					icDocumento = (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
					tipoCobranza = (rs.getString("TIPO_COBRANZA")==null)?"":rs.getString("TIPO_COBRANZA");
					tipoCredito = (rs.getString("TIPO_CREDITO")==null)?"":rs.getString("TIPO_CREDITO");
					numAcuse = (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
					fechaPublicacion = (rs.getString("FECHA_PUBLICACION")==null)?"":rs.getString("FECHA_PUBLICACION");
					fechaCambio = (rs.getString("FECHA_CAMBIO")==null)?"":rs.getString("FECHA_CAMBIO");
					causa	= (rs.getString("CAUSA")==null)?"":rs.getString("CAUSA");
					bandeVentaCartera	= (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
					montoValuado = monto*tipoCambio;
					montoDescontar = monto*Double.parseDouble(porcDescuento)/100;
					if("1".equals(icMoneda)){
						tipoCambio = 1;
					}
					if(bandeVentaCartera.equals("S")){
						modoPlazo = "";
					}	
						
					contenidoArchivo.append(epo.replace(',',' ')+", "+
													distribuidor.replace(',',' ')+", "+	
													numAcuse.replace(',',' ')+", "+
													numDocto.replace(',',' ')+", "+
													fechaEmision.replace(',',' ')+", "+
													fechaVencimiento.replace(',',' ')+", "+
													fechaPublicacion.replace(',',' ')+", "+
													plazoDocto.replace(',',' ')+", "+
													Comunes.formatoDecimal(monto,2).replace(',',' ')+", "+
													moneda.replace(',',' ')+", "+
													plazoDescuento.replace(',',' ')+", "+
													porcDescuento.replace(',',' ')+", "+
													montoDescontar+", "+
													tipoConversion.replace(',',' ')+", "+
													tipoCambio+", "+
													Comunes.formatoDecimal(montoValuado,2).replace(',',' ')+", "+
													modoPlazo.replace(',',' ')+", "+
													fechaCambio.replace(',',' ')+", "+
													causa.replace(',',' ')+"\n");
					total++;
					if(total==1000){					
						total=0;	
						buffer.write(contenidoArchivo.toString());
						contenidoArchivo = new StringBuffer();//Limpio  
					}																					
				} //Fin del while			
													
			}// fin del if opcionEstatus = 4, 22, 24
			if(opcionEstatus.equals("2")){
				contenidoArchivo.append("EPO, Distribuidor, N�mero de Acuse de Carga, N�mero de documento inicial, Fecha de emisi�n,"+ 
												"Fecha de vencimiento, Fecha de publicaci�n, Plazo docto, Monto, Moneda, Plazo para descuento en d�as,"+
												"% de descuento, Monto % de descuento, Tipo conv., Tipo cambio, Monto valuado en pesos, Modalidad de plazo,"+
												"N�mero de documento final, Monto, Plazo, Fecha de vencimiento, IF, Referencia tasa de inter�s,"+
												"Valor tasa de inter�s, Monto de Intereses, Monto Total de Capital e Inter�s, Fecha de Rechazo\n");				
				while (rs.next())	{		
					epo = (rs.getString("NOMBRE_EPO")==null)?"":rs.getString("NOMBRE_EPO");
					distribuidor = (rs.getString("NOMBRE_DIST")==null)?"":rs.getString("NOMBRE_DIST");
					numDocto = (rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO");
					numAcuseCarga = (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
					fechaEmision = (rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION");
					fechaVencimiento = (rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC");
					plazoDocto = (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");
					moneda = (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
					monto = rs.getDouble("FN_MONTO");
					tipoConversion = (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");
					tipoCambio = rs.getDouble("TIPO_CAMBIO");
					plazoDescuento = (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
					porcDescuento = (rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO");
					modoPlazo = (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
					estatus = (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");
					icMoneda = (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");
					icDocumento	= (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
					tipoCobranza = (rs.getString("TIPO_COBRANZA")==null)?"":rs.getString("TIPO_COBRANZA");
					icDocumento = (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
					intermediario = (rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF");
					tipoCredito = (rs.getString("TIPO_CREDITO")==null)?"":rs.getString("TIPO_CREDITO");
					montoCredito = rs.getDouble("MONTO_CREDITO");
					plazoCredito = (rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO");
					tipoCobroInt = (rs.getString("TIPO_COBRO_INT")==null)?"":rs.getString("TIPO_COBRO_INT");
					referenciaTasaInt	= (rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT");
					valorTasaInt = rs.getDouble("VALOR_TASA_INT");
					montoTasaInt = rs.getDouble("MONTO_TASA_INT");
					numAcuse = (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
					fechaPublicacion = (rs.getString("FECHA_PUBLICACION")==null)?"":rs.getString("FECHA_PUBLICACION");
					fechaVencCredito = (rs.getString("FECHA_VENC_CREDITO")==null)?"":rs.getString("FECHA_VENC_CREDITO");
					montoCapitalInt = montoCredito + montoTasaInt;
					fechaCambio = (rs.getString("FECHA_CAMBIO")==null)?"":rs.getString("FECHA_CAMBIO");
					bandeVentaCartera = (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
					montoValuado = monto*tipoCambio;
					montoDescontar = monto*Double.parseDouble(porcDescuento)/100;
					if("1".equals(icMoneda)){
						tipoCambio = 1;
					}
					if(bandeVentaCartera.equals("S")){
						modoPlazo = "";
					}
						
					contenidoArchivo.append(epo.replace(',',' ')+", "+
													distribuidor.replace(',',' ')+", "+	
													numAcuse.replace(',',' ')+", "+
													numDocto.replace(',',' ')+", "+
													fechaEmision.replace(',',' ')+", "+
													fechaVencimiento.replace(',',' ')+", "+
													fechaPublicacion.replace(',',' ')+", "+
													plazoDocto.replace(',',' ')+", "+
													Comunes.formatoDecimal(monto,2).replace(',',' ')+", "+
													moneda.replace(',',' ')+", "+
													plazoDescuento.replace(',',' ')+", "+
													porcDescuento.replace(',',' ')+", "+
													montoDescontar+", "+
													tipoConversion+", "+
													tipoCambio+", "+
													Comunes.formatoDecimal(montoValuado,2).replace(',',' ')+", "+
													modoPlazo.replace(',',' ')+", "+
													icDocumento.replace(',',' ')+", "+
													Comunes.formatoDecimal(montoCredito,2).replace(',',' ')+", "+
													plazoCredito.replace(',',' ')+", "+
													fechaVencCredito.replace(',',' ')+", "+
													intermediario.replace(',',' ')+", "+
													referenciaTasaInt.replace(',',' ')+", "+
													valorTasaInt+", "+
													Comunes.formatoDecimal(montoTasaInt,2).replace(',',' ')+", "+
													Comunes.formatoDecimal(montoCapitalInt,2).replace(',',' ')+", "+
													fechaCambio.replace(',',' ')+"\n");
					total++;
					if(total==1000){					
						total=0;	
						buffer.write(contenidoArchivo.toString());
						contenidoArchivo = new StringBuffer();//Limpio  
					}																					
				} //Fin del while					
				
			}// fin del if opcionEstatus = 2	
			///agregado
			if(opcionEstatus.equals("23") || opcionEstatus.equals("34")){
				contenidoArchivo.append("EPO, Distribuidor, N�mero de Acuse de carga, N�mero de documento inicial, Fecha de emisi�n,"+ 
												"Fecha de vencimiento, Fecha de publicaci�n, Plazo de documento, Moneda, Monto, Plazo para descuento en d�as,"+
												"Porcentaje de descuento, Monto porcentaje de descuento,  Modalidad de plazo,"+
												"N�mero de documento final,Tipo de cr�dito, IF, Monto documento Final, Plazo,  Fecha de vencimiento,"+
												"Referencia tasa de inter�s , Valor Tasa de Inter�s , Monto de Inter�s,"+
												"Monto total de capital, Fecha de rechazo,Causa\n");				
				while (rs.next())	{		
					epo = (rs.getString("NOMBRE_EPO")==null)?"":rs.getString("NOMBRE_EPO");//
					distribuidor = (rs.getString("NOMBRE_DIST")==null)?"":rs.getString("NOMBRE_DIST");//
					numDocto = (rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO");//
					numAcuseCarga = (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");//
					fechaEmision = (rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION");
					fechaVencimiento = (rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC");
					plazoDocto = (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");
					moneda = (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
					monto = rs.getDouble("FN_MONTO");
					tipoConversion = (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");
					tipoCambio = rs.getDouble("TIPO_CAMBIO");
					plazoDescuento = (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
					porcDescuento = (rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO");
					modoPlazo = (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
					estatus = (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");
					icMoneda = (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");
					icDocumento	= (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
					tipoCobranza = (rs.getString("TIPO_COBRANZA")==null)?"":rs.getString("TIPO_COBRANZA");
					icDocumento = (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
					intermediario = (rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF");
					tipoCredito = (rs.getString("TIPO_CREDITO")==null)?"":rs.getString("TIPO_CREDITO");
					montoCredito = rs.getDouble("MONTO_CREDITO");
					plazoCredito = (rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO");
					tipoCobroInt = (rs.getString("TIPO_COBRO_INT")==null)?"":rs.getString("TIPO_COBRO_INT");
					referenciaTasaInt	= (rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT");
					valorTasaInt = rs.getDouble("VALOR_TASA_INT");
					montoTasaInt = rs.getDouble("MONTO_TASA_INT");
					numAcuse = (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
					causa = (rs.getString("CAUSA")==null)?"":rs.getString("CAUSA");
					fechaPublicacion = (rs.getString("FECHA_PUBLICACION")==null)?"":rs.getString("FECHA_PUBLICACION");
					fechaVencCredito = (rs.getString("FECHA_VENC_CREDITO")==null)?"":rs.getString("FECHA_VENC_CREDITO");
					montoCapitalInt = montoCredito + montoTasaInt;
					fechaCambio = (rs.getString("FECHA_CAMBIO")==null)?"":rs.getString("FECHA_CAMBIO");
					bandeVentaCartera = (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
					montoValuado = monto*tipoCambio;
					montoDescontar = monto*Double.parseDouble(porcDescuento)/100;
					if("1".equals(icMoneda)){
						tipoCambio = 1;
					}
					if(bandeVentaCartera.equals("S")){
						modoPlazo = "";
					}
						
					contenidoArchivo.append(epo.replace(',',' ')+", "+
													distribuidor.replace(',',' ')+", "+	
													numAcuse.replace(',',' ')+", "+
													numDocto.replace(',',' ')+", "+
													fechaEmision.replace(',',' ')+", "+
													fechaVencimiento.replace(',',' ')+", "+
													fechaPublicacion.replace(',',' ')+", "+
													plazoDocto.replace(',',' ')+", "+
													moneda.replace(',',' ')+", "+
													"$"+Comunes.formatoDecimal(monto,2).replace(',',' ')+", "+
													plazoDescuento.replace(',',' ')+", "+
													Comunes.formatoDecimal(porcDescuento,2)+"%".replace(',',' ')+", "+//Comunes.formatoDecimal(monto,2).replace(',',' ') valorTasaInt
													"$"+Comunes.formatoDecimal(montoDescontar,2).replace(',',' ')+", "+
													modoPlazo+", "+
													icDocumento+", "+
													tipoCredito.replace(',',' ')+", "+
													intermediario.replace(',',' ')+", "+
													"$"+Comunes.formatoDecimal(monto,2).replace(',',' ')+", "+
													plazoCredito.replace(',',' ')+", "+
													fechaVencCredito.replace(',',' ')+", "+
													referenciaTasaInt.replace(',',' ')+", "+
													Comunes.formatoDecimal(valorTasaInt,2)+"%".replace(',',' ')+", "+
													"$"+Comunes.formatoDecimal(montoTasaInt,2).replace(',',' ')+", "+
													"$"+Comunes.formatoDecimal(montoCapitalInt,2).replace(',',' ')+", "+
													fechaCambio.replace(',',' ')+", "+
													causa.replace(',',' ')+"\n");
					total++;
					if(total==1000){					
						total=0;	
						buffer.write(contenidoArchivo.toString());
						contenidoArchivo = new StringBuffer();//Limpio  
					}																					
				} //Fin del while					
				
			}// fin del if opcionEstatus = 23, 34
			//agregado
			
			if(opcionEstatus.equals("21")){
				contenidoArchivo.append("EPO, Distribuidor, N�mero de Acuse de Carga, N�mero de documento inicial, Fecha de emisi�n,"+ 
												"Fecha de vencimiento, Fecha de publicaci�n, Plazo docto, Monto, Moneda, Plazo para descuento en d�as,"+
												"% de descuento, Monto % de descuento, Tipo conv., Tipo cambio, Monto valuado en pesos,"+
												"Modalidad de plazo, Fecha de cambio, Anterior fecha de emisi�n, Anterior fecha de vencimiento,"+
												"Anterior monto del documento, Anterior modalidad de plazo \n");				
				while (rs.next())	{		
					epo = (rs.getString("NOMBRE_EPO")==null)?"":rs.getString("NOMBRE_EPO");
					distribuidor = (rs.getString("NOMBRE_DIST")==null)?"":rs.getString("NOMBRE_DIST");
					numDocto = (rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO");
					numAcuseCarga = (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
					fechaEmision = (rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION");
					fechaVencimiento = (rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC");
					plazoDocto = (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");
					moneda = (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
					monto = rs.getDouble("FN_MONTO");
					tipoConversion = (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");
					tipoCambio = rs.getDouble("TIPO_CAMBIO");
					plazoDescuento = (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
					porcDescuento = (rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO");
					modoPlazo = (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
					icMoneda = (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");
					icDocumento = (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
					tipoCredito = (rs.getString("TIPO_CREDITO")==null)?"":rs.getString("TIPO_CREDITO");
					numAcuse = (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
					fechaPublicacion = (rs.getString("FECHA_PUBLICACION")==null)?"":rs.getString("FECHA_PUBLICACION");
					fechaCambio = (rs.getString("FECHA_CAMBIO")==null)?"":rs.getString("FECHA_CAMBIO");
					antFechaEmision = (rs.getString("FECHA_EMISION_ANT")==null)?"":rs.getString("FECHA_EMISION_ANT");
					antFechaVto = (rs.getString("FECHA_VENC_ANT")==null)?"":rs.getString("FECHA_VENC_ANT");
					antMonto = rs.getDouble("FN_MONTO_ANTERIOR");
					antModoPlazo = (rs.getString("MODO_PLAZO_ANT")==null)?"":rs.getString("MODO_PLAZO_ANT");
					bandeVentaCartera	= (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
					montoValuado = monto*tipoCambio;
					montoDescontar = monto*Double.parseDouble(porcDescuento)/100;
					if("1".equals(icMoneda)){
						tipoCambio = 0;
					}
					if(bandeVentaCartera.equals("S")){
						modoPlazo = "";
					}			
					if(antMonto==0){
						antMontoStr = "";
					} else{
						antMontoStr = ""+antMonto;
					}

					contenidoArchivo.append(epo.replace(',',' ')+", "+
													distribuidor.replace(',',' ')+", "+	
													numAcuse.replace(',',' ')+", "+
													numDocto.replace(',',' ')+", "+
													fechaEmision.replace(',',' ')+", "+
													fechaVencimiento.replace(',',' ')+", "+
													fechaPublicacion.replace(',',' ')+", "+
													plazoDocto.replace(',',' ')+", "+
													Comunes.formatoDecimal(monto,2).replace(',',' ')+", "+
													moneda.replace(',',' ')+", "+
													plazoDescuento.replace(',',' ')+", "+
													porcDescuento.replace(',',' ')+", "+
													montoDescontar+", "+
													tipoConversion.replace(',',' ')+", "+
													tipoCambio+", "+
													Comunes.formatoDecimal(montoValuado,2).replace(',',' ')+", "+
													modoPlazo.replace(',',' ')+", "+
													fechaCambio.replace(',',' ')+", "+
													antFechaEmision.replace(',',' ')+", "+
													antFechaVto+", "+
													antMontoStr.replace(',',' ')+", "+
													antModoPlazo.replace(',',' ')+"\n");
					total++;
					if(total==1000){					
						total=0;	
						buffer.write(contenidoArchivo.toString());
						contenidoArchivo = new StringBuffer();//Limpio  
					}																					
				} //Fin del while		

			}// fin del if opcionEstatus = 21			
			
			buffer.write(contenidoArchivo.toString());
			buffer.close();	
			contenidoArchivo = new StringBuffer();//Limpio    
		} catch(Throwable e) {
				throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
					//rs.close();
			} catch(Exception e) {}
		}
	   return nombreArchivo;
			
	}
	
/******************************************************************************
 * 		Los siguientes m�todos regresan el string de las consultas	         *
 ******************************************************************************/
	/**
	 * estatus: Negociable a Baja (4)
	 * @return sentencia sql
	 */
	public String consultaNegociableABaja(){	
		sentencia.append(
				 " SELECT PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"+
				 " 		,D.IG_NUMERO_DOCTO"+
				 " 		,D.CC_ACUSE"+
				 "			,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"+
				 "			,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"+
				 "			,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"+
				 "			,D.IG_PLAZO_DOCTO"+
				 "			,M.CD_NOMBRE AS MONEDA"+
				 "			,D.FN_MONTO"+
             "			,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
             "			,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
				 "			,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"+
				 "			,NVL(D.FN_PORC_DESCUENTO,0) AS FN_PORC_DESCUENTO"+
				 "			,TF.CD_DESCRIPCION AS MODO_PLAZO"+
				 "			,M.ic_moneda"+
				 "			,D.ic_documento"+
				 "			,E.cg_razon_social as NOMBRE_EPO"+
				 "			,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
				 "			,decode(PEP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"+
				 "			,D.cc_acuse "+
				 "			,to_char(D.df_carga,'dd/mm/yyyy') as FECHA_PUBLICACION"+
				 "			,to_char(CE.DC_FECHA_CAMBIO,'dd/mm/yyyy') as FECHA_CAMBIO"+
				 "			,CE.ct_cambio_motivo as CAUSA"+
				 " 		,d.CG_VENTACARTERA as CG_VENTACARTERA, ' ' as MONTO_VALUADO, ' ' as MONTO_DESCONTAR "+
				 " FROM DIS_DOCUMENTO D"+
				 " 		,COMCAT_PYME PY"+
				 " 		,COMCAT_MONEDA M"+
				 " 		,COMREL_PRODUCTO_EPO PE"+
				 " 		,COMCAT_PRODUCTO_NAFIN PN"+
				 " 		,COM_TIPO_CAMBIO TC"+
				 " 		,COMCAT_TIPO_FINANCIAMIENTO TF"+
				 " 		,COMCAT_EPO E"+
				 " 		,DIS_CAMBIO_ESTATUS CE"+
				 " 		,COMREL_PYME_EPO_X_PRODUCTO PEP"+
				 " WHERE D.IC_PYME = PY.IC_PYME"+
				 "			AND D.IC_MONEDA = M.IC_MONEDA"+
				 " 		AND PN.IC_PRODUCTO_NAFIN = 4"+
				 " 		AND PEP.IC_PYME = PY.IC_PYME"+
				 " 		AND PEP.IC_EPO = E.IC_EPO"+
				 " 		AND PEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
				 " 		AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"+
				 " 		AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
				 " 		AND PE.IC_EPO = D.IC_EPO"+
				 " 		AND E.IC_EPO = D.IC_EPO"+
				 " 		AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
             " 		AND M.ic_moneda = TC.ic_moneda" +
				 " 		AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
				 " 		AND D.IC_ESTATUS_DOCTO = 5"+
				 " 		AND CE.IC_CAMBIO_ESTATUS = 4"+
				 " 		AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE)"
				 //"		AND ROWNUM BETWEEN 1 AND 5"		
		);
		log.info("******** consultaNegociableABaja(S)"+sentencia.toString());
		return sentencia.toString();
	}	
	
	/**
	 * estatus: Negociable a Vencido Sin Operar (22)
	 * @return sentnecia sql 
	 */
	public String consultaNegociableAVencidoSinOperar(){
		sentencia.append(
					" SELECT PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"+
					" 		  ,D.IG_NUMERO_DOCTO"+
					" 		  ,D.CC_ACUSE"+
					" 		  ,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"+
					" 		  ,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"+
					" 		  ,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"+
					" 		  ,D.IG_PLAZO_DOCTO"+
					" 		  ,M.CD_NOMBRE AS MONEDA"+
					" 		  ,D.FN_MONTO"+
					" 		  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"+
					" 		  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"+
					" 		  ,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"+
					" 		  ,NVL(D.FN_PORC_DESCUENTO,0) AS FN_PORC_DESCUENTO"+
					" 		  ,TF.CD_DESCRIPCION AS MODO_PLAZO"+
					" 		  ,M.ic_moneda"+
					" 		  ,D.ic_documento"+
					" 		  ,E.cg_razon_social as NOMBRE_EPO"+
					" 		  ,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
					" 		  ,decode(PEP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"+
					" 		  ,D.cc_acuse "+
					" 		  ,to_char(D.df_carga,'dd/mm/yyyy') as FECHA_PUBLICACION"+
					" 		  ,to_char(CE.DC_FECHA_CAMBIO,'dd/mm/yyyy') as FECHA_CAMBIO"+
					" 		  ,d.CG_VENTACARTERA as CG_VENTACARTERA, ' 'AS CAUSA, ' ' as MONTO_VALUADO, ' ' as MONTO_DESCONTAR "+
					" FROM DIS_DOCUMENTO D"+
					" 		,COMCAT_PYME PY"+
					" 		,COMCAT_MONEDA M"+
					" 		,COMREL_PRODUCTO_EPO PE"+
					" 		,COMCAT_PRODUCTO_NAFIN PN"+
					" 		,COM_TIPO_CAMBIO TC"+
					" 		,COMCAT_TIPO_FINANCIAMIENTO TF"+
					" 		,COMCAT_EPO E"+
					" 		,DIS_CAMBIO_ESTATUS CE"+
					" 		,COMREL_PYME_EPO_X_PRODUCTO PEP"+
					" WHERE D.IC_PYME = PY.IC_PYME"+
					" 		AND D.IC_MONEDA = M.IC_MONEDA"+
					" 		AND PN.IC_PRODUCTO_NAFIN = 4"+
					" 		AND PEP.IC_PYME = PY.IC_PYME"+
					" 		AND PEP.IC_EPO = E.IC_EPO"+
					" 		AND PEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
					" 		AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"+
					" 		AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
					" 		AND PE.IC_EPO = D.IC_EPO"+
					" 		AND E.IC_EPO = D.IC_EPO"+
					" 		AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"+
					" 		AND M.ic_moneda = TC.ic_moneda"+
					" 		AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
					" 		AND D.IC_ESTATUS_DOCTO = 9"+
					"     AND CE.IC_CAMBIO_ESTATUS = 22"+
					" 		AND TRUNC (ce.dc_fecha_cambio) = TRUNC (SYSDATE)"
					//" 		AND rownum between 1 and 10 "		
		);
		log.info("******** consultaNegociableAVencidoSinOperar(S)"+sentencia.toString());
		return 	sentencia.toString();
	}
	
	/**
	 * estatus: Seleccionado Pyme a Negociable (2)
	 * @return sentencia sql
	 */
	public String consultaSeleccionadoPymeANegociable(String status){
		qrySentenciaDM= " SELECT PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"+
							 " 	   ,D.IG_NUMERO_DOCTO"+
		   				 " 	   ,D.CC_ACUSE"+
					       "		   ,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"+
							 "		   ,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"+
							 "		   ,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"+
							 "		   ,D.IG_PLAZO_DOCTO"+
							 "		   ,M.CD_NOMBRE AS MONEDA"+
							 "		   ,D.FN_MONTO"+
                   	 "		   ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
                   	 "		   ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
							 "		   ,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"+
							 "		   ,NVL(D.FN_PORC_DESCUENTO,0) AS FN_PORC_DESCUENTO"+
							 "		   ,TF.CD_DESCRIPCION AS MODO_PLAZO"+
							 "		   ,ED.CD_DESCRIPCION AS ESTATUS"+
							 "		   ,M.ic_moneda"+
							 "		   ,D.ic_documento"+
							 "		   ,E.cg_razon_social as NOMBRE_EPO"+
							 "		   ,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
							 "		   ,I.cg_razon_social as NOMBRE_IF"+
							 "		   ,decode(PP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"+
							 "		   ,DS.fn_importe_recibir as MONTO_CREDITO"+
							 "		   ,D.ig_plazo_credito"+
							 "		   ,TCI.cd_descripcion as TIPO_COBRO_INT"+
							 "  		,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) as REFERENCIA_INT"+
							 "		   ,decode(DS.cg_rel_mat,'+',fn_valor_tasa+fn_puntos,'-',fn_valor_tasa-fn_puntos,'*',fn_valor_tasa*fn_puntos,'/',fn_valor_tasa/fn_puntos) as VALOR_TASA_INT"+
							 "		   ,DS.fn_importe_interes as MONTO_TASA_INT"+
							 "		   ,D.cc_acuse"+
							"		   ,CE.ct_cambio_motivo AS causa"+
							 "		   ,to_char(D.df_carga,'dd/mm/yyyy') as FECHA_PUBLICACION"+
							 "		   ,to_char(D.df_fecha_venc_credito,'dd/mm/yyyy') as FECHA_VENC_CREDITO"+
							 "		   ,to_char(CE.dc_fecha_cambio,'dd/mm/yyyy') as FECHA_CAMBIO"+
							 " 	   ,d.CG_VENTACARTERA as CG_VENTACARTERA, ' ' as MONTO_VALUADO, ' ' as MONTO_DESCONTAR, ' ' as MONTO_CAPITAL_INT "+
							 " FROM DIS_DOCUMENTO D"+
							 " 	 ,COMCAT_PYME PY"+
							 " 	 ,COMCAT_MONEDA M"+
							 " 	 ,COMREL_PRODUCTO_EPO PE"+
							 " 	 ,COMCAT_PRODUCTO_NAFIN PN"+
							 " 	 ,COM_TIPO_CAMBIO TC"+
							 " 	 ,COMCAT_TIPO_FINANCIAMIENTO TF"+
							 " 	 ,COMCAT_ESTATUS_DOCTO ED"+
							 " 	 ,DIS_LINEA_CREDITO_DM LC"+
							 " 	 ,COMCAT_EPO E"+
							 " 	 ,DIS_DOCTO_SELECCIONADO DS"+
							 " 	 ,COMCAT_IF I"+
							 " 	 ,COMREL_PYME_EPO_X_PRODUCTO PP"+
							 " 	 ,COMCAT_TIPO_COBRO_INTERES TCI"+
							 " 	 ,COMCAT_TASA CT"+
							 " 	 ,DIS_CAMBIO_ESTATUS CE"+
							 " WHERE D.IC_PYME = PY.IC_PYME"+
							 " 	 AND DS.IC_DOCUMENTO = D.IC_DOCUMENTO"+
							 " 	 AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"+
							 " 	 AND D.IC_MONEDA = M.IC_MONEDA"+
							 " 	 AND PN.IC_PRODUCTO_NAFIN = 4"+
							 " 	 AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
							 " 	 AND PE.IC_EPO = D.IC_EPO"+
							 " 	 AND I.IC_IF = LC.IC_IF"+
							 " 	 AND E.IC_EPO = D.IC_EPO"+
							 " 	 AND PP.IC_EPO = E.IC_EPO"+
							 " 	 AND PP.IC_PYME = PY.IC_PYME"+
							 " 	 AND PP.IC_PRODUCTO_NAFIN = 4"+
							 " 	 AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
                      " 	 AND M.ic_moneda = TC.ic_moneda" +
							 " 	 AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
							 " 	 AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"+
							 " 	 AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM"+
							 " 	 AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES"+
							 " 	 AND CT.IC_TASA = DS.IC_TASA"+
							 " 	 AND CE.IC_CAMBIO_ESTATUS ="+status+
							 "       AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE)";
							// "		AND ROWNUM BETWEEN 1 AND 5";

	qrySentenciaCCC =	" SELECT PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"+
							" 	     ,D.IG_NUMERO_DOCTO"+
							" 	     ,D.CC_ACUSE"+
							"	     ,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"+
							"	     ,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"+
							"	     ,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"+
							"	     ,D.IG_PLAZO_DOCTO"+
							"	     ,M.CD_NOMBRE AS MONEDA"+
							"	     ,D.FN_MONTO"+
                   	"	     ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
                   	"	     ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
							"	     ,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"+
							"	     ,NVL(D.FN_PORC_DESCUENTO,0) AS FN_PORC_DESCUENTO"+
							"	     ,TF.CD_DESCRIPCION AS MODO_PLAZO"+
							"	     ,ED.CD_DESCRIPCION AS ESTATUS"+
							"	     ,M.ic_moneda"+
							"	     ,D.ic_documento"+
							"	     ,E.cg_razon_social as NOMBRE_EPO"+
							"	     ,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
							"	     ,I.cg_razon_social as NOMBRE_IF"+
							"	     ,decode(PP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"+
							"	     ,DS.fn_importe_recibir as MONTO_CREDITO"+
							"	     ,D.ig_plazo_credito"+
							"	     ,TCI.cd_descripcion as TIPO_COBRO_INT"+
							"	     ,CT.cd_nombre||' '||DS.cg_rel_mat||' '||DS.fn_puntos as REFERENCIA_INT"+
							"	     ,decode(DS.cg_rel_mat,'+',fn_valor_tasa+fn_puntos,'-',fn_valor_tasa-fn_puntos,'*',fn_valor_tasa*fn_puntos,'/',fn_valor_tasa/fn_puntos) as VALOR_TASA_INT"+
							"	     ,DS.fn_importe_interes as MONTO_TASA_INT"+
							"	     ,D.cc_acuse"+
							"		   ,CE.ct_cambio_motivo AS causa"+
							"	     ,to_char(D.df_carga,'dd/mm/yyyy') as FECHA_PUBLICACION"+
							"	     ,to_char(D.df_fecha_venc_credito,'dd/mm/yyyy') as FECHA_VENC_CREDITO"+
							"	     ,to_char(CE.dc_fecha_cambio,'dd/mm/yyyy') as FECHA_CAMBIO"+
							"       ,d.CG_VENTACARTERA as CG_VENTACARTERA, ' ' as MONTO_VALUADO, ' ' as MONTO_DESCONTAR, ' ' as MONTO_CAPITAL_INT  "+
							" FROM DIS_DOCUMENTO D"+
							"     ,COMCAT_PYME PY"+
							"     ,COMCAT_MONEDA M"+
							"     ,COMREL_PRODUCTO_EPO PE"+
							"     ,COMCAT_PRODUCTO_NAFIN PN"+
							"     ,COM_TIPO_CAMBIO TC"+
							"     ,COMCAT_TIPO_FINANCIAMIENTO TF"+
							"     ,COMCAT_ESTATUS_DOCTO ED"+
							"     ,COM_LINEA_CREDITO LC"+
							"     ,COMCAT_EPO E"+
							"     ,DIS_DOCTO_SELECCIONADO DS"+
							"     ,COMCAT_IF I"+
							"     ,COMREL_PYME_EPO_X_PRODUCTO PP"+
							"     ,COMCAT_TIPO_COBRO_INTERES TCI"+
							"     ,COMCAT_TASA CT"+
							"     ,DIS_CAMBIO_ESTATUS CE"+
							" WHERE D.IC_PYME = PY.IC_PYME"+
							"       AND DS.IC_DOCUMENTO = D.IC_DOCUMENTO"+
							"       AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"+
							"       AND D.IC_MONEDA = M.IC_MONEDA"+
							"       AND PN.IC_PRODUCTO_NAFIN = 4"+
							"       AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
							"       AND PE.IC_EPO = D.IC_EPO"+
							"       AND I.IC_IF = LC.IC_IF"+  
							"       AND E.IC_EPO = D.IC_EPO"+
							"       AND PP.IC_EPO = E.IC_EPO"+
							"       AND PP.IC_PYME = PY.IC_PYME"+
							"       AND PP.IC_PRODUCTO_NAFIN = 4"+
							"       AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
                     "       AND M.ic_moneda = TC.ic_moneda" +
							"       AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
					 		"       AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"+
							"       AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO"+
							"       AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES"+
							"       AND CT.IC_TASA = DS.IC_TASA"+
							"       AND CE.IC_CAMBIO_ESTATUS ="+status+
							"       AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE)";
							//"		AND ROWNUM BETWEEN 1 AND 5";
		
		sentencia.append(qrySentenciaDM + " UNION ALL "+qrySentenciaCCC);
		log.info("******** consultaSeleccionadoPymeANegociable:::"+sentencia.toString());
		return sentencia.toString();
	}
	
	/**
	 * estatus: Modificar (21)
	 * @return 
	 */
	public String consultaModificar(){	
		sentencia.append(
					" SELECT PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"+
					" 			,D.IG_NUMERO_DOCTO"+
					" 			,D.CC_ACUSE"+
					"			,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"+
					"			,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"+
					"			,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"+
					"			,D.IG_PLAZO_DOCTO"+
					"			,M.CD_NOMBRE AS MONEDA"+
					"			,D.FN_MONTO"+
               "			,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
               "			,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
					"			,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"+
					"			,NVL(D.FN_PORC_DESCUENTO,0) AS FN_PORC_DESCUENTO"+
					"			,TF.CD_DESCRIPCION AS MODO_PLAZO"+
					"			,M.ic_moneda"+
					"			,D.ic_documento"+
					"			,E.cg_razon_social as NOMBRE_EPO"+
					"			,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
					"			,decode(PEP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"+
					"			,D.cc_acuse "+
					"			,to_char(D.df_carga,'dd/mm/yyyy') as FECHA_PUBLICACION"+
					"			,to_char(CE.DC_FECHA_CAMBIO,'dd/mm/yyyy') as FECHA_CAMBIO"+
					"			,to_char(CE.DF_FECHA_EMISION_ANTERIOR,'dd/mm/yyyy') as FECHA_EMISION_ANT"+
					"			,to_char(CE.DF_FECHA_VENC_ANTERIOR,'dd/mm/yyyy') as FECHA_VENC_ANT"+
					"			,CE.fn_monto_anterior"+
					"			,TF2.cd_descripcion AS MODO_PLAZO_ANT"+
					" 			,d.CG_VENTACARTERA as CG_VENTACARTERA, ' ' as MONTO_VALUADO, ' ' as MONTO_DESCONTAR "+
					" FROM DIS_DOCUMENTO D"+
					" 		,COMCAT_PYME PY"+
					" 		,COMCAT_MONEDA M"+
					" 		,COMREL_PRODUCTO_EPO PE"+
					" 		,COMCAT_PRODUCTO_NAFIN PN"+
					" 		,COM_TIPO_CAMBIO TC"+
					" 		,COMCAT_TIPO_FINANCIAMIENTO TF"+
					" 		,COMCAT_TIPO_FINANCIAMIENTO TF2"+
					" 		,COMCAT_EPO E"+
					" 		,DIS_CAMBIO_ESTATUS CE"+
					" 		,COMREL_PYME_EPO_X_PRODUCTO PEP"+
					" WHERE D.IC_PYME = PY.IC_PYME"+
					" 		AND D.IC_MONEDA = M.IC_MONEDA"+
					" 		AND PN.IC_PRODUCTO_NAFIN = 4"+
					" 		AND PEP.IC_PYME = PY.IC_PYME"+
					" 		AND PEP.IC_EPO = E.IC_EPO"+
					" 		AND PEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
					" 		AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"+
					" 		AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
					" 		AND PE.IC_EPO = D.IC_EPO"+
					" 		AND E.IC_EPO = D.IC_EPO"+
					" 		AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
               " 		AND M.ic_moneda = TC.ic_moneda" +
					" 		AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
					" 		AND CE.IC_CAMBIO_ESTATUS = 21"+
					" 		AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE)"+
					" 		AND TF2.IC_TIPO_FINANCIAMIENTO(+) = CE.IC_TIPO_FINAN_ANT"
					//"		AND ROWNUM BETWEEN 1 AND 5"		
		);
		log.info("********consultaModificar(S)"+sentencia.toString());
		return sentencia.toString();
	}
	
	/**
	 * estatus: No Negociable a Baja (24)
	 * @return sentnecia sql
	 */
	public String consultaNoNegociableABaja(){		
		sentencia.append(
					" SELECT PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"+
					" 		  ,D.IG_NUMERO_DOCTO"+
					" 		  ,D.CC_ACUSE"+
					"		  ,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"+
					"		  ,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"+
					"		  ,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"+
					"		  ,D.IG_PLAZO_DOCTO"+
					"		  ,M.CD_NOMBRE AS MONEDA"+
					"		  ,D.FN_MONTO"+
               "		  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
               "		  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
					"		  ,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"+
					"		  ,NVL(D.FN_PORC_DESCUENTO,0) AS FN_PORC_DESCUENTO"+
					"		  ,TF.CD_DESCRIPCION AS MODO_PLAZO"+
					"		  ,M.ic_moneda"+
					"		  ,D.ic_documento"+
					"		  ,E.cg_razon_social as NOMBRE_EPO"+
					"		  ,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
					"		  ,decode(PEP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"+
					"		  ,D.cc_acuse "+
					"		  ,to_char(D.df_carga,'dd/mm/yyyy') as FECHA_PUBLICACION"+
					"		  ,to_char(CE.DC_FECHA_CAMBIO,'dd/mm/yyyy') as FECHA_CAMBIO"+
					"		  ,CE.ct_cambio_motivo as CAUSA"+
					" 	  	  ,d.CG_VENTACARTERA as CG_VENTACARTERA, ' ' as MONTO_VALUADO, ' ' as MONTO_DESCONTAR "+
					" FROM DIS_DOCUMENTO D"+
					" 	  	,COMCAT_PYME PY"+
					" 	  	,COMCAT_MONEDA M"+
					" 	  	,COMREL_PRODUCTO_EPO PE"+
					" 	  	,COMCAT_PRODUCTO_NAFIN PN"+
					" 	  	,COM_TIPO_CAMBIO TC"+
					" 	  	,COMCAT_TIPO_FINANCIAMIENTO TF"+
					" 	  	,COMCAT_EPO E"+
					" 	  	,DIS_CAMBIO_ESTATUS CE"+
					" 	  	,COMREL_PYME_EPO_X_PRODUCTO PEP"+
					" WHERE D.IC_PYME = PY.IC_PYME"+
					"       AND D.IC_MONEDA = M.IC_MONEDA"+
					"       AND PN.IC_PRODUCTO_NAFIN = 4"+
					"       AND PEP.IC_PYME = PY.IC_PYME"+
					"       AND PEP.IC_EPO = E.IC_EPO"+
					"       AND PEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
					"       AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"+
					"       AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
					"       AND PE.IC_EPO = D.IC_EPO"+
					"       AND E.IC_EPO = D.IC_EPO"+
					"       AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
               "       AND M.ic_moneda = TC.ic_moneda" +
					"       AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
					"       AND CE.IC_CAMBIO_ESTATUS = 24"+
					"       AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE)"
					//"		AND ROWNUM BETWEEN 1 AND 5"
			);
			log.info("********consultaNoNegociableABaja(S)"+sentencia.toString());
		return sentencia.toString();			
	}
	
	
/**	
	public String consultaSeleccionadoPymeARechazadoIF(){
		String qrySentenciaDM=" SELECT PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"+
					" 	,D.IG_NUMERO_DOCTO"+
					" 	,D.CC_ACUSE"+
					"	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"+
					"	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"+
					"	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"+
					"	,D.IG_PLAZO_DOCTO"+
					"	,M.CD_NOMBRE AS MONEDA"+
					"	,D.FN_MONTO"+
                   	"	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
                   	"	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
					"	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"+
					"	,NVL(D.FN_PORC_DESCUENTO,0) AS FN_PORC_DESCUENTO"+
					"	,TF.CD_DESCRIPCION AS MODO_PLAZO"+
					"	,ED.CD_DESCRIPCION AS ESTATUS"+
					"	,M.ic_moneda"+
					"	,D.ic_documento"+
					"	,E.cg_razon_social as NOMBRE_EPO"+
					"	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
					"	,I.cg_razon_social as NOMBRE_IF"+
					"	,decode(PP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"+
					"	,DS.fn_importe_recibir as MONTO_CREDITO"+
					"	,D.ig_plazo_credito"+
					"	,TCI.cd_descripcion as TIPO_COBRO_INT"+
					"  	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) as REFERENCIA_INT"+
					"	,decode(DS.cg_rel_mat,'+',fn_valor_tasa+fn_puntos,'-',fn_valor_tasa-fn_puntos,'*',fn_valor_tasa*fn_puntos,'/',fn_valor_tasa/fn_puntos) as VALOR_TASA_INT"+
					"	,DS.fn_importe_interes as MONTO_TASA_INT"+
					"	,D.cc_acuse"+
					"	,to_char(D.df_carga,'dd/mm/yyyy') as FECHA_PUBLICACION"+
					"	,to_char(D.df_fecha_venc_credito,'dd/mm/yyyy') as FECHA_VENC_CREDITO"+
					"	,to_char(CE.dc_fecha_cambio,'dd/mm/yyyy') as FECHA_CAMBIO"+
					" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
					" FROM DIS_DOCUMENTO D"+
					" ,COMCAT_PYME PY"+
					" ,COMCAT_MONEDA M"+
					" ,COMREL_PRODUCTO_EPO PE"+
					" ,COMCAT_PRODUCTO_NAFIN PN"+
					" ,COM_TIPO_CAMBIO TC"+
					" ,COMCAT_TIPO_FINANCIAMIENTO TF"+
					" ,COMCAT_ESTATUS_DOCTO ED"+
					" ,DIS_LINEA_CREDITO_DM LC"+
					" ,COMCAT_EPO E"+
					" ,DIS_DOCTO_SELECCIONADO DS"+
					" ,COMCAT_IF I"+
					" ,COMREL_PYME_EPO_X_PRODUCTO PP"+
					" ,COMCAT_TIPO_COBRO_INTERES TCI"+
					" ,COMCAT_TASA CT"+
					" ,DIS_CAMBIO_ESTATUS CE"+
					" WHERE D.IC_PYME = PY.IC_PYME"+
					" AND DS.IC_DOCUMENTO = D.IC_DOCUMENTO"+
					" AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"+
					" AND D.IC_MONEDA = M.IC_MONEDA"+
					" AND PN.IC_PRODUCTO_NAFIN = 4"+
					" AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
					" AND PE.IC_EPO = D.IC_EPO"+
					" AND I.IC_IF = LC.IC_IF"+
					" AND E.IC_EPO = D.IC_EPO"+
					" AND PP.IC_EPO = E.IC_EPO"+
					" AND PP.IC_PYME = PY.IC_PYME"+
					" AND PP.IC_PRODUCTO_NAFIN = 4"+
					" AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
                    " AND M.ic_moneda = TC.ic_moneda" +
					" AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
					" AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"+
					" AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM"+
					" AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES"+
					" AND CT.IC_TASA = DS.IC_TASA"+
//					" AND D.IC_ESTATUS_DOCTO = 3"+
					" AND CE.IC_CAMBIO_ESTATUS = 2"+
					" AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE)";
		String qrySentenciaCCC=" SELECT PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"+
					" 	,D.IG_NUMERO_DOCTO"+
					" 	,D.CC_ACUSE"+
					"	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"+
					"	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"+
					"	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"+
					"	,D.IG_PLAZO_DOCTO"+
					"	,M.CD_NOMBRE AS MONEDA"+
					"	,D.FN_MONTO"+
                   	"	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
                   	"	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
					"	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"+
					"	,NVL(D.FN_PORC_DESCUENTO,0) AS FN_PORC_DESCUENTO"+
					"	,TF.CD_DESCRIPCION AS MODO_PLAZO"+
					"	,ED.CD_DESCRIPCION AS ESTATUS"+
					"	,M.ic_moneda"+
					"	,D.ic_documento"+
					"	,E.cg_razon_social as NOMBRE_EPO"+
					"	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
					"	,I.cg_razon_social as NOMBRE_IF"+
					"	,decode(PP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"+
					"	,DS.fn_importe_recibir as MONTO_CREDITO"+
					"	,D.ig_plazo_credito"+
					"	,TCI.cd_descripcion as TIPO_COBRO_INT"+
					"	,CT.cd_nombre||' '||DS.cg_rel_mat||' '||DS.fn_puntos as REFERENCIA_INT"+
					"	,decode(DS.cg_rel_mat,'+',fn_valor_tasa+fn_puntos,'-',fn_valor_tasa-fn_puntos,'*',fn_valor_tasa*fn_puntos,'/',fn_valor_tasa/fn_puntos) as VALOR_TASA_INT"+
					"	,DS.fn_importe_interes as MONTO_TASA_INT"+
					"	,D.cc_acuse"+
					"	,to_char(D.df_carga,'dd/mm/yyyy') as FECHA_PUBLICACION"+
					"	,to_char(D.df_fecha_venc_credito,'dd/mm/yyyy') as FECHA_VENC_CREDITO"+
					"	,to_char(CE.dc_fecha_cambio,'dd/mm/yyyy') as FECHA_CAMBIO"+
					" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
					" FROM DIS_DOCUMENTO D"+
					" ,COMCAT_PYME PY"+
					" ,COMCAT_MONEDA M"+
					" ,COMREL_PRODUCTO_EPO PE"+
					" ,COMCAT_PRODUCTO_NAFIN PN"+
					" ,COM_TIPO_CAMBIO TC"+
					" ,COMCAT_TIPO_FINANCIAMIENTO TF"+
					" ,COMCAT_ESTATUS_DOCTO ED"+
					" ,COM_LINEA_CREDITO LC"+
					" ,COMCAT_EPO E"+
					" ,DIS_DOCTO_SELECCIONADO DS"+
					" ,COMCAT_IF I"+
					" ,COMREL_PYME_EPO_X_PRODUCTO PP"+
					" ,COMCAT_TIPO_COBRO_INTERES TCI"+
					" ,COMCAT_TASA CT"+
					" ,DIS_CAMBIO_ESTATUS CE"+
					" WHERE D.IC_PYME = PY.IC_PYME"+
					" AND DS.IC_DOCUMENTO = D.IC_DOCUMENTO"+
					" AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"+
					" AND D.IC_MONEDA = M.IC_MONEDA"+
					" AND PN.IC_PRODUCTO_NAFIN = 4"+
					" AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
					" AND PE.IC_EPO = D.IC_EPO"+
					" AND I.IC_IF = LC.IC_IF"+
					" AND E.IC_EPO = D.IC_EPO"+
					" AND PP.IC_EPO = E.IC_EPO"+
					" AND PP.IC_PYME = PY.IC_PYME"+
					" AND PP.IC_PRODUCTO_NAFIN = 4"+
					" AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
                    " AND M.ic_moneda = TC.ic_moneda" +
					" AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
					" AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"+
					" AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO"+
					" AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES"+
					" AND CT.IC_TASA = DS.IC_TASA"+
					" AND CE.IC_CAMBIO_ESTATUS = 2"+
					" AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE)";
		sentencia = qrySentenciaDM + " UNION ALL "+qrySentenciaCCC;
		return sentencia;
	}
	*/
/******************************************************************************
 * 								Getters y Setters	        							      *
 ******************************************************************************/	
	public List getConditions(){
		return conditions; 
	}

	public String getOpcionEstatus() {
		return opcionEstatus;
	}

	public void setOpcionEstatus(String opcionEstatus) {
		this.opcionEstatus = opcionEstatus;
	}

	public String getTituloTabla() {
		return tituloTabla;
	}

	public void setTituloTabla(String tituloTabla) {
		this.tituloTabla = tituloTabla;
	}
/**
 	double totalDoctosMN=0, totalMontoMN=0, totalMontoValuadoMN=0, totalMontoCreditoMN=0, totalMontoDescuentoMN=0, totalMontoInteresesMN=0;
	double totalDoctosUS=0, totalMontoUS=0, totalMontoValuadoUS=0, totalMontoCreditoUS=0, totalMontoDescuentoUS=0, totalMontoInteresesUS=0;
	if(icMoneda.equals("1")){
		totalDoctosMN++;
		totalMontoMN+=monto;
		totalMontoValuadoMN+=montoValuado; 
	} else{
		totalDoctosUS++;
		totalMontoUS+=monto;
		totalMontoValuadoUS+=montoValuado; 
	}	
 */
	
	
}