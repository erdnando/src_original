package com.netro.distribuidores;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Registros;

public class DetalleCambiosDoctosIf{
 	 
	private StringBuffer qrysentencia;
	private String ic_docto;
	private String tipo_credito;
	private Registros registros;
	private List conditions;
	
	public Registros executeQuery() {
		AccesoDB con = new AccesoDB();
		Registros registros = new Registros();
		conditions = new ArrayList();
		this.qrysentencia = new StringBuffer();

		if("D".equals(this.tipo_credito)){
	        this.qrysentencia.append(
				" SELECT E.CG_RAZON_SOCIAL AS EPO"   +
				" 	   ,D.CC_ACUSE"   +
				" 	   ,D.IG_NUMERO_DOCTO"   +
				" 	   ,TO_CHAR(D.DF_CARGA,'dd/mm/yyyy') as fecha_publicacion"   +
				" 	   ,TO_CHAR(D.DF_FECHA_EMISION,'dd/mm/yyyy') as fecha_emision"   +
				" 	   ,TO_CHAR(D.DF_FECHA_VENC,'dd/mm/yyyy') as fecha_venc"   +
				" 	   ,D.IG_PLAZO_DOCTO"   +
				" 	   ,D.fn_monto"   +
				" 	   ,M.cd_nombre as MONEDA"   +
				" 	   ,D.IG_PLAZO_DESCUENTO"   +
				" 	   ,D.FN_PORC_DESCUENTO"   +
				" 	   ,TF.CD_DESCRIPCION AS MODO_PLAZO"   +
				"	   ,NVL(pe.cg_tipo_conversion,pn.cg_tipo_conversion) as TIPO_CONVERSION"+
				"	   ,D.fn_tipo_cambio as TIPO_CAMBIO"+
				"	   ,D.ic_moneda AS MONEDA_DOCTO"+
				"	   ,LC.ic_moneda AS MONEDA_LINEA"+
				"     , a.FN_VALOR_AFORO as PORCENTAJE_AFORO "+
				" FROM DIS_DOCUMENTO D"   +
				" 	 ,COMCAT_EPO E"   +
				" 	 ,COMCAT_MONEDA M"   +
				" 	 ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
				"	 ,COMREL_PRODUCTO_EPO PE"+
				"	 ,COMCAT_PRODUCTO_NAFIN PN"+
				"	 ,DIS_LINEA_CREDITO_DM LC"+
				"  , dis_aforo_epo a  "+
				" WHERE E.IC_EPO = D.IC_EPO"   +
				" AND D.IC_MONEDA = M.IC_MONEDA"   +
				" AND TF.IC_TIPO_FINANCIAMIENTO = D.IC_TIPO_FINANCIAMIENTO"   +
				" AND PE.IC_EPO = D.IC_EPO"+
				" AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
				" AND PN.IC_PRODUCTO_NAFIN = ? "+
				" AND PE.IC_PRODUCTO_NAFIN = ? "+
				" AND LC.IC_LINEA_CREDITO_DM = D.IC_LINEA_CREDITO_DM"+
				" AND D.IC_DOCUMENTO = ? " +
				" and a.ic_epo(+) = d.ic_epo  "+
				" and a.ic_moneda(+) = d.ic_moneda ");
				
		}else if("C".equals(this.tipo_credito)	||	"F".equals(this.tipo_credito)){
	        this.qrysentencia.append(
				" SELECT E.CG_RAZON_SOCIAL AS EPO"   +
				" 	   ,D.CC_ACUSE"   +
				" 	   ,D.IG_NUMERO_DOCTO"   +
				" 	   ,TO_CHAR(D.DF_CARGA,'dd/mm/yyyy') as fecha_publicacion"   +
				" 	   ,TO_CHAR(D.DF_FECHA_EMISION,'dd/mm/yyyy') as fecha_emision"   +
				" 	   ,TO_CHAR(D.DF_FECHA_VENC,'dd/mm/yyyy') as fecha_venc"   +
				" 	   ,D.IG_PLAZO_DOCTO"   +
				" 	   ,D.fn_monto"   +
				" 	   ,M.cd_nombre as MONEDA"   +
				" 	   ,D.IG_PLAZO_DESCUENTO"   +
				" 	   ,D.FN_PORC_DESCUENTO"   +
				" 	   ,TF.CD_DESCRIPCION AS MODO_PLAZO"   +
				"	   ,NVL(pe.cg_tipo_conversion,pn.cg_tipo_conversion) as TIPO_CONVERSION"+
				"	   ,D.fn_tipo_cambio as TIPO_CAMBIO"+
				"	   ,D.ic_moneda AS MONEDA_DOCTO"+
				"	   ,LC.ic_moneda AS MONEDA_LINEA"+
				" FROM DIS_DOCUMENTO D"   +
				" 	 ,COMCAT_EPO E"   +
				" 	 ,COMCAT_MONEDA M"   +
				" 	 ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
				"	 ,COMREL_PRODUCTO_EPO PE"+
				"	 ,COMCAT_PRODUCTO_NAFIN PN"+
				"	 ,COM_LINEA_CREDITO LC"+
				" WHERE E.IC_EPO = D.IC_EPO"   +
				" AND D.IC_MONEDA = M.IC_MONEDA"   +
				" AND TF.IC_TIPO_FINANCIAMIENTO = D.IC_TIPO_FINANCIAMIENTO"   +
				" AND PE.IC_EPO = D.IC_EPO"+
				" AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
				" AND PN.IC_PRODUCTO_NAFIN = ?"+
				" AND PE.IC_PRODUCTO_NAFIN = ?"+
				" AND LC.IC_LINEA_CREDITO = D.IC_LINEA_CREDITO" +
				" AND D.IC_DOCUMENTO = ? " );
		}

		try{
			con.conexionDB();
			conditions.add(new Integer(4));
			conditions.add(new Integer(4));
			conditions.add(this.ic_docto);
			registros = con.consultarDB(this.qrysentencia.toString(),conditions);
			System.out.println("qrysentencia "+qrysentencia.toString());
			System.out.println("conditions "+conditions);
			
			return registros;
		} catch(Exception e) {
			throw new AppException("detalleCambiosDoctosIf::ExecuteQuery(Exception) ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(true); //Para consultas que hagan uso de tablas remotas via dblink
				con.cierraConexionDB();
			}
		}
	}

	public void setIcDocumento(String ic_docto) {
		this.ic_docto = ic_docto;
	}

	public void setTipoCredito(String tipo_credito) {
		this.tipo_credito = tipo_credito;
	}

}//Class End.