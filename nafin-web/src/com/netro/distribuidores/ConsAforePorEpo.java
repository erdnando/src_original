package com.netro.distribuidores;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsAforePorEpo implements  IQueryGeneratorRegExtJS {
	public ConsAforePorEpo() {  }
	private List 	conditions;
	StringBuffer 	strQuery;
	private static final Log log = ServiceLocator.getInstance().getLog(ConsAforePorEpo.class);//Variable para enviar mensajes al log.
	private String claveEPO;
	private String claveIF;
	public String getAggregateCalculationQuery() {
		return "";
 	}  
		 
	public String getDocumentQuery(){
		conditions = new ArrayList();	
		strQuery 		= new StringBuffer(); 
		log.debug("getDocumentQuery)"+strQuery.toString()); 
		log.debug("getDocumentQuery)"+conditions);
		return strQuery.toString();
 	}  
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
		strQuery = new StringBuffer(); 
		log.debug("getDocumentSummaryQueryForIds "+strQuery.toString()); 
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
		return strQuery.toString();
 	} 
					
	public String getDocumentQueryFile(){
		conditions = new ArrayList();   
		strQuery 		= new StringBuffer(); 
		log.info("getDocumentQueryFile(E)");
		strQuery.append("SELECT /*+ use_nl(lc,cn,ce,cm, dae)         index(lc cp_dis_linea_credito_dm_pk)         index(cn cp_comrel_nafin_pk)*/ " +
			"	distinct	"+
			"	lc.ic_if,	" +
			"	ce.ic_epo, " +
		   "	ce.cg_razon_social AS epo, " +
			"	lc.ic_moneda, " +
			"	cm.cd_nombre , " +
			"	dae.FN_VALOR_AFORO, " +
			"	dae.FN_VALOR_AFORO AS FN_VALOR_AFORO_AUX , " +
			"	dae.CG_USUARIO_ALTA, " +
			"  TO_CHAR(dae.DF_ALTA,'DD/MM/YYYY')  AS DF_ALTA, " +
			"	dae.CG_USUARIO_MODIFICA,  " +
			"  TO_CHAR(dae.DF_MODIFICA,'DD/MM/YYYY')  AS DF_MODIFICACION " +
			"	FROM dis_linea_credito_dm lc " +
			"	LEFT JOIN dis_aforo_epo  dae   on (lc.ic_epo  = dae.ic_epo and  lc.ic_if  = dae.ic_if  and  lc.ic_moneda  = dae.ic_moneda ),	" +
			"	comcat_epo ce, " +
			"	comcat_moneda cm,  " +
			"	comrel_producto_epo pre  " +
			"	WHERE lc.ic_epo = ce.ic_epo  " +
			" 	AND lc.cg_tipo_solicitud = 'I'  " +
			" 	AND pre.ic_epo = ce.ic_epo  " +
			" 	AND pre.cg_tipos_credito ='D'  " +
			" 	AND lc.ic_moneda = cm.ic_moneda  " +
			" 	AND lc.ic_estatus_linea = 12  " );
		if(!claveIF.equals("")){
				strQuery.append(" AND lc.ic_if = ? ");
				conditions.add(claveIF);
		}
		if(!claveEPO.equals("")){
				strQuery.append(" and ce.ic_epo = ? ");
				conditions.add(claveEPO);
		}
		log.debug("getDocumentQueryFile "+strQuery.toString());
		log.debug("getDocumentQueryFile "+conditions);
		log.info("getDocumentQueryFile(S)");
		return strQuery.toString();
 	} 		
	
	/**
	 * En este método se debe realizar la implementación de la generación de archivo
	 * con base en el resultset que se recibe como parámetro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generará el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		return nombreArchivo;
	}
	
	/**
	 * En este método se debe realizar la implementación de la generación de archivo
	 * con base en el objeto Registros que recibe como parámetro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generará el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	public List getConditions() {
		return conditions;
	}

	public String getClaveEPO() {
		return claveEPO;  
	}

	public void setClaveEPO(String claveEPO) {
		this.claveEPO = claveEPO;
	}

	public String getClaveIF() {
		return claveIF;
	}

	public void setClaveIF(String claveIF) {
		this.claveIF = claveIF;
	}

	


}