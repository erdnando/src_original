package com.netro.distribuidores;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsPagoCreditos implements IQueryGeneratorRegExtJS {
public ConsPagoCreditos(){}

	private List 	conditions;
	StringBuffer strQuery = new StringBuffer();
	private static final Log log = ServiceLocator.getInstance().getLog(ConsPagoCreditos.class);//Variable para enviar mensajes al log.

	private String tipoCredito;
	private String ic_epo;
	private String numero_credito;
	private String fechaOpera_ini;
	private String fechaOpera_fin;
	private String monto_ini;
	private String monto_fin;
	private String fechaVence_ini;
	private String fechaVence_fin;
	private String estatus;
	private String tipo_cobro;
	private String ic_pyme;
	private String ic_if;

	public String getAggregateCalculationQuery() {
		conditions	=	new ArrayList();	
		strQuery		=	new StringBuffer();
		StringBuffer qrySentencia = new StringBuffer();
		strQuery.append(
					"SELECT /*+ index (docto IN_DIS_DOCUMENTO_01_NUK, docto_sel CP_DIS_DOCTO_SELECCIONADO_PK) */"   +
					"     docto.ic_moneda as moneda, moneda.cd_nombre as nommoneda, docto.fn_monto as monto, "   +
					"		(docto.fn_monto * docto.fn_tipo_cambio) AS monto_valuado, (docto_sel.fn_importe_interes) AS monto_interes "
		 );
		strQuery.append(
					"FROM comcat_epo             epo, " +
					"     comcat_pyme            pyme, " +
					"     dis_documento          docto, " +
					"     comcat_moneda          moneda, " +
					"     comrel_producto_epo    prod_epo, " +
					"     comcat_producto_nafin  prod_nafin, " +
					"     dis_solicitud          soli, " +
					"     dis_docto_seleccionado docto_sel ");
		 if("D".equals(tipoCredito)){
			strQuery.append(", dis_linea_credito_dm lin_cred ");
		 }else if("C".equals(tipoCredito)){
			strQuery.append(", com_linea_credito lin_cred ");
		 }
		strQuery.append(
					"WHERE docto.ic_epo = epo.ic_epo " +
					"      AND docto.ic_pyme = pyme.ic_pyme " +
					"      AND lin_cred.ic_moneda = moneda.ic_moneda " +
					"      AND prod_nafin.ic_producto_nafin = ? " +
					"      AND prod_epo.ic_producto_nafin   = prod_nafin.ic_producto_nafin " +
					"      AND prod_epo.ic_epo = docto.ic_epo " +
					"      AND docto.ic_documento = soli.ic_documento(+) " +
					"      AND docto.ic_documento = docto_sel.ic_documento " +
					" 	   AND epo.cs_habilitado = ? "
		);
		conditions.add(new Integer(4));
		conditions.add("S");
		
		if("D".equals(tipoCredito)){
			strQuery.append(" AND docto.ic_linea_credito_dm = lin_cred.ic_linea_credito_dm ");
		}else if("C".equals(tipoCredito)){
			strQuery.append(" AND docto.ic_linea_credito = lin_cred.ic_linea_credito ");
		}
		if (ic_if != null && !"".equals(ic_if)){
			strQuery.append(" AND lin_cred.ic_if = ? ");
			conditions.add(ic_if);
		}
		if (ic_epo != null && !"".equals(ic_epo)){
			strQuery.append(" AND epo.ic_epo = ? ");
			conditions.add(ic_epo);
		}
		if (ic_pyme != null && !"".equals(ic_pyme)){
			strQuery.append(" AND pyme.ic_pyme = ? ");
			conditions.add(ic_pyme);
		}
		if (numero_credito != null && !numero_credito.equals("") )	{
			strQuery.append(" AND docto.ic_documento = ? ");
			conditions.add(numero_credito);
		}
		if (fechaOpera_ini != null && !fechaOpera_ini.equals("") )	{
			strQuery.append(" AND soli.df_operacion >= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaOpera_ini);
		}
		if (fechaOpera_fin != null && !fechaOpera_fin.equals("") )	{
			strQuery.append(" AND soli.df_operacion >= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaOpera_fin);
		}
		if ( monto_ini != null && !monto_ini.equals("") && Double.parseDouble(monto_ini) > 0  )	{
			strQuery.append(" AND docto.fn_monto >= ? ");
			conditions.add(new Integer(monto_ini));
		}
		if ( monto_fin != null && !monto_fin.equals("") && Double.parseDouble(monto_fin) > 0 )	{
			strQuery.append(" AND docto.fn_monto <= ? ");
			conditions.add(new Integer(monto_fin));
		}
		if (!fechaVence_ini.equals(""))	{
			strQuery.append(" AND docto.df_fecha_venc_credito >= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaVence_ini);
		}
		if (!fechaVence_fin.equals(""))	{
			strQuery.append(" AND docto.df_fecha_venc_credito <= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaVence_fin);
		}
		if ( !tipo_cobro.equals("") )	{
			strQuery.append(" AND lin_cred.ic_tipo_cobro_interes = ? ");
			conditions.add(new Integer(tipo_cobro));
		}
		if (!estatus.equals("") )	{
			strQuery.append(" AND docto.ic_estatus_docto = ? ");
			conditions.add(new Integer(estatus));
		}

		qrySentencia.append(
			"SELECT moneda, nommoneda, COUNT (1) as total_registros, SUM (monto) AS total_monto, SUM(monto_valuado) AS total_monto_valuado, SUM(monto_interes) AS total_monto_interes "+
			"  FROM ("+strQuery.toString()+")"+
			" GROUP BY  moneda, nommoneda ORDER BY moneda ");
		
		return qrySentencia.toString();
 	}

	public String getDocumentQuery(){
		conditions = new ArrayList();	
		strQuery 		= new StringBuffer();
		 
		strQuery.append(	"SELECT docto.ic_documento           			AS num_credito "	);
		strQuery.append(
					"FROM comcat_epo             epo, " +
					"     comcat_pyme            pyme, " +
					"     dis_documento          docto, " +
					"     comcat_moneda          moneda, " +
					"     comrel_producto_epo    prod_epo, " +
					"     comcat_producto_nafin  prod_nafin, " +
					"     dis_solicitud          soli, " +
					"     dis_docto_seleccionado docto_sel ");
		 if("D".equals(tipoCredito)){
			strQuery.append(", dis_linea_credito_dm lin_cred ");
		 }else if("C".equals(tipoCredito)){
			strQuery.append(", com_linea_credito lin_cred ");
		 }
		strQuery.append(
					"WHERE docto.ic_epo = epo.ic_epo " +
					"      AND docto.ic_pyme = pyme.ic_pyme " +
					"      AND lin_cred.ic_moneda = moneda.ic_moneda " +
					"      AND prod_nafin.ic_producto_nafin = ? " +
					"      AND prod_epo.ic_producto_nafin   = prod_nafin.ic_producto_nafin " +
					"      AND prod_epo.ic_epo = docto.ic_epo " +
					"      AND docto.ic_documento = soli.ic_documento(+) " +
					"      AND docto.ic_documento = docto_sel.ic_documento " +
					" 	   AND epo.cs_habilitado = ? "
		);
		conditions.add(new Integer(4));
		conditions.add("S");
		
		if("D".equals(tipoCredito)){
			strQuery.append(" AND docto.ic_linea_credito_dm = lin_cred.ic_linea_credito_dm ");
		}else if("C".equals(tipoCredito)){
			strQuery.append(" AND docto.ic_linea_credito = lin_cred.ic_linea_credito ");
		}
		if (ic_if != null && !"".equals(ic_if)){
			strQuery.append(" AND lin_cred.ic_if = ? ");
			conditions.add(ic_if);
		}
		if (ic_epo != null && !"".equals(ic_epo)){
			strQuery.append(" AND epo.ic_epo = ? ");
			conditions.add(ic_epo);
		}
		if (ic_pyme != null && !"".equals(ic_pyme)){
			strQuery.append(" AND pyme.ic_pyme = ? ");
			conditions.add(ic_pyme);
		}
		if (numero_credito != null && !numero_credito.equals("") )	{
			strQuery.append(" AND docto.ic_documento = ? ");
			conditions.add(numero_credito);
		}
		if (fechaOpera_ini != null && !fechaOpera_ini.equals("") )	{
			strQuery.append(" AND soli.df_operacion >= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaOpera_ini);
		}
		if (fechaOpera_fin != null && !fechaOpera_fin.equals("") )	{
			strQuery.append(" AND soli.df_operacion >= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaOpera_fin);
		}
		if ( monto_ini != null && !monto_ini.equals("") && Double.parseDouble(monto_ini) > 0  )	{
			strQuery.append(" AND docto.fn_monto >= ? ");
			conditions.add(new Integer(monto_ini));
		}
		if ( monto_fin != null && !monto_fin.equals("") && Double.parseDouble(monto_fin) > 0 )	{
			strQuery.append(" AND docto.fn_monto <= ? ");
			conditions.add(new Integer(monto_fin));
		}
		if (!fechaVence_ini.equals(""))	{
			strQuery.append(" AND docto.df_fecha_venc_credito >= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaVence_ini);
		}
		if (!fechaVence_fin.equals(""))	{
			strQuery.append(" AND docto.df_fecha_venc_credito <= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaVence_fin);
		}
		if ( !tipo_cobro.equals("") )	{
			strQuery.append(" AND lin_cred.ic_tipo_cobro_interes = ? ");
			conditions.add(new Integer(tipo_cobro));
		}
		if (!estatus.equals("") )	{
			strQuery.append(" AND docto.ic_estatus_docto = ? ");
			conditions.add(new Integer(estatus));
		}
		strQuery.append(" ORDER BY docto.ic_documento");

		log.debug("getDocumentQuery)"+strQuery.toString()); 
		log.debug("getDocumentQuery)"+conditions);
		return strQuery.toString();
 	}  

	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
		strQuery = new StringBuffer(); 

		strQuery.append(
					"SELECT docto.ic_documento           			AS num_credito,   " +
					"       epo.cg_razon_social          			AS nombre_epo,    " +
					"       pyme.cg_razon_social         			AS nombre_pyme,   " +
					"       moneda.cd_nombre AS tipo_moneda, " +
					"       docto_sel.fn_importe_recibir   			AS monto,         " +
					"       DECODE(NVL(prod_epo.cg_tipo_conversion, prod_nafin.cg_tipo_conversion), 'P', 'Dolar-Peso', " +
					"			'Sin Conversion')        			AS tipo_conversion, " +
					"       DECODE(docto.fn_tipo_cambio, 1, ' ', docto.fn_tipo_cambio) AS tipo_cambio, " +
					"       docto.fn_monto * docto.fn_tipo_cambio  	AS monto_valuado, " +
					"       NVL((SELECT cd_nombre FROM comcat_tasa WHERE ic_tasa =  docto_sel.ic_tasa), '') AS ref_tasa, " +
					"       DECODE(docto_sel.cg_rel_mat, '+', docto_sel.fn_puntos + docto_sel.fn_valor_tasa, '-', " +
					"             docto_sel.fn_valor_tasa - docto_sel.fn_puntos, '*', " +
					"			  docto_sel.fn_puntos * docto_sel.fn_valor_tasa, '/', " +
					"             docto_sel.fn_valor_tasa / docto_sel.fn_puntos)  AS tasa_interes, " +
					"       docto.ig_plazo_credito                  AS plazo, " +
					"       TO_CHAR(docto.df_fecha_venc_credito, 'DD/MM/YYYY')    AS fch_vencimiento, " +
					"       docto_sel.fn_importe_interes            AS monto_interes, " +
					"       (SELECT cd_descripcion FROM comcat_tipo_cobro_interes WHERE ic_tipo_cobro_interes = " +
					"	            lin_cred.ic_tipo_cobro_interes ) AS tipo_cobro_inte, " +
					"       (SELECT cd_descripcion FROM comcat_estatus_docto WHERE ic_estatus_docto = docto.ic_estatus_docto ) " +
					"				AS estatus_soli, '' AS flag_combo, '' AS flag_pago_ref "
		 );
		strQuery.append(
					"FROM comcat_epo             epo, " +
					"     comcat_pyme            pyme, " +
					"     dis_documento          docto, " +
					"     comcat_moneda          moneda, " +
					"     comrel_producto_epo    prod_epo, " +
					"     comcat_producto_nafin  prod_nafin, " +
					"     dis_solicitud          soli, " +
					"     dis_docto_seleccionado docto_sel ");
		 if("D".equals(tipoCredito)){
			strQuery.append(", dis_linea_credito_dm lin_cred ");
		 }else if("C".equals(tipoCredito)){
			strQuery.append(", com_linea_credito lin_cred ");
		 }
		strQuery.append(
					"WHERE docto.ic_epo = epo.ic_epo " +
					"      AND docto.ic_pyme = pyme.ic_pyme " +
					"      AND lin_cred.ic_moneda = moneda.ic_moneda " +
					"      AND prod_nafin.ic_producto_nafin = ? " +
					"      AND prod_epo.ic_producto_nafin   = prod_nafin.ic_producto_nafin " +
					"      AND prod_epo.ic_epo = docto.ic_epo " +
					"      AND docto.ic_documento = soli.ic_documento(+) " +
					"      AND docto.ic_documento = docto_sel.ic_documento " +
					" 	   AND epo.cs_habilitado = ? "
		);
		conditions.add(new Integer(4));
		conditions.add("S");
		
		if("D".equals(tipoCredito)){
			strQuery.append(" AND docto.ic_linea_credito_dm = lin_cred.ic_linea_credito_dm ");
		}else if("C".equals(tipoCredito)){
			strQuery.append(" AND docto.ic_linea_credito = lin_cred.ic_linea_credito ");
		}
		if (ic_if != null && !"".equals(ic_if)){
			strQuery.append(" AND lin_cred.ic_if = ? ");
			conditions.add(ic_if);
		}
		if (ic_epo != null && !"".equals(ic_epo)){
			strQuery.append(" AND epo.ic_epo = ? ");
			conditions.add(ic_epo);
		}
		if (ic_pyme != null && !"".equals(ic_pyme)){
			strQuery.append(" AND pyme.ic_pyme = ? ");
			conditions.add(ic_pyme);
		}
		if (numero_credito != null && !numero_credito.equals("") )	{
			strQuery.append(" AND docto.ic_documento = ? ");
			conditions.add(numero_credito);
		}
		if (fechaOpera_ini != null && !fechaOpera_ini.equals("") )	{
			strQuery.append(" AND soli.df_operacion >= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaOpera_ini);
		}
		if (fechaOpera_fin != null && !fechaOpera_fin.equals("") )	{
			strQuery.append(" AND soli.df_operacion >= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaOpera_fin);
		}
		if ( monto_ini != null && !monto_ini.equals("") && Double.parseDouble(monto_ini) > 0  )	{
			strQuery.append(" AND docto.fn_monto >= ? ");
			conditions.add(new Integer(monto_ini));
		}
		if ( monto_fin != null && !monto_fin.equals("") && Double.parseDouble(monto_fin) > 0 )	{
			strQuery.append(" AND docto.fn_monto <= ? ");
			conditions.add(new Integer(monto_fin));
		}
		if (!fechaVence_ini.equals(""))	{
			strQuery.append(" AND docto.df_fecha_venc_credito >= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaVence_ini);
		}
		if (!fechaVence_fin.equals(""))	{
			strQuery.append(" AND docto.df_fecha_venc_credito <= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaVence_fin);
		}
		if ( !tipo_cobro.equals("") )	{
			strQuery.append(" AND lin_cred.ic_tipo_cobro_interes = ? ");
			conditions.add(new Integer(tipo_cobro));
		}
		if (!estatus.equals("") )	{
			strQuery.append(" AND docto.ic_estatus_docto = ? ");
			conditions.add(new Integer(estatus));
		}

		strQuery.append("   AND ( ");
      
		for(int i=0;i<pageIds.size();i++) {
			List lItem = (ArrayList)pageIds.get(i);
			if(i>0) {
				strQuery.append(" OR ");
			}
			strQuery.append(" (docto.ic_documento = ? ) ");
			conditions.add(new Long(lItem.get(0).toString()));
		}//for(int i=0;i<ids.size();i++)
		strQuery.append(" ) ");
		
		log.debug("getDocumentSummaryQueryForIds "+strQuery.toString()); 
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
		return strQuery.toString();
 	}

	public String getDocumentQueryFile(){
		conditions = new ArrayList();
		strQuery = new StringBuffer(); 
		log.debug("getDocumentQueryFile "+strQuery.toString());
		log.debug("getDocumentQueryFile)"+conditions);
		return strQuery.toString();
	}

	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		return "";
	}

	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}	

	public void setTipoCredito(String tipo_credito) {
		this.tipoCredito = tipo_credito;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public void setNumeroCredito(String numero_credito) {
		this.numero_credito = numero_credito;
	}

	public void setFechaOperaIni(String fechaOperaIni) {
		this.fechaOpera_ini = fechaOperaIni;
	}

	public void setFechaOperaFin(String fechaOperaFin) {
		this.fechaOpera_fin = fechaOperaFin;
	}

	public void setMontoIni(String monto_ini) {
		this.monto_ini = monto_ini;
	}

	public void setMontoFin(String monto_fin) {
		this.monto_fin = monto_fin;
	}

	public void setFechaVenceIni(String fechaVenceIni) {
		this.fechaVence_ini = fechaVenceIni;
	}

	public void setFechaVenceFin(String fechaVenceFin) {
		this.fechaVence_fin = fechaVenceFin;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public void setTipoCobro(String tipo_cobro) {
		this.tipo_cobro = tipo_cobro;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getIc_epo() {
		return ic_epo;
	}

	public String getIc_pyme() {
		return ic_pyme;
	}

	public String getIc_if() {
		return ic_if;
	}

	public List getConditions() {
		return conditions;
	}

}

