package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.CQueryHelperRegExtJS;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsPagosCreditosIfDist   implements IQueryGenerator,IQueryGeneratorRegExtJS {
	public ConsPagosCreditosIfDist(){}
	
	private List conditions;
	private String ic_epo;
	private String ctCredito;
	private String icDist;
	private String numCred;	
	private String fechaOper1;
	private String fechaOper2;
	private String monto1;
	private String monto2;	
	private String fechaVenc1;
	private String fechaVenc2;
	private String tipoCoInt;
	private String estatus;
	private String fechaPago1;
	private String fechaPago2;
	private String sesIf;
	private static final Log log = ServiceLocator.getInstance().getLog(ConsPagosCreditosIfDist.class);

	//Metodos IQueryGeneratorRegExtJS
		public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
			String nombreArchivo = "";
			if("PDF".equals(tipo)){
			try{
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
				
				String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual		= fechaActual.substring(0,2);
				String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual		= fechaActual.substring(6,10);
				String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
				
				//pdfDoc.setTable(16, 100);
				List lEncabezados = new ArrayList();
				
				lEncabezados.add("N�mero de Documento Final");
				lEncabezados.add("EPO");
				lEncabezados.add("Distribuidor");
				lEncabezados.add("Tipo de cr�dito");
				lEncabezados.add("Resp. pago de inter�s");
				lEncabezados.add("Tipo de cobranza");
				lEncabezados.add("Fecha de operaci�n");
				lEncabezados.add("Moneda");
				lEncabezados.add("Monto");
				lEncabezados.add("Referencia tasa");
				lEncabezados.add("Tasa de inter�s");
				lEncabezados.add("Plazo");
				lEncabezados.add("Fecha de vencimiento");
				lEncabezados.add("Monto inter�s");
				lEncabezados.add("Tipo de cobro inter�s");
				lEncabezados.add("Estatus");
				lEncabezados.add("N�m. pago");
				lEncabezados.add("Fecha de cambio de estatus");
				pdfDoc.setTable(lEncabezados, "formasmenB", 100);
				
				
				while (reg.next()){
					String numeroDocumento		= (reg.getString("NUM_CREDITO") == null) ? "" : reg.getString("NUM_CREDITO");
					String epo						= (reg.getString("EPO") == null) ? "" : reg.getString("EPO");
					String dist						= (reg.getString("DISTRIBUIDOR") == null) ? "" : reg.getString("DISTRIBUIDOR");
					String tipoCredito			= (reg.getString("TIPO_CREDITO") == null) ? "" : reg.getString("TIPO_CREDITO");
					String respInt					= (reg.getString("RESPONSABLE_INTERES") == null) ? "" : reg.getString("RESPONSABLE_INTERES");
					String tipoCob					= (reg.getString("TIPO_COBRANZA") == null) ? "" : reg.getString("TIPO_COBRANZA");
					String fOpera					= (reg.getString("FECHA_OPERACION") == null) ? "" : reg.getString("FECHA_OPERACION");
					String moneda					= (reg.getString("TIPO_MONEDA") == null) ? "" : reg.getString("TIPO_MONEDA");
					String monto					= (reg.getString("MONTO") == 	null) ? "" : reg.getString("MONTO");
					String refTasa					= (reg.getString("REF_TASA") == null) ? "" : reg.getString("REF_TASA");
					String tasaInt					= (reg.getString("TASA_INTERES") == null) ? "" : reg.getString("TASA_INTERES");
					String plazo					= (reg.getString("PLAZO") == null) ? "" : reg.getString("PLAZO");
					String fVenc					= (reg.getString("FCH_VENCIMIENTO") == 	null) ? "" : reg.getString("FCH_VENCIMIENTO");
					String montoInteres			= (reg.getString("MONTO_INTERES") == null) ? "" : reg.getString("MONTO_INTERES");
					String cobroInt				= (reg.getString("TIPO_COBRO_INTE") == null) ? "" : reg.getString("TIPO_COBRO_INTE");
					String estatus					= (reg.getString("ESTATUS_SOLI") == null) ? "" : reg.getString("ESTATUS_SOLI");
					String numPago					= (reg.getString("NUM_PAGO") == null) ? "" : reg.getString("NUM_PAGO");
					String fechaEstatus			= (reg.getString("FECHA_PAGO") == null) ? "" : reg.getString("FECHA_PAGO");
					
					
					pdfDoc.setCell(numeroDocumento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(epo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(dist,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(tipoCredito,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(respInt,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(tipoCob,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fOpera,"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(refTasa,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(tasaInt,2)+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(plazo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fVenc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(montoInteres,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cobroInt,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(estatus,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(numPago,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaEstatus,"formas",ComunesPDF.CENTER);
				}
				//pdfDoc.addTable();
				
				
				CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS();//toma el valor de sesi�n
				Registros regi = queryHelper.getResultCount(request);
				pdfDoc.setCell("TOTALES ","celda01rep",ComunesPDF.LEFT,18);
				pdfDoc.setCell("MONEDA ","celda01rep",ComunesPDF.LEFT,4);
				pdfDoc.setCell("TOTAL REGISTROS","celda01rep",ComunesPDF.CENTER,4);
				pdfDoc.setCell("TOTAL MONTO","celda01rep",ComunesPDF.CENTER,5);
				pdfDoc.setCell("TOTAL MONTO INTERES","celda01rep",ComunesPDF.CENTER,5);

				while(regi.next()){
						pdfDoc.setCell("TOTAL "+regi.getString(2),"formas",ComunesPDF.LEFT,4);
						pdfDoc.setCell(regi.getString(3),"formas",ComunesPDF.CENTER,4);
						pdfDoc.setCell("$"+regi.getString(4),"formas",ComunesPDF.CENTER,5);
						pdfDoc.setCell("$"+regi.getString(5),"formas",ComunesPDF.CENTER,5);
				}
			
				pdfDoc.addTable();
				pdfDoc.endDocument();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo",e);
			}finally{
				try{
				}catch(Exception e){}
			}
		}	
			return  nombreArchivo;
		}
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
		
		log.info("crearCustomFile (E)");
		String linea = "";
		String nombreArchivo = "";
		String espacio = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;

		try {
			if(tipo.equals("CSV")){

				linea = "No. Docto Final,EPO,Distribuidor,Tipo de Cr�dito,Resp. Pago de Inter�s,"+
				"Tipo de Cobranza,Fecha de Operaci�n,Moneda,Monto,Ref. Tasa"
				+",Tasa Inter�s,Plazo,Fecha de Vencimiento,Monto Inter�s,Tipo Cobro Inter�s,Estatus,N�m. pago,Fecha de cambio de estatus\n";
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
				buffer = new BufferedWriter(writer);

				buffer.write(linea);

				while (rs.next()) {
					String numeroDocumento = (rs.getString("NUM_CREDITO")         == null) ? "" : rs.getString("NUM_CREDITO");
					String epo             = (rs.getString("EPO")                 == null) ? "" : rs.getString("EPO");
					String dist            = (rs.getString("DISTRIBUIDOR")        == null) ? "" : rs.getString("DISTRIBUIDOR");
					String tipoCredito     = (rs.getString("TIPO_CREDITO")        == null) ? "" : rs.getString("TIPO_CREDITO");
					String respInt         = (rs.getString("RESPONSABLE_INTERES") == null) ? "" : rs.getString("RESPONSABLE_INTERES");
					String tipoCob         = (rs.getString("TIPO_COBRANZA")       == null) ? "" : rs.getString("TIPO_COBRANZA");
					String fOpera          = (rs.getString("FECHA_OPERACION")     == null) ? "" : rs.getString("FECHA_OPERACION");
					String moneda          = (rs.getString("TIPO_MONEDA")         == null) ? "" : rs.getString("TIPO_MONEDA");
					String monto           = (rs.getString("MONTO")               == null) ? "" : rs.getString("MONTO");
					String refTasa         = (rs.getString("REF_TASA")            == null) ? "" : rs.getString("REF_TASA");
					String tasaInt         = (rs.getString("TASA_INTERES")        == null) ? "" : rs.getString("TASA_INTERES");
					String plazo           = (rs.getString("PLAZO")               == null) ? "" : rs.getString("PLAZO");
					String fVenc           = (rs.getString("FCH_VENCIMIENTO")     == null) ? "" : rs.getString("FCH_VENCIMIENTO");
					String montoInteres    = (rs.getString("MONTO_INTERES")       == null) ? "" : rs.getString("MONTO_INTERES");
					String cobroInt        = (rs.getString("TIPO_COBRO_INTE")     == null) ? "" : rs.getString("TIPO_COBRO_INTE");
					String estatus         = (rs.getString("ESTATUS_SOLI")        == null) ? "" : rs.getString("ESTATUS_SOLI");
					String numPago         = (rs.getString("NUM_PAGO")            == null) ? "" : rs.getString("NUM_PAGO");
					String fechaEstatus    = (rs.getString("FECHA_PAGO")          == null) ? "" : rs.getString("FECHA_PAGO");

					linea = numeroDocumento.replace(',',' ') + ", " + 
								epo.replace(',',' ') + ", " +
								dist.replace(',',' ') + ", " +
								tipoCredito.replace(',',' ') + ", " +
								respInt.replace(',',' ') + ", " +
								tipoCob.replace(',',' ') + ", " +
								fOpera.replace(',',' ') + ", " +
								moneda.replace(',',' ') + ", " +
								monto.replace(',',' ') + ", " +
								refTasa.replace(',',' ') + ", " +
								tasaInt.replace(',',' ') + ", " +
								plazo.replace(',',' ') + ", " +
								fVenc.replace(',',' ') + ", " +
								montoInteres.replace(',',' ') + ", " +
								cobroInt.replace(',',' ') + ", " +
								estatus.replace(',',' ') + ", " +
								numPago.replace(',',' ') + ", " +
								fechaEstatus.replace(',',' ') + "\n";

					buffer.write(linea);
				}
				CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS();//toma el valor de sesi�n
				Registros regi = queryHelper.getResultCount(request);
				linea="TOTALES\n";
				while(regi.next()){
					linea+="MONEDA,TOTAL REGISTROS,TOTAL MONTO,TOTAL MONTO INTERES\n";
					linea+=regi.getString(2)+","+regi.getString(3)+","+"$"+regi.getString(4)+","+regi.getString(5)+"\n";
				}
				buffer.write(linea);
				buffer.close();

			} else if(tipo.equals("PDF")){

				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);

				String meses[]     = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

				pdfDoc.setLTable(18,100);
				pdfDoc.setLCell("N�mero de Documento Final","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Distribuidor","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de cr�dito","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Resp. pago de inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de cobranza","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de operaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Referencia tasa","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tasa de inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de cobro inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("N�m. pago","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de cambio de estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setLHeaders();

				while (rs.next()){
					String numeroDocumento = (rs.getString("NUM_CREDITO")         == null) ? "" : rs.getString("NUM_CREDITO");
					String epo             = (rs.getString("EPO")                 == null) ? "" : rs.getString("EPO");
					String dist            = (rs.getString("DISTRIBUIDOR")        == null) ? "" : rs.getString("DISTRIBUIDOR");
					String tipoCredito     = (rs.getString("TIPO_CREDITO")        == null) ? "" : rs.getString("TIPO_CREDITO");
					String respInt         = (rs.getString("RESPONSABLE_INTERES") == null) ? "" : rs.getString("RESPONSABLE_INTERES");
					String tipoCob         = (rs.getString("TIPO_COBRANZA")       == null) ? "" : rs.getString("TIPO_COBRANZA");
					String fOpera          = (rs.getString("FECHA_OPERACION")     == null) ? "" : rs.getString("FECHA_OPERACION");
					String moneda          = (rs.getString("TIPO_MONEDA")         == null) ? "" : rs.getString("TIPO_MONEDA");
					String monto           = (rs.getString("MONTO")               == null) ? "" : rs.getString("MONTO");
					String refTasa         = (rs.getString("REF_TASA")            == null) ? "" : rs.getString("REF_TASA");
					String tasaInt         = (rs.getString("TASA_INTERES")        == null) ? "" : rs.getString("TASA_INTERES");
					String plazo           = (rs.getString("PLAZO")               == null) ? "" : rs.getString("PLAZO");
					String fVenc           = (rs.getString("FCH_VENCIMIENTO")     == null) ? "" : rs.getString("FCH_VENCIMIENTO");
					String montoInteres    = (rs.getString("MONTO_INTERES")       == null) ? "" : rs.getString("MONTO_INTERES");
					String cobroInt        = (rs.getString("TIPO_COBRO_INTE")     == null) ? "" : rs.getString("TIPO_COBRO_INTE");
					String estatus         = (rs.getString("ESTATUS_SOLI")        == null) ? "" : rs.getString("ESTATUS_SOLI");
					String numPago         = (rs.getString("NUM_PAGO")            == null) ? "" : rs.getString("NUM_PAGO");
					String fechaEstatus    = (rs.getString("FECHA_PAGO")          == null) ? "" : rs.getString("FECHA_PAGO");

					pdfDoc.setLCell(numeroDocumento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(epo,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(dist,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipoCredito,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(respInt,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipoCob,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fOpera,"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(refTasa,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(Comunes.formatoDecimal(tasaInt,2)+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(plazo,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fVenc,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(montoInteres,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(cobroInt,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(estatus,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(numPago,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaEstatus,"formas",ComunesPDF.CENTER);
				}
				//pdfDoc.addTable();

				CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS();//toma el valor de sesi�n
				Registros regi = queryHelper.getResultCount(request);
				pdfDoc.setLCell("TOTALES ","celda01rep",ComunesPDF.LEFT,18);
				pdfDoc.setLCell("MONEDA ","celda01rep",ComunesPDF.LEFT,4);
				pdfDoc.setLCell("TOTAL REGISTROS","celda01rep",ComunesPDF.CENTER,4);
				pdfDoc.setLCell("TOTAL MONTO","celda01rep",ComunesPDF.CENTER,5);
				pdfDoc.setLCell("TOTAL MONTO INTERES","celda01rep",ComunesPDF.CENTER,5);

				while(regi.next()){
					pdfDoc.setLCell("TOTAL "+regi.getString(2),"formas",ComunesPDF.LEFT,4);
					pdfDoc.setLCell(regi.getString(3),"formas",ComunesPDF.CENTER,4);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(regi.getString(4),2),"formas",ComunesPDF.CENTER,5);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(regi.getString(5),2),"formas",ComunesPDF.CENTER,5);
				}

				pdfDoc.addLTable();
				pdfDoc.endDocument();

			}

		}catch (Throwable e) {
			throw new AppException("Error al generar el archivo",e);
		} finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
		log.info("crearCustomFile (S)");
		return nombreArchivo;
		}

		public String getDocumentQueryFile() {
			String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		String qryAux = "";	
		StringBuffer qryDM 		= new StringBuffer();
		StringBuffer qryCCC 	= new StringBuffer();
		StringBuffer condicion 	= new StringBuffer();
		try{
		conditions=new ArrayList();
		conditions.add(sesIf);

		if(!"".equals(icDist)){
			condicion.append(" and docto.ic_pyme = ? ");
			conditions.add(icDist);
		}
		
		if(!"".equals(ic_epo)){
			condicion.append(" and docto.ic_epo = ? ");
			conditions.add(ic_epo);
		}
		if(!"".equals(numCred)){
			condicion.append(" and docto.ic_documento = ? ");
			conditions.add(numCred);
		}
		if (!fechaOper1.equals("")){
			condicion.append(" AND soli.df_operacion >= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaOper1);
		}
		if (!fechaOper2.equals("")){
			condicion.append(" AND soli.df_operacion <= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaOper2);
		}
		if ( Double.parseDouble(monto1) > 0 )		{
			condicion.append(" AND docto.fn_monto >= ? ");
			conditions.add(monto1);
		}
		if ( Double.parseDouble(monto2) > 0 ){		
			condicion.append(" AND docto.fn_monto <= ? ");
			conditions.add(monto2);
		}
		if (!fechaVenc1.equals("")){
			condicion.append(" AND docto.df_fecha_venc_credito >= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaVenc1);
		}
		if (!fechaVenc2.equals("")){
			condicion.append(" AND docto.df_fecha_venc_credito <= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaVenc2);
		}
		
		if (!tipoCoInt.equals("")){
			condicion.append(" AND lin_cred.ic_tipo_cobro_interes = ? ");
			conditions.add(tipoCoInt);
		}
		if (!estatus.equals("") ){
			condicion.append(" AND docto.ic_estatus_docto = ? ");
			conditions.add(estatus);
		}
		if (!fechaPago1.equals("") ){	
			condicion.append(" AND camb_estatus.dc_fecha_cambio >= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaPago1);
		}
		if (!fechaPago2.equals("") ){
			condicion.append(" AND camb_estatus.dc_fecha_cambio <= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaPago2);
		}
		qryDM.append(
					" SELECT /*+use_nl(docto,doct-o_sel,soli,ca3,camb_estatus,lin_cred,pyme,epo,prod_epo,prod_nafi,moneda,tci,ed,ct) "   +
					"     index(docto cp_dis_documento_pk)"   +
					"     index(lin_cred cp_dis_linea_credito_dm_pk)  "   +
					"     index(tci cp_comcat_tipo_cobro_inter_pk) */"   +
					"        docto.ic_documento     AS num_credito,"   +
					"       epo.cg_razon_social    AS epo,"   +
					"       pyme.cg_razon_social   AS distribuidor,"   +
					"       'Modalidad 1 (Riesgo Empresa de Primer Orden )'  AS tipo_credito,"   +
					"       DECODE (NVL (prod_epo.cg_responsable_interes, prod_nafin.cg_responsable_interes),'E', 'EPO','D', 'Distribuidor','') AS responsable_interes,"   +
					"       DECODE (NVL (prod_epo.cg_tipo_cobranza, prod_nafin.cg_tipo_cobranza),'D', 'Delegada','N', 'No Delegada','') AS tipo_cobranza,"   +
					"       TO_CHAR(soli.df_operacion , 'DD/MM/YYYY')  AS fecha_operacion,"   +
					"       moneda.cd_nombre 	   AS tipo_moneda,"   +
					"       docto.fn_monto AS monto,"   +
					"       DECODE(NVL(prod_epo.cg_tipo_conversion, prod_nafin.cg_tipo_conversion), 'P', 'Dolar-Peso', 'Sin Conversion')  AS tipo_conversion,"   +
					"       DECODE(docto.fn_tipo_cambio, 1, ' ', docto.fn_tipo_cambio) AS tipo_cambio,"   +
					"       docto.fn_monto * docto.fn_tipo_cambio AS monto_valuado,"   +
					"       NVL(ct.cd_nombre, 'No Autorizado') AS ref_tasa,"   +
					"       DECODE(docto_sel.cg_rel_mat, '+', docto_sel.fn_puntos + docto_sel.fn_valor_tasa, '-',"   +
					"       docto_sel.fn_valor_tasa - docto_sel.fn_puntos, '*', docto_sel.fn_puntos * docto_sel.fn_valor_tasa, '/',"   +
					"       docto_sel.fn_valor_tasa / docto_sel.fn_puntos)  AS tasa_interes,"   +
					"       docto.ig_plazo_credito AS plazo,"   +
					"       TO_CHAR(docto.df_fecha_venc_credito, 'DD/MM/YYYY') AS fch_vencimiento,"   +
					"       docto_sel.fn_importe_interes AS monto_interes,"   +
					"       tci.cd_descripcion AS tipo_cobro_inte,"   +
					"       ed.cd_descripcion  AS estatus_soli,"   +
					"       TO_CHAR(camb_estatus.dc_fecha_cambio, 'DD/MM/YYYY') AS fecha_pago,"   +
					"       camb_estatus.cg_numero_pago  AS num_pago,"   +
					"       moneda.ic_moneda"   +
					"  FROM dis_documento          docto "   +
					"		,dis_docto_seleccionado docto_sel"   +
					"		,dis_solicitud          soli"   +
					"		,com_acuse3			 		ca3"   +
					"		,dis_cambio_estatus     camb_estatus"   +
					"		,dis_linea_credito_dm   lin_cred"   +
					"		,comcat_pyme            pyme"   +
					"		,comcat_epo             epo"   +
					"		,comrel_producto_epo    prod_epo"   +
					"		,comcat_producto_nafin  prod_nafin"   +
					"		,comcat_moneda          moneda"   +
					"		,comcat_tipo_cobro_interes tci"   +
					"		,comcat_estatus_docto ed"   +
					"		,comcat_tasa ct"   +
					"  WHERE docto.ic_documento = docto_sel.ic_documento"   +
					" 	AND docto.ic_epo = epo.ic_epo"   +
					" 	AND docto.ic_pyme = pyme.ic_pyme"   +
					" 	AND docto.ic_epo  = prod_epo.ic_epo"   +
					" 	AND docto.ic_documento = camb_estatus.ic_documento"   +
					" 	AND docto.ic_linea_credito_dm = lin_cred.ic_linea_credito_dm"   +
					" 	AND docto_sel.ic_documento = soli.ic_documento"   +
					" 	AND docto.ic_estatus_docto = ed.ic_estatus_docto"   +
					" 	AND soli.cc_acuse = ca3.cc_acuse"   +
					" 	AND camb_estatus.dc_fecha_cambio IN(SELECT MAX(dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_documento = docto.ic_documento)"   +
					" 	AND lin_cred.ic_moneda = moneda.ic_moneda"   +
					" 	AND lin_cred.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes"   +
					" 	AND prod_epo.ic_producto_nafin = prod_nafin.ic_producto_nafin"   +
					" 	AND soli.ic_tasaif = ct.ic_tasa "   +
					" 	AND prod_nafin.ic_producto_nafin = 4"   +
					"	AND lin_cred.ic_if = ? "+
					" 	AND epo.cs_habilitado = 'S' " +
					condicion.toString()  );

		qryCCC.append(
					" SELECT /*+use_nl(docto,doct-o_sel,soli,ca3,camb_estatus,lin_cred,pyme,epo,prod_epo,prod_nafi,moneda,tci,ed,ct) "   +
					"     index(docto cp_dis_documento_pk)"   +
					"     index(lin_cred CP_COM_LINEA_CREDITO_PK)  "   +
					"     index(tci cp_comcat_tipo_cobro_inter_pk) */"   +
					"       docto.ic_documento     AS num_credito,"   +
					"       epo.cg_razon_social    AS epo,"   +
					"       pyme.cg_razon_social   AS distribuidor,"   +
					"       'Modalidad 2 (Riesgo Distribuidor)'  AS tipo_credito,"   +
					"       DECODE (NVL (prod_epo.cg_responsable_interes, prod_nafin.cg_responsable_interes),'E', 'EPO','D', 'Distribuidor','') AS responsable_interes,"   +
					"       DECODE (NVL (prod_epo.cg_tipo_cobranza, prod_nafin.cg_tipo_cobranza),'D', 'Delegada','N', 'No Delegada','') AS tipo_cobranza,"   +
					"       TO_CHAR(soli.df_operacion , 'DD/MM/YYYY')  AS fecha_operacion,"   +
					"       moneda.cd_nombre 	   AS tipo_moneda,"   +
					"       docto.fn_monto AS monto,"   +
					"       DECODE(NVL(prod_epo.cg_tipo_conversion, prod_nafin.cg_tipo_conversion), 'P', 'Dolar-Peso', 'Sin Conversion')  AS tipo_conversion,"   +
					"       DECODE(docto.fn_tipo_cambio, 1, ' ', docto.fn_tipo_cambio) AS tipo_cambio,"   +
					"       docto.fn_monto * docto.fn_tipo_cambio AS monto_valuado,"   +
					"       NVL(ct.cd_nombre, 'No Autorizado') AS ref_tasa,"   +
					"       DECODE(docto_sel.cg_rel_mat, '+', docto_sel.fn_puntos + docto_sel.fn_valor_tasa, '-',"   +
					"       docto_sel.fn_valor_tasa - docto_sel.fn_puntos, '*', docto_sel.fn_puntos * docto_sel.fn_valor_tasa, '/',"   +
					"       docto_sel.fn_valor_tasa / docto_sel.fn_puntos)  AS tasa_interes,"   +
					"       docto.ig_plazo_credito AS plazo,"   +
					"       TO_CHAR(docto.df_fecha_venc_credito, 'DD/MM/YYYY') AS fch_vencimiento,"   +
					"       docto_sel.fn_importe_interes AS monto_interes,"   +
					"       tci.cd_descripcion AS tipo_cobro_inte,"   +
					"       ed.cd_descripcion  AS estatus_soli,"   +
					"       TO_CHAR(camb_estatus.dc_fecha_cambio, 'DD/MM/YYYY') AS fecha_pago,"   +
					"       camb_estatus.cg_numero_pago  AS num_pago,"   +
					"       moneda.ic_moneda"   +
					"  FROM dis_documento           docto "   +
					"		,dis_docto_seleccionado docto_sel"   +
					"		,dis_solicitud          soli"   +
					"		,com_acuse3			 	ca3"   +
					"		,dis_cambio_estatus     camb_estatus"   +
					"		,com_linea_credito	   	lin_cred"   +
					"		,comcat_pyme            pyme"   +
					"		,comcat_epo             epo"   +
					"		,comrel_producto_epo    prod_epo"   +
					"		,comcat_producto_nafin  prod_nafin"   +
					"		,comcat_moneda          moneda"   +
					"		,comcat_tipo_cobro_interes tci"   +
					"		,comcat_estatus_docto ed"   +
					"		,comcat_tasa ct"   +
					"  WHERE docto.ic_documento = docto_sel.ic_documento (+)"   +
					" 	AND docto.ic_epo = epo.ic_epo"   +
					" 	AND docto.ic_pyme = pyme.ic_pyme"   +
					" 	AND docto.ic_epo  = prod_epo.ic_epo"   +
					" 	AND docto.ic_documento = camb_estatus.ic_documento"   +
					" 	AND docto.ic_linea_credito = lin_cred.ic_linea_credito (+)"   +
					" 	AND docto_sel.ic_documento = soli.ic_documento (+)"   +
					" 	AND docto.ic_estatus_docto = ed.ic_estatus_docto"   +
					" 	AND soli.cc_acuse = ca3.cc_acuse (+)"   +
					" 	AND camb_estatus.dc_fecha_cambio IN(SELECT MAX(dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_documento = docto.ic_documento)"   +
					" 	AND lin_cred.ic_moneda = moneda.ic_moneda"   +
					" 	AND lin_cred.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes"   +
					" 	AND prod_epo.ic_producto_nafin = prod_nafin.ic_producto_nafin"   +
					" 	AND soli.ic_tasaif = ct.ic_tasa "   +
					" 	AND prod_nafin.ic_producto_nafin = 4"   +
					"	AND lin_cred.ic_if = ? "+
					" 	AND epo.cs_habilitado = 'S' " +
					condicion.toString()  );

			qryAux = "";	
			if("D".equals(ctCredito))
				qryAux = qryDM.toString();
			else if("C".equals(ctCredito))
				qryAux = qryCCC.toString();
			else{
				qryAux = qryDM.toString()+ " UNION ALL "+qryCCC.toString();
				conditions.addAll(conditions);
			}

		}catch(Exception e){
			System.out.println("ConsPagosCreditosIfDist::getDocumentQueryFileException "+e);
		}
		log.info("Sentencia: " + qryAux.toString());
		log.info("Conditions: " + conditions.toString());
		return qryAux.toString();
		}
			
	 	public String getAggregateCalculationQuery() {
			String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		String qryAux = "";	
		StringBuffer qryDM 		= new StringBuffer();
		StringBuffer qryCCC		= new StringBuffer();
		StringBuffer condicion  = new StringBuffer();
		StringBuffer auxiliar  = new StringBuffer();
		auxiliar.append("select ic_moneda, cd_nombre, sum(registros), sum(suma_recibir), sum(suma_interes) from (");
		try {

		conditions=new ArrayList();
		conditions.add(sesIf);

		if(!"".equals(icDist)){
			condicion.append(" and docto.ic_pyme = ? ");
			conditions.add(icDist);
		}
		
		if(!"".equals(ic_epo)){
			condicion.append(" and docto.ic_epo = ? ");
			conditions.add(ic_epo);
		}
		if(!"".equals(numCred)){
			condicion.append(" and docto.ic_documento = ? ");
			conditions.add(numCred);
		}
		if (!fechaOper1.equals("")){
			condicion.append(" AND soli.df_operacion >= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaOper1);
		}
		if (!fechaOper2.equals("")){
			condicion.append(" AND soli.df_operacion <= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaOper2);
		}
		if ( Double.parseDouble(monto1) > 0 )		{
			condicion.append(" AND docto.fn_monto >= ? ");
			conditions.add(monto1);
		}
		if ( Double.parseDouble(monto2) > 0 ){		
			condicion.append(" AND docto.fn_monto <= ? ");
			conditions.add(monto2);
		}
		if (!fechaVenc1.equals("")){
			condicion.append(" AND docto.df_fecha_venc_credito >= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaVenc1);
		}
		if (!fechaVenc2.equals("")){
			condicion.append(" AND docto.df_fecha_venc_credito <= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaVenc2);
		}
		
		if (!tipoCoInt.equals("")){
			condicion.append(" AND lin_cred.ic_tipo_cobro_interes = ? ");
			conditions.add(tipoCoInt);
		}
		if (!estatus.equals("") ){
			condicion.append(" AND docto.ic_estatus_docto = ? ");
			conditions.add(estatus);
		}
		if (!fechaPago1.equals("") ){	
			condicion.append(" AND camb_estatus.dc_fecha_cambio >= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaPago1);
		}
		if (!fechaPago2.equals("") ){
			condicion.append(" AND camb_estatus.dc_fecha_cambio <= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaPago2);
		}

		
		
		qryDM.append(
					"  SELECT /*+ use_nl(docto,docto_sel,soli,ca3,epo,camb_estatus,lin_cred,prod_epo,moneda)"   +
					"         index(docto cp_dis_documento_pk)"   +
					"         index(lin_cred cp_dis_linea_credito_dm_pk)*/"   +
					"        moneda.ic_moneda"   +
					" 	   	,moneda.cd_nombre"   +
					" 	   	,COUNT(1) AS registros"   +
					"        ,SUM(docto.fn_monto) as SUMA_RECIBIR"   +
					"        ,SUM(docto_sel.fn_importe_interes) as SUMA_INTERES"   +
					"   FROM dis_documento          docto"   +
					" 		,dis_docto_seleccionado docto_sel"   +
					" 		,dis_solicitud          soli"   +
					" 		,com_acuse3			 		ca3"   +
					" 		, comcat_epo epo " +
					" 		,dis_cambio_estatus     camb_estatus"   +
					" 		,dis_linea_credito_dm   lin_cred"   +
					" 		,comrel_producto_epo    prod_epo"   +
					" 		,comcat_moneda          moneda"   +
					"   WHERE docto.ic_documento = docto_sel.ic_documento"   +
					"  	AND docto.ic_epo  = epo.ic_epo"   +
					"  	AND docto.ic_epo  = prod_epo.ic_epo"   +
					"  	AND docto.ic_documento = camb_estatus.ic_documento"   +
					"  	AND docto.ic_linea_credito_dm = lin_cred.ic_linea_credito_dm"   +
					"  	AND docto_sel.ic_documento = soli.ic_documento"   +
					" 	AND docto.ic_producto_nafin = prod_epo.ic_producto_nafin"   +
					"  	AND soli.cc_acuse = ca3.cc_acuse"   +
					"  	AND camb_estatus.dc_fecha_cambio IN(SELECT MAX(dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_documento = docto.ic_documento)"   +
					"  	AND lin_cred.ic_moneda = moneda.ic_moneda"   +
					"	AND lin_cred.ic_if = ? "+
					" 		AND epo.cs_habilitado = 'S' " +
			  		condicion.toString()+
					"  GROUP BY moneda.ic_moneda,moneda.cd_nombre");

		qryCCC.append(
					" SELECT /*+ use_nl(docto,docto_sel,soli,ca3,epo,camb_estatus,lin_cred,prod_epo,moneda)"   +
					"         index(docto cp_dis_documento_pk)"   +
					"         index(lin_cred cp_com_linea_credito_pk)*/"   +
					"        moneda.ic_moneda"   +
					" 	   	,moneda.cd_nombre"   +
					" 	   	,COUNT(1) as registros"   +
					"        ,SUM(docto.fn_monto) AS SUMA_RECIBIR "   +
					"        ,SUM(docto_sel.fn_importe_interes) AS SUMA_INTERES "   +
					"   FROM dis_documento          docto"   +
					" 		,dis_docto_seleccionado docto_sel"   +
					" 		,dis_solicitud          soli"   +
					" 		,com_acuse3			 	ca3"   +
					" 		, comcat_epo epo " +
					" 		,dis_cambio_estatus     camb_estatus"   +
					" 		,com_linea_credito   	lin_cred"   +
					" 		,comrel_producto_epo    prod_epo"   +
					" 		,comcat_moneda          moneda"   +
					"   WHERE docto.ic_documento = docto_sel.ic_documento (+)"   +
					"  	AND docto.ic_epo  = epo.ic_epo"   +
					"  	AND docto.ic_epo  = prod_epo.ic_epo"   +
					"  	AND docto.ic_documento = camb_estatus.ic_documento"   +
					"  	AND docto.ic_linea_credito = lin_cred.ic_linea_credito (+)"   +
					"  	AND docto_sel.ic_documento = soli.ic_documento (+)"   +
					" 	AND docto.ic_producto_nafin = prod_epo.ic_producto_nafin"   +
					"  	AND soli.cc_acuse = ca3.cc_acuse (+)"   +
					"  	AND camb_estatus.dc_fecha_cambio IN(SELECT MAX(dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_documento = docto.ic_documento)"   +
					"  	AND lin_cred.ic_moneda = moneda.ic_moneda"   +
					"	AND lin_cred.ic_if = ? "+
					" 		AND epo.cs_habilitado = 'S' " +
			  		condicion.toString()+
					"  GROUP BY moneda.ic_moneda,moneda.cd_nombre" );

			if("D".equals(ctCredito))
				qryAux = qryDM.toString();
			else if("C".equals(ctCredito))
				qryAux = qryCCC.toString();
			else{
				qryAux ="select ic_moneda, cd_nombre, sum(registros) as registros, sum(suma_recibir) as suma_recibir, sum(suma_interes) as suma_interes from ("+
				qryDM.toString()+ " UNION ALL "+qryCCC.toString()+" ) GROUP BY ic_moneda, cd_nombre";
				conditions.addAll(conditions);
			}
			log.debug("getAggregateCalculationQuery "+qryAux.toString());
			log.debug(" getAggregateCalculationQuery"+conditions.toString());
		
		}catch(Exception e){
			System.out.println("ConsPagosCreditosIfDist::getAggregateCalculationQuery "+e);
		}
		return qryAux.toString();
		}
	  public String getDocumentSummaryQueryForIds(List pageIds) {
			int i=0;
		StringBuffer qryDM		= new StringBuffer();
    	StringBuffer qryCCC		= new StringBuffer();
    	StringBuffer condicion	= new StringBuffer();
    	StringBuffer clavesDocumentos = new StringBuffer();

    
		condicion.append(" AND camb_estatus.ic_documento||'-'||to_char(camb_estatus.dc_fecha_cambio,'ddmmyyyyhhmiss') in (");
		conditions 		= new ArrayList();
		conditions.add(sesIf);
		for(int j = 0; j < pageIds.size(); j++){
			List lItem = (ArrayList)pageIds.get(j);
				clavesDocumentos.append("?,");
				conditions.add(new String(lItem.get(0).toString()));      
			}
			clavesDocumentos.deleteCharAt(clavesDocumentos.length()-1);

		qryDM.append(
					" SELECT /*+use_nl(docto,doct-o_sel,soli,ca3,camb_estatus,lin_cred,pyme,epo,prod_epo,prod_nafi,moneda,tci,ed,ct) "   +
					"     index(docto cp_dis_documento_pk)"   +
					"     index(lin_cred cp_dis_linea_credito_dm_pk)  "   +
					"     index(tci cp_comcat_tipo_cobro_inter_pk) */"   +
					"       docto.ic_documento     AS num_credito,"   +
					"       epo.cg_razon_social    AS epo,"   +
					"       pyme.cg_razon_social   AS distribuidor,"   +
					"       'Modalidad 1 (Riesgo Empresa de Primer Orden )'  AS tipo_credito,"   +
					"       DECODE (NVL (prod_epo.cg_responsable_interes, prod_nafin.cg_responsable_interes),'E', 'EPO','D', 'Distribuidor','') AS responsable_interes,"   +
					"       DECODE (NVL (prod_epo.cg_tipo_cobranza, prod_nafin.cg_tipo_cobranza),'D', 'Delegada','N', 'No Delegada','') AS tipo_cobranza,"   +
					"       TO_CHAR(soli.df_operacion , 'DD/MM/YYYY')  AS fecha_operacion,"   +
					"       moneda.cd_nombre 	   AS tipo_moneda,"   +
					"       docto.fn_monto AS monto,"   +
					"       DECODE(NVL(prod_epo.cg_tipo_conversion, prod_nafin.cg_tipo_conversion), 'P', 'Dolar-Peso', 'Sin Conversion')  AS tipo_conversion,"   +
					"       DECODE(docto.fn_tipo_cambio, 1, ' ', docto.fn_tipo_cambio) AS tipo_cambio,"   +
					"       docto.fn_monto * docto.fn_tipo_cambio AS monto_valuado,"   +
					"       NVL(ct.cd_nombre, 'No Autorizado') AS ref_tasa,"   +
					"       DECODE(docto_sel.cg_rel_mat, '+', docto_sel.fn_puntos + docto_sel.fn_valor_tasa, '-',"   +
					"       docto_sel.fn_valor_tasa - docto_sel.fn_puntos, '*', docto_sel.fn_puntos * docto_sel.fn_valor_tasa, '/',"   +
					"       docto_sel.fn_valor_tasa / docto_sel.fn_puntos)  AS tasa_interes,"   +
					"       docto.ig_plazo_credito AS plazo,"   +
					"       TO_CHAR(docto.df_fecha_venc_credito, 'DD/MM/YYYY') AS fch_vencimiento,"   +
					"       docto_sel.fn_importe_interes AS monto_interes,"   +
					"       tci.cd_descripcion AS tipo_cobro_inte,"   +
					"       ed.cd_descripcion  AS estatus_soli,"   +
					"       TO_CHAR(camb_estatus.dc_fecha_cambio, 'DD/MM/YYYY') AS fecha_pago,"   +
					"       camb_estatus.cg_numero_pago  AS num_pago,"   +
					"       moneda.ic_moneda"   +
					"  FROM dis_documento          docto "   +
					"		,dis_docto_seleccionado docto_sel"   +
					"		,dis_solicitud          soli"   +
					"		,com_acuse3			 		ca3"   +
					"		,dis_cambio_estatus     camb_estatus"   +
					"		,dis_linea_credito_dm   lin_cred"   +
					"		,comcat_pyme            pyme"   +
					"		,comcat_epo             epo"   +
					"		,comrel_producto_epo    prod_epo"   +
					"		,comcat_producto_nafin  prod_nafin"   +
					"		,comcat_moneda          moneda"   +
					"		,comcat_tipo_cobro_interes tci"   +
					"		,comcat_estatus_docto ed"   +
					"		,comcat_tasa ct"   +
					"  WHERE docto.ic_documento = docto_sel.ic_documento"   +
					" 	AND docto.ic_epo = epo.ic_epo"   +
					" 	AND docto.ic_pyme = pyme.ic_pyme"   +
					" 	AND docto.ic_epo  = prod_epo.ic_epo"   +
					" 	AND docto.ic_documento = camb_estatus.ic_documento"   +
					" 	AND docto.ic_linea_credito_dm = lin_cred.ic_linea_credito_dm"   +
					" 	AND docto_sel.ic_documento = soli.ic_documento"   +
					" 	AND docto.ic_estatus_docto = ed.ic_estatus_docto"   +
					" 	AND soli.cc_acuse = ca3.cc_acuse"   +
					" 	AND camb_estatus.dc_fecha_cambio IN(SELECT MAX(dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_documento = docto.ic_documento)"   +
					" 	AND lin_cred.ic_moneda = moneda.ic_moneda"   +
					" 	AND lin_cred.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes"   +
					" 	AND prod_epo.ic_producto_nafin = prod_nafin.ic_producto_nafin"   +
					" 	AND soli.ic_tasaif = ct.ic_tasa "   +
					" 	AND prod_nafin.ic_producto_nafin = 4"   +
					"	AND lin_cred.ic_if = ? "+
					" AND camb_estatus.ic_documento||'-'||to_char(camb_estatus.dc_fecha_cambio,'ddmmyyyyhhmiss') in ("+
					clavesDocumentos.toString()+" )");
					

		qryCCC.append(
					" SELECT /*+use_nl(docto,doct-o_sel,soli,ca3,camb_estatus,lin_cred,pyme,epo,prod_epo,prod_nafi,moneda,tci,ed,ct) "   +
					"     index(docto cp_dis_documento_pk)"   +
					"     index(lin_cred cp_com_linea_credito_pk)  "   +
					"     index(tci cp_comcat_tipo_cobro_inter_pk) */"   +
					"       docto.ic_documento     AS num_credito,"   +
					"       epo.cg_razon_social    AS epo,"   +
					"       pyme.cg_razon_social   AS distribuidor,"   +
					"       'Modalidad 2 (Riesgo Distribuidor)'  AS tipo_credito,"   +
					"       DECODE (NVL (prod_epo.cg_responsable_interes, prod_nafin.cg_responsable_interes),'E', 'EPO','D', 'Distribuidor','') AS responsable_interes,"   +
					"       DECODE (NVL (prod_epo.cg_tipo_cobranza, prod_nafin.cg_tipo_cobranza),'D', 'Delegada','N', 'No Delegada','') AS tipo_cobranza,"   +
					"       TO_CHAR(soli.df_operacion , 'DD/MM/YYYY')  AS fecha_operacion,"   +
					"       moneda.cd_nombre 	   AS tipo_moneda,"   +
					"       docto.fn_monto AS monto,"   +
					"       DECODE(NVL(prod_epo.cg_tipo_conversion, prod_nafin.cg_tipo_conversion), 'P', 'Dolar-Peso', 'Sin Conversion')  AS tipo_conversion,"   +
					"       DECODE(docto.fn_tipo_cambio, 1, ' ', docto.fn_tipo_cambio) AS tipo_cambio,"   +
					"       docto.fn_monto * docto.fn_tipo_cambio AS monto_valuado,"   +
					"       NVL(ct.cd_nombre, 'No Autorizado') AS ref_tasa,"   +
					"       DECODE(docto_sel.cg_rel_mat, '+', docto_sel.fn_puntos + docto_sel.fn_valor_tasa, '-',"   +
					"       docto_sel.fn_valor_tasa - docto_sel.fn_puntos, '*', docto_sel.fn_puntos * docto_sel.fn_valor_tasa, '/',"   +
					"       docto_sel.fn_valor_tasa / docto_sel.fn_puntos)  AS tasa_interes,"   +
					"       docto.ig_plazo_credito AS plazo,"   +
					"       TO_CHAR(docto.df_fecha_venc_credito, 'DD/MM/YYYY') AS fch_vencimiento,"   +
					"       docto_sel.fn_importe_interes AS monto_interes,"   +
					"       tci.cd_descripcion AS tipo_cobro_inte,"   +
					"       ed.cd_descripcion  AS estatus_soli,"   +
					"       TO_CHAR(camb_estatus.dc_fecha_cambio, 'DD/MM/YYYY') AS fecha_pago,"   +
					"       camb_estatus.cg_numero_pago  AS num_pago,"   +
					"       moneda.ic_moneda"   +
					"  FROM dis_documento           docto "   +
					"		,dis_docto_seleccionado docto_sel"   +
					"		,dis_solicitud          soli"   +
					"		,com_acuse3			 	ca3"   +
					"		,dis_cambio_estatus     camb_estatus"   +
					"		,com_linea_credito   	lin_cred"   +
					"		,comcat_pyme            pyme"   +
					"		,comcat_epo             epo"   +
					"		,comrel_producto_epo    prod_epo"   +
					"		,comcat_producto_nafin  prod_nafin"   +
					"		,comcat_moneda          moneda"   +
					"		,comcat_tipo_cobro_interes tci"   +
					"		,comcat_estatus_docto ed"   +
					"		,comcat_tasa ct"   +
					"  WHERE docto.ic_documento = docto_sel.ic_documento (+)"   +
					" 	AND docto.ic_epo = epo.ic_epo"   +
					" 	AND docto.ic_pyme = pyme.ic_pyme"   +
					" 	AND docto.ic_epo  = prod_epo.ic_epo"   +
					" 	AND docto.ic_documento = camb_estatus.ic_documento"   +
					" 	AND docto.ic_linea_credito = lin_cred.ic_linea_credito (+)"   +
					" 	AND docto_sel.ic_documento = soli.ic_documento (+)"   +
					" 	AND docto.ic_estatus_docto = ed.ic_estatus_docto"   +
					" 	AND soli.cc_acuse = ca3.cc_acuse (+)"   +
					" 	AND camb_estatus.dc_fecha_cambio IN(SELECT MAX(dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_documento = docto.ic_documento)"   +
					" 	AND lin_cred.ic_moneda = moneda.ic_moneda"   +
					" 	AND lin_cred.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes"   +
					" 	AND prod_epo.ic_producto_nafin = prod_nafin.ic_producto_nafin"   +
					" 	AND soli.ic_tasaif = ct.ic_tasa "   +
					" 	AND prod_nafin.ic_producto_nafin = 4"   +
					"	AND lin_cred.ic_if = ? "+
					" AND camb_estatus.ic_documento||'-'||to_char(camb_estatus.dc_fecha_cambio,'ddmmyyyyhhmiss') in ("+
					clavesDocumentos.toString()+" )");

			String qryAux = "";	
			if("D".equals(ctCredito))
				qryAux = qryDM.toString();
			else if("C".equals(ctCredito))
				qryAux = qryCCC.toString();
			else{
				qryAux = qryDM.toString()+ " UNION ALL "+qryCCC.toString();
				conditions.addAll(conditions);
			}

		System.out.println("el query queda de la siguiente manera "+qryAux.toString());
		return qryAux.toString();
	  }
		public String getDocumentQuery() {
			//Primero
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		String qryAux = "";	
		StringBuffer qryDM 		= new StringBuffer();
		StringBuffer qryCCC 	= new StringBuffer();
		StringBuffer condicion  = new StringBuffer();
		
		

		try {
		conditions=new ArrayList();
		conditions.add(sesIf);

		if(!"".equals(icDist)){
			condicion.append(" and docto.ic_pyme = ? ");
			conditions.add(icDist);
		}
		
		if(!"".equals(ic_epo)){
			condicion.append(" and docto.ic_epo = ? ");
			conditions.add(ic_epo);
		}
		if(!"".equals(numCred)){
			condicion.append(" and docto.ic_documento = ? ");
			conditions.add(numCred);
		}
		if (!fechaOper1.equals("")){
			condicion.append(" AND soli.df_operacion >= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaOper1);
		}
		if (!fechaOper2.equals("")){
			condicion.append(" AND soli.df_operacion <= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaOper2);
		}
		if ( Double.parseDouble(monto1) > 0 )		{
			condicion.append(" AND docto.fn_monto >= ? ");
			conditions.add(monto1);
		}
		if ( Double.parseDouble(monto2) > 0 ){		
			condicion.append(" AND docto.fn_monto <= ? ");
			conditions.add(monto2);
		}
		if (!fechaVenc1.equals("")){
			condicion.append(" AND docto.df_fecha_venc_credito >= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaVenc1);
		}
		if (!fechaVenc2.equals("")){
			condicion.append(" AND docto.df_fecha_venc_credito <= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaVenc2);
		}
		
		if (!tipoCoInt.equals("")){
			condicion.append(" AND lin_cred.ic_tipo_cobro_interes = ? ");
			conditions.add(tipoCoInt);
		}
		if (!estatus.equals("") ){
			condicion.append(" AND docto.ic_estatus_docto = ? ");
			conditions.add(estatus);
		}
		if (!fechaPago1.equals("") ){	
			condicion.append(" AND camb_estatus.dc_fecha_cambio >= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaPago1);
		}
		if (!fechaPago2.equals("") ){
			condicion.append(" AND camb_estatus.dc_fecha_cambio <= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaPago2);
		}

		qryDM.append(
					"  SELECT /*+ use_nl(docto,docto_sel,soli,ca3,camb_estatus,lin_cred,prod_epo)"   +
					"     index(docto cp_dis_documento_pk)"   +
					"     index(lin_cred cp_dis_linea_credito_dm_pk)*/"   +
					"     camb_estatus.ic_documento||'-'||TO_CHAR(camb_estatus.dc_fecha_cambio,'ddmmyyyyhhmiss')"   +
					"   FROM dis_documento          docto"   +
					" 		,dis_docto_seleccionado docto_sel"   +
					" 		,dis_solicitud          soli"   +
					" 		,com_acuse3			 	ca3"   +
					" 		,comcat_epo epo " +
					" 		,dis_cambio_estatus     camb_estatus"   +
					" 		,dis_linea_credito_dm   lin_cred"   +
					" 		,comrel_producto_epo    prod_epo"   +
					"   WHERE docto.ic_documento = docto_sel.ic_documento"   +
					"  	AND docto.ic_epo  = epo.ic_epo"   +
					"  	AND docto.ic_epo  = prod_epo.ic_epo"   +
					"  	AND docto.ic_documento = camb_estatus.ic_documento"   +
					"  	AND docto.ic_linea_credito_dm = lin_cred.ic_linea_credito_dm"   +
					"  	AND docto_sel.ic_documento = soli.ic_documento"   +
					"  	AND docto.ic_producto_nafin = prod_epo.ic_producto_nafin"   +
					"  	AND soli.cc_acuse = ca3.cc_acuse"   +
					"  	AND camb_estatus.dc_fecha_cambio IN(SELECT MAX(dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_documento = docto.ic_documento)"   +
					"	AND lin_cred.ic_if = ? "+
					" 		AND epo.cs_habilitado = 'S' " +
					condicion.toString()  );

		qryCCC.append(
					"  SELECT /*+ use_nl(docto,docto_sel,soli,ca3,camb_estatus,lin_cred,prod_epo)"   +
					"     index(docto cp_dis_documento_pk)"   +
					"     index(lin_cred cp_com_linea_credito_pk)*/"   +
					"     camb_estatus.ic_documento||'-'||TO_CHAR(camb_estatus.dc_fecha_cambio,'ddmmyyyyhhmiss')"   +
					"   FROM dis_documento          docto"   +
					" 		,dis_docto_seleccionado docto_sel"   +
					" 		,dis_solicitud          soli"   +
					" 		,com_acuse3			 	ca3"   +
					" 		,comcat_epo			 	epo "   +
					" 		,dis_cambio_estatus     camb_estatus"   +
					" 		,com_linea_credito	    lin_cred"   +
					" 		,comrel_producto_epo    prod_epo"   +
					"   WHERE docto.ic_documento = docto_sel.ic_documento (+)"   +
					"  	AND docto.ic_epo  = epo.ic_epo"   +
					"  	AND docto.ic_epo  = prod_epo.ic_epo"   +
					"  	AND docto.ic_documento = camb_estatus.ic_documento"   +
					"  	AND docto.ic_linea_credito = lin_cred.ic_linea_credito (+)"   +
					"  	AND docto_sel.ic_documento = soli.ic_documento (+)"   +
					"  	AND docto.ic_producto_nafin = prod_epo.ic_producto_nafin"   +
					"  	AND soli.cc_acuse = ca3.cc_acuse (+)"   +
					"  	AND camb_estatus.dc_fecha_cambio IN(SELECT MAX(dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_documento = docto.ic_documento)"   +
					"	AND lin_cred.ic_if = ? "+
					" 		AND epo.cs_habilitado = 'S' " +
					condicion.toString()  );

			if("D".equals(ctCredito))
				qryAux = qryDM.toString();
			else if("C".equals(ctCredito))
				qryAux = qryCCC.toString();
			else{
				qryAux = qryDM.toString()+ " UNION ALL "+qryCCC.toString();
				conditions.addAll(conditions);

			}

			System.out.println("EL query de la llave primaria: "+qryAux.toString());
		   System.out.println("Conditions de la llave primaria: "+conditions.toString());

		}catch(Exception e){
			System.out.println("ConsPagosCreditosIfDist::getDocumentQueryException "+e);
		}
		return qryAux.toString();
			
			
		}

	
	
	//Fin Metodos IQueryGeneratorRegExtJS
	
	

	public String getAggregateCalculationQuery(HttpServletRequest request) {
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		String qryAux = "";	
		StringBuffer qryDM 		= new StringBuffer();
		StringBuffer qryCCC		= new StringBuffer();
		StringBuffer condicion  = new StringBuffer();

		String 	lsIntermediario	= request.getSession().getAttribute("iNoCliente").toString();
		String	lsNumCredito		= (request.getParameter("txtNumCredito") 	  == null) ? ""   : request.getParameter("txtNumCredito");
		String	lsNumEPO			= (request.getParameter("lstEpo")         == null) ? "0"  : request.getParameter("lstEpo");
		String	lsNumPYME			= (request.getParameter("lstPyme")        	  == null) ? ""   : request.getParameter("lstPyme");
		String	lsEstatus			= (request.getParameter("lstEstatus")         == null) ? "11" : request.getParameter("lstEstatus");
		String	lsFecha				= (request.getParameter("msFecha")   		  == null) ? fechaHoy   : request.getParameter("msFecha");
		String	lsFchOperIni		= (request.getParameter("txtFchOper1")   	  == null) ? lsFecha   : request.getParameter("txtFchOper1");
		String	lsFchOperFin		= (request.getParameter("txtFchOper2")   	  == null) ? lsFecha   : request.getParameter("txtFchOper2");
		String	lsFchPagoIni		= (request.getParameter("txtFchPago1")   	  == null) ? ""   : request.getParameter("txtFchPago1");
		String	lsFchPagoFin		= (request.getParameter("txtFchPago2")   	  == null) ? ""   : request.getParameter("txtFchPago2");
		String	lsFchVenciIni		= (request.getParameter("txtFchVencimiento1") == null) ? ""   : request.getParameter("txtFchVencimiento1");
		String	lsFchVenciFin		= (request.getParameter("txtFchVencimiento2") == null) ? ""   : request.getParameter("txtFchVencimiento2");
		String	lsMontoCredIni		= (request.getParameter("txtMonto1")       	  == null) ? "0"  : (request.getParameter("txtMonto1").equals("")) ? "0" : request.getParameter("txtMonto1");
		String	lsMontoCredFin 		= (request.getParameter("txtMonto2")     	  == null) ? "0"  : (request.getParameter("txtMonto2").equals("")) ? "0" : request.getParameter("txtMonto2");
		String	tipo_credito		= (request.getParameter("lstTipoCredito")     == null) ? ""   : request.getParameter("lstTipoCredito");
		String	lsTipoCobroInte		= (request.getParameter("lstTipoCobroInte")   == null) ? ""   : request.getParameter("lstTipoCobroInte");
		
		try {

		if (!lsNumPYME.equals(""))
			condicion.append(" AND docto.ic_pyme = " + lsNumPYME);
		if (!lsNumEPO.equals(""))
			condicion.append(" AND docto.ic_epo  = " + lsNumEPO);
		if (!lsNumCredito.equals(""))	
			condicion.append(" AND docto.ic_documento = " + lsNumCredito);
		if (!lsFchOperIni.equals(""))	
			condicion.append(" AND soli.df_operacion >= TO_DATE('"+lsFchOperIni+"', 'DD/MM/YYYY') ");
		if (!lsFchOperFin.equals(""))
			condicion.append(" AND soli.df_operacion <= TO_DATE('"+lsFchOperFin+"', 'DD/MM/YYYY') ");
		if ( Double.parseDouble(lsMontoCredIni) > 0 )		
			condicion.append(" AND docto.fn_monto >= " + lsMontoCredIni);
		if ( Double.parseDouble(lsMontoCredFin) > 0 )		
			condicion.append(" AND docto.fn_monto <= " + lsMontoCredFin);
		if (!lsFchVenciIni.equals(""))	
			condicion.append(" AND docto.df_fecha_venc_credito >= TO_DATE('"+lsFchVenciIni+"', 'DD/MM/YYYY') ");
		if (!lsFchVenciFin.equals(""))
			condicion.append(" AND docto.df_fecha_venc_credito <= TO_DATE('"+lsFchVenciFin+"', 'DD/MM/YYYY') ");
		if (!lsTipoCobroInte.equals(""))
			condicion.append(" AND lin_cred.ic_tipo_cobro_interes = " + lsTipoCobroInte);
		if (!lsEstatus.equals("") )		
			condicion.append(" AND docto.ic_estatus_docto = " + lsEstatus);
		if (!lsFchPagoIni.equals("") )	
			condicion.append(" AND camb_estatus.dc_fecha_cambio >= TO_DATE('"+lsFchPagoIni+"', 'DD/MM/YYYY') ");
		if (!lsFchPagoFin.equals("") )	
			condicion.append(" AND camb_estatus.dc_fecha_cambio <= TO_DATE('"+lsFchPagoFin+"', 'DD/MM/YYYY') ");

		qryDM.append(
					"  SELECT /*+ use_nl(docto,docto_sel,soli,ca3,epo,camb_estatus,lin_cred,prod_epo,moneda)"   +
					"         index(docto cp_dis_documento_pk)"   +
					"         index(lin_cred cp_dis_linea_credito_dm_pk)*/"   +
					"        moneda.ic_moneda"   +
					" 	   	,moneda.cd_nombre"   +
					" 	   	,COUNT(1)"   +
					"        ,SUM(docto.fn_monto) as fn_importe_recibir"   +
					"        ,SUM(docto_sel.fn_importe_interes)"   +
					"   FROM dis_documento          docto"   +
					" 		,dis_docto_seleccionado docto_sel"   +
					" 		,dis_solicitud          soli"   +
					" 		,com_acuse3			 		ca3"   +
					" 		, comcat_epo epo " +
					" 		,dis_cambio_estatus     camb_estatus"   +
					" 		,dis_linea_credito_dm   lin_cred"   +
					" 		,comrel_producto_epo    prod_epo"   +
					" 		,comcat_moneda          moneda"   +
					"   WHERE docto.ic_documento = docto_sel.ic_documento"   +
					"  	AND docto.ic_epo  = epo.ic_epo"   +
					"  	AND docto.ic_epo  = prod_epo.ic_epo"   +
					"  	AND docto.ic_documento = camb_estatus.ic_documento"   +
					"  	AND docto.ic_linea_credito_dm = lin_cred.ic_linea_credito_dm"   +
					"  	AND docto_sel.ic_documento = soli.ic_documento"   +
					" 	AND docto.ic_producto_nafin = prod_epo.ic_producto_nafin"   +
					"  	AND soli.cc_acuse = ca3.cc_acuse"   +
					"  	AND camb_estatus.dc_fecha_cambio IN(SELECT MAX(dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_documento = docto.ic_documento)"   +
					"  	AND lin_cred.ic_moneda = moneda.ic_moneda"   +
					"	AND lin_cred.ic_if = "+lsIntermediario+
					" 		AND epo.cs_habilitado = 'S' " +
			  		condicion.toString()+
					"  GROUP BY moneda.ic_moneda,moneda.cd_nombre");

		qryCCC.append(
					" SELECT /*+ use_nl(docto,docto_sel,soli,ca3,epo,camb_estatus,lin_cred,prod_epo,moneda)"   +
					"         index(docto cp_dis_documento_pk)"   +
					"         index(lin_cred cp_com_linea_credito_pk)*/"   +
					"        moneda.ic_moneda"   +
					" 	   	,moneda.cd_nombre"   +
					" 	   	,COUNT(1)"   +
					"        ,SUM(docto.fn_monto) as fn_importe_recibir"   +
					"        ,SUM(docto_sel.fn_importe_interes)"   +
					"   FROM dis_documento          docto"   +
					" 		,dis_docto_seleccionado docto_sel"   +
					" 		,dis_solicitud          soli"   +
					" 		,com_acuse3			 	ca3"   +
					" 		, comcat_epo epo " +
					" 		,dis_cambio_estatus     camb_estatus"   +
					" 		,com_linea_credito   	lin_cred"   +
					" 		,comrel_producto_epo    prod_epo"   +
					" 		,comcat_moneda          moneda"   +
					"   WHERE docto.ic_documento = docto_sel.ic_documento (+)"   +
					"  	AND docto.ic_epo  = epo.ic_epo"   +
					"  	AND docto.ic_epo  = prod_epo.ic_epo"   +
					"  	AND docto.ic_documento = camb_estatus.ic_documento"   +
					"  	AND docto.ic_linea_credito = lin_cred.ic_linea_credito (+)"   +
					"  	AND docto_sel.ic_documento = soli.ic_documento (+)"   +
					" 	AND docto.ic_producto_nafin = prod_epo.ic_producto_nafin"   +
					"  	AND soli.cc_acuse = ca3.cc_acuse (+)"   +
					"  	AND camb_estatus.dc_fecha_cambio IN(SELECT MAX(dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_documento = docto.ic_documento)"   +
					"  	AND lin_cred.ic_moneda = moneda.ic_moneda"   +
					"	AND lin_cred.ic_if = "+lsIntermediario+
					" 		AND epo.cs_habilitado = 'S' " +
			  		condicion.toString()+
					"  GROUP BY moneda.ic_moneda,moneda.cd_nombre" );

			if("D".equals(tipo_credito))
				qryAux = qryDM.toString();
			else if("C".equals(tipo_credito))
				qryAux = qryCCC.toString();
			else
				qryAux = qryDM.toString()+ " UNION ALL "+qryCCC.toString();
		}catch(Exception e){
			System.out.println("ConsPagosCreditosIfDist::getAggregateCalculationQuery "+e);
		}
		return qryAux.toString();
	}

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids) {
		int i=0;
		StringBuffer qryDM		= new StringBuffer();
    	StringBuffer qryCCC		= new StringBuffer();
    	StringBuffer condicion	= new StringBuffer();
    	String 	lsIntermediario	= request.getSession().getAttribute("iNoCliente").toString();
		String	tipo_credito	= (request.getParameter("lstTipoCredito") == null) ? "" : request.getParameter("lstTipoCredito");
    
		condicion.append(" AND camb_estatus.ic_documento||'-'||to_char(camb_estatus.dc_fecha_cambio,'ddmmyyyyhhmiss') in (");
		i=0;
		for (Iterator it = ids.iterator(); it.hasNext();i++){
        	if(i>0){
				condicion.append(",");
			}
			condicion.append(" '"+it.next().toString()+"' ");
		}
		condicion.append(") ");

		qryDM.append(
					" SELECT /*+use_nl(docto,doct-o_sel,soli,ca3,camb_estatus,lin_cred,pyme,epo,prod_epo,prod_nafi,moneda,tci,ed,ct) "   +
					"     index(docto cp_dis_documento_pk)"   +
					"     index(lin_cred cp_dis_linea_credito_dm_pk)  "   +
					"     index(tci cp_comcat_tipo_cobro_inter_pk) */"   +
					"       docto.ic_documento     AS num_credito,"   +
					"       epo.cg_razon_social    AS epo,"   +
					"       pyme.cg_razon_social   AS distribuidor,"   +
					"       'Modalidad 1 (Riesgo Empresa de Primer Orden )'  AS tipo_credito,"   +
					"       DECODE (NVL (prod_epo.cg_responsable_interes, prod_nafin.cg_responsable_interes),'E', 'EPO','D', 'Distribuidor','') AS responsable_interes,"   +
					"       DECODE (NVL (prod_epo.cg_tipo_cobranza, prod_nafin.cg_tipo_cobranza),'D', 'Delegada','N', 'No Delegada','') AS tipo_cobranza,"   +
					"       TO_CHAR(soli.df_operacion , 'DD/MM/YYYY')  AS fecha_operacion,"   +
					"       moneda.cd_nombre 	   AS tipo_moneda,"   +
					"       docto.fn_monto AS monto,"   +
					"       DECODE(NVL(prod_epo.cg_tipo_conversion, prod_nafin.cg_tipo_conversion), 'P', 'Dolar-Peso', 'Sin Conversion')  AS tipo_conversion,"   +
					"       DECODE(docto.fn_tipo_cambio, 1, ' ', docto.fn_tipo_cambio) AS tipo_cambio,"   +
					"       docto.fn_monto * docto.fn_tipo_cambio AS monto_valuado,"   +
					"       NVL(ct.cd_nombre, 'No Autorizado') AS ref_tasa,"   +
					"       DECODE(docto_sel.cg_rel_mat, '+', docto_sel.fn_puntos + docto_sel.fn_valor_tasa, '-',"   +
					"       docto_sel.fn_valor_tasa - docto_sel.fn_puntos, '*', docto_sel.fn_puntos * docto_sel.fn_valor_tasa, '/',"   +
					"       docto_sel.fn_valor_tasa / docto_sel.fn_puntos)  AS tasa_interes,"   +
					"       docto.ig_plazo_credito AS plazo,"   +
					"       TO_CHAR(docto.df_fecha_venc_credito, 'DD/MM/YYYY') AS fch_vencimiento,"   +
					"       docto_sel.fn_importe_interes AS monto_interes,"   +
					"       tci.cd_descripcion AS tipo_cobro_inte,"   +
					"       ed.cd_descripcion  AS estatus_soli,"   +
					"       TO_CHAR(camb_estatus.dc_fecha_cambio, 'DD/MM/YYYY') AS fecha_pago,"   +
					"       camb_estatus.cg_numero_pago  AS num_pago,"   +
					"       moneda.ic_moneda"   +
					"  FROM dis_documento          docto "   +
					"		,dis_docto_seleccionado docto_sel"   +
					"		,dis_solicitud          soli"   +
					"		,com_acuse3			 		ca3"   +
					"		,dis_cambio_estatus     camb_estatus"   +
					"		,dis_linea_credito_dm   lin_cred"   +
					"		,comcat_pyme            pyme"   +
					"		,comcat_epo             epo"   +
					"		,comrel_producto_epo    prod_epo"   +
					"		,comcat_producto_nafin  prod_nafin"   +
					"		,comcat_moneda          moneda"   +
					"		,comcat_tipo_cobro_interes tci"   +
					"		,comcat_estatus_docto ed"   +
					"		,comcat_tasa ct"   +
					"  WHERE docto.ic_documento = docto_sel.ic_documento"   +
					" 	AND docto.ic_epo = epo.ic_epo"   +
					" 	AND docto.ic_pyme = pyme.ic_pyme"   +
					" 	AND docto.ic_epo  = prod_epo.ic_epo"   +
					" 	AND docto.ic_documento = camb_estatus.ic_documento"   +
					" 	AND docto.ic_linea_credito_dm = lin_cred.ic_linea_credito_dm"   +
					" 	AND docto_sel.ic_documento = soli.ic_documento"   +
					" 	AND docto.ic_estatus_docto = ed.ic_estatus_docto"   +
					" 	AND soli.cc_acuse = ca3.cc_acuse"   +
					" 	AND camb_estatus.dc_fecha_cambio IN(SELECT MAX(dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_documento = docto.ic_documento)"   +
					" 	AND lin_cred.ic_moneda = moneda.ic_moneda"   +
					" 	AND lin_cred.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes"   +
					" 	AND prod_epo.ic_producto_nafin = prod_nafin.ic_producto_nafin"   +
					" 	AND soli.ic_tasaif = ct.ic_tasa "   +
					" 	AND prod_nafin.ic_producto_nafin = 4"   +
					"	AND lin_cred.ic_if = "+lsIntermediario+
					condicion.toString());

		qryCCC.append(
					" SELECT /*+use_nl(docto,doct-o_sel,soli,ca3,camb_estatus,lin_cred,pyme,epo,prod_epo,prod_nafi,moneda,tci,ed,ct) "   +
					"     index(docto cp_dis_documento_pk)"   +
					"     index(lin_cred cp_com_linea_credito_pk)  "   +
					"     index(tci cp_comcat_tipo_cobro_inter_pk) */"   +
					"       docto.ic_documento     AS num_credito,"   +
					"       epo.cg_razon_social    AS epo,"   +
					"       pyme.cg_razon_social   AS distribuidor,"   +
					"       'Modalidad 2 (Riesgo Distribuidor)'  AS tipo_credito,"   +
					"       DECODE (NVL (prod_epo.cg_responsable_interes, prod_nafin.cg_responsable_interes),'E', 'EPO','D', 'Distribuidor','') AS responsable_interes,"   +
					"       DECODE (NVL (prod_epo.cg_tipo_cobranza, prod_nafin.cg_tipo_cobranza),'D', 'Delegada','N', 'No Delegada','') AS tipo_cobranza,"   +
					"       TO_CHAR(soli.df_operacion , 'DD/MM/YYYY')  AS fecha_operacion,"   +
					"       moneda.cd_nombre 	   AS tipo_moneda,"   +
					"       docto.fn_monto AS monto,"   +
					"       DECODE(NVL(prod_epo.cg_tipo_conversion, prod_nafin.cg_tipo_conversion), 'P', 'Dolar-Peso', 'Sin Conversion')  AS tipo_conversion,"   +
					"       DECODE(docto.fn_tipo_cambio, 1, ' ', docto.fn_tipo_cambio) AS tipo_cambio,"   +
					"       docto.fn_monto * docto.fn_tipo_cambio AS monto_valuado,"   +
					"       NVL(ct.cd_nombre, 'No Autorizado') AS ref_tasa,"   +
					"       DECODE(docto_sel.cg_rel_mat, '+', docto_sel.fn_puntos + docto_sel.fn_valor_tasa, '-',"   +
					"       docto_sel.fn_valor_tasa - docto_sel.fn_puntos, '*', docto_sel.fn_puntos * docto_sel.fn_valor_tasa, '/',"   +
					"       docto_sel.fn_valor_tasa / docto_sel.fn_puntos)  AS tasa_interes,"   +
					"       docto.ig_plazo_credito AS plazo,"   +
					"       TO_CHAR(docto.df_fecha_venc_credito, 'DD/MM/YYYY') AS fch_vencimiento,"   +
					"       docto_sel.fn_importe_interes AS monto_interes,"   +
					"       tci.cd_descripcion AS tipo_cobro_inte,"   +
					"       ed.cd_descripcion  AS estatus_soli,"   +
					"       TO_CHAR(camb_estatus.dc_fecha_cambio, 'DD/MM/YYYY') AS fecha_pago,"   +
					"       camb_estatus.cg_numero_pago  AS num_pago,"   +
					"       moneda.ic_moneda"   +
					"  FROM dis_documento           docto "   +
					"		,dis_docto_seleccionado docto_sel"   +
					"		,dis_solicitud          soli"   +
					"		,com_acuse3			 	ca3"   +
					"		,dis_cambio_estatus     camb_estatus"   +
					"		,com_linea_credito   	lin_cred"   +
					"		,comcat_pyme            pyme"   +
					"		,comcat_epo             epo"   +
					"		,comrel_producto_epo    prod_epo"   +
					"		,comcat_producto_nafin  prod_nafin"   +
					"		,comcat_moneda          moneda"   +
					"		,comcat_tipo_cobro_interes tci"   +
					"		,comcat_estatus_docto ed"   +
					"		,comcat_tasa ct"   +
					"  WHERE docto.ic_documento = docto_sel.ic_documento (+)"   +
					" 	AND docto.ic_epo = epo.ic_epo"   +
					" 	AND docto.ic_pyme = pyme.ic_pyme"   +
					" 	AND docto.ic_epo  = prod_epo.ic_epo"   +
					" 	AND docto.ic_documento = camb_estatus.ic_documento"   +
					" 	AND docto.ic_linea_credito = lin_cred.ic_linea_credito (+)"   +
					" 	AND docto_sel.ic_documento = soli.ic_documento (+)"   +
					" 	AND docto.ic_estatus_docto = ed.ic_estatus_docto"   +
					" 	AND soli.cc_acuse = ca3.cc_acuse (+)"   +
					" 	AND camb_estatus.dc_fecha_cambio IN(SELECT MAX(dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_documento = docto.ic_documento)"   +
					" 	AND lin_cred.ic_moneda = moneda.ic_moneda"   +
					" 	AND lin_cred.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes"   +
					" 	AND prod_epo.ic_producto_nafin = prod_nafin.ic_producto_nafin"   +
					" 	AND soli.ic_tasaif = ct.ic_tasa "   +
					" 	AND prod_nafin.ic_producto_nafin = 4"   +
					"	AND lin_cred.ic_if = "+lsIntermediario+
					condicion.toString());

			String qryAux = "";	
			if("D".equals(tipo_credito))
				qryAux = qryDM.toString();
			else if("C".equals(tipo_credito))
				qryAux = qryCCC.toString();
			else
				qryAux = qryDM.toString()+ " UNION ALL "+qryCCC.toString();

		System.out.println("el query queda de la siguiente manera "+qryAux.toString());
		return qryAux.toString();
	}
	
	public String getDocumentQuery(HttpServletRequest request) {

      String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		String qryAux = "";	
		StringBuffer qryDM 		= new StringBuffer();
		StringBuffer qryCCC 	= new StringBuffer();
		StringBuffer condicion  = new StringBuffer();
		
		String 	lsIntermediario	= request.getSession().getAttribute("iNoCliente").toString();
		String	lsNumCredito		= (request.getParameter("txtNumCredito") 	  == null) ? ""   : request.getParameter("txtNumCredito");
		String	lsNumEPO			= (request.getParameter("lstEpo")        	  == null) ? "0"  : request.getParameter("lstEpo");
		String	lsNumPYME			= (request.getParameter("lstPyme")        	  == null) ? ""   : request.getParameter("lstPyme");
		String	lsEstatus			= (request.getParameter("lstEstatus")         == null) ? "11" : request.getParameter("lstEstatus");
		String	lsFecha				= (request.getParameter("msFecha")   		  == null) ? fechaHoy   : request.getParameter("msFecha");
		String	lsFchOperIni		= (request.getParameter("txtFchOper1")   	  == null) ? lsFecha   : request.getParameter("txtFchOper1");
		String	lsFchOperFin		= (request.getParameter("txtFchOper2")   	  == null) ? lsFecha   : request.getParameter("txtFchOper2");
		String	lsFchPagoIni		= (request.getParameter("txtFchPago1")   	  == null) ? ""   : request.getParameter("txtFchPago1");
		String	lsFchPagoFin		= (request.getParameter("txtFchPago2")   	  == null) ? ""   : request.getParameter("txtFchPago2");
		String	lsFchVenciIni		= (request.getParameter("txtFchVencimiento1") == null) ? ""   : request.getParameter("txtFchVencimiento1");
		String	lsFchVenciFin		= (request.getParameter("txtFchVencimiento2") == null) ? ""   : request.getParameter("txtFchVencimiento2");
		String	lsMontoCredIni		= (request.getParameter("txtMonto1")       	  == null) ? "0"  : (request.getParameter("txtMonto1").equals("")) ? "0" : request.getParameter("txtMonto1");
		String	lsMontoCredFin 		= (request.getParameter("txtMonto2")     	  == null) ? "0"  : (request.getParameter("txtMonto2").equals("")) ? "0" : request.getParameter("txtMonto2");
		String	lsTipoCobroInte		= (request.getParameter("lstTipoCobroInte")   == null) ? ""   : request.getParameter("lstTipoCobroInte");
		String	tipo_credito		= (request.getParameter("lstTipoCredito")     == null) ? ""   : request.getParameter("lstTipoCredito");
		
		try {

		if (!lsNumPYME.equals(""))
			condicion.append(" AND docto.ic_pyme = " + lsNumPYME);
		if (!lsNumEPO.equals(""))
			condicion.append(" AND docto.ic_epo  = " + lsNumEPO);
		if (!lsNumCredito.equals(""))	
			condicion.append(" AND docto.ic_documento = " + lsNumCredito);
		if (!lsFchOperIni.equals(""))	
			condicion.append(" AND soli.df_operacion >= TO_DATE('"+lsFchOperIni+"', 'DD/MM/YYYY') ");
		if (!lsFchOperFin.equals(""))
			condicion.append(" AND soli.df_operacion <= TO_DATE('"+lsFchOperFin+"', 'DD/MM/YYYY') ");
		if ( Double.parseDouble(lsMontoCredIni) > 0 )		
			condicion.append(" AND docto.fn_monto >= " + lsMontoCredIni);
		if ( Double.parseDouble(lsMontoCredFin) > 0 )		
			condicion.append(" AND docto.fn_monto <= " + lsMontoCredFin);
		if (!lsFchVenciIni.equals(""))	
			condicion.append(" AND docto.df_fecha_venc_credito >= TO_DATE('"+lsFchVenciIni+"', 'DD/MM/YYYY') ");
		if (!lsFchVenciFin.equals(""))
			condicion.append(" AND docto.df_fecha_venc_credito <= TO_DATE('"+lsFchVenciFin+"', 'DD/MM/YYYY') ");
		if (!lsTipoCobroInte.equals(""))
			condicion.append(" AND lin_cred.ic_tipo_cobro_interes = " + lsTipoCobroInte);
		if (!lsEstatus.equals("") )		
			condicion.append(" AND docto.ic_estatus_docto = " + lsEstatus);
		if (!lsFchPagoIni.equals("") )	
			condicion.append(" AND camb_estatus.dc_fecha_cambio >= TO_DATE('"+lsFchPagoIni+"', 'DD/MM/YYYY') ");
		if (!lsFchPagoFin.equals("") )	
			condicion.append(" AND camb_estatus.dc_fecha_cambio <= TO_DATE('"+lsFchPagoFin+"', 'DD/MM/YYYY') ");

		qryDM.append(
					"  SELECT /*+ use_nl(docto,docto_sel,soli,ca3,camb_estatus,lin_cred,prod_epo)"   +
					"     index(docto cp_dis_documento_pk)"   +
					"     index(lin_cred cp_dis_linea_credito_dm_pk)*/"   +
					"     camb_estatus.ic_documento||'-'||TO_CHAR(camb_estatus.dc_fecha_cambio,'ddmmyyyyhhmiss')"   +
					"   FROM dis_documento          docto"   +
					" 		,dis_docto_seleccionado docto_sel"   +
					" 		,dis_solicitud          soli"   +
					" 		,com_acuse3			 	ca3"   +
					" 		,comcat_epo epo " +
					" 		,dis_cambio_estatus     camb_estatus"   +
					" 		,dis_linea_credito_dm   lin_cred"   +
					" 		,comrel_producto_epo    prod_epo"   +
					"   WHERE docto.ic_documento = docto_sel.ic_documento"   +
					"  	AND docto.ic_epo  = epo.ic_epo"   +
					"  	AND docto.ic_epo  = prod_epo.ic_epo"   +
					"  	AND docto.ic_documento = camb_estatus.ic_documento"   +
					"  	AND docto.ic_linea_credito_dm = lin_cred.ic_linea_credito_dm"   +
					"  	AND docto_sel.ic_documento = soli.ic_documento"   +
					"  	AND docto.ic_producto_nafin = prod_epo.ic_producto_nafin"   +
					"  	AND soli.cc_acuse = ca3.cc_acuse"   +
					"  	AND camb_estatus.dc_fecha_cambio IN(SELECT MAX(dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_documento = docto.ic_documento)"   +
					"	AND lin_cred.ic_if = "+lsIntermediario+
					" 		AND epo.cs_habilitado = 'S' " +
					condicion.toString()  );

		qryCCC.append(
					"  SELECT /*+ use_nl(docto,docto_sel,soli,ca3,camb_estatus,lin_cred,prod_epo)"   +
					"     index(docto cp_dis_documento_pk)"   +
					"     index(lin_cred cp_com_linea_credito_pk)*/"   +
					"     camb_estatus.ic_documento||'-'||TO_CHAR(camb_estatus.dc_fecha_cambio,'ddmmyyyyhhmiss')"   +
					"   FROM dis_documento          docto"   +
					" 		,dis_docto_seleccionado docto_sel"   +
					" 		,dis_solicitud          soli"   +
					" 		,com_acuse3			 	ca3"   +
					" 		,comcat_epo			 	epo "   +
					" 		,dis_cambio_estatus     camb_estatus"   +
					" 		,com_linea_credito	    lin_cred"   +
					" 		,comrel_producto_epo    prod_epo"   +
					"   WHERE docto.ic_documento = docto_sel.ic_documento (+)"   +
					"  	AND docto.ic_epo  = epo.ic_epo"   +
					"  	AND docto.ic_epo  = prod_epo.ic_epo"   +
					"  	AND docto.ic_documento = camb_estatus.ic_documento"   +
					"  	AND docto.ic_linea_credito = lin_cred.ic_linea_credito (+)"   +
					"  	AND docto_sel.ic_documento = soli.ic_documento (+)"   +
					"  	AND docto.ic_producto_nafin = prod_epo.ic_producto_nafin"   +
					"  	AND soli.cc_acuse = ca3.cc_acuse (+)"   +
					"  	AND camb_estatus.dc_fecha_cambio IN(SELECT MAX(dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_documento = docto.ic_documento)"   +
					"	AND lin_cred.ic_if = "+lsIntermediario+
					" 		AND epo.cs_habilitado = 'S' " +
					condicion.toString()  );

			if("D".equals(tipo_credito))
				qryAux = qryDM.toString();
			else if("C".equals(tipo_credito))
				qryAux = qryCCC.toString();
			else
				qryAux = qryDM.toString()+ " UNION ALL "+qryCCC.toString();

			//System.out.println("EL query de la llave primaria: "+qryAux.toString());

		}catch(Exception e){
			System.out.println("ConsPagosCreditosIfDist::getDocumentQueryException "+e);
		}
		return qryAux.toString();
	}
	
	public String getDocumentQueryFile(HttpServletRequest request) {
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		String qryAux = "";	
		StringBuffer qryDM 		= new StringBuffer();
		StringBuffer qryCCC 	= new StringBuffer();
		StringBuffer condicion 	= new StringBuffer();
		
		String 	lsIntermediario	= request.getSession().getAttribute("iNoCliente").toString();
		String	lsNumCredito		= (request.getParameter("txtNumCredito") 	  == null) ? ""   : request.getParameter("txtNumCredito");
		String	lsNumEPO			= (request.getParameter("lstEpo")         	  == null) ? "0"  : request.getParameter("lstEpo");
		String	lsNumPYME			= (request.getParameter("lstPyme")        	  == null) ? ""   : request.getParameter("lstPyme");
		String	lsEstatus			= (request.getParameter("lstEstatus")         == null) ? "11" : request.getParameter("lstEstatus");
		String	lsFecha				= (request.getParameter("msFecha")   		  == null) ? fechaHoy   : request.getParameter("msFecha");
		String	lsFchOperIni		= (request.getParameter("txtFchOper1")   	  == null) ? lsFecha   : request.getParameter("txtFchOper1");
		String	lsFchOperFin		= (request.getParameter("txtFchOper2")   	  == null) ? lsFecha   : request.getParameter("txtFchOper2");
		String	lsFchPagoIni		= (request.getParameter("txtFchPago1")   	  == null) ? ""   : request.getParameter("txtFchPago1");
		String	lsFchPagoFin		= (request.getParameter("txtFchPago2")   	  == null) ? ""   : request.getParameter("txtFchPago2");
		String	lsFchVenciIni		= (request.getParameter("txtFchVencimiento1") == null) ? ""   : request.getParameter("txtFchVencimiento1");
		String	lsFchVenciFin		= (request.getParameter("txtFchVencimiento2") == null) ? ""   : request.getParameter("txtFchVencimiento2");
		String	lsMontoCredIni		= (request.getParameter("txtMonto1")       	  == null) ? "0"  : (request.getParameter("txtMonto1").equals("")) ? "0" : request.getParameter("txtMonto1");
		String	lsMontoCredFin 		= (request.getParameter("txtMonto2")     	  == null) ? "0"  : (request.getParameter("txtMonto2").equals("")) ? "0" : request.getParameter("txtMonto2");
		String	lsTipoCobroInte		= (request.getParameter("lstTipoCobroInte")   == null) ? ""   : request.getParameter("lstTipoCobroInte");
		String	tipo_credito		= (request.getParameter("lstTipoCredito")     == null) ? ""   : request.getParameter("lstTipoCredito");
		
		try {

		if (!lsNumPYME.equals(""))
			condicion.append(" AND docto.ic_pyme = " + lsNumPYME);
		if (!lsNumEPO.equals(""))
			condicion.append(" AND docto.ic_epo  = " + lsNumEPO);
		if (!lsNumCredito.equals(""))	
			condicion.append(" AND docto.ic_documento = " + lsNumCredito);
		if (!lsFchOperIni.equals(""))	
			condicion.append(" AND soli.df_operacion >= TO_DATE('"+lsFchOperIni+"', 'DD/MM/YYYY') ");
		if (!lsFchOperFin.equals(""))
			condicion.append(" AND soli.df_operacion <= TO_DATE('"+lsFchOperFin+"', 'DD/MM/YYYY') ");
		if ( Double.parseDouble(lsMontoCredIni) > 0 )		
			condicion.append(" AND docto.fn_monto >= " + lsMontoCredIni);
		if ( Double.parseDouble(lsMontoCredFin) > 0 )		
			condicion.append(" AND docto.fn_monto <= " + lsMontoCredFin);
		if (!lsFchVenciIni.equals(""))	
			condicion.append(" AND docto.df_fecha_venc_credito >= TO_DATE('"+lsFchVenciIni+"', 'DD/MM/YYYY') ");
		if (!lsFchVenciFin.equals(""))
			condicion.append(" AND docto.df_fecha_venc_credito <= TO_DATE('"+lsFchVenciFin+"', 'DD/MM/YYYY') ");
		if (!lsTipoCobroInte.equals(""))
			condicion.append(" AND lin_cred.ic_tipo_cobro_interes = " + lsTipoCobroInte);
		if (!lsEstatus.equals("") )		
			condicion.append(" AND docto.ic_estatus_docto = " + lsEstatus);
		if (!lsFchPagoIni.equals("") )	
			condicion.append(" AND camb_estatus.dc_fecha_cambio >= TO_DATE('"+lsFchPagoIni+"', 'DD/MM/YYYY') ");
		if (!lsFchPagoFin.equals("") )	
			condicion.append(" AND camb_estatus.dc_fecha_cambio <= TO_DATE('"+lsFchPagoFin+"', 'DD/MM/YYYY') ");

		qryDM.append(
					" SELECT /*+use_nl(docto,doct-o_sel,soli,ca3,camb_estatus,lin_cred,pyme,epo,prod_epo,prod_nafi,moneda,tci,ed,ct) "   +
					"     index(docto cp_dis_documento_pk)"   +
					"     index(lin_cred cp_dis_linea_credito_dm_pk)  "   +
					"     index(tci cp_comcat_tipo_cobro_inter_pk) */"   +
					"        docto.ic_documento     AS num_credito,"   +
					"       epo.cg_razon_social    AS epo,"   +
					"       pyme.cg_razon_social   AS distribuidor,"   +
					"       'Modalidad 1 (Riesgo Empresa de Primer Orden )'  AS tipo_credito,"   +
					"       DECODE (NVL (prod_epo.cg_responsable_interes, prod_nafin.cg_responsable_interes),'E', 'EPO','D', 'Distribuidor','') AS responsable_interes,"   +
					"       DECODE (NVL (prod_epo.cg_tipo_cobranza, prod_nafin.cg_tipo_cobranza),'D', 'Delegada','N', 'No Delegada','') AS tipo_cobranza,"   +
					"       TO_CHAR(soli.df_operacion , 'DD/MM/YYYY')  AS fecha_operacion,"   +
					"       moneda.cd_nombre 	   AS tipo_moneda,"   +
					"       docto.fn_monto AS monto,"   +
					"       DECODE(NVL(prod_epo.cg_tipo_conversion, prod_nafin.cg_tipo_conversion), 'P', 'Dolar-Peso', 'Sin Conversion')  AS tipo_conversion,"   +
					"       DECODE(docto.fn_tipo_cambio, 1, ' ', docto.fn_tipo_cambio) AS tipo_cambio,"   +
					"       docto.fn_monto * docto.fn_tipo_cambio AS monto_valuado,"   +
					"       NVL(ct.cd_nombre, 'No Autorizado') AS ref_tasa,"   +
					"       DECODE(docto_sel.cg_rel_mat, '+', docto_sel.fn_puntos + docto_sel.fn_valor_tasa, '-',"   +
					"       docto_sel.fn_valor_tasa - docto_sel.fn_puntos, '*', docto_sel.fn_puntos * docto_sel.fn_valor_tasa, '/',"   +
					"       docto_sel.fn_valor_tasa / docto_sel.fn_puntos)  AS tasa_interes,"   +
					"       docto.ig_plazo_credito AS plazo,"   +
					"       TO_CHAR(docto.df_fecha_venc_credito, 'DD/MM/YYYY') AS fch_vencimiento,"   +
					"       docto_sel.fn_importe_interes AS monto_interes,"   +
					"       tci.cd_descripcion AS tipo_cobro_inte,"   +
					"       ed.cd_descripcion  AS estatus_soli,"   +
					"       TO_CHAR(camb_estatus.dc_fecha_cambio, 'DD/MM/YYYY') AS fecha_pago,"   +
					"       camb_estatus.cg_numero_pago  AS num_pago,"   +
					"       moneda.ic_moneda"   +
					"  FROM dis_documento          docto "   +
					"		,dis_docto_seleccionado docto_sel"   +
					"		,dis_solicitud          soli"   +
					"		,com_acuse3			 		ca3"   +
					"		,dis_cambio_estatus     camb_estatus"   +
					"		,dis_linea_credito_dm   lin_cred"   +
					"		,comcat_pyme            pyme"   +
					"		,comcat_epo             epo"   +
					"		,comrel_producto_epo    prod_epo"   +
					"		,comcat_producto_nafin  prod_nafin"   +
					"		,comcat_moneda          moneda"   +
					"		,comcat_tipo_cobro_interes tci"   +
					"		,comcat_estatus_docto ed"   +
					"		,comcat_tasa ct"   +
					"  WHERE docto.ic_documento = docto_sel.ic_documento"   +
					" 	AND docto.ic_epo = epo.ic_epo"   +
					" 	AND docto.ic_pyme = pyme.ic_pyme"   +
					" 	AND docto.ic_epo  = prod_epo.ic_epo"   +
					" 	AND docto.ic_documento = camb_estatus.ic_documento"   +
					" 	AND docto.ic_linea_credito_dm = lin_cred.ic_linea_credito_dm"   +
					" 	AND docto_sel.ic_documento = soli.ic_documento"   +
					" 	AND docto.ic_estatus_docto = ed.ic_estatus_docto"   +
					" 	AND soli.cc_acuse = ca3.cc_acuse"   +
					" 	AND camb_estatus.dc_fecha_cambio IN(SELECT MAX(dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_documento = docto.ic_documento)"   +
					" 	AND lin_cred.ic_moneda = moneda.ic_moneda"   +
					" 	AND lin_cred.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes"   +
					" 	AND prod_epo.ic_producto_nafin = prod_nafin.ic_producto_nafin"   +
					" 	AND soli.ic_tasaif = ct.ic_tasa "   +
					" 	AND prod_nafin.ic_producto_nafin = 4"   +
					"	AND lin_cred.ic_if = "+lsIntermediario+
					" 	AND epo.cs_habilitado = 'S' " +
					condicion.toString()  );

		qryCCC.append(
					" SELECT /*+use_nl(docto,doct-o_sel,soli,ca3,camb_estatus,lin_cred,pyme,epo,prod_epo,prod_nafi,moneda,tci,ed,ct) "   +
					"     index(docto cp_dis_documento_pk)"   +
					"     index(lin_cred CP_COM_LINEA_CREDITO_PK)  "   +
					"     index(tci cp_comcat_tipo_cobro_inter_pk) */"   +
					"       docto.ic_documento     AS num_credito,"   +
					"       epo.cg_razon_social    AS epo,"   +
					"       pyme.cg_razon_social   AS distribuidor,"   +
					"       'Modalidad 2 (Riesgo Distribuidor)'  AS tipo_credito,"   +
					"       DECODE (NVL (prod_epo.cg_responsable_interes, prod_nafin.cg_responsable_interes),'E', 'EPO','D', 'Distribuidor','') AS responsable_interes,"   +
					"       DECODE (NVL (prod_epo.cg_tipo_cobranza, prod_nafin.cg_tipo_cobranza),'D', 'Delegada','N', 'No Delegada','') AS tipo_cobranza,"   +
					"       TO_CHAR(soli.df_operacion , 'DD/MM/YYYY')  AS fecha_operacion,"   +
					"       moneda.cd_nombre 	   AS tipo_moneda,"   +
					"       docto.fn_monto AS monto,"   +
					"       DECODE(NVL(prod_epo.cg_tipo_conversion, prod_nafin.cg_tipo_conversion), 'P', 'Dolar-Peso', 'Sin Conversion')  AS tipo_conversion,"   +
					"       DECODE(docto.fn_tipo_cambio, 1, ' ', docto.fn_tipo_cambio) AS tipo_cambio,"   +
					"       docto.fn_monto * docto.fn_tipo_cambio AS monto_valuado,"   +
					"       NVL(ct.cd_nombre, 'No Autorizado') AS ref_tasa,"   +
					"       DECODE(docto_sel.cg_rel_mat, '+', docto_sel.fn_puntos + docto_sel.fn_valor_tasa, '-',"   +
					"       docto_sel.fn_valor_tasa - docto_sel.fn_puntos, '*', docto_sel.fn_puntos * docto_sel.fn_valor_tasa, '/',"   +
					"       docto_sel.fn_valor_tasa / docto_sel.fn_puntos)  AS tasa_interes,"   +
					"       docto.ig_plazo_credito AS plazo,"   +
					"       TO_CHAR(docto.df_fecha_venc_credito, 'DD/MM/YYYY') AS fch_vencimiento,"   +
					"       docto_sel.fn_importe_interes AS monto_interes,"   +
					"       tci.cd_descripcion AS tipo_cobro_inte,"   +
					"       ed.cd_descripcion  AS estatus_soli,"   +
					"       TO_CHAR(camb_estatus.dc_fecha_cambio, 'DD/MM/YYYY') AS fecha_pago,"   +
					"       camb_estatus.cg_numero_pago  AS num_pago,"   +
					"       moneda.ic_moneda"   +
					"  FROM dis_documento           docto "   +
					"		,dis_docto_seleccionado docto_sel"   +
					"		,dis_solicitud          soli"   +
					"		,com_acuse3			 	ca3"   +
					"		,dis_cambio_estatus     camb_estatus"   +
					"		,com_linea_credito	   	lin_cred"   +
					"		,comcat_pyme            pyme"   +
					"		,comcat_epo             epo"   +
					"		,comrel_producto_epo    prod_epo"   +
					"		,comcat_producto_nafin  prod_nafin"   +
					"		,comcat_moneda          moneda"   +
					"		,comcat_tipo_cobro_interes tci"   +
					"		,comcat_estatus_docto ed"   +
					"		,comcat_tasa ct"   +
					"  WHERE docto.ic_documento = docto_sel.ic_documento (+)"   +
					" 	AND docto.ic_epo = epo.ic_epo"   +
					" 	AND docto.ic_pyme = pyme.ic_pyme"   +
					" 	AND docto.ic_epo  = prod_epo.ic_epo"   +
					" 	AND docto.ic_documento = camb_estatus.ic_documento"   +
					" 	AND docto.ic_linea_credito = lin_cred.ic_linea_credito (+)"   +
					" 	AND docto_sel.ic_documento = soli.ic_documento (+)"   +
					" 	AND docto.ic_estatus_docto = ed.ic_estatus_docto"   +
					" 	AND soli.cc_acuse = ca3.cc_acuse (+)"   +
					" 	AND camb_estatus.dc_fecha_cambio IN(SELECT MAX(dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_documento = docto.ic_documento)"   +
					" 	AND lin_cred.ic_moneda = moneda.ic_moneda"   +
					" 	AND lin_cred.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes"   +
					" 	AND prod_epo.ic_producto_nafin = prod_nafin.ic_producto_nafin"   +
					" 	AND soli.ic_tasaif = ct.ic_tasa "   +
					" 	AND prod_nafin.ic_producto_nafin = 4"   +
					"	AND lin_cred.ic_if = "+lsIntermediario+
					" 	AND epo.cs_habilitado = 'S' " +
					condicion.toString()  );

			if("D".equals(tipo_credito))
				qryAux = qryDM.toString();
			else if("C".equals(tipo_credito))
				qryAux = qryCCC.toString();
			else
				qryAux = qryDM.toString()+ " UNION ALL "+qryCCC.toString();

		}catch(Exception e){
			System.out.println("ConsPagosCreditosIfDist::getDocumentQueryFileException "+e);
		}
		return qryAux.toString();
	}


	public void setConditions(List conditions) {
		this.conditions = conditions;
	}


	public List getConditions() {
		return conditions;
	}


	public void set_conditions(List conditions) {
		this.conditions = conditions;
	}


	public List get_conditions() {
		return conditions;
	}


	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}


	public String getIc_epo() {
		return ic_epo;
	}


	public void setCtCredito(String ctCredito) {
		this.ctCredito = ctCredito;
	}


	public String getCtCredito() {
		return ctCredito;
	}


	public void setIcDist(String icDist) {
		this.icDist = icDist;
	}


	public String getIcDist() {
		return icDist;
	}


	public void setNumCred(String numCred) {
		this.numCred = numCred;
	}


	public String getNumCred() {
		return numCred;
	}


	public void setFechaOper1(String fechaOper1) {
		this.fechaOper1 = fechaOper1;
	}


	public String getFechaOper1() {
		return fechaOper1;
	}


	public void setFechaOper2(String fechaOper2) {
		this.fechaOper2 = fechaOper2;
	}


	public String getFechaOper2() {
		return fechaOper2;
	}


	public void setMonto1(String monto1) {
		this.monto1 = monto1;
	}


	public String getMonto1() {
		return monto1;
	}


	public void setMonto2(String monto2) {
		this.monto2 = monto2;
	}


	public String getMonto2() {
		return monto2;
	}


	public void setFechaVenc1(String fechaVenc1) {
		this.fechaVenc1 = fechaVenc1;
	}


	public String getFechaVenc1() {
		return fechaVenc1;
	}


	public void setTipoCoInt(String tipoCoInt) {
		this.tipoCoInt = tipoCoInt;
	}


	public String getTipoCoInt() {
		return tipoCoInt;
	}


	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}


	public String getEstatus() {
		return estatus;
	}


	public void setFechaPago1(String fechaPago1) {
		this.fechaPago1 = fechaPago1;
	}


	public String getFechaPago1() {
		return fechaPago1;
	}


	public void setFechaPago2(String fechaPago2) {
		this.fechaPago2 = fechaPago2;
	}


	public String getFechaPago2() {
		return fechaPago2;
	}


	public void setFechaVenc2(String fechaVenc2) {
		this.fechaVenc2 = fechaVenc2;
	}


	public String getFechaVenc2() {
		return fechaVenc2;
	}


	public void setSesIf(String sesIf) {
		this.sesIf = sesIf;
	}


	public String getSesIf() {
		return sesIf;
	}

}