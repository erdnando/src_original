package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsTasasGeneralesNafin implements IQueryGeneratorRegExtJS{

  /*******************************************************************
	* Variables utilizadas en los metodos de IQueryGeneratorRegExtJS	*
	*******************************************************************/
	private static final Log log = ServiceLocator.getInstance().getLog(ConsTasasGeneralesNafin.class);

	private List conditions;
	private List listaParametrosTasas;
	private String encabezado;
	private String gridPorEsquemaVisible;

	public ConsTasasGeneralesNafin() {}
	
	public String getDocumentQuery(){ return null; }

	public String getDocumentSummaryQueryForIds(List ids){ return null; }
	
	public String getAggregateCalculationQuery(){ return null; }
	/**
	 * Este m�todo en particular realiza la consulta de tasas fijadas por Nafin 
	 * en el esquema general, esta consulta no requiere paginaci�n
	 * @return String 
	 */
	public String getDocumentQueryFile(){
			
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();
		log.info("***** getDocumentQueryFile (E) *****");	
		try{
			qrySentencia.append(
				"select cm.cd_nombre as MONEDA, ct.cd_nombre as TIPO_TASA, " +
				" 		  ct.ic_tasa as CVE_TASA, cp.cg_descripcion as PLAZO, " +
				"       mt.fn_valor as VALOR, tg.cg_rel_mat as REL_MAT, " +
				"       tg.fn_puntos as PUNTOS, " +
				"       DECODE (tg.cg_rel_mat," +
				"		  	'+', (mt.fn_valor + tg.fn_puntos)," +
				"			'-', (mt.fn_valor - tg.fn_puntos)," +
				"			'*', (mt.fn_valor * tg.fn_puntos)," +
				"			'/', (mt.fn_valor / tg.fn_puntos)," +
				"			0) as TASA_PISO, " +
				"       TO_CHAR(tg.df_captura,'dd/mm/yyyy') as FECHA," +
				" 		  cp.ic_plazo, cm.ic_moneda, cp.in_plazo_meses"+
				"       ,cp.in_plazo_dias "+
				" from com_tasa_general tg, com_mant_tasa mt, " +
				"      comcat_plazo cp, comcat_tasa ct, comcat_moneda cm, comrel_tasa_producto tp " +
				" where ct.ic_tasa = tg.ic_tasa" +
				"       and ct.ic_plazo = cp.ic_plazo" +
				"       and ct.ic_tasa = mt.ic_tasa" +
				"       and mt.ic_tasa = tp.ic_tasa" +
				"       and ct.ic_tasa = tp.ic_tasa" +
				"       and tg.ic_producto_nafin = tp.ic_producto_nafin "+
				"        and tp.ic_producto_nafin = 4"+//+icProductoNafin+" "+
				"        and cm.ic_moneda = ct.ic_moneda" +
				"        and mt.dc_fecha = (select max(mi.dc_fecha)" +
				"                    from com_mant_tasa mi, comcat_tasa ti, comrel_tasa_producto ctp" +
				"                    where mi.ic_tasa = mt.ic_tasa" +
				"                    and ti.ic_tasa = mi.ic_tasa " +
				"                    and ti.ic_tasa = ctp.ic_tasa " +
				"                    and ctp.ic_producto_nafin = 4)"+//+icProductoNafin+")"+
				"       and tg.ic_tasa_general = (select max(ti.ic_tasa_general)" +
				"                    from com_tasa_general ti " +
				"                    where ti.ic_tasa = tg.ic_tasa "+
				"                    and ti.ic_producto_nafin = 4)"+//icProductoNafin+")"+
				" order by cm.ic_moneda, cp.in_plazo_meses");
					
		}catch(Exception e){
			log.warn("ConsTasasGeneralesNafin::getDocumentQueryFileException "+e);
		}
		log.info("***** getDocumentQueryFile(S) *****");
		return qrySentencia.toString();
	}
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){ 
	
		log.debug("********** crearCustomFile (E)**********");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		int total = 0;
		float numeroDecimal;
		String moneda = "";	
		String tipoTasa =  "";				
		String plazo = "";
		String valor = "";
		String relMat = "";
		String puntosAdicionales = "";
		String tasaAplicar = "";
		String fechaUltimaActualizacion = "";
		String cadena = "";
		String[] vectorTabla = null;
		
		if(tipo.equals("PDF")){
			try {
				Comunes comunes = new Comunes();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" 		","formas",ComunesPDF.RIGHT);
				pdfDoc.addText("Tasas fijadas por Nafin en el esquema general ","formas",ComunesPDF.LEFT);
			
				pdfDoc.setTable(8,100);
				pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de tasa ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Valor ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Rel. mat. ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Puntos adicionales ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa a aplicar ","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell("Fecha �ltima actualizaci�n ","celda01",ComunesPDF.CENTER);
				
				while (rs.next())	{		
					moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");	
					tipoTasa = (rs.getString("TIPO_TASA") == null) ? "" : rs.getString("TIPO_TASA");				
					plazo = (rs.getString("PLAZO") == null) ? "" : rs.getString("PLAZO");
					valor = (rs.getString("VALOR") == null) ? "" : rs.getString("VALOR");
					relMat = (rs.getString("REL_MAT") == null) ? "" : rs.getString("REL_MAT");
					puntosAdicionales = (rs.getString("PUNTOS") == null) ? "" : rs.getString("PUNTOS");
					tasaAplicar = (rs.getString("TASA_PISO") == null) ? "" : rs.getString("TASA_PISO");
					fechaUltimaActualizacion = (rs.getString("FECHA") == null) ? "" : rs.getString("FECHA");
					
					//Le doy formato decimal a los campos num�ricos
					numeroDecimal = Float.parseFloat(valor);
					valor = Comunes.formatoDecimal(numeroDecimal,2);
					numeroDecimal = Float.parseFloat(puntosAdicionales);
					puntosAdicionales = Comunes.formatoDecimal(numeroDecimal,2);
					numeroDecimal = Float.parseFloat(tasaAplicar);
					tasaAplicar = Comunes.formatoDecimal(numeroDecimal,2);
					
					pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);			
					pdfDoc.setCell(tipoTasa,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(plazo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(valor,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(relMat,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(puntosAdicionales,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(tasaAplicar,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaUltimaActualizacion,"formas",ComunesPDF.CENTER);
					
				}
				pdfDoc.addTable();
				
				/************************************************************
				 *		 Aqui se arma ta tabla de las tasas por esquema	   	*
				************************************************************/ 
				if(gridPorEsquemaVisible.equals("ok")){				
					//pdfDoc.addText(" 	","formas",ComunesPDF.RIGHT);
					pdfDoc.addText(encabezado,"formas",ComunesPDF.LEFT);
					pdfDoc.setTable(8,100);
					pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tipo de tasa ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Plazo ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Valor ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Rel. mat. ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Puntos adicionales ","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tasa a aplicar ","celda01",ComunesPDF.CENTER); 
					pdfDoc.setCell("Fecha �ltima actualizaci�n ","celda01",ComunesPDF.CENTER);
					
					for(int i=0; i<listaParametrosTasas.size(); i++){
						cadena = listaParametrosTasas.get(i).toString();
						cadena = cadena.substring(1,cadena.length()-1);
						vectorTabla = cadena.split(","); 
						moneda = vectorTabla[0];	
						tipoTasa = vectorTabla[1];				
						plazo = vectorTabla[3];
						valor = vectorTabla[4];
						relMat = vectorTabla[5];
						puntosAdicionales = vectorTabla[6];
						tasaAplicar = vectorTabla[7];
						fechaUltimaActualizacion = vectorTabla[8];
						
						//--Le doy formato decimal a los campos num�ricos
						numeroDecimal = Float.parseFloat(valor);
						valor = Comunes.formatoDecimal(numeroDecimal,2);
						numeroDecimal = Float.parseFloat(puntosAdicionales);
						puntosAdicionales = Comunes.formatoDecimal(numeroDecimal,2);
						numeroDecimal = Float.parseFloat(tasaAplicar);
						tasaAplicar = Comunes.formatoDecimal(numeroDecimal,2);
						
						pdfDoc.setCell(moneda.trim(),"formas",ComunesPDF.CENTER);			
						pdfDoc.setCell(tipoTasa.trim(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(plazo.trim(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(valor.trim(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(relMat.trim(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(puntosAdicionales.trim(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(tasaAplicar.trim(),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaUltimaActualizacion.trim(),"formas",ComunesPDF.CENTER);
					}
					pdfDoc.addTable();
				}
				/************************************************************
				 *		 Fin de la tabla de las tasas por esquema	   	      *
				 ************************************************************/ 					
				
				pdfDoc.endDocument();	
			
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo ", e);
			} finally {
				try {
					//rs.close();
				} catch(Exception e) {}
			}
			
		} else if(tipo.equals("CSV")){
								
			try {
							
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
				buffer = new BufferedWriter(writer);
			
				contenidoArchivo = new StringBuffer();	
				
				contenidoArchivo.append("Tasas fijadas por Nafin en el esquema general  \n");
				contenidoArchivo.append("Moneda, Tipo de tasa, Plazo, Valor, Rel. mat.,"+ 
												"Puntos adicionales, Tasa a aplicar, Fecha �ltima actualizaci�n  \n");			
			
				while (rs.next())	{	
					moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");	
					tipoTasa = (rs.getString("TIPO_TASA") == null) ? "" : rs.getString("TIPO_TASA");				
					plazo = (rs.getString("PLAZO") == null) ? "" : rs.getString("PLAZO");
					valor = (rs.getString("VALOR") == null) ? "" : rs.getString("VALOR");
					relMat = (rs.getString("REL_MAT") == null) ? "" : rs.getString("REL_MAT");
					puntosAdicionales = (rs.getString("PUNTOS") == null) ? "" : rs.getString("PUNTOS");
					tasaAplicar = (rs.getString("TASA_PISO") == null) ? "" : rs.getString("TASA_PISO");
					fechaUltimaActualizacion = (rs.getString("FECHA") == null) ? "" : rs.getString("FECHA");
					
					contenidoArchivo.append(moneda.replace(',',' ')+", "+
													tipoTasa.replace(',',' ')+", "+	
													plazo.replace(',',' ')+", "+
													valor.replace(',',' ')+", "+
													relMat.replace(',',' ')+", "+
													puntosAdicionales.replace(',',' ')+", "+
													tasaAplicar.replace(',',' ')+", "+
													fechaUltimaActualizacion.replace(',',' ')+"\n");		
				
					total++;
					if(total==1000){					
						total=0;	
						buffer.write(contenidoArchivo.toString());
						contenidoArchivo = new StringBuffer();//Limpio  
					}
				}//while(rs.next()){
			
				/************************************************************
				 *		 Aqui se arma ta tabla de las tasas por esquema	   	*
				************************************************************/ 
				if(gridPorEsquemaVisible.equals("ok")){
					contenidoArchivo.append("\n");
					contenidoArchivo.append(""+encabezado+"  \n");
					contenidoArchivo.append("Moneda, Tipo de tasa, Plazo, Valor, Rel. mat.,"+ 
													"Puntos adicionales, Tasa a aplicar, Fecha �ltima actualizaci�n  \n");	
					for(int i=0; i<listaParametrosTasas.size(); i++){
						cadena = listaParametrosTasas.get(i).toString();
						cadena = cadena.substring(1,cadena.length()-1);
						vectorTabla = cadena.split(",");
						
							moneda = (vectorTabla[0]).trim();	
						tipoTasa = (vectorTabla[1]).trim();				
						plazo = (vectorTabla[3]).trim();
						valor = (vectorTabla[4]).trim();
						relMat = (vectorTabla[5]).trim();
						puntosAdicionales = (vectorTabla[6]).trim();
						tasaAplicar = (vectorTabla[7]).trim();
						fechaUltimaActualizacion = (vectorTabla[8]).trim();
						
						contenidoArchivo.append(moneda.replace(',',' ')+", "+
														tipoTasa.replace(',',' ')+", "+	
														plazo.replace(',',' ')+", "+
														valor.replace(',',' ')+", "+
														relMat.replace(',',' ')+", "+
														puntosAdicionales.replace(',',' ')+", "+
														tasaAplicar.replace(',',' ')+", "+
														fechaUltimaActualizacion.replace(',',' ')+"\n");		
					
						total++;
						if(total==1000){					
							total=0;	
							buffer.write(contenidoArchivo.toString());
							contenidoArchivo = new StringBuffer();//Limpio  
						}
					}
				}
				/************************************************************
				 *		 Fin de la tabla de las tasas por esquema	   	      *
				 ************************************************************/ 	
				 
				buffer.write(contenidoArchivo.toString());
				buffer.close();	
				contenidoArchivo = new StringBuffer();//Limpio    
				
				
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo ", e);
			} finally {
				try {
					//rs.close();
				} catch(Exception e) {}
			
			}
						
		}
		log.debug("******** crearCustomFile (S)");
		return nombreArchivo;
	}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){	 
		return null;
	}
	
	
	
/************************************************************
 *								Get() y Set()   							*
 ************************************************************/

	public List getConditions(){
		return conditions; 
	}
	
	public void setListaParametrosTasas(List listaParametrosTasas) {
		this.listaParametrosTasas = listaParametrosTasas;
	}
	
	public List getListaParametrosTasas() {
		return listaParametrosTasas;
	}

	public String getEncabezado() {
		return encabezado;
	}

	public void setEncabezado(String encabezado) {
		this.encabezado = encabezado;
	}

	public String getGridPorEsquemaVisible() {
		return gridPorEsquemaVisible;
	}

	public void setGridPorEsquemaVisible(String gridPorEsquemaVisible) {
		this.gridPorEsquemaVisible = gridPorEsquemaVisible;
	}
	
}