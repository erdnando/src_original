package com.netro.distribuidores;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsDocOperCambios implements IQueryGeneratorRegExtJS {
	public ConsDocOperCambios() {}
	
	private List conditions;
	StringBuffer strQuery;
	
	private static final Log log = ServiceLocator.getInstance().getLog(ConsDetallePagInteres.class);
	
	private String numeroDocto;
	
	public String getAggregateCalculationQuery() {
		return "";
	}

	public String getDocumentQuery() {
		conditions	=	new ArrayList();
		strQuery	=	new StringBuffer();
		return strQuery.toString();
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
		strQuery = new StringBuffer();
		return strQuery.toString();
	}
	
	public String getDocumentQueryFile() {
		conditions	= new ArrayList();
		strQuery		= new StringBuffer();
		
		strQuery.append("SELECT distinct TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fech_cambio, "+
									" TO_CHAR (ce.df_fecha_emision_anterior, 'dd/mm/yyyy') AS fech_emi_ant, "+
									" ce.ct_cambio_motivo, ce.fn_monto_anterior, ce.fn_monto_nuevo, "+
									" TO_CHAR (ce.df_fecha_emision_nueva, 'dd/mm/yyyy') AS fech_emi_new, "+
									" TO_CHAR (ce.df_fecha_venc_anterior, 'dd/mm/yyyy') AS fech_venc_ant, "+
									" TO_CHAR (ce.df_fecha_venc_nueva, 'dd/mm/yyyy') AS fech_venc_new, "+
									" cce.cd_descripcion "+
									",TFA.cd_descripcion as modo_plazo_anterior"+
									",TFN.cd_descripcion as modo_plazo_nuevo"+
							"	FROM dis_cambio_estatus ce, comcat_cambio_estatus cce "+
								",comcat_tipo_financiamiento TFA,comcat_tipo_financiamiento TFN"+
							"	WHERE ic_documento = ? "+
							"	AND cce.ic_cambio_estatus = ce.ic_cambio_estatus"+
							"	AND ce.ic_tipo_finan_ant = TFA.ic_tipo_financiamiento(+)"+
							"	AND ce.ic_tipo_finan_nuevo = TFN.ic_tipo_financiamiento(+)");
							
		conditions.add(this.numeroDocto);
		
		return strQuery.toString();
	}
	
		public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs,String path,String tipo) {
			String nombreArchivo = "";
			return nombreArchivo;
		}
		public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg,String path,String tipo) {
		return "";
		}
		public List getConditions() {
			return conditions;
		}
		public String getNumeroDocto() {
			return numeroDocto;
		}
		public void setNumeroDocto(String numeroDocto) {
			this.numeroDocto=numeroDocto;
		}
}