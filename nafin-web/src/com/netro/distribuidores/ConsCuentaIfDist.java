package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsCuentaIfDist implements IQueryGeneratorRegExtJS   {

	private List conditions;
	StringBuffer strQuery;
	String  ic_financiera, ic_if, ic_moneda, ic_epo,
	  cg_rfc, ic_nafinElec, ic_pyme, cg_sucursal, cg_numcta, ic_plaza;
	int maximoNumeroRegistros= 1000;
	private static final Log log = ServiceLocator.getInstance().getLog(ConsCuentaIfDist.class);
	
	public ConsCuentaIfDist() {
	}
	
	
	
	public String getAggregateCalculationQuery() {
		return "";
   }
  
   public String getDocumentQuery() {
		
		
			return "";
  }
  
  public String getDocumentSummaryQueryForIds(List pageIds) {
		
		
		return "";
  }
  
  public String getDocumentQueryFile() {
  
  
  
  
		conditions =new ArrayList();
		log.info("getCuentaIfDist(E)");
		AccesoDB	con							= null;
		StringBuffer	qrySentencia  =new StringBuffer();
		StringBuffer	condicion  =new StringBuffer(); 		

		int 			cont						= 0;
		
					if(!"".equals(ic_epo)){
						condicion.append( " AND cbp.ic_epo = ? ");
						conditions.add(ic_epo);
					}
					if(!"".equals(cg_rfc)){
						condicion.append(" AND pym.cg_rfc = ? ");
						conditions.add(cg_rfc);
						
					}
					if(!"".equals(ic_pyme)){
						condicion.append( " AND pym.ic_pyme = ? ");
						conditions.add(ic_pyme);

					}
					if(!"".equals(ic_financiera)){
						condicion.append(" AND fin.ic_financiera = ? ");
						conditions.add(ic_financiera);
					}
					if(!"".equals(ic_nafinElec)){
						condicion.append( " AND crn.ic_nafin_electronico = ? ");
						conditions.add(ic_nafinElec);
					}
					if(!"".equals(cg_numcta)){
						condicion.append( " AND cbp.cg_numero_cuenta = ? ");
						conditions.add(cg_numcta);
					}
					if(!"".equals(ic_moneda)){
						condicion.append( " AND cbp.ic_moneda = ? ");
						conditions.add(ic_moneda);
					}
					if(!"".equals(cg_sucursal)){
						condicion.append( " AND cbp.cg_sucursal = ? ");
						conditions.add(cg_sucursal);
					}
					if(!"".equals(ic_plaza)){
						condicion.append( " AND cbp.ic_plaza = ? ");
						conditions.add(ic_plaza);
					}
				
				String qryTraeCuentas = 
					" SELECT epo.cg_razon_social AS nomepo,"   +
					"     fin.cd_nombre AS nomfin,"   +
					"cbp.ic_plaza AS ic_plaza, "+
					"     pym.cg_razon_social AS nompyme,"   +
					"     pym.cg_rfc AS rfc,"   +
					"     cbp.cg_sucursal AS sucursal,"   +
					"     mon.cd_nombre AS moneda,"   +
					
					"     cbp.cg_numero_cuenta AS nocta,"   +
					"     decode (plz.cd_descripcion, NULL, ' ', plz.cd_descripcion||', '||plz.cg_estado) AS plaza, "   +
					"     crn.ic_nafin_electronico AS ne "+
					"	  ,decode(aut.cs_vobo_if,'S','Si','No') AS autorizado	"+
					"	  ,cbp.ic_cuenta_bancaria AS ID	, epo.ic_epo AS ic_epo,  pym.ic_pyme AS ic_pyme "+
					" ,mon.ic_moneda AS ic_moneda,  fin.ic_financiera AS ic_banco"+
					/*"	  ,cbp.ic_cuenta_bancaria AS ID, epo.ic_epo AS ic_epo,  pym.ic_pyme AS ic_pyme "+
					" ,mon.ic_moneda AS ic_moneda,  fin.ic_financiera AS ic_banco"+*/
					" FROM comrel_cuenta_bancaria_x_prod cbp, com_cuenta_aut_x_prod aut, comcat_epo epo, "   +
					" comcat_financiera fin, comcat_pyme pym, comcat_moneda mon, comcat_plaza plz, "   +
					" comrel_nafin crn "   +
					" WHERE cbp.ic_epo = epo.ic_epo "   +
					" AND cbp.ic_cuenta_bancaria = aut.ic_cuenta_bancaria(+) "+
					" AND cbp.ic_financiera = fin.ic_financiera (+) "   +
					" AND cbp.ic_pyme = pym.ic_pyme "   +
					" AND cbp.ic_moneda = mon.ic_moneda "   +
					" AND cbp.ic_plaza = plz.ic_plaza "   +
					" AND crn.IC_EPO_PYME_IF = pym.ic_pyme "   +
					" AND crn.CG_TIPO = 'P' "   +
					" AND cbp.ic_producto_nafin = 4 "+condicion.toString()+" ORDER BY 1"  ;
//out.println("<br>qryTraeCuentas: "+qryTraeCuentas);
				
		
		
	
					
		
   		
			log.debug(" qrySentencia "+qryTraeCuentas);
			log.debug("  "+conditions);
			return qryTraeCuentas;
  }
  
  
  /**
		* En este m�todo se debe realizar la implementaci�n de la generaci�n del archivo
		* con base en el resulset que se recibe como par�metro. El ResulSet es el resultado
		* de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
		* @param request HttpRequest empleado principalmente para obtener el objeto session
		* @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
		* @param path Ruta f�sica donde se generar� el archivo
		* @param tipo cadena que identifica el tipo o variante de archivo que va a generar.
		* @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo)
	{
		String linea = "";
		String nombreArchivo = "";
		String espacio = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
      
		try {
		 if(tipo.equals("CSV")) {
		  linea = "EPO,Banco de servicio,Distribuidor,RFC,Sucursal,Moneda,No. de Cuenta,Plaza,Autorizado\n";
		  nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
		  writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
		  buffer = new BufferedWriter(writer);
		  buffer.write(linea);
		  while (rs.next()) {
		  
					String razonEpo = (rs.getString("nomepo") == null) ? "" : rs.getString("nomepo");
					String nomBanco = (rs.getString(2) == null) ? "" : rs.getString(2);
					String plaza2 = (rs.getString("plaza") == null) ? "" : rs.getString("plaza");
					String rfc = (rs.getString(4) == null) ? "" : rs.getString(4);
					String sucursal = (rs.getString(5) == null) ? "" : rs.getString(5);
					String moneda = (rs.getString(6) == null) ? "" : rs.getString(6);
					String nocta = (rs.getString(7) == null) ? "" : rs.getString(7);
					String plaza = (rs.getString(8) == null) ? "" : rs.getString(8);
					String autorizado = (rs.getString("autorizado") == null) ? "" : rs.getString("autorizado");

						
					linea = razonEpo.replace(',',' ') + ", " + 
								nomBanco.replace(',',' ') + ", " +
								//razonPyme.replace(',',' ') + ", " +
								rfc.replace(',',' ') + ", " +
								sucursal.replace(',',' ') + ", " +
								moneda.replace(',',' ') + ", " +
								nocta.replace(',',' ') + ", " +
								plaza.replace(',',' ') + ","+
								plaza2.replace(',',' ')+","+
								autorizado+ "\n " ;
								
					buffer.write(linea);
		  }
		  buffer.close();
		 }
		 else if(tipo.equals("PDF")){
		  HttpSession session = request.getSession();
		  nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
		  ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
		  String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		  String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		  String diaActual = fechaActual.substring(0,2);
		  String mesActual = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		  String anioActual = fechaActual.substring(6,10);
		  String horaActual = new SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
		  pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		  session.getAttribute("iNoNafinElectronico").toString(),
		  (String)session.getAttribute("sesExterno"),
		  (String)session.getAttribute("strNombre"),
		  (String)session.getAttribute("strNombreUsuario"),
		  (String)session.getAttribute("strLogo"),
		  (String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
		  pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual, "formas",ComunesPDF.RIGHT);
		  pdfDoc.setTable(9,100);
		  pdfDoc.setCell("Nombre EPO", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setCell("Banco de servicio", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setCell("Distribuidor", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setCell("RFC", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setCell("Sucursal", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setCell("Moneda", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setCell("No. de Cuenta", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setCell("Plaza", "celda01",ComunesPDF.CENTER);
		  pdfDoc.setCell("Autorizado", "celda01",ComunesPDF.CENTER);
		  
		  while (rs.next()) {
					String razonEpo = (rs.getString(1) == null) ? "" : rs.getString(1);
					String nomBanco = (rs.getString(2) == null) ? "" : rs.getString(2);
					String razonPyme = (rs.getString(4) == null) ? "" : rs.getString(4);
					String rfc = (rs.getString(5) == null) ? "" : rs.getString(5);
					String sucursal = (rs.getString(6) == null) ? "" : rs.getString(6);
					String moneda = (rs.getString(7) == null) ? "" : rs.getString(7);
					String nocta = (rs.getString(8) == null) ? "" : rs.getString(8);
					String plaza = (rs.getString(9) == null) ? "" : rs.getString(9);
					String autorizado = (rs.getString("autorizado") == null) ? "" : rs.getString("autorizado");
				
				pdfDoc.setCell(razonEpo,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(nomBanco,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(razonPyme,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(rfc,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(sucursal,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(nocta,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(plaza,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(autorizado,"formas",ComunesPDF.CENTER);

			}
		  pdfDoc.addTable();
		  pdfDoc.endDocument();
		 
		 }
		}
		catch (Throwable e) {
			throw new AppException("Error al generar el archivo",e);
		}
		finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
		
		return nombreArchivo;
   }
	
	
	/** En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va a generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	
	public List getConditions() {
		return conditions;
	}


	public void setStrQuery(StringBuffer strQuery) {
		this.strQuery = strQuery;
	}


	public StringBuffer getStrQuery() {
		return strQuery;
	}


	public void set_strQuery(StringBuffer strQuery) {
		this.strQuery = strQuery;
	}


	public StringBuffer get_strQuery() {
		return strQuery;
	}


	public void setIc_financiera(String ic_financiera) {
		this.ic_financiera = ic_financiera;
	}


	public String getIc_financiera() {
		return ic_financiera;
	}


	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}


	public String getIc_if() {
		return ic_if;
	}


	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}


	public String getIc_moneda() {
		return ic_moneda;
	}


	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}


	public String getIc_epo() {
		return ic_epo;
	}


	public void setCg_rfc(String cg_rfc) {
		this.cg_rfc = cg_rfc;
	}


	public String getCg_rfc() {
		return cg_rfc;
	}


	public void setIc_nafinElec(String ic_nafinElec) {
		this.ic_nafinElec = ic_nafinElec;
	}


	public String getIc_nafinElec() {
		return ic_nafinElec;
	}


	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}


	public String getIc_pyme() {
		return ic_pyme;
	}


	public void setCg_sucursal(String cg_sucursal) {
		this.cg_sucursal = cg_sucursal;
	}


	public String getCg_sucursal() {
		return cg_sucursal;
	}


	public void setCg_numcta(String cg_numcta) {
		this.cg_numcta = cg_numcta;
	}


	public String getCg_numcta() {
		return cg_numcta;
	}


	public void setIc_plaza(String ic_plaza) {
		this.ic_plaza = ic_plaza;
	}


	public String getIc_plaza() {
		return ic_plaza;
	}


	public void setMaximoNumeroRegistros(int maximoNumeroRegistros) {
		this.maximoNumeroRegistros = maximoNumeroRegistros;
	}


	public int getMaximoNumeroRegistros() {
		return maximoNumeroRegistros;
	}



}