package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class LiberaLimNaf implements IQueryGeneratorRegExtJS {

	private String cve_epo;
	private String no_nae;
	private String cve_if;
	private String fec_ven_ini;
	private String fec_ven_fin;
	private String pantalla;
	private String documentos;
	private String cmp_ref;

	public LiberaLimNaf() {
	}
	
	private static final Log log = ServiceLocator.getInstance().getLog(LiberaLimNaf.class);//Variable para enviar mensajes al log.
	
	public String getAggregateCalculationQuery()	{
		
		StringBuffer 	qrySentencia 	= new StringBuffer();
		return qrySentencia.toString();
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds)  {
		StringBuffer qrySentencia = new StringBuffer();
		return qrySentencia.toString();
	}
	
	public String getDocumentQuery()	{
	
		StringBuffer qrySentencia = new StringBuffer();
		
		log.debug("getDocumentQuery)");
		return qrySentencia.toString();
	}
	
	public String getDocumentQueryFile() {
		this.variablesBind = new ArrayList();
		StringBuffer 	qrySentencia 	= new StringBuffer();
    	StringBuffer	condicion 		= new StringBuffer();
	
		try{
			qrySentencia.append(
						" SELECT distinct s.ic_documento as IC_DOCUMENTO, i.cg_razon_social AS INT_FINAN, p.cg_razon_social AS DISTRIBUIDOR, d.ig_numero_docto AS NO_DOCTO, "+
						" ds.FN_VALOR_TASA AS TASA_FON, s.ig_numero_prestamo AS NO_PRESTAMO, m.cd_nombre AS MONEDA, m.ic_moneda AS IC_MONEDA, d.ct_referencia as REFERENCIA, "+
						" d.fn_monto AS MONTO_DESC, TO_CHAR (df_fecha_venc_credito, 'DD/MM/YYYY') AS FEC_VEN, d.ic_epo AS IC_EPO, 'N' AS SELECCION ");
						if(pantalla.equals("Con"))
							qrySentencia.append(", d.fn_monto AS MONTO_DESC, TO_CHAR (b.df_fecha_prepago, 'DD/MM/YYYY') AS FEC_PRE ");
						qrySentencia.append(
						" FROM dis_documento d,comcat_if i, comcat_moneda m, dis_solicitud s, comcat_pyme p, dis_docto_seleccionado ds ");
						if(pantalla.equals("Con"))
							qrySentencia.append(" , bit_bitacora_libif b ");
						qrySentencia.append(" WHERE s.ic_if = i.ic_if "+
						" AND m.ic_moneda = d.ic_moneda "+
						" AND s.ic_documento = d.ic_documento "+
						" AND d.ic_pyme = p.ic_pyme "+
						" AND ds.ic_documento = d.ic_documento ");
						if(pantalla.equals("Cap"))
							qrySentencia.append(" AND ic_estatus_docto = 4 ");
						else if(pantalla.equals("Con")){
							qrySentencia.append(" AND ds.ic_documento = b.ic_documento "+
													  " AND ic_estatus_docto = 11 ");
						}
						
			if(!cve_epo.equals("")){
				condicion.append("   AND d.ic_epo = ? ");
					this.variablesBind.add(cve_epo);
			}
			
			if(!no_nae.equals("")){
				condicion.append("   AND p.ic_pyme = ? ");
					this.variablesBind.add(no_nae);
			}
			
			if(!cve_if.equals("")){
				condicion.append("   AND i.ic_if = ? ");
					this.variablesBind.add(cve_if);
			}
			
			if(!"".equals(fec_ven_ini)&&!"".equals(fec_ven_fin)){
				condicion.append("    AND D.df_fecha_venc_credito >= TO_DATE (?, 'dd/mm/yyyy') ");
				condicion.append("    AND D.df_fecha_venc_credito < TRUNC (TO_DATE (?, 'dd/mm/yyyy') + 1) ");
				this.variablesBind.add(fec_ven_ini);
				this.variablesBind.add(fec_ven_fin);
			}
			
			qrySentencia.append(condicion.toString());
		
			qrySentencia.append("   AND s.ic_if IN( SELECT DISTINCT ci.ic_if "+
																 " FROM comcat_if ci, comrel_if_epo_x_producto rie, comcat_epo epo "+
																 " WHERE ci.cs_habilitado = 'S' "+
																 " AND rie.ic_if = ci.ic_if "+
																 " AND rie.ic_epo = epo.ic_epo "+
																 " AND rie.ic_epo = ? ");
																 this.variablesBind.add(cve_epo);
																 if(!cve_if.equals("")){
																	qrySentencia.append("   AND ci.ic_if = ? ");
																	this.variablesBind.add(cve_if);
																 }
																 qrySentencia.append(" AND rie.ic_producto_nafin = 4 )");
			
			if (documentos!=null && !documentos.equals("")){
				qrySentencia.append(" and d.ic_documento  in( "+documentos+") ");				
			} 
			
		}catch (Exception e){
			log.debug("LiberaLimNaf:: getDocumentQueryException "+e);
		}
	
		log.debug("getDocumentQueryFile(S) "+qrySentencia.toString());
		return qrySentencia.toString();
	}
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String nombreArchivo = "";
		StringBuffer 	contenidoArchivo 	= new StringBuffer();
		CreaArchivo 	archivo 			= new CreaArchivo();
		
		int registros = 0;
		String in_fin = "", dist= "", no_docto = "", tasa= "", no_prest="", moneda= "",  mon_desc = "", fec_ven= "", fec_pre ="", ref="";
		
		if ("CSV".equals(tipo)) {
			try {
				
				while(rs.next()){
						if(registros==0) {
							if(!cmp_ref.equals("")){
								contenidoArchivo.append(" Distribuidor"+ "," +
												  " N�mero de documento"+ "," +
												  " Tasa de Fondeo"+ "," +
												  " N�mero de Prestamo"+ "," +
												  " Moneda"+ "," +
												  " Monto a Descontar"+ "," +
												  " Fecha de Vencimiento del Cr�dito"+ "," +
												  " Fecha de Prepago"+ "," +
												  " Referencia"+ "," +"\n");
							}else{
								contenidoArchivo.append(" Intermediario Financiero"+ "," +
												  " Distribuidor"+ "," +
												  " N�mero de documento"+ "," +
												  " Tasa de Fondeo"+ "," +
												  " N�mero de Prestamo"+ "," +
												  " Moneda"+ "," +
												  " Monto a Descontar"+ "," +
												  " Fecha de Vencimiento del Cr�dito"+ "," +
												  " Fecha de Prepago"+ "," +"\n");
							}
							
						}
						registros++;
						if(!cmp_ref.equals("")){
							dist = (rs.getString("DISTRIBUIDOR") == null) ? "" : rs.getString("DISTRIBUIDOR");
							no_docto= (rs.getString("NO_DOCTO") == null) ? "" : rs.getString("NO_DOCTO");
							tasa=(rs.getString("TASA_FON") == null)?"":rs.getString("TASA_FON");
							no_prest= (rs.getString("NO_PRESTAMO") == null) ? "" : rs.getString("NO_PRESTAMO");
							moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
							mon_desc= (rs.getString("MONTO_DESC") == null) ? "" : rs.getString("MONTO_DESC");
							fec_ven =(rs.getString("FEC_VEN") == null) ? "" : rs.getString("FEC_VEN");
							fec_pre =(rs.getString("FEC_PRE") == null) ? "" : rs.getString("FEC_PRE");
							ref = (rs.getString("REFERENCIA") == null) ? "" : rs.getString("REFERENCIA");
							
							contenidoArchivo.append( dist.replaceAll(",","") +","+
                                     no_docto.replaceAll(",","") +","+
                                     tasa.replaceAll(",","") +","+
                                     no_prest.replaceAll(",","") +","+
                                     moneda.replaceAll(",","") +","+
                                     mon_desc.replaceAll(",","") +","+
												 fec_ven.replaceAll(",","") +","+
                                     fec_pre.replaceAll(",","") +
												 ref.replaceAll(",","") +"\n");
						}else{
							in_fin = (rs.getString("INT_FINAN") == null) ? "" : rs.getString("INT_FINAN");
							dist = (rs.getString("DISTRIBUIDOR") == null) ? "" : rs.getString("DISTRIBUIDOR");
							no_docto= (rs.getString("NO_DOCTO") == null) ? "" : rs.getString("NO_DOCTO");
							tasa=(rs.getString("TASA_FON") == null)?"":rs.getString("TASA_FON");
							no_prest= (rs.getString("NO_PRESTAMO") == null) ? "" : rs.getString("NO_PRESTAMO");
							moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
							mon_desc= (rs.getString("MONTO_DESC") == null) ? "" : rs.getString("MONTO_DESC");
							fec_ven =(rs.getString("FEC_VEN") == null) ? "" : rs.getString("FEC_VEN");
							fec_pre =(rs.getString("FEC_PRE") == null) ? "" : rs.getString("FEC_PRE");	

							contenidoArchivo.append( in_fin.replaceAll(",","") +","+
                                     dist.replaceAll(",","") +","+
                                     no_docto.replaceAll(",","") +","+
                                     tasa.replaceAll(",","") +","+
                                     no_prest.replaceAll(",","") +","+
                                     moneda.replaceAll(",","") +","+
                                     mon_desc.replaceAll(",","") +","+
												 fec_ven.replaceAll(",","") +","+
                                     fec_pre.replaceAll(",","") +"\n");
						}
						
					}

					if(registros >= 1){
						if(!archivo.make(contenidoArchivo.toString(),path, ".csv")){
							nombreArchivo="ERROR";
						}else{
							nombreArchivo = archivo.nombre;
						}
					}
				
			} catch (Throwable e) {
					throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
			
		}else if("PDF".equals(tipo)){
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
	
				String meses[]	=	{"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	=	new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual	=	fechaActual.substring(0,2);
				String mesActual	=	meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual	=	fechaActual.substring(6,10);
				String horaActual	=	new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),
				(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + "-----------------------------" +
				horaActual, "formas", ComunesPDF.CENTER);
				pdfDoc.addText("  ", "formas", ComunesPDF.CENTER);
				if(cmp_ref.equals("")){
					pdfDoc.addText("Se realiz� la liberaci�n de los vencimientos de los siguientes Intermediarios Financieros", "formas", ComunesPDF.CENTER);
					pdfDoc.addText("  ", "formas", ComunesPDF.CENTER);
				}
				
				pdfDoc.setTable(8, 100);
				if(!cmp_ref.equals("")){
					pdfDoc.setCell("Distribuidor","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tasa de Fondeo","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Prestamo","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Vencimiento del Cr�dito","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Referencia","celda01",ComunesPDF.CENTER);
				}else{
					pdfDoc.setCell("Intermediario Financiero","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Distribuidor","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de documento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tasa de Fondeo","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("N�mero de Prestamo","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Vencimiento del Cr�dito","celda01",ComunesPDF.CENTER);
				}
			
				while (rs.next()) {
					if(!cmp_ref.equals("")){
						dist = (rs.getString("DISTRIBUIDOR") == null) ? "" : rs.getString("DISTRIBUIDOR");
						no_docto= (rs.getString("NO_DOCTO") == null) ? "" : rs.getString("NO_DOCTO");
						tasa=(rs.getString("TASA_FON") == null)?"":rs.getString("TASA_FON");
						no_prest= (rs.getString("NO_PRESTAMO") == null) ? "" : rs.getString("NO_PRESTAMO");
						moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
						mon_desc= (rs.getString("MONTO_DESC") == null) ? "" : rs.getString("MONTO_DESC");
						fec_ven =(rs.getString("FEC_VEN") == null) ? "" : rs.getString("FEC_VEN");
						ref = (rs.getString("REFERENCIA") == null) ? "" : rs.getString("REFERENCIA");
					
						pdfDoc.setCell(dist,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(no_docto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(Comunes.formatoDecimal(tasa,4)+"%","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(no_prest,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(mon_desc,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(fec_ven,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(ref,"formas",ComunesPDF.CENTER);
					}else{
						in_fin = (rs.getString("INT_FINAN") == null) ? "" : rs.getString("INT_FINAN");
						dist = (rs.getString("DISTRIBUIDOR") == null) ? "" : rs.getString("DISTRIBUIDOR");
						no_docto= (rs.getString("NO_DOCTO") == null) ? "" : rs.getString("NO_DOCTO");
						tasa=(rs.getString("TASA_FON") == null)?"":rs.getString("TASA_FON");
						no_prest= (rs.getString("NO_PRESTAMO") == null) ? "" : rs.getString("NO_PRESTAMO");
						moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
						mon_desc= (rs.getString("MONTO_DESC") == null) ? "" : rs.getString("MONTO_DESC");
						fec_ven =(rs.getString("FEC_VEN") == null) ? "" : rs.getString("FEC_VEN");
					
						pdfDoc.setCell(in_fin,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(dist,"formas",ComunesPDF.LEFT);
						pdfDoc.setCell(no_docto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(Comunes.formatoDecimal(tasa,4)+"%","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(no_prest,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(mon_desc,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(fec_ven,"formas",ComunesPDF.CENTER);
					}
				}
				
				pdfDoc.addTable();
				pdfDoc.endDocument();
			
			}catch (Throwable e) {
				throw new AppException("Error al generar el archivo",e);
			}
		}
		
		return nombreArchivo;
	}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		log.debug("crearCustomFile (E)");

		return nombreArchivo;
	}
	
	public List getConditions() {
		log.debug("*************getConditions=" + variablesBind);
		return variablesBind;
	}
	private ArrayList variablesBind = null;

	/**
	 * METODOS GET & SET DE ATRIBUTOS DE CLASE
	 */

	public void setCve_epo(String cve_epo) {
		this.cve_epo = cve_epo;
	}


	public String getCve_epo() {
		return cve_epo;
	}


	public void setNo_nae(String no_nae) {
		this.no_nae = no_nae;
	}


	public String getNo_nae() {
		return no_nae;
	}


	public void setCve_if(String cve_if) {
		this.cve_if = cve_if;
	}


	public String getCve_if() {
		return cve_if;
	}


	public void setFec_ven_ini(String fec_ven_ini) {
		this.fec_ven_ini = fec_ven_ini;
	}


	public String getFec_ven_ini() {
		return fec_ven_ini;
	}


	public void setFec_ven_fin(String fec_ven_fin) {
		this.fec_ven_fin = fec_ven_fin;
	}


	public String getFec_ven_fin() {
		return fec_ven_fin;
	}
	
	public void set_Pantalla(String pantalla){
		this.pantalla = pantalla;
	}
	
	public String getDocumentos() {
		return documentos;
	}

	public void setDocumentos(String documentos) {
		this.documentos = documentos;
	}
	
	public String getReferencia() {
		return cmp_ref;
	}
	
	public void setReferencia(String referencia) {
		this.cmp_ref = referencia;
	}
	
}