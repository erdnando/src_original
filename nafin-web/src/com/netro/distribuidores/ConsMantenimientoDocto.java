package com.netro.distribuidores;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsMantenimientoDocto implements IQueryGeneratorRegExtJS {

	public ConsMantenimientoDocto() {  }

	private List conditions;
	StringBuffer query;


	
	private static final Log log = ServiceLocator.getInstance().getLog(ConsMantenimientoDocto.class);//Variable para enviar mensajes al log.
	private String ic_pyme;
	private String ic_moneda;
	private String modo_plazo;
	private String ic_estatus_docto;
	private String ig_numero_docto;
	private String cc_acuse;
	private String fecha_emision_de;
	private String fecha_emision_a;
	private String fecha_vto_de;
	private String fecha_vto_a;
	private String fecha_publicacion_de;
	private String fecha_publicacion_a;
	private String fn_monto_de;
	private String fn_monto_a;
	private String monto_con_descuento;
	private String solo_cambio;
	private String nOnegociable;
	private String ic_epo;

	public String getAggregateCalculationQuery() {
		conditions = new ArrayList();	
		query 		= new StringBuffer();
		 
		query.append(" SELECT  "+
		"m.ic_moneda as ic_moneda,"+
		" m.cd_nombre as MONEDA, "+
		"	count(*) as TOTAL_REGISTROS,  "+
		" Sum(d.fn_monto) as TOTAL_MONT0,  "+
		" sum(d.fn_monto * (fn_porc_descuento/100)) as TOTAL_MONTO_DESCUENTO,"+		
		" decode(m.ic_moneda,0,nvl( SUM (d.fn_monto) - SUM (d.fn_monto * (fn_porc_descuento / 100)) ,0),54,nvl( SUM (d.fn_monto) - SUM (d.fn_monto * (fn_porc_descuento / 100)),0))  as TOTAL_MONTO_VALUADO 	 "+
		" FROM dis_documento D"+
		" ,comcat_pyme P"+
		" ,comcat_moneda M"+
		" ,comrel_producto_epo PE"+
		" ,comcat_producto_nafin PN"+
		" ,com_tipo_cambio TC"+
		" ,comcat_tipo_financiamiento TF,comrel_clasificacion clas "+
		" ,comcat_estatus_docto ED ")	;
		
		if("S".equals(solo_cambio)&&solo_cambio!=null){
			query.append(" ,dis_cambio_estatus CE,comcat_tipo_financiamiento TF2 ");
		}	
		
		query.append(" WHERE D.ic_pyme = P.ic_pyme"+
		" AND D.ic_moneda = M.ic_moneda"+
		" AND PE.ic_epo = D.ic_epo"+
		" AND PN.ic_producto_nafin = PE.ic_producto_nafin"+
		" AND PN.ic_producto_nafin = 4"+
		" AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
		" AND M.ic_moneda = TC.ic_moneda"   +
		" AND TF.ic_tipo_financiamiento = D.ic_tipo_financiamiento"+
		" AND D.ic_estatus_docto = ED.ic_estatus_docto"+
		" AND D.ic_clasificacion = clas.ic_clasificacion (+) ");
		
		if("S".equals(solo_cambio)&&solo_cambio!=null){     
			query.append(" AND D.IC_DOCUMENTO = CE.IC_DOCUMENTO"+
			" AND CE.DC_FECHA_CAMBIO IN(SELECT MAX(DC_FECHA_CAMBIO) FROM DIS_CAMBIO_ESTATUS WHERE IC_DOCUMENTO = D.IC_DOCUMENTO)"+
			" AND CE.IC_TIPO_FINAN_ANT = TF2.IC_TIPO_FINANCIAMIENTO(+)");                
		}
	
		if(!"".equals(ic_epo)&&ic_epo!=null){
			query.append(" AND D.IC_EPO = ? ");
			conditions.add(ic_epo);
		}
		if(!"".equals(ic_pyme)&&ic_pyme!=null){
			query.append(" and D.ic_pyme = ?");
			conditions.add(ic_pyme);
		}
				
		if(!"".equals(ic_estatus_docto)&&ic_estatus_docto!=null){
			query.append(" and D.ic_estatus_docto =  ? ");
			conditions.add(ic_estatus_docto);
		}
		if("".equals(ic_estatus_docto)&& nOnegociable.equals("N")){ 
			query.append(" and D.ic_estatus_docto in (2, 9)");			
		}else  if("".equals(ic_estatus_docto)&& nOnegociable.equals("S")){ //Fodea 029-2010 Distribuidores Fase III
				query.append(" and D.ic_estatus_docto in (2, 9,1)");
    }      
		if(!"".equals(ic_moneda)&&ic_moneda!=null){
			query.append(" and M.ic_moneda = ? ");
			conditions.add(ic_moneda);
		}
		if(!"".equals(modo_plazo)&& modo_plazo!=null){
			query.append(" and TF.ic_tipo_financiamiento = ? ");
			conditions.add(modo_plazo);
		}
		if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null){
			query.append(" and D.ig_numero_docto = ? ");
			conditions.add(ig_numero_docto);
		}
		if(!"".equals(cc_acuse)&& cc_acuse!=null){
			query.append(" and D.cc_acuse = ? ");
			conditions.add(cc_acuse);
		}
		if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null){
			query.append(" and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))");
		}
		if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null){
			query.append("  and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
		}
		if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null){
			query.append(" and trunc(D.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))");
		}
		if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"N".equals(monto_con_descuento)){
			query.append(" and D.fn_monto between "+fn_monto_de+" and "+fn_monto_a);
		}
		if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"S".equals(monto_con_descuento)){
			query.append(" and (D.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de+" and "+fn_monto_a);
		}
		if(!"".equals(solo_cambio)&& solo_cambio!=null){
			query.append(" and D.ic_documento in (select ic_documento from dis_cambio_estatus)"); 
		}
			
		
		query.append(" group by m.ic_moneda, d.ic_moneda , m.cd_nombre  ");
		 
		log.debug("getAggregateCalculationQuery)"+query.toString());
		log.debug("getAggregateCalculationQuery)"+conditions);
		
		
			return query.toString();
 	}
		

	public String getDocumentQuery(){
		conditions = new ArrayList();	
		query 		= new StringBuffer();
			
		query.append(" SELECT  DISTINCT D.ic_documento"+
		" FROM dis_documento D"+
		" ,comcat_pyme P"+
		" ,comcat_moneda M"+
		" ,comrel_producto_epo PE"+
		" ,comcat_producto_nafin PN"+
		" ,com_tipo_cambio TC"+
		" ,comcat_tipo_financiamiento TF,comrel_clasificacion clas "+
		" ,comcat_estatus_docto ED ")	;
		
		if("S".equals(solo_cambio)&&solo_cambio!=null){
			query.append(" ,dis_cambio_estatus CE,comcat_tipo_financiamiento TF2 ");
		}	
		
		query.append(" WHERE D.ic_pyme = P.ic_pyme"+
		" AND D.ic_moneda = M.ic_moneda"+
		" AND PE.ic_epo = D.ic_epo"+
		" AND PN.ic_producto_nafin = PE.ic_producto_nafin"+
		" AND PN.ic_producto_nafin = 4"+
		" AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
		" AND M.ic_moneda = TC.ic_moneda"   +
		" AND TF.ic_tipo_financiamiento = D.ic_tipo_financiamiento"+
		" AND D.ic_estatus_docto = ED.ic_estatus_docto"+
		" AND D.ic_clasificacion = clas.ic_clasificacion (+) ");
		
		if("S".equals(solo_cambio)&&solo_cambio!=null){     
			query.append(" AND D.IC_DOCUMENTO = CE.IC_DOCUMENTO"+
			" AND CE.DC_FECHA_CAMBIO IN(SELECT MAX(DC_FECHA_CAMBIO) FROM DIS_CAMBIO_ESTATUS WHERE IC_DOCUMENTO = D.IC_DOCUMENTO)"+
			" AND CE.IC_TIPO_FINAN_ANT = TF2.IC_TIPO_FINANCIAMIENTO(+)");                
		}
	
		if(!"".equals(ic_epo)&&ic_epo!=null){
			query.append(" AND D.IC_EPO = ? ");
			conditions.add(ic_epo);
		}
		if(!"".equals(ic_pyme)&&ic_pyme!=null){
			query.append(" and D.ic_pyme = ?");
			conditions.add(ic_pyme);
		}
				
		if(!"".equals(ic_estatus_docto)&&ic_estatus_docto!=null){
			query.append(" and D.ic_estatus_docto =  ? ");
			conditions.add(ic_estatus_docto);
		}
		if("".equals(ic_estatus_docto)&& nOnegociable.equals("N")){ 
			query.append(" and D.ic_estatus_docto in (2, 9)");			
		}else  if("".equals(ic_estatus_docto)&& nOnegociable.equals("S")){ //Fodea 029-2010 Distribuidores Fase III
				query.append(" and D.ic_estatus_docto in (2, 9,1)");
    }      
		if(!"".equals(ic_moneda)&&ic_moneda!=null){
			query.append(" and M.ic_moneda = ? ");
			conditions.add(ic_moneda);
		}
		if(!"".equals(modo_plazo)&& modo_plazo!=null){
			query.append(" and TF.ic_tipo_financiamiento = ? ");
			conditions.add(modo_plazo);
		}
		if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null){
			query.append(" and D.ig_numero_docto = ? ");
			conditions.add(ig_numero_docto);
		}
		if(!"".equals(cc_acuse)&& cc_acuse!=null){
			query.append(" and D.cc_acuse = ? ");
			conditions.add(cc_acuse);
		}
		if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null){
			query.append(" and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))");
		}
		if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null){
			query.append("  and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
		}
		if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null){
			query.append(" and trunc(D.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))");
		}
		if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"N".equals(monto_con_descuento)){
			query.append(" and D.fn_monto between "+fn_monto_de+" and "+fn_monto_a);
		}
		if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"S".equals(monto_con_descuento)){
			query.append(" and (D.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de+" and "+fn_monto_a);
		}
		if(!"".equals(solo_cambio)&& solo_cambio!=null){
			query.append(" and D.ic_documento in (select ic_documento from dis_cambio_estatus)"); 
		}
		
				
		log.debug("getDocumentQuery)"+query.toString());
		log.debug("getDocumentQuery)"+conditions);
		
		return query.toString();
 	}
	
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
		query 		= new StringBuffer();
		
		query.append(" SELECT   P.cg_razon_social as NOMBRE_PYME "+
			" ,D.ig_numero_docto  as  NO_DOCUMENTO"+
			" ,D.ic_documento as IC_DOCUMENTO"+
			" ,D.cc_acuse as ACUSE"+
			" ,to_char(D.df_fecha_emision,'dd/mm/yyyy') as FECHA_EMISION"+
			" ,to_char(D.df_carga,'dd/mm/yyyy') as FECHA_PUBLICACION"+
			" ,to_char(D.df_fecha_venc,'dd/mm/yyyy') as FECHA_VENCIMIENTO"+
			" ,D.ig_plazo_docto as PLAZO_DOCTO"+
			" ,M.ic_moneda as IC_MONEDA"+
			" ,M.cd_nombre AS MONEDA"+
			" ,D.fn_monto as MONTO"+
			" ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
			" ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
			" ,D.ig_plazo_descuento as PLAZO_DESC "+
			" ,D.fn_porc_descuento as PORC_DESC"+
			" ,D.fn_monto * (fn_porc_descuento/100) as MONTO_DESC "+
			" ,TF.ic_tipo_financiamiento as IC_TIPO_FINANCIAMIENTO"+
			" ,TF.cd_descripcion as TIPO_FINANCIAMIENTO"+
			" ,ED.ic_estatus_docto  AS IC_ESTATUS"+
			" ,ED.cd_descripcion as ESTATUS"+
			", d.CG_VENTACARTERA as CG_VENTACARTERA "+
			" ,to_char(sysdate,'dd/mm/yyyy') as FECHA_MODIFICACION,clas.cg_descripcion as CATEGORIA");
         
			if("S".equals(solo_cambio)&&solo_cambio!=null){ 
				query.append(" ,to_char(df_fecha_emision_anterior,'dd/mm/yyyy') as df_fecha_emision_anterior"+
				" ,to_char(df_fecha_emision_nueva,'dd/mm/yyyy') as df_fecha_emision_nueva"+
				" ,to_char(df_fecha_venc_anterior,'dd/mm/yyyy') as df_fecha_venc_anterior"+
				" ,to_char(df_fecha_venc_nueva,'dd/mm/yyyy') as df_fecha_venc_nueva"+
				" ,fn_monto_anterior"+
				" ,fn_monto_nuevo"+
				" ,TF2.cd_descripcion as modo_plazo_anterior"+
				" ,TF.cd_descripcion as modo_plazo_nuevo");
			}else  if("N".equals(solo_cambio)&&solo_cambio!=null){ 	
				query.append(" ,'' as df_fecha_emision_anterior"+
				" ,'' as df_fecha_emision_nueva"+
				" ,'' as df_fecha_venc_anterior"+
				" ,'' as df_fecha_venc_nueva"+
				" ,'' as fn_monto_anterior"+
				" ,'' as fn_monto_nuevo"+
				" ,'' as modo_plazo_anterior"+
				" ,'' as modo_plazo_nuevo");						
			}
			
			query.append(" ,'' as FECHA_NUEVA_EMISION" +
			" ,'' as FECHA_NUEVA_VENCIMIENTO" +
			" ,'' as NUEVO_MONTO"+
			" ,'' as FECHA_LIMITE_DESC"+
			" ,'' as NUEVO_MODO_PLAZO");
			
			
         
			query.append("  FROM dis_documento D"+
				" ,comcat_pyme P"+
				" ,comcat_moneda M"+
				" ,comrel_producto_epo PE"+
				" ,comcat_producto_nafin PN"+
				" ,com_tipo_cambio TC"+
				" ,comcat_tipo_financiamiento TF,comrel_clasificacion clas "+
				" ,comcat_estatus_docto ED")	;
			
			if("S".equals(solo_cambio)&&solo_cambio!=null){ 
				query.append(" ,dis_cambio_estatus CE,comcat_tipo_financiamiento TF2");
			}	
								
      query.append(" WHERE D.ic_pyme = P.ic_pyme"+ 
				" AND D.ic_moneda = M.ic_moneda"+
				" AND PE.ic_epo = D.ic_epo"+
				" AND PN.ic_producto_nafin = PE.ic_producto_nafin"+
				" AND PN.ic_producto_nafin = 4"+
				" AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
				" AND M.ic_moneda = TC.ic_moneda"   +
				" AND TF.ic_tipo_financiamiento = D.ic_tipo_financiamiento"+
				" AND D.ic_estatus_docto = ED.ic_estatus_docto"+
				" AND D.ic_clasificacion = clas.ic_clasificacion (+) ");
				
			if(solo_cambio.equals("S")){       
				query.append(" AND D.IC_DOCUMENTO = CE.IC_DOCUMENTO"+
				" AND CE.DC_FECHA_CAMBIO IN(SELECT MAX(DC_FECHA_CAMBIO) FROM DIS_CAMBIO_ESTATUS WHERE IC_DOCUMENTO = D.IC_DOCUMENTO)"+
				" AND CE.IC_TIPO_FINAN_ANT = TF2.IC_TIPO_FINANCIAMIENTO(+)");                
			}
	
			if(!"".equals(ic_epo)&&ic_epo!=null){
				query.append(" AND D.IC_EPO = ? ");
				conditions.add(ic_epo);
			}
			if(!"".equals(ic_pyme)&&ic_pyme!=null){
				query.append(" and D.ic_pyme = ?");
				conditions.add(ic_pyme);
			}
			if(!"".equals(ic_estatus_docto)&&ic_estatus_docto!=null){
				query.append(" and D.ic_estatus_docto =  ? ");
				conditions.add(ic_estatus_docto);
			}
			
      if("".equals(ic_estatus_docto)&& nOnegociable.equals("N")){ 
      	query.append(" and D.ic_estatus_docto in (2, 9)");			
      }else  if("".equals(ic_estatus_docto)&& nOnegociable.equals("S")){ //Fodea 029-2010 Distribuidores Fase III
				query.append(" and D.ic_estatus_docto in (2, 9,1)");
      }      
			
			if(!"".equals(ic_moneda)&&ic_moneda!=null){
				query.append(" and M.ic_moneda = ? ");
				conditions.add(ic_moneda);
			}
			if(!"".equals(modo_plazo)&& modo_plazo!=null){
				query.append(" and TF.ic_tipo_financiamiento = ? ");
				conditions.add(modo_plazo);
			}
			
			if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null){
				query.append(" and D.ig_numero_docto = ? ");
				conditions.add(ig_numero_docto);
			}
			if(!"".equals(cc_acuse)&& cc_acuse!=null){
				query.append(" and D.cc_acuse = ? ");
				conditions.add(cc_acuse);
			}
						
			if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null){
				query.append(" and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))");
			}
			
			if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null){
				query.append("  and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
			}
			
			if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null){
				query.append(" and trunc(D.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))");
			}
			
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"N".equals(monto_con_descuento)){
				query.append(" and D.fn_monto between "+fn_monto_de+" and "+fn_monto_a);
			}
			
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"S".equals(monto_con_descuento)){
				query.append(" and (D.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de+" and "+fn_monto_a);
			}
			
			if(!"".equals(solo_cambio)&& solo_cambio!=null){
				query.append(" and D.ic_documento in (select ic_documento from dis_cambio_estatus)"); 
			}
						
		
			for(int i=0;i<pageIds.size();i++) {
				List lItem = (List)pageIds.get(i);
				if(i==0) {
					query.append(" AND D.ic_documento  IN ( ");
				}
				query.append("?");
				conditions.add(new Long(lItem.get(0).toString()));					
				if(i!=(pageIds.size()-1)) {
					query.append(",");
				} else {
					query.append(" ) ");
				}			
			}
				
		log.debug("getDocumentSummaryQueryForIds "+query.toString());
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
		
						
		return query.toString();
 	}
		
			
	public String getDocumentQueryFile(){
		conditions = new ArrayList();
		query = new StringBuffer();
		
		
					
		log.debug("getDocumentQueryFile "+query.toString());
		log.debug("getDocumentQueryFile)"+conditions);
	
		return query.toString();
 	} 		
	
	/**
	 * En este método se debe realizar la implementación de la generación de archivo
	 * con base en el resultset que se recibe como parámetro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generará el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
	
		return nombreArchivo;
	}
	
	/**
	 * En este método se debe realizar la implementación de la generación de archivo
	 * con base en el objeto Registros que recibe como parámetro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generará el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}

	
	public List getConditions() {
		return conditions;
	}

	public String getIc_pyme() {
		return ic_pyme;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}

	public String getIc_moneda() {
		return ic_moneda;
	}

	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}

	public String getModo_plazo() {
		return modo_plazo;
	}

	public void setModo_plazo(String modo_plazo) {
		this.modo_plazo = modo_plazo;
	}

	public String getIc_estatus_docto() {
		return ic_estatus_docto;
	}

	public void setIc_estatus_docto(String ic_estatus_docto) {
		this.ic_estatus_docto = ic_estatus_docto;
	}

	public String getIg_numero_docto() {
		return ig_numero_docto;
	}

	public void setIg_numero_docto(String ig_numero_docto) {
		this.ig_numero_docto = ig_numero_docto;
	}

	public String getCc_acuse() {
		return cc_acuse;
	}

	public void setCc_acuse(String cc_acuse) {
		this.cc_acuse = cc_acuse;
	}

	public String getFecha_emision_de() {
		return fecha_emision_de;
	}

	public void setFecha_emision_de(String fecha_emision_de) {
		this.fecha_emision_de = fecha_emision_de;
	}

	public String getFecha_emision_a() {
		return fecha_emision_a;
	}

	public void setFecha_emision_a(String fecha_emision_a) {
		this.fecha_emision_a = fecha_emision_a;
	}

	public String getFecha_vto_de() {
		return fecha_vto_de;
	}

	public void setFecha_vto_de(String fecha_vto_de) {
		this.fecha_vto_de = fecha_vto_de;
	}

	public String getFecha_vto_a() {
		return fecha_vto_a;
	}

	public void setFecha_vto_a(String fecha_vto_a) {
		this.fecha_vto_a = fecha_vto_a;
	}

	public String getFecha_publicacion_de() {
		return fecha_publicacion_de;
	}

	public void setFecha_publicacion_de(String fecha_publicacion_de) {
		this.fecha_publicacion_de = fecha_publicacion_de;
	}

	public String getFecha_publicacion_a() {
		return fecha_publicacion_a;
	}

	public void setFecha_publicacion_a(String fecha_publicacion_a) {
		this.fecha_publicacion_a = fecha_publicacion_a;
	}

	public String getFn_monto_de() {
		return fn_monto_de;
	}

	public void setFn_monto_de(String fn_monto_de) {
		this.fn_monto_de = fn_monto_de;
	}

	public String getFn_monto_a() {
		return fn_monto_a;
	}

	public void setFn_monto_a(String fn_monto_a) {
		this.fn_monto_a = fn_monto_a;
	}

	public String getMonto_con_descuento() {
		return monto_con_descuento;
	}

	public void setMonto_con_descuento(String monto_con_descuento) {
		this.monto_con_descuento = monto_con_descuento;
	}

	public String getSolo_cambio() {
		return solo_cambio;
	}

	public void setSolo_cambio(String solo_cambio) {
		this.solo_cambio = solo_cambio;
	}

	public String getNOnegociable() {
		return nOnegociable;
	}

	public void setNOnegociable(String nOnegociable) {
		this.nOnegociable = nOnegociable;
	}

	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}






	
	

}