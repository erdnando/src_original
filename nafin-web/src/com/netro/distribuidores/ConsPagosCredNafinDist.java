package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsPagosCredNafinDist implements IQueryGenerator ,IQueryGeneratorRegExtJS{
	public ConsPagosCredNafinDist(){}
	

	public String getAggregateCalculationQuery(HttpServletRequest request) {
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		String qryAux 	= "";	
		StringBuffer qryDM 		= new StringBuffer();
		StringBuffer qryCCC		= new StringBuffer();
		StringBuffer condicion  = new StringBuffer();

		String 	lsNumCredito    = (request.getParameter("txtNumCredito") 	  == null) ? ""  : request.getParameter("txtNumCredito");
		String 	tipo_credito   	= (request.getParameter("lstTipoCredito") 	  == null) ? ""  : request.getParameter("lstTipoCredito");
		String 	lsMoneda        = (request.getParameter("lstMoneda") 	      == null) ? ""  : request.getParameter("lstMoneda");
		String 	lsMontoCredIni  = (request.getParameter("txtMonto1")       	  == null) ? "0"  : (request.getParameter("txtMonto1").equals("")) ? "0" : request.getParameter("txtMonto1");
		String 	lsMontoCredFin  = (request.getParameter("txtMonto2")     	  == null) ? "0"  : (request.getParameter("txtMonto2").equals("")) ? "0" : request.getParameter("txtMonto2");
		String 	lsFchVenciIni   = (request.getParameter("txtFchVencimiento1") == null) ? ""   : request.getParameter("txtFchVencimiento1");
		String 	lsFchVenciFin   = (request.getParameter("txtFchVencimiento2") == null) ? ""   : request.getParameter("txtFchVencimiento2");
		String 	lsTipoCobroInte = (request.getParameter("lstTipoCobroInte")   == null) ? ""   : request.getParameter("lstTipoCobroInte");
		String 	lsIntermediario = (request.getParameter("lstIF")    		  == null) ? ""   : request.getParameter("lstIF");
		String 	lidAccion 		= (request.getParameter("hidAccion")    	  == null) ? ""   : request.getParameter("hidAccion");
		String 	lsEstatus       = (request.getParameter("lstEstatus")         == null) ? "5" : request.getParameter("lstEstatus");
		String 	lsFchPagoIni    = (request.getParameter("txtFchPago1")   	  == null) ? ""   : request.getParameter("txtFchPago1");
		String 	lsFchPagoFin    = (request.getParameter("txtFchPago2")   	  == null) ? ""   : request.getParameter("txtFchPago2");
		
		try {

		if (!lsNumCredito.equals("") )	
			condicion.append(" AND docto.ic_documento = " + lsNumCredito);
		
		if (!lsMoneda.equals("") )		
			condicion.append(" AND docto.ic_moneda = " + lsMoneda);
		
		if ( !lsMontoCredIni.equals("0"))		
			condicion.append(" AND docto.fn_monto >= " + lsMontoCredIni);
		if (!lsMontoCredFin.equals("0"))		
			condicion.append(" AND docto.fn_monto <= " + lsMontoCredFin);
		if (!lsFchVenciIni.equals(""))	
			condicion.append(" AND docto.df_fecha_venc_credito >= TO_DATE('"+lsFchVenciIni+"', 'DD/MM/YYYY') ");
		if (!lsFchVenciFin.equals(""))	
			condicion.append(" AND docto.df_fecha_venc_credito <= TO_DATE('"+lsFchVenciFin+"', 'DD/MM/YYYY') ");
		if (!lsTipoCobroInte.equals(""))
			condicion.append(" AND lin_cred.ic_tipo_cobro_interes = " + lsTipoCobroInte);
		if (!lsFchPagoIni.equals("") )	
			condicion.append(" AND camb_estatus.dc_fecha_cambio >= TO_DATE('"+lsFchPagoIni+"', 'DD/MM/YYYY') ");
		if (!lsFchPagoFin.equals("") )	
			condicion.append(" AND camb_estatus.dc_fecha_cambio >= TO_DATE('"+lsFchPagoFin+"', 'DD/MM/YYYY') ");
		if (!lsEstatus.equals("") )		
			condicion.append(" AND soli.ic_estatus_solic =  "+lsEstatus);
		else 							
			condicion.append(" AND soli.ic_estatus_solic in(5,11)");

		qryDM.append(
					"   SELECT /*+index(docto) index(docto_sel) use_nl(docto docto_sel soli camb_estatus lin_cred prod_epo epo moneda)*/ "   +
					" 		 moneda.ic_moneda"   +
					" 		 ,moneda.cd_nombre"   +
					"          ,SUM(docto_sel.fn_importe_recibir)"   +
					"          ,SUM(docto_sel.fn_importe_interes)"   +
					"   FROM dis_documento          docto"   +
					"    ,dis_docto_seleccionado    docto_sel"   +
					"    ,dis_solicitud             soli"   +
					"    ,dis_cambio_estatus        camb_estatus"   +
					"    ,dis_linea_credito_dm      lin_cred"   +
					"    ,comrel_producto_epo       prod_epo"   +
					"    ,comcat_epo				epo"   +
					"    ,comcat_moneda             moneda"   +
					"   WHERE docto.ic_documento = docto_sel.ic_documento"   +
					"  	AND docto.ic_documento = soli.ic_documento"   +
					"  	AND docto.ic_documento = camb_estatus.ic_documento"   +
					"     AND docto.ic_linea_credito_dm = lin_cred.ic_linea_credito_dm"   +
					"     AND docto.ic_epo  = prod_epo.ic_epo"   +
					"     AND prod_epo.ic_epo  = epo.ic_epo"   +
					"     AND lin_cred.ic_moneda = moneda.ic_moneda"   +
					"  	AND camb_estatus.dc_fecha_cambio IN(SELECT /*+index(dce)*/ MAX(dc_fecha_cambio) FROM dis_cambio_estatus dce WHERE ic_documento = docto.ic_documento)"   +
					"     AND prod_epo.ic_producto_nafin = 4"   +
					"     AND epo.cs_habilitado = 'S' "   +
					"	  AND lin_cred.ic_if = "+lsIntermediario+
					condicion.toString()+
					" GROUP BY moneda.ic_moneda,moneda.cd_nombre"   +
					" ORDER BY moneda.ic_moneda");

		qryCCC.append(
					"   SELECT /*+index(docto_sel) use_nl(docto docto_sel soli camb_estatus lin_cred prod_epo epo moneda)*/ "   +
					" 		 moneda.ic_moneda"   +
					" 		 ,moneda.cd_nombre"   +
					"          ,SUM(docto_sel.fn_importe_recibir)"   +
					"          ,SUM(docto_sel.fn_importe_interes)"   +
					"   FROM dis_documento          docto"   +
					"    ,dis_docto_seleccionado    docto_sel"   +
					"    ,dis_solicitud             soli"   +
					"    ,dis_cambio_estatus        camb_estatus"   +
					"    ,com_linea_credito      	lin_cred"   +
					"    ,comrel_producto_epo       prod_epo"   +
					"    ,comcat_epo 				epo"   +
					"    ,comcat_moneda             moneda"   +
					"   WHERE docto.ic_documento = docto_sel.ic_documento"   +
					"  	AND docto.ic_documento = soli.ic_documento"   +
					"  	AND docto.ic_documento = camb_estatus.ic_documento"   +
					"     AND docto.ic_linea_credito = lin_cred.ic_linea_credito"   +
					"     AND docto.ic_epo  = prod_epo.ic_epo"   +
					"     AND prod_epo.ic_epo  = epo.ic_epo"   +
					"     AND lin_cred.ic_moneda = moneda.ic_moneda"   +
					"  	AND camb_estatus.dc_fecha_cambio IN(SELECT /*+index(dce)*/ MAX(dc_fecha_cambio) FROM dis_cambio_estatus dce WHERE ic_documento = docto.ic_documento)"   +
					"     AND prod_epo.ic_producto_nafin = 4"   +
					"	  AND lin_cred.ic_if = "+lsIntermediario+
					"     AND epo.cs_habilitado = 'S'"   +
					condicion.toString()+
					" GROUP BY moneda.ic_moneda,moneda.cd_nombre"   +
					" ORDER BY moneda.ic_moneda");

			if("D".equals(tipo_credito))
				qryAux = qryDM.toString();
			else if("C".equals(tipo_credito))
				qryAux = qryCCC.toString();
			else
				qryAux = qryDM.toString()+ " UNION ALL "+qryCCC.toString();

		}catch(Exception e){

			System.out.println("ConsPagosCreditosNafinDist::getAggregateCalculationQuery "+e);
		}
		return qryAux.toString();
	}

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids) {
		int i=0;
		String qryAux 	= "";	
		StringBuffer qryDM 		= new StringBuffer();
		StringBuffer qryCCC		= new StringBuffer();
    	StringBuffer condicion	= new StringBuffer();
		String 	lsIntermediario	= (request.getParameter("lstIF")==null)?"":request.getParameter("lstIF");
		String 	tipo_credito   	= (request.getParameter("lstTipoCredito") 	  == null) ? ""  : request.getParameter("lstTipoCredito");
    
		condicion.append(" AND docto.ic_documento in (");
		i=0;
		for (Iterator it = ids.iterator(); it.hasNext();i++){
        	if(i>0){
				condicion.append(",");
			}
			condicion.append(" "+it.next().toString()+" ");
		}
		condicion.append(") ");

		qryDM.append(
					"  SELECT /*+ use_nl(docto,docto_sel,soli,camb_estatus,lin_cred,prod_epo,prod_nafin,moneda,epo,pyme,ci,ct,cti,ed)"   +
					"         index(docto cp_dis_documento_pk)"   +
					"         index(lin_cred in_dis_linea_credito_dm_01_nuk)"   +
					"         index(cti cp_comcat_tipo_cobro_inter_pk)*/ "   +
					"         docto.ic_documento     AS num_credito,"   +
					" 		epo.cg_razon_social    AS epo,"   +
					"         pyme.cg_razon_social   AS distribuidor,"   +
					" 		ci.cg_razon_social	   AS Intermediario,"   +
					"         'Descuento y/o Factoraje'  AS tipo_credito,"   +
					"         DECODE (NVL (prod_epo.cg_responsable_interes, prod_nafin.cg_responsable_interes),'E', 'EPO','D', 'Distribuidor','')"   +
					"             AS responsable_interes,"   +
					"         DECODE (NVL (prod_epo.cg_tipo_cobranza, prod_nafin.cg_tipo_cobranza),'D', 'Delegada','N', 'No Delegada','')"   +
					"                                AS tipo_cobranza,"   +
					"         TO_CHAR(soli.df_operacion, 'DD/MM/YYYY')"   +
					"                                AS fecha_operacion,"   +
					"         moneda.cd_nombre	   AS tipo_moneda,"   +
					"         docto_sel.fn_importe_recibir         AS monto,"   +
					"         DECODE(NVL(prod_epo.cg_tipo_conversion, prod_nafin.cg_tipo_conversion), 'P', 'Dolar-Peso', 'Sin Conversion')"   +
					"                                AS tipo_conversion,"   +
					"         docto.fn_tipo_cambio   AS tipo_cambio,"   +
					"         docto.fn_monto * docto.fn_tipo_cambio"   +
					"                               AS monto_valuado,"   +
					"         NVL(ct.cd_nombre, 'No Autorizado') AS ref_tasa,"   +
					"         DECODE(docto_sel.cg_rel_mat, '+', docto_sel.fn_puntos + docto_sel.fn_valor_tasa, '-',"   +
					"               docto_sel.fn_valor_tasa - docto_sel.fn_puntos, '*', docto_sel.fn_puntos * docto_sel.fn_valor_tasa, '/',"   +
					"               docto_sel.fn_valor_tasa / docto_sel.fn_puntos)  AS tasa_interes,"   +
					"         docto.ig_plazo_credito AS plazo,"   +
					"         TO_CHAR(docto.df_fecha_venc_credito, 'DD/MM/YYYY') AS fch_vencimiento,"   +
					"         docto_sel.fn_importe_interes AS monto_interes,"   +
					"         cti.cd_descripcion			 AS tipo_cobro_inte,"   +
					"         ed.ic_estatus_docto AS estatus_soli,"   +
					" 		camb_estatus.cg_numero_pago  AS num_pago,"   +
					" 		TO_CHAR(camb_estatus.dc_fecha_cambio, 'DD/MM/YYYY') AS fecha_pago"   +
					" 		,moneda.ic_moneda"   +
					"  FROM dis_documento          docto"   +
					"   ,dis_docto_seleccionado    docto_sel"   +
					"   ,dis_solicitud             soli"   +
					" 	 ,dis_cambio_estatus        camb_estatus"   +
					"   ,dis_linea_credito_dm      lin_cred"   +
					"   ,comrel_producto_epo       prod_epo"   +
					"   ,comcat_producto_nafin     prod_nafin"   +
					"   ,comcat_moneda             moneda"   +
					"   ,comcat_epo                epo"   +
					"   ,comcat_pyme               pyme"   +
					" 	 ,comcat_if				    	 ci"   +
					" 	 ,comcat_tasa			    	 ct"   +
					" 	 ,comcat_tipo_cobro_interes cti"   +
					" 	 ,comcat_estatus_docto			ed"   +
					"  WHERE docto.ic_documento = docto_sel.ic_documento"   +
					" 	  AND docto.ic_documento = soli.ic_documento"   +
					" 	  AND docto.ic_documento = camb_estatus.ic_documento"   +
					"    AND docto.ic_linea_credito_dm = lin_cred.ic_linea_credito_dm"   +
					"    AND docto.ic_epo = epo.ic_epo"   +
					"    AND docto.ic_pyme = pyme.ic_pyme"   +
					"    AND docto.ic_epo  = prod_epo.ic_epo"   +
					" 	  AND docto.ic_estatus_docto = ed.ic_estatus_docto"   +
					"    AND prod_epo.ic_producto_nafin = prod_nafin.ic_producto_nafin"   +
					"    AND lin_cred.ic_moneda = moneda.ic_moneda"   +
					" 	  AND camb_estatus.dc_fecha_cambio IN(SELECT MAX(dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_documento = docto.ic_documento)"   +
					" 	  AND lin_cred.ic_if = ci.ic_if"   +
					" 	  AND lin_cred.ic_tipo_cobro_interes = cti.ic_tipo_cobro_interes"   +
					" 	  AND soli.ic_tasaif = ct.ic_tasa"   +
					"    AND prod_nafin.ic_producto_nafin = 4"   +
					"	  AND lin_cred.ic_if = "+lsIntermediario+
					condicion.toString()  );

		qryCCC.append(
					"  SELECT /*+ use_nl(docto,docto_sel,soli,camb_estatus,lin_cred,prod_epo,prod_nafin,moneda,epo,pyme,ci,ct,cti,ed)"   +
					"         index(docto cp_dis_documento_pk)"   +
					"         index(lin_cred cp_com_linea_credito_pk)"   +
					"         index(cti cp_comcat_tipo_cobro_inter_pk)*/ "   +
					"         docto.ic_documento     AS num_credito,"   +
					" 		epo.cg_razon_social    AS epo,"   +
					"         pyme.cg_razon_social   AS distribuidor,"   +
					" 		ci.cg_razon_social	   AS Intermediario,"   +
					"         'Cr�dito en Cuenta Corriente'  AS tipo_credito,"   +
					"         DECODE (NVL (prod_epo.cg_responsable_interes, prod_nafin.cg_responsable_interes),'E', 'EPO','D', 'Distribuidor','')"   +
					"             AS responsable_interes,"   +
					"         DECODE (NVL (prod_epo.cg_tipo_cobranza, prod_nafin.cg_tipo_cobranza),'D', 'Delegada','N', 'No Delegada','')"   +
					"                                AS tipo_cobranza,"   +
					"         TO_CHAR(soli.df_operacion, 'DD/MM/YYYY')"   +
					"                                AS fecha_operacion,"   +
					"         moneda.cd_nombre	   AS tipo_moneda,"   +
					"         docto_sel.fn_importe_recibir         AS monto,"   +
					"         DECODE(NVL(prod_epo.cg_tipo_conversion, prod_nafin.cg_tipo_conversion), 'P', 'Dolar-Peso', 'Sin Conversion')"   +
					"                                AS tipo_conversion,"   +
					"         docto.fn_tipo_cambio   AS tipo_cambio,"   +
					"         docto.fn_monto * docto.fn_tipo_cambio"   +
					"                               AS monto_valuado,"   +
					"         NVL(ct.cd_nombre, 'No Autorizado') AS ref_tasa,"   +
					"         DECODE(docto_sel.cg_rel_mat, '+', docto_sel.fn_puntos + docto_sel.fn_valor_tasa, '-',"   +
					"               docto_sel.fn_valor_tasa - docto_sel.fn_puntos, '*', docto_sel.fn_puntos * docto_sel.fn_valor_tasa, '/',"   +
					"               docto_sel.fn_valor_tasa / docto_sel.fn_puntos)  AS tasa_interes,"   +
					"         docto.ig_plazo_credito AS plazo,"   +
					"         TO_CHAR(docto.df_fecha_venc_credito, 'DD/MM/YYYY') AS fch_vencimiento,"   +
					"         docto_sel.fn_importe_interes AS monto_interes,"   +
					"         cti.cd_descripcion			 AS tipo_cobro_inte,"   +
					"         ed.ic_estatus_docto AS estatus_soli,"   +
					" 		camb_estatus.cg_numero_pago  AS num_pago,"   +
					" 		TO_CHAR(camb_estatus.dc_fecha_cambio, 'DD/MM/YYYY') AS fecha_pago"   +
					" 		,moneda.ic_moneda"   +
					"  FROM dis_documento          docto"   +
					"   ,dis_docto_seleccionado    docto_sel"   +
					"   ,dis_solicitud             soli"   +
					" 	 ,dis_cambio_estatus       camb_estatus"   +
					"   ,com_linea_credito    	   lin_cred"   +
					"   ,comrel_producto_epo       prod_epo"   +
					"   ,comcat_producto_nafin     prod_nafin"   +
					"   ,comcat_moneda             moneda"   +
					"   ,comcat_epo                epo"   +
					"   ,comcat_pyme               pyme"   +
					" 	 ,comcat_if				    	 ci"   +
					" 	 ,comcat_tasa			    	 ct"   +
					" 	 ,comcat_tipo_cobro_interes cti"   +
					" 	 ,comcat_estatus_docto			ed"   +
					"  WHERE docto.ic_documento = docto_sel.ic_documento"   +
					" 	  AND docto.ic_documento = soli.ic_documento"   +
					" 	  AND docto.ic_documento = camb_estatus.ic_documento"   +
					"    AND docto.ic_linea_credito = lin_cred.ic_linea_credito"   +
					"    AND docto.ic_epo = epo.ic_epo"   +
					"    AND docto.ic_pyme = pyme.ic_pyme"   +
					"    AND docto.ic_epo  = prod_epo.ic_epo"   +
					" 	  AND docto.ic_estatus_docto = ed.ic_estatus_docto"   +
					"    AND prod_epo.ic_producto_nafin = prod_nafin.ic_producto_nafin"   +
					"    AND lin_cred.ic_moneda = moneda.ic_moneda"   +
					" 	  AND camb_estatus.dc_fecha_cambio IN(SELECT MAX(dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_documento = docto.ic_documento)"   +
					" 	  AND lin_cred.ic_if = ci.ic_if"   +
					" 	  AND lin_cred.ic_tipo_cobro_interes = cti.ic_tipo_cobro_interes"   +
					" 	  AND soli.ic_tasaif = ct.ic_tasa"   +
					"    AND prod_nafin.ic_producto_nafin = 4"   +
					"	  AND lin_cred.ic_if = "+lsIntermediario+
					condicion.toString()  );

			if("D".equals(tipo_credito))
				qryAux = qryDM.toString();
			else if("C".equals(tipo_credito))
				qryAux = qryCCC.toString();
			else
				qryAux = qryDM.toString()+ " UNION ALL "+qryCCC.toString();

		System.out.println("el query queda de la siguiente manera "+qryAux.toString());

		return qryAux.toString();
	}
	
	public String getDocumentQuery(HttpServletRequest request) {

      String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		String qryAux 	= "";	
		StringBuffer qryDM 		= new StringBuffer();
		StringBuffer qryCCC		= new StringBuffer();
		StringBuffer condicion    = new StringBuffer();
		
		String 	lsNumCredito    = (request.getParameter("txtNumCredito") 	  == null) ? ""  : request.getParameter("txtNumCredito");
		String 	tipo_credito   	= (request.getParameter("lstTipoCredito") 	  == null) ? ""  : request.getParameter("lstTipoCredito");
		String 	lsMoneda        = (request.getParameter("lstMoneda") 	      == null) ? ""  : request.getParameter("lstMoneda");
		String 	lsMontoCredIni  = (request.getParameter("txtMonto1")       	  == null) ? "0"  : (request.getParameter("txtMonto1").equals("")) ? "0" : request.getParameter("txtMonto1");
		String 	lsMontoCredFin  = (request.getParameter("txtMonto2")     	  == null) ? "0"  : (request.getParameter("txtMonto2").equals("")) ? "0" : request.getParameter("txtMonto2");
		String 	lsFchVenciIni   = (request.getParameter("txtFchVencimiento1") == null) ? ""   : request.getParameter("txtFchVencimiento1");
		String 	lsFchVenciFin   = (request.getParameter("txtFchVencimiento2") == null) ? ""   : request.getParameter("txtFchVencimiento2");
		String 	lsTipoCobroInte = (request.getParameter("lstTipoCobroInte")   == null) ? ""   : request.getParameter("lstTipoCobroInte");
		String 	lsIntermediario = (request.getParameter("lstIF")    		  == null) ? ""   : request.getParameter("lstIF");
		String 	lidAccion 		 = (request.getParameter("hidAccion")    	  == null) ? ""   : request.getParameter("hidAccion");
		String 	lsEstatus       = (request.getParameter("lstEstatus")         == null) ? "5" : request.getParameter("lstEstatus");
		String 	lsFchPagoIni    = (request.getParameter("txtFchPago1")   	  == null) ? ""   : request.getParameter("txtFchPago1");
		String 	lsFchPagoFin    = (request.getParameter("txtFchPago2")   	  == null) ? ""   : request.getParameter("txtFchPago2");
		
		try {

		if (!lsNumCredito.equals("") )	
			condicion.append(" AND docto.ic_documento = " + lsNumCredito);
		
		if (!lsMoneda.equals("") )		
			condicion.append(" AND docto.ic_moneda = " + lsMoneda);
		
		if ( !lsMontoCredIni.equals("0"))		
			condicion.append(" AND docto.fn_monto >= " + lsMontoCredIni);
		if (!lsMontoCredFin.equals("0"))		
			condicion.append(" AND docto.fn_monto <= " + lsMontoCredFin);
		if (!lsFchVenciIni.equals(""))	
			condicion.append(" AND docto.df_fecha_venc_credito >= TO_DATE('"+lsFchVenciIni+"', 'DD/MM/YYYY') ");
		if (!lsFchVenciFin.equals(""))	
			condicion.append(" AND docto.df_fecha_venc_credito <= TO_DATE('"+lsFchVenciFin+"', 'DD/MM/YYYY') ");
		if (!lsTipoCobroInte.equals(""))
			condicion.append(" AND lin_cred.ic_tipo_cobro_interes = " + lsTipoCobroInte);
		if (!lsFchPagoIni.equals("") )	
			condicion.append(" AND camb_estatus.dc_fecha_cambio >= TO_DATE('"+lsFchPagoIni+"', 'DD/MM/YYYY') ");
		if (!lsFchPagoFin.equals("") )	
			condicion.append(" AND camb_estatus.dc_fecha_cambio >= TO_DATE('"+lsFchPagoFin+"', 'DD/MM/YYYY') ");
		if (!lsEstatus.equals("") )		
			condicion.append(" AND soli.ic_estatus_solic =  "+lsEstatus);
		else 							
			condicion.append(" AND soli.ic_estatus_solic in(5,11)");

		qryDM.append(
					"   SELECT /*+index(docto) index(docto_sel) use_nl(docto docto_sel soli camb_estatus lin_cred prod_epo epo moneda)*/"   +
					"   docto.ic_documento"   +
					"   FROM dis_documento          docto 		  "   +
					"    ,dis_docto_seleccionado    docto_sel		  "   +
					"    ,dis_solicitud             soli	   		  "   +
					"    ,dis_cambio_estatus        camb_estatus  "   +
					"    ,dis_linea_credito_dm      lin_cred		"   +
					"    ,comrel_producto_epo       prod_epo"   +
					"    ,comcat_epo 				epo"   +
					"   WHERE docto.ic_documento = docto_sel.ic_documento"   +
					"     AND docto.ic_documento = soli.ic_documento"   +
					"  	AND docto.ic_documento = camb_estatus.ic_documento"   +
					"     AND docto.ic_linea_credito_dm = lin_cred.ic_linea_credito_dm"   +
					"     AND docto.ic_epo  = prod_epo.ic_epo"   +
					"     AND prod_epo.ic_epo  = epo.ic_epo"   +
					"  	AND camb_estatus.dc_fecha_cambio IN(SELECT /*+index(dce)*/ MAX(dc_fecha_cambio) FROM dis_cambio_estatus dce WHERE ic_documento = docto.ic_documento)"   +
					"     AND prod_epo.ic_producto_nafin = 4"   +
					"     AND epo.cs_habilitado = 'S'"   +
					"	  AND lin_cred.ic_if = "+lsIntermediario+
					condicion.toString()  );

		qryCCC.append(
					"   SELECT /*+index(docto_sel) use_nl(docto docto_sel soli camb_estatus lin_cred prod_epo epo moneda)*/ "   +
					"   docto.ic_documento"   +
					"   FROM dis_documento          docto 		  "   +
					"    ,dis_docto_seleccionado    docto_sel		  "   +
					"    ,dis_solicitud             soli	   		  "   +
					"    ,dis_cambio_estatus        camb_estatus  "   +
					"    ,com_linea_credito		    lin_cred		"   +
					"    ,comrel_producto_epo       prod_epo"   +
					"    ,comcat_epo				epo"   +
					"   WHERE docto.ic_documento = docto_sel.ic_documento"   +
					"     AND docto.ic_documento = soli.ic_documento"   +
					"  	AND docto.ic_documento = camb_estatus.ic_documento"   +
					"     AND docto.ic_linea_credito = lin_cred.ic_linea_credito"   +
					"     AND docto.ic_epo  = prod_epo.ic_epo"   +
					"     AND prod_epo.ic_epo  = epo.ic_epo"   +
					"  	AND camb_estatus.dc_fecha_cambio IN(SELECT /*+index(dce)*/ MAX(dc_fecha_cambio) FROM dis_cambio_estatus dce WHERE ic_documento = docto.ic_documento)"   +
					"     AND prod_epo.ic_producto_nafin = 4"   +
					"     AND epo.cs_habilitado = 'S' "   +
					"	  AND lin_cred.ic_if = "+lsIntermediario+
					condicion.toString()  );

			if("D".equals(tipo_credito))
				qryAux = qryDM.toString();
			else if("C".equals(tipo_credito))
				qryAux = qryCCC.toString();
			else
				qryAux = qryDM.toString()+ " UNION ALL "+qryCCC.toString();

			System.out.println("EL query de la llave primaria: "+qryAux.toString());

		}catch(Exception e){

			System.out.println("ConsPagosCreditosNafinDist::getDocumentQueryException "+e);
		}
		return qryAux.toString();
	}
	
	public String getDocumentQueryFile(HttpServletRequest request) {
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		String qryAux 	= "";	
		StringBuffer qryDM 		= new StringBuffer();
		StringBuffer qryCCC		= new StringBuffer();
		StringBuffer condicion  = new StringBuffer();
		
		String 	lsNumCredito    = (request.getParameter("txtNumCredito") 	  == null) ? ""  : request.getParameter("txtNumCredito");
		String 	tipo_credito   	= (request.getParameter("lstTipoCredito") 	  == null) ? ""  : request.getParameter("lstTipoCredito");
		String 	lsMoneda        = (request.getParameter("lstMoneda") 	      == null) ? ""  : request.getParameter("lstMoneda");
		String 	lsMontoCredIni  = (request.getParameter("txtMonto1")       	  == null) ? "0"  : (request.getParameter("txtMonto1").equals("")) ? "0" : request.getParameter("txtMonto1");
		String 	lsMontoCredFin  = (request.getParameter("txtMonto2")     	  == null) ? "0"  : (request.getParameter("txtMonto2").equals("")) ? "0" : request.getParameter("txtMonto2");
		String 	lsFchVenciIni   = (request.getParameter("txtFchVencimiento1") == null) ? ""   : request.getParameter("txtFchVencimiento1");
		String 	lsFchVenciFin   = (request.getParameter("txtFchVencimiento2") == null) ? ""   : request.getParameter("txtFchVencimiento2");
		String 	lsTipoCobroInte = (request.getParameter("lstTipoCobroInte")   == null) ? ""   : request.getParameter("lstTipoCobroInte");
		String 	lsIntermediario = (request.getParameter("lstIF")    		  == null) ? ""   : request.getParameter("lstIF");
		String 	lidAccion 		= (request.getParameter("hidAccion")    	  == null) ? ""   : request.getParameter("hidAccion");
		String 	lsEstatus       = (request.getParameter("lstEstatus")         == null) ? "5" : request.getParameter("lstEstatus");
		String 	lsFchPagoIni    = (request.getParameter("txtFchPago1")   	  == null) ? ""   : request.getParameter("txtFchPago1");
		String 	lsFchPagoFin    = (request.getParameter("txtFchPago2")   	  == null) ? ""   : request.getParameter("txtFchPago2");
		
		try {

		if (!lsNumCredito.equals("") )	
			condicion.append(" AND docto.ic_documento = " + lsNumCredito);
		
		if (!lsMoneda.equals("") )		
			condicion.append(" AND docto.ic_moneda = " + lsMoneda);
		
		if ( !lsMontoCredIni.equals("0"))		
			condicion.append(" AND docto.fn_monto >= " + lsMontoCredIni);
		if (!lsMontoCredFin.equals("0"))		
			condicion.append(" AND docto.fn_monto <= " + lsMontoCredFin);
		if (!lsFchVenciIni.equals(""))	
			condicion.append(" AND docto.df_fecha_venc_credito >= TO_DATE('"+lsFchVenciIni+"', 'DD/MM/YYYY') ");
		if (!lsFchVenciFin.equals(""))	
			condicion.append(" AND docto.df_fecha_venc_credito <= TO_DATE('"+lsFchVenciFin+"', 'DD/MM/YYYY') ");
		if (!lsTipoCobroInte.equals(""))
			condicion.append(" AND lin_cred.ic_tipo_cobro_interes = " + lsTipoCobroInte);
		if (!lsFchPagoIni.equals("") )	
			condicion.append(" AND camb_estatus.dc_fecha_cambio >= TO_DATE('"+lsFchPagoIni+"', 'DD/MM/YYYY') ");
		if (!lsFchPagoFin.equals("") )	
			condicion.append(" AND camb_estatus.dc_fecha_cambio >= TO_DATE('"+lsFchPagoFin+"', 'DD/MM/YYYY') ");
		if (!lsEstatus.equals("") )		
			condicion.append(" AND soli.ic_estatus_solic =  "+lsEstatus);
		else 							
			condicion.append(" AND soli.ic_estatus_solic in(5,11)");

		qryDM.append(
					"  SELECT /*+ use_nl(docto,docto_sel,soli,camb_estatus,lin_cred,prod_epo,prod_nafin,moneda,epo,pyme,ci,ct,cti,ed)"   +
					"         index(docto cp_dis_documento_pk)"   +
					"         index(lin_cred in_dis_linea_credito_dm_01_nuk)"   +
					"         index(cti cp_comcat_tipo_cobro_inter_pk)*/ "   +
					"         docto.ic_documento     AS num_credito,"   +
					" 		epo.cg_razon_social    AS epo,"   +
					"         pyme.cg_razon_social   AS distribuidor,"   +
					" 		ci.cg_razon_social	   AS Intermediario,"   +
					"         'Descuento y/o Factoraje'  AS tipo_credito,"   +
					"         DECODE (NVL (prod_epo.cg_responsable_interes, prod_nafin.cg_responsable_interes),'E', 'EPO','D', 'Distribuidor','')"   +
					"             AS responsable_interes,"   +
					"         DECODE (NVL (prod_epo.cg_tipo_cobranza, prod_nafin.cg_tipo_cobranza),'D', 'Delegada','N', 'No Delegada','')"   +
					"                                AS tipo_cobranza,"   +
					"         TO_CHAR(soli.df_operacion, 'DD/MM/YYYY')"   +
					"                                AS fecha_operacion,"   +
					"         moneda.cd_nombre	   AS tipo_moneda,"   +
					"         docto_sel.fn_importe_recibir         AS monto,"   +
					"         DECODE(NVL(prod_epo.cg_tipo_conversion, prod_nafin.cg_tipo_conversion), 'P', 'Dolar-Peso', 'Sin Conversion')"   +
					"                                AS tipo_conversion,"   +
					"         docto.fn_tipo_cambio   AS tipo_cambio,"   +
					"         docto.fn_monto * docto.fn_tipo_cambio"   +
					"                               AS monto_valuado,"   +
					"         NVL(ct.cd_nombre, 'No Autorizado') AS ref_tasa,"   +
					"         DECODE(docto_sel.cg_rel_mat, '+', docto_sel.fn_puntos + docto_sel.fn_valor_tasa, '-',"   +
					"               docto_sel.fn_valor_tasa - docto_sel.fn_puntos, '*', docto_sel.fn_puntos * docto_sel.fn_valor_tasa, '/',"   +
					"               docto_sel.fn_valor_tasa / docto_sel.fn_puntos)  AS tasa_interes,"   +
					"         docto.ig_plazo_credito AS plazo,"   +
					"         TO_CHAR(docto.df_fecha_venc_credito, 'DD/MM/YYYY') AS fch_vencimiento,"   +
					"         docto_sel.fn_importe_interes AS monto_interes,"   +
					"         cti.cd_descripcion			 AS tipo_cobro_inte,"   +
					"         ed.ic_estatus_docto AS estatus_soli,"   +
					" 		camb_estatus.cg_numero_pago  AS num_pago,"   +
					" 		TO_CHAR(camb_estatus.dc_fecha_cambio, 'DD/MM/YYYY') AS fecha_pago"   +
					" 		,moneda.ic_moneda"   +
					"  FROM dis_documento          docto"   +
					"   ,dis_docto_seleccionado    docto_sel"   +
					"   ,dis_solicitud             soli"   +
					" 	 ,dis_cambio_estatus        camb_estatus"   +
					"   ,dis_linea_credito_dm      lin_cred"   +
					"   ,comrel_producto_epo       prod_epo"   +
					"   ,comcat_producto_nafin     prod_nafin"   +
					"   ,comcat_moneda             moneda"   +
					"   ,comcat_epo                epo"   +
					"   ,comcat_pyme               pyme"   +
					" 	 ,comcat_if				    	 ci"   +
					" 	 ,comcat_tasa			    	 ct"   +
					" 	 ,comcat_tipo_cobro_interes cti"   +
					" 	 ,comcat_estatus_docto			ed"   +
					"  WHERE docto.ic_documento = docto_sel.ic_documento"   +
					" 	  AND docto.ic_documento = soli.ic_documento"   +
					" 	  AND docto.ic_documento = camb_estatus.ic_documento"   +
					"    AND docto.ic_linea_credito_dm = lin_cred.ic_linea_credito_dm"   +
					"    AND docto.ic_epo = epo.ic_epo"   +
					"    AND docto.ic_pyme = pyme.ic_pyme"   +
					"    AND docto.ic_epo  = prod_epo.ic_epo"   +
					" 	  AND docto.ic_estatus_docto = ed.ic_estatus_docto"   +
					"    AND prod_epo.ic_producto_nafin = prod_nafin.ic_producto_nafin"   +
					"    AND lin_cred.ic_moneda = moneda.ic_moneda"   +
					" 	  AND camb_estatus.dc_fecha_cambio IN(SELECT MAX(dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_documento = docto.ic_documento)"   +
					" 	  AND lin_cred.ic_if = ci.ic_if"   +
					" 	  AND lin_cred.ic_tipo_cobro_interes = cti.ic_tipo_cobro_interes"   +
					" 	  AND soli.ic_tasaif = ct.ic_tasa"   +
					"    AND prod_nafin.ic_producto_nafin = 4"   +
					"    AND epo.cs_habilitado  = 'S' "   +
					"	  AND lin_cred.ic_if = "+lsIntermediario+
					condicion.toString()  );

		qryCCC.append(
					"  SELECT /*+ use_nl(docto,docto_sel,soli,camb_estatus,lin_cred,prod_epo,prod_nafin,moneda,epo,pyme,ci,ct,cti,ed)"   +
					"         index(docto cp_dis_documento_pk)"   +
					"         index(lin_cred cp_com_linea_credito_pk)"   +
					"         index(cti cp_comcat_tipo_cobro_inter_pk)*/ "   +
					"         docto.ic_documento     AS num_credito,"   +
					" 		epo.cg_razon_social    AS epo,"   +
					"         pyme.cg_razon_social   AS distribuidor,"   +
					" 		ci.cg_razon_social	   AS Intermediario,"   +
					"         'Cr�dito en Cuenta Corriente'  AS tipo_credito,"   +
					"         DECODE (NVL (prod_epo.cg_responsable_interes, prod_nafin.cg_responsable_interes),'E', 'EPO','D', 'Distribuidor','')"   +
					"             AS responsable_interes,"   +
					"         DECODE (NVL (prod_epo.cg_tipo_cobranza, prod_nafin.cg_tipo_cobranza),'D', 'Delegada','N', 'No Delegada','')"   +
					"                                AS tipo_cobranza,"   +
					"         TO_CHAR(soli.df_operacion, 'DD/MM/YYYY')"   +
					"                                AS fecha_operacion,"   +
					"         moneda.cd_nombre	   AS tipo_moneda,"   +
					"         docto_sel.fn_importe_recibir         AS monto,"   +
					"         DECODE(NVL(prod_epo.cg_tipo_conversion, prod_nafin.cg_tipo_conversion), 'P', 'Dolar-Peso', 'Sin Conversion')"   +
					"                                AS tipo_conversion,"   +
					"         docto.fn_tipo_cambio   AS tipo_cambio,"   +
					"         docto.fn_monto * docto.fn_tipo_cambio"   +
					"                               AS monto_valuado,"   +
					"         NVL(ct.cd_nombre, 'No Autorizado') AS ref_tasa,"   +
					"         DECODE(docto_sel.cg_rel_mat, '+', docto_sel.fn_puntos + docto_sel.fn_valor_tasa, '-',"   +
					"               docto_sel.fn_valor_tasa - docto_sel.fn_puntos, '*', docto_sel.fn_puntos * docto_sel.fn_valor_tasa, '/',"   +
					"               docto_sel.fn_valor_tasa / docto_sel.fn_puntos)  AS tasa_interes,"   +
					"         docto.ig_plazo_credito AS plazo,"   +
					"         TO_CHAR(docto.df_fecha_venc_credito, 'DD/MM/YYYY') AS fch_vencimiento,"   +
					"         docto_sel.fn_importe_interes AS monto_interes,"   +
					"         cti.cd_descripcion			 AS tipo_cobro_inte,"   +
					"         ed.ic_estatus_docto AS estatus_soli,"   +
					" 		camb_estatus.cg_numero_pago  AS num_pago,"   +
					" 		TO_CHAR(camb_estatus.dc_fecha_cambio, 'DD/MM/YYYY') AS fecha_pago"   +
					" 		,moneda.ic_moneda"   +
					"  FROM dis_documento          docto"   +
					"   ,dis_docto_seleccionado    docto_sel"   +
					"   ,dis_solicitud             soli"   +
					" 	 ,dis_cambio_estatus       camb_estatus"   +
					"   ,com_linea_credito         lin_cred"   +
					"   ,comrel_producto_epo       prod_epo"   +
					"   ,comcat_producto_nafin     prod_nafin"   +
					"   ,comcat_moneda             moneda"   +
					"   ,comcat_epo                epo"   +
					"   ,comcat_pyme               pyme"   +
					" 	 ,comcat_if				    	 ci"   +
					" 	 ,comcat_tasa			    	 ct"   +
					" 	 ,comcat_tipo_cobro_interes cti"   +
					" 	 ,comcat_estatus_docto			ed"   +
					"  WHERE docto.ic_documento = docto_sel.ic_documento"   +
					" 	  AND docto.ic_documento = soli.ic_documento"   +
					" 	  AND docto.ic_documento = camb_estatus.ic_documento"   +
					"    AND docto.ic_linea_credito = lin_cred.ic_linea_credito"   +
					"    AND docto.ic_epo = epo.ic_epo"   +
					"    AND docto.ic_pyme = pyme.ic_pyme"   +
					"    AND docto.ic_epo  = prod_epo.ic_epo"   +
					" 	  AND docto.ic_estatus_docto = ed.ic_estatus_docto"   +
					"    AND prod_epo.ic_producto_nafin = prod_nafin.ic_producto_nafin"   +
					"    AND lin_cred.ic_moneda = moneda.ic_moneda"   +
					" 	  AND camb_estatus.dc_fecha_cambio IN(SELECT MAX(dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_documento = docto.ic_documento)"   +
					" 	  AND lin_cred.ic_if = ci.ic_if"   +
					" 	  AND lin_cred.ic_tipo_cobro_interes = cti.ic_tipo_cobro_interes"   +
					" 	  AND soli.ic_tasaif = ct.ic_tasa"   +
					"    AND prod_nafin.ic_producto_nafin = 4"   +
					"    AND epo.cs_habilitado  = 'S' "   +
					"	  AND lin_cred.ic_if = "+lsIntermediario+
					condicion.toString()  );

			if("D".equals(tipo_credito))
				qryAux = qryDM.toString();
			else if("C".equals(tipo_credito))
				qryAux = qryCCC.toString();
			else
				qryAux = qryDM.toString()+ " UNION ALL "+qryCCC.toString();
				
			qryAux += 
				" ORDER BY num_credito, epo, distribuidor ";

		}catch(Exception e){

			System.out.println("ConsPagosCreditosNafinDist::getDocumentQueryFileException "+e);

		}
		return qryAux.toString();
	}
	
	//**********************************MIGRACION******************************************
	
	private final static Log log = ServiceLocator.getInstance().getLog(ConsPagosCredNafinDist.class);
	
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions ;
	
	private String numDocFinal;
	private String tipoCobroInter;
	private String tipoCredito;
	private String claveIF;
	private String moneda;
	private String estatus;
	private String montoMin;
	private String montoMax;
	private String fechaPagoInicio;
	private String fechaPagoFin;
	private String fechaVencInicio;
	private String fechaVencFin;
	
	StringBuffer sqlSentencia = new StringBuffer("");
	
	public String getAggregateCalculationQuery(){
		log.info("getAggregateCalculationQuery(E)");
		
		sqlSentencia = new StringBuffer("");
		StringBuffer sqlSentenciaTipoCreditoC = new StringBuffer("");
		StringBuffer sqlSentenciaTipoCreditoD = new StringBuffer("");
		conditions = new ArrayList();
		
		sqlSentenciaTipoCreditoD.append(
				"   SELECT /*+index(docto) index(docto_sel) use_nl(docto docto_sel soli camb_estatus lin_cred prod_epo epo moneda)*/ "   +
				" 		 moneda.ic_moneda"   +
				" 		 ,moneda.cd_nombre TIPO_MONEDA"   +//TIPO_MONEDA Agregado
				"          ,SUM(docto_sel.fn_importe_recibir) MONTO_TOTAL"   + //MONTO_TOTAL Agragado
				"          ,SUM(docto_sel.fn_importe_interes) MONTO_TOTAL_INTERES"   + //MONTO_TOTAL_INTERES Agregado 
				"				, count (*) NO_DOCUMENTOS "+
				"   FROM dis_documento          docto"   +
				"    ,dis_docto_seleccionado    docto_sel"   +
				"    ,dis_solicitud             soli"   +
				"    ,dis_cambio_estatus        camb_estatus"   +
				"    ,dis_linea_credito_dm      lin_cred"   +
				"    ,comrel_producto_epo       prod_epo"   +
				"    ,comcat_epo				epo"   +
				"    ,comcat_moneda             moneda	"   +
				"   WHERE docto.ic_documento = docto_sel.ic_documento	"   +
				"  	AND docto.ic_documento = soli.ic_documento	"   +
				"  	AND docto.ic_documento = camb_estatus.ic_documento	"   +
				"     AND docto.ic_linea_credito_dm = lin_cred.ic_linea_credito_dm	"   +
				"     AND docto.ic_epo  = prod_epo.ic_epo	"   +
				"     AND prod_epo.ic_epo  = epo.ic_epo	"   +
				"     AND lin_cred.ic_moneda = moneda.ic_moneda	"   +
				"  	AND camb_estatus.dc_fecha_cambio IN(SELECT /*+index(dce)*/ MAX(dc_fecha_cambio) FROM dis_cambio_estatus dce WHERE ic_documento = docto.ic_documento)"   +
				"     AND prod_epo.ic_producto_nafin = 4"   +
				" 		AND soli.ic_estatus_solic in(5,11)"+
				"     AND epo.cs_habilitado = 'S' " 
		);
					
		sqlSentenciaTipoCreditoC.append(
				"   SELECT /*+index(docto_sel) use_nl(docto docto_sel soli camb_estatus lin_cred prod_epo epo moneda)*/ "   +
				" 		 moneda.ic_moneda"   +
				" 		 ,moneda.cd_nombre TIPO_MONEDA"   +//TIPO_MONEDA Agregado
				"          ,SUM(docto_sel.fn_importe_recibir) MONTO_TOTAL"   + //MONTO_TOTAL Agragado
				"          ,SUM(docto_sel.fn_importe_interes) MONTO_TOTAL_INTERES"   + //MONTO_TOTAL_INTERES Agregado 
				"				, count (*) NO_DOCUMENTOS "+
				"   FROM dis_documento          docto"   +
				"    ,dis_docto_seleccionado    docto_sel"   +
				"    ,dis_solicitud             soli"   +
				"    ,dis_cambio_estatus        camb_estatus"   +
				"    ,com_linea_credito      	lin_cred"   +
				"    ,comrel_producto_epo       prod_epo"   +
				"    ,comcat_epo 				epo"   +
				"    ,comcat_moneda             moneda"   +
				"   WHERE docto.ic_documento = docto_sel.ic_documento"   +
				"  	AND docto.ic_documento = soli.ic_documento"   +
				"  	AND docto.ic_documento = camb_estatus.ic_documento"   +
				"     AND docto.ic_linea_credito = lin_cred.ic_linea_credito"   +
				"     AND docto.ic_epo  = prod_epo.ic_epo"   +
				"     AND prod_epo.ic_epo  = epo.ic_epo"   +
				"     AND lin_cred.ic_moneda = moneda.ic_moneda"   +
				"  	AND camb_estatus.dc_fecha_cambio IN(SELECT /*+index(dce)*/ MAX(dc_fecha_cambio) FROM dis_cambio_estatus dce WHERE ic_documento = docto.ic_documento)"   +
				"     AND prod_epo.ic_producto_nafin = 4"  +
				"		AND soli.ic_estatus_solic in(5,11)"+
				"     AND epo.cs_habilitado = 'S'"   
		);
		
		if("D".equals(tipoCredito)){
			sqlSentenciaTipoCreditoD.append(getCondicionGeneral().toString());
			sqlSentencia.append(sqlSentenciaTipoCreditoD.toString());
			sqlSentencia.append(" GROUP BY moneda.ic_moneda,moneda.cd_nombre"   +
											  " ORDER BY moneda.ic_moneda"
											  );
		}else if("C".equals(tipoCredito)){
			sqlSentenciaTipoCreditoC.append(getCondicionGeneral().toString());
			sqlSentencia.append(sqlSentenciaTipoCreditoC.toString());
			sqlSentencia.append(" GROUP BY moneda.ic_moneda,moneda.cd_nombre"  +
											  " ORDER BY moneda.ic_moneda"
											  );
		}else {
			sqlSentenciaTipoCreditoD.append(getCondicionGeneral().toString());
			sqlSentenciaTipoCreditoD.append(" GROUP BY moneda.ic_moneda,moneda.cd_nombre" );
			sqlSentenciaTipoCreditoC.append(getCondicionGeneral().toString());
			sqlSentenciaTipoCreditoC.append(" GROUP BY moneda.ic_moneda,moneda.cd_nombre" );
			sqlSentencia.append( sqlSentenciaTipoCreditoD.toString()+ " UNION ALL "+sqlSentenciaTipoCreditoC.toString());
			sqlSentencia.append( "  ORDER BY ic_moneda" );
											  
			
		}
			
		log.info("sqlSentencia getAggregateCalculationQuery "+sqlSentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
	
		return sqlSentencia.toString();
	}
	
	public String getDocumentQuery(){
		log.info("getDocumentQuery(E)");
		
		sqlSentencia = new StringBuffer("");
		StringBuffer sqlSentenciaTipoCreditoC = new StringBuffer("");
		StringBuffer sqlSentenciaTipoCreditoD = new StringBuffer("");
		conditions = new ArrayList();
				
		sqlSentenciaTipoCreditoD.append(
				"   SELECT /*+index(docto) index(docto_sel) use_nl(docto docto_sel soli camb_estatus lin_cred prod_epo epo moneda)*/"   +
				"   docto.ic_documento"   +
				"   FROM dis_documento          docto 		  "   +
				"    ,dis_docto_seleccionado    docto_sel		  "   +
				"    ,dis_solicitud             soli	   		  "   +
				"    ,dis_cambio_estatus        camb_estatus  "   +
				"    ,dis_linea_credito_dm      lin_cred		"   +
				"    ,comrel_producto_epo       prod_epo"   +
				"    ,comcat_epo 				epo"   +
				"   WHERE docto.ic_documento = docto_sel.ic_documento"   +
				"     AND docto.ic_documento = soli.ic_documento"   +
				"  	AND docto.ic_documento = camb_estatus.ic_documento"   +
				"     AND docto.ic_linea_credito_dm = lin_cred.ic_linea_credito_dm"   +
				"     AND docto.ic_epo  = prod_epo.ic_epo"   +
				"     AND prod_epo.ic_epo  = epo.ic_epo"   +
				"  	AND camb_estatus.dc_fecha_cambio IN(SELECT /*+index(dce)*/ MAX(dc_fecha_cambio) FROM dis_cambio_estatus dce WHERE ic_documento = docto.ic_documento)"   +
				"     AND prod_epo.ic_producto_nafin = 4"   +
				"     AND epo.cs_habilitado = 'S'"
		);

		sqlSentenciaTipoCreditoC.append(
				"   SELECT /*+index(docto_sel) use_nl(docto docto_sel soli camb_estatus lin_cred prod_epo epo moneda)*/ "   +
				"   docto.ic_documento"   +
				"   FROM dis_documento          docto 		  "   +
				"    ,dis_docto_seleccionado    docto_sel		  "   +
				"    ,dis_solicitud             soli	   		  "   +
				"    ,dis_cambio_estatus        camb_estatus  "   +
				"    ,com_linea_credito		    lin_cred		"   +
				"    ,comrel_producto_epo       prod_epo"   +
				"    ,comcat_epo				epo"   +
				"   WHERE docto.ic_documento = docto_sel.ic_documento"   +
				"     AND docto.ic_documento = soli.ic_documento"   +
				"  	AND docto.ic_documento = camb_estatus.ic_documento"   +
				"     AND docto.ic_linea_credito = lin_cred.ic_linea_credito"   +
				"     AND docto.ic_epo  = prod_epo.ic_epo"   +
				"     AND prod_epo.ic_epo  = epo.ic_epo"   +
				"  	AND camb_estatus.dc_fecha_cambio IN(SELECT /*+index(dce)*/ MAX(dc_fecha_cambio) FROM dis_cambio_estatus dce WHERE ic_documento = docto.ic_documento)"   +
				"     AND prod_epo.ic_producto_nafin = 4"   +
				"     AND epo.cs_habilitado = 'S' " 
		);

		if("D".equals(tipoCredito)){
			sqlSentenciaTipoCreditoD.append(getCondicionGeneral().toString());
			sqlSentencia.append(sqlSentenciaTipoCreditoD.toString());
		}else if("C".equals(tipoCredito)){
			sqlSentenciaTipoCreditoC.append(getCondicionGeneral().toString());
			sqlSentencia.append(sqlSentenciaTipoCreditoC.toString());
		}else {
			sqlSentenciaTipoCreditoD.append(getCondicionGeneral().toString());
			sqlSentenciaTipoCreditoC.append(getCondicionGeneral().toString());
			sqlSentencia.append( sqlSentenciaTipoCreditoD.toString()+ " UNION ALL "+sqlSentenciaTipoCreditoC.toString());
		}
		log.info("sqlSentencia getDocumentQuery "+sqlSentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		
		return sqlSentencia.toString();
	}
	/**
	 * 
	 * @return 
	 * @param pageIds
	 */
	public String getDocumentSummaryQueryForIds(List pageIds) {
		log.info("getDocumentSummaryQueryForIds(E)");
		
		sqlSentencia = new StringBuffer("");
		StringBuffer sqlSentenciaTipoCreditoC = new StringBuffer("");
		StringBuffer sqlSentenciaTipoCreditoD = new StringBuffer("");
		StringBuffer condicion  = new StringBuffer("");
		StringBuffer condicionAUx  = new StringBuffer("");
		conditions 		= new ArrayList();
		
		sqlSentenciaTipoCreditoD.append(
				"  SELECT /*+ use_nl(docto,docto_sel,soli,camb_estatus,lin_cred,prod_epo,prod_nafin,moneda,epo,pyme,ci,ct,cti,ed)"   +
				"         index(docto cp_dis_documento_pk)"   +
				"         index(lin_cred in_dis_linea_credito_dm_01_nuk)"   +
				"         index(cti cp_comcat_tipo_cobro_inter_pk)*/ "   +
				"         docto.ic_documento     AS num_credito,"   +
				" 		epo.cg_razon_social    AS epo,"   +
				"         pyme.cg_razon_social   AS distribuidor,"   +
				" 		ci.cg_razon_social	   AS Intermediario,"   +
				"         'Descuento y/o Factoraje'  AS tipo_credito,"   +
				"         DECODE (NVL (prod_epo.cg_responsable_interes, prod_nafin.cg_responsable_interes),'E', 'EPO','D', 'Distribuidor','')"   +
				"             AS responsable_interes,"   +
				"         DECODE (NVL (prod_epo.cg_tipo_cobranza, prod_nafin.cg_tipo_cobranza),'D', 'Delegada','N', 'No Delegada','')"   +
				"                                AS tipo_cobranza,"   +
				"         TO_CHAR(soli.df_operacion, 'DD/MM/YYYY')"   +
				"                                AS fecha_operacion,"   +
				"         moneda.cd_nombre	   AS tipo_moneda,"   +
				"         docto_sel.fn_importe_recibir         AS monto,"   +
				"         DECODE(NVL(prod_epo.cg_tipo_conversion, prod_nafin.cg_tipo_conversion), 'P', 'Dolar-Peso', 'Sin Conversion')"   +
				"                                AS tipo_conversion,"   +
				"         docto.fn_tipo_cambio   AS tipo_cambio,"   +
				"         docto.fn_monto * docto.fn_tipo_cambio"   +
				"                               AS monto_valuado,"   +
				"         NVL(ct.cd_nombre, 'No Autorizado') AS ref_tasa,"   +
				"         DECODE(docto_sel.cg_rel_mat, '+', docto_sel.fn_puntos + docto_sel.fn_valor_tasa, '-',"   +
				"               docto_sel.fn_valor_tasa - docto_sel.fn_puntos, '*', docto_sel.fn_puntos * docto_sel.fn_valor_tasa, '/',"   +
				"               docto_sel.fn_valor_tasa / docto_sel.fn_puntos)  AS tasa_interes,"   +
				"         docto.ig_plazo_credito AS plazo,"   +
				"         TO_CHAR(docto.df_fecha_venc_credito, 'DD/MM/YYYY') AS fch_vencimiento,"   +
				"         docto_sel.fn_importe_interes AS monto_interes,"   +
				"         cti.cd_descripcion			 AS tipo_cobro_inte,"   +
				"         ed.ic_estatus_docto AS estatus_soli,"   +
				" 		camb_estatus.cg_numero_pago  AS num_pago,"   +
				" 		TO_CHAR(camb_estatus.dc_fecha_cambio, 'DD/MM/YYYY') AS fecha_pago"   +
				" 		,moneda.ic_moneda"   +
				"  FROM dis_documento          docto"   +
				"   ,dis_docto_seleccionado    docto_sel"   +
				"   ,dis_solicitud             soli"   +
				" 	 ,dis_cambio_estatus        camb_estatus"   +
				"   ,dis_linea_credito_dm      lin_cred"   +
				"   ,comrel_producto_epo       prod_epo"   +
				"   ,comcat_producto_nafin     prod_nafin"   +
				"   ,comcat_moneda             moneda"   +
				"   ,comcat_epo                epo"   +
				"   ,comcat_pyme               pyme"   +
				" 	 ,comcat_if				    	 ci"   +
				" 	 ,comcat_tasa			    	 ct"   +
				" 	 ,comcat_tipo_cobro_interes cti"   +
				" 	 ,comcat_estatus_docto			ed"   +
				"  WHERE docto.ic_documento = docto_sel.ic_documento"   +
				" 	  AND docto.ic_documento = soli.ic_documento"   +
				" 	  AND docto.ic_documento = camb_estatus.ic_documento"   +
				"    AND docto.ic_linea_credito_dm = lin_cred.ic_linea_credito_dm"   +
				"    AND docto.ic_epo = epo.ic_epo"   +
				"    AND docto.ic_pyme = pyme.ic_pyme"   +
				"    AND docto.ic_epo  = prod_epo.ic_epo"   +
				" 	  AND docto.ic_estatus_docto = ed.ic_estatus_docto"   +
				"    AND prod_epo.ic_producto_nafin = prod_nafin.ic_producto_nafin"   +
				"    AND lin_cred.ic_moneda = moneda.ic_moneda"   +
				" 	  AND camb_estatus.dc_fecha_cambio IN(SELECT MAX(dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_documento = docto.ic_documento)"   +
				" 	  AND lin_cred.ic_if = ci.ic_if"   +
				" 	  AND lin_cred.ic_tipo_cobro_interes = cti.ic_tipo_cobro_interes"   +
				" 	  AND soli.ic_tasaif = ct.ic_tasa"   +
				"    AND prod_nafin.ic_producto_nafin = 4" 
		);

		sqlSentenciaTipoCreditoC.append(
				"  SELECT /*+ use_nl(docto,docto_sel,soli,camb_estatus,lin_cred,prod_epo,prod_nafin,moneda,epo,pyme,ci,ct,cti,ed)"   +
				"         index(docto cp_dis_documento_pk)"   +
				"         index(lin_cred cp_com_linea_credito_pk)"   +
				"         index(cti cp_comcat_tipo_cobro_inter_pk)*/ "   +
				"         docto.ic_documento     AS num_credito,"   +
				" 		epo.cg_razon_social    AS epo,"   +
				"         pyme.cg_razon_social   AS distribuidor,"   +
				" 		ci.cg_razon_social	   AS Intermediario,"   +
				"         'Cr�dito en Cuenta Corriente'  AS tipo_credito,"   +
				"         DECODE (NVL (prod_epo.cg_responsable_interes, prod_nafin.cg_responsable_interes),'E', 'EPO','D', 'Distribuidor','')"   +
				"             AS responsable_interes,"   +
				"         DECODE (NVL (prod_epo.cg_tipo_cobranza, prod_nafin.cg_tipo_cobranza),'D', 'Delegada','N', 'No Delegada','')"   +
				"                                AS tipo_cobranza,"   +
				"         TO_CHAR(soli.df_operacion, 'DD/MM/YYYY')"   +
				"                                AS fecha_operacion,"   +
				"         moneda.cd_nombre	   AS tipo_moneda,"   +
				"         docto_sel.fn_importe_recibir         AS monto,"   +
				"         DECODE(NVL(prod_epo.cg_tipo_conversion, prod_nafin.cg_tipo_conversion), 'P', 'Dolar-Peso', 'Sin Conversion')"   +
				"                                AS tipo_conversion,"   +
				"         docto.fn_tipo_cambio   AS tipo_cambio,"   +
				"         docto.fn_monto * docto.fn_tipo_cambio"   +
				"                               AS monto_valuado,"   +
				"         NVL(ct.cd_nombre, 'No Autorizado') AS ref_tasa,"   +
				"         DECODE(docto_sel.cg_rel_mat, '+', docto_sel.fn_puntos + docto_sel.fn_valor_tasa, '-',"   +
				"               docto_sel.fn_valor_tasa - docto_sel.fn_puntos, '*', docto_sel.fn_puntos * docto_sel.fn_valor_tasa, '/',"   +
				"               docto_sel.fn_valor_tasa / docto_sel.fn_puntos)  AS tasa_interes,"   +
				"         docto.ig_plazo_credito AS plazo,"   +
				"         TO_CHAR(docto.df_fecha_venc_credito, 'DD/MM/YYYY') AS fch_vencimiento,"   +
				"         docto_sel.fn_importe_interes AS monto_interes,"   +
				"         cti.cd_descripcion			 AS tipo_cobro_inte,"   +
				"         ed.ic_estatus_docto AS estatus_soli,"   +
				" 		camb_estatus.cg_numero_pago  AS num_pago,"   +
				" 		TO_CHAR(camb_estatus.dc_fecha_cambio, 'DD/MM/YYYY') AS fecha_pago"   +
				" 		,moneda.ic_moneda"   +
				"  FROM dis_documento          docto"   +
				"   ,dis_docto_seleccionado    docto_sel"   +
				"   ,dis_solicitud             soli"   +
				" 	 ,dis_cambio_estatus       camb_estatus"   +
				"   ,com_linea_credito    	   lin_cred"   +
				"   ,comrel_producto_epo       prod_epo"   +
				"   ,comcat_producto_nafin     prod_nafin"   +
				"   ,comcat_moneda             moneda"   +
				"   ,comcat_epo                epo"   +
				"   ,comcat_pyme               pyme"   +
				" 	 ,comcat_if				    	 ci"   +
				" 	 ,comcat_tasa			    	 ct"   +
				" 	 ,comcat_tipo_cobro_interes cti"   +
				" 	 ,comcat_estatus_docto			ed"   +
				"  WHERE docto.ic_documento = docto_sel.ic_documento"   +
				" 	  AND docto.ic_documento = soli.ic_documento"   +
				" 	  AND docto.ic_documento = camb_estatus.ic_documento"   +
				"    AND docto.ic_linea_credito = lin_cred.ic_linea_credito"   +
				"    AND docto.ic_epo = epo.ic_epo"   +
				"    AND docto.ic_pyme = pyme.ic_pyme"   +
				"    AND docto.ic_epo  = prod_epo.ic_epo"   +
				" 	  AND docto.ic_estatus_docto = ed.ic_estatus_docto"   +
				"    AND prod_epo.ic_producto_nafin = prod_nafin.ic_producto_nafin"   +
				"    AND lin_cred.ic_moneda = moneda.ic_moneda"   +
				" 	  AND camb_estatus.dc_fecha_cambio IN(SELECT MAX(dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_documento = docto.ic_documento)"   +
				" 	  AND lin_cred.ic_if = ci.ic_if"   +
				" 	  AND lin_cred.ic_tipo_cobro_interes = cti.ic_tipo_cobro_interes"   +
				" 	  AND soli.ic_tasaif = ct.ic_tasa"   +
				"    AND prod_nafin.ic_producto_nafin = 4" 
		);
		
		if("D".equals(tipoCredito)){
			for(int i=0;i<pageIds.size();i++) {
				List lItem = (List)pageIds.get(i);
				if(i==0) {
					condicion.append(" AND docto.ic_documento in (");
				}
				condicion.append("?");
				conditions.add(new Long(lItem.get(0).toString()));
				if(i!=(pageIds.size()-1)) {
					condicion.append(",");
				} else {
					condicion.append(" ) ");
				}			
			}
			if(!claveIF.equals("")){
				condicion.append(" AND lin_cred.ic_if = ?");
				conditions.add(claveIF);
			}
			sqlSentenciaTipoCreditoD.append(condicion.toString());
			sqlSentencia.append(sqlSentenciaTipoCreditoD.toString());
			
		}else if("C".equals(tipoCredito)){
			for(int i=0;i<pageIds.size();i++) {
				List lItemAux = (List)pageIds.get(i);
				if(i==0) {
					condicionAUx.append(" AND docto.ic_documento in (");
				}
				condicionAUx.append("?");
				conditions.add(new Long(lItemAux.get(0).toString()));
				if(i!=(pageIds.size()-1)) {
					condicionAUx.append(",");
				} else {
					condicionAUx.append(" ) ");
				}			
			}
			if(!claveIF.equals("")){
				condicionAUx.append(" AND lin_cred.ic_if = ?");
				conditions.add(claveIF);
			}
			sqlSentenciaTipoCreditoC.append(condicionAUx.toString());	
			sqlSentencia.append(sqlSentenciaTipoCreditoC.toString());
			
		}else {
			for(int i=0;i<pageIds.size();i++) {
				List lItem = (List)pageIds.get(i);
				if(i==0) {
					condicion.append(" AND docto.ic_documento in (");
				}
				condicion.append("?");
				conditions.add(new Long(lItem.get(0).toString()));
				if(i!=(pageIds.size()-1)) {
					condicion.append(",");
				} else {
					condicion.append(" ) ");
				}			
			}
			if(!claveIF.equals("")){
				condicion.append(" AND lin_cred.ic_if = ?");
				conditions.add(claveIF);
			}
			sqlSentenciaTipoCreditoD.append(condicion.toString());
		
		
			for(int i=0;i<pageIds.size();i++) {
				List lItemAux = (List)pageIds.get(i);
				if(i==0) {
					condicionAUx.append(" AND docto.ic_documento in (");
				}
				condicionAUx.append("?");
				conditions.add(new Long(lItemAux.get(0).toString()));
				if(i!=(pageIds.size()-1)) {
					condicionAUx.append(",");
				} else {
					condicionAUx.append(" ) ");
				}			
			}
			if(!claveIF.equals("")){
				condicionAUx.append(" AND lin_cred.ic_if = ?");
				conditions.add(claveIF);
			}
			sqlSentenciaTipoCreditoC.append(condicion.toString());
			
			sqlSentencia.append( sqlSentenciaTipoCreditoD.toString()+ " UNION ALL "+sqlSentenciaTipoCreditoC.toString());
			
		}

		log.info("sqlSentencia getDocumentSummaryQueryForIds "+sqlSentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		
		return sqlSentencia.toString();
	}
	
	public String getDocumentQueryFile(){
		log.info("getDocumentQueryFile(E)");
	
		sqlSentencia = new StringBuffer("");
		StringBuffer sqlSentenciaTipoCreditoC = new StringBuffer("");
		StringBuffer sqlSentenciaTipoCreditoD = new StringBuffer("");
		StringBuffer condicion  = new StringBuffer("");
		conditions 		= new ArrayList();
		
		sqlSentenciaTipoCreditoD.append(
				"  SELECT /*+ use_nl(docto,docto_sel,soli,camb_estatus,lin_cred,prod_epo,prod_nafin,moneda,epo,pyme,ci,ct,cti,ed)"   +
				"         index(docto cp_dis_documento_pk)"   +
				"         index(lin_cred in_dis_linea_credito_dm_01_nuk)"   +
				"         index(cti cp_comcat_tipo_cobro_inter_pk)*/ "   +		
				"         docto.ic_documento     AS num_credito,"   +
				" 		epo.cg_razon_social    AS epo,"   +
				"         pyme.cg_razon_social   AS distribuidor,"   +
				" 		ci.cg_razon_social	   AS Intermediario,"   +
				"         'Descuento y/o Factoraje'  AS tipo_credito,"   +
				"         DECODE (NVL (prod_epo.cg_responsable_interes, prod_nafin.cg_responsable_interes),'E', 'EPO','D', 'Distribuidor','')"   +
				"             AS responsable_interes,"   +
				"         DECODE (NVL (prod_epo.cg_tipo_cobranza, prod_nafin.cg_tipo_cobranza),'D', 'Delegada','N', 'No Delegada','')"   +
				"                                AS tipo_cobranza,"   +
				"         TO_CHAR(soli.df_operacion, 'DD/MM/YYYY')"   +
				"                                AS fecha_operacion,"   +
				"         moneda.cd_nombre	   AS tipo_moneda,"   +
				"         docto_sel.fn_importe_recibir         AS monto,"   +
				"         DECODE(NVL(prod_epo.cg_tipo_conversion, prod_nafin.cg_tipo_conversion), 'P', 'Dolar-Peso', 'Sin Conversion')"   +
				"                                AS tipo_conversion,"   +
				"         docto.fn_tipo_cambio   AS tipo_cambio,"   +
				"         docto.fn_monto * docto.fn_tipo_cambio"   +
				"                               AS monto_valuado,"   +
				"         NVL(ct.cd_nombre, 'No Autorizado') AS ref_tasa,"   +
				"         DECODE(docto_sel.cg_rel_mat, '+', docto_sel.fn_puntos + docto_sel.fn_valor_tasa, '-',"   +
				"               docto_sel.fn_valor_tasa - docto_sel.fn_puntos, '*', docto_sel.fn_puntos * docto_sel.fn_valor_tasa, '/',"   +
				"               docto_sel.fn_valor_tasa / docto_sel.fn_puntos)  AS tasa_interes,"   +
				"         docto.ig_plazo_credito AS plazo,"   +
				"         TO_CHAR(docto.df_fecha_venc_credito, 'DD/MM/YYYY') AS fch_vencimiento,"   +
				"         docto_sel.fn_importe_interes AS monto_interes,"   +
				"         cti.cd_descripcion			 AS tipo_cobro_inte,"   +
				"         ed.ic_estatus_docto AS estatus_soli,"   +
				" 		camb_estatus.cg_numero_pago  AS num_pago,"   +
				" 		TO_CHAR(camb_estatus.dc_fecha_cambio, 'DD/MM/YYYY') AS fecha_pago"   +
				" 		,moneda.ic_moneda"   +
				"  FROM dis_documento          docto"   +
				"   ,dis_docto_seleccionado    docto_sel"   +
				"   ,dis_solicitud             soli"   +
				" 	 ,dis_cambio_estatus        camb_estatus"   +
				"   ,dis_linea_credito_dm      lin_cred"   +
				"   ,comrel_producto_epo       prod_epo"   +
				"   ,comcat_producto_nafin     prod_nafin"   +
				"   ,comcat_moneda             moneda"   +
				"   ,comcat_epo                epo"   +
				"   ,comcat_pyme               pyme"   +
				" 	 ,comcat_if				    	 ci"   +
				" 	 ,comcat_tasa			    	 ct"   +
				" 	 ,comcat_tipo_cobro_interes cti"   +
				" 	 ,comcat_estatus_docto			ed"   +
				"  WHERE docto.ic_documento = docto_sel.ic_documento"   +
				" 	  AND docto.ic_documento = soli.ic_documento"   +
				" 	  AND docto.ic_documento = camb_estatus.ic_documento"   +
				"    AND docto.ic_linea_credito_dm = lin_cred.ic_linea_credito_dm"   +
				"    AND docto.ic_epo = epo.ic_epo"   +
				"    AND docto.ic_pyme = pyme.ic_pyme"   +
				"    AND docto.ic_epo  = prod_epo.ic_epo"   +
				" 	  AND docto.ic_estatus_docto = ed.ic_estatus_docto"   +
				"    AND prod_epo.ic_producto_nafin = prod_nafin.ic_producto_nafin"   +
				"    AND lin_cred.ic_moneda = moneda.ic_moneda"   +
				" 	  AND camb_estatus.dc_fecha_cambio IN(SELECT MAX(dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_documento = docto.ic_documento)"   +
				" 	  AND lin_cred.ic_if = ci.ic_if"   +
				" 	  AND lin_cred.ic_tipo_cobro_interes = cti.ic_tipo_cobro_interes"   +
				" 	  AND soli.ic_tasaif = ct.ic_tasa"   +
				"    AND prod_nafin.ic_producto_nafin = 4"   +
				"    AND epo.cs_habilitado  = 'S' "
		);
		
		sqlSentenciaTipoCreditoC.append(
				"  SELECT /*+ use_nl(docto,docto_sel,soli,camb_estatus,lin_cred,prod_epo,prod_nafin,moneda,epo,pyme,ci,ct,cti,ed)"   +
				"         index(docto cp_dis_documento_pk)"   +
				"         index(lin_cred cp_com_linea_credito_pk)"   +
				"         index(cti cp_comcat_tipo_cobro_inter_pk)*/ "   +
				"         docto.ic_documento     AS num_credito,"   +
				" 		epo.cg_razon_social    AS epo,"   +
				"         pyme.cg_razon_social   AS distribuidor,"   +
				" 		ci.cg_razon_social	   AS Intermediario,"   +
				"         'Cr�dito en Cuenta Corriente'  AS tipo_credito,"   +
				"         DECODE (NVL (prod_epo.cg_responsable_interes, prod_nafin.cg_responsable_interes),'E', 'EPO','D', 'Distribuidor','')"   +
				"             AS responsable_interes,"   +
				"         DECODE (NVL (prod_epo.cg_tipo_cobranza, prod_nafin.cg_tipo_cobranza),'D', 'Delegada','N', 'No Delegada','')"   +
				"                                AS tipo_cobranza,"   +
				"         TO_CHAR(soli.df_operacion, 'DD/MM/YYYY')"   +
				"                                AS fecha_operacion,"   +
				"         moneda.cd_nombre	   AS tipo_moneda,"   +
				"         docto_sel.fn_importe_recibir         AS monto,"   +
				"         DECODE(NVL(prod_epo.cg_tipo_conversion, prod_nafin.cg_tipo_conversion), 'P', 'Dolar-Peso', 'Sin Conversion')"   +
				"                                AS tipo_conversion,"   +
				"         docto.fn_tipo_cambio   AS tipo_cambio,"   +
				"         docto.fn_monto * docto.fn_tipo_cambio"   +
				"                               AS monto_valuado,"   +
				"         NVL(ct.cd_nombre, 'No Autorizado') AS ref_tasa,"   +
				"         DECODE(docto_sel.cg_rel_mat, '+', docto_sel.fn_puntos + docto_sel.fn_valor_tasa, '-',"   +
				"               docto_sel.fn_valor_tasa - docto_sel.fn_puntos, '*', docto_sel.fn_puntos * docto_sel.fn_valor_tasa, '/',"   +
				"               docto_sel.fn_valor_tasa / docto_sel.fn_puntos)  AS tasa_interes,"   +
				"         docto.ig_plazo_credito AS plazo,"   +
				"         TO_CHAR(docto.df_fecha_venc_credito, 'DD/MM/YYYY') AS fch_vencimiento,"   +
				"         docto_sel.fn_importe_interes AS monto_interes,"   +
				"         cti.cd_descripcion			 AS tipo_cobro_inte,"   +
				"         ed.ic_estatus_docto AS estatus_soli,"   +
				" 		camb_estatus.cg_numero_pago  AS num_pago,"   +
				" 		TO_CHAR(camb_estatus.dc_fecha_cambio, 'DD/MM/YYYY') AS fecha_pago"   +
				" 		,moneda.ic_moneda"   +
				"  FROM dis_documento          docto"   +
				"   ,dis_docto_seleccionado    docto_sel"   +
				"   ,dis_solicitud             soli"   +
				" 	 ,dis_cambio_estatus       camb_estatus"   +
				"   ,com_linea_credito         lin_cred"   +
				"   ,comrel_producto_epo       prod_epo"   +
				"   ,comcat_producto_nafin     prod_nafin"   +
				"   ,comcat_moneda             moneda"   +
				"   ,comcat_epo                epo"   +
				"   ,comcat_pyme               pyme"   +
				" 	 ,comcat_if				    	 ci"   +
				" 	 ,comcat_tasa			    	 ct"   +
				" 	 ,comcat_tipo_cobro_interes cti"   +
				" 	 ,comcat_estatus_docto			ed"   +
				"  WHERE docto.ic_documento = docto_sel.ic_documento"   +
				" 	  AND docto.ic_documento = soli.ic_documento"   +
				" 	  AND docto.ic_documento = camb_estatus.ic_documento"   +
				"    AND docto.ic_linea_credito = lin_cred.ic_linea_credito"   +
				"    AND docto.ic_epo = epo.ic_epo"   +
				"    AND docto.ic_pyme = pyme.ic_pyme"   +
				"    AND docto.ic_epo  = prod_epo.ic_epo"   +
				" 	  AND docto.ic_estatus_docto = ed.ic_estatus_docto"   +
				"    AND prod_epo.ic_producto_nafin = prod_nafin.ic_producto_nafin"   +
				"    AND lin_cred.ic_moneda = moneda.ic_moneda"   +
				" 	  AND camb_estatus.dc_fecha_cambio IN(SELECT MAX(dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_documento = docto.ic_documento)"   +
				" 	  AND lin_cred.ic_if = ci.ic_if"   +
				" 	  AND lin_cred.ic_tipo_cobro_interes = cti.ic_tipo_cobro_interes"   +
				" 	  AND soli.ic_tasaif = ct.ic_tasa"   +
				"    AND prod_nafin.ic_producto_nafin = 4"   +
				"    AND epo.cs_habilitado  = 'S' "
		);
	
		if("D".equals(tipoCredito)){
			sqlSentenciaTipoCreditoD.append( getCondicionGeneral().toString());
			sqlSentencia.append(sqlSentenciaTipoCreditoD.toString());
		}else if("C".equals(tipoCredito)){
			sqlSentenciaTipoCreditoC.append( getCondicionGeneral().toString());
			sqlSentencia.append(sqlSentenciaTipoCreditoC.toString());
		}else{
			sqlSentenciaTipoCreditoD.append( getCondicionGeneral().toString());
			sqlSentenciaTipoCreditoC.append( getCondicionGeneral().toString());
			sqlSentencia.append( sqlSentenciaTipoCreditoD.toString()+ " UNION ALL "+sqlSentenciaTipoCreditoC.toString());
		}		
			
		sqlSentencia.append( " ORDER BY num_credito, epo, distribuidor ");
			
		log.info("sqlSentencia getDocumentQueryFile "+sqlSentencia);
		log.info("conditions  "+conditions);
		log.info("getDocumentQueryFile(S)");
		
		return sqlSentencia.toString();
	}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		log.info("crearPageCustomFile(E)");
		
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try {
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
		
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
			pdfDoc.setLTable(19, 100);
			pdfDoc.setLCell("Num. cr�dito","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("EPO","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Dist.","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("IF","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Tipo cr�dito","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Resp. pago de inter�s","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Tipo de cobranza","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Fecha de operaci�n","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Monto ","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Referencia tasa","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Tasa de inter�s","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Plazo","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Fecha de vencimiento","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Monto inter�s","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Tipo de cobro de inter�s","celda01",ComunesPDF.CENTER); 
			pdfDoc.setLCell("Estatus","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Num. pago","celda01",ComunesPDF.CENTER);			
			pdfDoc.setLCell("Fecha cambio de estatus","celda01",ComunesPDF.CENTER);
			
			while (rs.next())	{
				String	numDocFinal= (rs.getString("num_credito")==null)?"":rs.getString("num_credito");
				String	nombreEPO 			= (rs.getString("epo")==null)?"":rs.getString("epo");
				String	nombreDistribuidor = (rs.getString("distribuidor")==null)?"":rs.getString("distribuidor");
				String	intermediario       = (rs.getString("Intermediario")==null)?"":rs.getString("Intermediario");
				String	tipoCredito 		= (rs.getString("tipo_credito")==null)?"":rs.getString("tipo_credito");
				String	responsableInteres = (rs.getString("responsable_interes")==null)?"":rs.getString("responsable_interes");
				String	tipoCobranza 		= (rs.getString("tipo_cobranza")==null)?"":rs.getString("tipo_cobranza");
				String	fechaOperacion 	= (rs.getString("fecha_operacion")==null)?"":rs.getString("fecha_operacion");
				String	tipoMoneda 		= (rs.getString("tipo_moneda")==null)?"":rs.getString("tipo_moneda");
				String	monto 				= (rs.getString("monto")==null)?"":rs.getString("monto");
				String	tipoConversion 	= (rs.getString("tipo_conversion")==null)?"":rs.getString("tipo_conversion");
				String	tipoCambio 		= (rs.getString("tipo_cambio")==null)?"":rs.getString("tipo_cambio");
				String	montoValuado 		= (rs.getString("monto_valuado")==null)?"":rs.getString("monto_valuado");
				String	referenciaTasa 			= (rs.getString("ref_tasa")==null)?"":rs.getString("ref_tasa");
				String	tasaInteres 		= (rs.getString("tasa_interes")==null)?"":rs.getString("tasa_interes");
				String	plazo 				= (rs.getString("plazo")==null)?"":rs.getString("plazo");
				String	fechaVencimiento 	= (rs.getString("fch_vencimiento")==null)?"":rs.getString("fch_vencimiento");
				String	montoInteres 		= (rs.getString("monto_interes")==null)?"":rs.getString("monto_interes");
				String	tipoCobroInteres		= (rs.getString("tipo_cobro_inte")==null)?"":rs.getString("tipo_cobro_inte");
				String	estatus 		= (rs.getString("estatus_soli")==null)?"":rs.getString("estatus_soli");
				String	numPago    		= (rs.getString("num_pago")==null)?"":rs.getString("num_pago");
				String	fechaPago   		= (rs.getString("fecha_pago")==null)?"":rs.getString("fecha_pago");
				String	Clavemoneda   		= (rs.getString("ic_moneda")==null)?"":rs.getString("ic_moneda");			////////////
				
				pdfDoc.setLCell(numDocFinal,"formas",ComunesPDF.LEFT);
				pdfDoc.setLCell(nombreEPO,"formas",ComunesPDF.LEFT);
				pdfDoc.setLCell(nombreDistribuidor,"formas",ComunesPDF.LEFT);
				pdfDoc.setLCell(intermediario,"formas",ComunesPDF.LEFT);
				pdfDoc.setLCell(tipoCredito,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(responsableInteres,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(tipoCobranza,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(fechaOperacion,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(tipoMoneda,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell("$"+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setLCell(referenciaTasa ,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(tasaInteres+ "%","formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(plazo,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoInteres,2),"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(tipoCobroInteres,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(estatus,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(numPago,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(fechaPago,"formas",ComunesPDF.CENTER);
			}
			pdfDoc.addLTable();
			pdfDoc.endDocument();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally{
			
		}
		log.info("crearPageCustomFile(S)");
		return nombreArchivo;
	}
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		log.info("crearCustomFile (E)");

		String nombreArchivo = "";
		HttpSession session = request.getSession();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		ComunesPDF pdfDoc = new ComunesPDF();

		if(tipo.equals("CSV")){
			try {
					contenidoArchivo.append("Num. cr�dito,EOP, Dist, IF,  Tipo cr�dito , Resp. pago de inter�s,  Tipo cobranza,"+ 
												" Fecha de operacion, Moneda ,Monto, Referencia taza, Taza de inter�s, "+
												" Plazo, Fecha vencimiento, Monto interes, Tipo cobro inter�s, Estatus, "+
												" Num. pago, Fecha de cambio estatus, \n"
												);

				while (rs.next()){
					String numDocFinal        = (rs.getString("num_credito")         ==null)?"":rs.getString("num_credito");
					String nombreEPO          = (rs.getString("epo")                 ==null)?"":rs.getString("epo");
					String nombreDistribuidor = (rs.getString("distribuidor")        ==null)?"":rs.getString("distribuidor");
					String intermediario      = (rs.getString("Intermediario")       ==null)?"":rs.getString("Intermediario");
					String tipoCredito        = (rs.getString("tipo_credito")        ==null)?"":rs.getString("tipo_credito");
					String responsableInteres = (rs.getString("responsable_interes") ==null)?"":rs.getString("responsable_interes");
					String tipoCobranza       = (rs.getString("tipo_cobranza")       ==null)?"":rs.getString("tipo_cobranza");
					String fechaOperacion     = (rs.getString("fecha_operacion")     ==null)?"":rs.getString("fecha_operacion");
					String tipoMoneda         = (rs.getString("tipo_moneda")         ==null)?"":rs.getString("tipo_moneda");
					String monto              = (rs.getString("monto")               ==null)?"":rs.getString("monto");

					String tipoConversion     = (rs.getString("tipo_conversion")     ==null)?"":rs.getString("tipo_conversion");
					String tipoCambio         = (rs.getString("tipo_cambio")         ==null)?"":rs.getString("tipo_cambio");
					String montoValuado       = (rs.getString("monto_valuado")       ==null)?"":rs.getString("monto_valuado");

					String referenciaTasa     = (rs.getString("ref_tasa")            ==null)?"":rs.getString("ref_tasa");
					String tasaInteres        = (rs.getString("tasa_interes")        ==null)?"":rs.getString("tasa_interes");
					String plazo              = (rs.getString("plazo")               ==null)?"":rs.getString("plazo");
					String fechaVencimiento   = (rs.getString("fch_vencimiento")     ==null)?"":rs.getString("fch_vencimiento");
					String montoInteres       = (rs.getString("monto_interes")       ==null)?"":rs.getString("monto_interes");
					String tipoCobroInteres   = (rs.getString("tipo_cobro_inte")     ==null)?"":rs.getString("tipo_cobro_inte");
					String estatus            = (rs.getString("estatus_soli")        ==null)?"":rs.getString("estatus_soli");
					String numPago            = (rs.getString("num_pago")            ==null)?"":rs.getString("num_pago");
					String fechaPago          = (rs.getString("fecha_pago")          ==null)?"":rs.getString("fecha_pago");
					String Clavemoneda        = (rs.getString("ic_moneda")           ==null)?"":rs.getString("ic_moneda");

					contenidoArchivo.append(numDocFinal	+","+
												nombreEPO.replaceAll(",","")+","+
												nombreDistribuidor.replaceAll(",","")+","+
												intermediario.replaceAll(",","")+","+
												tipoCredito+","+
												responsableInteres+","+
												tipoCobranza+","+
												fechaOperacion+","+
												tipoMoneda+","+
												monto+","+
												referenciaTasa+","+
												tasaInteres+","+
												plazo+","+
												fechaVencimiento+","+
												montoInteres+","+
												tipoCobroInteres+","+
												estatus+","+
												numPago+","+
												fechaPago+","+"\n"
											);
				}
				creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
				nombreArchivo = creaArchivo.getNombre();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo ", e);
			}

		} else if(tipo.equals("PDF")){

			try {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));  

				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

				pdfDoc.setLTable(19, 100);
				pdfDoc.setLCell("Num. cr�dito",             "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("EPO",                      "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Dist.",                    "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("IF",                       "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo cr�dito",             "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Resp. pago de inter�s",    "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de cobranza",         "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de operaci�n",       "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda",                   "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto ",                   "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Referencia tasa",          "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tasa de inter�s",          "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo",                    "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de vencimiento",     "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto inter�s",            "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de cobro de inter�s", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Estatus",                  "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Num. pago",                "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha cambio de estatus",  "celda01",ComunesPDF.CENTER);
				pdfDoc.setLHeaders();

				while (rs.next()) {
					String numDocFinal        = (rs.getString("num_credito")         ==null)?"":rs.getString("num_credito");
					String nombreEPO          = (rs.getString("epo")                 ==null)?"":rs.getString("epo");
					String nombreDistribuidor = (rs.getString("distribuidor")        ==null)?"":rs.getString("distribuidor");
					String intermediario      = (rs.getString("Intermediario")       ==null)?"":rs.getString("Intermediario");
					String tipoCredito        = (rs.getString("tipo_credito")        ==null)?"":rs.getString("tipo_credito");
					String responsableInteres = (rs.getString("responsable_interes") ==null)?"":rs.getString("responsable_interes");
					String tipoCobranza       = (rs.getString("tipo_cobranza")       ==null)?"":rs.getString("tipo_cobranza");
					String fechaOperacion     = (rs.getString("fecha_operacion")     ==null)?"":rs.getString("fecha_operacion");
					String tipoMoneda         = (rs.getString("tipo_moneda")         ==null)?"":rs.getString("tipo_moneda");
					String monto              = (rs.getString("monto")               ==null)?"":rs.getString("monto");
					String tipoConversion     = (rs.getString("tipo_conversion")     ==null)?"":rs.getString("tipo_conversion");
					String tipoCambio         = (rs.getString("tipo_cambio")         ==null)?"":rs.getString("tipo_cambio");
					String montoValuado       = (rs.getString("monto_valuado")       ==null)?"":rs.getString("monto_valuado");
					String referenciaTasa     = (rs.getString("ref_tasa")            ==null)?"":rs.getString("ref_tasa");
					String tasaInteres        = (rs.getString("tasa_interes")        ==null)?"":rs.getString("tasa_interes");
					String plazo              = (rs.getString("plazo")               ==null)?"":rs.getString("plazo");
					String fechaVencimiento   = (rs.getString("fch_vencimiento")     ==null)?"":rs.getString("fch_vencimiento");
					String montoInteres       = (rs.getString("monto_interes")       ==null)?"":rs.getString("monto_interes");
					String tipoCobroInteres   = (rs.getString("tipo_cobro_inte")     ==null)?"":rs.getString("tipo_cobro_inte");
					String estatus            = (rs.getString("estatus_soli")        ==null)?"":rs.getString("estatus_soli");
					String numPago            = (rs.getString("num_pago")            ==null)?"":rs.getString("num_pago");
					String fechaPago          = (rs.getString("fecha_pago")          ==null)?"":rs.getString("fecha_pago");
					String Clavemoneda        = (rs.getString("ic_moneda")           ==null)?"":rs.getString("ic_moneda");

					pdfDoc.setLCell(numDocFinal,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(nombreEPO,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(nombreDistribuidor,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(intermediario,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(tipoCredito,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(responsableInteres,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipoCobranza,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaOperacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipoMoneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(referenciaTasa ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tasaInteres+ "%","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(plazo,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoInteres,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipoCobroInteres,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(estatus,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(numPago,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaPago,"formas",ComunesPDF.CENTER);
				}
				pdfDoc.addLTable();
				pdfDoc.endDocument();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo ", e);
			}

		}

		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}

	public String getCondicionGeneral(){
		StringBuffer condicion = new StringBuffer("");
		
		if (!numDocFinal.equals("") ){	
			condicion.append(" AND docto.ic_documento = ?");
			conditions.add(numDocFinal);
		}
		if (!tipoCobroInter.equals("")){
			condicion.append(" AND lin_cred.ic_tipo_cobro_interes = ?");
			conditions.add(tipoCobroInter);
		}
		if(!claveIF.equals("")){
			condicion.append(" AND lin_cred.ic_if = ?");
			conditions.add(claveIF);
		}
		if (!moneda.equals("") )		{
			condicion.append(" AND docto.ic_moneda = ?");
			conditions.add(moneda);
		}
		
		if ( !montoMin.equals("0"))		{
			condicion.append(" AND docto.fn_monto >= ?");
			conditions.add(montoMin);
		}
		if (!montoMax.equals("0"))		{
			condicion.append(" AND docto.fn_monto <= ?");
			conditions.add(montoMax);
		}
		if (!fechaPagoInicio.equals("") )	{
			condicion.append(" AND camb_estatus.dc_fecha_cambio >= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaPagoInicio);
		}
		if (!fechaPagoFin.equals("") )	{
			condicion.append(" AND camb_estatus.dc_fecha_cambio <= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaPagoFin);
		}
		if (!fechaVencInicio.equals(""))	{
				condicion.append(" AND docto.df_fecha_venc_credito >= TO_DATE(?, 'DD/MM/YYYY') ");
				conditions.add(fechaVencInicio);
		}
		if (!fechaVencFin.equals(""))	{
			condicion.append(" AND docto.df_fecha_venc_credito <= TO_DATE(?, 'DD/MM/YYYY') ");
			conditions.add(fechaVencFin);
		}
		
		
		if (!estatus.equals("") )		{
			condicion.append(" AND soli.ic_estatus_solic = ?");
			conditions.add(estatus);
		}
		
			
		return condicion.toString();
	}
	
	/**
	 * 
	 * @return
	 */
	public List getDatosCatalogoIFDist(){
	log.info("getDatosCatalogoIFDist(E)");
		sqlSentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer condicion  = new StringBuffer("");
		AccesoDB con = new AccesoDB();
		PreparedStatement pstmt = null;
		ResultSet rs = null;	
		HashMap datos =  new HashMap();
		List registros  = new ArrayList();
		try{
			con.conexionDB();
			sqlSentencia.append(	"	SELECT   ci.ic_if as clave, ci.cg_razon_social as descripcion	"+
										"	FROM comcat_if ci, comrel_producto_if rpi	"+
										"	WHERE ci.ic_if = rpi.ic_if AND rpi.ic_producto_nafin = 4	"+
										"	ORDER BY ci.cg_razon_social	");

			pstmt = con.queryPrecompilado(sqlSentencia.toString());
			rs = pstmt.executeQuery();
			while(rs.next()) {			
				datos = new HashMap();			
				datos.put("clave", rs.getString("clave") );
				datos.put("descripcion", rs.getString("descripcion"));
				registros.add(datos);
			}
			rs.close();
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("getDatosCatalogoIFDist  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 	
	
		log.info("sqlSentencia : "+sqlSentencia.toString());
		log.info("conditions "+conditions);
		
		log.info("getDatosCatalogoIFDist (S) ");
		return registros;
	
	
	}
/********************************
 * SETTESR Y GETTERS
 *******************************/
	public void setConditions(List conditions) {
		this.conditions = conditions;
	}

	public List getConditions() {
		return conditions;
	}

	public void setNumDocFinal(String numDocFinal) {
		this.numDocFinal = numDocFinal;
	}


	public String getNumDocFinal() {
		return numDocFinal;
	}


	public void setTipoCobroInter(String tipoCobroInter) {
		this.tipoCobroInter = tipoCobroInter;
	}


	public String getTipoCobroInter() {
		return tipoCobroInter;
	}


	public void setTipoCredito(String tipoCredito) {
		this.tipoCredito = tipoCredito;
	}


	public String getTipoCredito() {
		return tipoCredito;
	}


	public void setClaveIF(String claveIF) {
		this.claveIF = claveIF;
	}


	public String getClaveIF() {
		return claveIF;
	}


	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}


	public String getMoneda() {
		return moneda;
	}


	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setMontoMin(String montoMin) {
		this.montoMin = montoMin;
	}

	public String getMontoMin() {
		return montoMin;
	}

	public void setMontoMax(String montoMax) {
		this.montoMax = montoMax;
	}

	public String getMontoMax() {
		return montoMax;
	}

	public void setFechaPagoInicio(String fechaPagoInicio) {
		this.fechaPagoInicio = fechaPagoInicio;
	}

	public String getFechaPagoInicio() {
		return fechaPagoInicio;
	}

	public void setFechaPagoFin(String fechaPagoFin) {
		this.fechaPagoFin = fechaPagoFin;
	}

	public String getFechaPagoFin() {
		return fechaPagoFin;
	}

	public void setFechaVencInicio(String fechaVencInicio) {
		this.fechaVencInicio = fechaVencInicio;
	}

	public String getFechaVencInicio() {
		return fechaVencInicio;
	}

	public void setFechaVencFin(String fechaVencFin) {
		this.fechaVencFin = fechaVencFin;
	}

	public String getFechaVencFin() {
		return fechaVencFin;
	}

}