package com.netro.distribuidores;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Registros;

public class DetalleCambiosDoctos{
 	 
	private StringBuffer qrysentencia;
	private String ic_docto="";
	private Registros registros;
	private List conditions;
	
	public void setIcDocumento(String ic_docto) {
		this.ic_docto = ic_docto;
	}

	public Registros executeQuery() {
		AccesoDB con = new AccesoDB();
		Registros registros = new Registros();
		conditions = new ArrayList();
		this.qrysentencia = new StringBuffer();
		this.qrysentencia.append("  SELECT distinct TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fech_cambio, "+
							" TO_CHAR (ce.df_fecha_emision_anterior, 'dd/mm/yyyy') AS fech_emi_ant, "+
							" ce.ct_cambio_motivo, ce.fn_monto_anterior, ce.fn_monto_nuevo, "+
							" TO_CHAR (ce.df_fecha_emision_nueva, 'dd/mm/yyyy') AS fech_emi_new, "+
							" TO_CHAR (ce.df_fecha_venc_anterior, 'dd/mm/yyyy') AS fech_venc_ant, "+
							" TO_CHAR (ce.df_fecha_venc_nueva, 'dd/mm/yyyy') AS fech_venc_new, "+
							" cce.cd_descripcion, TFA.cd_descripcion as modo_plazo_anterior"	+
							" ,TFN.cd_descripcion as modo_plazo_nuevo"	+
							" FROM dis_cambio_estatus ce, comcat_cambio_estatus cce "	+
							" ,comcat_tipo_financiamiento TFA,comcat_tipo_financiamiento TFN"	+
							" WHERE ic_documento = ? "	+
							" AND cce.ic_cambio_estatus = ce.ic_cambio_estatus "	+
							" AND ce.ic_tipo_finan_ant = TFA.ic_tipo_financiamiento(+)"	+
							" AND ce.ic_tipo_finan_nuevo = TFN.ic_tipo_financiamiento(+) ");
		try{
			con.conexionDB();
			conditions.add(this.ic_docto);
			registros = con.consultarDB(this.qrysentencia.toString(),conditions);
			return registros;
		} catch(Exception e) {
			throw new AppException("detalleCambiosDoctos::ExecuteQuery(Exception) ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(true); //Para consultas que hagan uso de tablas remotas via dblink
				con.cierraConexionDB();
			}
		}
	}
}//Class End.