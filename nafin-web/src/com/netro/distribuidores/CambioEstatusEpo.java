package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class CambioEstatusEpo implements IQueryGeneratorRegExtJS{
 

	public CambioEstatusEpo() {  }

	private List conditions;
	StringBuffer query;
	StringBuffer qrySentenciaDM;
	StringBuffer qrySentenciaCCC;


	private static final Log log = ServiceLocator.getInstance().getLog(CambioEstatusEpo.class);//Variable para enviar mensajes al log.
	public String noEstatus;
	private String noEpo;
	private String tiposCredito;

 
	public String getAggregateCalculationQuery() {
		query 		= new StringBuffer();
		conditions 		= new ArrayList();
		if(noEstatus.equals("21")){
		
			query	.append(" SELECT "+
			"m.cd_nombre as MONEDA,"+
			"	count(*) as TOTAL_REGISTROS, "+
			" Sum(d.fn_monto) as TOTAL_MONTO, "+
			" Sum((  NVL (d.fn_monto, 0) * DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1')) )AS MONTO_VALUADO "+	
			"	FROM DIS_DOCUMENTO D "+
						" ,COMCAT_PYME PY"+
						" ,COMCAT_MONEDA M"+
						" ,COMREL_PRODUCTO_EPO PE"+
						" ,COMCAT_PRODUCTO_NAFIN PN"+
						" ,COM_TIPO_CAMBIO TC"+
						" ,COMCAT_TIPO_FINANCIAMIENTO TF"+
						" ,COMCAT_EPO E"+
						" ,DIS_CAMBIO_ESTATUS CE"+
						" ,COMREL_PYME_EPO_X_PRODUCTO PEP"+
						" WHERE D.IC_PYME = PY.IC_PYME"+
						" AND D.IC_MONEDA = M.IC_MONEDA"+
						" AND PN.IC_PRODUCTO_NAFIN = 4"+
						" AND PEP.IC_PYME = PY.IC_PYME"+
						" AND PEP.IC_EPO = E.IC_EPO"+
						" AND PEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
						" AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"+
						" AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
						" AND PE.IC_EPO = D.IC_EPO"+
						" AND E.IC_EPO = D.IC_EPO"+
						" AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
											" AND M.ic_moneda = TC.ic_moneda" +
						" AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
						" AND D.IC_ESTATUS_DOCTO = 9"+
						" AND CE.IC_CAMBIO_ESTATUS = 21"+
						" AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE)" 
						);	
		
			if(!"".equals(noEpo)){	
				query.append(" AND D.IC_EPO =  ? ");
				conditions.add(new Integer(noEpo));
			}	
			
			query.append(" group by d.ic_moneda , m.cd_nombre ");
			 
		}else	if(noEstatus.equals("4")){
			
			query	.append(" SELECT "+
			"m.cd_nombre as MONEDA,"+
			"	count(*) as TOTAL_REGISTROS, "+
			" Sum(d.fn_monto) as TOTAL_MONTO, "+
			" Sum((  NVL (d.fn_monto, 0) * DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1')) )AS MONTO_VALUADO "+	
			" FROM DIS_DOCUMENTO D"+
					" ,COMCAT_PYME PY"+
					" ,COMCAT_MONEDA M"+
					" ,COMREL_PRODUCTO_EPO PE"+
					" ,COMCAT_PRODUCTO_NAFIN PN"+
					" ,COM_TIPO_CAMBIO TC"+
					" ,COMCAT_TIPO_FINANCIAMIENTO TF"+
					" ,COMCAT_EPO E"+
					" ,DIS_CAMBIO_ESTATUS CE"+
					" ,COMREL_PYME_EPO_X_PRODUCTO PEP"+
					" WHERE D.IC_PYME = PY.IC_PYME"+
					" AND D.IC_MONEDA = M.IC_MONEDA"+
					" AND PN.IC_PRODUCTO_NAFIN = 4"+
					" AND PEP.IC_PYME = PY.IC_PYME"+
					" AND PEP.IC_EPO = E.IC_EPO"+
					" AND PEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
					" AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"+
					" AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
					" AND PE.IC_EPO = D.IC_EPO"+
					" AND E.IC_EPO = D.IC_EPO"+
					" AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
                    " AND M.ic_moneda = TC.ic_moneda" +
					" AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
					" AND D.IC_ESTATUS_DOCTO = 5"+
					" AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE)"+				
					" AND CE.IC_CAMBIO_ESTATUS = 4");
					
				if(!"".equals(noEpo)){	
					query.append(" AND D.IC_EPO =  ? ");
					conditions.add(new Integer(noEpo));
				}	
				
				query.append(" group by d.ic_moneda , m.cd_nombre ");
		
		}else	if(noEstatus.equals("22")){
		
			query	.append(" SELECT "+
				"m.cd_nombre as MONEDA,"+
				"	count(*) as TOTAL_REGISTROS, "+
				" Sum(d.fn_monto) as TOTAL_MONTO, "+
				" Sum((  NVL (d.fn_monto, 0) * DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1')) )AS MONTO_VALUADO "+	
				
				" FROM DIS_DOCUMENTO D"+
				" ,COMCAT_PYME PY"+
				" ,COMCAT_MONEDA M"+
				" ,COMREL_PRODUCTO_EPO PE"+
				" ,COMCAT_PRODUCTO_NAFIN PN"+
				" ,COM_TIPO_CAMBIO TC"+
				" ,COMCAT_TIPO_FINANCIAMIENTO TF"+
				" ,COMCAT_EPO E"+
				" ,DIS_CAMBIO_ESTATUS CE"+
				" ,COMREL_PYME_EPO_X_PRODUCTO PEP"+
				" WHERE D.IC_PYME = PY.IC_PYME"+
				" AND D.IC_MONEDA = M.IC_MONEDA"+
				" AND PN.IC_PRODUCTO_NAFIN = 4"+
				" AND PEP.IC_PYME = PY.IC_PYME"+
				" AND PEP.IC_EPO = E.IC_EPO"+
				" AND PEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
				" AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"+
				" AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
				" AND PE.IC_EPO = D.IC_EPO"+
				" AND E.IC_EPO = D.IC_EPO"+
				" AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
				" AND M.ic_moneda = TC.ic_moneda" +
				" AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
				" AND D.IC_ESTATUS_DOCTO = 9"+
				" AND CE.IC_CAMBIO_ESTATUS = 22"+
				" AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE)"
				);				
		
			if(!"".equals(noEpo)){	
					query.append(" AND D.IC_EPO =  ? ");
					conditions.add(new Integer(noEpo));
				}	
				
				query.append(" group by d.ic_moneda , m.cd_nombre ");
		
		}else	if(noEstatus.equals("24")){
			query	.append(" SELECT "+
				"m.cd_nombre as MONEDA,"+
				"	count(*) as TOTAL_REGISTROS, "+
				" Sum(d.fn_monto) as TOTAL_MONTO, "+
				" Sum((  NVL (d.fn_monto, 0) * DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1')) )AS MONTO_VALUADO "+	
				" FROM DIS_DOCUMENTO D"+
				" ,COMCAT_PYME PY"+
				" ,COMCAT_MONEDA M"+
				" ,COMREL_PRODUCTO_EPO PE"+
				" ,COMCAT_PRODUCTO_NAFIN PN"+
				" ,COM_TIPO_CAMBIO TC"+
				" ,COMCAT_TIPO_FINANCIAMIENTO TF"+
				" ,COMCAT_EPO E"+
				" ,DIS_CAMBIO_ESTATUS CE"+
				" ,COMREL_PYME_EPO_X_PRODUCTO PEP"+
				" WHERE D.IC_PYME = PY.IC_PYME"+
				" AND D.IC_MONEDA = M.IC_MONEDA"+
				" AND PN.IC_PRODUCTO_NAFIN = 4"+
				" AND PEP.IC_PYME = PY.IC_PYME"+
				" AND PEP.IC_EPO = E.IC_EPO"+
				" AND PEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
				" AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"+
				" AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
				" AND PE.IC_EPO = D.IC_EPO"+
				" AND E.IC_EPO = D.IC_EPO"+
				" AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
        " AND M.ic_moneda = TC.ic_moneda" +
				" AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
				" AND CE.IC_CAMBIO_ESTATUS = 24"+
				" AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE) "
				);
				if(!"".equals(noEpo)){	
					query.append(" AND D.IC_EPO =  ? ");
					conditions.add(new Integer(noEpo));
				}	
				
				query.append(" group by d.ic_moneda , m.cd_nombre ");
			
		}
	
		log.debug("qrySentencia:  getAggregateCalculationQuery  "+query);
		log.debug("conditions: "+conditions);
		
		return query.toString();
 	}
		

	public String getDocumentQuery(){
		conditions = new ArrayList();	
		query 		= new StringBuffer();
		
		log.debug("getDocumentQuery)"+query.toString());
		log.debug("getDocumentQuery)"+conditions);
		return query.toString();
 	}
	
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
		query 		= new StringBuffer();
		
		log.debug("getDocumentSummaryQueryForIds "+query.toString());
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
		
						
		return query.toString();
 	}
		
			
	public String getDocumentQueryFile(){
		conditions = new ArrayList();
		query = new StringBuffer();
		qrySentenciaDM = new StringBuffer();
		qrySentenciaCCC = new StringBuffer();
	
		
	
		if(noEstatus.equals("4")){ //Negociable a Baja
		
			query.append(" SELECT PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"+
					" 	,D.IG_NUMERO_DOCTO"+
					" 	,D.CC_ACUSE"+
					"	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"+
					"	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"+
					"	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"+
					"	,D.IG_PLAZO_DOCTO"+
					"	,M.CD_NOMBRE AS MONEDA"+
					"	,D.FN_MONTO"+
          "	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
					" ,DECODE (m.ic_moneda, 54, DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') , '') as TIPO_CAMBIO " +
					"	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"+
					"	,NVL(D.FN_PORC_DESCUENTO,0) AS FN_PORC_DESCUENTO  "+
					"	,TF.CD_DESCRIPCION AS MODO_PLAZO"+
					"	,M.ic_moneda"+
					"	,D.ic_documento"+
					"	,E.cg_razon_social as NOMBRE_EPO"+
					"	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
					"	,decode(PEP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"+
					"	,D.cc_acuse "+
					"	,to_char(D.df_carga,'dd/mm/yyyy') as FECHA_PUBLICACION"+
					"	,to_char(CE.DC_FECHA_CAMBIO,'dd/mm/yyyy') as FECHA_CAMBIO"+
					"	,CE.ct_cambio_motivo"+
					" ,d.CG_VENTACARTERA as CG_VENTACARTERA,  "+
					" ( NVL( d.fn_monto, 0) * NVL (d.fn_porc_descuento, 0) /100) as monto_descuento , "+
					"  ( NVL( d.fn_monto, 0)  * DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1')) as monto_Valuado "+
							
					" FROM DIS_DOCUMENTO D"+
					" ,COMCAT_PYME PY"+
					" ,COMCAT_MONEDA M"+
					" ,COMREL_PRODUCTO_EPO PE"+
					" ,COMCAT_PRODUCTO_NAFIN PN"+
					" ,COM_TIPO_CAMBIO TC"+
					" ,COMCAT_TIPO_FINANCIAMIENTO TF"+
					" ,COMCAT_EPO E"+
					" ,DIS_CAMBIO_ESTATUS CE"+
					" ,COMREL_PYME_EPO_X_PRODUCTO PEP"+
					" WHERE D.IC_PYME = PY.IC_PYME"+
					" AND D.IC_MONEDA = M.IC_MONEDA"+
					" AND PN.IC_PRODUCTO_NAFIN = 4"+
					" AND PEP.IC_PYME = PY.IC_PYME"+
					" AND PEP.IC_EPO = E.IC_EPO"+
					" AND PEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
					" AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"+
					" AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
					" AND PE.IC_EPO = D.IC_EPO"+
					" AND E.IC_EPO = D.IC_EPO"+
					" AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
                    " AND M.ic_moneda = TC.ic_moneda" +
					" AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
					" AND D.IC_ESTATUS_DOCTO = 5"+
					" AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE)"+				
					" AND CE.IC_CAMBIO_ESTATUS = 4");
					
				if(!"".equals(noEpo)){	
					query.append(" AND D.IC_EPO =  ? ");
					conditions.add(new Integer(noEpo));
				}	
				
				
		}else 	if(noEstatus.equals("22")){ //Negociable a Vencido sin Operar
		
			query.append(" SELECT PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"+
						" 	,D.IG_NUMERO_DOCTO"+
						" 	,D.CC_ACUSE"+
						"	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"+
						"	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"+
						"	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"+
						"	,D.IG_PLAZO_DOCTO"+
						"	,M.CD_NOMBRE AS MONEDA"+
						"	,D.FN_MONTO"+
						"	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Peso-Dolar','') as TIPO_CONVERSION"   +					
						" ,DECODE (m.ic_moneda, 54, DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') , '') as TIPO_CAMBIO " +
						"	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"+
						"	,NVL(D.FN_PORC_DESCUENTO,0) AS FN_PORC_DESCUENTO"+
						"	,TF.CD_DESCRIPCION AS MODO_PLAZO"+
						"	,M.ic_moneda"+
						"	,D.ic_documento"+
						"	,E.cg_razon_social as NOMBRE_EPO"+
						"	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
						"	,decode(PEP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"+						
						"	,to_char(D.df_carga,'dd/mm/yyyy') as FECHA_PUBLICACION"+
						"	,to_char(CE.DC_FECHA_CAMBIO,'dd/mm/yyyy') as FECHA_CAMBIO"+
							" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
						" ,( NVL( d.fn_monto, 0) * NVL (d.fn_porc_descuento, 0) /100) as monto_descuento  "+
						",  ( NVL( d.fn_monto, 0)  * DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1')) as monto_Valuado "+
						"	,CE.ct_cambio_motivo as CT_CAMBIO_MOTIVO"+
						" FROM DIS_DOCUMENTO D"+
						" ,COMCAT_PYME PY"+
						" ,COMCAT_MONEDA M"+
						" ,COMREL_PRODUCTO_EPO PE"+
						" ,COMCAT_PRODUCTO_NAFIN PN"+
						" ,COM_TIPO_CAMBIO TC"+
						" ,COMCAT_TIPO_FINANCIAMIENTO TF"+
						" ,COMCAT_EPO E"+
						" ,DIS_CAMBIO_ESTATUS CE"+
						" ,COMREL_PYME_EPO_X_PRODUCTO PEP"+
						" WHERE D.IC_PYME = PY.IC_PYME"+
						" AND D.IC_MONEDA = M.IC_MONEDA"+
						" AND PN.IC_PRODUCTO_NAFIN = 4"+
						" AND PEP.IC_PYME = PY.IC_PYME"+
						" AND PEP.IC_EPO = E.IC_EPO"+
						" AND PEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
						" AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"+
						" AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
						" AND PE.IC_EPO = D.IC_EPO"+
						" AND E.IC_EPO = D.IC_EPO"+
						" AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
											" AND M.ic_moneda = TC.ic_moneda" +
						" AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
						" AND D.IC_ESTATUS_DOCTO = 9"+
						" AND CE.IC_CAMBIO_ESTATUS = 22"+
						" AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE)" 
						);				
		
			if(!"".equals(noEpo)){	
				query.append(" AND D.IC_EPO =  ? ");
				conditions.add(new Integer(noEpo));
			}	
		}else 	if(noEstatus.equals("14")){ //seleccionado pyme a rechazado IF
			
			qrySentenciaDM.append(" SELECT PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"+
					" 	,D.IG_NUMERO_DOCTO"+
					" 	,D.CC_ACUSE"+
					"	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"+
					"	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"+
					"	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"+
					"	,D.IG_PLAZO_DOCTO"+
					"	,M.CD_NOMBRE AS MONEDA"+
					"	,D.FN_MONTO"+
         	"	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
       		" ,DECODE (m.ic_moneda, 54, DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') , '') as TIPO_CAMBIO " +
					"	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"+
					"	,NVL(D.FN_PORC_DESCUENTO,0) AS FN_PORC_DESCUENTO"+
					"	,TF.CD_DESCRIPCION AS MODO_PLAZO"+
					"	,ED.CD_DESCRIPCION AS ESTATUS"+
					"	,M.ic_moneda"+
					"	,D.ic_documento"+
					"	,E.cg_razon_social as NOMBRE_EPO"+
					"	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
					"	,I.cg_razon_social as NOMBRE_IF"+
					"	,decode(PP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"+
					"	,DS.fn_importe_recibir as MONTO_CREDITO"+
					"	,D.ig_plazo_credito"+
					"	,TCI.cd_descripcion as TIPO_COBRO_INT"+
					"  	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) as REFERENCIA_INT"+
					"	,decode(DS.cg_rel_mat,'+',fn_valor_tasa+fn_puntos,'-',fn_valor_tasa-fn_puntos,'*',fn_valor_tasa*fn_puntos,'/',fn_valor_tasa/fn_puntos) as VALOR_TASA_INT"+
					"	,DS.fn_importe_interes as MONTO_TASA_INT"+
					"	,D.cc_acuse"+
					"	,to_char(D.df_carga,'dd/mm/yyyy') as FECHA_PUBLICACION"+
					"	,to_char(D.df_fecha_venc_credito,'dd/mm/yyyy') as FECHA_VENC_CREDITO"+
					"	,to_char(CE.dc_fecha_cambio,'dd/mm/yyyy') as FECHA_CAMBIO"+
					" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+					
					",  nvl(ds.fn_importe_recibir,0) + nvl( ds.fn_importe_interes, 0)  AS MONTO_CAP_INT "+
					" , ( NVL( d.fn_monto, 0) * NVL (d.fn_porc_descuento, 0) /100) as monto_descuento  "+
					"  ,( NVL( d.fn_monto, 0)  * DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1')) as monto_Valuado "+
					" ,'' as DF_OPERACIONIF "+				
					" FROM DIS_DOCUMENTO D"+
					" ,COMCAT_PYME PY"+
					" ,COMCAT_MONEDA M"+
					" ,COMREL_PRODUCTO_EPO PE"+
					" ,COMCAT_PRODUCTO_NAFIN PN"+
					" ,COM_TIPO_CAMBIO TC"+
					" ,COMCAT_TIPO_FINANCIAMIENTO TF"+
					" ,COMCAT_ESTATUS_DOCTO ED"+
					" ,DIS_LINEA_CREDITO_DM LC"+
					" ,COMCAT_EPO E"+
					" ,DIS_DOCTO_SELECCIONADO DS"+
					" ,COMCAT_IF I"+
					" ,COMREL_PYME_EPO_X_PRODUCTO PP"+
					" ,COMCAT_TIPO_COBRO_INTERES TCI"+
					" ,COMCAT_TASA CT"+
					" ,DIS_CAMBIO_ESTATUS CE"+
					" WHERE D.IC_PYME = PY.IC_PYME"+
					" AND DS.IC_DOCUMENTO = D.IC_DOCUMENTO"+
					" AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"+
					" AND D.IC_MONEDA = M.IC_MONEDA"+
					" AND PN.IC_PRODUCTO_NAFIN = 4"+
					" AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
					" AND PE.IC_EPO = D.IC_EPO"+
					" AND I.IC_IF = LC.IC_IF"+
					" AND E.IC_EPO = D.IC_EPO"+
					" AND PP.IC_EPO = E.IC_EPO"+
					" AND PP.IC_PYME = PY.IC_PYME"+
					" AND PP.IC_PRODUCTO_NAFIN = 4"+
					" AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
                    " AND M.ic_moneda = TC.ic_moneda" +
					" AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
					" AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"+
					" AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM"+
					" AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES"+
					" AND CT.IC_TASA = DS.IC_TASA"+
					" AND CE.IC_CAMBIO_ESTATUS = 14"+
				   " AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE)"
					);
					if("D".equals(tiposCredito)){
						if(!"".equals(noEpo)){	
							qrySentenciaDM.append(" AND D.IC_EPO=  ? ");
							conditions.add(new Integer(noEpo));
						}	
					}
			

			qrySentenciaCCC.append(" SELECT PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"+
					" 	,D.IG_NUMERO_DOCTO"+
					" 	,D.CC_ACUSE"+
					"	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"+
					"	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"+
					"	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"+
					"	,D.IG_PLAZO_DOCTO"+
					"	,M.CD_NOMBRE AS MONEDA"+
					"	,D.FN_MONTO"+
         	"	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
        	" ,DECODE (m.ic_moneda, 54, DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') , '') as TIPO_CAMBIO " +
					"	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"+
					"	,NVL(D.FN_PORC_DESCUENTO,0) AS FN_PORC_DESCUENTO"+
					"	,TF.CD_DESCRIPCION AS MODO_PLAZO"+
					"	,ED.CD_DESCRIPCION AS ESTATUS"+
					"	,M.ic_moneda"+
					"	,D.ic_documento"+
					"	,E.cg_razon_social as NOMBRE_EPO"+
					"	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
					"	,I.cg_razon_social as NOMBRE_IF"+
					"	,decode(PP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"+
					"	,DS.fn_importe_recibir as MONTO_CREDITO"+
					"	,D.ig_plazo_credito"+
					"	,TCI.cd_descripcion as TIPO_COBRO_INT"+
					"	,CT.cd_nombre||' '||DS.cg_rel_mat||' '||DS.fn_puntos as REFERENCIA_INT"+
					"	,decode(DS.cg_rel_mat,'+',fn_valor_tasa+fn_puntos,'-',fn_valor_tasa-fn_puntos,'*',fn_valor_tasa*fn_puntos,'/',fn_valor_tasa/fn_puntos) as VALOR_TASA_INT"+
					"	,DS.fn_importe_interes as MONTO_TASA_INT"+
					"	,D.cc_acuse"+
					"	,to_char(D.df_carga,'dd/mm/yyyy') as FECHA_PUBLICACION"+
					"	,to_char(D.df_fecha_venc_credito,'dd/mm/yyyy') as FECHA_VENC_CREDITO"+
					"	,to_char(CE.dc_fecha_cambio,'dd/mm/yyyy') as FECHA_CAMBIO"+
					" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
					",  nvl(ds.fn_importe_recibir,0) + nvl( ds.fn_importe_interes, 0) AS MONTO_CAP_INT "+
					" , ( NVL( d.fn_monto, 0) * NVL (d.fn_porc_descuento, 0) /100) as monto_descuento  "+
					"  ,( NVL( d.fn_monto, 0)  * DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1')) as monto_Valuado "+
					" ,'' as DF_OPERACIONIF "+				
					" FROM DIS_DOCUMENTO D"+
					" ,COMCAT_PYME PY"+
					" ,COMCAT_MONEDA M"+
					" ,COMREL_PRODUCTO_EPO PE"+
					" ,COMCAT_PRODUCTO_NAFIN PN"+
					" ,COM_TIPO_CAMBIO TC"+
					" ,COMCAT_TIPO_FINANCIAMIENTO TF"+
					" ,COMCAT_ESTATUS_DOCTO ED"+
					" ,COM_LINEA_CREDITO LC"+
					" ,COMCAT_EPO E"+
					" ,DIS_DOCTO_SELECCIONADO DS"+
					" ,COMCAT_IF I"+
					" ,COMREL_PYME_EPO_X_PRODUCTO PP"+
					" ,COMCAT_TIPO_COBRO_INTERES TCI"+
					" ,COMCAT_TASA CT"+
					" ,DIS_CAMBIO_ESTATUS CE"+
					" WHERE D.IC_PYME = PY.IC_PYME"+
					" AND DS.IC_DOCUMENTO = D.IC_DOCUMENTO"+
					" AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"+
					" AND D.IC_MONEDA = M.IC_MONEDA"+
					" AND PN.IC_PRODUCTO_NAFIN = 4"+
					" AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
					" AND PE.IC_EPO = D.IC_EPO"+
					" AND I.IC_IF = LC.IC_IF"+
					" AND E.IC_EPO = D.IC_EPO"+
					" AND PP.IC_EPO = E.IC_EPO"+
					" AND PP.IC_PYME = PY.IC_PYME"+
					" AND PP.IC_PRODUCTO_NAFIN = 4"+
					" AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
                    " AND M.ic_moneda = TC.ic_moneda" +
					" AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
					" AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"+
					" AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO"+
					" AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES"+
					" AND CT.IC_TASA = DS.IC_TASA"+
					" AND CE.IC_CAMBIO_ESTATUS = 14"+
				   " AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE)"  
					);
					
					if("C".equals(tiposCredito)){
						if(!"".equals(noEpo)){	
							qrySentenciaCCC.append(" AND D.IC_EPO=  ? ");
							conditions.add(new Integer(noEpo));
						}	
					}
					
				if("D".equals(tiposCredito)){
					query = qrySentenciaDM;
				}else if("C".equals(tiposCredito)){
					query = qrySentenciaCCC;
				}else{
					query.append(qrySentenciaDM + " UNION ALL "+qrySentenciaCCC);
				}
		
		
		}else 	if(noEstatus.equals("2")||noEstatus.equals("23")||noEstatus.equals("34")){ 	
			qrySentenciaDM.append(" SELECT PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"+
					" 	,D.IG_NUMERO_DOCTO"+
					" 	,D.CC_ACUSE"+
					"	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"+
					"	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"+
					"	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"+
					"	,D.IG_PLAZO_DOCTO"+
					"	,M.CD_NOMBRE AS MONEDA"+
					"	,D.FN_MONTO"+
         	"	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Peso-Dolar','') as TIPO_CONVERSION"   +
        	" ,DECODE (m.ic_moneda, 54, DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') , '') as TIPO_CAMBIO " +
					"	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"+
					"	,NVL(D.FN_PORC_DESCUENTO,0) AS FN_PORC_DESCUENTO"+
					"	,TF.CD_DESCRIPCION AS MODO_PLAZO"+
					"	,ED.CD_DESCRIPCION AS ESTATUS"+
					"	,M.ic_moneda"+
					"	,D.ic_documento"+
					"	,E.cg_razon_social as NOMBRE_EPO"+
					"	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
					"	,I.cg_razon_social as NOMBRE_IF"+
					"	,decode(PP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"+
					"	,DS.fn_importe_recibir as MONTO_CREDITO"+
					"	,D.ig_plazo_credito"+
					"	,TCI.cd_descripcion as TIPO_COBRO_INT"+
					"  	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) as REFERENCIA_INT"+
					"	,decode(DS.cg_rel_mat,'+',fn_valor_tasa+fn_puntos,'-',fn_valor_tasa-fn_puntos,'*',fn_valor_tasa*fn_puntos,'/',fn_valor_tasa/fn_puntos) as VALOR_TASA_INT"+
					"	,DS.fn_importe_interes as MONTO_TASA_INT"+
					"	,D.cc_acuse"+
					"	,to_char(D.df_carga,'dd/mm/yyyy') as FECHA_PUBLICACION"+
					"	,to_char(D.df_fecha_venc_credito,'dd/mm/yyyy') as FECHA_VENC_CREDITO"+
					"	,to_char(CE.dc_fecha_cambio,'dd/mm/yyyy') as FECHA_CAMBIO"+
					" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
					",  nvl(ds.fn_importe_recibir,0) + nvl( ds.fn_importe_interes, 0)  AS MONTO_CAP_INT "+
					" , ( NVL( d.fn_monto, 0) * NVL (d.fn_porc_descuento, 0) /100) as monto_descuento  "+
					"  ,( NVL( d.fn_monto, 0)  * DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1')) as monto_Valuado "+
					" ,'' as DF_OPERACIONIF "+	
					" , (SELECT nvl(fn_valor_aforo, 0)  FROM dis_aforo_epo   WHERE ic_if = i.ic_if      AND ic_epo = d.ic_epo        AND ic_moneda = d.ic_moneda)  as FN_PORC_AFORO "+
					" , NVL (d.fn_monto, 0) *  (  (SELECT nvl(fn_valor_aforo, 0)     FROM dis_aforo_epo   WHERE ic_if = i.ic_if      AND ic_epo = d.ic_epo        AND ic_moneda = d.ic_moneda) /100) as MONTO_DESC "+				
					" FROM DIS_DOCUMENTO D"+  
					" ,COMCAT_PYME PY"+
					" ,COMCAT_MONEDA M"+
					" ,COMREL_PRODUCTO_EPO PE"+
					" ,COMCAT_PRODUCTO_NAFIN PN"+
					" ,COM_TIPO_CAMBIO TC"+
					" ,COMCAT_TIPO_FINANCIAMIENTO TF"+
					" ,COMCAT_ESTATUS_DOCTO ED"+
					" ,DIS_LINEA_CREDITO_DM LC"+
					" ,COMCAT_EPO E"+
					" ,DIS_DOCTO_SELECCIONADO DS"+
					" ,COMCAT_IF I"+
					" ,COMREL_PYME_EPO_X_PRODUCTO PP"+
					" ,COMCAT_TIPO_COBRO_INTERES TCI"+
					" ,COMCAT_TASA CT"+
					" ,DIS_CAMBIO_ESTATUS CE"+
					" WHERE D.IC_PYME = PY.IC_PYME"+
					" AND DS.IC_DOCUMENTO = D.IC_DOCUMENTO"+
					" AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"+
					" AND D.IC_MONEDA = M.IC_MONEDA"+  
					" AND PN.IC_PRODUCTO_NAFIN = 4"+
					" AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
					" AND PE.IC_EPO = D.IC_EPO"+
					" AND I.IC_IF = LC.IC_IF"+    
					" AND E.IC_EPO = D.IC_EPO"+ 
					" AND PP.IC_EPO = E.IC_EPO"+
					" AND PP.IC_PYME = PY.IC_PYME"+
					" AND PP.IC_PRODUCTO_NAFIN = 4"+
					" AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
                    " AND M.ic_moneda = TC.ic_moneda" +
					" AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
					" AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"+
					" AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM"+
					" AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES"+
					" AND CT.IC_TASA = DS.IC_TASA"+
					" AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE)"
					);			
					if("D".equals(tiposCredito)){
						if(!"".equals(noEpo)){	
							qrySentenciaDM.append(" AND D.IC_EPO=  ? ");
							conditions.add(new Integer(noEpo));
						}	
					}
					if (noEstatus.equals("2") )	{
						qrySentenciaDM.append(" AND CE.IC_CAMBIO_ESTATUS ="+noEstatus);
					}  
					if (noEstatus.equals("23") )	{
						qrySentenciaDM.append(" AND CE.IC_CAMBIO_ESTATUS ="+noEstatus);
					}
					if (noEstatus.equals("34") )	{         
						qrySentenciaDM.append(" AND CE.IC_CAMBIO_ESTATUS ="+noEstatus); 
					}
			qrySentenciaCCC.append(" SELECT PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"+
					" 	,D.IG_NUMERO_DOCTO"+
					" 	,D.CC_ACUSE"+
					"	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"+
					"	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"+
					"	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"+
					"	,D.IG_PLAZO_DOCTO"+
					"	,M.CD_NOMBRE AS MONEDA"+
					"	,D.FN_MONTO"+
         	"	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Peso-Dolar','') as TIPO_CONVERSION"   +
        	" ,DECODE (m.ic_moneda, 54, DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') , '') as TIPO_CAMBIO " +
				
					"	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"+
					"	,NVL(D.FN_PORC_DESCUENTO,0) AS FN_PORC_DESCUENTO"+
					"	,TF.CD_DESCRIPCION AS MODO_PLAZO"+
					"	,ED.CD_DESCRIPCION AS ESTATUS"+
					"	,M.ic_moneda"+
					"	,D.ic_documento"+
					"	,E.cg_razon_social as NOMBRE_EPO"+
					"	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
					"	,I.cg_razon_social as NOMBRE_IF"+
					"	,decode(PP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"+
					"	,DS.fn_importe_recibir as MONTO_CREDITO"+
					"	,D.ig_plazo_credito"+
					"	,TCI.cd_descripcion as TIPO_COBRO_INT"+
					"	,CT.cd_nombre||' '||DS.cg_rel_mat||' '||DS.fn_puntos as REFERENCIA_INT"+
					"	,decode(DS.cg_rel_mat,'+',fn_valor_tasa+fn_puntos,'-',fn_valor_tasa-fn_puntos,'*',fn_valor_tasa*fn_puntos,'/',fn_valor_tasa/fn_puntos) as VALOR_TASA_INT"+
					"	,DS.fn_importe_interes as MONTO_TASA_INT"+
					"	,D.cc_acuse"+
					"	,to_char(D.df_carga,'dd/mm/yyyy') as FECHA_PUBLICACION"+
					"	,to_char(D.df_fecha_venc_credito,'dd/mm/yyyy') as FECHA_VENC_CREDITO"+
					"	,to_char(CE.dc_fecha_cambio,'dd/mm/yyyy') as FECHA_CAMBIO"+
					" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
					",  nvl(ds.fn_importe_recibir,0) + nvl( ds.fn_importe_interes, 0)  AS MONTO_CAP_INT "+
					" , ( NVL( d.fn_monto, 0) * NVL (d.fn_porc_descuento, 0) /100) as monto_descuento  "+
					"  ,( NVL( d.fn_monto, 0)  * DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1')) as monto_Valuado "+
					" ,'' as DF_OPERACIONIF "+	
					" , (SELECT nvl(fn_valor_aforo, 0)  FROM dis_aforo_epo   WHERE ic_if = i.ic_if      AND ic_epo = d.ic_epo        AND ic_moneda = d.ic_moneda)  as FN_PORC_AFORO "+
					" , NVL (d.fn_monto, 0) *  (  (SELECT nvl(fn_valor_aforo, 0)     FROM dis_aforo_epo   WHERE ic_if = i.ic_if      AND ic_epo = d.ic_epo        AND ic_moneda = d.ic_moneda) /100) as MONTO_DESC "+				
				
					" FROM DIS_DOCUMENTO D"+
					" ,COMCAT_PYME PY"+
					" ,COMCAT_MONEDA M"+
					" ,COMREL_PRODUCTO_EPO PE"+
					" ,COMCAT_PRODUCTO_NAFIN PN"+
					" ,COM_TIPO_CAMBIO TC"+
					" ,COMCAT_TIPO_FINANCIAMIENTO TF"+
					" ,COMCAT_ESTATUS_DOCTO ED"+
					" ,COM_LINEA_CREDITO LC"+
					" ,COMCAT_EPO E"+
					" ,DIS_DOCTO_SELECCIONADO DS"+
					" ,COMCAT_IF I"+
					" ,COMREL_PYME_EPO_X_PRODUCTO PP"+
					" ,COMCAT_TIPO_COBRO_INTERES TCI"+
					" ,COMCAT_TASA CT"+
					" ,DIS_CAMBIO_ESTATUS CE"+
					" WHERE D.IC_PYME = PY.IC_PYME"+
					" AND DS.IC_DOCUMENTO = D.IC_DOCUMENTO"+
					" AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"+
					" AND D.IC_MONEDA = M.IC_MONEDA"+
					" AND PN.IC_PRODUCTO_NAFIN = 4"+
					" AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
					" AND PE.IC_EPO = D.IC_EPO"+
					" AND I.IC_IF = LC.IC_IF"+
					" AND E.IC_EPO = D.IC_EPO"+
					" AND PP.IC_EPO = E.IC_EPO"+
					" AND PP.IC_PYME = PY.IC_PYME"+
					" AND PP.IC_PRODUCTO_NAFIN = 4"+
					" AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
                    " AND M.ic_moneda = TC.ic_moneda" +
					" AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
					" AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"+
					" AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO"+
					" AND TCI.IC_TIPO_COBRO_INTERES = LC.IC_TIPO_COBRO_INTERES"+
					" AND CT.IC_TASA = DS.IC_TASA"+
					" AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE)" 
					);
					if("C".equals(tiposCredito)){
						if(!"".equals(noEpo)){	
							qrySentenciaCCC.append(" AND D.IC_EPO=  ? ");
							conditions.add(new Integer(noEpo));
						}	
					}
					if (noEstatus.equals("2") )	{
						qrySentenciaCCC.append(" AND CE.IC_CAMBIO_ESTATUS ="+noEstatus);
					}
					if (noEstatus.equals("23") )	{
						qrySentenciaCCC.append(" AND CE.IC_CAMBIO_ESTATUS ="+noEstatus);
					}
					if (noEstatus.equals("34") )	{
						qrySentenciaCCC.append(" AND CE.IC_CAMBIO_ESTATUS ="+noEstatus);
					}
					
				if("D".equals(tiposCredito)){
					query = qrySentenciaDM;
				}else if("C".equals(tiposCredito)){
					query = qrySentenciaCCC;
				}else{
					query.append(qrySentenciaDM + " UNION ALL "+qrySentenciaCCC);
				}
				
		}else 	if(noEstatus.equals("21")){ //MODIFICACION		
		
		
		query.append(" SELECT PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"+
					" 	,D.IG_NUMERO_DOCTO"+
					" 	,D.CC_ACUSE"+
					"	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"+
					"	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"+
					"	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"+
					"	,D.IG_PLAZO_DOCTO"+
					"	,M.CD_NOMBRE AS MONEDA"+
					"	,D.FN_MONTO"+
          "	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
        	" ,DECODE (m.ic_moneda, 54, DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') , '') as TIPO_CAMBIO " +
					"	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"+
					"	,NVL(D.FN_PORC_DESCUENTO,0) AS FN_PORC_DESCUENTO"+
					"	,TF.CD_DESCRIPCION AS MODO_PLAZO"+
					"	,M.ic_moneda"+
					"	,D.ic_documento"+
					"	,E.cg_razon_social as NOMBRE_EPO"+
					"	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
					"	,decode(PEP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"+
					"	,D.cc_acuse "+
					"	,to_char(D.df_carga,'dd/mm/yyyy') as FECHA_PUBLICACION"+
					"	,to_char(CE.DC_FECHA_CAMBIO,'dd/mm/yyyy') as FECHA_CAMBIO"+
					"	,to_char(CE.DF_FECHA_EMISION_ANTERIOR,'dd/mm/yyyy') as FECHA_EMISION_ANT"+
					"	,to_char(CE.DF_FECHA_VENC_ANTERIOR,'dd/mm/yyyy') as FECHA_VENC_ANT"+
					"	,CE.fn_monto_anterior"+
					"	,TF2.cd_descripcion AS MODO_PLAZO_ANT"+
					" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+					
					" , ( NVL( d.fn_monto, 0) * NVL (d.fn_porc_descuento, 0) /100) as monto_descuento  "+
					"  ,( NVL( d.fn_monto, 0)  * DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1')) as monto_Valuado "+
							
					" FROM DIS_DOCUMENTO D"+
					" ,COMCAT_PYME PY"+
					" ,COMCAT_MONEDA M"+
					" ,COMREL_PRODUCTO_EPO PE"+
					" ,COMCAT_PRODUCTO_NAFIN PN"+
					" ,COM_TIPO_CAMBIO TC"+
					" ,COMCAT_TIPO_FINANCIAMIENTO TF"+
					" ,COMCAT_TIPO_FINANCIAMIENTO TF2"+
					" ,COMCAT_EPO E"+
					" ,DIS_CAMBIO_ESTATUS CE"+
					" ,COMREL_PYME_EPO_X_PRODUCTO PEP"+
					" WHERE D.IC_PYME = PY.IC_PYME"+
					" AND D.IC_MONEDA = M.IC_MONEDA"+
					" AND PN.IC_PRODUCTO_NAFIN = 4"+
					" AND PEP.IC_PYME = PY.IC_PYME"+
					" AND PEP.IC_EPO = E.IC_EPO"+
					" AND PEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
					" AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"+
					" AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
					" AND PE.IC_EPO = D.IC_EPO"+
					" AND E.IC_EPO = D.IC_EPO"+
					" AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
                    " AND M.ic_moneda = TC.ic_moneda" +
					" AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
					" AND CE.IC_CAMBIO_ESTATUS = 21"+
					" AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE)"+ 
					" AND TF2.IC_TIPO_FINANCIAMIENTO(+) = CE.IC_TIPO_FINAN_ANT ");
					
						if(!"".equals(noEpo)){	
						query.append(" AND D.IC_EPO=  ? ");
						conditions.add(new Integer(noEpo));
					}	
		
		}else 	if(noEstatus.equals("24")){ //No negociable a baja
		
		
		query.append(" SELECT PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"+
					" 	,D.IG_NUMERO_DOCTO"+
					" 	,D.CC_ACUSE"+
					"	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"+
					"	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"+
					"	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"+
					"	,D.IG_PLAZO_DOCTO"+
					"	,M.CD_NOMBRE AS MONEDA"+
					"	,D.FN_MONTO"+
          "	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') as TIPO_CONVERSION"   +
        	" ,DECODE (m.ic_moneda, 54, DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') , '') as TIPO_CAMBIO " +
					"	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"+
					"	,NVL(D.FN_PORC_DESCUENTO,0) AS FN_PORC_DESCUENTO"+
					"	,TF.CD_DESCRIPCION AS MODO_PLAZO"+
					"	,M.ic_moneda"+
					"	,D.ic_documento"+
					"	,E.cg_razon_social as NOMBRE_EPO"+
					"	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
					"	,decode(PEP.cg_tipo_credito,'D','Descuento y/o Factoraje','C','Credito en Cuenta Corriente') as TIPO_CREDITO"+
					"	,D.cc_acuse "+
					"	,to_char(D.df_carga,'dd/mm/yyyy') as FECHA_PUBLICACION"+
					"	,to_char(CE.DC_FECHA_CAMBIO,'dd/mm/yyyy') as FECHA_CAMBIO"+
					"	,CE.ct_cambio_motivo"+
					" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
					" , ( NVL( d.fn_monto, 0) * NVL (d.fn_porc_descuento, 0) /100) as monto_descuento  "+
					"  ,( NVL( d.fn_monto, 0)  * DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1')) as monto_Valuado "+
					
					" FROM DIS_DOCUMENTO D"+
					" ,COMCAT_PYME PY"+
					" ,COMCAT_MONEDA M"+
					" ,COMREL_PRODUCTO_EPO PE"+
					" ,COMCAT_PRODUCTO_NAFIN PN"+
					" ,COM_TIPO_CAMBIO TC"+
					" ,COMCAT_TIPO_FINANCIAMIENTO TF"+
					" ,COMCAT_EPO E"+
					" ,DIS_CAMBIO_ESTATUS CE"+
					" ,COMREL_PYME_EPO_X_PRODUCTO PEP"+
					" WHERE D.IC_PYME = PY.IC_PYME"+
					" AND D.IC_MONEDA = M.IC_MONEDA"+
					" AND PN.IC_PRODUCTO_NAFIN = 4"+
					" AND PEP.IC_PYME = PY.IC_PYME"+
					" AND PEP.IC_EPO = E.IC_EPO"+
					" AND PEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
					" AND CE.IC_DOCUMENTO = D.IC_DOCUMENTO"+
					" AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"+
					" AND PE.IC_EPO = D.IC_EPO"+
					" AND E.IC_EPO = D.IC_EPO"+
					" AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
                    " AND M.ic_moneda = TC.ic_moneda" +
					" AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
					" AND CE.IC_CAMBIO_ESTATUS = 24"+
					" AND TRUNC(CE.DC_FECHA_CAMBIO) = TRUNC(SYSDATE) "
					);
						
				if(!"".equals(noEpo)){	
					query.append(" AND D.IC_EPO=  ? ");
					conditions.add(new Integer(noEpo));
				}			
		
		}
		
		
		
		log.debug("getDocumentQueryFile "+query.toString());
		log.debug("getDocumentQueryFile)**"+conditions);
	
		return query.toString();
 	} 		
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String nombreArchivo = "";	
		ComunesPDF pdfDoc = new ComunesPDF();
		String linea = "", lineaT = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		int totalDoctosMN =0,  totalDoctosUSD =0;
		double totalMontoMN =0,  totalMontoValuadoMN = 0,  totalMontoUSD =0, totalMontoValuadoUSD =0; 
		double totalMontoCreditoMN =0, totalMontoDescuentoMN  =0, totalMontoInteresMN =0;
		double	totalMontoCreditoUSD =0, totalMontoDescuentoUSD =0,  totalMontoInteresUSD =0;
		String icMoneda =""; 
		HashMap totales21t = null;
		JSONArray totales21r = new JSONArray();	
		String infoRegresar =""; 
		try{
			if ("PDF".equals(tipo)) {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[]	=	{"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	=	new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual	=	fechaActual.substring(0,2);
				String mesActual	=	meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual	=	fechaActual.substring(6,10);
				String horaActual	=	new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),  
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),
				(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + "------------------------------" +	horaActual, "formas", ComunesPDF.RIGHT);
			}
			if ("CSV".equals(tipo)) {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
				buffer = new BufferedWriter(writer);
			}
				
			if( noEstatus.equals("4")  &&  tipo.equals("PDF") ) {
				totalDoctosMN =0;  totalDoctosUSD =0;
				totalMontoMN =0;  totalMontoValuadoMN = 0;  totalMontoUSD =0; totalMontoValuadoUSD =0; 
			  totalMontoCreditoMN =0; totalMontoDescuentoMN  =0; totalMontoInteresMN =0;
			  totalMontoCreditoUSD =0; totalMontoDescuentoUSD =0;  totalMontoInteresUSD =0;
		
				pdfDoc.addText("   ","formas", ComunesPDF.CENTER);
				pdfDoc.addText(" Negociable a Baja   ","formas", ComunesPDF.CENTER);
				pdfDoc.addText("    ","formas", ComunesPDF.CENTER);
	
				pdfDoc.setTable(18, 100);
				pdfDoc.setCell(" Distribuidor","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" N�mero de Acuse de Carga ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" N�mero de documento inicial ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de emisi�n ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de publicaci�n ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Plazo docto. ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Moneda ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Plazo para descuento en d�as ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" % de descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto % de descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Tipo conv. ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Tipo cambio","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto valuado en pesos ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Modalidad de plazo ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de Cambio Causa ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Causa ","celda01",ComunesPDF.CENTER);
			}else if( noEstatus.equals("4")  &&  tipo.equals("CSV") ) {		
				lineaT = " Negociable a Baja \n";
				lineaT += " Distribuidor, N�mero de Acuse de Carga, N�mero de documento inicial, Fecha de emisi�n , Fecha de vencimiento , Fecha de publicaci�n ,Plazo docto. , Moneda , Monto , Plazo para descuento en d�as , % de descuento , Monto % de descuento , Tipo conv. , Tipo cambio, Monto valuado en pesos , Modalidad de plazo , Fecha de Cambio Causa, Causa \n";
				buffer.write(lineaT); //linea Descripcion Titulos 	
			}
			
			if( noEstatus.equals("22")  &&  tipo.equals("PDF") ) {
				totalDoctosMN =0;  totalDoctosUSD =0;
				totalMontoMN =0;  totalMontoValuadoMN = 0;  totalMontoUSD =0; totalMontoValuadoUSD =0; 
			  totalMontoCreditoMN =0; totalMontoDescuentoMN  =0; totalMontoInteresMN =0;
			  totalMontoCreditoUSD =0; totalMontoDescuentoUSD =0;  totalMontoInteresUSD =0;
				
				pdfDoc.addText("   ","formas", ComunesPDF.CENTER);
				pdfDoc.addText(" Negociable a Vencido sin Operar   ","formas", ComunesPDF.CENTER);
				pdfDoc.addText("    ","formas", ComunesPDF.CENTER);
				
				pdfDoc.setTable(18, 100);
				pdfDoc.setCell(" Distribuidor","celda01",ComunesPDF.CENTER);		
				pdfDoc.setCell(" N�mero de Acuse de Carga ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" N�mero de documento inicial ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de emisi�n  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de publicaci�n ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Plazo docto.","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Moneda ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Plazo para descuento en d�as ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" % de descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto % de descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Tipo conv.","celda01",ComunesPDF.CENTER);	
				pdfDoc.setCell(" Tipo cambio ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto valuado en pesos ","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell(" Modalidad de plazo ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de Cambio","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell(" Causa","celda01",ComunesPDF.CENTER);
				
				
			}else if( noEstatus.equals("22")  &&  tipo.equals("CSV") ) {	
				lineaT = " Negociable a Vencido sin Operar \n";
				lineaT += " Distribuidor, N�mero de Acuse de Carga , N�mero de documento inicial ,Fecha de emisi�n , Fecha de vencimiento , Fecha de publicaci�n , Plazo docto., Moneda , Monto , Plazo para descuento en d�as , % de descuento , Monto % de descuento , Tipo conv., Tipo cambio , Monto valuado en pesos , Modalidad de plazo , Fecha de Cambio, Causa  \n";
				buffer.write(lineaT); //linea Descripcion Titulos 									
			}
			
			if( noEstatus.equals("14")  &&  tipo.equals("PDF") ) {
			
				totalDoctosMN =0;  totalDoctosUSD =0;
				totalMontoMN =0;  totalMontoValuadoMN = 0;  totalMontoUSD =0; totalMontoValuadoUSD =0; 
			  totalMontoCreditoMN =0; totalMontoDescuentoMN  =0; totalMontoInteresMN =0;
			  totalMontoCreditoUSD =0; totalMontoDescuentoUSD =0;  totalMontoInteresUSD =0;
				
				pdfDoc.addText("   ","formas", ComunesPDF.CENTER);
				pdfDoc.addText(" Seleccionado pyme a rechazado IF   ","formas", ComunesPDF.CENTER);
				pdfDoc.addText("    ","formas", ComunesPDF.CENTER);
							
				pdfDoc.setTable(15, 100);
				pdfDoc.setCell(" A ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Distribuidor  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" N�mero de Acuse de Carga ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" N�mero de documento inicial ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de emisi�n ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de publicaci�n ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Plazo docto. ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Moneda ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Plazo para descuento en d�as ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" % de descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto % de descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Tipo conv.","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell(" Tipo cambio.","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell(" B ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto valuado en pesos.","celda01",ComunesPDF.CENTER); 
				pdfDoc.setCell(" Modalidad de plazo ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Tipo de cr�dito ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" N�mero de documento final ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Plazo ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de Vencimiento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de operaci�n IF ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" IF ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Referencia tasa de inter�s ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Valor tasa de inter�s ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto de Intereses ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto Total(C�pital e Intereses ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de Rechazo ","celda01",ComunesPDF.CENTER);				
			
			}else if( noEstatus.equals("14")  &&  tipo.equals("CSV") ) {			
				lineaT = "  Seleccionado pyme a rechazado IF  \n";
				lineaT += " Distribuidor , N�mero de Acuse de Carga , N�mero de documento inicial ,Fecha de emisi�n , Fecha de vencimiento , Fecha de publicaci�n , Plazo docto. ,Moneda , Monto ,Plazo para descuento en d�as , % de descuento ,Monto % de descuento , Tipo conv., Tipo cambio., "+
				"Monto valuado en pesos. , Modalidad de plazo , Tipo de cr�dito , N�mero de documento final ,Monto , Plazo ,Fecha de Vencimiento , Fecha de operaci�n IF , IF , Referencia tasa de inter�s , Valor tasa de inter�s , Monto de Intereses , Monto Total(C�pital e Intereses) , Fecha de Rechazo  \n";				
				buffer.write(lineaT); //linea Descripcion Titulos 									
			}
			
			if( noEstatus.equals("2")  &&  tipo.equals("PDF") ) {
				totalDoctosMN =0;  totalDoctosUSD =0;
				totalMontoMN =0;  totalMontoValuadoMN = 0;  totalMontoUSD =0; totalMontoValuadoUSD =0; 
			  totalMontoCreditoMN =0; totalMontoDescuentoMN  =0; totalMontoInteresMN =0;
			  totalMontoCreditoUSD =0; totalMontoDescuentoUSD =0;  totalMontoInteresUSD =0;
				
				pdfDoc.addText("   ","formas", ComunesPDF.CENTER);
				pdfDoc.addText(" Seleccionado Pyme a Negociable  ","formas", ComunesPDF.CENTER);
				pdfDoc.addText("    ","formas", ComunesPDF.CENTER);				
				
				pdfDoc.setTable(16, 100);
				pdfDoc.setCell(" A ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Distribuidor  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de Acuse de Carga  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de documento inicial  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de emisi�n  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de vencimiento  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de publicaci�n  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo docto. ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("% de descuento Aforo  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a descontar  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo para descuento en d�as  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("% de descuento  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto % de descuento  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo conv. ","celda01",ComunesPDF.CENTER);			
				pdfDoc.setCell(" B ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo cambio. ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto valuado en pesos. ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Modalidad de plazo  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de cr�dito  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de documento final  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Vencimiento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de operaci�n IF ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("IF  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Referencia tasa de inter�s ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Valor tasa de inter�s ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto de Intereses ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Total(C�pital e Intereses) ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Rechazo ","celda01",ComunesPDF.CENTER);
				
			}else if( noEstatus.equals("2")  &&  tipo.equals("CSV") ) {	
				lineaT = "  Seleccionado Pyme a Negociable  \n";
				lineaT += " Distribuidor ,N�mero de Acuse de Carga ,N�mero de documento inicial,Fecha de emisi�n ,Fecha de vencimiento ,Fecha de publicaci�n ,Plazo docto. ,Moneda , Monto, % de descuento Aforo, Monto a descontar, Plazo para descuento en d�as ,% de descuento,Monto % de descuento,Tipo conv.,Tipo cambio.,"+
								" Monto valuado en pesos.,Modalidad de plazo ,Tipo de cr�dito ,N�mero de documento final ,Monto ,Plazo ,Fecha de Vencimiento ,Fecha de operaci�n IF ,IF ,Referencia tasa de inter�s,Valor tasa de inter�s ,Monto de Intereses ,Monto Total(C�pital e Intereses) ,Fecha de Rechazo \n";	
				buffer.write(lineaT); //linea Descripcion Titulos 					
			}
			if( (noEstatus.equals("23")  &&  tipo.equals("PDF"))|| (noEstatus.equals("34")  &&  tipo.equals("PDF")) ) {
				totalDoctosMN =0;  totalDoctosUSD =0;
				totalMontoMN =0;  totalMontoValuadoMN = 0;  totalMontoUSD =0; totalMontoValuadoUSD =0; 
			  totalMontoCreditoMN =0; totalMontoDescuentoMN  =0; totalMontoInteresMN =0;
			  totalMontoCreditoUSD =0; totalMontoDescuentoUSD =0;  totalMontoInteresUSD =0;
				
				pdfDoc.addText("   ","formas", ComunesPDF.CENTER);
				if(noEstatus.equals("23")){
					pdfDoc.addText("En Proceso de Autorizacion IF a Negociable  ","formas", ComunesPDF.CENTER);
				}else{
					pdfDoc.addText(" En Proceso de Autorizacion IF a Seleccionado Pyme  ","formas", ComunesPDF.CENTER);
				}
				
				pdfDoc.addText("    ","formas", ComunesPDF.CENTER);				
				
				pdfDoc.setTable(12, 100);
				pdfDoc.setCell(" A ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("EPO  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de Acuse de carga  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de documento inicial  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de emisi�n  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de vencimiento  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de publicaci�n  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo de documento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo para descuento en d�as  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Porcentaje de descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" B","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto porcenteje de descuento  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Modalidad de plazo  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de documento final  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("IF  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Referencia de tasa de inter�s ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Valor Tasa de Inter�s ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto de Inter�s ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Total de capital","celda01",ComunesPDF.CENTER);
				
			}else if( (noEstatus.equals("23")  &&  tipo.equals("CSV"))|| (noEstatus.equals("34")  &&  tipo.equals("CSV"))) {
				if(noEstatus.equals("23")){
					lineaT = " En Proceso de Autorizacion IF a Negociable   \n";
				}else{
					lineaT = " En Proceso de Autorizacion IF a Seleccionado Pyme   \n";
				}
				
				lineaT += " EPO ,N�mero de Acuse de carga ,N�mero de documento inicial,Fecha de emisi�n ,Fecha de vencimiento ,Fecha de publicaci�n ,Plazo de documento ,Moneda , Monto,Plazo para descuento en d�as,Porcentaje de descuento,Monto porcenteje de descuento,"+
								"Modalidad de plazo ,N�mero de documento final ,Monto ,Plazo ,Fecha de vencimiento ,IF ,Referencia tasa de inter�s,Valor Tasa de Inter�s ,Monto de Inter�s ,Monto Total de capital \n";	
				buffer.write(lineaT); //linea Descripcion Titulos 					
			}
			
			if( noEstatus.equals("21")  &&  tipo.equals("PDF") ) {
			
				totalDoctosMN =0;  totalDoctosUSD =0;
				totalMontoMN =0;  totalMontoValuadoMN = 0;  totalMontoUSD =0; totalMontoValuadoUSD =0; 
			  totalMontoCreditoMN =0; totalMontoDescuentoMN  =0; totalMontoInteresMN =0;
			  totalMontoCreditoUSD =0; totalMontoDescuentoUSD =0;  totalMontoInteresUSD =0;
				
				pdfDoc.addText("   ","formas", ComunesPDF.CENTER);
				pdfDoc.addText(" MODIFICACION  ","formas", ComunesPDF.CENTER);
				pdfDoc.addText("    ","formas", ComunesPDF.CENTER);		
				
				pdfDoc.setTable(21, 100);			
				pdfDoc.setCell("Distribuidor  ","celda01",ComunesPDF.CENTER);		
				pdfDoc.setCell("N�mero de Acuse de Carga ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de documento inicial ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de emisi�n ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de publicaci�n ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo docto.","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo para descuento en d�as","celda01",ComunesPDF.CENTER);				
				pdfDoc.setCell("% de descuento","celda01",ComunesPDF.CENTER);			
				pdfDoc.setCell("Monto % de descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo conv.","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo cambio.","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto valuado en pesos.","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Modalidad de plazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Cambio","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Anterior Fecha de Emisi�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Anterior Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Anterior Monto del Documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Anterior Modalidad de Plazo","celda01",ComunesPDF.CENTER);				
			
			}else if( noEstatus.equals("21")  &&  tipo.equals("CSV") ) {	
				lineaT = "  MODIFICACION  \n";
				lineaT += "	Distribuidor , N�mero de Acuse de Carga , N�mero de documento inicial ,Fecha de emisi�n ,Fecha de vencimiento,Fecha de publicaci�n ,Plazo docto.,Monto,Moneda,Plazo para descuento en d�as,% de descuento, "+
								 " Monto % de descuento,Tipo conv.,Tipo cambio.,Monto valuado en pesos.,Modalidad de plazo,Fecha de Cambio,Anterior Fecha de Emisi�n,Anterior Fecha de Vencimiento,Anterior Monto del Documento,Anterior Modalidad de Plazo  \n";
				buffer.write(lineaT); //linea Descripcion Titulos 					
			}
			
			if( noEstatus.equals("24")  &&  tipo.equals("PDF") ) {	
			
				totalDoctosMN =0;  totalDoctosUSD =0;
				totalMontoMN =0;  totalMontoValuadoMN = 0;  totalMontoUSD =0; totalMontoValuadoUSD =0; 
			  totalMontoCreditoMN =0; totalMontoDescuentoMN  =0; totalMontoInteresMN =0;
			  totalMontoCreditoUSD =0; totalMontoDescuentoUSD =0;  totalMontoInteresUSD =0;
				
				pdfDoc.addText("   ","formas", ComunesPDF.CENTER);
				pdfDoc.addText(" NO Negociable a Baja  ","formas", ComunesPDF.CENTER);
				pdfDoc.addText("    ","formas", ComunesPDF.CENTER);		
			
				pdfDoc.setTable(18, 100);
				pdfDoc.setCell(" Distribuidor ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" N�mero de Acuse de Carga ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" N�mero de documento inicial ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de emisi�n ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de publicaci�n ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Plazo docto. ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Plazo para descuento en d�as ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" % de descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto % de descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Tipo conv. ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Tipo cambio. ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Monto valuado en pesos. ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Modalidad de plazo ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Fecha de Cambio ","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell(" Causa ","celda01",ComunesPDF.CENTER);
			
			}else if( noEstatus.equals("24")  &&  tipo.equals("CSV") ) {		
				lineaT = "  NO Negociable a Baja  \n";
				lineaT += "Distribuidor , N�mero de Acuse de Carga , N�mero de documento inicial , Fecha de emisi�n , Fecha de vencimiento , Fecha de publicaci�n , Plazo docto. ,Moneda, Monto,Plazo para descuento en d�as , % de descuento ,Monto % de descuento , Tipo conv. , Tipo cambio. , Monto valuado en pesos., Modalidad de plazo ,Fecha de Cambio , Causa \n";
				buffer.write(lineaT); //linea Descripcion Titulos 					
			}		
			
			//INICIA DATOS 
			while (rs.next()) {
				
				if(noEstatus.equals("4")  ) {					
					String nombre_dist  = (rs.getString("NOMBRE_DIST")== null)? "" : rs.getString("NOMBRE_DIST");
					String acuse  = (rs.getString("CC_ACUSE") == null) ? "" : rs.getString("CC_ACUSE");
					String ig_numero_docto  = (rs.getString("IG_NUMERO_DOCTO") == null) ? "" : rs.getString("IG_NUMERO_DOCTO");
					String fecha_emision  = (rs.getString("DF_FECHA_EMISION") == null) ? "" : rs.getString("DF_FECHA_EMISION");
					String fecha_venc  = (rs.getString("DF_FECHA_VENC") == null) ? " " : rs.getString("DF_FECHA_VENC");
					String fecha_public  = (rs.getString("FECHA_PUBLICACION") == null) ? " " : rs.getString("FECHA_PUBLICACION");
					String plazo_docto  = (rs.getString("IG_PLAZO_DOCTO") == null) ? "0" : rs.getString("IG_PLAZO_DOCTO");
					String moneda  = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
					String plazo_desc  = (rs.getString("IG_PLAZO_DESCUENTO") == null) ? "0" : rs.getString("IG_PLAZO_DESCUENTO");
					String por_descuento  = (rs.getString("FN_PORC_DESCUENTO") == null) ? "0" : rs.getString("FN_PORC_DESCUENTO");
					String monto_descuento  = (rs.getString("MONTO_DESCUENTO") == null) ? "0" : rs.getString("MONTO_DESCUENTO");
					String tipo_convers  = (rs.getString("TIPO_CONVERSION") == null) ? "" : rs.getString("TIPO_CONVERSION");
					String tipo_cambio  = (rs.getString("TIPO_CAMBIO") == null) ? "" : rs.getString("TIPO_CAMBIO");
					String modalidad_plazo  = (rs.getString("MODO_PLAZO") == null) ? "" : rs.getString("MODO_PLAZO");
					String fecha_cambio  = (rs.getString("FECHA_CAMBIO") == null) ? "" : rs.getString("FECHA_CAMBIO");
					String cambio  = (rs.getString("CT_CAMBIO_MOTIVO") == null) ? "" : rs.getString("CT_CAMBIO_MOTIVO");
					icMoneda =  (rs.getString("IC_MONEDA")== null)? "" : rs.getString("IC_MONEDA");
					double monto =  rs.getDouble("FN_MONTO");
					double montoValuado =   rs.getDouble("MONTO_VALUADO"); 
							
					if("1".equals(icMoneda)){
						totalDoctosMN ++;
						totalMontoMN += monto;
						totalMontoValuadoMN += montoValuado;
					}else{
						totalDoctosUSD ++;
						totalMontoUSD += monto;
						totalMontoValuadoUSD += montoValuado;
					}
					
					
					if( tipo.equals("PDF") ) {
						pdfDoc.setCell(nombre_dist,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(acuse,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(ig_numero_docto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fecha_emision,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fecha_venc,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fecha_public,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(plazo_docto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+ Comunes.formatoDecimal(monto,2)  ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(plazo_desc,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(por_descuento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+ Comunes.formatoDecimal(monto_descuento,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(tipo_convers,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(tipo_cambio,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+ Comunes.formatoDecimal(montoValuado,2)  ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(modalidad_plazo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fecha_cambio,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(cambio,"formas",ComunesPDF.CENTER);					
					}else if(  tipo.equals("CSV") ) {						
						linea = nombre_dist.replace(',',' ')+", "+
						acuse.replace(',',' ')+", "+
						ig_numero_docto.replace(',',' ')+", "+
						fecha_emision.replace(',',' ')+", "+
						fecha_venc.replace(',',' ')+", "+
						fecha_public.replace(',',' ')+", "+
						plazo_docto.replace(',',' ')+", "+
						moneda.replace(',',' ')+", "+
						Double.toString (monto).replace(',',' ')+", "+
						plazo_desc.replace(',',' ')+", "+
						por_descuento.replace(',',' ')+", "+
						monto_descuento.replace(',',' ')+", "+
						tipo_convers.replace(',',' ')+", "+
						tipo_cambio.replace(',',' ')+", "+
						Double.toString (montoValuado).replace(',',' ')+", "+
						modalidad_plazo.replace(',',' ')+", "+
						fecha_cambio.replace(',',' ')+", "+
						cambio.replace(',',' ')+"\n ";
						buffer.write(linea); 
					}
					
				}else if(noEstatus.equals("22")  ) {	
				
					String modo_plazo  ="", fecha_cambio = "", cambio_motivo = "";
						
					String nombre_dist  = (rs.getString("NOMBRE_DIST")== null)? "" : rs.getString("NOMBRE_DIST");
					String acuse  = (rs.getString("CC_ACUSE")== null)? "" : rs.getString("CC_ACUSE");	
					String no_docto  = (rs.getString("IG_NUMERO_DOCTO")== null)? "" : rs.getString("IG_NUMERO_DOCTO");	
					String fechaEmision  = (rs.getString("DF_FECHA_EMISION")== null)? "" : rs.getString("DF_FECHA_EMISION");	
					String fechaVecimiento = (rs.getString("DF_FECHA_VENC")== null)? "" : rs.getString("DF_FECHA_VENC");	
					String fechaPublic = (rs.getString("FECHA_PUBLICACION")== null)? "" : rs.getString("FECHA_PUBLICACION");					
					String plazo_docto = (rs.getString("IG_PLAZO_DOCTO")== null)? "0" : rs.getString("IG_PLAZO_DOCTO");	
					String moneda = (rs.getString("MONEDA")== null)? "" : rs.getString("MONEDA");	
					double monto =  rs.getDouble("FN_MONTO");
					String plazo_des = (rs.getString("IG_PLAZO_DESCUENTO")== null)? "0" : rs.getString("IG_PLAZO_DESCUENTO");
					String por_des = (rs.getString("FN_PORC_DESCUENTO")== null)? "0" : rs.getString("FN_PORC_DESCUENTO");
					String monto_des = (rs.getString("MONTO_DESCUENTO")== null)? "0" : rs.getString("MONTO_DESCUENTO");
					String tipo_conversion = (rs.getString("TIPO_CONVERSION")== null)? "" : rs.getString("TIPO_CONVERSION");
					String tipo_cambio = (rs.getString("TIPO_CAMBIO")== null)? "" : rs.getString("TIPO_CAMBIO");
					double montoValuado =   rs.getDouble("MONTO_VALUADO"); 
					modo_plazo =  (rs.getString("MODO_PLAZO")== null)? "0" : rs.getString("MODO_PLAZO");
					fecha_cambio =  (rs.getString("FECHA_CAMBIO")== null)? "" : rs.getString("FECHA_CAMBIO");
					cambio_motivo =  (rs.getString("CT_CAMBIO_MOTIVO")== null)? "" : rs.getString("CT_CAMBIO_MOTIVO");
					icMoneda =  (rs.getString("IC_MONEDA")== null)? "" : rs.getString("IC_MONEDA");
									
					if("1".equals(icMoneda)){
						totalDoctosMN ++;
						totalMontoMN += monto;
						totalMontoValuadoMN += montoValuado;
					}else{
						totalDoctosUSD ++;
						totalMontoUSD += monto;
						totalMontoValuadoUSD += montoValuado;
					}
					
					if( tipo.equals("PDF") ) {
									
						pdfDoc.setCell(nombre_dist,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(acuse,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(no_docto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaEmision,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaVecimiento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaPublic,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(plazo_docto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+ Comunes.formatoDecimal(monto,2)  ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(plazo_des,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(por_des,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+ Comunes.formatoDecimal(monto_des,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(tipo_conversion,"formas",ComunesPDF.CENTER);	
						pdfDoc.setCell(tipo_cambio,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+ Comunes.formatoDecimal(montoValuado,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(modo_plazo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fecha_cambio,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(cambio_motivo,"formas",ComunesPDF.CENTER);
						
											
						
					}else if( tipo.equals("CSV") ) {	
						linea = nombre_dist.replace(',',' ')+", "+
						acuse.replace(',',' ')+", "+
						no_docto.replace(',',' ')+", "+
						fechaEmision.replace(',',' ')+", "+
						fechaVecimiento.replace(',',' ')+", "+
						fechaPublic.replace(',',' ')+", "+
						plazo_docto.replace(',',' ')+", "+
						moneda.replace(',',' ')+", "+
						Double.toString (monto).replace(',',' ')+", "+
						plazo_des.replace(',',' ')+", "+
						por_des.replace(',',' ')+", "+
						monto_des.replace(',',' ')+", "+
						tipo_conversion.replace(',',' ')+", "+
						tipo_cambio.replace(',',' ')+", "+
						Double.toString (montoValuado).replace(',',' ')+", "+
						modo_plazo.replace(',',' ')+", "+
						fecha_cambio.replace(',',' ')+", "+
						cambio_motivo.replace(',',' ')+"\n ";
						buffer.write(linea); 
					}
				}else if(noEstatus.equals("14")  ) {	
				
					String nombre_dist =  (rs.getString("NOMBRE_DIST")== null)? "" : rs.getString("NOMBRE_DIST");
					String acuse =  (rs.getString("CC_ACUSE")== null)? "" : rs.getString("CC_ACUSE");
					String nume_docto =  (rs.getString("IG_NUMERO_DOCTO")== null)? "" : rs.getString("IG_NUMERO_DOCTO");
					String fecha_emision =  (rs.getString("DF_FECHA_EMISION")== null)? "" : rs.getString("DF_FECHA_EMISION");
					String fecha_venc =  (rs.getString("DF_FECHA_VENC")== null)? "" : rs.getString("DF_FECHA_VENC");
					String fechapublic =  (rs.getString("FECHA_PUBLICACION")== null)? "" : rs.getString("FECHA_PUBLICACION");
					String plazo_docto =  (rs.getString("IG_PLAZO_DOCTO")== null)? "0" : rs.getString("IG_PLAZO_DOCTO");
					String moneda =  (rs.getString("MONEDA")== null)? "" : rs.getString("MONEDA");
					String plazo_desc =  (rs.getString("IG_PLAZO_DESCUENTO")== null)? "0" : rs.getString("IG_PLAZO_DESCUENTO");
					String por_descuento =  (rs.getString("FN_PORC_DESCUENTO")== null)? "0" : rs.getString("FN_PORC_DESCUENTO");
					String tipo_conver =  (rs.getString("TIPO_CONVERSION")== null)? "" : rs.getString("TIPO_CONVERSION");
					String tipo_cambio =  (rs.getString("TIPO_CAMBIO")== null)? "" : rs.getString("TIPO_CAMBIO");
					String modo_plazo =  (rs.getString("MODO_PLAZO")== null)? "" : rs.getString("MODO_PLAZO");
					String tipo_credito =  (rs.getString("TIPO_CREDITO")== null)? "" : rs.getString("TIPO_CREDITO");
					String ic_documento =  (rs.getString("IC_DOCUMENTO")== null)? "" : rs.getString("IC_DOCUMENTO");
					String plazo_credito =  (rs.getString("IG_PLAZO_CREDITO")== null)? "0" : rs.getString("IG_PLAZO_CREDITO");
					String fecha_ven_credi =  (rs.getString("FECHA_VENC_CREDITO")== null)? "" : rs.getString("FECHA_VENC_CREDITO");
					String fecha_opeIF =  (rs.getString("DF_OPERACIONIF")== null)? "" : rs.getString("DF_OPERACIONIF");
					String nombre_IF =  (rs.getString("NOMBRE_IF")== null)? "" : rs.getString("NOMBRE_IF");
					String referencia =  (rs.getString("REFERENCIA_INT")== null)? "" : rs.getString("REFERENCIA_INT");
					String valor_tasa =  (rs.getString("VALOR_TASA_INT")== null)? "0" : rs.getString("VALOR_TASA_INT");
					String monto_cap_int=  (rs.getString("MONTO_CAP_INT")== null)? "0" : rs.getString("MONTO_CAP_INT");
					String fecha_cambio=  (rs.getString("FECHA_CAMBIO")== null)? "" : rs.getString("FECHA_CAMBIO");
					
					icMoneda =  (rs.getString("IC_MONEDA")== null)? "" : rs.getString("IC_MONEDA");
					double monto =  rs.getDouble("FN_MONTO");
					double montoValuado =   rs.getDouble("MONTO_VALUADO"); 
					double monto_credito =rs.getDouble("MONTO_CREDITO"); 
					double monto_descuento =rs.getDouble("MONTO_DESCUENTO"); 
					double monto_tasa =rs.getDouble("MONTO_TASA_INT"); 
						
					if("1".equals(icMoneda)){
						totalDoctosMN ++;
						totalMontoMN += monto;
						totalMontoValuadoMN += montoValuado;
						totalMontoCreditoMN += monto_credito;
						totalMontoDescuentoMN += monto_descuento;
						totalMontoInteresMN += monto_tasa;
					}else{
						totalDoctosUSD ++;
						totalMontoUSD += monto;
						totalMontoValuadoUSD += montoValuado;
						totalMontoCreditoUSD += monto_credito;
						totalMontoDescuentoUSD += monto_descuento;
						totalMontoInteresUSD += monto_tasa;
					}
							
					if( tipo.equals("PDF") ) {
					
						pdfDoc.setCell("A ","formas",ComunesPDF.CENTER);						
						pdfDoc.setCell(nombre_dist,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell( acuse ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nume_docto ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fecha_emision,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fecha_venc ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechapublic ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(plazo_docto ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(moneda ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+ Comunes.formatoDecimal(monto,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(plazo_desc ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(por_descuento ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+ Comunes.formatoDecimal(monto_descuento,2)  ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(tipo_conver ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(tipo_cambio ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("B ","formas",ComunesPDF.CENTER);						
						pdfDoc.setCell("$"+ Comunes.formatoDecimal(montoValuado,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(modo_plazo ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(tipo_credito ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(ic_documento ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+ Comunes.formatoDecimal(monto_credito,2)  ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(plazo_credito ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fecha_ven_credi ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fecha_opeIF ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombre_IF ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(referencia ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(valor_tasa ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+ Comunes.formatoDecimal(monto_tasa,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+ Comunes.formatoDecimal(monto_cap_int,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(fecha_cambio ,"formas",ComunesPDF.CENTER);
											
					}else if( tipo.equals("CSV") ) {
					
						linea = nombre_dist.replace(',',' ')+", "+				
						acuse .replace(',',' ')+", "+		
						nume_docto.replace(',',' ')+", "+		
						fecha_emision.replace(',',' ')+", "+		
						fecha_venc.replace(',',' ')+", "+		
						fechapublic.replace(',',' ')+", "+		
						plazo_docto.replace(',',' ')+", "+		
						moneda .replace(',',' ')+", "+		
						Double.toString (monto).replace(',',' ')+", "+		
						plazo_desc.replace(',',' ')+", "+		
						por_descuento.replace(',',' ')+", "+		
						Double.toString (monto_descuento).replace(',',' ')+", "+		
						tipo_conver .replace(',',' ')+", "+		
						tipo_cambio.replace(',',' ')+", "+		
						Double.toString (montoValuado).replace(',',' ')+", "+		
						modo_plazo.replace(',',' ')+", "+		
						tipo_credito.replace(',',' ')+", "+		
						ic_documento.replace(',',' ')+", "+		
						Double.toString (monto_credito).replace(',',' ')+", "+		
						plazo_credito.replace(',',' ')+", "+		
						fecha_ven_credi.replace(',',' ')+", "+		
						fecha_opeIF.replace(',',' ')+", "+		
						nombre_IF.replace(',',' ')+", "+		
						referencia.replace(',',' ')+", "+		
						valor_tasa.replace(',',' ')+", "+		
						Double.toString (monto_tasa).replace(',',' ')+", "+		
						monto_cap_int.replace(',',' ')+", "+		
						fecha_cambio.replace(',',' ')+"\n ";	
						buffer.write(linea); 
						
					}
				}else if(noEstatus.equals("2")  ) {
					
					String nombre_Dist=  (rs.getString("NOMBRE_DIST")== null)? "0" : rs.getString("NOMBRE_DIST");
					String acuse=  (rs.getString("CC_ACUSE")== null)? "" : rs.getString("CC_ACUSE");
					String nume_docto=  (rs.getString("IG_NUMERO_DOCTO")== null)? "" : rs.getString("IG_NUMERO_DOCTO");
					String fechaEmision=  (rs.getString("DF_FECHA_EMISION")== null)? "" : rs.getString("DF_FECHA_EMISION");
					String fechaVenc=  (rs.getString("DF_FECHA_VENC")== null)? "" : rs.getString("DF_FECHA_VENC");
					String fechaPublic=  (rs.getString("FECHA_PUBLICACION")== null)? "" : rs.getString("FECHA_PUBLICACION");
					String plazo_docto=  (rs.getString("IG_PLAZO_DOCTO")== null)? "0" : rs.getString("IG_PLAZO_DOCTO");
					String moneda=  (rs.getString("MONEDA")== null)? "" : rs.getString("MONEDA");
					String por_aforo=  (rs.getString("FN_PORC_AFORO")== null)? "0" : rs.getString("FN_PORC_AFORO");
					String plazo_desc=  (rs.getString("IG_PLAZO_DESCUENTO")== null)? "0" : rs.getString("IG_PLAZO_DESCUENTO");
					String porc_des=  (rs.getString("FN_PORC_DESCUENTO")== null)? "0" : rs.getString("FN_PORC_DESCUENTO");
					String monto_descuento=  (rs.getString("MONTO_DESCUENTO")== null)? "0" : rs.getString("MONTO_DESCUENTO");
					String tipo_conversion=  (rs.getString("TIPO_CONVERSION")== null)? " " : rs.getString("TIPO_CONVERSION");
					String tipo_cambio=  (rs.getString("TIPO_CAMBIO")== null)? " " : rs.getString("TIPO_CAMBIO");
					String modo_plazo=  (rs.getString("MODO_PLAZO")== null)? "0" : rs.getString("MODO_PLAZO");
					String tipo_crdito=  (rs.getString("TIPO_CREDITO")== null)? " " : rs.getString("TIPO_CREDITO");
					String ic_documento=  (rs.getString("IC_DOCUMENTO")== null)? " " : rs.getString("IC_DOCUMENTO");
					String plazo_credito=  (rs.getString("IG_PLAZO_CREDITO")== null)? "0" : rs.getString("IG_PLAZO_CREDITO");
					String fecha_credito=  (rs.getString("FECHA_VENC_CREDITO")== null)? " " : rs.getString("FECHA_VENC_CREDITO");
					String fecha_OpeIF=  (rs.getString("DF_OPERACIONIF")== null)? " " : rs.getString("DF_OPERACIONIF");
					String nombreIF=  (rs.getString("NOMBRE_IF")== null)? " " : rs.getString("NOMBRE_IF");
					String referencia=  (rs.getString("REFERENCIA_INT")== null)? " " : rs.getString("REFERENCIA_INT");
					String valor_tasa=  (rs.getString("VALOR_TASA_INT")== null)? "0" : rs.getString("VALOR_TASA_INT");
					String monto_capital=  (rs.getString("MONTO_CAP_INT")== null)? "0" : rs.getString("MONTO_CAP_INT");
					String fecha_cambio=  (rs.getString("FECHA_CAMBIO")== null)? " " : rs.getString("FECHA_CAMBIO");
					icMoneda =  (rs.getString("IC_MONEDA")== null)? "" : rs.getString("IC_MONEDA");
					double monto =  rs.getDouble("FN_MONTO");
					double montoValuado =   rs.getDouble("MONTO_VALUADO"); 
					double monto_credito =   rs.getDouble("MONTO_CREDITO"); 
					double monto_tasa =   rs.getDouble("MONTO_TASA_INT"); 
					double monto_descontar =   rs.getDouble("MONTO_DESC"); 
					
					if("1".equals(icMoneda)){
					totalDoctosMN ++;
					totalMontoMN += monto;
					totalMontoValuadoMN += montoValuado;
					totalMontoCreditoMN += monto_credito;
					totalMontoDescuentoMN += monto_descontar;
					totalMontoInteresMN += monto_tasa;
				}else{
					totalDoctosUSD ++;
					totalMontoUSD += monto;
					totalMontoValuadoUSD += montoValuado;
					totalMontoCreditoUSD += monto_credito;
					totalMontoDescuentoUSD += monto_descontar;
					totalMontoInteresUSD += monto_tasa;
				}
						

					if( tipo.equals("PDF") ) {
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombre_Dist,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(acuse,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nume_docto ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaEmision,"formas",ComunesPDF.CENTER); 
						pdfDoc.setCell(fechaVenc,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaPublic,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(plazo_docto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell( "$"+ Comunes.formatoDecimal(monto,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(por_aforo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+ Comunes.formatoDecimal(monto_descontar,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(plazo_desc,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(porc_des,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+ Comunes.formatoDecimal(monto_descuento,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(tipo_conversion,"formas",ComunesPDF.CENTER);						
						pdfDoc.setCell("B","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(tipo_cambio,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+ Comunes.formatoDecimal(montoValuado,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(modo_plazo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(tipo_crdito,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(ic_documento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell( "$"+ Comunes.formatoDecimal(monto_credito,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(plazo_credito,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fecha_credito,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fecha_OpeIF,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell( nombreIF,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(referencia,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(valor_tasa,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+ Comunes.formatoDecimal(monto_tasa,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+ Comunes.formatoDecimal(monto_capital,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(fecha_cambio,"formas",ComunesPDF.CENTER);				
						
					}else if( tipo.equals("CSV") ) {
					
					linea = nombre_Dist.replace(',',' ')+", "+
						acuse.replace(',',' ')+", "+
						nume_docto.replace(',',' ')+", "+
						fechaEmision.replace(',',' ')+", "+
						fechaVenc.replace(',',' ')+", "+
						fechaPublic.replace(',',' ')+", "+
						plazo_docto.replace(',',' ')+", "+
						moneda.replace(',',' ')+", "+
						Double.toString (monto).replace(',',' ')+", "+
						por_aforo.replace(',',' ')+", "+
						Double.toString (monto_descontar).replace(',',' ')+", "+
						plazo_desc.replace(',',' ')+", "+
						porc_des.replace(',',' ')+", "+
						monto_descuento.replace(',',' ')+", "+
						tipo_conversion.replace(',',' ')+", "+
						tipo_cambio.replace(',',' ')+", "+
						Double.toString (montoValuado).replace(',',' ')+", "+
						modo_plazo.replace(',',' ')+", "+
						tipo_crdito.replace(',',' ')+", "+
						ic_documento.replace(',',' ')+", "+
						Double.toString (monto_credito).replace(',',' ')+", "+
						plazo_credito.replace(',',' ')+", "+
						fecha_credito.replace(',',' ')+", "+
						fecha_OpeIF.replace(',',' ')+", "+
						nombreIF.replace(',',' ')+", "+
						referencia.replace(',',' ')+", "+
						valor_tasa.replace(',',' ')+", "+
						Double.toString (monto_tasa).replace(',',' ')+", "+
						monto_capital.replace(',',' ')+", "+
						fecha_cambio.replace(',',' ')+"\n ";
						buffer.write(linea); 
						
						
					}
				}else if(noEstatus.equals("23") ||noEstatus.equals("34") ) {
					String nombre_Dist=  (rs.getString("NOMBRE_DIST")== null)? "0" : rs.getString("NOMBRE_DIST");
					String nombre_Epo=  (rs.getString("NOMBRE_EPO")== null)? "0" : rs.getString("NOMBRE_EPO");
					String acuse=  (rs.getString("CC_ACUSE")== null)? "" : rs.getString("CC_ACUSE");
					String nume_docto=  (rs.getString("IG_NUMERO_DOCTO")== null)? "" : rs.getString("IG_NUMERO_DOCTO");
					String fechaEmision=  (rs.getString("DF_FECHA_EMISION")== null)? "" : rs.getString("DF_FECHA_EMISION");
					String fechaVenc=  (rs.getString("DF_FECHA_VENC")== null)? "" : rs.getString("DF_FECHA_VENC");
					String fechaPublic=  (rs.getString("FECHA_PUBLICACION")== null)? "" : rs.getString("FECHA_PUBLICACION");
					String plazo_docto=  (rs.getString("IG_PLAZO_DOCTO")== null)? "0" : rs.getString("IG_PLAZO_DOCTO");
					String moneda=  (rs.getString("MONEDA")== null)? "" : rs.getString("MONEDA");
					String por_aforo=  (rs.getString("FN_PORC_AFORO")== null)? "0" : rs.getString("FN_PORC_AFORO");
					String plazo_desc=  (rs.getString("IG_PLAZO_DESCUENTO")== null)? "0" : rs.getString("IG_PLAZO_DESCUENTO");
					String porc_des=  (rs.getString("FN_PORC_DESCUENTO")== null)? "0" : rs.getString("FN_PORC_DESCUENTO");
					String monto_descuento=  (rs.getString("MONTO_DESCUENTO")== null)? "0" : rs.getString("MONTO_DESCUENTO");
					String tipo_conversion=  (rs.getString("TIPO_CONVERSION")== null)? " " : rs.getString("TIPO_CONVERSION");
					String tipo_cambio=  (rs.getString("TIPO_CAMBIO")== null)? " " : rs.getString("TIPO_CAMBIO");
					String modo_plazo=  (rs.getString("MODO_PLAZO")== null)? "0" : rs.getString("MODO_PLAZO");
					String tipo_crdito=  (rs.getString("TIPO_CREDITO")== null)? " " : rs.getString("TIPO_CREDITO");
					String ic_documento=  (rs.getString("IC_DOCUMENTO")== null)? " " : rs.getString("IC_DOCUMENTO");
					String plazo_credito=  (rs.getString("IG_PLAZO_CREDITO")== null)? "0" : rs.getString("IG_PLAZO_CREDITO");
					String fecha_credito=  (rs.getString("FECHA_VENC_CREDITO")== null)? " " : rs.getString("FECHA_VENC_CREDITO");
					String fecha_OpeIF=  (rs.getString("DF_OPERACIONIF")== null)? " " : rs.getString("DF_OPERACIONIF");
					String nombreIF=  (rs.getString("NOMBRE_IF")== null)? " " : rs.getString("NOMBRE_IF");
					String referencia=  (rs.getString("REFERENCIA_INT")== null)? " " : rs.getString("REFERENCIA_INT");
					String valor_tasa=  (rs.getString("VALOR_TASA_INT")== null)? "0" : rs.getString("VALOR_TASA_INT");
					String monto_capital=  (rs.getString("MONTO_CAP_INT")== null)? "0" : rs.getString("MONTO_CAP_INT");
					String fecha_cambio=  (rs.getString("FECHA_CAMBIO")== null)? " " : rs.getString("FECHA_CAMBIO");
					icMoneda =  (rs.getString("IC_MONEDA")== null)? "" : rs.getString("IC_MONEDA");
					double monto =  rs.getDouble("FN_MONTO");
					double montoValuado =   rs.getDouble("MONTO_VALUADO"); 
					double monto_credito =   rs.getDouble("MONTO_CREDITO"); 
					double monto_tasa =   rs.getDouble("MONTO_TASA_INT"); 
					double monto_descontar =   rs.getDouble("MONTO_DESC"); 
					
					if("1".equals(icMoneda)){
					totalDoctosMN ++;
					totalMontoMN += monto;
					totalMontoValuadoMN += montoValuado;
					totalMontoCreditoMN += monto_credito;
					totalMontoDescuentoMN += monto_descontar;
					totalMontoInteresMN += monto_tasa;
					}else{
					totalDoctosUSD ++;
					totalMontoUSD += monto;
					totalMontoValuadoUSD += montoValuado;
					totalMontoCreditoUSD += monto_credito;
					totalMontoDescuentoUSD += monto_descontar;
					totalMontoInteresUSD += monto_tasa;
					}
						

					if( tipo.equals("PDF") ) {
						pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nombre_Epo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(acuse,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(nume_docto ,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaEmision,"formas",ComunesPDF.CENTER); 
						pdfDoc.setCell(fechaVenc,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaPublic,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(plazo_docto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell( "$"+ Comunes.formatoDecimal(monto,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(plazo_desc,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(Comunes.formatoDecimal(porc_des,4)+"%","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("B","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+ Comunes.formatoDecimal(monto_descuento,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(modo_plazo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(ic_documento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell( "$"+ Comunes.formatoDecimal(monto_credito,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(plazo_credito,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fecha_credito,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell( nombreIF,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(referencia,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(Comunes.formatoDecimal(valor_tasa,4)+"%","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+ Comunes.formatoDecimal(monto_tasa,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+ Comunes.formatoDecimal(monto_capital,2) ,"formas",ComunesPDF.RIGHT);
									
						
					}else if( tipo.equals("CSV") ) {
					
					linea = nombre_Epo.replace(',',' ')+", "+
						acuse.replace(',',' ')+", "+
						nume_docto.replace(',',' ')+", "+
						fechaEmision.replace(',',' ')+", "+
						fechaVenc.replace(',',' ')+", "+
						fechaPublic.replace(',',' ')+", "+
						plazo_docto.replace(',',' ')+", "+
						moneda.replace(',',' ')+", "+
						"$"+Double.toString (monto).replace(',',' ')+", "+
						plazo_desc.replace(',',' ')+", "+
						porc_des.replace(',',' ')+"%"+", "+
						"$"+monto_descuento.replace(',',' ')+", "+
						modo_plazo.replace(',',' ')+", "+
						ic_documento.replace(',',' ')+", "+
						"$"+Double.toString (monto_credito).replace(',',' ')+", "+
						plazo_credito.replace(',',' ')+", "+
						fecha_credito.replace(',',' ')+", "+
						nombreIF.replace(',',' ')+", "+
						referencia.replace(',',' ')+", "+
						valor_tasa.replace(',',' ')+"%"+", "+
						"$"+Double.toString (monto_tasa).replace(',',' ')+", "+
						"$"+monto_capital.replace(',',' ')+"\n ";
						buffer.write(linea); 
						
						
						
					}
				}else if(noEstatus.equals("21")) {
					
					String nombre_dist=  (rs.getString("NOMBRE_DIST")== null)? " " : rs.getString("NOMBRE_DIST");
					String acuse=  (rs.getString("CC_ACUSE")== null)? " " : rs.getString("CC_ACUSE");
					String num_docto=  (rs.getString("IG_NUMERO_DOCTO")== null)? " " : rs.getString("IG_NUMERO_DOCTO");
					String fechaEmision=  (rs.getString("DF_FECHA_EMISION")== null)? " " : rs.getString("DF_FECHA_EMISION");
					String fecha_Venc=  (rs.getString("DF_FECHA_VENC")== null)? " " : rs.getString("DF_FECHA_VENC");
					String fecha_public=  (rs.getString("FECHA_PUBLICACION")== null)? " " : rs.getString("FECHA_PUBLICACION");
					String plazo_docto=  (rs.getString("IG_PLAZO_DOCTO")== null)? " " : rs.getString("IG_PLAZO_DOCTO");
					String moneda=  (rs.getString("MONEDA")== null)? " " : rs.getString("MONEDA");
					String plazo_desc=  (rs.getString("IG_PLAZO_DESCUENTO")== null)? " " : rs.getString("IG_PLAZO_DESCUENTO");
					String por_desc=  (rs.getString("FN_PORC_DESCUENTO")== null)? " " : rs.getString("FN_PORC_DESCUENTO");
					String monto_desc=  (rs.getString("MONTO_DESCUENTO")== null)? " " : rs.getString("MONTO_DESCUENTO");
					String tipo_conver=  (rs.getString("TIPO_CONVERSION")== null)? " " : rs.getString("TIPO_CONVERSION");
					String tipo_cambio=  (rs.getString("TIPO_CAMBIO")== null)? " " : rs.getString("TIPO_CAMBIO");
					String modo_plazo=  (rs.getString("MODO_PLAZO")== null)? " " : rs.getString("MODO_PLAZO");
					String fechaCambio=  (rs.getString("FECHA_CAMBIO")== null)? " " : rs.getString("FECHA_CAMBIO");
					String fechaAnEmision=  (rs.getString("FECHA_EMISION_ANT")== null)? " " : rs.getString("FECHA_EMISION_ANT");
					String fechaAnVen=  (rs.getString("FECHA_VENC_ANT")== null)? " " : rs.getString("FECHA_VENC_ANT");
					String monto_Anterior=  (rs.getString("FN_MONTO_ANTERIOR")== null)? " " : rs.getString("FN_MONTO_ANTERIOR");
					String modo_plazoAn=  (rs.getString("MODO_PLAZO_ANT")== null)? " " : rs.getString("MODO_PLAZO_ANT");
					icMoneda =  (rs.getString("IC_MONEDA")== null)? "" : rs.getString("IC_MONEDA");
					double monto =  rs.getDouble("FN_MONTO");
					double montoValuado =   rs.getDouble("MONTO_VALUADO"); 
					
					if("1".equals(icMoneda)){
						totalDoctosMN ++;
						totalMontoMN += monto;
						totalMontoValuadoMN += montoValuado;
					}else{
						totalDoctosUSD ++;
						totalMontoUSD += monto;
						totalMontoValuadoUSD += montoValuado;
					}
								
					if( tipo.equals("PDF") ) {
						 pdfDoc.setCell(nombre_dist,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(acuse,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(num_docto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaEmision,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fecha_Venc,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fecha_public,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(plazo_docto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell( "$"+ Comunes.formatoDecimal(monto,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(plazo_desc,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(por_desc,"formas",ComunesPDF.CENTER);						
						pdfDoc.setCell("$"+ Comunes.formatoDecimal(monto_desc,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(tipo_conver,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(tipo_cambio,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell( "$"+ Comunes.formatoDecimal(montoValuado,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(modo_plazo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaCambio,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaAnEmision,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaAnVen,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(monto_Anterior,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(modo_plazoAn,"formas",ComunesPDF.CENTER);						
					
					}else if( tipo.equals("CSV") ) {
						linea = nombre_dist.replace(',',' ')+", "+
										acuse.replace(',',' ')+", "+
										num_docto.replace(',',' ')+", "+
										fechaEmision.replace(',',' ')+", "+
										fecha_Venc.replace(',',' ')+", "+
										fecha_public.replace(',',' ')+", "+
										plazo_docto.replace(',',' ')+", "+
										Double.toString (monto).replace(',',' ')+", "+
										moneda.replace(',',' ')+", "+
										plazo_desc.replace(',',' ')+", "+
										por_desc.replace(',',' ')+", "+					
										monto_desc.replace(',',' ')+", "+
										tipo_conver.replace(',',' ')+", "+
										tipo_cambio.replace(',',' ')+", "+
										Double.toString (montoValuado).replace(',',' ')+", "+
										modo_plazo.replace(',',' ')+", "+
										fechaCambio.replace(',',' ')+", "+
										fechaAnEmision.replace(',',' ')+", "+
										fechaAnVen.replace(',',' ')+", "+
										monto_Anterior.replace(',',' ')+", "+
										modo_plazoAn.replace(',',' ')+"\n ";
						buffer.write(linea); 						
					}
				}else  if(noEstatus.equals("24")  ) {	
				
					String nombreDis=  (rs.getString("NOMBRE_DIST")== null)? " " : rs.getString("NOMBRE_DIST");
					String acuse=  (rs.getString("CC_ACUSE")== null)? " " : rs.getString("CC_ACUSE");
					String num_docto=  (rs.getString("IG_NUMERO_DOCTO")== null)? " " : rs.getString("IG_NUMERO_DOCTO");
					String fechaEmision=  (rs.getString("DF_FECHA_EMISION")== null)? " " : rs.getString("DF_FECHA_EMISION");
					String fechaVenc=  (rs.getString("DF_FECHA_VENC")== null)? " " : rs.getString("DF_FECHA_VENC");
					String fechaPublic=  (rs.getString("FECHA_PUBLICACION")== null)? " " : rs.getString("FECHA_PUBLICACION");
					String plazo_Docto=  (rs.getString("IG_PLAZO_DOCTO")== null)? " " : rs.getString("IG_PLAZO_DOCTO");
					String moneda=  (rs.getString("MONEDA")== null)? " " : rs.getString("MONEDA");
					String plazo_desc=  (rs.getString("IG_PLAZO_DESCUENTO")== null)? " " : rs.getString("IG_PLAZO_DESCUENTO");
					String por_desc=  (rs.getString("FN_PORC_DESCUENTO")== null)? " " : rs.getString("FN_PORC_DESCUENTO");
					String monto_desc=  (rs.getString("MONTO_DESCUENTO")== null)? " " : rs.getString("MONTO_DESCUENTO");
					String tipo_conver=  (rs.getString("TIPO_CONVERSION")== null)? " " : rs.getString("TIPO_CONVERSION");
					String tipo_cambio=  (rs.getString("TIPO_CAMBIO")== null)? " " : rs.getString("TIPO_CAMBIO");
					String modo_plazo=  (rs.getString("MODO_PLAZO")== null)? " " : rs.getString("MODO_PLAZO");
					String fecha_cambio=  (rs.getString("FECHA_CAMBIO")== null)? " " : rs.getString("FECHA_CAMBIO");
					String causa=  (rs.getString("CT_CAMBIO_MOTIVO")== null)? " " : rs.getString("CT_CAMBIO_MOTIVO");
					icMoneda =  (rs.getString("IC_MONEDA")== null)? "" : rs.getString("IC_MONEDA");
					double monto =  rs.getDouble("FN_MONTO");
					double montoValuado =   rs.getDouble("MONTO_VALUADO"); 
					
					if("1".equals(icMoneda)){
						totalDoctosMN ++;
						totalMontoMN += monto;
						totalMontoValuadoMN += montoValuado;
					}else{
						totalDoctosUSD ++;
						totalMontoUSD += monto;
						totalMontoValuadoUSD += montoValuado;
					}
					
				
		
					
					if( tipo.equals("PDF") ) {
						pdfDoc.setCell(nombreDis,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(acuse,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(num_docto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaEmision,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaVenc,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaPublic,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(plazo_Docto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+ Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(plazo_desc,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(por_desc,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell( "$"+ Comunes.formatoDecimal(monto_desc,2) ,"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(tipo_conver,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(tipo_cambio,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+ Comunes.formatoDecimal(montoValuado,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(modo_plazo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fecha_cambio,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(causa,"formas",ComunesPDF.CENTER);					
					
					}else 	if( tipo.equals("CSV") ) {
						
						linea  =  nombreDis.replace(',',' ')+", "+
										  acuse.replace(',',' ')+", "+
											num_docto.replace(',',' ')+", "+
											fechaEmision.replace(',',' ')+", "+
											fechaVenc.replace(',',' ')+", "+
											fechaPublic.replace(',',' ')+", "+
											plazo_Docto.replace(',',' ')+", "+
											moneda.replace(',',' ')+", "+
											Double.toString (monto).replace(',',' ')+", "+
											plazo_desc.replace(',',' ')+", "+
											por_desc.replace(',',' ')+", "+
											monto_desc.replace(',',' ')+", "+
											tipo_conver.replace(',',' ')+", "+
											tipo_cambio.replace(',',' ')+", "+
											Double.toString (montoValuado).replace(',',' ')+", "+
											modo_plazo.replace(',',' ')+", "+
											fecha_cambio.replace(',',' ')+", "+
											causa.replace(',',' ')+"\n ";	
							buffer.write(linea); 												
					}			
				}			
			}//while (rs.next()) {
			
			//*************************** Totales ****************************************
			if(noEstatus.equals("21") ) {
					totales21r = new JSONArray();
				if("PDF".equals(tipo) )  {
					for(int i= 0; i<2; i++){				
						if(totalDoctosMN>0  && i ==0){
								pdfDoc.setCell("Total M.N." ,"celda01",ComunesPDF.CENTER,2);							
								pdfDoc.setCell( Double.toString (totalDoctosMN) ,"formas",ComunesPDF.CENTER);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,4);
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoMN,2)    ,"formas",ComunesPDF.RIGHT,2);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,5);
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoValuadoMN,2) ,"formas",ComunesPDF.RIGHT,2);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,5);
						}else if( totalDoctosUSD>0  && i ==1){
								pdfDoc.setCell("Total Dolares" ,"celda01",ComunesPDF.CENTER,2);
								pdfDoc.setCell( Double.toString (totalDoctosUSD) ,"formas",ComunesPDF.CENTER);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,4);
								pdfDoc.setCell(   "$"+ Comunes.formatoDecimal(totalMontoUSD, 2)  ,"formas",ComunesPDF.RIGHT,2);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,5);
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoValuadoUSD,2)  ,"formas",ComunesPDF.RIGHT,2);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,5);
						}			
					}
				}else if("Totales".equals(tipo) )  {
					for(int i= 0; i<2; i++){	
						if(i ==0 ){
							totales21t = new HashMap();		
							totales21t.put("MONEDA","MONEDA NACIONAL");
							totales21t.put("TOTAL_REGISTROS",Double.toString (totalDoctosMN));
							totales21t.put("TOTAL_MONTO",Double.toString (totalMontoMN));
							totales21t.put("MONTO_VALUADO",Double.toString (totalMontoValuadoMN));
							totales21t.put("MONTO_CREDITO",Double.toString (totalMontoCreditoMN));
						}
						if(i ==1){						
							totales21t = new HashMap();		
							totales21t.put("MONEDA","MONEDA DOLAR");
							totales21t.put("TOTAL_REGISTROS",Double.toString (totalDoctosUSD));
							totales21t.put("TOTAL_MONTO",Double.toString (totalMontoUSD));
							totales21t.put("MONTO_VALUADO",Double.toString (totalMontoValuadoUSD));
							totales21t.put("MONTO_CREDITO",Double.toString (totalMontoCreditoUSD));
						}
						totales21r.add(totales21t);	
					}
					nombreArchivo = "({\"success\": true, \"total\": \"" + totales21r.size() + "\", \"registros\": " + totales21r.toString()+"})";
				}
				
				
			}
			
			
			if(noEstatus.equals("4") ) {
					totales21r = new JSONArray();
				if("PDF".equals(tipo) )  {
					for(int i= 0; i<2; i++){			
						if(totalDoctosMN>0  && i ==0){
								pdfDoc.setCell("Total M.N." ,"celda01",ComunesPDF.CENTER,2);							
								pdfDoc.setCell( Double.toString (totalDoctosMN) ,"formas",ComunesPDF.CENTER);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,5);
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoMN,2)    ,"formas",ComunesPDF.RIGHT,2);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,4);
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoValuadoMN,2) ,"formas",ComunesPDF.RIGHT,2);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,2);
						}else if( totalDoctosUSD>0  && i ==1){
								pdfDoc.setCell("Total Dolares" ,"celda01",ComunesPDF.CENTER,2);
								pdfDoc.setCell( Double.toString (totalDoctosUSD) ,"formas",ComunesPDF.CENTER);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,5);
								pdfDoc.setCell(   "$"+ Comunes.formatoDecimal(totalMontoUSD, 2)  ,"formas",ComunesPDF.RIGHT,2);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,4);
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoValuadoUSD,2)  ,"formas",ComunesPDF.RIGHT,2);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,2);
						}			
					}				
				
				}else if("Totales".equals(tipo) )  {
					for(int i= 0; i<2; i++){	
						if(i ==0 ){
							totales21t = new HashMap();		
							totales21t.put("MONEDA","MONEDA NACIONAL");
							totales21t.put("TOTAL_REGISTROS",Double.toString (totalDoctosMN));
							totales21t.put("TOTAL_MONTO",Double.toString (totalMontoMN));
							totales21t.put("MONTO_VALUADO",Double.toString (totalMontoValuadoMN));
							totales21t.put("MONTO_CREDITO",Double.toString (totalMontoCreditoMN));
						}
						if(i ==1){						
							totales21t = new HashMap();		
							totales21t.put("MONEDA","MONEDA DOLAR");
							totales21t.put("TOTAL_REGISTROS",Double.toString (totalDoctosUSD));
							totales21t.put("TOTAL_MONTO",Double.toString (totalMontoUSD));
							totales21t.put("MONTO_VALUADO",Double.toString (totalMontoValuadoUSD));
							totales21t.put("MONTO_CREDITO",Double.toString (totalMontoCreditoUSD));
						}
						totales21r.add(totales21t);	
					}
					nombreArchivo = "({\"success\": true, \"total\": \"" + totales21r.size() + "\", \"registros\": " + totales21r.toString()+"})";
				}
				
				
			}
			
			
			if(noEstatus.equals("2") ) {
				totales21r = new JSONArray();
				if("PDF".equals(tipo) )  {				
					for(int i= 0; i<2; i++){			
						if(totalDoctosMN>0  && i ==0){
								pdfDoc.setCell("Total M.N." ,"celda01",ComunesPDF.CENTER,2);							
								pdfDoc.setCell( Double.toString (totalDoctosMN) ,"formas",ComunesPDF.CENTER);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,5);
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoMN,2)    ,"formas",ComunesPDF.RIGHT,2);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER);
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoDescuentoMN,2) ,"formas",ComunesPDF.RIGHT);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,2);
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoValuadoMN,2) ,"formas",ComunesPDF.RIGHT);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,2);
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoCreditoMN,2) ,"formas",ComunesPDF.RIGHT);						
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,10);
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoInteresMN,2) ,"formas",ComunesPDF.RIGHT);								
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoCreditoMN+totalMontoInteresMN,2) ,"formas",ComunesPDF.RIGHT);	
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER);
						}else if( totalDoctosUSD>0  && i ==1){
								pdfDoc.setCell("Total Dolares" ,"celda01",ComunesPDF.CENTER,2);
								pdfDoc.setCell( Double.toString (totalDoctosUSD) ,"formas",ComunesPDF.CENTER);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,5);
								pdfDoc.setCell(   "$"+ Comunes.formatoDecimal(totalMontoUSD, 2)  ,"formas",ComunesPDF.RIGHT,2);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER);
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoDescuentoUSD,2)  ,"formas",ComunesPDF.RIGHT);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,2);
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoValuadoUSD,2)  ,"formas",ComunesPDF.RIGHT);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,2);
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoCreditoUSD,2)  ,"formas",ComunesPDF.RIGHT);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,10);
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoInteresUSD,2)  ,"formas",ComunesPDF.RIGHT);
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoCreditoUSD+totalMontoInteresUSD,2)  ,"formas",ComunesPDF.RIGHT);							
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER);								
						}			
					}				
				}else if("Totales".equals(tipo) )  {												
					for(int i= 0; i<2; i++){						
						if( i ==0 ){							
							totales21t = new HashMap();		
							totales21t.put("MONEDA","MONEDA NACIONAL");
							totales21t.put("TOTAL_REGISTROS",Double.toString (totalDoctosMN));
							totales21t.put("TOTAL_MONTO",Double.toString (totalMontoMN));
							totales21t.put("MONTO_DESCUENTO",Double.toString (totalMontoDescuentoMN));
							totales21t.put("MONTO_VALUADO",Double.toString (totalMontoValuadoMN));
							totales21t.put("MONTO_CREDITO",Double.toString (totalMontoCreditoMN));
							totales21t.put("MONTO_INTERES",Double.toString (totalMontoInteresMN));	
							totales21t.put("MONTO_CAPITAL_INT",Double.toString (totalMontoCreditoMN+totalMontoInteresMN));
						}
						if( i ==1){	
							totales21t = new HashMap();		
							totales21t.put("MONEDA","MONEDA DOLAR");
							totales21t.put("TOTAL_REGISTROS",Double.toString (totalDoctosUSD));
							totales21t.put("TOTAL_MONTO",Double.toString (totalMontoUSD));
							totales21t.put("MONTO_DESCUENTO",Double.toString (totalMontoDescuentoUSD));
							totales21t.put("MONTO_VALUADO",Double.toString (totalMontoValuadoUSD));	
							totales21t.put("MONTO_CREDITO",Double.toString (totalMontoCreditoUSD));	
							totales21t.put("MONTO_INTERES",Double.toString (totalMontoInteresUSD));	
							totales21t.put("MONTO_CAPITAL_INT",Double.toString (totalMontoCreditoUSD+totalMontoInteresUSD));	
						}		
						totales21r.add(totales21t);	
					}
					
					nombreArchivo = "({\"success\": true, \"total\": \"" + totales21r.size() + "\", \"registros\": " + totales21r.toString()+"})";
				}				
			}
			if(noEstatus.equals("23")|| noEstatus.equals("34")) {
				totales21r = new JSONArray();
				if("PDF".equals(tipo) )  {				
					for(int i= 0; i<2; i++){			
						if(totalDoctosMN>0  && i ==0){
								pdfDoc.setCell("Total M.N." ,"celda01",ComunesPDF.CENTER,1);
								pdfDoc.setCell("Total Documentos" ,"celda01",ComunesPDF.CENTER,1);
								pdfDoc.setCell("Total Monto" ,"celda01",ComunesPDF.CENTER,2);
								pdfDoc.setCell("Total Monto Descuento" ,"celda01",ComunesPDF.CENTER,2);
								pdfDoc.setCell("Total Monto Credito" ,"celda01",ComunesPDF.CENTER,2);
								pdfDoc.setCell("Total Monto de Intereses" ,"celda01",ComunesPDF.CENTER,2);
								pdfDoc.setCell("Monto Total(C�pital e Intereses)" ,"celda01",ComunesPDF.CENTER,2);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,1);
								pdfDoc.setCell( Double.toString (totalDoctosMN) ,"formas",ComunesPDF.CENTER,1);
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoMN,2)    ,"formas",ComunesPDF.RIGHT,2);
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoDescuentoMN,2) ,"formas",ComunesPDF.RIGHT,2);
								//pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoValuadoMN,2) ,"formas",ComunesPDF.RIGHT);
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoCreditoMN,2) ,"formas",ComunesPDF.RIGHT,2);						
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoInteresMN,2) ,"formas",ComunesPDF.RIGHT,2);								
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoCreditoMN+totalMontoInteresMN,2) ,"formas",ComunesPDF.RIGHT,2);	
						}else if( totalDoctosUSD>0  && i ==1){
								pdfDoc.setCell("Total Dolares" ,"celda01",ComunesPDF.CENTER,1);
								pdfDoc.setCell("Total Documentos" ,"celda01",ComunesPDF.CENTER,1);
								pdfDoc.setCell("Total Monto" ,"celda01",ComunesPDF.CENTER,2);
								pdfDoc.setCell("Total Monto Descuento" ,"celda01",ComunesPDF.CENTER,2);
								pdfDoc.setCell("Total Monto Credito" ,"celda01",ComunesPDF.CENTER,2);
								pdfDoc.setCell("Total Monto de Intereses" ,"celda01",ComunesPDF.CENTER,2);
								pdfDoc.setCell("Monto Total(C�pital e Intereses)" ,"celda01",ComunesPDF.CENTER,2);
								
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,1);
								pdfDoc.setCell( Double.toString (totalDoctosUSD) ,"formas",ComunesPDF.CENTER,2);
								pdfDoc.setCell(   "$"+ Comunes.formatoDecimal(totalMontoUSD, 2)  ,"formas",ComunesPDF.RIGHT,2);
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoDescuentoUSD,2)  ,"formas",ComunesPDF.RIGHT,2);
								//pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoValuadoUSD,2)  ,"formas",ComunesPDF.RIGHT);
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoCreditoUSD,2)  ,"formas",ComunesPDF.RIGHT,2);
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoInteresUSD,2)  ,"formas",ComunesPDF.RIGHT,2);
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoCreditoUSD+totalMontoInteresUSD,2)  ,"formas",ComunesPDF.RIGHT,2);							
						}			
					}				
				}else if("Totales".equals(tipo) )  {												
					for(int i= 0; i<2; i++){						
						if( i ==0 ){							
							totales21t = new HashMap();		
							totales21t.put("MONEDA","MONEDA NACIONAL");
							totales21t.put("TOTAL_REGISTROS",Double.toString (totalDoctosMN));
							totales21t.put("TOTAL_MONTO",Double.toString (totalMontoMN));
							totales21t.put("MONTO_DESCUENTO",Double.toString (totalMontoDescuentoMN));
							totales21t.put("MONTO_VALUADO",Double.toString (totalMontoValuadoMN));
							totales21t.put("MONTO_CREDITO",Double.toString (totalMontoCreditoMN));
							totales21t.put("MONTO_INTERES",Double.toString (totalMontoInteresMN));	
							totales21t.put("MONTO_CAPITAL_INT",Double.toString (totalMontoCreditoMN+totalMontoInteresMN));
						}
						if( i ==1){	
							totales21t = new HashMap();		
							totales21t.put("MONEDA","MONEDA DOLAR");
							totales21t.put("TOTAL_REGISTROS",Double.toString (totalDoctosUSD));
							totales21t.put("TOTAL_MONTO",Double.toString (totalMontoUSD));
							totales21t.put("MONTO_DESCUENTO",Double.toString (totalMontoDescuentoUSD));
							totales21t.put("MONTO_VALUADO",Double.toString (totalMontoValuadoUSD));	
							totales21t.put("MONTO_CREDITO",Double.toString (totalMontoCreditoUSD));	
							totales21t.put("MONTO_INTERES",Double.toString (totalMontoInteresUSD));	
							totales21t.put("MONTO_CAPITAL_INT",Double.toString (totalMontoCreditoUSD+totalMontoInteresUSD));	
						}		
						totales21r.add(totales21t);	
					}
					
					nombreArchivo = "({\"success\": true, \"total\": \"" + totales21r.size() + "\", \"registros\": " + totales21r.toString()+"})";
				}				
			}
			
			if(noEstatus.equals("14") ) {
				totales21r = new JSONArray();
				if("PDF".equals(tipo) )  {
					for(int i= 0; i<2; i++){		
						if(totalDoctosMN>0 && i ==0){
							pdfDoc.setCell("Total M.N." ,"celda01",ComunesPDF.CENTER,2);	
							pdfDoc.setCell( Double.toString (totalDoctosMN)  ,"formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell( ""  ,"formas",ComunesPDF.CENTER,5);
							pdfDoc.setCell("$"+ Comunes.formatoDecimal(totalMontoMN,2)  ,"formas",ComunesPDF.RIGHT);	
							pdfDoc.setCell( ""  ,"formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell("$"+ Comunes.formatoDecimal(totalMontoDescuentoMN,2)  ,"formas",ComunesPDF.RIGHT);	
							pdfDoc.setCell( ""  ,"formas",ComunesPDF.CENTER,2);	
							pdfDoc.setCell( ""  ,"formas",ComunesPDF.CENTER);	
							pdfDoc.setCell("$"+ Comunes.formatoDecimal(totalMontoValuadoMN,2)  ,"formas",ComunesPDF.RIGHT);	
							pdfDoc.setCell( ""  ,"formas",ComunesPDF.CENTER,3);	
							pdfDoc.setCell("$"+ Comunes.formatoDecimal(totalMontoCreditoMN,2) ,"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell( ""  ,"formas",ComunesPDF.CENTER,6);	
							pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoInteresMN,2) ,"formas",ComunesPDF.RIGHT);	
							pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoCreditoMN+totalMontoInteresMN,2) ,"formas",ComunesPDF.RIGHT);	
							pdfDoc.setCell( ""  ,"formas",ComunesPDF.CENTER);	
						}else if(totalDoctosUSD>0 && i ==1){
							pdfDoc.setCell("Total Dolares" ,"celda01",ComunesPDF.CENTER,2);	
							pdfDoc.setCell( Double.toString (totalDoctosMN)  ,"formas",ComunesPDF.CENTER,2);
							pdfDoc.setCell( ""  ,"formas",ComunesPDF.CENTER,5);
							pdfDoc.setCell("$"+ Comunes.formatoDecimal(totalMontoUSD,2)  ,"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell( ""  ,"formas",ComunesPDF.CENTER,2);	
							pdfDoc.setCell("$"+ Comunes.formatoDecimal(totalMontoDescuentoUSD,2)  ,"formas",ComunesPDF.RIGHT);
								pdfDoc.setCell( ""  ,"formas",ComunesPDF.CENTER,2);	
							pdfDoc.setCell( ""  ,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoValuadoUSD,2)  ,"formas",ComunesPDF.RIGHT);	
							pdfDoc.setCell( ""  ,"formas",ComunesPDF.CENTER,3);							
							pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoCreditoUSD,2)  ,"formas",ComunesPDF.RIGHT);
							pdfDoc.setCell( ""  ,"formas",ComunesPDF.CENTER,6);							
							pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoInteresUSD,2)  ,"formas",ComunesPDF.RIGHT);	
							pdfDoc.setCell("$"+Comunes.formatoDecimal(totalMontoCreditoUSD+totalMontoInteresUSD,2)  ,"formas",ComunesPDF.RIGHT);	
							pdfDoc.setCell( ""  ,"formas",ComunesPDF.CENTER);	
						}
					}			
				}else if("Totales".equals(tipo) )  {
					for(int i= 0; i<2; i++){						
						if(i ==0 ){							
							totales21t = new HashMap();		
							totales21t.put("MONEDA","MONEDA NACIONAL");
							totales21t.put("TOTAL_REGISTROS",Double.toString (totalDoctosMN));
							totales21t.put("TOTAL_MONTO",Double.toString (totalMontoMN));
							totales21t.put("MONTO_DESCUENTO",Double.toString (totalMontoDescuentoMN));
							totales21t.put("MONTO_VALUADO",Double.toString (totalMontoValuadoMN));
							totales21t.put("MONTO_CREDITO",Double.toString (totalMontoCreditoMN));
							totales21t.put("MONTO_INTERES",Double.toString (totalMontoInteresMN));	
							totales21t.put("MONTO_CAPITAL_INT",Double.toString (totalMontoCreditoMN+totalMontoInteresMN));
						}
						if(i ==1){	
							totales21t = new HashMap();		
							totales21t.put("MONEDA","MONEDA DOLAR");
							totales21t.put("TOTAL_REGISTROS",Double.toString (totalDoctosUSD));
							totales21t.put("TOTAL_MONTO",Double.toString (totalMontoUSD));
							totales21t.put("MONTO_DESCUENTO",Double.toString (totalMontoDescuentoUSD));
							totales21t.put("MONTO_VALUADO",Double.toString (totalMontoValuadoUSD));	
							totales21t.put("MONTO_CREDITO",Double.toString (totalMontoCreditoUSD));	
							totales21t.put("MONTO_INTERES",Double.toString (totalMontoInteresUSD));	
							totales21t.put("MONTO_CAPITAL_INT",Double.toString (totalMontoCreditoUSD+totalMontoInteresUSD));	
						}		
						totales21r.add(totales21t);
					}						
					nombreArchivo = "({\"success\": true, \"total\": \"" + totales21r.size() + "\", \"registros\": " + totales21r.toString()+"})";
				}		
				
			}
			if(noEstatus.equals("24") ) {
				totales21r = new JSONArray();
				if("PDF".equals(tipo) )  {
					for(int i= 0; i<2; i++){		
						if(totalDoctosMN>0  && i ==0){
								pdfDoc.setCell("Total M.N." ,"celda01",ComunesPDF.CENTER,2);							
								pdfDoc.setCell( Double.toString (totalDoctosMN) ,"formas",ComunesPDF.CENTER);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,5);
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoMN,2)    ,"formas",ComunesPDF.RIGHT);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,5);
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoValuadoMN,2) ,"formas",ComunesPDF.RIGHT);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,3);
						}else if( totalDoctosUSD>0  && i ==1){
								pdfDoc.setCell("Total Dolares" ,"celda01",ComunesPDF.CENTER,2);
								pdfDoc.setCell( Double.toString (totalDoctosUSD) ,"formas",ComunesPDF.CENTER);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,5);
								pdfDoc.setCell(   "$"+ Comunes.formatoDecimal(totalMontoUSD, 2)  ,"formas",ComunesPDF.RIGHT);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,5);
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoValuadoUSD,2)  ,"formas",ComunesPDF.RIGHT);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,3);
						}	
					}				
				}else if("Totales".equals(tipo) )  {
					for(int i= 0; i<2; i++){	
						if(i ==0 ){
							totales21t = new HashMap();		
							totales21t.put("MONEDA","MONEDA NACIONAL");
							totales21t.put("TOTAL_REGISTROS",Double.toString (totalDoctosMN));
							totales21t.put("TOTAL_MONTO",Double.toString (totalMontoMN));
							totales21t.put("MONTO_VALUADO",Double.toString (totalMontoValuadoMN));							
						}
						if(i ==1){						
							totales21t = new HashMap();		
							totales21t.put("MONEDA","MONEDA DOLAR");
							totales21t.put("TOTAL_REGISTROS",Double.toString (totalDoctosUSD));
							totales21t.put("TOTAL_MONTO",Double.toString (totalMontoUSD));
							totales21t.put("MONTO_VALUADO",Double.toString (totalMontoValuadoUSD));							
						}
						totales21r.add(totales21t);	
					}
					nombreArchivo = "({\"success\": true, \"total\": \"" + totales21r.size() + "\", \"registros\": " + totales21r.toString()+"})";
				}
			}
			
			if(noEstatus.equals("22") ) {
				totales21r = new JSONArray();
				if("PDF".equals(tipo) )  {
					for(int i= 0; i<2; i++){		
						if(totalDoctosMN>0  && i ==0){
								pdfDoc.setCell("Total M.N." ,"celda01",ComunesPDF.CENTER,2);							
								pdfDoc.setCell( Double.toString (totalDoctosMN) ,"formas",ComunesPDF.CENTER);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,5);
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoMN,2)    ,"formas",ComunesPDF.RIGHT);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,5);
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoValuadoMN,2) ,"formas",ComunesPDF.RIGHT);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,3);
						}else if( totalDoctosUSD>0  && i ==1){
								pdfDoc.setCell("Total Dolares" ,"celda01",ComunesPDF.CENTER,2);
								pdfDoc.setCell( Double.toString (totalDoctosUSD) ,"formas",ComunesPDF.CENTER);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,5);
								pdfDoc.setCell(   "$"+ Comunes.formatoDecimal(totalMontoUSD, 2)  ,"formas",ComunesPDF.RIGHT);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,5);
								pdfDoc.setCell( "$"+ Comunes.formatoDecimal(totalMontoValuadoUSD,2)  ,"formas",ComunesPDF.RIGHT);
								pdfDoc.setCell(" " ,"formas",ComunesPDF.CENTER,3);
						}	
					}
				}else if("Totales".equals(tipo) )  {
					for(int i= 0; i<2; i++){	
						if( i ==0 ){
							totales21t = new HashMap();		
							totales21t.put("MONEDA","MONEDA NACIONAL");
							totales21t.put("TOTAL_REGISTROS",Double.toString (totalDoctosMN));
							totales21t.put("TOTAL_MONTO",Double.toString (totalMontoMN));
							totales21t.put("MONTO_VALUADO",Double.toString (totalMontoValuadoMN));							
						}
						if( i ==1){						
							totales21t = new HashMap();		
							totales21t.put("MONEDA","MONEDA DOLAR");
							totales21t.put("TOTAL_REGISTROS",Double.toString (totalDoctosUSD));
							totales21t.put("TOTAL_MONTO",Double.toString (totalMontoUSD));
							totales21t.put("MONTO_VALUADO",Double.toString (totalMontoValuadoUSD));							
						}
						totales21r.add(totales21t);	
					}
					nombreArchivo = "({\"success\": true, \"total\": \"" + totales21r.size() + "\", \"registros\": " + totales21r.toString()+"})";
				}
			}
						
			if ("PDF".equals(tipo)) {
					pdfDoc.addTable();
					pdfDoc.endDocument();
			}		
			if ("CSV".equals(tipo)) {
				buffer.close();
			}		
		
		
		} catch(Exception e) {
			throw new AppException("Error al generar el archivo", e);
				
		} finally {
			try {
				rs.close();
			} catch(Exception e) {
			log.debug(" Error al generar el archivo "+ e);
			}
		}
			




			return nombreArchivo;
		}

	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}

	
	public List getConditions() {
		return conditions;
	}

	public String getNoEstatus() {
		return noEstatus;
	}

	public void setNoEstatus(String noEstatus) {
		this.noEstatus = noEstatus;
	}

	public String getNoEpo() {
		return noEpo;
	}

	public void setNoEpo(String noEpo) {
		this.noEpo = noEpo;
	}

	public String getTiposCredito() {
		return tiposCredito;
	}

	public void setTiposCredito(String tiposCredito) {
		this.tiposCredito = tiposCredito;
	}



  
}