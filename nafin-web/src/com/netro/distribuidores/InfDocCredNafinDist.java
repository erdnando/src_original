package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.IQueryGeneratorThreadRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class InfDocCredNafinDist  implements IQueryGenerator, IQueryGeneratorThreadRegExtJS{
	public InfDocCredNafinDist(){}


	public String getAggregateCalculationQuery(HttpServletRequest request) {
		StringBuffer	qrySentencia	= new StringBuffer();
		StringBuffer	qryDM			= new StringBuffer();
		StringBuffer	qryCCC			= new StringBuffer();
		StringBuffer	condicion		= new StringBuffer();
		String			condicionDstoA	= "";
		String			tablasDstoA		= "";
		String	fechaHoy			= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String	ic_epo				=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String	ic_pyme				=	(request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String	ic_if				=	(request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String	ic_estatus			=	(request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto");
		String 	ig_numero_docto		=	(request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String 	fecha_seleccion_de	=	(request.getParameter("fecha_seleccion_de")==null)?"":request.getParameter("fecha_seleccion_de");
		String 	fecha_seleccion_a	=	(request.getParameter("fecha_seleccion_a")==null)?"":request.getParameter("fecha_seleccion_a");
		String 	cc_acuse			=	(request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String 	monto_credito_de	=	(request.getParameter("monto_credito_de")==null)?"":request.getParameter("monto_credito_de");
		String 	monto_credito_a		=	(request.getParameter("monto_credito_a")==null)?"":request.getParameter("monto_credito_a");
		String 	fecha_emision_de	=	(request.getParameter("fecha_emision_de")==null)?"":request.getParameter("fecha_emision_de");
		String 	fecha_emision_a		=	(request.getParameter("fecha_emision_a")==null)?"":request.getParameter("fecha_emision_a");
		String 	fecha_vto_de		=	(request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String 	fecha_vto_a			=	(request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String 	fecha_vto_credito_de=	(request.getParameter("fecha_vto_credito_de")==null)?"":request.getParameter("fecha_vto_credito_de");
		String 	fecha_vto_credito_a =	(request.getParameter("fecha_vto_credito_a")==null)?"":request.getParameter("fecha_vto_credito_a");
		String 	ic_tipo_cobro_int	=	(request.getParameter("ic_tipo_cobro_int")==null)?"":request.getParameter("ic_tipo_cobro_int");
		String	ic_moneda			=	(request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String 	fn_monto_de			=	(request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String 	fn_monto_a			=	(request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String 	monto_con_descuento	=	(request.getParameter("monto_con_descuento")==null)?"":"checked";
		String 	solo_cambio				=	(request.getParameter("solo_cambio")==null)?"":request.getParameter("solo_cambio");		
		String	modo_plazo			=	(request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
		String	tipo_credito		=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String	ic_documento		=	(request.getParameter("ic_documento")==null)?"":request.getParameter("ic_documento");
		String 	fecha_publicacion_de=	(request.getParameter("fecha_publicacion_de")==null)?fechaHoy:request.getParameter("fecha_publicacion_de");
		String 	fecha_publicacion_a	=	(request.getParameter("fecha_publicacion_a")==null)?fechaHoy:request.getParameter("fecha_publicacion_a");
		String 	chk_proc_desc		=	(request.getParameter("chk_proc_desc")==null)?"":request.getParameter("chk_proc_desc");
		String 	tipoSolic			= "";
		String	ic_estatus_docto	= "";
		try {		   

		
			if(!"".equals(ic_estatus)){
				ic_estatus_docto = ic_estatus.substring(1,ic_estatus.length());
				tipoSolic		 = ic_estatus.substring(0,1);
			}
		
			if(!"".equals(ic_epo)&&ic_epo!=null)
				condicion.append(" AND D.IC_EPO = "+ic_epo);
			if(!"".equals(ic_pyme)&&ic_pyme!=null)
				condicion.append(" AND D.IC_PYME = "+ic_pyme);
			if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
				condicion.append(" and D.ig_numero_docto = '"+ig_numero_docto+"'");
			if(!"".equals(cc_acuse)&& cc_acuse!=null)
				condicion.append(" and D.cc_acuse = '"+cc_acuse+"'");
			if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null)
				condicion.append(" and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date('"+fecha_vto_credito_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_credito_a+"','dd/mm/yyyy'))");
			if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null)
				condicion.append(" AND LC.ic_tipo_cobro_interes = "+ic_tipo_cobro_int);
			if(!"".equals(ic_moneda)&&ic_moneda!=null)
				condicion.append(" and M.ic_moneda = "+ic_moneda);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
				condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
				condicion.append(" and D.fn_monto between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(solo_cambio)&& solo_cambio!=null)
				condicion.append(" and D.ic_documento in (select ic_documento from dis_cambio_estatus)");
			if(!"".equals(modo_plazo)&& modo_plazo!=null)
				condicion.append(" and TF.ic_tipo_financiamiento = "+modo_plazo);
			if(!"".equals(ic_documento)&&ic_documento!=null)
				condicion.append(" AND D.IC_DOCUMENTO = "+ic_documento);
			if(!"".equals(ic_if)&&ic_if!=null)
				condicion.append(" AND LC.IC_IF = "+ic_if);
			if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
				condicion.append(" and trunc(D.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))");
			if("2".equals(ic_estatus_docto)||"3".equals(ic_estatus_docto)||"5".equals(ic_estatus_docto)||"9".equals(ic_estatus_docto)||"11".equals(ic_estatus_docto)||"20".equals(ic_estatus_docto)||"1".equals(ic_estatus_docto))
				condicion.append(" AND D.IC_ESTATUS_DOCTO ="+ic_estatus_docto);
			if("S".equals(chk_proc_desc)) {
				ic_estatus_docto = "3";
				condicion.append(" AND D.IC_ESTATUS_DOCTO ="+ic_estatus_docto);
				condicionDstoA	= 
					" AND D.ic_documento = ds.ic_documento "+
					" AND SUBSTR (ds.cc_acuse, 1, 2) = 'D5' ";
				tablasDstoA		= " ,dis_docto_seleccionado ds ";

			}
			if("".equals(ic_estatus_docto))
				condicion.append(" and D.ic_estatus_docto in(1,2,3,5,9,11 )");
			if(!"".equals(ic_estatus_docto)&&!"2".equals(ic_estatus_docto)&&!"3".equals(ic_estatus_docto)&&!"5".equals(ic_estatus_docto)&&!"9".equals(ic_estatus_docto)&&!"11".equals(ic_estatus_docto)&&!"20".equals(ic_estatus_docto)&&!"1".equals(ic_estatus_docto))	
				condicion.append(" and D.ic_documento is null ");
			if("C".equals(tipoSolic)||(!"".equals(monto_credito_de)&& monto_credito_de!=null&&!"".equals(monto_credito_a)&& monto_credito_a!=null))
				condicion.append(" and D.ic_documento is null ");



			if(!fecha_seleccion_de.equals("") &&  !fecha_seleccion_a.equals(""))  {
				tablasDstoA = ", DIS_SOLICITUD S";
				condicionDstoA = "AND d.ic_documento = s.ic_documento";			
			
				condicion.append(" AND  S.DF_OPERACION >= to_date('"+fecha_seleccion_de+"', 'dd/mm/yyyy')"  +
									  " AND  S.DF_OPERACION <= to_date('"+fecha_seleccion_a+"', 'dd/mm/yyyy')  ") ;
			}
			
		   if(!"".equals(ic_tipo_Pago)) {
				condicion.append("AND d.ig_tipo_pago ="+ic_tipo_Pago);   
		   }
			
			qryDM.append(
				"    SELECT /*+ use_nl(d,lc,m,tf,e)"   +
				"       index(d in_dis_documento_03_nuk)"   +
				"       index(tf cp_comcat_tipo_finan_pk)"   +
				"       index(e cp_comcat_epo_pk) */"   +
				"       m.ic_moneda"   +
				" 	  ,m.cd_nombre"   +
				" 	  ,d.fn_monto"   +
				"    FROM DIS_DOCUMENTO D"   +
				"    ,DIS_LINEA_CREDITO_DM LC "+tablasDstoA+" "+
				"    ,COMCAT_MONEDA M"   +
				"    ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
				"    ,COMCAT_EPO E"   +			
				"    WHERE D.IC_EPO = E.IC_EPO " +condicionDstoA+" "+
				"    AND D.IC_MONEDA = M.IC_MONEDA"   +
				"    AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM"   +
				"    AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO" +
				"    AND E.CS_HABILITADO = 'S' " +
					condicion.toString()
				);
					
			qryCCC.append(
				"    SELECT /*+ use_nl(d,lc,m,tf,e)"   +
				"       index(d in_dis_documento_03_nuk)"   +
				"       index(tf cp_comcat_tipo_finan_pk)"   +
				"       index(e cp_comcat_epo_pk) */"   +
				"       m.ic_moneda"   +
				" 	  ,m.cd_nombre"   +
				" 	  ,d.fn_monto"   +
				"    FROM DIS_DOCUMENTO D"   +
				"    ,COM_LINEA_CREDITO LC "+
				"    ,COMCAT_MONEDA M"   +
				"    ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
				"    ,COMCAT_EPO E"  +tablasDstoA+" "+		
				"    WHERE D.IC_EPO = E.IC_EPO  " +condicionDstoA+" "+
				"    AND D.IC_MONEDA = M.IC_MONEDA"   +
				"    AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO (+)"   +
				"    AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO " +
				"    AND D.IC_LINEA_CREDITO_DM IS NULL " +
				"    AND E.CS_HABILITADO = 'S' " +
					condicion.toString());

			String qryAux = "";	
			if("D".equals(tipo_credito))
				qryAux = qryDM.toString();
			else if("C".equals(tipo_credito))
				qryAux = qryCCC.toString();
			else
				qryAux = qryDM.toString()+ " UNION ALL "+qryCCC.toString();

			qrySentencia.append(
				"SELECT ic_moneda, cd_nombre, COUNT (1), SUM (fn_monto), 'InfDocCredNafinDist::getAggregateCalculationQuery'"+
				"  FROM ("+qryAux.toString()+")"+
				"GROUP BY  ic_moneda,cd_nombre ORDER BY ic_moneda ");

		}catch(Exception e){
			System.out.println("InfLinCredNafinDist::getAggregateCalculationQuery "+e);
		}
		return qrySentencia.toString();
	}

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids) {
		int i=0;
		String	tipo_credito			=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		StringBuffer qrySentencia	= new StringBuffer();
		StringBuffer qryDM			= new StringBuffer();
		StringBuffer qryCCC			= new StringBuffer();
		StringBuffer condicion		= new StringBuffer();
		
		condicion.append(" AND d.ic_documento in (");

		i=0;
		for (Iterator it = ids.iterator(); it.hasNext();i++){
        	if(i>0){
				condicion.append(",");
			}
			condicion.append(" "+it.next().toString()+" ");
		}
		condicion.append(") ");

			qryDM.append(
					"  SELECT /*+ use_nl(d,lc,py,m,pe,pn,tc,tf,ed,e)"   +
					"      index(d cp_dis_documento_pk)"   +
					"     index(tf cp_comcat_tipo_finan_pk)"   +
					"     index(e cp_comcat_epo_pk)"   +
					"     index(ed cp_comcat_estatus_docto_pk)*/"   +
					"     PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"   +
					"  	,D.IG_NUMERO_DOCTO"   +
					"  	,D.CC_ACUSE"   +
					" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"   +
					" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"   +
					" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"   +
					" 	,D.IG_PLAZO_DOCTO"   +
					" 	,M.CD_NOMBRE AS MONEDA"   +
					" 	,D.FN_MONTO"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') AS TIPO_CONVERSION"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
					" 	,NVL(D.IG_PLAZO_DESCUENTO,'') AS IG_PLAZO_DESCUENTO"   +
					" 	,DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) AS FN_PORC_DESCUENTO"   +
					" 	,TF.CD_DESCRIPCION AS MODO_PLAZO"   +
					" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
					" 	,M.ic_moneda"   +
					" 	,D.ic_documento"   +
					" 	,LC.ic_moneda AS MONEDA_LINEA"   +
					" 	,E.cg_razon_social AS epo"   +
					" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
					" , DECODE (d.ig_tipo_pago, '1', 'Financiamiento con intereses', '2', 'Meses sin Intereses' , '3', 'Tarjeta de Cr�dito') AS TIPOPAGO "+ //F09-2015
	                  
							
					"  FROM DIS_DOCUMENTO D"   +
					"  ,DIS_LINEA_CREDITO_DM LC"   +
					"  ,COMCAT_PYME PY"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  ,COMCAT_ESTATUS_DOCTO ED"   +
					"  ,COMCAT_EPO E"   +
					"  WHERE D.IC_PYME = PY.IC_PYME"   +
					"  AND D.IC_EPO = E.IC_EPO"   +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM"   +
					"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
					"  AND D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND TC.DC_FECHA IN(SELECT /*+ index(tc2 in_com_tipo_cambio_01_nuk)*/ MAX(tc2.DC_FECHA) FROM COM_TIPO_CAMBIO tc2 WHERE tc2.IC_MONEDA = M.IC_MONEDA)"   +
					"  AND M.ic_moneda = TC.ic_moneda"   +
					"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"  AND PN.IC_PRODUCTO_NAFIN = 4"  +
					condicion.toString()
				);
					
			qryCCC.append(
					"  SELECT /*+ use_nl(d,lc,py,m,pe,pn,tc,tf,ed,e)"   +
					"     index(d cp_dis_documento_pk)"   +
					"     index(tf cp_comcat_tipo_finan_pk)"   +
					"     index(e cp_comcat_epo_pk)"   +
					"     index(ed cp_comcat_estatus_docto_pk)*/ "   +
					"  	PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"   +
					"  	,D.IG_NUMERO_DOCTO"   +
					"  	,D.CC_ACUSE"   +
					" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"   +
					" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"   +
					" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"   +
					" 	,D.IG_PLAZO_DOCTO"   +
					" 	,M.CD_NOMBRE AS MONEDA"   +
					" 	,D.FN_MONTO"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') AS TIPO_CONVERSION"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
					" 	,NVL(D.IG_PLAZO_DESCUENTO,'') AS IG_PLAZO_DESCUENTO"   +
					" 	,DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) AS FN_PORC_DESCUENTO"   +
					" 	,TF.CD_DESCRIPCION AS MODO_PLAZO"   +
					" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
					" 	,M.ic_moneda"   +
					" 	,D.ic_documento"   +
					" 	,LC.ic_moneda AS MONEDA_LINEA"   +
					" 	,E.cg_razon_social AS epo"   +
					" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
					" , DECODE (d.ig_tipo_pago, '1', 'Financiamiento con intereses', '2', 'Meses sin Intereses' , '3', 'Tarjeta de Cr�dito') AS TIPOPAGO "+ //F09-2015
	                  
					"  FROM DIS_DOCUMENTO D"   +
					"  ,COM_LINEA_CREDITO LC"   +
					"  ,COMCAT_PYME PY"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  ,COMCAT_ESTATUS_DOCTO ED"   +
					"  ,COMCAT_EPO E"   +
					"  WHERE D.IC_PYME = PY.IC_PYME"   +
					"  AND D.IC_EPO = E.IC_EPO"   +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
					"  AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO (+)"   +
					"  AND D.IC_EPO = PE.IC_EPO"   +
					"  AND TC.DC_FECHA IN(SELECT /*+ index(tc2 in_com_tipo_cambio_01_nuk)*/ MAX(tc2.DC_FECHA) FROM COM_TIPO_CAMBIO tc2 WHERE tc2.IC_MONEDA = M.IC_MONEDA)"   +
					"  AND M.ic_moneda = TC.ic_moneda"   +
					"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"  AND PN.IC_PRODUCTO_NAFIN = 4 "  +
					"  AND D.IC_LINEA_CREDITO_DM IS NULL " +
					condicion.toString());

			if("D".equals(tipo_credito))
				qrySentencia = qryDM;
			else if("C".equals(tipo_credito))
				qrySentencia = qryCCC;
			else
				qrySentencia.append(qryDM.toString() +" UNION ALL " +qryCCC.toString());
					
		System.out.println("el query queda de la siguiente manera "+qrySentencia.toString());
		return qrySentencia.toString();
	}
	
	public String getDocumentQuery(HttpServletRequest request) {
		StringBuffer qrySentencia	= new StringBuffer();
		StringBuffer qryDM			= new StringBuffer();
		StringBuffer qryCCC			= new StringBuffer();
		StringBuffer condicion		= new StringBuffer();
		String			condicionDstoA	= "";
		String			tablasDstoA		= "";
		String	fechaHoy					= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String	ic_epo					=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String	ic_pyme					=	(request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String	ic_if						=	(request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String	ic_estatus				=	(request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto");
		String 	ig_numero_docto		=	(request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String 	fecha_seleccion_de	=	(request.getParameter("fecha_seleccion_de")==null)?"":request.getParameter("fecha_seleccion_de");
		String 	fecha_seleccion_a		=	(request.getParameter("fecha_seleccion_a")==null)?"":request.getParameter("fecha_seleccion_a");
		String 	cc_acuse					=	(request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String 	monto_credito_de		=	(request.getParameter("monto_credito_de")==null)?"":request.getParameter("monto_credito_de");
		String 	monto_credito_a		=	(request.getParameter("monto_credito_a")==null)?"":request.getParameter("monto_credito_a");
		String 	fecha_emision_de		=	(request.getParameter("fecha_emision_de")==null)?"":request.getParameter("fecha_emision_de");
		String 	fecha_emision_a		=	(request.getParameter("fecha_emision_a")==null)?"":request.getParameter("fecha_emision_a");
		String 	fecha_vto_de			=	(request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String 	fecha_vto_a				=	(request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String 	fecha_vto_credito_de	=	(request.getParameter("fecha_vto_credito_de")==null)?"":request.getParameter("fecha_vto_credito_de");
		String 	fecha_vto_credito_a 	=	(request.getParameter("fecha_vto_credito_a")==null)?"":request.getParameter("fecha_vto_credito_a");
		String 	ic_tipo_cobro_int		=	(request.getParameter("ic_tipo_cobro_int")==null)?"":request.getParameter("ic_tipo_cobro_int");
		String	ic_moneda				=	(request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String 	fn_monto_de				=	(request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String 	fn_monto_a				=	(request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String 	monto_con_descuento	=	(request.getParameter("monto_con_descuento")==null)?"":"checked";
		String 	solo_cambio				=	(request.getParameter("solo_cambio")==null)?"":request.getParameter("solo_cambio");		
		String	modo_plazo				=	(request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
		String	tipo_credito			=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String	ic_documento			=	(request.getParameter("ic_documento")==null)?"":request.getParameter("ic_documento");
		String 	fecha_publicacion_de	=	(request.getParameter("fecha_publicacion_de")==null)?fechaHoy:request.getParameter("fecha_publicacion_de");
		String 	fecha_publicacion_a	=	(request.getParameter("fecha_publicacion_a")==null)?fechaHoy:request.getParameter("fecha_publicacion_a");
		String 	chk_proc_desc		=	(request.getParameter("chk_proc_desc")==null)?"":request.getParameter("chk_proc_desc");
		String 	tipoSolic				= "";
		String	ic_estatus_docto		= "";
		try {		
			
			if(!"".equals(ic_estatus)){
				ic_estatus_docto = ic_estatus.substring(1,ic_estatus.length());
				tipoSolic		 = ic_estatus.substring(0,1);
			}			
	
			if(!"".equals(ic_epo)&&ic_epo!=null)
				condicion.append(" AND D.IC_EPO = "+ic_epo);
			if(!"".equals(ic_pyme)&&ic_pyme!=null)
				condicion.append(" AND D.IC_PYME = "+ic_pyme);
			if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
				condicion.append(" and D.ig_numero_docto = '"+ig_numero_docto+"'");
			if(!"".equals(cc_acuse)&& cc_acuse!=null)
				condicion.append(" and D.cc_acuse = '"+cc_acuse+"'");
			if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null)
				condicion.append(" and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date('"+fecha_vto_credito_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_credito_a+"','dd/mm/yyyy'))");
			if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null)
				condicion.append(" AND LC.ic_tipo_cobro_interes = "+ic_tipo_cobro_int);
			if(!"".equals(ic_moneda)&&ic_moneda!=null)
				condicion.append(" and M.ic_moneda = "+ic_moneda);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
				condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
				condicion.append(" and D.fn_monto between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(solo_cambio)&& solo_cambio!=null)
				condicion.append(" and D.ic_documento in (select ic_documento from dis_cambio_estatus)");
			if(!"".equals(modo_plazo)&& modo_plazo!=null)
				condicion.append(" and TF.ic_tipo_financiamiento = "+modo_plazo);
			if(!"".equals(ic_documento)&&ic_documento!=null)
				condicion.append(" AND D.IC_DOCUMENTO = "+ic_documento);
			if(!"".equals(ic_if)&&ic_if!=null)
				condicion.append(" AND LC.IC_IF = "+ic_if);
			if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
				condicion.append(" and trunc(D.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))");
			if("2".equals(ic_estatus_docto)||"3".equals(ic_estatus_docto)||"5".equals(ic_estatus_docto)||"9".equals(ic_estatus_docto)||"11".equals(ic_estatus_docto)||"20".equals(ic_estatus_docto)||"1".equals(ic_estatus_docto))
				condicion.append(" AND D.IC_ESTATUS_DOCTO ="+ic_estatus_docto);
			if("S".equals(chk_proc_desc)) {
				ic_estatus_docto = "3";
				condicion.append(" AND D.IC_ESTATUS_DOCTO ="+ic_estatus_docto);
				condicionDstoA	= 
					" AND D.ic_documento = ds.ic_documento "+
					" AND SUBSTR (ds.cc_acuse, 1, 2) = 'D5' ";
				tablasDstoA		= " ,dis_docto_seleccionado ds ";

			}
			if("".equals(ic_estatus_docto))
				condicion.append(" and D.ic_estatus_docto in(1,2,3,5,9,11)");
			if(!"".equals(ic_estatus_docto)&&!"2".equals(ic_estatus_docto)&&!"3".equals(ic_estatus_docto)&&!"5".equals(ic_estatus_docto)&&!"9".equals(ic_estatus_docto)&&!"11".equals(ic_estatus_docto)&&!"20".equals(ic_estatus_docto)&&!"1".equals(ic_estatus_docto))	
				condicion.append(" and D.ic_documento is null ");
			if("C".equals(tipoSolic)||(!"".equals(monto_credito_de)&& monto_credito_de!=null&&!"".equals(monto_credito_a)&& monto_credito_a!=null))
				condicion.append(" and D.ic_documento is null ");

			if(!fecha_seleccion_de.equals("") &&  !fecha_seleccion_a.equals(""))  {
				tablasDstoA = ", DIS_SOLICITUD S";
				condicionDstoA = "AND d.ic_documento = s.ic_documento";			
			
				condicion.append(" AND  S.DF_OPERACION >= to_date('"+fecha_seleccion_de+"', 'dd/mm/yyyy')"  +
									  " AND  S.DF_OPERACION <= to_date('"+fecha_seleccion_a+"', 'dd/mm/yyyy')  ") ;
			}
			
		   if(!"".equals(ic_tipo_Pago)) {
				condicion.append("AND d.ig_tipo_pago ="+ic_tipo_Pago);   
		   }
			
			qryDM.append(
				"   SELECT /*+ use_nl(d,lc,m,tf,e)"   +
				"      index(d in_dis_documento_03_nuk)"   +
				"      index(tf cp_comcat_tipo_finan_pk)"   +
				"      index(e cp_comcat_epo_pk) */"   +
				"      D.ic_documento"   +
				"   FROM DIS_DOCUMENTO D"   +
				"   ,DIS_LINEA_CREDITO_DM LC "+tablasDstoA+" "+
				"   ,COMCAT_MONEDA M"   +
				"   ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
				"   ,COMCAT_EPO E"   +
				"   WHERE D.IC_EPO = E.IC_EPO " +condicionDstoA+" "+
				"   AND D.IC_MONEDA = M.IC_MONEDA"   +
				"   AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM"   +
				"   AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO" +
				"   AND E.CS_HABILITADO = 'S' " +
					condicion.toString()
				);
					
			qryCCC.append(
					"   SELECT /*+ use_nl(d,lc,m,tf,e)"   +
					"      index(d in_dis_documento_03_nuk)"   +
					"      index(tf cp_comcat_tipo_finan_pk)"   +
					"      index(e cp_comcat_epo_pk) */"   +
					"  	D.ic_documento"   +
					"   FROM DIS_DOCUMENTO D"   +
					"   ,COM_LINEA_CREDITO LC "+
					"   ,COMCAT_MONEDA M"   +
					"   ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"   ,COMCAT_EPO E  "+tablasDstoA+" "+
					"   WHERE D.IC_EPO = E.IC_EPO  " +condicionDstoA+" "+
					"   AND D.IC_MONEDA = M.IC_MONEDA"   +
					"   AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO (+)"   +
					"   AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
					"   AND D.IC_LINEA_CREDITO_DM IS NULL " +
					"   AND E.CS_HABILITADO = 'S' " +
					condicion.toString());

			if("D".equals(tipo_credito))
				qrySentencia = qryDM;
			else if("C".equals(tipo_credito))
				qrySentencia = qryCCC;
			else
				qrySentencia.append(qryDM.toString() +" UNION ALL " +qryCCC.toString());

			System.out.println("EL query de la llave primaria: "+qrySentencia.toString());
		}catch(Exception e){
			System.out.println("InfLinCredNafinDist::getDocumentQueryException "+e);
		}
		return qrySentencia.toString();
	}
	
	public String getDocumentQueryFile(HttpServletRequest request) {
		StringBuffer qrySentencia	= new StringBuffer();
		StringBuffer qryDM			= new StringBuffer();
		StringBuffer qryCCC			= new StringBuffer();
		StringBuffer condicion		= new StringBuffer();
		String			condicionDstoA	= "";
		String			tablasDstoA		= "";
		String	fechaHoy					= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String	ic_epo					=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String	ic_pyme					=	(request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String	ic_if						=	(request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String	ic_estatus				=	(request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto");
		String 	ig_numero_docto		=	(request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String 	fecha_seleccion_de	=	(request.getParameter("fecha_seleccion_de")==null)?"":request.getParameter("fecha_seleccion_de");
		String 	fecha_seleccion_a		=	(request.getParameter("fecha_seleccion_a")==null)?"":request.getParameter("fecha_seleccion_a");
		String 	cc_acuse					=	(request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String 	monto_credito_de		=	(request.getParameter("monto_credito_de")==null)?"":request.getParameter("monto_credito_de");
		String 	monto_credito_a		=	(request.getParameter("monto_credito_a")==null)?"":request.getParameter("monto_credito_a");
		String 	fecha_emision_de		=	(request.getParameter("fecha_emision_de")==null)?"":request.getParameter("fecha_emision_de");
		String 	fecha_emision_a		=	(request.getParameter("fecha_emision_a")==null)?"":request.getParameter("fecha_emision_a");
		String 	fecha_vto_de			=	(request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String 	fecha_vto_a				=	(request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String 	fecha_vto_credito_de	=	(request.getParameter("fecha_vto_credito_de")==null)?"":request.getParameter("fecha_vto_credito_de");
		String 	fecha_vto_credito_a 	=	(request.getParameter("fecha_vto_credito_a")==null)?"":request.getParameter("fecha_vto_credito_a");
		String 	ic_tipo_cobro_int		=	(request.getParameter("ic_tipo_cobro_int")==null)?"":request.getParameter("ic_tipo_cobro_int");
		String	ic_moneda				=	(request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String 	fn_monto_de				=	(request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String 	fn_monto_a				=	(request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String 	monto_con_descuento	=	(request.getParameter("monto_con_descuento")==null)?"":"checked";
		String 	solo_cambio				=	(request.getParameter("solo_cambio")==null)?"":request.getParameter("solo_cambio");		
		String	modo_plazo				=	(request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
		String	tipo_credito			=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String	ic_documento			=	(request.getParameter("ic_documento")==null)?"":request.getParameter("ic_documento");
		String 	fecha_publicacion_de	=	(request.getParameter("fecha_publicacion_de")==null)?fechaHoy:request.getParameter("fecha_publicacion_de");
		String 	fecha_publicacion_a	=	(request.getParameter("fecha_publicacion_a")==null)?fechaHoy:request.getParameter("fecha_publicacion_a");
		String 	chk_proc_desc		=	(request.getParameter("chk_proc_desc")==null)?"":request.getParameter("chk_proc_desc");
		String 	tipoSolic				= "";
		String	ic_estatus_docto		= "";
		try {		

			if(!"".equals(ic_estatus)){
				ic_estatus_docto = ic_estatus.substring(1,ic_estatus.length());
				tipoSolic		 = ic_estatus.substring(0,1);
			}
	
			if(!"".equals(ic_epo)&&ic_epo!=null)
				condicion.append(" AND D.IC_EPO = "+ic_epo);
			if(!"".equals(ic_pyme)&&ic_pyme!=null)
				condicion.append(" AND D.IC_PYME = "+ic_pyme);
			if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
				condicion.append(" and D.ig_numero_docto = '"+ig_numero_docto+"'");
			if(!"".equals(cc_acuse)&& cc_acuse!=null)
				condicion.append(" and D.cc_acuse = '"+cc_acuse+"'");
			if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null)
				condicion.append(" and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date('"+fecha_vto_credito_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_credito_a+"','dd/mm/yyyy'))");
			if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null)
				condicion.append(" AND LC.ic_tipo_cobro_interes = "+ic_tipo_cobro_int);
			if(!"".equals(ic_moneda)&&ic_moneda!=null)
				condicion.append(" and M.ic_moneda = "+ic_moneda);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
				condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
				condicion.append(" and D.fn_monto between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(solo_cambio)&& solo_cambio!=null)
				condicion.append(" and D.ic_documento in (select ic_documento from dis_cambio_estatus)");
			if(!"".equals(modo_plazo)&& modo_plazo!=null)
				condicion.append(" and TF.ic_tipo_financiamiento = "+modo_plazo);
			if(!"".equals(ic_documento)&&ic_documento!=null)
				condicion.append(" AND D.IC_DOCUMENTO = "+ic_documento);
			if(!"".equals(ic_if)&&ic_if!=null)
				condicion.append(" AND LC.IC_IF = "+ic_if);
			if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
				condicion.append(" and trunc(D.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))");
			if("2".equals(ic_estatus_docto)||"3".equals(ic_estatus_docto)||"5".equals(ic_estatus_docto)||"9".equals(ic_estatus_docto)||"11".equals(ic_estatus_docto)||"20".equals(ic_estatus_docto)||"1".equals(ic_estatus_docto))
				condicion.append(" AND D.IC_ESTATUS_DOCTO ="+ic_estatus_docto);
			if("S".equals(chk_proc_desc)) {
				ic_estatus_docto = "3";
				condicion.append(" AND D.IC_ESTATUS_DOCTO ="+ic_estatus_docto);
				condicionDstoA	= 
					" AND D.ic_documento = ds.ic_documento "+
					" AND SUBSTR (ds.cc_acuse, 1, 2) = 'D5' ";
				tablasDstoA		= " ,dis_docto_seleccionado ds ";

			}
			if("".equals(ic_estatus_docto))
				condicion.append(" and D.ic_estatus_docto in(1,2,3,5,9,11)");
			if(!"".equals(ic_estatus_docto)&&!"2".equals(ic_estatus_docto)&&!"3".equals(ic_estatus_docto)&&!"5".equals(ic_estatus_docto)&&!"9".equals(ic_estatus_docto)&&!"11".equals(ic_estatus_docto)&&!"20".equals(ic_estatus_docto)&&!"1".equals(ic_estatus_docto))	
				condicion.append(" and D.ic_documento is null ");
			if("C".equals(tipoSolic)||(!"".equals(monto_credito_de)&& monto_credito_de!=null&&!"".equals(monto_credito_a)&& monto_credito_a!=null))
				condicion.append(" and D.ic_documento is null ");

			if(!fecha_seleccion_de.equals("") &&  !fecha_seleccion_a.equals(""))  {
				tablasDstoA = ", DIS_SOLICITUD S";
				condicionDstoA = "AND d.ic_documento = s.ic_documento";			
			
				condicion.append(" AND  S.DF_OPERACION >= to_date('"+fecha_seleccion_de+"', 'dd/mm/yyyy')"  +
									  " AND  S.DF_OPERACION <= to_date('"+fecha_seleccion_a+"', 'dd/mm/yyyy')  ") ;
			}
			
		   if(!"".equals(ic_tipo_Pago)) {
				condicion.append("AND d.ig_tipo_pago ="+ic_tipo_Pago);   
		   }
			
			qryDM.append(
					"  SELECT /*+ use_nl(d,lc,py,m,pe,pn,tc,tf,ed,e)"   +
					"     index(d in_dis_documento_03_nuk)"   +
					"     index(tf cp_comcat_tipo_finan_pk)"   +
					"     index(e cp_comcat_epo_pk)"   +
					"     index(ed cp_comcat_estatus_docto_pk)*/"   +
					"     PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"   +
					"  	,D.IG_NUMERO_DOCTO"   +
					"  	,D.CC_ACUSE"   +
					" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"   +
					" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"   +
					" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"   +
					" 	,D.IG_PLAZO_DOCTO"   +
					" 	,M.CD_NOMBRE AS MONEDA"   +
					" 	,D.FN_MONTO"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') AS TIPO_CONVERSION"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
					" 	,NVL(D.IG_PLAZO_DESCUENTO,'') AS IG_PLAZO_DESCUENTO"   +
					" 	,DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) AS FN_PORC_DESCUENTO"   +
					" 	,TF.CD_DESCRIPCION AS MODO_PLAZO"   +
					" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
					" 	,M.ic_moneda"   +
					" 	,D.ic_documento"   +
					" 	,LC.ic_moneda AS MONEDA_LINEA"   +
					" 	,E.cg_razon_social AS epo"   +
					" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
					", DECODE (d.ig_tipo_pago, '1', 'Financiamiento con intereses', '2', 'Meses sin Intereses','3', 'Tarjeta de Cr�dito' ) AS TIPOPAGO "+ //F09-2015
		          
					"  FROM DIS_DOCUMENTO D"   +
					"  ,DIS_LINEA_CREDITO_DM LC "+tablasDstoA+" "+
					"  ,COMCAT_PYME PY"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  ,COMCAT_ESTATUS_DOCTO ED"   +
					"  ,COMCAT_EPO E"   +
					"  WHERE D.IC_PYME = PY.IC_PYME"   +
					"  AND D.IC_EPO = E.IC_EPO " +condicionDstoA+" "+
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM"   +
					"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
					"  AND D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND TC.DC_FECHA IN(SELECT /*+ index(tc2 in_com_tipo_cambio_01_nuk)*/ MAX(tc2.DC_FECHA) FROM COM_TIPO_CAMBIO tc2 WHERE tc2.IC_MONEDA = M.IC_MONEDA)"   +
					"  AND M.ic_moneda = TC.ic_moneda"   +
					"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"  AND PN.IC_PRODUCTO_NAFIN = 4"  +
					"   AND E.CS_HABILITADO = 'S' " +
					condicion.toString()
				);
					
			qryCCC.append(
					"  SELECT /*+ use_nl(d,lc,py,m,pe,pn,tc,tf,ed,e)"   +
					"     index(d in_dis_documento_03_nuk)"   +
					"     index(tf cp_comcat_tipo_finan_pk)"   +
					"     index(e cp_comcat_epo_pk)"   +
					"     index(ed cp_comcat_estatus_docto_pk)*/ "   +
					"  	PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"   +
					"  	,D.IG_NUMERO_DOCTO"   +
					"  	,D.CC_ACUSE"   +
					" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"   +
					" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"   +
					" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"   +
					" 	,D.IG_PLAZO_DOCTO"   +
					" 	,M.CD_NOMBRE AS MONEDA"   +
					" 	,D.FN_MONTO"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') AS TIPO_CONVERSION"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
					" 	,NVL(D.IG_PLAZO_DESCUENTO,'') AS IG_PLAZO_DESCUENTO"   +
					" 	,DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) AS FN_PORC_DESCUENTO"   +
					" 	,TF.CD_DESCRIPCION AS MODO_PLAZO"   +
					" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
					" 	,M.ic_moneda"   +
					" 	,D.ic_documento"   +
					" 	,LC.ic_moneda AS MONEDA_LINEA"   +
					" 	,E.cg_razon_social AS epo"   +
					" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
					", DECODE (d.ig_tipo_pago, '1', 'Financiamiento con intereses', '2', 'Meses sin Intereses','3', 'Tarjeta de Cr�dito' ) AS TIPOPAGO "+ //F09-2015
		         
					"  FROM DIS_DOCUMENTO D"   +
					"  ,COM_LINEA_CREDITO LC "+
					"  ,COMCAT_PYME PY"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  ,COMCAT_ESTATUS_DOCTO ED"   +
					"  ,COMCAT_EPO E   "+tablasDstoA+" "+
					"  WHERE D.IC_PYME = PY.IC_PYME " +condicionDstoA+" "+
					"  AND D.IC_EPO = E.IC_EPO " +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
					"  AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO (+)"   +
					"  AND D.IC_EPO = PE.IC_EPO"   +
					"  AND TC.DC_FECHA IN(SELECT /*+ index(tc2 in_com_tipo_cambio_01_nuk)*/ MAX(tc2.DC_FECHA) FROM COM_TIPO_CAMBIO tc2 WHERE tc2.IC_MONEDA = M.IC_MONEDA)"   +
					"  AND M.ic_moneda = TC.ic_moneda"   +
					"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"  AND PN.IC_PRODUCTO_NAFIN = 4 "  +
					"  AND D.IC_LINEA_CREDITO_DM IS NULL " +
				"   AND E.CS_HABILITADO = 'S' " +
					condicion.toString());

			if("D".equals(tipo_credito))
				qrySentencia = qryDM;
			else if("C".equals(tipo_credito))
				qrySentencia = qryCCC;
			else
				qrySentencia.append(qryDM.toString() +" UNION ALL " +qryCCC.toString());
            
			System.out.println("getDocumentQueryFile qrySentencia: "+qrySentencia.toString());
		}catch(Exception e){
			System.out.println("InfLinCredNafinDist::getDocumentQueryFileException "+e);
		}
		return qrySentencia.toString();
	}



//***************** Fodea 011-2014 Migraci�n Nafin ******************
  
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");	
	private final static Log log = ServiceLocator.getInstance().getLog(InfDocCredNafinDist.class);
	private String ic_epo;
	private String ic_documento;
	private String ic_pyme;
	private String tipo_credito;
	private String ic_if;
	private String fecha_seleccion_de;
	private String fecha_seleccion_a;
	private String monto_credito_de;
	private String monto_credito_a;
	private String ig_numero_docto;
	private String fecha_vto_credito_de;
	private String fecha_vto_credito_a;
	private String fecha_vto_de;
	private String fecha_vto_a;
	private String ic_tipo_cobro_int;
	private String fecha_publicacion_de;
	private String fecha_publicacion_a;
	private String fecha_emision_de;
	private String fecha_emision_a;
	private String ic_moneda;
	private String cc_acuse;
	private String fn_monto_de;
	private String fn_monto_a;
	private String monto_con_descuento;
	private String chk_proc_desc;
	private String solo_cambio;
	private String modo_plazo;
	private String tipoConsulta;
	private String ic_estatus;
	private int countReg;

	
	StringBuffer  condicion 	= new StringBuffer();
	//StringBuffer  qryAux 	= new StringBuffer();
	//StringBuffer  qryCCC 	= new StringBuffer();
	//StringBuffer  qryDM 	= new StringBuffer();	
	String condicionDstoA ="", tablasDstoA ="", tipoSolic	= "", ic_estatus_docto =""; 
	private String ic_tipo_Pago;
	
	
  public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		StringBuffer  qryCCC    = new StringBuffer();
		StringBuffer  qryDM  = new StringBuffer();   
		conditions 		= new ArrayList();
		
		condicion 	= new StringBuffer();
	   String qryAux 	= "";
	   qryCCC 	= new StringBuffer();
	   qryDM 	= new StringBuffer();	
	  			
				
		if(!"".equals(ic_estatus)){
			ic_estatus_docto = ic_estatus.substring(1,ic_estatus.length());
			tipoSolic		 = ic_estatus.substring(0,1);
		}
			
			if(!"".equals(ic_epo)&&ic_epo!=null)
				condicion.append(" AND D.IC_EPO = "+ic_epo);
			if(!"".equals(ic_pyme)&&ic_pyme!=null)
				condicion.append(" AND D.IC_PYME = "+ic_pyme);
			if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
				condicion.append(" and D.ig_numero_docto = '"+ig_numero_docto+"'");
			if(!"".equals(cc_acuse)&& cc_acuse!=null)
				condicion.append(" and D.cc_acuse = '"+cc_acuse+"'");
			if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null)
				condicion.append(" and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date('"+fecha_vto_credito_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_credito_a+"','dd/mm/yyyy'))");
			if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null)
				condicion.append(" AND LC.ic_tipo_cobro_interes = "+ic_tipo_cobro_int);
			if(!"".equals(ic_moneda)&&ic_moneda!=null)
				condicion.append(" and M.ic_moneda = "+ic_moneda);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
				condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de.replaceAll(",","")+" and "+fn_monto_a.replaceAll(",",""));
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
				condicion.append(" and D.fn_monto between "+fn_monto_de.replaceAll(",","")+" and "+fn_monto_a.replaceAll(",",""));
			if(!"".equals(solo_cambio)&& solo_cambio!=null)
				condicion.append(" and D.ic_documento in (select ic_documento from dis_cambio_estatus)");
			if(!"".equals(modo_plazo)&& modo_plazo!=null)
				condicion.append(" and TF.ic_tipo_financiamiento = "+modo_plazo);
			if(!"".equals(ic_documento)&&ic_documento!=null)
				condicion.append(" AND D.IC_DOCUMENTO = "+ic_documento);
			if(!"".equals(ic_if)&&ic_if!=null)
				condicion.append(" AND (LC.IC_IF = "+ic_if+" or bins.ic_if = "+ic_if+" )");
			if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
				condicion.append(" and trunc(D.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))");
			if("2".equals(ic_estatus_docto)||"3".equals(ic_estatus_docto)||"5".equals(ic_estatus_docto)||"9".equals(ic_estatus_docto)||"11".equals(ic_estatus_docto)||"20".equals(ic_estatus_docto)||"1".equals(ic_estatus_docto)||"24".equals(ic_estatus_docto)||"32".equals(ic_estatus_docto)||"28".equals(ic_estatus_docto)||"30".equals(ic_estatus_docto))//MOD +(||"24".equals(ic_estatus_docto))
				condicion.append(" AND D.IC_ESTATUS_DOCTO ="+ic_estatus_docto);
			if("S".equals(chk_proc_desc)) {
				ic_estatus_docto = "3";
				condicion.append(" AND D.IC_ESTATUS_DOCTO ="+ic_estatus_docto);
				condicionDstoA	= 
						" AND D.ic_documento = ds.ic_documento "+
						" AND SUBSTR (ds.cc_acuse, 1, 2) = 'D5' ";
				tablasDstoA		= " ,dis_docto_seleccionado ds ";
	
			}
			if("".equals(ic_estatus_docto))
				condicion.append(" and D.ic_estatus_docto in(1,2,3,5,9,11,24,28,30,32)");//MOD +(24)
			if(!"".equals(ic_estatus_docto)&&!"2".equals(ic_estatus_docto)&&!"3".equals(ic_estatus_docto)&&!"5".equals(ic_estatus_docto)&&!"9".equals(ic_estatus_docto)&&!"11".equals(ic_estatus_docto)&&!"20".equals(ic_estatus_docto)&&!"1".equals(ic_estatus_docto)&&!"24".equals(ic_estatus_docto)&&!"32".equals(ic_estatus_docto)&&!"28".equals(ic_estatus_docto)&&!"30".equals(ic_estatus_docto))//MOD +(&&!"24".equals(ic_estatus_docto))	
				condicion.append(" and D.ic_documento is null ");
			if("C".equals(tipoSolic)||(!"".equals(monto_credito_de)&& monto_credito_de!=null&&!"".equals(monto_credito_a)&& monto_credito_a!=null))
				condicion.append(" and D.ic_documento is null ");
	
			if(!fecha_seleccion_de.equals("") &&  !fecha_seleccion_a.equals(""))  {
				tablasDstoA = ", DIS_SOLICITUD S";
				condicionDstoA = "AND d.ic_documento = s.ic_documento";			
				condicion.append(" AND  S.DF_OPERACION >= to_date('"+fecha_seleccion_de+"', 'dd/mm/yyyy')"  +
										  " AND  S.DF_OPERACION <= to_date('"+fecha_seleccion_a+"', 'dd/mm/yyyy')  ") ;
			}
				
			if(!"".equals(ic_tipo_Pago)) {
	               condicion.append("AND d.ig_tipo_pago ="+ic_tipo_Pago);   
			}
		
			qryDM.append(
					"    SELECT /*+ use_nl(d,lc,m,tf,e)"   +
					"       index(d in_dis_documento_03_nuk)"   +
					"       index(tf cp_comcat_tipo_finan_pk)"   +
					"       index(e cp_comcat_epo_pk) */"   +
					"       m.ic_moneda"   +
					" 	  ,m.cd_nombre"   +
					" 	  ,d.fn_monto"   +
					"    FROM DIS_DOCUMENTO D"   +
					"    ,DIS_LINEA_CREDITO_DM LC "+tablasDstoA+" "+
					"    ,COMCAT_MONEDA M"   +
					"    ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"    ,COMCAT_EPO E"   +			
					"    WHERE D.IC_EPO = E.IC_EPO " +condicionDstoA+" "+
					"    AND D.IC_MONEDA = M.IC_MONEDA"   +
					"    AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM"   +
					"    AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO" +
					"    AND E.CS_HABILITADO = 'S' " +
						condicion.toString()
					);
						
			qryCCC.append(
					"    SELECT /*+ use_nl(d,lc,m,tf,e)"   +
					"       index(d in_dis_documento_03_nuk)"   +
					"       index(tf cp_comcat_tipo_finan_pk)"   +
					"       index(e cp_comcat_epo_pk) */"   +
					"       m.ic_moneda"   +
					" 	  ,m.cd_nombre"   +
					" 	  ,d.fn_monto"   +
					"    FROM DIS_DOCUMENTO D"   +
					"    ,COM_LINEA_CREDITO LC "+
					"    ,COMCAT_MONEDA M"   +
					"    ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"    ,COMCAT_EPO E"  +tablasDstoA+" "+	
					
					"	  ,com_bins_if bins	"	+
					"	  ,dis_doctos_pago_tc dtc	"	+
					
					"    WHERE D.IC_EPO = E.IC_EPO  " +condicionDstoA+" "+
					"    AND D.IC_MONEDA = M.IC_MONEDA"   +
					"    AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO (+)"   +
					
					"	  AND d.ic_bins = bins.ic_bins(+)	"	+
					"	  AND d.ic_orden_pago_tc = dtc.ic_orden_pago_tc(+)	"	+
					
					"    AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO " +
					"    AND D.IC_LINEA_CREDITO_DM IS NULL " +
					"    AND E.CS_HABILITADO = 'S' " +
						condicion.toString());
	
			if("D".equals(tipo_credito))
				qryAux = qryDM.toString().replaceAll("or bins.ic_if = "+ic_if,"");
			else if("C".equals(tipo_credito))
				qryAux = qryCCC.toString();
			else
				qryAux = qryDM.toString().replaceAll("or bins.ic_if = "+ic_if,"")+ " UNION ALL "+qryCCC.toString();
	
			qrySentencia.append(
					"SELECT ic_moneda, cd_nombre AS MONEDA  , COUNT (1) AS  TOTAL_DOCTOS  , SUM (fn_monto) AS TOTAL_MONTO "+
					"  FROM ("+qryAux.toString()+")"+
					"GROUP BY  ic_moneda,cd_nombre ORDER BY ic_moneda ");
					
		
		log.debug("qrySentencia  1.1 "+qrySentencia);
		log.debug("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		StringBuffer  qryCCC    = new StringBuffer();
		StringBuffer  qryDM  = new StringBuffer();
		conditions 		= new ArrayList();		
		condicion 	= new StringBuffer();
	   String qryAux 	= "";
	   qryCCC 	= new StringBuffer();
	   qryDM 	= new StringBuffer();	  
		 		
		if(!"".equals(ic_estatus)){
			ic_estatus_docto = ic_estatus.substring(1,ic_estatus.length());
			tipoSolic		 = ic_estatus.substring(0,1);
		}
		
		
			if(!"".equals(ic_epo)&&ic_epo!=null)
				condicion.append(" AND D.IC_EPO = "+ic_epo);
			if(!"".equals(ic_pyme)&&ic_pyme!=null)
				condicion.append(" AND D.IC_PYME = "+ic_pyme);
			if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
				condicion.append(" and D.ig_numero_docto = '"+ig_numero_docto+"'");
			if(!"".equals(cc_acuse)&& cc_acuse!=null)
				condicion.append(" and D.cc_acuse = '"+cc_acuse+"'");
			if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null)
				condicion.append(" and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date('"+fecha_vto_credito_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_credito_a+"','dd/mm/yyyy'))");
			if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null)
				condicion.append(" AND LC.ic_tipo_cobro_interes = "+ic_tipo_cobro_int);
			if(!"".equals(ic_moneda)&&ic_moneda!=null)
				condicion.append(" and M.ic_moneda = "+ic_moneda);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
				condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de.replaceAll(",","")+" and "+fn_monto_a.replaceAll(",",""));
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
				condicion.append(" and D.fn_monto between "+fn_monto_de.replaceAll(",","")+" and "+fn_monto_a.replaceAll(",",""));
			if(!"".equals(solo_cambio)&& solo_cambio!=null)
				condicion.append(" and D.ic_documento in (select ic_documento from dis_cambio_estatus)");
			if(!"".equals(modo_plazo)&& modo_plazo!=null)
				condicion.append(" and TF.ic_tipo_financiamiento = "+modo_plazo);
			if(!"".equals(ic_documento)&&ic_documento!=null)
				condicion.append(" AND D.IC_DOCUMENTO = "+ic_documento);
			if(!"".equals(ic_if)&&ic_if!=null)
				condicion.append(" AND (LC.IC_IF = "+ic_if+" or bins.ic_if = "+ic_if+" )" );
			if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
				condicion.append(" and trunc(D.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))");
			if("2".equals(ic_estatus_docto)||"3".equals(ic_estatus_docto)
				||"5".equals(ic_estatus_docto)||"9".equals(ic_estatus_docto)
				||"11".equals(ic_estatus_docto)||"20".equals(ic_estatus_docto)
				||"1".equals(ic_estatus_docto)||"24".equals(ic_estatus_docto)
				||"32".equals(ic_estatus_docto)
				||"28".equals(ic_estatus_docto)||"30".equals(ic_estatus_docto))//<---FODEA-032-2014
				condicion.append(" AND D.IC_ESTATUS_DOCTO ="+ic_estatus_docto);
			if("S".equals(chk_proc_desc)) {
				ic_estatus_docto = "3";
				condicion.append(" AND D.IC_ESTATUS_DOCTO ="+ic_estatus_docto);
				condicionDstoA	= 
					" AND D.ic_documento = ds.ic_documento "+
					" AND SUBSTR (ds.cc_acuse, 1, 2) = 'D5' ";
				tablasDstoA		= " ,dis_docto_seleccionado ds ";

			}
			if("".equals(ic_estatus_docto))
				condicion.append(" and D.ic_estatus_docto in(1,2,3,5,9,11,24,28,30,32)");
			if(!"".equals(ic_estatus_docto)&&!"2".equals(ic_estatus_docto)&&!"3".equals(ic_estatus_docto)&&!"5".equals(ic_estatus_docto)&&!"9".equals(ic_estatus_docto)&&!"11".equals(ic_estatus_docto)&&!"20".equals(ic_estatus_docto)&&!"1".equals(ic_estatus_docto)&&!"24".equals(ic_estatus_docto)&&!"32".equals(ic_estatus_docto)&&!"28".equals(ic_estatus_docto)&&!"30".equals(ic_estatus_docto))//MOD +(&&!"24".equals(ic_estatus_docto))	
				condicion.append(" and D.ic_documento is null ");
			if("C".equals(tipoSolic)||(!"".equals(monto_credito_de)&& monto_credito_de!=null&&!"".equals(monto_credito_a)&& monto_credito_a!=null))
				condicion.append(" and D.ic_documento is null ");

			if(!fecha_seleccion_de.equals("") &&  !fecha_seleccion_a.equals(""))  {
				tablasDstoA = ", DIS_SOLICITUD S";
				condicionDstoA = "AND d.ic_documento = s.ic_documento";			
			
				condicion.append(" AND  S.DF_OPERACION >= to_date('"+fecha_seleccion_de+"', 'dd/mm/yyyy')"  +
									  " AND  S.DF_OPERACION <= to_date('"+fecha_seleccion_a+"', 'dd/mm/yyyy')  ") ;
			}
			
			if(!"".equals(ic_tipo_Pago)) {
				condicion.append("AND d.ig_tipo_pago ="+ic_tipo_Pago);     
			}
		
			qryDM.append(
				"   SELECT /*+ use_nl(d,lc,m,tf,e)"   +
				"      index(d in_dis_documento_03_nuk)"   +
				"      index(tf cp_comcat_tipo_finan_pk)"   +
				"      index(e cp_comcat_epo_pk) */"   +
				"      D.ic_documento"   +
				"   FROM DIS_DOCUMENTO D"   +
				"   ,DIS_LINEA_CREDITO_DM LC "+tablasDstoA+" "+
				"   ,COMCAT_MONEDA M"   +
				"   ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
				"   ,COMCAT_EPO E"   +
				"   WHERE D.IC_EPO = E.IC_EPO " +condicionDstoA+" "+
				"   AND D.IC_MONEDA = M.IC_MONEDA"   +
				"   AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM"   +
				"   AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO" +
				"   AND E.CS_HABILITADO = 'S' " +
					condicion.toString()
				);
					
			qryCCC.append(
					"   SELECT /*+ use_nl(d,lc,m,tf,e)"   +
					"      index(d in_dis_documento_03_nuk)"   +
					"      index(tf cp_comcat_tipo_finan_pk)"   +
					"      index(e cp_comcat_epo_pk) */"   +
					"  	D.ic_documento"   +
					"   FROM DIS_DOCUMENTO D"   +
					"   ,COM_LINEA_CREDITO LC "+
					"   ,COMCAT_MONEDA M"   +
					"   ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"   ,COMCAT_EPO E  "+tablasDstoA+" "+
					
					"	  ,com_bins_if bins	"	+
					"	  ,dis_doctos_pago_tc dtc	"	+
					
					"   WHERE D.IC_EPO = E.IC_EPO  " +condicionDstoA+" "+
					"   AND D.IC_MONEDA = M.IC_MONEDA"   +
					"   AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO (+)"   +
					
					"	  AND d.ic_bins = bins.ic_bins(+)	"	+
					"	  AND d.ic_orden_pago_tc = dtc.ic_orden_pago_tc(+)	"	+
					
					"   AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+
					"   AND D.IC_LINEA_CREDITO_DM IS NULL " +
					"   AND E.CS_HABILITADO = 'S' " +
					condicion.toString());

			if("D".equals(tipo_credito))
				qrySentencia.append(qryDM.toString().replaceAll("or bins.ic_if = "+ic_if,""));
			else if("C".equals(tipo_credito))
				qrySentencia = qryCCC;
			else
				qrySentencia.append(qryDM.toString().replaceAll("or bins.ic_if = "+ic_if,"") +" UNION ALL " +qryCCC.toString());//+" order by 1 desc");
		
		
		log.debug("qrySentencia 1.2  "+qrySentencia);
		log.debug("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		StringBuffer  qryCCC    = new StringBuffer();
		StringBuffer  qryDM  = new StringBuffer();
		conditions 		= new ArrayList();
		condicion 	= new StringBuffer();
	   String qryAux 	= "";
	   qryCCC 	= new StringBuffer();
	   qryDM 	= new StringBuffer();	
		
		condicion.append(" AND d.ic_documento in ( ");
		for (int i = 0; i < pageIds.size(); i++) { 
			List lItem = (ArrayList)pageIds.get(i);
			if(i > 0){condicion.append(" ,   ");}
				condicion.append( new Long(lItem.get(0).toString()));
		}			
		condicion.append(" ) ");
			
		qryDM.append(
						"  SELECT /*+ use_nl(d,lc,py,m,pe,pn,tc,tf,ed,e)"   +
						"      index(d cp_dis_documento_pk)"   +
						"     index(tf cp_comcat_tipo_finan_pk)"   +
						"     index(e cp_comcat_epo_pk)"   +
						"     index(ed cp_comcat_estatus_docto_pk)*/"   +
					
						"     PY.CG_RAZON_SOCIAL AS NOMBRE_PYME "   +
						"  	,D.IG_NUMERO_DOCTO as NUM_DOCTO_INICIAL"   +
						"  	,D.CC_ACUSE  as NUM_ACUSE_CARGA  "   +
						" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS FECHA_EMISION"   +
						" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS FECHA_PUBLICACION"   +
						" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS FECHA_VENCIMIENTO"   +
						" 	,D.IG_PLAZO_DOCTO as PLAZO_DOCTO "   +
						" 	,M.CD_NOMBRE AS MONEDA"   +
						" 	,D.FN_MONTO as MONTO  "   +
						" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') AS TIPO_CONVENIO "   +
						" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
						" 	,NVL(D.IG_PLAZO_DESCUENTO,'') AS PLAZO_DESC_DIAS"   +
						" 	,DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) AS PORCENTAJE_DESC"   +
						//" 	,NVL(D.FN_PORC_DESCUENTO,0) AS PORCENTAJE_DESC"   +
						" 	,TF.CD_DESCRIPCION AS MODALIDAD_PLAZO"   +
						" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
						" 	,M.ic_moneda as IC_MONEDA"   +
						" 	,D.ic_documento AS IC_DOCUMENTO"   + 
						" 	,LC.ic_moneda AS IC_MONEDA_LINEA"   +
						" 	,E.cg_razon_social AS NOMBRE_EPO"   +
						" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
						" ,  (  ( D.FN_MONTO  - ( ( D.FN_MONTO * DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) ) /100)  ) *    DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')  )      AS monto_valuado  "+
						" , ( ( D.FN_MONTO * DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) ) /100  ) as MONTO_DESCUENTO  "+
						", DECODE (d.ig_tipo_pago, '1', 'Financiamiento con intereses', '2', 'Meses sin Intereses ','3', 'Tarjeta de Cr�dito' ) AS TIPOPAGO "+ //F09-2015
						", (SELECT CASE WHEN COUNT(*) > 0 THEN 'S' ELSE 'N' END FROM COM_ARCDOCTOS WHERE CC_ACUSE = D.CC_ACUSE) AS MUESTRA_VISOR" + 
						"  FROM DIS_DOCUMENTO D"   +
						"  ,DIS_LINEA_CREDITO_DM LC"   +
						"  ,COMCAT_PYME PY"   +
						"  ,COMCAT_MONEDA M"   +
						"  ,COMREL_PRODUCTO_EPO PE"   +
						"  ,COMCAT_PRODUCTO_NAFIN PN"   +
						"  ,COM_TIPO_CAMBIO TC"   +
						"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
						"  ,COMCAT_ESTATUS_DOCTO ED"   +
						"  ,COMCAT_EPO E"   +
						"  WHERE D.IC_PYME = PY.IC_PYME"   +
						"  AND D.IC_EPO = E.IC_EPO"   +
						"  AND D.IC_MONEDA = M.IC_MONEDA"   +
						"  AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM"   +
						"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
						"  AND D.IC_EPO = PE.IC_EPO"   +
						"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
						"  AND TC.DC_FECHA IN(SELECT /*+ index(tc2 in_com_tipo_cambio_01_nuk)*/ MAX(tc2.DC_FECHA) FROM COM_TIPO_CAMBIO tc2 WHERE tc2.IC_MONEDA = M.IC_MONEDA)"   +
						"  AND M.ic_moneda = TC.ic_moneda"   +
						"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
						"  AND PN.IC_PRODUCTO_NAFIN = 4"  +
						condicion.toString()
					);
						
				qryCCC.append(
						"  SELECT /*+ use_nl(d,lc,py,m,pe,pn,tc,tf,ed,e)"   +
						"     index(d cp_dis_documento_pk)"   +
						"     index(tf cp_comcat_tipo_finan_pk)"   +
						"     index(e cp_comcat_epo_pk)"   +
						"     index(ed cp_comcat_estatus_docto_pk)*/ "   +
						
						"  	PY.CG_RAZON_SOCIAL AS NOMBRE_PYME"   +
						"  	,D.IG_NUMERO_DOCTO as NUM_DOCTO_INICIAL"   + 
						"  	,D.CC_ACUSE as NUM_ACUSE_CARGA "   +
						" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS FECHA_EMISION"   +
						" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS FECHA_PUBLICACION"   +
						" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS FECHA_VENCIMIENTO"   +
						" 	,D.IG_PLAZO_DOCTO as PLAZO_DOCTO "   +
						" 	,M.CD_NOMBRE AS MONEDA"   +
						" 	,D.FN_MONTO as MONTO"   +
						" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') AS TIPO_CONVENIO"   +
						" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
						" 	,NVL(D.IG_PLAZO_DESCUENTO,'') AS PLAZO_DESC_DIAS"   +
						" 	,DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) AS PORCENTAJE_DESC"   +
						" 	,TF.CD_DESCRIPCION AS MODALIDAD_PLAZO"   +
						" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
						" 	,M.ic_moneda IC_MONEDA"   +
						" 	,D.ic_documento AS IC_DOCUMENTO"   +
						" 	,LC.ic_moneda AS IC_MONEDA_LINEA"   +
						" 	,E.cg_razon_social AS NOMBRE_EPO"   +
						" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
						" ,  (  ( D.FN_MONTO  - ( ( D.FN_MONTO * DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) ) /100)  ) *    DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')  )      AS monto_valuado  "+
						" , ( ( D.FN_MONTO * DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) ) /100  ) as MONTO_DESCUENTO  "+
						", DECODE (d.ig_tipo_pago, '1', 'Financiamiento con intereses', '2', 'Meses sin Intereses ','3', 'Tarjeta de Cr�dito' ) AS TIPOPAGO "+ //F09-2015
						", (SELECT CASE WHEN COUNT(*) > 0 THEN 'S' ELSE 'N' END FROM COM_ARCDOCTOS WHERE CC_ACUSE = D.CC_ACUSE) AS MUESTRA_VISOR" +
						"  FROM DIS_DOCUMENTO D"   +
						"  ,COM_LINEA_CREDITO LC"   +
						"  ,COMCAT_PYME PY"   +
						"  ,COMCAT_MONEDA M"   +
						"  ,COMREL_PRODUCTO_EPO PE"   +
						"  ,COMCAT_PRODUCTO_NAFIN PN"   +
						"  ,COM_TIPO_CAMBIO TC"   +
						"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
						"  ,COMCAT_ESTATUS_DOCTO ED"   +
						"  ,COMCAT_EPO E"   +
						"  WHERE D.IC_PYME = PY.IC_PYME"   +
						"  AND D.IC_EPO = E.IC_EPO"   +
						"  AND D.IC_MONEDA = M.IC_MONEDA"   +
						"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
						"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
						"  AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO (+)"   +
						"  AND D.IC_EPO = PE.IC_EPO"   +
						"  AND TC.DC_FECHA IN(SELECT /*+ index(tc2 in_com_tipo_cambio_01_nuk)*/ MAX(tc2.DC_FECHA) FROM COM_TIPO_CAMBIO tc2 WHERE tc2.IC_MONEDA = M.IC_MONEDA)"   +
						"  AND M.ic_moneda = TC.ic_moneda"   +
						"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
						"  AND PN.IC_PRODUCTO_NAFIN = 4 "  +
						"  AND D.IC_LINEA_CREDITO_DM IS NULL " +
						condicion.toString());
	
				if("D".equals(tipo_credito))  
					qrySentencia = qryDM;
				else if("C".equals(tipo_credito))
					qrySentencia = qryCCC;
				else
					qrySentencia.append(qryDM.toString() +" UNION ALL " +qryCCC.toString());
			
		log.debug("IDS  "+pageIds);	
		log.debug("qrySentencia 1.3  "+qrySentencia);
		log.debug("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		StringBuffer  qryCCC    = new StringBuffer();
		StringBuffer  qryDM  = new StringBuffer();
		conditions 		= new ArrayList();  
	
		if("1".equals(tipoConsulta)) {
	
			if(!"".equals(ic_estatus)){
				ic_estatus_docto = ic_estatus.substring(1,ic_estatus.length());
				tipoSolic		 = ic_estatus.substring(0,1);
			}
	
			if(!"".equals(ic_epo)&&ic_epo!=null)
				condicion.append(" AND D.IC_EPO = "+ic_epo);
			if(!"".equals(ic_pyme)&&ic_pyme!=null)
				condicion.append(" AND D.IC_PYME = "+ic_pyme);
			if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
				condicion.append(" and D.ig_numero_docto = '"+ig_numero_docto+"'");
			if(!"".equals(cc_acuse)&& cc_acuse!=null)
				condicion.append(" and D.cc_acuse = '"+cc_acuse+"'");
			if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null)
				condicion.append(" and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date('"+fecha_vto_credito_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_credito_a+"','dd/mm/yyyy'))");
			if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null)
				condicion.append(" AND LC.ic_tipo_cobro_interes = "+ic_tipo_cobro_int);
			if(!"".equals(ic_moneda)&&ic_moneda!=null)
				condicion.append(" and M.ic_moneda = "+ic_moneda);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
				condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de.replaceAll(",","")+" and "+fn_monto_a.replaceAll(",",""));
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
				condicion.append(" and D.fn_monto between "+fn_monto_de.replaceAll(",","")+" and "+fn_monto_a.replaceAll(",",""));
			if(!"".equals(solo_cambio)&& solo_cambio!=null)
				condicion.append(" and D.ic_documento in (select ic_documento from dis_cambio_estatus)");
			if(!"".equals(modo_plazo)&& modo_plazo!=null)
				condicion.append(" and TF.ic_tipo_financiamiento = "+modo_plazo);
			if(!"".equals(ic_documento)&&ic_documento!=null)
				condicion.append(" AND D.IC_DOCUMENTO = "+ic_documento);
			if(!"".equals(ic_if)&&ic_if!=null)
				condicion.append(" AND (LC.IC_IF = "+ic_if+" or bins.ic_if = "+ic_if+" )");
			if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
				condicion.append(" and trunc(D.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))");
			if("2".equals(ic_estatus_docto)||"3".equals(ic_estatus_docto)||"5".equals(ic_estatus_docto)||"9".equals(ic_estatus_docto)||"11".equals(ic_estatus_docto)||"20".equals(ic_estatus_docto)||"1".equals(ic_estatus_docto)||"24".equals(ic_estatus_docto)||"32".equals(ic_estatus_docto)||"28".equals(ic_estatus_docto)||"30".equals(ic_estatus_docto))//MOD +(||"24".equals(ic_estatus_docto))
				condicion.append(" AND D.IC_ESTATUS_DOCTO ="+ic_estatus_docto);
			if("S".equals(chk_proc_desc)) {
				ic_estatus_docto = "3";
				condicion.append(" AND D.IC_ESTATUS_DOCTO ="+ic_estatus_docto);
				condicionDstoA	= 
					" AND D.ic_documento = ds.ic_documento "+
					" AND SUBSTR (ds.cc_acuse, 1, 2) = 'D5' ";
				tablasDstoA		= " ,dis_docto_seleccionado ds ";

			}
			if("".equals(ic_estatus_docto))
				condicion.append(" and D.ic_estatus_docto in(1,2,3,5,9,11,24,28,30,32)");//MOD +(24)
			if(!"".equals(ic_estatus_docto)&&!"2".equals(ic_estatus_docto)&&!"3".equals(ic_estatus_docto)&&!"5".equals(ic_estatus_docto)&&!"9".equals(ic_estatus_docto)&&!"11".equals(ic_estatus_docto)&&!"20".equals(ic_estatus_docto)&&!"1".equals(ic_estatus_docto)&&!"24".equals(ic_estatus_docto)&&!"32".equals(ic_estatus_docto)&&!"28".equals(ic_estatus_docto)&&!"30".equals(ic_estatus_docto))//MOD +(&&!"24".equals(ic_estatus_docto))	
				condicion.append(" and D.ic_documento is null ");
			if("C".equals(tipoSolic)||(!"".equals(monto_credito_de)&& monto_credito_de!=null&&!"".equals(monto_credito_a)&& monto_credito_a!=null))
				condicion.append(" and D.ic_documento is null ");

			if(!fecha_seleccion_de.equals("") &&  !fecha_seleccion_a.equals(""))  {
				tablasDstoA = ", DIS_SOLICITUD S";
				condicionDstoA = "AND d.ic_documento = s.ic_documento";			
			
				condicion.append(" AND  S.DF_OPERACION >= to_date('"+fecha_seleccion_de+"', 'dd/mm/yyyy')"  +
									  " AND  S.DF_OPERACION <= to_date('"+fecha_seleccion_a+"', 'dd/mm/yyyy')  ") ;
			}
						
		   if(!"".equals(ic_tipo_Pago)) {
				condicion.append("AND d.ig_tipo_pago ="+ic_tipo_Pago);    
			}
		            
						
		qryDM.append(
					"  SELECT /*+ use_nl(d,lc,py,m,pe,pn,tc,tf,ed,e)"   +
					"     index(d in_dis_documento_03_nuk)"   +
					"     index(tf cp_comcat_tipo_finan_pk)"   +
					"     index(e cp_comcat_epo_pk)"   +
					"     index(ed cp_comcat_estatus_docto_pk)*/"   +
					
					"     PY.CG_RAZON_SOCIAL AS NOMBRE_PYME "   +
					"  	,D.IG_NUMERO_DOCTO as NUM_DOCTO_INICIAL"   +
					"  	,D.CC_ACUSE  as NUM_ACUSE_CARGA  "   +
					" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS FECHA_EMISION"   +
					" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS FECHA_PUBLICACION"   +
					" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS FECHA_VENCIMIENTO"   +
					" 	,D.IG_PLAZO_DOCTO as PLAZO_DOCTO "   +
					" 	,M.CD_NOMBRE AS MONEDA"   +
					" 	,D.FN_MONTO as MONTO  "   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') AS TIPO_CONVENIO "   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
					" 	,NVL(D.IG_PLAZO_DESCUENTO,'') AS PLAZO_DESC_DIAS"   +
					" 	,DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) AS PORCENTAJE_DESC"   +
					" 	,TF.CD_DESCRIPCION AS MODALIDAD_PLAZO"   +
					" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
					" 	,M.ic_moneda as IC_MONEDA"   +
					" 	,D.ic_documento AS IC_DOCUMENTO"   +
					" 	,LC.ic_moneda AS IC_MONEDA_LINEA"   +
					" 	,E.cg_razon_social AS NOMBRE_EPO"   +
					" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
					" ,  (  ( D.FN_MONTO  - ( ( D.FN_MONTO * DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) ) /100)  ) *    DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')  )      AS monto_valuado  "+
					" , ( ( D.FN_MONTO * DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) ) /100  ) as MONTO_DESCUENTO  "+
					 ", DECODE (d.ig_tipo_pago, '1', 'Financiamiento con intereses', '2', 'Meses sin Intereses ','3', 'Tarjeta de Cr�dito' ) AS TIPOPAGO "+ //F09-2015
		          
					
					"  FROM DIS_DOCUMENTO D"   +
					"  ,DIS_LINEA_CREDITO_DM LC "+tablasDstoA+" "+
					"  ,COMCAT_PYME PY"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  ,COMCAT_ESTATUS_DOCTO ED"   +
					"  ,COMCAT_EPO E"   +
					"  WHERE D.IC_PYME = PY.IC_PYME"   +
					"  AND D.IC_EPO = E.IC_EPO " +condicionDstoA+" "+
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM"   +
					"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
					"  AND D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND TC.DC_FECHA IN(SELECT /*+ index(tc2 in_com_tipo_cambio_01_nuk)*/ MAX(tc2.DC_FECHA) FROM COM_TIPO_CAMBIO tc2 WHERE tc2.IC_MONEDA = M.IC_MONEDA)"   +
					"  AND M.ic_moneda = TC.ic_moneda"   +
					"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"  AND PN.IC_PRODUCTO_NAFIN = 4"  +
					"   AND E.CS_HABILITADO = 'S' " +
					condicion.toString()
				);
					
			qryCCC.append(
					"  SELECT /*+ use_nl(d,lc,py,m,pe,pn,tc,tf,ed,e)"   +
					"     index(d in_dis_documento_03_nuk)"   +
					"     index(tf cp_comcat_tipo_finan_pk)"   +
					"     index(e cp_comcat_epo_pk)"   +
					"     index(ed cp_comcat_estatus_docto_pk)*/ "   +
					
					"  	PY.CG_RAZON_SOCIAL AS NOMBRE_PYME"   +
					"  	,D.IG_NUMERO_DOCTO as NUM_DOCTO_INICIAL"   +
					"  	,D.CC_ACUSE as NUM_ACUSE_CARGA "   +
					" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS FECHA_EMISION"   +
					" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS FECHA_PUBLICACION"   +
					" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS FECHA_VENCIMIENTO"   +
					" 	,D.IG_PLAZO_DOCTO as PLAZO_DOCTO "   +
					" 	,M.CD_NOMBRE AS MONEDA"   +
					" 	,D.FN_MONTO as MONTO"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') AS TIPO_CONVENIO"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
					" 	,NVL(D.IG_PLAZO_DESCUENTO,'') AS PLAZO_DESC_DIAS"   +
					" 	,DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) AS PORCENTAJE_DESC"   +
					" 	,TF.CD_DESCRIPCION AS MODALIDAD_PLAZO"   +
					" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
					" 	,M.ic_moneda IC_MONEDA"   +
					" 	,D.ic_documento AS IC_DOCUMENTO"   +
					" 	,LC.ic_moneda AS IC_MONEDA_LINEA"   +
					" 	,E.cg_razon_social AS NOMBRE_EPO"   +
					" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
					" ,  (  ( D.FN_MONTO  - ( ( D.FN_MONTO * DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) ) /100)  ) *    DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')  )      AS monto_valuado  "+
					" , ( ( D.FN_MONTO * DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) ) /100  ) as MONTO_DESCUENTO  "+
					", DECODE (d.ig_tipo_pago, '1', 'Financiamiento con intereses', '2', 'Meses sin Intereses ','3', 'Tarjeta de Cr�dito' ) AS TIPOPAGO "+ //F09-2015
		                  
													
					"  FROM DIS_DOCUMENTO D"   +
					"  ,COM_LINEA_CREDITO LC "+
					"  ,COMCAT_PYME PY"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  ,COMCAT_ESTATUS_DOCTO ED"   +
					"  ,COMCAT_EPO E   "+tablasDstoA+" "+
					
					"	  ,com_bins_if bins	"	+
					"	  ,dis_doctos_pago_tc dtc	"	+
					
					"  WHERE D.IC_PYME = PY.IC_PYME " +condicionDstoA+" "+
					"  AND D.IC_EPO = E.IC_EPO " +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
					"  AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO (+)"   +
					
					"	  AND d.ic_bins = bins.ic_bins(+)	"	+
					"	  AND d.ic_orden_pago_tc = dtc.ic_orden_pago_tc(+)	"	+
					
					"  AND D.IC_EPO = PE.IC_EPO"   +
					"  AND TC.DC_FECHA IN(SELECT /*+ index(tc2 in_com_tipo_cambio_01_nuk)*/ MAX(tc2.DC_FECHA) FROM COM_TIPO_CAMBIO tc2 WHERE tc2.IC_MONEDA = M.IC_MONEDA)"   +
					"  AND M.ic_moneda = TC.ic_moneda"   +
					"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"  AND PN.IC_PRODUCTO_NAFIN = 4 "  +
					"  AND D.IC_LINEA_CREDITO_DM IS NULL " +
				"   AND E.CS_HABILITADO = 'S' " +
					condicion.toString());

			if("D".equals(tipo_credito))
				qrySentencia.append( qryDM.toString().replaceAll("or bins.ic_if = "+ic_if,""));
			else if("C".equals(tipo_credito))
				qrySentencia = qryCCC;
			else
				qrySentencia.append(qryDM.toString().replaceAll("or bins.ic_if = "+ic_if,"") +" UNION ALL " +qryCCC.toString());
      
		
		}else	if("Detalle".equals(tipoConsulta)) {
		
			qrySentencia.append(" SELECT DISTINCT TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS FECHA_CAMBIO, "+
							" cce.cd_descripcion as CAMBIO_ESTATUS ,       "+           
							" TO_CHAR (ce.df_fecha_emision_anterior, 'dd/mm/yyyy' ) As F_EMISION_ANTERIOR ,  "+
							" TO_CHAR (ce.df_fecha_emision_nueva, 'dd/mm/yyyy'  ) AS F_EMISION_NUEVA ,  "+
							" ce.fn_monto_anterior  as MONTO_ANTERIOR  ,  "+
							" ce.fn_monto_nuevo as MONTO_NUEVO ,   "+
							" TO_CHAR (ce.df_fecha_venc_anterior, 'dd/mm/yyyy'  ) AS F_VENCIMIENTO_ANTERIOR,  "+
							" TO_CHAR (ce.df_fecha_venc_nueva, 'dd/mm/yyyy' ) AS F_VENCIMIENTO_NUEVO,      "+               
							" tfa.cd_descripcion AS MODALIDAD_ANTERIOR  ,  "+
							" tfn.cd_descripcion AS MODALIDAD_NUEVO  "+
						" FROM dis_cambio_estatus ce, "+
						" comcat_cambio_estatus cce, "+
						" comcat_tipo_financiamiento tfa, "+
						" comcat_tipo_financiamiento tfn "+
				 " WHERE cce.ic_cambio_estatus = ce.ic_cambio_estatus "+
				 "  AND ce.ic_tipo_finan_ant = tfa.ic_tipo_financiamiento(+)  "+
				 "  AND ce.ic_tipo_finan_nuevo = tfn.ic_tipo_financiamiento(+) ");
				 
				 if(!"".equals(ic_documento) ){
					qrySentencia.append(" and  ic_documento = ? ");
					conditions.add(ic_documento);
				}
		}
		
	log.debug("qrySentencia 1.5 "+qrySentencia); 
		log.debug("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}

/**
 * Genera los archivos CSV y PDF sin utilizar la paginaci�n, los datos que recibe en el request vienen de la consulta
 * en el m�todo getDocumentQueryFile()
 * @param request
 * @param rs
 * @param path
 * @param tipo CSV o PDF
 * @return nombreArchivo
 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {

		log.info("crearCustomFile (E)");
		String nombreArchivo     = "";
		String nombreEpo         = "";
		String nombrePYME        = "";
		String num_acuse         = "";
		String num_docto_inicial = "";
		String fecha_publicacion = "";
		String fecha_Emision     = "";
		String FechaVencimiento  = "";
		String plazo_docto       = "";
		String moneda            = "";
		String monto             = "";
		String plazo_desc        = "";
		String porc_des          = "";
		String monto_desc        = "";
		String tipo_convenio     = "";
		String tipo_cambio       = "";
		String monto_valuado     = "";
		String modalidad_plazo   = "";
		String estatus           = "";
		String ic_moneda_linea   = "";
		String ic_moneda         = "";
		String ventaCartera      = "";

		if(tipo.equals("CSV")){

			try{
				int total = 0;
				OutputStreamWriter writer = null;
				BufferedWriter buffer = null;
				StringBuffer contenidoArchivo = new StringBuffer();

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
				buffer = new BufferedWriter(writer);

				contenidoArchivo.append("EPO,"                         );
				contenidoArchivo.append("Distribuidor,"                );
				contenidoArchivo.append("N�m. acuse carga,"            );
				contenidoArchivo.append("N�mero de documento inicial," );
				contenidoArchivo.append("Fecha de publicaci�n,"        );
				contenidoArchivo.append("Fecha de emisi�n,"            );
				contenidoArchivo.append("Fecha vencimiento,"           );
				contenidoArchivo.append("Plazo docto.,"                );
				contenidoArchivo.append("Moneda,"                      );
				contenidoArchivo.append("Monto,"                       );
				contenidoArchivo.append("Plazo para descuento en d�as,");
				contenidoArchivo.append("% de descuento,"              );
				contenidoArchivo.append("Monto % de descuento,"        );
				contenidoArchivo.append("Tipo conv.,"                  );
				contenidoArchivo.append("Tipo cambio,"                 );
				contenidoArchivo.append("Monto valuado en Pesos,"      );
				contenidoArchivo.append("Modalidad de plazo,"          );
			   contenidoArchivo.append("Tipo de Pago ,"          );
				contenidoArchivo.append("Estatus  \n"                  );

				while (rs.next()){
					nombreEpo         = (rs.getString("NOMBRE_EPO")        == null) ? "" : rs.getString("NOMBRE_EPO");
					nombrePYME        = (rs.getString("NOMBRE_PYME")       == null) ? "" : rs.getString("NOMBRE_PYME");
					num_acuse         = (rs.getString("NUM_ACUSE_CARGA")   == null) ? "" : rs.getString("NUM_ACUSE_CARGA");
					num_docto_inicial = (rs.getString("NUM_DOCTO_INICIAL") == null) ? "" : rs.getString("NUM_DOCTO_INICIAL");
					fecha_publicacion = (rs.getString("FECHA_PUBLICACION") == null) ? "" : rs.getString("FECHA_PUBLICACION");
					fecha_Emision     = (rs.getString("FECHA_EMISION")     == null) ? "" : rs.getString("FECHA_EMISION");
					FechaVencimiento  = (rs.getString("FECHA_VENCIMIENTO") == null) ? "" : rs.getString("FECHA_VENCIMIENTO");
					plazo_docto       = (rs.getString("PLAZO_DOCTO")       == null) ? "" : rs.getString("PLAZO_DOCTO");
					moneda            = (rs.getString("MONEDA")            == null) ? "" : rs.getString("MONEDA");
					monto             = (rs.getString("MONTO")             == null) ? "" : rs.getString("MONTO");
					plazo_desc        = (rs.getString("PLAZO_DESC_DIAS")   == null) ? "" : rs.getString("PLAZO_DESC_DIAS");
					porc_des          = (rs.getString("PORCENTAJE_DESC")   == null) ? "" : rs.getString("PORCENTAJE_DESC");
					monto_desc        = (rs.getString("MONTO_DESCUENTO")   == null) ? "" : rs.getString("MONTO_DESCUENTO");
					tipo_convenio     = (rs.getString("TIPO_CONVENIO")     == null) ? "" : rs.getString("TIPO_CONVENIO");
					tipo_cambio       = (rs.getString("TIPO_CAMBIO")       == null) ? "" : rs.getString("TIPO_CAMBIO");
					monto_valuado     = (rs.getString("MONTO_VALUADO")     == null) ? "" : rs.getString("MONTO_VALUADO");
					modalidad_plazo   = (rs.getString("MODALIDAD_PLAZO")   == null) ? "" : rs.getString("MODALIDAD_PLAZO");
					estatus           = (rs.getString("ESTATUS")           == null) ? "" : rs.getString("ESTATUS");
					ic_moneda_linea   = (rs.getString("IC_MONEDA_LINEA")   == null) ? "" : rs.getString("IC_MONEDA_LINEA");
					ic_moneda         = (rs.getString("IC_MONEDA")         == null) ? "" : rs.getString("IC_MONEDA");
					ventaCartera      = (rs.getString("CG_VENTACARTERA")   == null) ? "" : rs.getString("CG_VENTACARTERA");
				   String tipoPago = (rs.getString("TIPOPAGO") == null) ? "" : rs.getString("TIPOPAGO"); 
					
					contenidoArchivo.append(nombreEpo.replace(',',' ')         +", ");
					contenidoArchivo.append(nombrePYME.replace(',',' ')        +", ");
					contenidoArchivo.append(num_acuse.replace(',',' ')         +", ");
					contenidoArchivo.append(num_docto_inicial.replace(',',' ') +", ");
					contenidoArchivo.append(fecha_publicacion.replace(',',' ') +", ");
					contenidoArchivo.append(fecha_Emision.replace(',',' ')     +", ");
					contenidoArchivo.append(FechaVencimiento.replace(',',' ')  +", ");
					contenidoArchivo.append(plazo_docto.replace(',',' ')       +", ");
					contenidoArchivo.append(moneda.replace(',',' ')            +", ");
					contenidoArchivo.append(monto.replace(',',' ')             +", ");
					contenidoArchivo.append(plazo_desc.replace(',',' ')        +", ");
					contenidoArchivo.append(porc_des.replace(',',' ')          +", ");
					contenidoArchivo.append(monto_desc.replace(',',' ')        +", ");
					if(ic_moneda.equals(ic_moneda_linea)){
						contenidoArchivo.append(" , , , ");
					} else{
						contenidoArchivo.append(tipo_convenio.replace(',',' ')+", ");
						contenidoArchivo.append(tipo_cambio.replace(',',' ')  +", ");
						contenidoArchivo.append(monto_valuado.replace(',',' ')+", ");
					}
					if(ventaCartera.equals("S")){
						contenidoArchivo.append(" , ");
					} else{
						contenidoArchivo.append(modalidad_plazo.replace(',',' ')+", ");
					}
				   contenidoArchivo.append(tipoPago.replace(',',' ')+",");
					contenidoArchivo.append(estatus.replace(',',' ')+"\n");

					total++;
					if(total==1000){
						total=0; 
						buffer.write(contenidoArchivo.toString());
						contenidoArchivo = new StringBuffer();//Limpio el buffer
					}
				}

				buffer.write(contenidoArchivo.toString());
				buffer.close();
				contenidoArchivo = new StringBuffer();//Limpio el buffer

			} catch(Throwable e){
				throw new AppException("Error al generar el archivo CSV. ", e);
			}

		} else if(tipo.equals("PDF")){

			try{
				HttpSession session = request.getSession();
				ComunesPDF pdfDoc   = new ComunesPDF();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));  
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

				pdfDoc.setLTable(11,100);
				if(tipo_credito.equals("D")) {
					pdfDoc.setLCell(" Datos Documento Inicial  ","celda01",ComunesPDF.CENTER, 11);
				}else if(tipo_credito.equals("C")  ||  tipo_credito.equals("")  ) {
					pdfDoc.setLCell(" Documento ","celda01",ComunesPDF.CENTER, 11);
				}
				pdfDoc.setLCell("A ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("EPO ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Distribuidor  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("N�m. acuse carga ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("N�mero de documento inicial ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de publicaci�n ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de emisi�n ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha vencimiento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo docto. ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto ","celda01",ComunesPDF.CENTER);

				pdfDoc.setLCell("B ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo para descuento en d�as  ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("% de descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto % de descuento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo conv. ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo cambio ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto valuado en Pesos ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Modalidad de plazo ","celda01",ComunesPDF.CENTER);
			   pdfDoc.setLCell("Tipo de Pago ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Estatus ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);				
				pdfDoc.setLHeaders();


				while (rs.next()){
					nombreEpo         = (rs.getString("NOMBRE_EPO")        == null) ? "" : rs.getString("NOMBRE_EPO");
					nombrePYME        = (rs.getString("NOMBRE_PYME")       == null) ? "" : rs.getString("NOMBRE_PYME");
					num_acuse         = (rs.getString("NUM_ACUSE_CARGA")   == null) ? "" : rs.getString("NUM_ACUSE_CARGA");
					num_docto_inicial = (rs.getString("NUM_DOCTO_INICIAL") == null) ? "" : rs.getString("NUM_DOCTO_INICIAL");
					fecha_publicacion = (rs.getString("FECHA_PUBLICACION") == null) ? "" : rs.getString("FECHA_PUBLICACION");
					fecha_Emision     = (rs.getString("FECHA_EMISION")     == null) ? "" : rs.getString("FECHA_EMISION");
					FechaVencimiento  = (rs.getString("FECHA_VENCIMIENTO") == null) ? "" : rs.getString("FECHA_VENCIMIENTO");
					plazo_docto       = (rs.getString("PLAZO_DOCTO")       == null) ? "" : rs.getString("PLAZO_DOCTO");
					moneda            = (rs.getString("MONEDA")            == null) ? "" : rs.getString("MONEDA");
					monto             = (rs.getString("MONTO")             == null) ? "" : rs.getString("MONTO");
					plazo_desc        = (rs.getString("PLAZO_DESC_DIAS")   == null) ? "" : rs.getString("PLAZO_DESC_DIAS");
					porc_des          = (rs.getString("PORCENTAJE_DESC")   == null) ? "" : rs.getString("PORCENTAJE_DESC");
					monto_desc        = (rs.getString("MONTO_DESCUENTO")   == null) ? "0": rs.getString("MONTO_DESCUENTO");
					tipo_convenio     = (rs.getString("TIPO_CONVENIO")     == null) ? "" : rs.getString("TIPO_CONVENIO");
					tipo_cambio       = (rs.getString("TIPO_CAMBIO")       == null) ? "" : rs.getString("TIPO_CAMBIO");
					monto_valuado     = (rs.getString("MONTO_VALUADO")     == null) ? "" : rs.getString("MONTO_VALUADO");
					modalidad_plazo   = (rs.getString("MODALIDAD_PLAZO")   == null) ? "" : rs.getString("MODALIDAD_PLAZO");
					estatus           = (rs.getString("ESTATUS")           == null) ? "" : rs.getString("ESTATUS");
					ic_moneda_linea   = (rs.getString("IC_MONEDA_LINEA")   == null) ? "" : rs.getString("IC_MONEDA_LINEA");
					ic_moneda         = (rs.getString("IC_MONEDA")         == null) ? "" : rs.getString("IC_MONEDA");
					ventaCartera      = (rs.getString("CG_VENTACARTERA")   == null) ? "" : rs.getString("CG_VENTACARTERA");
				   String tipoPago = (rs.getString("TIPOPAGO") == null) ? "" : rs.getString("TIPOPAGO"); 
					
					pdfDoc.setLCell(" A ","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(nombreEpo,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(nombrePYME,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(num_acuse,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(num_docto_inicial,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_publicacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_Emision,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(FechaVencimiento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(plazo_docto,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(String.valueOf(monto), 2),"formas",ComunesPDF.RIGHT);

					pdfDoc.setLCell(" B ","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(plazo_desc,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(porc_des+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(String.valueOf(monto_desc), 2),"formas",ComunesPDF.RIGHT);

					if(ic_moneda.equals(ic_moneda_linea)){
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
					}else {
						pdfDoc.setLCell(tipo_convenio,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(tipo_cambio,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(String.valueOf(monto_valuado), 2),"formas",ComunesPDF.RIGHT);
					}
					if(ventaCartera.equals("S")){
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
					} else{
						pdfDoc.setLCell(modalidad_plazo,"formas",ComunesPDF.CENTER);
					}
				   pdfDoc.setLCell(tipoPago,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(estatus,"formas",ComunesPDF.CENTER);					
					pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);

					countReg++;
					//System.out.println("Registros procesados: " + countReg);
				}

				pdfDoc.addLTable();
				pdfDoc.endDocument();

			} catch(Throwable e){
				throw new AppException("Error al generar el archivo PDF. ", e);
			}


		}

		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}

	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		return "";
	}

		/**
	 * metodo para crear el combo de estatus 
	 * @return 
	 */
	
	public List  catEstatusData( ){
		log.info("catEstatusData (E) ");
		AccesoDB con = new AccesoDB(); 
		ResultSet rs = null;	
		HashMap datos =  new HashMap();
		List registros  = new ArrayList();
		try{
			con.conexionDB(); 
					
		String 	strSQL =	" select 'D' || ic_estatus_docto as clave , "+
									" cd_descripcion as descripcion "+
									" FROM comcat_estatus_docto "+								
									" where ic_estatus_docto  in(1, 2, 3, 5, 9, 11,24,28,30) " +
									" union all "+
									" select 'C'|| ic_estatus_solic as clave , "+
									" cd_descripcion as descripcion "+
									" FROM comcat_estatus_solic "+
									" where ic_estatus_solic  in(1, 2, 3, 4, 10 ) " +
									" union all "+									
									" select 'C' || ic_estatus_docto as clave , "+
									" cd_descripcion as descripcion "+
									" FROM comcat_estatus_docto "+								
									" where ic_estatus_docto  in(32) " +
									" order by 1 "; 
									
			log.debug(" strSQL  "+strSQL );
			rs =con.queryDB(strSQL);				
			while(rs.next()) {			
				datos = new HashMap();			
				datos.put("clave", rs.getString("clave") );
				datos.put("descripcion", rs.getString("descripcion"));
				registros.add(datos);
			}
			rs.close();
			con.cierraStatement();
		
		} catch (Exception e) {
			log.error("catEstatusData  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("catEstatusData (S) ");
		return registros;
	}
		/**
	 * metodo para crear el combo de BIN's 
	 * @return 
	 */
	public Registros  catBINSData( ){
		log.info("catBINSData (E) ");
		AccesoDB con = new AccesoDB(); 
		ResultSet rs = null;	
		HashMap datos =  new HashMap();
		Registros registros  = new Registros();
		conditions 		= new ArrayList();  
		try{
			con.conexionDB();
					
			String 	strSQL =	" SELECT \n"+
									"	BINS.IC_BINS AS clave,  \n"+
									"	BINS.CG_CODIGO_BIN||' '||BINS.CG_DESCRIPCION AS descripcion \n"+
									"FROM  \n"+
									"	COM_BINS_IF BINS \n"+
									"WHERE \n"+
									"	BINS.IC_IF=? AND  \n"+
									"	BINS.CG_ESTATUS='A'  \n"+
									"ORDER BY 1 ";  
			conditions.add(ic_if);						
			log.debug(" strSQL  "+strSQL );
			log.debug(" conditions  "+conditions );
			registros = con.consultarDB(strSQL,conditions);
		} catch (Exception e) {
			log.error("catBINSData  Error: " + e);
			e.printStackTrace();
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("catBINSData (S) ");
		return registros;
	}
		
	
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getIc_documento() {
		return ic_documento;
	}

	public void setIc_documento(String ic_documento) {
		this.ic_documento = ic_documento;
	}

	public String getIc_pyme() {
		return ic_pyme;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}

	public String getTipo_credito() {
		return tipo_credito;
	}

	public void setTipo_credito(String tipo_credito) {
		this.tipo_credito = tipo_credito;
	}

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getFecha_seleccion_de() {
		return fecha_seleccion_de;
	}

	public void setFecha_seleccion_de(String fecha_seleccion_de) {
		this.fecha_seleccion_de = fecha_seleccion_de;
	}

	public String getFecha_seleccion_a() {
		return fecha_seleccion_a;
	}

	public void setFecha_seleccion_a(String fecha_seleccion_a) {
		this.fecha_seleccion_a = fecha_seleccion_a;
	}

	public String getMonto_credito_de() {
		return monto_credito_de;
	}

	public void setMonto_credito_de(String monto_credito_de) {
		this.monto_credito_de = monto_credito_de;
	}

	public String getMonto_credito_a() {
		return monto_credito_a;
	}

	public void setMonto_credito_a(String monto_credito_a) {
		this.monto_credito_a = monto_credito_a;
	}

	public String getIg_numero_docto() {
		return ig_numero_docto;
	}

	public void setIg_numero_docto(String ig_numero_docto) {
		this.ig_numero_docto = ig_numero_docto;
	}

	public String getFecha_vto_credito_de() {
		return fecha_vto_credito_de;
	}

	public void setFecha_vto_credito_de(String fecha_vto_credito_de) {
		this.fecha_vto_credito_de = fecha_vto_credito_de;
	}

	public String getFecha_vto_credito_a() {
		return fecha_vto_credito_a;
	}

	public void setFecha_vto_credito_a(String fecha_vto_credito_a) {
		this.fecha_vto_credito_a = fecha_vto_credito_a;
	}

	public String getFecha_vto_de() {
		return fecha_vto_de;
	}

	public void setFecha_vto_de(String fecha_vto_de) {
		this.fecha_vto_de = fecha_vto_de;
	}

	public String getFecha_vto_a() {
		return fecha_vto_a;
	}

	public void setFecha_vto_a(String fecha_vto_a) {
		this.fecha_vto_a = fecha_vto_a;
	}

	public String getIc_tipo_cobro_int() {
		return ic_tipo_cobro_int;
	}

	public void setIc_tipo_cobro_int(String ic_tipo_cobro_int) {
		this.ic_tipo_cobro_int = ic_tipo_cobro_int;
	}

	public String getFecha_publicacion_de() {
		return fecha_publicacion_de;
	}

	public void setFecha_publicacion_de(String fecha_publicacion_de) {
		this.fecha_publicacion_de = fecha_publicacion_de;
	}

	public String getFecha_publicacion_a() {
		return fecha_publicacion_a;
	}

	public void setFecha_publicacion_a(String fecha_publicacion_a) {
		this.fecha_publicacion_a = fecha_publicacion_a;
	}

	public String getFecha_emision_de() {
		return fecha_emision_de;
	}

	public void setFecha_emision_de(String fecha_emision_de) {
		this.fecha_emision_de = fecha_emision_de;
	}

	public String getFecha_emision_a() {
		return fecha_emision_a;
	}

	public void setFecha_emision_a(String fecha_emision_a) {
		this.fecha_emision_a = fecha_emision_a;
	}

	public String getIc_moneda() {
		return ic_moneda;
	}

	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}

	public String getCc_acuse() {
		return cc_acuse;
	}

	public void setCc_acuse(String cc_acuse) {
		this.cc_acuse = cc_acuse;
	}

	public String getFn_monto_de() {
		return fn_monto_de;
	}

	public void setFn_monto_de(String fn_monto_de) {
		this.fn_monto_de = fn_monto_de;
	}

	public String getFn_monto_a() {
		return fn_monto_a;
	}

	public void setFn_monto_a(String fn_monto_a) {
		this.fn_monto_a = fn_monto_a;
	}

	public String getMonto_con_descuento() {
		return monto_con_descuento;
	}

	public void setMonto_con_descuento(String monto_con_descuento) {
		this.monto_con_descuento = monto_con_descuento;
	}

	public String getChk_proc_desc() {
		return chk_proc_desc;
	}

	public void setChk_proc_desc(String chk_proc_desc) {
		this.chk_proc_desc = chk_proc_desc;
	}

	public String getSolo_cambio() {
		return solo_cambio;
	}

	public void setSolo_cambio(String solo_cambio) {
		this.solo_cambio = solo_cambio;
	}

	public String getModo_plazo() {
		return modo_plazo;
	}

	public void setModo_plazo(String modo_plazo) {
		this.modo_plazo = modo_plazo;
	}

	public String getTipoConsulta() {
		return tipoConsulta;
	}

	public void setTipoConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}

	public String getIc_estatus() {
		return ic_estatus;
	}

	public void setIc_estatus(String ic_estatus) {
		this.ic_estatus = ic_estatus;
	}

	public int getCountReg() {
		return countReg;
	}
	
	public String getIc_tipo_Pago() {
			return ic_tipo_Pago;
	}

	public void setIc_tipo_Pago(String ic_tipo_Pago) {
		this.ic_tipo_Pago = ic_tipo_Pago;
	}
		 

}