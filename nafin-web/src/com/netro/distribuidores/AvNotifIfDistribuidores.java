package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import org.apache.commons.logging.Log;

/**
 * FODEA 11-2015.
 * Esta clase se crea para generar el archivo PDF de la p�gina: ADMIN IF Distribuidores / Avisos de notificaci�n.
 * Aunque implementa la interfaz IQueryGeneratorRegExtJS, la p�gina ya utiliza la clase AvNotifIfDist.java para realizar sus consultas.
 * Esta clase solo utiliza los m�todos necesarios para crear el PDF sin utilizar paginaci�n, puede utilizarse en futuras ocaciones
 * para actualizar la pantalla completamente.
 * BY: Agust�n Bautista Ruiz
 */
public class AvNotifIfDistribuidores implements IQueryGeneratorRegExtJS{

	private static final Log log = ServiceLocator.getInstance().getLog(AvNotifIfDistribuidores.class);
	private List conditions;
	private String ic_epo;
	private String ic_pyme;
	private String ic_if;
	private String fecha_ini;
	private String fecha_fin;
	private String tipoConsulta;
	private String numOrden;
	private String tipoCredito;
	private String perfil;

	public AvNotifIfDistribuidores(){}

	public String getDocumentQuery() {
		return null;
	}

	public String getDocumentSummaryQueryForIds(List ids) {
		return null;
	}

	public String getAggregateCalculationQuery() {
		return null;
	}

	public String getDocumentQueryFile() {

		log.info("getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion    = new StringBuffer();
		conditions = new ArrayList();
		if(!tipoConsulta.equals("")&&tipoConsulta.equals("Operada TC"))
			condicion.append("AND ed.ic_estatus_docto = 32 " );
		condicion.append(" AND D.ic_estatus_docto in(4,11,32)  ");
		if(!ic_epo.equals(""))   
			condicion.append("AND D.IC_EPO =  " + ic_epo);
		if(!ic_pyme.equals(""))  
			condicion.append(" AND D.IC_PYME =  " +   ic_pyme) ;
		if (!fecha_ini.equals("") && !fecha_fin.equals(""))
			condicion.append(" AND trunc(C3.df_fecha_hora) BETWEEN TO_DATE('"+fecha_ini+"','DD/MM/YYYY') and TO_DATE('"+fecha_fin+"','DD/MM/YYYY') ");
		if(ic_epo.equals("") && ic_pyme.equals("") && fecha_ini.equals("") && fecha_fin.equals(""))
			condicion.append(" AND TO_CHAR(C3.df_fecha_hora,'DD/MM/YYYY')=TO_CHAR(SYSDATE,'DD/MM/YYYY') ");

		if(numOrden.equals("")){
			qrySentencia.append(
			"  SELECT /*+ use_nl(D,DS,S,C3,LCD,P,PE,PN,M,TC,TCI,IEP,CT,E,ED,CI,TF)"   +
			"   index(d cp_dis_documento_pk)"   +
			"   index(m cp_comcat_moneda_pk)*/"   +
			"  P.cg_razon_social AS DISTRIBUIDOR"   +
			"  ,D.ig_numero_docto"   +
			"  ,D.cc_acuse"   +
			"  ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') AS df_fecha_emision"   +
			"  ,TO_CHAR(D.df_carga,'dd/mm/yyyy') AS df_fecha_publicacion"   +
			"  ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') AS df_fecha_venc"   +
			"  ,D.IG_PLAZO_DOCTO"   +
			"  ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') AS df_fecha_seleccion"   +
			"  ,M.cd_nombre AS MONEDA"   +
			"  ,M.ic_moneda"   +
			"  ,D.fn_monto"   +
			"  ,0 as COMISION_APLICABLE"   +
			"  ,'' as MONTO_DEPOSITAR"   +
			"  ,'' as MONTO_COMISION"   +
			"  ,'' as IC_ORDEN_ENVIADO"   +
			"  ,'' as NUMERO_TC"   +
			"  ,'' as IC_OPERACION_CCACUSE"   +
			"  ,'' AS codigo_autorizado"   +
			"  ,'' as FECHA_REGISTRO"   +
			"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') AS TIPO_CONVERSION"   +
			"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
			"  ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) AS MONTO_VALUADO"   +
			"  ,D.IG_PLAZO_DESCUENTO"   +
			"  ,D.FN_MONTO"   +
			"  ,D.fn_porc_descuento"   +
			"  ,D.fn_monto*(NVL(D.fn_porc_descuento,0)/100) AS MONTO_DESCUENTO"   +
			"  ,ED.CD_DESCRIPCION AS ESTATUS"   +
			"  ,0 AS ic_estatus_docto"   +
			"  ,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) AS REFERENCIA_INT" +
			"  ,DS.fn_valor_tasa  AS VALOR_TASA"   +
			"  ,D.IG_PLAZO_CREDITO"   +
			"  ,DS.fn_importe_recibir AS MONTO_CREDITO"   +
			"  ,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') AS DF_FECHA_VENC_CREDITO"   +
			"  ,TCI.ic_tipo_cobro_interes"   +
			"  ,TCI.cd_descripcion AS tipo_cobro_interes"   +
			"  ,DS.fn_importe_interes AS MONTO_INTERES"   +
			"  ,D.ic_documento"   +
			"  ,E.cg_razon_social AS EPO"   +
			"  , D.ic_documento"   +
			"  ,CI.cg_razon_social AS DIF"   +
			"  ,TF.cd_descripcion AS MODO_PLAZO"   +
			"  ,TO_CHAR(C3.df_fecha_hora,'dd/mm/yyyy') AS df_fecha_hora"   +
			"  ,LCD.ic_moneda AS moneda_linea"   +
			" , pn.cg_tipo_conversion as cg_tipo_conversion "+
			"    , p.ic_pyme as ic_pyme  "+
			"   , e.ic_epo  as ic_epo "+
			"   , DS.IC_TASA as IC_TASA"+
			"  ,'' as cg_descripcion_bins"   +
			" , D.IG_TIPO_PAGO "+
			"   FROM DIS_DOCUMENTO D"   +
			"  ,DIS_DOCTO_SELECCIONADO DS"   +
			"  ,DIS_SOLICITUD S"   +
			"  ,COM_ACUSE3 C3"   +
			"  ,DIS_LINEA_CREDITO_DM LCD"   +
			"  ,COMCAT_PYME P"   +
			"  ,COMREL_PRODUCTO_EPO PE"   +
			"  ,COMCAT_PRODUCTO_NAFIN PN"   +
			"  ,COMCAT_MONEDA M"   +
			"  ,COM_TIPO_CAMBIO TC"   +
			"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
			"  ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
			"  ,COMCAT_TASA CT"   +
			"  ,COMCAT_EPO E"   +
			"  ,COMCAT_ESTATUS_DOCTO ED"   +
			"  ,COMCAT_IF CI"   +
			"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
			"  WHERE D.IC_EPO = PE.IC_EPO"   +
			"  AND D.IC_PYME = P.IC_PYME"   +
			"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
			"  AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM"   +
			"  AND D.IC_EPO = PE.IC_EPO"   +
			"  AND D.IC_MONEDA = M.IC_MONEDA"   +
			"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
			"  AND D.IC_EPO = E.IC_EPO"   +
			"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
			"  AND D.IC_EPO = IEP.IC_EPO"   +
			"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
			"  AND DS.IC_TASA = CT.IC_TASA"   +
			"  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"  AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"  AND LCD.IC_IF = IEP.IC_IF"   +
			"  AND LCD.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES"   +
			"  AND S.CC_ACUSE = C3.CC_ACUSE"   +
			"  AND LCD.IC_IF = CI.IC_IF"   +
			"  AND M.IC_MONEDA = TC.IC_MONEDA"   +
			"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
			"  AND PN.IC_PRODUCTO_NAFIN = 4"   +
			"  AND LCD.IC_IF = "+ic_if+
			"  AND E.cs_habilitado = 'S' " +
			condicion.toString());
			qrySentencia.append(" UNION ALL");
			qrySentencia.append(
			"  SELECT /*+ use_nl(D,DS,S,C3,LCD,P,PE,PN,M,TC,TCI,IEP,CT,E,ED,CI,TF)"   +
			"   index(d cp_dis_documento_pk)"   +
			"   index(m cp_comcat_moneda_pk)*/"   +
			"  P.cg_razon_social AS DISTRIBUIDOR"   +
			"  ,D.ig_numero_docto"   +
			"  ,D.cc_acuse"   +
			"  ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') AS df_fecha_emision"   +
			"  ,TO_CHAR(D.df_carga,'dd/mm/yyyy') AS df_fecha_publicacion"   +
			"  ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') AS df_fecha_venc"   +
			"  ,D.IG_PLAZO_DOCTO"   +
			"  ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') AS df_fecha_seleccion"   +
			"  ,M.cd_nombre AS MONEDA"   +
			"  ,M.ic_moneda"   +
			"  ,D.fn_monto"   +
			"  ,DS.FN_PORC_COMISION_APLI as COMISION_APLICABLE"   +/////////////////7
			"  ,'' as MONTO_DEPOSITAR"   +
			"  ,'' as MONTO_COMISION"   +
			"  ,DDPTC.IC_ORDEN_PAGO_TC as IC_ORDEN_ENVIADO"   +
			"  ,DDPTC.CG_NUM_TARJETA as NUMERO_TC"   +
			"  ,DDPTC.CG_TRANSACCION_ID as IC_OPERACION_CCACUSE"   +
			"  ,decode(CONCAT(ddptc.CG_CODIGO_RESP ,ddptc.CG_CODIGO_DESC),'','',ddptc.CG_CODIGO_RESP||'-' ||ddptc.CG_CODIGO_DESC)AS codigo_autorizado"   +
			"  ,TO_CHAR(DDPTC.DF_FECHA_AUTORIZACION,'dd/mm/yyyy') as FECHA_REGISTRO"   +////////////////
			"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') AS TIPO_CONVERSION"   +
			"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
			"  ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) AS MONTO_VALUADO"   +
			"  ,D.IG_PLAZO_DESCUENTO"   +
			"  ,D.FN_MONTO"   +
			"  ,D.fn_porc_descuento"   +
			"  ,D.fn_monto*(NVL(D.fn_porc_descuento,0)/100) AS MONTO_DESCUENTO"   +
			"  ,ED.CD_DESCRIPCION AS ESTATUS"   +
			"  ,ED.IC_ESTATUS_DOCTO AS IC_ESTATUS_DOCTO "+
			"  ,CT.CD_NOMBRE||' '||ds.cg_rel_mat||' '||ds.fn_puntos AS REFERENCIA_INT"   +
			"  ,DECODE(DS.CG_REL_MAT"   +
			"        ,'+',DS.fn_valor_tasa + DS.fn_puntos"   +
			"        ,'-',DS.fn_valor_tasa - DS.fn_puntos"   +
			"        ,'*',DS.fn_valor_tasa * DS.fn_puntos"   +
			"        ,'/',DS.fn_valor_tasa / DS.fn_puntos"   +
			"        ,0) AS VALOR_TASA"   +
			"  ,D.IG_PLAZO_CREDITO"   +
			"  ,DS.fn_importe_recibir AS MONTO_CREDITO"   +
			"  ,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') AS DF_FECHA_VENC_CREDITO"   +
			"  ,TCI.ic_tipo_cobro_interes"   +
			"  ,TCI.cd_descripcion AS tipo_cobro_interes"   +
			"  ,DS.fn_importe_interes AS MONTO_INTERES"   +
			"  ,D.ic_documento"   +
			"  ,E.cg_razon_social AS EPO"   +
			"  , D.ic_documento"   +
			"  ,decode(ed.ic_estatus_docto,32,ci2.cg_razon_social,ci.cg_razon_social) AS dif"   +
			"  ,TF.cd_descripcion AS MODO_PLAZO"   +
			"  ,TO_CHAR(C3.df_fecha_hora,'dd/mm/yyyy') AS df_fecha_hora"   +
			"  ,LCD.ic_moneda AS moneda_linea"   +
			" , pn.cg_tipo_conversion as cg_tipo_conversion "+
			"    , p.ic_pyme as ic_pyme  "+
			"   , e.ic_epo  as ic_epo "+
			"   , DS.IC_TASA as IC_TASA,   cbi.CG_CODIGO_BIN||' ' ||cbi.cg_descripcion AS cg_descripcion_bins"+
			" , D.IG_TIPO_PAGO "+
			"   FROM DIS_DOCUMENTO D"   +
			"  ,DIS_DOCTOS_PAGO_TC DDPTC"   +
			"  ,COM_BINS_IF cbi "+
			"  ,comcat_if ci2 "+
			"  ,DIS_DOCTO_SELECCIONADO DS"   +
			"  ,DIS_SOLICITUD S"   +
			"  ,COM_ACUSE3 C3"   +
			"  ,COM_LINEA_CREDITO LCD"   +
			"  ,COMCAT_PYME P"   +
			"  ,COMREL_PRODUCTO_EPO PE"   +
			"  ,COMCAT_PRODUCTO_NAFIN PN"   +
			"  ,COMCAT_MONEDA M"   +
			"  ,COM_TIPO_CAMBIO TC"   +
			"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
			"  ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
			"  ,COMCAT_TASA CT"   +
			"  ,COMCAT_EPO E"   +
			"  ,COMCAT_ESTATUS_DOCTO ED"   +
			"  ,COMCAT_IF CI"   +
			"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
			"  WHERE D.IC_EPO = PE.IC_EPO"   +
			"  AND D.IC_ORDEN_PAGO_TC = DDPTC.IC_ORDEN_PAGO_TC(+)"   +
			"  AND D.IC_PYME = P.IC_PYME"   +
			"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
			"  AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
			"  AND D.IC_EPO = PE.IC_EPO"   +
			"  AND D.IC_MONEDA = M.IC_MONEDA"   +
			"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
			"  AND D.IC_EPO = E.IC_EPO"   +
			"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
			"  AND D.IC_EPO = IEP.IC_EPO"   +
			"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
			"  AND DS.IC_TASA = CT.IC_TASA"   +
			"  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"  AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"  AND (LCD.IC_IF = IEP.IC_IF OR cbi.IC_IF = IEP.IC_IF) "   +
			"  AND (LCD.IC_IF ="+ic_if+" OR cbi.IC_IF = "+ic_if+") "   +
			"  AND LCD.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES"   +
			"  AND S.CC_ACUSE = C3.CC_ACUSE"   +
			"  AND d.IC_BINS = cbi.IC_BINS(+)"  +
			"  AND cbi.IC_if = ci2.IC_if(+)"  +
			"  AND LCD.IC_IF = CI.IC_IF"   +
			"  AND M.IC_MONEDA = TC.IC_MONEDA"   +
			"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
			"  AND PN.IC_PRODUCTO_NAFIN = 4"  +
			" AND E.cs_habilitado = 'S' " +
			condicion.toString());
			if(!numOrden.equals(""))
				condicion.append(" and ddptc.ic_orden_pago_tc = " + numOrden) ;
		}else{
			qrySentencia.append(
			"  SELECT /*+ use_nl(D,DS,S,C3,LCD,P,PE,PN,M,TC,TCI,IEP,CT,E,ED,CI,TF)"   +
			"   index(d cp_dis_documento_pk)"   +
			"   index(m cp_comcat_moneda_pk)*/"   +
			"  P.cg_razon_social AS DISTRIBUIDOR"   +
			"  ,D.ig_numero_docto"   +
			"  ,D.cc_acuse"   +
			"  ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') AS df_fecha_emision"   +
			"  ,TO_CHAR(D.df_carga,'dd/mm/yyyy') AS df_fecha_publicacion"   +
			"  ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') AS df_fecha_venc"   +
			"  ,D.IG_PLAZO_DOCTO"   +
			"  ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') AS df_fecha_seleccion"   +
			"  ,M.cd_nombre AS MONEDA"   +
			"  ,M.ic_moneda"   +
			"  ,D.fn_monto"   +
			"  ,DS.FN_PORC_COMISION_APLI as COMISION_APLICABLE"   +
			"  ,'' as MONTO_DEPOSITAR"   +
			"  ,'' as MONTO_COMISION"   +
			"  ,DDPTC.IC_ORDEN_PAGO_TC as IC_ORDEN_ENVIADO"   +
			"  ,DDPTC.CG_NUM_TARJETA as NUMERO_TC"   +
			"  ,DDPTC.CG_TRANSACCION_ID as IC_OPERACION_CCACUSE"   +
			"  ,decode(CONCAT(ddptc.CG_CODIGO_RESP ,ddptc.CG_CODIGO_DESC),'','',ddptc.CG_CODIGO_RESP||'-' ||ddptc.CG_CODIGO_DESC)AS codigo_autorizado"   +
			"  ,TO_CHAR(DDPTC.DF_FECHA_AUTORIZACION,'dd/mm/yyyy') as FECHA_REGISTRO"   +
			"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') AS TIPO_CONVERSION"   +
			"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
			"  ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) AS MONTO_VALUADO"   +
			"  ,D.IG_PLAZO_DESCUENTO"   +
			"  ,D.FN_MONTO"   +
			"  ,D.fn_porc_descuento"   +
			"  ,D.fn_monto*(NVL(D.fn_porc_descuento,0)/100) AS MONTO_DESCUENTO"   +
			"  ,ED.CD_DESCRIPCION AS ESTATUS"   +
			"  ,ED.IC_ESTATUS_DOCTO AS IC_ESTATUS_DOCTO "+
			"  ,CT.CD_NOMBRE||' '||ds.cg_rel_mat||' '||ds.fn_puntos AS REFERENCIA_INT"   +
			"  ,DECODE(DS.CG_REL_MAT"   +
			"        ,'+',DS.fn_valor_tasa + DS.fn_puntos"   +
			"        ,'-',DS.fn_valor_tasa - DS.fn_puntos"   +
			"        ,'*',DS.fn_valor_tasa * DS.fn_puntos"   +
			"        ,'/',DS.fn_valor_tasa / DS.fn_puntos"   +
			"        ,0) AS VALOR_TASA"   +
			"  ,D.IG_PLAZO_CREDITO"   +
			"  ,DS.fn_importe_recibir AS MONTO_CREDITO"   +
			"  ,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') AS DF_FECHA_VENC_CREDITO"   +
			"  ,TCI.ic_tipo_cobro_interes"   +
			"  ,TCI.cd_descripcion AS tipo_cobro_interes"   +
			"  ,DS.fn_importe_interes AS MONTO_INTERES"   +
			"  ,D.ic_documento"   +
			"  ,E.cg_razon_social AS EPO"   +
			"  , D.ic_documento"   +
			"  ,decode(ed.ic_estatus_docto,32,ci2.cg_razon_social,ci.cg_razon_social) AS dif"   +
			"  ,TF.cd_descripcion AS MODO_PLAZO"   +
			"  ,TO_CHAR(C3.df_fecha_hora,'dd/mm/yyyy') AS df_fecha_hora"   +
			"  ,LCD.ic_moneda AS moneda_linea"   +
			" , pn.cg_tipo_conversion as cg_tipo_conversion "+
			"    , p.ic_pyme as ic_pyme  "+
			"   , e.ic_epo  as ic_epo "+
			"   , DS.IC_TASA as IC_TASA,   cbi.CG_CODIGO_BIN||' ' ||cbi.cg_descripcion AS cg_descripcion_bins"+
			" , D.IG_TIPO_PAGO "+
			"   FROM DIS_DOCUMENTO D"   +
			"  ,DIS_DOCTOS_PAGO_TC DDPTC"   +
			"  ,COM_BINS_IF cbi "+
			"  ,comcat_if ci2 "+
			"  ,DIS_DOCTO_SELECCIONADO DS"   +
			"  ,DIS_SOLICITUD S"   +
			"  ,COM_ACUSE3 C3"   +
			"  ,COM_LINEA_CREDITO LCD"   +
			"  ,COMCAT_PYME P"   +
			"  ,COMREL_PRODUCTO_EPO PE"   +
			"  ,COMCAT_PRODUCTO_NAFIN PN"   +
			"  ,COMCAT_MONEDA M"   +
			"  ,COM_TIPO_CAMBIO TC"   +
			"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
			"  ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
			"  ,COMCAT_TASA CT"   +
			"  ,COMCAT_EPO E"   +
			"  ,COMCAT_ESTATUS_DOCTO ED"   +
			"  ,COMCAT_IF CI"   +
			"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
			"  WHERE D.IC_EPO = PE.IC_EPO"   +
			"  AND D.IC_ORDEN_PAGO_TC = DDPTC.IC_ORDEN_PAGO_TC(+)"   +
			"  AND D.IC_PYME = P.IC_PYME"   +
			"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
			"  AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
			"  AND D.IC_EPO = PE.IC_EPO"   +
			"  AND D.IC_MONEDA = M.IC_MONEDA"   +
			"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
			"  AND D.IC_EPO = E.IC_EPO"   +
			"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
			"  AND D.IC_EPO = IEP.IC_EPO"   +
			"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
			"  AND DS.IC_TASA = CT.IC_TASA"   +
			"  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"  AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"  AND (LCD.IC_IF = IEP.IC_IF OR cbi.IC_IF = IEP.IC_IF) "   +
			"  AND (LCD.IC_IF ="+ic_if+" OR cbi.IC_IF = "+ic_if+") "   +
			"  AND LCD.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES"   +
			"  AND S.CC_ACUSE = C3.CC_ACUSE"   +
			"  AND d.IC_BINS = cbi.IC_BINS(+)"  +
			"  AND cbi.IC_if = ci2.IC_if(+)"  +
			"  AND LCD.IC_IF = CI.IC_IF"   +
			"  AND M.IC_MONEDA = TC.IC_MONEDA"   +
			"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
			"  AND PN.IC_PRODUCTO_NAFIN = 4"  +
			"  AND E.cs_habilitado = 'S' " );
			if(!numOrden.equals(""))
				condicion.append(" and ddptc.ic_orden_pago_tc = " + numOrden) ;
			qrySentencia.append(condicion.toString());
			
		}
		log.info("qrySentencia: " + qrySentencia.toString());
		//log.info("conditions: " + conditions.toString());
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();
	}

	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo) {

		log.info("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();

		String epo              = "";
		String distribuidor     = "";
		String numdocto         = "";
		String acuse            = "";
		String fchemision       = "";
		String fchpublica       = "";
		String fchvence         = "";
		String plazodocto       = "";
		String ic_moneda        = "";
		String moneda           = "";
		String tipoconversion   = "";
		String tipocambio       = "";
		String plzodescto       = "";
		String porcdescto       = "";
		String estatus          = "";
		String numcredito       = "";
		String descif           = "";
		String referencia       = "";
		String plazocred        = "";
		String fchvenccred      = "";
		String tipocobroint     = "";
		String modoPlazo        = "";
		String valortaza        = "";
		String fchopera         = "";
		String monedaLinea      = "";
		String tipoPago         = "";
		double mntovaluado      = 0;
		double mntodescuento    = 0;
		double montoint         = 0;
		double mntocredito      = 0;
		double imontoTot        = 0;
		double imontoValTot     = 0;
		double imontodescTot    = 0;
		double imontocredTot    = 0;
		double imontotasaTot    = 0;
		double monto            = 0;
		double imontoTotDls     = 0;
		double imontoValTotDls  = 0;
		double imontodescTotDls = 0;
		double imontocredTotDls = 0;
		double imontotasaTotDls = 0;
		int    totalDoctosMN    = 0;
		int    totalDoctosUSD   = 0;
		int    numreg           = 0;
		int    numCols          = 19;
	        String operaContrato     = "";

		try{
			if(tipo.equals("PDF")){
                                
			    ParametrosDist BeanParametro = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
			    if(!ic_epo.equals("")){
                                operaContrato = BeanParametro.obtieneOperaSinCesion(ic_epo); //PARAMETRO NUEVO JAGE 14012017
                            
                            }   
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				
			    String leyenda = "";
			               if(operaContrato.equals("S")){
			                      leyenda = "Las operaciones que aparecen en esta pantalla le ser�n financiadas al DISTRIBUIDOR exclusivamente con el fin de pagar " +
                                                        "a la EMPRESA DE PRIMER ORDEN las cuentas pendientes de pago. Los derechos de cobro de los financiamientos que amparan " +
                                                        "dichas operaciones fueron cedidos a Nacional Financiera, S.N.C., para todos los efectos a que haya lugar.";
			                  }
                           else{
                            leyenda = "Las operaciones que aparecen en esta pantalla fueron generadas por el CLIENTE o DISTRIBUIDOR y a su vez notificadas a la EMPRESA DE PRIMER ORDEN.";   
                            }
                           
            leyenda +=  
                    "\nPor otra parte, a partir del 17 de octubre del 2018, la EMPRESA DE PRIMER ORDEN ha manifestado bajo protesta de decir verdad, que s� ha emitido o emitir� a su CLIENTE o DISTRIBUIDOR "+
                    "el CFDI por la operaci�n comercial que le dio origen a esta transacci�n, seg�n sea el caso y conforme establezcan las disposiciones fiscales vigentes.";
			    pdfDoc.addText(leyenda,"formas",ComunesPDF.LEFT);
                                
                                pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

				while (rs.next()){

					ic_moneda      = (rs.getString("ic_moneda")           ==null)?"":rs.getString("ic_moneda");
					distribuidor   = (rs.getString("DISTRIBUIDOR")        ==null)?"":rs.getString("DISTRIBUIDOR");
					numdocto       = (rs.getString("ig_numero_docto")     ==null)?"":rs.getString("ig_numero_docto");
					acuse          = (rs.getString("cc_acuse")            ==null)?"":rs.getString("cc_acuse");
					fchemision     = (rs.getString("df_fecha_emision")    ==null)?"":rs.getString("df_fecha_emision");
					fchpublica     = (rs.getString("df_fecha_publicacion")==null)?"":rs.getString("df_fecha_publicacion");
					fchvence       = (rs.getString("df_fecha_venc")       ==null)?"":rs.getString("df_fecha_venc");
					plazodocto     = (rs.getString("IG_PLAZO_DOCTO")      ==null)?"":rs.getString("IG_PLAZO_DOCTO");
					moneda         = (rs.getString("MONEDA")              ==null)?"":rs.getString("MONEDA");
					tipoconversion = (rs.getString("TIPO_CONVERSION")     ==null)?"":rs.getString("TIPO_CONVERSION");
					tipocambio     = (rs.getString("TIPO_CAMBIO")         ==null)?"":rs.getString("TIPO_CAMBIO");
					plzodescto     = (rs.getString("IG_PLAZO_DESCUENTO")  ==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
					porcdescto     = (rs.getString("fn_porc_descuento")   ==null)?"":rs.getString("fn_porc_descuento");
					estatus        = (rs.getString("ESTATUS")             ==null)?"":rs.getString("ESTATUS");
					if(!perfil.equals("IF FACT RECURSO")){
						modoPlazo   = (rs.getString("MODO_PLAZO")          ==null)?"":rs.getString("MODO_PLAZO");
					}
					monedaLinea    = (rs.getString("MONEDA_LINEA")        ==null)?"":rs.getString("MONEDA_LINEA");
					epo            = (rs.getString("EPO")                 ==null)?"":rs.getString("EPO");
					monto          = Double.parseDouble((rs.getString("FN_MONTO")==null)?"0":rs.getString("FN_MONTO"));
					mntodescuento  = Double.parseDouble((rs.getString("MONTO_DESCUENTO")==null)?"0":rs.getString("MONTO_DESCUENTO"));
					mntovaluado    = (monto-mntodescuento)*Double.parseDouble(tipocambio);

					/**********PARA CREDITO**************/
					numcredito   = (rs.getString("ic_documento")         ==null)?"":rs.getString("ic_documento");
					numcredito   = (rs.getString("ic_documento")         ==null)?"":rs.getString("ic_documento");
					descif       = (rs.getString("DIF")                  ==null)?"":rs.getString("DIF");
					referencia   = (rs.getString("REFERENCIA_INT")       ==null)?"":rs.getString("REFERENCIA_INT");
					plazocred    = (rs.getString("IG_PLAZO_CREDITO")     ==null)?"":rs.getString("IG_PLAZO_CREDITO");
					fchvenccred  = (rs.getString("DF_FECHA_VENC_CREDITO")==null)?"":rs.getString("DF_FECHA_VENC_CREDITO");
					tipocobroint = (rs.getString("tipo_cobro_interes")   ==null)?"":rs.getString("tipo_cobro_interes");
					valortaza    = (rs.getString("VALOR_TASA")           ==null)?"":rs.getString("VALOR_TASA");
					fchopera     = (rs.getString("df_fecha_hora")        ==null)?"":rs.getString("df_fecha_hora");
					tipoPago     = (rs.getString("IG_TIPO_PAGO")         ==null)?"":rs.getString("IG_TIPO_PAGO");
					montoint     = Double.parseDouble((rs.getString("MONTO_INTERES")==null)?"0":rs.getString("MONTO_INTERES"));
					mntocredito  = Double.parseDouble((rs.getString("MONTO_CREDITO")==null)?"0":rs.getString("MONTO_CREDITO"));

					if(tipoPago.equals("2")){
						plazocred = plazocred + " (M)";
					}

					if(ic_moneda.equals("1")){
						totalDoctosMN++;
						imontoTot+= monto; 
						imontoValTot+= mntovaluado;
						imontodescTot+= mntodescuento;
						imontocredTot+= mntocredito; 
						imontotasaTot+= montoint;     
					}
					if(ic_moneda.equals("54")){
						totalDoctosUSD++;
						imontoTotDls+= monto;
						if(ic_moneda.equals(monedaLinea))
							imontoValTotDls+= mntovaluado;
						imontodescTotDls+= mntodescuento;
						imontocredTotDls+= mntocredito; 
						imontotasaTotDls+= montoint;
					}

					if(numreg == 0){
						pdfDoc.setLTable(numCols,100);
						pdfDoc.setLCell("Datos del Documento Inicial","celda01",ComunesPDF.CENTER,numCols);
						pdfDoc.setLCell("A","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("EPO","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Dist.","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("No. Docto. Inicial","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("No. Acuse Carga","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha Emisi�n","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha Pub.","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha Venc.","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha Oper.","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Plazo docto.","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Plazo Dscto D�as","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("% Dscto","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto % Dscto","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Tipo conv.","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Tipo cambio","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto valuado","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Modo Plazo","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("B","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Estatus","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("No. Docto Final","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Plazo","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha Venc.","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Ref. Tasa Inter�s","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Valor Tasa Inter�s","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto Inter�s","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Tipo Cobro Inter�s","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("","celda01",ComunesPDF.CENTER,numCols-10);
						pdfDoc.setLHeaders();
					}

					pdfDoc.setLCell("A","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(epo,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(distribuidor,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(numdocto,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(acuse,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fchemision,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fchpublica,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fchvence,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fchopera,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(plazodocto,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(plzodescto,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(porcdescto,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(Comunes.formatoDecimal(mntodescuento,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell((ic_moneda.equals(monedaLinea))?"":tipoconversion,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell((ic_moneda.equals(monedaLinea))?"":tipocambio,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell((ic_moneda.equals(monedaLinea))?"":"$ "+Comunes.formatoDecimal(mntovaluado,2),"formas",ComunesPDF.CENTER);
					if(!perfil.equals("IF FACT RECURSO")){
						pdfDoc.setLCell(modoPlazo,"formas",ComunesPDF.CENTER);
					}

					pdfDoc.setLCell("B","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(estatus,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(numcredito,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(Comunes.formatoDecimal(mntocredito,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(plazocred,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fchvenccred,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(referencia,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(Comunes.formatoDecimal(valortaza,2)+" %","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(Comunes.formatoDecimal(montoint,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipocobroint,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("","formas",ComunesPDF.CENTER,numCols-10);
					numreg++;
				}
				pdfDoc.addLTable();
				pdfDoc.endDocument();

			}
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo. ", e);
		}

		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}

	public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo) {
		return null;
	}

/****************************************
 *         GETTERS AND SETTERS          *
 ****************************************/
	public List getConditions(){
		return conditions; 
	}

	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getIc_pyme() {
		return ic_pyme;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}

	public String getFecha_ini() {
		return fecha_ini;
	}

	public void setFecha_ini(String fecha_ini) {
		this.fecha_ini = fecha_ini;
	}

	public String getFecha_fin() {
		return fecha_fin;
	}

	public void setFecha_fin(String fecha_fin) {
		this.fecha_fin = fecha_fin;
	}

	public String getTipoConsulta() {
		return tipoConsulta;
	}

	public void setTipoConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}

	public String getNumOrden() {
		return numOrden;
	}

	public void setNumOrden(String numOrden) {
		this.numOrden = numOrden;
	}

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getTipoCredito() {
		return tipoCredito;
	}

	public void setTipoCredito(String tipoCredito) {
		this.tipoCredito = tipoCredito;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
}
