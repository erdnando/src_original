package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.math.BigDecimal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class MonitorLineasIfDist implements IQueryGenerator, IQueryGeneratorRegExtJS {
	public MonitorLineasIfDist(){}
	private static final Log log = ServiceLocator.getInstance().getLog(MonitorLineasIfDist.class);

	StringBuffer 	querySentencia;
	private List 	conditions;
	private String paginaOffset;
	private String paginaNo;
	private String	ses_ic_if;
	private String	tipo_credito;
	private String	tipo_solic;
	private String	ic_epo;
	private String	ic_pyme;
	private String	ic_estatus_linea;
	private String	dc_fecha_solic_de;
	private String	dc_fecha_solic_a;
	private String	dc_fecha_auto_de;
	private String	dc_fecha_auto_a;
	
	public String getAggregateCalculationQuery(HttpServletRequest request) {
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		String		 qrySentencia = "";
		StringBuffer qryAux		  = new StringBuffer();
		StringBuffer qryDM		  = new StringBuffer();
		StringBuffer qryCCC		  = new StringBuffer();
		StringBuffer condicionDM    = new StringBuffer();
		StringBuffer condicionCCC    = new StringBuffer();

		String	ses_ic_if            = request.getSession().getAttribute("iNoCliente").toString();
		
		String		tipo_credito		=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String		tipo_solic			=	(request.getParameter("tipo_solic")==null)?"":request.getParameter("tipo_solic");
		String		ic_epo				=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String		ic_pyme				=	(request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String		ic_estatus_linea	=	(request.getParameter("ic_estatus_linea")==null)?"":request.getParameter("ic_estatus_linea");
		String		dc_fecha_solic_de	=	(request.getParameter("dc_fecha_solic_de")==null)?"":request.getParameter("dc_fecha_solic_de");
		String		dc_fecha_solic_a	=	(request.getParameter("dc_fecha_solic_a")==null)?"":request.getParameter("dc_fecha_solic_a");
		String		dc_fecha_auto_de	=	(request.getParameter("dc_fecha_auto_de")==null)?"":request.getParameter("dc_fecha_auto_de");
		String		dc_fecha_auto_a	=	(request.getParameter("dc_fecha_auto_a")==null)?"":request.getParameter("dc_fecha_auto_a");
			
		try {

		if (!"".equals(tipo_solic)) {
			condicionDM.append(" and lc.cg_tipo_solicitud =  '"+tipo_solic+"' ");
			condicionCCC.append(" and lc.cg_tipo_solicitud =  '"+tipo_solic+"' ");
		}
		if (!"".equals(ic_epo)) 
			condicionDM.append(" and lc.ic_epo = "+ic_epo+" ");
		if (!"".equals(ic_pyme)) 
			condicionCCC.append(" and lc.ic_pyme = "+ic_pyme+" ");
		if (!"".equals(ic_estatus_linea)) {
			condicionDM.append(" and lc.ic_estatus_linea =  "+ic_estatus_linea+" ");
			condicionCCC.append(" and lc.ic_estatus_linea =  "+ic_estatus_linea+" ");
		}
        if (!("".equals(dc_fecha_solic_de)) && !("".equals(dc_fecha_solic_a)))
			condicionCCC.append("    AND TRUNC(lc.df_captura) BETWEEN TRUNC(TO_DATE('"+dc_fecha_solic_de+"','dd/mm/yyyy')) AND TRUNC(TO_DATE('"+dc_fecha_solic_a+"','dd/mm/yyyy')) ");
        if (!("".equals(dc_fecha_auto_de)) && !("".equals(dc_fecha_auto_a))) {
			condicionDM.append("    AND TRUNC(ac4.df_acuse) BETWEEN TRUNC(TO_DATE('"+dc_fecha_auto_de+"','dd/mm/yyyy')) AND TRUNC(TO_DATE('"+dc_fecha_auto_a+"','dd/mm/yyyy')) ");
			condicionCCC.append("    AND TRUNC(lc.df_autorizacion_nafin) BETWEEN TRUNC(TO_DATE('"+dc_fecha_solic_de+"','dd/mm/yyyy')) AND TRUNC(TO_DATE('"+dc_fecha_solic_a+"','dd/mm/yyyy')) ");
		}

		qryDM.append(
			"    SELECT /*+ use_nl(lc,ac4)"   +
			"            index(lc in_dis_linea_credito_dm_01_NUK) */"   +
			" 	   cm.ic_moneda,cm.cd_nombre"   +
			" 	   ,lc.fn_monto_autorizado_total"+
			"	   ,lc.FN_SALDO_TOTAL as saldo_total"   +
			"    FROM dis_linea_credito_dm lc"   +
			"     ,dis_acuse4 ac4"   +
			" 	   ,comcat_moneda cm		   "   +
			"    WHERE lc.cc_acuse = ac4.cc_acuse"   +
			"     AND lc.cg_tipo_solicitud = 'I'"   +
			"     AND lc.ic_moneda = cm.ic_moneda"   +
			"    	AND lc.ic_if = "+ses_ic_if+" "+
			condicionDM.toString());
		qryCCC.append(
			"   SELECT /*+ index(lc in_com_linea_credito_01_nuk) */"   +
			" 		cm.ic_moneda"   +
			" 		,cm.cd_nombre"   +
			" 		,lc.fn_monto_autorizado_total"   +
			" 		,lc.FN_SALDO_LINEA_TOTAL AS saldo_total"   +
			"     FROM com_linea_credito lc"   +
			" 	,comcat_moneda cm"   +
			"    WHERE lc.ic_moneda = cm.ic_moneda   "   +
			"     AND lc.cg_tipo_solicitud = 'I'"   +
			"   AND lc.ic_if = "+ses_ic_if+" "+
			condicionCCC.toString());

			if("D".equals(tipo_credito))
				qryAux.append(qryDM.toString());
			else if("C".equals(tipo_credito))
				qryAux.append(qryCCC.toString());
			else
				qryAux.append(qryDM.toString()+" UNION ALL "+qryCCC.toString());

			qrySentencia = 
				" SELECT ic_moneda,cd_nombre,COUNT(1),SUM(fn_monto_autorizado_total),SUM(saldo_total)"   +
				" 	FROM (" +
					qryAux.toString()+
				" )"   +
				" GROUP BY ic_moneda,cd_nombre"   +
				" ORDER BY ic_moneda"  ;												
		}catch(Exception e){
			log.error("MonitorLineasIfDist::getAggregateCalculationQuery "+e);
		}
		return qrySentencia;
	}

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids) {
		int iDM=0,iCCC;
		StringBuffer qrySentencia	= new StringBuffer();
    	StringBuffer qryDM			= new StringBuffer();
    	StringBuffer qryCCC			= new StringBuffer();
    	StringBuffer condicionDM	= new StringBuffer();
    	StringBuffer condicionCCC	= new StringBuffer();    	
    	StringBuffer condicion	= new StringBuffer();    	
		String	ses_ic_if         = request.getSession().getAttribute("iNoCliente").toString();
		String	tipo_credito		=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
    

		condicionDM.append(" AND lc.ic_linea_credito_dm in (");
		condicionCCC.append(" AND lc.ic_linea_credito in (");

		iDM=0;
		iCCC=0;
		String cadena	= "";
		String prefijo	= "";
		String valor	= "";
		for (Iterator it = ids.iterator(); it.hasNext();){
			cadena	= it.next().toString();
			prefijo	= cadena.substring(0,1);
			valor 	= cadena.substring(1,cadena.length());
			if("D".equals(prefijo)){
	        	if(iDM>0){
					condicionDM.append(",");
				}
				condicionDM.append(" "+valor+" ");
				iDM++;
			}else if("C".equals(prefijo)){
				if(iCCC>0){
					condicionCCC.append(",");
				}
				condicionCCC.append(" "+valor+" ");
				iCCC++;
			}
			
		}
		if(iDM==0){
			condicionDM.append(" -1 ");
		}
		if(iCCC==0){
			condicionCCC.append(" -1 ");
		}
		condicionDM.append(") ");
		condicionCCC.append(") ");		

		qryDM.append(
			"  SELECT /*+ use_nl(lc,ac4,cn,ce,cm,el,cf)"   +
			"         index(lc cp_dis_linea_credito_dm_pk)"   +
			"         index(cn cp_comrel_nafin_pk)*/"   +
			"         lc.ic_linea_credito_dm AS folio,  'Modalidad 1 (Riesgo Empresa de Primer Orden )' AS tipoCredito,"   +
			"         'D' AS ic_tipoCredito,"   +			
			"         DECODE (lc.cg_tipo_solicitud,'I', 'Inicial','A', 'Ampliacion','R', 'Renovacion') AS tipo_sol,"   +
			"         TO_CHAR (cn.ic_nafin_electronico) AS nEpo, ce.cg_razon_social AS EPO, 'N/A' AS NPyme, 'N/A' AS Pyme, ic_linea_credito_dm_padre AS lineaPadre,"   +
			"         'N/A' AS FechaSolic, TO_CHAR (ac4.df_acuse, 'DD/MM/YYYY') AS fecha_auto, cm.cd_nombre AS moneda,"   +
			"         el.cd_descripcion, '' AS causas, lc.ig_plazo AS plazo,"   +
			"         TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') AS fecha_venc,"   +
			"         cf.cd_nombre, lc.cg_num_cuenta_epo AS cuenta_epo, lc.cg_num_cuenta_if AS cuenta_if, lc.fn_monto_autorizado_total,lc.FN_SALDO_TOTAL AS saldo_total, lc.ic_moneda"   +
			"  		,el.ic_estatus_linea"   +
			"  		,TO_CHAR(lc.df_cambio,'dd/mm/yyyy') AS fecha_cambio, '' AS estatus_asigna"   +
			"    FROM dis_linea_credito_dm lc,"   +
			"         dis_acuse4 ac4,"   +
			"         comrel_nafin cn,"   +
			"         comcat_epo ce,"   +
			"         comcat_moneda cm,"   +
			"         comcat_estatus_linea el,"   +
			"         comcat_financiera cf"   +
			"   WHERE lc.ic_epo = cn.ic_epo_pyme_if"   +
			"    AND lc.ic_epo = ce.ic_epo"   +
			"    AND lc.cc_acuse = ac4.cc_acuse"   +
			"    AND lc.ic_moneda = cm.ic_moneda"   +
			"    AND lc.ic_estatus_linea = el.ic_estatus_linea"   +
			"    AND lc.ic_financiera = cf.ic_financiera (+)"   +
			"    AND lc.cg_tipo_solicitud = 'I'"   +
			"    AND cn.cg_tipo = 'E'"   +
			"    AND lc.ic_if = "+ses_ic_if+" "+
			condicionDM.toString());
		qryCCC.append(
			" SELECT /*+ use_nl(lc,cn,cp,cm,el,cpl,cf)"   +
			"         index(lc cp_com_linea_credito_pk)"   +
			"         index(cn cp_comrel_nafin_pk)*/"   +
			" 	   lc.ic_linea_credito AS folio, 'Modalidad 2 (Riesgo Distribuido' AS tipoCredito,"   +
			"         'C' AS ic_tipoCredito,"   +
			"        DECODE (lc.cg_tipo_solicitud,'I', 'Inicial', 'A', 'Ampliacion', 'R', 'Renovacion') AS tipo_sol,"   +
			"        'N/A' AS nEpo, 'N/A' AS EPO, TO_CHAR (cn.ic_nafin_electronico) AS NPyme, cp.cg_razon_social AS Pyme, ic_linea_credito_padre AS lineaPadre,"   +
			"        TO_CHAR (lc.df_captura, 'DD/MM/YYYY') AS FechaSolic,"   +
			"        TO_CHAR (lc.df_autorizacion_nafin, 'DD/MM/YYYY') AS fecha_auto,"   +
			"        cm.cd_nombre AS moneda, el.cd_descripcion, lc.cg_causa_rechazo AS causas, cpl.in_plazo_dias AS plazo,"   +
			"        TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') AS fecha_venc,"   +
			"        cf.cd_nombre, lc.cg_numero_cuenta AS cuenta_epo, lc.cg_numero_cuenta_if AS cuenta_if, lc.fn_monto_autorizado_total, lc.FN_SALDO_LINEA_TOTAL AS saldo_total, lc.ic_moneda"   +
			" 		,el.ic_estatus_linea"   +
			" 		,TO_CHAR(lc.df_cambio,'dd/mm/yyyy') AS fecha_cambio, '' AS estatus_asigna"   +
			"   FROM com_linea_credito lc,"   +
			"        comrel_nafin cn,"   +
			"        comcat_pyme cp,"   +
			"        comcat_moneda cm,"   +
			"        comcat_estatus_linea el,"   +
			"        comcat_plazo cpl,"   +
			"        comcat_financiera cf"   +
			"  WHERE lc.ic_pyme = cn.ic_epo_pyme_if"   +
			"   AND lc.ic_pyme = cp.ic_pyme"   +
			"   AND lc.ic_moneda = cm.ic_moneda"   +
			"   AND lc.ic_estatus_linea = el.ic_estatus_linea"   +
			"   AND lc.ic_plazo = cpl.ic_plazo(+)"   +
			"   AND lc.ic_financiera = cf.ic_financiera (+)"   +
			"   AND lc.cg_tipo_solicitud = 'I'"   +
			"   AND cn.cg_tipo = 'P'"   +
			"   AND lc.ic_if = "+ses_ic_if+" "+
			condicionCCC.toString());

			if("D".equals(tipo_credito))
				qrySentencia.append(qryDM.toString());
			else if("C".equals(tipo_credito))
				qrySentencia.append(qryCCC.toString());
			else
				qrySentencia.append(qryDM.toString()+" UNION ALL "+qryCCC.toString());
		log.error("el query queda de la siguiente manera "+qrySentencia.toString());
		return qrySentencia.toString();
	}
	
	public String getDocumentQuery(HttpServletRequest request) {
      String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer qryDM		  = new StringBuffer();
		StringBuffer qryCCC		  = new StringBuffer();
		StringBuffer condicionDM    = new StringBuffer();
		StringBuffer condicionCCC    = new StringBuffer();

		String	ses_ic_if            = request.getSession().getAttribute("iNoCliente").toString();
		
		String		tipo_credito		=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String		tipo_solic			=	(request.getParameter("tipo_solic")==null)?"":request.getParameter("tipo_solic");
		String		ic_epo				=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String		ic_pyme				=	(request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String		ic_estatus_linea	=	(request.getParameter("ic_estatus_linea")==null)?"":request.getParameter("ic_estatus_linea");
		String		dc_fecha_solic_de	=	(request.getParameter("dc_fecha_solic_de")==null)?"":request.getParameter("dc_fecha_solic_de");
		String		dc_fecha_solic_a	=	(request.getParameter("dc_fecha_solic_a")==null)?"":request.getParameter("dc_fecha_solic_a");
		String		dc_fecha_auto_de	=	(request.getParameter("dc_fecha_auto_de")==null)?"":request.getParameter("dc_fecha_auto_de");
		String		dc_fecha_auto_a	=	(request.getParameter("dc_fecha_auto_a")==null)?"":request.getParameter("dc_fecha_auto_a");
			
		try {

		if (!"".equals(tipo_solic)) {
			condicionDM.append(" and lc.cg_tipo_solicitud =  '"+tipo_solic+"' ");
			condicionCCC.append(" and lc.cg_tipo_solicitud =  '"+tipo_solic+"' ");
		}
		if (!"".equals(ic_epo)) 
			condicionDM.append(" and lc.ic_epo = "+ic_epo+" ");
		if (!"".equals(ic_pyme)) 
			condicionCCC.append(" and lc.ic_pyme = "+ic_pyme+" ");
		if (!"".equals(ic_estatus_linea)) {
			condicionDM.append(" and lc.ic_estatus_linea =  "+ic_estatus_linea+" ");
			condicionCCC.append(" and lc.ic_estatus_linea =  "+ic_estatus_linea+" ");
		}
        if (!("".equals(dc_fecha_solic_de)) && !("".equals(dc_fecha_solic_a)))
			condicionCCC.append("    AND TRUNC(lc.df_captura) BETWEEN TRUNC(TO_DATE('"+dc_fecha_solic_de+"','dd/mm/yyyy')) AND TRUNC(TO_DATE('"+dc_fecha_solic_a+"','dd/mm/yyyy')) ");
        if (!("".equals(dc_fecha_auto_de)) && !("".equals(dc_fecha_auto_a))) {
			condicionDM.append("    AND TRUNC(ac4.df_acuse) BETWEEN TRUNC(TO_DATE('"+dc_fecha_auto_de+"','dd/mm/yyyy')) AND TRUNC(TO_DATE('"+dc_fecha_auto_a+"','dd/mm/yyyy')) ");
			condicionCCC.append("    AND TRUNC(lc.df_autorizacion_nafin) BETWEEN TRUNC(TO_DATE('"+dc_fecha_solic_de+"','dd/mm/yyyy')) AND TRUNC(TO_DATE('"+dc_fecha_solic_a+"','dd/mm/yyyy')) ");
		}

		qryDM.append(
			"    SELECT /*+ use_nl(lc,ac4,cn)"   +
			"           index(lc in_dis_linea_credito_dm_01_NUK) */"   +
			"           'D'||lc.ic_linea_credito_dm"   +
			"      FROM dis_linea_credito_dm lc"   +
			"           ,dis_acuse4 ac4"   +
			"     WHERE lc.cc_acuse = ac4.cc_acuse"   +
			"      AND lc.cg_tipo_solicitud = 'I'"   +
			"    AND lc.ic_if = "+ses_ic_if+" "+
			condicionDM.toString());
		qryCCC.append(
			"  SELECT /*+ index(lc in_com_linea_credito_01_nuk) */"   +
			"  	    'C'||lc.ic_linea_credito"   +
			"    FROM com_linea_credito lc"   +
			"   WHERE lc.cg_tipo_solicitud = 'I'"   +
			"    AND lc.ic_if = "+ses_ic_if+" "+
			condicionCCC.toString());

			if("D".equals(tipo_credito))
				qrySentencia.append(qryDM.toString());
			else if("C".equals(tipo_credito))
				qrySentencia.append(qryCCC.toString());
			else
				qrySentencia.append(qryDM.toString()+" UNION ALL "+qryCCC.toString());
			log.info("EL query de la llave primaria: "+qrySentencia.toString());
		}catch(Exception e){
			log.error("MonitorLineasIfDist::getDocumentQueryException "+e);
		}
		return qrySentencia.toString();
	}
	
	public String getDocumentQueryFile(HttpServletRequest request) {
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer qryDM		  = new StringBuffer();
		StringBuffer qryCCC		  = new StringBuffer();
		StringBuffer condicionDM    = new StringBuffer();
		StringBuffer condicionCCC    = new StringBuffer();

		String	ses_ic_if            = request.getSession().getAttribute("iNoCliente").toString();
		
		String		tipo_credito		=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String		tipo_solic			=	(request.getParameter("tipo_solic")==null)?"":request.getParameter("tipo_solic");
		String		ic_epo				=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String		ic_pyme				=	(request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String		ic_estatus_linea	=	(request.getParameter("ic_estatus_linea")==null)?"":request.getParameter("ic_estatus_linea");
		String		dc_fecha_solic_de	=	(request.getParameter("dc_fecha_solic_de")==null)?"":request.getParameter("dc_fecha_solic_de");
		String		dc_fecha_solic_a	=	(request.getParameter("dc_fecha_solic_a")==null)?"":request.getParameter("dc_fecha_solic_a");
		String		dc_fecha_auto_de	=	(request.getParameter("dc_fecha_auto_de")==null)?"":request.getParameter("dc_fecha_auto_de");
		String		dc_fecha_auto_a	=	(request.getParameter("dc_fecha_auto_a")==null)?"":request.getParameter("dc_fecha_auto_a");
			
		try {

		if (!"".equals(tipo_solic)) {
			condicionDM.append(" and lc.cg_tipo_solicitud =  '"+tipo_solic+"' ");
			condicionCCC.append(" and lc.cg_tipo_solicitud =  '"+tipo_solic+"' ");
		}
		if (!"".equals(ic_epo)) 
			condicionDM.append(" and lc.ic_epo = "+ic_epo+" ");
		if (!"".equals(ic_pyme)) 
			condicionCCC.append(" and lc.ic_pyme = "+ic_pyme+" ");
		if (!"".equals(ic_estatus_linea)) {
			condicionDM.append(" and lc.ic_estatus_linea =  "+ic_estatus_linea+" ");
			condicionCCC.append(" and lc.ic_estatus_linea =  "+ic_estatus_linea+" ");
		}
        if (!("".equals(dc_fecha_solic_de)) && !("".equals(dc_fecha_solic_a)))
			condicionCCC.append("    AND TRUNC(lc.df_captura) BETWEEN TRUNC(TO_DATE('"+dc_fecha_solic_de+"','dd/mm/yyyy')) AND TRUNC(TO_DATE('"+dc_fecha_solic_a+"','dd/mm/yyyy')) ");
        if (!("".equals(dc_fecha_auto_de)) && !("".equals(dc_fecha_auto_a))) {
			condicionDM.append("    AND TRUNC(ac4.df_acuse) BETWEEN TRUNC(TO_DATE('"+dc_fecha_auto_de+"','dd/mm/yyyy')) AND TRUNC(TO_DATE('"+dc_fecha_auto_a+"','dd/mm/yyyy')) ");
			condicionCCC.append("    AND TRUNC(lc.df_autorizacion_nafin) BETWEEN TRUNC(TO_DATE('"+dc_fecha_solic_de+"','dd/mm/yyyy')) AND TRUNC(TO_DATE('"+dc_fecha_solic_a+"','dd/mm/yyyy')) ");
		}

		qryDM.append(
			"  SELECT /*+ use_nl(lc,ac4,cn,ce,cm,el,cf)"   +
			"         index(lc in_dis_linea_credito_dm_01_NUK)"   +
			"         index(cn cp_comrel_nafin_pk)*/"   +
			"         lc.ic_linea_credito_dm AS folio,  'Modalidad 1 (Riesgo Empresa de Primer Orden )' AS tipoCredito,"   +
			"         'D' AS ic_tipoCredito,"   +
			"         DECODE (lc.cg_tipo_solicitud,'I', 'Inicial','A', 'Ampliacion','R', 'Renovacion') AS tipo_sol,"   +
			"         TO_CHAR (cn.ic_nafin_electronico) AS nEpo, ce.cg_razon_social AS EPO, 'N/A' AS NPyme, 'N/A' AS Pyme, ic_linea_credito_dm_padre AS lineaPadre,"   +
			"         'N/A' AS FechaSolic, TO_CHAR (ac4.df_acuse, 'DD/MM/YYYY') AS fecha_auto, cm.cd_nombre AS moneda,"   +
			"         el.cd_descripcion, '' AS causas, lc.ig_plazo AS plazo,"   +
			"         TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') AS fecha_venc,"   +
			"         cf.cd_nombre, lc.cg_num_cuenta_epo AS cuenta_epo, lc.cg_num_cuenta_if AS cuenta_if, lc.fn_monto_autorizado_total,lc.FN_SALDO_TOTAL AS saldo_total, lc.ic_moneda"   +
			"  		,el.ic_estatus_linea"   +
			"  		,TO_CHAR(lc.df_cambio,'dd/mm/yyyy') AS fecha_cambio, '' AS estatus_asigna"   +
			"    FROM dis_linea_credito_dm lc,"   +
			"         dis_acuse4 ac4,"   +
			"         comrel_nafin cn,"   +
			"         comcat_epo ce,"   +
			"         comcat_moneda cm,"   +
			"         comcat_estatus_linea el,"   +
			"         comcat_financiera cf"   +
			"   WHERE lc.ic_epo = cn.ic_epo_pyme_if"   +
			"    AND lc.ic_epo = ce.ic_epo"   +
			"    AND lc.cc_acuse = ac4.cc_acuse"   +
			"    AND lc.ic_moneda = cm.ic_moneda"   +
			"    AND lc.ic_estatus_linea = el.ic_estatus_linea"   +
			"    AND lc.ic_financiera = cf.ic_financiera (+)"   +
			"    AND lc.cg_tipo_solicitud = 'I'"   +
			"    AND cn.cg_tipo = 'E'"   +
			"    AND lc.ic_if = "+ses_ic_if+" "+
			condicionDM.toString());
		qryCCC.append(
			" SELECT /*+ use_nl(lc,cn,cp,cm,el,cpl,cf)"   +
			"         index(lc in_com_linea_credito_01_nuk)"   +
			"         index(cn cp_comrel_nafin_pk)*/"   +
			" 	   lc.ic_linea_credito AS folio, 'Modalidad 2 (Riesgo Distribuidor)' AS tipoCredito,"   +
			"         'C' AS ic_tipoCredito,"   +
			"        DECODE (lc.cg_tipo_solicitud,'I', 'Inicial', 'A', 'Ampliacion', 'R', 'Renovacion') AS tipo_sol,"   +
			"        'N/A' AS nEpo, 'N/A' AS EPO, TO_CHAR (cn.ic_nafin_electronico) AS NPyme, cp.cg_razon_social AS Pyme, ic_linea_credito_padre AS lineaPadre,"   +
			"        TO_CHAR (lc.df_captura, 'DD/MM/YYYY') AS FechaSolic,"   +
			"        TO_CHAR (lc.df_autorizacion_nafin, 'DD/MM/YYYY') AS fecha_auto,"   +
			"        cm.cd_nombre AS moneda, el.cd_descripcion, lc.cg_causa_rechazo AS causas, cpl.in_plazo_dias AS plazo,"   +
			"        TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') AS fecha_venc,"   +
			"        cf.cd_nombre, lc.cg_numero_cuenta AS cuenta_epo, lc.cg_numero_cuenta_if AS cuenta_if, lc.fn_monto_autorizado_total, lc.FN_SALDO_LINEA_TOTAL AS saldo_total, lc.ic_moneda"   +
			" 		,el.ic_estatus_linea"   +
			" 		,TO_CHAR(lc.df_cambio,'dd/mm/yyyy') AS fecha_cambio, '' AS estatus_asigna"   +
			"   FROM com_linea_credito lc,"   +
			"        comrel_nafin cn,"   +
			"        comcat_pyme cp,"   +
			"        comcat_moneda cm,"   +
			"        comcat_estatus_linea el,"   +
			"        comcat_plazo cpl,"   +
			"        comcat_financiera cf"   +
			"  WHERE lc.ic_pyme = cn.ic_epo_pyme_if"   +
			"   AND lc.ic_pyme = cp.ic_pyme"   +
			"   AND lc.ic_moneda = cm.ic_moneda"   +
			"   AND lc.ic_estatus_linea = el.ic_estatus_linea"   +
			"   AND lc.ic_plazo = cpl.ic_plazo(+)"   +
			"   AND lc.ic_financiera = cf.ic_financiera (+)"   +
			"   AND lc.cg_tipo_solicitud = 'I'"   +
			"   AND cn.cg_tipo = 'P'"   +
			"   AND lc.ic_if = "+ses_ic_if+" "+
			condicionCCC.toString());

			if("D".equals(tipo_credito))
				qrySentencia.append(qryDM.toString());
			else if("C".equals(tipo_credito))
				qrySentencia.append(qryCCC.toString());
			else
				qrySentencia.append(qryDM.toString()+" UNION ALL "+qryCCC.toString());

		}catch(Exception e){
			log.error("MonitorLineasIdDist::getDocumentQueryFileException "+e);
		}
		return qrySentencia.toString();
	}

	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		querySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();

		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		StringBuffer qryAux		  = new StringBuffer();
		StringBuffer qryDM		  = new StringBuffer();
		StringBuffer qryCCC		  = new StringBuffer();
		StringBuffer qryFF		  = new StringBuffer();

		try {

			if("D".equals(tipo_credito)){
				qryDM.append(
					"    SELECT /*+ use_nl(lc,ac4)"   +
					"            index(lc in_dis_linea_credito_dm_01_NUK) */"   +
					" 	   cm.ic_moneda,cm.cd_nombre"   +
					" 	   ,lc.fn_monto_autorizado_total"+
					"	   ,lc.FN_SALDO_TOTAL as saldo_total"   +
					"    FROM dis_linea_credito_dm lc"   +
					"     ,dis_acuse4 ac4"   +
					" 	   ,comcat_moneda cm		   "   +
					"    WHERE lc.cc_acuse = ac4.cc_acuse"   +
					"     AND lc.cg_tipo_solicitud = ?"   +
					"     AND lc.ic_moneda = cm.ic_moneda"   +
					"    	AND lc.ic_if = ? ");
				conditions.add("I");
				conditions.add(ses_ic_if);

				/*if (!"".equals(tipo_solic)) {
					qryDM.append(" and lc.cg_tipo_solicitud = ? ");
					conditions.add(tipo_solic);
				}*/
				if (!"".equals(ic_epo)){
					qryDM.append(" and lc.ic_epo = ? ");
					conditions.add(ic_epo);
				}
				if (!"".equals(ic_estatus_linea)) {
					qryDM.append(" and lc.ic_estatus_linea = ? ");
					conditions.add(ic_estatus_linea);
				}
				if (!("".equals(dc_fecha_auto_de)) && !("".equals(dc_fecha_auto_a))) {
					qryDM.append("    AND TRUNC(ac4.df_acuse) BETWEEN TRUNC(TO_DATE(?,'dd/mm/yyyy')) AND TRUNC(TO_DATE(?,'dd/mm/yyyy')) ");
					conditions.add(dc_fecha_auto_de);
					conditions.add(dc_fecha_auto_a);
				}

				qryAux.append(qryDM.toString());

			}else if("C".equals(tipo_credito)){

				qryCCC.append(
					"   SELECT /*+ index(lc in_com_linea_credito_01_nuk) */"   +
					" 		cm.ic_moneda"   +
					" 		,cm.cd_nombre"   +
					" 		,lc.fn_monto_autorizado_total"   +
					" 		,lc.FN_SALDO_LINEA_TOTAL AS saldo_total"   +
					"     FROM com_linea_credito lc"   +
					" 	,comcat_moneda cm"   +
					"    WHERE lc.ic_moneda = cm.ic_moneda   "   +
					"     AND lc.cg_tipo_solicitud = ? "   +
					"   	AND lc.ic_if = ? "+
					"   AND lc.cs_factoraje_con_rec = ? ");
				conditions.add("I");
				conditions.add(ses_ic_if);
				conditions.add("N");
				/*if (!"".equals(tipo_solic)) {
					qryCCC.append(" AND lc.cg_tipo_solicitud = ? ");
					conditions.add(tipo_solic);
				}*/
				if (!"".equals(ic_pyme)){
					qryCCC.append(" AND lc.ic_pyme = ? ");
					conditions.add(ic_pyme);
				}
				if (!"".equals(ic_estatus_linea)) {
					qryCCC.append(" AND lc.ic_estatus_linea = ? ");
					conditions.add(ic_estatus_linea);
				}
				if (!("".equals(dc_fecha_solic_de)) && !("".equals(dc_fecha_solic_a))){
					qryCCC.append("    AND TRUNC(lc.df_captura) BETWEEN TRUNC(TO_DATE('"+dc_fecha_solic_de+"','dd/mm/yyyy')) AND TRUNC(TO_DATE('"+dc_fecha_solic_a+"','dd/mm/yyyy')) ");
					conditions.add(dc_fecha_solic_de);
					conditions.add(dc_fecha_solic_a);
				}
				if (!("".equals(dc_fecha_auto_de)) && !("".equals(dc_fecha_auto_a))) {
					qryCCC.append("    AND TRUNC(lc.df_autorizacion_nafin) BETWEEN TRUNC(TO_DATE('"+dc_fecha_solic_de+"','dd/mm/yyyy')) AND TRUNC(TO_DATE('"+dc_fecha_solic_a+"','dd/mm/yyyy')) ");
					conditions.add(dc_fecha_solic_de);
					conditions.add(dc_fecha_solic_a);
				}
				qryAux.append(qryCCC.toString());

			}else if("F".equals(tipo_credito)){
				//fodea 019 factoraje con recurso
				qryFF.append(
					"   SELECT /*+ index(lc in_com_linea_credito_01_nuk) */"   +
					" 		cm.ic_moneda"   +
					" 		,cm.cd_nombre"   +
					" 		,lc.fn_monto_autorizado_total"   +
					" 		,lc.FN_SALDO_LINEA_TOTAL AS saldo_total"   +
					"     FROM com_linea_credito lc"   +
					" 	,comcat_moneda cm"   +
					"    WHERE lc.ic_moneda = cm.ic_moneda   "   +
					"     AND lc.cg_tipo_solicitud = ? "   +
					"   	AND lc.ic_if = ? "+
					"   AND lc.cs_factoraje_con_rec = ? ");
				conditions.add("I");
				conditions.add(ses_ic_if);
				conditions.add("S");

				if (!"".equals(ic_epo)){
					qryFF.append(" and lc.ic_epo = ? ");
					conditions.add(ic_epo);
				}

				if (!"".equals(ic_pyme)){
					qryFF.append(" AND lc.ic_pyme = ? ");
					conditions.add(ic_pyme);
				}

				if (!("".equals(dc_fecha_auto_de)) && !("".equals(dc_fecha_auto_a))) {
					qryFF.append("    AND TRUNC(ac4.df_acuse) BETWEEN TRUNC(TO_DATE(?,'dd/mm/yyyy')) AND TRUNC(TO_DATE(?,'dd/mm/yyyy')) ");
					conditions.add(dc_fecha_auto_de);
					conditions.add(dc_fecha_auto_a);
				}

				qryAux.append(qryFF.toString());

			}else{

				qryDM.append(
					"    SELECT /*+ use_nl(lc,ac4)"   +
					"            index(lc in_dis_linea_credito_dm_01_NUK) */"   +
					" 	   cm.ic_moneda,cm.cd_nombre"   +
					" 	   ,lc.fn_monto_autorizado_total"+
					"	   ,lc.FN_SALDO_TOTAL as saldo_total"   +
					"    FROM dis_linea_credito_dm lc"   +
					"     ,dis_acuse4 ac4"   +
					" 	   ,comcat_moneda cm		   "   +
					"    WHERE lc.cc_acuse = ac4.cc_acuse"   +
					"     AND lc.cg_tipo_solicitud = ? "   +
					"     AND lc.ic_moneda = cm.ic_moneda"   +
					"    	AND lc.ic_if = ? ");
				conditions.add("I");
				conditions.add(ses_ic_if);

				/*if (!"".equals(tipo_solic)) {
					qryDM.append(" and lc.cg_tipo_solicitud = ? ");
					conditions.add(tipo_solic);
				}*/
				if (!"".equals(ic_epo)){
					qryDM.append(" and lc.ic_epo = ? ");
					conditions.add(ic_epo);
				}
				if (!"".equals(ic_estatus_linea)) {
					qryDM.append(" and lc.ic_estatus_linea = ? ");
					conditions.add(ic_estatus_linea);
				}
				if (!("".equals(dc_fecha_auto_de)) && !("".equals(dc_fecha_auto_a))) {
					qryDM.append("    AND TRUNC(ac4.df_acuse) BETWEEN TRUNC(TO_DATE(?,'dd/mm/yyyy')) AND TRUNC(TO_DATE(?,'dd/mm/yyyy')) ");
					conditions.add(dc_fecha_auto_de);
					conditions.add(dc_fecha_auto_a);
				}

				qryCCC.append(
					"   SELECT /*+ index(lc in_com_linea_credito_01_nuk) */"   +
					" 		cm.ic_moneda"   +
					" 		,cm.cd_nombre"   +
					" 		,lc.fn_monto_autorizado_total"   +
					" 		,lc.FN_SALDO_LINEA_TOTAL AS saldo_total"   +
					"     FROM com_linea_credito lc"   +
					" 	,comcat_moneda cm"   +
					"    WHERE lc.ic_moneda = cm.ic_moneda   "   +
					"     AND lc.cg_tipo_solicitud = ? "   +
					"   	AND lc.ic_if = ? "+
					"   AND lc.cs_factoraje_con_rec = ? ");
				conditions.add("I");
				conditions.add(ses_ic_if);
				conditions.add("N");
				/*if (!"".equals(tipo_solic)) {
					qryCCC.append(" AND lc.cg_tipo_solicitud = ? ");
					conditions.add(tipo_solic);
				}*/
				if (!"".equals(ic_pyme)){
					qryCCC.append(" AND lc.ic_pyme = ? ");
					conditions.add(ic_pyme);
				}
				if (!"".equals(ic_estatus_linea)) {
					qryCCC.append(" AND lc.ic_estatus_linea = ? ");
					conditions.add(ic_estatus_linea);
				}
				if (!("".equals(dc_fecha_solic_de)) && !("".equals(dc_fecha_solic_a))){
					qryCCC.append("    AND TRUNC(lc.df_captura) BETWEEN TRUNC(TO_DATE('"+dc_fecha_solic_de+"','dd/mm/yyyy')) AND TRUNC(TO_DATE('"+dc_fecha_solic_a+"','dd/mm/yyyy')) ");
					conditions.add(dc_fecha_solic_de);
					conditions.add(dc_fecha_solic_a);
				}
				if (!("".equals(dc_fecha_auto_de)) && !("".equals(dc_fecha_auto_a))) {
					qryCCC.append("    AND TRUNC(lc.df_autorizacion_nafin) BETWEEN TRUNC(TO_DATE('"+dc_fecha_solic_de+"','dd/mm/yyyy')) AND TRUNC(TO_DATE('"+dc_fecha_solic_a+"','dd/mm/yyyy')) ");
					conditions.add(dc_fecha_solic_de);
					conditions.add(dc_fecha_solic_a);
				}
				qryAux.append(qryDM.toString()+" UNION ALL "+qryCCC.toString());
			}

			querySentencia.append( 
				" SELECT ic_moneda,cd_nombre,COUNT(1) AS total_registros, SUM(fn_monto_autorizado_total) AS total_monto,SUM(saldo_total) AS saldo_total"   +
				" 	FROM (" +
					qryAux.toString()+
				" )"   +
				" GROUP BY ic_moneda,cd_nombre"   +
				" ORDER BY ic_moneda");
		}catch(Exception e){
			log.debug("MonitorLineasIfDist::getAggregateCalculationQuery "+e);
		}
		log.debug("..:: qrySentencia "+querySentencia.toString());
		log.debug("..:: conditions: "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return querySentencia.toString();
	}
	public String getDocumentSummaryQueryForIds(List pageIds) {
		log.info("getDocumentSummaryQueryForIds(E)");
		querySentencia = new StringBuffer();
		conditions 		= new ArrayList();

    	StringBuffer qryDM	= new StringBuffer();
    	StringBuffer qryCCC	= new StringBuffer();
		StringBuffer qryFF	= new StringBuffer();

		if("D".equals(tipo_credito)){
			qryDM.append(
				"  SELECT /*+ use_nl(lc,ac4,cn,ce,cm,el,cf)"   +
				"         index(lc cp_dis_linea_credito_dm_pk)"   +
				"         index(cn cp_comrel_nafin_pk)*/"   +
				"         lc.ic_linea_credito_dm AS folio,  'Modalidad 1 (Riesgo Empresa de Primer Orden )' AS tipoCredito,"   +
				"         'D' AS ic_tipoCredito,"   +
				"         DECODE (lc.cg_tipo_solicitud,'I', 'Inicial','A', 'Ampliacion','R', 'Renovacion') AS tipo_sol,"   +
				"         TO_CHAR (cn.ic_nafin_electronico) AS nEpo, ce.cg_razon_social AS EPO, 'N/A' AS NPyme, 'N/A' AS Pyme, ic_linea_credito_dm_padre AS lineaPadre,"   +
				"         'N/A' AS FechaSolic, TO_CHAR (ac4.df_acuse, 'DD/MM/YYYY') AS fecha_auto, cm.cd_nombre AS moneda,"   +
				"         el.cd_descripcion, '' AS causas, lc.ig_plazo AS plazo,"   +
				"         TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') AS fecha_venc,"   +
				"         cf.cd_nombre, lc.cg_num_cuenta_epo AS cuenta_epo, lc.cg_num_cuenta_if AS cuenta_if, lc.fn_monto_autorizado_total,lc.FN_SALDO_TOTAL AS saldo_total, lc.ic_moneda"   +
				"  		,el.ic_estatus_linea"   +
				"  		,TO_CHAR(lc.df_cambio,'dd/mm/yyyy') AS fecha_cambio, '' AS estatus_asigna"   +
				"    FROM dis_linea_credito_dm lc,"   +
				"         dis_acuse4 ac4,"   +
				"         comrel_nafin cn,"   +
				"         comcat_epo ce,"   +
				"         comcat_moneda cm,"   +
				"         comcat_estatus_linea el,"   +
				"         comcat_financiera cf"   +
				"   WHERE lc.ic_epo = cn.ic_epo_pyme_if"   +
				"    AND lc.ic_epo = ce.ic_epo"   +
				"    AND lc.cc_acuse = ac4.cc_acuse"   +
				"    AND lc.ic_moneda = cm.ic_moneda"   +
				"    AND lc.ic_estatus_linea = el.ic_estatus_linea"   +
				"    AND lc.ic_financiera = cf.ic_financiera (+)"   +
				"    AND lc.cg_tipo_solicitud = ? "   +
				"    AND cn.cg_tipo = ? "   +
				"    AND lc.ic_if = ? ");
			conditions.add("I");
			conditions.add("E");
			conditions.add(ses_ic_if);

			for(int i=0;i<pageIds.size();i++) {
				List lItem = (List)pageIds.get(i);
				if(i==0) {
					qryDM.append(" AND lc.ic_linea_credito_dm in ( ");
				}
				qryDM.append("?");
				conditions.add(new Long(lItem.get(0).toString()));					
				if(i!=(pageIds.size()-1)) {
					qryDM.append(",");
				} else {
					qryDM.append(" ) ");
				}
			}
			querySentencia.append(qryDM.toString());

		}else if("C".equals(tipo_credito)){
			qryCCC.append(
				" SELECT  "+
				" 	   lc.ic_linea_credito AS folio, 'Modalidad 2 (Riesgo Distribuidor)' AS tipoCredito,"   +
				"         'C' AS ic_tipoCredito,"   +
				"        DECODE (lc.cg_tipo_solicitud,'I', 'Inicial', 'A', 'Ampliacion', 'R', 'Renovacion') AS tipo_sol,"   +
				"        'N/A' AS nEpo, 'N/A' AS EPO, TO_CHAR (cn.ic_nafin_electronico) AS NPyme, cp.cg_razon_social AS Pyme, ic_linea_credito_padre AS lineaPadre,"   +
				"        TO_CHAR (lc.df_captura, 'DD/MM/YYYY') AS FechaSolic,"   +
				"        TO_CHAR (lc.df_autorizacion_nafin, 'DD/MM/YYYY') AS fecha_auto,"   +
				"        cm.cd_nombre AS moneda, el.cd_descripcion, lc.cg_causa_rechazo AS causas, cpl.in_plazo_dias AS plazo,"   +
				"        TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') AS fecha_venc,"   +
				"        cf.cd_nombre, lc.cg_numero_cuenta AS cuenta_epo, lc.cg_numero_cuenta_if AS cuenta_if, lc.fn_monto_autorizado_total, lc.FN_SALDO_LINEA_TOTAL AS saldo_total, lc.ic_moneda"   +
				" 		,el.ic_estatus_linea"   +
				" 		,TO_CHAR(lc.df_cambio,'dd/mm/yyyy') AS fecha_cambio, '' AS estatus_asigna"   +
				"   FROM com_linea_credito lc,"   +
				"        comrel_nafin cn,"   +
				"        comcat_pyme cp,"   +
				"        comcat_moneda cm,"   +
				"        comcat_estatus_linea el,"   +
				"        comcat_plazo cpl,"   +
				"        comcat_financiera cf"   +
				"  WHERE lc.ic_pyme = cn.ic_epo_pyme_if"   +
				"   AND lc.ic_pyme = cp.ic_pyme"   +
				"   AND lc.ic_moneda = cm.ic_moneda"   +
				"   AND lc.ic_estatus_linea = el.ic_estatus_linea"   +
				"   AND lc.ic_plazo = cpl.ic_plazo(+)"   +
				"   AND lc.ic_financiera = cf.ic_financiera (+)"   +
				"   AND lc.cg_tipo_solicitud = ? "   +
				"   AND cn.cg_tipo = ? " +
				"   AND lc.ic_if = ? "+
				"   AND lc.cs_factoraje_con_rec = ? ");
			conditions.add("I");
			conditions.add("P");
			conditions.add(ses_ic_if);
			conditions.add("N");

			for(int i=0;i<pageIds.size();i++) {
				List lItem = (List)pageIds.get(i);
				if(i==0) {
					qryCCC.append(" AND lc.ic_linea_credito in ( ");
				}
				qryCCC.append("?");
				conditions.add(new Long(lItem.get(0).toString()));
				if(i!=(pageIds.size()-1)) {
					qryCCC.append(",");
				} else {
					qryCCC.append(" ) ");
				}
			}
			querySentencia.append(qryCCC.toString());

		}else if("F".equals(tipo_credito)){

			qryFF.append(
				" SELECT /*+ use_nl(lc,cn,cp,cm,el,cpl,cf)"   +
				"         index(lc cp_com_linea_credito_pk)"   +
				"         index(cn cp_comrel_nafin_pk)*/"   +
				" 	   lc.ic_linea_credito AS folio, 'Factoraje con recurso' AS tipoCredito,"   +
				"         'F' AS ic_tipoCredito,"   +
				"        DECODE (lc.cg_tipo_solicitud,'I', 'Inicial', 'A', 'Ampliacion', 'R', 'Renovacion') AS tipo_sol,"   +
				"        'N/A' AS nEpo, 'N/A' AS EPO, TO_CHAR (cn.ic_nafin_electronico) AS NPyme, cp.cg_razon_social AS Pyme, ic_linea_credito_padre AS lineaPadre,"   +
				"        TO_CHAR (lc.df_captura, 'DD/MM/YYYY') AS FechaSolic,"   +
				//"        TO_CHAR (lc.df_autorizacion_nafin, 'DD/MM/YYYY') AS fecha_auto,"   +
				"        TO_CHAR (lc.df_captura, 'DD/MM/YYYY') AS fecha_auto,"   +
				"        cm.cd_nombre AS moneda, el.cd_descripcion, lc.cg_causa_rechazo AS causas, cpl.in_plazo_dias AS plazo,"   +
				"        TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') AS fecha_venc,"   +
				"        cf.cd_nombre, lc.cg_numero_cuenta AS cuenta_epo, lc.cg_numero_cuenta_if AS cuenta_if, lc.fn_monto_autorizado_total, lc.FN_SALDO_LINEA_TOTAL AS saldo_total, lc.ic_moneda"   +
				" 		,el.ic_estatus_linea"   +
				" 		,TO_CHAR(lc.df_cambio,'dd/mm/yyyy hh24:mm:ss') AS fecha_cambio, '' AS estatus_asigna"+
				"	, lc.ig_plazo_max, lc.ig_cuenta_bancaria, lc.cg_usuario_cambio, lc.cs_factoraje_con_rec "+
				"   FROM com_linea_credito lc,"   +
				"        comrel_nafin cn,"   +
				"        comcat_pyme cp,"   +
				"        comcat_moneda cm,"   +
				"        comcat_estatus_linea el,"   +
				"        comcat_plazo cpl,"   +
				"        comcat_financiera cf"   +
				"  WHERE lc.ic_pyme = cn.ic_epo_pyme_if"   +
				"   AND lc.ic_pyme = cp.ic_pyme"   +
				"   AND lc.ic_moneda = cm.ic_moneda"   +
				"   AND lc.ic_estatus_linea = el.ic_estatus_linea"   +
				"   AND lc.ic_plazo = cpl.ic_plazo(+)"   +
				"   AND lc.ic_financiera = cf.ic_financiera (+)"   +
				"   AND lc.cg_tipo_solicitud = ? "   +
				"   AND cn.cg_tipo = ? " +
				"   AND lc.ic_if = ? "+
				"   AND lc.cs_factoraje_con_rec = ? ");
			conditions.add("I");
			conditions.add("P");
			conditions.add(ses_ic_if);
			conditions.add("S");

			for(int i=0;i<pageIds.size();i++) {
				List lItem = (List)pageIds.get(i);
				if(i==0) {
					qryFF.append(" AND lc.ic_linea_credito in ( ");
				}
				qryFF.append("?");
				conditions.add(new Long(lItem.get(0).toString()));
				if(i!=(pageIds.size()-1)) {
					qryFF.append(",");
				} else {
					qryFF.append(" ) ");
				}
			}
			querySentencia.append(qryFF.toString());

		}else{
			qryDM.append(
				"  SELECT /*+ use_nl(lc,ac4,cn,ce,cm,el,cf)"   +
				"         index(lc cp_dis_linea_credito_dm_pk)"   +
				"         index(cn cp_comrel_nafin_pk)*/"   +
				"         lc.ic_linea_credito_dm AS folio,  'Modalidad 1 (Riesgo Empresa de Primer Orden )' AS tipoCredito,"   +
				"         'D' AS ic_tipoCredito,"   +
				"         DECODE (lc.cg_tipo_solicitud,'I', 'Inicial','A', 'Ampliacion','R', 'Renovacion') AS tipo_sol,"   +
				"         TO_CHAR (cn.ic_nafin_electronico) AS nEpo, ce.cg_razon_social AS EPO, 'N/A' AS NPyme, 'N/A' AS Pyme, ic_linea_credito_dm_padre AS lineaPadre,"   +
				"         'N/A' AS FechaSolic, TO_CHAR (ac4.df_acuse, 'DD/MM/YYYY') AS fecha_auto, cm.cd_nombre AS moneda,"   +
				"         el.cd_descripcion, '' AS causas, lc.ig_plazo AS plazo,"   +
				"         TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') AS fecha_venc,"   +
				"         cf.cd_nombre, lc.cg_num_cuenta_epo AS cuenta_epo, lc.cg_num_cuenta_if AS cuenta_if, lc.fn_monto_autorizado_total,lc.FN_SALDO_TOTAL AS saldo_total, lc.ic_moneda"   +
				"  		,el.ic_estatus_linea"   +
				"  		,TO_CHAR(lc.df_cambio,'dd/mm/yyyy') AS fecha_cambio, '' AS estatus_asigna"   +
				"    FROM dis_linea_credito_dm lc,"   +
				"         dis_acuse4 ac4,"   +
				"         comrel_nafin cn,"   +
				"         comcat_epo ce,"   +
				"         comcat_moneda cm,"   +
				"         comcat_estatus_linea el,"   +
				"         comcat_financiera cf"   +
				"   WHERE lc.ic_epo = cn.ic_epo_pyme_if"   +
				"    AND lc.ic_epo = ce.ic_epo"   +
				"    AND lc.cc_acuse = ac4.cc_acuse"   +
				"    AND lc.ic_moneda = cm.ic_moneda"   +
				"    AND lc.ic_estatus_linea = el.ic_estatus_linea"   +
				"    AND lc.ic_financiera = cf.ic_financiera (+)"   +
				"    AND lc.cg_tipo_solicitud = ? "   +
				"    AND cn.cg_tipo = ? "   +
				"    AND lc.ic_if = ? ");
			conditions.add("I");
			conditions.add("E");
			conditions.add(ses_ic_if);

			for(int i=0;i<pageIds.size();i++) {
				List lItem = (List)pageIds.get(i);
				if(i==0) {
					qryDM.append(" AND lc.ic_linea_credito_dm in ( ");
				}
				qryDM.append("?");
				conditions.add(new Long(lItem.get(0).toString()));					
				if(i!=(pageIds.size()-1)) {
					qryDM.append(",");
				} else {
					qryDM.append(" ) ");
				}
			}

			qryCCC.append(
				" SELECT "   +
				" 	   lc.ic_linea_credito AS folio, 'Modalidad 2 (Riesgo Distribuidor)' AS tipoCredito,"   +
				"         'C' AS ic_tipoCredito,"   +
				"        DECODE (lc.cg_tipo_solicitud,'I', 'Inicial', 'A', 'Ampliacion', 'R', 'Renovacion') AS tipo_sol,"   +
				"        'N/A' AS nEpo, 'N/A' AS EPO, TO_CHAR (cn.ic_nafin_electronico) AS NPyme, cp.cg_razon_social AS Pyme, ic_linea_credito_padre AS lineaPadre,"   +
				"        TO_CHAR (lc.df_captura, 'DD/MM/YYYY') AS FechaSolic,"   +
				"        TO_CHAR (lc.df_autorizacion_nafin, 'DD/MM/YYYY') AS fecha_auto,"   +
				"        cm.cd_nombre AS moneda, el.cd_descripcion, lc.cg_causa_rechazo AS causas, cpl.in_plazo_dias AS plazo,"   +
				"        TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') AS fecha_venc,"   +
				"        cf.cd_nombre, lc.cg_numero_cuenta AS cuenta_epo, lc.cg_numero_cuenta_if AS cuenta_if, lc.fn_monto_autorizado_total, lc.FN_SALDO_LINEA_TOTAL AS saldo_total, lc.ic_moneda"   +
				" 		,el.ic_estatus_linea"   +
				" 		,TO_CHAR(lc.df_cambio,'dd/mm/yyyy') AS fecha_cambio, '' AS estatus_asigna"   +
				"   FROM com_linea_credito lc,"   +
				"        comrel_nafin cn,"   +
				"        comcat_pyme cp,"   +
				"        comcat_moneda cm,"   +
				"        comcat_estatus_linea el,"   +
				"        comcat_plazo cpl,"   +
				"        comcat_financiera cf"   +
				"  WHERE lc.ic_pyme = cn.ic_epo_pyme_if"   +
				"   AND lc.ic_pyme = cp.ic_pyme"   +
				"   AND lc.ic_moneda = cm.ic_moneda"   +
				"   AND lc.ic_estatus_linea = el.ic_estatus_linea"   +
				"   AND lc.ic_plazo = cpl.ic_plazo(+)"   +
				"   AND lc.ic_financiera = cf.ic_financiera (+)"   +
				"   AND lc.cg_tipo_solicitud = ? "   +
				"   AND cn.cg_tipo = ? " +
				"   AND lc.ic_if = ? "+
				"   AND lc.cs_factoraje_con_rec = ? ");
			conditions.add("I");
			conditions.add("P");
			conditions.add(ses_ic_if);
			conditions.add("N");

			for(int i=0;i<pageIds.size();i++) {
				List lItem = (List)pageIds.get(i);
				if(i==0) {
					qryCCC.append(" AND lc.ic_linea_credito in ( ");
				}
				qryCCC.append("?");
				conditions.add(new Long(lItem.get(0).toString()));
				if(i!=(pageIds.size()-1)) {
					qryCCC.append(",");
				} else {
					qryCCC.append(" ) ");
				}
			}
			querySentencia.append(qryDM.toString()+" UNION ALL "+qryCCC.toString());
		}
		log.debug("..:: qrySentencia "+querySentencia.toString());
		log.debug("..:: conditions: "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return querySentencia.toString();
	}

	public String getDocumentQuery() {
		log.info("getDocumentQuery(E)");
		querySentencia = new StringBuffer();
		conditions 		= new ArrayList();
    	StringBuffer qryDM	= new StringBuffer();
    	StringBuffer qryCCC	= new StringBuffer();
		StringBuffer qryFF	= new StringBuffer();

		if("D".equals(tipo_credito)){
			qryDM.append(
				"    SELECT /*+ use_nl(lc,ac4,cn)"   +
				"           index(lc in_dis_linea_credito_dm_01_NUK) */"   +
				"           lc.ic_linea_credito_dm"   +
				//"           'D'||lc.ic_linea_credito_dm"   +
				"      FROM dis_linea_credito_dm lc"   +
				"           ,dis_acuse4 ac4"   +
				"     WHERE lc.cc_acuse = ac4.cc_acuse"   +
				"      AND lc.cg_tipo_solicitud = ? "   +
				"    AND lc.ic_if = ? ");
			conditions.add("I");
			conditions.add(ses_ic_if);

			/*if (!"".equals(tipo_solic)) {
				qryDM.append(" AND lc.cg_tipo_solicitud = ? ");
				conditions.add(tipo_solic);
			}*/
			if (!"".equals(ic_epo)){
				qryDM.append(" AND lc.ic_epo = ? ");
				conditions.add(ic_epo);
			}
			if (!"".equals(ic_estatus_linea)) {
				qryDM.append(" AND lc.ic_estatus_linea = ? ");
				conditions.add(ic_estatus_linea);
			}
			if (!("".equals(dc_fecha_auto_de)) && !("".equals(dc_fecha_auto_a))) {
				qryDM.append("    AND TRUNC(ac4.df_acuse) BETWEEN TRUNC(TO_DATE(?,'dd/mm/yyyy')) AND TRUNC(TO_DATE(?,'dd/mm/yyyy')) ");
				conditions.add(dc_fecha_auto_de);
				conditions.add(dc_fecha_auto_a);
			}
			querySentencia.append(qryDM.toString());

		}else if("C".equals(tipo_credito)){

			qryCCC.append(
				"  SELECT /*+ index(lc in_com_linea_credito_01_nuk) */"   +
				"  	    lc.ic_linea_credito"   +
				//"  	    'C'||lc.ic_linea_credito"   +
				"	FROM com_linea_credito lc"   +
				"	WHERE lc.ic_if = ? "+
				"	AND lc.cg_tipo_solicitud = ? "   +
				"	AND lc.cs_factoraje_con_rec = ? ");

			conditions.add(ses_ic_if);
			conditions.add("I");
			conditions.add("N");

			/*if (!"".equals(tipo_solic)) {
				qryCCC.append(" AND lc.cg_tipo_solicitud = ? ");
				conditions.add(tipo_solic);
			}*/
			if (!"".equals(ic_pyme)){
				qryCCC.append(" AND lc.ic_pyme = ? ");
				conditions.add(ic_pyme);
			}
			if (!"".equals(ic_estatus_linea)) {
				qryCCC.append(" AND lc.ic_estatus_linea = ? ");
				conditions.add(ic_estatus_linea);
			}
			if (!("".equals(dc_fecha_solic_de)) && !("".equals(dc_fecha_solic_a))){
				qryCCC.append("    AND TRUNC(lc.df_captura) BETWEEN TRUNC(TO_DATE(?,'dd/mm/yyyy')) AND TRUNC(TO_DATE(?,'dd/mm/yyyy')) ");
				conditions.add(dc_fecha_solic_de);
				conditions.add(dc_fecha_solic_a);
			}
			if (!("".equals(dc_fecha_auto_de)) && !("".equals(dc_fecha_auto_a))) {
				qryCCC.append("    AND TRUNC(lc.df_autorizacion_nafin) BETWEEN TRUNC(TO_DATE(?,'dd/mm/yyyy')) AND TRUNC(TO_DATE(?,'dd/mm/yyyy')) ");
				conditions.add(dc_fecha_solic_de);
				conditions.add(dc_fecha_solic_a);
			}
			querySentencia.append(qryCCC.toString());

		}else if("F".equals(tipo_credito)){	//fodea 019 factoraje con recurso
			qryFF.append(
				"  SELECT /*+ index(lc in_com_linea_credito_01_nuk) */"   +
				"  	    lc.ic_linea_credito"   +
				"	FROM com_linea_credito lc"   +
				"	WHERE lc.cg_tipo_solicitud = ? "+
				"	AND lc.cs_factoraje_con_rec = ? "+
				"  AND lc.ic_if = ? ");
			conditions.add("I");
			conditions.add("S");
			conditions.add(ses_ic_if);

			if (!"".equals(ic_epo)){
				qryFF.append(" AND lc.ic_epo = ? ");
				conditions.add(ic_epo);
			}

			if (!"".equals(ic_pyme)){
				qryFF.append(" AND lc.ic_pyme = ? ");
				conditions.add(ic_pyme);
			}

			if (!("".equals(dc_fecha_auto_de)) && !("".equals(dc_fecha_auto_a))) {
				qryFF.append("    AND TRUNC(ac4.df_acuse) BETWEEN TRUNC(TO_DATE(?,'dd/mm/yyyy')) AND TRUNC(TO_DATE(?,'dd/mm/yyyy')) ");
				conditions.add(dc_fecha_auto_de);
				conditions.add(dc_fecha_auto_a);
			}

			querySentencia.append(qryFF.toString());

		}else{
			qryDM.append(
				"    SELECT /*+ use_nl(lc,ac4,cn)"   +
				"           index(lc in_dis_linea_credito_dm_01_NUK) */"   +
				"           lc.ic_linea_credito_dm"   +
				//"           'D'||lc.ic_linea_credito_dm"   +
				"      FROM dis_linea_credito_dm lc"   +
				"           ,dis_acuse4 ac4"   +
				"     WHERE lc.cc_acuse = ac4.cc_acuse"   +
				"      AND lc.cg_tipo_solicitud = ? "   +
				"    AND lc.ic_if = ? ");
			conditions.add("I");
			conditions.add(ses_ic_if);

			/*if (!"".equals(tipo_solic)) {
				qryDM.append(" AND lc.cg_tipo_solicitud = ? ");
				conditions.add(tipo_solic);
			}*/
			if (!"".equals(ic_epo)){
				qryDM.append(" AND lc.ic_epo = ? ");
				conditions.add(ic_epo);
			}
			if (!"".equals(ic_estatus_linea)) {
				qryDM.append(" AND lc.ic_estatus_linea = ? ");
				conditions.add(ic_estatus_linea);
			}
			if (!("".equals(dc_fecha_auto_de)) && !("".equals(dc_fecha_auto_a))) {
				qryDM.append("    AND TRUNC(ac4.df_acuse) BETWEEN TRUNC(TO_DATE(?,'dd/mm/yyyy')) AND TRUNC(TO_DATE(?,'dd/mm/yyyy')) ");
				conditions.add(dc_fecha_auto_de);
				conditions.add(dc_fecha_auto_a);
			}

			qryCCC.append(
				"  SELECT /*+ index(lc in_com_linea_credito_01_nuk) */"   +
				"  	    lc.ic_linea_credito"   +
				//"  	    'C'||lc.ic_linea_credito"   +
				"	FROM com_linea_credito lc"   +
				"	WHERE lc.ic_if = ? "+
				"	AND lc.cg_tipo_solicitud = ? "   +
				"	AND lc.cs_factoraje_con_rec = ? ");

			conditions.add(ses_ic_if);
			conditions.add("I");
			conditions.add("N");

			/*if (!"".equals(tipo_solic)) {
				qryCCC.append(" AND lc.cg_tipo_solicitud = ? ");
				conditions.add(tipo_solic);
			}*/
			if (!"".equals(ic_pyme)){
				qryCCC.append(" AND lc.ic_pyme = ? ");
				conditions.add(ic_pyme);
			}
			if (!"".equals(ic_estatus_linea)) {
				qryCCC.append(" AND lc.ic_estatus_linea = ? ");
				conditions.add(ic_estatus_linea);
			}
			if (!("".equals(dc_fecha_solic_de)) && !("".equals(dc_fecha_solic_a))){
				qryCCC.append("    AND TRUNC(lc.df_captura) BETWEEN TRUNC(TO_DATE(?,'dd/mm/yyyy')) AND TRUNC(TO_DATE(?,'dd/mm/yyyy')) ");
				conditions.add(dc_fecha_solic_de);
				conditions.add(dc_fecha_solic_a);
			}
			if (!("".equals(dc_fecha_auto_de)) && !("".equals(dc_fecha_auto_a))) {
				qryCCC.append("    AND TRUNC(lc.df_autorizacion_nafin) BETWEEN TRUNC(TO_DATE(?,'dd/mm/yyyy')) AND TRUNC(TO_DATE(?,'dd/mm/yyyy')) ");
				conditions.add(dc_fecha_solic_de);
				conditions.add(dc_fecha_solic_a);
			}
			querySentencia.append(qryDM.toString()+" UNION ALL "+qryCCC.toString());
		}
		log.debug("..:: qrySentencia: "+querySentencia.toString());
		log.debug("..:: conditions: "+conditions);
		log.info("getDocumentQuery(S)");
		return querySentencia.toString();
	}
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		querySentencia = new StringBuffer();
		conditions 		= new ArrayList();
    	StringBuffer qryDM	= new StringBuffer();
    	StringBuffer qryCCC	= new StringBuffer();
		StringBuffer qryFF	= new StringBuffer();
		
		if("D".equals(tipo_credito)){
			qryDM.append(
				"  SELECT /*+ use_nl(lc,ac4,cn,ce,cm,el,cf)"   +
				"         index(lc in_dis_linea_credito_dm_01_NUK)"   +
				"         index(cn cp_comrel_nafin_pk)*/"   +
				"         lc.ic_linea_credito_dm AS folio,  'Modalidad 1 (Riesgo Empresa de Primer Orden ' AS tipoCredito,"   +
				"         'D' AS ic_tipoCredito,"   +
				"         DECODE (lc.cg_tipo_solicitud,'I', 'Inicial','A', 'Ampliacion','R', 'Renovacion') AS tipo_sol,"   +
				"         TO_CHAR (cn.ic_nafin_electronico) AS nEpo, ce.cg_razon_social AS EPO, 'N/A' AS NPyme, 'N/A' AS Pyme, ic_linea_credito_dm_padre AS lineaPadre,"   +
				"         'N/A' AS FechaSolic, TO_CHAR (ac4.df_acuse, 'DD/MM/YYYY') AS fecha_auto, cm.cd_nombre AS moneda,"   +
				"         el.cd_descripcion, '' AS causas, lc.ig_plazo AS plazo,"   +
				"         TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') AS fecha_venc,"   +
				"         cf.cd_nombre, lc.cg_num_cuenta_epo AS cuenta_epo, lc.cg_num_cuenta_if AS cuenta_if, lc.fn_monto_autorizado_total,lc.FN_SALDO_TOTAL AS saldo_total, lc.ic_moneda"   +
				"  		,el.ic_estatus_linea"   +
				"  		,TO_CHAR(lc.df_cambio,'dd/mm/yyyy') AS fecha_cambio, '' AS estatus_asigna"   +
				"    FROM dis_linea_credito_dm lc,"   +
				"         dis_acuse4 ac4,"   +
				"         comrel_nafin cn,"   +
				"         comcat_epo ce,"   +
				"         comcat_moneda cm,"   +
				"         comcat_estatus_linea el,"   +
				"         comcat_financiera cf"   +
				"   WHERE lc.ic_epo = cn.ic_epo_pyme_if"   +
				"    AND lc.ic_epo = ce.ic_epo"   +
				"    AND lc.cc_acuse = ac4.cc_acuse"   +
				"    AND lc.ic_moneda = cm.ic_moneda"   +
				"    AND lc.ic_estatus_linea = el.ic_estatus_linea"   +
				"    AND lc.ic_financiera = cf.ic_financiera (+)"   +
				"    AND lc.cg_tipo_solicitud = ? "   +
				"    AND cn.cg_tipo = ? "   +
				"    AND lc.ic_if = ? ");
			conditions.add("I");
			conditions.add("E");
			conditions.add(ses_ic_if);

			/*if (!"".equals(tipo_solic)) {
				qryDM.append(" and lc.cg_tipo_solicitud = ? ");
				conditions.add(tipo_solic);
			}*/
			if (!"".equals(ic_epo)){
				qryDM.append(" and lc.ic_epo = ? ");
				conditions.add(ic_epo);
			}
			if (!"".equals(ic_estatus_linea)) {
				qryDM.append(" and lc.ic_estatus_linea = ? ");
				conditions.add(ic_estatus_linea);
			}
			if (!("".equals(dc_fecha_auto_de)) && !("".equals(dc_fecha_auto_a))) {
				qryDM.append("    AND TRUNC(ac4.df_acuse) BETWEEN TRUNC(TO_DATE(?,'dd/mm/yyyy')) AND TRUNC(TO_DATE(?,'dd/mm/yyyy')) ");
				conditions.add(dc_fecha_auto_de);
				conditions.add(dc_fecha_auto_a);
			}
			querySentencia.append(qryDM.toString());
			
		}else if("C".equals(tipo_credito)){
			qryCCC.append(
				" SELECT /*+ use_nl(lc,cn,cp,cm,el,cpl,cf)"   +
				"         index(lc in_com_linea_credito_01_nuk)"   +
				"         index(cn cp_comrel_nafin_pk)*/"   +
				" 	   lc.ic_linea_credito AS folio, 'Modalidad 2 (Riesgo Distribuidor)' AS tipoCredito,"   +
				"         'C' AS ic_tipoCredito,"   +
				"        DECODE (lc.cg_tipo_solicitud,'I', 'Inicial', 'A', 'Ampliacion', 'R', 'Renovacion') AS tipo_sol,"   +
				"        'N/A' AS nEpo, 'N/A' AS EPO, TO_CHAR (cn.ic_nafin_electronico) AS NPyme, cp.cg_razon_social AS Pyme, ic_linea_credito_padre AS lineaPadre,"   +
				"        TO_CHAR (lc.df_captura, 'DD/MM/YYYY') AS FechaSolic,"   +
				"        TO_CHAR (lc.df_autorizacion_nafin, 'DD/MM/YYYY') AS fecha_auto,"   +
				"        cm.cd_nombre AS moneda, el.cd_descripcion, lc.cg_causa_rechazo AS causas, cpl.in_plazo_dias AS plazo,"   +
				"        TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') AS fecha_venc,"   +
				"        cf.cd_nombre, lc.cg_numero_cuenta AS cuenta_epo, lc.cg_numero_cuenta_if AS cuenta_if, lc.fn_monto_autorizado_total, lc.FN_SALDO_LINEA_TOTAL AS saldo_total, lc.ic_moneda"   +
				" 		,el.ic_estatus_linea"   +
				" 		,TO_CHAR(lc.df_cambio,'dd/mm/yyyy') AS fecha_cambio, '' AS estatus_asigna"   +
				"   FROM com_linea_credito lc,"   +
				"        comrel_nafin cn,"   +
				"        comcat_pyme cp,"   +
				"        comcat_moneda cm,"   +
				"        comcat_estatus_linea el,"   +
				"        comcat_plazo cpl,"   +
				"        comcat_financiera cf"   +
				"  WHERE lc.ic_pyme = cn.ic_epo_pyme_if"   +
				"   AND lc.ic_pyme = cp.ic_pyme"   +
				"   AND lc.ic_moneda = cm.ic_moneda"   +
				"   AND lc.ic_estatus_linea = el.ic_estatus_linea"   +
				"   AND lc.ic_plazo = cpl.ic_plazo(+)"   +
				"   AND lc.ic_financiera = cf.ic_financiera (+)"   +
				"   AND lc.cg_tipo_solicitud = ? "   +
				"   AND cn.cg_tipo = ? "   +
				"   AND lc.ic_if = ? "+
				"	 AND lc.cs_factoraje_con_rec = ? ");
			conditions.add("I");
			conditions.add("P");
			conditions.add(ses_ic_if);
			conditions.add("N");

			/*if (!"".equals(tipo_solic)) {
				qryCCC.append(" and lc.cg_tipo_solicitud = ? ");
				conditions.add(tipo_solic);
			}*/
			if (!"".equals(ic_pyme)){
				qryCCC.append(" and lc.ic_pyme = ? ");
				conditions.add(ic_pyme);
			}
			if (!"".equals(ic_estatus_linea)) {
				qryCCC.append(" and lc.ic_estatus_linea =  ? ");
				conditions.add(ic_estatus_linea);
			}
			if (!("".equals(dc_fecha_solic_de)) && !("".equals(dc_fecha_solic_a))){
				qryCCC.append("    AND TRUNC(lc.df_captura) BETWEEN TRUNC(TO_DATE(?,'dd/mm/yyyy')) AND TRUNC(TO_DATE(?,'dd/mm/yyyy')) ");
				conditions.add(dc_fecha_solic_de);
				conditions.add(dc_fecha_solic_a);
			}
			if (!("".equals(dc_fecha_auto_de)) && !("".equals(dc_fecha_auto_a))) {
				qryCCC.append("    AND TRUNC(lc.df_autorizacion_nafin) BETWEEN TRUNC(TO_DATE(?,'dd/mm/yyyy')) AND TRUNC(TO_DATE(?,'dd/mm/yyyy')) ");
				conditions.add(dc_fecha_solic_de);
				conditions.add(dc_fecha_solic_a);
			}
			querySentencia.append(qryCCC.toString());

		}else if("F".equals(tipo_credito)){

			qryFF.append(
				" SELECT /*+ use_nl(lc,cn,cp,cm,el,cpl,cf)"   +
				"         index(lc in_com_linea_credito_01_nuk)"   +
				"         index(cn cp_comrel_nafin_pk)*/"   +
				" 	   lc.ic_linea_credito AS folio, 'Factoraje con recurso' AS tipoCredito,"   +
				"         'F' AS ic_tipoCredito,"   +
				"        DECODE (lc.cg_tipo_solicitud,'I', 'Inicial', 'A', 'Ampliacion', 'R', 'Renovacion') AS tipo_sol,"   +
				"        'N/A' AS nEpo, 'N/A' AS EPO, TO_CHAR (cn.ic_nafin_electronico) AS NPyme, cp.cg_razon_social AS Pyme, ic_linea_credito_padre AS lineaPadre,"   +
				"        TO_CHAR (lc.df_captura, 'DD/MM/YYYY') AS FechaSolic,"   +
				//"        TO_CHAR (lc.df_autorizacion_nafin, 'DD/MM/YYYY') AS fecha_auto,"   +
				"        TO_CHAR (lc.df_captura, 'DD/MM/YYYY') AS fecha_auto,"   +
				"        cm.cd_nombre AS moneda, el.cd_descripcion, lc.cg_causa_rechazo AS causas, cpl.in_plazo_dias AS plazo,"   +
				"        TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') AS fecha_venc,"   +
				"        cf.cd_nombre, lc.cg_numero_cuenta AS cuenta_epo, lc.cg_numero_cuenta_if AS cuenta_if, lc.fn_monto_autorizado_total, lc.FN_SALDO_LINEA_TOTAL AS saldo_total, lc.ic_moneda"   +
				" 		,el.ic_estatus_linea"   +
				" 		,TO_CHAR(lc.df_cambio,'dd/mm/yyyy hh24:mm:ss') AS fecha_cambio, '' AS estatus_asigna"+
				"	, lc.ig_plazo_max, lc.ig_cuenta_bancaria, lc.cg_usuario_cambio, lc.cs_factoraje_con_rec "+
				"   FROM com_linea_credito lc,"   +
				"        comrel_nafin cn,"   +
				"        comcat_pyme cp,"   +
				"        comcat_moneda cm,"   +
				"        comcat_estatus_linea el,"   +
				"        comcat_plazo cpl,"   +
				"        comcat_financiera cf"   +
				"  WHERE lc.ic_pyme = cn.ic_epo_pyme_if"   +
				"   AND lc.ic_pyme = cp.ic_pyme"   +
				"   AND lc.ic_moneda = cm.ic_moneda"   +
				"   AND lc.ic_estatus_linea = el.ic_estatus_linea"   +
				"   AND lc.ic_plazo = cpl.ic_plazo(+)"   +
				"   AND lc.ic_financiera = cf.ic_financiera (+)"   +
				"   AND lc.cg_tipo_solicitud = ? "   +
				"   AND cn.cg_tipo = ? "   +
				"   AND lc.ic_if = ? "+
				"	AND lc.cs_factoraje_con_rec = ? ");
			conditions.add("I");
			conditions.add("P");
			conditions.add(ses_ic_if);
			conditions.add("S");

			/*if (!"".equals(tipo_solic)) {
				qryFF.append(" and lc.cg_tipo_solicitud = ? ");
				conditions.add(tipo_solic);
			}*/
			if (!"".equals(ic_pyme)){
				qryFF.append(" and lc.ic_pyme = ? ");
				conditions.add(ic_pyme);
			}
			if (!"".equals(ic_estatus_linea)) {
				qryFF.append(" and lc.ic_estatus_linea =  ? ");
				conditions.add(ic_estatus_linea);
			}
			if (!("".equals(dc_fecha_solic_de)) && !("".equals(dc_fecha_solic_a))){
				qryFF.append("    AND TRUNC(lc.df_captura) BETWEEN TRUNC(TO_DATE(?,'dd/mm/yyyy')) AND TRUNC(TO_DATE(?,'dd/mm/yyyy')) ");
				conditions.add(dc_fecha_solic_de);
				conditions.add(dc_fecha_solic_a);
			}
			if (!("".equals(dc_fecha_auto_de)) && !("".equals(dc_fecha_auto_a))) {
				qryFF.append("    AND TRUNC(lc.df_autorizacion_nafin) BETWEEN TRUNC(TO_DATE(?,'dd/mm/yyyy')) AND TRUNC(TO_DATE(?,'dd/mm/yyyy')) ");
				conditions.add(dc_fecha_solic_de);
				conditions.add(dc_fecha_solic_a);
			}
			querySentencia.append(qryFF.toString());

		}else{

			qryDM.append(
				"  SELECT /*+ use_nl(lc,ac4,cn,ce,cm,el,cf)"   +
				"         index(lc in_dis_linea_credito_dm_01_NUK)"   +
				"         index(cn cp_comrel_nafin_pk)*/"   +
				"         lc.ic_linea_credito_dm AS folio,  'Modalidad 1 (Riesgo Empresa de Primer Orden )' AS tipoCredito,"   +
				"         'D' AS ic_tipoCredito,"   +
				"         DECODE (lc.cg_tipo_solicitud,'I', 'Inicial','A', 'Ampliacion','R', 'Renovacion') AS tipo_sol,"   +
				"         TO_CHAR (cn.ic_nafin_electronico) AS nEpo, ce.cg_razon_social AS EPO, 'N/A' AS NPyme, 'N/A' AS Pyme, ic_linea_credito_dm_padre AS lineaPadre,"   +
				"         'N/A' AS FechaSolic, TO_CHAR (ac4.df_acuse, 'DD/MM/YYYY') AS fecha_auto, cm.cd_nombre AS moneda,"   +
				"         el.cd_descripcion, '' AS causas, lc.ig_plazo AS plazo,"   +
				"         TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') AS fecha_venc,"   +
				"         cf.cd_nombre, lc.cg_num_cuenta_epo AS cuenta_epo, lc.cg_num_cuenta_if AS cuenta_if, lc.fn_monto_autorizado_total,lc.FN_SALDO_TOTAL AS saldo_total, lc.ic_moneda"   +
				"  		,el.ic_estatus_linea"   +
				"  		,TO_CHAR(lc.df_cambio,'dd/mm/yyyy') AS fecha_cambio, '' AS estatus_asigna"   +
				"    FROM dis_linea_credito_dm lc,"   +
				"         dis_acuse4 ac4,"   +
				"         comrel_nafin cn,"   +
				"         comcat_epo ce,"   +
				"         comcat_moneda cm,"   +
				"         comcat_estatus_linea el,"   +
				"         comcat_financiera cf"   +
				"   WHERE lc.ic_epo = cn.ic_epo_pyme_if"   +
				"    AND lc.ic_epo = ce.ic_epo"   +
				"    AND lc.cc_acuse = ac4.cc_acuse"   +
				"    AND lc.ic_moneda = cm.ic_moneda"   +
				"    AND lc.ic_estatus_linea = el.ic_estatus_linea"   +
				"    AND lc.ic_financiera = cf.ic_financiera (+)"   +
				"    AND lc.cg_tipo_solicitud = ? "   +
				"    AND cn.cg_tipo = ? "   +
				"    AND lc.ic_if = ? ");
			conditions.add("I");
			conditions.add("E");
			conditions.add(ses_ic_if);

			/*if (!"".equals(tipo_solic)) {
				qryDM.append(" and lc.cg_tipo_solicitud = ? ");
				conditions.add(tipo_solic);
			}*/
			if (!"".equals(ic_epo)){
				qryDM.append(" and lc.ic_epo = ? ");
				conditions.add(ic_epo);
			}
			if (!"".equals(ic_estatus_linea)) {
				qryDM.append(" and lc.ic_estatus_linea = ? ");
				conditions.add(ic_estatus_linea);
			}
			if (!("".equals(dc_fecha_auto_de)) && !("".equals(dc_fecha_auto_a))) {
				qryDM.append("    AND TRUNC(ac4.df_acuse) BETWEEN TRUNC(TO_DATE(?,'dd/mm/yyyy')) AND TRUNC(TO_DATE(?,'dd/mm/yyyy')) ");
				conditions.add(dc_fecha_auto_de);
				conditions.add(dc_fecha_auto_a);
			}

			qryCCC.append(
				" SELECT /*+ use_nl(lc,cn,cp,cm,el,cpl,cf)"   +
				"         index(lc in_com_linea_credito_01_nuk)"   +
				"         index(cn cp_comrel_nafin_pk)*/"   +
				" 	   lc.ic_linea_credito AS folio, 'Modalidad 2 (Riesgo Distribuidor)' AS tipoCredito,"   +
				"         'C' AS ic_tipoCredito,"   +
				"        DECODE (lc.cg_tipo_solicitud,'I', 'Inicial', 'A', 'Ampliacion', 'R', 'Renovacion') AS tipo_sol,"   +
				"        'N/A' AS nEpo, 'N/A' AS EPO, TO_CHAR (cn.ic_nafin_electronico) AS NPyme, cp.cg_razon_social AS Pyme, ic_linea_credito_padre AS lineaPadre,"   +
				"        TO_CHAR (lc.df_captura, 'DD/MM/YYYY') AS FechaSolic,"   +
				"        TO_CHAR (lc.df_autorizacion_nafin, 'DD/MM/YYYY') AS fecha_auto,"   +
				"        cm.cd_nombre AS moneda, el.cd_descripcion, lc.cg_causa_rechazo AS causas, cpl.in_plazo_dias AS plazo,"   +
				"        TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') AS fecha_venc,"   +
				"        cf.cd_nombre, lc.cg_numero_cuenta AS cuenta_epo, lc.cg_numero_cuenta_if AS cuenta_if, lc.fn_monto_autorizado_total, lc.FN_SALDO_LINEA_TOTAL AS saldo_total, lc.ic_moneda"   +
				" 		,el.ic_estatus_linea"   +
				" 		,TO_CHAR(lc.df_cambio,'dd/mm/yyyy') AS fecha_cambio, '' AS estatus_asigna"   +
				"   FROM com_linea_credito lc,"   +
				"        comrel_nafin cn,"   +
				"        comcat_pyme cp,"   +
				"        comcat_moneda cm,"   +
				"        comcat_estatus_linea el,"   +
				"        comcat_plazo cpl,"   +
				"        comcat_financiera cf"   +
				"  WHERE lc.ic_pyme = cn.ic_epo_pyme_if"   +
				"   AND lc.ic_pyme = cp.ic_pyme"   +
				"   AND lc.ic_moneda = cm.ic_moneda"   +
				"   AND lc.ic_estatus_linea = el.ic_estatus_linea"   +
				"   AND lc.ic_plazo = cpl.ic_plazo(+)"   +
				"   AND lc.ic_financiera = cf.ic_financiera (+)"   +
				"   AND lc.cg_tipo_solicitud = ? "   +
				"   AND cn.cg_tipo = ? "   +
				"   AND lc.ic_if = ? "+
				"	 AND lc.cs_factoraje_con_rec = ? ");
			conditions.add("I");
			conditions.add("P");
			conditions.add(ses_ic_if);
			conditions.add("N");

			/*if (!"".equals(tipo_solic)) {
				qryCCC.append(" and lc.cg_tipo_solicitud = ? ");
				conditions.add(tipo_solic);
			}*/
			if (!"".equals(ic_pyme)){
				qryCCC.append(" and lc.ic_pyme = ? ");
				conditions.add(ic_pyme);
			}
			if (!"".equals(ic_estatus_linea)) {
				qryCCC.append(" and lc.ic_estatus_linea =  ? ");
				conditions.add(ic_estatus_linea);
			}
			if (!("".equals(dc_fecha_solic_de)) && !("".equals(dc_fecha_solic_a))){
				qryCCC.append("    AND TRUNC(lc.df_captura) BETWEEN TRUNC(TO_DATE(?,'dd/mm/yyyy')) AND TRUNC(TO_DATE(?,'dd/mm/yyyy')) ");
				conditions.add(dc_fecha_solic_de);
				conditions.add(dc_fecha_solic_a);
			}
			if (!("".equals(dc_fecha_auto_de)) && !("".equals(dc_fecha_auto_a))) {
				qryCCC.append("    AND TRUNC(lc.df_autorizacion_nafin) BETWEEN TRUNC(TO_DATE(?,'dd/mm/yyyy')) AND TRUNC(TO_DATE(?,'dd/mm/yyyy')) ");
				conditions.add(dc_fecha_solic_de);
				conditions.add(dc_fecha_solic_a);
			}
			querySentencia.append(qryDM.toString()+" UNION ALL "+qryCCC.toString());
		}

		log.debug("..:: qrySentencia: "+querySentencia.toString());
		log.debug("..:: conditions: "+conditions);
		log.info("getDocumentQueryFile(S)");
		return querySentencia.toString();
	}

  	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {

		log.info("crearCustomFile (E)");
		String nombreArchivo	=	"";
		String linea 			= "";
		OutputStreamWriter writer	= null;
		BufferedWriter buffer 		= null;

		if ("CSV".equals(tipo)) {
			try {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
				buffer = new BufferedWriter(writer);
				
				int numreg = 0;
				BigDecimal montoSolMN = new BigDecimal("0.00");
				BigDecimal montoSolDL = new BigDecimal("0.00");
				BigDecimal montoAutoMN = new BigDecimal("0.00");
				BigDecimal montoAutoDL = new BigDecimal("0.00");
				int numRegistrosMN = 0;
				int numRegistrosDL = 0;

				while(rs.next()){

					String rsFolioSolic = "",  rsTipoCredito = "", rsTipoSol = "" , rsNumEpo = "" , rsEpo = "", rsNumPyme = "", rsPyme = "";
					String rsFolioSolicRel = "",  rsFechaSol = "", rsFechaAuto = "" , rsMoneda = "", rsMtoSol = "", rsestatus = "";
					String rsCausas = "",  rsPlazo = "", rsFechaVenc = "" , rsBancoServ = "", rsBcoEpo = "", rsBcoIf = "", rsMontoAuto = "";
					String rsic_moneda = "", rsIcEstatus; //cgTipoCredito= "",
					String fechaCambio	= "", ctaBancaria="", usuarioMod="";

					numreg++;
					rsFolioSolic	= rs.getString(1)==null?"":rs.getString(1);
					rsTipoCredito 	= rs.getString(2)==null?"":rs.getString(2);
					rsTipoSol 		= rs.getString(3)==null?"":rs.getString(3);
					rsNumEpo 		= rs.getString(4)==null?"":rs.getString(4);
					rsEpo 			= rs.getString(5)==null?"":rs.getString(5);
					rsNumPyme 		= rs.getString(6)==null?"":rs.getString(6);
					rsPyme 			= rs.getString(7)==null?"":rs.getString(7);
					rsFolioSolicRel = rs.getString(8)==null?"":rs.getString(8);
					rsFechaSol		= rs.getString(9)==null?"":rs.getString(9);
					rsFechaAuto		= rs.getString(10)==null?"":rs.getString(10);
					rsMoneda		= rs.getString(11)==null?"":rs.getString(11);
					rsestatus		= rs.getString(12)==null?"":rs.getString(12);
					rsCausas		= rs.getString(13)==null?"":rs.getString(13);
					rsPlazo			= rs.getString(14)==null?"":rs.getString(14);
					rsFechaVenc		= rs.getString(15)==null?"":rs.getString(15);
					rsBancoServ		= rs.getString(16)==null?"":rs.getString(16);
					rsBcoEpo		= rs.getString(17)==null?"":rs.getString(17);
					rsBcoIf			= rs.getString(18)==null?"":rs.getString(18);
					rsMontoAuto		= rs.getString(19)==null?"0":rs.getString(19);
					rsMtoSol		= rs.getString(20)==null?"0":rs.getString(20);
					rsic_moneda		= rs.getString(21)==null?"":rs.getString(21);
					rsIcEstatus		= rs.getString(22)==null?"":rs.getString(22);
					fechaCambio		= rs.getString("fecha_cambio")==null?"":rs.getString("fecha_cambio");
					if("F".equals(tipo_credito)){
						ctaBancaria		= rs.getString("ig_cuenta_bancaria")==null?"":rs.getString("ig_cuenta_bancaria");
						usuarioMod		= rs.getString("cg_usuario_cambio")==null?"":rs.getString("cg_usuario_cambio");
						//rsFechaAuto		= rsFechaSol;
					}
					//cgTipoCredito = rsTipoCredito.substring(0,1);

					if(numreg!=0){
						if(numreg==1){
							if("F".equals(tipo_credito)){	//fodea 019 factoraje con recurso
								linea =	"\n Num. EPO,EPO,Folio de solicitud,Tipo de Credito,Monto autorizado,Saldo Disponible,Moneda,Fecha autorizaci�n,"+
											"Fecha de vencimiento,Estatus actual,Cuenta Bancaria,Usuario �ltima modificaci�n,Fecha y hora �ltima modificaci�n";
							}else{
								linea =	"\n Num. EPO,EPO,Folio de solicitud,Tipo de Credito,Monto autorizado,Saldo Disponible,Moneda,Fecha autorizaci�n,"+
											"Fecha de vencimiento,Estatus actual,Fecha de Cambio de estatus";
							}
							buffer.write(linea);
						}
					}//if !0
					if("F".equals(tipo_credito)){	//fodea 019 factoraje con recurso
						linea =	"\n"+rsNumEpo+","+rsEpo.replace(',',' ')+","+rsFolioSolic+","+rsTipoCredito+","+
												rsMontoAuto+","+rsMtoSol+","+rsMoneda.replace(',',' ')+","+rsFechaAuto+
												","+rsFechaVenc+","+rsestatus+","+ctaBancaria+","+usuarioMod+","+fechaCambio;
												//rsNumPyme+","+rsPyme.replace(',',' ')+","+rsTipoSol+","+rsFolioSolicRel+","+
												//rsFechaSol+","+rsCausas.replace(',',' ')+","+rsBancoServ.replace(',',' ')+","+rsBcoEpo+","+rsBcoIf;
					}else{
						linea =	"\n"+rsNumEpo+","+rsEpo.replace(',',' ')+","+rsFolioSolic+","+rsTipoCredito+","+
												rsMontoAuto+","+rsMtoSol+","+rsMoneda.replace(',',' ')+","+rsFechaAuto+","+rsFechaVenc+","+rsestatus+","+fechaCambio;
												//rsNumPyme+","+rsPyme.replace(',',' ')+","+rsTipoSol+","+rsFolioSolicRel+","+
												//rsFechaSol+","+rsCausas.replace(',',' ')+","+rsBancoServ.replace(',',' ')+","+rsBcoEpo+","+rsBcoIf;
					}
					buffer.write(linea);

					if ("1".equals(rsic_moneda)){
						numRegistrosMN++;
						if (rsMtoSol!=null&&!"".equals(rsMtoSol)) montoSolMN = montoSolMN.add(new BigDecimal(rsMtoSol));
						if (rsMontoAuto!=null&&!"".equals(rsMontoAuto)) montoAutoMN = montoAutoMN.add(new BigDecimal(rsMontoAuto));
					}
					else {
						if ("54".equals(rsic_moneda)){
							numRegistrosDL++;
							if (rsMtoSol!=null&&!"".equals(rsMtoSol)) montoSolDL = montoSolDL.add(new BigDecimal(rsMtoSol));
							if (rsMontoAuto!=null&&!"".equals(rsMontoAuto)) montoAutoDL = montoAutoDL.add(new BigDecimal(rsMontoAuto));
						}
					}
				}//while
				linea =	"\nTotales Moneda Nacional,"+numRegistrosMN+",,,"+montoAutoMN+","+montoSolMN.toPlainString();
				buffer.write(linea);
				linea =	"\nTotales Dolares,"+numRegistrosDL+",,,"+montoAutoDL.toPlainString()+","+montoSolDL.toPlainString();
				buffer.write(linea);
				buffer.close();
			}catch (Throwable e)	{
				throw new AppException("Error al generar el archivo", e);
			}finally{
				try {
					rs.close();
				}catch(Exception e) {}
			}
		} else 		if ("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				int nRow = 0;
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				int numRegistrosMN = 0;
				int numRegistrosDL = 0;
				BigDecimal montoSolMN = new BigDecimal("0.00");
				BigDecimal montoSolDL = new BigDecimal("0.00");
				BigDecimal montoAutoMN = new BigDecimal("0.00");
				BigDecimal montoAutoDL = new BigDecimal("0.00");
				
				String rsFolioSolic = "",  rsTipoCredito = "", rsTipoSol = "" , rsNumEpo = "" , rsEpo = "", rsNumPyme = "", rsPyme = "";
				String rsFolioSolicRel = "",  rsFechaSol = "", rsFechaAuto = "" , rsMoneda = "", rsMtoSol = "", rsestatus = "";
				String rsCausas = "",  rsPlazo = "", rsFechaVenc = "" , rsBancoServ = "", rsBcoEpo = "", rsBcoIf = "", rsMontoAuto = "";
				String rsic_moneda = "", rsIcEstatus;	//cgTipoCredito= "",
				String fechaCambio	= "",	ctaBancaria="",	usuarioMod="";

				while (rs.next()) {
					if(nRow == 0){
						String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
						String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
						String diaActual    = fechaActual.substring(0,2);
						String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
						String anioActual   = fechaActual.substring(6,10);
						String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
						pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
						session.getAttribute("iNoNafinElectronico").toString(),
						(String)session.getAttribute("sesExterno"),
						(String) session.getAttribute("strNombre"),
						(String) session.getAttribute("strNombreUsuario"),
						(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
						
						pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
						pdfDoc.addText(" ","formas", ComunesPDF.CENTER);
						int nCols=0;
						if("F".equals(tipo_credito)){	//fodea 019 factoraje con recurso
							nCols = 13;
						}else{
							nCols = 11;
						}
						pdfDoc.setLTable(nCols, 100);
						pdfDoc.setLCell("Num. EPO", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setLCell("EPO", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setLCell("Folio de solicitud", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setLCell("Tipo de cr�dito", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto autorizado", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setLCell("Saldo disponible", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setLCell("Moneda", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha autorizaci�n", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha de vencimiento", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setLCell("Estatus actual", "formasmenB", ComunesPDF.CENTER);
						if("F".equals(tipo_credito)){	//fodea 019 factoraje con recurso
							pdfDoc.setLCell("Cuenta Bancaria","formasmenB",ComunesPDF.CENTER);
							pdfDoc.setLCell("Usuario �ltima modificaci�n","formasmenB",ComunesPDF.CENTER);
							pdfDoc.setLCell("Fecha y hora �ltima modificaci�n","formasmenB",ComunesPDF.CENTER);
						}else{
							pdfDoc.setLCell("Fecha de cambio de estatus", "formasmenB", ComunesPDF.CENTER);
						}
						pdfDoc.setLHeaders();
					}
					rsFolioSolic	= rs.getString(1)==null?"":rs.getString(1);
					rsTipoCredito 	= rs.getString(2)==null?"":rs.getString(2);
					rsTipoSol 		= rs.getString(3)==null?"":rs.getString(3);
					rsNumEpo 		= rs.getString(4)==null?"":rs.getString(4);
					rsEpo 			= rs.getString(5)==null?"":rs.getString(5);
					rsNumPyme 		= rs.getString(6)==null?"":rs.getString(6);
					rsPyme 			= rs.getString(7)==null?"":rs.getString(7);
					rsFolioSolicRel = rs.getString(8)==null?"":rs.getString(8);
					rsFechaSol		= rs.getString(9)==null?"":rs.getString(9);
					rsFechaAuto		= rs.getString(10)==null?"":rs.getString(10);
					rsMoneda			= rs.getString(11)==null?"":rs.getString(11);
					rsestatus		= rs.getString(12)==null?"":rs.getString(12);
					rsCausas			= rs.getString(13)==null?"":rs.getString(13);
					rsPlazo			= rs.getString(14)==null?"":rs.getString(14);
					rsFechaVenc		= rs.getString(15)==null?"":rs.getString(15);
					rsBancoServ		= rs.getString(16)==null?"":rs.getString(16);
					rsBcoEpo			= rs.getString(17)==null?"":rs.getString(17);
					rsBcoIf			= rs.getString(18)==null?"":rs.getString(18);
					rsMontoAuto		= rs.getString(19)==null?"0":rs.getString(19);
					rsMtoSol			= rs.getString(20)==null?"0":rs.getString(20);
					rsic_moneda		= rs.getString(21)==null?"":rs.getString(21);
					rsIcEstatus		= rs.getString(22)==null?"":rs.getString(22);
					fechaCambio		= rs.getString("fecha_cambio")==null?"":rs.getString("fecha_cambio");
					if("F".equals(tipo_credito)){
						ctaBancaria		= rs.getString("ig_cuenta_bancaria")==null?"":rs.getString("ig_cuenta_bancaria");
						usuarioMod		= rs.getString("cg_usuario_cambio")==null?"":rs.getString("cg_usuario_cambio");
						//rsFechaAuto		= rsFechaSol;
					}
					//cgTipoCredito	= rsTipoCredito.substring(0,1);

					if ("1".equals(rsic_moneda)){
						numRegistrosMN++;
						if (rsMtoSol!=null&&!"".equals(rsMtoSol)) montoSolMN = montoSolMN.add(new BigDecimal(rsMtoSol));
						if (rsMontoAuto!=null&&!"".equals(rsMontoAuto)) montoAutoMN = montoAutoMN.add(new BigDecimal(rsMontoAuto));
					}
					else {
						if ("54".equals(rsic_moneda)){
							numRegistrosDL++;
							if (rsMtoSol!=null&&!"".equals(rsMtoSol)) montoSolDL = montoSolDL.add(new BigDecimal(rsMtoSol));
							if (rsMontoAuto!=null&&!"".equals(rsMontoAuto)) montoAutoDL = montoAutoDL.add(new BigDecimal(rsMontoAuto));
						}
					}
					pdfDoc.setLCell(rsNumEpo, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(rsEpo, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(rsFolioSolic, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(rsTipoCredito, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell("$ "+Comunes.formatoDecimal(rsMontoAuto,2), "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell("$ "+Comunes.formatoDecimal(rsMtoSol,2), "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(rsMoneda, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(rsFechaAuto, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(rsFechaVenc, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(rsestatus, "formas", ComunesPDF.CENTER);
					if("F".equals(tipo_credito)){	//fodea 019 factoraje con recurso
						pdfDoc.setLCell(ctaBancaria, "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(usuarioMod, "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(fechaCambio, "formas", ComunesPDF.CENTER);
					}else{
						pdfDoc.setLCell(fechaCambio, "formas", ComunesPDF.CENTER);	
					}
					nRow++;
				}
				if (nRow > 0){
					if (numRegistrosMN>0){
						pdfDoc.setLCell("Totales Moneda Nacional", "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(Integer.toString(numRegistrosMN), "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell("", "formas", ComunesPDF.CENTER,2);
						pdfDoc.setLCell("$ "+Comunes.formatoDecimal(montoAutoMN,2), "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell("$ "+Comunes.formatoDecimal(montoSolMN.toPlainString(),2), "formas", ComunesPDF.CENTER);
						if("F".equals(tipo_credito)){	//fodea 019 factoraje con recurso
							pdfDoc.setLCell("", "formas", ComunesPDF.CENTER,7);
						}else{
							pdfDoc.setLCell("", "formas", ComunesPDF.CENTER,5);
						}
					}
					if (numRegistrosDL>0){
						pdfDoc.setLCell("Totales D�lares", "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(Integer.toString(numRegistrosDL), "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell("", "formas", ComunesPDF.CENTER,2);
						pdfDoc.setLCell("$ "+Comunes.formatoDecimal(montoAutoDL.toPlainString(),2), "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell("$ "+Comunes.formatoDecimal(montoSolDL.toPlainString(),2), "formas", ComunesPDF.CENTER);
						if("F".equals(tipo_credito)){	//fodea 019 factoraje con recurso
							pdfDoc.setLCell("", "formas", ComunesPDF.CENTER,7);
						}else{
							pdfDoc.setLCell("", "formas", ComunesPDF.CENTER,5);
						}
					}
					pdfDoc.addLTable();
				}else{
					pdfDoc.addText("No se encontr� ning�n registro","formas", ComunesPDF.CENTER);
				}
				pdfDoc.endDocument();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
			}
		}
		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}

	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		String nombreArchivo			=	"";
		if ("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				int nRow = 0;
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				int numRegistrosMN = 0;
				int numRegistrosDL = 0;
				BigDecimal montoSolMN = new BigDecimal("0.00");
				BigDecimal montoSolDL = new BigDecimal("0.00");
				BigDecimal montoAutoMN = new BigDecimal("0.00");
				BigDecimal montoAutoDL = new BigDecimal("0.00");
				
				String rsFolioSolic = "",  rsTipoCredito = "", rsTipoSol = "" , rsNumEpo = "" , rsEpo = "", rsNumPyme = "", rsPyme = "";
				String rsFolioSolicRel = "",  rsFechaSol = "", rsFechaAuto = "" , rsMoneda = "", rsMtoSol = "", rsestatus = "";
				String rsCausas = "",  rsPlazo = "", rsFechaVenc = "" , rsBancoServ = "", rsBcoEpo = "", rsBcoIf = "", rsMontoAuto = "";
				String rsic_moneda = "", rsIcEstatus;	//cgTipoCredito= "",
				String fechaCambio	= "",	ctaBancaria="",	usuarioMod="";

				while (reg.next()) {
					if(nRow == 0){
						String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
						String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
						String diaActual    = fechaActual.substring(0,2);
						String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
						String anioActual   = fechaActual.substring(6,10);
						String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
						pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
						session.getAttribute("iNoNafinElectronico").toString(),
						(String)session.getAttribute("sesExterno"),
						(String) session.getAttribute("strNombre"),
						(String) session.getAttribute("strNombreUsuario"),
						(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
						
						pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
						pdfDoc.addText(" ","formas", ComunesPDF.CENTER);
						int nCols=0;
						if("F".equals(tipo_credito)){	//fodea 019 factoraje con recurso
							nCols = 13;
						}else{
							nCols = 11;
						}
						pdfDoc.setLTable(nCols, 100);
						pdfDoc.setLCell("Num. EPO", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setLCell("EPO", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setLCell("Folio de solicitud", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setLCell("Tipo de cr�dito", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto autorizado", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setLCell("Saldo disponible", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setLCell("Moneda", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha autorizaci�n", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha de vencimiento", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setLCell("Estatus actual", "formasmenB", ComunesPDF.CENTER);
						if("F".equals(tipo_credito)){	//fodea 019 factoraje con recurso
							pdfDoc.setLCell("Cuenta Bancaria","formasmenB",ComunesPDF.CENTER);
							pdfDoc.setLCell("Usuario �ltima modificaci�n","formasmenB",ComunesPDF.CENTER);
							pdfDoc.setLCell("Fecha y hora �ltima modificaci�n","formasmenB",ComunesPDF.CENTER);
						}else{
							pdfDoc.setLCell("Fecha de cambio de estatus", "formasmenB", ComunesPDF.CENTER);
						}
					}
					rsFolioSolic	= reg.getString(1)==null?"":reg.getString(1);
					rsTipoCredito 	= reg.getString(2)==null?"":reg.getString(2);
					rsTipoSol 		= reg.getString(3)==null?"":reg.getString(3);
					rsNumEpo 		= reg.getString(4)==null?"":reg.getString(4);
					rsEpo 			= reg.getString(5)==null?"":reg.getString(5);
					rsNumPyme 		= reg.getString(6)==null?"":reg.getString(6);
					rsPyme 			= reg.getString(7)==null?"":reg.getString(7);
					rsFolioSolicRel = reg.getString(8)==null?"":reg.getString(8);
					rsFechaSol		= reg.getString(9)==null?"":reg.getString(9);
					rsFechaAuto		= reg.getString(10)==null?"":reg.getString(10);
					rsMoneda			= reg.getString(11)==null?"":reg.getString(11);
					rsestatus		= reg.getString(12)==null?"":reg.getString(12);
					rsCausas			= reg.getString(13)==null?"":reg.getString(13);
					rsPlazo			= reg.getString(14)==null?"":reg.getString(14);
					rsFechaVenc		= reg.getString(15)==null?"":reg.getString(15);
					rsBancoServ		= reg.getString(16)==null?"":reg.getString(16);
					rsBcoEpo			= reg.getString(17)==null?"":reg.getString(17);
					rsBcoIf			= reg.getString(18)==null?"":reg.getString(18);
					rsMontoAuto		= reg.getString(19)==null?"0":reg.getString(19);
					rsMtoSol			= reg.getString(20)==null?"0":reg.getString(20);
					rsic_moneda		= reg.getString(21)==null?"":reg.getString(21);
					rsIcEstatus		= reg.getString(22)==null?"":reg.getString(22);
					fechaCambio		= reg.getString("fecha_cambio")==null?"":reg.getString("fecha_cambio");
					if("F".equals(tipo_credito)){
						ctaBancaria		= reg.getString("ig_cuenta_bancaria")==null?"":reg.getString("ig_cuenta_bancaria");
						usuarioMod		= reg.getString("cg_usuario_cambio")==null?"":reg.getString("cg_usuario_cambio");
						//rsFechaAuto		= rsFechaSol;
					}
					//cgTipoCredito	= rsTipoCredito.substring(0,1);

					if ("1".equals(rsic_moneda)){
						numRegistrosMN++;
						if (rsMtoSol!=null&&!"".equals(rsMtoSol)) montoSolMN = montoSolMN.add(new BigDecimal(rsMtoSol));
						if (rsMontoAuto!=null&&!"".equals(rsMontoAuto)) montoAutoMN = montoAutoMN.add(new BigDecimal(rsMontoAuto));
					}
					else {
						if ("54".equals(rsic_moneda)){
							numRegistrosDL++;
							if (rsMtoSol!=null&&!"".equals(rsMtoSol)) montoSolDL = montoSolDL.add(new BigDecimal(rsMtoSol));
							if (rsMontoAuto!=null&&!"".equals(rsMontoAuto)) montoAutoDL = montoAutoDL.add(new BigDecimal(rsMontoAuto));
						}
					}
					pdfDoc.setLCell(rsNumEpo, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(rsEpo, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(rsFolioSolic, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(rsTipoCredito, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell("$ "+Comunes.formatoDecimal(rsMontoAuto,2), "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell("$ "+Comunes.formatoDecimal(rsMtoSol,2), "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(rsMoneda, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(rsFechaAuto, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(rsFechaVenc, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(rsestatus, "formas", ComunesPDF.CENTER);
					if("F".equals(tipo_credito)){	//fodea 019 factoraje con recurso
						pdfDoc.setLCell(ctaBancaria, "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(usuarioMod, "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(fechaCambio, "formas", ComunesPDF.CENTER);
					}else{
						pdfDoc.setLCell(fechaCambio, "formas", ComunesPDF.CENTER);	
					}
					nRow++;
				}
				if (nRow > 0){
					if (numRegistrosMN>0){
						pdfDoc.setLCell("Totales Moneda Nacional", "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(Integer.toString(numRegistrosMN), "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell("", "formas", ComunesPDF.CENTER,2);
						pdfDoc.setLCell("$ "+Comunes.formatoDecimal(montoAutoMN,2), "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell("$ "+Comunes.formatoDecimal(montoSolMN.toPlainString(),2), "formas", ComunesPDF.CENTER);
						if("F".equals(tipo_credito)){	//fodea 019 factoraje con recurso
							pdfDoc.setLCell("", "formas", ComunesPDF.CENTER,7);
						}else{
							pdfDoc.setLCell("", "formas", ComunesPDF.CENTER,5);
						}
					}
					if (numRegistrosDL>0){
						pdfDoc.setLCell("Totales D�lares", "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell(Integer.toString(numRegistrosDL), "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell("", "formas", ComunesPDF.CENTER,2);
						pdfDoc.setLCell("$ "+Comunes.formatoDecimal(montoAutoDL.toPlainString(),2), "formas", ComunesPDF.CENTER);
						pdfDoc.setLCell("$ "+Comunes.formatoDecimal(montoSolDL.toPlainString(),2), "formas", ComunesPDF.CENTER);
						if("F".equals(tipo_credito)){	//fodea 019 factoraje con recurso
							pdfDoc.setLCell("", "formas", ComunesPDF.CENTER,7);
						}else{
							pdfDoc.setLCell("", "formas", ComunesPDF.CENTER,5);
						}
					}
					pdfDoc.addLTable();
				}else{
					pdfDoc.addText("No se encontr� ning�n registro","formas", ComunesPDF.CENTER);
				}
				pdfDoc.endDocument();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
			}
		}
		return nombreArchivo;
	}

	/**
	  Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}

/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public void setIc_if(String ses_ic_if) {
		this.ses_ic_if = ses_ic_if;
	}

	public void setTipoCredito(String tipo_credito) {
		this.tipo_credito = tipo_credito;
	}

	public void setTipoSolic(String tipo_solic) {
		this.tipo_solic = tipo_solic;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}

	public void setIc_estatus(String ic_estatus_linea) {
		this.ic_estatus_linea = ic_estatus_linea;
	}
	
	public void setFechaSolic_de(String dc_fecha_solic_de) {
		this.dc_fecha_solic_de = dc_fecha_solic_de;
	}

	public void setFechaSolic_a(String dc_fecha_solic_a) {
		this.dc_fecha_solic_a = dc_fecha_solic_a;
	}

	public void setFechaAuto_de(String dc_fecha_auto_de) {
		this.dc_fecha_auto_de = dc_fecha_auto_de;
	}

	public void setFechaAuto_a(String dc_fecha_auto_a) {
		this.dc_fecha_auto_a = dc_fecha_auto_a;
	}

}