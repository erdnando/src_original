package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class EstadoAdeudosPyme2 implements IQueryGeneratorRegExtJS {
  public EstadoAdeudosPyme2() { }

	private List conditions;
	StringBuffer strQuery;
	StringBuffer condiciones;
  
  String claveEpo = "";
  String clavePyme = "";
  String claveIf = "";
  String claveMoneda = "";
  String fecha_inicio = "";
  String fecha_final = "";
  String cgTipoCredito = "";
  String cliente = "";
  String bancoRef = "";
  
  private static final Log log = ServiceLocator.getInstance().getLog(EstadoAdeudosPyme2.class);//Variable para enviar mensajes al log.  
  
  public String getAggregateCalculationQuery() {
    return "";
  }

  public String getDocumentQuery() {

     return "";
  }
  public String getDocumentSummaryQueryForIds(List pageIds) {
    return "";
  }

	public String getDocumentQueryFile(){
		conditions	= new ArrayList();
		strQuery		= new StringBuffer();
		condiciones = new StringBuffer();
		condiciones.append("");
		if(!"".equals(claveEpo)){
			condiciones.append(" AND doc.ic_epo = ? AND pe.ic_epo = ? AND cpe.ic_epo = ? ");
		}
		if(!"".equals(claveIf)){
			condiciones.append(" AND lc.ic_if = ? ");
		}
		if(!"".equals(fecha_inicio)&&!"".equals(fecha_final)&&fecha_inicio!=null&&fecha_final!=null) {
			condiciones.append(" AND ((    doc.ic_estatus_docto = 4 AND TRUNC (ac3.df_fecha_hora) BETWEEN "+
									" TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND "+
									" TRUNC (TO_DATE (?, 'dd/mm/yyyy'))) "+
									" OR (    doc.ic_estatus_docto IN (11,  22) "+
									" AND doc.ic_documento IN "+
									" (SELECT ic_documento FROM dis_cambio_estatus WHERE dc_fecha_cambio IN "+
									" (SELECT MAX (dc_fecha_cambio) FROM dis_cambio_estatus "+
									" WHERE ic_documento = doc.ic_documento AND TRUNC (dc_fecha_cambio) BETWEEN "+
									"			TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND "+
									"        TRUNC (TO_DATE (?, 'dd/mm/yyyy'))) "+
									"        AND ic_cambio_estatus IN (18,  19,  20))))");
		}
		strQuery.append(" SELECT TO_CHAR (ac3.df_fecha_hora, 'DD/MM/YYYY') AS fecha_opera, cpe.cg_num_distribuidor AS num_cliente, e.cg_razon_social AS epo, "+
		"       ig_numero_docto AS num_docto, doc.ic_documento AS num_credito, TO_CHAR (sol.df_v_credito, 'DD/MM/YYYY') AS f_venc_credito, "+
		"       doc.fn_monto AS monto_cred, DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
		"          'N', 'Sin conversion','P', 'Peso-Dolar','') AS tipo_conversion, sel.fn_importe_interes AS int_generados, "+
		"       DECODE (doc.ic_estatus_docto, 11, doc.fn_monto, 0) AS pago_a_capital, '' AS pago_int, "+
		"       DECODE (doc.ic_estatus_docto,4, doc.fn_monto,22, doc.fn_monto,0) AS saldo_cre, "+
		"       tc.cd_descripcion AS tipo_cob_int, ed.cg_desc_alterna AS estatus ,tc.ic_tipo_cobro_interes,doc.ic_estatus_docto, "+
		"       sel.fn_importe_recibir AS monto_doc_final "+
		"	FROM dis_documento doc, dis_solicitud sol, dis_docto_seleccionado sel, comrel_pyme_epo cpe, comcat_epo e, "+
		"       comrel_producto_epo pe, comcat_producto_nafin pn, comcat_tipo_cobro_interes tc, dis_linea_credito_dm lc, "+
		"       com_acuse3 ac3, comcat_estatus_docto ed "+
		"	WHERE sol.ic_documento = doc.ic_documento "+
		"		AND sel.ic_documento = doc.ic_documento "+
		"		AND cpe.ic_pyme = doc.ic_pyme "+
		"		AND e.ic_epo = doc.ic_epo "+
		"		AND pn.ic_producto_nafin = pe.ic_producto_nafin "+
		"		AND pe.ic_epo = doc.ic_epo "+
		"		AND cpe.ic_epo = pe.ic_epo "+
		"		AND lc.ic_tipo_cobro_interes = tc.ic_tipo_cobro_interes "+
		"		AND ac3.cc_acuse = sol.cc_acuse "+
		"		AND ed.ic_estatus_docto = doc.ic_estatus_docto "+
		"		AND lc.ic_linea_credito_dm = doc.ic_linea_credito_dm "+
		"		AND doc.ic_pyme = ? "+
		"		AND cpe.ic_pyme = ? "+
		"		AND doc.ic_moneda = ? "+
		"		AND pn.ic_producto_nafin = 4 "+
		"		AND pe.ic_producto_nafin = 4 "+
		"		AND lc.ic_producto_nafin = 4 "+ condiciones.toString());
		conditions.add(new Integer(clavePyme));
		conditions.add(new Integer(clavePyme));
		conditions.add(new Integer(claveMoneda));
		if(!"".equals(claveEpo)){
			conditions.add(new Integer(claveEpo));
			conditions.add(new Integer(claveEpo));
			conditions.add(new Integer(claveEpo));
		}
		if(!"".equals(claveIf)){
			conditions.add(new Integer(claveIf));
		}
		if(!"".equals(fecha_inicio)&&!"".equals(fecha_final)&&fecha_inicio!=null&&fecha_final!=null) {
			conditions.add(fecha_inicio);
			conditions.add(fecha_final);
			conditions.add(fecha_inicio);
			conditions.add(fecha_final);
		}
		log.debug("getDocumentQuery)"+strQuery.toString());
		log.debug("getDocumentQuery)"+conditions);
		return strQuery.toString();
	}
  	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		log.info("crearCustomFile (E)");
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo			=	"";
		String rs_fecha_opera	 	=	"";
		String rs_num_cliente	 	=	"";
		String rs_nombre_epo	 		=	"";
		String rs_num_docto	 		=	"";
		String rs_num_credito	 	=	"";
		String rs_f_venc_credito	=	"";
		String rs_monto_cred	 		=	"";
		String rs_tipo_conversion	=	"";
		String rs_int_generados		=	"";
		String rs_pago_a_capital	=	"";
		String rs_pago_int		 	=	"";
		String rs_saldo_cre	 		=	"";
		String rs_tipo_cob_int		=	"";
		String rs_estatus				=	"";
		String rs_ic_tipo_cob_int	=	"";
		String rs_ic_estatus_docto	=	"";
		String rs_monto_docto_final=	"";
		String tipo_moneda			=	"";
		double total_pagado			=	0;
		int 		totalreg				=	0;
		double	totalMontoCre		=	0;
		double	totalMontoFinal	=	0;
		double	totalInt				=	0;
		double	totalPago			=	0;
		double	totalSaldo			=	0;
		double	totalTotPagado		=	0;

    
	if ("CSV".equals(tipo)) {
		try {
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
			buffer = new BufferedWriter(writer);
			linea = "Res�men estado de adeudos\nCliente,"+cliente.replace(',',' ')+"\nBanco de Referencia,"+bancoRef.replace(',',' ')+"\n";
			buffer.write(linea);
			int numRegistros = 0;
			while (rs.next()) {
				numRegistros++;
				rs_fecha_opera	 	= (rs.getString(1)==null)?"":rs.getString(1);
				rs_num_cliente	 	= (rs.getString(2)==null)?"":rs.getString(2);
				rs_nombre_epo	 	= (rs.getString(3)==null)?"":rs.getString(3);
				rs_num_docto	 	= (rs.getString(4)==null)?"":rs.getString(4);
				rs_num_credito	 	= (rs.getString(5)==null)?"":rs.getString(5);
				rs_f_venc_credito	= (rs.getString(6)==null)?"":rs.getString(6);
				rs_monto_cred	 	= (rs.getString(7)==null)?"":rs.getString(7);
				rs_tipo_conversion	= (rs.getString(8)==null)?"":rs.getString(8);
				rs_int_generados	= (rs.getString(9)==null)?"":rs.getString(9);
				rs_pago_a_capital	= (rs.getString(10)==null)?"":rs.getString(10);
				rs_pago_int		 	= (rs.getString(11)==null)?"":rs.getString(11);
				rs_saldo_cre	 	= (rs.getString(12)==null)?"":rs.getString(12);
				rs_tipo_cob_int	 	= (rs.getString(13)==null)?"":rs.getString(13);
				rs_estatus			= (rs.getString(14)==null)?"":rs.getString(14);
				rs_ic_tipo_cob_int 	= (rs.getString(15)==null)?"":rs.getString(15);
				rs_ic_estatus_docto	= (rs.getString(16)==null)?"":rs.getString(16);
				rs_monto_docto_final= (rs.getString("monto_doc_final")==null)?"":rs.getString("monto_doc_final");
				rs_pago_int = (rs_pago_int.equals(""))?"0":rs_pago_int;
				total_pagado = Double.parseDouble(rs_pago_a_capital) + Double.parseDouble(rs_pago_int);
				if("1".equals(rs_ic_tipo_cob_int)){  //Pago Anticipado
					rs_pago_int = rs_int_generados;
				}else {
					if("2".equals(rs_ic_tipo_cob_int)){  //Pago al Vencimiento
						if("11".equals(rs_ic_estatus_docto)){
							rs_pago_int = rs_int_generados;
						}else{
							rs_pago_int = "0";
						}
					}
				}
				totalreg++;
				totalTotPagado += total_pagado;
				if (!"".equals(rs_monto_cred)){
					totalMontoCre += Double.parseDouble(rs_monto_cred);
					totalMontoFinal += Double.parseDouble(rs_monto_docto_final);
				}
				if (!"".equals(rs_pago_int)){
					totalInt += Double.parseDouble(rs_pago_int);
				}
				if (!"".equals(rs_pago_a_capital)){
					totalPago += Double.parseDouble(rs_pago_a_capital);
				}
				if (!"".equals(rs_saldo_cre)){
					totalSaldo += Double.parseDouble(rs_saldo_cre);
				}
				if (numRegistros == 1){
					linea = "\nMovimientos del "+fecha_inicio+" al "+fecha_final+" "+
								"\nFecha de operaci�n,EPO,Num. de docto. inicial,Monto docto. inicial,Num. de docto. final,Monto docto. final,Fecha de vencimiento final,Estatus,Intereses generados,Capital pagado,Intereses Pagados,Total Pagado,Saldo del cr�dito,Tipo de cobro de intereses";
					buffer.write(linea);
				}
				linea = "\n"+rs_fecha_opera+","+rs_nombre_epo.replace(',',' ')+","+rs_num_docto+","+rs_monto_cred+","+rs_num_credito+","+rs_monto_docto_final+","+rs_f_venc_credito+","+rs_estatus+","+rs_int_generados+","+rs_pago_a_capital+","+rs_pago_int+","+total_pagado+","+rs_saldo_cre+","+rs_tipo_cob_int;
				buffer.write(linea);
			}
			/*if(totalreg>0){
				linea	=	"\nTotal,"+totalreg+",,"+totalMontoCre+",,"+totalMontoFinal+",,,,"+totalPago+","+totalInt+","+totalTotPagado+","+totalSaldo;
				buffer.write(linea);
			}*/
			buffer.close();
		}catch (Throwable e)	{
			throw new AppException("Error al generar el archivo", e);
		}finally{
			try {
				rs.close();
			}catch(Exception e) {}
		}
        
	}else if ("PDF".equals(tipo)) {
		try {
			HttpSession session = request.getSession();
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			int nRow = 0;
			ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
			while (rs.next()) {
				rs_fecha_opera	 	= (rs.getString(1)==null)?"":rs.getString(1);
				rs_num_cliente	 	= (rs.getString(2)==null)?"":rs.getString(2);
				rs_nombre_epo	 	= (rs.getString(3)==null)?"":rs.getString(3);
				rs_num_docto	 	= (rs.getString(4)==null)?"":rs.getString(4);
				rs_num_credito	 	= (rs.getString(5)==null)?"":rs.getString(5);
				rs_f_venc_credito	= (rs.getString(6)==null)?"":rs.getString(6);
				rs_monto_cred	 	= (rs.getString(7)==null)?"":rs.getString(7);
				rs_tipo_conversion	= (rs.getString(8)==null)?"":rs.getString(8);
				rs_int_generados	= (rs.getString(9)==null)?"":rs.getString(9);
				rs_pago_a_capital	= (rs.getString(10)==null)?"":rs.getString(10);
				rs_pago_int		 	= (rs.getString(11)==null)?"":rs.getString(11);
				rs_saldo_cre	 	= (rs.getString(12)==null)?"":rs.getString(12);
				rs_tipo_cob_int	 	= (rs.getString(13)==null)?"":rs.getString(13);
				rs_estatus			= (rs.getString(14)==null)?"":rs.getString(14);
				rs_ic_tipo_cob_int 	= (rs.getString(15)==null)?"":rs.getString(15);
				rs_ic_estatus_docto	= (rs.getString(16)==null)?"":rs.getString(16);
				rs_monto_docto_final= (rs.getString("monto_doc_final")==null)?"":rs.getString("monto_doc_final");
				rs_pago_int = (rs_pago_int.equals(""))?"0":rs_pago_int;
				if("1".equals(rs_ic_tipo_cob_int)){  //Pago Anticipado
					rs_pago_int = rs_int_generados;
				}else {
					if("2".equals(rs_ic_tipo_cob_int)){  //Pago al Vencimiento
						if("11".equals(rs_ic_estatus_docto)){
							rs_pago_int = rs_int_generados;
						}else{
							rs_pago_int = "0";
						}
					}
				}
				total_pagado = Double.parseDouble(rs_pago_a_capital) + Double.parseDouble(rs_pago_int);
				totalreg++;
				totalTotPagado += total_pagado;
				if (!"".equals(rs_monto_cred)){
					totalMontoCre += Double.parseDouble(rs_monto_cred);
					totalMontoFinal += Double.parseDouble(rs_monto_docto_final);
				}
				if (!"".equals(rs_pago_int)){
					totalInt += Double.parseDouble(rs_pago_int);
				}
				if (!"".equals(rs_pago_a_capital)){
					totalPago += Double.parseDouble(rs_pago_a_capital);
				}
				if (!"".equals(rs_saldo_cre)){
					totalSaldo += Double.parseDouble(rs_saldo_cre);
				}
				/*if (claveMoneda == "1"){
					tipo_moneda = "MONEDA_NACIONAL";
				}else{
					if (claveMoneda == "54"){
						tipo_moneda = "DOLAR AMERICANO";
					}	
				}*/
				if(nRow == 0){
					String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
					String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
					String diaActual    = fechaActual.substring(0,2);
					String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
					String anioActual   = fechaActual.substring(6,10);
					String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
					pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					session.getAttribute("iNoNafinElectronico").toString(),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
					
					pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
					pdfDoc.addText(" ","formas", ComunesPDF.CENTER);
					pdfDoc.addText("Cliente:					"+this.cliente, "formas", ComunesPDF.LEFT);
					pdfDoc.addText("Banco de Referencia:	"+this.bancoRef, "formas", ComunesPDF.LEFT);
					pdfDoc.setLTable(14, 100);
					pdfDoc.setLCell("Fecha de operaci�n", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("EPO", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("No. docto. inicial", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto docto. inicial", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("No. docto. final", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto docto. final", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Fecha vencimiento final", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Estatus", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Intereses generados", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Capital pagado", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Intereses Pagados", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Total Pagado", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Saldo del Cr�dito", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Tipo Cobro Intereses", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLHeaders();
				}
				pdfDoc.setLCell(rs_fecha_opera, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setLCell(rs_nombre_epo, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setLCell(rs_num_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(rs_monto_cred,2),"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setLCell(rs_num_credito, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(rs_monto_docto_final,2),"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setLCell(rs_f_venc_credito, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setLCell(rs_estatus, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(rs_int_generados,2),"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(rs_pago_a_capital,2),"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(rs_int_generados,2),"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(total_pagado,2),"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(rs_saldo_cre,2),"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setLCell(rs_tipo_cob_int, "formasmen", ComunesPDF.CENTER);
				nRow++;
			}
			/*if(totalreg>0){
				pdfDoc.setCell("TOTAL "+tipo_moneda,"celda01rep",ComunesPDF.LEFT,4);
			}*/
			pdfDoc.addLTable();
			pdfDoc.endDocument();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo", e);
		} finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
	}
	log.info("crearCustomFile (S)");
	return nombreArchivo;
	}
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}

	public List getConditions() {
		return conditions;
	}

	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}

	public String getClaveEpo() {
		return claveEpo;
	}

	public void setClavePyme(String clavePyme) {
		this.clavePyme = clavePyme;
	}

	public String getClavePyme() {
		return clavePyme;
	}

	public void setCgTipoCredito(String cgTipoCredito) {
		this.cgTipoCredito = cgTipoCredito;
	}
	public void setClaveIf(String claveIf){
		this.claveIf = claveIf;
	}
	public void setFechaInicial(String fecha_ini){
		this.fecha_inicio = fecha_ini;
	}
	public void setFechaFinal(String fecha_fin){
		this.fecha_final = fecha_fin;
	}
	public void setClaveMoneda(String moneda){
		this.claveMoneda = moneda;
	}
	public String getCgTipoCredito() {
		return cgTipoCredito;
	}
	public void setCliente(String clie){
		this.cliente = clie;
	}
	public void setBanco(String ref){
		this.bancoRef = ref;
	}

}