package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class AvNotifEPODist  implements IQueryGenerator, IQueryGeneratorRegExtJS {

  public AvNotifEPODist(){}
  
//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(AvNotifEPODist.class);
	private String 	paginaOffset;
	private String 	paginaNo;
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String ic_if;
	private String ic_pyme;
	private String fchinicial;
	private String fchfinal;
	private String ic_epo;
	private String tipo_credito;
	private String responsable;
	private String cgTipoConversion;
	private String numOrden;
	private String publicaDoctosFinanciables;
        private String operaContrato;
	
	public String getAggregateCalculationQuery(HttpServletRequest request) {
		log.debug("::getAggregateCalculationQuery(S) ");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer qryDM			  = new StringBuffer();
		StringBuffer qryCCC		    = new StringBuffer();   
		StringBuffer condicion    = new StringBuffer();
		
		String ic_epo             = request.getSession().getAttribute("iNoCliente").toString();
		String ic_pyme            = (request.getParameter("ic_pyme") == null)?"":request.getParameter("ic_pyme");
		String ic_if              = (request.getParameter("ic_if") == null)?"":request.getParameter("ic_if");
		String fchinicial         = (request.getParameter("Txtfchini") == null)?"":request.getParameter("Txtfchini");
		String fchfinal           = (request.getParameter("Txtffin") == null)?"":request.getParameter("Txtffin");		
		String	tipo_credito		=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String numOrden = (request.getParameter("numeroOrden") == null)?"":request.getParameter("numeroOrden");

		try {
			String iva = this.obtenIVA();
		    condicion.append(" AND D.ic_estatus_docto in(4,11,32)  ");
			  
			if(!ic_pyme.equals(""))	 
				condicion.append(" AND D.IC_PYME =  " +	ic_pyme) ;
			if (!fchinicial.equals("") && !fchfinal.equals(""))
				condicion.append(" AND trunc(C3.df_fecha_hora) BETWEEN TO_DATE('"+fchinicial+"','DD/MM/YYYY') and TO_DATE('"+fchfinal+"','DD/MM/YYYY') ");		
			if(ic_if.equals("") && ic_pyme.equals("") && fchinicial.equals("") && fchfinal.equals(""))	 
				condicion.append(" AND TO_CHAR(C3.df_fecha_hora,'DD/MM/YYYY')=TO_CHAR(SYSDATE,'DD/MM/YYYY') ");		
			
			qryDM.append(
			"   SELECT /*+ index(d CP_DIS_DOCUMENTO_PK)"   +
			"     index(ds CP_DIS_DOCTO_SELECCIONADO_PK)"   +
			"     index(s CP_DIS_SOLICITUD_PK)    "   +
			"     index(m CP_COMCAT_MONEDA_PK)"   +
			"     index(tc CP_COM_TIPO_CAMBIO_PK)"   +
			"     index(pe CP_COMREL_PRODUCTO_EPO_PK)"   +
			"     index(lcd CP_DIS_LINEA_CREDITO_DM_PK)"   +
			"   */"   +
			"    d.ic_moneda as moneda, m.cd_nombre as nommoneda, d.fn_monto as monto"   +
			"   ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
			"   ,DS.fn_importe_recibir as MONTO_CREDITO"   +
			"   ,DS.fn_importe_interes as MONTO_INTERES"   +
			"   ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
			"   FROM DIS_DOCUMENTO D"   +
			"  ,DIS_DOCTO_SELECCIONADO DS"   +
			"  ,DIS_SOLICITUD S"   +
			"  ,COM_ACUSE3 C3"   +
			"  ,COMREL_PRODUCTO_EPO PE"   +
			"  ,COMCAT_PRODUCTO_NAFIN PN"   +
			"  ,COMCAT_MONEDA M"   +
			"  ,COM_TIPO_CAMBIO TC"   +
			"  ,DIS_LINEA_CREDITO_DM LCD"   +
			"  ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
			"  ,COMCAT_TASA CT"   +
			"  WHERE D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
			"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
			"  AND D.IC_EPO = PE.IC_EPO"   +
			"  AND D.IC_MONEDA = M.IC_MONEDA"   +
			"  AND D.IC_EPO = PE.IC_EPO"   +
			"  AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM"   +
			"  AND DS.IC_TASA = CT.IC_TASA"   +
			"  AND S.CC_ACUSE = C3.CC_ACUSE"   +
			"  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"  AND M.IC_MONEDA = TC.IC_MONEDA"   +
			"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
			"  AND LCD.IC_IF = IEP.IC_IF"   +
			"  AND IEP.IC_EPO = D.IC_EPO"   +
			"  AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"  AND PN.IC_PRODUCTO_NAFIN = 4"  +
			"  AND D.ic_epo = "+ic_epo+
			condicion.toString()  );
			if(!ic_if.equals("")){	 
				qryDM.append(" AND LCD.IC_IF =  " + ic_if) ; 
			}
				
		qryCCC.append(
			"  	SELECT /*+ index(d CP_DIS_DOCUMENTO_PK)"   +   
			"     index(ds CP_DIS_DOCTO_SELECCIONADO_PK)"   +
			"     index(s CP_DIS_SOLICITUD_PK)    "   +
			"     index(m CP_COMCAT_MONEDA_PK)"   +
			"     index(tc CP_COM_TIPO_CAMBIO_PK)"   +
			"     index(pe CP_COMREL_PRODUCTO_EPO_PK)"   +
			"     index(lcd CP_COM_LINEA_CREDITO_PK)"   +
			"   */"   +
			"    d.ic_moneda as moneda, m.cd_nombre as nommoneda, d.fn_monto as monto"   +
			"	 ,DECODE (d.ic_estatus_docto,32, DS.fn_importe_recibir - (DS.fn_importe_recibir * ((NVL (ds.fn_porc_comision_apli, 0)/100))*1."+iva+")) AS monto_depositar "+
			"   ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
			"   ,DS.fn_importe_recibir as MONTO_CREDITO"   +
			"   ,DS.fn_importe_interes as MONTO_INTERES"   +
			"   ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
			"    FROM DIS_DOCUMENTO D"   +
			"   ,DIS_DOCTO_SELECCIONADO DS"   +
			"   ,DIS_SOLICITUD S"   +
			" 	,COM_BINS_IF cbi"+////////////////
			" 	,dis_doctos_pago_tc ddptc"+
			"   ,COM_ACUSE3 C3"   +
			"   ,COMREL_PRODUCTO_EPO PE"   +
			"   ,COMCAT_PRODUCTO_NAFIN PN"   +
			"   ,COMCAT_MONEDA M"   +
			"   ,COM_TIPO_CAMBIO TC"   +
			"   ,COM_LINEA_CREDITO LCD"   +
			"   ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
			"   ,COMCAT_TASA CT"   +
			"   WHERE D.IC_EPO = PE.IC_EPO"   +
			"  AND d.IC_BINS = cbi.IC_BINS(+)"  +//////////////////
			"   AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			" 	AND d.ic_orden_pago_tc = ddptc.ic_orden_pago_tc(+)" +
			"   AND PN.IC_PRODUCTO_NAFIN = 4"   +
			"   AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
			"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
			"   AND D.IC_EPO = PE.IC_EPO"   +
			"   AND D.IC_MONEDA = M.IC_MONEDA"   +
			"   AND M.IC_MONEDA = TC.IC_MONEDA"   +
			"   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
			"   AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
			"   AND (LCD.IC_IF = IEP.IC_IF OR cbi.IC_IF = IEP.IC_IF) "   +//////////////
			"   AND IEP.IC_EPO = D.IC_EPO"   +
			"   AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"   AND DS.IC_TASA = CT.IC_TASA"   +
			"   AND S.CC_ACUSE = C3.CC_ACUSE"  +
			"  AND D.ic_epo = "+ic_epo+
	  		condicion.toString());
			if(!ic_if.equals("")){
				qryCCC.append(" AND (LCD.IC_IF = "+ic_if+" OR cbi.IC_IF = "+ic_if+")");
				
			}
			if(!numOrden.equals("")){
				qryCCC.append(" and ddptc.ic_orden_pago_tc ="+numOrden);
			}


			String qryAux = "";	
			if("D".equals(tipo_credito))
				qryAux = qryDM.toString();
			else if("C".equals(tipo_credito))    
				qryAux = qryCCC.toString();
			else
				qryAux = qryDM.toString()+ " UNION ALL "+qryCCC.toString();

			qrySentencia.append(
				"SELECT moneda, nommoneda, COUNT (1), SUM (monto), SUM(monto_descuento),SUM(monto_credito),SUM(monto_interes),SUM(monto_depositar) as monto_depositar, 'AvisNotifEpoDist::getAggregateCalculationQuery'"+
				"  FROM ("+qryAux.toString()+")"+
				"GROUP BY  moneda, nommoneda ORDER BY moneda ");

		}catch(Exception e){
			log.debug("::getAggregateCalculationQuery "+e);
		}
		log.debug("getAggregateCalculationQuery(S) ::: "+qrySentencia.toString());
		return qrySentencia.toString();
	}

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids) {
		int i=0;
		StringBuffer qrySentencia	= new StringBuffer();
    	StringBuffer qryDM			= new StringBuffer();
    	StringBuffer qryCCC			= new StringBuffer();
    	StringBuffer condicion		= new StringBuffer();
    	
		//String ic_epo = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
		String tipo_credito		=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		
		condicion.append(" AND d.ic_documento in (");
		i=0;
		for (Iterator it = ids.iterator(); it.hasNext();i++){
        	if(i>0){
				condicion.append(",");
			}
			condicion.append(" "+it.next().toString()+" ");
		}
		condicion.append(") ");
		qryDM.append(
      "   SELECT /*+ index (d CP_DIS_DOCUMENTO_PK) "   +
      "   index (ds CP_DIS_DOCTO_SELECCIONADO_PK)"   +
      "   index (lcd CP_DIS_LINEA_CREDITO_DM_PK) "   +
      "   index (pn CP_COMCAT_PRODUCTO_NAFIN_PK)"   +
      "   index (pe CP_COMREL_PRODUCTO_EPO_PK)"   +
      "   index (m CP_COMCAT_MONEDA_PK)"   +
      "   index (tc CP_COM_TIPO_CAMBIO_PK)"   +
      "   index (e CP_COMCAT_EPO_PK)"   +
      "   index (ci CP_COMCAT_IF_PK ) "   +
      "   index (ed CP_COMCAT_ESTATUS_DOCTO_PK ) "   +
      "   index (tf CP_COMCAT_TIPO_FINANCIAMIENTO_PK ) */  "   +
      "   P.cg_razon_social as DISTRIBUIDOR"   +
      "  ,D.ig_numero_docto"   +
      "  ,D.cc_acuse"   +
      "  ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') as df_fecha_emision"   +
      "  ,TO_CHAR(D.df_carga,'dd/mm/yyyy') as df_fecha_publicacion"   +
      "  ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc"   +
      "  ,D.IG_PLAZO_DOCTO"   +
      "  ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as df_fecha_seleccion"   +
      "  ,M.cd_nombre as MONEDA"   +
      "  ,M.ic_moneda"   +
      "  ,D.fn_monto"   +
      "  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
      "  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
      "  ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) as MONTO_VALUADO"   +
      "  ,D.IG_PLAZO_DESCUENTO"   +
      "  ,D.FN_MONTO"   +
      "  ,D.fn_porc_descuento"   +
      "  ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
      "  ,ED.CD_DESCRIPCION AS ESTATUS"   +
		"  ,ED.IC_ESTATUS_DOCTO AS IC_ESTATUS"   +
		"	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) AS REFERENCIA_INT" +
		"  ,DS.fn_valor_tasa  AS VALOR_TASA"   +
      "  ,D.IG_PLAZO_CREDITO"   +
      "  ,DS.fn_importe_recibir as MONTO_CREDITO"   +
      "  ,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO"   +
      "  ,TCI.ic_tipo_cobro_interes"   +
      "        ,TCI.cd_descripcion as tipo_cobro_interes"   +
      "        ,DS.fn_importe_interes as MONTO_INTERES"   +
      "        ,D.ic_documento"   +
      "        ,E.cg_razon_social as EPO"   +   
      "  , D.ic_documento"   +
      "  ,CI.cg_razon_social as DIF"   +
      "  ,TF.cd_descripcion as MODO_PLAZO"   +
      "  ,to_char(C3.df_fecha_hora,'dd/mm/yyyy') as df_fecha_hora"   +
      "  ,'Modalidad 1 (Riesgo Empresa de Primer Orden)' as tipo_credito"   +
      "  ,LCD.ic_moneda as moneda_linea"   +
		"  ,DS.FN_PORC_COMISION_APLI as COMISION_APLICABLE"   +
		"  ,'' as MONTO_DEPOSITAR"   +
		"  ,'' as MONTO_COMISION"   +
		"  ,TF.IC_TIPO_FINANCIAMIENTO as IC_TIPO_FINANCIAMIENTO"   +
		"  ,'' as IC_ORDEN_ENVIADO"   +
		"  ,'' as NUMERO_TC"   +
		"  ,'' as IC_OPERACION_CCACUSE"   +
		"  ,'' AS codigo_autorizado"   +
		"  ,'' as FECHA_REGISTRO"   +
		"   ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
      "   FROM DIS_DOCUMENTO D"   +
      "  ,DIS_DOCTO_SELECCIONADO DS"   +
      "  ,DIS_SOLICITUD S"   +
      "  ,COM_ACUSE3 C3"   +
      "  ,COMCAT_PYME P"   +
      "  ,DIS_LINEA_CREDITO_DM LCD"   +
      "  ,COMCAT_PRODUCTO_NAFIN PN"   +
      "  ,COMREL_PRODUCTO_EPO PE"   +
      "  ,COMCAT_TASA CT"   +
      "  ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
      "  ,COMCAT_MONEDA M"   +
      "  ,COM_TIPO_CAMBIO TC"   +
      "  ,COMCAT_TIPO_COBRO_INTERES TCI "   +
      "  ,COMCAT_EPO E "   +
      "  ,COMCAT_ESTATUS_DOCTO ED"   +
      "  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
      "  WHERE "   +
      "  D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
      "  AND D.IC_EPO = PE.IC_EPO"   +
      "  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
      "  AND PN.IC_PRODUCTO_NAFIN = 4"   +
      "  AND P.IC_PYME = D.IC_PYME"   +
      "  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +   
      "  AND D.IC_EPO = PE.IC_EPO"   +
      "  AND D.IC_MONEDA = M.IC_MONEDA"   +
      "  AND M.IC_MONEDA = TC.IC_MONEDA"   +
      "  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
      "  AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM"   +
      "  AND LCD.IC_IF = IEP.IC_IF"   +
      "  AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES"   +
      "  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
      "  AND IEP.IC_EPO = D.IC_EPO"   +
      "  AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
      "  AND DS.IC_TASA = CT.IC_TASA"   +
      "  AND D.IC_EPO = E.IC_EPO"   +
      "  AND S.CC_ACUSE = C3.CC_ACUSE"   +
      "  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
		
		
      "  AND LCD.IC_IF = CI.IC_IF"  +
			condicion.toString()  );
			
				
		qryCCC.append(
			"  SELECT  /*+ index (d CP_DIS_DOCUMENTO_PK) "   +
			"   index (ds CP_DIS_DOCTO_SELECCIONADO_PK)"   +
			"   index (lcd CP_COM_LINEA_CREDITO_PK) "   +
			"   index (pn CP_COMCAT_PRODUCTO_NAFIN_PK)"   +
			"   index (pe CP_COMREL_PRODUCTO_EPO_PK)"   +
			"   index (m CP_COMCAT_MONEDA_PK)"   +
			"   index (tc CP_COM_TIPO_CAMBIO_PK)"   +
			"   index (e CP_COMCAT_EPO_PK)"   +
			"   index (ci CP_COMCAT_IF_PK ) "   +
			"   index (ed CP_COMCAT_ESTATUS_DOCTO_PK ) "   +
			"   index (tf CP_COMCAT_TIPO_FINANCIAMIENTO_PK ) */  "   +
			"    P.cg_razon_social as DISTRIBUIDOR"   +
			"   ,D.ig_numero_docto"   +
			"   ,D.cc_acuse"   +
			"   ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') as df_fecha_emision"   +
			"   ,TO_CHAR(D.df_carga,'dd/mm/yyyy') as df_fecha_publicacion"   +
			"   ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc"   +
			"   ,D.IG_PLAZO_DOCTO"   +
			"   ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as df_fecha_seleccion"   +
			"   ,M.cd_nombre as MONEDA"   +
			"   ,M.ic_moneda"   +
			"   ,D.fn_monto"   +
			"   ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
			"   ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
			"   ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) as MONTO_VALUADO"   +
			"   ,D.IG_PLAZO_DESCUENTO"   +
			"   ,D.FN_MONTO"   +
			"   ,D.fn_porc_descuento"   +
			"   ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
			"   ,ED.CD_DESCRIPCION AS ESTATUS"   +
			"  ,ED.IC_ESTATUS_DOCTO AS IC_ESTATUS"   +
			"   ,CT.CD_NOMBRE||' '||ds.cg_rel_mat||' '||ds.fn_puntos AS REFERENCIA_INT"   +
			"   ,DECODE(DS.CG_REL_MAT"   +
			"   ,'+',DS.fn_valor_tasa + DS.fn_puntos"   +
			"   ,'-',DS.fn_valor_tasa - DS.fn_puntos"   +
			"   ,'*',DS.fn_valor_tasa * DS.fn_puntos"   +
			"   ,'/',DS.fn_valor_tasa / DS.fn_puntos"   +
			"   ,0) AS VALOR_TASA"   +
			"   ,D.IG_PLAZO_CREDITO"   +
			"   ,DS.fn_importe_recibir as MONTO_CREDITO"   +
			"   ,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO"   +
			"   ,TCI.ic_tipo_cobro_interes"   +
			"   ,TCI.cd_descripcion as tipo_cobro_interes"   +
			"   ,DS.fn_importe_interes as MONTO_INTERES"   +
			"   ,D.ic_documento"   +
			"   ,E.cg_razon_social as EPO"   +
			"   , D.ic_documento"   +
			"  ,decode(ed.ic_estatus_docto,32,ci2.cg_razon_social,ci.cg_razon_social) AS dif"   +
			"   ,TF.cd_descripcion as MODO_PLAZO"   +
			"   ,to_char(C3.df_fecha_hora,'dd/mm/yyyy') as df_fecha_hora"   +
			"   ,'Modalidad 2 (Riesgo Distribuidor)' as tipo_credito"   +
			"   ,LCD.ic_moneda as moneda_linea"   +
			"  ,DS.FN_PORC_COMISION_APLI as COMISION_APLICABLE"   +
			"  ,'' as MONTO_DEPOSITAR"   +
			"  ,'' as MONTO_COMISION"   +
			"  ,TF.IC_TIPO_FINANCIAMIENTO as IC_TIPO_FINANCIAMIENTO"   +
			"  ,DDPTC.IC_ORDEN_PAGO_TC as IC_ORDEN_ENVIADO"   +
			"  ,DDPTC.CG_NUM_TARJETA as NUMERO_TC"   +
			"  ,DDPTC.CG_TRANSACCION_ID as IC_OPERACION_CCACUSE"   +
			"  ,decode(CONCAT(ddptc.CG_CODIGO_RESP ,ddptc.CG_CODIGO_DESC),'','',ddptc.CG_CODIGO_RESP||'-' ||ddptc.CG_CODIGO_DESC)AS codigo_autorizado"   +
			"  ,TO_CHAR(DDPTC.DF_FECHA_AUTORIZACION,'dd/mm/yyyy') as FECHA_REGISTRO"   +
			"   ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
			"    FROM DIS_DOCUMENTO D"   +
			"  ,DIS_DOCTOS_PAGO_TC DDPTC"   +////////////////7
			"   ,DIS_DOCTO_SELECCIONADO DS"   +
			"   ,DIS_SOLICITUD S"   +
			"   ,COM_ACUSE3 C3"   +
			"   ,COMCAT_PYME P"   +
			"   ,COM_LINEA_CREDITO LCD"   +
			"   ,COMCAT_PRODUCTO_NAFIN PN"   +
			"   ,COMREL_PRODUCTO_EPO PE"   +
			"   ,COMCAT_TASA CT"   +
			"   ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
			"   ,COMCAT_MONEDA M"   +
			"   ,COM_TIPO_CAMBIO TC"   +
			"   ,COMCAT_TIPO_COBRO_INTERES TCI"   +
			"   ,COMCAT_EPO E"   +
			"   ,COMCAT_IF CI"   +
			"	,comcat_if ci2 "+/////////////////agregados 
			"  ,COM_BINS_IF cbi "+/////////////7
			"   ,COMCAT_ESTATUS_DOCTO ED"   +
			"   ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
			"   WHERE D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
			"  AND D.IC_ORDEN_PAGO_TC = DDPTC.IC_ORDEN_PAGO_TC(+)"   +
			"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +  
			"   AND D.IC_EPO = PE.IC_EPO"   +
			"   AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"   AND PN.IC_PRODUCTO_NAFIN = 4"   +
			"   AND P.IC_PYME = D.IC_PYME"   +
			"   AND D.IC_EPO = PE.IC_EPO"   +
			"   AND D.IC_MONEDA = M.IC_MONEDA"   +
			"   AND M.IC_MONEDA = TC.IC_MONEDA"   +
			"   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
			"   AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
			"   AND (LCD.IC_IF = IEP.IC_IF OR cbi.IC_IF = IEP.IC_IF) "   +/////////////////
			"   AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES"   +
			"   AND TF.IC_TIPO_FINANCIAMIENTO = D.IC_TIPO_FINANCIAMIENTO"   +
			"   AND IEP.IC_EPO = D.IC_EPO"   +
			"   AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"   AND DS.IC_TASA = CT.IC_TASA"   +
			"   AND D.IC_EPO = E.IC_EPO"   +
			"   AND S.CC_ACUSE = C3.CC_ACUSE"   +
			"   AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
			"  AND d.IC_BINS = cbi.IC_BINS(+)"  +////////////77
			"  AND cbi.IC_if = ci2.IC_if(+)"  +/////////////////7
			"   AND LCD.IC_IF = CI.IC_IF"  +
			
  		condicion.toString());
		if(!ic_if.equals("")){
				qryCCC.append(" AND (LCD.IC_IF = "+ic_if+" OR cbi.IC_IF = "+ic_if+")");
				
		}
		if(!numOrden.equals("")){
			qryCCC.append(" and ddptc.ic_orden_pago_tc ="+numOrden);
		}

			if("D".equals(tipo_credito))
				qrySentencia.append(qryDM.toString());
			else if("C".equals(tipo_credito))
				qrySentencia.append(qryCCC.toString());
			else
				qrySentencia.append(qryDM.toString()+ " UNION ALL "+qryCCC.toString());

		log.debug("el query queda de la siguiente manera "+qrySentencia.toString());
		return qrySentencia.toString();
	}
	
	public String getDocumentQuery(HttpServletRequest request) {

      StringBuffer qrySentencia = new StringBuffer();
      StringBuffer qryDM			  = new StringBuffer();
      StringBuffer qryCCC		    = new StringBuffer();
      StringBuffer condicion    = new StringBuffer();

      String ic_epo             = request.getSession().getAttribute("iNoCliente").toString();
    	String ic_pyme            = (request.getParameter("ic_pyme") == null)?"":request.getParameter("ic_pyme");
    	String ic_if              = (request.getParameter("ic_if") == null)?"":request.getParameter("ic_if");
    	String fchinicial         = (request.getParameter("Txtfchini") == null)?"":request.getParameter("Txtfchini");
    	String fchfinal           = (request.getParameter("Txtffin") == null)?"":request.getParameter("Txtffin");
		String numOrden = (request.getParameter("numeroOrden") == null)?"":request.getParameter("numeroOrden");

      String	tipo_credito		=	(request.getParameter("tipos_credito")==null)?"":request.getParameter("tipos_credito");
		
		try {

    condicion.append(" AND D.ic_estatus_docto in(4,11,32)  ");
  	
	
	
  	if(!ic_pyme.equals(""))	 
      condicion.append(" AND D.IC_PYME =  " +	ic_pyme) ;  
    if (!fchinicial.equals("") && !fchfinal.equals(""))
      condicion.append(" AND trunc(C3.df_fecha_hora) BETWEEN TO_DATE('"+fchinicial+"','DD/MM/YYYY') and TO_DATE('"+fchfinal+"','DD/MM/YYYY') ");		
    if(ic_if.equals("") && ic_pyme.equals("") && fchinicial.equals("") && fchfinal.equals(""))	 
      condicion.append(" AND TO_CHAR(C3.df_fecha_hora,'DD/MM/YYYY')=TO_CHAR(SYSDATE,'DD/MM/YYYY') ");		
	
		qryDM.append(
  			"  SELECT /*+ INDEX(D CP_DIS_DOCUMENTO_PK)"   +
  			"  INDEX (LCD CP_DIS_LINEA_CREDITO_DM_PK)*/"   +
  			"  d.ic_documento"   +
    		"    FROM DIS_DOCUMENTO D"   +
      		"   ,DIS_DOCTO_SELECCIONADO DS"   +
        	"   ,DIS_SOLICITUD S"   +
  			"   ,COM_ACUSE3 C3"   +
    		"   ,COMREL_PRODUCTO_EPO PE"   +
      		"   ,COMCAT_PRODUCTO_NAFIN PN"   +
  			"   ,DIS_LINEA_CREDITO_DM LCD"   +
    		"   ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
      	"   WHERE D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
  			"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
    		"   AND D.IC_EPO = PE.IC_EPO"   +
      		"   AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM"   +
  			"   AND D.IC_EPO = PE.IC_EPO"   +
    		"   AND S.CC_ACUSE = C3.CC_ACUSE"   +
      		"   AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
  			"   AND LCD.IC_IF = IEP.IC_IF"   +
    		"   AND IEP.IC_EPO = D.IC_EPO"   +
      		"   AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
  			"   AND PN.IC_PRODUCTO_NAFIN = 4"  +
	       "  AND D.IC_EPO = "+ic_epo+
	  		condicion.toString()  );
		if(!ic_if.equals("")){	 
				qryDM.append(" AND LCD.IC_IF =  " + ic_if) ; 
			}
				
		qryCCC.append(
				"   SELECT /*+ INDEX(D CP_DIS_DOCUMENTO_PK)"   +
				"    INDEX (LCD CP_com_LINEA_CREDITO_PK)*/"   +
				"     d.ic_documento"   +
				"     FROM DIS_DOCUMENTO D"   +
				" 	,COM_BINS_IF cbi"+
				"  ,DIS_DOCTOS_PAGO_TC DDPTC"   +
				"    ,DIS_DOCTO_SELECCIONADO DS"   +
				"    ,DIS_SOLICITUD S"   +
				"    ,COM_ACUSE3 C3"   +
				"    ,COMREL_PRODUCTO_EPO PE"   +
				"    ,COMCAT_PRODUCTO_NAFIN PN"   +
				"    ,COM_LINEA_CREDITO LCD"   +
				"    ,COMCAT_TIPO_COBRO_INTERES TCI"   +
				"    ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
				"    WHERE D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
				"  AND d.IC_BINS = cbi.IC_BINS(+)"  +
				"  AND D.IC_ORDEN_PAGO_TC = DDPTC.IC_ORDEN_PAGO_TC(+)"   +
				"    AND D.IC_EPO = PE.IC_EPO"   +
				"    AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
				"    AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
				"    AND S.CC_ACUSE = C3.CC_ACUSE"   +
				"    AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
				"   AND (LCD.IC_IF = IEP.IC_IF OR cbi.IC_IF = IEP.IC_IF) "   +////////////
				"    AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES"   +
				"    AND IEP.IC_EPO = D.IC_EPO"   +
				"    AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
				"    AND PN.IC_PRODUCTO_NAFIN = 4"  +
	      "   AND D.IC_EPO = "  +ic_epo+
	  		condicion.toString());
			if(!ic_if.equals("")){
				qryCCC.append(" AND (LCD.IC_IF = "+ic_if+" OR cbi.IC_IF = "+ic_if+")");
				
			}
			if(!numOrden.equals("")){
				qryCCC.append(" and ddptc.ic_orden_pago_tc ="+numOrden);
			}

			if("D".equals(tipo_credito))
				qrySentencia.append(qryDM.toString());
			else if("C".equals(tipo_credito))
				qrySentencia.append(qryCCC.toString());
			else
				qrySentencia.append(qryDM.toString()+ " UNION ALL "+qryCCC.toString());

			log.debug("EL query de la llave primaria: "+ tipo_credito +"---"+qrySentencia.toString());
		}catch(Exception e){
			log.debug("ConsInfDocEpoDist::getDocumentQueryException "+e);
		}
		return qrySentencia.toString();
	}
	
	public String getDocumentQueryFile(HttpServletRequest request) {
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer qryDM			  = new StringBuffer();
		StringBuffer qryCCC		    = new StringBuffer();
		StringBuffer condicion    = new StringBuffer();
		String ic_epo             = request.getSession().getAttribute("iNoCliente").toString();
		String ic_pyme            = (request.getParameter("ic_pyme") == null)?"":request.getParameter("ic_pyme");
		String ic_if              = (request.getParameter("ic_if") == null)?"":request.getParameter("ic_if");
		String fchinicial         = (request.getParameter("Txtfchini") == null)?"":request.getParameter("Txtfchini");
		String fchfinal           = (request.getParameter("Txtffin") == null)?"":request.getParameter("Txtffin");
		String	tipo_credito		=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		
		try {

		    condicion.append(" AND D.ic_estatus_docto in(4,11,32)  ");
			  
			if(!ic_pyme.equals(""))	 
				condicion.append(" AND D.IC_PYME =  " +	ic_pyme) ;
			if (!fchinicial.equals("") && !fchfinal.equals(""))
				condicion.append(" AND trunc(C3.df_fecha_hora) BETWEEN TO_DATE('"+fchinicial+"','DD/MM/YYYY') and TO_DATE('"+fchfinal+"','DD/MM/YYYY') ");		
			if(ic_if.equals("") && ic_pyme.equals("") && fchinicial.equals("") && fchfinal.equals(""))	 
				condicion.append(" AND TO_CHAR(C3.df_fecha_hora,'DD/MM/YYYY')=TO_CHAR(SYSDATE,'DD/MM/YYYY') ");		
			
		
		qryDM.append(
      "   SELECT /*+ index (d CP_DIS_DOCUMENTO_PK) "   +
      "   index (ds CP_DIS_DOCTO_SELECCIONADO_PK)"   +
      "   index (lcd CP_DIS_LINEA_CREDITO_DM_PK) "   +
      "   index (pn CP_COMCAT_PRODUCTO_NAFIN_PK)"   +
      "   index (pe CP_COMREL_PRODUCTO_EPO_PK)"   +
      "   index (m CP_COMCAT_MONEDA_PK)"   +
      "   index (tc CP_COM_TIPO_CAMBIO_PK)"   +
      "   index (e CP_COMCAT_EPO_PK)"   +
      "   index (ci CP_COMCAT_IF_PK ) "   +
      "   index (ed CP_COMCAT_ESTATUS_DOCTO_PK ) "   +
      "   index (tf CP_COMCAT_TIPO_FINANCIAMIENTO_PK ) */  "   +
      "   P.cg_razon_social as DISTRIBUIDOR"   +
      "  ,D.ig_numero_docto"   +
      "  ,D.cc_acuse"   +
      "  ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') as df_fecha_emision"   +
      "  ,TO_CHAR(D.df_carga,'dd/mm/yyyy') as df_fecha_publicacion"   +
      "  ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc"   +
      "  ,D.IG_PLAZO_DOCTO"   +
      "  ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as df_fecha_seleccion"   +
      "  ,M.cd_nombre as MONEDA"   +
      "  ,M.ic_moneda"   +
      "  ,D.fn_monto"   +
      "  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
      "  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
      "  ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) as MONTO_VALUADO"   +
      "  ,D.IG_PLAZO_DESCUENTO"   +
      "  ,D.FN_MONTO"   +
      "  ,D.fn_porc_descuento"   +
      "  ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
      "  ,ED.CD_DESCRIPCION AS ESTATUS"   +
		"  ,ED.IC_ESTATUS_DOCTO AS IC_ESTATUS"   +
		"	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) AS REFERENCIA_INT" +
		"  ,DS.fn_valor_tasa  AS VALOR_TASA"   +
      "  ,D.IG_PLAZO_CREDITO"   +
      "  ,DS.fn_importe_recibir as MONTO_CREDITO"   +
      "  ,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO"   +
      "  ,TCI.ic_tipo_cobro_interes"   +
      "        ,TCI.cd_descripcion as tipo_cobro_interes"   +
      "        ,DS.fn_importe_interes as MONTO_INTERES"   +
      "        ,D.ic_documento"   +
      "        ,E.cg_razon_social as EPO"   +
      "  , D.ic_documento"   +
      "  ,CI.cg_razon_social as DIF"   +
      "  ,TF.cd_descripcion as MODO_PLAZO"   +
      "  ,to_char(C3.df_fecha_hora,'dd/mm/yyyy') as df_fecha_hora"   +
      "  ,'Modalidad 1 (Riesgo Empresa de Primer Orden)' as tipo_credito"   +
      "  ,LCD.ic_moneda as moneda_linea"   +
		"  ,DS.FN_PORC_COMISION_APLI as COMISION_APLICABLE"   +
		"  ,'' as MONTO_DEPOSITAR"   +
		"  ,'' as MONTO_COMISION"   +
		"  ,TF.IC_TIPO_FINANCIAMIENTO as IC_TIPO_FINANCIAMIENTO"   +
		"  ,'' as IC_ORDEN_ENVIADO"   +
		"  ,'' as NUMERO_TC"   +
		"  '' as IC_OPERACION_CCACUSE"   +
		"  ,'' AS codigo_autorizado"   +
		"  ,'' as FECHA_REGISTRO"   +
		"   ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
      "   FROM DIS_DOCUMENTO D"   +
      "  ,DIS_DOCTO_SELECCIONADO DS"   +
      "  ,DIS_SOLICITUD S"   +
      "  ,COM_ACUSE3 C3"   +
      "  ,COMCAT_PYME P"   +
      "  ,DIS_LINEA_CREDITO_DM LCD"   +
      "  ,COMCAT_PRODUCTO_NAFIN PN"   +
      "  ,COMREL_PRODUCTO_EPO PE"   +
      "  ,COMCAT_TASA CT"   +
      "  ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
      "  ,COMCAT_MONEDA M"   +
      "  ,COM_TIPO_CAMBIO TC"   +
      "  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
      "  ,COMCAT_EPO E"   +
      "  ,COMCAT_IF CI"   +
      "  ,COMCAT_ESTATUS_DOCTO ED"   +
      "  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
      "  WHERE "   +
      "  D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
      "  AND D.IC_EPO = PE.IC_EPO"   +
      "  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
      "  AND PN.IC_PRODUCTO_NAFIN = 4"   +
      "  AND P.IC_PYME = D.IC_PYME"   +
      "  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
      "  AND D.IC_EPO = PE.IC_EPO"   +
      "  AND D.IC_MONEDA = M.IC_MONEDA"   +
      "  AND M.IC_MONEDA = TC.IC_MONEDA"   +
      "  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
      "  AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM"   +
      "  AND LCD.IC_IF = IEP.IC_IF"   +
      "  AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES"   +
      "  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
      "  AND IEP.IC_EPO = D.IC_EPO"   +
      "  AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
      "  AND DS.IC_TASA = CT.IC_TASA"   +
      "  AND D.IC_EPO = E.IC_EPO"   +
      "  AND S.CC_ACUSE = C3.CC_ACUSE"   +
      "  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
      "  AND LCD.IC_IF = CI.IC_IF"  +
			"  AND D.ic_epo = "+ic_epo+
			condicion.toString()  );
		if(!ic_if.equals("")){	 
				qryDM.append(" AND LCD.IC_IF =  " + ic_if) ; 
		}
				
		qryCCC.append(
			"  SELECT  /*+ index (d CP_DIS_DOCUMENTO_PK) "   +
			"   index (ds CP_DIS_DOCTO_SELECCIONADO_PK)"   +
			"   index (lcd CP_COM_LINEA_CREDITO_PK) "   +
			"   index (pn CP_COMCAT_PRODUCTO_NAFIN_PK)"   +
			"   index (pe CP_COMREL_PRODUCTO_EPO_PK)"   +
			"   index (m CP_COMCAT_MONEDA_PK)"   +
			"   index (tc CP_COM_TIPO_CAMBIO_PK)"   +
			"   index (e CP_COMCAT_EPO_PK)"   +
			"   index (ci CP_COMCAT_IF_PK ) "   +
			"   index (ed CP_COMCAT_ESTATUS_DOCTO_PK ) "   +
			"   index (tf CP_COMCAT_TIPO_FINANCIAMIENTO_PK ) */  "   +
			"    P.cg_razon_social as DISTRIBUIDOR"   +
			"   ,D.ig_numero_docto"   +
			"   ,D.cc_acuse"   +
			"   ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') as df_fecha_emision"   +
			"   ,TO_CHAR(D.df_carga,'dd/mm/yyyy') as df_fecha_publicacion"   +
			"   ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc"   +
			"   ,D.IG_PLAZO_DOCTO"   +
			"   ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as df_fecha_seleccion"   +
			"   ,M.cd_nombre as MONEDA"   +
			"   ,M.ic_moneda"   +
			"   ,D.fn_monto"   +
			"   ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
			"   ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
			"   ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) as MONTO_VALUADO"   +
			"   ,D.IG_PLAZO_DESCUENTO"   +
			"   ,D.FN_MONTO"   +
			"   ,D.fn_porc_descuento"   +
			"   ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
			"   ,ED.CD_DESCRIPCION AS ESTATUS"   +
			"  ,ED.IC_ESTATUS_DOCTO AS IC_ESTATUS"   +
			"   ,CT.CD_NOMBRE||' '||ds.cg_rel_mat||' '||ds.fn_puntos AS REFERENCIA_INT"   +
			"   ,DECODE(DS.CG_REL_MAT"   +
			"   ,'+',DS.fn_valor_tasa + DS.fn_puntos"   +
			"   ,'-',DS.fn_valor_tasa - DS.fn_puntos"   +
			"   ,'*',DS.fn_valor_tasa * DS.fn_puntos"   +
			"   ,'/',DS.fn_valor_tasa / DS.fn_puntos"   +
			"   ,0) AS VALOR_TASA"   +
			"   ,D.IG_PLAZO_CREDITO"   +
			"   ,DS.fn_importe_recibir as MONTO_CREDITO"   +
			"   ,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO"   +
			"   ,TCI.ic_tipo_cobro_interes"   +
			"   ,TCI.cd_descripcion as tipo_cobro_interes"   +
			"   ,DS.fn_importe_interes as MONTO_INTERES"   +
			"   ,D.ic_documento"   +
			"   ,E.cg_razon_social as EPO"   +
			"   , D.ic_documento"   +
			"  ,decode(ed.ic_estatus_docto,32,ci2.cg_razon_social,ci.cg_razon_social) AS dif"   +
			"   ,TF.cd_descripcion as MODO_PLAZO"   +
			"   ,to_char(C3.df_fecha_hora,'dd/mm/yyyy') as df_fecha_hora"   +
			"   ,'Modalidad 2 (Riesgo Distribuidor)' as tipo_credito"   +
			"   ,LCD.ic_moneda as moneda_linea"   +
			"  ,DS.FN_PORC_COMISION_APLI as COMISION_APLICABLE"   +
			"  ,'' as MONTO_DEPOSITAR"   +
			"  ,'' as MONTO_COMISION"   +
			"  ,TF.IC_TIPO_FINANCIAMIENTO as IC_TIPO_FINANCIAMIENTO"   +
			"  ,DDPTC.IC_ORDEN_PAGO_TC as IC_ORDEN_ENVIADO"   +
			"  ,DDPTC.CG_NUM_TARJETA as NUMERO_TC"   +
			"  ,DDPTC.CG_TRANSACCION_ID as IC_OPERACION_CCACUSE"   +
			"  ,decode(CONCAT(ddptc.CG_CODIGO_RESP ,ddptc.CG_CODIGO_DESC),'','',ddptc.CG_CODIGO_RESP||'-' ||ddptc.CG_CODIGO_DESC)AS codigo_autorizado"   +
			"  ,TO_CHAR(DDPTC.DF_FECHA_AUTORIZACION,'dd/mm/yyyy') as FECHA_REGISTRO"   +
		  "   ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
			"    FROM DIS_DOCUMENTO D"   +
			"  ,DIS_DOCTOS_PAGO_TC DDPTC"   +//////////////777
			"   ,DIS_DOCTO_SELECCIONADO DS"   +
			"   ,DIS_SOLICITUD S"   +
			"   ,COM_ACUSE3 C3"   +
			"   ,COMCAT_PYME P"   +
			"   ,COM_LINEA_CREDITO LCD"   +
			"   ,COMCAT_PRODUCTO_NAFIN PN"   +
			"   ,COMREL_PRODUCTO_EPO PE"   +
			"   ,COMCAT_TASA CT"   +
			"   ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
			"   ,COMCAT_MONEDA M"   +
			"   ,COM_TIPO_CAMBIO TC"   +
			"   ,COMCAT_TIPO_COBRO_INTERES TCI"   +
			"   ,COMCAT_EPO E"   +
			"   ,COMCAT_IF CI"   +
			"	,comcat_if ci2 "+/////////////////agregados 
			"  ,COM_BINS_IF cbi "+///////////////////7
			"   ,COMCAT_ESTATUS_DOCTO ED"   +
			"   ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
			"   WHERE D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
			"  AND D.IC_ORDEN_PAGO_TC = DDPTC.IC_ORDEN_PAGO_TC(+)"   +
			"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
			"   AND D.IC_EPO = PE.IC_EPO"   +
			"   AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"   AND PN.IC_PRODUCTO_NAFIN = 4"   +
			"   AND P.IC_PYME = D.IC_PYME"   +
			"   AND D.IC_EPO = PE.IC_EPO"   +
			"   AND D.IC_MONEDA = M.IC_MONEDA"   +
			"   AND M.IC_MONEDA = TC.IC_MONEDA"   +
			"   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
			"   AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
			"   AND (LCD.IC_IF = IEP.IC_IF OR cbi.IC_IF = IEP.IC_IF) "   +//////////////
			"   AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES"   +
			"   AND TF.IC_TIPO_FINANCIAMIENTO = D.IC_TIPO_FINANCIAMIENTO"   +
			"   AND IEP.IC_EPO = D.IC_EPO"   +
			"   AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"   AND DS.IC_TASA = CT.IC_TASA"   +
			"   AND D.IC_EPO = E.IC_EPO"   +
			"   AND S.CC_ACUSE = C3.CC_ACUSE"   +
			"   AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
			"   AND d.IC_BINS = cbi.IC_BINS(+)"  +////////////77
			"   AND cbi.IC_if = ci2.IC_if(+)"  +/////////////////7
			"   AND LCD.IC_IF = CI.IC_IF"  +
			"  AND D.ic_epo = "+ic_epo+
	  		condicion.toString());
			if(!ic_if.equals("")){
				qryCCC.append(" AND (LCD.IC_IF = "+ic_if+" OR cbi.IC_IF = "+ic_if+")");
				
			}
			if(!numOrden.equals("")){
				qryCCC.append(" and ddptc.ic_orden_pago_tc ="+numOrden);
			}

			if("D".equals(tipo_credito))
				qrySentencia.append(qryDM.toString());
			else if("C".equals(tipo_credito))
				qrySentencia.append(qryCCC.toString());
			else
				qrySentencia.append(qryDM.toString()+ " UNION ALL "+qryCCC.toString());

		}catch(Exception e){
			log.debug("::getDocumentQueryFileException "+e);
		}
		return qrySentencia.toString();
	}
	
	//** Migraci�n IF

	
	public String getAggregateCalculationQuery() {
	
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer qryDM			  = new StringBuffer();
		StringBuffer qryCCC		    = new StringBuffer();
		StringBuffer condicion    = new StringBuffer();
		StringBuffer qryFdR			= new StringBuffer();   
		
		try {
			String iva = this.obtenIVA();
			
			  
			if(!ic_pyme.equals(""))	 
				condicion.append(" AND D.IC_PYME =  " +	ic_pyme) ;
			if (!fchinicial.equals("") && !fchfinal.equals(""))
				condicion.append(" AND trunc(C3.df_fecha_hora) BETWEEN TO_DATE('"+fchinicial+"','DD/MM/YYYY') and TO_DATE('"+fchfinal+"','DD/MM/YYYY') ");		
			if(ic_if.equals("") && ic_pyme.equals("") && fchinicial.equals("") && fchfinal.equals(""))	 
				condicion.append(" AND TO_CHAR(C3.df_fecha_hora,'DD/MM/YYYY')=TO_CHAR(SYSDATE,'DD/MM/YYYY') ");		
			
			qryDM.append(
				"   SELECT /*+ index(d CP_DIS_DOCUMENTO_PK)"   +
				"     index(ds CP_DIS_DOCTO_SELECCIONADO_PK)"   +
				"     index(s CP_DIS_SOLICITUD_PK)    "   +
				"     index(m CP_COMCAT_MONEDA_PK)"   +
				"     index(tc CP_COM_TIPO_CAMBIO_PK)"   +
				"     index(pe CP_COMREL_PRODUCTO_EPO_PK)"   +
				"     index(lcd CP_DIS_LINEA_CREDITO_DM_PK)"   +
				"   */"   +
				"    d.ic_moneda as moneda, m.cd_nombre as nommoneda, d.fn_monto as monto"   +
				"   ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
				"   ,DS.fn_importe_recibir as MONTO_CREDITO"   +
				"   ,DS.fn_importe_interes as MONTO_INTERES"   +
				"	 ,DECODE (d.ic_estatus_docto,32, DS.fn_importe_recibir - (DS.fn_importe_recibir * (NVL (ds.fn_porc_comision_apli, 0)/100))) AS monto_depositar "+ 
				"   ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				"   FROM DIS_DOCUMENTO D"   +
				"  ,DIS_DOCTO_SELECCIONADO DS"   +
				"  ,DIS_SOLICITUD S"   +
				"  ,COM_ACUSE3 C3"   +
				"  ,COMREL_PRODUCTO_EPO PE"   +
				"  ,COMCAT_PRODUCTO_NAFIN PN"   +
				"  ,COMCAT_MONEDA M"   +
				"  ,COM_TIPO_CAMBIO TC"   +
				"  ,DIS_LINEA_CREDITO_DM LCD"   +
				"  ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
				"  ,COMCAT_TASA CT"   +
				"  WHERE D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
				"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
				"  AND D.IC_EPO = PE.IC_EPO"   +
				"  AND D.IC_MONEDA = M.IC_MONEDA"   +
				"  AND D.IC_EPO = PE.IC_EPO"   +
				"  AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM"   +
				"  AND DS.IC_TASA = CT.IC_TASA"   +
				"  AND S.CC_ACUSE = C3.CC_ACUSE"   +
				"  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
				"  AND M.IC_MONEDA = TC.IC_MONEDA"   +
				"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
				"  AND LCD.IC_IF = IEP.IC_IF"   +
				"  AND IEP.IC_EPO = D.IC_EPO"   +
				"  AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
				"  AND PN.IC_PRODUCTO_NAFIN = 4"  +
				"  AND D.ic_epo = "+ic_epo+
				condicion.toString()  );
			qryDM.append(" AND D.ic_estatus_docto in(4,11,32)  ") ; 
			if(!ic_if.equals("")){	 
				qryDM.append(" AND LCD.IC_IF =  " + ic_if) ; 
			}
				
			qryCCC.append(
			"  	SELECT /*+ index(d CP_DIS_DOCUMENTO_PK)"   +
			"     index(ds CP_DIS_DOCTO_SELECCIONADO_PK)"   +
			"     index(s CP_DIS_SOLICITUD_PK)    "   +
			"     index(m CP_COMCAT_MONEDA_PK)"   +
			"     index(tc CP_COM_TIPO_CAMBIO_PK)"   +
			"     index(pe CP_COMREL_PRODUCTO_EPO_PK)"   +
			"     index(lcd CP_COM_LINEA_CREDITO_PK)"   +
			"   */"   +
			"    d.ic_moneda as moneda, m.cd_nombre as nommoneda, d.fn_monto as monto"   +
			"	 ,DECODE (d.ic_estatus_docto,32, DS.fn_importe_recibir - (DS.fn_importe_recibir * ((NVL (ds.fn_porc_comision_apli, 0)/100))*1."+iva+")) AS monto_depositar "+
			"   ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
			"   ,DS.fn_importe_recibir as MONTO_CREDITO"   +
			"   ,DS.fn_importe_interes as MONTO_INTERES"   +
			"   ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
			"    FROM DIS_DOCUMENTO D"   +
			"   ,DIS_DOCTO_SELECCIONADO DS"   +
			" 	,COM_BINS_IF cbi"+////////////////
			" 	,dis_doctos_pago_tc ddptc"+
			"   ,DIS_SOLICITUD S"   +
			"   ,COM_ACUSE3 C3"   +
			"   ,COMREL_PRODUCTO_EPO PE"   +
			"   ,COMCAT_PRODUCTO_NAFIN PN"   +
			"   ,COMCAT_MONEDA M"   +
			"   ,COM_TIPO_CAMBIO TC"   +
			"   ,COM_LINEA_CREDITO LCD"   +
			"   ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
			"   ,COMCAT_TASA CT"   +
			"   WHERE D.IC_EPO = PE.IC_EPO"   +
			"   AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			" 	AND d.ic_orden_pago_tc = ddptc.ic_orden_pago_tc(+)" +
			"   AND PN.IC_PRODUCTO_NAFIN = 4"   +
			"   AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
			"  AND d.IC_BINS = cbi.IC_BINS(+)"  +//////////////////
			"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
			"   AND D.IC_EPO = PE.IC_EPO"   +
			"   AND D.IC_MONEDA = M.IC_MONEDA"   +
			"   AND M.IC_MONEDA = TC.IC_MONEDA"   +
			"   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
			"   AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
			"   AND (LCD.IC_IF = IEP.IC_IF OR cbi.IC_IF = IEP.IC_IF) "   +//////////////
			"   AND IEP.IC_EPO = D.IC_EPO"   +
			"   AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"   AND DS.IC_TASA = CT.IC_TASA"   +
			"   AND S.CC_ACUSE = C3.CC_ACUSE"  +
			"  AND D.ic_epo = "+ic_epo+
	  		condicion.toString());
			qryCCC.append(" AND D.ic_estatus_docto in(4,11,32)  ") ; 
			if(!ic_if.equals("")){
				qryCCC.append(" AND (LCD.IC_IF = "+ic_if+" OR cbi.IC_IF = "+ic_if+")");
				
			}
			if(!numOrden.equals("")){
				qryCCC.append(" and ddptc.ic_orden_pago_tc ="+numOrden);
			}

			
			qryFdR.append(
				"  	SELECT /*+ index(d CP_DIS_DOCUMENTO_PK)"   +
				"     index(ds CP_DIS_DOCTO_SELECCIONADO_PK)"   +
				"     index(s CP_DIS_SOLICITUD_PK)    "   +
				"     index(m CP_COMCAT_MONEDA_PK)"   +
				"     index(tc CP_COM_TIPO_CAMBIO_PK)"   +
				"     index(pe CP_COMREL_PRODUCTO_EPO_PK)"   +
				"     index(lcd CP_COM_LINEA_CREDITO_PK)"   +
				"   */"   +
				"    d.ic_moneda as moneda, m.cd_nombre as nommoneda, d.fn_monto as monto"   +
				"   ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
				"   ,DS.fn_importe_recibir as MONTO_CREDITO"   +
				"   ,DS.fn_importe_interes as MONTO_INTERES"   +
				"	 ,DECODE (d.ic_estatus_docto,32, DS.fn_importe_recibir - (DS.fn_importe_recibir * (NVL (ds.fn_porc_comision_apli, 0)/100))) AS monto_depositar "+
				"   ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				"    FROM DIS_DOCUMENTO D"   +
				"   ,DIS_DOCTO_SELECCIONADO DS"   +
				"   ,DIS_SOLICITUD S"   +
				"   ,COM_ACUSE3 C3"   +
				"   ,COMREL_PRODUCTO_EPO PE"   +
				"   ,COMCAT_PRODUCTO_NAFIN PN"   +
				"   ,COMCAT_MONEDA M"   +
				"   ,COM_TIPO_CAMBIO TC"   +
				"   ,COM_LINEA_CREDITO LCD"   +
				"   ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
				"   ,COMCAT_TASA CT"   +
				"   WHERE D.IC_EPO = PE.IC_EPO"   +
				"   AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
				"   AND PN.IC_PRODUCTO_NAFIN = 4"   +
				"   AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
				"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   + 
				"   AND D.IC_EPO = PE.IC_EPO"   +
				"   AND D.IC_MONEDA = M.IC_MONEDA"   +
				"   AND M.IC_MONEDA = TC.IC_MONEDA"   +
				"   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
				"   AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
				"   AND LCD.IC_IF = IEP.IC_IF"   +
				"   AND IEP.IC_EPO = D.IC_EPO"   +
				"   AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
				"   AND DS.IC_TASA = CT.IC_TASA"   +
				"   AND S.CC_ACUSE = C3.CC_ACUSE "  + 
				" and d.IC_TIPO_FINANCIAMIENTO is null "+
				" AND LCD.cg_tipo_solicitud = 'I' "+
			  "  AND LCD.cs_factoraje_con_rec = 'S' "+
				"  AND D.ic_epo = "+ic_epo+
				condicion.toString());
				qryFdR.append(" AND D.ic_estatus_docto in(4,11)  ") ; 
				if(!ic_if.equals("")){	 
				qryFdR.append(" AND LCD.IC_IF =  " + ic_if) ; 
			}
			
			String qryAux = "";	
			if("D".equals(tipo_credito))
				qryAux = qryDM.toString();
			else if("C".equals(tipo_credito))
				qryAux = qryCCC.toString(); 
			else if("F".equals(tipo_credito))
				qryAux = qryFdR.toString(); 
			else
				qryAux = qryDM.toString()+ " UNION ALL "+qryCCC.toString();  

			qrySentencia.append(
				"SELECT moneda as ic_moneda, nommoneda as MONEDA, COUNT (1) as NO_REGISTROS, SUM (monto) as MONTO, SUM(monto_descuento) as MONTO_DESCUENTO ,SUM(monto_credito) as MONTO_CREDITO,SUM(monto_interes) as MONTO_INTERES, SUM(monto_depositar) as monto_depositar,'AvisNotifEpoDist::getAggregateCalculationQuery'"+
				"  FROM ("+qryAux.toString()+")"+
				"GROUP BY  moneda, nommoneda ORDER BY moneda ");


		}catch(Exception e){
			log.debug("::getAggregateCalculationQuery "+e);
		}
		log.info("getAggregateCalculationQuery(E)");
		log.info("getAggregateCalculationQuery --> query :: "+qrySentencia.toString());
		return qrySentencia.toString();
		
	}
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
      StringBuffer qrySentencia = new StringBuffer();
      StringBuffer qryDM			  = new StringBuffer();
      StringBuffer qryCCC		    = new StringBuffer();
      StringBuffer condicion    = new StringBuffer();
		StringBuffer qryFdR			= new StringBuffer();
		
		try {

			
			 
			if(!ic_pyme.equals(""))	 
				condicion.append(" AND D.IC_PYME =  " +	ic_pyme) ;
			if (!fchinicial.equals("") && !fchfinal.equals(""))
				condicion.append(" AND trunc(C3.df_fecha_hora) BETWEEN TO_DATE('"+fchinicial+"','DD/MM/YYYY') and TO_DATE('"+fchfinal+"','DD/MM/YYYY') ");		
			if(ic_if.equals("") && ic_pyme.equals("") && fchinicial.equals("") && fchfinal.equals(""))	 
				condicion.append(" AND TO_CHAR(C3.df_fecha_hora,'DD/MM/YYYY')=TO_CHAR(SYSDATE,'DD/MM/YYYY') ");		
			
			qryDM.append(
				"  SELECT /*+ INDEX(D CP_DIS_DOCUMENTO_PK)"   +
				"  INDEX (LCD CP_DIS_LINEA_CREDITO_DM_PK)*/"   +
				"  d.ic_documento"   +
				"    FROM DIS_DOCUMENTO D"   +
				"   ,DIS_DOCTO_SELECCIONADO DS"   +
				"   ,DIS_SOLICITUD S"   +
				"   ,COM_ACUSE3 C3"   +
				"   ,COMREL_PRODUCTO_EPO PE"   +
				"   ,COMCAT_PRODUCTO_NAFIN PN"   +
				"   ,DIS_LINEA_CREDITO_DM LCD"   +
				"   ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
				"   WHERE D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
				"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
				"   AND D.IC_EPO = PE.IC_EPO"   +
				"   AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM"   +
				"   AND D.IC_EPO = PE.IC_EPO"   +
				"   AND S.CC_ACUSE = C3.CC_ACUSE"   +
				"   AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
				"   AND LCD.IC_IF = IEP.IC_IF"   +
				"   AND IEP.IC_EPO = D.IC_EPO"   +
				"   AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
				"   AND PN.IC_PRODUCTO_NAFIN = 4"  +
				"  AND D.IC_EPO = "+ic_epo+
				condicion.toString()  );
				qryDM.append(" AND D.ic_estatus_docto in(4,11,32)  ") ;
				if(!ic_if.equals("")){	 
					qryDM.append(" AND LCD.IC_IF =  " + ic_if) ; 
				}
				
			qryCCC.append(
					"   SELECT /*+ INDEX(D CP_DIS_DOCUMENTO_PK)"   +
					"    INDEX (LCD CP_com_LINEA_CREDITO_PK)*/"   +
					"     d.ic_documento"   +
					"     FROM DIS_DOCUMENTO D"   +
					"    ,DIS_DOCTO_SELECCIONADO DS"   +
					" 	,COM_BINS_IF cbi"+
					"  ,DIS_DOCTOS_PAGO_TC DDPTC"   +
					"    ,DIS_SOLICITUD S"   +
					"    ,COM_ACUSE3 C3"   +
					"    ,COMREL_PRODUCTO_EPO PE"   +
					"    ,COMCAT_PRODUCTO_NAFIN PN"   +
					"    ,COM_LINEA_CREDITO LCD"   +
					"    ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"    ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"    WHERE D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"  AND d.IC_BINS = cbi.IC_BINS(+)"  +
					"  AND D.IC_ORDEN_PAGO_TC = DDPTC.IC_ORDEN_PAGO_TC(+)"   +//////////////7
					"    AND D.IC_EPO = PE.IC_EPO"   +
					"    AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
					"    AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"    AND S.CC_ACUSE = C3.CC_ACUSE"   +
					"    AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"   AND (LCD.IC_IF = IEP.IC_IF OR cbi.IC_IF = IEP.IC_IF) "   +//////////////
					"    AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES"   +
					"    AND IEP.IC_EPO = D.IC_EPO"   +
					"    AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"    AND PN.IC_PRODUCTO_NAFIN = 4"  +
				"   AND D.IC_EPO = "  +ic_epo+
				condicion.toString());
				qryCCC.append(" AND D.ic_estatus_docto in(4,11,32)  ") ;
				if(!ic_if.equals("")){
					qryCCC.append(" AND (LCD.IC_IF = "+ic_if+" OR cbi.IC_IF = "+ic_if+")");
				}
				if(!numOrden.equals("")){
					qryCCC.append(" and ddptc.ic_orden_pago_tc ="+numOrden);
				}
			
				qryFdR.append(
					"   SELECT /*+ INDEX(D CP_DIS_DOCUMENTO_PK)"   +
					"    INDEX (LCD CP_com_LINEA_CREDITO_PK)*/"   +
					"     d.ic_documento"   +
					"     FROM DIS_DOCUMENTO D"   +
					"    ,DIS_DOCTO_SELECCIONADO DS"   +
					"    ,DIS_SOLICITUD S"   +
					"    ,COM_ACUSE3 C3"   +
					"    ,COMREL_PRODUCTO_EPO PE"   +
					"    ,COMCAT_PRODUCTO_NAFIN PN"   +
					"    ,COM_LINEA_CREDITO LCD"   +
					"    ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"    ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"    WHERE D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"    AND D.IC_EPO = PE.IC_EPO"   +
					"    AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
					"    AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO "   +
					"    AND S.CC_ACUSE = C3.CC_ACUSE "   + 
					"    AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"    AND LCD.IC_IF = IEP.IC_IF"   +
					"    AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES"   +
					"    AND IEP.IC_EPO = D.IC_EPO"   +
					"    AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"    AND PN.IC_PRODUCTO_NAFIN = 4"  +				
					"    and d.IC_TIPO_FINANCIAMIENTO is null "+
					" AND LCD.cg_tipo_solicitud = 'I' "+
					"  AND LCD.cs_factoraje_con_rec = 'S' "+
					"   AND D.IC_EPO = "  +ic_epo+
				condicion.toString());
				qryFdR.append(" AND D.ic_estatus_docto in(4,11)  ") ; 
				if(!ic_if.equals("")){	 
					qryFdR.append(" AND LCD.IC_IF =  " + ic_if) ; 
				}
			

			if("D".equals(tipo_credito))
				qrySentencia.append(qryDM.toString());
			else if("C".equals(tipo_credito))
				qrySentencia.append(qryCCC.toString()); 
			else if("F".equals(tipo_credito))
				qrySentencia.append(qryFdR.toString());
			else
				qrySentencia.append(qryDM.toString()+ " UNION ALL "+qryCCC.toString());

			log.debug("EL query de la llave primaria: "+ tipo_credito +"---"+qrySentencia.toString());
		}catch(Exception e){
			log.debug("ConsInfDocEpoDist::getDocumentQueryException "+e);
		}
		return qrySentencia.toString();
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
	
		StringBuffer qrySentencia	= new StringBuffer();
    	StringBuffer qryDM			= new StringBuffer();
    	StringBuffer qryCCC			= new StringBuffer();
    	StringBuffer condicion		= new StringBuffer();
		StringBuffer qryFdR			= new StringBuffer();
   	
		
		
		if(!ic_pyme.equals(""))	 
			condicion.append(" AND D.IC_PYME =  " +	ic_pyme) ;
		if (!fchinicial.equals("") && !fchfinal.equals(""))
			condicion.append(" AND trunc(C3.df_fecha_hora) BETWEEN TO_DATE('"+fchinicial+"','DD/MM/YYYY') and TO_DATE('"+fchfinal+"','DD/MM/YYYY') ");		
		if(ic_if.equals("") && ic_pyme.equals("") && fchinicial.equals("") && fchfinal.equals("")){	
			condicion.append(" AND TO_CHAR(C3.df_fecha_hora,'DD/MM/YYYY')=TO_CHAR(SYSDATE,'DD/MM/YYYY') ");
		}
		
		condicion.append(" AND (");
		for (int i = 0; i < pageIds.size(); i++) { 
			List lItem = (ArrayList)pageIds.get(i);
			if(i > 0){condicion.append("  OR  ");}
			condicion.append(" d.ic_documento = "+new Long(lItem.get(0).toString()));			
		}
		condicion.append(" ) ");
			
		
		qryDM.append(
      "   SELECT /*+ index (d CP_DIS_DOCUMENTO_PK) "   +
      "   index (ds CP_DIS_DOCTO_SELECCIONADO_PK)"   +
      "   index (lcd CP_DIS_LINEA_CREDITO_DM_PK) "   +
      "   index (pn CP_COMCAT_PRODUCTO_NAFIN_PK)"   +
      "   index (pe CP_COMREL_PRODUCTO_EPO_PK)"   +
      "   index (m CP_COMCAT_MONEDA_PK)"   +
      "   index (tc CP_COM_TIPO_CAMBIO_PK)"   +
      "   index (e CP_COMCAT_EPO_PK)"   +
      "   index (ci CP_COMCAT_IF_PK ) "   +
      "   index (ed CP_COMCAT_ESTATUS_DOCTO_PK ) "   +
      "   index (tf CP_COMCAT_TIPO_FINANCIAMIENTO_PK ) */  "   +
      "   P.cg_razon_social as DISTRIBUIDOR"   +
      "  ,D.ig_numero_docto"   +
      "  ,D.cc_acuse"   +
      "  ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') as df_fecha_emision"   +
      "  ,TO_CHAR(D.df_carga,'dd/mm/yyyy') as df_fecha_publicacion"   +
      "  ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc"   +
      "  ,D.IG_PLAZO_DOCTO"   +
      "  ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as df_fecha_seleccion"   +
      "  ,M.cd_nombre as MONEDA"   +
      "  ,M.ic_moneda"   +
      "  ,D.fn_monto"   +
      "  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
      "  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
      "  ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) as MONTO_VALUADO"   +
      "  ,D.IG_PLAZO_DESCUENTO"   +
      "  ,D.FN_MONTO"   +
      "  ,D.fn_porc_descuento"   +
      "  ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
      "  ,ED.CD_DESCRIPCION AS ESTATUS"   +
		"  ,ED.IC_ESTATUS_DOCTO AS IC_ESTATUS"   +
		"	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) AS REFERENCIA_INT" +
		"  ,DS.fn_valor_tasa  AS VALOR_TASA"   +
      "  ,D.IG_PLAZO_CREDITO"   +
      "  ,DS.fn_importe_recibir as MONTO_CREDITO"   +
      "  ,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO"   +
      "  ,TCI.ic_tipo_cobro_interes"   +
      "        ,TCI.cd_descripcion as tipo_cobro_interes"   +
      "        ,DS.fn_importe_interes as MONTO_INTERES"   +
      "        ,D.ic_documento"   +
      "        ,E.cg_razon_social as EPO"   +
      "  , D.ic_documento"   +
      "  ,CI.cg_razon_social as DIF"   +
      "  ,TF.cd_descripcion as MODO_PLAZO"   +
      "  ,to_char(C3.df_fecha_hora,'dd/mm/yyyy') as df_fecha_hora"   +
      "  ,'Modalidad 1 (Riesgo Empresa de Primer Orden)' as tipo_credito"   +
      "  ,LCD.ic_moneda as moneda_linea"   +
		"  ,DS.FN_PORC_COMISION_APLI as COMISION_APLICABLE"   +
		"  ,'' as MONTO_DEPOSITAR"   +
		"  ,'' as MONTO_COMISION"   +
		"  ,TF.IC_TIPO_FINANCIAMIENTO as IC_TIPO_FINANCIAMIENTO"   +
		"  ,'' as IC_ORDEN_ENVIADO"   +
		"  ,'' as NUMERO_TC"   +
		"  ,'' as IC_OPERACION_CCACUSE"   +
		"  ,'' AS codigo_autorizado"   +
		"  ,'' as FECHA_REGISTRO"   +
		"   ,d.CG_VENTACARTERA as CG_VENTACARTERA, D.IG_TIPO_PAGO "+
      "   FROM DIS_DOCUMENTO D"   +
      "  ,DIS_DOCTO_SELECCIONADO DS"   +
      "  ,DIS_SOLICITUD S"   +
      "  ,COM_ACUSE3 C3"   +
      "  ,COMCAT_PYME P"   +
      "  ,DIS_LINEA_CREDITO_DM LCD"   +
      "  ,COMCAT_PRODUCTO_NAFIN PN"   +
      "  ,COMREL_PRODUCTO_EPO PE"   +
      "  ,COMCAT_TASA CT"   +
      "  ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
      "  ,COMCAT_MONEDA M"   +
      "  ,COM_TIPO_CAMBIO TC"   +
      "  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
      "  ,COMCAT_EPO E"   +
     "  ,COMCAT_IF CI "   +
      "  ,COMCAT_ESTATUS_DOCTO ED"   +
      "  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
      "  WHERE "   +
      "  D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
      "  AND D.IC_EPO = PE.IC_EPO"   +
      "  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
      "  AND PN.IC_PRODUCTO_NAFIN = 4"   +
      "  AND P.IC_PYME = D.IC_PYME"   +
      "  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
      "  AND D.IC_EPO = PE.IC_EPO"   +
      "  AND D.IC_MONEDA = M.IC_MONEDA"   +
      "  AND M.IC_MONEDA = TC.IC_MONEDA"   +
      "  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
      "  AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM"   +
      "  AND LCD.IC_IF = IEP.IC_IF"   +
      "  AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES"   +
      "  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
      "  AND IEP.IC_EPO = D.IC_EPO"   +
      "  AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
      "  AND DS.IC_TASA = CT.IC_TASA"   +
      "  AND D.IC_EPO = E.IC_EPO"   +
      "  AND S.CC_ACUSE = C3.CC_ACUSE"   +
      "  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
      "  AND LCD.IC_IF = CI.IC_IF"  +
			condicion.toString()  );
			qryDM.append(" AND D.ic_estatus_docto in(4,11,32)  ") ; 
		if(!ic_if.equals("")){	 
					qryDM.append(" AND LCD.IC_IF =  " + ic_if) ; 
		}
				
		qryCCC.append(
			"  SELECT  /*+ index (d CP_DIS_DOCUMENTO_PK) "   +
			"   index (ds CP_DIS_DOCTO_SELECCIONADO_PK)"   +
			"   index (lcd CP_COM_LINEA_CREDITO_PK) "   +
			"   index (pn CP_COMCAT_PRODUCTO_NAFIN_PK)"   +
			"   index (pe CP_COMREL_PRODUCTO_EPO_PK)"   +
			"   index (m CP_COMCAT_MONEDA_PK)"   +
			"   index (tc CP_COM_TIPO_CAMBIO_PK)"   +
			"   index (e CP_COMCAT_EPO_PK)"   +
			"   index (ci CP_COMCAT_IF_PK ) "   +
			"   index (ed CP_COMCAT_ESTATUS_DOCTO_PK ) "   +
			"   index (tf CP_COMCAT_TIPO_FINANCIAMIENTO_PK ) */  "   +
			"    P.cg_razon_social as DISTRIBUIDOR"   +
			"   ,D.ig_numero_docto"   +
			"   ,D.cc_acuse"   +
			"   ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') as df_fecha_emision"   +
			"   ,TO_CHAR(D.df_carga,'dd/mm/yyyy') as df_fecha_publicacion"   +
			"   ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc"   +
			"   ,D.IG_PLAZO_DOCTO"   +
			"   ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as df_fecha_seleccion"   +
			"   ,M.cd_nombre as MONEDA"   +
			"   ,M.ic_moneda"   +
			"   ,D.fn_monto"   +
			"   ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
			"   ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
			"   ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) as MONTO_VALUADO"   +
			"   ,D.IG_PLAZO_DESCUENTO"   +
			"   ,D.FN_MONTO"   +
			"   ,D.fn_porc_descuento"   +
			"   ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
			"   ,ED.CD_DESCRIPCION AS ESTATUS"   +
			"  ,ED.IC_ESTATUS_DOCTO AS IC_ESTATUS"   +
			"   ,CT.CD_NOMBRE||' '||ds.cg_rel_mat||' '||ds.fn_puntos AS REFERENCIA_INT"   +
			"   ,DECODE(DS.CG_REL_MAT"   +
			"   ,'+',DS.fn_valor_tasa + DS.fn_puntos"   +
			"   ,'-',DS.fn_valor_tasa - DS.fn_puntos"   +
			"   ,'*',DS.fn_valor_tasa * DS.fn_puntos"   +
			"   ,'/',DS.fn_valor_tasa / DS.fn_puntos"   +
			"   ,0) AS VALOR_TASA"   +
			"   ,D.IG_PLAZO_CREDITO"   +
			"   ,DS.fn_importe_recibir as MONTO_CREDITO"   +
			"   ,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO"   +
			"   ,TCI.ic_tipo_cobro_interes"   +
			"   ,TCI.cd_descripcion as tipo_cobro_interes"   +
			"   ,DS.fn_importe_interes as MONTO_INTERES"   +
			"   ,D.ic_documento"   +
			"   ,E.cg_razon_social as EPO"   +
			"   , D.ic_documento"   +
			"  ,decode(ed.ic_estatus_docto,32,ci2.cg_razon_social,ci.cg_razon_social) AS dif"   +
			"   ,TF.cd_descripcion as MODO_PLAZO"   +
			"   ,to_char(C3.df_fecha_hora,'dd/mm/yyyy') as df_fecha_hora"   +
			"   ,'Modalidad 2 (Riesgo Distribuidor)' as tipo_credito"   +
			"   ,LCD.ic_moneda as moneda_linea"   +
			"  ,DS.FN_PORC_COMISION_APLI as COMISION_APLICABLE"   +
			"  ,'' as MONTO_DEPOSITAR"   +
			"  ,'' as MONTO_COMISION"   +
			"  ,TF.IC_TIPO_FINANCIAMIENTO as IC_TIPO_FINANCIAMIENTO"   +
			"  ,DDPTC.IC_ORDEN_PAGO_TC as IC_ORDEN_ENVIADO"   +
			"  ,DDPTC.CG_NUM_TARJETA as NUMERO_TC"   +
			"  ,DDPTC.CG_TRANSACCION_ID as IC_OPERACION_CCACUSE"   +
			"  ,decode(CONCAT(ddptc.CG_CODIGO_RESP ,ddptc.CG_CODIGO_DESC),'','',ddptc.CG_CODIGO_RESP||'-' ||ddptc.CG_CODIGO_DESC)AS codigo_autorizado"   +
			"  ,TO_CHAR(DDPTC.DF_FECHA_AUTORIZACION,'dd/mm/yyyy') as FECHA_REGISTRO"   +
			"   ,d.CG_VENTACARTERA as CG_VENTACARTERA, D.IG_TIPO_PAGO "+
			"    FROM DIS_DOCUMENTO D"   +
			"  ,DIS_DOCTOS_PAGO_TC DDPTC"   +////////////77
			"   ,DIS_DOCTO_SELECCIONADO DS"   +
			"   ,DIS_SOLICITUD S"   +
			"   ,COM_ACUSE3 C3"   +
			"   ,COMCAT_PYME P"   +
			"   ,COM_LINEA_CREDITO LCD"   +
			"   ,COMCAT_PRODUCTO_NAFIN PN"   +
			"   ,COMREL_PRODUCTO_EPO PE"   +
			"   ,COMCAT_TASA CT"   +
			"   ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
			"   ,COMCAT_MONEDA M"   +
			"   ,COM_TIPO_CAMBIO TC"   +
			"   ,COMCAT_TIPO_COBRO_INTERES TCI"   +
			"   ,COMCAT_EPO E"   +
			"  ,COMCAT_IF CI "   +
			"  ,COM_BINS_IF cbi "+///////////7agegados 
			"	,comcat_if ci2 "+/////////////////agregados 
			"   ,COMCAT_ESTATUS_DOCTO ED"   +
			"   ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
			"   WHERE D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
			"  AND D.IC_ORDEN_PAGO_TC = DDPTC.IC_ORDEN_PAGO_TC(+)"   +//////////
			"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
			"   AND D.IC_EPO = PE.IC_EPO"   +
			"   AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"   AND PN.IC_PRODUCTO_NAFIN = 4"   +
			"   AND P.IC_PYME = D.IC_PYME"   +
			"   AND D.IC_EPO = PE.IC_EPO"   +
			"   AND D.IC_MONEDA = M.IC_MONEDA"   +
			"   AND M.IC_MONEDA = TC.IC_MONEDA"   +
			"   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
			"   AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
			"   AND (LCD.IC_IF = IEP.IC_IF OR cbi.IC_IF = IEP.IC_IF) "   +//////////////
			"   AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES"   +
			"   AND TF.IC_TIPO_FINANCIAMIENTO = D.IC_TIPO_FINANCIAMIENTO"   +
			"   AND IEP.IC_EPO = D.IC_EPO"   +
			"   AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"   AND DS.IC_TASA = CT.IC_TASA"   +
			"   AND D.IC_EPO = E.IC_EPO"   +
			"   AND S.CC_ACUSE = C3.CC_ACUSE"   +
			"   AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
			"  AND d.IC_BINS = cbi.IC_BINS(+)"  +////////////77
			"  AND cbi.IC_if = ci2.IC_if(+)"  +/////////////////7
			"   AND LCD.IC_IF = CI.IC_IF"  +
  		condicion.toString());
		qryCCC.append(" AND D.ic_estatus_docto in(4,11,32)  ") ; 

		if(!ic_if.equals("")){
				qryCCC.append(" AND (LCD.IC_IF = "+ic_if+" OR cbi.IC_IF = "+ic_if+")");
				
			}
			if(!numOrden.equals("")){
				qryCCC.append(" and ddptc.ic_orden_pago_tc ="+numOrden);
			}
		
		qryFdR.append(
			"  SELECT  /*+ index (d CP_DIS_DOCUMENTO_PK) "   +
			"   index (ds CP_DIS_DOCTO_SELECCIONADO_PK)"   +
			"   index (lcd CP_COM_LINEA_CREDITO_PK) "   +
			"   index (pn CP_COMCAT_PRODUCTO_NAFIN_PK)"   +
			"   index (pe CP_COMREL_PRODUCTO_EPO_PK)"   +
			"   index (m CP_COMCAT_MONEDA_PK)"   +
			"   index (tc CP_COM_TIPO_CAMBIO_PK)"   +
			"   index (e CP_COMCAT_EPO_PK)"   +
			"   index (ci CP_COMCAT_IF_PK ) "   +
			"   index (ed CP_COMCAT_ESTATUS_DOCTO_PK ) "   +
			"   index (tf CP_COMCAT_TIPO_FINANCIAMIENTO_PK ) */  "   +
			"    P.cg_razon_social as DISTRIBUIDOR"   +
			"   ,D.ig_numero_docto"   +
			"   ,D.cc_acuse"   +
			"   ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') as df_fecha_emision"   +
			"   ,TO_CHAR(D.df_carga,'dd/mm/yyyy') as df_fecha_publicacion"   +
			"   ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc"   +
			"   ,D.IG_PLAZO_DOCTO"   +
			"   ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as df_fecha_seleccion"   +
			"   ,M.cd_nombre as MONEDA"   +
			"   ,M.ic_moneda"   +
			"   ,D.fn_monto"   +
			"   ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
			"   ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
			"   ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) as MONTO_VALUADO"   +
			"   ,D.IG_PLAZO_DESCUENTO"   +
			"   ,D.FN_MONTO"   +
			"   ,D.fn_porc_descuento"   +
			"   ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
			"   ,ED.CD_DESCRIPCION AS ESTATUS"   +
			"  ,ED.IC_ESTATUS_DOCTO AS IC_ESTATUS"   +
			"   ,CT.CD_NOMBRE||' '||ds.cg_rel_mat||' '||ds.fn_puntos AS REFERENCIA_INT"   +
			"   ,DECODE(DS.CG_REL_MAT"   +
			"   ,'+',DS.fn_valor_tasa + DS.fn_puntos"   +
			"   ,'-',DS.fn_valor_tasa - DS.fn_puntos"   +
			"   ,'*',DS.fn_valor_tasa * DS.fn_puntos"   +
			"   ,'/',DS.fn_valor_tasa / DS.fn_puntos"   +
			"   ,0) AS VALOR_TASA"   +
			"   ,D.IG_PLAZO_CREDITO"   +
			"   ,DS.fn_importe_recibir as MONTO_CREDITO"   +
			"   ,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO"   +
			"   ,TCI.ic_tipo_cobro_interes"   +
			"   ,TCI.cd_descripcion as tipo_cobro_interes"   +
			"   ,DS.fn_importe_interes as MONTO_INTERES"   +
			"   ,D.ic_documento"   +
			"   ,E.cg_razon_social as EPO"   +
			"   , D.ic_documento"   +
			"  ,CI.cg_razon_social as DIF"   +
			"   ,'NA' as MODO_PLAZO"   +
			"   ,to_char(C3.df_fecha_hora,'dd/mm/yyyy') as df_fecha_hora"   +
			"   ,'Factoraje con Recurso' as tipo_credito"   +
			"   ,LCD.ic_moneda as moneda_linea"   +
			"   ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
			"  ,DS.FN_PORC_COMISION_APLI as COMISION_APLICABLE"   +
			"  ,'' as MONTO_DEPOSITAR"   +
			"  ,'' as MONTO_COMISION"   +
			"  ,TF.IC_TIPO_FINANCIAMIENTO as IC_TIPO_FINANCIAMIENTO"   +
			"  ,'' as IC_ORDEN_ENVIADO"   +
			"  ,'' as NUMERO_TC"   +
			"  ,'' as IC_OPERACION_CCACUSE"   +
			"  ,'' AS codigo_autorizado"   +
			"  ,'' as FECHA_REGISTRO"   +
			" ,lcd.ig_cuenta_bancaria as CG_NUMERO_CUENTA, D.IG_TIPO_PAGO "+
			
			"    FROM DIS_DOCUMENTO D"   +
			"   ,DIS_DOCTO_SELECCIONADO DS"   +
			"   ,DIS_SOLICITUD S"   +
			"   ,COM_ACUSE3 C3"   +
			"   ,COMCAT_PYME P"   +
			"   ,COM_LINEA_CREDITO LCD"   +
			"   ,COMCAT_PRODUCTO_NAFIN PN"   +
			"   ,COMREL_PRODUCTO_EPO PE"   +
			"   ,COMCAT_TASA CT"   +
			"   ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
			"   ,COMCAT_MONEDA M"   +
			"   ,COM_TIPO_CAMBIO TC"   +
			"   ,COMCAT_TIPO_COBRO_INTERES TCI"   +
			"   ,COMCAT_EPO E"   +
			"  ,COMCAT_IF CI "   +
			"   ,COMCAT_ESTATUS_DOCTO ED"   +
			"   ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
			"   WHERE D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
			"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO "   +
			"   AND D.IC_EPO = PE.IC_EPO"   +
			"   AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"   AND PN.IC_PRODUCTO_NAFIN = 4"   +
			"   AND P.IC_PYME = D.IC_PYME"   +
			"   AND D.IC_EPO = PE.IC_EPO"   +
			"   AND D.IC_MONEDA = M.IC_MONEDA"   +
			"   AND M.IC_MONEDA = TC.IC_MONEDA"   +
			"   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
			"   AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
			"   AND LCD.IC_IF = IEP.IC_IF"   +
			"   AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES"   +
			"   AND TF.IC_TIPO_FINANCIAMIENTO(+) = D.IC_TIPO_FINANCIAMIENTO"   +
			"   AND IEP.IC_EPO = D.IC_EPO"   +
			"   AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"   AND DS.IC_TASA = CT.IC_TASA"   +
			"   AND D.IC_EPO = E.IC_EPO"   +
			"   AND S.CC_ACUSE = C3.CC_ACUSE "   +
			"   AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
			"   AND LCD.IC_IF = CI.IC_IF"  +
			"   and d.IC_TIPO_FINANCIAMIENTO is null "+	
			" AND LCD.cg_tipo_solicitud = 'I' "+
			"  AND LCD.cs_factoraje_con_rec = 'S' "+
			condicion.toString());
			qryFdR.append(" AND D.ic_estatus_docto in(4,11)  ") ;
			if(!ic_if.equals("")){	 
					qryFdR.append(" AND LCD.IC_IF =  " + ic_if) ; 
			}

			if("D".equals(tipo_credito))
				qrySentencia.append(qryDM.toString());
			else if("C".equals(tipo_credito))
				qrySentencia.append(qryCCC.toString());
			else if("F".equals(tipo_credito))
				qrySentencia.append(qryFdR.toString());
			else
				qrySentencia.append(qryDM.toString()+ " UNION ALL "+qryCCC.toString());

		log.debug("el query queda de la siguiente manera "+qrySentencia.toString());
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();
	}

	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer qryDM			  = new StringBuffer();
		StringBuffer qryCCC		    = new StringBuffer();
		StringBuffer condicion    = new StringBuffer();
		StringBuffer qryFdR			= new StringBuffer();
				
		try {

		  
			   
			if(!ic_pyme.equals(""))	 
				condicion.append(" AND D.IC_PYME =  " +	ic_pyme) ;
			if (!fchinicial.equals("") && !fchfinal.equals(""))
				condicion.append(" AND trunc(C3.df_fecha_hora) BETWEEN TO_DATE('"+fchinicial+"','DD/MM/YYYY') and TO_DATE('"+fchfinal+"','DD/MM/YYYY') ");		
			if(ic_if.equals("") && ic_pyme.equals("") && fchinicial.equals("") && fchfinal.equals(""))	 
				condicion.append(" AND TO_CHAR(C3.df_fecha_hora,'DD/MM/YYYY')=TO_CHAR(SYSDATE,'DD/MM/YYYY') ");
			
		
		qryDM.append(
      "   SELECT /*+ index (d CP_DIS_DOCUMENTO_PK) "   +    
      "   index (ds CP_DIS_DOCTO_SELECCIONADO_PK)"   +
      "   index (lcd CP_DIS_LINEA_CREDITO_DM_PK) "   +
      "   index (pn CP_COMCAT_PRODUCTO_NAFIN_PK)"   +
      "   index (pe CP_COMREL_PRODUCTO_EPO_PK)"   +
      "   index (m CP_COMCAT_MONEDA_PK)"   +
      "   index (tc CP_COM_TIPO_CAMBIO_PK)"   +
      "   index (e CP_COMCAT_EPO_PK)"   +
      "   index (ci CP_COMCAT_IF_PK ) "   +
      "   index (ed CP_COMCAT_ESTATUS_DOCTO_PK ) "   +
      "   index (tf CP_COMCAT_TIPO_FINANCIAMIENTO_PK ) */  "   +
      "   P.cg_razon_social as DISTRIBUIDOR"   +
      "  ,D.ig_numero_docto"   +
      "  ,D.cc_acuse"   +
      "  ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') as df_fecha_emision"   +
      "  ,TO_CHAR(D.df_carga,'dd/mm/yyyy') as df_fecha_publicacion"   +
      "  ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc"   +
      "  ,D.IG_PLAZO_DOCTO"   +
      "  ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as df_fecha_seleccion"   +
      "  ,M.cd_nombre as MONEDA"   +
      "  ,M.ic_moneda"   +
      "  ,D.fn_monto"   +
      "  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
      "  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
      "  ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) as MONTO_VALUADO"   +
      "  ,D.IG_PLAZO_DESCUENTO"   +
      "  ,D.FN_MONTO"   +
      "  ,D.fn_porc_descuento"   +
      "  ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
      "  ,ED.CD_DESCRIPCION AS ESTATUS"   +
		"  ,ED.IC_ESTATUS_DOCTO AS IC_ESTATUS"   +
		"	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) AS REFERENCIA_INT" +
		"  ,DS.fn_valor_tasa  AS VALOR_TASA"   +
      "  ,D.IG_PLAZO_CREDITO"   +
      "  ,DS.fn_importe_recibir as MONTO_CREDITO"   +
      "  ,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO"   +
      "  ,TCI.ic_tipo_cobro_interes"   +
      "        ,TCI.cd_descripcion as tipo_cobro_interes"   +
      "        ,DS.fn_importe_interes as MONTO_INTERES"   +
      "        ,D.ic_documento"   +
      "        ,E.cg_razon_social as EPO"   +
      "  , D.ic_documento"   +
      "  ,CI.cg_razon_social as DIF"   +
      "  ,TF.cd_descripcion as MODO_PLAZO"   +
      "  ,to_char(C3.df_fecha_hora,'dd/mm/yyyy') as df_fecha_hora"   +
      "  ,'Modalidad 1 (Riesgo Empresa de Primer Orden)' as tipo_credito"   +
      "  ,LCD.ic_moneda as moneda_linea"   +
		"  ,DS.FN_PORC_COMISION_APLI as COMISION_APLICABLE"   +
		"  ,'' as MONTO_DEPOSITAR"   +
		"  ,'' as MONTO_COMISION"   +
		"  ,TF.IC_TIPO_FINANCIAMIENTO as IC_TIPO_FINANCIAMIENTO"   +
		"  ,'' as IC_ORDEN_ENVIADO"   +
		"  ,'' as NUMERO_TC"   +
		"  ,'' as IC_OPERACION_CCACUSE"   +
		"  ,'' AS codigo_autorizado"   +
		"  ,'' as FECHA_REGISTRO"   +
		"   ,d.CG_VENTACARTERA as CG_VENTACARTERA, D.IG_TIPO_PAGO "+		
      "   FROM DIS_DOCUMENTO D"   +
      "  ,DIS_DOCTO_SELECCIONADO DS"   +
      "  ,DIS_SOLICITUD S"   +
      "  ,COM_ACUSE3 C3"   +
      "  ,COMCAT_PYME P"   +
      "  ,DIS_LINEA_CREDITO_DM LCD"   +
      "  ,COMCAT_PRODUCTO_NAFIN PN"   +
      "  ,COMREL_PRODUCTO_EPO PE"   +
      "  ,COMCAT_TASA CT"   +
      "  ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
      "  ,COMCAT_MONEDA M"   +
      "  ,COM_TIPO_CAMBIO TC"   +
      "  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
      "  ,COMCAT_EPO E"   +
      "  ,COMCAT_IF CI "   + 
      "  ,COMCAT_ESTATUS_DOCTO ED"   +
      "  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
      "  WHERE "   +
      "  D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
      "  AND D.IC_EPO = PE.IC_EPO"   +
      "  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
      "  AND PN.IC_PRODUCTO_NAFIN = 4"   +
      "  AND P.IC_PYME = D.IC_PYME"   +
      "  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
      "  AND D.IC_EPO = PE.IC_EPO"   +
      "  AND D.IC_MONEDA = M.IC_MONEDA"   +
      "  AND M.IC_MONEDA = TC.IC_MONEDA"   +
      "  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
      "  AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM"   +
      "  AND LCD.IC_IF = IEP.IC_IF"   +
      "  AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES"   +
      "  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
      "  AND IEP.IC_EPO = D.IC_EPO"   +
      "  AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
      "  AND DS.IC_TASA = CT.IC_TASA"   +
      "  AND D.IC_EPO = E.IC_EPO"   +
      "  AND S.CC_ACUSE = C3.CC_ACUSE"   +
      "  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
      "  AND LCD.IC_IF = CI.IC_IF"  +
			"  AND D.ic_epo = "+ic_epo+
			condicion.toString()  );
		qryDM.append(" AND D.ic_estatus_docto in(4,11,32)  ") ; 

		if(!ic_if.equals("")){	 
					qryDM.append(" AND LCD.IC_IF =  " + ic_if) ; 
		}
				
		qryCCC.append(
			"  SELECT  /*+ index (d CP_DIS_DOCUMENTO_PK) "   +
			"   index (ds CP_DIS_DOCTO_SELECCIONADO_PK)"   +
			"   index (lcd CP_COM_LINEA_CREDITO_PK) "   +
			"   index (pn CP_COMCAT_PRODUCTO_NAFIN_PK)"   +
			"   index (pe CP_COMREL_PRODUCTO_EPO_PK)"   +
			"   index (m CP_COMCAT_MONEDA_PK)"   +
			"   index (tc CP_COM_TIPO_CAMBIO_PK)"   +
			"   index (e CP_COMCAT_EPO_PK)"   +
			"   index (ci CP_COMCAT_IF_PK ) "   +
			"   index (ed CP_COMCAT_ESTATUS_DOCTO_PK ) "   +
			"   index (tf CP_COMCAT_TIPO_FINANCIAMIENTO_PK ) */  "   +
			"    P.cg_razon_social as DISTRIBUIDOR"   +
			"   ,D.ig_numero_docto"   +
			"   ,D.cc_acuse"   +
			"   ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') as df_fecha_emision"   +
			"   ,TO_CHAR(D.df_carga,'dd/mm/yyyy') as df_fecha_publicacion"   +
			"   ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc"   +
			"   ,D.IG_PLAZO_DOCTO"   +
			"   ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as df_fecha_seleccion"   +
			"   ,M.cd_nombre as MONEDA"   +
			"   ,M.ic_moneda"   +
			"   ,D.fn_monto"   +
			"   ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
			"   ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
			"   ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) as MONTO_VALUADO"   +
			"   ,D.IG_PLAZO_DESCUENTO"   +
			"   ,D.FN_MONTO"   +
			"   ,D.fn_porc_descuento"   +
			"   ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
			"   ,ED.CD_DESCRIPCION AS ESTATUS"   +
			"  ,ED.IC_ESTATUS_DOCTO AS IC_ESTATUS"   +
			"   ,CT.CD_NOMBRE||' '||ds.cg_rel_mat||' '||ds.fn_puntos AS REFERENCIA_INT"   +
			"   ,DECODE(DS.CG_REL_MAT"   +
			"   ,'+',DS.fn_valor_tasa + DS.fn_puntos"   +
			"   ,'-',DS.fn_valor_tasa - DS.fn_puntos"   +
			"   ,'*',DS.fn_valor_tasa * DS.fn_puntos"   +
			"   ,'/',DS.fn_valor_tasa / DS.fn_puntos"   +
			"   ,0) AS VALOR_TASA"   +
			"   ,D.IG_PLAZO_CREDITO"   +
			"   ,DS.fn_importe_recibir as MONTO_CREDITO"   +
			"   ,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO"   +
			"   ,TCI.ic_tipo_cobro_interes"   +
			"   ,TCI.cd_descripcion as tipo_cobro_interes"   +
			"   ,DS.fn_importe_interes as MONTO_INTERES"   +
			"   ,D.ic_documento"   +
			"   ,E.cg_razon_social as EPO"   +
			"   , D.ic_documento"   +
			"  ,decode(ed.ic_estatus_docto,32,ci2.cg_razon_social,ci.cg_razon_social) AS dif"   +
			"   ,TF.cd_descripcion as MODO_PLAZO"   +
			"   ,to_char(C3.df_fecha_hora,'dd/mm/yyyy') as df_fecha_hora"   +
			"   ,'Modalidad 2 (Riesgo Distribuidor)' as tipo_credito"   +
			"   ,LCD.ic_moneda as moneda_linea"   +
			"  ,DS.FN_PORC_COMISION_APLI as COMISION_APLICABLE"   +
			"  ,'' as MONTO_DEPOSITAR"   +
			"  ,'' as MONTO_COMISION"   +
			"  ,TF.IC_TIPO_FINANCIAMIENTO as IC_TIPO_FINANCIAMIENTO"   +
			"  ,DDPTC.IC_ORDEN_PAGO_TC as IC_ORDEN_ENVIADO"   +
			"  ,DDPTC.CG_NUM_TARJETA as NUMERO_TC"   +
			"  ,DDPTC.CG_TRANSACCION_ID as IC_OPERACION_CCACUSE"   +
			"  ,decode(CONCAT(ddptc.CG_CODIGO_RESP ,ddptc.CG_CODIGO_DESC),'','',ddptc.CG_CODIGO_RESP||'-' ||ddptc.CG_CODIGO_DESC)AS codigo_autorizado"   +
			"  ,TO_CHAR(DDPTC.DF_FECHA_AUTORIZACION,'dd/mm/yyyy') as FECHA_REGISTRO"   +
		  "   ,d.CG_VENTACARTERA as CG_VENTACARTERA, D.IG_TIPO_PAGO "+
			"    FROM DIS_DOCUMENTO D"   +
			"  ,DIS_DOCTOS_PAGO_TC DDPTC"   +/////////////77
			"   ,DIS_DOCTO_SELECCIONADO DS"   +
			"   ,DIS_SOLICITUD S"   +
			"   ,COM_ACUSE3 C3"   +
			"   ,COMCAT_PYME P"   +
			"   ,COM_LINEA_CREDITO LCD"   +
			"   ,COMCAT_PRODUCTO_NAFIN PN"   +
			"   ,COMREL_PRODUCTO_EPO PE"   +
			"   ,COMCAT_TASA CT"   +
			"   ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
			"   ,COMCAT_MONEDA M"   +
			"   ,COM_TIPO_CAMBIO TC"   +
			"   ,COMCAT_TIPO_COBRO_INTERES TCI"   +
			"   ,COMCAT_EPO E"   +
			"  ,COMCAT_IF CI "   +
			"  ,COM_BINS_IF cbi "+///////////7agegados 
			"	,comcat_if ci2 "+/////////////////agregados
			"   ,COMCAT_ESTATUS_DOCTO ED"   +
			"   ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
			"   WHERE D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
			"  AND D.IC_ORDEN_PAGO_TC = DDPTC.IC_ORDEN_PAGO_TC(+)"   +
			"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
			"   AND D.IC_EPO = PE.IC_EPO"   +
			"   AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"   AND PN.IC_PRODUCTO_NAFIN = 4"   +
			"   AND P.IC_PYME = D.IC_PYME"   +
			"   AND D.IC_EPO = PE.IC_EPO"   +
			"   AND D.IC_MONEDA = M.IC_MONEDA"   +
			"   AND M.IC_MONEDA = TC.IC_MONEDA"   +
			"   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
			"   AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
			"   AND (LCD.IC_IF = IEP.IC_IF OR cbi.IC_IF = IEP.IC_IF) "   +//////////////
			"   AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES"   +
			"   AND TF.IC_TIPO_FINANCIAMIENTO = D.IC_TIPO_FINANCIAMIENTO"   +
			"   AND IEP.IC_EPO = D.IC_EPO"   +
			"   AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"   AND DS.IC_TASA = CT.IC_TASA"   +
			"   AND D.IC_EPO = E.IC_EPO"   +
			"   AND S.CC_ACUSE = C3.CC_ACUSE"   +
			"   AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
			"  AND d.IC_BINS = cbi.IC_BINS(+)"  +////////////77
			"  AND cbi.IC_if = ci2.IC_if(+)"  +/////////////////7
			"   AND LCD.IC_IF = CI.IC_IF"  +
			"  AND D.ic_epo = "+ic_epo+
	  		condicion.toString());
			qryCCC.append(" AND D.ic_estatus_docto in(4,11,32)  ") ; 

			if(!ic_if.equals("")){
				qryCCC.append(" AND (LCD.IC_IF = "+ic_if+" OR cbi.IC_IF = "+ic_if+")");
				
			}
			if(!numOrden.equals("")){
				qryCCC.append(" and ddptc.ic_orden_pago_tc ="+numOrden);
			}



		qryFdR.append(
					"  SELECT  /*+ index (d CP_DIS_DOCUMENTO_PK) "   +
					"   index (ds CP_DIS_DOCTO_SELECCIONADO_PK)"   +
					"   index (lcd CP_COM_LINEA_CREDITO_PK) "   +
					"   index (pn CP_COMCAT_PRODUCTO_NAFIN_PK)"   +
					"   index (pe CP_COMREL_PRODUCTO_EPO_PK)"   +
					"   index (m CP_COMCAT_MONEDA_PK)"   +
					"   index (tc CP_COM_TIPO_CAMBIO_PK)"   +
					"   index (e CP_COMCAT_EPO_PK)"   +
					"   index (ci CP_COMCAT_IF_PK ) "   +
					"   index (ed CP_COMCAT_ESTATUS_DOCTO_PK ) "   +
					"   index (tf CP_COMCAT_TIPO_FINANCIAMIENTO_PK ) */  "   +
					"    P.cg_razon_social as DISTRIBUIDOR"   +
					"   ,D.ig_numero_docto"   +
					"   ,D.cc_acuse"   +
					"   ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') as df_fecha_emision"   +
					"   ,TO_CHAR(D.df_carga,'dd/mm/yyyy') as df_fecha_publicacion"   +
					"   ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc"   +
					"   ,D.IG_PLAZO_DOCTO"   +
					"   ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as df_fecha_seleccion"   +
					"   ,M.cd_nombre as MONEDA"   +
					"   ,M.ic_moneda"   +
					"   ,D.fn_monto"   +
					"   ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
					"   ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
					"   ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) as MONTO_VALUADO"   +
					"   ,D.IG_PLAZO_DESCUENTO"   +
					"   ,D.FN_MONTO"   +
					"   ,D.fn_porc_descuento"   +
					"   ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
					"   ,ED.CD_DESCRIPCION AS ESTATUS"   +
					"  ,ED.IC_ESTATUS_DOCTO AS IC_ESTATUS"   +
					"   ,CT.CD_NOMBRE||' '||ds.cg_rel_mat||' '||ds.fn_puntos AS REFERENCIA_INT"   +
					"   ,DECODE(DS.CG_REL_MAT"   +
					"   ,'+',DS.fn_valor_tasa + DS.fn_puntos"   +
					"   ,'-',DS.fn_valor_tasa - DS.fn_puntos"   +
					"   ,'*',DS.fn_valor_tasa * DS.fn_puntos"   +
					"   ,'/',DS.fn_valor_tasa / DS.fn_puntos"   +
					"   ,0) AS VALOR_TASA"   +
					"   ,D.IG_PLAZO_CREDITO"   +
					"   ,DS.fn_importe_recibir as MONTO_CREDITO"   +
					"   ,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO"   +
					"   ,TCI.ic_tipo_cobro_interes"   +
					"   ,TCI.cd_descripcion as tipo_cobro_interes"   +
					"   ,DS.fn_importe_interes as MONTO_INTERES"   +
					"   ,D.ic_documento"   +
					"   ,E.cg_razon_social as EPO"   +
					"   , D.ic_documento"   +
					"  ,CI.cg_razon_social as DIF"   +
					"   ,'NA' as MODO_PLAZO"   +
					"   ,to_char(C3.df_fecha_hora,'dd/mm/yyyy') as df_fecha_hora"   +
					"   ,'Factoraje con Recurso' as tipo_credito"   +
					"   ,LCD.ic_moneda as moneda_linea"   +
				  "   ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				  "  ,DS.FN_PORC_COMISION_APLI as COMISION_APLICABLE"   +
					"  ,'' as MONTO_DEPOSITAR"   +
					"  ,'' as MONTO_COMISION"   +
					"  ,TF.IC_TIPO_FINANCIAMIENTO as IC_TIPO_FINANCIAMIENTO"   +
					"  ,'' as IC_ORDEN_ENVIADO"   +
					"  ,'' as NUMERO_TC"   +
					"  ,'' as IC_OPERACION_CCACUSE"   +
					"  ,'' AS codigo_autorizado"   +
					"  ,'' as FECHA_REGISTRO"   +
				  " ,lcd.ig_cuenta_bancaria as CG_NUMERO_CUENTA, D.IG_TIPO_PAGO "+				  
					"    FROM DIS_DOCUMENTO D"   +
					"   ,DIS_DOCTO_SELECCIONADO DS"   +
					"   ,DIS_SOLICITUD S"   +
					"   ,COM_ACUSE3 C3"   +
					"   ,COMCAT_PYME P"   +
					"   ,COM_LINEA_CREDITO LCD"   +
					"   ,COMCAT_PRODUCTO_NAFIN PN"   +
					"   ,COMREL_PRODUCTO_EPO PE"   +
					"   ,COMCAT_TASA CT"   +
					"   ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"   ,COMCAT_MONEDA M"   +
					"   ,COM_TIPO_CAMBIO TC"   +
					"   ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"   ,COMCAT_EPO E"   +
					"  ,COMCAT_IF CI "   +
					"   ,COMCAT_ESTATUS_DOCTO ED"   +
					"   ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"   WHERE D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO "   +
					"   AND D.IC_EPO = PE.IC_EPO"   +
					"   AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"   AND PN.IC_PRODUCTO_NAFIN = 4"   +
					"   AND P.IC_PYME = D.IC_PYME"   +
					"   AND D.IC_EPO = PE.IC_EPO"   +
					"   AND D.IC_MONEDA = M.IC_MONEDA"   +
					"   AND M.IC_MONEDA = TC.IC_MONEDA"   +
					"   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"   AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
					"   AND LCD.IC_IF = IEP.IC_IF"   +
					"   AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES"   +
					"   AND TF.IC_TIPO_FINANCIAMIENTO(+) = D.IC_TIPO_FINANCIAMIENTO"   +
					"   AND IEP.IC_EPO = D.IC_EPO"   +
					"   AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"   AND DS.IC_TASA = CT.IC_TASA"   +
					"   AND D.IC_EPO = E.IC_EPO"   +
					"   AND S.CC_ACUSE = C3.CC_ACUSE "   +
					"   AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
					"   AND LCD.IC_IF = CI.IC_IF"  +
					" and d.IC_TIPO_FINANCIAMIENTO is null "+
					" AND LCD.cg_tipo_solicitud = 'I' "+
					"  AND LCD.cs_factoraje_con_rec = 'S' "+
					"  AND D.ic_epo = "+ic_epo+
					condicion.toString());
			qryFdR.append(" AND D.ic_estatus_docto in(4,11)  ") ; 
			if(!ic_if.equals("")){	 
					qryFdR.append(" AND LCD.IC_IF =  " + ic_if) ; 
			}
			
			if("D".equals(tipo_credito))
				qrySentencia.append(qryDM.toString());
			else if("C".equals(tipo_credito))
				qrySentencia.append(qryCCC.toString());
			else if("F".equals(tipo_credito))
				qrySentencia.append(qryFdR.toString());
			else
				qrySentencia.append(qryDM.toString()+ " UNION ALL "+qryCCC.toString());
		
		log.debug("query "+tipo_credito+"--"+qrySentencia.toString());

		}catch(Exception e){
			log.debug("::getDocumentQueryFileException "+e);
		}
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();
	}
		
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		//CreaArchivo creaArchivo = new CreaArchivo();
		//StringBuffer contenidoArchivo = new StringBuffer();
		ComunesPDF pdfDoc = new ComunesPDF();
		String distribuidor		= "", numdocto	= "", acuse	= "",  fchemision	= "", fchpublica = "", fchvence = "", plazodocto= "", 
		moneda= "", ic_moneda		= "", tipoconversion	= "", tipocambio	= "", plzodescto	= "", porcdescto	= "",ic_estatus="",  estatus	= "",
		numcredito	= "", descif = "", referencia	= "", plazocred	= "", fchvenccred	= "", tipocobroint = "",  fchopera	= "",
		monedaLinea	= "", modoPlazo = "",  bandeVentaCartera ="", valortaza	= "", tipoCreditos ="", cuenta_bancaria ="", 
		monto = "", num_orden="", mntodescuento = "", montoint	= "", mntocredito = "";
		String comAplicable= "",  montoComision= "", ordenEnvio = "", opAcuse = "", codAutorizado = "", fechRegistro="" ;	
		int numCols  =0, numColsB	=0;
		double mntovaluado	= 0, auxMonComision = 0, montoDepositar = 0;
		
		try {
			if(tipo.equals("PDF") ) {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(), 
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),  
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				
				numCols = 17;	
				numColsB = 11;
				if(!"".equals(cgTipoConversion)){
					numCols  +=3;
				} 
				if(tipo_credito.equals("F")) {
					numCols++;
				}
				pdfDoc.setLTable(numCols,100);
				pdfDoc.setLCell("Datos del Documento Inicial","celda01rep",ComunesPDF.CENTER,numCols);	
				pdfDoc.setLCell("A","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("IF","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Distribuidor","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("No. Docto Inicial","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("No. Acuse Carga","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Emisi�n","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Pub.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Venc.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Oper.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo docto.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto","celda01rep",ComunesPDF.CENTER);//12
				if(!"".equals(cgTipoConversion)) {
					pdfDoc.setLCell("Tipo conv.","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setLCell("Tipo cambio","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto valuado","celda01rep",ComunesPDF.CENTER);
				}							
				pdfDoc.setLCell("Plazo Dscto D�as","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("% Dscto","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto % Dscto","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Modo Plazo","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Estatus","celda01rep",ComunesPDF.CENTER);
				if(tipo_credito.equals("F")) {
					pdfDoc.setLCell("Cuenta Bancaria Pyme","celda01rep",ComunesPDF.CENTER);
				}	
				pdfDoc.setLCell("Datos Documento Final","celda01rep",ComunesPDF.CENTER,numCols);
				pdfDoc.setLCell("B","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo Cred.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("No. Docto Final","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto","celda01rep",ComunesPDF.CENTER);
				
				pdfDoc.setLCell("% Comisi�n Aplicable de Terceros","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Comisi�n de Terceros (IVA Incluido)","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto a Depositar por Operaci�n","celda01rep",ComunesPDF.CENTER);
				
				if(!tipo_credito.equals("F")) {
					pdfDoc.setLCell("Plazo","celda01rep",ComunesPDF.CENTER);
					pdfDoc.setLCell("Fecha Venc.","celda01rep",ComunesPDF.CENTER);
					numColsB  +=2;
				}
				if(!responsable.equals("D")) {	
					if(!tipo_credito.equals("F")) {
						pdfDoc.setLCell("Ref. Tasa Inter�s","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setLCell("Valor Tasa de Inter�s","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto Inter�s","celda01rep",ComunesPDF.CENTER);
						numColsB  +=3;
					}
				}
				pdfDoc.setLCell("Id Orden enviado","celda01rep",ComunesPDF.CENTER);	
				pdfDoc.setLCell("Respuesta de Operaci�n","celda01rep",ComunesPDF.CENTER);	
				pdfDoc.setLCell("C�digo Autorizaci�n","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("N�mero Tarjeta de Cr�dito","celda01rep",ComunesPDF.CENTER);
				
				if(numCols==17){
					for(int i =numColsB; i < numCols; i++ ){
						pdfDoc.setLCell("","celda01rep",ComunesPDF.CENTER);
					}
				}else if(numCols==20){
					for(int i =numColsB; i < numCols; i++ ){
						pdfDoc.setLCell("","celda01rep",ComunesPDF.CENTER);
					}
				}else if(numCols==21){
					for(int i =numColsB; i < numCols; i++ ){
						pdfDoc.setLCell("","celda01rep",ComunesPDF.CENTER);
					}
				}
			
				
					
			}
		   pdfDoc.setLHeaders();
			String iva = "1."+this.obtenIVA();
			while(rs.next()){
			
				ic_moneda = (rs.getString("ic_moneda")==null)?"":rs.getString("ic_moneda");	
				distribuidor = (rs.getString("DISTRIBUIDOR")==null)?"":rs.getString("DISTRIBUIDOR");
				numdocto = (rs.getString("ig_numero_docto")==null)?"":rs.getString("ig_numero_docto");
				acuse = (rs.getString("cc_acuse")==null)?"":rs.getString("cc_acuse");
				fchemision = (rs.getString("df_fecha_emision")==null)?"":rs.getString("df_fecha_emision");
				fchpublica = (rs.getString("df_fecha_publicacion")==null)?"":rs.getString("df_fecha_publicacion");		
				fchvence = (rs.getString("df_fecha_venc")==null)?"":rs.getString("df_fecha_venc");		
				plazodocto = (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");				
				moneda = (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");				
				monto = (rs.getString("FN_MONTO")==null)?"0":rs.getString("FN_MONTO");			
				tipoconversion = (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");								
				tipocambio = (rs.getString("TIPO_CAMBIO")==null)?"":rs.getString("TIPO_CAMBIO");										
				plzodescto = (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");														
				porcdescto = (rs.getString("fn_porc_descuento")==null)?"":rs.getString("fn_porc_descuento");
				mntodescuento = (rs.getString("MONTO_DESCUENTO")==null)?"":rs.getString("MONTO_DESCUENTO");					
				mntovaluado = ( Double.parseDouble(monto) -Double.parseDouble(mntodescuento) )*Double.parseDouble(tipocambio);
				estatus = (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");///descomentar
				ic_estatus = (rs.getString("IC_ESTATUS")==null)?"":rs.getString("IC_ESTATUS");//descomentar
				modoPlazo = (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
				fchopera = (rs.getString("df_fecha_hora")==null)?"":rs.getString("df_fecha_hora");		
				numcredito =(rs.getString("ic_documento")==null)?"":rs.getString("ic_documento");
				descif = (rs.getString("DIF")==null)?"":rs.getString("DIF");
				monedaLinea = (rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA");
				numcredito =(rs.getString("ic_documento")==null)?"":rs.getString("ic_documento");
				referencia = (rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT");
				tipoCreditos = (rs.getString("TIPO_CREDITO")==null)?"":rs.getString("TIPO_CREDITO");
				plazocred = (rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO");
				fchvenccred = (rs.getString("DF_FECHA_VENC_CREDITO")==null)?"":rs.getString("DF_FECHA_VENC_CREDITO");
				tipocobroint = (rs.getString("tipo_cobro_interes")==null)?"":rs.getString("tipo_cobro_interes");				
				montoint = (rs.getString("MONTO_INTERES")==null)?"":rs.getString("MONTO_INTERES");	
				mntocredito = (rs.getString("MONTO_CREDITO")==null)?"":rs.getString("MONTO_CREDITO");				
				valortaza = (rs.getString("VALOR_TASA")==null)?"":rs.getString("VALOR_TASA");	
				comAplicable = (rs.getString("COMISION_APLICABLE")==null)?"0":rs.getString("COMISION_APLICABLE");	
			//	montoDepositar = (rs.getString("MONTO_DEPOSITAR")==null)?"":rs.getString("MONTO_DEPOSITAR");	
				montoComision = (rs.getString("MONTO_COMISION")==null)?"":rs.getString("MONTO_COMISION");	  
				ordenEnvio = (rs.getString("IC_ORDEN_ENVIADO")==null)?"":rs.getString("IC_ORDEN_ENVIADO");
				num_orden = (rs.getString("NUMERO_TC").equals(""))?"":"XXXX-XXXX-XXXX-"+rs.getString("NUMERO_TC");
				
				opAcuse = (rs.getString("IC_OPERACION_CCACUSE")==null)?"":rs.getString("IC_OPERACION_CCACUSE");
				codAutorizado = (rs.getString("CODIGO_AUTORIZADO")==null)?"":rs.getString("CODIGO_AUTORIZADO");
				fechRegistro = (rs.getString("FECHA_REGISTRO")==null)?"":rs.getString("FECHA_REGISTRO");
				auxMonComision = Double.parseDouble(mntocredito)*(((Double.parseDouble(comAplicable))/100)*Double.parseDouble(iva));
				//String tipoFinanciamiento = rs.getString("IC_TIPO_FINANCIAMIENTO")==null?"":rs.getString("IC_TIPO_FINANCIAMIENTO");
				montoDepositar = Double.parseDouble(mntocredito)-auxMonComision;
					
				bandeVentaCartera			= (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
				if(tipo_credito.equals("F")) {
					cuenta_bancaria			= (rs.getString("CG_NUMERO_CUENTA")==null)?"":rs.getString("CG_NUMERO_CUENTA");
				}
				if (bandeVentaCartera.equals("S") ) {
					modoPlazo ="";
				}		
				if(tipo.equals("PDF") ) {
				
					pdfDoc.setLCell("A","formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell(descif,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell(distribuidor ,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell(numdocto,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell(acuse,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell(fchemision,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell(fchpublica,"formasrep",ComunesPDF.CENTER);	
					pdfDoc.setLCell(fchvence,"formasrep",ComunesPDF.CENTER);	
					pdfDoc.setLCell(fchopera,"formasrep",ComunesPDF.CENTER);	
					pdfDoc.setLCell(plazodocto,"formasrep",ComunesPDF.CENTER);	
					pdfDoc.setLCell(moneda,"formasrep",ComunesPDF.CENTER);	   
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(monto,2),"formasrep",ComunesPDF.CENTER);					
					
					if(!"".equals(tipoconversion)) {
						pdfDoc.setLCell(tipoconversion,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setLCell(tipocambio,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(mntovaluado,2),"formasrep",ComunesPDF.CENTER);
					}
						
					pdfDoc.setLCell(plzodescto,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell(porcdescto,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(mntodescuento,2),"formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell(modoPlazo,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell(estatus,"formasrep",ComunesPDF.CENTER);
					if(tipo_credito.equals("F")) {
						pdfDoc.setLCell(cuenta_bancaria,"formasrep",ComunesPDF.CENTER);					
					}
								
					pdfDoc.setLCell("B","formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipoCreditos,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell(numcredito,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(mntocredito,2),"formasrep",ComunesPDF.CENTER);
					if(!ic_estatus.equals("32")){
						pdfDoc.setLCell("N/A","formasrep",ComunesPDF.CENTER);
						pdfDoc.setLCell("N/A","formasrep",ComunesPDF.CENTER);
						pdfDoc.setLCell("N/A","formasrep",ComunesPDF.CENTER);
					}else{
						pdfDoc.setLCell(Comunes.formatoDecimal(comAplicable,2)+"%","formasrep",ComunesPDF.CENTER);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(auxMonComision,2),"formasrep",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoDepositar,2),"formasrep",ComunesPDF.RIGHT);
					}
					if(!tipo_credito.equals("F")) {
						if(ic_estatus.equals("32")){
							pdfDoc.setLCell("N/A","formasrep",ComunesPDF.CENTER);
							pdfDoc.setLCell("N/A","formasrep",ComunesPDF.CENTER);
						}else{
							pdfDoc.setLCell(plazocred,"formasrep",ComunesPDF.CENTER);
							pdfDoc.setLCell(fchvence,"formasrep",ComunesPDF.CENTER);
						}
					}
					if(!responsable.equals("D")) {
						if(!tipo_credito.equals("F")) {
							if(ic_estatus.equals("32")){
								pdfDoc.setLCell("N/A","formasrep",ComunesPDF.CENTER);
								pdfDoc.setLCell("N/A","formasrep",ComunesPDF.CENTER);
								pdfDoc.setLCell("N/A","formasrep",ComunesPDF.CENTER);
						   }else{
								pdfDoc.setLCell(referencia,"formasrep",ComunesPDF.CENTER);
								pdfDoc.setLCell(valortaza,"formasrep",ComunesPDF.CENTER);
								pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoint,2),"formasrep",ComunesPDF.CENTER);
								
							}
						}
					}
					if(!ic_estatus.equals("32")){
						pdfDoc.setLCell("N/A","formasrep",ComunesPDF.CENTER);
						pdfDoc.setLCell("N/A","formasrep",ComunesPDF.CENTER);
						pdfDoc.setLCell("N/A","formasrep",ComunesPDF.CENTER);
						pdfDoc.setLCell("N/A","formasrep",ComunesPDF.CENTER);
						
					}else{
						pdfDoc.setLCell(ordenEnvio,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setLCell(opAcuse,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setLCell(codAutorizado,"formasrep",ComunesPDF.CENTER);//num_orden
						pdfDoc.setLCell(num_orden,"formasrep",ComunesPDF.CENTER);
						
					}
					if(numCols==17){
						for(int i =numColsB; i < numCols; i++ ){
							pdfDoc.setLCell("","formasrep",ComunesPDF.CENTER);
						}
					}else if(numCols==20){
						for(int i =numColsB; i < numCols; i++ ){
							pdfDoc.setLCell("","formasrep",ComunesPDF.CENTER);
						}
					}else if(numCols==21){
						for(int i =numColsB; i < numCols; i++ ){
							pdfDoc.setLCell("","formasrep",ComunesPDF.CENTER);
						}
					}
				
				} 
			}
			
		
		
			if(tipo.equals("PDF") ) {
			   pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
				pdfDoc.addLTable();
				pdfDoc.setLTable(7,35);
				pdfDoc.setLCell("La informaci�n del porcentaje de '% Comisi�n aplicable de Terceros' (Adquirencia), 'Monto Comisi�n de Terceros (IVA Incluido)' y 'Monto a Depositar por operaci�n', es meramente informativa, y depende del servicio de Adquirencia que tenga contratado con terceros, favor de consultar su Estado de Cuenta referenciada. ","celda01rep",ComunesPDF.JUSTIFIED,7);
				pdfDoc.addLTable();
			
				pdfDoc.endDocument();	
			}
		   return nombreArchivo;
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		}
	}
	
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.info("crearCustomFile (E)");
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		ComunesPDF pdfDoc = new ComunesPDF();
		String distribuidor		= "", numdocto	= "", acuse	= "",  fchemision	= "", fchpublica = "", fchvence = "", plazodocto= "", 
		moneda= "", ic_moneda		= "", tipoconversion	= "", tipocambio	= "", plzodescto	= "", porcdescto	= "", ic_estatus="",estatus	= "",
		numcredito	= "", descif = "", referencia	= "", plazocred	= "", fchvenccred	= "", tipocobroint = "",  fchopera	= "",
		monedaLinea	= "",num_orden = "", modoPlazo = "",  bandeVentaCartera ="", valortaza	= "", tipoCreditos ="", cuenta_bancaria ="";
		String comAplicable= "",  montoComision= "", ordenEnvio = "", opAcuse = "", codAutorizado = "", fechRegistro="" ;
	   String tipoPago = "";
		double monto = 0, mntovaluado	= 0,montoDepositar = 0, mntodescuento = 0, montoint	= 0, mntocredito = 0,auxMonComision = 0;	
		int registros =0,  numCols  =0, numColsB	=0;
		String auxNA= "N/A";
		try {
		
			if(tipo.equals("PDF") ) {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(), 
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),  
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			        
			   
                            
                 String leyenda = "";
                    if(operaContrato!=null&&operaContrato.equals("S")){
                           leyenda = "Otorgo mi anuencia sobre los documentos publicados en esta p�gina y para los efectos legales conducentes de conformidad con lo establecido en el Contrato de Financiamiento a Clientes y Distribuidores.";
                    }else{
                        leyenda = "De conformidad a lo dispuesto en los art�culos 32 C del C�digo Fiscal o 2038 y 2041 del C�digo Civil Federal seg�n corresponda, "+
                                "me doy por notificado de la cesi�n de derechos de las operaciones descritas en esta p�gina y para los efectos legales conducentes.\n"+ 
                                "Por otra parte, manifiesto que, de los documentos y/o facturas publicadas y notificadas a partir del 17 de octubre, s� he emitido o emitir� al CLIENTE o DISTRIBUIDOR "+
                                "el CFDI por la operaci�n comercial que le dio origen a esta transacci�n, seg�n sea el caso y conforme establezcan las disposiciones fiscales vigentes.\n";                                 
                    }
                                
                            
			    pdfDoc.addText(leyenda,"formas",ComunesPDF.LEFT);
			    
                                
				numCols = 17;
				numColsB = 11;
				if(!"".equals(cgTipoConversion)){
					numCols  +=3;
				} 
				if(tipo_credito.equals("F")) {
					numCols++;
				}
				pdfDoc.setLTable(numCols,100);
				pdfDoc.setLCell("Datos del Documento Inicial","celda01rep",ComunesPDF.CENTER,numCols);	
				pdfDoc.setLCell("A","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("IF","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Distribuidor","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("No. Docto Inicial","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("No. Acuse Carga","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Emisi�n","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Pub.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Venc.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Oper.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo docto.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto","celda01rep",ComunesPDF.CENTER);
				if(!"".equals(cgTipoConversion)) {
				pdfDoc.setLCell("Tipo conv.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo cambio","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto valuado","celda01rep",ComunesPDF.CENTER);
				}							
				pdfDoc.setLCell("Plazo Dscto D�as","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("% Dscto","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto % Dscto","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Modo Plazo","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Estatus","celda01rep",ComunesPDF.CENTER);
				if(tipo_credito.equals("F")) {
					pdfDoc.setLCell("Cuenta Bancaria Pyme","celda01rep",ComunesPDF.CENTER);
				}	
				pdfDoc.setLCell("Datos Documento Final","celda01rep",ComunesPDF.CENTER,numCols);
				pdfDoc.setLCell("B","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo Cred.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("No. Docto Final","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto","celda01rep",ComunesPDF.CENTER);
				
				pdfDoc.setLCell("% Comisi�n Aplicable de Terceros","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Comisi�n de Terceros (IVA Incluido)","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto a Depositar por Operaci�n","celda01rep",ComunesPDF.CENTER);
				if(!tipo_credito.equals("F")) {
					pdfDoc.setLCell("Plazo","celda01rep",ComunesPDF.CENTER);
					if(publicaDoctosFinanciables.equals("S")){
						pdfDoc.setLCell("Tipo de Pago","celda01rep",ComunesPDF.CENTER);
						numColsB++;
					}
					pdfDoc.setLCell("Fecha Venc.","celda01rep",ComunesPDF.CENTER);
					numColsB  +=2;
				}
				if(!responsable.equals("D")) {	
					if(!tipo_credito.equals("F")) {
						pdfDoc.setLCell("Ref. Tasa Inter�s","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setLCell("Valor Tasa de Inter�s","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto Inter�s","celda01rep",ComunesPDF.CENTER);
						numColsB  +=3;
					}
				}
				pdfDoc.setLCell("Id Orden enviado","celda01rep",ComunesPDF.CENTER);	
				pdfDoc.setLCell("Respuesta de Operaci�n","celda01rep",ComunesPDF.CENTER);	
				pdfDoc.setLCell("C�digo Autorizaci�n","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell("N�mero Tarjeta de Cr�dito","celda01rep",ComunesPDF.CENTER);
				
				if(numCols==17){
					for(int i =numColsB; i < numCols; i++ ){
						pdfDoc.setLCell("","celda01rep",ComunesPDF.CENTER);
					}
				}else if(numCols==20){
					for(int i =numColsB; i < numCols; i++ ){
						pdfDoc.setLCell("","celda01rep",ComunesPDF.CENTER);
					}
				}else if(numCols==21){
					for(int i =numColsB; i < numCols; i++ ){
						pdfDoc.setLCell("","celda01rep",ComunesPDF.CENTER);
					}
				}
				pdfDoc.setLHeaders();
			} else if(tipo.equals("CSV") ) {
				numCols = 17;	
				numColsB = 11;
				contenidoArchivo.append(" IF, Distribuidor, No. Docto Inicial, No. Acuse Carga, Fecha Emisi�n, Fecha Pub., Fecha Venc., Fecha Oper., Plazo docto.,Moneda, Monto ");
				if(!"".equals(cgTipoConversion)) {
					numCols  +=3;
					contenidoArchivo.append(",  Tipo conv., Tipo cambio, Monto valuado ");
				}
				contenidoArchivo.append(", Plazo Dscto D�as, % Dscto, Monto % Dscto, Modo Plazo, Estatus ");
				if(tipo_credito.equals("F")) {
					contenidoArchivo.append(", Cuenta Bancaria Pyme ");
					numCols  ++;
				}				
				contenidoArchivo.append(",Tipo Cr�dito., No. Docto Final, Monto");
				contenidoArchivo.append(",% Comisi�n Aplicable de Terceros, Monto Comisi�n de Terceros (IVA Incluido), Monto a Depositar por Operaci�n");
				if(!tipo_credito.equals("F")) {
					contenidoArchivo.append(", Plazo");
					if(publicaDoctosFinanciables.equals("S")){
						contenidoArchivo.append(", Tipo de pago ");
					}
					contenidoArchivo.append(", Fecha Venc. ");
				}				
				if(!responsable.equals("D") ) {
					if(!tipo_credito.equals("F") ) {					
					contenidoArchivo.append(",Ref. Tasa Inter�s, Valor Tasa de Inter�s, Monto Inter�s");
					}		
				}
				contenidoArchivo.append(",Id Orden enviado, Respuesta de Operaci�n, C�digo Autorizaci�n,N�mero Tarjeta de Cr�dito");
				contenidoArchivo.append(" "+"\n");
			}
			String iva = "1."+this.obtenIVA();
			while(rs.next()){
			
				ic_moneda = (rs.getString("ic_moneda")==null)?"":rs.getString("ic_moneda");	
				distribuidor = (rs.getString("DISTRIBUIDOR")==null)?"":rs.getString("DISTRIBUIDOR");
				numdocto = (rs.getString("ig_numero_docto")==null)?"":rs.getString("ig_numero_docto");
				acuse = (rs.getString("cc_acuse")==null)?"":rs.getString("cc_acuse");
				fchemision = (rs.getString("df_fecha_emision")==null)?"":rs.getString("df_fecha_emision");
				fchpublica = (rs.getString("df_fecha_publicacion")==null)?"":rs.getString("df_fecha_publicacion");		
				fchvence = (rs.getString("df_fecha_venc")==null)?"":rs.getString("df_fecha_venc");		
				plazodocto = (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");				
				moneda = (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");				
				monto = rs.getDouble("FN_MONTO");	
				tipoconversion = (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");								
				tipocambio = (rs.getString("TIPO_CAMBIO")==null)?"":rs.getString("TIPO_CAMBIO");										
				plzodescto = (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");														
				porcdescto = (rs.getString("fn_porc_descuento")==null)?"":rs.getString("fn_porc_descuento");														
				mntodescuento = rs.getDouble("MONTO_DESCUENTO");
				mntovaluado = (monto-mntodescuento)*Double.parseDouble(tipocambio);
				estatus = (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");	
				ic_estatus = (rs.getString("IC_ESTATUS")==null)?"":rs.getString("IC_ESTATUS");
				modoPlazo = (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
				fchopera = (rs.getString("df_fecha_hora")==null)?"":rs.getString("df_fecha_hora");		
				numcredito =(rs.getString("ic_documento")==null)?"":rs.getString("ic_documento");
				descif = (rs.getString("DIF")==null)?"":rs.getString("DIF");
				monedaLinea = (rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA");
				numcredito =(rs.getString("ic_documento")==null)?"":rs.getString("ic_documento");
				referencia = (rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT");
				tipoCreditos = (rs.getString("TIPO_CREDITO")==null)?"":rs.getString("TIPO_CREDITO");
				plazocred = (rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO");
				fchvenccred = (rs.getString("DF_FECHA_VENC_CREDITO")==null)?"":rs.getString("DF_FECHA_VENC_CREDITO");
				tipocobroint = (rs.getString("tipo_cobro_interes")==null)?"":rs.getString("tipo_cobro_interes");				
				montoint = rs.getDouble("MONTO_INTERES");
				valortaza = (rs.getString("VALOR_TASA")==null)?"":rs.getString("VALOR_TASA");
				mntocredito = rs.getDouble("MONTO_CREDITO");	
				     
				comAplicable = (rs.getString("COMISION_APLICABLE")==null)?"0":rs.getString("COMISION_APLICABLE");	
				//montoDepositar = (rs.getString("MONTO_DEPOSITAR")==null)?"":rs.getString("MONTO_DEPOSITAR");	
				montoComision = (rs.getString("MONTO_COMISION")==null)?"":rs.getString("MONTO_COMISION");	
				ordenEnvio = (rs.getString("IC_ORDEN_ENVIADO")==null)?"":rs.getString("IC_ORDEN_ENVIADO");
				num_orden = (rs.getString("NUMERO_TC")==null)?"":"XXXX-XXXX-XXXX-"+rs.getString("NUMERO_TC");
				opAcuse = (rs.getString("IC_OPERACION_CCACUSE")==null)?"":rs.getString("IC_OPERACION_CCACUSE");
				codAutorizado = (rs.getString("CODIGO_AUTORIZADO")==null)?"":rs.getString("CODIGO_AUTORIZADO");
				fechRegistro = (rs.getString("FECHA_REGISTRO")==null)?"":rs.getString("FECHA_REGISTRO");
				auxMonComision =mntocredito*(((Double.parseDouble(comAplicable))/100)*Double.parseDouble(iva));
				//String tipoFinanciamiento = rs.getString("IC_TIPO_FINANCIAMIENTO")==null?"":rs.getString("IC_TIPO_FINANCIAMIENTO");
				montoDepositar = mntocredito-auxMonComision;
				tipoPago = rs.getString("IG_TIPO_PAGO")==null?"":rs.getString("IG_TIPO_PAGO");
				if(tipoPago.equals("1")){
					tipoPago = "Financiamiento con intereses";
				} else if(tipoPago.equals("2")){
					tipoPago = "Meses sin intereses";
				}
				bandeVentaCartera			= (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
				if(tipo_credito.equals("F")) {
					cuenta_bancaria			= (rs.getString("CG_NUMERO_CUENTA")==null)?"":rs.getString("CG_NUMERO_CUENTA");
				}
				if (bandeVentaCartera.equals("S") ) {
					modoPlazo ="";
				}		
				if(tipo.equals("PDF") ) {
				
					pdfDoc.setLCell("A","formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell(descif,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell(distribuidor ,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell(numdocto,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell(acuse,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell(fchemision,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell(fchpublica,"formasrep",ComunesPDF.CENTER);	
					pdfDoc.setLCell(fchvence,"formasrep",ComunesPDF.CENTER);	
					pdfDoc.setLCell(fchopera,"formasrep",ComunesPDF.CENTER);	
					pdfDoc.setLCell(plazodocto,"formasrep",ComunesPDF.CENTER);	
					pdfDoc.setLCell(moneda,"formasrep",ComunesPDF.CENTER);	
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(monto,2),"formasrep",ComunesPDF.CENTER);					
					
					if(!"".equals(tipoconversion)) {
						pdfDoc.setLCell(tipoconversion,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setLCell(tipocambio,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(mntovaluado,2),"formasrep",ComunesPDF.CENTER);
					}
						
					pdfDoc.setLCell(plzodescto,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell(porcdescto,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(mntodescuento,2),"formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell(modoPlazo,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell(estatus,"formasrep",ComunesPDF.CENTER);
					if(tipo_credito.equals("F")) {
						pdfDoc.setLCell(cuenta_bancaria,"formasrep",ComunesPDF.CENTER);					
					}
					pdfDoc.setLCell("B","formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipoCreditos,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell(numcredito,"formasrep",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(mntocredito,2),"formasrep",ComunesPDF.CENTER);
					if(!ic_estatus.equals("32")){
						pdfDoc.setLCell("N/A","formasrep",ComunesPDF.CENTER);  
						pdfDoc.setLCell("N/A","formasrep",ComunesPDF.CENTER);
						pdfDoc.setLCell("N/A","formasrep",ComunesPDF.CENTER);
					}else{
						pdfDoc.setLCell(Comunes.formatoDecimal(comAplicable,2)+"%","formasrep",ComunesPDF.CENTER);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(auxMonComision,2),"formasrep",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoDepositar,2),"formasrep",ComunesPDF.RIGHT);
					}
					if(!tipo_credito.equals("F")) {
						if(ic_estatus.equals("32")){
							pdfDoc.setLCell("N/A","formasrep",ComunesPDF.CENTER);
							if(publicaDoctosFinanciables.equals("S")){
								pdfDoc.setLCell("N/A","formasrep",ComunesPDF.CENTER);
							}
							pdfDoc.setLCell("N/A","formasrep",ComunesPDF.CENTER);
						}else{
							pdfDoc.setLCell(plazocred,"formasrep",ComunesPDF.CENTER);
							if(publicaDoctosFinanciables.equals("S")){
								pdfDoc.setLCell(tipoPago,"formasrep",ComunesPDF.CENTER);
							}
							pdfDoc.setLCell(fchvence,"formasrep",ComunesPDF.CENTER);
						}
					}
					if(!responsable.equals("D")) {
						if(!tipo_credito.equals("F")) {
							if(ic_estatus.equals("32")){
								pdfDoc.setLCell("N/A","formasrep",ComunesPDF.CENTER);
								pdfDoc.setLCell("N/A","formasrep",ComunesPDF.CENTER);
								pdfDoc.setLCell("N/A","formasrep",ComunesPDF.CENTER);
						   }else{
								pdfDoc.setLCell(referencia,"formasrep",ComunesPDF.CENTER);
								pdfDoc.setLCell(valortaza,"formasrep",ComunesPDF.CENTER);
								pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoint,2),"formasrep",ComunesPDF.CENTER);
								
							}
						}
					}
					if(!ic_estatus.equals("32")){
						pdfDoc.setLCell("N/A","formasrep",ComunesPDF.CENTER);
						pdfDoc.setLCell("N/A","formasrep",ComunesPDF.CENTER);
						pdfDoc.setLCell("N/A","formasrep",ComunesPDF.CENTER);
						pdfDoc.setLCell("N/A","formasrep",ComunesPDF.CENTER);
						
					}else{
						pdfDoc.setLCell(ordenEnvio,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setLCell(opAcuse,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setLCell(codAutorizado,"formasrep",ComunesPDF.CENTER);
						pdfDoc.setLCell(num_orden,"formasrep",ComunesPDF.CENTER);
						
					}
					if(numCols==17){
						for(int i =numColsB; i < numCols; i++ ){
							pdfDoc.setLCell("","formasrep",ComunesPDF.CENTER);
						}
					}else if(numCols==20){
						for(int i =numColsB; i < numCols; i++ ){
							pdfDoc.setLCell("","formasrep",ComunesPDF.CENTER);
						}
					}else if(numCols==21){
						for(int i =numColsB; i < numCols; i++ ){
							pdfDoc.setLCell("","formasrep",ComunesPDF.CENTER);
						}
					}
				
				} else if(tipo.equals("CSV") ) {
				
					contenidoArchivo.append(descif.replace(',',' ')+", ");
					contenidoArchivo.append(distribuidor.replace(',',' ')+", ");
					contenidoArchivo.append(numdocto.replace(',',' ')+", ");
					contenidoArchivo.append(acuse.replace(',',' ')+", ");
					contenidoArchivo.append(fchemision.replace(',',' ')+", ");
					contenidoArchivo.append(fchpublica.replace(',',' ')+", ");
					contenidoArchivo.append(fchvence.replace(',',' ')+", ");
					contenidoArchivo.append(fchopera.replace(',',' ')+", ");
					contenidoArchivo.append(plazodocto.replace(',',' ')+", ");
					contenidoArchivo.append(moneda.replace(',',' ')+", ");			
					contenidoArchivo.append(Comunes.formatoDecimal(monto,2,false)+",");	
				
					if(!"".equals(cgTipoConversion)) {
						contenidoArchivo.append(tipoconversion.replace(',',' ')+", ");	
						contenidoArchivo.append(tipocambio.replace(',',' ')+", ");	
						contenidoArchivo.append(Comunes.formatoDecimal(mntovaluado,2,false)+",");
					}
			
					contenidoArchivo.append(plzodescto.replace(',',' ')+", ");	
					contenidoArchivo.append(porcdescto.replace(',',' ')+", ");
					contenidoArchivo.append(Comunes.formatoDecimal(mntodescuento,2,false)+",");
					contenidoArchivo.append(modoPlazo.replace(',',' ')+", ");	
					contenidoArchivo.append(estatus.replace(',',' ')+", ");
					if(tipo_credito.equals("F")) {
					contenidoArchivo.append(cuenta_bancaria.replace(',',' ')+", ");
					}
					
					contenidoArchivo.append(tipoCreditos.replace(',',' ')+", ");	
					contenidoArchivo.append(numcredito.replace(',',' ')+", ");
					contenidoArchivo.append(Comunes.formatoDecimal(mntocredito,2,false)+",");	
					if(!ic_estatus.equals("32")){
						
						contenidoArchivo.append(auxNA.replace(',',' ')+", ");
						contenidoArchivo.append(auxNA.replace(',',' ')+",");
						contenidoArchivo.append(auxNA.replace(',',' ')+",");
					}else{
						contenidoArchivo.append(comAplicable.replace(',',' ')+", ");
						contenidoArchivo.append(Comunes.formatoDecimal(auxMonComision,2,false)+",");
						contenidoArchivo.append(Comunes.formatoDecimal(montoDepositar,2,false)+",");	
					}
					if(!tipo_credito.equals("F")) {
						if(ic_estatus.equals("32")){
							contenidoArchivo.append(auxNA.replace(',',' ')+", ");
							if(publicaDoctosFinanciables.equals("S")){
								contenidoArchivo.append(auxNA.replace(',',' ')+",");
							}
							contenidoArchivo.append(auxNA.replace(',',' ')+",");
						}else{
							contenidoArchivo.append(plazocred.replace(',',' ')+", ");
							if(publicaDoctosFinanciables.equals("S")){
								contenidoArchivo.append(tipoPago.replace(',',' ')+",");
							}
							contenidoArchivo.append(fchvenccred.replace(',',' ')+", ");
						}
					}
			
					if(!responsable.equals("D")) {
						if(!tipo_credito.equals("F") ) {	
							if(ic_estatus.equals("32")){
								contenidoArchivo.append(auxNA.replace(',',' ')+", ");
								contenidoArchivo.append(auxNA.replace(',',' ')+",");
								contenidoArchivo.append(auxNA.replace(',',' ')+",");
							}else{
								contenidoArchivo.append(referencia.replace(',',' ')+", ");	
								contenidoArchivo.append(valortaza.replace(',',' ')+", ");
								contenidoArchivo.append(Comunes.formatoDecimal(montoint,2,false)+",");	
							}
						}
					}
					if(!ic_estatus.equals("32")){
						contenidoArchivo.append(auxNA.replace(',',' ')+", ");	
						contenidoArchivo.append(auxNA.replace(',',' ')+", ");
						contenidoArchivo.append(auxNA.replace(',',' ')+", ");	
						contenidoArchivo.append(auxNA.replace(',',' ')+", ");	
						
					}else{
						contenidoArchivo.append(ordenEnvio.replace(',',' ')+"', ");	
						contenidoArchivo.append(opAcuse.replace(',',' ')+", ");
						contenidoArchivo.append(codAutorizado.replace(',',' ')+", ");	 
						contenidoArchivo.append(num_orden.replace(',',' ')+", ");	 
						
					}
					contenidoArchivo.append("  "+"\n");		
				}
						
				registros++;			 
			
			} // while
			if(tipo.equals("PDF") ) {
				pdfDoc.addLTable();
				pdfDoc.addText("\n","formas",ComunesPDF.RIGHT);
				pdfDoc.setLTable(7,35);
				pdfDoc.setLHeaders();
				pdfDoc.setLCell("La informaci�n del porcentaje de '% Comisi�n aplicable de Terceros' (Adquirencia), 'Monto Comisi�n de Terceros (IVA Incluido)' y 'Monto a Depositar por operaci�n', es meramente informativa, y depende del servicio de Adquirencia que tenga contratado con terceros, favor de consultar su Estado de Cuenta referenciada. ","celda01rep",ComunesPDF.JUSTIFIED,7);
				pdfDoc.addLTable();
				pdfDoc.endDocument();
			}
			if(tipo.equals("CSV")){
				creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
				nombreArchivo = creaArchivo.getNombre();
			}
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		}
		log.debug("crearCustomFile (S)");
		return nombreArchivo;
	}
	
	/**
	 * Obtiene el ultimo porcentaje de IVA registrado
	 * @return Cadena con el porcentaje de iva
	 * @throws Exception
	 */
	public String obtenIVA() throws Exception {
		String iva="";
		AccesoDB 	con = new AccesoDB();
		Registros reg = null;
		try{ 
			con.conexionDB();
			String query =
					" SELECT fn_porcentaje_iva " +
					" FROM comcat_iva " +
					" WHERE df_aplicacion IN( " +
					"    SELECT MAX(df_aplicacion) " +
					"    FROM comcat_iva ) ";
				
				log.debug(" obtenIVA-> "+query);
				reg = con.consultarDB(query);
				if (reg.next()){
					iva = reg.getString("FN_PORCENTAJE_IVA");
				}
		}catch(Exception e){
			log.error("obtenIVA(Exception)",e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		return iva;
	}
	
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getIc_pyme() {
		return ic_pyme;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}

	public String getFchinicial() {
		return fchinicial;
	}

	public void setFchinicial(String fchinicial) {
		this.fchinicial = fchinicial;
	}

	public String getFchfinal() {
		return fchfinal;
	}

	public void setFchfinal(String fchfinal) {
		this.fchfinal = fchfinal;
	}

	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getTipo_credito() {
		return tipo_credito;
	}

	public void setTipo_credito(String tipo_credito) {
		this.tipo_credito = tipo_credito;  
		
	}

	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	public String getCgTipoConversion() {
		return cgTipoConversion;
	}

	public void setCgTipoConversion(String cgTipoConversion) {
		this.cgTipoConversion = cgTipoConversion;
	}
	
	public String getNumOrden() {
		return numOrden;
	}

	public void setNumOrden(String numOrden) {
		this.numOrden = numOrden;
	}

	public String getPublicaDoctosFinanciables() {
		return publicaDoctosFinanciables;
	}

	public void setPublicaDoctosFinanciables(String publicaDoctosFinanciables) {
		this.publicaDoctosFinanciables = publicaDoctosFinanciables;
	}


    public void setOperaContrato(String operaContrato) {
        this.operaContrato = operaContrato;
    }

    public String getOperaContrato() {
        return operaContrato;
    }
}
