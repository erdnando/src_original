package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsDetallePagInteres implements IQueryGeneratorRegExtJS  {
	public ConsDetallePagInteres() {}

	private List	conditions;
	StringBuffer strQuery;
	
	private static final Log log = ServiceLocator.getInstance().getLog(ConsDetallePagInteres.class);
	
	private String claveEPO;
	private String clavePyme;
	private String numeroDocto;
	private String tipoCobro;
	
	public String getAggregateCalculationQuery() {
		return "";
	}
	
	public String getDocumentQuery() {
		conditions	=	new ArrayList();
		strQuery	=	new StringBuffer();
		return strQuery.toString();
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
		strQuery = new StringBuffer();
		return strQuery.toString();
	}
	
	public String getDocumentQueryFile(){
		String rs_ic_linea_credito_dm	= "";
		String rs_ic_linea_credito		= "";
		String rs_ic_tipo_cobro_interes	= "";
		
		conditions	=	new ArrayList();
		strQuery		= new StringBuffer();
		Map map		= new HashMap();
		
		map			= obtenerLineaCredito();
		if (map!=null)
		{
			rs_ic_linea_credito_dm	= String.valueOf(map.get("LINEA_CREDITO_DM"));
			rs_ic_linea_credito		= String.valueOf(map.get("LINEA_CREDITO"));
			rs_ic_tipo_cobro_interes	= String.valueOf(map.get("TIPO_LINEA_CREDITO"));
		}
		else {
			rs_ic_linea_credito_dm	= "";
			rs_ic_linea_credito		= "";
			rs_ic_tipo_cobro_interes	= "";
		}
		
			strQuery.append("SELECT	doc.ic_documento," +
									" to_char(ac3.df_fecha_hora,'dd/mm/yyyy') as fecha_dpo,");
			if("1".equals(rs_ic_tipo_cobro_interes))
				strQuery.append(" to_char(ac3.df_fecha_hora,'dd/mm/yyyy') as fecha_pago,");
			else if("2".equals(rs_ic_tipo_cobro_interes))
				strQuery.append(" to_char(doc.df_fecha_venc_credito,'dd/mm/yyyy') as fecha_pago,");
			else if ("".equals(rs_ic_tipo_cobro_interes))
				strQuery.append(" '' as fecha_pago,");
		
			strQuery.append(	" doc.ig_plazo_credito," +
									" ct.cd_nombre || ' ' || sel.cg_rel_mat || ' ' || sel.fn_puntos AS tasa_referencia," +
									"DECODE (sel.cg_rel_mat," +
												"'+', fn_valor_tasa + fn_puntos," +
												"'-', fn_valor_tasa - fn_puntos," +
												"'*', fn_valor_tasa * fn_puntos," +
												"'/', fn_valor_tasa / fn_puntos)" +
										     " AS valor_tasa_int," +
										      "sel.fn_importe_interes AS monto_tasa_int"  +
								  " FROM dis_documento doc," +
											"dis_docto_seleccionado sel," +
										   "dis_solicitud sol," +
										 "com_acuse3 ac3," +
										  "comcat_tasa ct" +
								 " WHERE doc.ic_documento = sel.ic_documento" +
									" AND doc.ic_documento = sol.ic_documento" +
									" AND sol.cc_acuse = ac3.cc_acuse" +
									" AND ct.ic_tasa = sel.ic_tasa" +
									" AND doc.ig_numero_docto = ?");
		
			conditions.add(this.numeroDocto);
		
			if(!"".equals(rs_ic_linea_credito_dm) && rs_ic_linea_credito_dm != null) {
				strQuery.append(" AND doc.ic_linea_credito_dm = ?");
				conditions.add(rs_ic_linea_credito_dm);
			}
			else if(!"".equals(rs_ic_linea_credito) && rs_ic_linea_credito != null) {
				strQuery.append(" AND doc.ic_linea_credito = ?");
				conditions.add(rs_ic_linea_credito);
			}
		log.debug("getDocumentQueryFile "+strQuery.toString());
		log.debug("getDocumentQueryFile"+conditions);
		
		return strQuery.toString();
	}
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs,String path,String tipo) {
		log.info("crearCustomFile (E)");
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		double montoInteresTotal = 0;
		String montoInteres = "";
		String montoIntTotal = "";
		String fecha_operacion ="";
		String espacio = "";
		
		if("CSV".equals(tipo)) {
			linea = "N�mero pago, Fecha pago, Plazo, Tasa referenciada, Tasa inter�s, Monto de intereses a pagar\n";
			try{
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
				buffer = new BufferedWriter(writer);
				buffer.write(linea);
				
				while (rs.next()) {
							 fecha_operacion = (rs.getString("FECHA_DPO") == null) ? "" : rs.getString("FECHA_DPO");
					String ic_documento = (rs.getString("IC_DOCUMENTO") == null) ? "": rs.getString("IC_DOCUMENTO");
					String fecha_pago = (rs.getString("FECHA_PAGO") == null) ? "": rs.getString("FECHA_PAGO");
					String ig_plazo_credito = (rs.getString("IG_PLAZO_CREDITO") == null) ? "": rs.getString("IG_PLAZO_CREDITO");
					String tasa_referencia = (rs.getString("TASA_REFERENCIA") == null) ? "": rs.getString("TASA_REFERENCIA");
					String valor_tasa_int = (rs.getString("VALOR_TASA_INT") == null)? "": rs.getString("VALOR_TASA_INT");
					double monto_tasa_int = rs.getDouble("MONTO_TASA_INT");
					montoInteres = Double.toString(monto_tasa_int);
					linea =	ic_documento.replace(',',' ') + ", " +
								fecha_pago.replace(',',' ') + ", " +
								ig_plazo_credito.replace(',', ' ') + ", " +
								tasa_referencia.replace(',',' ') + ", " +
								valor_tasa_int.replace(',',' ') + ", " +
								montoInteres.replace(',',' ') + "\n";
					buffer.write(linea);
					montoInteresTotal += monto_tasa_int;
				}
				montoIntTotal = Double.toString(montoInteresTotal);
				linea =	"Total, " +
				espacio.replace(',',' ') + ", " +
				espacio.replace(',', ' ') + ", " +
				espacio.replace(',',' ') + ", " +
				espacio.replace(',',' ') + ", " +
				montoIntTotal.replace(',',' ') + "\n";
				buffer.write(linea);
				linea = "\nFecha de operacion :," + fecha_operacion;
				buffer.write(linea);
				buffer.close();
			} catch (Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		}else if ("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual = fechaActual.substring(0,2);
				String mesActual = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual = fechaActual.substring(6,10);
				String horaActual = new SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),
				(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual, "formas",ComunesPDF.RIGHT);
				pdfDoc.setLTable(7,100);
				pdfDoc.setLCell("Fecha de Operaci�n", "celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("N�mero Pago","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha pago","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tasa referenciada","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tasa de inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto de intereses a pagar","celda01",ComunesPDF.CENTER);
				pdfDoc.setLHeaders();
				while (rs.next()) {
							 fecha_operacion = (rs.getString("FECHA_DPO") == null) ? "" : rs.getString("FECHA_DPO");
					String ic_documento = (rs.getString("IC_DOCUMENTO") == null) ? "" : rs.getString("IC_DOCUMENTO");
					String fecha_pago = (rs.getString("FECHA_PAGO") == null) ? "" : rs.getString("FECHA_PAGO");
					String ig_plazo_credito = (rs.getString("IG_PLAZO_CREDITO") == null) ? "" : rs.getString("IG_PLAZO_CREDITO");
					String tasa_referencia = (rs.getString("TASA_REFERENCIA") == null ) ? "" : rs.getString("TASA_REFERENCIA");
					String valor_tasa_int = (rs.getString("VALOR_TASA_INT") == null) ? "" : rs.getString("VALOR_TASA_INT");
					double monto_tasa_int = rs.getDouble("MONTO_TASA_INT");
					
					pdfDoc.setLCell(fecha_operacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(ic_documento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_pago,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(ig_plazo_credito,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tasa_referencia,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(Comunes.formatoDecimal(valor_tasa_int,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(Comunes.formatoDecimal(monto_tasa_int,2),"formas",ComunesPDF.CENTER);
				   montoInteresTotal += monto_tasa_int;
				}
				pdfDoc.addLTable();
			
				pdfDoc.setLTable(7,100);
				pdfDoc.setLHeaders();
				pdfDoc.setLCell("TOTAL","celda01",ComunesPDF.CENTER);	
				pdfDoc.setLCell("","formas",ComunesPDF.CENTER,5);
				pdfDoc.setLCell("$" + Comunes.formatoDecimal(montoInteresTotal,2),"formasrep",ComunesPDF.CENTER);
				pdfDoc.addLTable();
				
				pdfDoc.endDocument();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		}
		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg,String path,String tipo) {
		return "";
	}
	
	public Map obtenerLineaCredito() {
		log.debug ("ClavePyme=" + this.clavePyme);
		log.debug ("ClaveEP0=" + this.claveEPO);
		
		//Configurar conexi�n
		
		AccesoDB		con				=	new AccesoDB();
		StringBuffer qrySentencia;
		List			lVarBind			=	null;
		Registros	registros		=	null;
		Registros	registros2		=	null;
		boolean		hayExito			=	true;
		Map			map				=  null;
		lVarBind							=	new ArrayList();
		hayExito	=	true;
		try {
			con.conexionDB();
			qrySentencia = new StringBuffer();
			qrySentencia.append(" SELECT cg_tipo_credito, ic_linea_credito_dm, ic_linea_credito" +
										" FROM comrel_pyme_epo_x_producto pep, dis_documento doc" +
										" WHERE pep.ic_pyme = doc.ic_pyme" +
										" AND pep.ic_producto_nafin = 4" +
										" AND doc.ic_pyme = ?" +
										" AND doc.ig_numero_docto = ?" +
										" AND pep.ic_epo = ?");
			lVarBind.add(new Integer(this.clavePyme));
			lVarBind.add(this.numeroDocto);
			lVarBind.add(new Integer(this.claveEPO));	
			
			registros = con.consultarDB(qrySentencia.toString(),lVarBind,false);
			
			log.debug("qrySentencia= "+qrySentencia.toString());
			log.debug("lVarBind= "+lVarBind);
			log.debug("registros");
			
		} catch(Exception e){
			log.error("obtenerLineaCredito(Error)",e);
			hayExito=false;
		} finally{
				if(!hayExito && con.hayConexionAbierta()) {
					con.cierraConexionDB();
				}
		}
		//OBTENER EL TIPO_COBRO_INTERES DEPENDIENDO DE LA LINEA DE CREDITO
		while (registros != null && registros.next()) {
			String rs_cg_tipo_credito			=	"";
			String rs_ic_linea_credito_dm		=	"";
			String rs_ic_linea_credito			=	"";
			String rs_ic_tipo_cobro_interes	=	"";
			
			try{
				hayExito	=true;
				rs_cg_tipo_credito		=	registros.getString("cg_tipo_credito");
				rs_ic_linea_credito_dm	=	registros.getString("ic_linea_credito_dm");
				rs_ic_linea_credito		=	registros.getString("ic_linea_credito");
			
				log.debug("rs_cg_tipo_credito= "+rs_cg_tipo_credito);
				log.debug("rs_ic_linea_credito_dm= "+rs_ic_linea_credito_dm);
				log.debug("rs_ic_linea_credito= "+rs_ic_linea_credito);
				
				if("D".equals(rs_cg_tipo_credito) && !"".equals(rs_ic_linea_credito_dm) && rs_ic_linea_credito_dm != null){
					qrySentencia = new StringBuffer();
					qrySentencia.append(" SELECT ic_tipo_cobro_interes" +
											" FROM dis_linea_credito_dm" +
											" WHERE ic_linea_credito_dm = ?");
					lVarBind.clear();
					lVarBind.add(new Integer(rs_ic_linea_credito_dm));
					
					log.debug("qrySentencia 1= "+qrySentencia.toString());
					
					registros2		= con.consultarDB(qrySentencia.toString(),lVarBind);
				}
				else if("C".equals(rs_cg_tipo_credito) && !"".equals(rs_ic_linea_credito) && rs_ic_linea_credito != null) {
					qrySentencia = new StringBuffer();
					qrySentencia.append(" SELECT ic_tipo_cobro_interes" +
											" FROM com_linea_credito" +
											" WHERE ic_linea_credito = ?");
					lVarBind.clear();
					lVarBind.add(new Integer(rs_ic_linea_credito));
					
					log.debug("qrySentencia 2= "+qrySentencia.toString());
					log.debug("lVarBind= "+lVarBind);
					
					registros2		= con.consultarDB(qrySentencia.toString(),lVarBind);
				}
				if (registros2 != null){
					if (registros2.next()){
						rs_ic_tipo_cobro_interes 	= registros2.getString("ic_tipo_cobro_interes");
					}
					log.debug("rs_ic_tipo_cobro_interes= "+rs_ic_tipo_cobro_interes);
				}
				
				map = new HashMap();
				
				rs_ic_linea_credito_dm = (rs_ic_linea_credito_dm == null) ? "" : rs_ic_linea_credito_dm;
				rs_ic_linea_credito = (rs_ic_linea_credito == null) ? "" : rs_ic_linea_credito;
				rs_ic_tipo_cobro_interes = (rs_ic_tipo_cobro_interes == null) ? "" : rs_ic_tipo_cobro_interes;
				
				map.put("LINEA_CREDITO_DM", rs_ic_linea_credito_dm);
				map.put("LINEA_CREDITO", rs_ic_linea_credito);
				map.put("TIPO_LINEA_CREDITO", rs_ic_tipo_cobro_interes);
				
			} catch (Exception e) {
					log.error("obtenerLineaCredito(Error2)", e);
					hayExito = false;
			} finally {
				if(con.hayConexionAbierta()) {
					con.cierraConexionDB();
					log.info("obtenerTipoCredito");
				}		
			}
		}
		log.debug("map=" + map);
		return map;
	}	
	public List getConditions() {
		return conditions;
	}
	public String getClaveEPO() {
		return claveEPO;
	}
	public void setClaveEPO(String claveEPO){
		this.claveEPO=claveEPO;
	}
	public String getClavePyme() {
		return clavePyme;
	}
	public void setClavePyme(String clavePyme) {
		this.clavePyme=clavePyme;
	}
	public String getNumeroDocto() {
		return numeroDocto;
	}
	public void setNumeroDocto(String numeroDocto){
		this.numeroDocto=numeroDocto;
	}
}