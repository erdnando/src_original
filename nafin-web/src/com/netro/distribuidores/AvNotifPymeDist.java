package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.math.BigDecimal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class AvNotifPymeDist  implements IQueryGenerator, IQueryGeneratorRegExtJS{
	private String cgTipoConversion;
	private String tiposCredito;
	
//	private String numOrden;

  public AvNotifPymeDist(){}
  
	private static final Log log = ServiceLocator.getInstance().getLog(AvNotifPymeDist.class);//Variable para enviar mensajes al log.
	private String tipoCreditoXepo;
	StringBuffer 	condicion;
       

	/**
	 *
	 */
	public String getAggregateCalculationQuery(HttpServletRequest request) {
		log.debug("getAggregateCalculationQuery(E)");
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer qryDM			  = new StringBuffer();
		StringBuffer qryCCC		    = new StringBuffer();
		StringBuffer qryFdR			= new StringBuffer();
		StringBuffer condicion    = new StringBuffer();
		String valor = request.getSession().getAttribute("iNoCliente").toString();		
		String ic_epo            = (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");		
		String ic_pyme            = (request.getParameter("ic_pyme") == null)?valor:request.getParameter("ic_pyme");
		String ic_if              = (request.getParameter("ic_if") == null)?"":request.getParameter("ic_if");
		String fchinicial         = (request.getParameter("Txtfchini") == null)?"":request.getParameter("Txtfchini");
		String fchfinal           = (request.getParameter("Txtffin") == null)?"":request.getParameter("Txtffin");
		String tiposCredito	=	(request.getParameter("tiposCredito")==null)?"":request.getParameter("tiposCredito");	
		String tipoCreditoXepo		=	(request.getParameter("tipoCreditoXepo")==null)?"":request.getParameter("tipoCreditoXepo");
		String numOrden = (request.getParameter("numeroOrden") == null)?"":request.getParameter("numeroOrden");
		if(tiposCredito.equals("")){
			tiposCredito = this.getTiposCredito();
		}
		if(tipoCreditoXepo.equals("")){
			tipoCreditoXepo = this.getTipoCreditoXepo();
		}
		
		try { 

		    
			 if(!ic_epo.equals(""))	 
	 			condicion.append("AND D.IC_EPO =  " + ic_epo); 
				
			
			if(!ic_pyme.equals(""))	 
				condicion.append(" AND D.IC_PYME =  " +	ic_pyme) ;
			if (!fchinicial.equals("") && !fchfinal.equals(""))
				condicion.append(" AND trunc(C3.df_fecha_hora) BETWEEN TO_DATE('"+fchinicial+"','DD/MM/YYYY') and TO_DATE('"+fchfinal+"','DD/MM/YYYY') ");		
			if(ic_if.equals("") && ic_pyme.equals("") && fchinicial.equals("") && fchfinal.equals(""))	 
				condicion.append(" AND TO_CHAR(C3.df_fecha_hora,'DD/MM/YYYY')=TO_CHAR(SYSDATE,'DD/MM/YYYY') ");		
			
			qryDM.append(
					" SELECT /*+index(d) index(ds) index(lcd) index(tc) use_nl(d s pe tc lcd c3 iep ds epo pn m)*/  " +
					"	distinct m.ic_moneda as moneda, m.cd_nombre as nommoneda," +
					"	sum(D.fn_monto) as monto ," +
					"	sum(D.fn_monto*decode(D.ic_tipo_financiamiento,1,D.fn_porc_descuento,0)/100) as monto_descuento, " +
					"	sum(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) as monto_valuado, " +
					"	sum(DS.fn_importe_recibir) as monto_credito, " +
					"	sum(DS.fn_importe_interes) as monto_interes " +
				
				" FROM DIS_DOCUMENTO D  " +
				" 	,DIS_SOLICITUD S " +
				" 	,COMREL_PRODUCTO_EPO PE  " +
				" 	,COM_TIPO_CAMBIO TC  " +
				" 	,DIS_LINEA_CREDITO_DM LCD  " +
				" 	,COM_ACUSE3 C3 " +
				" 	,COMREL_IF_EPO_X_PRODUCTO IEP  " +
				" 	,DIS_DOCTO_SELECCIONADO DS  " +
				" 	,COMCAT_PYME P  " +
				" 	,COMCAT_EPO E  " +
				" 	,COMCAT_IF CI " +
				" 	,COMCAT_TIPO_COBRO_INTERES TCI  " +
				" 	,COMCAT_TASA CT  " +
				" 	,COMCAT_ESTATUS_DOCTO ED  " +
				" 	,COMCAT_PRODUCTO_NAFIN PN  " +
				" 	,COMCAT_MONEDA M  " +
				" 	,COMCAT_TIPO_FINANCIAMIENTO TF " +
				" WHERE D.IC_EPO = PE.IC_EPO  " +
				" 	AND D.IC_PYME = P.IC_PYME  " +
				" 	AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO  " +
				" 	AND D.IC_MONEDA = M.IC_MONEDA  " +
				" 	AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM  " +
				" 	AND D.IC_EPO = E.IC_EPO  " +
				" 	AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO " +
				" 	AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO  " +
				" 	AND D.IC_EPO = IEP.IC_EPO  " +
				" 	AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO " +
				" 	AND DS.IC_TASA = CT.IC_TASA  " +
				" 	AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN  " +
				" 	AND S.CC_ACUSE = C3.CC_ACUSE  " +
				" 	AND TC.IC_MONEDA = M.IC_MONEDA  " +
				" 	AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)  " +
				" 	AND LCD.IC_IF = IEP.IC_IF  " +
				" 	AND LCD.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES " +
				" 	AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN  " +
				" 	AND LCD.IC_IF = CI.IC_IF  " +  
				" 	AND LCD.IC_LINEA_CREDITO_DM IS NOT NULL " +
				" 	AND PN.IC_PRODUCTO_NAFIN = 4  " +
					" AND D.IC_PYME = " + ic_pyme + " " + condicion 
				//" AND EPO.CS_HABILITADO = 'S'  " +
					);
				if(!ic_if.equals("")){	 
					qryDM.append("   AND LCD.IC_IF =  " + ic_if);
				}
				qryDM.append("   AND D.ic_estatus_docto in(4,11,32)  group by m.ic_moneda, m.cd_nombre");
				

				
			qryCCC.append(
					" SELECT /*+index(d) index(ds) index(lcd) use_nl(d pe pn ds s c3 tc lcd tci iep epo m ct tf)*/  " +
					"	distinct m.ic_moneda as moneda, m.cd_nombre as nommoneda," +
					"	sum(D.fn_monto) as monto," +
					"	sum(D.fn_monto*decode(D.ic_tipo_financiamiento,1,D.fn_porc_descuento,0)/100) as monto_descuento, " +
					"	sum(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) as monto_valuado, " +
					"	sum(DS.fn_importe_recibir) as monto_credito, " +
					"	sum(DS.fn_importe_interes) as monto_interes " +
					" FROM DIS_DOCUMENTO D " +
				" 	,COMCAT_PYME P " +
				" 	,COM_BINS_IF cbif"+
				" 	,dis_doctos_pago_tc ddptc"+
				" 	,COMREL_PRODUCTO_EPO PE " +
				" 	,COMCAT_PRODUCTO_NAFIN PN " +
				" 	,COMCAT_MONEDA M " +
				" 	,COM_TIPO_CAMBIO TC " +
				" 	,COM_LINEA_CREDITO LCD " +
				" 	,COMCAT_TIPO_COBRO_INTERES TCI " +
				" 	,COMREL_IF_EPO_X_PRODUCTO IEP " +
				" 	,COMCAT_TASA CT " +
				" 	,COMCAT_EPO E " +
				" 	,DIS_DOCTO_SELECCIONADO DS " +
				" 	,COMCAT_ESTATUS_DOCTO ED " +
				" 	,COMCAT_IF CI " +
				" 	,DIS_SOLICITUD S " +
				" 	,COM_ACUSE3 C3 " +
				" 	,COMCAT_TIPO_FINANCIAMIENTO TF " +
				" WHERE D.IC_EPO = PE.IC_EPO " +
				" 	AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN " +
				" 	AND d.ic_orden_pago_tc = ddptc.ic_orden_pago_tc(+)" +
				" 	AND P.IC_PYME = D.IC_PYME " +
				" 	AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO " +
				" 	AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO " +
				" 	AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO " +
				" 	AND D.IC_EPO = PE.IC_EPO " +
				" 	AND D.IC_MONEDA = M.IC_MONEDA " +
				" 	AND M.IC_MONEDA = TC.IC_MONEDA " +
				" 	AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA) " +
				" 	AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO " +
				"   AND (LCD.IC_IF = IEP.IC_IF OR cbif.IC_IF = IEP.IC_IF) "   +//////////////
				" 	AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES " +
				" 	AND IEP.IC_EPO = D.IC_EPO " +
				" 	AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN " +
				" 	AND DS.IC_TASA = CT.IC_TASA " +
				" 	AND D.IC_EPO = E.IC_EPO " +
				" 	AND S.CC_ACUSE = C3.CC_ACUSE " +
				" 	AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO " +
				" 	AND LCD.IC_IF = CI.IC_IF " +
				" 	AND PN.IC_PRODUCTO_NAFIN = 4 " +
				"  AND d.IC_BINS = cbif.IC_BINS(+)"  +
					"	AND D.IC_PYME = " + ic_pyme + " " + condicion 
					//"	AND EPO.CS_HABILITADO = 'S' " +
					);
			if(!numOrden.equals("")){
				qryCCC.append(" and ddptc.ic_orden_pago_tc ="+numOrden);
			}
			if(!ic_if.equals("")){	 
					qryCCC.append("   AND (LCD.IC_IF = "+ic_if+" OR cbif.IC_IF = "+ic_if+") ");
			}
			qryCCC.append(" AND D.ic_estatus_docto in(4,11,32) group by m.ic_moneda, m.cd_nombre ");
					
			qryFdR.append(
					" SELECT /*+index(d) index(ds) index(lcd) use_nl(d pe pn ds s c3 tc lcd tci iep epo m ct tf)*/  " +
					"	distinct m.ic_moneda as moneda, m.cd_nombre as nommoneda," +
					"	sum(D.fn_monto) as monto," +
					"	sum(D.fn_monto*decode(D.ic_tipo_financiamiento,1,D.fn_porc_descuento,0)/100) as monto_descuento, " +
					"	sum(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) as monto_valuado, " +
					"	sum(DS.fn_importe_recibir) as monto_credito, " +
					"	sum(DS.fn_importe_interes) as monto_interes " +
			" FROM DIS_DOCUMENTO D " +
				" 	,COMCAT_PYME P " +
				" 	,COMREL_PRODUCTO_EPO PE " +
				" 	,COMCAT_PRODUCTO_NAFIN PN " +
				" 	,COMCAT_MONEDA M " +
				" 	,COM_TIPO_CAMBIO TC " +
				" 	,COM_LINEA_CREDITO LCD " +
				" 	,COMCAT_TIPO_COBRO_INTERES TCI " +
				" 	,COMREL_IF_EPO_X_PRODUCTO IEP " +
				" 	,COMCAT_TASA CT " +
				" 	,COMCAT_EPO E " +
				" 	,DIS_DOCTO_SELECCIONADO DS " +
				" 	,COMCAT_ESTATUS_DOCTO ED " +
				" 	,COMCAT_IF CI " +
				" 	,DIS_SOLICITUD S " +
				" 	,COM_ACUSE3 C3 " +
				" 	,COMCAT_TIPO_FINANCIAMIENTO TF " +
				" WHERE D.IC_EPO = PE.IC_EPO " +
				" 	AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN " +
				" 	AND P.IC_PYME = D.IC_PYME " +
				" 	AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO " +
				" 	AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO " +
				" 	AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO (+)" +
				" 	AND D.IC_EPO = PE.IC_EPO " +
				" 	AND D.IC_MONEDA = M.IC_MONEDA " +
				" 	AND M.IC_MONEDA = TC.IC_MONEDA " +
				" 	AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA) " +
				" 	AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO " +
				" 	AND LCD.IC_IF = IEP.IC_IF " +
				" 	AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES " +
				" 	AND IEP.IC_EPO = D.IC_EPO " +
				" 	AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN " +
				" 	AND DS.IC_TASA = CT.IC_TASA " +
				" 	AND D.IC_EPO = E.IC_EPO " +
				" 	AND S.CC_ACUSE = C3.CC_ACUSE " +
				" 	AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO " +
				
				" 	AND LCD.IC_IF = CI.IC_IF " +
				" 	AND PN.IC_PRODUCTO_NAFIN = 4 " +
				" and d.IC_TIPO_FINANCIAMIENTO is null "+
				"	AND LCD.cg_tipo_solicitud = 'I' "+
				"	AND LCD.cs_factoraje_con_rec = 'S' "+
					"	AND D.IC_PYME = " + ic_pyme + " " + condicion +
				//	"	AND EPO.CS_HABILITADO = 'S' " +
					" and d.IC_TIPO_FINANCIAMIENTO is null "+
					"	AND LCD.cg_tipo_solicitud = 'I' "+
					"	AND LCD.cs_factoraje_con_rec = 'S' ");
			if(!ic_if.equals("")){	 
					qryFdR.append("   AND LCD.IC_IF =  " + ic_if);
			}
			qryFdR.append(" AND D.ic_estatus_docto in(4,11) group by m.ic_moneda, m.cd_nombre ");
					
					

			String qryAux = "";	
			if("D".equals(tiposCredito))
				qryAux = qryDM.toString();
			else if("C".equals(tiposCredito))
				qryAux = qryCCC.toString();
			else  if("".equals(tiposCredito)  && tipoCreditoXepo.equals("F")) 
				qryAux = qryFdR.toString();
			else
				qryAux = qryDM.toString()+ " UNION  "+qryCCC.toString();

			qrySentencia.append(
				"SELECT moneda, nommoneda, COUNT (*), SUM (monto), SUM(monto_descuento), " +
				" SUM(monto_valuado), SUM(monto_credito), SUM(monto_interes), " +
				" 'AvNotifPymeDist::getAggregateCalculationQuery'"+
				"  FROM ("+qryAux.toString()+")"+
				"GROUP BY  moneda, nommoneda ORDER BY moneda "); 


	log.error("qrySentencia  " + qrySentencia);

		}catch(Exception e){
			log.error("AvNotifPymeDist::getAggregateCalculationQuery " + e);
		}
		log.debug("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();
	}


	/**
	 *
	 */
	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids) {
		log.debug("getDocumentSummaryQueryForIds(E)");
		int i=0;
		StringBuffer qrySentencia	= new StringBuffer();
		StringBuffer qryDM			= new StringBuffer();
		StringBuffer qryCCC			= new StringBuffer();
		StringBuffer qryFdR			= new StringBuffer();
		StringBuffer condicion		= new StringBuffer();
		String valor = request.getSession().getAttribute("iNoCliente").toString();
		String ic_pyme = (request.getParameter("ic_pyme") == null)?valor:request.getParameter("ic_pyme");
		String tiposCredito	=	(request.getParameter("tiposCredito")==null)?"":request.getParameter("tiposCredito");	
		String ic_epo            = (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");		
		String tipoCreditoXepo		=	(request.getParameter("tipoCreditoXepo")==null)?"":request.getParameter("tipoCreditoXepo");
		String numOrden = (request.getParameter("numeroOrden") == null)?"":request.getParameter("numeroOrden");
		
		if(tiposCredito.equals("")){
			tiposCredito = this.getTiposCredito();
		}
		if(tipoCreditoXepo.equals("")){
			tipoCreditoXepo = this.getTipoCreditoXepo();
		}
		
		condicion.append(" AND d.ic_documento in (");
		i=0;
		for (Iterator it = ids.iterator(); it.hasNext();i++){
        	if(i>0){
				condicion.append(",");
			}
			condicion.append(" "+it.next().toString()+" ");
		}
		condicion.append(") ");
		
		qryDM.append(
				" SELECT /*+ index (d CP_DIS_DOCUMENTO_PK) index (lcd CP_DIS_LINEA_CREDITO_DM) use_nl(p lcd iep ci d) */ " +
				" 	distinct '' as CG_DESCRIPCION_BINS, P.cg_razon_social as DISTRIBUIDOR  " +
				" 	,E.cg_razon_social as EPO  " +
				" 	,D.ig_numero_docto  " +
				" 	,D.cc_acuse  " +
				" 	,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') as df_fecha_emision  " +
				" 	,TO_CHAR(D.df_carga,'dd/mm/yyyy') as df_fecha_publicacion  " +
				" 	,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc	  " +
				" 	,D.IG_PLAZO_DOCTO " +
				" 	,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as df_fecha_seleccion  " +
				" 	,M.cd_nombre as MONEDA  " +
				" 	,M.ic_moneda  " +
				" 	,D.fn_monto  " +
				" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION  " +
				" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO  " +
				" 	,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) as MONTO_VALUADO  " +
				" 	,D.IG_PLAZO_DESCUENTO  " +
				" 	,D.FN_MONTO " +
				" 	,decode(D.ic_tipo_financiamiento,1,D.fn_porc_descuento,0) as fn_porc_descuento	  " +
				" 	,D.fn_monto*decode(D.ic_tipo_financiamiento,1,D.fn_porc_descuento,0)/100 as MONTO_DESCUENTO  " +
				" 	,ED.CD_DESCRIPCION AS ESTATUS  " +
				"	,ED.IC_ESTATUS_DOCTO AS IC_ESTATUS_DOCTO "+
				"	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE) AS REFERENCIA_INT" +
				" 	,DS.fn_valor_tasa AS VALOR_TASA  " +
				" 	,D.IG_PLAZO_CREDITO  " +
				" 	,DS.fn_importe_recibir as MONTO_CREDITO  " +
				" 	,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO  " +
				" 	,TCI.ic_tipo_cobro_interes  " +
				" 	,TCI.cd_descripcion as tipo_cobro_interes  " +
				" 	,DS.fn_importe_interes as MONTO_INTERES  " +
				" 	,D.ic_documento  " +
				" 	,E.cg_razon_social as EPO  " +
				" 	, D.ic_documento " +
				
				" 	,CI.cg_razon_social as DIF  " +
				" 	,LCD.ic_moneda as moneda_linea " +
				"  ,'' as IC_ORDEN_ENVIADO"   +
				"  ,'' as NUMERO_TC"   +
				"  ,'' as IC_OPERACION_CCACUSE"   +
				"  ,'' AS codigo_autorizado"   +
				"  ,'' as FECHA_REGISTRO"   +
				" 	,TF.CD_DESCRIPCION AS MODO_PLAZO, 'AvNotifPymeDist::getDocumentQueryFile' " +
				"   ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				"  ,  (SELECT nvl(fn_valor_aforo, 0)     FROM dis_aforo_epo   WHERE ic_if = ci.ic_if      AND ic_epo = d.ic_epo        AND ic_moneda = d.ic_moneda) AS POR_AFRO   "+
				" , D.IG_TIPO_PAGO " +
				" FROM DIS_DOCUMENTO D  " +
				" 	,DIS_SOLICITUD S " +
				" 	,COMREL_PRODUCTO_EPO PE  " +
				" 	,COM_TIPO_CAMBIO TC  " +
				" 	,DIS_LINEA_CREDITO_DM LCD  " +
				" 	,COM_ACUSE3 C3 " +
				" 	,COMREL_IF_EPO_X_PRODUCTO IEP  " +
				" 	,DIS_DOCTO_SELECCIONADO DS  " +
				" 	,COMCAT_PYME P  " +
				" 	,COMCAT_EPO E  " +
				"  ,COMCAT_IF CI "   +
				" 	,COMCAT_TIPO_COBRO_INTERES TCI  " +
				" 	,COMCAT_TASA CT  " +
				" 	,COMCAT_ESTATUS_DOCTO ED  " +
				" 	,COMCAT_PRODUCTO_NAFIN PN  " +
				" 	,COMCAT_MONEDA M  " +
				" 	,COMCAT_TIPO_FINANCIAMIENTO TF " +
				" WHERE D.IC_EPO = PE.IC_EPO  " +
				" 	AND D.IC_PYME = P.IC_PYME  " +
				" 	AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO  " +
				" 	AND D.IC_MONEDA = M.IC_MONEDA  " +
				" 	AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM  " +
				" 	AND D.IC_EPO = E.IC_EPO  " +
				" 	AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO " +
				" 	AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO  " +
				" 	AND D.IC_EPO = IEP.IC_EPO  " +
				" 	AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO " +
				" 	AND DS.IC_TASA = CT.IC_TASA  " +
				" 	AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN  " +
				" 	AND S.CC_ACUSE = C3.CC_ACUSE  " +
				" 	AND TC.IC_MONEDA = M.IC_MONEDA  " +
				" 	AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)  " +
				" 	AND LCD.IC_IF = IEP.IC_IF  " +
				" 	AND LCD.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES " +
				" 	AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN  " +
				" 	AND LCD.IC_IF = CI.IC_IF  " +
				" 	AND LCD.IC_LINEA_CREDITO_DM IS NOT NULL " +
				" 	AND PN.IC_PRODUCTO_NAFIN = 4  " +
				" 	AND D.IC_PYME = " + ic_pyme + " " + condicion);
				
		qryCCC.append(
				" SELECT /*+ index (d CP_DIS_DOCUMENTO_PK) index (lcd CP_DIS_LINEA_CREDITO_DM) */ " +
				" 	distinct cbif.CG_CODIGO_BIN||' ' ||cbif.cg_descripcion as CG_DESCRIPCION_BINS, P.cg_razon_social as DISTRIBUIDOR " +
				" 	,E.cg_razon_social as EPO " +
				" 	,D.ig_numero_docto " +
				" 	,D.cc_acuse " +
				" 	,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') as df_fecha_emision " +
				" 	,TO_CHAR(D.df_carga,'dd/mm/yyyy') as df_fecha_publicacion " +
				" 	,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc " +
				" 	,D.IG_PLAZO_DOCTO " +
				" 	,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as df_fecha_seleccion " +
				" 	,M.cd_nombre as MONEDA " +
				" 	,M.ic_moneda " +
				" 	,D.fn_monto " +
				" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION " +
				" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO " +
				" 	,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) as MONTO_VALUADO " +
				" 	,D.IG_PLAZO_DESCUENTO " +
				" 	,D.FN_MONTO " +
				" 	,decode(D.ic_tipo_financiamiento,1,D.fn_porc_descuento,0) as fn_porc_descuento " +
				" 	,D.fn_monto*decode(D.ic_tipo_financiamiento,1,D.fn_porc_descuento,0)/100 as MONTO_DESCUENTO " +
				" 	,ED.CD_DESCRIPCION AS ESTATUS " +
				"	,ED.IC_ESTATUS_DOCTO AS IC_ESTATUS_DOCTO "+
				" 	,CT.CD_NOMBRE AS REFERENCIA_INT " +
				" 	,DECODE(DS.CG_REL_MAT " +
				" 			,'+',DS.fn_valor_tasa + DS.fn_puntos " +
				" 			,'-',DS.fn_valor_tasa - DS.fn_puntos " +
				" 			,'*',DS.fn_valor_tasa * DS.fn_puntos " +
				" 			,'/',DS.fn_valor_tasa / DS.fn_puntos " +
				" 			,0) AS VALOR_TASA " +
				" 	,D.IG_PLAZO_CREDITO " +
				" 	,DS.fn_importe_recibir as MONTO_CREDITO " +
				" 	,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO " +
				" 	,TCI.ic_tipo_cobro_interes " +
				" 		,TCI.cd_descripcion as tipo_cobro_interes " +
				" 	,DS.fn_importe_interes as MONTO_INTERES " +
				" 	,D.ic_documento " +
				" 	,E.cg_razon_social as EPO " +   
				" 	, D.ic_documento " +
				"  ,decode(ed.ic_estatus_docto,32,ci2.cg_razon_social,ci.cg_razon_social) AS dif"   +
				" 	,LCD.ic_moneda as moneda_linea " +
				"  ,DDPTC.IC_ORDEN_PAGO_TC as IC_ORDEN_ENVIADO"   +
				"  ,DDPTC.CG_NUM_TARJETA as NUMERO_TC"   +
				"  ,DDPTC.CG_TRANSACCION_ID as IC_OPERACION_CCACUSE"   +
				"  ,decode(CONCAT(ddptc.CG_CODIGO_RESP ,ddptc.CG_CODIGO_DESC),'','',ddptc.CG_CODIGO_RESP||'-' ||ddptc.CG_CODIGO_DESC)AS codigo_autorizado"   +
				"  ,TO_CHAR(DDPTC.DF_FECHA_AUTORIZACION,'dd/mm/yyyy') as FECHA_REGISTRO"   +
		
				" 	,TF.CD_DESCRIPCION AS MODO_PLAZO,  'AvNotifPymeDist::getDocumentQueryFile' " +
				" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				"  ,  (SELECT nvl(fn_valor_aforo, 0)     FROM dis_aforo_epo   WHERE ic_if = ci.ic_if      AND ic_epo = d.ic_epo        AND ic_moneda = d.ic_moneda) AS POR_AFRO  "+
				" , D.IG_TIPO_PAGO " +
				" FROM DIS_DOCUMENTO D " +
				"  ,DIS_DOCTOS_PAGO_TC DDPTC"   +/////////////////777
				" 	,COM_BINS_IF cbif " +////////////////////77
				" 	,COMCAT_PYME P " +
				" 	,COMREL_PRODUCTO_EPO PE " +
				" 	,COMCAT_PRODUCTO_NAFIN PN " +
				" 	,COMCAT_MONEDA M " +
				" 	,COM_TIPO_CAMBIO TC " +
				" 	,COM_LINEA_CREDITO LCD " +
				" 	,COMCAT_TIPO_COBRO_INTERES TCI " +
				" 	,COMREL_IF_EPO_X_PRODUCTO IEP " +
				" 	,COMCAT_TASA CT " +
				" 	,COMCAT_EPO E " +
				" 	,DIS_DOCTO_SELECCIONADO DS " +
				" 	,COMCAT_ESTATUS_DOCTO ED " +
				"  ,COMCAT_IF CI "   +
				"	,comcat_if ci2 "+/////////////////agregados 
				" 	,DIS_SOLICITUD S " +
				" 	,COM_ACUSE3 C3 " +
				" 	,COMCAT_TIPO_FINANCIAMIENTO TF " +
				" WHERE D.IC_EPO = PE.IC_EPO " +
				"  AND D.IC_ORDEN_PAGO_TC = DDPTC.IC_ORDEN_PAGO_TC(+)"   +///////////////////
				" 	AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN " +
				" 	AND P.IC_PYME = D.IC_PYME " +
				" 	AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO " +
				" 	AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO " +
				" 	AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO " +
				" 	AND D.IC_EPO = PE.IC_EPO " +
				" 	AND D.IC_MONEDA = M.IC_MONEDA " +
				" 	AND M.IC_MONEDA = TC.IC_MONEDA " +
				" 	AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA) " +
				" 	AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO " +
				"   AND (LCD.IC_IF = IEP.IC_IF OR cbif.IC_IF = IEP.IC_IF) "   +//////////////
				" 	AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES " +
				" 	AND IEP.IC_EPO = D.IC_EPO " +
				" 	AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN " +
				" 	AND DS.IC_TASA = CT.IC_TASA " +
				" 	AND D.IC_EPO = E.IC_EPO " +
				" 	AND S.CC_ACUSE = C3.CC_ACUSE " +
				" 	AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO " +
				"  AND d.IC_BINS = cbif.IC_BINS(+)"  +////////////77
				"  AND cbif.IC_if = ci2.IC_if(+)"  +/////////////////7
				" 	AND LCD.IC_IF = CI.IC_IF " +
				" 	AND PN.IC_PRODUCTO_NAFIN = 4 " +
				" 	AND D.IC_PYME = " + ic_pyme + " " + condicion);
				
				
				
		qryFdR.append(
				" SELECT /*+ index (d CP_DIS_DOCUMENTO_PK) index (lcd CP_DIS_LINEA_CREDITO_DM) */ " +
				" 	distinct '' as CG_DESCRIPCION_BINS, P.cg_razon_social as DISTRIBUIDOR " +
				" 	,E.cg_razon_social as EPO " +
				" 	,D.ig_numero_docto " +
				" 	,D.cc_acuse " +
				" 	,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') as df_fecha_emision " +
				" 	,TO_CHAR(D.df_carga,'dd/mm/yyyy') as df_fecha_publicacion " +
				" 	,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc " +
				" 	,D.IG_PLAZO_DOCTO " +
				" 	,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as df_fecha_seleccion " +
				" 	,M.cd_nombre as MONEDA " +
				" 	,M.ic_moneda " +
				" 	,D.fn_monto " +
				" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION " +
				" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO " +
				" 	,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) as MONTO_VALUADO " +
				" 	,D.IG_PLAZO_DESCUENTO " +
				" 	,D.FN_MONTO " +
				" 	,decode(D.ic_tipo_financiamiento,1,D.fn_porc_descuento,0) as fn_porc_descuento " +
				" 	,D.fn_monto*decode(D.ic_tipo_financiamiento,1,D.fn_porc_descuento,0)/100 as MONTO_DESCUENTO " +
				" 	,ED.CD_DESCRIPCION AS ESTATUS " +
				"	,ED.IC_ESTATUS_DOCTO AS IC_ESTATUS_DOCTO "+
				" 	,CT.CD_NOMBRE AS REFERENCIA_INT " +
				" 	,DECODE(DS.CG_REL_MAT " +
				" 			,'+',DS.fn_valor_tasa + DS.fn_puntos " +
				" 			,'-',DS.fn_valor_tasa - DS.fn_puntos " +
				" 			,'*',DS.fn_valor_tasa * DS.fn_puntos " +
				" 			,'/',DS.fn_valor_tasa / DS.fn_puntos " +
				" 			,0) AS VALOR_TASA " +
				" 	,D.IG_PLAZO_CREDITO " +
				" 	,DS.fn_importe_recibir as MONTO_CREDITO " +
				" 	,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO " +
				" 	,TCI.ic_tipo_cobro_interes " +
				" 		,TCI.cd_descripcion as tipo_cobro_interes " +
				" 	,DS.fn_importe_interes as MONTO_INTERES " +
				" 	,D.ic_documento " +
				" 	,E.cg_razon_social as EPO " +
				" 	, D.ic_documento " +
				" 	,CI.cg_razon_social as DIF  " +
				" 	,LCD.ic_moneda as moneda_linea " +
				"  ,'' as IC_ORDEN_ENVIADO"   +
				"  ,'' as NUMERO_TC"   +
				"  ,'' as IC_OPERACION_CCACUSE"   +
				"  ,'' AS codigo_autorizado"   +
				"  ,'' as FECHA_REGISTRO"   +
				" 	,'NA' AS MODO_PLAZO,  'AvNotifPymeDist::getDocumentQueryFile' " +
				" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				" ,lcd.ig_cuenta_bancaria as CG_NUMERO_CUENTA "+
				"  ,  (SELECT nvl(fn_valor_aforo, 0)     FROM dis_aforo_epo   WHERE ic_if = ci.ic_if      AND ic_epo = d.ic_epo        AND ic_moneda = d.ic_moneda) AS POR_AFRO  "+				
				" , D.IG_TIPO_PAGO " +
				" FROM DIS_DOCUMENTO D " +
				" 	,COMCAT_PYME P " +
				" 	,COMREL_PRODUCTO_EPO PE " +
				" 	,COMCAT_PRODUCTO_NAFIN PN " +
				" 	,COMCAT_MONEDA M " +
				" 	,COM_TIPO_CAMBIO TC " +
				" 	,COM_LINEA_CREDITO LCD " +
				" 	,COMCAT_TIPO_COBRO_INTERES TCI " +
				" 	,COMREL_IF_EPO_X_PRODUCTO IEP " +
				" 	,COMCAT_TASA CT " +
				" 	,COMCAT_EPO E " +
				" 	,DIS_DOCTO_SELECCIONADO DS " +
				" 	,COMCAT_ESTATUS_DOCTO ED " +
				"  ,COMCAT_IF CI "   + 
				" 	,DIS_SOLICITUD S " +
				" 	,COM_ACUSE3 C3 " +
				" 	,COMCAT_TIPO_FINANCIAMIENTO TF " +
				" WHERE D.IC_EPO = PE.IC_EPO " +
				" 	AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN " +
				" 	AND P.IC_PYME = D.IC_PYME " +
				" 	AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO " +
				" 	AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO " +
				" 	AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO (+)" +
				" 	AND D.IC_EPO = PE.IC_EPO " +
				" 	AND D.IC_MONEDA = M.IC_MONEDA " +
				" 	AND M.IC_MONEDA = TC.IC_MONEDA " +
				" 	AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA) " +
				" 	AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO " +
				" 	AND LCD.IC_IF = IEP.IC_IF " +
				" 	AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES " +
				" 	AND IEP.IC_EPO = D.IC_EPO " +
				" 	AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN " +
				" 	AND DS.IC_TASA = CT.IC_TASA " +
				" 	AND D.IC_EPO = E.IC_EPO " +
				" 	AND S.CC_ACUSE = C3.CC_ACUSE " +
				" 	AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO " +
				" 	AND LCD.IC_IF = CI.IC_IF " +
				" 	AND PN.IC_PRODUCTO_NAFIN = 4 " +
				" and d.IC_TIPO_FINANCIAMIENTO is null "+
				"	AND LCD.cg_tipo_solicitud = 'I' "+
				"	AND LCD.cs_factoraje_con_rec = 'S' "+
				" 	AND D.IC_PYME = " + ic_pyme + " " + condicion);

		if("D".equals(tiposCredito))
			qrySentencia.append(qryDM.toString());
		else if("C".equals(tiposCredito))
			qrySentencia.append(qryCCC.toString());
		else if("".equals(tiposCredito)  && tipoCreditoXepo.equals("F"))   
				qrySentencia.append(qryFdR.toString());
		else
			qrySentencia.append(qryDM + " UNION  " + qryCCC);
		
		log.debug("el query queda de la siguiente manera "+qrySentencia.toString());
		log.debug("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();
	}
	
	
	/**
	 *En esta funcion se obtienen las llaves primarias de la consulta 
	 */
	public String getDocumentQuery(HttpServletRequest request) {
		log.debug("getDocumentQuery(E)");
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
    StringBuffer qrySentencia = new StringBuffer();
    StringBuffer qryDM			  = new StringBuffer();
    StringBuffer qryCCC		    = new StringBuffer();
    StringBuffer condicion    = new StringBuffer();
	 StringBuffer qryFdR			= new StringBuffer();

		String valor = request.getSession().getAttribute("iNoCliente").toString();
		String ic_pyme = (request.getParameter("ic_pyme") == null)?valor:request.getParameter("ic_pyme");
		String ic_if = (request.getParameter("ic_if") == null)?"":request.getParameter("ic_if");
		String ic_epo = (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");
		String tiposCredito	=	(request.getParameter("tiposCredito")==null)?"":request.getParameter("tiposCredito");	
		String fchinicial = (request.getParameter("Txtfchini") == null)?"":request.getParameter("Txtfchini");
		String fchfinal = (request.getParameter("Txtffin") == null)?"":request.getParameter("Txtffin");	
		String numOrden = (request.getParameter("numeroOrden") == null)?"":request.getParameter("numeroOrden");
		String cgTipoConversion	=	(request.getParameter("tipo_conversion")==null)?"":request.getParameter("tipo_conversion");	
		String tipoCreditoXepo		=	(request.getParameter("tipoCreditoXepo")==null)?"":request.getParameter("tipoCreditoXepo");
		
		if(tiposCredito.equals("")){
			tiposCredito = this.getTiposCredito();
		}
		if(tipoCreditoXepo.equals("")){
			tipoCreditoXepo = this.getTipoCreditoXepo();
		}
		
		
		try {  

		   
			if(!ic_epo.equals(""))	 
	 			condicion.append("AND D.IC_EPO =  " + ic_epo);
			
			if (!fchinicial.equals("") && !fchfinal.equals(""))
				condicion.append(" AND trunc(C3.df_fecha_hora) BETWEEN TO_DATE('"+fchinicial+"','DD/MM/YYYY') and TO_DATE('"+fchfinal+"','DD/MM/YYYY') ");
			if(ic_epo.equals("") && ic_if.equals("") && fchinicial.equals("") && fchfinal.equals(""))	 
				condicion.append(" AND TO_CHAR(C3.df_fecha_hora,'DD/MM/YYYY')=TO_CHAR(SYSDATE,'DD/MM/YYYY') ");
		
		
		qryDM.append(
				" SELECT /*+index(d) index(lcd) use_nl(d s pe tc lcd c3 iep ds epo pn)*/  " +
				" 	distinct D.ic_documento  " +
				" FROM DIS_DOCUMENTO D  " +
				" 	,DIS_SOLICITUD S " +
				" 	,COMREL_PRODUCTO_EPO PE  " +
				" 	,COM_TIPO_CAMBIO TC  " +
				" 	,DIS_LINEA_CREDITO_DM LCD  " +
				" 	,COM_ACUSE3 C3 " +
				" 	,COMREL_IF_EPO_X_PRODUCTO IEP  " +
				" 	,DIS_DOCTO_SELECCIONADO DS  " +
				" 	,COMCAT_EPO EPO  " +
				" 	,COMCAT_PRODUCTO_NAFIN PN  " +
				" WHERE D.IC_EPO = PE.IC_EPO  " +
				" 	AND PE.IC_EPO = EPO.IC_EPO  " +
				" 	AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO  " +
				" 	AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM  " +
				" 	AND D.IC_EPO = IEP.IC_EPO  " +
				" 	AND IEP.IC_EPO = EPO.IC_EPO  " +
				" 	AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO " +
				" 	AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN  " +
				" 	AND S.CC_ACUSE = C3.CC_ACUSE  " +
				" 	AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = D.IC_MONEDA)  " +
				" 	AND LCD.IC_IF = IEP.IC_IF  " +
				" 	AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN  " +
				" 	AND LCD.IC_LINEA_CREDITO_DM IS NOT NULL " +
				" 	AND PN.IC_PRODUCTO_NAFIN = 4  " +
				" 	AND EPO.CS_HABILITADO = 'S'  " +
				" 	AND D.IC_PYME =  " + ic_pyme + " " + condicion);
				if(!ic_if.equals("")){	 
					qryDM.append("   AND LCD.IC_IF =  " + ic_if);
				}				
				qryDM.append(" AND D.ic_estatus_docto in(4,11,32)  ");
				
		qryCCC.append(
				" SELECT /*+ index (d CP_DIS_DOCUMENTO_PK) index (lcd CP_DIS_LINEA_CREDITO_DM) use_nl(lcd iep d s) */  " +
				" 	distinct D.ic_documento " +
				" FROM DIS_DOCUMENTO D " +
				" 	,COMREL_PRODUCTO_EPO PE " +
				" 	,COMCAT_PRODUCTO_NAFIN PN " +
				" 	,DIS_DOCTO_SELECCIONADO DS " +
				" 	,DIS_SOLICITUD S " +
				" 	,COM_ACUSE3 C3 " +
				" 	,COM_TIPO_CAMBIO TC " +
				" 	,COM_BINS_IF cbif"+///////////////7
				"  ,DIS_DOCTOS_PAGO_TC DDPTC"   +
				" 	,COM_LINEA_CREDITO LCD " +
				" 	,COMCAT_TIPO_COBRO_INTERES TCI " +
				" 	,COMREL_IF_EPO_X_PRODUCTO IEP " +
				" 	,COMCAT_EPO EPO " +
				" 	,COMCAT_MONEDA M " +
				" 	,COMCAT_TASA CT " +
				" 	,COMCAT_TIPO_FINANCIAMIENTO TF " +
				" WHERE D.IC_EPO = PE.IC_EPO " +   
				"  AND d.IC_BINS = cbif.IC_BINS(+)"  +////////////////77
				"  AND D.IC_ORDEN_PAGO_TC = DDPTC.IC_ORDEN_PAGO_TC(+)" +
				" 	AND PE.IC_EPO = EPO.IC_EPO " +
				" 	AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN " +
				" 	AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO " +
				" 	AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO " +
				" 	AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO " +
				" 	AND D.IC_EPO = PE.IC_EPO " +
				" 	AND D.IC_MONEDA = M.IC_MONEDA " +
				" 	AND M.IC_MONEDA = TC.IC_MONEDA " +
				" 	AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA) " +
				" 	AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO " +
				"   AND (LCD.IC_IF = IEP.IC_IF OR cbif.IC_IF = IEP.IC_IF) "   +//////////////
				" 	AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES " +
				" 	AND IEP.IC_EPO = D.IC_EPO " +
				" 	AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN " +
				" 	AND DS.IC_TASA = CT.IC_TASA " +
				" 	AND S.CC_ACUSE = C3.CC_ACUSE " +
				" 	AND PN.IC_PRODUCTO_NAFIN = 4 " +
				" 	AND EPO.CS_HABILITADO = 'S' " +
				" 	AND D.IC_PYME =  " + ic_pyme + " " + condicion);
				if(!numOrden.equals("")){
					qryCCC.append(" and ddptc.ic_orden_pago_tc ="+numOrden);
				}
				if(!ic_if.equals("")){	 
					qryCCC.append("   AND (LCD.IC_IF = "+ic_if+" OR cbif.IC_IF = "+ic_if+") ");
				}
				qryCCC.append(" AND D.ic_estatus_docto in(4,11,32)  ");
				
			qryFdR.append(
				" SELECT /*+ index (d CP_DIS_DOCUMENTO_PK) index (lcd CP_DIS_LINEA_CREDITO_DM) use_nl(lcd iep d s) */  " +
				" 	distinct D.ic_documento " +
				" FROM DIS_DOCUMENTO D " +
				" 	,COMREL_PRODUCTO_EPO PE " +
				" 	,COMCAT_PRODUCTO_NAFIN PN " +
				" 	,DIS_DOCTO_SELECCIONADO DS " +
				" 	,DIS_SOLICITUD S " +
				" 	,COM_ACUSE3 C3 " +
				" 	,COM_TIPO_CAMBIO TC " +
				" 	,COM_LINEA_CREDITO LCD " +
				" 	,COMCAT_TIPO_COBRO_INTERES TCI " +
				" 	,COMREL_IF_EPO_X_PRODUCTO IEP " +
				" 	,COMCAT_EPO EPO " +
				" 	,COMCAT_MONEDA M " +
				" 	,COMCAT_TASA CT " +
				" 	,COMCAT_TIPO_FINANCIAMIENTO TF " +
				" WHERE D.IC_EPO = PE.IC_EPO " +
				" 	AND PE.IC_EPO = EPO.IC_EPO " +
				" 	AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN " +
				" 	AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO " +
				" 	AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO " +
				" 	AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO(+) " +
				" 	AND D.IC_EPO = PE.IC_EPO " +
				" 	AND D.IC_MONEDA = M.IC_MONEDA " +
				" 	AND M.IC_MONEDA = TC.IC_MONEDA " +
				" 	AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA) " +
				" 	AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO " +
				" 	AND LCD.IC_IF = IEP.IC_IF " +
				" 	AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES " +
				" 	AND IEP.IC_EPO = D.IC_EPO " +
				" 	AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN " +
				" 	AND DS.IC_TASA = CT.IC_TASA " +
				" 	AND S.CC_ACUSE = C3.CC_ACUSE " +
				" 	AND PN.IC_PRODUCTO_NAFIN = 4 " +
				" 	AND EPO.CS_HABILITADO = 'S' " +
				" and d.IC_TIPO_FINANCIAMIENTO is null "+
				"	AND LCD.cg_tipo_solicitud = 'I' "+
				"	AND LCD.cs_factoraje_con_rec = 'S' "+
				" 	AND D.IC_PYME =  " + ic_pyme + " " + condicion);	
			if(!ic_if.equals("")){	 
					qryFdR.append("   AND LCD.IC_IF =  " + ic_if);
			}
			qryFdR.append(" AND D.ic_estatus_docto in(4,11)  ");
		
			if("D".equals(tiposCredito))
				qrySentencia.append(qryDM.toString());
			else if("C".equals(tiposCredito))
				qrySentencia.append(qryCCC.toString());					 
			else  if("".equals(tiposCredito)  && tipoCreditoXepo.equals("F"))   
				qrySentencia.append(qryFdR.toString());
			else
				qrySentencia.append(qryDM.toString()+ " UNION  "+qryCCC.toString());
	

			log.debug("El query de la llave primaria: "+qrySentencia.toString());
		}catch(Exception e){
			log.error("AvNotifPymeDist::getDocumentQuery Exception "+e);
		}
		log.debug("getDocumentQuery(S)");
		return qrySentencia.toString();
	}
	
	
	/**
	 *
	 */
	public String getDocumentQueryFile(HttpServletRequest request) {
		log.debug("getDocumentQueryFile(E)");
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer qryDM			  = new StringBuffer();
		StringBuffer qryCCC		    = new StringBuffer();
		StringBuffer qryFdR			= new StringBuffer();
		StringBuffer condicion    = new StringBuffer();
		String valor = request.getSession().getAttribute("iNoCliente").toString();
		String ic_pyme = (request.getParameter("ic_pyme") == null)?valor:request.getParameter("ic_pyme");
		String ic_if = (request.getParameter("ic_if") == null)?"":request.getParameter("ic_if");
		String ic_epo = (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");
		String numOrden = (request.getParameter("numeroOrden") == null)?"":request.getParameter("numeroOrden");
		String tiposCredito	=	(request.getParameter("tiposCredito")==null)?"":request.getParameter("tiposCredito");	
		String fchinicial = (request.getParameter("Txtfchini") == null)?"":request.getParameter("Txtfchini");
		String fchfinal = (request.getParameter("Txtffin") == null)?"":request.getParameter("Txtffin");		
		String cgTipoConversion	=	(request.getParameter("tipo_conversion")==null)?"":request.getParameter("tipo_conversion");	
		String tipoCreditoXepo		=	(request.getParameter("tipoCreditoXepo")==null)?"":request.getParameter("tipoCreditoXepo");
		if(tiposCredito.equals("")){
			tiposCredito = this.getTiposCredito();
		}
		if(tipoCreditoXepo.equals("")){
			tipoCreditoXepo = this.getTipoCreditoXepo();
		}
		try {

		   
			if(!ic_epo.equals(""))	 
	 			condicion.append("AND D.IC_EPO =  " + ic_epo);
			
			if (!fchinicial.equals("") && !fchfinal.equals(""))
				condicion.append(" AND trunc(C3.df_fecha_hora) BETWEEN TO_DATE('"+fchinicial+"','DD/MM/YYYY') and TO_DATE('"+fchfinal+"','DD/MM/YYYY') ");
			if(ic_epo.equals("") && ic_if.equals("") && fchinicial.equals("") && fchfinal.equals(""))	 
				condicion.append(" AND TO_CHAR(C3.df_fecha_hora,'DD/MM/YYYY')=TO_CHAR(SYSDATE,'DD/MM/YYYY') ");
			
		qryDM.append(
				" SELECT /*+ index (d CP_DIS_DOCUMENTO_PK) index (lcd CP_DIS_LINEA_CREDITO_DM) use_nl(p lcd iep ci d) */ " +
				" 	'' as CG_DESCRIPCION_BINS, P.cg_razon_social as DISTRIBUIDOR  " +
				" 	,E.cg_razon_social as EPO  " +
				" 	,D.ig_numero_docto  " +
				" 	,D.cc_acuse  " +
				" 	,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') as df_fecha_emision  " +
				" 	,TO_CHAR(D.df_carga,'dd/mm/yyyy') as df_fecha_publicacion  " +
				" 	,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc	  " +
				" 	,D.IG_PLAZO_DOCTO " +
				" 	,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as df_fecha_seleccion  " +
				" 	,M.cd_nombre as MONEDA  " +
				" 	,M.ic_moneda  " +   
				" 	,D.fn_monto  " +
				" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION  " +
				" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO  " +
				" 	,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) as MONTO_VALUADO  " +
				" 	,D.IG_PLAZO_DESCUENTO  " +
				" 	,D.FN_MONTO " +
				" 	,decode(D.ic_tipo_financiamiento,1,D.fn_porc_descuento,0) as fn_porc_descuento	  " +
				" 	,D.fn_monto*decode(D.ic_tipo_financiamiento,1,D.fn_porc_descuento,0)/100 as MONTO_DESCUENTO  " +
				" 	,ED.CD_DESCRIPCION AS ESTATUS  " +
				"	,ED.IC_ESTATUS_DOCTO AS IC_ESTATUS_DOCTO "+
				"	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE) AS REFERENCIA_INT" +
				" 	,DS.fn_valor_tasa AS VALOR_TASA  " +
				" 	,D.IG_PLAZO_CREDITO  " +   
				" 	,DS.fn_importe_recibir as MONTO_CREDITO  " +
				" 	,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO  " +
				" 	,TCI.ic_tipo_cobro_interes  " +
				" 	,TCI.cd_descripcion as tipo_cobro_interes  " +
				" 	,DS.fn_importe_interes as MONTO_INTERES  " +
				" 	,D.ic_documento  " +
				" 	,E.cg_razon_social as EPO  " +
				" 	, D.ic_documento " +
				" 	,CI.cg_razon_social as DIF  " +
				" 	,LCD.ic_moneda as moneda_linea " +
				"  ,'' as IC_ORDEN_ENVIADO"   +
				"  ,'' as NUMERO_TC"   +
				"  ,'' as IC_OPERACION_CCACUSE"   +
				"  ,'' AS codigo_autorizado"   +
				"  ,'' as FECHA_REGISTRO"   +
				" 	,TF.CD_DESCRIPCION AS MODO_PLAZO, 'AvNotifPymeDist::getDocumentQueryFile' " +
				" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				"  ,  (SELECT nvl(fn_valor_aforo, 0)     FROM dis_aforo_epo   WHERE ic_if = ci.ic_if      AND ic_epo = d.ic_epo        AND ic_moneda = d.ic_moneda) AS POR_AFRO  "+				
				" , D.IG_TIPO_PAGO " +
				" FROM DIS_DOCUMENTO D  " +
				" 	,DIS_SOLICITUD S " +
				" 	,COMREL_PRODUCTO_EPO PE  " +
				" 	,COM_TIPO_CAMBIO TC  " +
				" 	,DIS_LINEA_CREDITO_DM LCD  " +
				" 	,COM_ACUSE3 C3 " +
				" 	,COMREL_IF_EPO_X_PRODUCTO IEP  " +
				" 	,DIS_DOCTO_SELECCIONADO DS  " +
				" 	,COMCAT_PYME P  " +
				" 	,COMCAT_EPO E  " +
				"  ,COMCAT_IF CI "   +
				" 	,COMCAT_TIPO_COBRO_INTERES TCI  " +
				" 	,COMCAT_TASA CT  " +
				" 	,COMCAT_ESTATUS_DOCTO ED  " +
				" 	,COMCAT_PRODUCTO_NAFIN PN  " +
				" 	,COMCAT_MONEDA M  " +
				" 	,COMCAT_TIPO_FINANCIAMIENTO TF " +
				" WHERE D.IC_EPO = PE.IC_EPO  " +
				" 	AND D.IC_PYME = P.IC_PYME  " +
				" 	AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO  " +
				" 	AND D.IC_MONEDA = M.IC_MONEDA  " +
				" 	AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM  " +
				" 	AND D.IC_EPO = E.IC_EPO  " +
				" 	AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO " +
				" 	AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO  " +
				" 	AND D.IC_EPO = IEP.IC_EPO  " +
				" 	AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO " +
				" 	AND DS.IC_TASA = CT.IC_TASA  " +
				" 	AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN  " +
				" 	AND S.CC_ACUSE = C3.CC_ACUSE  " +
				" 	AND TC.IC_MONEDA = M.IC_MONEDA  " +
				" 	AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)  " +
				" 	AND LCD.IC_IF = IEP.IC_IF  " +
				" 	AND LCD.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES " +
				" 	AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN  " +
				" 	AND LCD.IC_IF = CI.IC_IF  " +
				" 	AND LCD.IC_LINEA_CREDITO_DM IS NOT NULL " +
				" 	AND PN.IC_PRODUCTO_NAFIN = 4  " +
				" 	AND E.CS_HABILITADO = 'S'  " +
				" 	AND D.IC_PYME = " + ic_pyme + " " + condicion);
			if(!ic_if.equals("")){	 
					qryDM.append("   AND LCD.IC_IF =  " + ic_if);
				}

			qryDM.append("AND D.ic_estatus_docto in(4,11,32)");	
				
		qryCCC.append(
				" SELECT /*+ index (d CP_DIS_DOCUMENTO_PK) index (lcd CP_DIS_LINEA_CREDITO_DM) */ " +
				" 	cbif.CG_CODIGO_BIN||' ' ||cbif.cg_descripcion as CG_DESCRIPCION_BINS, P.cg_razon_social as DISTRIBUIDOR " +
				" 	,E.cg_razon_social as EPO " +
				" 	,D.ig_numero_docto " +
				" 	,D.cc_acuse " +
				" 	,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') as df_fecha_emision " +
				" 	,TO_CHAR(D.df_carga,'dd/mm/yyyy') as df_fecha_publicacion " +
				" 	,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc " +
				" 	,D.IG_PLAZO_DOCTO " +
				" 	,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as df_fecha_seleccion " +
				" 	,M.cd_nombre as MONEDA " +
				" 	,M.ic_moneda " +
				" 	,D.fn_monto " +
				" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION " +
				" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO " +
				" 	,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) as MONTO_VALUADO " +
				" 	,D.IG_PLAZO_DESCUENTO " +
				" 	,D.FN_MONTO " +
				" 	,decode(D.ic_tipo_financiamiento,1,D.fn_porc_descuento,0) as fn_porc_descuento " +
				" 	,D.fn_monto*decode(D.ic_tipo_financiamiento,1,D.fn_porc_descuento,0)/100 as MONTO_DESCUENTO " +
				" 	,ED.CD_DESCRIPCION AS ESTATUS " +
				"	,ED.IC_ESTATUS_DOCTO AS IC_ESTATUS_DOCTO "+
				" 	,CT.CD_NOMBRE AS REFERENCIA_INT " +
				" 	,DECODE(DS.CG_REL_MAT " +
				" 			,'+',DS.fn_valor_tasa + DS.fn_puntos " +
				" 			,'-',DS.fn_valor_tasa - DS.fn_puntos " +
				" 			,'*',DS.fn_valor_tasa * DS.fn_puntos " +
				" 			,'/',DS.fn_valor_tasa / DS.fn_puntos " +
				" 			,0) AS VALOR_TASA " +
				" 	,D.IG_PLAZO_CREDITO " +
				" 	,DS.fn_importe_recibir as MONTO_CREDITO " +
				" 	,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO " +
				" 	,TCI.ic_tipo_cobro_interes " +
				" 		,TCI.cd_descripcion as tipo_cobro_interes " +
				" 	,DS.fn_importe_interes as MONTO_INTERES " +
				" 	,D.ic_documento " +
				" 	,E.cg_razon_social as EPO " +
				" 	, D.ic_documento " +
				"  ,decode(ed.ic_estatus_docto,32,ci2.cg_razon_social,ci.cg_razon_social) AS dif"   +
				" 	,LCD.ic_moneda as moneda_linea " +
				"  ,DDPTC.IC_ORDEN_PAGO_TC as IC_ORDEN_ENVIADO"   +
				"  ,DDPTC.CG_NUM_TARJETA as NUMERO_TC"   +
				"  ,DDPTC.CG_TRANSACCION_ID as IC_OPERACION_CCACUSE"   +
				"  ,decode(CONCAT(ddptc.CG_CODIGO_RESP ,ddptc.CG_CODIGO_DESC),'','',ddptc.CG_CODIGO_RESP||'-' ||ddptc.CG_CODIGO_DESC)AS codigo_autorizado"   +
				"  ,TO_CHAR(DDPTC.DF_FECHA_AUTORIZACION,'dd/mm/yyyy') as FECHA_REGISTRO"   +
				" 	,TF.CD_DESCRIPCION AS MODO_PLAZO,  'AvNotifPymeDist::getDocumentQueryFile' " +
				" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				"  ,  (SELECT nvl(fn_valor_aforo, 0)     FROM dis_aforo_epo   WHERE ic_if = ci.ic_if      AND ic_epo = d.ic_epo        AND ic_moneda = d.ic_moneda) AS POR_AFRO  "+				
				" , D.IG_TIPO_PAGO " +
				" FROM DIS_DOCUMENTO D " +
				" 	,COM_BINS_IF cbif " +//////////777
				"  ,DIS_DOCTOS_PAGO_TC DDPTC"   +/////////////77
				" 	,COMCAT_PYME P " +
				" 	,COMREL_PRODUCTO_EPO PE " +
				" 	,COMCAT_PRODUCTO_NAFIN PN " +
				" 	,COMCAT_MONEDA M " +
				" 	,COM_TIPO_CAMBIO TC " +
				" 	,COM_LINEA_CREDITO LCD " +
				" 	,COMCAT_TIPO_COBRO_INTERES TCI " +
				" 	,COMREL_IF_EPO_X_PRODUCTO IEP " +
				" 	,COMCAT_TASA CT " +
				" 	,COMCAT_EPO E " +
				" 	,DIS_DOCTO_SELECCIONADO DS " +
				" 	,COMCAT_ESTATUS_DOCTO ED " +
				"  ,COMCAT_IF CI "   +
				"	,comcat_if ci2 "+/////////////////agregados 
				" 	,DIS_SOLICITUD S " +
				" 	,COM_ACUSE3 C3 " +
				" 	,COMCAT_TIPO_FINANCIAMIENTO TF " +
				" WHERE D.IC_EPO = PE.IC_EPO " +
				" 	AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN " +
				"  AND D.IC_ORDEN_PAGO_TC = DDPTC.IC_ORDEN_PAGO_TC(+)"   +//////////////7
				" 	AND P.IC_PYME = D.IC_PYME " +
				" 	AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO " +
				" 	AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO " +
				" 	AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO " +
				" 	AND D.IC_EPO = PE.IC_EPO  " +
				" 	AND D.IC_MONEDA = M.IC_MONEDA " +
				" 	AND M.IC_MONEDA = TC.IC_MONEDA " +
				" 	AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA) " +
				" 	AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO " +
				"   AND (LCD.IC_IF = IEP.IC_IF OR cbif.IC_IF = IEP.IC_IF) "   +//////////////
				" 	AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES " +
				" 	AND IEP.IC_EPO = D.IC_EPO " +
				" 	AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN " +
				" 	AND DS.IC_TASA = CT.IC_TASA " +
				" 	AND D.IC_EPO = E.IC_EPO " +
				" 	AND S.CC_ACUSE = C3.CC_ACUSE " +
				" 	AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO " +
				"  AND d.IC_BINS = cbif.IC_BINS(+)"  +////////////77
				"  AND cbif.IC_if = ci2.IC_if(+)"  +/////////////////7
				" 	AND LCD.IC_IF = CI.IC_IF " +
				" 	AND PN.IC_PRODUCTO_NAFIN = 4 " +
				" 	AND E.CS_HABILITADO = 'S' " +
				" 	AND D.IC_PYME = " + ic_pyme + " " + condicion);
			if(!numOrden.equals("")){
				qryCCC.append(" and ddptc.ic_orden_pago_tc ="+numOrden);
			}
			if(!ic_if.equals("")){	 
					qryCCC.append("   AND (LCD.IC_IF = "+ic_if+" OR cbif.IC_IF = "+ic_if+") ");
			}
			qryCCC.append("AND D.ic_estatus_docto in(4,11,32)");	
				
			qryFdR.append(
				" SELECT /*+ index (d CP_DIS_DOCUMENTO_PK) index (lcd CP_DIS_LINEA_CREDITO_DM) */ " +
				" 	'' as CG_DESCRIPCION_BINS, P.cg_razon_social as DISTRIBUIDOR " +
				" 	,E.cg_razon_social as EPO " +
				" 	,D.ig_numero_docto " +
				" 	,D.cc_acuse " +
				" 	,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') as df_fecha_emision " +
				" 	,TO_CHAR(D.df_carga,'dd/mm/yyyy') as df_fecha_publicacion " +
				" 	,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc " +
				" 	,D.IG_PLAZO_DOCTO " +
				" 	,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as df_fecha_seleccion " +
				" 	,M.cd_nombre as MONEDA " +
				" 	,M.ic_moneda " +
				" 	,D.fn_monto " +
				" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION " +
				" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO " +
				" 	,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) as MONTO_VALUADO " +
				" 	,D.IG_PLAZO_DESCUENTO " +
				" 	,D.FN_MONTO " +
				" 	,decode(D.ic_tipo_financiamiento,1,D.fn_porc_descuento,0) as fn_porc_descuento " +
				" 	,D.fn_monto*decode(D.ic_tipo_financiamiento,1,D.fn_porc_descuento,0)/100 as MONTO_DESCUENTO " +
				" 	,ED.CD_DESCRIPCION AS ESTATUS " +
				"	,ED.IC_ESTATUS_DOCTO AS IC_ESTATUS_DOCTO "+
				" 	,CT.CD_NOMBRE AS REFERENCIA_INT " +
				" 	,DECODE(DS.CG_REL_MAT " +
				" 			,'+',DS.fn_valor_tasa + DS.fn_puntos " +
				" 			,'-',DS.fn_valor_tasa - DS.fn_puntos " +
				" 			,'*',DS.fn_valor_tasa * DS.fn_puntos " +
				" 			,'/',DS.fn_valor_tasa / DS.fn_puntos " +
				" 			,0) AS VALOR_TASA " +
				" 	,D.IG_PLAZO_CREDITO " +
				" 	,DS.fn_importe_recibir as MONTO_CREDITO " +
				" 	,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO " +
				" 	,TCI.ic_tipo_cobro_interes " +
				" 		,TCI.cd_descripcion as tipo_cobro_interes " +
				" 	,DS.fn_importe_interes as MONTO_INTERES " +
				" 	,D.ic_documento " +
				" 	,E.cg_razon_social as EPO " +
				" 	, D.ic_documento " +
				" 	,CI.cg_razon_social as DIF  " +
				" 	,LCD.ic_moneda as moneda_linea " +
				"  ,'' as IC_ORDEN_ENVIADO"   +
				"  ,'' as NUMERO_TC"   +
				"  ,'' as IC_OPERACION_CCACUSE"   +
				"  ,'' AS codigo_autorizado"   +
				"  ,'' as FECHA_REGISTRO"   +
				" 	,'NA' AS MODO_PLAZO,  'AvNotifPymeDist::getDocumentQueryFile' " +
				" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				" ,lcd.ig_cuenta_bancaria as CG_NUMERO_CUENTA "+	
				"  ,  (SELECT nvl(fn_valor_aforo, 0)     FROM dis_aforo_epo   WHERE ic_if = ci.ic_if      AND ic_epo = d.ic_epo        AND ic_moneda = d.ic_moneda) AS POR_AFRO  "+				
				" , D.IG_TIPO_PAGO " +
				" FROM DIS_DOCUMENTO D " +
				" 	,COMCAT_PYME P " +
				" 	,COMREL_PRODUCTO_EPO PE " +
				" 	,COMCAT_PRODUCTO_NAFIN PN " +
				" 	,COMCAT_MONEDA M " +
				" 	,COM_TIPO_CAMBIO TC " +
				" 	,COM_LINEA_CREDITO LCD " +
				" 	,COMCAT_TIPO_COBRO_INTERES TCI " +
				" 	,COMREL_IF_EPO_X_PRODUCTO IEP " +
				" 	,COMCAT_TASA CT " +
				" 	,COMCAT_EPO E " +
				" 	,DIS_DOCTO_SELECCIONADO DS " +
				" 	,COMCAT_ESTATUS_DOCTO ED " +
				"  ,COMCAT_IF CI "   +
				" 	,DIS_SOLICITUD S " +
				" 	,COM_ACUSE3 C3 " +
				" 	,COMCAT_TIPO_FINANCIAMIENTO TF " +
				" WHERE D.IC_EPO = PE.IC_EPO " +
				" 	AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN " +
				" 	AND P.IC_PYME = D.IC_PYME " +
				" 	AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO " +
				" 	AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO " +
				" 	AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO(+) " +
				" 	AND D.IC_EPO = PE.IC_EPO  " +
				" 	AND D.IC_MONEDA = M.IC_MONEDA " +
				" 	AND M.IC_MONEDA = TC.IC_MONEDA " +
				" 	AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA) " +
				" 	AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO " +
				" 	AND LCD.IC_IF = IEP.IC_IF " +
				" 	AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES " +
				" 	AND IEP.IC_EPO = D.IC_EPO " +
				" 	AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN " +
				" 	AND DS.IC_TASA = CT.IC_TASA " +
				" 	AND D.IC_EPO = E.IC_EPO " +
				" 	AND S.CC_ACUSE = C3.CC_ACUSE " +
				" 	AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO " +
				" 	AND LCD.IC_IF = CI.IC_IF " +
				" 	AND PN.IC_PRODUCTO_NAFIN = 4 " +
				" 	AND E.CS_HABILITADO = 'S' " +
				"  and d.IC_TIPO_FINANCIAMIENTO is null "+
				"	AND LCD.cg_tipo_solicitud = 'I' "+
				"	AND LCD.cs_factoraje_con_rec = 'S' "+
				" 	AND D.IC_PYME = " + ic_pyme + " " + condicion);
			if(!ic_if.equals("")){	 
					qryFdR.append("   AND LCD.IC_IF =  " + ic_if);
				}

			qryFdR.append("AND D.ic_estatus_docto in(4,11)");	

			if("D".equals(tiposCredito))
				qrySentencia.append(qryDM.toString());
			else if("C".equals(tiposCredito))
				qrySentencia.append(qryCCC.toString());
			else  if("".equals(tiposCredito)  && tipoCreditoXepo.equals("F"))   				
				qrySentencia.append(qryFdR.toString());
			else
				qrySentencia.append(qryDM + " UNION  " + qryCCC);

			log.debug("El query getDocumentQueryFile : "+qrySentencia.toString());
			
		}catch(Exception e){
			log.error("AvNotifPymeDist::getDocumentQueryFile Exception "+e);
		}
		log.debug("getDocumentQueryFile(S)");
		return qrySentencia.toString();
	}
	
	 /**
	  * Funci�n que regresa que tipo de pago tiene la epo
	  * @throws java.lang.Exception
	  * @return regresa 'S' si la epo tiene tipo de pago TC o H y 'N' si tiene o no cualquier otro tipo de pago
	  * @param ic_epo  es la clave de la EPO
	  */
	 
	 public String tipoPago(String ic_epo) {
	    log.info(" tipoPago(E)");
	    String pago="";
	    AccesoDB    con = new AccesoDB();
	    PreparedStatement ps = null;
	    conditions     = new ArrayList();
	    ResultSet rs = null;
	    boolean     commit = true;
	    try{ 
	       con.conexionDB();
	       String query =
	          " select decode(CG_TIPO_PAGO,'TC','S','H','S','N') as cg_tipo_pago  "   +
	          "   from comrel_producto_epo"   +
	          "  where  ic_epo = ? "+
	          "    and  IC_PRODUCTO_NAFIN = 4";
	       conditions.add(ic_epo); 
	       log.debug(" query :: -> "+query);
	       log.debug(" conditions :: -> "+conditions);
	       ps = con.queryPrecompilado(query,conditions);
	       rs = ps.executeQuery();
	       if (rs.next()){
	          pago = rs.getString("CG_TIPO_PAGO");
	       }
	       rs.close();
	       con.cierraStatement();

	    }catch(Exception e){
	       commit = false;
	       pago ="N";
	       throw new AppException("Error al regresar el tipo de pago ", e);
	    }finally{
	       try{
	          if(rs!=null)rs.close();
	          if(ps!=null)ps.close();
	       }catch(Exception t){
	          log.error("tipoPago():: Error al cerrar Recursos: " + t.getMessage());
	       }
	       if(con.hayConexionAbierta()){
	          con.terminaTransaccion(commit);
	          con.cierraConexionDB();
	       }
	    }
	    log.info(" tipoPago(S) pago "+pago);
	    return pago;
	 }





	public String getCgTipoConversion() {
		return cgTipoConversion;
	}

	public void setCgTipoConversion(String cgTipoConversion) {
		this.cgTipoConversion = cgTipoConversion;
	}

	public String getTiposCredito() {
		return tiposCredito;
	}

	public void setTiposCredito(String tiposCredito) {
		this.tiposCredito = tiposCredito;
	}

	public String getTipoCreditoXepo() {
		return tipoCreditoXepo;
	}

	public void setTipoCreditoXepo(String tipoCreditoXepo) {
		this.tipoCreditoXepo = tipoCreditoXepo;
	}
	//numOrden
	/*public String getNumOrden() {
		return numOrden;
	}

	public void setNumOrden(String numOrden) {
		this.numOrden = numOrden;
	}*/

/*******************************************************************************
 *          Migraci�n. Implementa IQueryGeneratorRegExtJS.java                 *
 *******************************************************************************/
	private List conditions;
	private String valor;
	private String ic_pyme;
	private String ic_if;
	private String ic_epo;
	private String numOrden;
	private String tipos_Credito;
	private String fchinicial;
	private String fchfinal;
	private String cg_Tipo_Conversion;
	private String tipo_Credito_Xepo;
	private String responsable;
	private String noCliente;
	private String publicaDoctosFinanciables;

	public String getDocumentQuery() {
		return null;
	}

	public String getDocumentSummaryQueryForIds(List ids) {
		return null;
	}

	public String getAggregateCalculationQuery() {
		return null;
	}

/**
 * Se forma la consulta para generar los archivos pdf y csv
 * @return qrySentencia
 */
	public String getDocumentQueryFile() {

		log.info("getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer qryDM        = new StringBuffer();
		StringBuffer qryCCC       = new StringBuffer();
		StringBuffer qryFdR       = new StringBuffer();
		StringBuffer condicion    = new StringBuffer();
		conditions = new ArrayList();
/*
		if(!ic_epo.equals("")){
			condicion.append("AND D.IC_EPO = ? ");
			conditions.add(ic_epo);
		}
		if (!fchinicial.equals("") && !fchfinal.equals("")){
			condicion.append(" AND trunc(C3.df_fecha_hora) BETWEEN TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
			conditions.add(fchinicial);
			conditions.add(fchfinal);
		}
		if(ic_epo.equals("") && ic_if.equals("") && fchinicial.equals("") && fchfinal.equals("")){
			condicion.append(" AND TO_CHAR(C3.df_fecha_hora,'DD/MM/YYYY')=TO_CHAR(SYSDATE,'DD/MM/YYYY') ");
		}
*/
		if(!ic_epo.equals(""))	 
			condicion.append("AND D.IC_EPO =  " + ic_epo + " ");
		if (!fchinicial.equals("") && !fchfinal.equals(""))
			condicion.append(" AND trunc(C3.df_fecha_hora) BETWEEN TO_DATE('"+fchinicial+"','DD/MM/YYYY') and TO_DATE('"+fchfinal+"','DD/MM/YYYY') ");
		if(ic_epo.equals("") && ic_if.equals("") && fchinicial.equals("") && fchfinal.equals(""))
			condicion.append(" AND TO_CHAR(C3.df_fecha_hora,'DD/MM/YYYY')=TO_CHAR(SYSDATE,'DD/MM/YYYY') ");

		qryDM.append(
			" SELECT /*+ index (d CP_DIS_DOCUMENTO_PK) index (lcd CP_DIS_LINEA_CREDITO_DM) use_nl(p lcd iep ci d) */ " +
			"  '' as CG_DESCRIPCION_BINS, P.cg_razon_social as DISTRIBUIDOR  " +
			"  ,E.cg_razon_social as EPO  " +
			"  ,D.ig_numero_docto  " +
			"  ,D.cc_acuse  " +
			"  ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') as df_fecha_emision  " +
			"  ,TO_CHAR(D.df_carga,'dd/mm/yyyy') as df_fecha_publicacion  " +
			"  ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc    " +
			"  ,D.IG_PLAZO_DOCTO " +
			"  ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as df_fecha_seleccion  " +
			"  ,M.cd_nombre as MONEDA  " +
			"  ,M.ic_moneda  " +   
			"  ,D.fn_monto  " +
			"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION  " +
			"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO  " +
			"  ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) as MONTO_VALUADO  " +
			"  ,D.IG_PLAZO_DESCUENTO  " +
			"  ,D.FN_MONTO " +
			"  ,decode(D.ic_tipo_financiamiento,1,D.fn_porc_descuento,0) as fn_porc_descuento     " +
			"  ,D.fn_monto*decode(D.ic_tipo_financiamiento,1,D.fn_porc_descuento,0)/100 as MONTO_DESCUENTO  " +
			"  ,ED.CD_DESCRIPCION AS ESTATUS  " +
			"  ,ED.IC_ESTATUS_DOCTO AS IC_ESTATUS_DOCTO "+
			"  ,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE) AS REFERENCIA_INT" +
			"  ,DS.fn_valor_tasa AS VALOR_TASA  " +
			"  ,D.IG_PLAZO_CREDITO  " +   
			"  ,DS.fn_importe_recibir as MONTO_CREDITO  " +
			"  ,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO  " +
			"  ,TCI.ic_tipo_cobro_interes  " +
			"  ,TCI.cd_descripcion as tipo_cobro_interes  " +
			"  ,DS.fn_importe_interes as MONTO_INTERES  " +
			"  ,D.ic_documento  " +
			"  ,E.cg_razon_social as EPO  " +
			"  , D.ic_documento " +
			"  ,CI.cg_razon_social as DIF  " +
			"  ,LCD.ic_moneda as moneda_linea " +
			"  ,'' as IC_ORDEN_ENVIADO"   +
			"  ,'' as NUMERO_TC"   +
			"  ,'' as IC_OPERACION_CCACUSE"   +
			"  ,'' AS codigo_autorizado"   +
			"  ,'' as FECHA_REGISTRO"   +
			"  ,TF.CD_DESCRIPCION AS MODO_PLAZO, 'AvNotifPymeDist::getDocumentQueryFile' " +
			" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
			"  ,  (SELECT nvl(fn_valor_aforo, 0)     FROM dis_aforo_epo   WHERE ic_if = ci.ic_if      AND ic_epo = d.ic_epo        AND ic_moneda = d.ic_moneda) AS POR_AFRO  "+
			"  , D.IG_TIPO_PAGO " + 
			" FROM DIS_DOCUMENTO D  " +
			"  ,DIS_SOLICITUD S " +
			"  ,COMREL_PRODUCTO_EPO PE  " +
			"  ,COM_TIPO_CAMBIO TC  " +
			"  ,DIS_LINEA_CREDITO_DM LCD  " +
			"  ,COM_ACUSE3 C3 " +
			"  ,COMREL_IF_EPO_X_PRODUCTO IEP  " +
			"  ,DIS_DOCTO_SELECCIONADO DS  " +
			"  ,COMCAT_PYME P  " +
			"  ,COMCAT_EPO E  " +
			"  ,COMCAT_IF CI "   +
			"  ,COMCAT_TIPO_COBRO_INTERES TCI  " +
			"  ,COMCAT_TASA CT  " +
			"  ,COMCAT_ESTATUS_DOCTO ED  " +
			"  ,COMCAT_PRODUCTO_NAFIN PN  " +
			"  ,COMCAT_MONEDA M  " +
			"  ,COMCAT_TIPO_FINANCIAMIENTO TF " +
			" WHERE D.IC_EPO = PE.IC_EPO  " +
			"  AND D.IC_PYME = P.IC_PYME  " +
			"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO  " +
			"  AND D.IC_MONEDA = M.IC_MONEDA  " +
			"  AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM  " +
			"  AND D.IC_EPO = E.IC_EPO  " +
			"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO " +
			"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO  " +
			"  AND D.IC_EPO = IEP.IC_EPO  " +
			"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO " +
			"  AND DS.IC_TASA = CT.IC_TASA  " +
			"  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN  " +
			"  AND S.CC_ACUSE = C3.CC_ACUSE  " +
			"  AND TC.IC_MONEDA = M.IC_MONEDA  " +
			"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)  " +
			"  AND LCD.IC_IF = IEP.IC_IF  " +
			"  AND LCD.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES " +
			"  AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN  " +
			"  AND LCD.IC_IF = CI.IC_IF  " +
			"  AND LCD.IC_LINEA_CREDITO_DM IS NOT NULL " +
			"  AND PN.IC_PRODUCTO_NAFIN = 4  " +
			"  AND E.CS_HABILITADO = 'S'  " +
			"  AND D.IC_PYME = " + ic_pyme + " " + condicion);
			if(!ic_if.equals("")){
				qryDM.append("   AND LCD.IC_IF =  " + ic_if);
			}

			qryDM.append("AND D.ic_estatus_docto in(4,11,32)");

			qryCCC.append(
				" SELECT /*+ index (d CP_DIS_DOCUMENTO_PK) index (lcd CP_DIS_LINEA_CREDITO_DM) */ " +
				"  cbif.CG_CODIGO_BIN||' ' ||cbif.cg_descripcion as CG_DESCRIPCION_BINS, P.cg_razon_social as DISTRIBUIDOR " +
				"  ,E.cg_razon_social as EPO " +
				"  ,D.ig_numero_docto " +
				"  ,D.cc_acuse " +
				"  ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') as df_fecha_emision " +
				"  ,TO_CHAR(D.df_carga,'dd/mm/yyyy') as df_fecha_publicacion " +
				"  ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc " +
				"  ,D.IG_PLAZO_DOCTO " +
				"  ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as df_fecha_seleccion " +
				"  ,M.cd_nombre as MONEDA " +
				"  ,M.ic_moneda " +
				"  ,D.fn_monto " +
				"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION " +
				"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO " +
				"  ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) as MONTO_VALUADO " +
				"  ,D.IG_PLAZO_DESCUENTO " +
				"  ,D.FN_MONTO " +
				"  ,decode(D.ic_tipo_financiamiento,1,D.fn_porc_descuento,0) as fn_porc_descuento " +
				"  ,D.fn_monto*decode(D.ic_tipo_financiamiento,1,D.fn_porc_descuento,0)/100 as MONTO_DESCUENTO " +
				"  ,ED.CD_DESCRIPCION AS ESTATUS " +
				"  ,ED.IC_ESTATUS_DOCTO AS IC_ESTATUS_DOCTO "+
				"  ,CT.CD_NOMBRE AS REFERENCIA_INT " +
				"  ,DECODE(DS.CG_REL_MAT " +
				"        ,'+',DS.fn_valor_tasa + DS.fn_puntos " +
				"        ,'-',DS.fn_valor_tasa - DS.fn_puntos " +
				"        ,'*',DS.fn_valor_tasa * DS.fn_puntos " +
				"        ,'/',DS.fn_valor_tasa / DS.fn_puntos " +
				"        ,0) AS VALOR_TASA " +
				"  ,D.IG_PLAZO_CREDITO " +
				"  ,DS.fn_importe_recibir as MONTO_CREDITO " +
				"  ,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO " +
				"  ,TCI.ic_tipo_cobro_interes " +
				"     ,TCI.cd_descripcion as tipo_cobro_interes " +
				"  ,DS.fn_importe_interes as MONTO_INTERES " +
				"  ,D.ic_documento " +
				"  ,E.cg_razon_social as EPO " +
				"  , D.ic_documento " +
				"  ,decode(ed.ic_estatus_docto,32,ci2.cg_razon_social,ci.cg_razon_social) AS dif"   +
				"  ,LCD.ic_moneda as moneda_linea " +
				"  ,DDPTC.IC_ORDEN_PAGO_TC as IC_ORDEN_ENVIADO"   +
				"  ,DDPTC.CG_NUM_TARJETA as NUMERO_TC"   +
				"  ,DDPTC.CG_TRANSACCION_ID as IC_OPERACION_CCACUSE"   +
				"  ,decode(CONCAT(ddptc.CG_CODIGO_RESP ,ddptc.CG_CODIGO_DESC),'','',ddptc.CG_CODIGO_RESP||'-' ||ddptc.CG_CODIGO_DESC)AS codigo_autorizado"   +
				"  ,TO_CHAR(DDPTC.DF_FECHA_AUTORIZACION,'dd/mm/yyyy') as FECHA_REGISTRO"   +
				"  ,TF.CD_DESCRIPCION AS MODO_PLAZO,  'AvNotifPymeDist::getDocumentQueryFile' " +
				" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				"  ,  (SELECT nvl(fn_valor_aforo, 0)     FROM dis_aforo_epo   WHERE ic_if = ci.ic_if      AND ic_epo = d.ic_epo        AND ic_moneda = d.ic_moneda) AS POR_AFRO  "+
				"  , D.IG_TIPO_PAGO " + 
				" FROM DIS_DOCUMENTO D " +
				"  ,COM_BINS_IF cbif " +
				"  ,DIS_DOCTOS_PAGO_TC DDPTC"   +
				"  ,COMCAT_PYME P " +
				"  ,COMREL_PRODUCTO_EPO PE " +
				"  ,COMCAT_PRODUCTO_NAFIN PN " +
				"  ,COMCAT_MONEDA M " +
				"  ,COM_TIPO_CAMBIO TC " +
				"  ,COM_LINEA_CREDITO LCD " +
				"  ,COMCAT_TIPO_COBRO_INTERES TCI " +
				"  ,COMREL_IF_EPO_X_PRODUCTO IEP " +
				"  ,COMCAT_TASA CT " +
				"  ,COMCAT_EPO E " +
				"  ,DIS_DOCTO_SELECCIONADO DS " +
				"  ,COMCAT_ESTATUS_DOCTO ED " +
				"  ,COMCAT_IF CI "   +
				"  ,comcat_if ci2 "+
				"  ,DIS_SOLICITUD S " +
				"  ,COM_ACUSE3 C3 " +
				"  ,COMCAT_TIPO_FINANCIAMIENTO TF " +
				" WHERE D.IC_EPO = PE.IC_EPO " +
				"  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN " +
				"  AND D.IC_ORDEN_PAGO_TC = DDPTC.IC_ORDEN_PAGO_TC(+)"   +
				"  AND P.IC_PYME = D.IC_PYME " +
				"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO " +
				"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO " +
				"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO " +
				"  AND D.IC_EPO = PE.IC_EPO  " +
				"  AND D.IC_MONEDA = M.IC_MONEDA " +
				"  AND M.IC_MONEDA = TC.IC_MONEDA " +
				"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA) " +
				"  AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO " +
				"   AND (LCD.IC_IF = IEP.IC_IF OR cbif.IC_IF = IEP.IC_IF) "   +
				"  AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES " +
				"  AND IEP.IC_EPO = D.IC_EPO " +
				"  AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN " +
				"  AND DS.IC_TASA = CT.IC_TASA " +
				"  AND D.IC_EPO = E.IC_EPO " +
				"  AND S.CC_ACUSE = C3.CC_ACUSE " +
				"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO " +
				"  AND d.IC_BINS = cbif.IC_BINS(+)"  +
				"  AND cbif.IC_if = ci2.IC_if(+)"  +
				"  AND LCD.IC_IF = CI.IC_IF " +
				"  AND PN.IC_PRODUCTO_NAFIN = 4 " +
				"  AND E.CS_HABILITADO = 'S' " +
				"  AND D.IC_PYME = " + ic_pyme + " " + condicion);
			if(!numOrden.equals("")){
				qryCCC.append(" and ddptc.ic_orden_pago_tc ="+numOrden);
			}
			if(!ic_if.equals("")){   
				qryCCC.append("   AND (LCD.IC_IF = "+ic_if+" OR cbif.IC_IF = "+ic_if+") ");
			}
			qryCCC.append("AND D.ic_estatus_docto in(4,11,32)");  

			qryFdR.append(
				" SELECT /*+ index (d CP_DIS_DOCUMENTO_PK) index (lcd CP_DIS_LINEA_CREDITO_DM) */ " +
				"  '' as CG_DESCRIPCION_BINS, P.cg_razon_social as DISTRIBUIDOR " +
				"  ,E.cg_razon_social as EPO " +
				"  ,D.ig_numero_docto " +
				"  ,D.cc_acuse " +
				"  ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') as df_fecha_emision " +
				"  ,TO_CHAR(D.df_carga,'dd/mm/yyyy') as df_fecha_publicacion " +
				"  ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') as df_fecha_venc " +
				"  ,D.IG_PLAZO_DOCTO " +
				"  ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') as df_fecha_seleccion " +
				"  ,M.cd_nombre as MONEDA " +
				"  ,M.ic_moneda " +
				"  ,D.fn_monto " +
				"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION " +
				"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO " +
				"  ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) as MONTO_VALUADO " +
				"  ,D.IG_PLAZO_DESCUENTO " +
				"  ,D.FN_MONTO " +
				"  ,decode(D.ic_tipo_financiamiento,1,D.fn_porc_descuento,0) as fn_porc_descuento " +
				"  ,D.fn_monto*decode(D.ic_tipo_financiamiento,1,D.fn_porc_descuento,0)/100 as MONTO_DESCUENTO " +
				"  ,ED.CD_DESCRIPCION AS ESTATUS " +
				"  ,ED.IC_ESTATUS_DOCTO AS IC_ESTATUS_DOCTO "+
				"  ,CT.CD_NOMBRE AS REFERENCIA_INT " +
				"  ,DECODE(DS.CG_REL_MAT " +
				"        ,'+',DS.fn_valor_tasa + DS.fn_puntos " +
				"        ,'-',DS.fn_valor_tasa - DS.fn_puntos " +
				"        ,'*',DS.fn_valor_tasa * DS.fn_puntos " +
				"        ,'/',DS.fn_valor_tasa / DS.fn_puntos " +
				"        ,0) AS VALOR_TASA " +
				"  ,D.IG_PLAZO_CREDITO " +
				"  ,DS.fn_importe_recibir as MONTO_CREDITO " +
				"  ,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as DF_FECHA_VENC_CREDITO " +
				"  ,TCI.ic_tipo_cobro_interes " +
				"     ,TCI.cd_descripcion as tipo_cobro_interes " +
				"  ,DS.fn_importe_interes as MONTO_INTERES " +
				"  ,D.ic_documento " +
				"  ,E.cg_razon_social as EPO " +
				"  , D.ic_documento " +
				"  ,CI.cg_razon_social as DIF  " +
				"  ,LCD.ic_moneda as moneda_linea " +
				"  ,'' as IC_ORDEN_ENVIADO"   +
				"  ,'' as NUMERO_TC"   +
				"  ,'' as IC_OPERACION_CCACUSE"   +
				"  ,'' AS codigo_autorizado"   +
				"  ,'' as FECHA_REGISTRO"   +
				"  ,'NA' AS MODO_PLAZO,  'AvNotifPymeDist::getDocumentQueryFile' " +
				" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				" ,lcd.ig_cuenta_bancaria as CG_NUMERO_CUENTA "+   
				"  ,  (SELECT nvl(fn_valor_aforo, 0)     FROM dis_aforo_epo   WHERE ic_if = ci.ic_if      AND ic_epo = d.ic_epo        AND ic_moneda = d.ic_moneda) AS POR_AFRO  "+
				"  , D.IG_TIPO_PAGO " + 
				" FROM DIS_DOCUMENTO D " +
				"  ,COMCAT_PYME P " +
				"  ,COMREL_PRODUCTO_EPO PE " +
				"  ,COMCAT_PRODUCTO_NAFIN PN " +
				"  ,COMCAT_MONEDA M " +
				"  ,COM_TIPO_CAMBIO TC " +
				"  ,COM_LINEA_CREDITO LCD " +
				"  ,COMCAT_TIPO_COBRO_INTERES TCI " +
				"  ,COMREL_IF_EPO_X_PRODUCTO IEP " +
				"  ,COMCAT_TASA CT " +
				"  ,COMCAT_EPO E " +
				"  ,DIS_DOCTO_SELECCIONADO DS " +
				"  ,COMCAT_ESTATUS_DOCTO ED " +
				"  ,COMCAT_IF CI "   +
				"  ,DIS_SOLICITUD S " +
				"  ,COM_ACUSE3 C3 " +
				"  ,COMCAT_TIPO_FINANCIAMIENTO TF " +
				" WHERE D.IC_EPO = PE.IC_EPO " +
				"  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN " +
				"  AND P.IC_PYME = D.IC_PYME " +
				"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO " +
				"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO " +
				"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO(+) " +
				"  AND D.IC_EPO = PE.IC_EPO  " +
				"  AND D.IC_MONEDA = M.IC_MONEDA " +
				"  AND M.IC_MONEDA = TC.IC_MONEDA " +
				"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA) " +
				"  AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO " +
				"  AND LCD.IC_IF = IEP.IC_IF " +
				"  AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES " +
				"  AND IEP.IC_EPO = D.IC_EPO " +
				"  AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN " +
				"  AND DS.IC_TASA = CT.IC_TASA " +
				"  AND D.IC_EPO = E.IC_EPO " +
				"  AND S.CC_ACUSE = C3.CC_ACUSE " +
				"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO " +
				"  AND LCD.IC_IF = CI.IC_IF " +
				"  AND PN.IC_PRODUCTO_NAFIN = 4 " +
				"  AND E.CS_HABILITADO = 'S' " +
				"  and d.IC_TIPO_FINANCIAMIENTO is null "+
				"  AND LCD.cg_tipo_solicitud = 'I' "+
				"  AND LCD.cs_factoraje_con_rec = 'S' "+
				"  AND D.IC_PYME = " + ic_pyme + " " + condicion);
			if(!ic_if.equals("")){
				qryFdR.append("   AND LCD.IC_IF =  " + ic_if);
			}

			qryFdR.append("AND D.ic_estatus_docto in(4,11)");

			if("D".equals(tipos_Credito))
				qrySentencia.append(qryDM.toString());
			else if("C".equals(tipos_Credito))
				qrySentencia.append(qryCCC.toString());
			else  if("".equals(tipos_Credito) && tipo_Credito_Xepo.equals("F"))
				qrySentencia.append(qryFdR.toString());
			else
				qrySentencia.append(qryDM + " UNION  " + qryCCC);

		log.info("qrySentencia: " + qrySentencia.toString());
		log.info("conditions: " + conditions.toString());
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();
	}


	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo) {

		log.info("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		String epo               = "";
		String numdocto          = "";
		String acuse             = "";
		String fchemision        = "";
		String fchpublica        = "";
		String fchvence          = "";
		String plazodocto        = "";
		String moneda            = "";
		String ic_moneda         = "";
		String tipoconversion    = "";
		String tipocambio        = "";
		String plzodescto        = "";
		String porcdescto        = "";
		String estatus           = "";
		String ic_estatus        = "";
		String numcredito        = "";
		String descif            = "";
		String referencia        = "";
		String plazocred         = "";
		String fchvenccred       = "";
		String tipocobroint      = "";
		String valortaza         = "";
		String fchopera          = "";
		String monedaLinea       = "";
		String modoPlazo         = "";
		String bandeVentaCartera = "";
		String cuenta_bancaria   = "";
		String numero_tc         = "";
		String ordenEnvio        = "";
		String opAcuse           = "";
		String codAutorizado     = "";
		String fechRegistro      = "";
		String descrip_bins      = "";
		String tipoPago          = "";
                String operaContrato     = "";
		double monto             = 0;
		double mntovaluado       = 0;
		double mntocredito       = 0;
		double montoint          = 0;
		double mntodescuento     = 0;
		int numreg               = 0;
		int numCols              = 22;

		if(tipo.equals("PDF")){
			try{

				ParametrosDist BeanParametro = ServiceLocator.getInstance().lookup("ParametrosDistEJB", ParametrosDist.class);
				if(!ic_epo.equals("")){
					responsable = BeanParametro.obtieneResponsable(ic_epo);
					cg_Tipo_Conversion =  BeanParametro. obtieneTipoConversion(noCliente);
					tipo_Credito_Xepo =  BeanParametro.obtieneTipoCredito (ic_epo);
					tipos_Credito = BeanParametro.getTiposCredito(noCliente, ic_epo);
				        operaContrato = BeanParametro.obtieneOperaSinCesion(ic_epo); //PARAMETRO NUEVO JAGE 14012017
				}

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				
                                String leyenda = "";
			                   if(operaContrato.equals("S")){
			                          leyenda = "En este acto manifiesto mi aceptaci�n para que sea(n) financiado(s) el(los) DOCUMENTO(S) que he seleccionado, mismo(s) que contiene(n) "+
                                                            "cuentas pendientes de pago a mi cargo y a favor de la EMPRESA DE PRIMER ORDEN por lo que en esta misma fecha me obligo a cubrir al "+
                                                            "INTERMEDIARIO FINANCIERO los intereses que se detallan en esta pantalla. Manifiesto tambi�n mi obligaci�n y aceptaci�n de pagar el 100% del "+
                                                            "valor de(los) DOCUMENTO(S) en la fecha de vencimiento al INTERMEDIARIO FINANCIERO.";
			                      }
                            leyenda = leyenda +="De conformidad con lo dispuesto en los art�culos 32 C del C�digo Fiscal o 2038 y 2041 del C�digo Civil Federal, me doy por notificado de la Cesi�n de derechos de las operaciones descritas en esta p�gina y para los efectos legales conducentes.\n" + 
                            "\n" + 
                            "Por otra parte, a partir del 17 de octubre de 2018 la EMPRESA DE PRIMER ORDEN ha manifestado bajo protesta de decir verdad, que s� ha emitido o emitir� a su CLIENTE o DISTRIBUIDOR el CFDI por la operaci�n comercial que le dio origen a esta transacci�n, seg�n sea el caso y conforme establezcan las disposiciones fiscales vigentes.\n";
                                pdfDoc.addText(leyenda,"formas",ComunesPDF.LEFT);
			    
                                
                                pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

				while(rs.next()){

					numreg++;
					descif        = (rs.getString("DIF")==null)?"":rs.getString("DIF");
					ic_moneda     = (rs.getString("ic_moneda")==null)?"":rs.getString("ic_moneda");
					epo           = (rs.getString("EPO")==null)?"":rs.getString("EPO");
					numdocto      = (rs.getString("ig_numero_docto")==null)?"":rs.getString("ig_numero_docto");
					acuse         = (rs.getString("cc_acuse")==null)?"":rs.getString("cc_acuse");
					fchemision    = (rs.getString("df_fecha_emision")==null)?"":rs.getString("df_fecha_emision");
					fchpublica    = (rs.getString("df_fecha_publicacion")==null)?"":rs.getString("df_fecha_publicacion");
					fchvence      = (rs.getString("df_fecha_venc")==null)?"":rs.getString("df_fecha_venc");
					plazodocto    = (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");
					moneda        = (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
					monto         = rs.getDouble("FN_MONTO");
					tipoconversion= (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");
					tipocambio    = (rs.getString("TIPO_CAMBIO")==null)?"":rs.getString("TIPO_CAMBIO");
					plzodescto    = (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
					porcdescto    = (rs.getString("fn_porc_descuento")==null)?"":rs.getString("fn_porc_descuento");
					mntodescuento = monto*(Double.parseDouble(porcdescto)/100);
					mntovaluado   = (monto-mntodescuento)*Double.parseDouble(tipocambio);
					estatus       = (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");
					ic_estatus    = (rs.getString("IC_ESTATUS_DOCTO")==null)?"":rs.getString("IC_ESTATUS_DOCTO");
					ordenEnvio    = (rs.getString("IC_ORDEN_ENVIADO")==null)?"":rs.getString("IC_ORDEN_ENVIADO");
					opAcuse       = (rs.getString("IC_OPERACION_CCACUSE")==null)?"":rs.getString("IC_OPERACION_CCACUSE");
					codAutorizado = (rs.getString("CODIGO_AUTORIZADO")==null)?"":rs.getString("CODIGO_AUTORIZADO");
					fechRegistro  = (rs.getString("FECHA_REGISTRO")==null)?"":rs.getString("FECHA_REGISTRO");
					descrip_bins  = (rs.getString("CG_DESCRIPCION_BINS")==null)?"":rs.getString("CG_DESCRIPCION_BINS");
					numero_tc     = (rs.getString("NUMERO_TC")==null)?"":"XXXX-XXXX-XXXX-"+rs.getString("NUMERO_TC");

					/***** PARA CREDITO *****/
					numcredito  = (rs.getString("ic_documento")==null)?"":rs.getString("ic_documento");
					monedaLinea = (rs.getString("moneda_linea")==null)?"":rs.getString("moneda_linea");
					modoPlazo   = (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
					if(responsable.equals("D")){
						numcredito   = (rs.getString("ic_documento")==null)?"":rs.getString("ic_documento");
						referencia   = (rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT");
						plazocred    = (rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO");
						fchvenccred  = (rs.getString("DF_FECHA_VENC_CREDITO")==null)?"":rs.getString("DF_FECHA_VENC_CREDITO");
						tipocobroint = (rs.getString("tipo_cobro_interes")==null)?"":rs.getString("tipo_cobro_interes");
						montoint     = rs.getDouble("MONTO_INTERES"); 
						valortaza    = (rs.getString("VALOR_TASA")==null)?"":rs.getString("VALOR_TASA");
						mntocredito  = rs.getDouble("MONTO_CREDITO");
						fchopera     = (rs.getString("df_fecha_seleccion")==null)?"":rs.getString("df_fecha_seleccion");
					}
					bandeVentaCartera = (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
					if(tipo_Credito_Xepo.equals("F")) {
						cuenta_bancaria = (rs.getString("CG_NUMERO_CUENTA")==null)?"":rs.getString("CG_NUMERO_CUENTA");
					}
					String porc_Aforo = (rs.getString("POR_AFRO")==null)?"0":rs.getString("POR_AFRO");
					BigDecimal por_Aforo= new BigDecimal(porc_Aforo);
					BigDecimal montoD= new BigDecimal(monto);
					BigDecimal MontoDesconta = por_Aforo.multiply(montoD);
					tipoPago = (rs.getString("IG_TIPO_PAGO")==null)?"":rs.getString("IG_TIPO_PAGO");
					if(tipoPago.equals("1")){
						tipoPago = "Financiamiento con intereses";
					} else if(tipoPago.equals("2")){
						tipoPago = "Meses sin intereses";
					}
					if (bandeVentaCartera.equals("S") ) {
						modoPlazo ="";
					}

					if(numreg==1) { //Es primer registro:
						if(!"".equals(cg_Tipo_Conversion))
							numCols+=3;
						if(responsable.equals("D")) {
							numCols++;
						}
						if(tipo_Credito_Xepo.equals("F")) {
							numCols++;
						}

						int colMonto = 10;
						if(!responsable.equals("D"))
							colMonto++;
						if(publicaDoctosFinanciables.equals("S")){
							numCols++;
						}
						pdfDoc.setLTable(numCols,100);
						pdfDoc.setLCell("Datos del Documento Inicial","celda01rep",ComunesPDF.CENTER,numCols);
						if(responsable.equals("D"))
							pdfDoc.setLCell("A","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("IF","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("EPO","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("No. Docto Inicial","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("No. Acuse Carga","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha Emisi�n","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha Pub.","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha Venc.","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Plazo docto.","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("% de Descuento Aforo","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Plazo Dscto D�as","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("% Dscto","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto % Dscto","celda01",ComunesPDF.CENTER);
						if(!"".equals(cg_Tipo_Conversion)) {
							pdfDoc.setLCell("Tipo conv.","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Tipo cambio","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Monto valuado","celda01",ComunesPDF.CENTER);
						}
						pdfDoc.setLCell("Modo Plazo","celda01",ComunesPDF.CENTER);
						if(publicaDoctosFinanciables.equals("S")){
							pdfDoc.setLCell("Tipo de pago","celda01",ComunesPDF.CENTER);
						}
						pdfDoc.setLCell("Estatus","celda01",ComunesPDF.CENTER);
						if(!responsable.equals("D")){
							pdfDoc.setLCell("Nombre del Producto","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("ID Orden enviado","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Respuesta de Operaci�n","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("C�digo Autorizaci�n","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("N�mero Tarjeta de Cr�dito","celda01",ComunesPDF.CENTER);
						} else if(responsable.equals("D")){
							pdfDoc.setLCell("","celda01rep",ComunesPDF.CENTER,5);
							pdfDoc.setLCell("Datos Documento Final","celda01rep",ComunesPDF.CENTER,numCols);
							pdfDoc.setLCell("B","celda01",ComunesPDF.CENTER);
							if(tipo_Credito_Xepo.equals("F")) {
								pdfDoc.setLCell("Tipo Cred","celda01",ComunesPDF.CENTER);
							}
							pdfDoc.setLCell("No. Docto Final","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Monto","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Plazo","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Fecha Venc.","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Fecha Oper.","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Ref. Tasa Int�s","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Valor Tasa Inter�s","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Monto Inter�s","celda01",ComunesPDF.CENTER);
							//pdfDoc.setLCell("Tipo Cobro Inter�s","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Nombre del Producto","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("ID Orden enviado","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Respuesta de Operaci�n","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("C�digo Autorizaci�n","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("N�mero Tarjeta de Cr�dito","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("","celda01",ComunesPDF.CENTER,numCols-14);
						}
						if(publicaDoctosFinanciables.equals("S")){
							//pdfDoc.setLCell(" ","celda01",ComunesPDF.CENTER);
						}
						pdfDoc.setLHeaders();
					}//if(numreg==1)

					if(responsable.equals("D"))
						pdfDoc.setLCell("A","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(descif,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(epo,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(numdocto,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(acuse,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fchemision,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fchpublica,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fchvence,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(plazodocto,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$" + Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.CENTER);

					pdfDoc.setLCell(Comunes.formatoDecimal(porc_Aforo,2)+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$" + Comunes.formatoDecimal(MontoDesconta.toPlainString(),2),"formas",ComunesPDF.CENTER);

					pdfDoc.setLCell(plzodescto,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(porcdescto,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(Comunes.formatoDecimal(mntodescuento,2),"formas",ComunesPDF.CENTER);
					if(!"".equals(cg_Tipo_Conversion)){
						pdfDoc.setLCell((ic_moneda.equals(monedaLinea))?"":tipoconversion,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell((ic_moneda.equals(monedaLinea))?"":tipocambio,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell((ic_moneda.equals(monedaLinea))?"":"$ "+Comunes.formatoDecimal(mntovaluado,2),"formas",ComunesPDF.CENTER);
					}

					pdfDoc.setLCell(modoPlazo,"formas",ComunesPDF.CENTER);
					if(publicaDoctosFinanciables.equals("S")){
						pdfDoc.setLCell(tipoPago,"formas",ComunesPDF.CENTER);
					}
					pdfDoc.setLCell(estatus,"formas",ComunesPDF.CENTER);

					if(!responsable.equals("D")){
						if(ic_estatus.equals("32")){
							pdfDoc.setLCell(descrip_bins,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell(ordenEnvio,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell(opAcuse,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell(codAutorizado,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell(numero_tc,"formas",ComunesPDF.CENTER);
						} else{
							pdfDoc.setLCell("N/A","formas",ComunesPDF.CENTER);
							pdfDoc.setLCell("N/A","formas",ComunesPDF.CENTER);
							pdfDoc.setLCell("N/A","formas",ComunesPDF.CENTER);
							pdfDoc.setLCell("N/A","formas",ComunesPDF.CENTER);
							pdfDoc.setLCell("N/A","formas",ComunesPDF.CENTER);
						}
					} else if(responsable.equals("D")){
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER,5);
						pdfDoc.setLCell("B","formas",ComunesPDF.CENTER);
						if(tipoCreditoXepo != null && tipoCreditoXepo.equals("F")) {
							pdfDoc.setLCell(cuenta_bancaria,"formas",ComunesPDF.CENTER);
						}
						pdfDoc.setLCell(numcredito,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(Comunes.formatoDecimal(mntocredito,2),"formas",ComunesPDF.CENTER);
						if(ic_estatus.equals("32")){
							pdfDoc.setLCell("N/A","formas",ComunesPDF.CENTER);
							pdfDoc.setLCell("N/A","formas",ComunesPDF.CENTER);
						} else{
							pdfDoc.setLCell(plazocred,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell(fchvenccred,"formas",ComunesPDF.CENTER);
						}
						pdfDoc.setLCell(fchopera,"formas",ComunesPDF.CENTER);
						if(ic_estatus.equals("32")){
							pdfDoc.setLCell("N/A","formas",ComunesPDF.CENTER);
							pdfDoc.setLCell("N/A","formas",ComunesPDF.CENTER);
							pdfDoc.setLCell("N/A","formas",ComunesPDF.CENTER);
						} else{
							pdfDoc.setLCell(referencia,"formasrep",ComunesPDF.CENTER);
							pdfDoc.setLCell(Comunes.formatoDecimal(valortaza,2),"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell(Comunes.formatoDecimal(montoint,2),"formas",ComunesPDF.CENTER);
						}

						//pdfDoc.setCell(tipocobroint,"formas",ComunesPDF.CENTER);
						if(ic_estatus.equals("32")){
							pdfDoc.setLCell(descrip_bins,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell(ordenEnvio,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell(opAcuse,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell(codAutorizado,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell(numero_tc,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell("","formasrep",ComunesPDF.CENTER,numCols-14);
						}else{
							pdfDoc.setLCell("N/A","formas",ComunesPDF.CENTER);
							pdfDoc.setLCell("N/A","formas",ComunesPDF.CENTER);
							pdfDoc.setLCell("N/A","formas",ComunesPDF.CENTER);
							pdfDoc.setLCell("N/A","formas",ComunesPDF.CENTER);
							pdfDoc.setLCell("N/A","formas",ComunesPDF.CENTER);
							pdfDoc.setLCell("","formas",ComunesPDF.CENTER,numCols-14);
						}
						if(publicaDoctosFinanciables.equals("S")){
							//pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						}
					}

				}
				pdfDoc.addLTable();
				pdfDoc.endDocument();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF. ", e);
			}
		}
		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}

	public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo) {
		return null;
	}

/*******************************************************************************
 *                       GETTERS AND SETTERS                                   *
 *******************************************************************************/
	public List getConditions(){
		return conditions; 
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public String getIc_pyme() {
		return ic_pyme;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getNumOrden() {
		return numOrden;
	}

	public void setNumOrden(String numOrden) {
		this.numOrden = numOrden;
	}

	public String getTipos_Credito() {
		return tipos_Credito;
	}

	public void setTipos_Credito(String tipos_Credito) {
		this.tipos_Credito = tipos_Credito;
	}

	public String getFchinicial() {
		return fchinicial;
	}

	public void setFchinicial(String fchinicial) {
		this.fchinicial = fchinicial;
	}

	public String getFchfinal() {
		return fchfinal;
	}

	public void setFchfinal(String fchfinal) {
		this.fchfinal = fchfinal;
	}

	public String getCg_Tipo_Conversion() {
		return cg_Tipo_Conversion;
	}

	public void setCg_Tipo_Conversion(String cg_Tipo_Conversion) {
		this.cg_Tipo_Conversion = cg_Tipo_Conversion;
	}

	public String getTipo_Credito_Xepo() {
		return tipo_Credito_Xepo;
	}

	public void setTipo_Credito_Xepo(String tipo_Credito_Xepo) {
		this.tipo_Credito_Xepo = tipo_Credito_Xepo;
	}

	public String getResponsable() {
		return responsable;
	}

	public void setResponsable(String responsable) {
		this.responsable = responsable;
	}

	public String getNoCliente() {
		return noCliente;
	}

	public void setNoCliente(String noCliente) {
		this.noCliente = noCliente;
	}


	public String getPublicaDoctosFinanciables() {
		return publicaDoctosFinanciables;
	}

	public void setPublicaDoctosFinanciables(String publicaDoctosFinanciables) {
		this.publicaDoctosFinanciables = publicaDoctosFinanciables;
	}

  

}
