package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.math.BigDecimal;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsNoOperPymeDist  implements IQueryGenerator, IQueryGeneratorRegExtJS {
	public ConsNoOperPymeDist(){}
	public String getAggregateCalculationQuery(HttpServletRequest request) {

		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
    	StringBuffer qrySentencia	= new StringBuffer();
    	StringBuffer condicion		= new StringBuffer();
      
		String	ic_pyme				= request.getSession().getAttribute("iNoCliente").toString();

		String ic_epo 					= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");	
		String ic_moneda				= (request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String ic_cambio_estatus 	= (request.getParameter("ic_cambio_estatus")==null)?"":request.getParameter("ic_cambio_estatus");
		String modplazo				= (request.getParameter("modplazo")==null)?"":request.getParameter("modplazo");
		String fn_monto_de			= (request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String fn_monto_a				= (request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String ig_numero_docto		= (request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String mto_descuento			= (request.getParameter("mto_descuento")==null)?"N":request.getParameter("mto_descuento");
		String cc_acuse				= (request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String doctos_cambio			= (request.getParameter("doctos_cambio")==null)?"N":request.getParameter("doctos_cambio");
		String df_fecha_emision_de	= (request.getParameter("df_fecha_emision_de")==null)?"":request.getParameter("df_fecha_emision_de");
		String df_fecha_emision_a	= (request.getParameter("df_fecha_emision_a")==null)?"":request.getParameter("df_fecha_emision_a");
		String df_fecha_venc_de		= (request.getParameter("df_fecha_venc_de")==null)?"":request.getParameter("df_fecha_venc_de");
		String df_fecha_venc_a		= (request.getParameter("df_fecha_venc_a")==null)?"":request.getParameter("df_fecha_venc_a");
		String df_fecha_pub_de		= (request.getParameter("df_fecha_pub_de")==null)?"":request.getParameter("df_fecha_pub_de");
		String df_fecha_pub_a		= (request.getParameter("df_fecha_pub_a")==null)?"":request.getParameter("df_fecha_pub_a");
		String cgTipoConversion		= (request.getParameter("tipo_conversion")==null)?"":request.getParameter("tipo_conversion");
				
		try {
			if ("".equals(ic_epo)&&"".equals(ic_moneda)&&"".equals(ic_cambio_estatus)&&
				"".equals(modplazo)&&"".equals(fn_monto_de)&&"".equals(fn_monto_a)&&
				"".equals(ig_numero_docto)&&"".equals(cc_acuse)&&"".equals(df_fecha_emision_de)&&
				"".equals(df_fecha_emision_a)&&"".equals(df_fecha_venc_de)&&
				"".equals(df_fecha_venc_a)&&"".equals(df_fecha_pub_de)&&"".equals(df_fecha_pub_a))	        
					condicion.append("   AND TRUNC (ce.dc_fecha_cambio) = TRUNC (SYSDATE) ");
			if (!("".equals(ic_epo)))
				condicion.append("   AND doc.ic_epo = "+ic_epo+" ");
			if (!("".equals(ic_moneda)))
				condicion.append("   AND doc.ic_moneda = "+ic_moneda+" ");
			if (!("".equals(modplazo)))
				condicion.append("   AND doc.ic_tipo_financiamiento = "+modplazo+" ");
			if (!("".equals(ic_cambio_estatus)))
				condicion.append("   AND ce.ic_cambio_estatus = "+ic_cambio_estatus+" ");
			if("S".equals(doctos_cambio))
				condicion.append(" AND doc.ic_documento in (select ic_documento from dis_cambio_estatus)");
			if (!("".equals(fn_monto_de)) && !("".equals(fn_monto_a)) && "N".equals(mto_descuento)) 
				condicion.append("    AND DOC.fn_monto BETWEEN "+fn_monto_de+" AND "+fn_monto_a+" ");
			if (!("".equals(fn_monto_de)) && !("".equals(fn_monto_a)) && "S".equals(mto_descuento))
				condicion.append("    AND DOC.fn_monto*(DOC.fn_porc_descuento/100) BETWEEN "+fn_monto_de+" AND "+fn_monto_a+" ");
			if (!("".equals(ig_numero_docto)))
				condicion.append("   AND doc.ig_numero_docto = '"+ig_numero_docto+"' ");
			if (!("".equals(cc_acuse)))
				condicion.append("   AND doc.cc_acuse = '"+cc_acuse+"' ");
			if (!("".equals(df_fecha_emision_de)) && !("".equals(df_fecha_emision_a)))
				condicion.append("    AND TRUNC(DOC.df_fecha_emision) BETWEEN TRUNC(TO_DATE('"+df_fecha_emision_de+"','dd/mm/yyyy')) AND TRUNC(TO_DATE('"+df_fecha_emision_a+"','dd/mm/yyyy')) ");
			if (!("".equals(df_fecha_venc_de)) && !("".equals(df_fecha_venc_a)))
				condicion.append("    AND TRUNC(DOC.df_fecha_venc) BETWEEN TRUNC(TO_DATE('"+df_fecha_venc_de+"','dd/mm/yyyy')) AND TRUNC(TO_DATE('"+df_fecha_venc_a+"','dd/mm/yyyy')) ");
			if (!("".equals(df_fecha_pub_de)) && !("".equals(df_fecha_pub_a)))
				condicion.append("    AND TRUNC(DOC.df_carga) BETWEEN TRUNC(TO_DATE('"+df_fecha_pub_de+"','dd/mm/yyyy')) AND TRUNC(TO_DATE('"+df_fecha_pub_a+"','dd/mm/yyyy')) ");

			qrySentencia.append(
				" SELECT /*+ use_nl(ce,doc,v,m) "   +
				"     index(ce cp_dis_cambio_estatus_pk)"   +
				"     index(doc cp_dis_documento_pk)"   +
				"     */ "   +
				"	M.ic_moneda, m.cd_nombre, count(1),SUM (doc.fn_monto)"   +
				"	FROM dis_cambio_estatus ce"   +
				"    	   ,dis_documento doc"   +
				" 		   ,(SELECT /*+index(dce)*/ MAX(dc_fecha_cambio) as fecha_cambio,ic_documento FROM dis_cambio_estatus dce group by ic_documento) v"   +
				"          ,comcat_epo epo"   +
				"          ,comcat_moneda m"   +
				"   WHERE ce.ic_documento = doc.ic_documento"   +
				"	AND ce.dc_fecha_cambio = v.fecha_cambio"   +
				"	AND ce.ic_documento = v.ic_documento"   +
				"	AND doc.ic_epo = epo.ic_epo"   +
				"	AND epo.cs_habilitado = 'S' "   +
				"	AND doc.ic_moneda = m.ic_moneda"   +
				"	AND ce.ic_cambio_estatus IN (34,23,2,24)"   +
				"	AND doc.ic_pyme = "   +ic_pyme+
				condicion.toString()+
				" GROUP BY  m.ic_moneda, m.cd_nombre"   +
				" ORDER BY m.ic_moneda");


		}catch(Exception e){
			System.out.println("CambioEstatusEpoDE::getAggregateCalculationQuery "+e);
		}
		return qrySentencia.toString();
	}

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids) {
		int i=0;
		StringBuffer qrySentencia	= new StringBuffer();
    	StringBuffer qryDM			= new StringBuffer();
    	StringBuffer qryCCC			= new StringBuffer();
    	StringBuffer condicion		= new StringBuffer();
    	
		String	ic_pyme				= request.getSession().getAttribute("iNoCliente").toString();
		//String tipo_credito		=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
    
		condicion.append(" AND ce.ic_documento||to_char(ce.dc_fecha_cambio,'ddmmyyyyhhmiss') in (");
		i=0;
		for (Iterator it = ids.iterator(); it.hasNext();i++){
        	if(i>0){
				condicion.append(",");
			}
			condicion.append(" '"+it.next().toString()+"' ");
		}
		condicion.append(") ");

			qrySentencia.append("  SELECT /*+ use_nl(ce,m,ed,tf)"   +
				"         index(m cp_comcat_moneda_pk)"   +
				"         index(ed cp_comcat_estatus_docto_pk)"   +
				"         index(tf cp_comcat_tipo_finan_pk)*/"   +
				"         doc.ic_documento AS clave, e.cg_razon_social AS epo,"   +
				"        doc.ig_numero_docto AS numdoc, doc.cc_acuse AS acuse,"   +
				"        TO_CHAR (doc.df_fecha_emision, 'dd/mm/yyyy') AS fecha_emision,"   +
				"        TO_CHAR (doc.df_fecha_venc, 'dd/mm/yyyy') AS df_fecha_venc,"   +
				"        TO_CHAR (doc.df_carga, 'dd/mm/yyyy') AS fecha_publicacion,"   +
				"        doc.ig_plazo_docto AS plazodoc, m.cd_nombre AS moneda,"   +
				"        doc.fn_monto AS monto,"   +
				"        DECODE ("   +
				"           NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),"   +
				"           'N', '',"   +
				"           'P', 'Dolar-Peso',"   +
				"           ''"   +
				"        )"   +
				"              AS conversion,"   +
				"        decode(doc.ic_moneda,1,1,doc.fn_tipo_cambio) AS tipcambio,"   +
				"        DECODE ("   +
				"           doc.ic_moneda,"   +
				"           1, doc.fn_monto,"   +
				"           54, (doc.fn_monto -(doc.fn_monto * decode(doc.ic_tipo_financiamiento,1,doc.fn_porc_descuento,0) / 100)) * doc.fn_tipo_cambio"   +
				"        )"   +
				"              AS monto_valuado,"   +
				"        doc.ig_plazo_descuento AS plazdesc,"   +
				"        doc.fn_porc_descuento AS porcdesc,"   +
				"        (doc.fn_monto * decode(doc.ic_tipo_financiamiento,1,doc.fn_porc_descuento,0) / 100) AS monto_con_desc,"   +
				"        tf.cd_descripcion AS modalidad_plazo,"   +
				"        ed.cd_descripcion AS estatus,"   +
				" 		TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fechacambio,"   +
				"        cce.cd_descripcion AS cambioestatus,"   +
				" 		ce.ct_cambio_motivo AS cambiomotivo, m.ic_moneda,clas.cg_descripcion as CATEGORIA"   +
				" ,doc.CG_VENTACARTERA as CG_VENTACARTERA "+
				" ,(SELECT nvl(fn_valor_aforo, 0)     FROM dis_aforo_epo   WHERE ic_if = i.ic_if      AND ic_epo = doc.ic_epo        AND ic_moneda = doc.ic_moneda) AS POR_AFRO  "+

				"   FROM dis_cambio_estatus ce "   +
				"   	   ,dis_documento doc"   +
				"        ,comrel_producto_epo pe"   +
				"        ,comrel_pyme_epo_x_producto x"   +
				"        ,comcat_producto_nafin pn"   +
				"        ,comcat_moneda m"   +
				"        ,comcat_epo e"   +
				"        ,comcat_estatus_docto ed"   +
				" 	   ,comcat_cambio_estatus cce"   +
				"        ,comcat_tipo_financiamiento tf,comrel_clasificacion clas,  "+
				"  	COMCAT_IF I,  DIS_LINEA_CREDITO_DM LC "+
				
				"  WHERE ce.ic_documento = doc.ic_documento"   +
				"    AND ce.dc_fecha_cambio = (SELECT MAX(dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_documento = doc.ic_documento)"   +
				"    AND doc.ic_epo = x.ic_epo"   +
				"    AND doc.ic_pyme = x.ic_pyme"   +
				"    AND doc.ic_producto_nafin = x.ic_producto_nafin"   +
				"    AND doc.ic_epo = pe.ic_epo"   +
				"    AND doc.ic_producto_nafin = pe.ic_producto_nafin"   +
				"    AND doc.ic_producto_nafin = pn.ic_producto_nafin"   +
				"    AND doc.ic_moneda = m.ic_moneda"   +
				"    AND doc.ic_epo = e.ic_epo"   +
				"    AND doc.ic_estatus_docto = ed.ic_estatus_docto"   +
				"    AND cce.ic_cambio_estatus = ce.ic_cambio_estatus"   +
				"    AND tf.ic_tipo_financiamiento = doc.ic_tipo_financiamiento"   +
				" 	 AND doc.ic_clasificacion = clas.ic_clasificacion (+) "+
				"    AND ce.ic_cambio_estatus IN (34,23,2,24)"   +
				"    AND pn.ic_producto_nafin = 4"   +				
				"    AND doc.ic_pyme = "+ic_pyme +
				"	AND LC.IC_IF = I.IC_IF(+)  "+
				"  AND doc.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM (+)  "+
					condicion.toString());
				
		System.out.println("el query queda de la siguiente manera  "+qrySentencia.toString());
		return qrySentencia.toString();
	}
	
	public String getDocumentQuery(HttpServletRequest request) {

		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
    	StringBuffer qrySentencia	= new StringBuffer();
    	StringBuffer condicion		= new StringBuffer();
      
		String	ic_pyme				= request.getSession().getAttribute("iNoCliente").toString();

		String ic_epo 					= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");	
		String ic_moneda				= (request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String ic_cambio_estatus 	= (request.getParameter("ic_cambio_estatus")==null)?"":request.getParameter("ic_cambio_estatus");
		String modplazo				= (request.getParameter("modplazo")==null)?"":request.getParameter("modplazo");
		String fn_monto_de			= (request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String fn_monto_a				= (request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String ig_numero_docto		= (request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String mto_descuento			= (request.getParameter("mto_descuento")==null)?"N":request.getParameter("mto_descuento");
		String cc_acuse				= (request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String doctos_cambio			= (request.getParameter("doctos_cambio")==null)?"N":request.getParameter("doctos_cambio");
		String df_fecha_emision_de	= (request.getParameter("df_fecha_emision_de")==null)?"":request.getParameter("df_fecha_emision_de");
		String df_fecha_emision_a	= (request.getParameter("df_fecha_emision_a")==null)?"":request.getParameter("df_fecha_emision_a");
		String df_fecha_venc_de		= (request.getParameter("df_fecha_venc_de")==null)?"":request.getParameter("df_fecha_venc_de");
		String df_fecha_venc_a		= (request.getParameter("df_fecha_venc_a")==null)?"":request.getParameter("df_fecha_venc_a");
		String df_fecha_pub_de		= (request.getParameter("df_fecha_pub_de")==null)?"":request.getParameter("df_fecha_pub_de");
		String df_fecha_pub_a		= (request.getParameter("df_fecha_pub_a")==null)?"":request.getParameter("df_fecha_pub_a");
		String cgTipoConversion		= (request.getParameter("tipo_conversion")==null)?"":request.getParameter("tipo_conversion");
		
		try {
			if ("".equals(ic_epo)&&"".equals(ic_moneda)&&"".equals(ic_cambio_estatus)&&
				"".equals(modplazo)&&"".equals(fn_monto_de)&&"".equals(fn_monto_a)&&
				"".equals(ig_numero_docto)&&"".equals(cc_acuse)&&"".equals(df_fecha_emision_de)&&
				"".equals(df_fecha_emision_a)&&"".equals(df_fecha_venc_de)&&
				"".equals(df_fecha_venc_a)&&"".equals(df_fecha_pub_de)&&"".equals(df_fecha_pub_a))	        
					condicion.append("   AND TRUNC (ce.dc_fecha_cambio) = TRUNC (SYSDATE) ");
			if (!("".equals(ic_epo)))
				condicion.append("   AND doc.ic_epo = "+ic_epo+" ");
			if (!("".equals(ic_moneda)))
				condicion.append("   AND doc.ic_moneda = "+ic_moneda+" ");
			if (!("".equals(modplazo)))
				condicion.append("   AND doc.ic_tipo_financiamiento = "+modplazo+" ");
			if (!("".equals(ic_cambio_estatus)))
				condicion.append("   AND ce.ic_cambio_estatus = "+ic_cambio_estatus+" ");
			if("S".equals(doctos_cambio))
				condicion.append(" AND doc.ic_documento in (select ic_documento from dis_cambio_estatus)");
			if (!("".equals(fn_monto_de)) && !("".equals(fn_monto_a)) && "N".equals(mto_descuento)) 
				condicion.append("    AND DOC.fn_monto BETWEEN "+fn_monto_de+" AND "+fn_monto_a+" ");
			if (!("".equals(fn_monto_de)) && !("".equals(fn_monto_a)) && "S".equals(mto_descuento))
				condicion.append("    AND DOC.fn_monto*(DOC.fn_porc_descuento/100) BETWEEN "+fn_monto_de+" AND "+fn_monto_a+" ");
			if (!("".equals(ig_numero_docto)))
				condicion.append("   AND doc.ig_numero_docto = '"+ig_numero_docto+"' ");
			if (!("".equals(cc_acuse)))
				condicion.append("   AND doc.cc_acuse = '"+cc_acuse+"' ");
			if (!("".equals(df_fecha_emision_de)) && !("".equals(df_fecha_emision_a)))
				condicion.append("    AND TRUNC(DOC.df_fecha_emision) BETWEEN TRUNC(TO_DATE('"+df_fecha_emision_de+"','dd/mm/yyyy')) AND TRUNC(TO_DATE('"+df_fecha_emision_a+"','dd/mm/yyyy')) ");
			if (!("".equals(df_fecha_venc_de)) && !("".equals(df_fecha_venc_a)))
				condicion.append("    AND TRUNC(DOC.df_fecha_venc) BETWEEN TRUNC(TO_DATE('"+df_fecha_venc_de+"','dd/mm/yyyy')) AND TRUNC(TO_DATE('"+df_fecha_venc_a+"','dd/mm/yyyy')) ");
			if (!("".equals(df_fecha_pub_de)) && !("".equals(df_fecha_pub_a)))
				condicion.append("    AND TRUNC(DOC.df_carga) BETWEEN TRUNC(TO_DATE('"+df_fecha_pub_de+"','dd/mm/yyyy')) AND TRUNC(TO_DATE('"+df_fecha_pub_a+"','dd/mm/yyyy')) ");

			qrySentencia.append(
				"   SELECT /*+ use_nl(ce,doc,v)"   +
				"		index(ce cp_dis_cambio_estatus_pk)"   +
				"		index(doc cp_dis_documento_pk)*/ "   +
				"	ce.ic_documento||to_char(ce.dc_fecha_cambio,'ddmmyyyyhhmiss')"   +
				"	FROM dis_cambio_estatus ce"   +
				"		,dis_documento doc"   +
				"		,(SELECT /*+index(dce)*/MAX(dc_fecha_cambio) as fecha_cambio,ic_documento FROM dis_cambio_estatus dce group by ic_documento) v"   +
				"		,comcat_epo epo"   +
				"   WHERE ce.ic_documento = doc.ic_documento"   +
				"	AND ce.dc_fecha_cambio = v.fecha_cambio"   +
				"	and ce.ic_documento = v.ic_documento"   +
				"	and doc.ic_epo = epo.ic_epo "   +
				"	and epo.cs_habilitado = 'S' "   +
				"	AND ce.ic_cambio_estatus IN (34,23,2,24)"   +
				"	AND doc.ic_pyme = "+ic_pyme  +
				condicion.toString());
					
				System.out.println("EL query de la llave primaria: "+qrySentencia.toString());
		}catch(Exception e){
			System.out.println("ConsInfDocEpoDist::getDocumentQueryException "+e);
		}
		return qrySentencia.toString();
	}
	
	public String getDocumentQueryFile(HttpServletRequest request) {

		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
    	StringBuffer qrySentencia	= new StringBuffer();
    	StringBuffer condicion		= new StringBuffer();
      
		String	ic_pyme				= request.getSession().getAttribute("iNoCliente").toString();

		String ic_epo 					= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");	
		String ic_moneda				= (request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String ic_cambio_estatus 	= (request.getParameter("ic_cambio_estatus")==null)?"":request.getParameter("ic_cambio_estatus");
		String modplazo				= (request.getParameter("modplazo")==null)?"":request.getParameter("modplazo");
		String fn_monto_de			= (request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String fn_monto_a				= (request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String ig_numero_docto		= (request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String mto_descuento			= (request.getParameter("mto_descuento")==null)?"N":request.getParameter("mto_descuento");
		String cc_acuse				= (request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String doctos_cambio			= (request.getParameter("doctos_cambio")==null)?"N":request.getParameter("doctos_cambio");
		String df_fecha_emision_de	= (request.getParameter("df_fecha_emision_de")==null)?"":request.getParameter("df_fecha_emision_de");
		String df_fecha_emision_a	= (request.getParameter("df_fecha_emision_a")==null)?"":request.getParameter("df_fecha_emision_a");
		String df_fecha_venc_de		= (request.getParameter("df_fecha_venc_de")==null)?"":request.getParameter("df_fecha_venc_de");
		String df_fecha_venc_a		= (request.getParameter("df_fecha_venc_a")==null)?"":request.getParameter("df_fecha_venc_a");
		String df_fecha_pub_de		= (request.getParameter("df_fecha_pub_de")==null)?"":request.getParameter("df_fecha_pub_de");
		String df_fecha_pub_a		= (request.getParameter("df_fecha_pub_a")==null)?"":request.getParameter("df_fecha_pub_a");
		String cgTipoConversion		= (request.getParameter("tipo_conversion")==null)?"":request.getParameter("tipo_conversion");
				
		try {
			if ("".equals(ic_epo)&&"".equals(ic_moneda)&&"".equals(ic_cambio_estatus)&&
				"".equals(modplazo)&&"".equals(fn_monto_de)&&"".equals(fn_monto_a)&&
				"".equals(ig_numero_docto)&&"".equals(cc_acuse)&&"".equals(df_fecha_emision_de)&&
				"".equals(df_fecha_emision_a)&&"".equals(df_fecha_venc_de)&&
				"".equals(df_fecha_venc_a)&&"".equals(df_fecha_pub_de)&&"".equals(df_fecha_pub_a))	        
					condicion.append("   AND TRUNC (ce.dc_fecha_cambio) = TRUNC (SYSDATE) ");
			if (!("".equals(ic_epo)))
				condicion.append("   AND doc.ic_epo = "+ic_epo+" ");
			if (!("".equals(ic_moneda)))
				condicion.append("   AND doc.ic_moneda = "+ic_moneda+" ");
			if (!("".equals(modplazo)))
				condicion.append("   AND doc.ic_tipo_financiamiento = "+modplazo+" ");
			if (!("".equals(ic_cambio_estatus)))
				condicion.append("   AND ce.ic_cambio_estatus = "+ic_cambio_estatus+" ");
			if("S".equals(doctos_cambio))
				condicion.append(" AND doc.ic_documento in (select ic_documento from dis_cambio_estatus)");
			if (!("".equals(fn_monto_de)) && !("".equals(fn_monto_a)) && "N".equals(mto_descuento)) 
				condicion.append("    AND DOC.fn_monto BETWEEN "+fn_monto_de+" AND "+fn_monto_a+" ");
			if (!("".equals(fn_monto_de)) && !("".equals(fn_monto_a)) && "S".equals(mto_descuento))
				condicion.append("    AND DOC.fn_monto*(DOC.fn_porc_descuento/100) BETWEEN "+fn_monto_de+" AND "+fn_monto_a+" ");
			if (!("".equals(ig_numero_docto)))
				condicion.append("   AND doc.ig_numero_docto = '"+ig_numero_docto+"' ");
			if (!("".equals(cc_acuse)))
				condicion.append("   AND doc.cc_acuse = '"+cc_acuse+"' ");
			if (!("".equals(df_fecha_emision_de)) && !("".equals(df_fecha_emision_a)))
				condicion.append("    AND TRUNC(DOC.df_fecha_emision) BETWEEN TRUNC(TO_DATE('"+df_fecha_emision_de+"','dd/mm/yyyy')) AND TRUNC(TO_DATE('"+df_fecha_emision_a+"','dd/mm/yyyy')) ");
			if (!("".equals(df_fecha_venc_de)) && !("".equals(df_fecha_venc_a)))
				condicion.append("    AND TRUNC(DOC.df_fecha_venc) BETWEEN TRUNC(TO_DATE('"+df_fecha_venc_de+"','dd/mm/yyyy')) AND TRUNC(TO_DATE('"+df_fecha_venc_a+"','dd/mm/yyyy')) ");
			if (!("".equals(df_fecha_pub_de)) && !("".equals(df_fecha_pub_a)))
				condicion.append("    AND TRUNC(DOC.df_carga) BETWEEN TRUNC(TO_DATE('"+df_fecha_pub_de+"','dd/mm/yyyy')) AND TRUNC(TO_DATE('"+df_fecha_pub_a+"','dd/mm/yyyy')) ");

			qrySentencia.append(
				"  SELECT /*+ use_nl(ce,m,ed,tf)"   +
				"         index(m cp_comcat_moneda_pk)"   +
				"         index(ed cp_comcat_estatus_docto_pk)"   +
				"         index(tf cp_comcat_tipo_finan_pk)*/"   +
				"         doc.ic_documento AS clave, e.cg_razon_social AS epo,"   +
				"        doc.ig_numero_docto AS numdoc, doc.cc_acuse AS acuse,"   +
				"        TO_CHAR (doc.df_fecha_emision, 'dd/mm/yyyy') AS fecha_emision,"   +
				"        TO_CHAR (doc.df_fecha_venc, 'dd/mm/yyyy') AS df_fecha_venc,"   +
				"        TO_CHAR (doc.df_carga, 'dd/mm/yyyy') AS fecha_publicacion,"   +
				"        doc.ig_plazo_docto AS plazodoc, m.cd_nombre AS moneda,"   +
				"        doc.fn_monto AS monto,"   +
				"        DECODE ("   +
				"           NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),"   +
				"           'N', '',"   +
				"           'P', 'Dolar-Peso',"   +
				"           ''"   +
				"        )"   +
				"              AS conversion,"   +
				"        decode(doc.ic_moneda,1,1,doc.fn_tipo_cambio) AS tipcambio,"   +
				"        DECODE ("   +
				"           doc.ic_moneda,"   +
				"           1, doc.fn_monto,"   +
				"           54, (doc.fn_monto -(doc.fn_monto * decode(doc.ic_tipo_financiamiento,1,doc.fn_porc_descuento,0) / 100)) * doc.fn_tipo_cambio"   +
				"        )"   +
				"              AS monto_valuado,"   +
				"        doc.ig_plazo_descuento AS plazdesc,"   +
				"        doc.fn_porc_descuento AS porcdesc,"   +
				"        (doc.fn_monto * decode(doc.ic_tipo_financiamiento,1,doc.fn_porc_descuento,0) / 100) AS monto_con_desc,"   +
				"        tf.cd_descripcion AS modalidad_plazo,"   +
				"        ed.cd_descripcion AS estatus,"   +
				" 		TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fechacambio,"   +
				"        cce.cd_descripcion AS cambioestatus,"   +
				" 		ce.ct_cambio_motivo AS cambiomotivo, m.ic_moneda,clas.cg_descripcion as CATEGORIA"   +
				" ,doc.CG_VENTACARTERA as CG_VENTACARTERA "+
				"   ,(SELECT nvl(fn_valor_aforo, 0)     FROM dis_aforo_epo   WHERE ic_if = i.ic_if      AND ic_epo = doc.ic_epo        AND ic_moneda = doc.ic_moneda) AS POR_AFRO   "+
	
				"   FROM dis_cambio_estatus ce "   +
				"   	   ,dis_documento doc"   +
				"        ,comrel_producto_epo pe"   +
				"        ,comrel_pyme_epo_x_producto x"   +
				"        ,comcat_producto_nafin pn"   +
				"        ,comcat_moneda m"   +
				"        ,comcat_epo e"   +
				"        ,comcat_estatus_docto ed"   +
				" 	   ,comcat_cambio_estatus cce"   +
				"        ,comcat_tipo_financiamiento tf,comrel_clasificacion clas,  "+
				" 		COMCAT_IF I,      DIS_LINEA_CREDITO_DM LC  "+				
				
				"  WHERE ce.ic_documento = doc.ic_documento"   +
				"    AND ce.dc_fecha_cambio = (SELECT MAX(dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_documento = doc.ic_documento)"   +
				"    AND doc.ic_epo = x.ic_epo"   +
				"    AND doc.ic_pyme = x.ic_pyme"   +
				"    AND doc.ic_producto_nafin = x.ic_producto_nafin"   +
				"    AND doc.ic_epo = pe.ic_epo"   +
				"    AND doc.ic_producto_nafin = pe.ic_producto_nafin"   +
				"    AND doc.ic_producto_nafin = pn.ic_producto_nafin"   +
				"    AND doc.ic_moneda = m.ic_moneda"   +
				"    AND doc.ic_epo = e.ic_epo"   +
				"    AND doc.ic_estatus_docto = ed.ic_estatus_docto"   +
				"    AND cce.ic_cambio_estatus = ce.ic_cambio_estatus"   +
				"    AND tf.ic_tipo_financiamiento = doc.ic_tipo_financiamiento"   +
				" 	 AND doc.ic_clasificacion = clas.ic_clasificacion (+) "+
				"    AND ce.ic_cambio_estatus IN (34,23,2,24)"   +
				"    AND pn.ic_producto_nafin = 4"   +
				"    AND e.cs_habilitado = 'S'"   +
				"   AND doc.ic_pyme = "+ic_pyme+" "+
				"   AND LC.IC_IF = I.IC_IF(+) "+
				"  AND doc.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM (+) "+
					condicion.toString());

			System.out.println("EL query queda as� : "+qrySentencia.toString());
		}catch(Exception e){
			System.out.println("CambioEstatusEpoDE::getDocumentQueryFileException "+e);
		}
		return qrySentencia.toString();
	}

/*******************************************************************************
 *          Migraci�n. Implementa IQueryGeneratorRegExtJS.java                 *
 *******************************************************************************/
	private static final Log log = ServiceLocator.getInstance().getLog(ConsNoOperPymeDist.class);
	private List conditions;
	private String ic_pyme;
	private String ic_epo;
	private String ic_moneda;
	private String ic_cambio_estatus;
	private String modplazo;
	private String fn_monto_de;
	private String fn_monto_a;
	private String ig_numero_docto;
	private String mto_descuento;
	private String cc_acuse;
	private String doctos_cambio;
	private String df_fecha_emision_de;
	private String df_fecha_emision_a;
	private String df_fecha_venc_de;
	private String df_fecha_venc_a;
	private String df_fecha_pub_de;
	private String df_fecha_pub_a;
	private String cgTipoConversion;
	private String tipo_credito;

	public String getDocumentQuery() {
		return null;
	}

	public String getDocumentSummaryQueryForIds(List ids) {
		return null;
	}

	public String getAggregateCalculationQuery() {
		return null;
	}

/**
 * Se crea el query para generar los archivs CSV y PDF sin utilizar paginaci�n
 * @return qrySentencia
 */
	public String getDocumentQueryFile() {

		log.info("getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion    = new StringBuffer();
		conditions = new ArrayList();

		conditions.add(ic_pyme);
		if ("".equals(ic_epo)&&"".equals(ic_moneda)&&"".equals(ic_cambio_estatus)&&
			"".equals(modplazo)&&"".equals(fn_monto_de)&&"".equals(fn_monto_a)&&
			"".equals(ig_numero_docto)&&"".equals(cc_acuse)&&"".equals(df_fecha_emision_de)&&
			"".equals(df_fecha_emision_a)&&"".equals(df_fecha_venc_de)&&
			"".equals(df_fecha_venc_a)&&"".equals(df_fecha_pub_de)&&"".equals(df_fecha_pub_a)){
				condicion.append("   AND TRUNC (ce.dc_fecha_cambio) = TRUNC (SYSDATE) ");
		}
		if (!("".equals(ic_epo))){
			condicion.append("     AND doc.ic_epo = ?");
			conditions.add(ic_epo);
		}
		if (!("".equals(ic_moneda))){
			condicion.append("     AND doc.ic_moneda = ?");
			conditions.add(ic_moneda);
		}
		if (!("".equals(modplazo))){
			condicion.append("     AND doc.ic_tipo_financiamiento = ?");
			conditions.add(modplazo);
		}
		if (!("".equals(ic_cambio_estatus))){
			condicion.append("     AND ce.ic_cambio_estatus = ?");
			conditions.add(ic_cambio_estatus);
		}
		if("S".equals(doctos_cambio)){
			condicion.append("     AND doc.ic_documento in (select ic_documento from dis_cambio_estatus)");
		}
		if (!("".equals(fn_monto_de)) && !("".equals(fn_monto_a)) && "N".equals(mto_descuento)){
			condicion.append("     AND DOC.fn_monto BETWEEN ? AND ?");
			conditions.add(fn_monto_de);
			conditions.add(fn_monto_a);
		}
		if (!("".equals(fn_monto_de)) && !("".equals(fn_monto_a)) && "S".equals(mto_descuento)){
			condicion.append("     AND DOC.fn_monto*(DOC.fn_porc_descuento/100) BETWEEN ? AND ?");
			conditions.add(fn_monto_de);
			conditions.add(fn_monto_a);
		}
		if (!("".equals(ig_numero_docto))){
			condicion.append("     AND doc.ig_numero_docto = ?");
			conditions.add(ig_numero_docto);
		}
		if (!("".equals(cc_acuse))){
			condicion.append("     AND doc.cc_acuse = ?");
			conditions.add(cc_acuse);
		}if (!("".equals(df_fecha_emision_de)) && !("".equals(df_fecha_emision_a))){
			condicion.append("     AND TRUNC(DOC.df_fecha_emision) BETWEEN TRUNC(TO_DATE(?,'dd/mm/yyyy')) AND TRUNC(TO_DATE(?,'dd/mm/yyyy')) ");
			conditions.add(df_fecha_emision_a);
			conditions.add(df_fecha_emision_a);
		}
		if (!("".equals(df_fecha_venc_de)) && !("".equals(df_fecha_venc_a))){
			condicion.append("     AND TRUNC(DOC.df_fecha_venc) BETWEEN TRUNC(TO_DATE(?,'dd/mm/yyyy')) AND TRUNC(TO_DATE(?,'dd/mm/yyyy')) ");
			conditions.add(df_fecha_venc_de);
			conditions.add(df_fecha_venc_a);
		}if (!("".equals(df_fecha_pub_de)) && !("".equals(df_fecha_pub_a))){
			condicion.append("     AND TRUNC(DOC.df_carga) BETWEEN TRUNC(TO_DATE(?,'dd/mm/yyyy')) AND TRUNC(TO_DATE(?,'dd/mm/yyyy')) ");
			conditions.add(df_fecha_pub_de);
			conditions.add(df_fecha_pub_a);
		}

		qrySentencia.append("  SELECT /*+ use_nl(ce,m,ed,tf)");
		qrySentencia.append("         index(m cp_comcat_moneda_pk)");
		qrySentencia.append("         index(ed cp_comcat_estatus_docto_pk)");
		qrySentencia.append("         index(tf cp_comcat_tipo_finan_pk)*/");
		qrySentencia.append("         doc.ic_documento AS clave, e.cg_razon_social AS epo,");
		qrySentencia.append("         doc.ig_numero_docto AS numdoc, doc.cc_acuse AS acuse,");
		qrySentencia.append("         TO_CHAR (doc.df_fecha_emision, 'dd/mm/yyyy') AS fecha_emision,");
		qrySentencia.append("         TO_CHAR (doc.df_fecha_venc, 'dd/mm/yyyy') AS df_fecha_venc,");
		qrySentencia.append("         TO_CHAR (doc.df_carga, 'dd/mm/yyyy') AS fecha_publicacion,");
		qrySentencia.append("         doc.ig_plazo_docto AS plazodoc, m.cd_nombre AS moneda,");
		qrySentencia.append("         doc.fn_monto AS monto,");
		qrySentencia.append("         DECODE (");
		qrySentencia.append("           NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),");
		qrySentencia.append("           N', '',");
		qrySentencia.append("           P', 'Dolar-Peso',");
		qrySentencia.append("           ''");
		qrySentencia.append("         ) AS conversion,");
		qrySentencia.append("         decode(doc.ic_moneda,1,1,doc.fn_tipo_cambio) AS tipcambio,");
		qrySentencia.append("         DECODE (");
		qrySentencia.append("           doc.ic_moneda,");
		qrySentencia.append("           1, doc.fn_monto,");
		qrySentencia.append("           54, (doc.fn_monto -(doc.fn_monto * decode(doc.ic_tipo_financiamiento,1,doc.fn_porc_descuento,0) / 100)) * doc.fn_tipo_cambio");
		qrySentencia.append("         ) AS monto_valuado,");
		qrySentencia.append("         doc.ig_plazo_descuento AS plazdesc,");
		qrySentencia.append("         doc.fn_porc_descuento AS porcdesc,");
		qrySentencia.append("         (doc.fn_monto * decode(doc.ic_tipo_financiamiento,1,doc.fn_porc_descuento,0) / 100) AS monto_con_desc,");
		qrySentencia.append("         tf.cd_descripcion AS modalidad_plazo,");
		qrySentencia.append("         ed.cd_descripcion AS estatus,");
		qrySentencia.append("         TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fechacambio,");
		qrySentencia.append("         cce.cd_descripcion AS cambioestatus,");
		qrySentencia.append("         ce.ct_cambio_motivo AS cambiomotivo, m.ic_moneda,clas.cg_descripcion as CATEGORIA");
		qrySentencia.append("         ,doc.CG_VENTACARTERA as CG_VENTACARTERA ");
		qrySentencia.append("         ,(SELECT nvl(fn_valor_aforo, 0)     FROM dis_aforo_epo   WHERE ic_if = i.ic_if      AND ic_epo = doc.ic_epo        AND ic_moneda = doc.ic_moneda) AS POR_AFRO ");
		qrySentencia.append("    FROM dis_cambio_estatus ce ");
		qrySentencia.append("         ,dis_documento doc");
		qrySentencia.append("         ,comrel_producto_epo pe");
		qrySentencia.append("         ,comrel_pyme_epo_x_producto x");
		qrySentencia.append("         ,comcat_producto_nafin pn");
		qrySentencia.append("         ,comcat_moneda m");
		qrySentencia.append("         ,comcat_epo e");
		qrySentencia.append("         ,comcat_estatus_docto ed");
		qrySentencia.append("         ,comcat_cambio_estatus cce");
		qrySentencia.append("         ,comcat_tipo_financiamiento tf,comrel_clasificacion clas," );
		qrySentencia.append("         COMCAT_IF I, DIS_LINEA_CREDITO_DM LC");
		qrySentencia.append("   WHERE ce.ic_documento = doc.ic_documento");
		qrySentencia.append("     AND ce.dc_fecha_cambio = (SELECT MAX(dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_documento = doc.ic_documento)");
		qrySentencia.append("     AND doc.ic_epo = x.ic_epo");
		qrySentencia.append("     AND doc.ic_pyme = x.ic_pyme");
		qrySentencia.append("     AND doc.ic_producto_nafin = x.ic_producto_nafin");
		qrySentencia.append("     AND doc.ic_epo = pe.ic_epo");
		qrySentencia.append("     AND doc.ic_producto_nafin = pe.ic_producto_nafin");
		qrySentencia.append("     AND doc.ic_producto_nafin = pn.ic_producto_nafin");
		qrySentencia.append("     AND doc.ic_moneda = m.ic_moneda");
		qrySentencia.append("     AND doc.ic_epo = e.ic_epo");
		qrySentencia.append("     AND doc.ic_estatus_docto = ed.ic_estatus_docto");
		qrySentencia.append("     AND cce.ic_cambio_estatus = ce.ic_cambio_estatus");
		qrySentencia.append("     AND tf.ic_tipo_financiamiento = doc.ic_tipo_financiamiento");
		qrySentencia.append("     AND doc.ic_clasificacion = clas.ic_clasificacion (+)");
		qrySentencia.append("     AND ce.ic_cambio_estatus IN (34,23,2,24)");
		qrySentencia.append("     AND pn.ic_producto_nafin = 4");
		qrySentencia.append("     AND e.cs_habilitado = 'S'");
		qrySentencia.append("     AND doc.ic_pyme = ?");
		qrySentencia.append("     AND LC.IC_IF = I.IC_IF(+)");
		qrySentencia.append("     AND doc.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM (+)");
		qrySentencia.append(condicion.toString());

		log.info("qrySentencia: " + qrySentencia.toString());
		log.info("conditions: " + conditions.toString());
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();
	}

/**
 * Genera el archivo PDF, y puede generar el archivo CSV sin contemplar paginaci�n, los datos para imprimir el archivo
 * vienen del m�todo getDocumentQueryFile()
 * @param request
 * @param rs
 * @param path
 * @param tipo
 * @return
 */
	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo) {

		log.info("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();

		String clave             = "";
		String epo               = "";
		String numdoc            = "";
		String acuse             = "";
		String fecha_emision     = "";
		String df_fecha_venc     = "";
		String fecha_publicacion = "";
		String plazodoc          = "";
		String moneda            = "";
		String conversion        = "";
		String plazdesc          = "";
		String porcdesc          = "";
		String modalidad_plazo   = "";
		String estatus           = "";
		String fechacambio       = "";
		String cambioestatus     = "";
		String cambiomotivo      = "";
		String bandeVentaCartera = "";
		String categoria         = "";
		String rs_ic_mon         = "";
		String porc_Aforo        = "";
		double monto             = 0;
		double tipcambio         = 0;
		double monto_valuado     = 0;
		double monto_con_desc    = 0;
		int    nRow              = 0;

		if(tipo.equals("PDF")){
			try{

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

				while (rs.next()){
					if(nRow == 0){
						int numCols = (!"".equals(cgTipoConversion)?23:20);
						pdfDoc.setLTable(numCols,100);
						pdfDoc.setLCell("Datos del Documento Inicial",  "celda01", ComunesPDF.CENTER, numCols);
						pdfDoc.setLCell("EPO",                          "celda01", ComunesPDF.CENTER);
						pdfDoc.setLCell("No. Docto Inicial",            "celda01", ComunesPDF.CENTER);
						pdfDoc.setLCell("No. Acuse Carga",              "celda01", ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha Emisi�n",                "celda01", ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha Pub.",                   "celda01", ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha Venc.",                  "celda01", ComunesPDF.CENTER);
						pdfDoc.setLCell("Plazo Docto",                  "celda01", ComunesPDF.CENTER);
						pdfDoc.setLCell("Moneda",                       "celda01", ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto",                        "celda01", ComunesPDF.CENTER);
						pdfDoc.setLCell("Categoria",                    "celda01", ComunesPDF.CENTER);
						pdfDoc.setLCell("% de Descuento Aforo",         "celda01", ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto a Descontar",            "celda01", ComunesPDF.CENTER);
						pdfDoc.setLCell("Plazo para descuento en d�as", "celda01", ComunesPDF.CENTER);
						pdfDoc.setLCell("% de descuento",               "celda01", ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto % de descuento",         "celda01", ComunesPDF.CENTER);
						if(!"".equals(cgTipoConversion)){
							pdfDoc.setLCell("Tipo Conv.",                "celda01", ComunesPDF.CENTER);
							pdfDoc.setLCell("Tipo Cambio",               "celda01", ComunesPDF.CENTER);
							pdfDoc.setLCell("Monto Valuado",             "celda01", ComunesPDF.CENTER);
						}
						pdfDoc.setLCell("Modalidad de plazo",           "celda01", ComunesPDF.CENTER);
						pdfDoc.setLCell("Estatus",                      "celda01", ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha Cambio Estatus",         "celda01", ComunesPDF.CENTER);
						pdfDoc.setLCell("Tipo Cambio Estatus",          "celda01", ComunesPDF.CENTER);
						pdfDoc.setLCell("Causa",                        "celda01", ComunesPDF.CENTER);
						pdfDoc.setLHeaders();
						nRow++;
					}

					clave             = rs.getString("clave")             ==null?"" :rs.getString("clave");
					epo               = rs.getString("EPO")               ==null?"" :rs.getString("EPO");
					numdoc            = rs.getString("NUMDOC")            ==null?"" :rs.getString("NUMDOC");
					acuse             = rs.getString("ACUSE")             ==null?"" :rs.getString("ACUSE");
					fecha_emision     = rs.getString("FECHA_EMISION")     ==null?"" :rs.getString("FECHA_EMISION");
					df_fecha_venc     = rs.getString("DF_FECHA_VENC")     ==null?"" :rs.getString("DF_FECHA_VENC");
					fecha_publicacion = rs.getString("FECHA_PUBLICACION") ==null?"" :rs.getString("FECHA_PUBLICACION");
					plazodoc          = rs.getString("PLAZODOC")          ==null?"" :rs.getString("PLAZODOC");
					moneda            = rs.getString("MONEDA")            ==null?"" :rs.getString("MONEDA");
					conversion        = rs.getString("CONVERSION")        ==null?"" :rs.getString("CONVERSION");
					plazdesc          = rs.getString("PLAZDESC")          ==null?"" :rs.getString("PLAZDESC");
					porcdesc          = rs.getString("PORCDESC")          ==null?"" :rs.getString("PORCDESC");
					modalidad_plazo   = rs.getString("MODALIDAD_PLAZO")   ==null?"" :rs.getString("MODALIDAD_PLAZO");
					estatus           = rs.getString("ESTATUS")           ==null?"" :rs.getString("ESTATUS");
					fechacambio       = rs.getString("FECHACAMBIO")       ==null?"" :rs.getString("FECHACAMBIO");
					cambioestatus     = rs.getString("CAMBIOESTATUS")     ==null?"" :rs.getString("CAMBIOESTATUS");
					cambiomotivo      = rs.getString("CAMBIOMOTIVO")      ==null?"" :rs.getString("CAMBIOMOTIVO");
					rs_ic_mon         = rs.getString("IC_MONEDA")         ==null?"" :rs.getString("IC_MONEDA");
					categoria         = rs.getString("CATEGORIA")         ==null?"" :rs.getString("CATEGORIA");
					bandeVentaCartera = rs.getString("CG_VENTACARTERA")   ==null?"" :rs.getString("CG_VENTACARTERA");
					porc_Aforo        = rs.getString("POR_AFRO")          ==null?"0":rs.getString("POR_AFRO");

					monto          = Double.parseDouble(rs.getString("MONTO")          ==null?"0":rs.getString("MONTO"));
					tipcambio      = Double.parseDouble(rs.getString("TIPCAMBIO")      ==null?"0":rs.getString("TIPCAMBIO"));
					monto_valuado  = Double.parseDouble(rs.getString("MONTO_VALUADO")  ==null?"0":rs.getString("MONTO_VALUADO"));
					monto_con_desc = Double.parseDouble(rs.getString("MONTO_CON_DESC") ==null?"0":rs.getString("MONTO_CON_DESC"));

					BigDecimal por_Aforo     = new BigDecimal(porc_Aforo);
					BigDecimal montoD        = new BigDecimal(monto);
					BigDecimal MontoDesconta = montoD.multiply(por_Aforo.divide(new BigDecimal("100"),10,BigDecimal.ROUND_HALF_UP));

					if(bandeVentaCartera.equals("S")){
						modalidad_plazo ="";
					}

					pdfDoc.setLCell(epo.replace(',',' '),"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(numdoc,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(acuse,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_emision,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(df_fecha_venc,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_publicacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(plazodoc,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(moneda,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell('$'+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(categoria,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(porc_Aforo+'%',"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell('$'+Comunes.formatoDecimal(MontoDesconta.toPlainString(),2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(plazdesc,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(porcdesc+'%',"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell('$'+Comunes.formatoDecimal(monto_con_desc,2),"formas",ComunesPDF.RIGHT);

					if(!"".equals(cgTipoConversion)){
						pdfDoc.setLCell(((tipcambio==0)?conversion:""),"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell('$'+((tipcambio==0)?"":Comunes.formatoDecimal(tipcambio,2,false)),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell('$'+((tipcambio==0)?"":Comunes.formatoDecimal(monto_valuado,2,false)),"formas",ComunesPDF.RIGHT);
					}
					pdfDoc.setLCell(modalidad_plazo,"formasr",ComunesPDF.LEFT);
					pdfDoc.setLCell(estatus,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(fechacambio,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(cambioestatus,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(cambiomotivo,"formas",ComunesPDF.LEFT);
				}

				pdfDoc.addLTable();
				pdfDoc.endDocument();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF. ", e);
			}
		}

		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}

	public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo) {
		return null;
	}

/*******************************************************************************
 *                       GETTERS AND SETTERS                                   *
 *******************************************************************************/
	public List getConditions() {
		return conditions;
	}

	public String getIc_pyme() {
		return ic_pyme;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}

	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getIc_moneda() {
		return ic_moneda;
	}

	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}

	public String getIc_cambio_estatus() {
		return ic_cambio_estatus;
	}

	public void setIc_cambio_estatus(String ic_cambio_estatus) {
		this.ic_cambio_estatus = ic_cambio_estatus;
	}

	public String getModplazo() {
		return modplazo;
	}

	public void setModplazo(String modplazo) {
		this.modplazo = modplazo;
	}

	public String getFn_monto_de() {
		return fn_monto_de;
	}

	public void setFn_monto_de(String fn_monto_de) {
		this.fn_monto_de = fn_monto_de;
	}

	public String getFn_monto_a() {
		return fn_monto_a;
	}

	public void setFn_monto_a(String fn_monto_a) {
		this.fn_monto_a = fn_monto_a;
	}

	public String getIg_numero_docto() {
		return ig_numero_docto;
	}

	public void setIg_numero_docto(String ig_numero_docto) {
		this.ig_numero_docto = ig_numero_docto;
	}

	public String getMto_descuento() {
		return mto_descuento;
	}

	public void setMto_descuento(String mto_descuento) {
		this.mto_descuento = mto_descuento;
	}

	public String getCc_acuse() {
		return cc_acuse;
	}

	public void setCc_acuse(String cc_acuse) {
		this.cc_acuse = cc_acuse;
	}

	public String getDoctos_cambio() {
		return doctos_cambio;
	}

	public void setDoctos_cambio(String doctos_cambio) {
		this.doctos_cambio = doctos_cambio;
	}

	public String getDf_fecha_emision_de() {
		return df_fecha_emision_de;
	}

	public void setDf_fecha_emision_de(String df_fecha_emision_de) {
		this.df_fecha_emision_de = df_fecha_emision_de;
	}

	public String getDf_fecha_emision_a() {
		return df_fecha_emision_a;
	}

	public void setDf_fecha_emision_a(String df_fecha_emision_a) {
		this.df_fecha_emision_a = df_fecha_emision_a;
	}

	public String getDf_fecha_venc_de() {
		return df_fecha_venc_de;
	}

	public void setDf_fecha_venc_de(String df_fecha_venc_de) {
		this.df_fecha_venc_de = df_fecha_venc_de;
	}

	public String getDf_fecha_venc_a() {
		return df_fecha_venc_a;
	}

	public void setDf_fecha_venc_a(String df_fecha_venc_a) {
		this.df_fecha_venc_a = df_fecha_venc_a;
	}

	public String getDf_fecha_pub_de() {
		return df_fecha_pub_de;
	}

	public void setDf_fecha_pub_de(String df_fecha_pub_de) {
		this.df_fecha_pub_de = df_fecha_pub_de;
	}

	public String getDf_fecha_pub_a() {
		return df_fecha_pub_a;
	}

	public void setDf_fecha_pub_a(String df_fecha_pub_a) {
		this.df_fecha_pub_a = df_fecha_pub_a;
	}

	public String getCgTipoConversion() {
		return cgTipoConversion;
	}

	public void setCgTipoConversion(String cgTipoConversion) {
		this.cgTipoConversion = cgTipoConversion;
	}

	public String getTipo_credito() {
		return tipo_credito;
	}

	public void setTipo_credito(String tipo_credito) {
		this.tipo_credito = tipo_credito;
	}
}
