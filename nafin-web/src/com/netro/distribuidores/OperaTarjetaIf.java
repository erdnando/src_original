package com.netro.distribuidores;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/******************************************************************************************
 * Fodea 13-2014																									*
 * Descripci�n: En la pantalla Productos/ Distribuidores/ Capturas/ Par�metros por IF		*
 * 				 si opera con tarjeta de cr�dito, se obtienen o insertan los datos de la	*
 *              tabla COM_BINS_IF por medio de esta clase.											*
 * Elabor�: Agust�n Bautista Ruiz																			*
 * Fecha: 29/07/2014																								*
 ******************************************************************************************/
public class OperaTarjetaIf implements IQueryGeneratorRegExtJS {

	private static final Log log = ServiceLocator.getInstance().getLog(OperaTarjetaIf.class);
	private int icIf;
	private int nafinElectronico;
	private List conditions;
	private JSONArray arrayRegNuevo;
	private String claveUsuario;
	private String nombreUsuario;

	public OperaTarjetaIf() {}
	public String getDocumentQuery(){ return null; }
	public String getAggregateCalculationQuery(){ return null; }
	public String getDocumentSummaryQueryForIds(List ids){ return null; }
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){ return null; }
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){ return null; }
	
	/**
	 * La consulta no requiere paginaci�n
	 * @return qrySentencia
	 */
	public String getDocumentQueryFile(){ 
	
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();
		log.info("getDocumentQueryFile (E)");	
		try{
			qrySentencia.append(
				" SELECT B.IC_BINS AS BIN, B.IC_IF, "           +
				"        M.IC_MONEDA AS MONEDA, "               + 
				"        B.CG_DESCRIPCION AS DESCRIPCION, "     +
				"        B.CG_CODIGO_BIN AS CODIGO_BIN, "       + 
				//"      B.FN_COMISION AS COMISION_APLICABLE, " + // ABR: Este campo no es requerido en el grid 15/08/2014
				"        CASE B.CG_ESTATUS "                    +
				"        WHEN 'A' THEN 'ACTIVO' "               +
				"        ELSE 'INACTIVO' END AS ESTATUS, "      +
				"        B.CG_DESCRIPCION_BAJA AS DESC_BAJA "   +
				"   FROM COM_BINS_IF B, COMCAT_MONEDA M "       +
				"  WHERE IC_IF =?  "                            +
				"        AND B.IC_MONEDA = M.IC_MONEDA "        +
				"  ORDER BY B.IC_BINS ASC "
			);
			
			conditions.add(new Integer(this.icIf));
			
			log.info("Sentencia: "   + qrySentencia.toString());		
			log.info("Condiciones: " + conditions.toString());	
			
		}catch(Exception e){
			log.warn("OperaTarjetaIf::getDocumentQueryFileException "+e);
		}
		log.info("getDocumentQueryFile (S)");
		
		return qrySentencia.toString(); 
	}	

	public int getIcNafinElectronico()  throws Exception{
	
		AccesoDB con = new AccesoDB();
		int campo = 0;
		ResultSet rs = null;
		StringBuffer consulta = new StringBuffer();
		log.info("getIcNafinElectronico (E)");
		try{
			con.conexionDB();
			consulta.append(
				"SELECT IC_NAFIN_ELECTRONICO FROM COMREL_NAFIN WHERE IC_EPO_PYME_IF = '"+ this.icIf +"' AND CG_TIPO =  'I'"
			);
			log.debug("Sentencia: " + consulta.toString());
			rs = con.queryDB(consulta.toString());
			if(rs.next()) {
				campo = rs.getInt("IC_NAFIN_ELECTRONICO");
			}
		} catch(Exception e){
			log.warn("getIcNafinElectronicoException " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		log.debug("IC_NAFIN_ELECTRONICO: " + campo);
		log.info("getIcNafinElectronico (S)");
		return campo;
		
	}
	/**
	 * Inserta o actualiza los datos en la tabla COM_BINS_IF. Asi mismo 
	 * registra los cambios en la bit�cora BIT_CAMBIOS_GRAL
	 * @return true: proceso exitoso. false: caso contrario.
	 */
	public boolean insertaBinsIf()  throws Exception{
	
		JSONObject resultado  = new JSONObject();
		AccesoDB con          = new AccesoDB();
		StringBuffer inserta  = new StringBuffer();
		StringBuffer modifica = new StringBuffer();
		StringBuffer insetBit = new StringBuffer();
		String consulta       = "";
		String cadenaAnt      = "";
		String cadenaAct      = "";
		String descripcion    = "";
		String codigoBin      = "";
		String descBaja       = "";
		String estatus        = "";
		String descripcionAnt = "";
		String codigoBinAnt   = "";
		String descBajaAnt    = "";
		String estatusAnt     = "";
		int icBin             = 0;
		int icMoneda          = 0;
		int icBinsAnt         = 0;
		int icIfAnt           = 0;
		int icMonedaAnt       = 0;
		boolean exito         = false;
		PreparedStatement ps  = null;  
		PreparedStatement ps1 = null;
		ResultSet rs          = null;
		
		log.info("insertaBinsIf (E)");
		
		inserta.append(
			"INSERT INTO COM_BINS_IF " + 
			"(IC_BINS, IC_IF, IC_MONEDA, CG_DESCRIPCION, CG_CODIGO_BIN, CG_ESTATUS, CG_DESCRIPCION_BAJA) " +
			"VALUES (SEQ_COM_BINS_IF.NextVal, ?, ?, ?, ?, ?, ?) " 
	   );
		 
		modifica.append(
			"UPDATE COM_BINS_IF SET " + 
			"IC_MONEDA = ?, CG_DESCRIPCION = ?, CG_CODIGO_BIN = ?, CG_ESTATUS = ?, CG_DESCRIPCION_BAJA = ? " +
			"WHERE IC_BINS = ? AND IC_IF = ? " 
	   );
		 
		insetBit.append(
			"INSERT INTO BIT_CAMBIOS_GRAL " + 
			"(IC_CAMBIO, CC_PANTALLA_BITACORA, CG_CLAVE_AFILIADO, IC_NAFIN_ELECTRONICO, DF_CAMBIO, IC_USUARIO, CG_NOMBRE_USUARIO, CG_ANTERIOR, CG_ACTUAL) " +
			"VALUES (SEQ_BIT_CAMBIOS_GRAL.NextVal, 'PARAM_IF', 'I', ?, SYSDATE, ?, ?, ?, ?) " 
	   );
		
		try{
			con.conexionDB();
			
			for(int i = 0; i < arrayRegNuevo.size(); i++){
				
				JSONObject auxiliar = arrayRegNuevo.getJSONObject(i);
				consulta 		= "";
				cadenaAnt		= "";
				cadenaAct		= "";
				descripcion    = "";
				codigoBin      = "";
				estatus		   = "";
				descBaja		   = "";
				descripcionAnt = "";
				codigoBinAnt   = "";
				estatusAnt     = "";
				icBin			   = 0;
				icMoneda		   = 0;
				icBinsAnt      = 0;
				icIfAnt        = 0;
				icMonedaAnt    = 0;
				rs 			   = null;
				ps  			   = null;
				ps1 			   = null;
				
				descripcion = auxiliar.getString("DESCRIPCION");
				codigoBin   = auxiliar.getString("CODIGO_BIN");
				descBaja    = auxiliar.getString("DESC_BAJA");
				
				if( auxiliar.getString("ESTATUS").equals("ACTIVO") ){
					estatus = "A";
				} else if( auxiliar.getString("ESTATUS").equals("INACTIVO") ){
					estatus = "I";
				};
				
				if(auxiliar.getString("MONEDA").equals("")){
					icMoneda = 1;
				} else{
					icMoneda = Integer.parseInt(auxiliar.getString("MONEDA"));
				}
				/***** Determino si INSERTO o ACTUALIZO los datos *****/
				if( auxiliar.getString("BIN").equals("") && auxiliar.getString("IC_IF").equals("") ){	
					
					ps = con.queryPrecompilado(inserta.toString());
					ps.setInt 	(1, this.icIf	);
					ps.setInt 	(2, icMoneda	);
					ps.setString(3, descripcion);
					ps.setString(4, codigoBin	);
					ps.setString(5, estatus		);
					ps.setString(6, descBaja   );
					cadenaAnt = "";
					cadenaAct = "IC_IF= " + this.icIf + ", IC_MONEDA= " + icMoneda + ", CG_DESCRIPCION= " + descripcion + 
									", CG_CODIGO_BIN= " + codigoBin + ", CG_ESTATUS= " + estatus + ", CG_DESCRIPCION_BAJA = " + descBaja;
					
					log.info("Sentencia (insert): " + inserta.toString());
					
				} else if( !auxiliar.getString("BIN").equals("") && !auxiliar.getString("IC_IF").equals("") ){
					
					/***** Obtengo los datos anteriores *****/
					icBin = Integer.parseInt(auxiliar.getString("BIN"));
					consulta = "SELECT IC_BINS, IC_IF, IC_MONEDA, CG_DESCRIPCION, CG_CODIGO_BIN, CG_ESTATUS, CG_DESCRIPCION_BAJA " + 
								  "FROM  COM_BINS_IF WHERE IC_BINS = "+ icBin +" AND IC_IF = " + this.icIf;
					rs = con.queryDB(consulta.toString());
					if(rs.next()) {
						icBinsAnt 		= rs.getInt("IC_BINS");
						icIfAnt 			= rs.getInt("IC_IF");
						icMonedaAnt 	= rs.getInt("IC_MONEDA");
						descripcionAnt = rs.getString("CG_DESCRIPCION").trim();
						codigoBinAnt 	= rs.getString("CG_CODIGO_BIN").trim();
						estatusAnt 		= rs.getString("CG_ESTATUS").trim();
						descBajaAnt    = rs.getString("CG_DESCRIPCION_BAJA");
					}
					rs.close();
					log.info("Sentencia (select): " + consulta.toString());
					
					if( icMonedaAnt != icMoneda || !descripcionAnt.equals(descripcion) || 
						!codigoBinAnt.equals(codigoBin) || !estatusAnt.equals(estatus)){
						cadenaAnt = "IC_IF= " + icIfAnt + ", IC_MONEDA= " + icMonedaAnt + ", CG_DESCRIPCION= " + descripcionAnt + 
										", CG_CODIGO_BIN= " + codigoBinAnt + ", CG_ESTATUS= " + estatusAnt + ", CG_DESCRIPCION_BAJA = " + descBajaAnt;
						cadenaAct = "IC_IF= " + this.icIf + ", IC_MONEDA= " + icMoneda + ", CG_DESCRIPCION= " + descripcion + 
										", CG_CODIGO_BIN= " + codigoBin + ", CG_ESTATUS= " + estatus + ", CG_DESCRIPCION_BAJA = " + descBaja;					
					} else{
						cadenaAnt = "";
						cadenaAct = "";
					}
					
					/***** Datos nuevos *****/
					ps = con.queryPrecompilado(modifica.toString());
					ps.setInt 	(1, icMoneda	);
					ps.setString(2, descripcion);
					ps.setString(3, codigoBin	);
					ps.setString(4, estatus		);
					ps.setString(5, descBaja   );
					ps.setInt 	(6, icBin		);
					ps.setInt 	(7, this.icIf	);
					
					log.info("Sentencia (update): " + modifica.toString());
				}
				
		      try{ 
					/***** INSERTO o ACTUALIZO *****/
					ps.executeUpdate();
					
					/***** INSERTO en bit�cora *****/
					if(!cadenaAct.equals("")){
						ps1 = con.queryPrecompilado(insetBit.toString());
						ps1.setInt	 (1, this.nafinElectronico);
						ps1.setString(2, this.claveUsuario);
						ps1.setString(3, this.nombreUsuario);
						ps1.setString(4, cadenaAnt);
						ps1.setString(5, cadenaAct);
						ps1.executeUpdate();
						log.info("Sentencia (insert BIT): " + insetBit.toString());
					}
					exito	= true;
					
				} catch(Exception e){
				
					exito	= false;
					log.warn("insertaBinsIfException (executeUpdate)");
					log.warn("insertaBinsIfException < icIf = " 			+ icIf 			+ " >");
					log.warn("insertaBinsIfException < icMoneda = " 	+ icMoneda 		+ " >");
					log.warn("insertaBinsIfException < descripcion = " + descripcion 	+ " >");
					log.warn("insertaBinsIfException < codigoBin = " 	+ codigoBin 	+ " >");
					log.warn("insertaBinsIfException < estatus = " 		+ estatus 		+ " >");
					log.warn(e);	
					break;
					
				}
			}
			
		} catch(Exception e){
			exito	= false;
			log.warn("insertaBinsIfException " + e);
			
		} finally {
			if( ps  != null ){ try{ ps.close();  } catch(Exception e){} }
			if( ps1 != null ){ try{ ps1.close(); } catch(Exception e){} }
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}	
		log.info("insertaBinsIf (S)");
		return exito;
	}
	
/************************************************************
 *								GETTERS ANS SETTERS						*
 ************************************************************/	
	public int getIcIf() {
		return icIf;
	}

	public void setIcIf(int icIf) {
		this.icIf = icIf;
	}

	public List getConditions(){
		return conditions; 
	}

	public JSONArray getArrayRegNuevo() {
		return arrayRegNuevo;
	}

	public void setArrayRegNuevo(JSONArray arrayRegNuevo) {
		this.arrayRegNuevo = arrayRegNuevo;
	}

	public String getClaveUsuario() {
		return claveUsuario;
	}

	public void setClaveUsuario(String claveUsuario) {
		this.claveUsuario = claveUsuario;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public int getNafinElectronico() {
		return nafinElectronico;
	}

	public void setNafinElectronico(int nafinElectronico) {
		this.nafinElectronico = nafinElectronico;
	}
	
}