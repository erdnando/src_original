package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class EstadoAdeudos implements IQueryGeneratorReg, IQueryGeneratorRegExtJS {

  public EstadoAdeudos(){}

 		//Variable para enviar mensajes al log.
	private static final Log log = ServiceLocator.getInstance().getLog(EstadoAdeudos.class);
	
	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer 		qrySentencia;
	private List 		conditions;
	private String 	paginaOffset;
	private String 	paginaNo;		
	private String directorio;
	public String paginar;
	public String ic_if;
	public String ic_pyme;
	public String ic_moneda;
	public String df_fecha_oper_de;
	public String df_fecha_oper_a;
	public String usuario;
	private Vector datosVec; 

	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */

	public String getAggregateCalculationQuery() {
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
	 
	log.debug("-------getAggregateCalculationQuery-----------------:: ");		
	log.debug("paginaOffset:: "+paginaOffset);
	log.debug("paginaNo:: "+paginaNo); 
	log.debug("ic_if:: "+ic_if);   
	log.debug("ic_pyme:: "+ic_pyme);   
	log.debug("ic_moneda:: "+ic_moneda);   
	log.debug("df_fecha_oper_de:: "+df_fecha_oper_de);   
	log.debug("df_fecha_oper_a:: "+df_fecha_oper_a); 
	log.debug("usuario:: "+usuario); 
	log.debug("-------------------------:: "); 
 
	  qrySentencia.append("SELECT count(1) total, 'EstadoAdeudos ::getAggregateCalculationQuery' origen " );
      
		  qrySentencia.append(" FROM dis_documento doc,  "+
                          " dis_solicitud sol,  "+
                          " dis_docto_seleccionado sel,  "+
                          " comrel_pyme_epo cpe,  "+
                          " comcat_pyme pyme ,  "+
                          " comrel_producto_epo pe,  "+
                          " comcat_producto_nafin pn,  "+
                          " comcat_tipo_cobro_interes tc,  "+
                          " com_linea_credito lc,  "+
                          " com_acuse3 ac3,  "+
                          " comcat_estatus_docto ed ");
                          
  qrySentencia.append(" WHERE sol.ic_documento = doc.ic_documento  "+
                      " AND sel.ic_documento = doc.ic_documento  "+
                      " AND cpe.ic_pyme = doc.ic_pyme  "+
                      " AND cpe.ic_epo = doc.ic_epo "+
                      " AND pn.ic_producto_nafin = 4 "+
                      " AND pyme.ic_pyme = doc.ic_pyme "+
                      " AND pn.ic_producto_nafin = pe.ic_producto_nafin "+
                      " AND pe.ic_epo = doc.ic_epo "+
                      " AND lc.ic_tipo_cobro_interes = tc.ic_tipo_cobro_interes "+
                      " AND ac3.cc_acuse = sol.cc_acuse "+
                      " AND ed.ic_estatus_docto = doc.ic_estatus_docto "+
                      " AND lc.ic_linea_credito = doc.ic_linea_credito ");

  if (!usuario.equals("null") ) {
    qrySentencia.append("   AND doc.ic_epo = ? ");
    conditions.add(usuario);
  }
    if (!ic_moneda.equals("null") ){
    qrySentencia.append("   AND doc.ic_moneda = ? ");
    conditions.add(ic_moneda);
  }
  
  if(ic_if!=null) {
        qrySentencia.append(" AND lc.ic_if = ?");
         conditions.add(ic_if);   
    }
    
 // if (df_fecha_oper_de!=null &&  df_fecha_oper_a!=null ){
  if (!df_fecha_oper_de.equals("") &&  !df_fecha_oper_a.equals("") ){
  
   qrySentencia.append("AND ((    doc.ic_estatus_docto = 4 "+
		"         AND TRUNC (ac3.df_fecha_hora) BETWEEN "+
		"                TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND "+
		"                TRUNC (TO_DATE (?, 'dd/mm/yyyy')) "+
		"           ) "+
		"        OR (    doc.ic_estatus_docto IN (11,  22) "+
		"            AND doc.ic_documento IN "+
		"                   (SELECT ic_documento "+
		"                      FROM dis_cambio_estatus "+
		"                     WHERE dc_fecha_cambio IN "+
		"                              (SELECT MAX (dc_fecha_cambio) "+
		"                                 FROM dis_cambio_estatus "+
		"                                WHERE ic_documento = doc.ic_documento "+
		"                                  AND TRUNC (dc_fecha_cambio) BETWEEN "+
		"                                         TRUNC ( "+
		"                                            TO_DATE (?, 'dd/mm/yyyy') "+
		"                                         ) AND "+
		"                                         TRUNC ( "+
		"                                            TO_DATE (?, 'dd/mm/yyyy') "+
		"                                         )) "+
		"                       AND ic_cambio_estatus IN (18,  19,  20)) "+
		"           ) "+
 		"       )");
    conditions.add(df_fecha_oper_de);
    conditions.add(df_fecha_oper_a);
    conditions.add(df_fecha_oper_de);
    conditions.add(df_fecha_oper_a);
   }
    
  if (!ic_pyme.equals("0")) {
      qrySentencia.append("AND doc.ic_pyme = ?"); 
      conditions.add(ic_pyme);
   }
    
		log.debug("getAggregateCalculationQuery "+conditions.toString());
		log.debug("getAggregateCalculationQuery "+qrySentencia.toString());
	
		return qrySentencia.toString();
		
	}
		
	 /**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery(){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
    
	log.debug("paginaOffset:: "+paginaOffset);
	log.debug("paginaNo:: "+paginaNo); 
	log.debug("ic_if:: "+ic_if);   
	log.debug("ic_pyme:: "+ic_pyme);   
	log.debug("ic_moneda:: "+ic_moneda);   
	log.debug("df_fecha_oper_de:: "+df_fecha_oper_de);   
	log.debug("df_fecha_oper_a:: "+df_fecha_oper_a); 
	log.debug("usuario:: "+usuario); 
    
	qrySentencia.append("SELECT doc.ic_documento, TO_CHAR (ac3.df_fecha_hora, 'DD/MM/YYYY') AS fecha_opera, "+
							 "       cpe.cg_num_distribuidor AS num_cliente, "+
							 "       pyme.cg_razon_social AS cliente, "+
							 "       ig_numero_docto AS num_docto, "+
							 "       doc.ic_documento AS num_credito, "+
							 "       TO_CHAR (sol.df_v_credito, 'DD/MM/YYYY') AS f_venc_credito, "+
							 "       doc.fn_monto AS monto_cred, ed.cg_desc_alterna estatus_alt, "+
							 "       sel.fn_importe_interes AS int_generados, "+
							 "       DECODE (doc.ic_estatus_docto, 11, doc.fn_monto, 0) AS pago_a_capital, "+
							 "       '' AS pago_int, "+
							 "       DECODE (doc.ic_estatus_docto,4, doc.fn_monto,22, doc.fn_monto,0) AS saldo_cre, "+
							 "       tc.cd_descripcion AS tipo_cob_int,tc.ic_tipo_cobro_interes,doc.ic_estatus_docto, "+
							 "       sel.fn_importe_recibir AS monto_doc_final ");
	 
	 
			  qrySentencia.append(" FROM dis_documento doc,  "+
								  " dis_solicitud sol,  "+
								  " dis_docto_seleccionado sel,  "+
								  " comrel_pyme_epo cpe,  "+
								  " comcat_pyme pyme ,  "+
								  " comrel_producto_epo pe,  "+
								  " comcat_producto_nafin pn,  "+
								  " comcat_tipo_cobro_interes tc,  "+
								  " com_linea_credito lc,  "+
								  " com_acuse3 ac3,  "+
								  " comcat_estatus_docto ed ");
								  
	qrySentencia.append(" WHERE sol.ic_documento = doc.ic_documento  "+
							 " AND sel.ic_documento = doc.ic_documento  "+
							 " AND cpe.ic_pyme = doc.ic_pyme  "+
							 " AND cpe.ic_epo = doc.ic_epo "+
							 " AND pn.ic_producto_nafin = 4 "+
							 " AND pyme.ic_pyme = doc.ic_pyme "+
							 " AND pn.ic_producto_nafin = pe.ic_producto_nafin "+
							 " AND pe.ic_epo = doc.ic_epo "+
							 " AND lc.ic_tipo_cobro_interes = tc.ic_tipo_cobro_interes "+
							 " AND ac3.cc_acuse = sol.cc_acuse "+
							 " AND ed.ic_estatus_docto = doc.ic_estatus_docto "+
							 " AND lc.ic_linea_credito = doc.ic_linea_credito ");
		
		
		
	if (!usuario.equals("null") ) {
	 qrySentencia.append("   AND doc.ic_epo = ? ");
	 conditions.add(usuario);
	}
	 if (!ic_moneda.equals("null") ){
	 qrySentencia.append("   AND doc.ic_moneda = ? ");
	 conditions.add(ic_moneda);
	}
	
	 if(ic_if!=null) {
		  qrySentencia.append(" AND lc.ic_if = ?");
			conditions.add(ic_if);   
	 }
		 
	//if (df_fecha_oper_de!=null &&  df_fecha_oper_a!=null ){
	if (!df_fecha_oper_de.equals("") &&  !df_fecha_oper_a.equals("") ){
	
	qrySentencia.append("AND ((    doc.ic_estatus_docto = 4 "+
		"         AND TRUNC (ac3.df_fecha_hora) BETWEEN "+
		"                TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND "+
		"                TRUNC (TO_DATE (?, 'dd/mm/yyyy')) "+
		"           ) "+
		"        OR (    doc.ic_estatus_docto IN (11,  22) "+
		"            AND doc.ic_documento IN "+
		"                   (SELECT ic_documento "+
		"                      FROM dis_cambio_estatus "+
		"                     WHERE dc_fecha_cambio IN "+
		"                              (SELECT MAX (dc_fecha_cambio) "+
		"                                 FROM dis_cambio_estatus "+
		"                                WHERE ic_documento = doc.ic_documento "+
		"                                  AND TRUNC (dc_fecha_cambio) BETWEEN "+
		"                                         TRUNC ( "+
		"                                            TO_DATE (?, 'dd/mm/yyyy') "+
		"                                         ) AND "+
		"                                         TRUNC ( "+
		"                                            TO_DATE (?, 'dd/mm/yyyy') "+
		"                                         )) "+
		"                       AND ic_cambio_estatus IN (18,  19,  20)) "+
		"           ) "+
		"       )");
	 conditions.add(df_fecha_oper_de);
	 conditions.add(df_fecha_oper_a);
	 conditions.add(df_fecha_oper_de);
	 conditions.add(df_fecha_oper_a);
	}
	
	
	if (!ic_pyme.equals("0")) {
		qrySentencia.append(" AND doc.ic_pyme = ? ");
		conditions.add(ic_pyme);
	 }
		
			log.debug("getDocumentQuery "+conditions.toString());
			log.debug("getDocumentQuery "+qrySentencia.toString());
	
			return qrySentencia.toString();
	}

	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();

		log.debug("paginaOffset:: "+paginaOffset);
		log.debug("paginaNo:: "+paginaNo); 
		log.debug("ic_if:: "+ic_if);   
		log.debug("ic_pyme:: "+ic_pyme);   
		log.debug("ic_moneda:: "+ic_moneda);   
		log.debug("df_fecha_oper_de:: "+df_fecha_oper_de);   
		log.debug("df_fecha_oper_a:: "+df_fecha_oper_a); 
		log.debug("usuario:: "+usuario); 
    
	 qrySentencia.append("SELECT doc.ic_documento, TO_CHAR (ac3.df_fecha_hora, 'DD/MM/YYYY') AS fecha_opera, "+
							 "       cpe.cg_num_distribuidor AS num_cliente, "+
							 "       pyme.cg_razon_social AS cliente, "+
							 "       ig_numero_docto AS num_docto, "+
							 "       doc.ic_documento AS num_credito, "+
							 "       TO_CHAR (sol.df_v_credito, 'DD/MM/YYYY') AS f_venc_credito, "+
							 "       doc.fn_monto AS monto_cred, ed.cg_desc_alterna estatus_alt, "+
							 "       sel.fn_importe_interes AS int_generados, "+
							 "       DECODE (doc.ic_estatus_docto, 11, doc.fn_monto, 0) AS pago_a_capital, "+
							 "       '' AS pago_int, "+
							 "       DECODE (doc.ic_estatus_docto,4, doc.fn_monto,22, doc.fn_monto,0) AS saldo_cre, "+
							 "       tc.cd_descripcion AS tipo_cob_int,tc.ic_tipo_cobro_interes,doc.ic_estatus_docto, "+
							 "       sel.fn_importe_recibir AS monto_doc_final ");
	 
	 
			  qrySentencia.append(" FROM dis_documento doc,  "+
								  " dis_solicitud sol,  "+
								  " dis_docto_seleccionado sel,  "+
								  " comrel_pyme_epo cpe,  "+
								  " comcat_pyme pyme ,  "+
								  " comrel_producto_epo pe,  "+
								  " comcat_producto_nafin pn,  "+
								  " comcat_tipo_cobro_interes tc,  "+
								  " com_linea_credito lc,  "+
								  " com_acuse3 ac3,  "+
								  " comcat_estatus_docto ed ");
								  
	qrySentencia.append(" WHERE sol.ic_documento = doc.ic_documento  "+
							 " AND sel.ic_documento = doc.ic_documento  "+
							 " AND cpe.ic_pyme = doc.ic_pyme  "+
							 " AND cpe.ic_epo = doc.ic_epo "+
							 " AND pn.ic_producto_nafin = 4 "+
							 " AND pyme.ic_pyme = doc.ic_pyme "+
							 " AND pn.ic_producto_nafin = pe.ic_producto_nafin "+
							 " AND pe.ic_epo = doc.ic_epo "+
							 " AND lc.ic_tipo_cobro_interes = tc.ic_tipo_cobro_interes "+
							 " AND ac3.cc_acuse = sol.cc_acuse "+
							 " AND ed.ic_estatus_docto = doc.ic_estatus_docto "+
							 " AND lc.ic_linea_credito = doc.ic_linea_credito ");
		
		
	if (!usuario.equals("null") ) {
	 qrySentencia.append("   AND doc.ic_epo = ? ");
	 conditions.add(usuario);
	}
	 if (!ic_moneda.equals("null") ){
	 qrySentencia.append("   AND doc.ic_moneda = ? ");
	 conditions.add(ic_moneda);
	}
	
	if(ic_if!=null) {
		  qrySentencia.append(" AND lc.ic_if = ?");
			conditions.add(ic_if);   
	 }
	 
	 
	 
	//if (df_fecha_oper_de!=null &&  df_fecha_oper_a!=null ){
	if (!df_fecha_oper_de.equals("") &&  !df_fecha_oper_a.equals("") ){
	qrySentencia.append("AND ((    doc.ic_estatus_docto = 4 "+
		"         AND TRUNC (ac3.df_fecha_hora) BETWEEN "+
		"                TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND "+
		"                TRUNC (TO_DATE (?, 'dd/mm/yyyy')) "+
		"           ) "+
		"        OR (    doc.ic_estatus_docto IN (11,  22) "+
		"            AND doc.ic_documento IN "+
		"                   (SELECT ic_documento "+
		"                      FROM dis_cambio_estatus "+
		"                     WHERE dc_fecha_cambio IN "+
		"                              (SELECT MAX (dc_fecha_cambio) "+
		"                                 FROM dis_cambio_estatus "+
		"                                WHERE ic_documento = doc.ic_documento "+
		"                                  AND TRUNC (dc_fecha_cambio) BETWEEN "+
		"                                         TRUNC ( "+
		"                                            TO_DATE (?, 'dd/mm/yyyy') "+
		"                                         ) AND "+
		"                                         TRUNC ( "+
		"                                            TO_DATE (?, 'dd/mm/yyyy') "+
		"                                         )) "+
		"                       AND ic_cambio_estatus IN (18,  19,  20)) "+
		"           ) "+
		"       )");
	 conditions.add(df_fecha_oper_de);
	 conditions.add(df_fecha_oper_a);
	 conditions.add(df_fecha_oper_de);
	 conditions.add(df_fecha_oper_a);
	}
	
	 
	 if (!ic_pyme.equals("0")) {
		qrySentencia.append("AND doc.ic_pyme = ? "); 
		conditions.add(ic_pyme);
	 }
	
	 
	 qrySentencia.append("   AND ( ");
		
		for(int i=0;i<pageIds.size();i++) {
			List lItem = (ArrayList)pageIds.get(i);
			if(i>0) {
				qrySentencia.append(" OR ");
			}
			qrySentencia.append(" (doc.ic_documento = ? ) ");
			conditions.add(new Long(lItem.get(0).toString()));
		}//for(int i=0;i<ids.size();i++)
		qrySentencia.append(" ) ");
	 
	 
						  
			log.debug("getDocumentSummaryQueryForIds "+conditions.toString());
			log.debug(" getDocumentSummaryQueryForIds "+qrySentencia.toString());
	
		
		return qrySentencia.toString();
	}
	

	public String getDocumentQueryFile(){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();

		log.debug("paginaOffset:: "+paginaOffset);
		log.debug("paginaNo:: "+paginaNo); 
		log.debug("ic_if:: "+ic_if);   
		log.debug("ic_pyme:: "+ic_pyme);   
		log.debug("ic_moneda:: "+ic_moneda);   
		log.debug("df_fecha_oper_de:: "+df_fecha_oper_de);   
		log.debug("df_fecha_oper_a:: "+df_fecha_oper_a); 
		log.debug("usuario:: "+usuario); 
    
      qrySentencia.append("SELECT doc.ic_documento, TO_CHAR (ac3.df_fecha_hora, 'DD/MM/YYYY') AS fecha_opera, "+
                      "       cpe.cg_num_distribuidor AS num_cliente, "+
                      "       pyme.cg_razon_social AS cliente, "+
                      "       ig_numero_docto AS num_docto, "+
                      "       doc.ic_documento AS num_credito, "+
                      "       TO_CHAR (sol.df_v_credito, 'DD/MM/YYYY') AS f_venc_credito, "+
                      "       doc.fn_monto AS monto_cred, ed.cg_desc_alterna estatus_alt, "+
                      "       sel.fn_importe_interes AS int_generados, "+
                      "       DECODE (doc.ic_estatus_docto, 11, doc.fn_monto, 0) AS pago_a_capital, "+
                      "       '' AS pago_int, "+
                      "       DECODE (doc.ic_estatus_docto,4, doc.fn_monto,22, doc.fn_monto,0) AS saldo_cre, "+
                      "       tc.cd_descripcion AS tipo_cob_int,tc.ic_tipo_cobro_interes,doc.ic_estatus_docto, "+
                      "       sel.fn_importe_recibir AS monto_doc_final ");
    
    
			  qrySentencia.append(" FROM dis_documento doc,  "+
                          " dis_solicitud sol,  "+
                          " dis_docto_seleccionado sel,  "+
                          " comrel_pyme_epo cpe,  "+
                          " comcat_pyme pyme ,  "+
                          " comrel_producto_epo pe,  "+
                          " comcat_producto_nafin pn,  "+
                          " comcat_tipo_cobro_interes tc,  "+
                          " com_linea_credito lc,  "+
                          " com_acuse3 ac3,  "+
                          " comcat_estatus_docto ed ");
                          
  qrySentencia.append(" WHERE sol.ic_documento = doc.ic_documento  "+
                      " AND sel.ic_documento = doc.ic_documento  "+
                      " AND cpe.ic_pyme = doc.ic_pyme  "+
                      " AND cpe.ic_epo = doc.ic_epo "+
                      " AND pn.ic_producto_nafin = 4 "+
                      " AND pyme.ic_pyme = doc.ic_pyme "+
                      " AND pn.ic_producto_nafin = pe.ic_producto_nafin "+
                      " AND pe.ic_epo = doc.ic_epo "+
                      " AND lc.ic_tipo_cobro_interes = tc.ic_tipo_cobro_interes "+
                      " AND ac3.cc_acuse = sol.cc_acuse "+
                      " AND ed.ic_estatus_docto = doc.ic_estatus_docto "+
                      " AND lc.ic_linea_credito = doc.ic_linea_credito ");
      
      
      
  if (!usuario.equals("null") ) {
    qrySentencia.append("   AND doc.ic_epo = ? ");
    conditions.add(usuario);
  }
    if (!ic_moneda.equals("null") ){
    qrySentencia.append("   AND doc.ic_moneda = ? ");
    conditions.add(ic_moneda);
  }
  
  if(ic_if!=null) {
        qrySentencia.append(" AND lc.ic_if = ?");
         conditions.add(ic_if);   
    }
    
  //if (df_fecha_oper_de!=null &&  df_fecha_oper_a!=null ){
   if (!df_fecha_oper_de.equals("") &&  !df_fecha_oper_a.equals("") ){
   qrySentencia.append("AND ((    doc.ic_estatus_docto = 4 "+
		"         AND TRUNC (ac3.df_fecha_hora) BETWEEN "+
		"                TRUNC (TO_DATE (?, 'dd/mm/yyyy')) AND "+
		"                TRUNC (TO_DATE (?, 'dd/mm/yyyy')) "+
		"           ) "+
		"        OR (    doc.ic_estatus_docto IN (11,  22) "+
		"            AND doc.ic_documento IN "+
		"                   (SELECT ic_documento "+
		"                      FROM dis_cambio_estatus "+
		"                     WHERE dc_fecha_cambio IN "+
		"                              (SELECT MAX (dc_fecha_cambio) "+
		"                                 FROM dis_cambio_estatus "+
		"                                WHERE ic_documento = doc.ic_documento "+
		"                                  AND TRUNC (dc_fecha_cambio) BETWEEN "+
		"                                         TRUNC ( "+
		"                                            TO_DATE (?, 'dd/mm/yyyy') "+
		"                                         ) AND "+
		"                                         TRUNC ( "+
		"                                            TO_DATE (?, 'dd/mm/yyyy') "+
		"                                         )) "+
		"                       AND ic_cambio_estatus IN (18,  19,  20)) "+
		"           ) "+
 		"       )");
    conditions.add(df_fecha_oper_de);
    conditions.add(df_fecha_oper_a);
    conditions.add(df_fecha_oper_de);
    conditions.add(df_fecha_oper_a);
   }
       
    if (!ic_pyme.equals("0")) {
      qrySentencia.append("AND doc.ic_pyme = ? "); 
      conditions.add(ic_pyme);
    }
		
		
			log.debug("getDocumentQueryFile "+conditions.toString());
			log.debug("getDocumentQueryFile "+qrySentencia.toString());
		
		return qrySentencia.toString();
	}//getDocumentQueryFile
	
  	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
		log.info("crearCustomFile (E)");
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		String rs_fecha_opera="",rs_num_cliente="",rs_cliente="",rs_num_docto="",rs_num_credito="",rs_f_venc_credito="",rs_monto_cred="";
		String rs_Estatus = "",rs_int_generados="",rs_pago_a_capital="",rs_pago_int="",rs_saldo_cre="",rs_tipo_cob_int="";
		String totPagado = "", rs_ic_tipo_cob_int="",rs_ic_estatus_docto="",rs_monto_doc_final="0";	
		int totalregMN=0;
		double totalMontoCreMN=0;
		double totalMontoFinalMN=0;
		double totalIntMN=0;
		double totalPagoMN=0;
		double totalPagadoMN=0;
		double totalSaldoMN=0;
		int totalregUSD=0;
		double totalMontoCreUSD=0;
		double totalMontoFinalUSD=0;
		double totalIntUSD=0;
		double totalPagoUSD=0;
		double totalPagadoUSD=0;
		double totalSaldoUSD=0;

	if ("CSV".equals(tipo)) {
		try {
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
			buffer = new BufferedWriter(writer);

			Vector vecColumnas = null;
			if (datosVec.size()>0 ){
				for(int i=0;i<datosVec.size();i++){
				  vecColumnas = (Vector)datosVec.get(i);
				  linea = "No. Cuenta"+","+(String)vecColumnas.get(0)+"\n"+
				  "Banco de referencia"+","+(String)vecColumnas.get(1)+"\n"+
				  "Monto de la l�nea autorizada"+","+(String)vecColumnas.get(2)+"\n"+
				  "Fecha de Vencimiento"+","+(String)vecColumnas.get(3)+"\n"+
				  "nMoneda"+","+(String)vecColumnas.get(4)+"\n"+
				  "Monto dispuesto"+","+(String)vecColumnas.get(5)+"\n"+
				  "Saldo disponible"+","+(String)vecColumnas.get(6)+"\n"+
				  "Estatus"+","+(String)vecColumnas.get(7)+"\n";
				  buffer.write(linea);
				}
			}else{
				linea =	"No. Cuenta"+"\n Banco de referencia"+"\n Monto de la l�nea autorizada"+"\n Fecha de Vencimiento"+
							"\n nMoneda"+"\n Monto dispuesto"+"\n Saldo disponible"+"\n Estatus"+"\n";
				buffer.write(linea);
			}

			linea = "Res�men estado de adeudos\n";
			buffer.write(linea);
			int numRegistros = 0;
			while (rs.next()) {
				numRegistros++;
				rs_fecha_opera	 	= (rs.getString("FECHA_OPERA")==null)?"":rs.getString("FECHA_OPERA");
				rs_num_cliente	 	= (rs.getString("NUM_CLIENTE")==null)?"":rs.getString("NUM_CLIENTE");
				rs_cliente	 	=	(rs.getString("CLIENTE")==null)?"":rs.getString("CLIENTE");
				rs_num_docto	 	= (rs.getString("NUM_DOCTO")==null)?"":rs.getString("NUM_DOCTO");
				rs_num_credito	 	= (rs.getString("NUM_CREDITO")==null)?"":rs.getString("NUM_CREDITO");
				rs_f_venc_credito	= (rs.getString("F_VENC_CREDITO")==null)?"":rs.getString("F_VENC_CREDITO");
				rs_monto_cred	 	= (rs.getString("MONTO_CRED")==null)?"":rs.getString("MONTO_CRED");
				rs_int_generados	= (rs.getString("INT_GENERADOS")==null)?"":rs.getString("INT_GENERADOS");
				rs_pago_a_capital	= (rs.getString("PAGO_A_CAPITAL")==null)?"":rs.getString("PAGO_A_CAPITAL");
				rs_pago_int		 	= (rs.getString("PAGO_INT")==null)?"":rs.getString("PAGO_INT");
				rs_saldo_cre	 	= (rs.getString("SALDO_CRE")==null)?"":rs.getString("SALDO_CRE");
				rs_tipo_cob_int	 	= (rs.getString("TIPO_COB_INT")==null)?"":rs.getString("TIPO_COB_INT");
				rs_Estatus			= (rs.getString("ESTATUS_ALT")==null)?"":rs.getString("ESTATUS_ALT");
				rs_ic_tipo_cob_int 	= (rs.getString("IC_TIPO_COBRO_INTERES")==null)?"":rs.getString("IC_TIPO_COBRO_INTERES");
				rs_ic_estatus_docto	= (rs.getString("IC_ESTATUS_DOCTO")==null)?"":rs.getString("IC_ESTATUS_DOCTO");
				rs_monto_doc_final= (rs.getString("MONTO_DOC_FINAL")==null)?"0":rs.getString("MONTO_DOC_FINAL");

				if("1".equals(rs_ic_tipo_cob_int)){  //Pago Anticipado
					rs_pago_int = rs_int_generados;
				}else {
					if("2".equals(rs_ic_tipo_cob_int)){  //Pago al Vencimiento
						if("11".equals(rs_ic_estatus_docto)){
							rs_pago_int = rs_int_generados;
						}
						else{
							rs_pago_int = "0";
						}
					}
				}
				 
				totPagado = (Double.parseDouble(rs_pago_a_capital)+Double.parseDouble(rs_pago_int))==0?"0":""+(Double.parseDouble(rs_pago_a_capital)+Double.parseDouble(rs_pago_int));		
				rs_int_generados = "".equals(rs_int_generados)?"":"$ "+Comunes.formatoMoneda2(rs_int_generados,false);
				
				if ("1".equals(ic_moneda)){
					totalregMN++;
					if (!"".equals(rs_monto_cred)){
						totalMontoCreMN += Double.parseDouble(rs_monto_cred);
						totalMontoFinalMN += Double.parseDouble(rs_monto_doc_final);
					}
					if (!"".equals(rs_pago_int)){
						totalIntMN += Double.parseDouble(rs_pago_int);
						rs_pago_int = "0".equals(rs_pago_int)?"":"$ "+Comunes.formatoDecimal(rs_pago_int,2);
					}
					if (!"".equals(rs_pago_a_capital)){
						totalPagoMN += Double.parseDouble(rs_pago_a_capital);
						rs_pago_a_capital = "0".equals(rs_pago_a_capital)?"":"$ "+Comunes.formatoMoneda2(rs_pago_a_capital,false);
					}
					if (!"0".equals(totPagado)){
						totalPagadoMN += Double.parseDouble(totPagado);       
					}
					if (!"".equals(rs_saldo_cre)){
						totalSaldoMN += Double.parseDouble(rs_saldo_cre);
						rs_saldo_cre = "0".equals(rs_saldo_cre)?"":"$ "+Comunes.formatoMoneda2(rs_saldo_cre,false);
					}
				} else {
				if("54".equals(ic_moneda)){
					totalregUSD++;
					if (!"".equals(rs_monto_cred)){
						totalMontoCreUSD += Double.parseDouble(rs_monto_cred);
						totalMontoFinalUSD += Double.parseDouble(rs_monto_doc_final);
					}
					if (!"".equals(rs_pago_int)){
						totalIntUSD += Double.parseDouble(rs_pago_int);
						rs_pago_int = "0".equals(rs_pago_int)?"":"$ "+Comunes.formatoDecimal(rs_pago_int,2);
					}
					if (!"".equals(rs_pago_a_capital)){
						totalPagoUSD += Double.parseDouble(rs_pago_a_capital);
						rs_pago_a_capital = "0".equals(rs_pago_a_capital)?"":"$ "+Comunes.formatoMoneda2(rs_pago_a_capital,false);
					}
					if (!"".equals(totPagado)){
						totalPagadoUSD += Double.parseDouble(totPagado);
					}
					if (!"".equals(rs_saldo_cre)){
						totalSaldoUSD += Double.parseDouble(rs_saldo_cre);
						rs_saldo_cre = "0".equals(rs_saldo_cre)?"":"$ "+Comunes.formatoMoneda2(rs_saldo_cre,false);
					}
				}
				}
				totPagado = "0".equals(totPagado)?"":"$ "+Comunes.formatoMoneda2(totPagado,false);						

				if (numRegistros == 1){
					linea = "\nMovimientos del "+ df_fecha_oper_de +" al "+ df_fecha_oper_a +
								"\nFecha de operaci�n,Num. cliente,Nombre,Num. de docto. inicial,Monto docto. inicial,Num. de docto. final,Monto docto. final,Fecha de vencimiento final,Estatus,Intereses generados,Capital pagado,Intereses Pagados,Total Pagado,Saldo del cr�dito,Tipo de cobro de intereses";
					buffer.write(linea);
				}
				linea = "\n"+rs_fecha_opera+","+rs_num_cliente.replace(',',' ')+","+rs_cliente.replace(',',' ')+","+rs_num_docto.replace(',',' ')+","+rs_monto_cred.replace(',',' ')+","+rs_num_credito.replace(',',' ')+
							","+rs_monto_doc_final.replace(',',' ')+","+rs_f_venc_credito.replace(',',' ')+","+rs_Estatus.replace(',',' ')+","+rs_int_generados.replace(',',' ')+
							","+rs_pago_a_capital.replace(',',' ')+","+rs_pago_int.replace(',',' ')+","+totPagado.replace(',',' ')+","+rs_saldo_cre.replace(',',' ')+","+rs_tipo_cob_int.replace(',',' ');
				buffer.write(linea);
			}
			if (totalregMN>0) {
				linea = "\nTotal M.N.,"+totalregMN+",,,"+totalMontoCreMN+",,"+totalMontoFinalMN+",,,,"+totalPagoMN+","+totalIntMN+","+totalPagadoMN+","+totalSaldoMN;
				buffer.write(linea);
			}
			if (totalregUSD>0)  {
				linea = "\nTotal Dolares,"+totalregUSD+",,,"+totalMontoCreUSD+",,"+totalMontoFinalUSD+",,,,"+totalPagoUSD+","+totalIntUSD+","+totalPagoUSD+","+totalSaldoUSD;
				buffer.write(linea);
			}
			buffer.close();
		}catch (Throwable e)	{
			throw new AppException("Error al generar el archivo", e);
		}finally{
			try {
				rs.close();
			}catch(Exception e) {}
		}

	} if ("PDF".equals(tipo)){
		try {
			HttpSession session = request.getSession();
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			int nRow = 0;
			ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);

			while (rs.next()){
				rs_fecha_opera      = (rs.getString("FECHA_OPERA")           ==null)?"" :rs.getString("FECHA_OPERA");
				rs_num_cliente      = (rs.getString("NUM_CLIENTE")           ==null)?"" :rs.getString("NUM_CLIENTE");
				rs_cliente          = (rs.getString("CLIENTE")               ==null)?"" :rs.getString("CLIENTE");
				rs_num_docto        = (rs.getString("NUM_DOCTO")             ==null)?"" :rs.getString("NUM_DOCTO");
				rs_num_credito      = (rs.getString("NUM_CREDITO")           ==null)?"" :rs.getString("NUM_CREDITO");
				rs_f_venc_credito   = (rs.getString("F_VENC_CREDITO")        ==null)?"" :rs.getString("F_VENC_CREDITO");
				rs_monto_cred       = (rs.getString("MONTO_CRED")            ==null)?"" :rs.getString("MONTO_CRED");
				rs_int_generados    = (rs.getString("INT_GENERADOS")         ==null)?"" :rs.getString("INT_GENERADOS");
				rs_pago_a_capital   = (rs.getString("PAGO_A_CAPITAL")        ==null)?"" :rs.getString("PAGO_A_CAPITAL");
				rs_pago_int         = (rs.getString("PAGO_INT")              ==null)?"" :rs.getString("PAGO_INT");
				rs_saldo_cre        = (rs.getString("SALDO_CRE")             ==null)?"" :rs.getString("SALDO_CRE");
				rs_tipo_cob_int     = (rs.getString("TIPO_COB_INT")          ==null)?"" :rs.getString("TIPO_COB_INT");
				rs_Estatus          = (rs.getString("ESTATUS_ALT")           ==null)?"" :rs.getString("ESTATUS_ALT");
				rs_ic_tipo_cob_int  = (rs.getString("IC_TIPO_COBRO_INTERES") ==null)?"" :rs.getString("IC_TIPO_COBRO_INTERES");
				rs_ic_estatus_docto = (rs.getString("IC_ESTATUS_DOCTO")      ==null)?"" :rs.getString("IC_ESTATUS_DOCTO");
				rs_monto_doc_final  = (rs.getString("MONTO_DOC_FINAL")       ==null)?"0":rs.getString("MONTO_DOC_FINAL");

				if("1".equals(rs_ic_tipo_cob_int)){  //Pago Anticipado
					rs_pago_int = rs_int_generados;
				}else {
					if("2".equals(rs_ic_tipo_cob_int)){  //Pago al Vencimiento
						if("11".equals(rs_ic_estatus_docto)){
							rs_pago_int = rs_int_generados;
						}
						else{
							rs_pago_int = "0";
						}
					}
				}

				totPagado = (Double.parseDouble(rs_pago_a_capital)+Double.parseDouble(rs_pago_int))==0?"0":""+(Double.parseDouble(rs_pago_a_capital)+Double.parseDouble(rs_pago_int));		

				if ("1".equals(ic_moneda)){
					totalregMN++;
					if (!"".equals(rs_monto_cred)){
						totalMontoCreMN += Double.parseDouble(rs_monto_cred);
						totalMontoFinalMN += Double.parseDouble(rs_monto_doc_final);
					}
					if (!"".equals(rs_pago_int)){
						totalIntMN += Double.parseDouble(rs_pago_int);
					}
					if (!"".equals(rs_pago_a_capital)){
						totalPagoMN += Double.parseDouble(rs_pago_a_capital);
					}
					if (!"0".equals(totPagado)){
						totalPagadoMN += Double.parseDouble(totPagado);
					}
					if (!"".equals(rs_saldo_cre)){
						totalSaldoMN += Double.parseDouble(rs_saldo_cre);
					}
				} else {
					if("54".equals(ic_moneda)){
						totalregUSD++;
						if (!"".equals(rs_monto_cred)){
							totalMontoCreUSD += Double.parseDouble(rs_monto_cred);
							totalMontoFinalUSD += Double.parseDouble(rs_monto_doc_final);
						}
						if (!"".equals(rs_pago_int)){
							totalIntUSD += Double.parseDouble(rs_pago_int);
						}
						if (!"".equals(rs_pago_a_capital)){
							totalPagoUSD += Double.parseDouble(rs_pago_a_capital);
						}
						if (!"".equals(totPagado)){
							totalPagadoUSD += Double.parseDouble(totPagado);
						}
						if (!"".equals(rs_saldo_cre)){
							totalSaldoUSD += Double.parseDouble(rs_saldo_cre);
						}
					}
				}
						
				if(nRow == 0){
					String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
					String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
					String diaActual    = fechaActual.substring(0,2);
					String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
					String anioActual   = fechaActual.substring(6,10);
					String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
					pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					session.getAttribute("iNoNafinElectronico").toString(),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
					
					pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
					pdfDoc.addText(" ","formas", ComunesPDF.CENTER);

					Vector vecColumnas = null;
					if (datosVec.size()>0 ){
						List cuenta		= new ArrayList();
						List banco_ref	= new ArrayList();
						List monto_auto= new ArrayList();
						List fecha_venc= new ArrayList();
						List moneda		= new ArrayList();
						List dispuesto	= new ArrayList();
						List disponible= new ArrayList();
						List estatus	= new ArrayList();
						int i=0;
						for(i=0;i<datosVec.size();i++){
							vecColumnas = (Vector)datosVec.get(i);
							cuenta.add((String)vecColumnas.get(0));
							banco_ref.add((String)vecColumnas.get(1));
							monto_auto.add((String)vecColumnas.get(2));
							fecha_venc.add((String)vecColumnas.get(3));
							moneda.add((String)vecColumnas.get(4));
							dispuesto.add((String)vecColumnas.get(5));
							disponible.add((String)vecColumnas.get(6));
							estatus.add((String)vecColumnas.get(7));
						}
						int nCol = cuenta.size()+1;
						Iterator iter;
						pdfDoc.setLTable(nCol, 80);
						pdfDoc.setLCell("Resumen de l�nea de cr�dito", "formasmenB", ComunesPDF.CENTER,nCol);
						iter = cuenta.iterator();
						i = 0;
						while (iter.hasNext()){
							if (i == 0){
								pdfDoc.setLCell("No. Cuenta:", "formasmenB", ComunesPDF.LEFT);
							}
							pdfDoc.setLCell((String)iter.next(), "formas", ComunesPDF.CENTER);
							i++;
						}
						iter = banco_ref.iterator();
						i = 0;
						while (iter.hasNext()){
							if (i == 0){
								pdfDoc.setLCell("Banco de Referencia:", "formasmenB", ComunesPDF.LEFT);
							}
							pdfDoc.setLCell((String)iter.next(), "formas", ComunesPDF.CENTER);
							i++;
						}
						iter = monto_auto.iterator();
						i = 0;
						while (iter.hasNext()){
							if (i == 0){
								pdfDoc.setLCell("Monto de la l�nea autorizada:", "formasmenB", ComunesPDF.LEFT);
							}
							pdfDoc.setLCell("$ "+Comunes.formatoDecimal((String)iter.next(),2), "formas", ComunesPDF.CENTER);
							i++;
						}
						iter = fecha_venc.iterator();
						i = 0;
						while (iter.hasNext()){
							if (i == 0){
								pdfDoc.setLCell("Fecha de Vencimiento:", "formasmenB", ComunesPDF.LEFT);
							}
							pdfDoc.setLCell((String)iter.next(), "formas", ComunesPDF.CENTER);
							i++;
						}
						iter = moneda.iterator();
						i = 0;
						while (iter.hasNext()){
							if (i == 0){
								pdfDoc.setLCell("Moneda:", "formasmenB", ComunesPDF.LEFT);
							}
							pdfDoc.setLCell((String)iter.next(), "formas", ComunesPDF.CENTER);
							i++;
						}
						iter = dispuesto.iterator();
						i = 0;
						while (iter.hasNext()){
							if (i == 0){
								pdfDoc.setLCell("Monto dispuesto:", "formasmenB", ComunesPDF.LEFT);
							}
							pdfDoc.setLCell("$ "+Comunes.formatoDecimal((String)iter.next(),2), "formas", ComunesPDF.CENTER);
							i++;
						}
						iter = disponible.iterator();
						i = 0;
						while (iter.hasNext()){
							if (i == 0){
								pdfDoc.setLCell("Saldo disponible:", "formasmenB", ComunesPDF.LEFT);
							}
							pdfDoc.setLCell("$ "+Comunes.formatoDecimal((String)iter.next(),2), "formas", ComunesPDF.CENTER);
							i++;
						}
						iter = estatus.iterator();
						i = 0;
						while (iter.hasNext()){
							if (i == 0){
								pdfDoc.setLCell("Estatus:", "formasmenB", ComunesPDF.LEFT);
							}
							pdfDoc.setLCell((String)iter.next(), "formas", ComunesPDF.CENTER);
							i++;
						}
						pdfDoc.addLTable();
					}
					pdfDoc.addText(" ","formas", ComunesPDF.CENTER);
					pdfDoc.setLTable(14, 100);
					pdfDoc.setLCell("Fecha de operaci�n", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Cliente", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("No. docto. inicial", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto docto. inicial", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("No. docto. final", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto docto. final", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Fecha vencimiento final", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Estatus", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Intereses generados", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Capital pagado", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Intereses Pagados", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Total Pagado", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Saldo del Cr�dito", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Tipo Cobro Intereses", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLHeaders();
				}
				pdfDoc.setLCell(rs_fecha_opera, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setLCell(rs_cliente, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setLCell(rs_num_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(rs_monto_cred,2),"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setLCell(rs_num_credito, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(rs_monto_doc_final,2),"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setLCell(rs_f_venc_credito, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setLCell(rs_Estatus, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setLCell("".equals(rs_int_generados)?"":"$ "+Comunes.formatoDecimal(rs_int_generados,2),"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setLCell("".equals(rs_pago_a_capital)?"":"$ "+Comunes.formatoDecimal(rs_pago_a_capital,2),"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setLCell("".equals(rs_pago_int)?"":"$ "+Comunes.formatoDecimal(rs_pago_int,2),"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setLCell("".equals(totPagado)?"":"$ "+Comunes.formatoDecimal(totPagado,2),"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setLCell("".equals(rs_saldo_cre)?"":"$ "+Comunes.formatoDecimal(rs_saldo_cre,2),"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setLCell(rs_tipo_cob_int, "formasmen", ComunesPDF.CENTER);
				nRow++;
			}
			if(totalregMN>0){
				pdfDoc.setLCell("Total M.N.", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setLCell("$ "+totalregMN,"formasmenB",ComunesPDF.RIGHT,2);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(totalMontoCreMN,2),"formasmenB",ComunesPDF.RIGHT);
				pdfDoc.setLCell(" ","formasmenB",ComunesPDF.RIGHT);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(totalMontoFinalMN,2),"formasmenB",ComunesPDF.RIGHT);
				pdfDoc.setLCell(" ","formasmenB",ComunesPDF.RIGHT,3);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(totalPagoMN,2),"formasmenB",ComunesPDF.RIGHT);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(totalIntMN,2),"formasmenB",ComunesPDF.RIGHT);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(totalPagadoMN,2),"formasmenB",ComunesPDF.RIGHT);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(totalSaldoMN,2),"formasmenB",ComunesPDF.RIGHT);
				pdfDoc.setLCell(" ","formasmenB",ComunesPDF.RIGHT);
			}
			if(totalregUSD>0){
				pdfDoc.setLCell("Total Dolares", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setLCell("$ "+totalregUSD,"formasmenB",ComunesPDF.RIGHT,2);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(totalMontoCreUSD,2),"formasmenB",ComunesPDF.RIGHT);
				pdfDoc.setLCell(" ","formasmenB",ComunesPDF.RIGHT);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(totalMontoFinalUSD,2),"formasmenB",ComunesPDF.RIGHT);
				pdfDoc.setLCell(" ","formasmenB",ComunesPDF.RIGHT,3);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(totalPagoUSD,2),"formasmenB",ComunesPDF.RIGHT);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(totalIntUSD,2),"formasmenB",ComunesPDF.RIGHT);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(totalPagadoUSD,2),"formasmenB",ComunesPDF.RIGHT);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(totalSaldoUSD,2),"formasmenB",ComunesPDF.RIGHT);
				pdfDoc.setLCell(" ","formasmenB",ComunesPDF.RIGHT);
			}
			pdfDoc.addLTable();
			pdfDoc.endDocument();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo", e);
		} finally {
		}
	}
	log.info("crearCustomFile (S)");
	return nombreArchivo;
	}

	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		String nombreArchivo			=	"";
		String rs_fecha_opera="",rs_num_cliente="",rs_cliente="",rs_num_docto="",rs_num_credito="",rs_f_venc_credito="",rs_monto_cred="";
		String rs_Estatus = "",rs_int_generados="",rs_pago_a_capital="",rs_pago_int="",rs_saldo_cre="",rs_tipo_cob_int="";
		String totPagado = "", rs_ic_tipo_cob_int="",rs_ic_estatus_docto="",rs_monto_doc_final="0";	
		int 		totalregMN=0;
		double		totalMontoCreMN=0;
		double		totalMontoFinalMN=0;
		double		totalIntMN=0;
		double		totalPagoMN=0;
		double		totalPagadoMN=0;
		double		totalSaldoMN=0;
		int 		totalregUSD=0;
		double		totalMontoCreUSD=0;
		double		totalMontoFinalUSD=0;
		double		totalIntUSD=0;
		double		totalPagoUSD=0;
		double		totalPagadoUSD=0;
		double		totalSaldoUSD=0;

		if ("PDF".equals(tipo)) {
		try {
			HttpSession session = request.getSession();
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			int nRow = 0;
			ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);

			while (reg.next()) {
				rs_fecha_opera	 	= (reg.getString("FECHA_OPERA")==null)?"":reg.getString("FECHA_OPERA");
				rs_num_cliente	 	= (reg.getString("NUM_CLIENTE")==null)?"":reg.getString("NUM_CLIENTE");
				rs_cliente	 		=	(reg.getString("CLIENTE")==null)?"":reg.getString("CLIENTE");
				rs_num_docto	 	= (reg.getString("NUM_DOCTO")==null)?"":reg.getString("NUM_DOCTO");
				rs_num_credito	 	= (reg.getString("NUM_CREDITO")==null)?"":reg.getString("NUM_CREDITO");
				rs_f_venc_credito	= (reg.getString("F_VENC_CREDITO")==null)?"":reg.getString("F_VENC_CREDITO");
				rs_monto_cred	 	= (reg.getString("MONTO_CRED")==null)?"":reg.getString("MONTO_CRED");
				rs_int_generados	= (reg.getString("INT_GENERADOS")==null)?"":reg.getString("INT_GENERADOS");
				rs_pago_a_capital	= (reg.getString("PAGO_A_CAPITAL")==null)?"":reg.getString("PAGO_A_CAPITAL");
				rs_pago_int		 	= (reg.getString("PAGO_INT")==null)?"":reg.getString("PAGO_INT");
				rs_saldo_cre	 	= (reg.getString("SALDO_CRE")==null)?"":reg.getString("SALDO_CRE");
				rs_tipo_cob_int	= (reg.getString("TIPO_COB_INT")==null)?"":reg.getString("TIPO_COB_INT");
				rs_Estatus			= (reg.getString("ESTATUS_ALT")==null)?"":reg.getString("ESTATUS_ALT");
				rs_ic_tipo_cob_int 	= (reg.getString("IC_TIPO_COBRO_INTERES")==null)?"":reg.getString("IC_TIPO_COBRO_INTERES");
				rs_ic_estatus_docto	= (reg.getString("IC_ESTATUS_DOCTO")==null)?"":reg.getString("IC_ESTATUS_DOCTO");
				rs_monto_doc_final= (reg.getString("MONTO_DOC_FINAL")==null)?"0":reg.getString("MONTO_DOC_FINAL");

				if("1".equals(rs_ic_tipo_cob_int)){  //Pago Anticipado
					rs_pago_int = rs_int_generados;
				}else {
					if("2".equals(rs_ic_tipo_cob_int)){  //Pago al Vencimiento
						if("11".equals(rs_ic_estatus_docto)){
							rs_pago_int = rs_int_generados;
						}
						else{
							rs_pago_int = "0";
						}
					}
				}

				totPagado = (Double.parseDouble(rs_pago_a_capital)+Double.parseDouble(rs_pago_int))==0?"0":""+(Double.parseDouble(rs_pago_a_capital)+Double.parseDouble(rs_pago_int));		
				
				if ("1".equals(ic_moneda)){
					totalregMN++;
					if (!"".equals(rs_monto_cred)){
						totalMontoCreMN += Double.parseDouble(rs_monto_cred);
						totalMontoFinalMN += Double.parseDouble(rs_monto_doc_final);
					}
					if (!"".equals(rs_pago_int)){
						totalIntMN += Double.parseDouble(rs_pago_int);
					}
					if (!"".equals(rs_pago_a_capital)){
						totalPagoMN += Double.parseDouble(rs_pago_a_capital);
					}
					if (!"0".equals(totPagado)){
						totalPagadoMN += Double.parseDouble(totPagado);       
					}
					if (!"".equals(rs_saldo_cre)){
						totalSaldoMN += Double.parseDouble(rs_saldo_cre);
					}
				} else {
					if("54".equals(ic_moneda)){
						totalregUSD++;
						if (!"".equals(rs_monto_cred)){
							totalMontoCreUSD += Double.parseDouble(rs_monto_cred);
							totalMontoFinalUSD += Double.parseDouble(rs_monto_doc_final);
						}
						if (!"".equals(rs_pago_int)){
							totalIntUSD += Double.parseDouble(rs_pago_int);
						}
						if (!"".equals(rs_pago_a_capital)){
							totalPagoUSD += Double.parseDouble(rs_pago_a_capital);
						}
						if (!"".equals(totPagado)){
							totalPagadoUSD += Double.parseDouble(totPagado);
						}
						if (!"".equals(rs_saldo_cre)){
							totalSaldoUSD += Double.parseDouble(rs_saldo_cre);
						}
					}
				}
						
				if(nRow == 0){
					String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
					String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
					String diaActual    = fechaActual.substring(0,2);
					String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
					String anioActual   = fechaActual.substring(6,10);
					String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
					pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					session.getAttribute("iNoNafinElectronico").toString(),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
					
					pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
					pdfDoc.addText(" ","formas", ComunesPDF.CENTER);

					Vector vecColumnas = null;
					if (datosVec.size()>0 ){
						List cuenta		= new ArrayList();
						List banco_ref	= new ArrayList();
						List monto_auto= new ArrayList();
						List fecha_venc= new ArrayList();
						List moneda		= new ArrayList();
						List dispuesto	= new ArrayList();
						List disponible= new ArrayList();
						List estatus	= new ArrayList();
						int i=0;
						for(i=0;i<datosVec.size();i++){
							vecColumnas = (Vector)datosVec.get(i);
							cuenta.add((String)vecColumnas.get(0));
							banco_ref.add((String)vecColumnas.get(1));
							monto_auto.add((String)vecColumnas.get(2));
							fecha_venc.add((String)vecColumnas.get(3));
							moneda.add((String)vecColumnas.get(4));
							dispuesto.add((String)vecColumnas.get(5));
							disponible.add((String)vecColumnas.get(6));
							estatus.add((String)vecColumnas.get(7));
						}
						int nCol = cuenta.size()+1;
						Iterator iter;
						pdfDoc.setLTable(nCol, 80);
						pdfDoc.setLCell("Resumen de l�nea de cr�dito", "formasmenB", ComunesPDF.CENTER,nCol);
						iter = cuenta.iterator();
						i = 0;
						while (iter.hasNext()){
							if (i == 0){
								pdfDoc.setLCell("No. Cuenta:", "formasmenB", ComunesPDF.LEFT);
							}
							pdfDoc.setLCell((String)iter.next(), "formas", ComunesPDF.CENTER);
							i++;
						}
						iter = banco_ref.iterator();
						i = 0;
						while (iter.hasNext()){
							if (i == 0){
								pdfDoc.setLCell("Banco de Referencia:", "formasmenB", ComunesPDF.LEFT);
							}
							pdfDoc.setLCell((String)iter.next(), "formas", ComunesPDF.CENTER);
							i++;
						}
						iter = monto_auto.iterator();
						i = 0;
						while (iter.hasNext()){
							if (i == 0){
								pdfDoc.setLCell("Monto de la l�nea autorizada:", "formasmenB", ComunesPDF.LEFT);
							}
							pdfDoc.setLCell((String)iter.next(), "formas", ComunesPDF.CENTER);
							i++;
						}
						iter = fecha_venc.iterator();
						i = 0;
						while (iter.hasNext()){
							if (i == 0){
								pdfDoc.setLCell("Fecha de Vencimiento:", "formasmenB", ComunesPDF.LEFT);
							}
							pdfDoc.setLCell((String)iter.next(), "formas", ComunesPDF.CENTER);
							i++;
						}
						iter = moneda.iterator();
						i = 0;
						while (iter.hasNext()){
							if (i == 0){
								pdfDoc.setLCell("Moneda:", "formasmenB", ComunesPDF.LEFT);
							}
							pdfDoc.setLCell((String)iter.next(), "formas", ComunesPDF.CENTER);
							i++;
						}
						iter = dispuesto.iterator();
						i = 0;
						while (iter.hasNext()){
							if (i == 0){
								pdfDoc.setLCell("Monto dispuesto:", "formasmenB", ComunesPDF.LEFT);
							}
							pdfDoc.setLCell((String)iter.next(), "formas", ComunesPDF.CENTER);
							i++;
						}
						iter = disponible.iterator();
						i = 0;
						while (iter.hasNext()){
							if (i == 0){
								pdfDoc.setLCell("Saldo disponible:", "formasmenB", ComunesPDF.LEFT);
							}
							pdfDoc.setLCell((String)iter.next(), "formas", ComunesPDF.CENTER);
							i++;
						}
						iter = estatus.iterator();
						i = 0;
						while (iter.hasNext()){
							if (i == 0){
								pdfDoc.setLCell("Estatus:", "formasmenB", ComunesPDF.LEFT);
							}
							pdfDoc.setLCell((String)iter.next(), "formas", ComunesPDF.CENTER);
							i++;
						}
						pdfDoc.addLTable();
					}
					pdfDoc.setLTable(14, 100);
					pdfDoc.setLCell("Fecha de operaci�n", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Cliente", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("No. docto. inicial", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto docto. inicial", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("No. docto. final", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto docto. final", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Fecha vencimiento final", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Estatus", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Intereses generados", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Capital pagado", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Intereses Pagados", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Total Pagado", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Saldo del Cr�dito", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Tipo Cobro Intereses", "formasmenB", ComunesPDF.CENTER);
				}
				pdfDoc.setLCell(rs_fecha_opera, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setLCell(rs_cliente, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setLCell(rs_num_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(rs_monto_cred,2),"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setLCell(rs_num_credito, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(rs_monto_doc_final,2),"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setLCell(rs_f_venc_credito, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setLCell(rs_Estatus, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setLCell("".equals(rs_int_generados)?"":"$ "+Comunes.formatoDecimal(rs_int_generados,2),"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setLCell("".equals(rs_pago_a_capital)?"":"$ "+Comunes.formatoDecimal(rs_pago_a_capital,2),"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setLCell("".equals(rs_pago_int)?"":"$ "+Comunes.formatoDecimal(rs_pago_int,2),"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setLCell("".equals(totPagado)?"":"$ "+Comunes.formatoDecimal(totPagado,2),"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setLCell("".equals(rs_saldo_cre)?"":"$ "+Comunes.formatoDecimal(rs_saldo_cre,2),"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setLCell(rs_tipo_cob_int, "formasmen", ComunesPDF.CENTER);
				nRow++;
			}
			if(totalregMN>0){
				pdfDoc.setLCell("Total M.N.", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setLCell("$ "+totalregMN,"formasmenB",ComunesPDF.RIGHT,2);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(totalMontoCreMN,2),"formasmenB",ComunesPDF.RIGHT);
				pdfDoc.setLCell(" ","formasmenB",ComunesPDF.RIGHT);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(totalMontoFinalMN,2),"formasmenB",ComunesPDF.RIGHT);
				pdfDoc.setLCell(" ","formasmenB",ComunesPDF.RIGHT,3);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(totalPagoMN,2),"formasmenB",ComunesPDF.RIGHT);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(totalIntMN,2),"formasmenB",ComunesPDF.RIGHT);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(totalPagadoMN,2),"formasmenB",ComunesPDF.RIGHT);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(totalSaldoMN,2),"formasmenB",ComunesPDF.RIGHT);
				pdfDoc.setLCell(" ","formasmenB",ComunesPDF.RIGHT);
			}
			if(totalregUSD>0){
				pdfDoc.setLCell("Total Dolares", "formasmenB", ComunesPDF.CENTER);
				pdfDoc.setLCell("$ "+totalregUSD,"formasmenB",ComunesPDF.RIGHT,2);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(totalMontoCreUSD,2),"formasmenB",ComunesPDF.RIGHT);
				pdfDoc.setLCell(" ","formasmenB",ComunesPDF.RIGHT);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(totalMontoFinalUSD,2),"formasmenB",ComunesPDF.RIGHT);
				pdfDoc.setLCell(" ","formasmenB",ComunesPDF.RIGHT,3);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(totalPagoUSD,2),"formasmenB",ComunesPDF.RIGHT);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(totalIntUSD,2),"formasmenB",ComunesPDF.RIGHT);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(totalPagadoUSD,2),"formasmenB",ComunesPDF.RIGHT);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(totalSaldoUSD,2),"formasmenB",ComunesPDF.RIGHT);
				pdfDoc.setLCell(" ","formasmenB",ComunesPDF.RIGHT);
			}
			pdfDoc.addLTable();
			pdfDoc.endDocument();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo", e);
		} finally {
		}
	}
	return nombreArchivo;
	}

/*****************************************************
	 GETTERS
*******************************************************/
 
	/**
	  Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
 	
	
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	
	public String getDirectorio() {
		return directorio;
	}

	public void setDirectorio(String directorio) {
		this.directorio = directorio;
	}

  public String getPaginar() {
    return paginar;
  }

  public void setPaginar(String paginar) {
    this.paginar = paginar;
  }

  public String getIc_if() {
    return ic_if;
  }

  public void setIc_if(String ic_if) {
    this.ic_if = ic_if;
  }

  public String getIc_pyme() {
    return ic_pyme;
  }

  public void setIc_pyme(String ic_pyme) {
    this.ic_pyme = ic_pyme;
  }

  public String getIc_moneda() {
    return ic_moneda;
  }

  public void setIc_moneda(String ic_moneda) {
    this.ic_moneda = ic_moneda;
  }

  public String getDf_fecha_oper_de() {
    return df_fecha_oper_de;
  }

  public void setDf_fecha_oper_de(String df_fecha_oper_de) {
    this.df_fecha_oper_de = df_fecha_oper_de;
  }

  public String getDf_fecha_oper_a() {
    return df_fecha_oper_a;
  }

  public void setDf_fecha_oper_a(String df_fecha_oper_a) {
    this.df_fecha_oper_a = df_fecha_oper_a;
  }

  public String getUsuario() {
    return usuario;
  }

  public void setUsuario(String usuario) {
    this.usuario = usuario;
  }
	public void setDatosVector(java.util.Vector datosVec){
		this.datosVec = datosVec;
	}
}
