package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 *
 *
 * @author Andrea Isabel Luna Aguill�n
 */ 

public class RepEstatusEpo implements IQueryGeneratorRegExtJS{
	public RepEstatusEpo() {  }
	
	private List conditions;
	StringBuffer strQuery;
	StringBuffer campos;
	StringBuffer camposUnion1;
	StringBuffer camposUnion2;
	StringBuffer camposAux;
	StringBuffer tablas;
	StringBuffer tablas1;
	StringBuffer tablas2;
	StringBuffer tablasAux;
	StringBuffer condiciones;
	StringBuffer condiciones1;
	StringBuffer condiciones2;
	StringBuffer condicionesAux;
	StringBuffer strQueryCCC;
	StringBuffer strQueryDM;
  double montoDepos =0;
	double montoDeposUSD =0;
	private static final Log log = ServiceLocator.getInstance().getLog(RepEstatusEpo.class);
	private String claveEpo;
	private String estatusDocto;
	private String tipoSolic;
	private String tipoCredito;
	
	public String getAggregateCalculationQuery() {
		conditions		= new ArrayList();
		
		log.debug("getAggregateCalculationQuery strQuery = " + strQuery.toString());
		log.debug("getAggregateCalculationQuery conditions = " + conditions);
		return strQuery.toString();
	}
	public String getDocumentQuery() {
		conditions	= new ArrayList();
		strQuery		= new StringBuffer();
		return strQuery.toString();
	}
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions	= new ArrayList();
		strQuery		= new StringBuffer();
		return strQuery.toString();
	}
	public String getDocumentQueryFile() {
		conditions		= new ArrayList();
		strQuery			= new StringBuffer();
		campos			= new StringBuffer();
		camposAux		= new StringBuffer();
		camposUnion1	= new StringBuffer();
		camposUnion2	= new StringBuffer();
		tablas			= new StringBuffer();
		tablas1			= new StringBuffer();
		tablas2			= new StringBuffer();
		tablasAux		= new StringBuffer();
		condiciones		= new StringBuffer();
		condiciones1	= new StringBuffer();
		condiciones2	= new StringBuffer();
		condicionesAux = new StringBuffer();
		strQueryCCC		= new StringBuffer();
		strQueryDM		= new StringBuffer();
		int tipo			= 0;
	
		log.debug("ic_epo= " + this.claveEpo);
		campos.append("SELECT py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse," +
								"TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision," +
								"TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga," +
								"TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc," +
								"d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto," +
								"'' AS monto_valuado," +
								"NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento," +
								"NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento," +
								"d.fn_monto*(NVL(d.fn_porc_descuento,0)/100) AS monto_descontar," +
								"DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion)," +
											"'N', ''," +
											"'P', 'Dolar-Peso'," +
											"''" +
								") AS tipo_conversion," +
								"DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion)," +
											"'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1')," +
											"'1'" +
											") AS tipo_cambio," +
								"m.ic_moneda, d.ic_documento,clas.cg_descripcion AS categoria");
		if(("5".equals(this.estatusDocto)&&"D".equals(this.tipoSolic))){
			campos.append(",TO_CHAR (ce.dc_fecha_cambio, 'DD/MM/YYYY') AS max_fecha_cambio" +
							",tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus");
		}
		if((("2".equals(this.estatusDocto)||
			 "11".equals(this.estatusDocto)||
			 "22".equals(this.estatusDocto)||
			 "9".equals(this.estatusDocto)|| "32".equals(this.estatusDocto)||
			 //"24".equals(this.estatusDocto)|| //MOD +("24".equals(this.estatusDocto)||)
			 "1".equals(this.estatusDocto))
			&&"D".equals(this.tipoSolic))
			||("1".equals(this.estatusDocto)&&"C".equals(this.tipoSolic))){
			campos.append(	" , '0' AS monto_credito ,e.cg_razon_social AS nombre_epo," +
								"tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus," +
								" DECODE (NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza)," +
											"'D', 'Delegada'," +
											"'N', 'No Delegada'" +
											") AS tipo_cobranza");
		}
		if("20".equals(this.estatusDocto)&&"D".equals(this.tipoSolic)){
			campos.append(",e.cg_razon_social AS nombre_epo," +
							"tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus");
		}
		if(("2".equals(this.estatusDocto)||
			 "9".equals(this.estatusDocto)||
			 "5".equals(this.estatusDocto)||
			 "20".equals(this.estatusDocto)|| "32".equals(this.estatusDocto)||
			 //"24".equals(this.estatusDocto)||//MOD +("24".equals(this.estatusDocto)||)
			 "1".equals(this.estatusDocto))&&"D".equals(this.tipoSolic)){
			campos.append(",d.cg_ventacartera AS cg_ventacartera ");
		}
    if ("32".equals(this.estatusDocto)){
      campos.append(",bins.cg_descripcion as desc_bins "+
                    ", DECODE ( ed.ic_estatus_docto, 32, cif2.cg_razon_social, cif.cg_razon_social ) as nombre_if " +
                    ", ca.FN_PORC_COMISION_APLI as comision_aplicable "+
                    ", ( d.fn_monto - ( d.fn_monto * ( DECODE (pe.fn_tasa_descuento, '', NVL (pe.fn_tasa_descuento, 0), pe.fn_tasa_descuento  ) /100)) ) as montodepos "+
                    ", ddp.ic_orden_pago_tc as orden_enviado "+
                    ", ddp.cg_transaccion_id as id_operacion "+
                    ", ( ddp.cg_codigo_resp || ' ' || ddp.cg_codigo_desc ) as codigo_autoriza "+
                    ", ddp.df_fecha_autorizacion as fecha_registro "+
						  ", (select fn_porcentaje_iva from  comcat_iva where ic_iva =3  ) as iva "+
						  ", ddp.cg_num_tarjeta AS num_tarjeta "
                    );
    }
    
    
		if((("3".equals(this.estatusDocto)||
			  "11".equals(this.estatusDocto)||
			  "22".equals(this.estatusDocto)||
			  "24".equals(this.estatusDocto)|| //MOD +("24".equals(this.estatusDocto)||)
			  "20".equals(this.estatusDocto))&&"D".equals(this.tipoSolic))
		||("1".equals(this.estatusDocto)&&"C".equals(this.tipoSolic))){
			campos.append(",i.cg_razon_social AS nombre_if," +
								"DECODE (pp.cg_tipo_credito," +
											"'D', 'Descuento y/o Factoraje'," +
											"'C', 'Credito en Cuenta Corriente'" +
											") AS tipo_credito," +
											"ds.fn_importe_recibir AS monto_credito, d.ig_plazo_credito," +
											"TO_CHAR (d.df_fecha_venc_credito, 'DD/MM/YYYY') AS fecha_venc_credito," +
								"ds.fn_importe_interes AS monto_tasa_int");
		}	
		if(("24".equals(this.estatusDocto)||"3".equals(this.estatusDocto))&&"D".equals(this.tipoSolic)){						//MOD +("24".equals(this.estatusDocto)||)
			campos.append(",TO_CHAR (SYSDATE, 'DD/MM/YYYY') AS fecha_operaif");
		}
		if((("11".equals(this.estatusDocto)||
			  "22".equals(this.estatusDocto)||
			  "20".equals(this.estatusDocto))&&"D".equals(this.tipoSolic))
		||("1".equals(this.estatusDocto)&&"C".equals(this.tipoSolic))){
			campos.append(",tci.cd_descripcion AS tipo_cobro_int,");
			if("20".equals(this.estatusDocto)){
				campos.append("TO_CHAR (sysdate, 'DD/MM/YYYY') AS fecha_operaif," +
									"TO_CHAR (ce.dc_fecha_cambio, 'DD/MM/YYYY') AS fecha_rech");
			}else{
				campos.append("TO_CHAR (ca.df_fecha_hora, 'DD/MM/YYYY') AS fecha_operaif");
			}
		}
		if(("11".equals(this.estatusDocto)||"22".equals(this.estatusDocto))&&"D".equals(this.tipoSolic)){
			campos.append(",ce.cg_numero_pago");
		}		
		if(("C".equals(this.tipoCredito)&&"3".equals(this.estatusDocto))
		||("D".equals(this.tipoCredito)&&(!(("3".equals(this.estatusDocto)||"24".equals(this.estatusDocto)))//MOD +(||"24".equals(this.estatusDocto))
		&&!("2".equals(this.estatusDocto))&&!("5".equals(this.estatusDocto))
		&&!("9".equals(this.estatusDocto))
		&&!("1".equals(this.estatusDocto)&&"D".equals(this.tipoSolic))))){
      if("32".equals(this.estatusDocto)){
        campos.append(" ,DECODE (ca.cg_tipo_tasa," +
											"'P', 'Preferencial'," +
											"'N', 'Negociada'," +
											"m.cd_nombre || ' ' || ca.cg_rel_mat || ' ' || ca.fn_puntos" +
											") AS referencia_int," +
								"ca.fn_valor_tasa AS valor_tasa_int");
      } else{
			campos.append(" ,DECODE (ds.cg_tipo_tasa," +
											"'P', 'Preferencial'," +
											"'N', 'Negociada'," +
											"ct.cd_nombre || ' ' || ds.cg_rel_mat || ' ' || ds.fn_puntos" +
											") AS referencia_int," +
								"ds.fn_valor_tasa AS valor_tasa_int"); }
		}
		else if(("D".equals(this.tipoCredito)&&("3".equals(this.estatusDocto)||"24".equals(this.estatusDocto)))//MOD +(||"24".equals(this.estatusDocto))
		||("C".equals(this.tipoCredito)&&(!("3".equals(this.estatusDocto))
		&&!("2".equals(this.estatusDocto))&&!("5".equals(this.estatusDocto))
		&&!("9".equals(this.estatusDocto))&&!("32".equals(this.estatusDocto))
		&&!("1".equals(this.estatusDocto)&&"D".equals(this.tipoSolic))))){
			campos.append(	",ct.cd_nombre " +
								"|| ' ' " +
								"|| ds.cg_rel_mat " +
								"|| ' ' " +
								"|| ds.fn_puntos AS referencia_int,");
			campos.append("DECODE (ds.cg_rel_mat," +
								"'+', fn_valor_tasa + fn_puntos," +
								"'-', fn_valor_tasa - fn_puntos," +
								"'*', fn_valor_tasa * fn_puntos," +
								"'/', fn_valor_tasa / fn_puntos" +
								") AS valor_tasa_int");
		}
		else if("A".equals(this.tipoCredito)
					&&!("2".equals(this.estatusDocto))
					&&!("5".equals(this.estatusDocto))
					&&!("9".equals(this.estatusDocto))
					&&!("1".equals(this.estatusDocto)&&"D".equals(this.tipoSolic))){
			tipo = 1;//Tipo Credito = A
			camposAux.append(campos.toString());
			camposUnion1.append(campos.toString());
			camposUnion1.append(" ,DECODE (ds.cg_tipo_tasa," +
											"'P', 'Preferencial'," +
											"'N', 'Negociada'," +
											"ct.cd_nombre || ' ' || ds.cg_rel_mat || ' ' || ds.fn_puntos" +
											") AS referencia_int," +
								"ds.fn_valor_tasa AS valor_tasa_int");
								
			camposUnion2.append(camposAux.toString());
			camposUnion2.append(	",ct.cd_nombre " +
								"|| ' ' " +
								"|| ds.cg_rel_mat " +
								"|| ' ' " +
								"|| ds.fn_puntos AS referencia_int,");
			camposUnion2.append("DECODE (ds.cg_rel_mat," +
								"'+', fn_valor_tasa + fn_puntos," +
								"'-', fn_valor_tasa - fn_puntos," +
								"'*', fn_valor_tasa * fn_puntos," +
								"'/', fn_valor_tasa / fn_puntos" +
								") AS valor_tasa_int");
		}
		tablas.append(" FROM dis_documento d," +
									"comcat_pyme py," +
									"comcat_moneda m," +
									"comrel_producto_epo pe," +
									"comcat_producto_nafin pn," +
									"com_tipo_cambio tc," +
									"comrel_clasificacion clas");
		if(!(("3".equals(this.estatusDocto)||"24".equals(this.estatusDocto))&&"D".equals(this.tipoSolic))){//MOD +(||"24".equals(this.estatusDocto))
			if(!"5".equals(this.estatusDocto)){
				tablas.append(",comcat_epo e");
			}
			tablas.append(		",comcat_tipo_financiamiento tf," +
										"comcat_estatus_docto ed"); 
      
      if ("32".equals(this.estatusDocto)){
        tablas.append(" , comcat_if cif, comcat_if cif2, com_bins_if bins, dis_docto_seleccionado ca,  dis_doctos_pago_tc ddp ");
      }
		}

		if((("3".equals(this.estatusDocto)||
			  "11".equals(this.estatusDocto)||
			  "22".equals(this.estatusDocto)||
			  "24".equals(this.estatusDocto)|| //MOD +("24".equals(this.estatusDocto)||)
			  "20".equals(this.estatusDocto))&&"D".equals(this.tipoSolic))
		||("1".equals(this.estatusDocto)&&"C".equals(this.tipoSolic))){
			tablas.append(		",dis_docto_seleccionado ds," +
									"comcat_if i," +
									"comrel_pyme_epo_x_producto pp," +
									"comcat_tasa ct");
		}
		if(("1".equals(this.estatusDocto)&&"C".equals(this.tipoSolic))||
		(("11".equals(this.estatusDocto)||
		  "22".equals(this.estatusDocto)||
		  "20".equals(this.estatusDocto))&&"D".equals(this.tipoSolic))){
			if(!"20".equals(this.estatusDocto)){
				tablas.append(		",dis_solicitud s," +
										"com_acuse3 ca");
			}
			tablas.append(	",comcat_tipo_cobro_interes tci");
		}
		if(("11".equals(this.estatusDocto)||
			 "22".equals(this.estatusDocto)||
			 "9".equals(this.estatusDocto)||
			 "5".equals(this.estatusDocto)||
			 "20".equals(this.estatusDocto))
		&&"D".equals(this.tipoSolic)){
			tablas.append(		",dis_cambio_estatus ce");
		}	
		
		if("32".equals(this.estatusDocto)){
         tablas.append(",com_linea_credito lc, comrel_pyme_epo_x_producto pp, comcat_tipo_cobro_interes tci, comcat_tasa ct" ); 
      }
		if(("3".equals(this.estatusDocto)&&"C".equals(this.tipoCredito))
		||("D".equals(this.tipoCredito)&&(!(("3".equals(this.estatusDocto)||"24".equals(this.estatusDocto)))//MOD +(||"24".equals(this.estatusDocto))
		&&!("2".equals(this.estatusDocto))
		&&!("5".equals(this.estatusDocto))
		&&!("9".equals(this.estatusDocto))
		&&!("1".equals(this.estatusDocto)&&"D".equals(this.tipoSolic))))){
        if ("32".equals(this.estatusDocto)){
		  }else {
			tablas.append(",dis_linea_credito_dm lc" );
		  }
		}
		else if((("3".equals(this.estatusDocto)||"24".equals(this.estatusDocto))&&"D".equals(this.tipoCredito))//MOD +(||"24".equals(this.estatusDocto))
		||("C".equals(this.tipoCredito)&&(!("3".equals(this.estatusDocto))
		&&!("2".equals(this.estatusDocto))	&&!("32".equals(this.estatusDocto))
		&&!("5".equals(this.estatusDocto))
		&&!("9".equals(this.estatusDocto))
		&&!("1".equals(this.estatusDocto)&&"D".equals(this.tipoSolic))))){
			tablas.append(",com_linea_credito lc");	
		}
		else if("A".equals(this.tipoCredito)
					&&!("2".equals(this.estatusDocto))
					&&!("5".equals(this.estatusDocto))
					&&!("9".equals(this.estatusDocto))
					&&!("1".equals(this.estatusDocto)&&"D".equals(this.tipoSolic))){
			tablasAux.append(tablas.toString());
			tablas1.append(tablas.toString());
			tablas1.append(",dis_linea_credito_dm lc" );
			tablas2.append(tablasAux.toString());
			tablas2.append(",com_linea_credito lc");
		}
		condiciones.append( " WHERE d.ic_pyme = py.ic_pyme" +
										" AND d.ic_moneda = m.ic_moneda" +
										" AND pn.ic_producto_nafin = 4" +
										" AND pn.ic_producto_nafin = pe.ic_producto_nafin" +
										" AND pe.ic_epo = d.ic_epo" +
										" AND tc.dc_fecha IN (SELECT MAX (dc_fecha)" +
																		" FROM com_tipo_cambio" +
																		" WHERE ic_moneda = m.ic_moneda)" +
										" AND m.ic_moneda = tc.ic_moneda" +	
										" AND d.ic_clasificacion = clas.ic_clasificacion(+)" );
    if("32".equals(this.estatusDocto)){
       //System.err.print(estatusDocto);  
      }else {
        condiciones.append("  AND d.ic_epo = ? ");
        conditions.add(claveEpo);
      }
																					
                    
		if((("2".equals(this.estatusDocto)||
			  "1".equals(this.estatusDocto)||
			  "11".equals(this.estatusDocto)||
			  "22".equals(this.estatusDocto)||
			  "9".equals(this.estatusDocto)|| "32".equals(this.estatusDocto)||
			  "20".equals(this.estatusDocto))&&"D".equals(this.tipoSolic))
		||("1".equals(this.estatusDocto)&&"C".equals(this.tipoSolic))){
			condiciones.append("	AND e.ic_epo = d.ic_epo" +
										" AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento" +
										" AND ed.ic_estatus_docto = d.ic_estatus_docto");
		}
		if(("2".equals(this.estatusDocto)|| "32".equals(this.estatusDocto)||
			 "1".equals(this.estatusDocto))&&"D".equals(this.tipoSolic)){
			if("32".equals(this.estatusDocto)){
				condiciones.append(" AND TRUNC (ddp.df_fecha_autorizacion) = TRUNC (SYSDATE)" +
										" AND d.ic_estatus_docto = ?");
			}else {
				condiciones.append(" AND TRUNC (d.df_carga) = TRUNC (SYSDATE)" +
										" AND d.ic_estatus_docto = ?");
			}
			if("1".equals(this.estatusDocto)){
										conditions.add(new Integer(1));
			}else {
        if ("32".equals(this.estatusDocto)){
          condiciones.append(" and d.IC_ORDEN_PAGO_TC  =ddp.IC_ORDEN_PAGO_TC (+)");    
          conditions.add(new Integer(32));
        } else
										conditions.add(new Integer(2));
			}
		}
		if((("3".equals(this.estatusDocto)||
				"11".equals(this.estatusDocto)||
				"24".equals(this.estatusDocto)||//MOD +("24".equals(this.estatusDocto)||)
				"22".equals(this.estatusDocto)||
				"20".equals(this.estatusDocto))&&"D".equals(this.tipoSolic))
		||("1".equals(this.estatusDocto)&&"C".equals(this.tipoSolic))){
			condiciones.append(" AND ds.ic_documento = d.ic_documento" +
										" AND i.ic_if = lc.ic_if" +
										" AND pp.ic_pyme = py.ic_pyme" +
										" AND pp.ic_producto_nafin = 4");
		}
		if("3".equals(this.estatusDocto)&&"D".equals(this.tipoSolic)){	
			condiciones.append(	" AND pp.ic_epo = pe.ic_epo" +
										" AND ct.ic_tasa = ds.ic_tasa" +
										" AND TRUNC (ds.df_fecha_seleccion) = TRUNC (SYSDATE)" +
										" AND d.ic_estatus_docto = ?");
										conditions.add(new Integer(3));
		}
		//MOD +(
		if("24".equals(this.estatusDocto)&&"D".equals(this.tipoSolic)){	
			condiciones.append(	" AND pp.ic_epo = pe.ic_epo" +
										" AND ct.ic_tasa = ds.ic_tasa" +
										" AND TRUNC (ds.df_fecha_seleccion) = TRUNC (SYSDATE)" +
										" AND d.ic_estatus_docto = ?");
										conditions.add(new Integer(24));
		}
		//)
		if((("11".equals(this.estatusDocto)||
			  "22".equals(this.estatusDocto)||
			  "20".equals(this.estatusDocto))&&"D".equals(this.tipoSolic))
		||("1".equals(this.estatusDocto)&&"C".equals(this.tipoSolic))){
			if(!"20".equals(this.estatusDocto)){	
				condiciones.append("	AND ca.cc_acuse = s.cc_acuse");
			}
			condiciones.append(" AND pp.ic_epo = e.ic_epo" +
									"	AND tci.ic_tipo_cobro_interes = lc.ic_tipo_cobro_interes" +
									"	AND ct.ic_tasa = ds.ic_tasa");
		}
		if("1".equals(this.estatusDocto)&&"C".equals(this.tipoSolic)){
			condiciones.append(" AND s.ic_documento = ds.ic_documento" +
									"	AND s.ic_estatus_solic = 1" +
									"	AND d.ic_estatus_docto = ?" +
									"	AND TRUNC (ca.df_fecha_hora) = TRUNC (SYSDATE)");
									conditions.add(new Integer(4));
		}
		if(("11".equals(this.estatusDocto)||"22".equals(this.estatusDocto))&&"D".equals(this.tipoSolic)){
			condiciones.append(" AND s.ic_documento = d.ic_documento" +
									"	AND ce.ic_documento = d.ic_documento" +
									"	AND TRUNC (ce.dc_fecha_cambio) = TRUNC (SYSDATE)");
		}
		if("11".equals(this.estatusDocto)&&"D".equals(this.tipoSolic)){
			condiciones.append("	AND ce.ic_cambio_estatus IN (19, 20)" +
									"	AND d.ic_estatus_docto = ?" +
									"  AND ce.dc_fecha_cambio IN (" +
											" SELECT MAX (dc_fecha_cambio)" +
												" FROM dis_cambio_estatus" +
												" WHERE ic_cambio_estatus IN (19, 20)" +
													" AND ic_documento = d.ic_documento)");
									conditions.add(new Integer(11));
		}
		if("22".equals(this.estatusDocto)&&"D".equals(this.tipoSolic)){
			condiciones.append(" AND ce.ic_cambio_estatus = 18" +
									"	AND d.ic_estatus_docto = ?" +
									"	AND ce.dc_fecha_cambio IN (" +
										" SELECT MAX (dc_fecha_cambio)" +
											" FROM dis_cambio_estatus" +
										" WHERE ic_cambio_estatus = 18" +
											" AND ic_documento = d.ic_documento)");
									conditions.add(new Integer(22));
		}
		if(("9".equals(this.estatusDocto)||
			 "5".equals(this.estatusDocto)||
			 "20".equals(this.estatusDocto))&&"D".equals(this.tipoSolic)){
			condiciones.append(" AND ce.ic_documento = d.ic_documento");
			condiciones.append("	AND TRUNC (ce.dc_fecha_cambio) = TRUNC (SYSDATE)");
		}
		if("9".equals(this.estatusDocto)&&"D".equals(this.tipoSolic)){
			condiciones.append("	AND ce.ic_cambio_estatus = 22" +
									"	AND d.ic_estatus_docto = ?");
									conditions.add(new Integer(9));
		}
		if(("5".equals(this.estatusDocto)||"20".equals(this.estatusDocto))&&"D".equals(this.tipoSolic)){
			if("5".equals(this.estatusDocto)){
				condiciones.append("	AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento" +
										"	AND ed.ic_estatus_docto = d.ic_estatus_docto");
				condiciones.append("	AND d.ic_estatus_docto = ?" +
										"	AND ce.ic_cambio_estatus = 4");
										conditions.add(new Integer(5));
			}
			condiciones.append("	AND TRUNC (ce.dc_fecha_cambio) IN (SELECT TRUNC (MAX(dc_fecha_cambio))" +
																						" FROM dis_cambio_estatus" +
																					" WHERE ic_documento=d.ic_documento)");
		}
		if("20".equals(this.estatusDocto)&&"D".equals(this.tipoSolic)){
			condiciones.append("	AND d.ic_estatus_docto = ?" +
									"	AND ce.ic_cambio_estatus = 14");
									conditions.add(new Integer(20));
		}
		if ("32".equals(this.estatusDocto)){
        condiciones.append(" AND tci.ic_tipo_cobro_interes = lc.ic_tipo_cobro_interes AND ct.ic_tasa = ca.ic_tasa AND ca.ic_documento = d.ic_documento "+
        " AND cif.ic_if = lc.ic_if  AND pp.ic_pyme = py.ic_pyme AND DECODE (pp.cg_tipo_credito, 'D', d.ic_linea_credito_dm,  'C', d.ic_linea_credito  ) = lc.ic_linea_credito "+
        " AND d.ic_bins = bins.ic_bins(+)    AND bins.ic_if = cif2.ic_if(+)");   
        }
		if((("3".equals(this.estatusDocto)||"24".equals(this.estatusDocto))&&"D".equals(this.tipoCredito))//MOD +(||"24".equals(this.estatusDocto))
		||("C".equals(this.tipoCredito)&&(!("3".equals(this.estatusDocto))
		&&!("2".equals(this.estatusDocto))
		&&!("5".equals(this.estatusDocto))
		&&!("9".equals(this.estatusDocto))
		&&!("1".equals(this.estatusDocto)&&"D".equals(this.tipoSolic))))){ if ("32".equals(this.estatusDocto)){}else
			condiciones.append("	AND d.ic_linea_credito = lc.ic_linea_credito");
		}
		else if(("3".equals(this.estatusDocto)&&"C".equals(this.tipoCredito))
		||("D".equals(this.tipoCredito)&&(!(("3".equals(this.estatusDocto)||"24".equals(this.estatusDocto)))//MOD +(||"24".equals(this.estatusDocto))
		&&!("2".equals(this.estatusDocto))
		&&!("5".equals(this.estatusDocto))
		&&!("9".equals(this.estatusDocto))
		&&!("1".equals(this.estatusDocto)&&"D".equals(this.tipoSolic))))){
			if ("32".equals(this.estatusDocto)){
			}else{
          condiciones.append(" AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm (+) "); 
			 }
		}
		else if("A".equals(this.tipoCredito)
					&&!("2".equals(this.estatusDocto))
					&&!("5".equals(this.estatusDocto))
					&&!("9".equals(this.estatusDocto))
					&&!("1".equals(this.estatusDocto)&&"D".equals(this.tipoSolic))){
			condicionesAux.append(condiciones.toString());
			condiciones1.append(condiciones.toString());
			condiciones1.append(" AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm (+) ");
			condiciones2.append(condicionesAux.toString());
			condiciones2.append(" AND d.ic_linea_credito = lc.ic_linea_credito");
		}
		if(tipo==0){
			strQuery.append(campos.toString());
			strQuery.append(tablas.toString());
			strQuery.append(condiciones.toString());
		}else{
			strQueryCCC.append(camposUnion1.toString());
			strQueryCCC.append(tablas1.toString());
			strQueryCCC.append(condiciones1.toString());
			
			strQueryDM.append(camposUnion2.toString());
			strQueryDM.append(tablas2.toString());
			strQueryDM.append(condiciones2.toString());
			conditions.add(claveEpo);
			if("1".equals(this.estatusDocto)&&"C".equals(this.tipoSolic)){
				conditions.add(new Integer(4));
			}else{
				conditions.add(new Integer(this.estatusDocto));
			}
			
			log.debug("strQueryCCC.toString().length() = " + strQueryCCC.toString().length());
			log.debug("strQueryDM.toString().length() = " + strQueryDM.toString().length());
			
			if(strQueryCCC.toString().length()>0){
				strQuery.append(strQueryCCC.toString());
			}
			if(strQueryCCC.toString().length()>0 && strQueryDM.toString().length()>0){
				strQuery.append(" UNION ALL ");
			}
			if(strQueryDM.toString().length()>0){ 
			strQuery.append(strQueryDM.toString());
			}
			
			
			
		}
		log.debug("getDocumentQueryFile strQuery = " + strQuery.toString());
		log.debug("getDocumentQueryFile conditions = " + conditions);
		return strQuery.toString();
	}
	/**
		* En este m�todo se debe realizar la implementaci�n de la generaci�n del archivo
		* con base en el resulset que se recibe como par�metro. El ResulSet es el resultado
		* de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
		* @param request HttpRequest empleado principalmente para obtener el objeto session
		* @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
		* @param path Ruta f�sica donde se generar� el archivo
		* @param tipo cadena que identifica el tipo o variante de archivo que va a generar.
		* @return Cadena con la ruta del archivo generado
	 */
	 public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs,String path,String tipo){
		String nombreArchivo = "";
		String linea = "";
		String totales = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		double montoTotal = 0;
		
		String auxTipoCambio = "";
		//totales
		int		totalDoctosMN		= 0;
		int 		totalDoctosUSD		= 0;
		double	totalMontoMN		= 0;
		double	totalMontoUSD		= 0;
		double	totalMontoValuadoMN	= 0;
		double	totalMontoValuadoUSD= 0;
		double	totalMontoCreditoMN	= 0;
		double	totalMontoCreditoUSD= 0;
		String	tipoConversion = "";
		if("CSV".equals(tipo)){
			try{
				if("2".equals(this.estatusDocto)|| 
					"9".equals(this.estatusDocto)||
					("1".equals(this.estatusDocto)&&"D".equals(this.tipoSolic))){
					if("2".equals(this.estatusDocto))
						linea +="Negociable";
					if("9".equals(this.estatusDocto))
						linea +="Vencido sin Operar";
					if("1".equals(this.estatusDocto))
						linea +="No negociable";
						
					linea +="\nDistribuidor,Num. de Acuse de Carga,Num. docto. inicial,Fecha de emisi�n,Fecha de vencimiento,Fecha de publicaci�n,Plazo docto.,Moneda"+
						",Monto,Categoria,Plazo para descuento en dias,% de descuento,Monto % de descuento,";
					if(!"".equals(tipoConversion))
						linea += "Tipo conv.,Tipo cambio,Monto valuado,";
					linea += "Modalidad de plazo";
				}else if("3".equals(this.estatusDocto)||
							"11".equals(this.estatusDocto)||
							"22".equals(this.estatusDocto)||
							"24".equals(this.estatusDocto)||//MOD +("24".equals(this.estatusDocto)||)
							("1".equals(this.estatusDocto)&&"C".equals(this.tipoSolic))){
					if("3".equals(this.estatusDocto))
						linea+="Seleccionado Pyme";
					if("11".equals(this.estatusDocto))
						linea+="Operado Pagado";
					if("22".equals(this.estatusDocto))
						linea+="Pendiente de Pago IF";
					if("1".equals(this.estatusDocto))
						linea+="Seleccionado IF / Operado";
					if("24".equals(this.estatusDocto))
						linea+="En Proceso de Autorizaci�n";
						
					linea+="\nDistribuidor,Num. de Acuse de Carga,Num. docto.,Fecha de emisi�n,Fecha de vencimiento"+
							",Fecha de publicaci�n,Plazo docto.,Moneda,Monto,Categoria,Plazo para descuento en dias"+
							",% de descuento,Monto % de descuento";
					if(!"".equals(tipoConversion))
						linea += ",Tipo conv.,Tipo cambio,Monto valuado";
					linea +=",Tipo de cr�dito"+
								",N�mero de documento final,Monto,Plazo,Fecha de Vencimiento,Fecha de operaci�n IF, IF"+
								",Referencia tasa de inter�s,Valor tasa de inter�s,Monto de Intereses,Monto Total(C�pital e Intereses)";
				}else if("5".equals(this.estatusDocto) ){
					linea +="Baja\nDistribuidor,Num. de Acuse de Carga,N�mero de documento inicial,"+
								"Fecha de emisi�n,Fecha de vencimiento,Fecha de publicaci�n,Plazo docto.,Moneda,"+
								"Monto,Plazo para descuento en dias,% de descuento,Monto % de descuento";
					if(!"".equals(tipoConversion))
						linea += ",Tipo conv.,Tipo cambio,Monto valuado,";
					linea +=",Modalidad de plazo,Fecha de Cambio de Estatus";					
				}else if("20".equals(this.estatusDocto)){
					linea +="Rechazado IF\nDistribuidor,N�mero de Acuse de Carga,"+
								"N�mero de documento inicial,Fecha de emisi�n,Fecha de vencimiento,Fecha de publicaci�n,"+
								"Plazo docto.,Moneda,Monto,Categoria,Plazo para descuento en dias,% de descuento,"+
								"Monto % de descuento,";
					if(!"".equals(tipoConversion))
						linea += "Tipo conv.,Tipo cambio,Monto valuado,";
					linea +="Modalidad de plazo,"+
								"N�mero docto. final,Monto,Plazo,"+
								"Fecha de Vencimiento,Fecha de operaci�n IF,IF,"+
								"Referencia tasa de inter�s,Valor tasa de inter�s,Monto de Intereses,"+
								"Monto Total(C�pital e Intereses)";	
				
        } else if("32".equals(this.estatusDocto)){
          linea +="\nDistribuidor,Num. de Acuse de Carga,Num. docto. inicial,Fecha de emisi�n,Fecha de vencimiento,Fecha de publicaci�n,Plazo docto.,Moneda"+
						",Monto,Categoria,Plazo para descuento en dias,% de descuento,Monto % de descuento, IF,Monto,% Comisi�n Aplicable de Terceros,Monto Comisi�n de Terceros (IVA Incluido), Monto a Depositar por Operaci�n"+
            ",ID Orden Enviado,Respuesta de Operaci�n, C�digo Autorizaci�n,N�mero Tarjeta de Cr�dito";  
        }               
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
				buffer = new BufferedWriter(writer);
				buffer.write(linea);
        
				while(rs.next() ){ 
					linea = "";
					String modoPlazo			= "";
					String fechaMax			= "";
					String icDocumento		= "";
					String tipoCredito		= "";
					double montoCredito		= 0;
					String plazoCredito		= "";
					String fechaVencCred		= "";		
					String fechaOperacionIf	= "";			
					String intermediario		= "";						
					String referenciaTasaInt= "";
					double valorTasaInt		= 0;
					double montoTasaInt		= 0;
					
					String distribuidor	= (rs.getString("NOMBRE_DIST")==null)?"":rs.getString("NOMBRE_DIST");
					String numAcuseCarga	= (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
					String numDocto		= (rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO");
					String fechaEmision		= (rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION");
					String fechaVencimiento	= (rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC");					
					String fechaPublicacion	= (rs.getString("DF_CARGA")==null)?"":rs.getString("DF_CARGA");
					String plazoDocto			= (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");
					String moneda 				= (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
					double monto 				= rs.getDouble("FN_MONTO");
					
					String categoria			= (rs.getString("CATEGORIA")==null)?"":rs.getString("CATEGORIA");
					String plazoDescuento	= (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
					String porcDescuento		= (rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO");
					double montoDescontar	= monto*Double.parseDouble(porcDescuento)/100;
          double montoDos =0.0;
							 tipoConversion	= (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");
					double tipoCambio			= rs.getDouble("TIPO_CAMBIO");
					double montoValuado		= monto*tipoCambio;
					
					if("2".equals(this.estatusDocto)
					 ||"9".equals(this.estatusDocto)
					 ||"5".equals(this.estatusDocto)
					 ||"20".equals(this.estatusDocto)
					 ||("1".equals(this.estatusDocto)&&"D".equals(this.tipoSolic))){
						modoPlazo	= (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
						if("5".equals(this.estatusDocto)){
							fechaMax	= (rs.getString("MAX_FECHA_CAMBIO")==null)?"":rs.getString("MAX_FECHA_CAMBIO");		
						}
					}
					if("3".equals(this.estatusDocto)
					||"11".equals(this.estatusDocto)
					||"22".equals(this.estatusDocto)
					||"20".equals(this.estatusDocto)
					||"24".equals(this.estatusDocto) //MOD +(||"24".equals(this.estatusDocto))
					||("1".equals(this.estatusDocto)&&"C".equals(this.tipoSolic))){
						icDocumento	= (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");					
						if(!"20".equals(this.estatusDocto)){//MOD -(!"24".equals(this.estatusDocto))
							tipoCredito	= (rs.getString("TIPO_CREDITO")==null)?"":rs.getString("TIPO_CREDITO");
						}
						montoCredito		= rs.getDouble("MONTO_CREDITO");
						if("24".equals(this.estatusDocto)){
							plazoCredito		= (rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO");
							fechaVencCred		= (rs.getString("FECHA_VENC_CREDITO")==null)?"":rs.getString("FECHA_VENC_CREDITO");		
							fechaOperacionIf	= (rs.getString("FECHA_OPERAIF")==null)?"":rs.getString("FECHA_OPERAIF");			
						intermediario		= (rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF");						
						referenciaTasaInt	= (rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT");
						valorTasaInt		= rs.getDouble("VALOR_TASA_INT");
						montoTasaInt		= rs.getDouble("MONTO_TASA_INT");
						montoTotal			= montoCredito+montoTasaInt;
						}
						
						
					}
					String icMoneda			= (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");		
					if(!("22".equals(this.estatusDocto))&&
						!("11".equals(this.estatusDocto))&&
						!("3".equals(this.estatusDocto))&&
						!("24".equals(this.estatusDocto))&&//MOD +(!("24".equals(this.estatusDocto))&&)
						!("1".equals(this.estatusDocto)&&"C".equals(this.tipoSolic))){
						String bandeVentaCartera	= (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
						if(bandeVentaCartera.equals("S")){
							modoPlazo= "";
						}
					}
					auxTipoCambio =  "";
					if("1".equals(icMoneda)){
						totalDoctosMN ++;
						totalMontoMN += monto;
						if(!("2".equals(this.estatusDocto))&&
							!("9".equals(this.estatusDocto))&&
							!("5".equals(this.estatusDocto))&&
							!("1".equals(this.estatusDocto)&&"D".equals(this.tipoSolic))){
							totalMontoCreditoMN += montoCredito;
							if("11".equals(this.estatusDocto)){
								totalMontoValuadoMN += montoValuado;
							}
						}
					}else{
						auxTipoCambio = Comunes.formatoDecimal(tipoCambio,2);
						totalDoctosUSD ++;
						totalMontoUSD += monto;
						if(tipoCambio!=1){
							totalMontoValuadoUSD += montoValuado;
						}
						if(!("9".equals(this.estatusDocto))&&
							!("5".equals(this.estatusDocto))&&
							!("2".equals(this.estatusDocto))&&
							!("1".equals(this.estatusDocto)&&"D".equals(this.tipoSolic))){
							totalMontoCreditoUSD += montoCredito;
						} 
					}
					montoValuado = (tipoCambio==1)?0:montoValuado;
					
					if("2".equals(this.estatusDocto)||
						"9".equals(this.estatusDocto)||
						("1".equals(this.estatusDocto)&&"D".equals(this.tipoSolic))){
						linea = "\n" +
										distribuidor.replace(',',' ') +","+
										numAcuseCarga+","+
										numDocto+","+
										fechaEmision+","+
										fechaVencimiento+","+
										fechaPublicacion+","+
										plazoDocto+","+
										moneda+","+
										monto+","+
										categoria+","+
										plazoDescuento+","+
										porcDescuento+","+
										montoDescontar+",";
						if(!"".equals(tipoConversion)){
							linea += tipoConversion+","+
										auxTipoCambio+","+
										montoValuado+",";
						}	
						linea += modoPlazo;						
					}
					else if("3".equals(this.estatusDocto)||"11".equals(this.estatusDocto)||"24".equals(this.estatusDocto)//MOD +(||"24".equals(this.estatusDocto))
								||"22".equals(this.estatusDocto)||
								("1".equals(this.estatusDocto)&&"C".equals(this.tipoSolic))){
						linea += "\n"+distribuidor.replace(',',' ')+","+
						numAcuseCarga+","+
						numDocto+","+
						fechaEmision+","+
						fechaVencimiento+","+
						fechaPublicacion+","+
						plazoDocto+","+
						moneda+","+
						monto+","+
						categoria+","+
						plazoDescuento+","+
						porcDescuento+","+
						montoDescontar;
						if(!"".equals(tipoConversion))
							linea += ","+tipoConversion+","+auxTipoCambio+","+montoValuado;
						linea +=	","+tipoCredito+","+icDocumento+","+ montoCredito+","+
									plazoCredito+","+fechaVencCred+","+fechaOperacionIf+","+
									intermediario.replace(',',' ')+","+referenciaTasaInt.replace(',',' ')+","+
									valorTasaInt+","+montoTasaInt+","+montoTotal;
					}else if("5".equals(this.estatusDocto)){
						montoValuado = (tipoCambio==1)?0:montoValuado;	
						linea += "\n"+distribuidor+","+
											numAcuseCarga+","+
											numDocto+","+
											fechaEmision+","+
											fechaVencimiento+","+
											fechaPublicacion+","+
											plazoDocto+","+
											moneda+","+
											monto+","+
											plazoDescuento+","+
											porcDescuento+","+
											montoDescontar;
						if(!"".equals(tipoConversion))
							linea += ","+tipoConversion+","+auxTipoCambio+","+montoValuado;
							linea += ","+modoPlazo+","+fechaMax;					
					}else if("20".equals(this.estatusDocto)){
						linea += "\n"+distribuidor.replace(',',' ')+","+
											numAcuseCarga+","+
											numDocto+","+
											fechaEmision+","+
											fechaVencimiento+","+
											fechaPublicacion+","+
											plazoDocto+","+
											moneda+","+
											monto+","+
											categoria+","+
											plazoDescuento+","+
											porcDescuento+","+
											montoDescontar;
						if(!"".equals(tipoConversion))
							linea +=","+tipoConversion+","+auxTipoCambio+","+montoValuado;
						linea +=	"," + modoPlazo.replace(',',' ')+","+icDocumento+","+
						montoCredito+","+plazoCredito+","+fechaVencCred+","+fechaOperacionIf+","+intermediario.replace(',',' ')+","+
						referenciaTasaInt.replace(',',' ')+","+valorTasaInt+","+montoTasaInt+","+montoTotal;
					
          } else if("32".equals(this.estatusDocto)){
            String descBin= (rs.getString("DESC_BINS")==null)?"":rs.getString("DESC_BINS");
            String nomIf	= (rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF"); 
            double comApl	= rs.getDouble("COMISION_APLICABLE");
            String ordenE	= (rs.getString("ORDEN_ENVIADO")==null)?"":rs.getString("ORDEN_ENVIADO");
            String idOper	= (rs.getString("ID_OPERACION")==null)?"":rs.getString("ID_OPERACION");
            String codAuto = (rs.getString("CODIGO_AUTORIZA")==null)?"":rs.getString("CODIGO_AUTORIZA");
            String tarjeta	= (rs.getString("NUM_TARJETA")==null)?"":rs.getString("NUM_TARJETA");
            String modoPla = (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
				String iva		= (rs.getString("IVA")==null)?"0":rs.getString("IVA");
				double nuevoIva= (Double.parseDouble(iva)/100)+1;	
            double monCom= monto * ( comApl / 100) ;
            double monDep = monto - monCom;
            if (modoPlazo.equals("Pronto pago"))
                monDep = monDep - (monDep * comApl);
            else
                monDep = monto - monCom;
            montoDos=monto-montoDescontar;
            monCom= (montoDos*comApl)/100;
				monCom= monCom * nuevoIva; 
            monDep=montoDos-monCom;
                linea = "\n" +
										distribuidor.replace(',',' ') +","+
										numAcuseCarga+","+
										numDocto+","+
										fechaEmision+","+
										"N/A"+","+
										fechaPublicacion+","+
										"N/A"+","+
										moneda+","+
										"$ "+monto+","+
										categoria+","+
										"N/A"+","+
										porcDescuento+" %,"+
										"$ "+montoDescontar+","+  
                    nomIf.replace(',',' ')+","+  
                    "$ "+montoDos+","+
                    comApl+" %,"+
                    "$ "+monCom+","+
                    "$ "+monDep+","+
                    "'"+ordenE+","+
                    idOper+","+
                    codAuto+","+
                    "XXXX-XXXX-XXXX-"+tarjeta+",";                  
                    
          } 
					buffer.write(linea);
				}
				if("2".equals(this.estatusDocto)||
					"9".equals(this.estatusDocto)||
					"5".equals(this.estatusDocto)||
					("1".equals(this.estatusDocto)&&"D".equals(this.tipoSolic))){
					if(totalDoctosMN>0){
						totales += "\nTotal M.N.,"+totalDoctosMN+",,,,,,,"+totalMontoMN;
						if(!"".equals(tipoConversion))
							totales += ",,,,,,,"+totalMontoValuadoMN;
					}if(totalDoctosUSD>0){
						totales += "\nTotal Dolares,"+totalDoctosUSD+",,,,,,,"+totalMontoUSD;
						if(!"".equals(tipoConversion))
							totales += ",,,,,,,"+totalMontoValuadoUSD;
					}
				}else if("3".equals(this.estatusDocto)||
							"11".equals(this.estatusDocto)||
							"22".equals(this.estatusDocto)||
							"20".equals(this.estatusDocto)||
							"24".equals(this.estatusDocto)|| //MOD +("24".equals(this.estatusDocto)||)
							("1".equals(this.estatusDocto)&&"C".equals(this.tipoSolic))){
					if(totalDoctosMN>0){
						totales += "\nTotal M.N.,"+totalDoctosMN+",,,,,,,"+totalMontoMN;
						if(!"".equals(tipoConversion)){
							if(!"22".equals(this.estatusDocto)){
								totales +=",,,,,,,"+totalMontoValuadoMN+",,,"+totalMontoCreditoMN;
							}else{
								totales += ",,,,,,"+totalMontoValuadoMN+",,,"+totalMontoCreditoMN;
							}
						}
						else{
							if("3".equals(this.estatusDocto)||
								"11".equals(this.estatusDocto)||
								"24".equals(this.estatusDocto)||//MOD +("24".equals(this.estatusDocto)||)
								"20".equals(this.estatusDocto))
								totales +=",,,,,,,"+totalMontoCreditoMN;
							else if("1".equals(this.estatusDocto)||"22".equals(this.estatusDocto))
								totales +=",,,,,,"+totalMontoCreditoMN;
						}
					}if(totalDoctosUSD>0){
						totales += "\nTotal Dolares,"+totalDoctosUSD+",,,,,,,"+totalMontoUSD;
						if(!"".equals(tipoConversion)){
							if(!"22".equals(this.estatusDocto)){
								totales +=",,,,,,,"+totalMontoValuadoUSD+",,,"+totalMontoCreditoUSD;
							}else{
								totales += ",,,,,,"+totalMontoValuadoUSD+",,,"+totalMontoCreditoUSD;
							}
						}
						else{
							if("3".equals(this.estatusDocto)||
								"11".equals(this.estatusDocto)||
								"24".equals(this.estatusDocto)||//MOD +("24".equals(this.estatusDocto)||)
								"20".equals(this.estatusDocto))
								totales +=",,,,,,,"+totalMontoCreditoUSD;
							else if("1".equals(this.estatusDocto)||"22".equals(this.estatusDocto))
								totales +=",,,,,,"+totalMontoCreditoUSD;
						}
					}					
				}
				buffer.write(totales);
				buffer.close();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo",e);
			}
		}
		else if("PDF".equals(tipo)){
			try{
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[]		= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual = fechaActual.substring(0,2);
				String mesActual = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual = fechaActual.substring(6,10);
				String horaActual = new SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				String estatus = "";
				if("1".equals(this.estatusDocto)&&"C".equals(this.tipoSolic)){
					estatus = "Seleccionado IF / Operado" ;
				}else if ("1".equals(this.estatusDocto)&&"D".equals(this.tipoSolic)){
					estatus = "No negociable";
				}else if("2".equals(this.estatusDocto)){
					estatus = "Negociable";
				}else if("3".equals(this.estatusDocto)){
					estatus = "Seleccionada Pyme";
				}else if("11".equals(this.estatusDocto)){
					estatus = "Operado Pagado";
				}else if("22".equals(this.estatusDocto)){
					estatus = "Pendiente de Pago IF";
				}else if("9".equals(this.estatusDocto)){
					estatus = "Vencido sin Operar";
				}else if("5".equals(this.estatusDocto)){
					estatus = "Baja";
				}else if("20".equals(this.estatusDocto)){
					estatus = "Rechazado IF";
				}else if("24".equals(this.estatusDocto)){
					estatus = "En Proceso de Autorizaci�n IF";
				} else if ("32".equals(this.estatusDocto)){estatus = "Operada TC";}
				pdfDoc.addText("Estatus: " + estatus,"celda01");
				if ("32".equals(this.estatusDocto)){pdfDoc.setTable(12,100);}else {
        pdfDoc.setTable(10,100); }
				pdfDoc.setCell("A","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("Distribuidor","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Num.de acuse de carga","celda01",ComunesPDF.CENTER);
				
				String numeroDocto = "N�mero de documento inicial";
				if("11".equals(this.estatusDocto)
				 ||"22".equals(this.estatusDocto)
				 ||"9".equals(this.estatusDocto)
				 ||("1".equals(this.estatusDocto)&&"C".equals(this.tipoSolic))){
					numeroDocto = "N�m. docto."; 
				}
				pdfDoc.setCell(numeroDocto,"celda01",ComunesPDF.CENTER);
				
				pdfDoc.setCell("Fecha de emisi�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de publicaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo docto.","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
				if ("32".equals(this.estatusDocto)){
          pdfDoc.setCell("Categor�a","celda01",ComunesPDF.CENTER);
          pdfDoc.setCell("Plazo para descuento en d�as","celda01",ComunesPDF.CENTER);
          
          pdfDoc.setCell("B","celda02",ComunesPDF.CENTER);
          pdfDoc.setCell("% de descuento","celda01",ComunesPDF.CENTER);
          pdfDoc.setCell("Monto % de descuento","celda01",ComunesPDF.CENTER);          
          pdfDoc.setCell("If","celda01",ComunesPDF.CENTER);
          pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER); 
          pdfDoc.setCell("% Comision Aplicable de Terceros","celda01",ComunesPDF.CENTER);
          pdfDoc.setCell("Monto Comisi�n de Terceros (IVA Incluido)","celda01",ComunesPDF.CENTER);
          pdfDoc.setCell("Monto a Depositar por Operaci�n","celda01",ComunesPDF.CENTER);
          pdfDoc.setCell("ID Orden Enviado","celda01",ComunesPDF.CENTER);
          pdfDoc.setCell("Respuesta de Operaci�n","celda01",ComunesPDF.CENTER);
          pdfDoc.setCell("C�digo Autorizaci�n","celda01",ComunesPDF.CENTER);
          pdfDoc.setCell("N�mero Tarjeta de Cr�dito","celda01",ComunesPDF.CENTER);
          
        } else {
				pdfDoc.setCell("B","celda02",ComunesPDF.CENTER);
				pdfDoc.setCell("Categor�a","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo para descuento en d�as","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("% de descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto % de descuento","celda01",ComunesPDF.CENTER);
        }
				if(!"".equals(tipoConversion)){
					pdfDoc.setCell("Tipo conv.","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Tipo cambio","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto valuado","celda01",ComunesPDF.CENTER);
				}
				if("2".equals(this.estatusDocto)
				 ||"9".equals(this.estatusDocto)
				 ||"5".equals(this.estatusDocto)
				 ||("1".equals(this.estatusDocto)&&"D".equals(this.tipoSolic))){
					pdfDoc.setCell("Modalidad de plazo","celda01",ComunesPDF.CENTER);
					if("5".equals(this.estatusDocto)){
						pdfDoc.setCell("Fecha de Cambio de Estatus","celda01",ComunesPDF.CENTER);
					}else{
						pdfDoc.setCell("", "celda01", ComunesPDF.LEFT);
					}
				}
				if("2".equals(this.estatusDocto)
				||"5".equals(this.estatusDocto)
				||"9".equals(this.estatusDocto)
				||("1".equals(this.estatusDocto)&&"D".equals(this.tipoSolic))){
					if("".equals(tipoConversion)){
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);					
					}				
				}
				if("3".equals(this.estatusDocto)
				||"11".equals(this.estatusDocto)
				||"22".equals(this.estatusDocto)
				||"20".equals(this.estatusDocto)
				||"24".equals(this.estatusDocto)//MOD +(||"24".equals(this.estatusDocto))
				||("1".equals(this.estatusDocto)&&"C".equals(this.tipoSolic))){
					pdfDoc.setCell("N�mero de documento final","celda01",ComunesPDF.CENTER);					
					if(!"20".equals(this.estatusDocto)){
						pdfDoc.setCell("Tipo de cr�dito","celda01",ComunesPDF.CENTER);
					}else{
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
					}
					if("".equals(tipoConversion)){
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell("","celda01",ComunesPDF.CENTER);					
					}	
					pdfDoc.setCell("C","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Fecha de operacion IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("IF","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Referencia tasa de inter�s","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Valor tasa de inter�s","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto de Intereses","celda01",ComunesPDF.CENTER);
					pdfDoc.setCell("Monto Total (C�pital e Intereses)","celda01",ComunesPDF.CENTER);				
				} 
				while(rs.next() ){
					String modoPlazo			= "";
					String fechaMax			= "";
					String icDocumento		= "";
					String tipoCredito		= "";
					double montoCredito		= 0;
					String plazoCredito		= "";
					String fechaVencCred		= "";		
					String fechaOperacionIf	= "";			
					String intermediario		= "";						
					String referenciaTasaInt= "";
					double valorTasaInt		= 0;
					double montoTasaInt		= 0;
          
					
					String distribuidor	= (rs.getString("NOMBRE_DIST")==null)?"":rs.getString("NOMBRE_DIST");
					String numAcuseCarga	= (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
					String numDocto		= (rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO");
					String fechaEmision		= (rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION");
					String fechaVencimiento	= (rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC");					
					String fechaPublicacion	= (rs.getString("DF_CARGA")==null)?"":rs.getString("DF_CARGA");
					String plazoDocto			= (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");
					String moneda 				= (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
					double monto 				= rs.getDouble("FN_MONTO");
					
					String categoria			= (rs.getString("CATEGORIA")==null)?"":rs.getString("CATEGORIA");
					String plazoDescuento	= (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
					String porcDescuento		= (rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO");
					double montoDescontar	= monto*Double.parseDouble(porcDescuento)/100;
							 tipoConversion	= (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");
					double tipoCambio			= rs.getDouble("TIPO_CAMBIO");
					double montoValuado		= monto*tipoCambio;
					         
					if("2".equals(this.estatusDocto)
					 ||"9".equals(this.estatusDocto)
					 ||"5".equals(this.estatusDocto)
					 ||("1".equals(this.estatusDocto)&&"D".equals(this.tipoSolic))){
						modoPlazo	= (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
						if("5".equals(this.estatusDocto)){
							fechaMax	= (rs.getString("MAX_FECHA_CAMBIO")==null)?"":rs.getString("MAX_FECHA_CAMBIO");		
						}
					}
					if("3".equals(this.estatusDocto)
					||"11".equals(this.estatusDocto)
					||"22".equals(this.estatusDocto)
					||"20".equals(this.estatusDocto)
					||"24".equals(this.estatusDocto)//MOD +(||"24".equals(this.estatusDocto))
					||("1".equals(this.estatusDocto)&&"C".equals(this.tipoSolic))){
						icDocumento	= (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");					
						if(!"20".equals(this.estatusDocto)){
							tipoCredito	= (rs.getString("TIPO_CREDITO")==null)?"":rs.getString("TIPO_CREDITO");
						}
						montoCredito		= rs.getDouble("MONTO_CREDITO");
						plazoCredito		= (rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO");
						fechaVencCred		= (rs.getString("FECHA_VENC_CREDITO")==null)?"":rs.getString("FECHA_VENC_CREDITO");		
						fechaOperacionIf	= (rs.getString("FECHA_OPERAIF")==null)?"":rs.getString("FECHA_OPERAIF");			
						intermediario		= (rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF");						
						referenciaTasaInt	= (rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT");
						valorTasaInt		= rs.getDouble("VALOR_TASA_INT");
						montoTasaInt		= rs.getDouble("MONTO_TASA_INT");
						montoTotal			= montoCredito+montoTasaInt;
					}
					String icMoneda			= (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");		
					if(!("22".equals(this.estatusDocto))&&
						!("11".equals(this.estatusDocto))&&
						!(("3".equals(this.estatusDocto)||"24".equals(this.estatusDocto)))&&//MOD +(||"24".equals(this.estatusDocto))
						!("1".equals(this.estatusDocto)&&"C".equals(this.tipoSolic))){
						String bandeVentaCartera	= (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
						if(bandeVentaCartera.equals("S")){
							modoPlazo= "";
						}
					}
					auxTipoCambio = "";
					if("1".equals(icMoneda)){
						totalDoctosMN ++;
						totalMontoMN += monto;
						if(!("2".equals(this.estatusDocto))&&
							!("9".equals(this.estatusDocto))&&
							!("5".equals(this.estatusDocto))&&
							!("1".equals(this.estatusDocto)&&"D".equals(this.tipoSolic))){
							totalMontoCreditoMN += montoCredito;
							if("11".equals(this.estatusDocto)){
								totalMontoValuadoMN += montoValuado;
							}
						}
					}else{
						auxTipoCambio = Comunes.formatoDecimal(tipoCambio,2);
						totalDoctosUSD ++;
						totalMontoUSD += monto;
						if(tipoCambio!=1){
							totalMontoValuadoUSD += montoValuado;
						}
						if(!("9".equals(this.estatusDocto))&&
							!("5".equals(this.estatusDocto))&&
							!("2".equals(this.estatusDocto))&&
							!("1".equals(this.estatusDocto)&&"D".equals(this.tipoSolic))){
							totalMontoCreditoUSD += montoCredito;
						} 
					}
          
          if("32".equals(this.estatusDocto)) {
            pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
            pdfDoc.setCell(distribuidor,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(numAcuseCarga,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(numDocto,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(fechaEmision,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);
            pdfDoc.setCell(fechaPublicacion,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);
            pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell("$"+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.RIGHT);
            pdfDoc.setCell(categoria,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);
            pdfDoc.setCell("B","formas",ComunesPDF.CENTER);            
            pdfDoc.setCell(porcDescuento+" %","formas",ComunesPDF.CENTER);
            pdfDoc.setCell("$"+Comunes.formatoDecimal(montoDescontar,2),"formas",ComunesPDF.RIGHT);
            String bins	  = (rs.getString("DESC_BINS")==null)?"":rs.getString("DESC_BINS");
            String nomIF  = (rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF");
            String comAp  = (rs.getString("COMISION_APLICABLE")==null)?"":rs.getString("COMISION_APLICABLE");
            String ordEn	= (rs.getString("ORDEN_ENVIADO")==null)?"":rs.getString("ORDEN_ENVIADO");
            String idOpe	= (rs.getString("ID_OPERACION")==null)?"":rs.getString("ID_OPERACION");
            String codAu	= (rs.getString("CODIGO_AUTORIZA")==null)?"":rs.getString("CODIGO_AUTORIZA");
            String tarjeta	= (rs.getString("NUM_TARJETA")==null)?"":rs.getString("NUM_TARJETA");
				String iva		= (rs.getString("IVA")==null)?"0":rs.getString("IVA");
				double nuevoIva= (Double.parseDouble(iva)/100)+1;	
             double comApl    = rs.getDouble("COMISION_APLICABLE");
             double monCom= (monto-montoDescontar) * ( comApl / 100);
				 monCom = monCom * nuevoIva;
             double monDep = (monto-montoDescontar) - monCom;
             
            String tipoMoneda	= "";
            tipoMoneda = (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA"); 
            if (tipoMoneda.equals("1")) {
              montoDepos = montoDepos+monDep;
            } else {
             montoDeposUSD = montoDeposUSD +monDep;
            }
            
            /*if (modoPlazo.equals("Pronto pago"))
                monDep = monDep - (monDep * (comApl /100)) ;
            else
                monDep = monto - monCom;*/
                       
            pdfDoc.setCell(nomIF,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell("$"+Comunes.formatoDecimal((monto-montoDescontar),2),"formas",ComunesPDF.RIGHT);
            pdfDoc.setCell(comAp+" %","formas",ComunesPDF.CENTER);
            pdfDoc.setCell("$"+Comunes.formatoDecimal(monCom,2),"formas",ComunesPDF.CENTER);
            pdfDoc.setCell("$"+Comunes.formatoDecimal(monDep,2),"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(ordEn,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(idOpe,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(codAu,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell("XXXX-XXXX-XXXX-"+tarjeta,"formas",ComunesPDF.CENTER);
            
          } else {
            pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
            pdfDoc.setCell(distribuidor,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(numAcuseCarga,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(numDocto,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(fechaEmision,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(fechaPublicacion,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(plazoDocto,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell("$"+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.RIGHT);
            pdfDoc.setCell("B","formas",ComunesPDF.CENTER);
            pdfDoc.setCell(categoria,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(plazoDescuento,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(porcDescuento,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell("$"+Comunes.formatoDecimal(montoDescontar,2),"formas",ComunesPDF.RIGHT);
          }
          if("32".equals(this.estatusDocto)) {
            //*********************************************************************//
          } else {
            if(!"".equals(tipoConversion)){
              pdfDoc.setCell(tipoConversion,"formas",ComunesPDF.CENTER);
              pdfDoc.setCell(auxTipoCambio,"formas",ComunesPDF.CENTER);
              pdfDoc.setCell("$"+Comunes.formatoDecimal(montoValuado,2),"formas",ComunesPDF.RIGHT);
            }
          }
					if("2".equals(this.estatusDocto)
					 ||"9".equals(this.estatusDocto)
					 ||"5".equals(this.estatusDocto)
					 ||("1".equals(this.estatusDocto)&&"D".equals(this.tipoSolic))){
						pdfDoc.setCell(modoPlazo,"formas",ComunesPDF.CENTER);
						if("5".equals(this.estatusDocto)){
							pdfDoc.setCell(fechaMax,"formas",ComunesPDF.CENTER);
						}else{
							pdfDoc.setCell("", "formas", ComunesPDF.LEFT);
						}
					}
					if("2".equals(this.estatusDocto)
					||"5".equals(this.estatusDocto)
					||"9".equals(this.estatusDocto)
					||("1".equals(this.estatusDocto)&&"D".equals(this.tipoSolic))){
						if("".equals(tipoConversion)){
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						}						
					}
					if("3".equals(this.estatusDocto)
					||"11".equals(this.estatusDocto)
					||"22".equals(this.estatusDocto)
					||"20".equals(this.estatusDocto)
					||"24".equals(this.estatusDocto)//MOD +(||"24".equals(this.estatusDocto))
					||("1".equals(this.estatusDocto)&&"C".equals(this.tipoSolic))){
						pdfDoc.setCell(icDocumento,"formas",ComunesPDF.CENTER);					
						if(!"20".equals(this.estatusDocto)){
							pdfDoc.setCell(tipoCredito,"formas",ComunesPDF.CENTER);
						}else{
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						}
						if("".equals(tipoConversion)){
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						}						
						pdfDoc.setCell("C","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(montoCredito,0),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell(plazoCredito,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaVencCred,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaOperacionIf,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(intermediario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(referenciaTasaInt,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(Comunes.formatoDecimal(valorTasaInt,2)+"%","formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTasaInt,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setCell("$"+Comunes.formatoDecimal(montoTotal,2),"formas",ComunesPDF.RIGHT);				
					}				
				}
				pdfDoc.addTable();
				pdfDoc.setTable(2,100);
				if(totalDoctosMN>0){
					pdfDoc.setCell("Total M.N. de Documentos","celda01",ComunesPDF.RIGHT);
					pdfDoc.setCell(Integer.toString(totalDoctosMN),"celda01",ComunesPDF.LEFT);
					pdfDoc.setCell("Total Monto M.N","celda01",ComunesPDF.RIGHT);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(totalMontoMN,2),"celda01",ComunesPDF.LEFT);
					if(!("2".equals(this.estatusDocto))&&
						!("9".equals(this.estatusDocto))&&
						!("5".equals(this.estatusDocto))&&
						!("1".equals(this.estatusDocto)&&"D".equals(this.tipoSolic))){
             if("32".equals(this.estatusDocto)) {
                pdfDoc.setCell("Total Monto a Depositar por Operaci�n M.N.","celda01",ComunesPDF.RIGHT);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(montoDepos,2),"celda01",ComunesPDF.LEFT);	
            } else {
                pdfDoc.setCell("Total Monto del Credito M.N.","celda01",ComunesPDF.RIGHT);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(totalMontoCreditoMN,2),"celda01",ComunesPDF.LEFT);						
            }
					}
				}
				if(totalDoctosUSD>0){
					pdfDoc.setCell("Total de Documentos D�lares","celda01",ComunesPDF.RIGHT);
					pdfDoc.setCell(Integer.toString(totalDoctosUSD),"celda01",ComunesPDF.LEFT);
					pdfDoc.setCell("Total Monto D�lares","celda01",ComunesPDF.RIGHT);
					pdfDoc.setCell("$" + Comunes.formatoDecimal(totalMontoUSD,2),"celda01",ComunesPDF.LEFT);
					if(!"".equals(tipoConversion)){
						pdfDoc.setCell("Total Monto Valuado Dolares","celda01",ComunesPDF.RIGHT);
						pdfDoc.setCell("$" + Comunes.formatoDecimal(totalMontoValuadoUSD,2),"celda01",ComunesPDF.LEFT);						
					}
					if(!("2".equals(this.estatusDocto))&&
						!("9".equals(this.estatusDocto))&&
						!("5".equals(this.estatusDocto))&&
						!("1".equals(this.estatusDocto)&&"D".equals(this.tipoSolic))){
             if("32".equals(this.estatusDocto)) {
                pdfDoc.setCell("Total Monto a Depositar por Operaci�n USD.","celda01",ComunesPDF.RIGHT);
                pdfDoc.setCell("$" + Comunes.formatoDecimal(montoDeposUSD,2),"celda01",ComunesPDF.LEFT);	
            } else {
              pdfDoc.setCell("Total Monto del Credito Dolares","celda01",ComunesPDF.RIGHT);
              pdfDoc.setCell("$" + Comunes.formatoDecimal(totalMontoCreditoUSD,2),"celda01",ComunesPDF.LEFT);						
            }
					}
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();
			}catch(Throwable e){
				throw new AppException ("Error al generar el archivo", e);
			}
		}	
		return nombreArchivo;
	 }
	/** En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va a generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	 public String crearPageCustomFile(HttpServletRequest request,netropology.utilerias.Registros reg,String path,String tipo){
		return "";
	 }
	 public List getConditions() {
		return conditions;
	 }
	public void setClaveEpo(String claveEpo) {
		this.claveEpo = claveEpo;
	}
	public String getClaveEpo() {
		return claveEpo;
	}
	public void setEstatusDocto(String estatusDocto) {
		this.estatusDocto = estatusDocto;
	}
	public String getEstatusDocto() {
		return estatusDocto;
	}
	public void setTipoSolic(String tipoSolic) {
		this.tipoSolic = tipoSolic;
	}
	public String getTipoSolic() {
		return tipoSolic;
	}
	public void setTipoCredito(String tipoCredito) {
		this.tipoCredito = tipoCredito;
	}
	public String getTipoCredito() {
		return tipoCredito;
	}
}