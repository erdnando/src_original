package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class BloqueaIfDistEpo implements IQueryGeneratorRegExtJS {
public BloqueaIfDistEpo(){}
	
	private List 	conditions;
	StringBuffer strQuery = new StringBuffer();
	private static final Log log = ServiceLocator.getInstance().getLog(BloqueaIfDistEpo.class);//Variable para enviar mensajes al log.

	private String ic_if;
	private String ic_epo;  
	private String ic_pyme;

	public String getAggregateCalculationQuery() {
		return "";
 	}

	public String getDocumentQuery(){
		conditions = new ArrayList();	
		strQuery 		= new StringBuffer();
		 
		strQuery.append(	"SELECT n.ic_nafin_electronico ne "	);//strQuery.append(	"SELECT n.ic_nafin_electronico ne "	);
		strQuery.append(
				" FROM comrel_if_epo ie,"+
				" comrel_if_epo_x_producto iexp " +
				" , comrel_pyme_epo pe "+
				" , comrel_pyme_epo_x_producto pexp LEFT JOIN comrel_pyme_epo_if_x_producto peixp ON (    pexp.ic_epo = peixp.ic_epo  AND pexp.ic_pyme = peixp.ic_pyme AND pexp.ic_producto_nafin = peixp.ic_producto_nafin  AND peixp.ic_if ="+ic_if+" ) " +//comrel_pyme_epo_x_producto  comrel_pyme_epo_if_x_producto pexp(tabla nueva)
				" , comcat_pyme p, comrel_nafin n"	);
		strQuery.append(
				" WHERE ie.ic_if = iexp.ic_if " +
				" AND ie.cs_distribuidores in (?,?) " +
				" AND iexp.cs_habilitado = ? " +
				" AND iexp.ic_producto_nafin = ? " +
				" AND ie.ic_epo = iexp.ic_epo " +
				" AND ie.ic_epo = pe.ic_epo " +
				" AND ie.ic_epo = pexp.ic_epo " +
				" AND iexp.ic_epo = pe.ic_epo " +
				" AND iexp.ic_epo = pexp.ic_epo " +
				" AND pe.ic_epo = pexp.ic_epo " +
				" AND pe.ic_pyme = pexp.ic_pyme " +
				" AND pexp.ic_producto_nafin = ? " +
				" AND iexp.ic_producto_nafin = pexp.ic_producto_nafin " +
				" AND pe.cs_distribuidores in(?,?) " +
				//" AND pexp.cg_tipo_credito = ? " +
				" AND p.ic_pyme = pexp.ic_pyme " +
				" AND p.ic_pyme = pe.ic_pyme " +
				" AND n.ic_epo_pyme_if = p.ic_pyme " +
				" AND n.ic_epo_pyme_if = p.ic_pyme " +
				" AND n.ic_epo_pyme_if = pexp.ic_pyme " +
				" AND n.cg_tipo = ? "
		);
		
		conditions.add("S");
		conditions.add("R");
		conditions.add("S");
		conditions.add(new Integer(4));
		conditions.add(new Integer(4));
		conditions.add("S");
		conditions.add("R");
		//conditions.add("D");
		conditions.add("P");

		if (ic_if != null && !"".equals(ic_if)){
			strQuery.append(" AND ie.ic_if = ? ");
			conditions.add(ic_if);
		}
		if (ic_epo != null && !"".equals(ic_epo)){
			strQuery.append(" AND ie.ic_epo = ? ");
			conditions.add(ic_epo);
		}
		if (ic_pyme != null && !"".equals(ic_pyme)){
			strQuery.append("	AND p.ic_pyme = ? ");
			conditions.add(ic_pyme);
		}
		strQuery.append(" ORDER BY n.ic_nafin_electronico");

		log.debug("getDocumentQuery)"+strQuery.toString()); 
		log.debug("getDocumentQuery)"+conditions);
		return strQuery.toString();
 	}  

	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
		strQuery = new StringBuffer(); 

		strQuery.append(	
				"SELECT n.ic_nafin_electronico ne, " +
				" p.cg_razon_social rz, " +
				" p.cg_rfc rfc, " +
				" to_char(peixp.df_bloqueo_desbloq,'dd/mm/yyyy hh24:mi') fecha, " +
				" peixp.cg_causa_bloq_desbloq causa,  p.ic_pyme icpyme, pe.ic_epo icepo," +
				" CASE WHEN peixp.CS_ACTIVO is null THEN 'N' ELSE peixp.CS_ACTIVO END  as bloqueado  , " +
				" CASE WHEN peixp.CS_ACTIVO is null THEN 'N' ELSE peixp.CS_ACTIVO END  as auxbloqueado  , " +
				" peixp.cg_usuario log_usuario"
		);
		strQuery.append(
				" FROM comrel_if_epo ie,"+
				" comrel_if_epo_x_producto iexp " +
				" , comrel_pyme_epo pe "+
				" , comrel_pyme_epo_x_producto pexp LEFT JOIN comrel_pyme_epo_if_x_producto peixp ON (    pexp.ic_epo = peixp.ic_epo  AND pexp.ic_pyme = peixp.ic_pyme AND pexp.ic_producto_nafin = peixp.ic_producto_nafin  AND peixp.ic_if ="+ic_if+" ) " +//comrel_pyme_epo_x_producto  comrel_pyme_epo_if_x_producto pexp(tabla nueva)
				" , comcat_pyme p, comrel_nafin n"	);
		strQuery.append(
				" WHERE ie.ic_if = iexp.ic_if " +
				" AND ie.cs_distribuidores in (?,?) " +
				" AND iexp.cs_habilitado = ? " +
				" AND iexp.ic_producto_nafin = ? " +
				" AND ie.ic_epo = iexp.ic_epo " +
				" AND ie.ic_epo = pe.ic_epo " +
				" AND ie.ic_epo = pexp.ic_epo " +
				" AND iexp.ic_epo = pe.ic_epo " +
				" AND iexp.ic_epo = pexp.ic_epo " +
				" AND pe.ic_epo = pexp.ic_epo " +
				" AND pe.ic_pyme = pexp.ic_pyme " +
				" AND pexp.ic_producto_nafin = ? " +
				" AND iexp.ic_producto_nafin = pexp.ic_producto_nafin " +
				" AND pe.cs_distribuidores in(?,?) " +
				//" AND pexp.cg_tipo_credito = ? " +
				" AND p.ic_pyme = pexp.ic_pyme " +
				" AND p.ic_pyme = pe.ic_pyme " +
				" AND n.ic_epo_pyme_if = p.ic_pyme " +
				" AND n.ic_epo_pyme_if = p.ic_pyme " +
				" AND n.ic_epo_pyme_if = pexp.ic_pyme " +
				" AND n.cg_tipo = ? "
		);
		
		conditions.add("S");
		conditions.add("R");
		conditions.add("S");
		conditions.add(new Integer(4));
		conditions.add(new Integer(4));
		conditions.add("S");
		conditions.add("R");
		//conditions.add("D");
		conditions.add("P");

		if (ic_if != null && !"".equals(ic_if)){
			strQuery.append(" AND ie.ic_if = ? ");
			conditions.add(ic_if);
		}
		if (ic_epo != null && !"".equals(ic_epo)){
			strQuery.append(" AND ie.ic_epo = ? ");
			conditions.add(ic_epo);
		}
		if (ic_pyme != null && !"".equals(ic_pyme)){
			strQuery.append("	AND p.ic_pyme = ? ");
			conditions.add(ic_pyme);
		}
		strQuery.append("   AND ( ");
      
		for(int i=0;i<pageIds.size();i++) {
			List lItem = (ArrayList)pageIds.get(i);
			if(i>0) {
				strQuery.append(" OR ");
			}
			strQuery.append(" (n.ic_nafin_electronico = ? ) ");
			conditions.add(new Long(lItem.get(0).toString()));
		}
		strQuery.append(" ) ");
		strQuery.append(" ORDER BY n.ic_nafin_electronico ");
		log.debug("getDocumentSummaryQueryForIds "+strQuery.toString()); 
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
		return strQuery.toString();
 	}

	public String getDocumentQueryFile(){
		conditions = new ArrayList();
		strQuery = new StringBuffer();

		strQuery.append(	
				"SELECT n.ic_nafin_electronico ne, " +
				" p.cg_razon_social rz, " +
				" p.cg_rfc rfc, " +
				" to_char(peixp.df_bloqueo_desbloq,'dd/mm/yyyy hh24:mi') fecha, " +
				" peixp.cg_causa_bloq_desbloq causa,  p.ic_pyme icpyme, pe.ic_epo icepo," +
				" CASE WHEN peixp.CS_ACTIVO is null THEN 'N' ELSE peixp.CS_ACTIVO END  as bloqueado  , " +
				" CASE WHEN peixp.CS_ACTIVO is null THEN 'N' ELSE peixp.CS_ACTIVO END  as auxbloqueado  , " +
				" peixp.cg_usuario log_usuario"
		);
		strQuery.append(
				" FROM comrel_if_epo ie,"+
				" comrel_if_epo_x_producto iexp " +
				" , comrel_pyme_epo pe "+
				" , comrel_pyme_epo_x_producto pexp LEFT JOIN comrel_pyme_epo_if_x_producto peixp ON (    pexp.ic_epo = peixp.ic_epo  AND pexp.ic_pyme = peixp.ic_pyme AND pexp.ic_producto_nafin = peixp.ic_producto_nafin  AND peixp.ic_if ="+ic_if+" ) " +//comrel_pyme_epo_x_producto  comrel_pyme_epo_if_x_producto pexp(tabla nueva)
				" , comcat_pyme p, comrel_nafin n"	);
		strQuery.append(
				" WHERE ie.ic_if = iexp.ic_if " +
				" AND ie.cs_distribuidores in (?,?) " +
				" AND iexp.cs_habilitado = ? " +
				" AND iexp.ic_producto_nafin = ? " +
				" AND ie.ic_epo = iexp.ic_epo " +
				" AND ie.ic_epo = pe.ic_epo " +
				" AND ie.ic_epo = pexp.ic_epo " +
				" AND iexp.ic_epo = pe.ic_epo " +
				" AND iexp.ic_epo = pexp.ic_epo " +
				" AND pe.ic_epo = pexp.ic_epo " +
				" AND pe.ic_pyme = pexp.ic_pyme " +
				" AND pexp.ic_producto_nafin = ? " +
				" AND iexp.ic_producto_nafin = pexp.ic_producto_nafin " +
				" AND pe.cs_distribuidores in(?,?) " +
				//" AND pexp.cg_tipo_credito = ? " +
				" AND p.ic_pyme = pexp.ic_pyme " +
				" AND p.ic_pyme = pe.ic_pyme " +
				" AND n.ic_epo_pyme_if = p.ic_pyme " +
				" AND n.ic_epo_pyme_if = p.ic_pyme " +
				" AND n.ic_epo_pyme_if = pexp.ic_pyme " +
				" AND n.cg_tipo = ? "
		);
		if (ic_if != null && !"".equals(ic_if)){
			strQuery.append(" and peixp.ic_if = ? ");
			conditions.add(ic_if);
		}
		conditions.add("S");
		conditions.add("R");
		conditions.add("S");
		conditions.add(new Integer(4));
		conditions.add(new Integer(4));
		conditions.add("S");
		conditions.add("R");
	//	conditions.add("D");
		conditions.add("P");

		if (ic_if != null && !"".equals(ic_if)){
			strQuery.append(" AND ie.ic_if = ? ");  
			conditions.add(ic_if);
		}
		if (ic_epo != null && !"".equals(ic_epo)){
			strQuery.append(" AND ie.ic_epo = ? ");
			conditions.add(ic_epo);
		}
		if (ic_pyme != null && !"".equals(ic_pyme)){
			strQuery.append("	AND p.ic_pyme = ? ");
			conditions.add(ic_pyme);
		}
		strQuery.append(" ORDER BY n.ic_nafin_electronico ");

		log.debug("getDocumentQueryFile "+strQuery.toString());
		log.debug("getDocumentQueryFile)"+conditions);  
		return strQuery.toString();
	}    

	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		log.debug("crearCustomFile (E) "); 
		
		return "";
	}

	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros  rs, String path, String tipo) {
		String nombreArchivo = "";
		log.debug("crearPageCustomFile (E)"); 
		if (tipo.equals("PDF")) {
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				int nRow = 0;
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				String nae="",razonS="",rfc="",fecha="",causa="",bloq="",ic_pyme = "",ic_epo= "",usuario1="";
				while (rs.next()) {
					nae	 	= (rs.getString("NE")==null)?"":rs.getString("NE");   
					razonS	= (rs.getString("RZ")==null)?"":rs.getString("RZ");
					rfc	 	= (rs.getString("RFC")==null)?"":rs.getString("RFC");
					usuario1 = (rs.getString("LOG_USUARIO")==null)?"":rs.getString("LOG_USUARIO");
					fecha	 	= (rs.getString("FECHA")==null)?"":rs.getString("FECHA");
					causa	 	= (rs.getString("CAUSA")==null)?"":rs.getString("CAUSA");
					bloq		= (rs.getString("BLOQUEADO")==null)?"":rs.getString("BLOQUEADO").equals("S")?"Si":"No";;
					ic_pyme	= (rs.getString("ICPYME")==null)?"":rs.getString("ICPYME");
					ic_epo	= (rs.getString("ICEPO")==null)?"":rs.getString("ICEPO");
					if(nRow == 0){
						String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
						String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
						String diaActual    = fechaActual.substring(0,2);
						String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
						String anioActual   = fechaActual.substring(6,10);
						String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
						session.getAttribute("iNoNafinElectronico").toString(),  
						(String)session.getAttribute("sesExterno"),
						(String)session.getAttribute("strNombre"),
						(String)session.getAttribute("strNombreUsuario"),
						(String)session.getAttribute("strLogo"),
						(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
						pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + "------------------------------" +	horaActual, "formas", ComunesPDF.RIGHT);
						pdfDoc.addText(" ","formas", ComunesPDF.CENTER);
						pdfDoc.setTable(7,100);
						pdfDoc.setCell("N@E","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("PyME","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("RFC","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Bloqueado","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha/Hora\nBloqueo/Desbloqueo","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("IF Bloqueo/Desbloqueo","celda01rep",ComunesPDF.CENTER);
						pdfDoc.setCell("Causa","celda01rep",ComunesPDF.CENTER);
					}
					pdfDoc.setCell(nae, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(razonS, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(rfc, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(bloq, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(fecha, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(usuario1, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(causa, "formas", ComunesPDF.CENTER);
					nRow++;
				}
				if(nRow>0){
					pdfDoc.addTable();   
					pdfDoc.endDocument();
				}
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			}
		}
		log.debug("crearPageCustomFile (S)"); 
		log.debug(nombreArchivo); 
		return nombreArchivo;
	}	

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getIc_epo() {
		return ic_epo;
	}

	public String getIc_pyme() {
		return ic_pyme;
	}

	public String getIc_if() {
		return ic_if;
	}

	public List getConditions() {
		return conditions;
	}

}

