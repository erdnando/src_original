package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;
 
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;
import netropology.utilerias.usuarios.Usuario;
import netropology.utilerias.usuarios.UtilUsr;

import org.apache.commons.logging.Log;

public class ConsMantDoctoPendienteNeg implements IQueryGeneratorRegExtJS {

	public ConsMantDoctoPendienteNeg() {  }   

	private List conditions;
	StringBuffer query;   


	
	private static final Log log = ServiceLocator.getInstance().getLog(ConsMantDoctoPendienteNeg.class);//Variable para enviar mensajes al log.
	
	private String numAcuse;
	private String fechaCargaIni;
	private String fechaCargaFin;
	private String numUsuario;
	private String tipoConsulta;
	private String ic_epo;//iNoCliente
	private String perfil;//iNoCliente
	

	public String getAggregateCalculationQuery() {
		conditions = new ArrayList();	
		query 		= new StringBuffer();
		 
		
		
		
			return "";
 	}
		

	public String getDocumentQuery(){// id de los registros 
		conditions = new ArrayList();	
		query 		= new StringBuffer();
		log.info("getDocumentQuery(E)");
		String estatusOriginal = "";
		if(perfil.equals("EPO MAN1 DISTR")){
			estatusOriginal = "30";
			
		}else if(perfil.equals("EPO MAN2 DISTR")){
			estatusOriginal = "28";
		}
		if(tipoConsulta.equals("Consultar")){
			query.append("SELECT d.ic_documento FROM dis_documento d, com_acuse1 ca WHERE d.cc_acuse=ca.cc_acuse AND d.ic_estatus_docto in ("+estatusOriginal+") " );
		
			if(!"".equals(numUsuario)){
				query.append(" AND ca.ic_usuario= ?");
				conditions.add(numUsuario);
			
			}
			if(!"".equals(fechaCargaIni) &&  !"".equals(fechaCargaFin)){
				query.append(" AND ca.df_fechahora_carga >= TO_DATE(?,'dd/mm/yyyy') AND ca.df_fechahora_carga< TO_DATE(?,'dd/mm/yyyy') + 1");
				conditions.add(fechaCargaIni);
				conditions.add(fechaCargaFin);
			}
		}else if(tipoConsulta.equals("Individual")){
			query.append(
				"		SELECT   d.ic_documento " +
				"    FROM dis_documento d,"+//
				"		comcat_pyme py,"+//
				"		comcat_moneda m,"+//
				"		comrel_pyme_epo pe,"+//
				"		comcat_producto_nafin pn,"+//
				"		comrel_clasificacion clas,"+//
				"		comcat_tipo_financiamiento tf,"+//
				"		comrel_pyme_epo_x_producto pp,"+//
				"     comcat_estatus_docto ed,"+
				"		com_acuse1 ca"+
				"	WHERE d.ic_pyme = py.ic_pyme"+
				"		and d.cc_acuse = ca.cc_acuse"+
				"		AND d.ic_moneda = m.ic_moneda"+
				"		AND pe.ic_pyme = py.ic_pyme"+
				"		AND d.ic_epo = pe.ic_epo"+
				"		AND pe.ic_epo = pp.ic_epo"+
				"		AND pe.ic_pyme = pp.ic_pyme"+
				"		AND pp.ic_producto_nafin = 4"+
				"		AND pn.ic_producto_nafin = pp.ic_producto_nafin"+
				"		AND d.ic_clasificacion = clas.ic_clasificacion(+)"+
				"		 AND d.ic_estatus_docto = ed.ic_estatus_docto"+
				"		AND tf.ic_tipo_financiamiento = d.ic_tipo_financiamiento"
				);
		}
		if(perfil.equals("EPO MAN1 DISTR")){
			query.append(" AND d.ic_estatus_docto  = ?");
			conditions.add("30");
			
		}else if(perfil.equals("EPO MAN2 DISTR")){
			query.append(" AND d.ic_estatus_docto  = ?");
			conditions.add("28");
		}
		if(!"".equals(numAcuse)){
			query.append(" AND ca.cc_acuse = ?");
			conditions.add(numAcuse);
			
		}
		if(!"".equals(ic_epo)){
			query.append(" AND d.ic_epo =?");
			conditions.add(ic_epo);
			
		}
		log.debug("QUERY :::  "+query.toString());
		log.debug("conditions :::  "+conditions.toString());
		log.info("getDocumentQuery(S)");
		return query.toString();
 	}
	
	
	public String getDocumentSummaryQueryForIds(List pageIds){// funcion que muestra los datos del grid
		conditions = new ArrayList();
		String condicion= "";
		query 		= new StringBuffer();
		StringBuffer lstdoctos = new StringBuffer();
		String estatusOriginal = "";
		if(perfil.equals("EPO MAN1 DISTR")){
			estatusOriginal = "30";
			
		}else if(perfil.equals("EPO MAN2 DISTR")){
			estatusOriginal = "28";
		}   
		for(int i =0; i<pageIds.size();i++){
			List auxList = (List)pageIds.get(i);
			lstdoctos.append(""+auxList.get(0)+",");     
		
		}
		lstdoctos = lstdoctos.delete(lstdoctos.length()-1,lstdoctos.length());
		lstdoctos.append(")");
		log.info("getDocumentSummaryQueryForIds(E)");
		if(tipoConsulta.equals("Consultar")){
			if(!"".equals(numUsuario)){
				condicion +=" AND ca.ic_usuario= ?";
				conditions.add(numUsuario);
			}
			if(!"".equals(fechaCargaIni) &&  !"".equals(fechaCargaFin)){
				condicion += " AND ca.df_fechahora_carga >= TO_DATE(?,'dd/mm/yyyy') AND ca.df_fechahora_carga< TO_DATE(?,'dd/mm/yyyy') + 1";
				conditions.add(fechaCargaIni);
				conditions.add(fechaCargaFin);
			}
			if(!"".equals(numAcuse)){
				condicion +=" AND ca.cc_acuse = ?";
				conditions.add(numAcuse);
			}
			if(!"".equals(ic_epo)){
				condicion += " AND d.ic_epo =?";   
				conditions.add(ic_epo);
			}
			query.append("  SELECT   ca.cc_acuse, TO_CHAR (ca.df_fechahora_carga, 'DD/MM/YYYY') fecha_carga,"+
						"	 ca.ic_usuario, ca.in_total_docto_mn, ca.fn_total_monto_mn,"+
						"	ca.in_total_docto_dl, ca.fn_total_monto_dl, ca.cg_hash, '' as IG_NUMERO_DOCTO"+
						"   FROM dis_documento d, com_acuse1 ca"+
						"   WHERE d.cc_acuse=ca.cc_acuse AND d.ic_estatus_docto in ("+estatusOriginal+") "+ condicion +
						" GROUP BY ca.cc_acuse, TO_CHAR (ca.df_fechahora_carga, 'DD/MM/YYYY') , "+
						" ca.ic_usuario, ca.in_total_docto_mn,ca.fn_total_monto_mn,ca.in_total_docto_dl,ca.fn_total_monto_dl,ca.cg_hash "+
						" ORDER BY ca.cc_acuse, fecha_carga"
						
						);
		}else if(tipoConsulta.equals("Individual")){
				query.append(
				"		SELECT  d.ic_documento, pe.cg_num_distribuidor, py.cg_razon_social AS nombre_dist, d.ig_numero_docto,d.ct_referencia, " +
				"		TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision," +
				"		TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc,d.cg_campo1,d.cg_campo2,d.cg_campo3,d.cg_campo4,d.cg_campo5,pp.cg_tipo_credito," +
				"		d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto,m.ic_moneda,clas.cg_descripcion AS categoria,clas.ic_clasificacion AS ic_categoria," +
				"		NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento,d.ig_plazo_credito," +
				"		NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento, ca.cc_acuse acuse,d.ic_estatus_docto AS ic_estatus, tf.cd_descripcion AS modo_plazo,DECODE (pp.cg_tipo_credito,'D', 'Descuento Mercantil','C', 'Credito en Cuenta Corriente') AS tipo_credito," +
				"		d.fn_monto*(NVL(d.fn_porc_descuento,0)/100) AS monto_descontar,TO_CHAR (NVL (d.df_fecha_venc_credito, SYSDATE),'dd/mm/yyyy' ) AS df_fecha_venc_credito, ed.cd_descripcion AS estatus" +
		      "    , DECODE (d.ig_tipo_pago, '1', 'Financiamiento con intereses', '2', 'Meses sin Intereses' ) AS TIPOPAGO "+  
				
				"    FROM dis_documento d,"+//
				"		comcat_pyme py,"+//
				"		comcat_moneda m,"+//
				"		comrel_pyme_epo pe,"+//
				"		comcat_producto_nafin pn,"+//
				"		comrel_clasificacion clas,"+//
				"		comcat_tipo_financiamiento tf,"+//
				"		comrel_pyme_epo_x_producto pp,"+//
				"		comcat_estatus_docto ed,"+
				"		com_acuse1 ca"+
				"	WHERE d.ic_pyme = py.ic_pyme"+
				"		and d.cc_acuse = ca.cc_acuse"+
				"		AND d.ic_moneda = m.ic_moneda"+
				"		AND pe.ic_pyme = py.ic_pyme"+
				"		AND d.ic_epo = pe.ic_epo"+
				"		AND pe.ic_epo = pp.ic_epo"+
				"		AND pe.ic_pyme = pp.ic_pyme"+
				"		AND pp.ic_producto_nafin = 4"+   
				"		AND pn.ic_producto_nafin = pp.ic_producto_nafin"+
				"		AND d.ic_clasificacion = clas.ic_clasificacion(+)"+
				"		 AND d.ic_estatus_docto = ed.ic_estatus_docto"+
				"		AND tf.ic_tipo_financiamiento = d.ic_tipo_financiamiento "+
				"     AND d.ic_documento in ("+lstdoctos
				
				);
		}
	
		
		log.debug("QUERY :::  "+query.toString());
		log.debug("conditions :::  "+conditions.toString());
		log.info("getDocumentSummaryQueryForIds(S)");
		
		
						
		return query.toString();
 	}
		
			
	public String getDocumentQueryFile(){// funcion para mostrar en el archivo 
		String condicion= "";
		conditions = new ArrayList();
		query = new StringBuffer();
		log.info("getDocumentQueryFile(E)");
		String estatusOriginal = "";
		if(perfil.equals("EPO MAN1 DISTR")){
			estatusOriginal = "30";
			
		}else if(perfil.equals("EPO MAN2 DISTR")){
			estatusOriginal = "28";
		}
		if(!"".equals(numUsuario)){
				condicion =" AND ca.ic_usuario= ?";
				conditions.add(numUsuario);
			}
			if(!"".equals(fechaCargaIni) &&  !"".equals(fechaCargaFin)){
				condicion = " AND ca.df_fechahora_carga >= TO_DATE(?,'dd/mm/yyyy') AND ca.df_fechahora_carga< TO_DATE(?,'dd/mm/yyyy') + 1";
				conditions.add(fechaCargaIni);
				conditions.add(fechaCargaFin);
			}
			if(!"".equals(numAcuse)){
				condicion =" AND ca.cc_acuse = ?";
				conditions.add(numAcuse);
			}
			if(!"".equals(ic_epo)){
				condicion = " AND d.ic_epo =?";   
				conditions.add(ic_epo);
			}
			query.append("  SELECT   ca.cc_acuse, TO_CHAR (ca.df_fechahora_carga, 'DD/MM/YYYY') fecha_carga,"+
						"	 ca.ic_usuario, ca.in_total_docto_mn, ca.fn_total_monto_mn,"+
						"	ca.in_total_docto_dl, ca.fn_total_monto_dl, ca.cg_hash, '' as IG_NUMERO_DOCTO"+
						"   FROM dis_documento d, com_acuse1 ca"+
						"   WHERE d.cc_acuse=ca.cc_acuse AND d.ic_estatus_docto in ("+estatusOriginal+")"+ condicion +
						" GROUP BY ca.cc_acuse, TO_CHAR (ca.df_fechahora_carga, 'DD/MM/YYYY') , "+
						" ca.ic_usuario, ca.in_total_docto_mn,ca.fn_total_monto_mn,ca.in_total_docto_dl,ca.fn_total_monto_dl,ca.cg_hash "+
						" ORDER BY ca.cc_acuse, fecha_carga"
						
						);
		
		log.debug("QUERY :::  "+query.toString());
		log.debug("conditions :::  "+conditions.toString());
		log.info("getDocumentQueryFile(S)");
	
		return query.toString();
 	} 		
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		
		String nombreArchivo = "";
		CreaArchivo archivo = new CreaArchivo();
		HttpSession session = request.getSession();	
		StringBuffer contenidoArchivo = new StringBuffer();
		ComunesPDF pdfDoc = new ComunesPDF();
		
		UtilUsr utilUsr = new UtilUsr();
		String numDoctos = "";
		
		try {
		
			if(tipo.equals("PDF") ) {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
				String pais        = (String)(request.getSession().getAttribute("strPais") == null?"":request.getSession().getAttribute("strPais"));
				String noCliente   = (String)(request.getSession().getAttribute("iNoCliente") == null?"":request.getSession().getAttribute("iNoCliente"));
				String strnombre   = (String)(request.getSession().getAttribute("strNombre") == null?"":request.getSession().getAttribute("strNombre"));
				String nombreUsr   = (String)(request.getSession().getAttribute("strNombreUsuario") == null?"":request.getSession().getAttribute("strNombreUsuario"));
				String logo      	 = (String)(request.getSession().getAttribute("strLogo") == null?"":request.getSession().getAttribute("strLogo"));
		
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
  
				pdfDoc.encabezadoConImagenes(pdfDoc,pais,noCliente,strnombre,nombreUsr, "", logo,(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formasrep",ComunesPDF.RIGHT);

				
				pdfDoc.setTable(4,80);
				if(perfil.equals("EPO MAN1 DISTR")){
					pdfDoc.setCell("Mantenimiento Doctos. Pendientes Negociables","celda01rep",ComunesPDF.CENTER,4);
				}else{
					pdfDoc.setCell("Mantenimiento Doctos. Pre-Negociables","celda01rep",ComunesPDF.CENTER,4);
				}
				pdfDoc.setCell("N�mero de Acuse","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Usuario","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Documentos","celda01rep",ComunesPDF.CENTER);
			} 
			while(rs.next()){
				int doctosMN = 0;
				int doctosDL = 0;
				if(!rs.getString("IN_TOTAL_DOCTO_MN").equals(null)){//in_total_docto_dl
					doctosMN = rs.getString("IN_TOTAL_DOCTO_MN")!=null?Integer.parseInt(rs.getString("IN_TOTAL_DOCTO_MN").toString()):doctosMN;

				}
				if(!rs.getString("IN_TOTAL_DOCTO_DL").equals(null)){//in_total_docto_mn
					doctosDL = rs.getString("IN_TOTAL_DOCTO_DL")!=null?Integer.parseInt(rs.getString("IN_TOTAL_DOCTO_DL").toString()):doctosDL;

				}
			
				String CC_ACUSE = (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");	
				String FECHA_CARGA = (rs.getString("FECHA_CARGA")==null)?"":rs.getString("FECHA_CARGA");	
				String IC_USUARIO = (rs.getString("IC_USUARIO")==null)?"":rs.getString("IC_USUARIO");	
				String IG_NUMERO_DOCTO = (rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO");		
				
				Usuario usuario = utilUsr.getUsuario(IC_USUARIO);
				String 	strNombreUsuario = usuario.getApellidoPaterno() + " " + usuario.getApellidoMaterno() + " " + usuario.getNombre();
				numDoctos	= String.valueOf(doctosMN+doctosDL);
				pdfDoc.setCell(CC_ACUSE,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(FECHA_CARGA ,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(IC_USUARIO+" "+strNombreUsuario,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(numDoctos,"formasrep",ComunesPDF.CENTER);
			} // while
			pdfDoc.addTable();
			pdfDoc.endDocument();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	/**
	 * Funcion que verifica si la EPO acepta "Factoraje Vencido" 
	 * @return 
	 * @throws java.lang.Exception
	 * @param sNoCliente
	 */

	
	public List getConditions() {
		return conditions;
	}

	public String getNumAcuse() {
		return numAcuse;
	}

	public void setNumAcuse(String numAcuse) {
		this.numAcuse = numAcuse;
	}

	public String getFechaCargaIni() {
		return fechaCargaIni;
	}

	public void setFechaCargaIni(String fechaCargaIni) {
		this.fechaCargaIni = fechaCargaIni;
	}
	
	public String getFechaCargaFin() {
		return fechaCargaFin;
	}

	public void setFechaCargaFin(String fechaCargaFin) {
		this.fechaCargaFin = fechaCargaFin;
	}
	
	public String getNumUsuario() {
		return numUsuario;
	}

	public void setNumUsuario(String numUsuario) {
		this.numUsuario = numUsuario;
	}

	public String getTipoConsulta() {
		return tipoConsulta;
	}

	public void setTipoConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}
	
	//perfil
	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}
	
	
	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}

	
	

}