package com.netro.distribuidores;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * FODEA-021-2014:
 * Esta clase tiene como finalidad brindar la funcionalidad para la pantalla.
 * 	Distribuidores / Capturas / Sub-l�mites distribuidor
 */
public class SubLimitesDistribuidor implements IQueryGeneratorRegExtJS {
	
	
	private static final Log log = ServiceLocator.getInstance().getLog(SubLimitesDistribuidor.class);
	
	//ATRIBUTOS
	private String icEPO;
	private String icIf;
	private String icPyme;
	private String modalidad;
		//DATOS QUE SE ALMACENAN (insertar)
	private String porcentajeLinea;//(insert || update)
	private String montoSubLimite;//(insert || update)
	private String icMoneda;
	private String usuarioAlta;//(insert )
	private String fechaAlta;//(insert )
	private String usuarioModifico;//(update)
	private String fechaModifico;//(update)
	
	private List 		conditions ;
	
	StringBuffer qrySentencia = new StringBuffer("");
	
	public SubLimitesDistribuidor() {
	}
	

		/**
	 * 
	 * @return 
	 */
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	
	public String getDocumentQuery() {
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		qrySentencia.append("SELECT	\n"+ 
										"      LC.IC_LINEA_CREDITO_DM	\n"+ 
										"FROM 	\n"+ 
										"	 DIS_LINEA_CREDITO_DM LC, 	\n"+ 
										"	 DIS_ACUSE4 AC4	\n"+ 
										"WHERE 	\n"+ 
										"	 LC.CC_ACUSE = AC4.CC_ACUSE	\n"+ 
										"	 AND LC.CG_TIPO_SOLICITUD = 'I'	\n"+ 
										"	 AND LC.IC_IF = ?	\n"+ 
										"	 AND LC.IC_EPO = ?");
			conditions.add(icIf);
			conditions.add(icEPO);
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	/**
	 * 
	 * @return 
	 * @param pageIds
	 */
	public String getDocumentSummaryQueryForIds(List pageIds) {
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		qrySentencia = new StringBuffer("");
		
		if(this.estaRegistrado() ){
		conditions 		= new ArrayList();
		qrySentencia = new StringBuffer("");
		qrySentencia.append(	"SELECT \n"+ 
									"   L.IC_LINEA_CREDITO_DM, \n"+
									"    L.IC_IF, \n"+
									"    L.IC_EPO, \n"+
									"    L.IC_PYME, \n"+
									"    L.DISTRIBUIDOR, \n"+
									"    L.MONEDA, \n"+
									"    L.FN_MONTO_AUTORIZADO, \n"+
									"    L.IC_MONEDA, \n"+
									"    S.FN_POR_LINEA AS PORCENTEJE_LINEA , \n"+
									"    TO_CHAR(S.DF_ALTA,'DD/MM/YYYY') AS FECHA_ALTA, \n"+
									"    S.CG_USUARIO_ALTA  AS USUARIO_ALTA, \n"+
									"    TO_CHAR(S.DF_MODIFICA,'DD/MM/YYYY') AS FECHA_ULTIMA_MODIFICACION , \n"+
									"    S.CG_USUARIO_MODIFICA AS USUARIO_ULTIMA_MODIFICACION \n"+
									" FROM \n"+
									"( SELECT   \n"+ 
									"    LD.IC_LINEA_CREDITO_DM, \n"+
									"    LD.IC_IF,  \n"+
									"    LD.IC_EPO, \n"+
									"    D.CLAVE AS IC_PYME, \n"+
									"    D.PYME||'|'||D.PYMERFC AS DISTRIBUIDOR, \n"+
									"    M.CD_NOMBRE AS MONEDA,  \n"+
									"    LD.FN_MONTO_AUTORIZADO, \n"+
									"    LD.IC_MONEDA  \n"+
									"FROM  \n"+
									"    DIS_LINEA_CREDITO_DM LD, \n"+
									"    COMCAT_MONEDA M, \n"+
									"    (SELECT   CP.IC_PYME AS CLAVE, CG_RAZON_SOCIAL AS PYME, CP.CG_RFC AS PYMERFC \n"+
									"         FROM COMCAT_PYME CP, \n"+
									"              COMREL_PYME_EPO CPE, \n"+
									"              COMREL_PYME_EPO_X_PRODUCTO PEXP \n"+
									"        WHERE CP.IC_PYME = CPE.IC_PYME \n"+
									"          AND CP.IC_PYME = PEXP.IC_PYME \n"+
									"          AND CPE.IC_EPO = PEXP.IC_EPO \n"+
									"         AND CPE.CS_DISTRIBUIDORES IN ('S', 'R') \n"+
									"          AND PEXP.IC_PRODUCTO_NAFIN = 4 \n"+
									"          AND CP.CS_INVALIDO != 'S' \n"+
									"         AND PEXP.IC_EPO = ? \n"+
									"          AND CP.IC_PYME = ? \n"+
									"          AND PEXP.CS_HABILITADO IN ('S', 'N') \n"+
									"     )D \n"+
									"WHERE \n"+ 
									"    LD.IC_MONEDA = M.IC_MONEDA \n"+
									"    AND LD.IC_EPO = ? \n"+
									"    AND LD.IC_IF = ? \n"+
									"    AND LD.IC_LINEA_CREDITO_DM IN ( \n"+
									"        SELECT LC.IC_LINEA_CREDITO_DM \n"+
									"            FROM DIS_LINEA_CREDITO_DM LC, DIS_ACUSE4 AC4 \n"+
									"        WHERE LC.CC_ACUSE = AC4.CC_ACUSE \n"+
									"            AND LC.CG_TIPO_SOLICITUD = 'I' \n"+
									"            AND LC.IC_EPO = ? \n"+
									"            AND LC.IC_IF = ? )	\n"+
									"GROUP BY \n"+
									"    LD.IC_LINEA_CREDITO_DM, \n"+ 
									"    LD.IC_IF, \n"+
									"    LD.IC_EPO, \n"+
									"    LD.FN_MONTO_AUTORIZADO, \n"+
									"    M.CD_NOMBRE, \n"+
									"    LD.IC_MONEDA, \n"+
									"    D.PYME, \n"+
									"    D.CLAVE, \n"+
									"    D.PYMERFC \n"+
									"ORDER BY LD.IC_MONEDA ) L, \n"+
									"    DIS_SUBLIMITES S \n"+
									"WHERE \n"+
									"   L.IC_IF	  = S.IC_IF	AND \n"+
									"   L.IC_EPO   = S.IC_EPO 	AND \n"+
									"   L.IC_PYME	= S.IC_PYME	AND \n"+
									"   L.IC_MONEDA = S.IC_MONEDA \n");
		}else{
			conditions 		= new ArrayList();
			qrySentencia = new StringBuffer("");
			qrySentencia.append("SELECT \n"+
									" L.IC_LINEA_CREDITO_DM,  \n"+
									" L.IC_IF,  \n"+
									" L.IC_EPO,  \n"+
									" L.IC_PYME,  \n"+
									" L.DISTRIBUIDOR,  \n"+
									" L.MONEDA,  \n"+
									" L.FN_MONTO_AUTORIZADO,  \n"+
									" L.IC_MONEDA,  \n"+
									" '' AS PORCENTEJE_LINEA ,  \n"+ 
									" '' AS FECHA_ALTA,  \n"+
									" '' AS USUARIO_ALTA,  \n"+
									" '' AS FECHA_ULTIMA_MODIFICACION ,  \n"+
									" '' AS USUARIO_ULTIMA_MODIFICACION  \n"+
								 "FROM  \n"+
								"( SELECT    \n"+
								"	 LD.IC_LINEA_CREDITO_DM,  \n"+
								"	 LD.IC_IF,   \n"+
								"	 LD.IC_EPO,  \n"+
								"	 D.CLAVE AS IC_PYME,  \n"+
								"	 D.PYME||'|'||D.PYMERFC AS DISTRIBUIDOR,  \n"+
								"	 M.CD_NOMBRE AS MONEDA,   \n"+
								"	 LD.FN_MONTO_AUTORIZADO,  \n"+
								"	 LD.IC_MONEDA   \n"+
								"FROM   \n"+
								"	 DIS_LINEA_CREDITO_DM LD,  \n"+
								"	 COMCAT_MONEDA M,  \n"+
								"	 (SELECT   CP.IC_PYME AS CLAVE, CG_RAZON_SOCIAL AS PYME, CP.CG_RFC AS PYMERFC  \n"+
								"			FROM COMCAT_PYME CP,  \n"+
								"				  COMREL_PYME_EPO CPE,  \n"+
								"				  COMREL_PYME_EPO_X_PRODUCTO PEXP  \n"+
								"		  WHERE CP.IC_PYME = CPE.IC_PYME  \n"+
								"			 AND CP.IC_PYME = PEXP.IC_PYME  \n"+
								"			 AND CPE.IC_EPO = PEXP.IC_EPO  \n"+
								"			AND CPE.CS_DISTRIBUIDORES IN ('S', 'R')  \n"+
								"			 AND PEXP.IC_PRODUCTO_NAFIN = 4  \n"+
								"			 AND CP.CS_INVALIDO != 'S'  \n"+
								"			AND PEXP.IC_EPO = ?  \n"+
								"			 AND CP.IC_PYME = ? \n"+
								"			 AND PEXP.CS_HABILITADO IN ('S', 'N')  \n"+
								"	  )D  \n"+
								"WHERE  \n"+
								"	 LD.IC_MONEDA = M.IC_MONEDA  \n"+
								"	 AND LD.IC_EPO = ?  \n"+
								"	 AND LD.IC_IF = ?  \n"+
								"	 AND LD.IC_LINEA_CREDITO_DM IN (  \n"+
								"		  SELECT LC.IC_LINEA_CREDITO_DM  \n"+
								"				FROM DIS_LINEA_CREDITO_DM LC, DIS_ACUSE4 AC4  \n"+
								"		  WHERE LC.CC_ACUSE = AC4.CC_ACUSE  \n"+
								"				AND LC.CG_TIPO_SOLICITUD = 'I'  \n"+
								"				AND LC.IC_EPO = ?  \n"+
								"				AND LC.IC_IF = ? )     \n"+
								"GROUP BY  \n"+
								"	 LD.IC_LINEA_CREDITO_DM,  \n"+
								"	 LD.IC_IF,  \n"+
								"	 LD.IC_EPO,  \n"+
								"	 LD.FN_MONTO_AUTORIZADO,  \n"+
								"	 M.CD_NOMBRE,  \n"+
								"	 LD.IC_MONEDA,  \n"+
								"	 D.PYME,  \n"+
								"	 D.CLAVE,  \n"+
								"	 D.PYMERFC  \n"+
								"ORDER BY LD.IC_MONEDA ) L ");
		
		}
		
			conditions.add(icEPO);
			conditions.add(icPyme);
			conditions.add(icEPO);
			conditions.add(icIf);
			conditions.add(icEPO);
			conditions.add(icIf);
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	/**
	 * 
	 * @return regresa la sentencia sql para generar la consulta 
	 */
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			qrySentencia.append("");

		log.info("qrySentencia > "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
	/**
	 * 
	 * @return 
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	 public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		log.info("crearPageCustomFile(E)");
		String nombreArchivo = "";
		return nombreArchivo;
					
	}
	/**
	 * 
	 * @return	: retorna el nombre del archivo
	 * @param tipo : de archivo a generar ('PDF' o 'CSV' )
	 * @param path : ruta temporal dond se descargara el archivo
	 * @param rs	: 
	 * @param request : request
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		log.info("crearCustomFile (E)");
		String nombreArchivo = "";
	return nombreArchivo;	
	}
	
	private boolean estaRegistrado(){
		log.info("estaRegistrado (E) ");
		boolean registrado= false;
		AccesoDB con = new AccesoDB();
		Registros registros  = new Registros();
		PreparedStatement ps = null;
		ResultSet rs = null; 
		qrySentencia = new StringBuffer("");
		conditions= new ArrayList();
		try{
			con.conexionDB();
			qrySentencia.append("SELECT	\n"+
								"	 DS.IC_SUB_LIMITES 	\n"+
								"FROM 	\n"+
								"	 DIS_SUBLIMITES DS	\n"+
								"WHERE	\n"+
								"	 DS.IC_IF = ?	\n"+
								"	 AND DS.IC_EPO= ?	\n"+
								"AND DS.IC_PYME= ?");
			conditions.add(icIf);
			conditions.add(icEPO);
			conditions.add(icPyme);
			registros=con.consultarDB(qrySentencia.toString(),conditions);

			registrado= registros.next();

		}catch(Exception e){
			registrado=false;
		
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		log.info("guardarDatos (E) ");
		
		return registrado;
	}
	/**
	 * Metodo que raaliza un INSERT a la tabla DIS_SUBLIMITES, 
	 * segun los parametros recibidos.
	 * @return  true si se ralizo el update false en caso contrario
	 */
	public boolean guardarDatos(){//INSERT
		log.info("guardarDatos (E) ");
		boolean exito= true;
		AccesoDB con = new AccesoDB();
		qrySentencia = new StringBuffer("");
		conditions= new ArrayList();
		try{
			con.conexionDB();
			qrySentencia.append("INSERT INTO dis_sublimites \n"+
			"    (IC_SUB_LIMITES,IC_IF,IC_EPO,IC_PYME,IC_MONEDA,FN_POR_LINEA,DF_ALTA,CG_USUARIO_ALTA) \n"+
			"    VALUES(seq_dis_sublimites.nextval,?,?,?,?,?,SYSDATE,?)");
			conditions.add(icIf);
			conditions.add(icEPO);
			conditions.add(icPyme);
			conditions.add(icMoneda);
			conditions.add(porcentajeLinea);
			conditions.add(usuarioAlta);
			
			int insert=con.ejecutaUpdateDB(qrySentencia.toString(),conditions);
			if(insert!=0){
				exito= true;
			}else{
				exito= false;
			}
		}catch(Exception e){
			log.error("guardarDatos  Error: " + e);
			exito=false;
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		} 
		log.info("guardarDatos (E) ");
		return exito;
	}
	/**
	 * Metodo que raaliza un UPDATE a la tabla DIS_SUBLIMITES, 
	 * segun los parametros recibidos.
	 * @return  true si se ralizo el update false en caso contrario
	 */
	public boolean actualizaDatos(){//UPDATE
		log.info("actualizaDatos (E) ");
		boolean exito= true;
		AccesoDB con = new AccesoDB();
		qrySentencia = new StringBuffer("");
		conditions= new ArrayList();
		try{
			con.conexionDB();
			qrySentencia.append("UPDATE DIS_SUBLIMITES \n"+
										"SET FN_POR_LINEA = ?, \n"+
										"    DF_MODIFICA = SYSDATE, \n"+
										"    CG_USUARIO_MODIFICA = ? \n"+
										"WHERE  \n"+
										"    IC_IF   = ? AND \n"+
										"    IC_EPO   = ? AND \n"+
										"    IC_PYME   = ? AND \n"+
										"    IC_MONEDA   = ? \n");
				
			
			conditions.add(porcentajeLinea);
			conditions.add(usuarioModifico);
			conditions.add(icIf);
			conditions.add(icEPO);
			conditions.add(icPyme);
			conditions.add(icMoneda);

			con.ejecutaUpdateDB(qrySentencia.toString(),conditions);
		}catch(Exception e){
			exito= false;
			log.error("actualizaDatos  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		} 
		log.info("actualizaDatos (E) ");
		return exito;
	}
	/**
	 *  Metodo que hace una consulta para saber si ya se a capturado un % linea y
	 *  asi udentificar si se realiza un insert (si false) o un opdate  (si verdadero)
	 * @return  true/false
	 */
	public boolean tienePocentajeCapturado(){
		boolean existe=true;
		log.info("tienePocentajeCapturado (E) ");
		boolean exito= true;
		AccesoDB con = new AccesoDB();
		Registros registros  = new Registros();
		PreparedStatement ps = null;
		ResultSet rs = null; 
		qrySentencia = new StringBuffer("");
		conditions = new ArrayList();
		try{
			con.conexionDB();
			qrySentencia.append("Select  \n"+
										"	 fn_por_linea   \n"+
										"from   \n"+
										"	dis_sublimites   \n"+
										"where   \n"+
										"	 ic_if=? and   \n"+
										"	 ic_epo =? and   \n"+
										"	 ic_pyme=? and   \n"+
										"	 ic_moneda =?");
				
				/**VALIDAR EL QUERY  */
			conditions.add(icIf);
			conditions.add(icEPO);
			conditions.add(icPyme);
			conditions.add(icMoneda);
			registros=con.consultarDB(qrySentencia.toString(),conditions);

			existe= registros.next();

		}catch(Exception e){
			e.printStackTrace();
			existe=false;
		
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("tienePocentajeCapturado (E) ");
		return existe;
	}
	/**
	 * Consulta a la tabla dis_sublimites para obtener los datos 
	 * @return Regostro con la informacion de la columna
	 */
	public Registros  getDataGuardada( ){
		log.info("getPorcentajeLineaData (E) ");
		AccesoDB con = new AccesoDB(); 
		ResultSet rs = null;	
		HashMap datos =  new HashMap();
		Registros registros  = new Registros();
		qrySentencia = new StringBuffer("");
		conditions = new ArrayList();
		try{
			con.conexionDB();
			qrySentencia.append("Select  \n"+
										"	 FN_POR_LINEA  as porcentaje_linea,  \n"+
										"	 to_char(DF_ALTA,'dd/mm/yyyy') as fecha_alta,     \n"+
										"	 to_char(DF_MODIFICA,'dd/mm/yyyy') as fecha_mmodificacion,     \n"+
										"	 CG_USUARIO_ALTA as usuario_alta,     \n"+
										"	 CG_USUARIO_MODIFICA  as usuario_modifico     \n"+
										"from   \n"+
										"	dis_sublimites   \n"+										
										"where   \n"+
										"	 ic_if=? and   \n"+
										"	 ic_epo= ? and   \n"+
										"	 ic_pyme=? and   \n"+
										"	 ic_moneda =?");
				
			
			conditions.add(icIf);
			conditions.add(icEPO);
			conditions.add(icPyme);
			conditions.add(icMoneda);
			registros=con.consultarDB(qrySentencia.toString(),conditions);
	
			//con.cierraConexionDB();
		} catch (Exception e) {
			log.error("getPorcentajeLineaData  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("getPorcentajeLineaData (S) ");
		return registros;
	}	
	
	public Registros  getFechaAltaData( ){
		log.info("getFechaAltaData (E) ");
		AccesoDB con = new AccesoDB(); 
		ResultSet rs = null;	
		HashMap datos =  new HashMap();
		Registros registros  = new Registros();
		qrySentencia = new StringBuffer("");
		conditions = new ArrayList();
		try{
			con.conexionDB();
			qrySentencia.append("Select  \n"+
										"	 to_char(DF_ALTA,'dd/mm/yyyy') as fecha_alta     \n"+
										"from   \n"+
										"	dis_sublimites   \n"+
										"where   \n"+
										"	 ic_if=? and   \n"+
										"	 ic_epo =? and   \n"+
										"	 ic_pyme=? and   \n"+
										"	 ic_moneda =?");
				
				/**VALIDAR EL QUERY  */
			
			conditions.add(icIf);
			conditions.add(icEPO);
			conditions.add(icPyme);
			conditions.add(icMoneda);
			registros=con.consultarDB(qrySentencia.toString(),conditions);
	
			//con.cierraConexionDB();
		} catch (Exception e) {
			log.error("getFechaAltaData  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("getFechaAltaData (S) ");
		return registros;
	}	

	
	
	public void setConditions(List conditions) {
		this.conditions = conditions;
	}


	public List getConditions() {
		return conditions;
	}




	public void setModalidad(String modalidad) {
		this.modalidad = modalidad;
	}


	public String getModalidad() {
		return modalidad;
	}


	public void setIcEPO(String icEPO) {
		this.icEPO = icEPO;
	}


	public String getIcEPO() {
		return icEPO;
	}


	public void setIcPyme(String icPyme) {
		this.icPyme = icPyme;
	}


	public String getIcPyme() {
		return icPyme;
	}


	public void setIcIf(String icIf) {
		this.icIf = icIf;
	}


	public String getIcIf() {
		return icIf;
	}


	public void setPorcentajeLinea(String porcentajeLinea) {
		this.porcentajeLinea = porcentajeLinea;
	}


	public String getPorcentajeLinea() {
		return porcentajeLinea;
	}


	public void setMontoSubLimite(String montoSubLimite) {
		this.montoSubLimite = montoSubLimite;
	}


	public String getMontoSubLimite() {
		return montoSubLimite;
	}


	public void setUsuarioAlta(String usuarioAlta) {
		this.usuarioAlta = usuarioAlta;
	}


	public String getUsuarioAlta() {
		return usuarioAlta;
	}


	public void setFechaAlta(String fechaAlta) {
		this.fechaAlta = fechaAlta;
	}


	public String getFechaAlta() {
		return fechaAlta;
	}


	public void setUsuarioModifico(String usuarioModifico) {
		this.usuarioModifico = usuarioModifico;
	}


	public String getUsuarioModifico() {
		return usuarioModifico;
	}


	public void setFechaModifico(String fechaModifico) {
		this.fechaModifico = fechaModifico;
	}


	public String getFechaModifico() {
		return fechaModifico;
	}


	public void setIcMoneda(String icMoneda) {
		this.icMoneda = icMoneda;
	}


	public String getIcMoneda() {
		return icMoneda;
	}
}