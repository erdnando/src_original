package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorReg;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class EstadoAdeudosPyme implements IQueryGeneratorReg, IQueryGeneratorRegExtJS {

  public EstadoAdeudosPyme(){} 
 
 		//Variable para enviar mensajes al log.
	private static final Log log = ServiceLocator.getInstance().getLog(EstadoAdeudosPyme.class);
	
	/**
	 * Contiene la lista de valores (parametros) a emplear en las condiciones
	 */
	StringBuffer 		qrySentencia;
	private List 		conditions;
	private String 	paginaOffset;
	private String 	paginaNo;		
	private String 	directorio;
	public String paginar;
	public String ic_if;
	public String ic_epo;
	public String ic_moneda;
	public String fecha_inicio;
	public String fecha_final;
	public String usuario;
	String cliente = "";
	String bancoRef = "";

	/**
	 * Obtiene el query para obtener los totales de la consulta
	 * @return Cadena con la consulta de SQL, para obtener los totales
	 */

	public String getAggregateCalculationQuery() {
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
	 
    log.debug("-------------------------:: ");		
		log.debug("paginaOffset:: "+paginaOffset);
    log.debug("paginaNo:: "+paginaNo); 
    log.debug("ic_if:: "+ic_if);   
    log.debug("ic_epo:: "+ic_epo);   
    log.debug("ic_moneda:: "+ic_moneda);   
    log.debug("fecha_inicio:: "+fecha_inicio);   
    log.debug("fecha_final:: "+fecha_final); 
    log.debug("usuario:: "+usuario); 
	  log.debug("-------------------------:: "); 
 
	  qrySentencia.append("SELECT count(1) total, 'EstadoAdeudosPyme ::getAggregateCalculationQuery' origen " );
      
      
      qrySentencia.append("  FROM dis_documento doc, "+
                          "       dis_solicitud sol, "+
                          "       dis_docto_seleccionado sel, "+
                          "       comrel_pyme_epo cpe, "+
                          "       comcat_epo e, "+
                          "       comrel_producto_epo pe, "+
                          "       comcat_producto_nafin pn, "+
                          "       comcat_tipo_cobro_interes tc, "+
                          "       com_linea_credito lc, "+
                          "       com_acuse3 ac3, "+
                          "       comcat_estatus_docto ed ");
                          
      qrySentencia.append(" WHERE sol.ic_documento = doc.ic_documento "+
                          "   AND sel.ic_documento = doc.ic_documento "+
                          "   AND cpe.ic_pyme = doc.ic_pyme "+
                          "   AND cpe.ic_epo = doc.ic_epo "+
                          "   AND pn.ic_producto_nafin = 4 "+
                          "   AND e.ic_epo = doc.ic_epo "+
                          "   AND pn.ic_producto_nafin = pe.ic_producto_nafin "+
                          "   AND pe.ic_epo = doc.ic_epo "+
                          "   AND lc.ic_tipo_cobro_interes = tc.ic_tipo_cobro_interes "+                  
                          "   AND ac3.cc_acuse = sol.cc_acuse "+
                          "   AND ed.ic_estatus_docto = doc.ic_estatus_docto "+
                          "   AND lc.ic_linea_credito = doc.ic_linea_credito ");
                          
	
    if (usuario!=null){ 
        qrySentencia.append(" AND doc.ic_pyme = ?  ");
        conditions.add(usuario);
    }
    if (ic_moneda!=null ) { 
        qrySentencia.append("   AND doc.ic_moneda = ? ");
        conditions.add(ic_moneda);
    }  
    if(ic_epo!=null) {
        qrySentencia.append(" AND doc.ic_epo = ?");
         conditions.add(ic_epo);   
    }
    if(ic_if!=null) {
        qrySentencia.append(" AND lc.ic_if = ?");
         conditions.add(ic_if);   
    }

     //if(fecha_inicio!=null && fecha_final!=null ){ 
    if( !fecha_inicio.equals("") && !fecha_final.equals("")){
         qrySentencia.append( "   AND ((    doc.ic_estatus_docto = 4 "+
                              "         AND TRUNC (ac3.df_fecha_hora) BETWEEN "+
                              "                TRUNC (TO_DATE ('"+fecha_inicio+"', 'dd/mm/yyyy')) AND "+
                              "                TRUNC (TO_DATE ('"+fecha_final+"', 'dd/mm/yyyy')) "+
                              "           ) "+
                              "        OR (    doc.ic_estatus_docto IN (11,  22) "+
                              "            AND doc.ic_documento IN "+
                              "                   (SELECT ic_documento "+
                              "                      FROM dis_cambio_estatus "+
                              "                     WHERE dc_fecha_cambio IN "+
                              "                              (SELECT MAX (dc_fecha_cambio) "+
                              "                                 FROM dis_cambio_estatus "+
                              "                                WHERE ic_documento = doc.ic_documento "+
                              "                                  AND TRUNC (dc_fecha_cambio) BETWEEN "+
                              "                                         TRUNC ( "+
                              "                                            TO_DATE ('"+fecha_inicio+"', 'dd/mm/yyyy') "+
                              "                                         ) AND "+
                              "                                         TRUNC ( "+
                              "                                            TO_DATE ('"+fecha_final+"', 'dd/mm/yyyy') "+
                              "                                         )) "+
                              "                       AND ic_cambio_estatus IN (18,  19,  20)) "+
                              "           ) "+
                              "       ) ");
    }
  
  

    log.debug("getAggregateCalculationQuery "+conditions.toString());
		log.debug("getAggregateCalculationQuery "+qrySentencia.toString());
	
		return qrySentencia.toString();
		
	}
  
 
		
	 /**
	 * Obtiene el query para obtener las llaves primarias de la consulta
	 * @return Cadena con la consulta de SQL, para obtener llaves primarias
	 */
	public String getDocumentQuery(){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
    
    log.debug("-------------------------:: ");		
		log.debug("paginaOffset:: "+paginaOffset);
    log.debug("paginaNo:: "+paginaNo); 
    log.debug("ic_if:: "+ic_if);   
    log.debug("ic_epo:: "+ic_epo);   
    log.debug("ic_moneda:: "+ic_moneda);   
    log.debug("fecha_inicio:: "+fecha_inicio);   
    log.debug("fecha_final:: "+fecha_final); 
    log.debug("usuario:: "+usuario); 
	  log.debug("-------------------------:: "); 
    
    
    qrySentencia.append(" SELECT doc.ic_documento, "+
                        " TO_CHAR (ac3.df_fecha_hora, 'DD/MM/YYYY') AS fecha_opera, "+
                        " cpe.cg_num_distribuidor AS num_cliente, "+
                        " e.cg_razon_social AS epo, "+
                        " ig_numero_docto AS num_docto, "+
                        " doc.ic_documento AS num_credito, "+
                        " TO_CHAR (sol.df_v_credito, 'DD/MM/YYYY') AS f_venc_credito, "+
                        " doc.fn_monto AS monto_cred, "+
                        " DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
                        " 'N', 'Sin conversion','P', 'Peso-Dolar','') AS tipo_conversion, "+
                        " sel.fn_importe_interes AS int_generados, "+
                        " DECODE (doc.ic_estatus_docto, 11, doc.fn_monto, 0) AS pago_a_capital, "+
                        " '' AS pago_int, "+
                        " DECODE (doc.ic_estatus_docto,4, doc.fn_monto,22, doc.fn_monto,0) AS saldo_cre, "+
                        " tc.cd_descripcion AS tipo_cob_int, "+
                        " ed.cg_desc_alterna AS estatus ,"+
                        " tc.ic_tipo_cobro_interes,"+
                        " doc.ic_estatus_docto ");
                        
     qrySentencia.append("  FROM dis_documento doc, "+
                          "       dis_solicitud sol, "+
                          "       dis_docto_seleccionado sel, "+
                          "       comrel_pyme_epo cpe, "+
                          "       comcat_epo e, "+
                          "       comrel_producto_epo pe, "+
                          "       comcat_producto_nafin pn, "+
                          "       comcat_tipo_cobro_interes tc, "+
                          "       com_linea_credito lc, "+
                          "       com_acuse3 ac3, "+
                          "       comcat_estatus_docto ed ");
                          
      qrySentencia.append(" WHERE sol.ic_documento = doc.ic_documento "+
                          "   AND sel.ic_documento = doc.ic_documento "+
                          "   AND cpe.ic_pyme = doc.ic_pyme "+
                          "   AND cpe.ic_epo = doc.ic_epo "+
                          "   AND pn.ic_producto_nafin = 4 "+
                          "   AND e.ic_epo = doc.ic_epo "+
                          "   AND pn.ic_producto_nafin = pe.ic_producto_nafin "+
                          "   AND pe.ic_epo = doc.ic_epo "+
                          "   AND lc.ic_tipo_cobro_interes = tc.ic_tipo_cobro_interes "+                  
                          "   AND ac3.cc_acuse = sol.cc_acuse "+
                          "   AND ed.ic_estatus_docto = doc.ic_estatus_docto "+
                          "   AND lc.ic_linea_credito = doc.ic_linea_credito ");
                          
	
     if (usuario!=null){ 
        qrySentencia.append(" AND doc.ic_pyme = ?  ");
        conditions.add(usuario);
    }
    if (ic_moneda!=null ) { 
        qrySentencia.append("   AND doc.ic_moneda = ? ");
        conditions.add(ic_moneda);
    }
    
    if(ic_epo!=null) {
        qrySentencia.append(" AND doc.ic_epo = ?");
         conditions.add(ic_epo);   
    }
    if(ic_if!=null) {
        qrySentencia.append(" AND lc.ic_if = ?");
         conditions.add(ic_if);   
    }

    //if(fecha_inicio!=null && fecha_final!=null ){  
  if( !fecha_inicio.equals("") && !fecha_final.equals("")){
  
   qrySentencia.append( "   AND ((    doc.ic_estatus_docto = 4 "+
                              "         AND TRUNC (ac3.df_fecha_hora) BETWEEN "+
                              "                TRUNC (TO_DATE ('"+fecha_inicio+"', 'dd/mm/yyyy')) AND "+
                              "                TRUNC (TO_DATE ('"+fecha_final+"', 'dd/mm/yyyy')) "+
                              "           ) "+
                              "        OR (    doc.ic_estatus_docto IN (11,  22) "+
                              "            AND doc.ic_documento IN "+
                              "                   (SELECT ic_documento "+
                              "                      FROM dis_cambio_estatus "+
                              "                     WHERE dc_fecha_cambio IN "+
                              "                              (SELECT MAX (dc_fecha_cambio) "+
                              "                                 FROM dis_cambio_estatus "+
                              "                                WHERE ic_documento = doc.ic_documento "+
                              "                                  AND TRUNC (dc_fecha_cambio) BETWEEN "+
                              "                                         TRUNC ( "+
                              "                                            TO_DATE ('"+fecha_inicio+"', 'dd/mm/yyyy') "+
                              "                                         ) AND "+
                              "                                         TRUNC ( "+
                              "                                            TO_DATE ('"+fecha_final+"', 'dd/mm/yyyy') "+
                              "                                         )) "+
                              "                       AND ic_cambio_estatus IN (18,  19,  20)) "+
                              "           ) "+
                              "       ) ");
    }
  
			log.debug("getDocumentQuery "+conditions.toString());
			log.debug("getDocumentQuery "+qrySentencia.toString());

			return qrySentencia.toString();
	}

	/**
	 * Obtiene el query necesario para mostrar la informaci�n completa de 
	 * una p�gina a partir de las llaves primarias enviadas como par�metro
	 * @return Cadena con la consulta de SQL, para obtener la informaci�n
	 * 	completa de los registros con las llaves especificadas
	 */
	public String getDocumentSummaryQueryForIds(List pageIds){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
     log.debug("-------------------------:: ");		
		log.debug("paginaOffset:: "+paginaOffset);
    log.debug("paginaNo:: "+paginaNo); 
    log.debug("ic_if:: "+ic_if);   
    log.debug("ic_epo:: "+ic_epo);   
    log.debug("ic_moneda:: "+ic_moneda);   
    log.debug("fecha_inicio:: "+fecha_inicio);   
    log.debug("fecha_final:: "+fecha_final); 
    log.debug("usuario:: "+usuario); 
	  log.debug("-------------------------:: "); 
    

  qrySentencia.append(" SELECT doc.ic_documento, "+
                        " TO_CHAR (ac3.df_fecha_hora, 'DD/MM/YYYY') AS fecha_opera, "+
                        " cpe.cg_num_distribuidor AS num_cliente, "+
                        " e.cg_razon_social AS epo, "+
                        " ig_numero_docto AS num_docto, "+
                        " doc.ic_documento AS num_credito, "+
                        " TO_CHAR (sol.df_v_credito, 'DD/MM/YYYY') AS f_venc_credito, "+
                        " doc.fn_monto AS monto_cred, "+
                        " DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
                        " 'N', 'Sin conversion','P', 'Peso-Dolar','') AS tipo_conversion, "+
                        " sel.fn_importe_interes AS int_generados, "+
                        " DECODE (doc.ic_estatus_docto, 11, doc.fn_monto, 0) AS pago_a_capital, "+
                        " '' AS pago_int, "+
                        " DECODE (doc.ic_estatus_docto,4, doc.fn_monto,22, doc.fn_monto,0) AS saldo_cre, "+
                        " tc.cd_descripcion AS tipo_cob_int, "+
                        " ed.cg_desc_alterna AS estatus ,"+
                        " tc.ic_tipo_cobro_interes,"+
                        " doc.ic_estatus_docto ");
                        
                        
     qrySentencia.append("  FROM dis_documento doc, "+
                          "       dis_solicitud sol, "+
                          "       dis_docto_seleccionado sel, "+
                          "       comrel_pyme_epo cpe, "+
                          "       comcat_epo e, "+
                          "       comrel_producto_epo pe, "+
                          "       comcat_producto_nafin pn, "+
                          "       comcat_tipo_cobro_interes tc, "+
                          "       com_linea_credito lc, "+
                          "       com_acuse3 ac3, "+
                          "       comcat_estatus_docto ed ");
                          
     qrySentencia.append(" WHERE sol.ic_documento = doc.ic_documento "+
                          "   AND sel.ic_documento = doc.ic_documento "+
                          "   AND cpe.ic_pyme = doc.ic_pyme "+
                          "   AND cpe.ic_epo = doc.ic_epo "+
                          "   AND pn.ic_producto_nafin = 4 "+
                          "   AND e.ic_epo = doc.ic_epo "+
                          "   AND pn.ic_producto_nafin = pe.ic_producto_nafin "+
                          "   AND pe.ic_epo = doc.ic_epo "+
                          "   AND lc.ic_tipo_cobro_interes = tc.ic_tipo_cobro_interes "+                  
                          "   AND ac3.cc_acuse = sol.cc_acuse "+
                          "   AND ed.ic_estatus_docto = doc.ic_estatus_docto "+
                          "   AND lc.ic_linea_credito = doc.ic_linea_credito ");
                          
	
      if (usuario!=null){ 
        qrySentencia.append(" AND doc.ic_pyme = ?  ");
        conditions.add(usuario);
    }
    if (ic_moneda!=null ) { 
        qrySentencia.append("   AND doc.ic_moneda = ? ");
        conditions.add(ic_moneda);
    }
    if(ic_epo!=null) {
        qrySentencia.append(" AND doc.ic_epo = ?");
         conditions.add(ic_epo);   
    }
    if(ic_if!=null) {
        qrySentencia.append(" AND lc.ic_if = ?");
         conditions.add(ic_if);   
    }
    
   //if(fecha_inicio!=null && fecha_final!=null ){  
  if( !fecha_inicio.equals("") && !fecha_final.equals("")){
    qrySentencia.append( "   AND ((    doc.ic_estatus_docto = 4 "+
                              "         AND TRUNC (ac3.df_fecha_hora) BETWEEN "+
                              "                TRUNC (TO_DATE ('"+fecha_inicio+"', 'dd/mm/yyyy')) AND "+
                              "                TRUNC (TO_DATE ('"+fecha_final+"', 'dd/mm/yyyy')) "+
                              "           ) "+
                              "        OR (    doc.ic_estatus_docto IN (11,  22) "+
                              "            AND doc.ic_documento IN "+
                              "                   (SELECT ic_documento "+
                              "                      FROM dis_cambio_estatus "+
                              "                     WHERE dc_fecha_cambio IN "+
                              "                              (SELECT MAX (dc_fecha_cambio) "+
                              "                                 FROM dis_cambio_estatus "+
                              "                                WHERE ic_documento = doc.ic_documento "+
                              "                                  AND TRUNC (dc_fecha_cambio) BETWEEN "+
                              "                                         TRUNC ( "+
                              "                                            TO_DATE ('"+fecha_inicio+"', 'dd/mm/yyyy') "+
                              "                                         ) AND "+
                              "                                         TRUNC ( "+
                              "                                            TO_DATE ('"+fecha_final+"', 'dd/mm/yyyy') "+
                              "                                         )) "+
                              "                       AND ic_cambio_estatus IN (18,  19,  20)) "+
                              "           ) "+
                              "       ) ");
    }
  
  
  
   
    
    qrySentencia.append("   AND ( ");
      
      for(int i=0;i<pageIds.size();i++) {
			List lItem = (ArrayList)pageIds.get(i);
			if(i>0) {
				qrySentencia.append(" OR ");
			}
			qrySentencia.append(" (doc.ic_documento = ? ) ");
			conditions.add(new Long(lItem.get(0).toString()));
      }//for(int i=0;i<ids.size();i++)
 		qrySentencia.append(" ) ");
    
    
                    
			log.debug("getDocumentSummaryQueryForIds "+conditions.toString());
			log.debug(" getDocumentSummaryQueryForIds "+qrySentencia.toString());
	
		
		return qrySentencia.toString();
	}
	

	public String getDocumentQueryFile(){
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();

			 log.debug("-------------------------:: ");		
		log.debug("paginaOffset:: "+paginaOffset);
    log.debug("paginaNo:: "+paginaNo); 
    log.debug("ic_if:: "+ic_if);   
    log.debug("ic_epo:: "+ic_epo);   
    log.debug("ic_moneda:: "+ic_moneda);   
    log.debug("fecha_inicio:: "+fecha_inicio);   
    log.debug("fecha_final:: "+fecha_inicio); 
    log.debug("usuario:: "+usuario); 
	  log.debug("-------------------------:: "); 
    
      
    qrySentencia.append(" SELECT doc.ic_documento, "+
                        " TO_CHAR (ac3.df_fecha_hora, 'DD/MM/YYYY') AS fecha_opera, "+
                        " cpe.cg_num_distribuidor AS num_cliente, "+
                        " e.cg_razon_social AS epo, "+
                        " ig_numero_docto AS num_docto, "+
                        " doc.ic_documento AS num_credito, "+
                        " TO_CHAR (sol.df_v_credito, 'DD/MM/YYYY') AS f_venc_credito, "+
                        " doc.fn_monto AS monto_cred, "+
                        " DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
                        " 'N', 'Sin conversion','P', 'Peso-Dolar','') AS tipo_conversion, "+
                        " sel.fn_importe_interes AS int_generados, "+
                        " DECODE (doc.ic_estatus_docto, 11, doc.fn_monto, 0) AS pago_a_capital, "+
                        " '' AS pago_int, "+
                        " DECODE (doc.ic_estatus_docto,4, doc.fn_monto,22, doc.fn_monto,0) AS saldo_cre, "+
                        " tc.cd_descripcion AS tipo_cob_int, "+
                        " ed.cg_desc_alterna AS estatus ,"+
                        " tc.ic_tipo_cobro_interes,"+
                        " doc.ic_estatus_docto ");
                        
                        
    qrySentencia.append("  FROM dis_documento doc, "+
                          "       dis_solicitud sol, "+
                          "       dis_docto_seleccionado sel, "+
                          "       comrel_pyme_epo cpe, "+
                          "       comcat_epo e, "+
                          "       comrel_producto_epo pe, "+
                          "       comcat_producto_nafin pn, "+
                          "       comcat_tipo_cobro_interes tc, "+
                          "       com_linea_credito lc, "+
                          "       com_acuse3 ac3, "+
                          "       comcat_estatus_docto ed ");
                          
       qrySentencia.append(" WHERE sol.ic_documento = doc.ic_documento "+
                          "   AND sel.ic_documento = doc.ic_documento "+
                          "   AND cpe.ic_pyme = doc.ic_pyme "+
                          "   AND cpe.ic_epo = doc.ic_epo "+
                          "   AND pn.ic_producto_nafin = 4 "+
                          "   AND e.ic_epo = doc.ic_epo "+
                          "   AND pn.ic_producto_nafin = pe.ic_producto_nafin "+
                          "   AND pe.ic_epo = doc.ic_epo "+
                          "   AND lc.ic_tipo_cobro_interes = tc.ic_tipo_cobro_interes "+                  
                          "   AND ac3.cc_acuse = sol.cc_acuse "+
                          "   AND ed.ic_estatus_docto = doc.ic_estatus_docto "+
                          "   AND lc.ic_linea_credito = doc.ic_linea_credito ");
                          
	  if (usuario!=null){ 
        qrySentencia.append(" AND doc.ic_pyme = ?  ");
        conditions.add(usuario);
    }
    if (ic_moneda!=null ) { 
        qrySentencia.append("   AND doc.ic_moneda = ? ");
        conditions.add(ic_moneda);
    }
  
    if(ic_epo!=null) {
        qrySentencia.append(" AND doc.ic_epo = ?");
         conditions.add(ic_epo);   
    }
    if(ic_if!=null) {
        qrySentencia.append(" AND lc.ic_if = ?");
         conditions.add(ic_if);   
    }
    

    if( !fecha_inicio.equals("") && !fecha_final.equals("")){ 
    
    qrySentencia.append( "   AND ((    doc.ic_estatus_docto = 4 "+
                              "         AND TRUNC (ac3.df_fecha_hora) BETWEEN "+
                              "                TRUNC (TO_DATE ('"+fecha_inicio+"', 'dd/mm/yyyy')) AND "+
                              "                TRUNC (TO_DATE ('"+fecha_final+"', 'dd/mm/yyyy')) "+
                              "           ) "+
                              "        OR (    doc.ic_estatus_docto IN (11,  22) "+
                              "            AND doc.ic_documento IN "+
                              "                   (SELECT ic_documento "+
                              "                      FROM dis_cambio_estatus "+
                              "                     WHERE dc_fecha_cambio IN "+
                              "                              (SELECT MAX (dc_fecha_cambio) "+
                              "                                 FROM dis_cambio_estatus "+
                              "                                WHERE ic_documento = doc.ic_documento "+
                              "                                  AND TRUNC (dc_fecha_cambio) BETWEEN "+
                              "                                         TRUNC ( "+
                              "                                            TO_DATE ('"+fecha_inicio+"', 'dd/mm/yyyy') "+
                              "                                         ) AND "+
                              "                                         TRUNC ( "+
                              "                                            TO_DATE ('"+fecha_final+"', 'dd/mm/yyyy') "+
                              "                                         )) "+
                              "                       AND ic_cambio_estatus IN (18,  19,  20)) "+
                              "           ) "+
                              "       ) ");
    }
  
		log.debug("getDocumentQueryFile "+conditions.toString());
		log.debug("getDocumentQueryFile "+qrySentencia.toString());
		
		return qrySentencia.toString();
	}//getDocumentQueryFile

  	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		log.info("crearCustomFile (E)");
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo			=	"";
		String rs_fecha_opera	= "", rs_num_cliente= "",  rs_nombre_epo = "", rs_cliente = "",
		rs_num_docto	= "", rs_num_credito	= "", rs_f_venc_credito	= "", rs_monto_cred	= "",
		rs_tipo_conversion	= "", rs_int_generados = "", rs_pago_a_capital	= "", rs_pago_int	= "",
		rs_saldo_cre	= "", rs_tipo_cob_int	= "", rs_estatus = "",	rs_total_pagado = "",
		rs_ic_tipo_cob_int 	= "", rs_ic_estatus_docto	= "";
		double total_pagado			=	0;
		int 		totalreg				=	0;
		double	totalMontoCre		=	0;
		double	totalMontoFinal	=	0;
		double	totalInt				=	0;
		double	totalPago			=	0;
		double	totalSaldo			=	0;
		double	totalTotPagado		=	0;

	if ("CSV".equals(tipo)) {
		try {
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
			buffer = new BufferedWriter(writer);
			linea = "Res�men estado de adeudos\nCliente,"+cliente.replace(',',' ')+"\nBanco de Referencia,"+bancoRef.replace(',',' ')+"\n";
			buffer.write(linea);
			int numRegistros = 0;
			while (rs.next()) {
				numRegistros++;
				rs_fecha_opera			=	(rs.getString(2)==null)?"":rs.getString(2);
				rs_num_cliente			=	(rs.getString(3)==null)?"":rs.getString(3);
				rs_nombre_epo			=	(rs.getString(4)==null)?"":rs.getString(4);
				rs_num_docto			=	(rs.getString(5)==null)?"":rs.getString(5);
				rs_num_credito			=	(rs.getString(6)==null)?"":rs.getString(6);
				rs_f_venc_credito		=	(rs.getString(7)==null)?"":rs.getString(7);
				rs_monto_cred			=	(rs.getString(8)==null)?"":rs.getString(8);
				rs_tipo_conversion	=	(rs.getString(9)==null)?"":rs.getString(9);
				rs_int_generados		=	(rs.getString(10)==null)?"":rs.getString(10);
				rs_pago_a_capital		=	(rs.getString(11)==null)?"":rs.getString(11);
				rs_pago_int				=	(rs.getString(12)==null)?"":rs.getString(12);
				rs_saldo_cre			=	(rs.getString(13)==null)?"":rs.getString(13);
				rs_tipo_cob_int		=	(rs.getString(14)==null)?"":rs.getString(14);
				rs_estatus				=	(rs.getString(15)==null)?"":rs.getString(15);
				rs_ic_tipo_cob_int	=	(rs.getString(16)==null)?"":rs.getString(16);
				rs_ic_estatus_docto	=	(rs.getString(17)==null)?"":rs.getString(17);	
				rs_pago_int = (rs_pago_int.equals(""))?"0":rs_pago_int;
				if("1".equals(rs_ic_tipo_cob_int)){  //Pago Anticipado
					rs_pago_int = rs_int_generados;
				}else {
					if("2".equals(rs_ic_tipo_cob_int)){  //Pago al Vencimiento
						if("11".equals(rs_ic_estatus_docto)){
							rs_pago_int = rs_int_generados;
						}else{
							rs_pago_int = "0";
						}
					}
				}
				total_pagado = Double.parseDouble(rs_pago_a_capital) + Double.parseDouble(rs_pago_int);
				totalreg++;
				totalTotPagado += total_pagado;
				if (!"".equals(rs_monto_cred)){
					totalMontoCre += Double.parseDouble(rs_monto_cred);
				}
				if (!"".equals(rs_pago_int)){
					totalInt += Double.parseDouble(rs_pago_int);
				}
				if (!"".equals(rs_pago_a_capital)){
					totalPago += Double.parseDouble(rs_pago_a_capital);
				}
				if (!"".equals(rs_saldo_cre)){
					totalSaldo += Double.parseDouble(rs_saldo_cre);
				}
				if (numRegistros == 1){
					linea = "\nMovimientos del "+fecha_inicio+" al "+fecha_final+" "+
								"\nFecha de operaci�n,EPO,Num. de Docto.,Num. de Financiamiento,Fecha de vencimiento del financiamiento, Monto del Financiamiento, Estatus del Financiamiento,Intereses generados, Capital pagado ,Intereses Pagados ,Total Pagado, Saldo del cr�dito,Tipo de cobro de intereses";
					buffer.write(linea);
				}
				linea = "\n"+rs_fecha_opera+","+rs_nombre_epo.replace(',',' ')+","+rs_num_docto+","+rs_num_credito.replace(',',' ')+","+rs_f_venc_credito+","+rs_monto_cred+","+rs_estatus.replace(',',' ')+","+rs_int_generados+","+rs_pago_a_capital+","+rs_pago_int+","+total_pagado+","+rs_saldo_cre+","+rs_tipo_cob_int;
				buffer.write(linea);
			}
			/*if(totalreg>0){
				linea	=	"\nTotal,"+totalreg+",,,,"+totalMontoCre+",,,"+totalPago+","+totalInt+","+totalTotPagado+","+totalSaldo;
				buffer.write(linea);
			}*/
			buffer.close();
		}catch (Throwable e)	{
			throw new AppException("Error al generar el archivo", e);
		}finally{
			try {
				rs.close();
			}catch(Exception e) {}
		}
        
	}else if ("PDF".equals(tipo)) {
		try {
			HttpSession session = request.getSession();
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			int nRow = 0;
			ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
			while (rs.next()) {
				rs_fecha_opera			=	(rs.getString(2)==null)?"":rs.getString(2);
				rs_num_cliente			=	(rs.getString(3)==null)?"":rs.getString(3);
				rs_nombre_epo			=	(rs.getString(4)==null)?"":rs.getString(4);
				rs_num_docto			=	(rs.getString(5)==null)?"":rs.getString(5);
				rs_num_credito			=	(rs.getString(6)==null)?"":rs.getString(6);
				rs_f_venc_credito		=	(rs.getString(7)==null)?"":rs.getString(7);
				rs_monto_cred			=	(rs.getString(8)==null)?"":rs.getString(8);
				rs_tipo_conversion	=	(rs.getString(9)==null)?"":rs.getString(9);
				rs_int_generados		=	(rs.getString(10)==null)?"":rs.getString(10);
				rs_pago_a_capital		=	(rs.getString(11)==null)?"":rs.getString(11);
				rs_pago_int				=	(rs.getString(12)==null)?"":rs.getString(12);
				rs_saldo_cre			=	(rs.getString(13)==null)?"":rs.getString(13);
				rs_tipo_cob_int		=	(rs.getString(14)==null)?"":rs.getString(14);
				rs_estatus				=	(rs.getString(15)==null)?"":rs.getString(15);
				rs_ic_tipo_cob_int	=	(rs.getString(16)==null)?"":rs.getString(16);
				rs_ic_estatus_docto	=	(rs.getString(17)==null)?"":rs.getString(17);	
				rs_pago_int = (rs_pago_int.equals(""))?"0":rs_pago_int;
				if("1".equals(rs_ic_tipo_cob_int)){  //Pago Anticipado
					rs_pago_int = rs_int_generados;
				}else {
					if("2".equals(rs_ic_tipo_cob_int)){  //Pago al Vencimiento
						if("11".equals(rs_ic_estatus_docto)){
							rs_pago_int = rs_int_generados;
						}else{
							rs_pago_int = "0";
						}
					}
				}
				total_pagado = Double.parseDouble(rs_pago_a_capital) + Double.parseDouble(rs_pago_int);
				totalreg++;
				totalTotPagado += total_pagado;
				if (!"".equals(rs_monto_cred)){
					totalMontoCre += Double.parseDouble(rs_monto_cred);
				}
				if (!"".equals(rs_pago_int)){
					totalInt += Double.parseDouble(rs_pago_int);
				}
				if (!"".equals(rs_pago_a_capital)){
					totalPago += Double.parseDouble(rs_pago_a_capital);
				}
				if (!"".equals(rs_saldo_cre)){
					totalSaldo += Double.parseDouble(rs_saldo_cre);
				}
				if(nRow == 0){
					String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
					String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
					String diaActual    = fechaActual.substring(0,2);
					String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
					String anioActual   = fechaActual.substring(6,10);
					String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
					pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					session.getAttribute("iNoNafinElectronico").toString(),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
					
					pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
					pdfDoc.addText(" ","formas", ComunesPDF.CENTER);
					pdfDoc.addText("Cliente:					"+this.cliente, "formas", ComunesPDF.LEFT);
					pdfDoc.addText("Banco de Referencia:	"+this.bancoRef, "formas", ComunesPDF.LEFT);
					pdfDoc.setLTable(13, 100);
					pdfDoc.setLCell("Fecha de operaci�n", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("EPO", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("No. Docto.", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("No. Financiamiento", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Fecha vencimiento del financiamiento", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto del financiamiento", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Estatus del financiamiento", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Intereses generados", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Capital pagado", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Intereses Pagados", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Total Pagado", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Saldo del Cr�dito", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLCell("Tipo Cobro Intereses", "formasmenB", ComunesPDF.CENTER);
					pdfDoc.setLHeaders();
				}
				pdfDoc.setLCell(rs_fecha_opera, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setLCell(rs_nombre_epo, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setLCell(rs_num_docto, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setLCell(rs_num_credito, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setLCell(rs_f_venc_credito, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(rs_monto_cred,2),"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setLCell(rs_estatus, "formasmen", ComunesPDF.CENTER);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(rs_int_generados,2),"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(rs_pago_a_capital,2),"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(rs_int_generados,2),"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(total_pagado,2),"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setLCell("$ "+Comunes.formatoDecimal(rs_saldo_cre,2),"formasmen",ComunesPDF.RIGHT);
				pdfDoc.setLCell(rs_tipo_cob_int, "formasmen", ComunesPDF.CENTER);
				nRow++;
			}
			/*if(totalreg>0){
				pdfDoc.setCell("TOTAL "+tipo_moneda,"celda01rep",ComunesPDF.LEFT,4);
			}*/
			pdfDoc.addLTable();
			pdfDoc.endDocument();
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo", e);
		} finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
	}
	log.info("crearCustomFile (S)");
	return nombreArchivo;
	}
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}
	
/*****************************************************
	 GETTERS
*******************************************************/
 
	/**
	  Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
 	
	
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	
	public String getDirectorio() {
		return directorio;
	}

	public void setDirectorio(String directorio) {
		this.directorio = directorio;
	}

  public String getPaginar() {
    return paginar;
  }

  public void setPaginar(String paginar) {
    this.paginar = paginar;
  }

  public String getIc_if() {
    return ic_if;
  }

  public void setIc_if(String ic_if) {
    this.ic_if = ic_if;
  }

  public String getIc_epo() {
    return ic_epo;
  }

  public void setIc_epo(String ic_epo) {
    this.ic_epo = ic_epo;
  }

  public String getIc_moneda() {
    return ic_moneda;
  }

  public void setIc_moneda(String ic_moneda) {
    this.ic_moneda = ic_moneda;
  }

  public String getFecha_inicio() {
    return fecha_inicio;
  }

  public void setFecha_inicio(String fecha_inicio) {
    this.fecha_inicio = fecha_inicio;
  }

  public String getFecha_final() {
    return fecha_final;
  }

  public void setFecha_final(String fecha_final) {
    this.fecha_final = fecha_final;
  }

  public String getUsuario() {
    return usuario;
  }

  public void setUsuario(String usuario) {
    this.usuario = usuario;
  }
	public void setCliente(String clie){
		this.cliente = clie;
	}
	public void setBanco(String ref){
		this.bancoRef = ref;
	}
	
}
