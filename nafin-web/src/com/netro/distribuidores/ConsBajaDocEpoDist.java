package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;
import com.netro.exception.*;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.VectorTokenizer;
import org.apache.commons.logging.Log;
import netropology.utilerias.ServiceLocator;

public class ConsBajaDocEpoDist implements IQueryGenerator, IQueryGeneratorRegExtJS{
	public ConsBajaDocEpoDist() {  }
  
	public String getAggregateCalculationQuery(HttpServletRequest request) {
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		StringBuffer qrySentencia	= new StringBuffer();
		StringBuffer condicion		= new StringBuffer();
		
		String	ic_epo				=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String	ic_pyme				=	(request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String	ic_moneda			=	(request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String	ic_cambio_estatus	=	(request.getParameter("ic_cambio_estatus")==null)?"":request.getParameter("ic_cambio_estatus");
		String 	fn_monto_de			=	(request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String 	fn_monto_a			=	(request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String 	ig_numero_docto		=	(request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String 	monto_con_descuento	=	(request.getParameter("monto_con_descuento")==null)?"":"checked";
		String 	cc_acuse			=	(request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String	modo_plazo			=	(request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
		String	fecha_emi_de		=	(request.getParameter("fecha_emi_de")==null)?"":request.getParameter("fecha_emi_de");
		String	fecha_emi_a			=	(request.getParameter("fecha_emi_a")==null)?"":request.getParameter("fecha_emi_a");
		String 	fecha_vto_de		=	(request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String 	fecha_vto_a			=	(request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String 	fecha_publicacion_de=	(request.getParameter("fecha_publicacion_de")==null)?"":request.getParameter("fecha_publicacion_de");
		String 	fecha_publicacion_a	=	(request.getParameter("fecha_publicacion_a")==null)?"":request.getParameter("fecha_publicacion_a");
		String 	fecha_cambio_de		=	(request.getParameter("fecha_cambio_de")==null)?fechaHoy:request.getParameter("fecha_cambio_de");
		String 	fecha_cambio_a		=	(request.getParameter("fecha_cambio_a")==null)?fechaHoy:request.getParameter("fecha_cambio_a");
    String 	NOnegociable		=	(request.getParameter("NOnegociable")==null)?"":request.getParameter("NOnegociable");//Fodea 0259-2010  Distribuidores Fase III
	

		try {
			
			if(!"".equals(ic_pyme))
				condicion.append(" AND d.ic_pyme = "+ic_pyme);
			if(!"".equals(ic_moneda))
				condicion.append(" AND d.ic_moneda = "+ic_moneda);
			if(!"".equals(ic_cambio_estatus))
				condicion.append(" AND ce.ic_cambio_estatus = "+ic_cambio_estatus);
			if(!"".equals(fn_monto_de)&&!"".equals(fn_monto_a)&&"".equals(monto_con_descuento))
				condicion.append(" and d.fn_monto between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(fn_monto_de)&&!"".equals(fn_monto_a)&&!"".equals(monto_con_descuento))
				condicion.append(" and (d.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(ig_numero_docto))
				condicion.append(" AND d.ig_numero_docto = '"+ig_numero_docto+"'");
			if(!"".equals(cc_acuse))
				condicion.append(" AND d.cc_acuse = '"+cc_acuse+"'");
			if(!"".equals(modo_plazo))
				condicion.append(" AND d.ic_tipo_financiamiento = "+modo_plazo);
			if(!"".equals(fecha_emi_de)&&!"".equals(fecha_emi_a))
				condicion.append(" and trunc(d.df_fecha_emision) between trunc(to_date('"+fecha_emi_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emi_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_de)&&!"".equals(fecha_vto_a))
				condicion.append(" and trunc(d.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_publicacion_de)&&!"".equals(fecha_publicacion_a))
				condicion.append(" and trunc(d.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_cambio_de)&&!"".equals(fecha_cambio_a))
				condicion.append(" and trunc(ce.dc_fecha_cambio) between trunc(to_date('"+fecha_cambio_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_cambio_a+"','dd/mm/yyyy'))");
     
     	if("".equals(ic_cambio_estatus) &&NOnegociable.equals("N")) { //Fodea 029-2010
        condicion.append("	AND ce.ic_cambio_estatus IN (23,34,4,  22,  2,  14) ");
      }else if("".equals(ic_cambio_estatus) &&NOnegociable.equals("S")) {
         condicion.append(" AND ce.ic_cambio_estatus IN (23,34,4,  22,  2,  14, 24) ");
      }
      
			qrySentencia.append(
					" SELECT /*+ index (d CP_DIS_DOCUMENTO_PK) index (ce IN_DIS_CAMBIO_ESTATUS_01_NUK) use_nl(ce tc d) */ " +
					"  m.ic_moneda, m.cd_nombre, count(*), " +
					" 	sum(d.fn_monto), 'ConsBajaDocEpoDist::getAggregateCalculationQuery' " +
					" FROM dis_cambio_estatus ce, dis_documento d, " +
				//	" 	com_tipo_cambio tc, "+
					" comrel_producto_epo pe, " +
					" 	comcat_moneda m " +
					" WHERE ce.ic_documento = d.ic_documento " +
					" 	AND pe.ic_epo = d.ic_epo  " +
					" 	AND d.ic_moneda = m.ic_moneda " +				
					" 	AND d.ic_epo = " + ic_epo + " "+
					" 	AND pe.ic_producto_nafin = 4  " +
				/*	" 	AND tc.dc_fecha IN ( " +
					" 			SELECT MAX (dc_fecha)  " +
					" 			FROM com_tipo_cambio  " +
					" 			WHERE ic_moneda = d.ic_moneda)  " +*/
					" " + condicion +
					" GROUP BY m.ic_moneda, m.cd_nombre " + 
					" ORDER BY m.ic_moneda ");

		}catch(Exception e){
			System.out.println("ConsBajaDocEpoDist::getAggregateCalculationQuery "+e);
		}
		System.out.println(qrySentencia.toString());
		return qrySentencia.toString();
	}

	/**
	 *
	 */
	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids) {
		int i=0;
		
		 StringBuffer qrySentencia	= new StringBuffer();
    	StringBuffer condicion		= new StringBuffer();
    	
		condicion.append(" AND (");
		i=0;
		for (Iterator it = ids.iterator(); it.hasNext();i++) {
        	if(i>0){
				condicion.append(" OR ");
			} 
			
			VectorTokenizer vt = new VectorTokenizer((String) it.next(), "_");
			Vector  claves = vt.getValuesVector(); //Vector contiene la clave de documento y la fecha de cambio
			String claveDocumento = (String) claves.get(0);
			String fechaDeCambio = (String) claves.get(1);
			
			condicion.append("ce.ic_documento = " + claveDocumento +
					" AND TO_CHAR(ce.dc_fecha_cambio, 'DD-MM-YYYY HH24:MI:SS') = " +
					" '" +  fechaDeCambio + "' ");
		}
		condicion.append(") ");
    
    String	ic_cambio_estatus	=	(request.getParameter("ic_cambio_estatus")==null)?"":request.getParameter("ic_cambio_estatus");
	  String 	NOnegociable		=	(request.getParameter("NOnegociable")==null)?"":request.getParameter("NOnegociable");//Fodea 0259-2010  Distribuidores Fase III
	
   	if(!"".equals(ic_cambio_estatus))
				condicion.append(" AND ce.ic_cambio_estatus = "+ic_cambio_estatus);
    if("".equals(ic_cambio_estatus) &&NOnegociable.equals("N")) {
        condicion.append("	AND ce.ic_cambio_estatus IN (23,34,4,  22,  2,  14) ");
    }else if("".equals(ic_cambio_estatus) &&NOnegociable.equals("S")) {
         condicion.append(" AND ce.ic_cambio_estatus IN (23,34,4,  22,  2,  14, 24) ");
    }
      
		qrySentencia.append(
				" SELECT /*+index(d CP_DIS_DOCUMENTO_PK) use_nl(e py tc pe tf clas ed cce m ed pn)*/ " +
				" 	py.cg_razon_social AS nombre_dist, d.ig_numero_docto,d.cc_acuse, " +
				" 	TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision, " +
				" 	TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_fecha_publicacion, " +
				" 	TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc, " +
				" 	d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto, " +
				" 	DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), " +
				" 	'N', '','P', 'Dolar-Peso','') AS tipo_conversion, " +
				" 	DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), " +
				//" 	'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio, " +
       	" 	'P','1') AS tipo_cambio, " +
				" 	NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento, " +
				" 	decode(d.ic_tipo_financiamiento,1,nvl(d.fn_porc_descuento,0), 0) AS fn_porc_descuento, " +
				" 	tf.cd_descripcion AS modo_plazo,cce.cd_descripcion AS tipo_cambio_estatus, " +
				" 	TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fecha_cambio, m.ic_moneda,clas.cg_descripcion as CATEGORIA " +
				"  ,d.CG_VENTACARTERA as CG_VENTACARTERA   "+
				"  ,ce.CT_CAMBIO_MOTIVO  as OBSERVACIONES  "+ 
				" FROM dis_cambio_estatus ce, " +
				" 	dis_documento d, " +
				" 	comcat_epo e, " +
				" 	comcat_pyme py, " +
				" 	comrel_producto_epo pe, " +
				" 	comcat_producto_nafin pn, " +
				//" 	com_tipo_cambio tc, " +
				" 	comcat_tipo_financiamiento tf, " +
				" 	comcat_estatus_docto ed, " +
				" 	comcat_moneda m, " +
				" 	comcat_cambio_estatus cce,comrel_clasificacion clas "+
				" WHERE " +
				" 	ce.ic_documento = d.ic_documento " +
				" 	AND ce.ic_cambio_estatus = cce.ic_cambio_estatus " +
				" 	AND d.ic_epo = e.ic_epo " +
				" 	AND d.ic_pyme = py.ic_pyme " +
				" 	AND d.ic_moneda = m.ic_moneda " +
				" 	AND d.ic_moneda = m.ic_moneda " +
				" 	AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento " +
				" 	AND d.ic_epo = pe.ic_epo " +
				" 	AND d.ic_epo = e.ic_epo " +
				" 	AND d.ic_estatus_docto = ed.ic_estatus_docto " +
				" 	AND pn.ic_producto_nafin = pe.ic_producto_nafin " +
				/*" 	AND tc.dc_fecha IN ( " +
				" 			SELECT MAX (dc_fecha) " +
				" 			FROM com_tipo_cambio " +
				" 			WHERE ic_moneda = d.ic_moneda) " +*/
				" 	AND pn.ic_producto_nafin = 4 " +
				" 	AND D.ic_clasificacion = clas.ic_clasificacion (+) "+			
        " " + condicion );
        

		System.out.println(qrySentencia.toString());

		return qrySentencia.toString();
	}
	
	public String getDocumentQuery(HttpServletRequest request) {

		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
    	StringBuffer qrySentencia	= new StringBuffer();
    	StringBuffer condicion		= new StringBuffer();
      
		String	ic_epo				=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String	ic_pyme				=	(request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String	ic_moneda			=	(request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String	ic_cambio_estatus	=	(request.getParameter("ic_cambio_estatus")==null)?"":request.getParameter("ic_cambio_estatus");
		String 	fn_monto_de			=	(request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String 	fn_monto_a			=	(request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String 	ig_numero_docto		=	(request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String 	monto_con_descuento	=	(request.getParameter("monto_con_descuento")==null)?"":"checked";
		String 	cc_acuse			=	(request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String	modo_plazo			=	(request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
		String	fecha_emi_de		=	(request.getParameter("fecha_emi_de")==null)?"":request.getParameter("fecha_emi_de");
		String	fecha_emi_a			=	(request.getParameter("fecha_emi_a")==null)?"":request.getParameter("fecha_emi_a");
		String 	fecha_vto_de		=	(request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String 	fecha_vto_a			=	(request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String 	fecha_publicacion_de=	(request.getParameter("fecha_publicacion_de")==null)?"":request.getParameter("fecha_publicacion_de");
		String 	fecha_publicacion_a	=	(request.getParameter("fecha_publicacion_a")==null)?"":request.getParameter("fecha_publicacion_a");
		String 	fecha_cambio_de		=	(request.getParameter("fecha_cambio_de")==null)?fechaHoy:request.getParameter("fecha_cambio_de");
		String 	fecha_cambio_a		=	(request.getParameter("fecha_cambio_a")==null)?fechaHoy:request.getParameter("fecha_cambio_a");
		String 	NOnegociable		=	(request.getParameter("NOnegociable")==null)?"":request.getParameter("NOnegociable");//Fodea 0259-2010  Distribuidores Fase III
	
         
		try {
			
			if(!"".equals(ic_pyme))
				condicion.append(" AND d.ic_pyme = "+ic_pyme);
			if(!"".equals(ic_moneda))
				condicion.append(" AND d.ic_moneda = "+ic_moneda);
			if(!"".equals(ic_cambio_estatus))
				condicion.append(" AND ce.ic_cambio_estatus = "+ic_cambio_estatus);
			if(!"".equals(fn_monto_de)&&!"".equals(fn_monto_a)&&"".equals(monto_con_descuento))
				condicion.append(" and d.fn_monto between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(fn_monto_de)&&!"".equals(fn_monto_a)&&!"".equals(monto_con_descuento))
				condicion.append(" and (d.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(ig_numero_docto))
				condicion.append(" AND d.ig_numero_docto = '"+ig_numero_docto+"'");
			if(!"".equals(cc_acuse))
				condicion.append(" AND d.cc_acuse = '"+cc_acuse+"'");
			if(!"".equals(modo_plazo))
				condicion.append(" AND d.ic_tipo_financiamiento = "+modo_plazo);
			if(!"".equals(fecha_emi_de)&&!"".equals(fecha_emi_a))
				condicion.append(" and trunc(d.df_fecha_emision) between trunc(to_date('"+fecha_emi_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emi_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_de)&&!"".equals(fecha_vto_a))
				condicion.append(" and trunc(d.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_publicacion_de)&&!"".equals(fecha_publicacion_a))
				condicion.append(" and trunc(d.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_cambio_de)&&!"".equals(fecha_cambio_a))
				condicion.append(" and trunc(ce.dc_fecha_cambio) between trunc(to_date('"+fecha_cambio_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_cambio_a+"','dd/mm/yyyy'))");
       
      if("".equals(ic_cambio_estatus) &&NOnegociable.equals("N")) {
        condicion.append("	AND ce.ic_cambio_estatus IN (23,34,4,  22,  2,  14) ");
      }else if("".equals(ic_cambio_estatus) &&NOnegociable.equals("S")) {
         condicion.append(" AND ce.ic_cambio_estatus IN (23,34,4,  22,  2,  14, 24) ");
      }
      
			qrySentencia.append(
					" SELECT /*+ index (d CP_DIS_DOCUMENTO_PK) index (ce IN_DIS_CAMBIO_ESTATUS_01_NUK) use_nl(ce tc d) */ " +
					"  d.ic_documento||'_'||TO_CHAR(ce.dc_fecha_cambio, 'DD-MM-YYYY HH24:MI:SS'), 'ConsBajaDocEpoDist::getDocumentQuery' " +
					" FROM dis_cambio_estatus ce, dis_documento d, "+
					//" com_tipo_cambio tc, "+
					" comrel_producto_epo pe " +
					" WHERE ce.ic_documento = d.ic_documento " +
					" 	AND pe.ic_epo = d.ic_epo  " +				
					" 	AND d.ic_epo = " + ic_epo + " "+
					" 	AND pe.ic_producto_nafin = 4  " +
					/*" 	AND tc.dc_fecha IN ( " +
					" 			SELECT MAX (dc_fecha)  " +
					" 			FROM com_tipo_cambio  " +
					" 			WHERE ic_moneda = d.ic_moneda)  " +*/
					" " + condicion +
					" ORDER BY d.ic_documento, ce.dc_fecha_cambio " );
		} catch(Exception e) {
			System.out.println("ConsBajaDocEpoDist::getDocumentQueryException "+e);
		}
		
		System.out.println(qrySentencia.toString());

		return qrySentencia.toString();
	}
	
	public String getDocumentQueryFile(HttpServletRequest request) {

		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
    	StringBuffer qrySentencia	= new StringBuffer();
    	StringBuffer condicion		= new StringBuffer();
      
		String	ic_epo				=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String	ic_pyme				=	(request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String	ic_moneda			=	(request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String	ic_cambio_estatus	=	(request.getParameter("ic_cambio_estatus")==null)?"":request.getParameter("ic_cambio_estatus");
		String 	fn_monto_de			=	(request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String 	fn_monto_a			=	(request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String 	ig_numero_docto		=	(request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String 	monto_con_descuento	=	(request.getParameter("monto_con_descuento")==null)?"":"checked";
		String 	cc_acuse			=	(request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String	modo_plazo			=	(request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
		String	fecha_emi_de		=	(request.getParameter("fecha_emi_de")==null)?"":request.getParameter("fecha_emi_de");
		String	fecha_emi_a			=	(request.getParameter("fecha_emi_a")==null)?"":request.getParameter("fecha_emi_a");
		String 	fecha_vto_de		=	(request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String 	fecha_vto_a			=	(request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String 	fecha_publicacion_de=	(request.getParameter("fecha_publicacion_de")==null)?"":request.getParameter("fecha_publicacion_de");
		String 	fecha_publicacion_a	=	(request.getParameter("fecha_publicacion_a")==null)?"":request.getParameter("fecha_publicacion_a");
		String 	fecha_cambio_de		=	(request.getParameter("fecha_cambio_de")==null)?fechaHoy:request.getParameter("fecha_cambio_de");
		String 	fecha_cambio_a		=	(request.getParameter("fecha_cambio_a")==null)?fechaHoy:request.getParameter("fecha_cambio_a");
   	String 	NOnegociable		=	(request.getParameter("NOnegociable")==null)?"":request.getParameter("NOnegociable");//Fodea 0259-2010  Distribuidores Fase III
	
    try {
			
			if(!"".equals(ic_pyme))
				condicion.append(" AND d.ic_pyme = "+ic_pyme);
			if(!"".equals(ic_moneda))
				condicion.append(" AND d.ic_moneda = "+ic_moneda);
			if(!"".equals(ic_cambio_estatus))
				condicion.append(" AND ce.ic_cambio_estatus = "+ic_cambio_estatus);
			if(!"".equals(fn_monto_de)&&!"".equals(fn_monto_a)&&"".equals(monto_con_descuento))
				condicion.append(" and d.fn_monto between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(fn_monto_de)&&!"".equals(fn_monto_a)&&!"".equals(monto_con_descuento))
				condicion.append(" and (d.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(ig_numero_docto))
				condicion.append(" AND d.ig_numero_docto = '"+ig_numero_docto+"'");
			if(!"".equals(cc_acuse))
				condicion.append(" AND d.cc_acuse = '"+cc_acuse+"'");
			if(!"".equals(modo_plazo))
				condicion.append(" AND d.ic_tipo_financiamiento = "+modo_plazo);
			if(!"".equals(fecha_emi_de)&&!"".equals(fecha_emi_a))
				condicion.append(" and trunc(d.df_fecha_emision) between trunc(to_date('"+fecha_emi_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emi_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_de)&&!"".equals(fecha_vto_a))
				condicion.append(" and trunc(d.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_publicacion_de)&&!"".equals(fecha_publicacion_a))
				condicion.append(" and trunc(d.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_cambio_de)&&!"".equals(fecha_cambio_a))
				condicion.append(" and trunc(ce.dc_fecha_cambio) between trunc(to_date('"+fecha_cambio_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_cambio_a+"','dd/mm/yyyy'))");
      
      if("".equals(ic_cambio_estatus) &&NOnegociable.equals("N")) {
        condicion.append("	AND ce.ic_cambio_estatus IN (23,34,4,  22,  2,  14) ");
      }else if("".equals(ic_cambio_estatus) &&NOnegociable.equals("S")) {
         condicion.append(" AND ce.ic_cambio_estatus IN (23,34,4,  22,  2,  14, 24) ");
      }
      
			qrySentencia.append(
				" SELECT /*+index(d CP_DIS_DOCUMENTO_PK) use_nl(e py tc pe tf clas ed cce m ed pn)*/ " +
				" 	py.cg_razon_social AS nombre_dist, d.ig_numero_docto,d.cc_acuse, " +
				" 	TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision, " +
				" 	TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_fecha_publicacion, " +
				" 	TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc, " +
				" 	d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto, " +
				" 	DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), " +
				" 	'N', '','P', 'Dolar-Peso','') AS tipo_conversion, " +
				" 	DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), " +
				//" 	'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio, " +
				" 	'P','1') AS tipo_cambio, " +
				" 	NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento, " +
				" 	decode(d.ic_tipo_financiamiento,1,nvl(d.fn_porc_descuento,0), 0) AS fn_porc_descuento, " +
				" 	tf.cd_descripcion AS modo_plazo,cce.cd_descripcion AS tipo_cambio_estatus, " +
				" 	TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fecha_cambio, m.ic_moneda,clas.cg_descripcion as CATEGORIA " +
				"   ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				"  ,ce.CT_CAMBIO_MOTIVO  as OBSERVACIONES  "+ 
				" FROM dis_cambio_estatus ce, " +
				" 	dis_documento d, " +
				" 	comcat_epo e, " +
				" 	comcat_pyme py, " +
				" 	comrel_producto_epo pe, " +
				" 	comcat_producto_nafin pn, " +
			//	" 	com_tipo_cambio tc, " +
				" 	comcat_tipo_financiamiento tf, " +
				" 	comcat_estatus_docto ed, " +
				" 	comcat_moneda m, " +
				" 	comcat_cambio_estatus cce,comrel_clasificacion clas "+
				" WHERE " +
				" 	ce.ic_documento = d.ic_documento " +
				" 	AND ce.ic_cambio_estatus = cce.ic_cambio_estatus " +
				" 	AND d.ic_epo = e.ic_epo " +
				" 	AND d.ic_pyme = py.ic_pyme " +
				" 	AND d.ic_moneda = m.ic_moneda " +
				" 	AND d.ic_moneda = m.ic_moneda " +
				" 	AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento " +
				" 	AND d.ic_epo = pe.ic_epo " +
				" 	AND d.ic_epo = e.ic_epo " +
				" 	AND d.ic_estatus_docto = ed.ic_estatus_docto " +
				" 	AND pn.ic_producto_nafin = pe.ic_producto_nafin " +
				" 	AND D.ic_clasificacion = clas.ic_clasificacion (+) "+
			/*	" 	AND tc.dc_fecha IN ( " +
				" 			SELECT MAX (dc_fecha) " +
				" 			FROM com_tipo_cambio " +
				" 			WHERE ic_moneda = d.ic_moneda) " +*/
			  " " + condicion +
				" 	AND pe.ic_producto_nafin = 4 " +
				" 	AND pe.ic_epo = " + ic_epo +
				" ORDER BY d.ic_documento ");
			
					System.out.println("-------------------------------------");
					System.out.println(qrySentencia.toString());
				System.out.println("-------------------------------------");
		}catch(Exception e){
			System.out.println("ConsBajaDocEpoDist::getDocumentQueryFileException "+e);
		}
		return qrySentencia.toString();
	}

/*******************************************************************************
 *          Migraci�n. Implementa IQueryGeneratorRegExtJS.java                 *
 *******************************************************************************/
	private String ic_epo;
	private String ic_pyme;
	private String ic_moneda;
	private String ic_cambio_estatus;
	private String fn_monto_de;
	private String fn_monto_a;
	private String ig_numero_docto;
	private String monto_con_descuento;
	private String cc_acuse;
	private String modo_plazo;
	private String fecha_emi_de;
	private String fecha_emi_a;
	private String fecha_vto_de;
	private String fecha_vto_a;
	private String fecha_publicacion_de;
	private String fecha_publicacion_a;
	private String fecha_cambio_de;
	private String fecha_cambio_a;
	private String NOnegociable;
	private List   conditions;
	private static final Log log = ServiceLocator.getInstance().getLog(ConsBajaDocEpoDist.class);

	public String getDocumentQuery(){
		return null;
	}

	public String getDocumentSummaryQueryForIds(List ids){
		return null;
	}

	public String getAggregateCalculationQuery(){
		return null;
	}

/**
 * Se arma el query para generar los archivos PDF y CSV
 * @return query
 */
	public String getDocumentQueryFile(){

		log.info("getDocumentQueryFile (E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();

		if(!"".equals(getIc_pyme())){
			condicion.append(" AND d.ic_pyme = ? ");
			conditions.add(getIc_pyme());
		}
		if(!"".equals(getIc_moneda())){
			condicion.append(" AND d.ic_moneda = ? ");
			conditions.add(getIc_moneda());
		}
		if(!"".equals(getIc_cambio_estatus())){
			condicion.append(" AND ce.ic_cambio_estatus = ? ");
			conditions.add(getIc_cambio_estatus());
		}
		if(!"".equals(getFn_monto_de()) && !"".equals(getFn_monto_a()) && "".equals(getMonto_con_descuento())){
			condicion.append(" AND d.fn_monto between ? and ? ");
			conditions.add(getFn_monto_de());
			conditions.add(getFn_monto_a());
		}
		if(!"".equals(getFn_monto_de()) && !"".equals(getFn_monto_a()) && !"".equals(getMonto_con_descuento())){
			condicion.append(" AND (d.fn_monto*(fn_porc_descuento/100)) between ? and ? ");
			conditions.add(getFn_monto_de());
			conditions.add(getFn_monto_a());
		}
		if(!"".equals(getIg_numero_docto())){
			condicion.append(" AND d.ig_numero_docto = ? ");
			conditions.add(getIg_numero_docto());
		}
		if(!"".equals(getCc_acuse())){
			condicion.append(" AND d.cc_acuse = ? ");
			conditions.add(getCc_acuse());
		}
		if(!"".equals(getModo_plazo())){
			condicion.append(" AND d.ic_tipo_financiamiento = ? ");
			conditions.add(getModo_plazo());
		}
		if(!"".equals(getFecha_emi_de())&&!"".equals(getFecha_emi_a())){
			condicion.append(" and trunc(d.df_fecha_emision) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");
			conditions.add(getFecha_emi_de());
			conditions.add(getFecha_emi_a());
		}
		if(!"".equals(getFecha_vto_de())&&!"".equals(getFecha_vto_a())){
			condicion.append(" and trunc(d.df_fecha_venc) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");
			conditions.add(getFecha_vto_de());
			conditions.add(getFecha_vto_a());
		}
		if(!"".equals(getFecha_publicacion_de())&&!"".equals(getFecha_publicacion_a())){
			condicion.append(" and trunc(d.df_carga) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");
			conditions.add(getFecha_publicacion_de());
			conditions.add(getFecha_publicacion_a());
		}
		if(!"".equals(getFecha_cambio_de())&&!"".equals(getFecha_cambio_a())){
			condicion.append(" and trunc(ce.dc_fecha_cambio) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");
			conditions.add(getFecha_cambio_de());
			conditions.add(getFecha_cambio_a());
		}
		if("".equals(getIc_cambio_estatus()) && getNOnegociable().equals("N")){
			condicion.append(" AND ce.ic_cambio_estatus IN (23, 34, 4, 22, 2, 14) ");
		}else if("".equals(getIc_cambio_estatus()) && getNOnegociable().equals("S")){
			condicion.append(" AND ce.ic_cambio_estatus IN (23, 34, 4, 22, 2, 14, 24) ");
		}
		conditions.add(getIc_epo());

		qrySentencia.append(" SELECT /*+index(d CP_DIS_DOCUMENTO_PK) use_nl(e py tc pe tf clas ed cce m ed pn)*/ ");
		qrySentencia.append(" py.cg_razon_social AS nombre_dist, d.ig_numero_docto,d.cc_acuse, ");
		qrySentencia.append(" TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision, ");
		qrySentencia.append(" TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_fecha_publicacion, ");
		qrySentencia.append(" TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc, ");
		qrySentencia.append(" d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto, ");
		qrySentencia.append(" DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), ");
		qrySentencia.append(" 'N', '','P', 'Dolar-Peso','') AS tipo_conversion, ");
		qrySentencia.append(" DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), ");
		qrySentencia.append(" 'P','1') AS tipo_cambio, ");
		qrySentencia.append(" NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento, ");
		qrySentencia.append(" decode(d.ic_tipo_financiamiento,1,nvl(d.fn_porc_descuento,0), 0) AS fn_porc_descuento, ");
		qrySentencia.append(" tf.cd_descripcion AS modo_plazo,cce.cd_descripcion AS tipo_cambio_estatus, ");
		qrySentencia.append(" TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fecha_cambio, m.ic_moneda,clas.cg_descripcion as CATEGORIA ");
		qrySentencia.append(" ,d.CG_VENTACARTERA as CG_VENTACARTERA ");
		qrySentencia.append(" ,ce.CT_CAMBIO_MOTIVO  as OBSERVACIONES "); 
		qrySentencia.append(" FROM dis_cambio_estatus ce, ");
		qrySentencia.append(" dis_documento d, ");
		qrySentencia.append(" comcat_epo e, ");
		qrySentencia.append(" comcat_pyme py, ");
		qrySentencia.append(" comrel_producto_epo pe, ");
		qrySentencia.append(" comcat_producto_nafin pn, ");
		qrySentencia.append(" comcat_tipo_financiamiento tf, ");
		qrySentencia.append(" comcat_estatus_docto ed, ");
		qrySentencia.append(" comcat_moneda m, ");
		qrySentencia.append(" comcat_cambio_estatus cce,comrel_clasificacion clas ");
		qrySentencia.append(" WHERE ");
		qrySentencia.append(" ce.ic_documento = d.ic_documento ");
		qrySentencia.append(" AND ce.ic_cambio_estatus = cce.ic_cambio_estatus ");
		qrySentencia.append(" AND d.ic_epo = e.ic_epo ");
		qrySentencia.append(" AND d.ic_pyme = py.ic_pyme ");
		qrySentencia.append(" AND d.ic_moneda = m.ic_moneda ");
		qrySentencia.append(" AND d.ic_moneda = m.ic_moneda ");
		qrySentencia.append(" AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento ");
		qrySentencia.append(" AND d.ic_epo = pe.ic_epo ");
		qrySentencia.append(" AND d.ic_epo = e.ic_epo ");
		qrySentencia.append(" AND d.ic_estatus_docto = ed.ic_estatus_docto ");
		qrySentencia.append(" AND pn.ic_producto_nafin = pe.ic_producto_nafin ");
		qrySentencia.append(" AND D.ic_clasificacion = clas.ic_clasificacion (+) ");
		qrySentencia.append(" " + condicion);
		qrySentencia.append(" AND pe.ic_producto_nafin = 4 ");
		qrySentencia.append(" AND pe.ic_epo = ? ");
		qrySentencia.append(" ORDER BY d.ic_documento ");

		log.info("Sentencia: " + qrySentencia.toString());
		log.info("Consiciones: " + conditions.toString());
		log.info("getDocumentQueryFile (S)");
		return qrySentencia.toString();
	}

/**
 * Genera lo archivos PDF sin contemplar paginaci�n. Obtiene los datos de la consulta generada por el m�todo getDocumentQueryFile
 * @param request
 * @param rs
 * @param path
 * @param tipo
 * @return nombre del archivo creado
 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){

		log.info("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();

		if(tipo.equals("PDF")){
			String dist              = "";
			String numDocto          = "";
			String numAcuseCarga     = "";
			String fechaEmision      = "";
			String fechaPublicacion  = "";
			String fechaVencimiento  = "";
			String plazoDocto        = "";
			String moneda            = "";
			String tipoConversion    = "";
			String tipoCambio        = "";
			String categoria         = "";
			String plazoDescuento    = "";
			String porcDescuento     = "";
			String modoPlazo         = "";
			String estatus           = "";
			String fechaCambio       = "";
			String icMoneda          = "";
			String observaciones     = "";
			String bandeVentaCartera = "";
			double monto             = 0;
			double montoValuado      = 0;
			double montoDescontar    = 0;
			try {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

				int numCols = (!"".equals(tipoConversion)?20:17);

				pdfDoc.setLTable(numCols,100);
				pdfDoc.setLCell("Datos del Documento Inicial", "celda01", ComunesPDF.CENTER,numCols);
				pdfDoc.setLCell("Distribuidor",                "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("No. Docto Inicial",           "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("No. Acuse Carga",             "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Emisi�n",               "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Pub.",                  "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha Venc.",                 "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo Docto",                 "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda",                      "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto",                       "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Categor�a",                   "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo para descuento en d�as","celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("% de Descuento",              "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto % de descuento",        "celda01", ComunesPDF.CENTER);
				if(!"".equals(tipoConversion)){
					pdfDoc.setLCell("Tipo Conv.",               "celda01", ComunesPDF.CENTER);
					pdfDoc.setLCell("Tipo Cambio",              "celda01", ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto Valuado",            "celda01", ComunesPDF.CENTER);
				}
				pdfDoc.setLCell("Modalidad de plazo",          "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de cambio de estatus",   "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de cambio de estatus",  "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Observaciones",               "celda01", ComunesPDF.CENTER);
				pdfDoc.setLHeaders();

				while (rs.next()){
					dist             = (rs.getString("NOMBRE_DIST")                 ==null)?"":rs.getString("NOMBRE_DIST");
					numDocto         = (rs.getString("IG_NUMERO_DOCTO")             ==null)?"":rs.getString("IG_NUMERO_DOCTO");
					numAcuseCarga    = (rs.getString("CC_ACUSE")                    ==null)?"":rs.getString("CC_ACUSE");
					fechaEmision     = (rs.getString("DF_FECHA_EMISION")            ==null)?"":rs.getString("DF_FECHA_EMISION");
					fechaPublicacion = (rs.getString("DF_FECHA_PUBLICACION")        ==null)?"":rs.getString("DF_FECHA_PUBLICACION");
					fechaVencimiento = (rs.getString("DF_FECHA_VENC")               ==null)?"":rs.getString("DF_FECHA_VENC");
					plazoDocto       = (rs.getString("IG_PLAZO_DOCTO")              ==null)?"":rs.getString("IG_PLAZO_DOCTO");
					moneda           = (rs.getString("MONEDA")                      ==null)?"":rs.getString("MONEDA");
					monto            = Double.parseDouble((rs.getString("FN_MONTO") ==null)?"0":rs.getString("FN_MONTO"));
					tipoConversion   = (rs.getString("TIPO_CONVERSION")             ==null)?"":rs.getString("TIPO_CONVERSION");
					tipoCambio       = (rs.getString("TIPO_CAMBIO")                 ==null)?"1":rs.getString("TIPO_CAMBIO");
					plazoDescuento   = (rs.getString("IG_PLAZO_DESCUENTO")          ==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
					porcDescuento    = (rs.getString("FN_PORC_DESCUENTO")           ==null)?"0":rs.getString("FN_PORC_DESCUENTO");
					modoPlazo        = (rs.getString("MODO_PLAZO")                  ==null)?"":rs.getString("MODO_PLAZO");
					estatus          = (rs.getString("TIPO_CAMBIO_ESTATUS")         ==null)?"":rs.getString("TIPO_CAMBIO_ESTATUS");
					fechaCambio      = (rs.getString("FECHA_CAMBIO")                ==null)?"":rs.getString("FECHA_CAMBIO");
					icMoneda         = (rs.getString("IC_MONEDA")                   ==null)?"":rs.getString("IC_MONEDA");
					categoria        = (rs.getString("CATEGORIA")                   ==null)?"":rs.getString("CATEGORIA");
					bandeVentaCartera= (rs.getString("CG_VENTACARTERA")             ==null)?"":rs.getString("CG_VENTACARTERA");
					observaciones    = (rs.getString("OBSERVACIONES")               ==null)?"":rs.getString("OBSERVACIONES");
					if (bandeVentaCartera.equals("S")){
						modoPlazo ="";
					}
					montoDescontar = monto*Double.parseDouble(porcDescuento)/100;
					montoValuado   = (monto-montoDescontar)*Double.parseDouble(tipoCambio);

					pdfDoc.setLCell(dist.replace(',',' '),                        "formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(numDocto,                                     "formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(numAcuseCarga,                                "formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(fechaEmision,                                 "formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(fechaPublicacion,                             "formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(fechaVencimiento,                             "formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(plazoDocto,                                   "formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(moneda,                                       "formas",ComunesPDF.LEFT);
					pdfDoc.setLCell("$ "+Comunes.formatoDecimal(monto,2),         "formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(categoria,                                    "formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(plazoDescuento,                               "formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(porcDescuento,                                "formas",ComunesPDF.LEFT);
					pdfDoc.setLCell("$ "+Comunes.formatoDecimal(montoDescontar,2),"formas",ComunesPDF.LEFT);

					if(!"".equals(tipoConversion)){
						pdfDoc.setLCell((("1".equals(tipoCambio))?tipoConversion:""),                              "formas",ComunesPDF.LEFT);
						pdfDoc.setLCell((("1".equals(tipoCambio))?Comunes.formatoDecimal(tipoCambio,2,false):""),  "formas",ComunesPDF.LEFT);
						pdfDoc.setLCell((("1".equals(tipoCambio))?Comunes.formatoDecimal(montoValuado,2,false):""),"formas",ComunesPDF.LEFT);
					}
					pdfDoc.setLCell(modoPlazo,     "formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(estatus,       "formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(fechaCambio,   "formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(observaciones, "formas",ComunesPDF.LEFT);
				}
				pdfDoc.addLTable();
				// TODO: LE HACEN FALTA LOS TOTALES 
				pdfDoc.endDocument();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF. ", e);
			}

		}
		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}

	public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo){
		return null;
	}


/************************************************************
 *                  GETTERS Y SETTERS                       *
 ************************************************************/
	public List getConditions(){
		return conditions; 
	}

	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getIc_pyme() {
		return ic_pyme;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}

	public String getIc_moneda() {
		return ic_moneda;
	}

	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}

	public String getIc_cambio_estatus() {
		return ic_cambio_estatus;
	}

	public void setIc_cambio_estatus(String ic_cambio_estatus) {
		this.ic_cambio_estatus = ic_cambio_estatus;
	}

	public String getFn_monto_de() {
		return fn_monto_de;
	}

	public void setFn_monto_de(String fn_monto_de) {
		this.fn_monto_de = fn_monto_de;
	}

	public String getFn_monto_a() {
		return fn_monto_a;
	}

	public void setFn_monto_a(String fn_monto_a) {
		this.fn_monto_a = fn_monto_a;
	}

	public String getIg_numero_docto() {
		return ig_numero_docto;
	}

	public void setIg_numero_docto(String ig_numero_docto) {
		this.ig_numero_docto = ig_numero_docto;
	}

	public String getMonto_con_descuento() {
		return monto_con_descuento;
	}

	public void setMonto_con_descuento(String monto_con_descuento) {
		this.monto_con_descuento = monto_con_descuento;
	}

	public String getCc_acuse() {
		return cc_acuse;
	}

	public void setCc_acuse(String cc_acuse) {
		this.cc_acuse = cc_acuse;
	}

	public String getModo_plazo() {
		return modo_plazo;
	}

	public void setModo_plazo(String modo_plazo) {
		this.modo_plazo = modo_plazo;
	}

	public String getFecha_emi_de() {
		return fecha_emi_de;
	}

	public void setFecha_emi_de(String fecha_emi_de) {
		this.fecha_emi_de = fecha_emi_de;
	}

	public String getFecha_emi_a() {
		return fecha_emi_a;
	}

	public void setFecha_emi_a(String fecha_emi_a) {
		this.fecha_emi_a = fecha_emi_a;
	}

	public String getFecha_vto_de() {
		return fecha_vto_de;
	}

	public void setFecha_vto_de(String fecha_vto_de) {
		this.fecha_vto_de = fecha_vto_de;
	}

	public String getFecha_vto_a() {
		return fecha_vto_a;
	}

	public void setFecha_vto_a(String fecha_vto_a) {
		this.fecha_vto_a = fecha_vto_a;
	}

	public String getFecha_publicacion_de() {
		return fecha_publicacion_de;
	}

	public void setFecha_publicacion_de(String fecha_publicacion_de) {
		this.fecha_publicacion_de = fecha_publicacion_de;
	}

	public String getFecha_publicacion_a() {
		return fecha_publicacion_a;
	}

	public void setFecha_publicacion_a(String fecha_publicacion_a) {
		this.fecha_publicacion_a = fecha_publicacion_a;
	}

	public String getFecha_cambio_de() {
		return fecha_cambio_de;
	}

	public void setFecha_cambio_de(String fecha_cambio_de) {
		this.fecha_cambio_de = fecha_cambio_de;
	}

	public String getFecha_cambio_a() {
		return fecha_cambio_a;
	}

	public void setFecha_cambio_a(String fecha_cambio_a) {
		this.fecha_cambio_a = fecha_cambio_a;
	}

	public String getNOnegociable() {
		return NOnegociable;
	}

	public void setNOnegociable(String NOnegociable) {
		this.NOnegociable = NOnegociable;
	}
}