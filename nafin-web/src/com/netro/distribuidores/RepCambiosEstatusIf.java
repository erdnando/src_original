package com.netro.distribuidores;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class RepCambiosEstatusIf{
	private static final Log log = ServiceLocator.getInstance().getLog(RepCambiosEstatusIf.class);
	private StringBuffer qrysentencia;
	private String ic_cambio_estatus="";
	private String tituloGrid="";
	private String ic_if = "";

	private Registros registros;
	private List conditions;

	public void setClaveEstatus(String ic_cambio_estatus){
		this.ic_cambio_estatus=ic_cambio_estatus;
	}
	
	public void setClaveIf(String ic_if) {
		this.ic_if = ic_if;
	}

	public Registros executeQuery() {
		AccesoDB con = new AccesoDB();
		Registros registros = new Registros();
		try{
			con.conexionDB();
			registros = con.consultarDB(this.getQrysentencia(),conditions);
			return registros;
		} catch(Exception e) {
			throw new AppException("RepCambiosEstatusIf::ExecuteQuery(Exception) ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(true); //Para consultas que hagan uso de tablas remotas via dblink
				con.cierraConexionDB();
			}
		}
	}

	public void setQrysentencia(){
		StringBuffer qrySentenciaDM = new StringBuffer(), qrySentenciaCC = new StringBuffer();
		this.qrysentencia = new StringBuffer();
		conditions = new ArrayList();
		if(	"14".equals(ic_cambio_estatus)	){

			qrySentenciaDM.append(
				"SELECT py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse, "+
				"       TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision, "+
				//"       TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga, "+
				"       TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc, "+
				"       d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto, "+
				"       DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
				"          'N', 'Sin conversion', 'P', 'Dolar-Peso', '') AS tipo_conversion, "+
				"       DECODE ( NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
				"          'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'), '1') AS tipo_cambio, "+
				"       NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento, "+
				"       NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento, "+
				"       tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus, "+
				"       m.ic_moneda, d.ic_documento, e.cg_razon_social AS nombre_epo, "+
				"       DECODE (NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza), "+
				"          'D', 'Delegada', 'N', 'No Delegada') AS tipo_cobranza, "+
				"       i.cg_razon_social AS nombre_if, "+
				"       DECODE ( pp.cg_tipo_credito, "+
				"          'D', 'Modalidad 1 (Riesgo Empresa de Primer Orden )', 'C', 'Modalidad 2 (Riesgo Distribuidor)') AS tipo_credito, "+
				"       ds.fn_importe_recibir AS monto_credito, d.ig_plazo_credito, "+
				"       tci.cd_descripcion AS tipo_cobro_int, "+
				"  		DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) as REFERENCIA_INT,"+
				"       DECODE (ds.cg_rel_mat, "+
				"          '+', fn_valor_tasa + fn_puntos, "+
				"          '-', fn_valor_tasa - fn_puntos, "+
				"          '*', fn_valor_tasa * fn_puntos, "+
				"          '/', fn_valor_tasa / fn_puntos) AS valor_tasa_int, "+
				"       ds.fn_importe_interes AS monto_tasa_int, "+
				"       TO_CHAR (d.df_carga, 'dd/mm/yyyy') AS fecha_publicacion, "+
				"       TO_CHAR (d.df_fecha_venc_credito, 'dd/mm/yyyy') AS fecha_venc_credito, "+
				"		TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fecha_cambio, "+
				"		ce.ct_cambio_motivo AS causa,"+
				"		lc.ic_moneda as moneda_linea"+
				"  FROM dis_documento d, "+
				"       comcat_pyme py, "+
				"       comcat_moneda m, "+
				"       comrel_producto_epo pe, "+
				"       comcat_producto_nafin pn, "+
				"       com_tipo_cambio tc, "+
				"       comcat_tipo_financiamiento tf, "+
				"       comcat_estatus_docto ed, "+
				"       dis_linea_credito_dm lc, "+
				"       comcat_epo e, "+
				"       dis_docto_seleccionado ds, "+
				"       comcat_if i, "+
				"       comrel_pyme_epo_x_producto pp, "+
				"       comcat_tipo_cobro_interes tci, "+
				"       comcat_tasa ct, "+
				"       dis_cambio_estatus ce "+
				" WHERE d.ic_pyme = py.ic_pyme "+
				"   AND ds.ic_documento = d.ic_documento "+
				"   AND ce.ic_documento = d.ic_documento "+
				"   AND d.ic_moneda = m.ic_moneda "+
				"   AND pn.ic_producto_nafin = ? "+
				"   AND pn.ic_producto_nafin = pe.ic_producto_nafin "+
				"   AND pe.ic_epo = d.ic_epo "+
				"   AND i.ic_if = lc.ic_if "+
				"   AND e.ic_epo = d.ic_epo "+
				"   AND pp.ic_epo = e.ic_epo "+
				"   AND pp.ic_pyme = py.ic_pyme "+
				"   AND pp.ic_producto_nafin = ? "+
				"   AND tc.dc_fecha IN (SELECT MAX (dc_fecha) "+
				"                         FROM com_tipo_cambio "+
				"                        WHERE ic_moneda = m.ic_moneda) "+
				"   AND m.ic_moneda = tc.ic_moneda "+
				"   AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento "+
				"   AND ed.ic_estatus_docto = d.ic_estatus_docto "+
				"   AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm "+
				"   AND tci.ic_tipo_cobro_interes = lc.ic_tipo_cobro_interes "+
				"   AND ct.ic_tasa = ds.ic_tasa "+
				"   AND ce.ic_cambio_estatus = ? "+
				"   AND TRUNC (ce.dc_fecha_cambio) = TRUNC (SYSDATE) "+
				"   AND lc.ic_if = ? ");
				conditions.add(new Integer(4));
				conditions.add(new Integer(4));
				conditions.add(new Integer(ic_cambio_estatus));
				conditions.add(new Integer(ic_if));

			qrySentenciaCC.append(
				"SELECT py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse, "+
				"       TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision, "+
				//"       TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga, "+
				"       TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc, "+
				"       d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto, "+
				"       DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
				"          'N', 'Sin conversion', 'P', 'Dolar-Peso', '') AS tipo_conversion, "+
				"       DECODE ( NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
				"          'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'), '1') AS tipo_cambio, "+
				"       NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento, "+
				"       NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento, "+
				"       tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus, "+
				"       m.ic_moneda, d.ic_documento, e.cg_razon_social AS nombre_epo, "+
				"       DECODE (NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza), "+
				"          'D', 'Delegada', 'N', 'No Delegada') AS tipo_cobranza, "+
				"       i.cg_razon_social AS nombre_if, "+
				"       DECODE ( pp.cg_tipo_credito, "+
				"          'D', 'Modalidad 1 (Riesgo Empresa de Primer Orden )', 'C', 'Modalidad 2 (Riesgo Distribuidor)') AS tipo_credito, "+
				"       ds.fn_importe_recibir AS monto_credito, d.ig_plazo_credito, "+
				"       tci.cd_descripcion AS tipo_cobro_int, "+
				"       ct.cd_nombre || ' ' || ds.cg_rel_mat || ' ' || ds.fn_puntos AS referencia_int, "+
				"       DECODE (ds.cg_rel_mat, "+
				"          '+', fn_valor_tasa + fn_puntos, "+
				"          '-', fn_valor_tasa - fn_puntos, "+
				"          '*', fn_valor_tasa * fn_puntos, "+
				"          '/', fn_valor_tasa / fn_puntos) AS valor_tasa_int, "+
				"       ds.fn_importe_interes AS monto_tasa_int, "+
				"       TO_CHAR (d.df_carga, 'dd/mm/yyyy') AS fecha_publicacion, "+
				"       TO_CHAR (d.df_fecha_venc_credito, 'dd/mm/yyyy') AS fecha_venc_credito, "+
				"		TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fecha_cambio, "+
				"		ce.ct_cambio_motivo AS causa,"+
				"		lc.ic_moneda as moneda_linea"+
				"  FROM dis_documento d, "+
				"       comcat_pyme py, "+
				"       comcat_moneda m, "+
				"       comrel_producto_epo pe, "+
				"       comcat_producto_nafin pn, "+
				"       com_tipo_cambio tc, "+
				"       comcat_tipo_financiamiento tf, "+
				"       comcat_estatus_docto ed, "+
				"       com_linea_credito lc, "+
				"       comcat_epo e, "+
				"       dis_docto_seleccionado ds, "+
				"       comcat_if i, "+
				"       comrel_pyme_epo_x_producto pp, "+
				"       comcat_tipo_cobro_interes tci, "+
				"       comcat_tasa ct, "+
				"       dis_cambio_estatus ce "+
				" WHERE d.ic_pyme = py.ic_pyme "+
				"   AND ds.ic_documento = d.ic_documento "+
				"   AND ce.ic_documento = d.ic_documento "+
				"   AND d.ic_moneda = m.ic_moneda "+
				"   AND pn.ic_producto_nafin = ? "+
				"   AND pn.ic_producto_nafin = pe.ic_producto_nafin "+
				"   AND pe.ic_epo = d.ic_epo "+
				"   AND i.ic_if = lc.ic_if "+
				"   AND e.ic_epo = d.ic_epo "+
				"   AND pp.ic_epo = e.ic_epo "+
				"   AND pp.ic_pyme = py.ic_pyme "+
				"   AND pp.ic_producto_nafin = ? "+
				"   AND tc.dc_fecha IN (SELECT MAX (dc_fecha) "+
				"                         FROM com_tipo_cambio "+
				"                        WHERE ic_moneda = m.ic_moneda) "+
				"   AND m.ic_moneda = tc.ic_moneda "+
				"   AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento "+
				"   AND ed.ic_estatus_docto = d.ic_estatus_docto "+
				"   AND d.ic_linea_credito = lc.ic_linea_credito "+
				"   AND tci.ic_tipo_cobro_interes = lc.ic_tipo_cobro_interes "+
				"   AND ct.ic_tasa = ds.ic_tasa "+
				"   AND ce.ic_cambio_estatus = ? "+
			   "   AND TRUNC (ce.dc_fecha_cambio) = TRUNC (SYSDATE) "+
			 	"   AND lc.ic_if = ? ");
			
			conditions.add(new Integer(4));
			conditions.add(new Integer(4));
			conditions.add(new Integer(ic_cambio_estatus));
			conditions.add(new Integer(ic_if));

			this.qrysentencia.append(qrySentenciaDM.toString() + " UNION ALL " + qrySentenciaCC.toString());
			log.info("query::::14::::::"+qrysentencia.toString()+":::conditions"+conditions.toString());
		}else if(	"2".equals(ic_cambio_estatus) || "23".equals(ic_cambio_estatus) || "34".equals(ic_cambio_estatus)	){
			qrySentenciaDM.append(
				"SELECT py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse, "+
				"       TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision, "+
				//"       TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga, "+
				"       TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc, "+
				"       d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto, "+
				"       DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
				"          'N', 'Sin conversion', 'P', 'Dolar-Peso', '') AS tipo_conversion, "+
				"       DECODE ( NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
				"          'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'), '1') AS tipo_cambio, "+
				"       NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento, "+
				"       NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento, "+
				"       tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus, "+
				"       m.ic_moneda, d.ic_documento, e.cg_razon_social AS nombre_epo, "+
				"       DECODE (NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza), "+
				"          'D', 'Delegada', 'N', 'No Delegada') AS tipo_cobranza, "+
				"       i.cg_razon_social AS nombre_if, "+
				"       DECODE ( pp.cg_tipo_credito, "+
				"          'D', 'Modalidad 1 (Riesgo Empresa de Primer Orden )', 'C', 'Modalidad 2 (Riesgo Distribuidor)') AS tipo_credito, "+
				"       ds.fn_importe_recibir AS monto_credito, d.ig_plazo_credito, "+
				"       tci.cd_descripcion AS tipo_cobro_int, "+
				"  		DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) as REFERENCIA_INT,"+
				"       DECODE (ds.cg_rel_mat, "+
				"          '+', fn_valor_tasa + fn_puntos, "+
				"          '-', fn_valor_tasa - fn_puntos, "+
				"          '*', fn_valor_tasa * fn_puntos, "+
				"          '/', fn_valor_tasa / fn_puntos) AS valor_tasa_int, "+
				"       ds.fn_importe_interes AS monto_tasa_int, "+
				"       TO_CHAR (d.df_carga, 'dd/mm/yyyy') AS fecha_publicacion, "+
				"       TO_CHAR (d.df_fecha_venc_credito, 'dd/mm/yyyy') AS fecha_venc_credito, "+
				"		TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fecha_cambio, "+
				"		ce.ct_cambio_motivo AS causa,"+
				"		lc.ic_moneda as moneda_linea"+
				"  FROM dis_documento d, "+
				"       comcat_pyme py, "+
				"       comcat_moneda m, "+
				"       comrel_producto_epo pe, "+
				"       comcat_producto_nafin pn, "+
				"       com_tipo_cambio tc, "+
				"       comcat_tipo_financiamiento tf, "+
				"       comcat_estatus_docto ed, "+
				"       dis_linea_credito_dm lc, "+
				"       comcat_epo e, "+
				"       dis_docto_seleccionado ds, "+
				"       comcat_if i, "+
				"       comrel_pyme_epo_x_producto pp, "+
				"       comcat_tipo_cobro_interes tci, "+
				"       comcat_tasa ct, "+
				"       dis_cambio_estatus ce "+
				" WHERE d.ic_pyme = py.ic_pyme "+
				"   AND ds.ic_documento = d.ic_documento "+
				"   AND ce.ic_documento = d.ic_documento "+
				"   AND d.ic_moneda = m.ic_moneda "+
				"   AND pn.ic_producto_nafin = ? "+
				"   AND pn.ic_producto_nafin = pe.ic_producto_nafin "+
				"   AND pe.ic_epo = d.ic_epo "+
				"   AND i.ic_if = lc.ic_if "+
				"   AND e.ic_epo = d.ic_epo "+
				"   AND pp.ic_epo = e.ic_epo "+
				"   AND pp.ic_pyme = py.ic_pyme "+
				"   AND pp.ic_producto_nafin = ? "+
				"   AND tc.dc_fecha IN (SELECT MAX (dc_fecha) "+
				"                         FROM com_tipo_cambio "+
				"                        WHERE ic_moneda = m.ic_moneda) "+
				"   AND m.ic_moneda = tc.ic_moneda "+
				"   AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento "+
				"   AND ed.ic_estatus_docto = d.ic_estatus_docto "+
				"   AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm "+
				"   AND tci.ic_tipo_cobro_interes = lc.ic_tipo_cobro_interes "+
				"   AND ct.ic_tasa = ds.ic_tasa "+
				"   AND ce.ic_cambio_estatus = ? "+
				"   AND TRUNC (ce.dc_fecha_cambio) = TRUNC (SYSDATE) "+
			   "   AND lc.ic_if = ? ");
			conditions.add(new Integer(4));
			conditions.add(new Integer(4));
			conditions.add(new Integer(ic_cambio_estatus));
			conditions.add(new Integer(ic_if));

		qrySentenciaCC.append(
				"SELECT py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse, "+
				"       TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision, "+
				//"       TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga, "+
				"       TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc, "+
				"       d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto, "+
				"       DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
				"          'N', 'Sin conversion', 'P', 'Dolar-Peso', '') AS tipo_conversion, "+
				"       DECODE ( NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
				"          'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'), '1') AS tipo_cambio, "+
				"       NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento, "+
				"       NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento, "+
				"       tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus, "+
				"       m.ic_moneda, d.ic_documento, e.cg_razon_social AS nombre_epo, "+
				"       DECODE (NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza), "+
				"          'D', 'Delegada', 'N', 'No Delegada') AS tipo_cobranza, "+
				"       i.cg_razon_social AS nombre_if, "+
				"       DECODE ( pp.cg_tipo_credito, "+
				"          'D', 'Modalidad 1 (Riesgo Empresa de Primer Orden )', 'C', 'Modalidad 2 (Riesgo Distribuidor)') AS tipo_credito, "+
				"       ds.fn_importe_recibir AS monto_credito, d.ig_plazo_credito, "+
				"       tci.cd_descripcion AS tipo_cobro_int, "+
				"       ct.cd_nombre || ' ' || ds.cg_rel_mat || ' ' || ds.fn_puntos AS referencia_int, "+
				"       DECODE (ds.cg_rel_mat, "+
				"          '+', fn_valor_tasa + fn_puntos, "+
				"          '-', fn_valor_tasa - fn_puntos, "+
				"          '*', fn_valor_tasa * fn_puntos, "+
				"          '/', fn_valor_tasa / fn_puntos) AS valor_tasa_int, "+
				"       ds.fn_importe_interes AS monto_tasa_int, "+
				"       TO_CHAR (d.df_carga, 'dd/mm/yyyy') AS fecha_publicacion, "+
				"       TO_CHAR (d.df_fecha_venc_credito, 'dd/mm/yyyy') AS fecha_venc_credito, "+
				"		TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fecha_cambio, "+
				"		ce.ct_cambio_motivo AS causa,"+
				"		lc.ic_moneda as moneda_linea"+
				"  FROM dis_documento d, "+
				"       comcat_pyme py, "+
				"       comcat_moneda m, "+
				"       comrel_producto_epo pe, "+
				"       comcat_producto_nafin pn, "+
				"       com_tipo_cambio tc, "+
				"       comcat_tipo_financiamiento tf, "+
				"       comcat_estatus_docto ed, "+
				"       com_linea_credito lc, "+
				"       comcat_epo e, "+
				"       dis_docto_seleccionado ds, "+
				"       comcat_if i, "+
				"       comrel_pyme_epo_x_producto pp, "+
				"       comcat_tipo_cobro_interes tci, "+
				"       comcat_tasa ct, "+
				"       dis_cambio_estatus ce "+
				" WHERE d.ic_pyme = py.ic_pyme "+
				"   AND ds.ic_documento = d.ic_documento "+
				"   AND ce.ic_documento = d.ic_documento "+
				"   AND d.ic_moneda = m.ic_moneda "+
				"   AND pn.ic_producto_nafin = ? "+
				"   AND pn.ic_producto_nafin = pe.ic_producto_nafin "+
				"   AND pe.ic_epo = d.ic_epo "+
				"   AND i.ic_if = lc.ic_if "+
				"   AND e.ic_epo = d.ic_epo "+
				"   AND pp.ic_epo = e.ic_epo "+
				"   AND pp.ic_pyme = py.ic_pyme "+
				"   AND pp.ic_producto_nafin = ? "+
				"   AND tc.dc_fecha IN (SELECT MAX (dc_fecha) "+
				"                         FROM com_tipo_cambio "+
				"                        WHERE ic_moneda = m.ic_moneda) "+
				"   AND m.ic_moneda = tc.ic_moneda "+
				"   AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento "+
				"   AND ed.ic_estatus_docto = d.ic_estatus_docto "+
				"   AND d.ic_linea_credito = lc.ic_linea_credito "+
				"   AND tci.ic_tipo_cobro_interes = lc.ic_tipo_cobro_interes "+
				"   AND ct.ic_tasa = ds.ic_tasa "+
			   "   AND ce.ic_cambio_estatus = ? "+
				"   AND TRUNC (ce.dc_fecha_cambio) = TRUNC (SYSDATE) "+
				"   AND lc.ic_if = ? ");
			conditions.add(new Integer(4));
			conditions.add(new Integer(4));
			conditions.add(new Integer(ic_cambio_estatus));
			conditions.add(new Integer(ic_if));

			this.qrysentencia.append(qrySentenciaDM.toString() + " UNION ALL " + qrySentenciaCC.toString());
			log.info("query:::::2-23-34::::"+qrysentencia.toString()+":::conditions"+conditions.toString());
		}else if(	"21".equals(ic_cambio_estatus)	){
		
			this.qrysentencia.append(
				"SELECT py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse, "+
				"       TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision, "+
				//"       TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga, "+
				"       TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc, "+
				"       d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto, "+
				"       DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
				"          'N', 'Sin conversion', 'P', 'Dolar-Peso', '') AS tipo_conversion, "+
				"       DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
				"          'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'), '1') AS tipo_cambio, "+
				"       NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento, "+
				"       NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento, "+
				"       tf.cd_descripcion AS modo_plazo, m.ic_moneda, d.ic_documento, "+
				"       e.cg_razon_social AS nombre_epo, "+
				"       DECODE (NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza), "+
				"          'D', 'Delegada', 'N', 'No Delegada') AS tipo_cobranza, "+
				"       DECODE (pep.cg_tipo_credito, "+
				"          'D', 'Modalidad 1 (Riesgo Empresa de Primer Orden )','C', 'Modalidad 2 (Riesgo Distribuidor)')AS tipo_credito, "+
				"       TO_CHAR (d.df_carga, 'dd/mm/yyyy') AS fecha_publicacion, "+
				"       TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fecha_cambio, "+
				"       TO_CHAR (ce.df_fecha_emision_anterior, 'dd/mm/yyyy') AS fecha_emision_ant, "+
				"       TO_CHAR (ce.df_fecha_venc_anterior, 'dd/mm/yyyy') AS fecha_venc_ant, "+
				"       ce.fn_monto_anterior, tf2.cd_descripcion AS modo_plazo_ant, "+
				"		d.ic_moneda,"+
				"		lc.ic_moneda as moneda_linea"+
				"  FROM dis_documento d, "+
				"       dis_linea_credito_dm lc, "+
				"       comcat_pyme py, "+
				"       comcat_moneda m, "+
				"       comrel_producto_epo pe, "+
				"       comcat_producto_nafin pn, "+
				"       com_tipo_cambio tc, "+
				"       comcat_tipo_financiamiento tf, "+
				"       comcat_tipo_financiamiento tf2, "+
				"       comcat_epo e, "+
				"       dis_cambio_estatus ce, "+
				"       comrel_pyme_epo_x_producto pep "+
				" WHERE d.ic_pyme = py.ic_pyme "+
				"   AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm "+
				"   AND d.ic_moneda = m.ic_moneda "+
				"   AND pn.ic_producto_nafin = ? "+
				"   AND pep.ic_pyme = py.ic_pyme "+
				"   AND pep.ic_epo = e.ic_epo "+
				"   AND pep.ic_producto_nafin = pn.ic_producto_nafin "+
				"   AND ce.ic_documento = d.ic_documento "+
				"   AND pn.ic_producto_nafin = pe.ic_producto_nafin "+
				"   AND pe.ic_epo = d.ic_epo "+
				"   AND e.ic_epo = d.ic_epo "+
				"   AND tc.dc_fecha IN (SELECT MAX (dc_fecha) "+
				"                         FROM com_tipo_cambio "+
				"                        WHERE ic_moneda = m.ic_moneda) "+
				"   AND m.ic_moneda = tc.ic_moneda "+
				"   AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento "+
				"   AND ce.ic_cambio_estatus = ? "+
				"   AND TRUNC (ce.dc_fecha_cambio) = TRUNC (SYSDATE) "+
				"   AND tf2.ic_tipo_financiamiento (+) = ce.ic_tipo_finan_ant "+
			  "   AND lc.ic_if = ? ");

			conditions.add(new Integer(4));
			conditions.add(new Integer(ic_cambio_estatus));
			conditions.add(new Integer(ic_if));
			log.info("query::::21::::::"+qrysentencia.toString()+":::conditions"+conditions.toString());
		}
		
	}
	
	public void setTituloGrid(String titulo){
		this.tituloGrid=titulo;
	}
	public String getTituloGrid(){
		return this.tituloGrid;
	}
	public String getQrysentencia(){
		return this.qrysentencia.toString();
	}

}//Class End.