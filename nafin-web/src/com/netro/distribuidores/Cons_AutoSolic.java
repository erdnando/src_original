package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class Cons_AutoSolic implements  IQueryGeneratorRegExtJS {	

	
	public Cons_AutoSolic() {}   
	
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(Cons_AutoSolic.class);  
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");  
	StringBuffer qrySentenciaDM = new StringBuffer("");  
	StringBuffer qrySentenciaCCC = new StringBuffer("");  
	StringBuffer qryCondicion = new StringBuffer("");  
	
	private String 	paginaOffset;
	private String 	paginaNo;
	private String ic_if;
	private String df_fecha_operacion_de;
	private String df_fecha_operacion_a;
	private String tipoCredito;
	private String ic_tipo_cobro_int;
	private String ic_documento;
	private String ig_numero_docto;
	private String ic_moneda;
	private String ic_estatus_solic;
	private String inDocumentos;
	private String pantalla;

	
	

  public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
		public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
	   log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
			
					
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		qrySentenciaDM = new StringBuffer("");  
	   qrySentenciaCCC = new StringBuffer("");  
		qryCondicion = new StringBuffer("");
		conditions 		= new ArrayList();
		
		if("Consulta".equals(pantalla)){
		
			if(!"".equals(ic_if)){
			  qryCondicion.append(" AND LCD.IC_IF = "+ic_if);
			}
	
			if(!"".equals(ic_documento)){
				qryCondicion.append(" AND D.IC_DOCUMENTO = "+ic_documento);
			}
			if(!"".equals(ic_moneda) ){
				qryCondicion.append(" AND D.IC_MONEDA = "+ic_moneda);
			}
			
			if(!"".equals(ic_estatus_solic) ){
				qryCondicion.append(" AND S.IC_ESTATUS_SOLIC =  "+ic_estatus_solic);
			}else if("".equals(ic_estatus_solic)) {
				qryCondicion.append("  AND S.IC_ESTATUS_SOLIC = 1 " );			
			}
			
			if(!"".equals(ic_tipo_cobro_int)) {
				qryCondicion.append(" AND TCI.ic_tipo_cobro_interes = "+ic_tipo_cobro_int);
			}
			if(!"".equals(ig_numero_docto))  {
				qryCondicion.append(" AND D.IG_NUMERO_DOCTO = '"+ig_numero_docto+"'");
			}
			
			if(!"".equals(df_fecha_operacion_de) &&  !"".equals(df_fecha_operacion_a) ) {
				qryCondicion.append("  AND TRUNC(CA.DF_FECHA_HORA)"+
										  "  between TRUNC(TO_DATE('"+df_fecha_operacion_de+"','DD/MM/YYYY'))"+
										  "  and TRUNC(TO_DATE('"+df_fecha_operacion_a+"','DD/MM/YYYY'))");
				
			}
			if(!"".equals(inDocumentos)) {
				qryCondicion.append(" AND D.IC_DOCUMENTO IN("+inDocumentos+")");		
			}
	
			
			qrySentenciaDM.append("  SELECT   " +
				"  E.cg_razon_social as NOMBRE_EPO"+
							"		,P.cg_razon_social as NOMBRE_DISTRIBUIDOR"   +
							"		,D.ig_numero_docto  as NUMERO_INICIAL "   +
							"		,to_char(D.DF_FECHA_EMISION,'dd/mm/yyyy') as FECHA_EMISION "   +
							"		,to_char(D.DF_FECHA_VENC,'dd/mm/yyyy') as FECHA_VENCIMIENTO "   +
							"		,P.ic_pyme"+
							"		,D.ig_plazo_docto as PLAZO_DOCTO "+
							"		,M.ic_moneda  AS IC_MONEDA "+
							"		,M.cd_nombre as MONEDA"   +
							"		,D.fn_monto as MONTO "   +
							"		,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONV"   +
							"		,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
							"		,D.IG_PLAZO_DESCUENTO as PLAZO_DESCUENTO  "   +
							"		,decode(D.ic_tipo_financiamiento,1,nvl(D.fn_porc_descuento,0),0) as PORCENTAJE_DESCUENTO"+
							"     ,(D.fn_monto * decode(D.ic_tipo_financiamiento,1,nvl(D.fn_porc_descuento,0),0))/100 as MONTO_DESCONTAR  " +
							" ,( D.fn_monto -  ( (D.fn_monto * decode(D.ic_tipo_financiamiento,1,nvl(D.fn_porc_descuento,0),0))/100 )  )  * (DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) as MONTO_VALUADO   " +		 
							
							"		,TF.cd_descripcion as MODALIDAD_PLAZO"+
							"		,TCI.cd_descripcion as TIPO_COBRO_INTERES"+
							"		,ES.cd_descripcion as ESTATUS"+
							"		,D.ic_documento  as NUMERO_DOCTO_FINAL" +
							"		,I.ic_if"+
							"		,I.cg_razon_social as NOMBRE_IF"+
							"		,PEP.cg_tipo_credito AS TIPO_LINEA_CREDITO"+
							"		,TO_CHAR(CA.df_fecha_hora,'dd/mm/yyyy') as FECHA_OPERACION"   +
							"		,DS.fn_importe_interes as MONTO_INTERES"+
							"		,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as FECHA_VENC_CREDITO	 "   +  
							"		,CT.CD_NOMBRE AS REFERENCIA_TASA		 "   +
							"		,DECODE(DS.CG_REL_MAT"+
							"			,'+',DS.fn_valor_tasa + DS.fn_puntos"+
							"			,'-',DS.fn_valor_tasa - DS.fn_puntos"+
							"			,'*',DS.fn_valor_tasa * DS.fn_puntos"+
							"			,'/',DS.fn_valor_tasa / DS.fn_puntos"+
							"		,0) AS REL_VALOR_TASA"   +
							" 		,TCI.ic_tipo_cobro_interes"   +
							"		,decode(nvl(PE.cg_tipo_cobranza,PN.cg_tipo_cobranza),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
							"		,I.ig_tipo_piso"+
							"		,D.ig_plazo_credito as PLAZO "+
							"		,decode(PEP.cg_tipo_credito,'D','Descuento Mercantil','C','Credito en Cuenta Corriente') as TIPO_CREDITO"+
							"		,PEP.cg_tipo_credito  as CG_TIPO_CREDITO "+							
							"		,LCD.ic_moneda as MONEDA_LINEA"+
							"		,DS.fn_importe_recibir as MONTO_CREDITO"+
							"		,Ds.FN_VALOR_TASA AS VALOR_TASA"+	
							"     ,'true' as SELECCIONAR  " +
				
				"  FROM DIS_DOCUMENTO D"   +
				"    ,COMCAT_PYME P"   +
				"    ,COMREL_PRODUCTO_EPO PE"   +
				"    ,COMCAT_PRODUCTO_NAFIN PN"   +
				"    ,COMCAT_MONEDA M"   +
				"    ,COM_TIPO_CAMBIO TC"   +
				"    ,DIS_LINEA_CREDITO_DM LCD"   +
				"    ,COMCAT_TIPO_COBRO_INTERES TCI"   +
				"    ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
				"    ,COMCAT_TASA CT"   +
				"    ,COMCAT_EPO E"+
				"    ,COMCAT_IF I"+
				"    ,DIS_DOCTO_SELECCIONADO DS"   +
				"    ,DIS_SOLICITUD S"  +
				"    ,COMCAT_ESTATUS_SOLIC ES"+
				"    ,COM_ACUSE3 CA"+
				"    ,COMCAT_TIPO_FINANCIAMIENTO TF"+
				"    ,COMREL_PYME_EPO_X_PRODUCTO PEP"+
				"  WHERE D.IC_EPO = PE.IC_EPO"   +
				"    AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
				"    AND PN.IC_PRODUCTO_NAFIN = 4"   +
				"    AND PEP.IC_EPO = E.IC_EPO"+
				"    AND PEP.IC_PYME = P.IC_PYME"+
				"    AND PEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
				"    AND P.IC_PYME = D.IC_PYME"   +
				"    AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
				"    AND D.IC_EPO = PE.IC_EPO"   +
				"    AND D.IC_MONEDA = M.IC_MONEDA"   +
				"    AND M.IC_MONEDA = TC.IC_MONEDA"   +
				"    AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
				"    AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM"   +
				"    AND LCD.IC_IF = IEP.IC_IF"   +
				"    AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES"   +
				"    AND IEP.IC_EPO = D.IC_EPO"   +
				"    AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
				"    AND DS.IC_TASA = CT.IC_TASA "  +
				"    AND D.IC_EPO = E.IC_EPO "+
				"    AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"+
				"    AND S.IC_ESTATUS_SOLIC = ES.IC_ESTATUS_SOLIC"+
				"	 AND S.IC_BLOQUEO = 1"+
				"    AND I.IC_IF = LCD.IC_IF"+
				"    AND S.CC_ACUSE = CA.CC_ACUSE"+
				"    AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO "+qryCondicion);
							  
								 
								 
	
			qrySentenciaCCC.append("  SELECT  "+
				"	E.cg_razon_social as NOMBRE_EPO"+
				" 		,P.cg_razon_social as NOMBRE_DISTRIBUIDOR"   +
				"       ,D.ig_numero_docto AS NUMERO_INICIAL "   +
							" 		,to_char(D.DF_FECHA_EMISION,'dd/mm/yyyy') as FECHA_EMISION "   +
							" 		,to_char(D.DF_FECHA_VENC,'dd/mm/yyyy') as FECHA_VENCIMIENTO "   +
							"       ,P.ic_pyme"+
							"       ,D.ig_plazo_docto AS PLAZO_DOCTO "+
							"       ,M.ic_moneda as IC_MONEDA"+
							" 		,M.cd_nombre as MONEDA"   +
							" 		,D.fn_monto AS  MONTO "   +
							"		,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONV"   +
							" 		,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
							" 		,D.IG_PLAZO_DESCUENTO AS PLAZO_DESCUENTO "   +
							"		,decode(D.ic_tipo_financiamiento,1,nvl(D.fn_porc_descuento,0),0) as PORCENTAJE_DESCUENTO"+
							 " ,(D.fn_monto * decode(D.ic_tipo_financiamiento,1,nvl(D.fn_porc_descuento,0),0))/100 as MONTO_DESCONTAR  " +
							 " ,( D.fn_monto -  ( (D.fn_monto * decode(D.ic_tipo_financiamiento,1,nvl(D.fn_porc_descuento,0),0))/100 )  )  * (DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) as MONTO_VALUADO   " +
			 
							"       ,TF.cd_descripcion as MODALIDAD_PLAZO"+
							"       ,TCI.cd_descripcion as tipo_cobro_interes"+
							"       ,ES.cd_descripcion as ESTATUS"+
							"       ,D.ic_documento as NUMERO_DOCTO_FINAL " +
							"       ,I.ic_if"+
							"       ,I.cg_razon_social as NOMBRE_IF"+
							"       ,PEP.cg_tipo_credito AS TIPO_LINEA_CREDITO"+
							"  		,TO_CHAR(CA.df_fecha_hora,'dd/mm/yyyy') as FECHA_OPERACION"   +
							"       ,DS.fn_importe_interes as MONTO_INTERES"+
							" 		,to_char(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') as FECHA_VENC_CREDITO "   +
							" 		,CT.CD_NOMBRE AS REFERENCIA_TASA		 "   +
							" 		,DECODE(DS.CG_REL_MAT"+
							"       ,'+',DS.fn_valor_tasa + DS.fn_puntos"+
							"       ,'-',DS.fn_valor_tasa - DS.fn_puntos"+
							"       ,'*',DS.fn_valor_tasa * DS.fn_puntos"+
							"       ,'/',DS.fn_valor_tasa / DS.fn_puntos"+
							"       ,0) AS REL_VALOR_TASA"   +
							" 		,TCI.ic_tipo_cobro_interes"   +
							"       ,decode(nvl(PE.cg_tipo_cobranza,PN.cg_tipo_cobranza),'D','Delegada','N','No Delegada') as TIPO_COBRANZA"+
							"       ,I.ig_tipo_piso"+
							"       ,D.ig_plazo_credito as PLAZO "+
							"		,decode(PEP.cg_tipo_credito,'D','Descuento Mercantil','C','Credito en Cuenta Corriente') as TIPO_CREDITO"+
							"		,PEP.cg_tipo_credito  as CG_TIPO_CREDITO "+	
							"		,LCD.ic_moneda as MONEDA_LINEA"+
							"		,DS.fn_importe_recibir as MONTO_CREDITO"+
							"		,DS.fn_valor_tasa AS VALOR_TASA"+
							"     ,'true' as SELECCIONAR  " +
				
						"  FROM DIS_DOCUMENTO D"   +
							"    ,COMCAT_PYME P"   +
							"    ,COMREL_PRODUCTO_EPO PE"   +
							"    ,COMCAT_PRODUCTO_NAFIN PN"   +
							"    ,COMCAT_MONEDA M"   +
							"    ,COM_TIPO_CAMBIO TC"   +
							"    ,COM_LINEA_CREDITO LCD"   +
							"    ,COMCAT_TIPO_COBRO_INTERES TCI"   +
							"    ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
							"    ,COMCAT_TASA CT"   +
							"    ,COMCAT_EPO E"+
							"    ,COMCAT_IF I"+
							"    ,DIS_DOCTO_SELECCIONADO DS"   +
							"    ,DIS_SOLICITUD S"  +
							"    ,COMCAT_ESTATUS_SOLIC ES"+
							"    ,COM_ACUSE3 CA"+
							"    ,COMCAT_TIPO_FINANCIAMIENTO TF"+
							"    ,COMREL_PYME_EPO_X_PRODUCTO PEP"+
							"  WHERE D.IC_EPO = PE.IC_EPO"   +
							"    AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
							"    AND PN.IC_PRODUCTO_NAFIN = 4"   +
							"    AND PEP.IC_EPO = E.IC_EPO"+
							"    AND PEP.IC_PYME = P.IC_PYME"+
							"    AND PEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"+
							"    AND P.IC_PYME = D.IC_PYME"   +
							"    AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
							"    AND D.IC_EPO = PE.IC_EPO"   +
							"    AND D.IC_MONEDA = M.IC_MONEDA"   +
							"    AND M.IC_MONEDA = TC.IC_MONEDA"   +
							"    AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
							"    AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
							"    AND LCD.IC_IF = IEP.IC_IF"   +
							"    AND TCI.IC_TIPO_COBRO_INTERES = LCD.IC_TIPO_COBRO_INTERES"   +						
							"    AND IEP.IC_EPO = D.IC_EPO"   +
							"    AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
							"    AND DS.IC_TASA = CT.IC_TASA "  +
							"    AND D.IC_EPO = E.IC_EPO "+
							"    AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"+
							"    AND S.IC_ESTATUS_SOLIC = ES.IC_ESTATUS_SOLIC"+
							"	 AND S.IC_BLOQUEO = 1"+
							"    AND I.IC_IF = LCD.IC_IF"+
							"    AND S.CC_ACUSE = CA.CC_ACUSE"+
							"    AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"+ qryCondicion);
					
					
					log.info("tipoCredito  "+tipoCredito);   
					
					log.info("qryCondicion  "+qryCondicion);   
							
					qrySentencia.append(" SELECT * FROM ( ");;
				if("D".equals(tipoCredito)){
							qrySentencia.append(qrySentenciaDM);
						 }else if("C".equals(tipoCredito)){
							qrySentencia.append(qrySentenciaCCC);
						 }else{
							qrySentencia.append(qrySentenciaDM +" UNION ALL "+qrySentenciaCCC);
						 }
						 qrySentencia.append("  )ORDER BY 1, 3  ");
						 
		}else  if("Detalle".equals(pantalla)){
		
			qrySentencia.append(" select lc.ic_linea_credito_dm as NO_FOLIO  "+
				"	 ,m.cd_nombre as MONEDA  "+
				"  ,TO_CHAR(lc.df_vencimiento_adicional,'dd/mm/yyyy') as FECHA_VENCIMIENTO  "+
				"  ,lc.fn_monto_autorizado_total as SALDO_INICIAL   "+
				"  ,lc.fn_saldo_financiado as SALDO_DOCUMENTOS_FINANCIADOS   "+
				"  ,lc.fn_saldo_total    as MONTO_DISPONIBLE   "+   
				" FROM DIS_LINEA_CREDITO_DM LC  "+
				"	,DIS_DOCUMENTO D  "+
				"	,COMCAT_MONEDA M  "+
				" WHERE lc.ic_linea_credito_dm = d.ic_linea_credito_dm  "+
				" AND lc.ic_moneda = m.ic_moneda ");
				
				if(!"".equals(ic_documento)){
					qrySentencia.append(" AND D.IC_DOCUMENTO = "+ic_documento);
				}		
		}
			 
		log.debug("qrySentencia  "+qrySentencia);
		log.debug("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";	
		return nombreArchivo;
					
	}
	
	/**
	 * 
	 * @return 
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
		
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		log.debug("crearCustomFile (S)");
		return nombreArchivo;
	}

	
	
	
	/**
	 * 
	 * @return 
	 * @param path
	 * @param vecFilas
	 */
	public String generar_Archivo_SIRAC(Vector vecFilas, String path) {
		
		log.info("generar_Archivo_SIRAC (E)");
		String nombreArchivo ="";
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		try { 
			
			OutputStreamWriter writer = null;
			BufferedWriter buffer = null;
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);	
			
			contenidoArchivo .append(" N�mero SIRAC,PYME,RFC,IF,Estado,Delg.,Domicilio,Colonia,C.P.,Fecha de constituci�n,Estrato,Ventas Netas Totales,Ventas Netas de Exportaci�n,Actividad,Principales productos,N�mero Electr�nico EPO,N�mero Electr�nico PYME,"+
									" N�mero de documento,Solicitud TNUF,Oficina,Funcionario,Emisor (Nombre EPO),Moneda,Importe del Cr�dito,Tipo de Producto,Tipo de cr�dito,Tipo de Tasa,Plazo,Fecha de Vencimiento,FPAGCAP,FPAGINT,FECHPPAGCAP,FECHPPAGINT,Tasa IF,"+
									" Relaci�n Matem�tica IF,Sobretasa IF,Tasa Usuario Final,Relaci�n Matem�tica Usuario Final,Sobretasa Usuario Final,Destino del Cr�dito,Tabla Amortizaci�n");
			
			for(int i=0;i<vecFilas.size();i++){
				Vector vecColumnas = (Vector)vecFilas.get(i);
				contenidoArchivo.append("\n");
				for(int j=0;j<=40;j++){
					if(j>0){
						contenidoArchivo .append(",");
					}
					contenidoArchivo .append(vecColumnas.get(j).toString().replace(',',' '));
				}
			}
										
			buffer.write(contenidoArchivo.toString());
			buffer.close();	
			contenidoArchivo = new StringBuffer();//Limpio    
			
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		 log.info("generar_Archivo_SIRAC (S)");
		}
		return nombreArchivo;
	}
	
	
	/**
	 * Metodo para imprimir Reporte SIRAC
	 * @return 
	 * @param path
	 * @param vecFilas
	 */
	public String imprimir_Reporte_SIRAC(HttpServletRequest request, Vector vecFilas, String path) {
		
		log.info("imprimir_Reporte_SIRAC (E)");
		String nombreArchivo ="";
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		ComunesPDF pdfDoc = new ComunesPDF();
		HttpSession session = request.getSession();
		
		try { 
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
			
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			 pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
		
				
				
				pdfDoc.setTable(11, 100);	
				pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero SIRAC","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("PYME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("RFC","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estado","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Delg.","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Domicilio","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Colonia","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("C.P.","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Constituci�n","celda01",ComunesPDF.CENTER);
				
				pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estrato","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Ventas Netas Totales","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Ventas Netas de Exportaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Actividad","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Principales productos","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Electr�nico EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Electr�nico PYME","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero de documento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Solicitud TNUF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Oficina","celda01",ComunesPDF.CENTER);
				
				pdfDoc.setCell("C","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Funcionario","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Emisor","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Importe del Cr�dito","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de Producto","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de cr�dito","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de Tasa","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("FPAGCAP","celda01",ComunesPDF.CENTER);
				
				pdfDoc.setCell("D","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("FPAGINT","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("FECHPPAGCAP","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("FECHPAGINT","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Relaci�n Matem�tica IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Sobretasa IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa Usuario Final","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Relaci�n Matem�tica Usuario Final","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Sobretasa Usuario Final","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Comunidad","celda01",ComunesPDF.CENTER);
				
				pdfDoc.setCell("E","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tabla Amortizaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01",ComunesPDF.CENTER, 9);				
			
				
			for(int i=0;i<vecFilas.size();i++){
				Vector vecColumnas = (Vector)vecFilas.get(i);
				String in_numero_sirac		=	(String)vecColumnas.get(0);
				String cg_razon_social		=	(String)vecColumnas.get(1);
				String cg_rfc				=	(String)vecColumnas.get(2);
				String ifb					=	(String)vecColumnas.get(3);
				String estado				=	(String)vecColumnas.get(4);
				String cg_municipio		=	(String)vecColumnas.get(5);
				String domicilio			=	(String)vecColumnas.get(6);
				String cg_colonia			=	(String)vecColumnas.get(7);
				String cg_cp				=	(String)vecColumnas.get(8);
				String fecha_const			=	(String)vecColumnas.get(9);
				String estrato				=	(String)vecColumnas.get(10);
				String fn_ventas_net_tot	=	(String)vecColumnas.get(11);
				String fn_ventas_net_exp	=	(String)vecColumnas.get(12);
				String sector				=	(String)vecColumnas.get(13);
				String cg_productos		=	(String)vecColumnas.get(14);
				String ne_epo				=	(String)vecColumnas.get(15);
				String ne_pyme				=	(String)vecColumnas.get(16);
				String igNumeroDocto		=	(String)vecColumnas.get(17);
				String tnuf				=	(String)vecColumnas.get(18);
				String oficina				=	(String)vecColumnas.get(19);
				String funcionario			=	(String)vecColumnas.get(20);
				String emisor				=	(String)vecColumnas.get(21);
				String moneda				=	(String)vecColumnas.get(22);
				String importeCredito		=	(String)vecColumnas.get(23);
				String tipoProducto		=	(String)vecColumnas.get(24);
				String tipoCredito			=	(String)vecColumnas.get(25);
				String tipoTasa			=	(String)vecColumnas.get(26);
				String plazo				=	(String)vecColumnas.get(27);
				String fechaVencimiento	=	(String)vecColumnas.get(28);
				String FPAGCAP				=	(String)vecColumnas.get(29);
				String FPAGINT				=	(String)vecColumnas.get(30);
				String FECHPPAGCAP			=	(String)vecColumnas.get(31);
				String FECHPPAGINT			=	(String)vecColumnas.get(32);
				String tasaIF				=	(String)vecColumnas.get(33);
				String relMatIF			=	(String)vecColumnas.get(34);
				String sobretasaIF			=	(String)vecColumnas.get(35);
				String tasaUF				=	(String)vecColumnas.get(36);
				String relMatUF			=	(String)vecColumnas.get(37);
				String sobretasaUF			=	(String)vecColumnas.get(38);
				String destinoCredito		=	(String)vecColumnas.get(39);
				String tablaAmort			=	(String)vecColumnas.get(40);
				
				
				pdfDoc.setCell("A","formas",ComunesPDF.CENTER);
			   pdfDoc.setCell(in_numero_sirac,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(cg_razon_social,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(cg_rfc,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(ifb,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(estado,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(cg_municipio,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(domicilio,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(cg_colonia,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(cg_cp,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fecha_const,"formas",ComunesPDF.CENTER);
		
				pdfDoc.setCell("B","formas",ComunesPDF.CENTER);
				pdfDoc.setCell(estrato,"formas",ComunesPDF.LEFT);            
				pdfDoc.setCell("$"+Comunes.formatoDecimal(fn_ventas_net_tot,2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(fn_ventas_net_exp,2),"formas",ComunesPDF.RIGHT);
				
				pdfDoc.setCell(sector,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(cg_productos,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(ne_epo,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(ne_pyme,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(igNumeroDocto,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(tnuf,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(oficina,"formas",ComunesPDF.LEFT);
				
				pdfDoc.setCell("C","formas",ComunesPDF.CENTER);
				pdfDoc.setCell(funcionario,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(emisor,"formas",ComunesPDF.LEFT);
            pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell("$"+Comunes.formatoDecimal(importeCredito,2),"formas",ComunesPDF.RIGHT);				
            pdfDoc.setCell(tipoProducto,"formas",ComunesPDF.LEFT);
            pdfDoc.setCell(tipoCredito,"formas",ComunesPDF.LEFT);
            pdfDoc.setCell(tipoTasa,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(plazo,"formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(FPAGCAP,"formas",ComunesPDF.CENTER);
      
				pdfDoc.setCell("D","formas",ComunesPDF.CENTER);
            pdfDoc.setCell(FPAGINT,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(FECHPPAGCAP,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(FECHPPAGINT,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(tasaIF,"formas",ComunesPDF.LEFT);
            pdfDoc.setCell(relMatIF,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(sobretasaIF,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(tasaUF,"formas",ComunesPDF.LEFT);
            pdfDoc.setCell(relMatUF,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(sobretasaUF,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(destinoCredito,"formas",ComunesPDF.LEFT);
      
			   pdfDoc.setCell("E","formas",ComunesPDF.CENTER);
				pdfDoc.setCell(tablaAmort,"formas",ComunesPDF.LEFT);
				pdfDoc.setCell(" ","formas",ComunesPDF.CENTER,9);		
								
			}
			pdfDoc.addTable();	
			pdfDoc.endDocument();	
			  
			
			
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		 log.info("imprimir_Reporte_SIRAC (S)");
		}
		return nombreArchivo;
	}
	

	
	public List getConditions() {  return conditions;  }  
	public String getPaginaNo() { return paginaNo; 	}
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getDf_fecha_operacion_de() {
		return df_fecha_operacion_de;
	}

	public void setDf_fecha_operacion_de(String df_fecha_operacion_de) {
		this.df_fecha_operacion_de = df_fecha_operacion_de;
	}

	public String getDf_fecha_operacion_a() {
		return df_fecha_operacion_a;
	}

	public void setDf_fecha_operacion_a(String df_fecha_operacion_a) {
		this.df_fecha_operacion_a = df_fecha_operacion_a;
	}

	public String getTipoCredito() {
		return tipoCredito;
	}

	public void setTipoCredito(String tipoCredito) {
		this.tipoCredito = tipoCredito;
	}

	public String getIc_tipo_cobro_int() {
		return ic_tipo_cobro_int;
	}

	public void setIc_tipo_cobro_int(String ic_tipo_cobro_int) {
		this.ic_tipo_cobro_int = ic_tipo_cobro_int;
	}

	public String getIc_documento() {
		return ic_documento;
	}

	public void setIc_documento(String ic_documento) {
		this.ic_documento = ic_documento;
	}

	public String getIg_numero_docto() {
		return ig_numero_docto;
	}

	public void setIg_numero_docto(String ig_numero_docto) {
		this.ig_numero_docto = ig_numero_docto;
	}

	public String getIc_moneda() {
		return ic_moneda;
	}

	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}

	public String getIc_estatus_solic() {
		return ic_estatus_solic;
	}

	public void setIc_estatus_solic(String ic_estatus_solic) {
		this.ic_estatus_solic = ic_estatus_solic;
	}

	public String getInDocumentos() {
		return inDocumentos;
	}

	public void setInDocumentos(String inDocumentos) {
		this.inDocumentos = inDocumentos;
	}

	public String getPantalla() {
		return pantalla;
	}

	public void setPantalla(String pantalla) {
		this.pantalla = pantalla;
	}

	
	
}