package com.netro.distribuidores;

import com.netro.exception.NafinException;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class LibLimDistribuidores  {

	public LibLimDistribuidores() {
	}

	//Variable para enviar mensajes al log.
	private final static Log log = ServiceLocator.getInstance().getLog(LibLimDistribuidores.class);

	//Agregado por FODEA 029 Distribuidores 2013

	public boolean liberarLimitesDist(String documentos [], String montos [], String claveNombre, String noAcuse) throws NafinException{
		System.out.println("liberarLimitesDist (E)");

		AccesoDB con = new AccesoDB();
		boolean exito = true;
		String qry = "Update dis_documento set ic_estatus_docto = 11 " +
					" where ic_documento = ? ";
		PreparedStatement psactualiza = null;
		try{
			con.conexionDB();

			psactualiza = con.queryPrecompilado(qry);


		if(documentos !=null){
		for (int i=0; i<documentos.length; i++) {

			String documento =  documentos[i].toString();
			String monto =  montos[i].toString();
			psactualiza.clearParameters();
			psactualiza.setInt(1,Integer.parseInt(documento));
			psactualiza.executeUpdate();
			bitacoraLimitesDist(documento,monto, claveNombre, noAcuse);

		}

	}
			psactualiza.close();

			System.out.println(" liberarLimitesDist ::qry::   "+qry);

		}catch(SQLException sqle){
			exito = false;
			System.out.println("\nError\n");
			sqle.printStackTrace();
			if (sqle.getErrorCode()==20001) {
					System.out.println("Excepcion "+sqle.getMessage());
					//throw new NafinException(sqle.getMessage());
			}
		}catch(Exception e){
			exito = false;
			System.out.println("\nError\n"+e);
			e.printStackTrace();
			//throw new NafinException("Hubo error en la confirmación del documento");
		}finally{
			System.out.println("liberarLimitesDist (S)");
			if (con.hayConexionAbierta()){
				con.terminaTransaccion(exito);
				con.cierraConexionDB();
			}
		}
	   return exito;
	}

	public void bitacoraLimitesDist(String documento, String monto, String claveNombre, String noAcuse) throws NafinException{
		log.info("bitacoraLimitesDist :::: (E)");

			AccesoDB con = new AccesoDB();
			boolean exito = true;
			StringBuffer strSQL =  new StringBuffer();

			try{
				con.conexionDB();
				PreparedStatement ps = null;

				strSQL.append(" INSERT INTO  BIT_BITACORA_LIBIF "+
						  "( DF_FECHA_PREPAGO, IC_DOCUMENTO, FN_MONTO_DOCUMENTO, CG_CLAVE_NOMBRE, IC_ACUSE ) "+
						  " VALUES(SYSDATE,?,?,?,?)");

				ps = con.queryPrecompilado(strSQL.toString());
				ps.setString(1,documento);
				ps.setString(2,monto);
				ps.setString(3,claveNombre);
				ps.setString(4,noAcuse);

				ps.executeUpdate();
				ps.close();
			log.debug("strSQL:::::::: :: "+strSQL);


			}catch(Exception e){
				exito = false;
				log.error("\nError\n"+e);
				e.printStackTrace();
			}finally{
				log.info("bitacoraLimitesDist :::: (S)");
				if (con.hayConexionAbierta()){
					con.terminaTransaccion(exito);
					con.cierraConexionDB();
				}
			}
	}

}