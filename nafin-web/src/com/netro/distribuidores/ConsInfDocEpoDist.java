package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;
import org.apache.commons.logging.Log;

public class ConsInfDocEpoDist implements IQueryGenerator, IQueryGeneratorRegExtJS {

	public ConsInfDocEpoDist(){}

	public String getAggregateCalculationQuery(HttpServletRequest request){
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
    	StringBuffer qrySentencia	= new StringBuffer();
    	StringBuffer qryDM			= new StringBuffer();
    	StringBuffer qryCCC			= new StringBuffer();
		StringBuffer qryFdR			= new StringBuffer();
    	StringBuffer condicion		= new StringBuffer();
      
		String	ic_epo				=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String	ic_pyme				=	(request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String	ic_if				=	(request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String	ic_estatus_docto	=	(request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto");
		String 	ig_numero_docto		=	(request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String 	fecha_seleccion_de	=	(request.getParameter("fecha_seleccion_de")==null)?"":request.getParameter("fecha_seleccion_de");
		String 	fecha_seleccion_a	=	(request.getParameter("fecha_seleccion_a")==null)?"":request.getParameter("fecha_seleccion_a");
		String 	cc_acuse			=	(request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String 	monto_credito_de	=	(request.getParameter("monto_credito_de")==null)?"":request.getParameter("monto_credito_de");
		String 	monto_credito_a		=	(request.getParameter("monto_credito_a")==null)?"":request.getParameter("monto_credito_a");
		String 	fecha_emision_de	=	(request.getParameter("fecha_emision_de")==null)?"":request.getParameter("fecha_emision_de");
		String 	fecha_emision_a		=	(request.getParameter("fecha_emision_a")==null)?"":request.getParameter("fecha_emision_a");
		String 	fecha_vto_de		=	(request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String 	fecha_vto_a			=	(request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String 	fecha_vto_credito_de=	(request.getParameter("fecha_vto_credito_de")==null)?"":request.getParameter("fecha_vto_credito_de");
		String 	fecha_vto_credito_a =	(request.getParameter("fecha_vto_credito_a")==null)?"":request.getParameter("fecha_vto_credito_a");
		String 	ic_tipo_cobro_int	=	(request.getParameter("ic_tipo_cobro_int")==null)?"":request.getParameter("ic_tipo_cobro_int");
		String	ic_moneda			=	(request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String 	fn_monto_de			=	(request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String 	fn_monto_a			=	(request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String 	monto_con_descuento	=	(request.getParameter("monto_con_descuento")==null)?"":"checked";
		String	solo_cambio			=	(request.getParameter("solo_cambio")==null)?"":request.getParameter("solo_cambio");
		String	modo_plazo			=	(request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
		String	tipo_credito		=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String	ic_documento		=	(request.getParameter("ic_documento")==null)?"":request.getParameter("ic_documento");
		String 	fecha_publicacion_de=	(request.getParameter("fecha_publicacion_de")==null)?fechaHoy:request.getParameter("fecha_publicacion_de");
		String 	fecha_publicacion_a	=	(request.getParameter("fecha_publicacion_a")==null)?fechaHoy:request.getParameter("fecha_publicacion_a");
		String 	NOnegociable			=	(request.getParameter("NOnegociable")==null)?"":request.getParameter("NOnegociable");//Fodea 029-2010 Distribuidores Fase III
		String tipo_pago =  (request.getParameter("tipo_pago")==null)?"":request.getParameter("tipo_pago"); // F009-2015
		try {
			if(!"".equals(ic_pyme)&&ic_pyme!=null)
				condicion.append(" AND D.IC_PYME = "+ic_pyme);
			if("2".equals(ic_estatus_docto)||"5".equals(ic_estatus_docto)||"9".equals(ic_estatus_docto)||"1".equals(ic_estatus_docto)||"32".equals(ic_estatus_docto)||"28".equals(ic_estatus_docto)||"30".equals(ic_estatus_docto))
				condicion.append(" AND D.IC_ESTATUS_DOCTO ="+ic_estatus_docto);
			if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
				condicion.append(" AND D.ig_numero_docto = '"+ig_numero_docto+"'");
			if(!"".equals(cc_acuse)&& cc_acuse!=null)
				condicion.append(" AND D.cc_acuse = '"+cc_acuse+"'");
			if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null)
				condicion.append(" AND TRUNC(D.df_fecha_emision) BETWEEN TRUNC(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) AND TRUNC(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
				condicion.append(" AND TRUNC(D.df_fecha_venc) BETWEEN TRUNC(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) AND TRUNC(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null)
				condicion.append(" AND TRUNC(D.df_fecha_venc_credito) BETWEEN TRUNC(to_date('"+fecha_vto_credito_de+"','dd/mm/yyyy')) AND TRUNC(to_date('"+fecha_vto_credito_a+"','dd/mm/yyyy'))");
			if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null)
				condicion.append(" AND LC.ic_tipo_cobro_interes = "+ic_tipo_cobro_int);
			if(!"".equals(ic_moneda)&&ic_moneda!=null)
				condicion.append(" AND D.ic_moneda = "+ic_moneda);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
				condicion.append(" AND (D.fn_monto*(fn_porc_descuento/100)) BETWEEN "+fn_monto_de+" AND "+fn_monto_a);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
				condicion.append(" AND D.fn_monto BETWEEN "+fn_monto_de+" AND "+fn_monto_a);
			if(!"".equals(solo_cambio)&& solo_cambio!=null)
				condicion.append(" and D.ic_documento in (select ic_documento from dis_cambio_estatus)");
				//condicion.append(" AND D.ic_documento IN (SELECT ic_documento FROM dis_cambio_estatus WHERE ic_cambio_estatus = 8)");
			if(!"".equals(modo_plazo)&& modo_plazo!=null)
				condicion.append(" AND D.ic_tipo_financiamiento = "+modo_plazo);
			if(!"".equals(ic_documento)&&ic_documento!=null)
				condicion.append(" AND D.IC_DOCUMENTO = "+ic_documento);
			if(!"".equals(ic_if)&&ic_if!=null)
				condicion.append(" AND LC.IC_IF = "+ic_if);
			if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
				condicion.append(" AND TRUNC(D.df_carga) BETWEEN TRUNC(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) AND TRUNC(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))");
			if("".equals(ic_estatus_docto) && NOnegociable.equals("N")){ //Fodea 029-2010 Distribuidores Fase III
				 condicion.append(" AND D.ic_estatus_docto IN(2,3,4,5,9,11,20,22,24,32,28,30)");//MOD +(24)
      }else 	if("".equals(ic_estatus_docto) && NOnegociable.equals("S")){
      	 condicion.append(" AND D.ic_estatus_docto IN(1,2,3,4,5,9,11,20,22,24,32,20,30)");//MOD +(24)
      }
			if(!"".equals(ic_estatus_docto)&&!"2".equals(ic_estatus_docto)&&!"5".equals(ic_estatus_docto)&&!"9".equals(ic_estatus_docto)&&!"1".equals(ic_estatus_docto)&&!"24".equals(ic_estatus_docto)&&!"32".equals(ic_estatus_docto)&&!"28".equals(ic_estatus_docto)&&!"30".equals(ic_estatus_docto))	//MOD +(&&!"24".equals(ic_estatus_docto))
				condicion.append(" AND D.ic_documento IS NULL");
			if(!"".equals(monto_credito_de)&& monto_credito_de!=null&&!"".equals(monto_credito_a)&& monto_credito_a!=null)
				condicion.append(" AND D.ic_documento IS NULL");
			if("F".equals(tipo_credito)) {
				//condicion.append(" AND pep.CG_TIPO_CREDITO IS NULL");
			} else {
				condicion.append(" AND pep.CG_TIPO_CREDITO IS NOT NULL");
			}
			if(!tipo_pago.equals("")){
				condicion.append(" AND D.IG_TIPO_PAGO = " + tipo_pago);
			}
			qryDM.append(
				" SELECT   /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */"   +
				"        d.ic_moneda as moneda, m.cd_nombre as nommoneda, d.fn_monto as monto "   +
				"   FROM dis_documento d,"   +
				"        comcat_pyme py,"   +
				"        comrel_producto_epo pe,"   +
				"        comrel_pyme_epo_x_producto pep,"   +
				"        dis_linea_credito_dm lc,"   +
				"        comcat_moneda m,"   +
				"        comcat_estatus_docto ed,"   +
				"        com_tipo_cambio tc,"   +
				"        comcat_producto_nafin pn,"   +
				"        comcat_tipo_financiamiento tf"   +
				"  WHERE d.ic_epo = pep.ic_epo"   +
				"    AND d.ic_pyme = pep.ic_pyme"   +
				"    AND d.ic_producto_nafin = pep.ic_producto_nafin"   +
				"    AND pep.ic_pyme = py.ic_pyme"   +
				"    AND d.ic_producto_nafin = pn.ic_producto_nafin"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND d.ic_producto_nafin = pe.ic_producto_nafin"   +
				"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento"   +
				"    AND d.ic_estatus_docto = ed.ic_estatus_docto"   +
				"    AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm (+)"   +
				"    AND d.ic_moneda = tc.ic_moneda"   +
				"    AND tc.dc_fecha IN (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = d.ic_moneda)"   +
				"    AND pep.cg_tipo_credito = 'D'"   +
				"    AND d.ic_epo = "+ic_epo+" "   +
				"    AND d.ic_producto_nafin = 4 "+condicion.toString());
				
			qryCCC.append(
				" SELECT   /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */"   +
				"        d.ic_moneda as moneda, m.cd_nombre as nommoneda, d.fn_monto as monto "   +
				"   FROM dis_documento d,"   +
				"        comcat_pyme py,"   +
				"        comrel_producto_epo pe,"   +
				"        comrel_pyme_epo_x_producto pep,"   +
				"        com_linea_credito lc,"   +
				"        comcat_moneda m,"   +
				"        comcat_estatus_docto ed,"   +
				"        com_tipo_cambio tc,"   +
				"        comcat_producto_nafin pn,"   +
				"        comcat_tipo_financiamiento tf"   +
				"  WHERE d.ic_epo = pep.ic_epo"   +
				"    AND d.ic_pyme = pep.ic_pyme"   +
				"    AND d.ic_producto_nafin = pep.ic_producto_nafin"   +
				"    AND pep.ic_pyme = py.ic_pyme"   +
				"    AND d.ic_producto_nafin = pn.ic_producto_nafin"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND d.ic_producto_nafin = pe.ic_producto_nafin"   +
				"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento"   +
				"    AND d.ic_estatus_docto = ed.ic_estatus_docto"   +
				"    AND d.ic_linea_credito = lc.ic_linea_credito (+) "   +
				"    AND d.ic_moneda = tc.ic_moneda"   +
				"    AND tc.dc_fecha IN (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = d.ic_moneda)"   +
				"    AND pep.cg_tipo_credito = 'C'"   +
				"    AND d.ic_epo = "+ic_epo+" "   +
				"    AND d.ic_producto_nafin = 4 "+condicion.toString());
				
			qryFdR.append(
				" SELECT   /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */"   +
				"        d.ic_moneda as moneda, m.cd_nombre as nommoneda, d.fn_monto as monto "   +
				"   FROM dis_documento d,"   +
				"        comcat_pyme py,"   +
				"        comrel_producto_epo pe,"   +
				"        comrel_pyme_epo_x_producto pep,"   +
				"        com_linea_credito lc,"   +
				"        comcat_moneda m,"   +
				"        comcat_estatus_docto ed,"   +
				"        com_tipo_cambio tc,"   +
				"        comcat_producto_nafin pn,"   +
				"        comcat_tipo_financiamiento tf"   +
				"  WHERE d.ic_epo = pep.ic_epo"   +
				"    AND d.ic_pyme = pep.ic_pyme"   +
				"    AND d.ic_producto_nafin = pep.ic_producto_nafin"   +
				"    AND pep.ic_pyme = py.ic_pyme"   +
				"    AND d.ic_producto_nafin = pn.ic_producto_nafin"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND d.ic_producto_nafin = pe.ic_producto_nafin"   +
				"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento (+)"   +
				"    AND d.ic_estatus_docto = ed.ic_estatus_docto"   +
				"    AND d.ic_linea_credito = lc.ic_linea_credito (+) "   +
				"    AND d.ic_moneda = tc.ic_moneda"   +
				"    AND tc.dc_fecha IN (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = d.ic_moneda)"   +
				"    AND pe.cg_tipos_credito = 'F'"   +
				"    AND d.ic_epo = "+ic_epo+" "   +
				" 	  and d.ic_tipo_financiamiento is null  "+			
				"    AND d.ic_producto_nafin = 4 "+condicion.toString());
				
				
			String qryAux = "";	
			if("D".equals(tipo_credito))
				qryAux = qryDM.toString();
			else if("C".equals(tipo_credito))
				qryAux = qryCCC.toString();
			else if("F".equals(tipo_credito))
				qryAux = qryFdR.toString();
			else
				qryAux = qryDM.toString()+ " UNION ALL "+qryCCC.toString();

		System.out.println("tipo_credito>>>>>>>>"+tipo_credito);

			qrySentencia.append(
				"SELECT moneda, nommoneda, COUNT (1), SUM (monto), 'ConsInfDocEpoDist::getAggregateCalculationQuery'"+
				"  FROM ("+qryAux.toString()+")"+
				"GROUP BY  moneda, nommoneda ORDER BY moneda ");

		}catch(Exception e){
			System.out.println("CambioEstatusEpoDE::getAggregateCalculationQuery "+e);
		}
		return qrySentencia.toString();
	}

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids) {
		int i=0;
		StringBuffer qrySentencia	= new StringBuffer();
    	StringBuffer qryDM			= new StringBuffer();
    	StringBuffer qryCCC			= new StringBuffer();
		StringBuffer qryFdR			= new StringBuffer();
    	StringBuffer condicion		= new StringBuffer();
    	
		String ic_epo = (request.getParameter("ic_epo") == null) ? "" : request.getParameter("ic_epo");
		String tipo_credito		=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
    
		condicion.append(" AND d.ic_documento in (");
		i=0;
		for (Iterator it = ids.iterator(); it.hasNext();i++){
        	if(i>0){
				condicion.append(",");
			}
			condicion.append(" "+it.next().toString()+" ");
		}
		condicion.append(") ");

    String	ic_estatus_docto	=	(request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto");
    String 	NOnegociable			=	(request.getParameter("NOnegociable")==null)?"":request.getParameter("NOnegociable");//Fodea 029-2010 Distribuidores Fase III
	
  	if("2".equals(ic_estatus_docto)||"5".equals(ic_estatus_docto)||"9".equals(ic_estatus_docto)||"1".equals(ic_estatus_docto)||"24".equals(ic_estatus_docto)||"32".equals(ic_estatus_docto)||"30".equals(ic_estatus_docto)||"30".equals(ic_estatus_docto))//MOD +(||"24".equals(ic_estatus_docto))
				condicion.append(" AND D.IC_ESTATUS_DOCTO ="+ic_estatus_docto);
		
    if("".equals(ic_estatus_docto) && NOnegociable.equals("N")){ //Fodea 029-2010 Distribuidores Fase III
				 condicion.append(" and D.ic_estatus_docto in(2,3,4,5,9,11,20,22,24,32,28,30)");//MOD +(24)
    }else 	if("".equals(ic_estatus_docto) && NOnegociable.equals("S")){
      	 condicion.append(" and D.ic_estatus_docto in(1,2,3,4,5,9,11,20,22,24,32,20,28,30)");//MOD +(24)
    }
    

		qryDM.append(
			" SELECT   /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */"   +
			"        py.cg_razon_social AS nombre_dist, d.ig_numero_docto,"   +
			"        d.cc_acuse,"   +
			"        TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision,"   +
			"        TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga,"   +
			"        TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc,"   +
			"        d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto,"   +
			"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', '','P', 'Dolar-Peso','') AS tipo_conversion,"   +
			"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio,"   +
			"        NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento,"   +
			"        NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento,"   +
			"        tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus,"   +
			"        m.ic_moneda, d.ic_documento, lc.ic_moneda AS moneda_linea,clas.cg_descripcion as CATEGORIA, 'ConsInfDocEpoDist::getDocumentSummaryQueryForIds'"   +
			"        ,d.CG_VENTACARTERA as CG_VENTACARTERA, D.IG_TIPO_PAGO "+
			"        ,(SELECT CASE WHEN COUNT(*) > 0 THEN 'S' ELSE 'N' END FROM COM_ARCDOCTOS WHERE CC_ACUSE = d.cc_acuse) AS MUESTRA_VISOR " +
			"   FROM dis_documento d,"   +
			"        comcat_pyme py,"   +
			"        comrel_producto_epo pe,"   +
			"        comrel_pyme_epo_x_producto pep,"   +
			"        dis_linea_credito_dm lc,"   +
			"        comcat_moneda m,"   +
			"        comcat_estatus_docto ed,"   +
			"        com_tipo_cambio tc,"   +
			"        comcat_producto_nafin pn,"   +
			"        comcat_tipo_financiamiento tf,comrel_clasificacion clas "+
			"  WHERE d.ic_epo = pep.ic_epo"   +
			"    AND d.ic_pyme = pep.ic_pyme"   +
			"    AND d.ic_epo = pep.ic_epo"   +
			"    AND d.ic_producto_nafin = pep.ic_producto_nafin"   +
			"    AND pep.ic_pyme = py.ic_pyme"   +
			"    AND d.ic_producto_nafin = pn.ic_producto_nafin"   +
			"    AND d.ic_moneda = m.ic_moneda"   +
			"    AND d.ic_epo = pe.ic_epo"   +
			"    AND d.ic_producto_nafin = pe.ic_producto_nafin"   +
			"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento"   +
			"    AND d.ic_estatus_docto = ed.ic_estatus_docto"   +
			"    AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm (+)"   +
			"    AND m.ic_moneda = tc.ic_moneda"   +
			"    AND tc.dc_fecha IN (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)"   +
			"    AND pep.cg_tipo_credito = 'D'"   +
			" 	 AND d.ic_clasificacion = clas.ic_clasificacion (+) "+
			"    AND d.ic_epo = "+ic_epo+" "   +
			"    AND d.ic_producto_nafin = 4 "+condicion.toString());
			
		qryCCC.append(
			" SELECT   /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */"   +
			"        py.cg_razon_social AS nombre_dist, d.ig_numero_docto,"   +
			"        d.cc_acuse,"   +
			"        TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision,"   +
			"        TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga,"   +
			"        TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc,"   +
			"        d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto,"   +
			"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', '','P', 'Dolar-Peso','') AS tipo_conversion,"   +
			"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio,"   +
			"        NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento,"   +
			"        NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento,"   +
			"        tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus,"   +
			"        m.ic_moneda, d.ic_documento, lc.ic_moneda AS moneda_linea,clas.cg_descripcion as CATEGORIA, 'ConsInfDocEpoDist::getDocumentSummaryQueryForIds'"   +
			"        ,d.CG_VENTACARTERA as CG_VENTACARTERA, D.IG_TIPO_PAGO "+
			"        ,(SELECT CASE WHEN COUNT(*) > 0 THEN 'S' ELSE 'N' END FROM COM_ARCDOCTOS WHERE CC_ACUSE = d.cc_acuse) AS MUESTRA_VISOR " +
			"   FROM dis_documento d,"   +
			"        comcat_pyme py,"   +
			"        comrel_producto_epo pe,"   +
			"        comrel_pyme_epo_x_producto pep,"   +
			"        com_linea_credito lc,"   +
			"        comcat_moneda m,"   +
			"        comcat_estatus_docto ed,"   +
			"        com_tipo_cambio tc,"   +
			"        comcat_producto_nafin pn,"   +
			"        comcat_tipo_financiamiento tf,comrel_clasificacion clas "+
			"  WHERE d.ic_epo = pep.ic_epo"   +
			"    AND d.ic_pyme = pep.ic_pyme"   +
			"    AND d.ic_epo = pep.ic_epo"   +
			"    AND d.ic_producto_nafin = pep.ic_producto_nafin"   +
			"    AND pep.ic_pyme = py.ic_pyme"   +
			"    AND d.ic_producto_nafin = pn.ic_producto_nafin"   +
			"    AND d.ic_moneda = m.ic_moneda"   +
			"    AND d.ic_epo = pe.ic_epo"   +
			"    AND d.ic_producto_nafin = pe.ic_producto_nafin"   +
			"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento"   +
			"    AND d.ic_estatus_docto = ed.ic_estatus_docto"   +
			"    AND d.ic_linea_credito = lc.ic_linea_credito (+) "   +
			"    AND m.ic_moneda = tc.ic_moneda"   +
			"    AND tc.dc_fecha IN (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)"   +
			"    AND pep.cg_tipo_credito = 'C'"   +
			" 	 AND d.ic_clasificacion = clas.ic_clasificacion (+) "+
			"    AND d.ic_epo = "+ic_epo+" "   +
			"    AND d.ic_producto_nafin = 4 "+condicion.toString());
		
		qryFdR.append(
			" SELECT   /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */"   +
			"        py.cg_razon_social AS nombre_dist, d.ig_numero_docto,"   +
			"        d.cc_acuse,"   +
			"        TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision,"   +
			"        TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga,"   +
			"        TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc,"   +
			"        d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto,"   +
			"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', '','P', 'Dolar-Peso','') AS tipo_conversion,"   +
			"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio,"   +
			"        NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento,"   +
			"        NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento,"   +
			"        'NA' AS modo_plazo, ed.cd_descripcion AS estatus,"   +
			"        m.ic_moneda, d.ic_documento, lc.ic_moneda AS moneda_linea,clas.cg_descripcion as CATEGORIA, 'ConsInfDocEpoDist::getDocumentSummaryQueryForIds'"   +
			"        ,d.CG_VENTACARTERA as CG_VENTACARTERA "+	
			" 			,lc.ig_cuenta_bancaria as CUENTA_BANCARIA, D.IG_TIPO_PAGO "+ 
			"        ,(SELECT CASE WHEN COUNT(*) > 0 THEN 'S' ELSE 'N' END FROM COM_ARCDOCTOS WHERE CC_ACUSE = d.cc_acuse) AS MUESTRA_VISOR " +
			"   FROM dis_documento d,"   +
			"        comcat_pyme py,"   +
			"        comrel_producto_epo pe,"   +
			"        comrel_pyme_epo_x_producto pep,"   +
			"        com_linea_credito lc,"   +
			"        comcat_moneda m,"   +
			"        comcat_estatus_docto ed,"   +
			"        com_tipo_cambio tc,"   +
			"        comcat_producto_nafin pn,"   +
			"        comcat_tipo_financiamiento tf,comrel_clasificacion clas "+
			"  WHERE d.ic_epo = pep.ic_epo"   +
			"    AND d.ic_pyme = pep.ic_pyme"   +
			"    AND d.ic_epo = pep.ic_epo"   +
			"    AND d.ic_producto_nafin = pep.ic_producto_nafin"   +
			"    AND pep.ic_pyme = py.ic_pyme"   +
			"    AND d.ic_producto_nafin = pn.ic_producto_nafin"   +
			"    AND d.ic_moneda = m.ic_moneda"   +
			"    AND d.ic_epo = pe.ic_epo"   +
			"    AND d.ic_producto_nafin = pe.ic_producto_nafin"   +
			"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento(+)"   +
			"    AND d.ic_estatus_docto = ed.ic_estatus_docto"   +
			"    AND d.ic_linea_credito = lc.ic_linea_credito (+) "   +
			"    AND m.ic_moneda = tc.ic_moneda"   +
			"    AND tc.dc_fecha IN (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)"   +
			"    AND pe.cg_tipos_credito = 'F'"   +
			" 	 AND d.ic_clasificacion = clas.ic_clasificacion (+) "+
			"   and d.IC_TIPO_FINANCIAMIENTO is null "+
			"    AND d.ic_epo = "+ic_epo+" "   +			
			"    AND d.ic_producto_nafin = 4 "+condicion.toString());
			
		if("D".equals(tipo_credito))
			qrySentencia.append(qryDM.toString());
		else if("C".equals(tipo_credito))
			qrySentencia.append(qryCCC.toString());
		else if("F".equals(tipo_credito))
			qrySentencia.append(qryFdR.toString());
		else
			qrySentencia.append(qryDM.toString()+ " UNION ALL "+qryCCC.toString());

		System.out.println("el query queda de la siguiente manera "+qrySentencia.toString());
		return qrySentencia.toString();
	}
	
	public String getDocumentQuery(HttpServletRequest request) {

		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
    	StringBuffer qrySentencia	= new StringBuffer();
    	StringBuffer qryDM			= new StringBuffer();
    	StringBuffer qryCCC			= new StringBuffer();
		StringBuffer qryFdR			= new StringBuffer();
    	StringBuffer condicion		= new StringBuffer();
      
		String	ic_epo				=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String	ic_pyme				=	(request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String	ic_if				=	(request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String	ic_estatus_docto	=	(request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto");
		String 	ig_numero_docto		=	(request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String 	fecha_seleccion_de	=	(request.getParameter("fecha_seleccion_de")==null)?"":request.getParameter("fecha_seleccion_de");
		String 	fecha_seleccion_a	=	(request.getParameter("fecha_seleccion_a")==null)?"":request.getParameter("fecha_seleccion_a");
		String 	cc_acuse			=	(request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String 	monto_credito_de	=	(request.getParameter("monto_credito_de")==null)?"":request.getParameter("monto_credito_de");
		String 	monto_credito_a		=	(request.getParameter("monto_credito_a")==null)?"":request.getParameter("monto_credito_a");
		String 	fecha_emision_de	=	(request.getParameter("fecha_emision_de")==null)?"":request.getParameter("fecha_emision_de");
		String 	fecha_emision_a		=	(request.getParameter("fecha_emision_a")==null)?"":request.getParameter("fecha_emision_a");
		String 	fecha_vto_de		=	(request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String 	fecha_vto_a			=	(request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String 	fecha_vto_credito_de=	(request.getParameter("fecha_vto_credito_de")==null)?"":request.getParameter("fecha_vto_credito_de");
		String 	fecha_vto_credito_a =	(request.getParameter("fecha_vto_credito_a")==null)?"":request.getParameter("fecha_vto_credito_a");
		String 	ic_tipo_cobro_int	=	(request.getParameter("ic_tipo_cobro_int")==null)?"":request.getParameter("ic_tipo_cobro_int");
		String	ic_moneda			=	(request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String 	fn_monto_de			=	(request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String 	fn_monto_a			=	(request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String 	monto_con_descuento	=	(request.getParameter("monto_con_descuento")==null)?"":"checked";
		String	solo_cambio			=	(request.getParameter("solo_cambio")==null)?"":request.getParameter("solo_cambio");
		String	modo_plazo			=	(request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
		String	tipo_credito		=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String	ic_documento		=	(request.getParameter("ic_documento")==null)?"":request.getParameter("ic_documento");
		String 	fecha_publicacion_de=	(request.getParameter("fecha_publicacion_de")==null)?fechaHoy:request.getParameter("fecha_publicacion_de");
		String 	fecha_publicacion_a	=	(request.getParameter("fecha_publicacion_a")==null)?fechaHoy:request.getParameter("fecha_publicacion_a");
		String 	NOnegociable			=	(request.getParameter("NOnegociable")==null)?"":request.getParameter("NOnegociable");//Fodea 029-2010 Distribuidores Fase III
		String tipo_pago = (request.getParameter("tipo_pago")==null)?"":request.getParameter("tipo_pago"); // F009-2015

		try {
			if(!"".equals(ic_pyme)&&ic_pyme!=null)
				condicion.append(" AND D.IC_PYME = "+ic_pyme);
			if("2".equals(ic_estatus_docto)||"5".equals(ic_estatus_docto)||"9".equals(ic_estatus_docto)||"1".equals(ic_estatus_docto)||"24".equals(ic_estatus_docto)||"32".equals(ic_estatus_docto)||"28".equals(ic_estatus_docto)||"30".equals(ic_estatus_docto))//MOD +(||"24".equals(ic_estatus_docto))
				condicion.append(" AND D.IC_ESTATUS_DOCTO ="+ic_estatus_docto);
			if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
				condicion.append(" AND D.ig_numero_docto = '"+ig_numero_docto+"'");
			if(!"".equals(cc_acuse)&& cc_acuse!=null)
				condicion.append(" AND D.cc_acuse = '"+cc_acuse+"'");
			if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null)
				condicion.append(" AND trunc(D.df_fecha_emision) BETWEEN TRUNC(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) AND TRUNC(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
				condicion.append(" AND trunc(D.df_fecha_venc) BETWEEN TRUNC(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) AND TRUNC(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null)
				condicion.append(" AND trunc(D.df_fecha_venc_credito) BETWEEN TRUNC(to_date('"+fecha_vto_credito_de+"','dd/mm/yyyy')) AND TRUNC(to_date('"+fecha_vto_credito_a+"','dd/mm/yyyy'))");
			if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null)
				condicion.append(" AND LC.ic_tipo_cobro_interes = "+ic_tipo_cobro_int);
			if(!"".equals(ic_moneda)&&ic_moneda!=null)
				condicion.append(" AND M.ic_moneda = "+ic_moneda);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
				condicion.append(" AND (D.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
				condicion.append(" AND D.fn_monto between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(solo_cambio)&& solo_cambio!=null)
				condicion.append(" AND D.ic_documento in (SELECT ic_documento FROM dis_cambio_estatus)");
			if(!"".equals(modo_plazo)&& modo_plazo!=null)
				condicion.append(" AND TF.ic_tipo_financiamiento = "+modo_plazo);
			if(!"".equals(ic_documento)&&ic_documento!=null)
				condicion.append(" AND D.IC_DOCUMENTO = "+ic_documento);
			if(!"".equals(ic_if)&&ic_if!=null)
				condicion.append(" AND LC.IC_IF = "+ic_if);
			if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
				condicion.append(" AND trunc(D.df_carga) BETWEEN TRUNC(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) AND TRUNC(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))");
			
      if("".equals(ic_estatus_docto) && NOnegociable.equals("N")){ //Fodea 029-2010 Distribuidores Fase III
				 condicion.append(" AND D.ic_estatus_docto IN(2,3,4,5,9,11,20,22,24,32,28,30)");//MOD +(24)
      }else 	if("".equals(ic_estatus_docto) && NOnegociable.equals("S")){
           condicion.append(" AND D.ic_estatus_docto IN(1,2,3,4,5,9,11,20,22,24,32,28,30)");//MOD +(24)
      }
			if(!"".equals(ic_estatus_docto)&&!"2".equals(ic_estatus_docto)&&!"5".equals(ic_estatus_docto)&&!"9".equals(ic_estatus_docto)&& !"1".equals(ic_estatus_docto)&& !"24".equals(ic_estatus_docto)&& !"32".equals(ic_estatus_docto)&& !"28".equals(ic_estatus_docto)&& !"30".equals(ic_estatus_docto))//MOD +(&& !"24".equals(ic_estatus_docto))	
				condicion.append(" AND D.ic_documento IS NULL");
			if(!"".equals(monto_credito_de)&& monto_credito_de!=null&&!"".equals(monto_credito_a)&& monto_credito_a!=null)
				condicion.append(" AND D.ic_documento IS NULL");
			if("F".equals(tipo_credito)) {
				//condicion.append(" AND pep.CG_TIPO_CREDITO IS NULL");  
			} else {
				condicion.append(" AND pep.CG_TIPO_CREDITO IS NOT NULL");
			}
			if(!tipo_pago.equals("")){
				condicion.append(" AND D.IG_TIPO_PAGO = " + tipo_pago);
			}
			qryDM.append(
				" SELECT   /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */"   +
				"        d.ic_documento, 'ConsInfDocEpoDist::getDocumentQuery'"   +
				"   FROM dis_documento d,"   +
				"        comcat_pyme py,"   +
				"        comrel_producto_epo pe,"   +
				"        comrel_pyme_epo_x_producto pep,"   +
				"        dis_linea_credito_dm lc,"   +
				"        comcat_moneda m,"   +
				"        comcat_estatus_docto ed,"   +
				"        com_tipo_cambio tc,"   +
				"        comcat_producto_nafin pn,"   +
				"        comcat_tipo_financiamiento tf"   +
				"  WHERE d.ic_epo = pep.ic_epo"   +
				"    AND d.ic_pyme = pep.ic_pyme"   +
				"    AND d.ic_epo = pep.ic_epo"   +
				"    AND d.ic_producto_nafin = pep.ic_producto_nafin"   +
				"    AND pep.ic_pyme = py.ic_pyme"   +
				"    AND d.ic_producto_nafin = pn.ic_producto_nafin"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND d.ic_producto_nafin = pe.ic_producto_nafin"   +
				"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento"   +
				"    AND d.ic_estatus_docto = ed.ic_estatus_docto"   +
				"    AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm (+)"   +
				"    AND m.ic_moneda = tc.ic_moneda"   +
				"    AND tc.dc_fecha IN (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)"   +
				"    AND pep.cg_tipo_credito = 'D'"   +
				"    AND d.ic_epo = "+ic_epo+" "   +
				"    AND d.ic_producto_nafin = 4 "+condicion.toString());
				
			qryCCC.append(
				" SELECT   /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */"   +
				"        d.ic_documento, 'ConsInfDocEpoDist::getDocumentQuery'"   +
				"   FROM dis_documento d,"   +
				"        comcat_pyme py,"   +
				"        comrel_producto_epo pe,"   +
				"        comrel_pyme_epo_x_producto pep,"   +
				"        com_linea_credito lc,"   +
				"        comcat_moneda m,"   +
				"        comcat_estatus_docto ed,"   +
				"        com_tipo_cambio tc,"   +
				"        comcat_producto_nafin pn,"   +
				"        comcat_tipo_financiamiento tf"   +
				"  WHERE d.ic_epo = pep.ic_epo"   +
				"    AND d.ic_pyme = pep.ic_pyme"   +
				"    AND d.ic_epo = pep.ic_epo"   +
				"    AND d.ic_producto_nafin = pep.ic_producto_nafin"   +
				"    AND pep.ic_pyme = py.ic_pyme"   +
				"    AND d.ic_producto_nafin = pn.ic_producto_nafin"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND d.ic_producto_nafin = pe.ic_producto_nafin"   +
				"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento"   +
				"    AND d.ic_estatus_docto = ed.ic_estatus_docto"   +
				"    AND d.ic_linea_credito = lc.ic_linea_credito (+) "   +
				"    AND m.ic_moneda = tc.ic_moneda"   +
				"    AND tc.dc_fecha IN (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)"   +
				"    AND pep.cg_tipo_credito = 'C'"   +
				"    AND d.ic_epo = "+ic_epo+" "   +
				"    AND d.ic_producto_nafin = 4 "+condicion.toString());
				
			qryFdR.append(
				" SELECT   /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */"   +
				"        d.ic_documento, 'ConsInfDocEpoDist::getDocumentQuery'"   +
				"   FROM dis_documento d,"   +
				"        comcat_pyme py,"   +
				"        comrel_producto_epo pe,"   +
				"        comrel_pyme_epo_x_producto pep,"   +
				"        com_linea_credito lc,"   +
				"        comcat_moneda m,"   +
				"        comcat_estatus_docto ed,"   +
				"        com_tipo_cambio tc,"   +
				"        comcat_producto_nafin pn,"   +
				"        comcat_tipo_financiamiento tf"   +
				"  WHERE d.ic_epo = pep.ic_epo"   +
				"    AND d.ic_pyme = pep.ic_pyme"   +
				"    AND d.ic_epo = pep.ic_epo"   +
				"    AND d.ic_producto_nafin = pep.ic_producto_nafin"   +
				"    AND pep.ic_pyme = py.ic_pyme"   +
				"    AND d.ic_producto_nafin = pn.ic_producto_nafin"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND d.ic_producto_nafin = pe.ic_producto_nafin"   +
				"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento(+)"   +
				"    AND d.ic_estatus_docto = ed.ic_estatus_docto"   +
				"    AND d.ic_linea_credito = lc.ic_linea_credito (+) "   +
				"    AND m.ic_moneda = tc.ic_moneda"   +
				"    AND tc.dc_fecha IN (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)"   +
				"    AND pe.cg_tipos_credito = 'F'"   +
				"   and d.IC_TIPO_FINANCIAMIENTO is null "+				
				"    AND d.ic_epo = "+ic_epo+" "   +
				"    AND d.ic_producto_nafin = 4 "+condicion.toString());
				

			if("D".equals(tipo_credito))
				qrySentencia.append(qryDM.toString());
			else if("C".equals(tipo_credito))
				qrySentencia.append(qryCCC.toString());
			else if("F".equals(tipo_credito))
				qrySentencia.append(qryFdR.toString());
			else
				qrySentencia.append(qryDM.toString()+ " UNION ALL "+qryCCC.toString());

			System.out.println("EL query de la llave primaria----->:"+tipo_credito+ "----"+qrySentencia.toString());   
		}catch(Exception e){
			System.out.println("ConsInfDocEpoDist::getDocumentQueryException "+e);
		}
		return qrySentencia.toString();
	}
	
	public String getDocumentQueryFile(HttpServletRequest request) {

		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
    	StringBuffer qrySentencia	= new StringBuffer();
    	StringBuffer qryDM			= new StringBuffer();
    	StringBuffer qryCCC			= new StringBuffer();
    	StringBuffer condicion		= new StringBuffer();
		StringBuffer qryFdR			= new StringBuffer();

		String	ic_epo				=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String	ic_pyme				=	(request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String	ic_if				=	(request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String	ic_estatus_docto	=	(request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto");
		String 	ig_numero_docto		=	(request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String 	fecha_seleccion_de	=	(request.getParameter("fecha_seleccion_de")==null)?"":request.getParameter("fecha_seleccion_de");
		String 	fecha_seleccion_a	=	(request.getParameter("fecha_seleccion_a")==null)?"":request.getParameter("fecha_seleccion_a");
		String 	cc_acuse			=	(request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String 	monto_credito_de	=	(request.getParameter("monto_credito_de")==null)?"":request.getParameter("monto_credito_de");
		String 	monto_credito_a		=	(request.getParameter("monto_credito_a")==null)?"":request.getParameter("monto_credito_a");
		String 	fecha_emision_de	=	(request.getParameter("fecha_emision_de")==null)?"":request.getParameter("fecha_emision_de");
		String 	fecha_emision_a		=	(request.getParameter("fecha_emision_a")==null)?"":request.getParameter("fecha_emision_a");
		String 	fecha_vto_de		=	(request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String 	fecha_vto_a			=	(request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String 	fecha_vto_credito_de=	(request.getParameter("fecha_vto_credito_de")==null)?"":request.getParameter("fecha_vto_credito_de");
		String 	fecha_vto_credito_a =	(request.getParameter("fecha_vto_credito_a")==null)?"":request.getParameter("fecha_vto_credito_a");
		String 	ic_tipo_cobro_int	=	(request.getParameter("ic_tipo_cobro_int")==null)?"":request.getParameter("ic_tipo_cobro_int");
		String	ic_moneda			=	(request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String 	fn_monto_de			=	(request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String 	fn_monto_a			=	(request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String 	monto_con_descuento	=	(request.getParameter("monto_con_descuento")==null)?"":"checked";
		String	solo_cambio			=	(request.getParameter("solo_cambio")==null)?"":request.getParameter("solo_cambio");
		String	modo_plazo			=	(request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
		String	tipo_credito		=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String	ic_documento		=	(request.getParameter("ic_documento")==null)?"":request.getParameter("ic_documento");
		String 	fecha_publicacion_de=	(request.getParameter("fecha_publicacion_de")==null)?fechaHoy:request.getParameter("fecha_publicacion_de");
		String 	fecha_publicacion_a	=	(request.getParameter("fecha_publicacion_a")==null)?fechaHoy:request.getParameter("fecha_publicacion_a");
		String 	NOnegociable			=	(request.getParameter("NOnegociable")==null)?"":request.getParameter("NOnegociable");//Fodea 029-2010 Distribuidores Fase III
		String tipo_pago = (request.getParameter("tipo_pago")==null)?"":request.getParameter("tipo_pago"); // F009-2015
		try {
			if(!"".equals(ic_pyme)&&ic_pyme!=null)
				condicion.append(" AND D.IC_PYME = "+ic_pyme);
			if("2".equals(ic_estatus_docto)||"5".equals(ic_estatus_docto)||"9".equals(ic_estatus_docto)||"1".equals(ic_estatus_docto)||"24".equals(ic_estatus_docto)||"32".equals(ic_estatus_docto)||"28".equals(ic_estatus_docto)||"30".equals(ic_estatus_docto))//MOD +(||"24".equals(ic_estatus_docto))
				condicion.append(" AND D.IC_ESTATUS_DOCTO ="+ic_estatus_docto);
			if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
				condicion.append(" and D.ig_numero_docto = '"+ig_numero_docto+"'");
			if(!"".equals(cc_acuse)&& cc_acuse!=null)
				condicion.append(" and D.cc_acuse = '"+cc_acuse+"'");
			if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null)
				condicion.append(" and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date('"+fecha_vto_credito_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_credito_a+"','dd/mm/yyyy'))");
			if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null)
				condicion.append(" AND LC.ic_tipo_cobro_interes = "+ic_tipo_cobro_int);
			if(!"".equals(ic_moneda)&&ic_moneda!=null)
				condicion.append(" and M.ic_moneda = "+ic_moneda);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
				condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
				condicion.append(" and D.fn_monto between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(solo_cambio)&& solo_cambio!=null)
				condicion.append(" and D.ic_documento in (select ic_documento from dis_cambio_estatus)");
			if(!"".equals(modo_plazo)&& modo_plazo!=null)
				condicion.append(" and TF.ic_tipo_financiamiento = "+modo_plazo);
			if(!"".equals(ic_documento)&&ic_documento!=null)
				condicion.append(" AND D.IC_DOCUMENTO = "+ic_documento);
			if(!"".equals(ic_if)&&ic_if!=null)
				condicion.append(" AND LC.IC_IF = "+ic_if);
			if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
				condicion.append(" and trunc(D.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))");
		
      if("".equals(ic_estatus_docto) && NOnegociable.equals("N")){ //Fodea 029-2010 Distribuidores Fase III
				 condicion.append(" and D.ic_estatus_docto in(2,3,4,5,9,11,20,22,24,32,28,30)");//MOD +(24)
      }else 	if("".equals(ic_estatus_docto) && NOnegociable.equals("S")){
      	 condicion.append(" and D.ic_estatus_docto in(1,2,3,4,5,9,11,20,22,24,32,28,30)");//MOD +(24)
      }
    
			if(!"".equals(ic_estatus_docto)&&!"2".equals(ic_estatus_docto)&&!"5".equals(ic_estatus_docto)&&!"9".equals(ic_estatus_docto) && !"1".equals(ic_estatus_docto) && !"24".equals(ic_estatus_docto) && !"32".equals(ic_estatus_docto)&& !"28".equals(ic_estatus_docto)&& !"30".equals(ic_estatus_docto))//mod +( && !"24".equals(ic_estatus_docto))	
				condicion.append(" and D.ic_documento is null");
			if(!"".equals(monto_credito_de)&& monto_credito_de!=null&&!"".equals(monto_credito_a)&& monto_credito_a!=null)
				condicion.append(" and D.ic_documento is null");

			if("F".equals(tipo_credito)) {
				//condicion.append(" AND pep.CG_TIPO_CREDITO IS NULL");
			} else {
				condicion.append(" AND pep.CG_TIPO_CREDITO IS NOT NULL");
			}
			if(!tipo_pago.equals("")){
				condicion.append(" AND D.IG_TIPO_PAGO = " + tipo_pago);
			}
			qryDM.append(
				" SELECT   /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */"   +
				"        py.cg_razon_social AS nombre_dist, d.ig_numero_docto,"   +
				"        d.cc_acuse,"   +
				"        TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision,"   +
				"        TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga,"   +
				"        TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc,"   +
				"        d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto,"   +
				"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', '','P', 'Dolar-Peso','') AS tipo_conversion,"   +
				"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio,"   +
				"        NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento,"   +
				"        NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento,"   +
				"        tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus,"   +
				"        m.ic_moneda, d.ic_documento, lc.ic_moneda AS moneda_linea,clas.cg_descripcion as CATEGORIA, 'getDocumentQueryFile::getDocumentSummaryQueryForIds'"   +
			  "        ,d.CG_VENTACARTERA as CG_VENTACARTERA, D.IG_TIPO_PAGO "+
				"   FROM dis_documento d,"   +
				"        comcat_pyme py,"   +
				"        comrel_producto_epo pe,"   +
				"        comrel_pyme_epo_x_producto pep,"   +
				"        dis_linea_credito_dm lc,"   +
				"        comcat_moneda m,"   +
				"        comcat_estatus_docto ed,"   +
				"        com_tipo_cambio tc,"   +
				"        comcat_producto_nafin pn,"   +
				"        comcat_tipo_financiamiento tf,comrel_clasificacion clas "+
				"  WHERE d.ic_epo = pep.ic_epo"   +
				"    AND d.ic_pyme = pep.ic_pyme"   +
				"    AND d.ic_epo = pep.ic_epo"   +
				"    AND d.ic_producto_nafin = pep.ic_producto_nafin"   +
				"    AND pep.ic_pyme = py.ic_pyme"   +
				"    AND d.ic_producto_nafin = pn.ic_producto_nafin"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND d.ic_producto_nafin = pe.ic_producto_nafin"   +
				"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento"   +
				"    AND d.ic_estatus_docto = ed.ic_estatus_docto"   +
				"    AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm (+)"   +
				"    AND m.ic_moneda = tc.ic_moneda"   +
				"    AND tc.dc_fecha IN (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)"   +
				"    AND pep.cg_tipo_credito = 'D'"   +
				" 	 AND d.ic_clasificacion = clas.ic_clasificacion (+) "+
				"    AND d.ic_epo = "+ic_epo+" "   +
				"    AND d.ic_producto_nafin = 4 "+condicion.toString());
				
			qryCCC.append(
				" SELECT   /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */"   +
				"        py.cg_razon_social AS nombre_dist, d.ig_numero_docto,"   +
				"        d.cc_acuse,"   +
				"        TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision,"   +
				"        TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga,"   +
				"        TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc,"   +
				"        d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto,"   +
				"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', '','P', 'Dolar-Peso','') AS tipo_conversion,"   +
				"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio,"   +
				"        NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento,"   +
				"        NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento,"   +
				"        tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus,"   +
				"        m.ic_moneda, d.ic_documento, lc.ic_moneda AS moneda_linea,clas.cg_descripcion as CATEGORIA, 'getDocumentQueryFile::getDocumentSummaryQueryForIds'"   +
				"        ,d.CG_VENTACARTERA as CG_VENTACARTERA, D.IG_TIPO_PAGO "+
				"   FROM dis_documento d,"   +
				"        comcat_pyme py,"   +
				"        comrel_producto_epo pe,"   +
				"        comrel_pyme_epo_x_producto pep,"   +
				"        com_linea_credito lc,"   +
				"        comcat_moneda m,"   +
				"        comcat_estatus_docto ed,"   +
				"        com_tipo_cambio tc,"   +
				"        comcat_producto_nafin pn,"   +
				"        comcat_tipo_financiamiento tf,comrel_clasificacion clas "+
				"  WHERE d.ic_epo = pep.ic_epo"   +
				"    AND d.ic_pyme = pep.ic_pyme"   +
				"    AND d.ic_epo = pep.ic_epo"   +
				"    AND d.ic_producto_nafin = pep.ic_producto_nafin"   +
				"    AND pep.ic_pyme = py.ic_pyme"   +
				"    AND d.ic_producto_nafin = pn.ic_producto_nafin"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND d.ic_producto_nafin = pe.ic_producto_nafin"   +
				"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento"   +
				"    AND d.ic_estatus_docto = ed.ic_estatus_docto"   +
				"    AND d.ic_linea_credito = lc.ic_linea_credito (+) "   +
				"    AND m.ic_moneda = tc.ic_moneda"   +
				"    AND tc.dc_fecha IN (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)"   +
				"    AND pep.cg_tipo_credito = 'C'"   +
				" 	 AND d.ic_clasificacion = clas.ic_clasificacion (+) "+
				"    AND d.ic_epo = "+ic_epo+" "   +
				"    AND d.ic_producto_nafin = 4 "+condicion.toString());
				
			qryFdR.append(
				" SELECT   /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */"   +
				"        py.cg_razon_social AS nombre_dist, d.ig_numero_docto,"   +
				"        d.cc_acuse,"   +
				"        TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision,"   +
				"        TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga,"   +
				"        TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc,"   +
				"        d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto,"   +
				"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', '','P', 'Dolar-Peso','') AS tipo_conversion,"   +
				"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio,"   +
				"        NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento,"   +
				"        NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento,"   +
				"        'NA' AS modo_plazo, ed.cd_descripcion AS estatus,"   +
				"        m.ic_moneda, d.ic_documento, lc.ic_moneda AS moneda_linea,clas.cg_descripcion as CATEGORIA, 'getDocumentQueryFile::getDocumentSummaryQueryForIds'"   +
				"        ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				" 			,lc.ig_cuenta_bancaria as CUENTA_BANCARIA, D.IG_TIPO_PAGO "+ 
				"   FROM dis_documento d,"   +
				"        comcat_pyme py,"   +
				"        comrel_producto_epo pe,"   +
				"        comrel_pyme_epo_x_producto pep,"   +
				"        com_linea_credito lc,"   +
				"        comcat_moneda m,"   +
				"        comcat_estatus_docto ed,"   +
				"        com_tipo_cambio tc,"   +
				"        comcat_producto_nafin pn,"   +
				"        comcat_tipo_financiamiento tf,comrel_clasificacion clas "+
				"  WHERE d.ic_epo = pep.ic_epo"   +
				"    AND d.ic_pyme = pep.ic_pyme"   +
				"    AND d.ic_epo = pep.ic_epo"   +
				"    AND d.ic_producto_nafin = pep.ic_producto_nafin"   +
				"    AND pep.ic_pyme = py.ic_pyme"   +
				"    AND d.ic_producto_nafin = pn.ic_producto_nafin"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND d.ic_producto_nafin = pe.ic_producto_nafin"   +
				"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento(+)"   +
				"    AND d.ic_estatus_docto = ed.ic_estatus_docto"   +
				"    AND d.ic_linea_credito = lc.ic_linea_credito (+) "   +
				"    AND m.ic_moneda = tc.ic_moneda"   +
				"    AND tc.dc_fecha IN (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)"   +
				"    AND pe.cg_tipos_credito = 'F'"   +
				" 	 AND d.ic_clasificacion = clas.ic_clasificacion (+) "+
				"   and d.IC_TIPO_FINANCIAMIENTO is null "+
				"    AND d.ic_epo = "+ic_epo+" "   +				
				"    AND d.ic_producto_nafin = 4 "+condicion.toString());

			if("D".equals(tipo_credito))
				qrySentencia.append(qryDM.toString());
			else if("C".equals(tipo_credito))
				qrySentencia.append(qryCCC.toString());
			else if("F".equals(tipo_credito))
				qrySentencia.append(qryFdR.toString());
			else
				qrySentencia.append(qryDM.toString()+ " UNION ALL "+qryCCC.toString());
				
			System.out.println("EL query queda as� : "+qrySentencia.toString());
		}catch(Exception e){
			System.out.println("CambioEstatusEpoDE::getDocumentQueryFileException "+e);
		}
		return qrySentencia.toString();
	}


/*******************************************************************************
 *          Migraci�n. Implementa IQueryGeneratorRegExtJS.java                 *
 *******************************************************************************/

private List conditions;
private String ic_epo;
private String ic_pyme;
private String ic_if;
private String ic_estatus_docto;
private String ig_numero_docto;
private String fecha_seleccion_de;
private String fecha_seleccion_a;
private String cc_acuse;
private String monto_credito_de;
private String monto_credito_a;
private String fecha_emision_de;
private String fecha_emision_a;
private String fecha_vto_de;
private String fecha_vto_a;
private String fecha_vto_credito_de;
private String fecha_vto_credito_a;
private String ic_tipo_cobro_int;
private String ic_moneda;
private String fn_monto_de;
private String fn_monto_a;
private String monto_con_descuento;
private String solo_cambio;
private String modo_plazo;
private String tipo_credito;
private String ic_documento;
private String fecha_publicacion_de;
private String fecha_publicacion_a;
private String NOnegociable;
private String tipo_pago;
private String publicaDoctosFinanciables;
private static final Log log = ServiceLocator.getInstance().getLog(ConsInfDocEpoDist.class);

	public String getDocumentQuery(){
		return null;
	}

	public String getDocumentSummaryQueryForIds(List ids){
		return null;
	}

	public String getAggregateCalculationQuery(){
		return null;
	}

/**
 * Se crea el query para generar el archivo CSV y PDF sin utilizar paginaci�n
 * @return cadena con la consulta.
 */
	public String getDocumentQueryFile(){

		log.info("getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer qryDM  = new StringBuffer();
		StringBuffer qryCCC = new StringBuffer();
		StringBuffer qryFdR = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();

		if(!"".equals(ic_pyme)&&ic_pyme!=null){
			condicion.append(" AND D.IC_PYME = ?");
			conditions.add(ic_pyme);
		}
		if("2".equals(ic_estatus_docto)||"5".equals(ic_estatus_docto)||"9".equals(ic_estatus_docto)||"1".equals(ic_estatus_docto)||"24".equals(ic_estatus_docto)||"32".equals(ic_estatus_docto)||"28".equals(ic_estatus_docto)||"30".equals(ic_estatus_docto)){//MOD +(||"24".equals(ic_estatus_docto))}
			condicion.append(" AND D.IC_ESTATUS_DOCTO = ?");
			conditions.add(ic_estatus_docto);
		}
		if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null){
			condicion.append(" AND D.ig_numero_docto = ?");
			conditions.add(ig_numero_docto);
		}
		if(!"".equals(cc_acuse)&& cc_acuse!=null){
			condicion.append(" AND D.cc_acuse = ?");
			conditions.add(cc_acuse);
		}
		if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null){
			condicion.append(" AND trunc(D.df_fecha_emision) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy')) ");
			conditions.add(fecha_emision_de);
			conditions.add(fecha_emision_a);
		}
		if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null){
			condicion.append(" AND trunc(D.df_fecha_venc) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy')) ");
			conditions.add(fecha_vto_de);
			conditions.add(fecha_vto_a);
		}
		if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null){
			condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy')) ");
			conditions.add(fecha_vto_credito_de);
			conditions.add(fecha_vto_credito_a);
		}
		if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null){
			condicion.append(" AND LC.ic_tipo_cobro_interes = ?");
			conditions.add(ic_tipo_cobro_int);
		}
		if(!"".equals(ic_moneda)&&ic_moneda!=null){
			condicion.append(" AND M.ic_moneda = ?");
			conditions.add(ic_moneda);
		}
		if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento)){
			condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between ? and ? ");
			conditions.add(fn_monto_de);
			conditions.add(fn_monto_a);
		}
		if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento)){
			condicion.append(" and D.fn_monto between ? and ? ");
			conditions.add(fn_monto_de);
			conditions.add(fn_monto_a);
		}
		if(!"".equals(solo_cambio)&& solo_cambio!=null){
			condicion.append(" and D.ic_documento in (select ic_documento from dis_cambio_estatus) ");
		}
		if(!"".equals(modo_plazo)&& modo_plazo!=null){
			condicion.append(" AND TF.ic_tipo_financiamiento = ?");
			conditions.add(modo_plazo);
		}
		if(!"".equals(ic_documento)&&ic_documento!=null){
			condicion.append(" AND D.IC_DOCUMENTO = ?");
			conditions.add(ic_documento);
		}
		if(!"".equals(ic_if)&&ic_if!=null){
			condicion.append(" AND LC.IC_IF = ?");
			conditions.add(ic_if);
		}
		if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null){
			condicion.append(" and trunc(D.df_carga) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy')) ");
			conditions.add(fecha_publicacion_de);
			conditions.add(fecha_publicacion_a);
		}
		if("".equals(ic_estatus_docto) && NOnegociable.equals("N")){
			condicion.append(" and D.ic_estatus_docto in(2,3,4,5,9,11,20,22,24,32,28,30) ");
		} else if("".equals(ic_estatus_docto) && NOnegociable.equals("S")){
			condicion.append(" and D.ic_estatus_docto in(1,2,3,4,5,9,11,20,22,24,32,28,30)");
		}
		if(!"".equals(ic_estatus_docto)&&!"2".equals(ic_estatus_docto)&&!"5".equals(ic_estatus_docto)&&!"9".equals(ic_estatus_docto) && !"1".equals(ic_estatus_docto) && !"24".equals(ic_estatus_docto) && !"32".equals(ic_estatus_docto)&& !"28".equals(ic_estatus_docto)&& !"30".equals(ic_estatus_docto)){
			condicion.append(" and D.ic_documento is null");
		}
		if(!"".equals(monto_credito_de)&& monto_credito_de!=null&&!"".equals(monto_credito_a)&& monto_credito_a!=null){
			condicion.append(" and D.ic_documento is null");
		}
		if("F".equals(tipo_credito)){
			//condicion.append(" AND pep.CG_TIPO_CREDITO IS NULL");
		} else{
			condicion.append(" AND pep.CG_TIPO_CREDITO IS NOT NULL");
		}
		if(!"".equals(tipo_pago)){
			condicion.append(" AND D.IG_TIPO_PAGO = ?");
			conditions.add(tipo_pago);
		}

		qryDM.append(" SELECT /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */");
		qryDM.append("        py.cg_razon_social AS nombre_dist, d.ig_numero_docto,");
		qryDM.append("        d.cc_acuse,");
		qryDM.append("        TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision,");
		qryDM.append("        TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga,");
		qryDM.append("        TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc,");
		qryDM.append("        d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto,");
		qryDM.append("        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', '','P', 'Dolar-Peso','') AS tipo_conversion,");
		qryDM.append("        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio,");
		qryDM.append("        NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento,");
		qryDM.append("        NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento,");
		qryDM.append("        tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus,");
		qryDM.append("        m.ic_moneda, d.ic_documento, lc.ic_moneda AS moneda_linea,clas.cg_descripcion as CATEGORIA, 'getDocumentQueryFile::getDocumentSummaryQueryForIds'");
		qryDM.append("        ,d.CG_VENTACARTERA as CG_VENTACARTERA, D.IG_TIPO_PAGO ");
		qryDM.append("   FROM dis_documento d,");
		qryDM.append("        comcat_pyme py,");
		qryDM.append("        comrel_producto_epo pe,");
		qryDM.append("        comrel_pyme_epo_x_producto pep,");
		qryDM.append("        dis_linea_credito_dm lc,");
		qryDM.append("        comcat_moneda m,");
		qryDM.append("        comcat_estatus_docto ed,");
		qryDM.append("        com_tipo_cambio tc,");
		qryDM.append("        comcat_producto_nafin pn,");
		qryDM.append("        comcat_tipo_financiamiento tf,comrel_clasificacion clas ");
		qryDM.append("  WHERE d.ic_epo = pep.ic_epo");
		qryDM.append("    AND d.ic_pyme = pep.ic_pyme");
		qryDM.append("    AND d.ic_epo = pep.ic_epo");
		qryDM.append("    AND d.ic_producto_nafin = pep.ic_producto_nafin");
		qryDM.append("    AND pep.ic_pyme = py.ic_pyme");
		qryDM.append("    AND d.ic_producto_nafin = pn.ic_producto_nafin");
		qryDM.append("    AND d.ic_moneda = m.ic_moneda");
		qryDM.append("    AND d.ic_epo = pe.ic_epo");
		qryDM.append("    AND d.ic_producto_nafin = pe.ic_producto_nafin");
		qryDM.append("    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento");
		qryDM.append("    AND d.ic_estatus_docto = ed.ic_estatus_docto");
		qryDM.append("    AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm (+)");
		qryDM.append("    AND m.ic_moneda = tc.ic_moneda");
		qryDM.append("    AND tc.dc_fecha IN (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)");
		qryDM.append("    AND pep.cg_tipo_credito = 'D'");
		qryDM.append("    AND d.ic_clasificacion = clas.ic_clasificacion (+) ");
		qryDM.append("    AND d.ic_epo = "+ic_epo+" ");
		qryDM.append("    AND d.ic_producto_nafin = 4 "+condicion.toString());

		qryCCC.append(" SELECT /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */");
		qryCCC.append("        py.cg_razon_social AS nombre_dist, d.ig_numero_docto,");
		qryCCC.append("        d.cc_acuse,");
		qryCCC.append("        TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision,");
		qryCCC.append("        TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga,");
		qryCCC.append("        TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc,");
		qryCCC.append("        d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto,");
		qryCCC.append("        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', '','P', 'Dolar-Peso','') AS tipo_conversion,");
		qryCCC.append("        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio,");
		qryCCC.append("        NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento,");
		qryCCC.append("        NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento,");
		qryCCC.append("        tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus,");
		qryCCC.append("        m.ic_moneda, d.ic_documento, lc.ic_moneda AS moneda_linea,clas.cg_descripcion as CATEGORIA, 'getDocumentQueryFile::getDocumentSummaryQueryForIds'");
		qryCCC.append("        ,d.CG_VENTACARTERA as CG_VENTACARTERA, D.IG_TIPO_PAGO ");
		qryCCC.append("   FROM dis_documento d,");
		qryCCC.append("        comcat_pyme py,");
		qryCCC.append("        comrel_producto_epo pe,");
		qryCCC.append("        comrel_pyme_epo_x_producto pep,");
		qryCCC.append("        com_linea_credito lc,");
		qryCCC.append("        comcat_moneda m,");
		qryCCC.append("        comcat_estatus_docto ed,");
		qryCCC.append("        com_tipo_cambio tc,");
		qryCCC.append("        comcat_producto_nafin pn,");
		qryCCC.append("        comcat_tipo_financiamiento tf,comrel_clasificacion clas ");
		qryCCC.append("  WHERE d.ic_epo = pep.ic_epo");
		qryCCC.append("    AND d.ic_pyme = pep.ic_pyme");
		qryCCC.append("    AND d.ic_epo = pep.ic_epo");
		qryCCC.append("    AND d.ic_producto_nafin = pep.ic_producto_nafin");
		qryCCC.append("    AND pep.ic_pyme = py.ic_pyme");
		qryCCC.append("    AND d.ic_producto_nafin = pn.ic_producto_nafin");
		qryCCC.append("    AND d.ic_moneda = m.ic_moneda");
		qryCCC.append("    AND d.ic_epo = pe.ic_epo");
		qryCCC.append("    AND d.ic_producto_nafin = pe.ic_producto_nafin");
		qryCCC.append("    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento");
		qryCCC.append("    AND d.ic_estatus_docto = ed.ic_estatus_docto");
		qryCCC.append("    AND d.ic_linea_credito = lc.ic_linea_credito (+) ");
		qryCCC.append("    AND m.ic_moneda = tc.ic_moneda");
		qryCCC.append("    AND tc.dc_fecha IN (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)");
		qryCCC.append("    AND pep.cg_tipo_credito = 'C'");
		qryCCC.append("    AND d.ic_clasificacion = clas.ic_clasificacion (+) ");
		qryCCC.append("    AND d.ic_epo = "+ ic_epo );
		qryCCC.append("    AND d.ic_producto_nafin = 4 " + condicion.toString());

		qryFdR.append(" SELECT   /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */");
		qryFdR.append("        py.cg_razon_social AS nombre_dist, d.ig_numero_docto,");
		qryFdR.append("        d.cc_acuse,");
		qryFdR.append("        TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision,");
		qryFdR.append("        TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga,");
		qryFdR.append("        TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc,");
		qryFdR.append("        d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto,");
		qryFdR.append("        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', '','P', 'Dolar-Peso','') AS tipo_conversion,");
		qryFdR.append("        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio,");
		qryFdR.append("        NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento,");
		qryFdR.append("        NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento,");
		qryFdR.append("        'NA' AS modo_plazo, ed.cd_descripcion AS estatus,");
		qryFdR.append("        m.ic_moneda, d.ic_documento, lc.ic_moneda AS moneda_linea,clas.cg_descripcion as CATEGORIA, 'getDocumentQueryFile::getDocumentSummaryQueryForIds'");
		qryFdR.append("        ,d.CG_VENTACARTERA as CG_VENTACARTERA ");
		qryFdR.append("        ,lc.ig_cuenta_bancaria as CUENTA_BANCARIA, D.IG_TIPO_PAGO "); 
		qryFdR.append("   FROM dis_documento d,");
		qryFdR.append("        comcat_pyme py,");
		qryFdR.append("        comrel_producto_epo pe,");
		qryFdR.append("        comrel_pyme_epo_x_producto pep,");
		qryFdR.append("        com_linea_credito lc,");
		qryFdR.append("        comcat_moneda m,");
		qryFdR.append("        comcat_estatus_docto ed,");
		qryFdR.append("        com_tipo_cambio tc,");
		qryFdR.append("        comcat_producto_nafin pn,");
		qryFdR.append("        comcat_tipo_financiamiento tf,comrel_clasificacion clas ");
		qryFdR.append("  WHERE d.ic_epo = pep.ic_epo");
		qryFdR.append("    AND d.ic_pyme = pep.ic_pyme");
		qryFdR.append("    AND d.ic_epo = pep.ic_epo");
		qryFdR.append("    AND d.ic_producto_nafin = pep.ic_producto_nafin");
		qryFdR.append("    AND pep.ic_pyme = py.ic_pyme");
		qryFdR.append("    AND d.ic_producto_nafin = pn.ic_producto_nafin");
		qryFdR.append("    AND d.ic_moneda = m.ic_moneda");
		qryFdR.append("    AND d.ic_epo = pe.ic_epo");
		qryFdR.append("    AND d.ic_producto_nafin = pe.ic_producto_nafin");
		qryFdR.append("    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento(+)");
		qryFdR.append("    AND d.ic_estatus_docto = ed.ic_estatus_docto");
		qryFdR.append("    AND d.ic_linea_credito = lc.ic_linea_credito (+) ");
		qryFdR.append("    AND m.ic_moneda = tc.ic_moneda");
		qryFdR.append("    AND tc.dc_fecha IN (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)");
		qryFdR.append("    AND pe.cg_tipos_credito = 'F'");
		qryFdR.append("    AND d.ic_clasificacion = clas.ic_clasificacion (+) ");
		qryFdR.append("    AND d.IC_TIPO_FINANCIAMIENTO is null ");
		qryFdR.append("    AND d.ic_epo = " + ic_epo );
		qryFdR.append("    AND d.ic_producto_nafin = 4 " + condicion.toString());

		if("D".equals(tipo_credito))
			qrySentencia.append(qryDM.toString());
		else if("C".equals(tipo_credito))
			qrySentencia.append(qryCCC.toString());
		else if("F".equals(tipo_credito))
			qrySentencia.append(qryFdR.toString());
		else
			qrySentencia.append(qryDM.toString()+ " UNION ALL " + qryCCC.toString());

		log.info("qrySentencia: " + qrySentencia.toString());
		log.info("conditions: " + conditions.toString());
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();

	}

/**
 * Genera el archivo CSV o PDF seg�n el tipo que se le indique.
 * En el caso del PDF, el archivo generado no comtempla paginaci�n, imprime todos los registros que trae la consulta.
 * @param request
 * @param rs
 * @param path
 * @param tipo: CSV o PDF
 * @return nombre del archivo
 */
	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo){

		log.info("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();

		String dist               = "";
		String numDocto           = "";
		String numAcuseCarga      = "";
		String fechaEmision       = "";
		String fechaPublicacion   = "";
		String fechaVencimiento   = "";
		String plazoDocto         = "";
		String moneda             = "";
		String icMoneda           = "";
		double monto              = 0;
		String tipoConversion     = "";
		double tipoCambio         = 0;
		double montoValuado       = 0;
		String categoria          = "";
		String plazoDescuento     = "";
		String porcDescuento      = "";
		double montoDescontar     = 0;
		String modoPlazo          = "";
		String estatus            = "";
		String icDocumento        = "";
		String monedaLinea        = "";
		String bandeVentaCartera  = "";
		String cuenta_Bancaria    = "";
		String tipoPago           = "";
		int nRow                  = 0;
		int totalRegistrosMN      = 0;
		int totalRegistrosDL      = 0;
		double totalMontoMN       = 0;
		double totalMontoDL       = 0;


		if(tipo.equals("PDF")){

			try {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

				while (rs.next()){
					dist              = (rs.getString("NOMBRE_DIST")        ==null)?"" :rs.getString("NOMBRE_DIST");
					numDocto          = (rs.getString("IG_NUMERO_DOCTO")    ==null)?"" :rs.getString("IG_NUMERO_DOCTO");
					numAcuseCarga     = (rs.getString("CC_ACUSE")           ==null)?"" :rs.getString("CC_ACUSE");
					fechaEmision      = (rs.getString("DF_FECHA_EMISION")   ==null)?"" :rs.getString("DF_FECHA_EMISION");
					fechaPublicacion  = (rs.getString("DF_CARGA")           ==null)?"" :rs.getString("DF_CARGA");
					fechaVencimiento  = (rs.getString("DF_FECHA_VENC")      ==null)?"" :rs.getString("DF_FECHA_VENC");
					plazoDocto        = (rs.getString("IG_PLAZO_DOCTO")     ==null)?"" :rs.getString("IG_PLAZO_DOCTO");
					moneda            = (rs.getString("MONEDA")             ==null)?"" :rs.getString("MONEDA");
					tipoConversion    = (rs.getString("TIPO_CONVERSION")    ==null)?"" :rs.getString("TIPO_CONVERSION");
					plazoDescuento    = (rs.getString("IG_PLAZO_DESCUENTO") ==null)?"" :rs.getString("IG_PLAZO_DESCUENTO");
					porcDescuento     = (rs.getString("FN_PORC_DESCUENTO")  ==null)?"0":rs.getString("FN_PORC_DESCUENTO");
					modoPlazo         = (rs.getString("MODO_PLAZO")         ==null)?"" :rs.getString("MODO_PLAZO");
					estatus           = (rs.getString("ESTATUS")            ==null)?"" :rs.getString("ESTATUS");
					icMoneda          = (rs.getString("IC_MONEDA")          ==null)?"" :rs.getString("IC_MONEDA");
					icDocumento       = (rs.getString("IC_DOCUMENTO")       ==null)?"" :rs.getString("IC_DOCUMENTO");
					monedaLinea       = (rs.getString("MONEDA_LINEA")       ==null)?"" :rs.getString("MONEDA_LINEA");
					categoria         = (rs.getString("CATEGORIA")          ==null)?"" :rs.getString("CATEGORIA");
					bandeVentaCartera = (rs.getString("CG_VENTACARTERA")    ==null)?"" :rs.getString("CG_VENTACARTERA");
					tipoPago          = (rs.getString("IG_TIPO_PAGO")       ==null)?"" :rs.getString("IG_TIPO_PAGO");
					tipoCambio        = Double.parseDouble(rs.getString("TIPO_CAMBIO"));
					monto             = Double.parseDouble(rs.getString("FN_MONTO"));
					montoDescontar    = monto*Double.parseDouble(porcDescuento)/100;
					montoValuado      = (monto-montoDescontar)*tipoCambio;

					if(tipo_credito.equals("C")){
						if(tipoPago.equals("1")){
							tipoPago = "Financiamiento con intereses";
						} else if(tipoPago.equals("2")){
							tipoPago = "Meses sin intereses";
						}
					} else{
						tipoPago = "N/A";
					}
					if (bandeVentaCartera.equals("S")){
						modoPlazo ="";
					}
					if (tipo_credito.equals("F")){
						cuenta_Bancaria = (rs.getString("CUENTA_BANCARIA")==null)?"":rs.getString("CUENTA_BANCARIA");
					}
					if(nRow == 0){
						int numCols = (!"".equals(tipoConversion)?18:15);
						if(tipo_credito.equals("F")){numCols=numCols+1;}
						if(publicaDoctosFinanciables.equals("S")){numCols++;}
						pdfDoc.setLTable(numCols,100);
						pdfDoc.setLCell("Datos del Documento Inicial","celda01",ComunesPDF.CENTER,numCols);
						pdfDoc.setLCell("Distribuidor","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("No. Docto Inicial","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("No. Acuse Carga","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha Emisi�n","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha Pub.","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha Venc.","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Plazo Docto","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Categor�a","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Plazo Descuento D�as","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("%   Descuento","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto % Descuento","celda01",ComunesPDF.CENTER);
						if(!"".equals(tipoConversion)){
							pdfDoc.setLCell("Tipo Conv.","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Tipo Cambio","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Monto Valuado","celda01",ComunesPDF.CENTER);
						}
						pdfDoc.setLCell("Modalidad de Plazo","celda01",ComunesPDF.CENTER);
						if(publicaDoctosFinanciables.equals("S")){
							pdfDoc.setLCell("Tipo de pago","celda01",ComunesPDF.CENTER);
						}
						pdfDoc.setLCell("Estatus","celda01",ComunesPDF.CENTER);

						if (tipo_credito.equals("F") ) {
							pdfDoc.setLCell("Cuenta Bancaria Pyme","celda01",ComunesPDF.CENTER);
						}
						pdfDoc.setLHeaders();
						nRow++;
					}

					pdfDoc.setLCell(dist.replace(',',' '),                        "formas", ComunesPDF.LEFT);
					pdfDoc.setLCell(numDocto,                                     "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(numAcuseCarga,                                "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaEmision,                                 "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaPublicacion,                             "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaVencimiento,                             "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(plazoDocto,                                   "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(moneda,                                       "formas", ComunesPDF.LEFT);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(monto,2),          "formas", ComunesPDF.RIGHT);
					pdfDoc.setLCell(categoria,                                    "formas", ComunesPDF.LEFT);
					pdfDoc.setLCell(plazoDescuento,                               "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell(porcDescuento+"%",                            "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoDescontar,2), "formas", ComunesPDF.RIGHT);

					if(!"".equals(tipoConversion)){
						pdfDoc.setLCell(((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?tipoConversion:""),"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell("$"+((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?Comunes.formatoDecimal(tipoCambio,2,false):""),   "formas", ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?Comunes.formatoDecimal(montoValuado,2,false):""), "formas", ComunesPDF.RIGHT);
					}
					pdfDoc.setLCell(modoPlazo, "formas", ComunesPDF.LEFT);
					if(publicaDoctosFinanciables.equals("S")){
						pdfDoc.setLCell(tipoPago, "formas", ComunesPDF.LEFT);
					}
					pdfDoc.setLCell(estatus,   "formas", ComunesPDF.LEFT);
					if (tipo_credito.equals("F")){
						pdfDoc.setLCell(cuenta_Bancaria, "formas", ComunesPDF.CENTER);
					}
					/***** Obtengo los totales *****/
					if(moneda.equals("MONEDA NACIONAL")){
						totalRegistrosMN++;
						totalMontoMN = totalMontoMN + monto;
					} else if(moneda.equals("DOLAR AMERICANO")){
						totalRegistrosDL++;
						totalMontoDL = totalMontoDL + monto;
					}
				}
				pdfDoc.addLTable();

				/***** Lleno la tabla de totales *****/
				try{
					pdfDoc.setLTable(3,100);
					pdfDoc.setLCell("TOTALES","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Registros","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto Documentos","celda01",ComunesPDF.CENTER);
					pdfDoc.setLHeaders();
					pdfDoc.setLCell("TOTAL MONEDA NACIONAL","formas",ComunesPDF.LEFT);
					pdfDoc.setLCell("" + totalRegistrosMN, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(totalMontoMN,2), "formas", ComunesPDF.RIGHT);
					pdfDoc.setLCell("TOTAL DOLAR AMERICANO","formas",ComunesPDF.LEFT);
					pdfDoc.setLCell("" + totalRegistrosDL, "formas", ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(totalMontoDL,2), "formas", ComunesPDF.RIGHT);
					pdfDoc.addLTable();
				} catch(Exception e){
					log.warn("Error al obtener la tabla de totales. (El resto del documento se imprimir� de forma normal)." + e);
				}

				pdfDoc.endDocument();

			} catch(Throwable e){
				throw new AppException("Error al generar el archivo PDF. ", e);
			}

		}

		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}

	public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo){
		return null;
	}

/*******************************************************************************
 *                             GETTERS Y SETTERS                               *
 *******************************************************************************/
	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getIc_pyme() {
		return ic_pyme;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getIc_estatus_docto() {
		return ic_estatus_docto;
	}

	public void setIc_estatus_docto(String ic_estatus_docto) {
		this.ic_estatus_docto = ic_estatus_docto;
	}

	public String getIg_numero_docto() {
		return ig_numero_docto;
	}

	public void setIg_numero_docto(String ig_numero_docto) {
		this.ig_numero_docto = ig_numero_docto;
	}

	public String getFecha_seleccion_de() {
		return fecha_seleccion_de;
	}

	public void setFecha_seleccion_de(String fecha_seleccion_de) {
		this.fecha_seleccion_de = fecha_seleccion_de;
	}

	public String getFecha_seleccion_a() {
		return fecha_seleccion_a;
	}

	public void setFecha_seleccion_a(String fecha_seleccion_a) {
		this.fecha_seleccion_a = fecha_seleccion_a;
	}

	public String getCc_acuse() {
		return cc_acuse;
	}

	public void setCc_acuse(String cc_acuse) {
		this.cc_acuse = cc_acuse;
	}

	public String getMonto_credito_de() {
		return monto_credito_de;
	}

	public void setMonto_credito_de(String monto_credito_de) {
		this.monto_credito_de = monto_credito_de;
	}

	public String getMonto_credito_a() {
		return monto_credito_a;
	}

	public void setMonto_credito_a(String monto_credito_a) {
		this.monto_credito_a = monto_credito_a;
	}

	public String getFecha_emision_de() {
		return fecha_emision_de;
	}

	public void setFecha_emision_de(String fecha_emision_de) {
		this.fecha_emision_de = fecha_emision_de;
	}

	public String getFecha_emision_a() {
		return fecha_emision_a;
	}

	public void setFecha_emision_a(String fecha_emision_a) {
		this.fecha_emision_a = fecha_emision_a;
	}

	public String getFecha_vto_de() {
		return fecha_vto_de;
	}

	public void setFecha_vto_de(String fecha_vto_de) {
		this.fecha_vto_de = fecha_vto_de;
	}

	public String getFecha_vto_a() {
		return fecha_vto_a;
	}

	public void setFecha_vto_a(String fecha_vto_a) {
		this.fecha_vto_a = fecha_vto_a;
	}

	public String getFecha_vto_credito_de() {
		return fecha_vto_credito_de;
	}

	public void setFecha_vto_credito_de(String fecha_vto_credito_de) {
		this.fecha_vto_credito_de = fecha_vto_credito_de;
	}

	public String getFecha_vto_credito_a() {
		return fecha_vto_credito_a;
	}

	public void setFecha_vto_credito_a(String fecha_vto_credito_a) {
		this.fecha_vto_credito_a = fecha_vto_credito_a;
	}

	public String getIc_tipo_cobro_int() {
		return ic_tipo_cobro_int;
	}

	public void setIc_tipo_cobro_int(String ic_tipo_cobro_int) {
		this.ic_tipo_cobro_int = ic_tipo_cobro_int;
	}

	public String getIc_moneda() {
		return ic_moneda;
	}

	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}

	public String getFn_monto_de() {
		return fn_monto_de;
	}

	public void setFn_monto_de(String fn_monto_de) {
		this.fn_monto_de = fn_monto_de;
	}

	public String getFn_monto_a() {
		return fn_monto_a;
	}

	public void setFn_monto_a(String fn_monto_a) {
		this.fn_monto_a = fn_monto_a;
	}

	public String getMonto_con_descuento() {
		return monto_con_descuento;
	}

	public void setMonto_con_descuento(String monto_con_descuento) {
		this.monto_con_descuento = monto_con_descuento;
	}

	public String getSolo_cambio() {
		return solo_cambio;
	}

	public void setSolo_cambio(String solo_cambio) {
		this.solo_cambio = solo_cambio;
	}

	public String getModo_plazo() {
		return modo_plazo;
	}

	public void setModo_plazo(String modo_plazo) {
		this.modo_plazo = modo_plazo;
	}

	public String getTipo_credito() {
		return tipo_credito;
	}

	public void setTipo_credito(String tipo_credito) {
		this.tipo_credito = tipo_credito;
	}

	public String getIc_documento() {
		return ic_documento;
	}

	public void setIc_documento(String ic_documento) {
		this.ic_documento = ic_documento;
	}

	public String getFecha_publicacion_de() {
		return fecha_publicacion_de;
	}

	public void setFecha_publicacion_de(String fecha_publicacion_de) {
		this.fecha_publicacion_de = fecha_publicacion_de;
	}

	public String getFecha_publicacion_a() {
		return fecha_publicacion_a;
	}

	public void setFecha_publicacion_a(String fecha_publicacion_a) {
		this.fecha_publicacion_a = fecha_publicacion_a;
	}

	public String getNOnegociable() {
		return NOnegociable;
	}

	public void setNOnegociable(String NOnegociable) {
		this.NOnegociable = NOnegociable;
	}

	public List getConditions(){
		return conditions; 
	}

	public String getTipo_pago() {
		return tipo_pago;
	}

	public void setTipo_pago(String tipo_pago) {
		this.tipo_pago = tipo_pago;
	}

	public String getPublicaDoctosFinanciables() {
		return publicaDoctosFinanciables;
	}

	public void setPublicaDoctosFinanciables(String publicaDoctosFinanciables) {
		this.publicaDoctosFinanciables = publicaDoctosFinanciables;
	}
}
