package com.netro.distribuidores;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Registros;

public class RepEstatusPyme{
 	 
	private StringBuffer qrysentencia;
	private String cveEstatus="";
	private String icpyme="";
	private String tiposCredito = "";
	private int producto;
	private Registros registros;
	private List conditions;
	
	public void setCveEstatus(String cveEstatus){		
		this.cveEstatus=cveEstatus;
	}
	
	public void setIcePyme(String icPyme) {
		this.icpyme = icPyme;
	}
	public void setTiposCredito(String tiposCredito){
		this.tiposCredito = tiposCredito;
	}
	public void setProducto(int prod){
		this.producto = prod;
	}

	public Registros executeQuery() {
		AccesoDB con = new AccesoDB();
		Registros registros = new Registros();
		conditions = new ArrayList();
		try{
			con.conexionDB();
			conditions.add(new Integer(this.producto));
      conditions.add(this.icpyme);
			conditions.add(this.cveEstatus);
			System.err.println(">>>>> "+this.qrysentencia.toString());
			System.err.println(">>>>> "+this.conditions);
			registros = con.consultarDB(this.getQrysentencia(),conditions);
			return registros;
		} catch(Exception e) {
			throw new AppException("RepEstatusPyme::ExecuteQuery(Exception) ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(true); //Para consultas que hagan uso de tablas remotas via dblink
				con.cierraConexionDB();
			}
		}
	}
	public void setQrysentencia(){
		StringBuffer campos, condiciones, tablas;		//, ordenarPor;

		campos = new StringBuffer();
		campos.append("	SELECT ce.cg_razon_social AS nombreepo, doc.ig_numero_docto,doc.cc_acuse, TO_CHAR (doc.df_fecha_emision, 'dd/mm/yyyy') AS fecha_emision, "+
						"	TO_CHAR (doc.df_fecha_venc, 'dd/mm/yyyy') AS fecha_venc,TO_CHAR (doc.df_carga, 'dd/mm/yyyy') AS fecha_carga, doc.ig_plazo_docto, mon.cd_nombre,doc.fn_monto, "+
						"	DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
						"	'N', 'Sin conversion','P', 'Dolar-Peso','') AS tipo_conversion, "+
						"	DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion), "+
						"	'P', DECODE (mon.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio, "+
						"	DECODE (doc.ic_moneda,1, doc.fn_monto,54, (doc.fn_monto -(doc.fn_monto * "+
						"	DECODE (doc.ic_tipo_financiamiento,1, doc.fn_porc_descuento, 0) / 100)) * doc.fn_tipo_cambio) as monto_val, "+
						"	NVL (doc.ig_plazo_descuento, '') AS ig_plazo_descuento, "+
						"	NVL (doc.fn_porc_descuento, 0) AS fn_porc_descuento, "+
						"	doc.fn_monto*(nvl(doc.fn_porc_descuento,0)/100) as monto_desc, "+
						"	tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus, ");
		if (cveEstatus.equals("9") || cveEstatus.equals("2") || cveEstatus.equals("5") || cveEstatus.equals("1"))	{
			campos.append("	doc.ic_moneda, doc.ic_documento, ");
		}
		
		if (cveEstatus.equals("3") || cveEstatus.equals("4") || cveEstatus.equals("11")  || cveEstatus.equals("22") || cveEstatus.equals("24")  )	{//MOD +(|| cveEstatus == "24")
			campos.append("  cif.cg_razon_social AS nombreif, DECODE (x.cg_tipo_credito,'D', 'Descuento y/o Factoraje','C', 'Credito en Cuenta Corriente',"   +
												"	'A', 'Ambas') AS tipo_cred,");
			if (cveEstatus.equals("3") || cveEstatus.equals("24"))	{
				campos.append(" TO_CHAR (ac2.df_fecha_hora, 'DD/MM/YYYY') AS fecha_opera, ");
			}else	{
				campos.append(" TO_CHAR (ac3.df_fecha_hora, 'DD/MM/YYYY') AS fecha_opera, ");
			}
			campos.append(" sel.fn_importe_recibir, doc.ig_plazo_credito, TO_CHAR (doc.df_fecha_venc_credito, 'DD/MM/YYYY') AS fecha_vencre, ");

			if("D".equals(this.tiposCredito)){
				campos.append(" lc.ic_linea_credito_dm, DECODE(sel.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || sel.CG_REL_MAT||' '||sel.FN_PUNTOS) as ref_tasa,");
			}else if("C".equals(this.tiposCredito)){
				campos.append(" lc.ic_linea_credito, ct.cd_nombre || ' ' || sel.cg_rel_mat || ' ' || sel.fn_puntos AS ref_tasa,");
			}
			campos.append("	sel.fn_valor_tasa, sel.fn_importe_interes, tci.cd_descripcion AS tipo_cob_int, doc.ic_moneda, doc.ic_documento,");
			if (cveEstatus.equals("4") || cveEstatus.equals("11")  || cveEstatus.equals("22"))	{
				campos.append("	TO_CHAR(ac3.df_fecha_hora,'dd/mm/yyyy') as fecha_oper_if,");
			}
			if ( cveEstatus.equals("22"))	{
				campos.append("	TO_CHAR(cest.dc_fecha_cambio,'dd/mm/yyyy') as fecha_cambio,");
			}
		}
		campos.append("	doc.CG_VENTACARTERA as CG_VENTACARTERA ");
	/*	if (cveEstatus == "3" || cveEstatus == "4"  || cveEstatus == "11")  {
			campos.append(",   (SELECT nvl(fn_valor_aforo, 0)     FROM dis_aforo_epo   WHERE ic_if = cif.ic_if      AND ic_epo = doc.ic_epo        AND ic_moneda = doc.ic_moneda) AS POR_AFRO  ");
		}
	*/	
  if (cveEstatus.equals("32")){
        campos.append(
        ",mon.ic_moneda, doc.ic_documento, doc.fn_monto - ( doc.fn_monto * (NVL (doc.fn_porc_descuento, 0) / 100) ) as montofinal"+
        ",DECODE ( ed.ic_estatus_docto, 32, cif2.cg_razon_social, cif.cg_razon_social ) as nombre_if "+
        ",bins.cg_codigo_bin,bins.cg_descripcion as desc_bins "+
        ",ddp.ic_orden_pago_tc as orden_enviado "+
        ",ddp.cg_transaccion_id as id_operacion "+
        ",( ddp.cg_codigo_resp || ' ' || ddp.cg_codigo_desc ) as codigo_autoriza "+
        ",ddp.df_fecha_autorizacion as fecha_registro, ddp.cg_num_tarjeta as num_tarjeta");
        }
//tablas
		tablas = new StringBuffer();
		tablas.append("	FROM dis_documento doc, comcat_epo ce, comcat_moneda mon, comrel_producto_epo pe, "+
						"	comcat_producto_nafin pn, com_tipo_cambio tc, comcat_tipo_financiamiento tf,	comcat_estatus_docto ed ");
		if (cveEstatus.equals("3") || cveEstatus.equals("4") || cveEstatus.equals("11") || cveEstatus.equals("22") || cveEstatus.equals("24")   )	{//MOD +( || cveEstatus == "24")
			if("D".equals(this.tiposCredito)){
				tablas.append("	,dis_linea_credito_dm lc");
			}else if("C".equals(this.tiposCredito)){
				tablas.append(" ,com_linea_credito lc");
			}
			tablas.append("	,comcat_if cif, comrel_pyme_epo_x_producto x	");
			if (cveEstatus.equals("3") || cveEstatus.equals("24"))	{
				tablas.append("	,com_acuse2 ac2	");
			}else	{
				tablas.append("	,dis_solicitud sol,	com_acuse3 ac3	");
			}
			tablas.append("	,dis_docto_seleccionado sel, comcat_tasa ct,	comcat_tipo_cobro_interes tci ");
		}
		if (cveEstatus.equals("9") || cveEstatus.equals("5") || cveEstatus.equals("11") || cveEstatus.equals("22")  )	{
			tablas.append("	,dis_cambio_estatus cest");
		}
			if (cveEstatus.equals("32")) {
      tablas.append(", comcat_if cif,  comcat_if cif2, com_bins_if bins, dis_doctos_pago_tc ddp");
    }
//condiciones		
	condiciones = new StringBuffer();
 
	condiciones.append("	WHERE doc.ic_producto_nafin = ? "+
																			"	AND doc.ic_pyme = ? "+
																			"	AND doc.ic_estatus_docto = ? "+
																			"	AND doc.ic_epo = ce.ic_epo "+
																			"	AND doc.ic_moneda = mon.ic_moneda "+
																			"	AND pn.ic_producto_nafin = doc.ic_producto_nafin "+
																			"	AND pn.ic_producto_nafin = pe.ic_producto_nafin "+
																			"	AND pe.ic_epo = doc.ic_epo "+
																			"	AND tc.dc_fecha IN (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = mon.ic_moneda) "+
																			"	AND mon.ic_moneda = tc.ic_moneda "+
																			"	AND doc.ic_tipo_financiamiento = tf.ic_tipo_financiamiento "+
																			"	AND ed.ic_estatus_docto = doc.ic_estatus_docto ");
		if (cveEstatus.equals("3") || cveEstatus.equals("4") || cveEstatus .equals("11")	 || cveEstatus.equals("22") || cveEstatus.equals("24")  )	{//MOD +(|| cveEstatus == "24" )
			condiciones.append("	AND lc.ic_if = cif.ic_if	"	);
			if("D".equals(this.tiposCredito)){
				condiciones.append("	AND doc.ic_linea_credito_dm = lc.ic_linea_credito_dm	AND x.cg_tipo_credito = 'D'	" );
			}else if("C".equals(this.tiposCredito)){
				condiciones.append(" AND doc.ic_linea_credito = lc.ic_linea_credito	AND x.cg_tipo_credito = 'C'	");
			}
			condiciones.append("	AND x.ic_epo = doc.ic_epo	AND x.ic_pyme = doc.ic_pyme	AND x.ic_producto_nafin = doc.ic_producto_nafin	");
			if (cveEstatus.equals("3") || cveEstatus.equals("24"))	{
				condiciones.append("	AND ac2.cc_acuse = sel.cc_acuse	AND sel.ic_documento = doc.ic_documento	AND sel.ic_tasa = ct.ic_tasa	"	+
															"	AND tci.ic_tipo_cobro_interes = lc.ic_tipo_cobro_interes"   +
															"	AND sel.df_fecha_seleccion >= TRUNC(SYSDATE) AND sel.df_fecha_seleccion < TRUNC(SYSDATE) + 1 ");
			}
			if (cveEstatus.equals("4")){
				condiciones.append("	AND sol.ic_documento = doc.ic_documento 	AND ac3.cc_acuse = sol.cc_acuse	AND sel.ic_documento = doc.ic_documento "	+
															"	AND sel.ic_tasa = ct.ic_tasa	AND tci.ic_tipo_cobro_interes = lc.ic_tipo_cobro_interes	AND sol.ic_documento=doc.ic_documento "	+
															"	AND ac3.df_fecha_hora >= TRUNC(SYSDATE) AND ac3.df_fecha_hora < TRUNC(SYSDATE) + 1 ");
			}
			if (cveEstatus.equals("11")	 || cveEstatus.equals("22"))	{
				condiciones.append("	AND sol.ic_documento = doc.ic_documento 	AND ac3.cc_acuse = sol.cc_acuse	AND sel.ic_documento = doc.ic_documento "	+
															"	AND sel.ic_tasa = ct.ic_tasa	AND tci.ic_tipo_cobro_interes = lc.ic_tipo_cobro_interes	");
			}
		}
		if (cveEstatus.equals("9") || cveEstatus.equals("5") || cveEstatus.equals("11") || cveEstatus.equals("22")  )	{
			condiciones.append("	AND cest.ic_documento = doc.ic_documento "	+
														"	AND cest.dc_fecha_cambio IN (SELECT MAX (dc_fecha_cambio) FROM dis_cambio_estatus  "+
																																"	WHERE ic_documento = doc.ic_documento "+
																																"	/*AND dc_fecha_cambio >= TRUNC(SYSDATE) AND dc_fecha_cambio < TRUNC(SYSDATE) + 1*/ ) ");
		}
		if (cveEstatus .equals("1") || cveEstatus.equals("2"))	{
			condiciones.append("	AND doc.DF_CARGA >= TRUNC(SYSDATE) AND doc.DF_CARGA < TRUNC(SYSDATE) + 1 ");
		}
    if (cveEstatus.equals("32"))	{
			condiciones.append("  AND cif.ic_if = 1   AND doc.ic_bins = bins.ic_bins(+) AND bins.ic_if = cif2.ic_if(+) AND doc.ic_orden_pago_tc = ddp.ic_orden_pago_tc(+) AND doc.DF_CARGA >= TRUNC(SYSDATE) AND doc.DF_CARGA < TRUNC(SYSDATE) + 1");
		}

		this.qrysentencia = new StringBuffer();
		this.qrysentencia.append(campos.toString());
		this.qrysentencia.append(tablas.toString());
		this.qrysentencia.append(condiciones.toString());
	}
	
	public String getQrysentencia(){
		return this.qrysentencia.toString();
	}
		
}//Class End.