package com.netro.distribuidores;


import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorThreadRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class InfDocCredNafinDist2   implements IQueryGenerator, IQueryGeneratorThreadRegExtJS{
	public InfDocCredNafinDist2(){}

	public String getAggregateCalculationQuery(HttpServletRequest request) {
		StringBuffer qrySentencia	= new StringBuffer();
		StringBuffer qryDM			= new StringBuffer();
		StringBuffer qryCCC			= new StringBuffer();
		StringBuffer condicion		= new StringBuffer();
		String			condicionDstoA	= "";
		String			tablasDstoA		= "";
		String	fechaHoy					= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String	ic_epo					=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String	ic_pyme					=	(request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String	ic_if						=	(request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String	ic_estatus				=	(request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto");
		String 	ig_numero_docto		=	(request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String 	fecha_seleccion_de	=	(request.getParameter("fecha_seleccion_de")==null)?"":request.getParameter("fecha_seleccion_de");
		String 	fecha_seleccion_a		=	(request.getParameter("fecha_seleccion_a")==null)?"":request.getParameter("fecha_seleccion_a");
		String 	cc_acuse					=	(request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String 	monto_credito_de		=	(request.getParameter("monto_credito_de")==null)?"":request.getParameter("monto_credito_de");
		String 	monto_credito_a		=	(request.getParameter("monto_credito_a")==null)?"":request.getParameter("monto_credito_a");
		String 	fecha_emision_de		=	(request.getParameter("fecha_emision_de")==null)?"":request.getParameter("fecha_emision_de");
		String 	fecha_emision_a		=	(request.getParameter("fecha_emision_a")==null)?"":request.getParameter("fecha_emision_a");
		String 	fecha_vto_de			=	(request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String 	fecha_vto_a				=	(request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String 	fecha_vto_credito_de	=	(request.getParameter("fecha_vto_credito_de")==null)?"":request.getParameter("fecha_vto_credito_de");
		String 	fecha_vto_credito_a 	=	(request.getParameter("fecha_vto_credito_a")==null)?"":request.getParameter("fecha_vto_credito_a");
		String 	ic_tipo_cobro_int		=	(request.getParameter("ic_tipo_cobro_int")==null)?"":request.getParameter("ic_tipo_cobro_int");
		String	ic_moneda				=	(request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String 	fn_monto_de				=	(request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String 	fn_monto_a				=	(request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String 	monto_con_descuento	=	(request.getParameter("monto_con_descuento")==null)?"":"checked";
		String 	solo_cambio				=	(request.getParameter("solo_cambio")==null)?"":request.getParameter("solo_cambio");		
		String	modo_plazo				=	(request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
		String	tipo_credito			=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String	ic_documento			=	(request.getParameter("ic_documento")==null)?"":request.getParameter("ic_documento");
		String 	fecha_publicacion_de	=	(request.getParameter("fecha_publicacion_de")==null)?fechaHoy:request.getParameter("fecha_publicacion_de");
		String 	fecha_publicacion_a	=	(request.getParameter("fecha_publicacion_a")==null)?fechaHoy:request.getParameter("fecha_publicacion_a");
		String 	chk_proc_desc		=	(request.getParameter("chk_proc_desc")==null)?"":request.getParameter("chk_proc_desc");
		String 	tipoSolic				= "";
		String	ic_estatus_docto		= "";
		try {		

			if(!"".equals(ic_estatus)){
				ic_estatus_docto = ic_estatus.substring(1,ic_estatus.length());
				tipoSolic		 = ic_estatus.substring(0,1);
			}
	
		if(!"".equals(ic_epo)&&ic_epo!=null)
			condicion.append(" AND D.IC_EPO = "+ic_epo);
		if(!"".equals(ic_pyme)&&ic_pyme!=null)
			condicion.append(" AND D.IC_PYME = "+ic_pyme);
		if("1".equals(ic_estatus_docto)||"2".equals(ic_estatus_docto)||"3".equals(ic_estatus_docto)||"4".equals(ic_estatus_docto)||"10".equals(ic_estatus_docto))
			condicion.append(" AND S.IC_ESTATUS_SOLIC ="+ic_estatus_docto);
		if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
			condicion.append(" and D.ig_numero_docto = '"+ig_numero_docto+"'");
		if(!"".equals(fecha_seleccion_de)&& fecha_seleccion_de!=null&&!"".equals(fecha_seleccion_a)&& fecha_seleccion_a!=null)
			condicion.append(" and trunc(DS.df_fecha_seleccion) between trunc(to_date('"+fecha_seleccion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_seleccion_a+"','dd/mm/yyyy'))");
		if(!"".equals(cc_acuse)&& cc_acuse!=null)
			condicion.append(" and D.cc_acuse = '"+cc_acuse+"'");
		if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null)
			condicion.append(" and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))");
		if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
			condicion.append(" and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
		if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null)
			condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date('"+fecha_vto_credito_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_credito_a+"','dd/mm/yyyy'))");
		if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null)
			condicion.append(" AND LC.ic_tipo_cobro_interes = "+ic_tipo_cobro_int);
		if(!"".equals(ic_moneda)&&ic_moneda!=null)
			condicion.append(" and M.ic_moneda = "+ic_moneda);
		if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
			condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de+" and "+fn_monto_a);
		if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
			condicion.append(" and D.fn_monto between "+fn_monto_de+" and "+fn_monto_a);
		if(!"".equals(solo_cambio)&& solo_cambio!=null)
			condicion.append(" and D.ic_documento in (select ic_documento from dis_cambio_estatus)");
		if(!"".equals(modo_plazo)&& modo_plazo!=null)
			condicion.append(" and D.ic_tipo_financiamiento = "+modo_plazo);
		if(!"".equals(ic_documento)&&ic_documento!=null)
			condicion.append(" AND D.IC_DOCUMENTO = "+ic_documento);
		if(!"".equals(ic_if)&&ic_if!=null)
			condicion.append(" AND LC.IC_IF = "+ic_if);
		if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
			condicion.append(" and trunc(D.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))");
		if(!"".equals(monto_credito_de)&& monto_credito_de!=null&&!"".equals(monto_credito_a)&& monto_credito_a!=null)
			condicion.append(" and (D.fn_monto-(fn_monto*(fn_porc_descuento/100))) between "+monto_credito_de+" and "+monto_credito_a);
		if("".equals(ic_estatus_docto))
			condicion.append(" and S.ic_estatus_solic in (1,2,3,4,10)");
		if("S".equals(chk_proc_desc)) {
			condicion.append(" AND D.IC_ESTATUS_DOCTO in (4,11)");
			condicionDstoA	= 
				" AND SUBSTR (ds.cc_acuse, 1, 2) = 'D5' ";

		}
		if("D".equals(tipoSolic))
			condicion.append(" and D.ic_estatus_docto is null ");		

		if(!"".equals(ic_tipo_Pago)) {
			condicion.append("AND d.ig_tipo_pago ="+ic_tipo_Pago);   
		} 
			qryDM.append(
				"    SELECT /*+use_nl(d,ds,s,lc,m,pep,e)"   +
				"      index(pep cp_comrel_pyme_epo_x_prod_pk)*/"   +
				"        m.ic_moneda"   +
				"  	  ,m.cd_nombre"   +
				"  	  ,d.fn_monto"   +
				"    FROM DIS_DOCUMENTO D"   +
				"    ,DIS_DOCTO_SELECCIONADO DS"   +
				"    ,DIS_SOLICITUD S"   +
				"    ,DIS_LINEA_CREDITO_DM LC"   +
				"    ,COMCAT_MONEDA M"   +
				"    ,COMREL_PYME_EPO_X_PRODUCTO PEP"   +
				"    ,COMCAT_EPO E"   +
				" ,  comcat_tipo_financiamiento tf  "+
				
				"    WHERE D.IC_EPO = E.IC_EPO"   +
				"    AND D.IC_MONEDA = M.IC_MONEDA"   +
				"    AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM"   +
				"    AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
				"    AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
				"    AND D.IC_PYME = PEP.IC_PYME"   +
				"    AND D.IC_EPO = PEP.IC_EPO"   +
				"    AND D.IC_PRODUCTO_NAFIN = PEP.IC_PRODUCTO_NAFIN"   +
				"    AND PEP.IC_PRODUCTO_NAFIN = 4 "+condicionDstoA+" "+
				//"    AND PEP.CS_HABILITADO = 'S'"  +
				"    AND E.CS_HABILITADO = 'S'"  +
				"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento  "+ 

					condicion.toString()
				);
					
			qryCCC.append(
				"    SELECT /*+ use_nl(d,ds,s,lc,m,pep,e)"   +
				"        index(d in_dis_documento_03_nuk)"   +
				"        index(pep cp_comrel_pyme_epo_x_prod_pk)*/"   +
				"        m.ic_moneda"   +
				"  	  ,m.cd_nombre"   +
				"  	  ,d.fn_monto"   +
				"    FROM DIS_DOCUMENTO D"   +
				"    ,DIS_DOCTO_SELECCIONADO DS"   +
				"    ,DIS_SOLICITUD S"   +
				"    ,COM_LINEA_CREDITO LC"   +
				"    ,COMCAT_MONEDA M"   +
				"    ,COMREL_PYME_EPO_X_PRODUCTO PEP"   +
				"    ,COMCAT_EPO E"   +
				" ,  comcat_tipo_financiamiento tf  "+
				"    WHERE D.IC_EPO = E.IC_EPO"   +
				"    AND D.IC_MONEDA = M.IC_MONEDA"   +
				"    AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO"   +
				"    AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
				"    AND D.IC_PYME = PEP.IC_PYME"   +
				"    AND D.IC_EPO = PEP.IC_EPO"   +
				"    AND D.IC_PRODUCTO_NAFIN = PEP.IC_PRODUCTO_NAFIN"   +
				"    AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
				"    AND PEP.IC_PRODUCTO_NAFIN = 4 "+
				//"    AND PEP.CS_HABILITADO = 'S'"  +
				"    AND E.CS_HABILITADO = 'S'"  +
				"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento  "+ 

				condicion.toString());

			String qryAux = "";	
			if("D".equals(tipo_credito))
				qryAux = qryDM.toString();
			else if("C".equals(tipo_credito))
				qryAux = qryCCC.toString();
			else
				qryAux = qryDM.toString()+ " UNION ALL "+qryCCC.toString();

			qrySentencia.append(
				"SELECT ic_moneda, cd_nombre, COUNT (1), SUM (fn_monto), 'InfDocCredNafinDist2::getAggregateCalculationQuery'"+
				"  FROM ("+qryAux.toString()+")"+
				"GROUP BY  ic_moneda,cd_nombre ORDER BY ic_moneda ");
				System.out.println("Query "+ qrySentencia);
		}catch(Exception e){
			System.out.println("InfLinCredNafinDist::getAggregateCalculationQuery "+e);
		}
		return qrySentencia.toString();
	}

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids) {
	
		log.info("getDocumentSummaryQueryForIds (E)");
	
		int i=0;
		String	tipo_credito			=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		StringBuffer qrySentencia	= new StringBuffer();
		StringBuffer qryDM			= new StringBuffer();
		StringBuffer qryCCC			= new StringBuffer();
		StringBuffer condicion		= new StringBuffer();
		
		condicion.append(" AND d.ic_documento in (");

		i=0;
		for (Iterator it = ids.iterator(); it.hasNext();i++){
        	if(i>0){
				condicion.append(",");
			}
			condicion.append(" "+it.next().toString()+" ");
		}
		condicion.append(") ");

		qryDM.append(
					"  SELECT /*+ use_nl(d,ds,s,lc,tc,py,m,pe,pn,tf,ed,i,pep,ct,tci,e)"   +
					"     index(d cp_dis_documento_pk)"   +
					"     index(tf cp_comcat_tipo_finan_pk)"   +
					"     index(pep cp_comrel_pyme_epo_x_prod_pk)*/"   +
					"     PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"   +
					"  	,D.IG_NUMERO_DOCTO"   +
					"  	,D.CC_ACUSE"   +
					" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"   +
					" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"   +
					" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"   +
					" 	,D.IG_PLAZO_DOCTO"   +
					" 	,M.CD_NOMBRE AS MONEDA"   +
					" 	,D.FN_MONTO"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') AS TIPO_CONVERSION"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
					" 	,NVL(D.IG_PLAZO_DESCUENTO,'') AS IG_PLAZO_DESCUENTO"   +
					" 	,DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) AS FN_PORC_DESCUENTO"   +
					" 	,TF.CD_DESCRIPCION AS MODO_PLAZO"   +
					" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
					" 	,M.ic_moneda"   +
					" 	,D.ic_documento"   +
					" 	,I.cg_razon_social AS NOMBRE_IF"   +
					" 	,DECODE(PEP.cg_tipo_credito,'D','Descuento y/o Factoraje','Credito en Cuenta Corriente') AS TIPO_CREDITO"   +
					" 	,TO_CHAR(DS.DF_FECHA_SELECCION,'DD/MM/YYYY') AS DF_OPERACION_CREDITO"   +
					" 	,d.ig_plazo_credito AS PLAZO_CREDITO"   +
					" 	,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'DD/MM/YYYY') AS DF_VENC_CREDITO"   +
					"	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) AS REF_TASA" +
					" 	,TCI.CD_DESCRIPCION AS TIPO_COBRO_INT"   +
/*					"  	,DECODE(DS.CG_REL_MAT"   +
					" 		,'+',DS.fn_valor_tasa + DS.fn_puntos"   +
					" 		,'-',DS.fn_valor_tasa - DS.fn_puntos"   +
					" 		,'*',DS.fn_valor_tasa * DS.fn_puntos"   +
					" 		,'/',DS.fn_valor_tasa / DS.fn_puntos"   +
					" 		,0) AS FN_VALOR_TASA"   +*/
					"  ,DS.fn_valor_tasa"+
					" 	,DS.FN_IMPORTE_INTERES"   +
					" 	,LC.ic_moneda AS MONEDA_LINEA"   +
					" 	,DS.fn_importe_recibir AS MONTO_CREDITO"   +
					" 	,E.cg_razon_social AS epo"   +
					" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
					", DECODE (d.ig_tipo_pago, '1', 'Financiamiento con intereses', '2', 'Meses sin Intereses ','3', 'Tarjeta de Cr�dito' ) AS TIPOPAGO "+ //F09-2015
	                  
					"  FROM DIS_DOCUMENTO D"   +
					"  ,DIS_DOCTO_SELECCIONADO DS"   +
					"  ,DIS_SOLICITUD S"   +
					"  ,DIS_LINEA_CREDITO_DM LC"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_PYME PY"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  ,COMCAT_ESTATUS_SOLIC ED"   +
					"  ,COMCAT_IF I"   +
					"  ,COMREL_PYME_EPO_X_PRODUCTO PEP"   +
					"  ,COMCAT_TASA CT"   +
					"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"  ,COMCAT_EPO E"   +
					"  WHERE D.IC_PYME = PY.IC_PYME"   +
					"  AND D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND D.IC_EPO = E.IC_EPO"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM"   +
					"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"  AND TC.DC_FECHA IN(SELECT /*+ index(tc2 in_com_tipo_cambio_01_nuk)*/MAX(DC_FECHA) FROM COM_TIPO_CAMBIO tc2 WHERE tc2.IC_MONEDA = M.IC_MONEDA)"   +
					"  AND M.ic_moneda = TC.ic_moneda"   +
					"  AND ED.IC_ESTATUS_SOLIC = S.IC_ESTATUS_SOLIC"   +
					"  AND LC.IC_IF = I.IC_IF"   +
					"  AND PEP.IC_PYME = PY.IC_PYME"   +
					"  AND PEP.IC_EPO = D.IC_EPO"   +
					"  AND DS.IC_TASA = CT.IC_TASA"   +
					"  AND LC.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES"   +
					"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"  AND PN.IC_PRODUCTO_NAFIN = 4"   +
					"  AND PEP.IC_PRODUCTO_NAFIN = 4"   +
					//"  AND PEP.CS_HABILITADO = 'S'  " +
					condicion);

			qryCCC.append(
					"  SELECT /*+ use_nl(d,ds,s,lc,tc,py,m,pe,pn,tf,ed,i,pep,ct,tci,e)"   +
					"      index(d cp_dis_documento_pk)"   +
					"      index(tf cp_comcat_tipo_finan_pk)"   +
					"      index(pep cp_comrel_pyme_epo_x_prod_pk)*/"   +
					"  PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"   +
					"  ,D.IG_NUMERO_DOCTO"   +
					"  ,D.CC_ACUSE"   +
					" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"   +
					" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"   +
					" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"   +
					" 	,D.IG_PLAZO_DOCTO"   +
					" 	,M.CD_NOMBRE AS MONEDA"   +
					" 	,D.FN_MONTO"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') AS TIPO_CONVERSION"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
					" 	,NVL(D.IG_PLAZO_DESCUENTO,'') AS IG_PLAZO_DESCUENTO"   +
					" 	,DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) AS FN_PORC_DESCUENTO"   +
					" 	,TF.CD_DESCRIPCION AS MODO_PLAZO"   +
					" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
					" 	,M.ic_moneda"   +
					" 	,D.ic_documento"   +
					" 	,I.cg_razon_social AS NOMBRE_IF"   +
					" 	,DECODE(PEP.cg_tipo_credito,'D','Descuento y/o Factoraje','Credito en Cuenta Corriente') AS TIPO_CREDITO"   +
					" 	,TO_CHAR(DS.DF_FECHA_SELECCION,'DD/MM/YYYY') AS DF_OPERACION_CREDITO"   +
					" 	,d.ig_plazo_credito AS PLAZO_CREDITO"   +
					" 	,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'DD/MM/YYYY') AS DF_VENC_CREDITO"   +
					" 	,CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS AS REF_TASA"   +
					" 	,TCI.CD_DESCRIPCION AS TIPO_COBRO_INT"   +
/*					"  	,DECODE(DS.CG_REL_MAT"   +
					" 		,'+',DS.fn_valor_tasa + DS.fn_puntos"   +
					" 		,'-',DS.fn_valor_tasa - DS.fn_puntos"   +
					" 		,'*',DS.fn_valor_tasa * DS.fn_puntos"   +
					" 		,'/',DS.fn_valor_tasa / DS.fn_puntos"   +
					" 		,0) AS FN_VALOR_TASA"   +*/
					"  ,DS.fn_valor_tasa"+					
					" 	,DS.FN_IMPORTE_INTERES"   +
					" 	,LC.ic_moneda AS MONEDA_LINEA"   +
					" 	,DS.fn_importe_recibir AS MONTO_CREDITO"   +
					" 	,E.cg_razon_social AS epo"   +
					" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
					", DECODE (d.ig_tipo_pago, '1', 'Financiamiento con intereses', '2', 'Meses sin Intereses ','3', 'Tarjeta de Cr�dito' ) AS TIPOPAGO "+ //F09-2015
	                  
					
					"  FROM DIS_DOCUMENTO D"   +
					"  ,DIS_DOCTO_SELECCIONADO DS"   +
					"  ,DIS_SOLICITUD S"   +
					"  ,COM_LINEA_CREDITO LC"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_PYME PY"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  ,COMCAT_ESTATUS_SOLIC ED"   +
					"  ,COMCAT_IF I"   +
					"  ,COMREL_PYME_EPO_X_PRODUCTO PEP"   +
					"  ,COMCAT_TASA CT"   +
					"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"  ,COMCAT_EPO E"   +
					"  WHERE D.IC_PYME = PY.IC_PYME"   +
					"  AND D.IC_EPO = E.IC_EPO"   +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO"   +
					"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"  AND DS.IC_TASA = CT.IC_TASA"   +
					"  AND TC.DC_FECHA IN(SELECT /*+ index(tc2 in_com_tipo_cambio_01_nuk)*/MAX(DC_FECHA) FROM COM_TIPO_CAMBIO tc2 WHERE tc2.IC_MONEDA = M.IC_MONEDA)"   +
					"  AND M.ic_moneda = TC.ic_moneda"   +
					"  AND ED.IC_ESTATUS_SOLIC = S.IC_ESTATUS_SOLIC"   +
					"  AND LC.IC_IF = I.IC_IF"   +
					"  AND PEP.IC_PYME = PY.IC_PYME"   +
					"  AND PEP.IC_EPO = D.IC_EPO"   +
					"  AND LC.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES"   +
					"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"  AND PN.IC_PRODUCTO_NAFIN = 4"   +
					"  AND PEP.IC_PRODUCTO_NAFIN = 4"   +
					//"  AND PEP.CS_HABILITADO = 'S'" +
					condicion);					

			if("D".equals(tipo_credito))
				qrySentencia = qryDM;
			else if("C".equals(tipo_credito))
				qrySentencia = qryCCC;
			else
				qrySentencia.append(qryDM.toString() +" UNION ALL " +qryCCC.toString());
					
		System.out.println("el query queda de la siguiente manera "+qrySentencia.toString());
		
		log.info("getDocumentSummaryQueryForIds (S) "); 
		return qrySentencia.toString();
		
		
	}
	
	public String getDocumentQuery(HttpServletRequest request) {
	
		log.info("getDocumentQuery (E)");
	
		StringBuffer qrySentencia	= new StringBuffer();
		StringBuffer qryDM			= new StringBuffer();
		StringBuffer qryCCC			= new StringBuffer();
		StringBuffer condicion		= new StringBuffer();
		String			condicionDstoA	= "";
		String			tablasDstoA		= "";
		String	fechaHoy					= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String	ic_epo					=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String	ic_pyme					=	(request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String	ic_if						=	(request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String	ic_estatus				=	(request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto");
		String 	ig_numero_docto		=	(request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String 	fecha_seleccion_de	=	(request.getParameter("fecha_seleccion_de")==null)?"":request.getParameter("fecha_seleccion_de");
		String 	fecha_seleccion_a		=	(request.getParameter("fecha_seleccion_a")==null)?"":request.getParameter("fecha_seleccion_a");
		String 	cc_acuse					=	(request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String 	monto_credito_de		=	(request.getParameter("monto_credito_de")==null)?"":request.getParameter("monto_credito_de");
		String 	monto_credito_a		=	(request.getParameter("monto_credito_a")==null)?"":request.getParameter("monto_credito_a");
		String 	fecha_emision_de		=	(request.getParameter("fecha_emision_de")==null)?"":request.getParameter("fecha_emision_de");
		String 	fecha_emision_a		=	(request.getParameter("fecha_emision_a")==null)?"":request.getParameter("fecha_emision_a");
		String 	fecha_vto_de			=	(request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String 	fecha_vto_a				=	(request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String 	fecha_vto_credito_de	=	(request.getParameter("fecha_vto_credito_de")==null)?"":request.getParameter("fecha_vto_credito_de");
		String 	fecha_vto_credito_a 	=	(request.getParameter("fecha_vto_credito_a")==null)?"":request.getParameter("fecha_vto_credito_a");
		String 	ic_tipo_cobro_int		=	(request.getParameter("ic_tipo_cobro_int")==null)?"":request.getParameter("ic_tipo_cobro_int");
		String	ic_moneda				=	(request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String 	fn_monto_de				=	(request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String 	fn_monto_a				=	(request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String 	monto_con_descuento	=	(request.getParameter("monto_con_descuento")==null)?"":"checked";
		String 	solo_cambio				=	(request.getParameter("solo_cambio")==null)?"":request.getParameter("solo_cambio");		
		String	modo_plazo				=	(request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
		String	tipo_credito			=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String	ic_documento			=	(request.getParameter("ic_documento")==null)?"":request.getParameter("ic_documento");
		String 	fecha_publicacion_de	=	(request.getParameter("fecha_publicacion_de")==null)?fechaHoy:request.getParameter("fecha_publicacion_de");
		String 	fecha_publicacion_a	=	(request.getParameter("fecha_publicacion_a")==null)?fechaHoy:request.getParameter("fecha_publicacion_a");
		String 	chk_proc_desc		=	(request.getParameter("chk_proc_desc")==null)?"":request.getParameter("chk_proc_desc");
		String 	tipoSolic				= "";
		String	ic_estatus_docto		= "";
		
			
		
		try {		

			if(!"".equals(ic_estatus)){
				ic_estatus_docto = ic_estatus.substring(1,ic_estatus.length());
				tipoSolic		 = ic_estatus.substring(0,1);
			}
	
		if(!"".equals(ic_epo)&&ic_epo!=null)
			condicion.append(" AND D.IC_EPO = "+ic_epo);
		if(!"".equals(ic_pyme)&&ic_pyme!=null)
			condicion.append(" AND D.IC_PYME = "+ic_pyme);
		if("1".equals(ic_estatus_docto)||"2".equals(ic_estatus_docto)||"3".equals(ic_estatus_docto)||"4".equals(ic_estatus_docto)||"10".equals(ic_estatus_docto))
			condicion.append(" AND S.IC_ESTATUS_SOLIC ="+ic_estatus_docto);
		if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
			condicion.append(" and D.ig_numero_docto = '"+ig_numero_docto+"'");
		if(!"".equals(fecha_seleccion_de)&& fecha_seleccion_de!=null&&!"".equals(fecha_seleccion_a)&& fecha_seleccion_a!=null)
			condicion.append(" and trunc(DS.df_fecha_seleccion) between trunc(to_date('"+fecha_seleccion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_seleccion_a+"','dd/mm/yyyy'))");
		if(!"".equals(cc_acuse)&& cc_acuse!=null)
			condicion.append(" and D.cc_acuse = '"+cc_acuse+"'");
		if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null)
			condicion.append(" and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))");
		if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
			condicion.append(" and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
		if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null)
			condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date('"+fecha_vto_credito_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_credito_a+"','dd/mm/yyyy'))");
		if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null)
			condicion.append(" AND LC.ic_tipo_cobro_interes = "+ic_tipo_cobro_int);
		if(!"".equals(ic_moneda)&&ic_moneda!=null)
			condicion.append(" and M.ic_moneda = "+ic_moneda);
		if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
			condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de+" and "+fn_monto_a);
		if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
			condicion.append(" and D.fn_monto between "+fn_monto_de+" and "+fn_monto_a);
		if(!"".equals(solo_cambio)&& solo_cambio!=null)
			condicion.append(" and D.ic_documento in (select ic_documento from dis_cambio_estatus)");  
		if(!"".equals(modo_plazo)&& modo_plazo!=null)
			condicion.append(" and D.ic_tipo_financiamiento = "+modo_plazo);
		if(!"".equals(ic_documento)&&ic_documento!=null)
			condicion.append(" AND D.IC_DOCUMENTO = "+ic_documento);
		if(!"".equals(ic_if)&&ic_if!=null)
			condicion.append(" AND LC.IC_IF = "+ic_if);
		if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
			condicion.append(" and trunc(D.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))");
		if(!"".equals(monto_credito_de)&& monto_credito_de!=null&&!"".equals(monto_credito_a)&& monto_credito_a!=null)
			condicion.append(" and (D.fn_monto-(fn_monto*(fn_porc_descuento/100))) between "+monto_credito_de+" and "+monto_credito_a);
		if("".equals(ic_estatus_docto))
			condicion.append(" and S.ic_estatus_solic in (1,2,3,4,10)");
		if("S".equals(chk_proc_desc)) {
			condicion.append(" AND D.IC_ESTATUS_DOCTO in (4,11)");
			condicionDstoA	= 
				" AND SUBSTR (ds.cc_acuse, 1, 2) = 'D5' ";
		}
		if("D".equals(tipoSolic))
			condicion.append(" and D.ic_estatus_docto is null ");		

		if(!"".equals(ic_tipo_Pago)) {
			condicion.append("AND d.ig_tipo_pago ="+ic_tipo_Pago);   
		}
			
		qryDM.append(
					"   SELECT /*+use_nl(d,ds,s,lc,m,pep,e)"   +
					"     index(pep cp_comrel_pyme_epo_x_prod_pk)*/"   +
					" 	 d.ic_documento"   +
					"   FROM DIS_DOCUMENTO D"   +
					"   ,DIS_DOCTO_SELECCIONADO DS"   +
					"   ,DIS_SOLICITUD S"   +
					"   ,DIS_LINEA_CREDITO_DM LC"   +
					"   ,COMCAT_MONEDA M"   +
					"   ,COMREL_PYME_EPO_X_PRODUCTO PEP"   +
					"   ,COMCAT_EPO E"   +
					" ,  comcat_tipo_financiamiento tf  "+
					"   WHERE D.IC_EPO = E.IC_EPO"   +
					"   AND D.IC_MONEDA = M.IC_MONEDA"   +
					"   AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM"   +
					"   AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"   AND D.IC_PYME = PEP.IC_PYME"   +
					"   AND D.IC_EPO = PEP.IC_EPO"   +
					"   AND D.IC_PRODUCTO_NAFIN = PEP.IC_PRODUCTO_NAFIN"   +
					"   AND PEP.IC_PRODUCTO_NAFIN = 4 "+condicionDstoA+" "+
					//"   AND PEP.CS_HABILITADO = 'S'"  +
					"   AND E.CS_HABILITADO = 'S'"  +
					"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento  "+ 
					condicion);

			qryCCC.append(
					"   SELECT /*+ use_nl(d,ds,s,lc,m,pep,e)"   +
					"       index(d in_dis_documento_03_nuk)"   +
					"       index(pep cp_comrel_pyme_epo_x_prod_pk)*/"   +
					" 	  d.ic_documento"   +
					"   FROM DIS_DOCUMENTO D"   +
					"   ,DIS_DOCTO_SELECCIONADO DS"   +
					"   ,DIS_SOLICITUD S"   +
					"   ,COM_LINEA_CREDITO LC"   +
					"   ,COMCAT_MONEDA M"   +
					"   ,COMREL_PYME_EPO_X_PRODUCTO PEP"   +
					"   ,COMCAT_EPO E"   +					
					" ,  comcat_tipo_financiamiento tf  "+
					"   WHERE D.IC_EPO = E.IC_EPO"   +
					"   AND D.IC_MONEDA = M.IC_MONEDA"   +
					"   AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO"   +
					"   AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"   AND D.IC_PYME = PEP.IC_PYME"   +
					"   AND D.IC_EPO = PEP.IC_EPO"   +
					"   AND D.IC_PRODUCTO_NAFIN = PEP.IC_PRODUCTO_NAFIN"   +
					"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"   AND PEP.IC_PRODUCTO_NAFIN = 4 "+
					//"   AND PEP.CS_HABILITADO = 'S'"  +
					"   AND E.CS_HABILITADO = 'S'"  +
					"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento  "+ 
					condicion);					

			if("D".equals(tipo_credito))
				qrySentencia = qryDM;
			else if("C".equals(tipo_credito))
				qrySentencia = qryCCC;
			else
				qrySentencia.append(qryDM.toString() +" UNION ALL " +qryCCC.toString());

			System.out.println("EL query de la llave primaria: "+qrySentencia.toString());
		}catch(Exception e){
			System.out.println("InfLinCredNafinDist::getDocumentQueryException "+e);
		}
		log.info("getDocumentQuery (S)"); 
		return qrySentencia.toString();		 
		
	}
	
	public String getDocumentQueryFile(HttpServletRequest request) {
	
		log.info("getDocumentQueryFile (E)");
		
		StringBuffer qrySentencia	= new StringBuffer();
		StringBuffer qryDM			= new StringBuffer();
		StringBuffer qryCCC			= new StringBuffer();
		StringBuffer condicion		= new StringBuffer();
		String			condicionDstoA	= "";
		String			tablasDstoA		= "";
		String	fechaHoy					= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		String	ic_epo					=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String	ic_pyme					=	(request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String	ic_if						=	(request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String	ic_estatus				=	(request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto");
		String 	ig_numero_docto		=	(request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String 	fecha_seleccion_de	=	(request.getParameter("fecha_seleccion_de")==null)?"":request.getParameter("fecha_seleccion_de");
		String 	fecha_seleccion_a		=	(request.getParameter("fecha_seleccion_a")==null)?"":request.getParameter("fecha_seleccion_a");
		String 	cc_acuse					=	(request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String 	monto_credito_de		=	(request.getParameter("monto_credito_de")==null)?"":request.getParameter("monto_credito_de");
		String 	monto_credito_a		=	(request.getParameter("monto_credito_a")==null)?"":request.getParameter("monto_credito_a");
		String 	fecha_emision_de		=	(request.getParameter("fecha_emision_de")==null)?"":request.getParameter("fecha_emision_de");
		String 	fecha_emision_a		=	(request.getParameter("fecha_emision_a")==null)?"":request.getParameter("fecha_emision_a");
		String 	fecha_vto_de			=	(request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String 	fecha_vto_a				=	(request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String 	fecha_vto_credito_de	=	(request.getParameter("fecha_vto_credito_de")==null)?"":request.getParameter("fecha_vto_credito_de");
		String 	fecha_vto_credito_a 	=	(request.getParameter("fecha_vto_credito_a")==null)?"":request.getParameter("fecha_vto_credito_a");
		String 	ic_tipo_cobro_int		=	(request.getParameter("ic_tipo_cobro_int")==null)?"":request.getParameter("ic_tipo_cobro_int");
		String	ic_moneda				=	(request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String 	fn_monto_de				=	(request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String 	fn_monto_a				=	(request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String 	monto_con_descuento	=	(request.getParameter("monto_con_descuento")==null)?"":"checked";
		String 	solo_cambio				=	(request.getParameter("solo_cambio")==null)?"":request.getParameter("solo_cambio");
		String	modo_plazo				=	(request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
		String	tipo_credito			=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String	ic_documento			=	(request.getParameter("ic_documento")==null)?"":request.getParameter("ic_documento");
		String 	fecha_publicacion_de	=	(request.getParameter("fecha_publicacion_de")==null)?fechaHoy:request.getParameter("fecha_publicacion_de");
		String 	fecha_publicacion_a	=	(request.getParameter("fecha_publicacion_a")==null)?fechaHoy:request.getParameter("fecha_publicacion_a");
		String 	chk_proc_desc		=	(request.getParameter("chk_proc_desc")==null)?"":request.getParameter("chk_proc_desc");
		String 	tipoSolic				= "";
		String	ic_estatus_docto		= "";
		try {		

			if(!"".equals(ic_estatus)){
				ic_estatus_docto = ic_estatus.substring(1,ic_estatus.length());
				tipoSolic		 = ic_estatus.substring(0,1);
			}
	
		if(!"".equals(ic_epo)&&ic_epo!=null)
			condicion.append(" AND D.IC_EPO = "+ic_epo);
		if(!"".equals(ic_pyme)&&ic_pyme!=null)
			condicion.append(" AND D.IC_PYME = "+ic_pyme);
		if("1".equals(ic_estatus_docto)||"2".equals(ic_estatus_docto)||"3".equals(ic_estatus_docto)||"4".equals(ic_estatus_docto))
			condicion.append(" AND S.IC_ESTATUS_SOLIC ="+ic_estatus_docto);
		if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
			condicion.append(" and D.ig_numero_docto = '"+ig_numero_docto+"'");
		if(!"".equals(fecha_seleccion_de)&& fecha_seleccion_de!=null&&!"".equals(fecha_seleccion_a)&& fecha_seleccion_a!=null)
			condicion.append(" and trunc(DS.df_fecha_seleccion) between trunc(to_date('"+fecha_seleccion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_seleccion_a+"','dd/mm/yyyy'))");
		if(!"".equals(cc_acuse)&& cc_acuse!=null)
			condicion.append(" and D.cc_acuse = '"+cc_acuse+"'");
		if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null)
			condicion.append(" and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))");
		if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
			condicion.append(" and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
		if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null)
			condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date('"+fecha_vto_credito_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_credito_a+"','dd/mm/yyyy'))");
		if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null)
			condicion.append(" AND LC.ic_tipo_cobro_interes = "+ic_tipo_cobro_int);
		if(!"".equals(ic_moneda)&&ic_moneda!=null)
			condicion.append(" and M.ic_moneda = "+ic_moneda);
		if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
			condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de+" and "+fn_monto_a);
		if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
			condicion.append(" and D.fn_monto between "+fn_monto_de+" and "+fn_monto_a);
		if(!"".equals(solo_cambio)&& solo_cambio!=null)
			condicion.append(" and D.ic_documento in (select ic_documento from dis_cambio_estatus)");
		if(!"".equals(modo_plazo)&& modo_plazo!=null)
			condicion.append(" and TF.ic_tipo_financiamiento = "+modo_plazo);
		if(!"".equals(ic_documento)&&ic_documento!=null)
			condicion.append(" AND D.IC_DOCUMENTO = "+ic_documento);
		if(!"".equals(ic_if)&&ic_if!=null)
			condicion.append(" AND LC.IC_IF = "+ic_if);
		if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
			condicion.append(" and trunc(D.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))");
		if(!"".equals(monto_credito_de)&& monto_credito_de!=null&&!"".equals(monto_credito_a)&& monto_credito_a!=null)
			condicion.append(" and (D.fn_monto-(fn_monto*(fn_porc_descuento/100))) between "+monto_credito_de+" and "+monto_credito_a);
		if("".equals(ic_estatus_docto))
			condicion.append(" and S.ic_estatus_solic in (1,2,3,4)");
		if("S".equals(chk_proc_desc)) {
			condicion.append(" AND D.IC_ESTATUS_DOCTO in (4,11)");
			condicionDstoA	= 
				" AND SUBSTR (ds.cc_acuse, 1, 2) = 'D5' ";
		}
		if("D".equals(tipoSolic))
			condicion.append(" and D.ic_estatus_docto is null ");	
			
		if(!"".equals(ic_tipo_Pago)) {
			condicion.append("AND d.ig_tipo_pago ="+ic_tipo_Pago);   
		}

		qryDM.append(
					"  SELECT /*+ use_nl(d,ds,s,lc,tc,py,m,pe,pn,tf,ed,i,pep,ct,tci,e)"   +
					"     index(d cp_dis_documento_pk)"   +
					"     index(tf cp_comcat_tipo_finan_pk)"   +
					"     index(pep cp_comrel_pyme_epo_x_prod_pk)*/"   +
					"     PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"   +
					"  	,D.IG_NUMERO_DOCTO"   +
					"  	,D.CC_ACUSE"   +
					" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"   +
					" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"   +
					" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"   +
					" 	,D.IG_PLAZO_DOCTO"   +
					" 	,M.CD_NOMBRE AS MONEDA"   +
					" 	,D.FN_MONTO"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') AS TIPO_CONVERSION"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
					" 	,NVL(D.IG_PLAZO_DESCUENTO,'') AS IG_PLAZO_DESCUENTO"   +
					" 	,DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) AS FN_PORC_DESCUENTO"   +
					" 	,TF.CD_DESCRIPCION AS MODO_PLAZO"   +
					" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
					" 	,M.ic_moneda"   +
					" 	,D.ic_documento"   +
					" 	,I.cg_razon_social AS NOMBRE_IF"   +
					" 	,DECODE(PEP.cg_tipo_credito,'D','Descuento y/o Factoraje','Credito en Cuenta Corriente') AS TIPO_CREDITO"   +
					" 	,TO_CHAR(DS.DF_FECHA_SELECCION,'DD/MM/YYYY') AS DF_OPERACION_CREDITO"   +
					" 	,d.ig_plazo_credito AS PLAZO_CREDITO"   +
					" 	,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'DD/MM/YYYY') AS DF_VENC_CREDITO"   +
					"	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) AS REF_TASA" +
					" 	,TCI.CD_DESCRIPCION AS TIPO_COBRO_INT"   +
/*					"  	,DECODE(DS.CG_REL_MAT"   +
					" 		,'+',DS.fn_valor_tasa + DS.fn_puntos"   +
					" 		,'-',DS.fn_valor_tasa - DS.fn_puntos"   +
					" 		,'*',DS.fn_valor_tasa * DS.fn_puntos"   +
					" 		,'/',DS.fn_valor_tasa / DS.fn_puntos"   +
					" 		,0) AS FN_VALOR_TASA"   +*/
					"  ,DS.fn_valor_tasa"+					
					" 	,DS.FN_IMPORTE_INTERES"   +
					" 	,LC.ic_moneda AS MONEDA_LINEA"   +
					" 	,DS.fn_importe_recibir AS MONTO_CREDITO"   +
					" 	,E.cg_razon_social AS epo"   +
					" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
					", DECODE (d.ig_tipo_pago, '1', 'Financiamiento con intereses', '2', 'Meses sin Intereses ','3', 'Tarjeta de Cr�dito' ) AS TIPOPAGO "+ //F09-2015
		         
					"  FROM DIS_DOCUMENTO D"   +
					"  ,DIS_DOCTO_SELECCIONADO DS"   +
					"  ,DIS_SOLICITUD S"   +
					"  ,DIS_LINEA_CREDITO_DM LC"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_PYME PY"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  ,COMCAT_ESTATUS_SOLIC ED"   +
					"  ,COMCAT_IF I"   +
					"  ,COMREL_PYME_EPO_X_PRODUCTO PEP"   +
					"  ,COMCAT_TASA CT"   +
					"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"  ,COMCAT_EPO E"   +
					"  WHERE D.IC_PYME = PY.IC_PYME"   +
					"  AND D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND D.IC_EPO = E.IC_EPO"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM"   +
					"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"  AND TC.DC_FECHA IN(SELECT /*+ index(tc2 in_com_tipo_cambio_01_nuk)*/MAX(DC_FECHA) FROM COM_TIPO_CAMBIO tc2 WHERE tc2.IC_MONEDA = M.IC_MONEDA)"   +
					"  AND M.ic_moneda = TC.ic_moneda"   +
					"  AND ED.IC_ESTATUS_SOLIC = S.IC_ESTATUS_SOLIC"   +
					"  AND LC.IC_IF = I.IC_IF"   +
					"  AND PEP.IC_PYME = PY.IC_PYME"   +
					"  AND PEP.IC_EPO = D.IC_EPO"   +
					"  AND DS.IC_TASA = CT.IC_TASA"   +
					"  AND LC.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES"   +
					"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"  AND PN.IC_PRODUCTO_NAFIN = 4 "+condicionDstoA+" "+
					"  AND PEP.IC_PRODUCTO_NAFIN = 4"   +
					//"  AND PEP.CS_HABILITADO = 'S'  " +
					"  AND E.CS_HABILITADO = 'S'  " +
					condicion);

			qryCCC.append(
					"  SELECT /*+ use_nl(d,ds,s,lc,tc,py,m,pe,pn,tf,ed,i,pep,ct,tci,e)"   +
					"      index(d cp_dis_documento_pk)"   +
					"      index(tf cp_comcat_tipo_finan_pk)"   +
					"      index(pep cp_comrel_pyme_epo_x_prod_pk)*/"   +
					"  PY.CG_RAZON_SOCIAL AS NOMBRE_DIST"   +
					"  ,D.IG_NUMERO_DOCTO"   +
					"  ,D.CC_ACUSE"   +
					" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"   +
					" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"   +
					" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"   +
					" 	,D.IG_PLAZO_DOCTO"   +
					" 	,M.CD_NOMBRE AS MONEDA"   +
					" 	,D.FN_MONTO"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') AS TIPO_CONVERSION"   +
					" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
					" 	,NVL(D.IG_PLAZO_DESCUENTO,'') AS IG_PLAZO_DESCUENTO"   +
					" 	,DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) AS FN_PORC_DESCUENTO"   +
					" 	,TF.CD_DESCRIPCION AS MODO_PLAZO"   +
					" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
					" 	,M.ic_moneda"   +
					" 	,D.ic_documento"   +
					" 	,I.cg_razon_social AS NOMBRE_IF"   +
					" 	,DECODE(PEP.cg_tipo_credito,'D','Descuento y/o Factoraje','Credito en Cuenta Corriente') AS TIPO_CREDITO"   +
					" 	,TO_CHAR(DS.DF_FECHA_SELECCION,'DD/MM/YYYY') AS DF_OPERACION_CREDITO"   +
					" 	,d.ig_plazo_credito AS PLAZO_CREDITO"   +
					" 	,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'DD/MM/YYYY') AS DF_VENC_CREDITO"   +
					" 	,CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS AS REF_TASA"   +
					" 	,TCI.CD_DESCRIPCION AS TIPO_COBRO_INT"   +
/*					"  	,DECODE(DS.CG_REL_MAT"   +
					" 		,'+',DS.fn_valor_tasa + DS.fn_puntos"   +
					" 		,'-',DS.fn_valor_tasa - DS.fn_puntos"   +
					" 		,'*',DS.fn_valor_tasa * DS.fn_puntos"   +
					" 		,'/',DS.fn_valor_tasa / DS.fn_puntos"   +
					" 		,0) AS FN_VALOR_TASA"   +*/
					"  ,DS.fn_valor_tasa"+					
					" 	,DS.FN_IMPORTE_INTERES"   +
					" 	,LC.ic_moneda AS MONEDA_LINEA"   +
					" 	,DS.fn_importe_recibir AS MONTO_CREDITO"   +
					" 	,E.cg_razon_social AS epo"   +
					" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
					 ", DECODE (d.ig_tipo_pago, '1', 'Financiamiento con intereses', '2', 'Meses sin Intereses ','3', 'Tarjeta de Cr�dito' ) AS TIPOPAGO "+ //F09-2015
		                  
					"  FROM DIS_DOCUMENTO D"   +
					"  ,DIS_DOCTO_SELECCIONADO DS"   +
					"  ,DIS_SOLICITUD S"   +
					"  ,COM_LINEA_CREDITO LC"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_PYME PY"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  ,COMCAT_ESTATUS_SOLIC ED"   +
					"  ,COMCAT_IF I"   +
					"  ,COMREL_PYME_EPO_X_PRODUCTO PEP"   +
					"  ,COMCAT_TASA CT"   +
					"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"  ,COMCAT_EPO E"   +
					"  WHERE D.IC_PYME = PY.IC_PYME"   +
					"  AND D.IC_EPO = E.IC_EPO"   +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO"   +
					"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"  AND DS.IC_TASA = CT.IC_TASA"   +
					"  AND TC.DC_FECHA IN(SELECT /*+ index(tc2 in_com_tipo_cambio_01_nuk)*/MAX(DC_FECHA) FROM COM_TIPO_CAMBIO tc2 WHERE tc2.IC_MONEDA = M.IC_MONEDA)"   +
					"  AND M.ic_moneda = TC.ic_moneda"   +
					"  AND ED.IC_ESTATUS_SOLIC = S.IC_ESTATUS_SOLIC"   +
					"  AND LC.IC_IF = I.IC_IF"   +
					"  AND PEP.IC_PYME = PY.IC_PYME"   +
					"  AND PEP.IC_EPO = D.IC_EPO"   +
					"  AND LC.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES"   +
					"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"  AND PN.IC_PRODUCTO_NAFIN = 4 "+
					"  AND PEP.IC_PRODUCTO_NAFIN = 4"   +
					//"  AND PEP.CS_HABILITADO = 'S'" +
					"  AND E.CS_HABILITADO = 'S'" +
					condicion);					

			if("D".equals(tipo_credito))
				qrySentencia = qryDM;
			else if("C".equals(tipo_credito))
				qrySentencia = qryCCC;
			else
				qrySentencia.append(qryDM.toString() +" UNION ALL " +qryCCC.toString());
            
			System.out.println("getDocumentQueryFile qrySentencia: "+qrySentencia.toString());
		}catch(Exception e){
			System.out.println("InfLinCredNafinDist::getDocumentQueryFileException "+e);
		}
		log.info("getDocumentQueryFile (S)");
		return qrySentencia.toString();
		
		
	}



//***************** Fodea 011-2014 Migraci�n Nafin ******************
  
	private String paginaOffset;
	private String paginaNo;
	private List conditions;
	StringBuffer qrySentencia = new StringBuffer("");	
	private final static Log log = ServiceLocator.getInstance().getLog(InfDocCredNafinDist2.class);
	private String ic_epo;
	private String ic_documento;
	private String ic_pyme;
	private String tipo_credito;
	private String ic_if;
	private String fecha_seleccion_de;
	private String fecha_seleccion_a;
	private String monto_credito_de;
	private String monto_credito_a;
	private String ig_numero_docto;
	private String fecha_vto_credito_de;
	private String fecha_vto_credito_a;
	private String fecha_vto_de;
	private String fecha_vto_a;
	private String ic_tipo_cobro_int;
	private String fecha_publicacion_de;
	private String fecha_publicacion_a;
	private String fecha_emision_de;
	private String fecha_emision_a;
	private String ic_moneda;
	private String cc_acuse;
	private String fn_monto_de;
	private String fn_monto_a;
	private String monto_con_descuento;
	private String chk_proc_desc;
	private String solo_cambio;
	private String modo_plazo;
	private String tipoConsulta;
	private String ic_estatus;
	private int countReg;
	
	private String bins;//FODEA-013-2014
	
	StringBuffer  condicion = new StringBuffer();
	StringBuffer  qryCCC = new StringBuffer();
	StringBuffer  qryDM = new StringBuffer();	
	String condicionDstoA ="", tablasDstoA ="", tipoSolic = "", ic_estatus_docto ="";
	private String ic_tipo_Pago;
	
	
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		condicion 	= new StringBuffer();
	   String qryAux 	= "";
	   qryCCC 	= new StringBuffer();
	   qryDM 	= new StringBuffer();	
		
		if(!"".equals(ic_estatus)){
				ic_estatus_docto = ic_estatus.substring(1,ic_estatus.length());
				tipoSolic		 = ic_estatus.substring(0,1);
			}
			if(!"".equals(ic_epo)&&ic_epo!=null)
				condicion.append(" AND D.IC_EPO = "+ic_epo);
			if(!"".equals(ic_pyme)&&ic_pyme!=null)
				condicion.append(" AND D.IC_PYME = "+ic_pyme);
			if("1".equals(ic_estatus_docto)||"2".equals(ic_estatus_docto)||"3".equals(ic_estatus_docto)||"4".equals(ic_estatus_docto)||"10".equals(ic_estatus_docto))
				condicion.append(" AND S.IC_ESTATUS_SOLIC ="+ic_estatus_docto);
			if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
				condicion.append(" and D.ig_numero_docto = '"+ig_numero_docto+"'");
			if(!"".equals(fecha_seleccion_de)&& fecha_seleccion_de!=null&&!"".equals(fecha_seleccion_a)&& fecha_seleccion_a!=null)
				condicion.append(" and trunc(DS.df_fecha_seleccion) between trunc(to_date('"+fecha_seleccion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_seleccion_a+"','dd/mm/yyyy'))");
			if(!"".equals(cc_acuse)&& cc_acuse!=null)
				condicion.append(" and D.cc_acuse = '"+cc_acuse+"'");
			if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null)
				condicion.append(" and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date('"+fecha_vto_credito_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_credito_a+"','dd/mm/yyyy'))");
			if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null)
				condicion.append(" AND LC.ic_tipo_cobro_interes = "+ic_tipo_cobro_int);
			if(!"".equals(ic_moneda)&&ic_moneda!=null)
				condicion.append(" and M.ic_moneda = "+ic_moneda);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
				condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de.replaceAll(",","")+" and "+fn_monto_a.replaceAll(",",""));
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
				condicion.append(" and D.fn_monto between "+fn_monto_de.replaceAll(",","")+" and "+fn_monto_a.replaceAll(",",""));
			if(!"".equals(solo_cambio)&& solo_cambio!=null)
				condicion.append(" and D.ic_documento in (select ic_documento from dis_cambio_estatus)");
			if(!"".equals(modo_plazo)&& modo_plazo!=null)
				condicion.append(" and D.ic_tipo_financiamiento = "+modo_plazo);
			if(!"".equals(ic_documento)&&ic_documento!=null)
				condicion.append(" AND D.IC_DOCUMENTO = "+ic_documento);
			if("D".equals(tipo_credito)){
				if(!"".equals(ic_if)&&ic_if!=null)  {
					condicion.append(" AND LC.IC_IF = "+ic_if); 
				}
			}else if("C".equals(tipo_credito)){
				if(!"".equals(ic_if)&&ic_if!=null && "32".equals(ic_estatus_docto)  )  { 
					condicion.append(" AND  bins.ic_if = "+ic_if );  
				}else if(!"".equals(ic_if)&&ic_if!=null && !"32".equals(ic_estatus_docto)  )  { 
					condicion.append(" AND LC.IC_IF = "+ic_if);							
				}
			}
			 
			
			if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
				condicion.append(" and trunc(D.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))");
			if(!"".equals(monto_credito_de)&& monto_credito_de!=null&&!"".equals(monto_credito_a)&& monto_credito_a!=null)
				condicion.append(" and (D.fn_monto-(fn_monto*(fn_porc_descuento/100))) between "+monto_credito_de.replaceAll(",","")+" and "+monto_credito_a.replaceAll(",",""));
			if("".equals(ic_estatus_docto))
				condicion.append(" and S.ic_estatus_solic in (1,2,3,4,10,14)");
			if("S".equals(chk_proc_desc)) {
				condicion.append(" AND D.IC_ESTATUS_DOCTO in (4,11)");
				condicionDstoA	= 
					" AND SUBSTR (ds.cc_acuse, 1, 2) = 'D5' ";
	
			}
			if("D".equals(tipoSolic)){
					//condicion.append(" and D.ic_estatus_docto is null");		
					condicion.append(" and D.ic_estatus_docto in("+ic_estatus_docto+")");		
			}
			
			if("32".equals(ic_estatus_docto)) {
				condicion.append(" and D.ic_estatus_docto =  32 ");
			}
	
			if(!"".equals(ic_tipo_Pago)) {
				condicion.append("AND d.ig_tipo_pago ="+ic_tipo_Pago);   
			} 
		
				qryDM.append(
					"    SELECT /*+use_nl(d,ds,s,lc,m,pep,e)"   +
					"      index(pep cp_comrel_pyme_epo_x_prod_pk)*/"   +
					"        m.ic_moneda"   +
					"  	  ,m.cd_nombre"   +
					"  	  ,d.fn_monto"   +
					"    FROM DIS_DOCUMENTO D"   +
					"    ,DIS_DOCTO_SELECCIONADO DS"   +
					"    ,DIS_SOLICITUD S"   +
					"    ,DIS_LINEA_CREDITO_DM LC"   +
					"    ,COMCAT_MONEDA M"   +
					"    ,COMREL_PYME_EPO_X_PRODUCTO PEP"   +
					"    ,COMCAT_EPO E"   +
					" ,  comcat_tipo_financiamiento tf  "+
					"    WHERE D.IC_EPO = E.IC_EPO"   +
					"    AND D.IC_MONEDA = M.IC_MONEDA"   +
					"    AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM"   +
					"    AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"    AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"    AND D.IC_PYME = PEP.IC_PYME"   +
					"    AND D.IC_EPO = PEP.IC_EPO"   +
					"    AND D.IC_PRODUCTO_NAFIN = PEP.IC_PRODUCTO_NAFIN"   +
					"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento "+
					"    AND PEP.IC_PRODUCTO_NAFIN = 4 "+condicionDstoA+" "+
					//"    AND PEP.CS_HABILITADO = 'S'"  +
					"    AND E.CS_HABILITADO = 'S'"  +
						condicion.toString()
					);
						
				qryCCC.append(
					"    SELECT /*+ use_nl(d,ds,s,lc,m,pep,e)"   +
					"        index(d in_dis_documento_03_nuk)"   +
					"        index(pep cp_comrel_pyme_epo_x_prod_pk)*/"   +
					"        m.ic_moneda"   +
					"  	  ,m.cd_nombre"   +
					"  	  ,d.fn_monto"   +
					"    FROM DIS_DOCUMENTO D"   +
					"    ,DIS_DOCTO_SELECCIONADO DS"   +
					"    ,DIS_SOLICITUD S"   +
					"    ,COM_LINEA_CREDITO LC"   +
					"    ,COMCAT_MONEDA M"   +
					"    ,COMREL_PYME_EPO_X_PRODUCTO PEP"   +
					"    ,COMCAT_EPO E"   +
					" ,  comcat_tipo_financiamiento tf  "	+
					
					"	,com_bins_if bins 	"	+
					"	,dis_doctos_pago_tc dtc 	"	+
					
					"    WHERE D.IC_EPO = E.IC_EPO"   +
					"    AND D.IC_MONEDA = M.IC_MONEDA"   +
					"    AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO"   +
					"    AND d.ic_bins = bins.ic_bins(+)"   +
					"    AND d.ic_orden_pago_tc = dtc.ic_orden_pago_tc(+)"   +
					"    AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"    AND D.IC_PYME = PEP.IC_PYME"   +
					"    AND D.IC_EPO = PEP.IC_EPO"   +
					"    AND D.IC_PRODUCTO_NAFIN = PEP.IC_PRODUCTO_NAFIN"   +
					"    AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"    AND PEP.IC_PRODUCTO_NAFIN = 4 "+
					//"    AND PEP.CS_HABILITADO = 'S'"  +
					"    AND E.CS_HABILITADO = 'S'"  +
					"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento  "+ 
					condicion.toString());
					
					if("C".equals(tipo_credito) || 	"32".equals(ic_estatus_docto)) { 
						if(!"".equals(bins)){
							qryCCC.append("	and bins.ic_bins = ?	");
							conditions.add(bins);
						}
					}
					
				if("D".equals(tipo_credito))					
					qryAux = qryDM.toString();
				else if("C".equals(tipo_credito))
					qryAux = qryCCC.toString();
				else			
					qryAux = qryDM.toString()+ " UNION ALL "+qryCCC.toString();
	
				qrySentencia.append(
					"SELECT ic_moneda, cd_nombre as MONEDA , COUNT (1) as TOTAL_DOCTOS , SUM (fn_monto) as TOTAL_MONTO "+
					"  FROM ("+qryAux.toString()+")"+
					"GROUP BY  ic_moneda,cd_nombre ORDER BY ic_moneda ");
		
	
		log.debug("qrySentencia   1    "+qrySentencia); 
		log.debug("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();		
		condicion 	= new StringBuffer();
	   String qryAux 	= "";
	   qryCCC 	= new StringBuffer();
	   qryDM 	= new StringBuffer();	
		
			
		if(!"".equals(ic_estatus)){
			ic_estatus_docto = ic_estatus.substring(1,ic_estatus.length());
			tipoSolic		 = ic_estatus.substring(0,1);
		}
	
			if(!"".equals(ic_epo)&&ic_epo!=null)
				condicion.append(" AND D.IC_EPO = "+ic_epo);
			if(!"".equals(ic_pyme)&&ic_pyme!=null)
				condicion.append(" AND D.IC_PYME = "+ic_pyme);
			if("1".equals(ic_estatus_docto)||"2".equals(ic_estatus_docto)||"3".equals(ic_estatus_docto)||"4".equals(ic_estatus_docto)||"10".equals(ic_estatus_docto))
				condicion.append(" AND S.IC_ESTATUS_SOLIC ="+ic_estatus_docto);
			if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
				condicion.append(" and D.ig_numero_docto = '"+ig_numero_docto+"'");
			if(!"".equals(fecha_seleccion_de)&& fecha_seleccion_de!=null&&!"".equals(fecha_seleccion_a)&& fecha_seleccion_a!=null)
				condicion.append(" and trunc(DS.df_fecha_seleccion) between trunc(to_date('"+fecha_seleccion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_seleccion_a+"','dd/mm/yyyy'))");
			if(!"".equals(cc_acuse)&& cc_acuse!=null)
				condicion.append(" and D.cc_acuse = '"+cc_acuse+"'");
			if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null)
				condicion.append(" and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date('"+fecha_vto_credito_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_credito_a+"','dd/mm/yyyy'))");
			if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null)
				condicion.append(" AND LC.ic_tipo_cobro_interes = "+ic_tipo_cobro_int);
			if(!"".equals(ic_moneda)&&ic_moneda!=null)
				condicion.append(" and M.ic_moneda = "+ic_moneda);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
				condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de.replaceAll(",","")+" and "+fn_monto_a.replaceAll(",",""));
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
				condicion.append(" and D.fn_monto between "+fn_monto_de.replaceAll(",","")+" and "+fn_monto_a.replaceAll(",",""));
			if(!"".equals(solo_cambio)&& solo_cambio!=null)//Solo doc modificados
				condicion.append(" and D.ic_documento in (select ic_documento from dis_cambio_estatus)");
			if(!"".equals(modo_plazo)&& modo_plazo!=null)
				condicion.append(" and D.ic_tipo_financiamiento = "+modo_plazo);
			if(!"".equals(ic_documento)&&ic_documento!=null)
				condicion.append(" AND D.IC_DOCUMENTO = "+ic_documento);
				
			if("D".equals(tipo_credito)){
				if(!"".equals(ic_if)&&ic_if!=null)  {
					condicion.append(" AND LC.IC_IF = "+ic_if);  
				}
			}else if("C".equals(tipo_credito)){
				if(!"".equals(ic_if)&&ic_if!=null && "32".equals(ic_estatus_docto)  )  { 
					condicion.append(" AND  bins.ic_if = "+ic_if );  
				}else if(!"".equals(ic_if)&&ic_if!=null && !"32".equals(ic_estatus_docto)  )  { 
					condicion.append(" AND LC.IC_IF = "+ic_if);						
				}
			}
				
				
				////////////////////////////////////////////////////////////
			if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
				condicion.append(" and trunc(D.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))");
			if(!"".equals(monto_credito_de)&& monto_credito_de!=null&&!"".equals(monto_credito_a)&& monto_credito_a!=null)
				condicion.append(" and (D.fn_monto-(fn_monto*(fn_porc_descuento/100))) between "+monto_credito_de.replaceAll(",","")+" and "+monto_credito_a.replaceAll(",",""));
			if("".equals(ic_estatus_docto))
				condicion.append(" and S.ic_estatus_solic in (1,2,3,4,10,14)");
			if("S".equals(chk_proc_desc)) {
				condicion.append(" AND D.IC_ESTATUS_DOCTO in (4,11)");
				condicionDstoA	= 
					" AND SUBSTR (ds.cc_acuse, 1, 2) = 'D5' ";
			}	
			if("D".equals(tipoSolic)){
				condicion.append(" and D.ic_estatus_docto in ("+ic_estatus_docto+")");	
			}
			if("32".equals(ic_estatus_docto)) {
				condicion.append(" and D.ic_estatus_docto =  32 ");
			}
			
			if(!"".equals(ic_tipo_Pago)) {
				condicion.append("AND d.ig_tipo_pago ="+ic_tipo_Pago);   
			} 
			
			qryDM.append(
						"   SELECT /*+use_nl(d,ds,s,lc,m,pep,e)"   +
						"     index(pep cp_comrel_pyme_epo_x_prod_pk)*/"   +
						" 	 d.ic_documento"   +
						"   FROM DIS_DOCUMENTO D"   +
						"   ,DIS_DOCTO_SELECCIONADO DS"   +
						"   ,DIS_SOLICITUD S"   +
						"   ,DIS_LINEA_CREDITO_DM LC"   +
						"   ,COMCAT_MONEDA M"   +
						"   ,COMREL_PYME_EPO_X_PRODUCTO PEP"   +
						"   ,COMCAT_EPO E"   +
						" ,  comcat_tipo_financiamiento tf  "+
						
						//"	, com_bins_if bins"+/////////////////////////////////////////////////////////////////////////////////
						
						"   WHERE D.IC_EPO = E.IC_EPO"   +
						"   AND D.IC_MONEDA = M.IC_MONEDA"   +
						"   AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM"   +

						//"	 AND d.ic_bins = bins.ic_bins(+)	"+///////////////////////////////////////////////////////////////////////////

						"   AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
						"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
						"   AND D.IC_PYME = PEP.IC_PYME"   +
						"   AND D.IC_EPO = PEP.IC_EPO"   +
						"   AND D.IC_PRODUCTO_NAFIN = PEP.IC_PRODUCTO_NAFIN"   +
						"   AND PEP.IC_PRODUCTO_NAFIN = 4 "+condicionDstoA+" "+
						//"   AND PEP.CS_HABILITADO = 'S'"  +
						"   AND E.CS_HABILITADO = 'S'"  +
						"   AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento  "+ 
						condicion);
	
				qryCCC.append(
						"   SELECT /*+ use_nl(d,ds,s,lc,m,pep,e)"   +
						"       index(d in_dis_documento_03_nuk) aa"   +
						"       index(pep cp_comrel_pyme_epo_x_prod_pk)*/"   +
						" 	  d.ic_documento"   +
						"   FROM DIS_DOCUMENTO D"   +
						"   ,DIS_DOCTO_SELECCIONADO DS"   +
						"   ,DIS_SOLICITUD S"   +
						"   ,COM_LINEA_CREDITO LC"   +
						"   ,COMCAT_MONEDA M"   +
						"   ,COMREL_PYME_EPO_X_PRODUCTO PEP"   +
						"   ,COMCAT_EPO E"   +
						" ,  comcat_tipo_financiamiento tf  "+
						"	, com_bins_if bins"+/////////////////////////////////////////////////////////////////////////////////
						"	,DIS_DOCTOS_PAGO_TC dtc "+////////////////
						
						"   WHERE D.IC_EPO = E.IC_EPO"   +
						"   AND D.IC_MONEDA = M.IC_MONEDA"   +
						"   AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO"   +

						"	 AND d.ic_bins = bins.ic_bins(+)	"+///////////////////////////////////////////////////////////////////////////
						"	AND d.ic_orden_pago_tc = dtc.ic_orden_pago_tc(+)	"+
						
						"   AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
						"   AND D.IC_PYME = PEP.IC_PYME"   +
						"   AND D.IC_EPO = PEP.IC_EPO"   +
						"   AND D.IC_PRODUCTO_NAFIN = PEP.IC_PRODUCTO_NAFIN"   +
						"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
						"   AND PEP.IC_PRODUCTO_NAFIN = 4 "+
						//"   AND PEP.CS_HABILITADO = 'S'"  +
						"   AND E.CS_HABILITADO = 'S'"  +
						"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento  "+ 
						condicion);					
												
						if(!"".equals(bins) && "C".equals(tipo_credito) ){ 
							qryCCC.append("	and bins.ic_bins = ?	");
							conditions.add(bins);
						}
						
						
	
				if("D".equals(tipo_credito))				
					qrySentencia.append( qryDM.toString());
				else if("C".equals(tipo_credito))
					qrySentencia = qryCCC;
				else
					qrySentencia.append(qryDM.toString() +" UNION ALL " +qryCCC.toString());
					
		
		log.debug("qrySentencia  "+qrySentencia);
		log.debug("conditions "+conditions);
		log.info("getDocumentQuery(S)"); 
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		condicion 	= new StringBuffer();
	   String qryAux 	= "";
	   qryCCC 	= new StringBuffer();
	   qryDM 	= new StringBuffer();	
		
		if(!"".equals(ic_estatus)){
			ic_estatus_docto = ic_estatus.substring(1,ic_estatus.length());
			tipoSolic		 = ic_estatus.substring(0,1);
		}
		
		condicion.append(" AND d.ic_documento in ( ");
		for (int i = 0; i < pageIds.size(); i++) { 
			List lItem = (ArrayList)pageIds.get(i);
			if(i > 0){condicion.append(" ,   ");}
				condicion.append( new Long(lItem.get(0).toString()));
		}			
		condicion.append(" ) ");
		qryDM.append(
						"  SELECT /*+ use_nl(d,ds,s,lc,tc,py,m,pe,pn,tf,ed,i,pep,ct,tci,e)"   +
						"     index(d cp_dis_documento_pk)"   +
						"     index(tf cp_comcat_tipo_finan_pk)"   +
						"     index(pep cp_comrel_pyme_epo_x_prod_pk)*/"   +
						
						"     PY.CG_RAZON_SOCIAL AS NOMBRE_PYME"   +
						"  	,D.IG_NUMERO_DOCTO as NUM_DOCTO_INICIAL  "   +
						"  	,D.CC_ACUSE  as NUM_ACUSE_CARGA "   +
						" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS FECHA_EMISION"   +
						" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS FECHA_PUBLICACION"   +
						" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS FECHA_VENCIMIENTO"   +
						" 	,D.IG_PLAZO_DOCTO as PLAZO_DOCTO "   +
						" 	,M.CD_NOMBRE AS MONEDA"   +
						" 	,D.FN_MONTO as MONTO  "   +
						" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') AS TIPO_CONVENIO"   +
						" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
						" 	,NVL(D.IG_PLAZO_DESCUENTO,'') AS PLAZO_DESC_DIAS"   +
						" 	,DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) AS PORCENTAJE_DESC"   +
						" 	,TF.CD_DESCRIPCION AS MODALIDAD_PLAZO"   +
						" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
						" 	,M.ic_moneda as IC_MONEDA "   +
						" 	,D.ic_documento as NUM_DOCTO_FINAL "   +
						" 	,I.cg_razon_social AS NOMBRE_IF"   +
						" 	,DECODE(PEP.cg_tipo_credito,'D','Descuento y/o Factoraje','Credito en Cuenta Corriente') AS TIPO_CREDITO"   +
						" 	,TO_CHAR(DS.DF_FECHA_SELECCION,'DD/MM/YYYY') AS FECHA_OPERA_FINAL"   +
						" 	,d.ig_plazo_credito AS PLAZO_FINAL"   +
						" 	,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'DD/MM/YYYY') AS FECHA_VENC_FINAL"   +
						"	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) AS REFERENCIA_TASA_INT" +
						" 	,TCI.CD_DESCRIPCION AS TIPO_COBRO_INT"   +	
						"  ,DS.fn_valor_tasa as VALOR_TASA_INT "+
						" 	,DS.FN_IMPORTE_INTERES as MONTO_INTERES "   +
						" 	,LC.ic_moneda AS IC_MONEDA_LINEA"   +
						" 	,DS.fn_importe_recibir AS MONTO_CREDITO"   +
						" 	,E.cg_razon_social AS NOMBRE_EPO"   +
						" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
						" ,  (  ( D.FN_MONTO  - ( ( D.FN_MONTO * DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) ) /100)  ) *    DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')  )      AS monto_valuado  "+
						" , ( ( D.FN_MONTO * DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) ) /100  ) as MONTO_DESCUENTO  "+
						/*
						"	,bins.CG_DESCRIPCION as BINS "+///////////////////////////////////////////////////////////////////////////////////////////////						
						"	,(ds.FN_PORC_COMISION_APLI ) as COMISION_APLICABLE "+///////////////////////////////////////////////////////////////////////////////////////////////
						"	,((ds.FN_IMPORTE_RECIBIR *nvl(ds.FN_PORC_COMISION_APLI,0) )/100) AS monto_comision "+///////////////////////////////////////////////////////////////////////////////////////////////
						"	,(ds.FN_IMPORTE_RECIBIR-((ds.FN_IMPORTE_RECIBIR *nvl(ds.FN_PORC_COMISION_APLI,0) )/100)) as MONTO_DEPOSITAR "+///////////////////////////////////////////////////////////////////////////////////////////////												
						"	,trim(cpi.cg_opera_tarjeta) as OPERA_TARJETA "+
						*/
						//"	,'' AS cg_ventacartera, "+
						"	,'' as bins, "+
						"	0  as comision_aplicable,"+
						"	0 as monto_comision,"+
						"	0 as monto_depositar,"+
						"	'' as Opera_tarjeta	"+
						"	,'' as NUM_TC	"+
						", DECODE (d.ig_tipo_pago, '1', 'Financiamiento con intereses', '2', 'Meses sin Intereses ','3', 'Tarjeta de Cr�dito' ) AS TIPOPAGO "+ //F09-2015
						", (SELECT CASE WHEN COUNT(*) > 0 THEN 'S' ELSE 'N' END FROM COM_ARCDOCTOS WHERE CC_ACUSE = D.CC_ACUSE) AS MUESTRA_VISOR" + 
						"  FROM DIS_DOCUMENTO D"+
						"  ,DIS_DOCTO_SELECCIONADO DS" + 
						"  "+((tipoSolic.equals("C")?" ,dis_solicitud s ":""))+   
						"  ,DIS_LINEA_CREDITO_DM LC"   +
						"  ,COM_TIPO_CAMBIO TC"   +
						"  ,COMCAT_PYME PY"   +
						"  ,COMCAT_MONEDA M"   +
						"  ,COMREL_PRODUCTO_EPO PE"   +
						"  ,COMCAT_PRODUCTO_NAFIN PN"   +
						"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
						"  ,"+((tipoSolic.equals("C")?"COMCAT_ESTATUS_SOLIC ED":"COMCAT_ESTATUS_DOCTO ED"))+   
						"  ,COMCAT_IF I"   +
						"  ,COMREL_PYME_EPO_X_PRODUCTO PEP"   +
						"  ,COMCAT_TASA CT"   +
						"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
						"  ,COMCAT_EPO E"   +
						/*
						"	, com_bins_if bins"+/////////////////////////////////////////////////////////////////////////////////
						"	,comrel_producto_if cpi "+
						*/
						"  WHERE D.IC_PYME = PY.IC_PYME"   +
						"  AND D.IC_EPO = PE.IC_EPO"   +
						"  AND D.IC_MONEDA = M.IC_MONEDA"   +
						"  AND D.IC_EPO = E.IC_EPO"   +
						"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
						"  AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM"   +
						/*
						"	 AND d.ic_bins = bins.ic_bins(+)	"+///////////////////////////////////////////////////////////////////////////
						"	and lc.IC_IF = cpi.ic_if "   +
						
						"	and pep.IC_PRODUCTO_NAFIN = cpi.IC_PRODUCTO_NAFIN "   +
						*/
						"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
						((tipoSolic.equals("C")?"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO":"")) +
						"  AND TC.DC_FECHA IN(SELECT /*+ index(tc2 in_com_tipo_cambio_01_nuk)*/MAX(DC_FECHA) FROM COM_TIPO_CAMBIO tc2 WHERE tc2.IC_MONEDA = M.IC_MONEDA)"   +
						"  AND M.ic_moneda = TC.ic_moneda"   +
						((tipoSolic.equals("C")?"  AND ED.IC_ESTATUS_SOLIC = S.IC_ESTATUS_SOLIC":"  AND ED.IC_ESTATUS_DOCTO = d.IC_ESTATUS_DOCTO")) +
						"  AND LC.IC_IF = I.IC_IF"   +
						"  AND PEP.IC_PYME = PY.IC_PYME"   +
						"  AND PEP.IC_EPO = D.IC_EPO"   +
						"  AND DS.IC_TASA = CT.IC_TASA"   +
						"  AND LC.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES"   +
						"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
						"  AND PN.IC_PRODUCTO_NAFIN = 4"   +
						"  AND PEP.IC_PRODUCTO_NAFIN = 4"   +
						//"  AND PEP.CS_HABILITADO = 'S'  " +
						condicion);
	
				qryCCC.append(
						"  SELECT /*+ use_nl(d,ds,s,lc,tc,py,m,pe,pn,tf,ed,i,pep,ct,tci,e)"   +
						"      index(d cp_dis_documento_pk)"   +
						"      index(tf cp_comcat_tipo_finan_pk)"   +
						"      index(pep cp_comrel_pyme_epo_x_prod_pk)*/"   +
						
						"     PY.CG_RAZON_SOCIAL AS NOMBRE_PYME"   +
						"  	,D.IG_NUMERO_DOCTO as NUM_DOCTO_INICIAL  "   +
						"  	,D.CC_ACUSE  as NUM_ACUSE_CARGA "   +
						" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS FECHA_EMISION"   +
						" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS FECHA_PUBLICACION"   +
						" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS FECHA_VENCIMIENTO"   +
						" 	,D.IG_PLAZO_DOCTO as PLAZO_DOCTO "   +
						" 	,M.CD_NOMBRE AS MONEDA"   +
						" 	,D.FN_MONTO as MONTO  "   +
						" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') AS TIPO_CONVENIO"   +
						" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
						" 	,NVL(D.IG_PLAZO_DESCUENTO,'') AS PLAZO_DESC_DIAS"   +
						//" 	,DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) AS PORCENTAJE_DESC"   +
						" 	,NVL(D.FN_PORC_DESCUENTO,0) AS PORCENTAJE_DESC"   +
						" 	,TF.CD_DESCRIPCION AS MODALIDAD_PLAZO"   +
						" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
						" 	,M.ic_moneda as IC_MONEDA "   +
						" 	,D.ic_documento as NUM_DOCTO_FINAL "   );
						//" 	,I.cg_razon_social AS NOMBRE_IF"   +  
					
							if(tipoSolic.equals("C"))  {
							
								qryCCC.append("	,decode("+((tipoSolic.equals("C")?"ed.ic_estatus_solic":"ed.ic_estatus_docto"))+",14,"+((tipoSolic.equals("C")?"i2.cg_razon_social":"i.cg_razon_social")) +" ) AS NOMBRE_IF	"	);
							}else  {
								qryCCC.append(" ,I2.cg_razon_social AS NOMBRE_IF"   );								
							}
						
						
						qryCCC.append(" 	,DECODE(PEP.cg_tipo_credito,'D','Descuento y/o Factoraje','Credito en Cuenta Corriente') AS TIPO_CREDITO"   +
						" 	,TO_CHAR(DS.DF_FECHA_SELECCION,'DD/MM/YYYY') AS FECHA_OPERA_FINAL"   +
						" 	,d.ig_plazo_credito AS PLAZO_FINAL"   +
						" 	,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'DD/MM/YYYY') AS FECHA_VENC_FINAL"   +
						"	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) AS REFERENCIA_TASA_INT" +
						" 	,TCI.CD_DESCRIPCION AS TIPO_COBRO_INT"   +	
						"  ,DS.fn_valor_tasa as VALOR_TASA_INT "+
						" 	,DS.FN_IMPORTE_INTERES as MONTO_INTERES "   +
						" 	,LC.ic_moneda AS IC_MONEDA_LINEA"   +
						" 	,DS.fn_importe_recibir AS MONTO_CREDITO"   +
						" 	,E.cg_razon_social AS NOMBRE_EPO"   +
						" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+						
						" ,  (  ( D.FN_MONTO  - ( ( D.FN_MONTO * DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) ) /100)  ) *    DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')  )      AS monto_valuado  "+
						//" , ( ( D.FN_MONTO * DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) ) /100  ) as MONTO_DESCUENTO  "+
						"	,(  (  d.fn_monto  * DECODE (d.ic_tipo_financiamiento, 1, NVL (d.fn_porc_descuento, 0),  DECODE (d.ic_tipo_financiamiento, 3, NVL (d.fn_porc_descuento, 0),  0   )   )  )  / 100     ) AS monto_descuento	"+
						
					  " ,  BINS.CG_CODIGO_BIN||' '||bins.CG_DESCRIPCION  as BINS" +    
						
						"	,(ds.FN_PORC_COMISION_APLI ) as COMISION_APLICABLE "+///////////////////////////////////////////////////////////////////////////////////////////////
						"	,((ds.FN_IMPORTE_RECIBIR *nvl(ds.FN_PORC_COMISION_APLI,0) )/100) AS monto_comision "+///////////////////////////////////////////////////////////////////////////////////////////////
						"	,(ds.FN_IMPORTE_RECIBIR-((ds.FN_IMPORTE_RECIBIR *nvl(ds.FN_PORC_COMISION_APLI,0) )/100)) as MONTO_DEPOSITAR ");
						
						if(tipoSolic.equals("C") )  {
							qryCCC.append("  "+((tipoSolic.equals("C")?" , trim(cpi.cg_opera_tarjeta) ":" , '' "))  +" as OPERA_TARJETA" +
											  "  "+((tipoSolic.equals("C")?" ,  SUBSTR(dtc.CG_NUM_TARJETA,-4,4)  ":" ,  ''"))+  " as NUM_TC "  );						
						}	else  {					
							qryCCC.append("  , decode(ed.ic_estatus_docto ,32, trim(cpi.cg_opera_tarjeta) , '')   as OPERA_TARJETA "+
								"   , decode(ed.ic_estatus_docto ,32, SUBSTR(dtc.CG_NUM_TARJETA,-4,4) , '')    as NUM_TC ");						
						}						
						
						qryCCC.append(", DECODE (d.ig_tipo_pago, '1', 'Financiamiento con intereses', '2', 'Meses sin Intereses ','3', 'Tarjeta de Cr�dito' ) AS TIPOPAGO "); //F09-2015
						qryCCC.append(", (SELECT CASE WHEN COUNT(*) > 0 THEN 'S' ELSE 'N' END FROM COM_ARCDOCTOS WHERE CC_ACUSE = D.CC_ACUSE) AS MUESTRA_VISOR");
						qryCCC.append("  FROM DIS_DOCUMENTO D"   +
						"  ,DIS_DOCTO_SELECCIONADO DS"   +
						//"  ,DIS_SOLICITUD S"   +
						"  "+((tipoSolic.equals("C")?" ,dis_solicitud s ":""))+   
						"  ,COM_LINEA_CREDITO LC"   +
						"  ,COM_TIPO_CAMBIO TC"   +
						"  ,COMCAT_PYME PY"   +
						"  ,COMCAT_MONEDA M"   +
						"  ,COMREL_PRODUCTO_EPO PE"   +
						"  ,COMCAT_PRODUCTO_NAFIN PN"   +
						"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
						//"  ,COMCAT_ESTATUS_SOLIC ED"   +
						"  ,"+((tipoSolic.equals("C")?"COMCAT_ESTATUS_SOLIC ED":"COMCAT_ESTATUS_DOCTO ED"))+   
						"  ,COMCAT_IF I"   +
						"	,comcat_if i2"	+
						"  ,COMREL_PYME_EPO_X_PRODUCTO PEP"   +
						"  ,COMCAT_TASA CT"   +
						"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
						"  ,COMCAT_EPO E"   +
						
						"	, com_bins_if bins"+/////////////////////////////////////////////////////////////////////////////////
						"	,comrel_producto_if cpi "+
						"	,DIS_DOCTOS_PAGO_TC dtc "+
						
						"  WHERE D.IC_PYME = PY.IC_PYME"   +
						"  AND D.IC_EPO = E.IC_EPO"   +
						"  AND D.IC_MONEDA = M.IC_MONEDA"   +
						"  AND D.IC_EPO = PE.IC_EPO"   +
						"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
						"  AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO"   +
						
						"	 AND d.ic_bins = bins.ic_bins(+)	"+///////////////////////////////////////////////////////////////////////////
						"	AND d.ic_orden_pago_tc = dtc.ic_orden_pago_tc(+)	"+
						"	AND bins.ic_if = i2.ic_if(+)	"	+
						"	and lc.IC_IF = cpi.ic_if "   +
						"	and cpi.ic_producto_nafin = 4	"+
						"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
						//"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
						((tipoSolic.equals("C")?"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO":"")) +
						"  AND DS.IC_TASA = CT.IC_TASA"   +
						"  AND TC.DC_FECHA IN(SELECT /*+ index(tc2 in_com_tipo_cambio_01_nuk)*/MAX(DC_FECHA) FROM COM_TIPO_CAMBIO tc2 WHERE tc2.IC_MONEDA = M.IC_MONEDA)"   +
						"  AND M.ic_moneda = TC.ic_moneda"   +
						//"  AND ED.IC_ESTATUS_SOLIC = S.IC_ESTATUS_SOLIC"   +
						((tipoSolic.equals("C")?"  AND ED.IC_ESTATUS_SOLIC = S.IC_ESTATUS_SOLIC":"  AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO")) +
						"  AND LC.IC_IF = I.IC_IF"   +
						"  AND PEP.IC_PYME = PY.IC_PYME"   +
						"  AND PEP.IC_EPO = D.IC_EPO"   +
						"  AND LC.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES"   +
						"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
						"  AND PN.IC_PRODUCTO_NAFIN = 4"   +
						"  AND PEP.IC_PRODUCTO_NAFIN = 4"   +
						//"  AND PEP.CS_HABILITADO = 'S'" +
						condicion);					
	
				if("D".equals(tipo_credito))
					qrySentencia = qryDM;
				else if("C".equals(tipo_credito))
					qrySentencia = qryCCC;
				else
					qrySentencia.append(qryDM.toString() +" UNION ALL " +qryCCC.toString());
				
					qrySentencia.append(
							"      group by " +
							"    py.cg_razon_social,d.ig_numero_docto,d.cc_acuse,d.df_fecha_emision, " +
							"    d.df_carga,d.df_fecha_venc,d.ig_plazo_docto,m.cd_nombre,d.fn_monto, " +
							"    pe.cg_tipo_conversion, pn.cg_tipo_conversion,pe.cg_tipo_conversion, " +
							"    pn.cg_tipo_conversion,d.ig_plazo_descuento,d.ic_tipo_financiamiento, " +
							"    d.fn_porc_descuento,tf.cd_descripcion,ed.cd_descripcion,m.ic_moneda, " +
							"    d.ic_documento,i.cg_razon_social,pep.cg_tipo_credito,ds.df_fecha_seleccion, " +
							"    d.ig_plazo_credito,d.df_fecha_venc_credito,ds.cg_tipo_tasa,tc.fn_valor_compra, " +
							"    ct.cd_nombre,ds.cg_rel_mat,ds.fn_puntos,tci.cd_descripcion,ds.fn_valor_tasa , " +
							"    ds.fn_importe_interes,lc.ic_moneda,ds.fn_importe_recibir,e.cg_razon_social , " +
							"    d.cg_ventacartera , ds.fn_porc_comision_apli,  d.ig_tipo_pago    ");
									
									
							if( "C".equals(tipo_credito)) {	
							
									qrySentencia.append( " , bins.cg_descripcion, cpi.cg_opera_tarjeta, BINS.cg_codigo_bin  , dtc.CG_NUM_TARJETA "); 
									
								if(tipoSolic.equals("C") ){
									qrySentencia.append( ",   ed.ic_estatus_solic , i2.cg_razon_social  ");
								}else {		
									
									qrySentencia.append( ", ed.ic_estatus_docto , i2.cg_razon_social ");								
								} 	
							}else  if( "D".equals(tipo_credito)) {		
								qrySentencia.append( ",  i.cg_razon_social ");		
							
							}else {									
								if(tipoSolic.equals("C") ){
									qrySentencia.append( " ,  ed.ic_estatus_solic , i2.cg_razon_social  ");
								}else {		
									
									qrySentencia.append( ", ed.ic_estatus_docto , i2.cg_razon_social ");								
								}
								qrySentencia.append( " , bins.cg_descripcion, cpi.cg_opera_tarjeta, BINS.cg_codigo_bin  , dtc.CG_NUM_TARJETA ");  
							} 
				
		log.debug(" tipo_credito ::  "+tipo_credito+"  qrySentencia:::  "+qrySentencia);     
		log.debug("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");  
		return qrySentencia.toString();	
	}

	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia  = new StringBuffer();
		conditions    = new ArrayList();
		condicion     = new StringBuffer();
		qryCCC        = new StringBuffer();
		qryDM         = new StringBuffer();
		String qryAux = "";

		if(!"".equals(ic_estatus)){
			ic_estatus_docto = ic_estatus.substring(1,ic_estatus.length());
			tipoSolic = ic_estatus.substring(0,1);
		}

		if(!"".equals(ic_epo)&&ic_epo!=null)
			condicion.append(" AND D.IC_EPO = "+ic_epo);
		if(!"".equals(ic_pyme)&&ic_pyme!=null)
			condicion.append(" AND D.IC_PYME = "+ic_pyme);
		if("1".equals(ic_estatus_docto)||"2".equals(ic_estatus_docto)||"3".equals(ic_estatus_docto)||"4".equals(ic_estatus_docto)||"10".equals(ic_estatus_docto))
			condicion.append(" AND S.IC_ESTATUS_SOLIC ="+ic_estatus_docto);
		if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
			condicion.append(" and D.ig_numero_docto = '"+ig_numero_docto+"'");
		if(!"".equals(fecha_seleccion_de)&& fecha_seleccion_de!=null&&!"".equals(fecha_seleccion_a)&& fecha_seleccion_a!=null)
			condicion.append(" and trunc(DS.df_fecha_seleccion) between trunc(to_date('"+fecha_seleccion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_seleccion_a+"','dd/mm/yyyy'))");
		if(!"".equals(cc_acuse)&& cc_acuse!=null)
			condicion.append(" and D.cc_acuse = '"+cc_acuse+"'");
		if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null)
			condicion.append(" and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))");
		if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
			condicion.append(" and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
		if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null)
			condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date('"+fecha_vto_credito_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_credito_a+"','dd/mm/yyyy'))");
		if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null)
			condicion.append(" AND LC.ic_tipo_cobro_interes = "+ic_tipo_cobro_int);
		if(!"".equals(ic_moneda)&&ic_moneda!=null)
			condicion.append(" and M.ic_moneda = "+ic_moneda);
		if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
			condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de.replaceAll(",","")+" and "+fn_monto_a.replaceAll(",",""));
		if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
			condicion.append(" and D.fn_monto between "+fn_monto_de.replaceAll(",","")+" and "+fn_monto_a.replaceAll(",",""));
		if(!"".equals(solo_cambio)&& solo_cambio!=null)
			condicion.append(" and D.ic_documento in (select ic_documento from dis_cambio_estatus)");
		if(!"".equals(modo_plazo)&& modo_plazo!=null)
			condicion.append(" and D.ic_tipo_financiamiento = "+modo_plazo);
		if(!"".equals(ic_documento)&&ic_documento!=null)
			condicion.append(" AND D.IC_DOCUMENTO = "+ic_documento);

		if("D".equals(tipo_credito)){
			if(!"".equals(ic_if)&&ic_if!=null){
				condicion.append(" AND LC.IC_IF = "+ic_if);
			}
		} else if("C".equals(tipo_credito)){
			if(!"".equals(ic_if)&&ic_if!=null && "32".equals(ic_estatus_docto)){
				condicion.append(" AND  bins.ic_if = "+ic_if );
			} else if(!"".equals(ic_if)&&ic_if!=null && !"32".equals(ic_estatus_docto)){
				condicion.append(" AND LC.IC_IF = "+ic_if);
			}
		}

		if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
			condicion.append(" and trunc(D.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))");
		if(!"".equals(monto_credito_de)&& monto_credito_de!=null&&!"".equals(monto_credito_a)&& monto_credito_a!=null)
			condicion.append(" and (D.fn_monto-(fn_monto*(fn_porc_descuento/100))) between "+monto_credito_de.replaceAll(",","")+" and "+monto_credito_a.replaceAll(",",""));
		if("".equals(ic_estatus_docto))
			condicion.append(" and S.ic_estatus_solic in (1,2,3,4,10,14)");
		if("S".equals(chk_proc_desc)) {
			condicion.append(" AND D.IC_ESTATUS_DOCTO in (4,11)");
			condicionDstoA = " AND SUBSTR (ds.cc_acuse, 1, 2) = 'D5' ";
		}
		if("D".equals(tipoSolic))
			condicion.append(" and D.ic_estatus_docto ="+ic_estatus_docto+" ");

		if("32".equals(ic_estatus_docto)) {
			condicion.append(" and D.ic_estatus_docto =  32 ");
		}

	   if(!"".equals(ic_tipo_Pago)) {
			condicion.append("AND d.ig_tipo_pago ="+ic_tipo_Pago);   
		} 
	         
				
		qryDM.append(" SELECT /*+ use_nl(d,ds,s,lc,tc,py,m,pe,pn,tf,ed,i,pep,ct,tci,e)");
		qryDM.append(" index(d cp_dis_documento_pk)");
		qryDM.append(" index(tf cp_comcat_tipo_finan_pk)");
		qryDM.append(" index(pep cp_comrel_pyme_epo_x_prod_pk)*/");
		qryDM.append(" distinct  PY.CG_RAZON_SOCIAL AS NOMBRE_PYME");
		qryDM.append(" ,D.IG_NUMERO_DOCTO as NUM_DOCTO_INICIAL ");
		qryDM.append(" ,D.CC_ACUSE  as NUM_ACUSE_CARGA ");
		qryDM.append(" ,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS FECHA_EMISION");
		qryDM.append(" ,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS FECHA_PUBLICACION");
		qryDM.append(" ,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS FECHA_VENCIMIENTO");
		qryDM.append(" ,D.IG_PLAZO_DOCTO as PLAZO_DOCTO ");
		qryDM.append(" ,M.CD_NOMBRE AS MONEDA");
		qryDM.append(" ,D.FN_MONTO as MONTO ");
		qryDM.append(" ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') AS TIPO_CONVENIO");
		qryDM.append(" ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO");
		qryDM.append(" ,NVL(D.IG_PLAZO_DESCUENTO,'') AS PLAZO_DESC_DIAS");
		qryDM.append(" ,DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) AS PORCENTAJE_DESC");
		qryDM.append(" ,TF.CD_DESCRIPCION AS MODALIDAD_PLAZO");
		qryDM.append(" ,CED.CD_DESCRIPCION AS ESTATUS");
		qryDM.append(" ,M.ic_moneda as IC_MONEDA ");
		qryDM.append(" ,D.ic_documento as NUM_DOCTO_FINAL ");
		qryDM.append(" ,I.cg_razon_social AS NOMBRE_IF");
		qryDM.append(" ,DECODE(PEP.cg_tipo_credito,'D','Descuento y/o Factoraje','Credito en Cuenta Corriente') AS TIPO_CREDITO");
		qryDM.append(" ,TO_CHAR(DS.DF_FECHA_SELECCION,'DD/MM/YYYY') AS FECHA_OPERA_FINAL");
		qryDM.append(" ,d.ig_plazo_credito AS PLAZO_FINAL");
		qryDM.append(" ,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'DD/MM/YYYY') AS FECHA_VENC_FINAL");
		qryDM.append(" ,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) AS REFERENCIA_TASA_INT");
		qryDM.append(" ,TCI.CD_DESCRIPCION AS TIPO_COBRO_INT");
		qryDM.append(" ,DS.fn_valor_tasa as VALOR_TASA_INT ");
		qryDM.append(" ,DS.FN_IMPORTE_INTERES as MONTO_INTERES ");
		qryDM.append(" ,LC.ic_moneda AS IC_MONEDA_LINEA");
		qryDM.append(" ,DS.fn_importe_recibir AS MONTO_CREDITO");
		qryDM.append(" ,E.cg_razon_social AS NOMBRE_EPO");
		qryDM.append(" ,d.CG_VENTACARTERA as CG_VENTACARTERA ");
		qryDM.append(" , ((D.FN_MONTO - ((D.FN_MONTO * DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0)) /100)) * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) AS monto_valuado  ");
		qryDM.append(" , ( ( D.FN_MONTO * DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) ) /100  ) as MONTO_DESCUENTO  ");
		qryDM.append(" ,'' as bins ");
		qryDM.append(" ,0  as comision_aplicable");
		qryDM.append(" ,0 as monto_comision");
		qryDM.append(" ,0 as monto_depositar");
		qryDM.append(" ,'' as Opera_tarjeta ");
		qryDM.append(" ,'' as IC_ESTATUS");
		qryDM.append(" ,'' as NUM_TC ");
		qryDM.append( ", DECODE (d.ig_tipo_pago, '1', 'Financiamiento con intereses', '2', 'Meses sin Intereses ','3', 'Tarjeta de Cr�dito' ) AS TIPOPAGO ");
	   					
		qryDM.append(" FROM DIS_DOCUMENTO D");
		qryDM.append(" ,DIS_DOCTO_SELECCIONADO DS");
		qryDM.append(" ,DIS_SOLICITUD S");
		qryDM.append(" ,DIS_LINEA_CREDITO_DM LC");
		qryDM.append(" ,COM_TIPO_CAMBIO TC");
		qryDM.append(" ,COMCAT_PYME PY");
		qryDM.append(" ,COMCAT_MONEDA M");
		qryDM.append(" ,COMREL_PRODUCTO_EPO PE");
		qryDM.append(" ,COMCAT_PRODUCTO_NAFIN PN");
		qryDM.append(" ,COMCAT_TIPO_FINANCIAMIENTO TF");
		qryDM.append(" ,COMCAT_ESTATUS_SOLIC ED");
		qryDM.append(" ,COMCAT_IF I");
		qryDM.append(" ,COMREL_PYME_EPO_X_PRODUCTO PEP");
		qryDM.append(" ,COMCAT_TASA CT");
		qryDM.append(" ,COMCAT_TIPO_COBRO_INTERES TCI");
		qryDM.append(" ,COMCAT_EPO E");
		qryDM.append(" ,comcat_estatus_docto ced ");
		qryDM.append(" WHERE D.IC_PYME = PY.IC_PYME");
		qryDM.append(" AND D.IC_EPO = PE.IC_EPO");
		qryDM.append(" AND D.IC_MONEDA = M.IC_MONEDA");
		qryDM.append(" AND D.IC_EPO = E.IC_EPO");
		qryDM.append(" AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO");
		qryDM.append(" AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM");
		qryDM.append(" AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO");
		qryDM.append(" AND D.ic_estatus_docto = ced.ic_estatus_docto ");
		qryDM.append(" AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO");
		qryDM.append(" AND M.ic_moneda = TC.ic_moneda");
		qryDM.append(" AND ED.IC_ESTATUS_SOLIC = S.IC_ESTATUS_SOLIC");
		qryDM.append(" AND LC.IC_IF = I.IC_IF");
		qryDM.append(" AND PEP.IC_PYME = PY.IC_PYME");
		qryDM.append(" AND PEP.IC_EPO = D.IC_EPO");
		qryDM.append(" AND DS.IC_TASA = CT.IC_TASA");
		qryDM.append(" AND LC.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES (+)");
		qryDM.append(" AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN");
		qryDM.append(" AND PN.IC_PRODUCTO_NAFIN = 4 "+condicionDstoA+" ");
		qryDM.append(" AND PEP.IC_PRODUCTO_NAFIN = 4");
		//qryDM.append(" AND PEP.CS_HABILITADO = 'S'  ");
		qryDM.append(condicion);

		qryCCC.append(" SELECT /*+ use_nl(d,ds,s,lc,tc,py,m,pe,pn,tf,ed,i,pep,ct,tci,e)");
		qryCCC.append(" index(d cp_dis_documento_pk)");
		qryCCC.append(" index(tf cp_comcat_tipo_finan_pk)");
		qryCCC.append(" index(pep cp_comrel_pyme_epo_x_prod_pk)*/");
		qryCCC.append(" distinct PY.CG_RAZON_SOCIAL AS NOMBRE_PYME");
		qryCCC.append(" ,D.IG_NUMERO_DOCTO as NUM_DOCTO_INICIAL  ");
		qryCCC.append(" ,D.CC_ACUSE  as NUM_ACUSE_CARGA ");
		qryCCC.append(" ,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS FECHA_EMISION");
		qryCCC.append(" ,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS FECHA_PUBLICACION");
		qryCCC.append(" ,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS FECHA_VENCIMIENTO");
		qryCCC.append(" ,D.IG_PLAZO_DOCTO as PLAZO_DOCTO ");
		qryCCC.append(" ,M.CD_NOMBRE AS MONEDA");
		qryCCC.append(" ,D.FN_MONTO as MONTO  ");
		qryCCC.append(" ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','Sin conversion','P','Dolar-Peso','') AS TIPO_CONVENIO");
		qryCCC.append(" ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO");
		qryCCC.append(" ,NVL(D.IG_PLAZO_DESCUENTO,'') AS PLAZO_DESC_DIAS");
		qryCCC.append(" ,DECODE (d.ic_tipo_financiamiento, 1, NVL (d.fn_porc_descuento, 0),DECODE (d.ic_tipo_financiamiento, 3, NVL (d.fn_porc_descuento, 0),0)) AS porcentaje_desc ");
		qryCCC.append(" ,TF.CD_DESCRIPCION AS MODALIDAD_PLAZO");
		qryCCC.append(" ,CED.CD_DESCRIPCION AS ESTATUS");
		qryCCC.append(" ,M.ic_moneda as IC_MONEDA ");
		qryCCC.append(" ,D.ic_documento as NUM_DOCTO_FINAL ");
		qryCCC.append(" ,decode(ed.ic_estatus_solic,14,i2.cg_razon_social,i.cg_razon_social) AS NOMBRE_IF ");
		qryCCC.append(" ,DECODE(PEP.cg_tipo_credito,'D','Descuento y/o Factoraje','Credito en Cuenta Corriente') AS TIPO_CREDITO");
		qryCCC.append(" ,TO_CHAR(DS.DF_FECHA_SELECCION,'DD/MM/YYYY') AS FECHA_OPERA_FINAL");
		qryCCC.append(" ,d.ig_plazo_credito AS PLAZO_FINAL");
		qryCCC.append(" ,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'DD/MM/YYYY') AS FECHA_VENC_FINAL");
		qryCCC.append(" ,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) AS REFERENCIA_TASA_INT");
		qryCCC.append(" ,TCI.CD_DESCRIPCION AS TIPO_COBRO_INT");
		qryCCC.append(" ,DS.fn_valor_tasa as VALOR_TASA_INT ");
		qryCCC.append(" ,DS.FN_IMPORTE_INTERES as MONTO_INTERES ");
		qryCCC.append(" ,LC.ic_moneda AS IC_MONEDA_LINEA");
		qryCCC.append(" ,DS.fn_importe_recibir AS MONTO_CREDITO");
		qryCCC.append(" ,E.cg_razon_social AS NOMBRE_EPO");
		qryCCC.append(" ,d.CG_VENTACARTERA as CG_VENTACARTERA ");
		qryCCC.append(" ,  (  ( D.FN_MONTO  - ( ( D.FN_MONTO * DECODE(D.ic_tipo_financiamiento,1,NVL(D.FN_PORC_DESCUENTO,0),0) ) /100)  ) *    DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')  )      AS monto_valuado  ");
		qryCCC.append(" ,(  ( d.fn_monto * DECODE(d.ic_tipo_financiamiento,1,NVL(d.fn_porc_descuento, 0),  DECODE (d.ic_tipo_financiamiento, 3, NVL (d.fn_porc_descuento, 0),  0   )   )  )  / 100     ) AS monto_descuento ");
		qryCCC.append(" ,BINS.CG_CODIGO_BIN||' '||bins.CG_DESCRIPCION as BINS ");
		qryCCC.append(" ,(ds.fn_porc_comision_apli) as COMISION_APLICABLE ");
		qryCCC.append(" ,((ds.fn_importe_recibir * (NVL (ds.fn_porc_comision_apli, 0)))/100) AS monto_comision ");
		qryCCC.append(" ,(ds.fn_importe_recibir-(( ds.fn_importe_recibir * NVL(ds.fn_porc_comision_apli,0)) /100)) as MONTO_DEPOSITAR ");
		qryCCC.append(" ,trim(cpi.cg_opera_tarjeta) as OPERA_TARJETA ");
		qryCCC.append(" ,to_char(ed.IC_ESTATUS_SOLIC) as IC_ESTATUS ");
		if(tipoSolic.equals("C") ) {
			qryCCC.append("   "+((tipoSolic.equals("C")?" ,  SUBSTR(dtc.CG_NUM_TARJETA,-4,4)  ":" ,  ''"))+  " as NUM_TC " );
		} else {
			qryCCC.append(" , decode(ed.IC_ESTATUS_SOLIC ,14, SUBSTR(dtc.CG_NUM_TARJETA,-4,4) , '')    as NUM_TC ");
		}
		
	   qryCCC.append(", DECODE (d.ig_tipo_pago, '1', 'Financiamiento con intereses', '2', 'Meses sin Intereses ','3', 'Tarjeta de Cr�dito' ) AS TIPOPAGO ");
		
					
		qryCCC.append(" FROM DIS_DOCUMENTO D");
		qryCCC.append(" ,DIS_DOCTO_SELECCIONADO DS");
		qryCCC.append(" ,DIS_SOLICITUD S");
		qryCCC.append(" ,COM_LINEA_CREDITO LC");
		qryCCC.append(" ,COM_TIPO_CAMBIO TC");
		qryCCC.append(" ,COMCAT_PYME PY");
		qryCCC.append(" ,COMCAT_MONEDA M");
		qryCCC.append(" ,COMREL_PRODUCTO_EPO PE");
		qryCCC.append(" ,COMCAT_PRODUCTO_NAFIN PN");
		qryCCC.append(" ,COMCAT_TIPO_FINANCIAMIENTO TF");
		qryCCC.append(" ,COMCAT_ESTATUS_SOLIC ED");
		qryCCC.append(" ,COMCAT_IF I");
		qryCCC.append(" ,comcat_if i2");
		qryCCC.append(" ,COMREL_PYME_EPO_X_PRODUCTO PEP");
		qryCCC.append(" ,COMCAT_TASA CT");
		qryCCC.append(" ,COMCAT_TIPO_COBRO_INTERES TCI");
		qryCCC.append(" ,COMCAT_EPO E");
		qryCCC.append(" ,com_bins_if bins");
		qryCCC.append(" ,comrel_producto_if cpi ");
		qryCCC.append(" ,DIS_DOCTOS_PAGO_TC dtc ");
		qryCCC.append(" ,comcat_estatus_docto ced ");
		qryCCC.append(" WHERE D.IC_PYME = PY.IC_PYME");
		qryCCC.append(" AND D.IC_EPO = E.IC_EPO");
		qryCCC.append(" AND D.IC_MONEDA = M.IC_MONEDA");
		qryCCC.append(" AND D.IC_EPO = PE.IC_EPO");
		qryCCC.append(" AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO");
		qryCCC.append(" AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO");
		qryCCC.append(" AND d.ic_bins = bins.ic_bins(+) ");
		qryCCC.append(" AND D.ic_estatus_docto = ced.ic_estatus_docto ");
		qryCCC.append(" AND bins.ic_if = i2.ic_if(+) ");
		qryCCC.append(" and lc.IC_IF = cpi.ic_if ");
		qryCCC.append(" and cpi.IC_PRODUCTO_NAFIN  = 4 ");
		qryCCC.append(" AND d.ic_orden_pago_tc = dtc.ic_orden_pago_tc(+) ");
		qryCCC.append(" AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO");
		qryCCC.append(" AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO");
		qryCCC.append(" AND DS.IC_TASA = CT.IC_TASA");
		qryCCC.append(" AND M.ic_moneda = TC.ic_moneda");
		qryCCC.append(" AND ED.IC_ESTATUS_SOLIC = S.IC_ESTATUS_SOLIC");
		qryCCC.append(" AND LC.IC_IF = I.IC_IF");
		qryCCC.append(" AND PEP.IC_PYME = PY.IC_PYME");
		qryCCC.append(" AND PEP.IC_EPO = D.IC_EPO");
		qryCCC.append(" AND LC.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES  (+)");
		qryCCC.append(" AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN");
		qryCCC.append(" AND PN.IC_PRODUCTO_NAFIN = 4");
		qryCCC.append(" AND PEP.IC_PRODUCTO_NAFIN = 4");
		//qryCCC.append(" AND PEP.CS_HABILITADO = 'S'");
		qryCCC.append(condicion);

		if(!"".equals(bins)  && "C".equals(tipo_credito)){
			qryCCC.append("	and bins.ic_bins = ?	");
			conditions.add(bins);
		}

		if("D".equals(tipo_credito))
			qrySentencia.append(qryDM.toString());
		else if("C".equals(tipo_credito))
			qrySentencia = qryCCC;
		else
			qrySentencia.append(qryDM.toString() +" UNION ALL " +qryCCC.toString());

		log.debug(" tipo_credito ::  "+tipo_credito+"  qrySentencia:::  "+qrySentencia);
		log.debug("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();
	}

/**
 * Genera los archivos CSV y PDF sin utilizar la paginaci�n, los datos que recibe en el request vienen de la consulta
 * en el m�todo getDocumentQueryFile()
 * @param request
 * @param rs
 * @param path
 * @param tipo CSV o PDF
 * @return nombreArchivo
 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {

		log.info("crearCustomFile (E)");
		String nombreArchivo     = "";
		String nombreEpo         = "";
		String nombrePYME        = "";
		String num_acuse         = "";
		String num_docto_inicial = "";
		String fecha_publicacion = "";
		String fecha_Emision     = "";
		String fecha_vencimiento = "";
		String plazo_docto       = "";
		String moneda            = "";
		String monto             = "";
		String plazo_desc        = "";
		String porc_desc         = "";
		String monto_desc        = "";
		String tipo_convenio     = "";
		String tipo_cambio       = "";
		String monto_Valuado     = "";
		String modalidad_plazo   = "";
		String estatus           = "";
		String tipo_credito      = "";
		String num_docto_final   = "";
		String plazo_final       = "";
		String fecha_venc_final  = "";
		String fecha_opera_final = "";
		String nombre_if         = "";
		String referencia        = "";
		String valor_tasa_int    = "";
		String monto_int         = "";
		String tipo_cobro_int    = "";
		String ic_moneda_linea   = "";
		String ic_moneda         = "";
		String ventaCartera      = "";
		String comisionAplicable = "";
		String montoComision     = "";
		String montoDepositar    = "";
		String bins              = "";
		String operaTajeta       = "";
		String numTarjeta        = "";
		String monto2_1          = "";
		double monto2            = 0;

		if(tipo.equals("CSV")){

			try{
				int total = 0;
				OutputStreamWriter writer = null;
				BufferedWriter buffer = null;
				StringBuffer contenidoArchivo = new StringBuffer();

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
				buffer = new BufferedWriter(writer);

				contenidoArchivo.append(" EPO,");
				contenidoArchivo.append("Distribuidor,");
				contenidoArchivo.append("N�m. acuse carga,");
				contenidoArchivo.append("N�mero de documento inicial,");
				contenidoArchivo.append("Fecha de publicaci�n,");
				contenidoArchivo.append("Fecha de emisi�n,");
				contenidoArchivo.append("Fecha vencimiento,");
				contenidoArchivo.append("Plazo docto.,");
				contenidoArchivo.append("Moneda,");
				contenidoArchivo.append("Monto,");
				contenidoArchivo.append("Plazo para descuento en d�as,");
				contenidoArchivo.append("% de descuento,");
				contenidoArchivo.append("Monto % de descuento,");
				contenidoArchivo.append("Tipo conv.,");
				contenidoArchivo.append("Tipo cambio,");
				contenidoArchivo.append("Monto valuado en Pesos,");
				contenidoArchivo.append("Modalidad de plazo,");
			   contenidoArchivo.append("Tipo de Pago ,"          );
				contenidoArchivo.append("Estatus,");
				contenidoArchivo.append("Tipo de cr�dito,");
				contenidoArchivo.append("N�mero de documento final,");
				contenidoArchivo.append("Monto,");
				contenidoArchivo.append("% Comisi�n Aplicable de Terceros,");
				contenidoArchivo.append("Monto Comisi�n de Terceros (IVA Incluido),");
				contenidoArchivo.append("Monto a Depositar por Operaci�n,");
				contenidoArchivo.append("Plazo,");
				contenidoArchivo.append("Fecha de vencimiento,");
				contenidoArchivo.append("Fecha de operaci�n,");
				contenidoArchivo.append("IF, Nombre del Producto,");
				contenidoArchivo.append("Referencia tasa de inter�s,");
				contenidoArchivo.append("Valor tasa de inter�s,");
				contenidoArchivo.append("Monto de intereses,");
				contenidoArchivo.append("Tipo de cobro inter�s,");
				contenidoArchivo.append("N�mero Tarjeta de Cr�dito \n ");

				while (rs.next()){

					nombreEpo         = (rs.getString("NOMBRE_EPO")          == null) ? "" : rs.getString("NOMBRE_EPO");
					nombrePYME        = (rs.getString("NOMBRE_PYME")         == null) ? "" : rs.getString("NOMBRE_PYME");
					num_acuse         = (rs.getString("NUM_ACUSE_CARGA")     == null) ? "" : rs.getString("NUM_ACUSE_CARGA");
					num_docto_inicial = (rs.getString("NUM_DOCTO_INICIAL")   == null) ? "" : rs.getString("NUM_DOCTO_INICIAL");
					fecha_publicacion = (rs.getString("FECHA_PUBLICACION")   == null) ? "" : rs.getString("FECHA_PUBLICACION");
					fecha_Emision     = (rs.getString("FECHA_EMISION")       == null) ? "" : rs.getString("FECHA_EMISION");
					fecha_vencimiento = (rs.getString("FECHA_VENCIMIENTO")   == null) ? "" : rs.getString("FECHA_VENCIMIENTO");
					plazo_docto       = (rs.getString("PLAZO_DOCTO")         == null) ? "" : rs.getString("PLAZO_DOCTO");
					moneda            = (rs.getString("MONEDA")              == null) ? "" : rs.getString("MONEDA");
					monto             = (rs.getString("MONTO")               == null) ? "" : rs.getString("MONTO");
					plazo_desc        = (rs.getString("PLAZO_DESC_DIAS")     == null) ? "" : rs.getString("PLAZO_DESC_DIAS");
					porc_desc         = (rs.getString("PORCENTAJE_DESC")     == null) ? "" : rs.getString("PORCENTAJE_DESC");
					monto_desc        = (rs.getString("MONTO_DESCUENTO")     == null) ? "" : rs.getString("MONTO_DESCUENTO");
					tipo_convenio     = (rs.getString("TIPO_CONVENIO")       == null) ? "" : rs.getString("TIPO_CONVENIO");
					tipo_cambio       = (rs.getString("TIPO_CAMBIO")         == null) ? "" : rs.getString("TIPO_CAMBIO");
					monto_Valuado     = (rs.getString("MONTO_VALUADO")       == null) ? "" : rs.getString("MONTO_VALUADO");
					modalidad_plazo   = (rs.getString("MODALIDAD_PLAZO")     == null) ? "" : rs.getString("MODALIDAD_PLAZO");
					estatus           = (rs.getString("ESTATUS")             == null) ? "" : rs.getString("ESTATUS");
					tipo_credito      = (rs.getString("TIPO_CREDITO")        == null) ? "" : rs.getString("TIPO_CREDITO");
					num_docto_final   = (rs.getString("NUM_DOCTO_FINAL")     == null) ? "" : rs.getString("NUM_DOCTO_FINAL");
					plazo_final       = (rs.getString("PLAZO_FINAL")         == null) ? "" : rs.getString("PLAZO_FINAL");
					fecha_venc_final  = (rs.getString("FECHA_VENC_FINAL")    == null) ? "" : rs.getString("FECHA_VENC_FINAL");
					fecha_opera_final = (rs.getString("FECHA_OPERA_FINAL")   == null) ? "" : rs.getString("FECHA_OPERA_FINAL");
					nombre_if         = (rs.getString("NOMBRE_IF")           == null) ? "" : rs.getString("NOMBRE_IF");
					referencia        = (rs.getString("REFERENCIA_TASA_INT") == null) ? "" : rs.getString("REFERENCIA_TASA_INT");
					valor_tasa_int    = (rs.getString("VALOR_TASA_INT")      == null) ? "" : rs.getString("VALOR_TASA_INT");
					monto_int         = (rs.getString("MONTO_INTERES")       == null) ? "" : rs.getString("MONTO_INTERES");
					tipo_cobro_int    = (rs.getString("TIPO_COBRO_INT")      == null) ? "" : rs.getString("TIPO_COBRO_INT");
					ic_moneda_linea   = (rs.getString("IC_MONEDA_LINEA")     == null) ? "" : rs.getString("IC_MONEDA_LINEA");
					ic_moneda         = (rs.getString("IC_MONEDA")           == null) ? "" : rs.getString("IC_MONEDA");
					ventaCartera      = (rs.getString("CG_VENTACARTERA")     == null) ? "" : rs.getString("CG_VENTACARTERA");
					comisionAplicable = (rs.getString("COMISION_APLICABLE")  == null) ? "" : rs.getString("COMISION_APLICABLE");
					montoComision     = (rs.getString("MONTO_COMISION")      == null) ? "" : rs.getString("MONTO_COMISION");
					montoDepositar    = (rs.getString("MONTO_DEPOSITAR")     == null) ? "" : rs.getString("MONTO_DEPOSITAR");
					bins              = (rs.getString("BINS")                == null) ? "" : rs.getString("BINS");
					operaTajeta       = (rs.getString("OPERA_TARJETA")       == null) ? "" : rs.getString("OPERA_TARJETA");
					numTarjeta        = (rs.getString("NUM_TC")              == null) ? "" : rs.getString("NUM_TC");
					monto2            = Double.parseDouble((String)monto)-Double.parseDouble((String)monto_desc);
					monto2_1          = Double.toString (monto2);
				   String tipoPago = (rs.getString("TIPOPAGO") == null) ? "" : rs.getString("TIPOPAGO"); 
					
				//Si no es Operada TC
				if(!estatus.equals("Operada TC")){
					bins = "N/A";
					comisionAplicable = "N/A";
					montoComision = "N/A";
					montoDepositar = "N/A";
					numTarjeta = "N/A";
				}else{
					plazo_final = "N/A";
					fecha_venc_final = "N/A";
					valor_tasa_int = "N/A";
					monto_int = "N/A";
					referencia = "N/A";
				}

				contenidoArchivo.append(nombreEpo.replace(',',' ')         +", ");
				contenidoArchivo.append(nombrePYME .replace(',',' ')       +", ");
				contenidoArchivo.append(num_acuse.replace(',',' ')         +", ");
				contenidoArchivo.append(num_docto_inicial.replace(',',' ') +", ");
				contenidoArchivo.append(fecha_publicacion.replace(',',' ') +", ");
				contenidoArchivo.append(fecha_Emision.replace(',',' ')     +", ");
				contenidoArchivo.append(fecha_vencimiento.replace(',',' ') +", ");
				contenidoArchivo.append(plazo_docto.replace(',',' ')       +", ");
				contenidoArchivo.append(moneda.replace(',',' ')            +", ");
				contenidoArchivo.append(monto.replace(',',' ')             +", ");
				contenidoArchivo.append(plazo_desc.replace(',',' ')        +", ");
				contenidoArchivo.append(porc_desc.replace(',',' ')         +", ");
				contenidoArchivo.append(monto_desc.replace(',',' ')        +", ");

				if(ic_moneda.equals(ic_moneda_linea)){
					contenidoArchivo.append(" , , , ");
				} else{
					contenidoArchivo.append(tipo_convenio.replace(',',' ')  +", ");
					contenidoArchivo.append(tipo_cambio.replace(',',' ')    +", ");
					contenidoArchivo.append(monto_Valuado.replace(',',' ')  +", ");
				}
				if(ventaCartera.equals("S")){
					contenidoArchivo.append(" , ");
				} else{
					contenidoArchivo.append(modalidad_plazo.replace(',',' ')+", ");
				}
				
				contenidoArchivo.append(tipoPago.replace(',',' ')           +", ");					
				contenidoArchivo.append(estatus.replace(',',' ')           +", ");
				contenidoArchivo.append(tipo_credito.replace(',',' ')      +", ");
				contenidoArchivo.append(num_docto_final.replace(',',' ')   +", ");
				contenidoArchivo.append(monto2_1.replace(',',' ')          +", ");
				contenidoArchivo.append(comisionAplicable.replace(',',' ') +", ");
				contenidoArchivo.append(montoComision.replace(',',' ')     +", ");
				contenidoArchivo.append(montoDepositar.replace(',',' ')    +", ");
				contenidoArchivo.append(plazo_final.replace(',',' ')       +", ");
				contenidoArchivo.append(fecha_venc_final.replace(',',' ')  +", ");
				contenidoArchivo.append(fecha_opera_final.replace(',',' ') +", ");
				contenidoArchivo.append(nombre_if.replace(',',' ')         +", ");
				contenidoArchivo.append(bins.replace(',',' ')              +", ");
				contenidoArchivo.append(referencia.replace(',',' ')        +", ");
				contenidoArchivo.append(valor_tasa_int.replace(',',' ')    +", ");
				contenidoArchivo.append(monto_int.replace(',',' ')         +", ");
				contenidoArchivo.append(tipo_cobro_int.replace(',',' ')    +", ");
				contenidoArchivo.append(((!"".equals(numTarjeta.trim()) && !"N/A".equals(numTarjeta.trim()))?"XXXX-XXXX-XXXX-"+numTarjeta:numTarjeta)+ "\n ");

					total++;
					if(total==1000){
						total=0; 
						buffer.write(contenidoArchivo.toString());
						contenidoArchivo = new StringBuffer();//Limpio el buffer
					}
				}

				buffer.write(contenidoArchivo.toString());
				buffer.close();
				contenidoArchivo = new StringBuffer();//Limpio el buffer

			} catch(Throwable e){
				throw new AppException("Error al generar el archivo CSV. ", e);
			}

		} else if(tipo.equals("PDF")){

			try{
				HttpSession session = request.getSession();
				ComunesPDF pdfDoc   = new ComunesPDF();

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));  
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

				pdfDoc.setLTable(11,100);
				pdfDoc.setLCell(" A ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Distribuidor","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("N�m. acuse carga","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("N�mero de documento inicial ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de publicaci�n ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de emisi�n ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha vencimiento ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo docto.","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto","celda01",ComunesPDF.CENTER);

				pdfDoc.setLCell("B","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo para descuento en d�as","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("% de descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto % de descuento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo conv.","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo cambio","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto valuado en Pesos","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Modalidad de plazo","celda01",ComunesPDF.CENTER);
			   pdfDoc.setLCell("Tipo de Pago","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de cr�dito","celda01",ComunesPDF.CENTER);
				

				pdfDoc.setLCell("C","celda01",ComunesPDF.CENTER);
			   pdfDoc.setLCell("N�mero de documento final","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("% Comisi�n Aplicable \n de Terceros ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Comisi�n \n de Terceros \n (IVA Incluido)","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto a Depositar \n por Operaci�n ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de operaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("IF","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Nombre del Producto","celda01",ComunesPDF.CENTER);
				

				pdfDoc.setLCell("D","celda01",ComunesPDF.CENTER);
			   pdfDoc.setLCell("Referencia tasa de inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Valor tasa de inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto de intereses","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de cobro inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("N�mero de Trjeta de Cr�dito ","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell(" ","celda01",ComunesPDF.CENTER,5);
				pdfDoc.setLHeaders();

				while (rs.next()){
					nombreEpo         = (rs.getString("NOMBRE_EPO")          == null) ? "" : rs.getString("NOMBRE_EPO");
					nombrePYME        = (rs.getString("NOMBRE_PYME")         == null) ? "" : rs.getString("NOMBRE_PYME");
					num_acuse         = (rs.getString("NUM_ACUSE_CARGA")     == null) ? "" : rs.getString("NUM_ACUSE_CARGA");
					num_docto_inicial = (rs.getString("NUM_DOCTO_INICIAL")   == null) ? "" : rs.getString("NUM_DOCTO_INICIAL");
					fecha_publicacion = (rs.getString("FECHA_PUBLICACION")   == null) ? "" : rs.getString("FECHA_PUBLICACION");
					fecha_Emision     = (rs.getString("FECHA_EMISION")       == null) ? "" : rs.getString("FECHA_EMISION");
					fecha_vencimiento = (rs.getString("FECHA_VENCIMIENTO")   == null) ? "" : rs.getString("FECHA_VENCIMIENTO");
					plazo_docto       = (rs.getString("PLAZO_DOCTO")         == null) ? "" : rs.getString("PLAZO_DOCTO");
					moneda            = (rs.getString("MONEDA")              == null) ? "" : rs.getString("MONEDA");
					monto             = (rs.getString("MONTO")               == null) ? "" : rs.getString("MONTO");
					plazo_desc        = (rs.getString("PLAZO_DESC_DIAS")     == null) ? "" : rs.getString("PLAZO_DESC_DIAS");
					porc_desc         = (rs.getString("PORCENTAJE_DESC")     == null) ? "" : rs.getString("PORCENTAJE_DESC");
					monto_desc        = (rs.getString("MONTO_DESCUENTO")     == null) ? "0": rs.getString("MONTO_DESCUENTO");
					tipo_convenio     = (rs.getString("TIPO_CONVENIO")       == null) ? "" : rs.getString("TIPO_CONVENIO");
					tipo_cambio       = (rs.getString("TIPO_CAMBIO")         == null) ? "" : rs.getString("TIPO_CAMBIO");
					monto_Valuado     = (rs.getString("MONTO_VALUADO")       == null) ? "" : rs.getString("MONTO_VALUADO");
					modalidad_plazo   = (rs.getString("MODALIDAD_PLAZO")     == null) ? "" : rs.getString("MODALIDAD_PLAZO");
					estatus           = (rs.getString("ESTATUS")             == null) ? "" : rs.getString("ESTATUS");
					tipo_credito      = (rs.getString("TIPO_CREDITO")        == null) ? "" : rs.getString("TIPO_CREDITO");
					num_docto_final   = (rs.getString("NUM_DOCTO_FINAL")     == null) ? "" : rs.getString("NUM_DOCTO_FINAL");
					plazo_final       = (rs.getString("PLAZO_FINAL")         == null) ? "" : rs.getString("PLAZO_FINAL");
					fecha_venc_final  = (rs.getString("FECHA_VENC_FINAL")    == null) ? "" : rs.getString("FECHA_VENC_FINAL");
					fecha_opera_final = (rs.getString("FECHA_OPERA_FINAL")   == null) ? "" : rs.getString("FECHA_OPERA_FINAL");
					nombre_if         = (rs.getString("NOMBRE_IF")           == null) ? "" : rs.getString("NOMBRE_IF");
					referencia        = (rs.getString("REFERENCIA_TASA_INT") == null) ? "" : rs.getString("REFERENCIA_TASA_INT");
					valor_tasa_int    = (rs.getString("VALOR_TASA_INT")      == null) ? "" : rs.getString("VALOR_TASA_INT");
					monto_int         = (rs.getString("MONTO_INTERES")       == null) ? "" : rs.getString("MONTO_INTERES");
					tipo_cobro_int    = (rs.getString("TIPO_COBRO_INT")      == null) ? "" : rs.getString("TIPO_COBRO_INT");
					ic_moneda_linea   = (rs.getString("IC_MONEDA_LINEA")     == null) ? "" : rs.getString("IC_MONEDA_LINEA");
					ic_moneda         = (rs.getString("IC_MONEDA")           == null) ? "" : rs.getString("IC_MONEDA");
					ventaCartera      = (rs.getString("CG_VENTACARTERA")     == null) ? "" : rs.getString("CG_VENTACARTERA");
					comisionAplicable = (rs.getString("COMISION_APLICABLE")  == null) ? "" : rs.getString("COMISION_APLICABLE");
					montoComision     = (rs.getString("MONTO_COMISION")      == null) ? "" : rs.getString("MONTO_COMISION");
					montoDepositar    = (rs.getString("MONTO_DEPOSITAR")     == null) ? "" : rs.getString("MONTO_DEPOSITAR");
					bins              = (rs.getString("BINS")                == null) ? "" : rs.getString("BINS");
					operaTajeta       = (rs.getString("OPERA_TARJETA")       == null) ? "" : rs.getString("OPERA_TARJETA");
					numTarjeta        = (rs.getString("NUM_TC")              == null) ? "" : rs.getString("NUM_TC");
					monto2            = Double.parseDouble((String)monto)-Double.parseDouble((String)monto_desc);
				   String tipoPago = (rs.getString("TIPOPAGO") == null) ? "" : rs.getString("TIPOPAGO"); 
					
					//Si no es Operada TC
					if(!estatus.equals("Operada TC")){
						bins     = "N/A";
						comisionAplicable = "N/A";
						montoComision = "N/A";
						montoDepositar = "N/A";
						numTarjeta  =  "N/A";
					}else{
						plazo_final = "N/A";
						fecha_venc_final = "N/A";
						valor_tasa_int = "N/A";
						monto_int = "N/A";
						referencia = "N/A";
					}

					pdfDoc.setLCell("A ","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( nombreEpo ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( nombrePYME ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( num_acuse ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( num_docto_inicial ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( fecha_publicacion ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( fecha_Emision ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( fecha_vencimiento ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( plazo_docto ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( moneda ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(String.valueOf(monto), 2),"formas",ComunesPDF.RIGHT);

					pdfDoc.setLCell("B","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( plazo_desc ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( porc_desc +"%","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(String.valueOf(monto_desc), 2),"formas",ComunesPDF.RIGHT);
					if(ic_moneda.equals(ic_moneda_linea)){
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
					} else {
						pdfDoc.setLCell( tipo_convenio ,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell( tipo_cambio ,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(String.valueOf(monto_Valuado), 2),"formas",ComunesPDF.RIGHT);
					}
					if(ventaCartera.equals("S")){
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
					} else {
						pdfDoc.setLCell(modalidad_plazo,"formas",ComunesPDF.CENTER);
					}
				   pdfDoc.setLCell( tipoPago ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( estatus ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( tipo_credito ,"formas",ComunesPDF.CENTER);
					

					pdfDoc.setLCell("C","formas",ComunesPDF.CENTER);
				   pdfDoc.setLCell( num_docto_final ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(Double.toString (monto2), 2),"formas",ComunesPDF.RIGHT);
					if(!estatus.equals("Operada TC") ){//!operaTajeta.equals("N") ||
						pdfDoc.setLCell("N/A","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("N/A","formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("N/A","formas",ComunesPDF.RIGHT);
					}else{
						pdfDoc.setLCell(Comunes.formatoDecimal(String.valueOf(comisionAplicable), 2)+"%","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(String.valueOf(montoComision), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(String.valueOf(montoDepositar), 2),"formas",ComunesPDF.RIGHT);
					}
					pdfDoc.setLCell( plazo_final,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( fecha_venc_final ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( fecha_opera_final ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( nombre_if,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( bins,"formas",ComunesPDF.CENTER);
					

					pdfDoc.setLCell("D","formas",ComunesPDF.CENTER);
				   pdfDoc.setLCell( referencia ,"formas",ComunesPDF.CENTER); 
					pdfDoc.setLCell( (!"".equals(valor_tasa_int.trim()) && !"N/A".equals(valor_tasa_int.trim()))?valor_tasa_int+"%":valor_tasa_int ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell((!"".equals(monto_int) && !"N/A".equals(monto_int))?"$"+Comunes.formatoDecimal(String.valueOf(monto_int), 2):monto_int,"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell( tipo_cobro_int ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( (!"".equals(numTarjeta.trim()) && !"N/A".equals(numTarjeta.trim()))?"XXXX-XXXX-XXXX-"+numTarjeta:numTarjeta ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( " " ,"formas",ComunesPDF.CENTER,5); 

					countReg++;
					//System.out.println("Registros procesados: " + countReg);
				}

				pdfDoc.addLTable();
				pdfDoc.endDocument();

			} catch(Throwable e){
				throw new AppException("Error al generar el archivo PDF. ", e);
			}

		}

		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}

  	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
	
		return "";
					
	}
  

		
	/**
	 Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	 @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	 
/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getIc_documento() {
		return ic_documento;
	}

	public void setIc_documento(String ic_documento) {
		this.ic_documento = ic_documento;
	}

	public String getIc_pyme() {
		return ic_pyme;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}

	public String getTipo_credito() {
		return tipo_credito;
	}

	public void setTipo_credito(String tipo_credito) {
		this.tipo_credito = tipo_credito;
	}

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getFecha_seleccion_de() {
		return fecha_seleccion_de;
	}

	public void setFecha_seleccion_de(String fecha_seleccion_de) {
		this.fecha_seleccion_de = fecha_seleccion_de;
	}

	public String getFecha_seleccion_a() {
		return fecha_seleccion_a;
	}

	public void setFecha_seleccion_a(String fecha_seleccion_a) {
		this.fecha_seleccion_a = fecha_seleccion_a;
	}
	

	public String getMonto_credito_de() {
		return monto_credito_de;
	}

	public void setMonto_credito_de(String monto_credito_de) {
		this.monto_credito_de = monto_credito_de;
	}

	public String getMonto_credito_a() {
		return monto_credito_a;
	}

	public void setMonto_credito_a(String monto_credito_a) {
		this.monto_credito_a = monto_credito_a;
	}

	public String getIg_numero_docto() {
		return ig_numero_docto;
	}

	public void setIg_numero_docto(String ig_numero_docto) {
		this.ig_numero_docto = ig_numero_docto;
	}

	public String getFecha_vto_credito_de() {
		return fecha_vto_credito_de;
	}

	public void setFecha_vto_credito_de(String fecha_vto_credito_de) {
		this.fecha_vto_credito_de = fecha_vto_credito_de;
	}

	public String getFecha_vto_credito_a() {
		return fecha_vto_credito_a;
	}

	public void setFecha_vto_credito_a(String fecha_vto_credito_a) {
		this.fecha_vto_credito_a = fecha_vto_credito_a;
	}

	public String getFecha_vto_de() {
		return fecha_vto_de;
	}

	public void setFecha_vto_de(String fecha_vto_de) {
		this.fecha_vto_de = fecha_vto_de;
	}

	public String getFecha_vto_a() {
		return fecha_vto_a;
	}

	public void setFecha_vto_a(String fecha_vto_a) {
		this.fecha_vto_a = fecha_vto_a;
	}

	public String getIc_tipo_cobro_int() {
		return ic_tipo_cobro_int;
	}

	public void setIc_tipo_cobro_int(String ic_tipo_cobro_int) {
		this.ic_tipo_cobro_int = ic_tipo_cobro_int;
	}

	public String getFecha_publicacion_de() {
		return fecha_publicacion_de;
	}

	public void setFecha_publicacion_de(String fecha_publicacion_de) {
		this.fecha_publicacion_de = fecha_publicacion_de;
	}

	public String getFecha_publicacion_a() {
		return fecha_publicacion_a;
	}

	public void setFecha_publicacion_a(String fecha_publicacion_a) {
		this.fecha_publicacion_a = fecha_publicacion_a;
	}

	public String getFecha_emision_de() {
		return fecha_emision_de;
	}

	public void setFecha_emision_de(String fecha_emision_de) {
		this.fecha_emision_de = fecha_emision_de;
	}

	public String getFecha_emision_a() {
		return fecha_emision_a;
	}

	public void setFecha_emision_a(String fecha_emision_a) {
		this.fecha_emision_a = fecha_emision_a;
	}

	public String getIc_moneda() {
		return ic_moneda;
	}

	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}

	public String getCc_acuse() {
		return cc_acuse;
	}

	public void setCc_acuse(String cc_acuse) {
		this.cc_acuse = cc_acuse;
	}

	public String getFn_monto_de() {
		return fn_monto_de;
	}

	public void setFn_monto_de(String fn_monto_de) {
		this.fn_monto_de = fn_monto_de;
	}

	public String getFn_monto_a() {
		return fn_monto_a;
	}

	public void setFn_monto_a(String fn_monto_a) {
		this.fn_monto_a = fn_monto_a;
	}

	public String getMonto_con_descuento() {
		return monto_con_descuento;
	}

	public void setMonto_con_descuento(String monto_con_descuento) {
		this.monto_con_descuento = monto_con_descuento;
	}

	public String getChk_proc_desc() {
		return chk_proc_desc;
	}

	public void setChk_proc_desc(String chk_proc_desc) {
		this.chk_proc_desc = chk_proc_desc;
	}

	public String getSolo_cambio() {
		return solo_cambio;
	}

	public void setSolo_cambio(String solo_cambio) {
		this.solo_cambio = solo_cambio;
	}

	public String getModo_plazo() {
		return modo_plazo;
	}

	public void setModo_plazo(String modo_plazo) {
		this.modo_plazo = modo_plazo;
	}

	public String getTipoConsulta() {
		return tipoConsulta;
	}

	public void setTipoConsulta(String tipoConsulta) {
		this.tipoConsulta = tipoConsulta;
	}

	public String getIc_estatus() {
		return ic_estatus;
	}

	public void setIc_estatus(String ic_estatus) {
		this.ic_estatus = ic_estatus;
	}


	public void setBins(String bins) {
		this.bins = bins;
	}


	public String getBins() {
		return bins;
	}

	public int getCountReg() {
		return countReg;
	}
	public String getIc_tipo_Pago() {
		return ic_tipo_Pago;
	}

	public void setIc_tipo_Pago(String ic_tipo_Pago) {
		this.ic_tipo_Pago = ic_tipo_Pago;
	} 
}