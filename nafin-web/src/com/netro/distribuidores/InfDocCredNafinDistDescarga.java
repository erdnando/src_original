package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class InfDocCredNafinDistDescarga{

	public InfDocCredNafinDistDescarga(){}

	private final static Log log = ServiceLocator.getInstance().getLog(InfDocCredNafinDistDescarga.class);
	private String tipo_credito;
	
	/** metodo para descargar todos los registros en formato PDF por Pagina de ambos grid�s
	 * @return 
	 * @param path
	 * @param reg2
	 * @param reg1
	 * @param request
	 */
	public String crearPageCustomFile(HttpServletRequest request, Registros reg1, Registros reg2, String path){
		log.info("crearPageCustomFile (E) ");

		String nombreArchivo = "";
		HttpSession session  = request.getSession();
		ComunesPDF pdfDoc    = new ComunesPDF();

		try {

			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);

			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual   = fechaActual.substring(0,2);
			String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual  = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

			if(reg1 !=null ) {
				if(reg1.getNumeroRegistros() >0) {
					pdfDoc.setLTable(11,100);
					if(tipo_credito.equals("D")) {
						pdfDoc.setLCell(" Datos Documento Inicial  ","celda01",ComunesPDF.CENTER, 11);
					}else if(tipo_credito.equals("C")  ||  tipo_credito.equals("")  ) {
						pdfDoc.setLCell(" Documento ","celda01",ComunesPDF.CENTER, 11);
					}
					pdfDoc.setLCell("A ","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("EPO ","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Distribuidor  ","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("N�m. acuse carga ","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("N�mero de documento inicial ","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Fecha de publicaci�n ","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Fecha de emisi�n ","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Fecha vencimiento ","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Plazo docto. ","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Moneda ","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto ","celda01",ComunesPDF.CENTER);

					pdfDoc.setLCell("B ","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Plazo para descuento en dias  ","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("% de descuento ","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto % de descuento ","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Tipo conv. ","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Tipo cambio ","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto valuado en Pesos ","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Modalidad de plazo ","celda01",ComunesPDF.CENTER);
				   pdfDoc.setLCell(" Tipo de Pago","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Estatus ","celda01",ComunesPDF.CENTER);					
					pdfDoc.setLCell(" ","celda01",ComunesPDF.CENTER);
					pdfDoc.setLHeaders();
				}
				while (reg1.next()){

					String nombreEpo         = (reg1.getString("NOMBRE_EPO")        == null) ? "" : reg1.getString("NOMBRE_EPO");
					String nombrePYME        = (reg1.getString("NOMBRE_PYME")       == null) ? "" : reg1.getString("NOMBRE_PYME");
					String num_acuse         = (reg1.getString("NUM_ACUSE_CARGA")   == null) ? "" : reg1.getString("NUM_ACUSE_CARGA");
					String num_docto_inicial = (reg1.getString("NUM_DOCTO_INICIAL") == null) ? "" : reg1.getString("NUM_DOCTO_INICIAL");
					String fecha_publicacion = (reg1.getString("FECHA_PUBLICACION") == null) ? "" : reg1.getString("FECHA_PUBLICACION");
					String fecha_Emision     = (reg1.getString("FECHA_EMISION")     == null) ? "" : reg1.getString("FECHA_EMISION");
					String FechaVencimiento  = (reg1.getString("FECHA_VENCIMIENTO") == null) ? "" : reg1.getString("FECHA_VENCIMIENTO");
					String plazo_docto       = (reg1.getString("PLAZO_DOCTO")       == null) ? "" : reg1.getString("PLAZO_DOCTO");
					String moneda            = (reg1.getString("MONEDA")            == null) ? "" : reg1.getString("MONEDA");
					String monto             = (reg1.getString("MONTO")             == null) ? "" : reg1.getString("MONTO");
					String plazo_desc        = (reg1.getString("PLAZO_DESC_DIAS")   == null) ? "" : reg1.getString("PLAZO_DESC_DIAS");
					String porc_des          = (reg1.getString("PORCENTAJE_DESC")   == null) ? "" : reg1.getString("PORCENTAJE_DESC");
					String monto_desc        = (reg1.getString("MONTO_DESCUENTO")   == null) ? "0": reg1.getString("MONTO_DESCUENTO");
					String tipo_convenio     = (reg1.getString("TIPO_CONVENIO")     == null) ? "" : reg1.getString("TIPO_CONVENIO");
					String tipo_cambio       = (reg1.getString("TIPO_CAMBIO")       == null) ? "" : reg1.getString("TIPO_CAMBIO");
					String monto_valuado     = (reg1.getString("MONTO_VALUADO")     == null) ? "" : reg1.getString("MONTO_VALUADO");
					String modalidad_plazo   = (reg1.getString("MODALIDAD_PLAZO")   == null) ? "" : reg1.getString("MODALIDAD_PLAZO");
					String estatus           = (reg1.getString("ESTATUS")           == null) ? "" : reg1.getString("ESTATUS");
					String ic_moneda_linea   = (reg1.getString("IC_MONEDA_LINEA")   == null) ? "" : reg1.getString("IC_MONEDA_LINEA");
					String ic_moneda         = (reg1.getString("IC_MONEDA")         == null) ? "" : reg1.getString("IC_MONEDA");
					String ventaCartera      = (reg1.getString("CG_VENTACARTERA")   == null) ? "" : reg1.getString("CG_VENTACARTERA");
				   String tipoPago = (reg1.getString("TIPOPAGO") == null) ? "" : reg1.getString("TIPOPAGO"); 
				                  
					pdfDoc.setLCell(" A ","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(nombreEpo,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(nombrePYME,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(num_acuse,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(num_docto_inicial,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_publicacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fecha_Emision,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(FechaVencimiento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(plazo_docto,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(String.valueOf(monto), 2),"formas",ComunesPDF.RIGHT);

					pdfDoc.setLCell(" B ","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(plazo_desc,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(porc_des+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(String.valueOf(monto_desc), 2),"formas",ComunesPDF.RIGHT);

					if(ic_moneda.equals(ic_moneda_linea)){
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
					}else {
						pdfDoc.setLCell(tipo_convenio,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(tipo_cambio,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(String.valueOf(monto_valuado), 2),"formas",ComunesPDF.RIGHT);
					}
					if(ventaCartera.equals("S")){
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
					} else{
						pdfDoc.setLCell(modalidad_plazo,"formas",ComunesPDF.CENTER);
					}
				   pdfDoc.setLCell(tipoPago,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(estatus,"formas",ComunesPDF.CENTER);					
					pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);

				}
				if(reg1.getNumeroRegistros() >0) {
					pdfDoc.addLTable();
				}

			}

			//EL IF ES PARA QUE LA SIGUIENTE TABLA INICIE EN UNA P�GINA NUEVA
			if(reg1 !=null && reg2 != null) {
				pdfDoc.newPage();
			}

			if(reg2 != null ) {
				if(reg2.getNumeroRegistros() >0) {
					pdfDoc.setLTable(11,100);
					pdfDoc.setLCell(" A ","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("EPO","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Distribuidor","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("N�m. acuse carga","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("N�mero de documento inicial ","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Fecha de publicaci�n ","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Fecha de emisi�n ","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Fecha vencimiento ","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Plazo docto.","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Moneda ","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto","celda01",ComunesPDF.CENTER);

					pdfDoc.setLCell("B","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Plazo para descuento en d�as","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("% de descuento","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto % de descuento","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Tipo conv.","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Tipo cambio","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto valuado en Pesos","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Modalidad de plazo","celda01",ComunesPDF.CENTER);
				   pdfDoc.setCell(" Tipo de Pago","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Estatus","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Tipo de cr�dito","celda01",ComunesPDF.CENTER);
					

					pdfDoc.setLCell("C","celda01",ComunesPDF.CENTER);
				   pdfDoc.setLCell("N�mero de documento final","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto","celda01",ComunesPDF.CENTER);
					//FODEA-013-2014 MOD(SE AGRAGARON LOS TITULOS DE LAS NUEVAS COLUMNAS AL PDF)
					pdfDoc.setLCell("% Comisi�n Aplicable\nde Terceros ","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto Comisi�n\nde Terceros\n(IVA Incluido)","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto a Depositar\npor Operaci�n ","celda01",ComunesPDF.CENTER);

					pdfDoc.setLCell("Plazo","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Fecha de vencimiento","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Fecha de operaci�n","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("IF","celda01",ComunesPDF.CENTER);

					//FODEA-013-2014 MOD(SE AGRAGARON LOS TITULOS DE LAS NUEVAS COLUMNAS AL PDF)
					//FODEA-013-2014 nueva funcionalidad 'Nombre del Producto',//"Bin's",
					//pdfDoc.setCell("Bin's","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Nombre del Producto","celda01",ComunesPDF.CENTER);

					pdfDoc.setLCell("Referencia tasa de inter�s","celda01",ComunesPDF.CENTER);

					pdfDoc.setLCell("D","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Valor tasa de inter�s","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Monto de intereses","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("Tipo de cobro inter�s","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell("N�mero de Trjeta de Cr�dito ","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell(" ","celda01",ComunesPDF.CENTER,5);
					pdfDoc.setLHeaders();
				}
				while (reg2.next()){
					String nombreEpo         = (reg2.getString("NOMBRE_EPO")          == null) ? "" : reg2.getString("NOMBRE_EPO");
					String nombrePYME        = (reg2.getString("NOMBRE_PYME")         == null) ? "" : reg2.getString("NOMBRE_PYME");
					String num_acuse         = (reg2.getString("NUM_ACUSE_CARGA")     == null) ? "" : reg2.getString("NUM_ACUSE_CARGA");
					String num_docto_inicial = (reg2.getString("NUM_DOCTO_INICIAL")   == null) ? "" : reg2.getString("NUM_DOCTO_INICIAL");
					String fecha_publicacion = (reg2.getString("FECHA_PUBLICACION")   == null) ? "" : reg2.getString("FECHA_PUBLICACION");
					String fecha_Emision     = (reg2.getString("FECHA_EMISION")       == null) ? "" : reg2.getString("FECHA_EMISION");
					String fecha_vencimiento = (reg2.getString("FECHA_VENCIMIENTO")   == null) ? "" : reg2.getString("FECHA_VENCIMIENTO");
					String plazo_docto       = (reg2.getString("PLAZO_DOCTO")         == null) ? "" : reg2.getString("PLAZO_DOCTO");
					String moneda            = (reg2.getString("MONEDA")              == null) ? "" : reg2.getString("MONEDA");
					String monto             = (reg2.getString("MONTO")               == null) ? "" : reg2.getString("MONTO");
					String plazo_desc        = (reg2.getString("PLAZO_DESC_DIAS")     == null) ? "" : reg2.getString("PLAZO_DESC_DIAS");
					String porc_desc         = (reg2.getString("PORCENTAJE_DESC")     == null) ? "" : reg2.getString("PORCENTAJE_DESC");
					String monto_desc        = (reg2.getString("MONTO_DESCUENTO")     == null) ? "0": reg2.getString("MONTO_DESCUENTO");
					String tipo_convenio     = (reg2.getString("TIPO_CONVENIO")       == null) ? "" : reg2.getString("TIPO_CONVENIO");
					String tipo_cambio       = (reg2.getString("TIPO_CAMBIO")         == null) ? "" : reg2.getString("TIPO_CAMBIO");
					String monto_Valuado     = (reg2.getString("MONTO_VALUADO")       == null) ? "" : reg2.getString("MONTO_VALUADO");
					String modalidad_plazo   = (reg2.getString("MODALIDAD_PLAZO")     == null) ? "" : reg2.getString("MODALIDAD_PLAZO");
					String estatus           = (reg2.getString("ESTATUS")             == null) ? "" : reg2.getString("ESTATUS");
					String tipo_credito      = (reg2.getString("TIPO_CREDITO")        == null) ? "" : reg2.getString("TIPO_CREDITO");
					String num_docto_final   = (reg2.getString("NUM_DOCTO_FINAL")     == null) ? "" : reg2.getString("NUM_DOCTO_FINAL");
					String plazo_final       = (reg2.getString("PLAZO_FINAL")         == null) ? "" : reg2.getString("PLAZO_FINAL");
					String fecha_venc_final  = (reg2.getString("FECHA_VENC_FINAL")    == null) ? "" : reg2.getString("FECHA_VENC_FINAL");
					String fecha_opera_final = (reg2.getString("FECHA_OPERA_FINAL")   == null) ? "" : reg2.getString("FECHA_OPERA_FINAL");
					String nombre_if         = (reg2.getString("NOMBRE_IF")           == null) ? "" : reg2.getString("NOMBRE_IF");
					String referencia        = (reg2.getString("REFERENCIA_TASA_INT") == null) ? "" : reg2.getString("REFERENCIA_TASA_INT");
					String valor_tasa_int    = (reg2.getString("VALOR_TASA_INT")      == null) ? "" : reg2.getString("VALOR_TASA_INT");
					String monto_int         = (reg2.getString("MONTO_INTERES")       == null) ? "" : reg2.getString("MONTO_INTERES");
					String tipo_cobro_int    = (reg2.getString("TIPO_COBRO_INT")      == null) ? "" : reg2.getString("TIPO_COBRO_INT");
					String ic_moneda_linea   = (reg2.getString("IC_MONEDA_LINEA")     == null) ? "" : reg2.getString("IC_MONEDA_LINEA");
					String ic_moneda         = (reg2.getString("IC_MONEDA")           == null) ? "" : reg2.getString("IC_MONEDA");
					String ventaCartera      = (reg2.getString("CG_VENTACARTERA")     == null) ? "" : reg2.getString("CG_VENTACARTERA");
					//FODEA-013-2014 MOD(SE OBTIENEN LOS VALORES DE LAS NUEVAS COLUMNAS)
					String comisionAplicable = (reg2.getString("COMISION_APLICABLE")  == null) ? "" : reg2.getString("COMISION_APLICABLE");
					String montoComision     = (reg2.getString("MONTO_COMISION")      == null) ? "" : reg2.getString("MONTO_COMISION");
					String montoDepositar    = (reg2.getString("MONTO_DEPOSITAR")     == null) ? "" : reg2.getString("MONTO_DEPOSITAR");
					String bins              = (reg2.getString("BINS")                == null) ? "" : reg2.getString("BINS");
					String operaTajeta       = (reg2.getString("OPERA_TARJETA")       == null) ? "" : reg2.getString("OPERA_TARJETA");
					String numTarjeta        = (reg2.getString("NUM_TC")              == null) ? "" : reg2.getString("NUM_TC");
				   String tipoPago = (reg2.getString("TIPOPAGO") == null) ? "" : reg2.getString("TIPOPAGO"); 
				                  
					double monto2            = Double.parseDouble((String)monto)-Double.parseDouble((String)monto_desc);

					//	Si opera con tarjeta de credito
					/*if(	operaTajeta.equals("S")	){
						plazo_final = "N/A";
						fecha_venc_final = "N/A";
						valor_tasa_int = "N/A";
						monto_int = "N/A";
						referencia = "N/A";
					}*/
					//Validaciones 
						//Si no es Operada TC
						if(!estatus.equals("Operada TC")){
							bins		= "N/A";
							comisionAplicable = "N/A";
							montoComision = "N/A";
							montoDepositar = "N/A";
							numTarjeta	=	"N/A";
						}else{
							plazo_final = "N/A";
							fecha_venc_final = "N/A";
							valor_tasa_int = "N/A";
							monto_int = "N/A";
							referencia = "N/A";
						}
					
					pdfDoc.setLCell("A ","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( nombreEpo ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( nombrePYME ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( num_acuse ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( num_docto_inicial ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( fecha_publicacion ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( fecha_Emision ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( fecha_vencimiento ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( plazo_docto ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( moneda ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(String.valueOf(monto), 2),"formas",ComunesPDF.RIGHT);

					pdfDoc.setLCell("B","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( plazo_desc ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( porc_desc +"%","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(String.valueOf(monto_desc), 2),"formas",ComunesPDF.RIGHT);

					if(ic_moneda.equals(ic_moneda_linea)){
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
					}else {
						pdfDoc.setLCell( tipo_convenio ,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell( tipo_cambio ,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(String.valueOf(monto_Valuado), 2),"formas",ComunesPDF.RIGHT);
					}
					if(ventaCartera.equals("S")){
						pdfDoc.setLCell("","formas",ComunesPDF.CENTER);
					}else  {
						pdfDoc.setLCell(modalidad_plazo,"formas",ComunesPDF.CENTER);
					}
				   pdfDoc.setLCell( tipoPago ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( estatus ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( tipo_credito ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( num_docto_final ,"formas",ComunesPDF.CENTER);

					pdfDoc.setLCell("C","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(Double.toString (monto2), 2),"formas",ComunesPDF.RIGHT);
					//FODEA-013-2014 MOD(SE AGRAGA LOS VALORES DE LAS NUEVAS COLUMNAS AL PDF)
					if(		!estatus.equals("Operada TC")	){//!operaTajeta.equals("N") ||
						pdfDoc.setLCell("N/A","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("N/A","formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("N/A","formas",ComunesPDF.RIGHT);
					}else{
						pdfDoc.setLCell(Comunes.formatoDecimal(String.valueOf(comisionAplicable), 2)+"%","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(String.valueOf(montoComision), 2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(String.valueOf(montoDepositar), 2),"formas",ComunesPDF.RIGHT);
					}

					pdfDoc.setLCell( plazo_final,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( fecha_venc_final ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( fecha_opera_final ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( nombre_if,"formas",ComunesPDF.CENTER);
					//FODEA-013-2014 MOD(SE AGRAGA LOS VALORES DE LAS NUEVAS COLUMNAS AL PDF)
					pdfDoc.setLCell( bins,"formas",ComunesPDF.CENTER);

					pdfDoc.setLCell( referencia ,"formas",ComunesPDF.CENTER);

					pdfDoc.setLCell("D","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( (!"".equals(valor_tasa_int.trim()) && !"N/A".equals(valor_tasa_int.trim()))?valor_tasa_int+"%":valor_tasa_int ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell((!"".equals(monto_int) && !"N/A".equals(monto_int))?"$"+Comunes.formatoDecimal(String.valueOf(monto_int), 2):monto_int,"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell( tipo_cobro_int ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( (!"".equals(numTarjeta.trim()) && !"N/A".equals(numTarjeta.trim()))?"XXXX-XXXX-XXXX-"+numTarjeta:numTarjeta ,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell( " " ,"formas",ComunesPDF.CENTER,5); 

				}
				if(reg2.getNumeroRegistros() >0) {
					pdfDoc.addLTable();
				}

			}
			pdfDoc.endDocument();

		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		}
		log.info (" crearPageCustomFile   (S) ");
		return nombreArchivo;
	}


	/**
	 * metodo para descargar todos los registros en formato csv de ambos grid�s
	 * @return 
	 * @param path
	 * @param reg2
	 * @param reg1
	 * @param request
	 */
	public String crearCustomFile(HttpServletRequest request, Registros reg1,    Registros reg2,  String path  ) {
			
		log.info("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		int total = 0;
		
		
		try {
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);

			if(reg1.getNumeroRegistros() >0) {
				contenidoArchivo.append(" EPO ,  Distribuidor ,  N�m. acuse carga,  N�mero de documento inicial ,  Fecha de publicaci�n,  Fecha de emisi�n , "+
												" Fecha vencimiento , Plazo docto. , Moneda , Monto ,  Plazo para descuento en dias ,  % de descuento ,  Monto % de descuento ,  Tipo conv.,  "+
												"  Tipo cambio ,  Monto valuado en Pesos,   Modalidad de plazo ,Tipo de  Pago ,  Estatus  \n" );					

			}
			
			while (reg1.next())	{	
			
				String nombreEpo = (reg1.getString("NOMBRE_EPO") == null) ? "" : reg1.getString("NOMBRE_EPO");	
				String nombrePYME = (reg1.getString("NOMBRE_PYME") == null) ? "" : reg1.getString("NOMBRE_PYME");	
				String num_acuse = (reg1.getString("NUM_ACUSE_CARGA") == null) ? "" : reg1.getString("NUM_ACUSE_CARGA");	
				String num_docto_inicial = (reg1.getString("NUM_DOCTO_INICIAL") == null) ? "" : reg1.getString("NUM_DOCTO_INICIAL");	
				String fecha_publicacion = (reg1.getString("FECHA_PUBLICACION") == null) ? "" : reg1.getString("FECHA_PUBLICACION");	
				String fecha_Emision = (reg1.getString("FECHA_EMISION") == null) ? "" : reg1.getString("FECHA_EMISION");	
				String FechaVencimiento = (reg1.getString("FECHA_VENCIMIENTO") == null) ? "" : reg1.getString("FECHA_VENCIMIENTO");	
				String plazo_docto = (reg1.getString("PLAZO_DOCTO") == null) ? "" : reg1.getString("PLAZO_DOCTO");	
				String moneda = (reg1.getString("MONEDA") == null) ? "" : reg1.getString("MONEDA");	
				String monto = (reg1.getString("MONTO") == null) ? "" : reg1.getString("MONTO");	
				String plazo_desc = (reg1.getString("PLAZO_DESC_DIAS") == null) ? "" : reg1.getString("PLAZO_DESC_DIAS");	
				String porc_des = (reg1.getString("PORCENTAJE_DESC") == null) ? "" : reg1.getString("PORCENTAJE_DESC");	
				String monto_desc = (reg1.getString("MONTO_DESCUENTO") == null) ? "" : reg1.getString("MONTO_DESCUENTO");	
				String tipo_convenio = (reg1.getString("TIPO_CONVENIO") == null) ? "" : reg1.getString("TIPO_CONVENIO");	
				String tipo_cambio = (reg1.getString("TIPO_CAMBIO") == null) ? "" : reg1.getString("TIPO_CAMBIO");	
				String monto_valuado = (reg1.getString("MONTO_VALUADO") == null) ? "" : reg1.getString("MONTO_VALUADO");	
				String modalidad_plazo = (reg1.getString("MODALIDAD_PLAZO") == null) ? "" : reg1.getString("MODALIDAD_PLAZO");	
				String estatus = (reg1.getString("ESTATUS") == null) ? "" : reg1.getString("ESTATUS");	
				String ic_moneda_linea = (reg1.getString("IC_MONEDA_LINEA") == null) ? "" : reg1.getString("IC_MONEDA_LINEA");	
				String ic_moneda = (reg1.getString("IC_MONEDA") == null) ? "" : reg1.getString("IC_MONEDA");	
				String ventaCartera = (reg1.getString("CG_VENTACARTERA") == null) ? "" : reg1.getString("CG_VENTACARTERA");	
			   String tipoPago = (reg1.getString("TIPOPAGO") == null) ? "" : reg1.getString("TIPOPAGO"); 
			            
				contenidoArchivo.append(nombreEpo.replace(',',' ')+", "+
					nombrePYME.replace(',',' ')+", "+
					num_acuse.replace(',',' ')+", "+
					num_docto_inicial.replace(',',' ')+", "+
					fecha_publicacion.replace(',',' ')+", "+
					fecha_Emision.replace(',',' ')+", "+
					FechaVencimiento.replace(',',' ')+", "+
					plazo_docto.replace(',',' ')+", "+
					moneda.replace(',',' ')+", "+
					monto.replace(',',' ')+", "+
					plazo_desc.replace(',',' ')+", "+  
					porc_des.replace(',',' ')+", "+
					monto_desc.replace(',',' ')+", ");
					
					if(ic_moneda.equals(ic_moneda_linea)){
						contenidoArchivo.append(" , , , ");					
					}else  {
						contenidoArchivo.append(tipo_convenio.replace(',',' ')+", "+
						tipo_cambio.replace(',',' ')+", "+
						monto_valuado.replace(',',' ')+", ");
					}
					if(ventaCartera.equals("S")){
						contenidoArchivo.append(" , ");
					}else  {
						contenidoArchivo.append(modalidad_plazo.replace(',',' ')+", ");
					}
			      
					contenidoArchivo.append(tipoPago.replace(',',' ')+", ");
					contenidoArchivo.append(estatus.replace(',',' ')+"\n");
							
				total++;
				if(total==1000){					
					total=0;	
					buffer.write(contenidoArchivo.toString());
					contenidoArchivo = new StringBuffer();//Limpio  
				}				
			}

			buffer.write(contenidoArchivo.toString());
			
			
			total=0;	
			contenidoArchivo.append("\n");
			contenidoArchivo = new StringBuffer();
			contenidoArchivo.append("\n");
			
			if(reg2.getNumeroRegistros() >0) {
			contenidoArchivo.append(" EPO,   Distribuidor, N�m. acuse carga,  N�mero de documento inicial, Fecha de publicaci�n ,  Fecha de emisi�n ,   "+
				 " Fecha vencimiento ,  Plazo docto.,  Moneda , Monto  ,  Plazo para descuento en d�as, % de descuento,  Monto % de descuento,  Tipo conv.,  "+
				 " Tipo cambio,  Monto valuado en Pesos,  Modalidad de plazo,   Tipo de Pago,  Estatus,  Tipo de cr�dito,  N�mero de documento final,  Monto, "+
				 "	% Comisi�n Aplicable de Terceros,	Monto Comisi�n de Terceros (IVA Incluido),	Monto a Depositar por Operaci�n,	"+////FODEA-013-2014 MOD(SE AGRAGAN TITULOS DE LAS NUEVAS COLUMNAS AL CSV)
				 " Plazo ,  Fecha de vencimiento , Fecha de operaci�n ,  IF, Nombre del Producto,	Referencia tasa de inter�s,  Valor tasa de inter�s,  Monto de intereses,  Tipo de cobro inter�s,N�mero Tarjeta de Cr�dito \n ");
				 
			}
			 while (reg2.next())	{		
				String nombreEpo = (reg2.getString("NOMBRE_EPO") == null) ? "" : reg2.getString("NOMBRE_EPO");	
				String nombrePYME = (reg2.getString("NOMBRE_PYME") == null) ? "" : reg2.getString("NOMBRE_PYME");	
				String num_acuse = (reg2.getString("NUM_ACUSE_CARGA") == null) ? "" : reg2.getString("NUM_ACUSE_CARGA");	
				String num_docto_inicial = (reg2.getString("NUM_DOCTO_INICIAL") == null) ? "" : reg2.getString("NUM_DOCTO_INICIAL");	
				String fecha_publicacion = (reg2.getString("FECHA_PUBLICACION") == null) ? "" : reg2.getString("FECHA_PUBLICACION");	
				String fecha_Emision = (reg2.getString("FECHA_EMISION") == null) ? "" : reg2.getString("FECHA_EMISION");
				String fecha_vencimiento = (reg2.getString("FECHA_VENCIMIENTO") == null) ? "" : reg2.getString("FECHA_VENCIMIENTO");
				String plazo_docto = (reg2.getString("PLAZO_DOCTO") == null) ? "" : reg2.getString("PLAZO_DOCTO");
				String moneda = (reg2.getString("MONEDA") == null) ? "" : reg2.getString("MONEDA");
				String monto = (reg2.getString("MONTO") == null) ? "" : reg2.getString("MONTO");
				String plazo_desc = (reg2.getString("PLAZO_DESC_DIAS") == null) ? "" : reg2.getString("PLAZO_DESC_DIAS");
				String porc_desc = (reg2.getString("PORCENTAJE_DESC") == null) ? "" : reg2.getString("PORCENTAJE_DESC");
				String monto_desc = (reg2.getString("MONTO_DESCUENTO") == null) ? "" : reg2.getString("MONTO_DESCUENTO");
				String tipo_convenio = (reg2.getString("TIPO_CONVENIO") == null) ? "" : reg2.getString("TIPO_CONVENIO");
				String tipo_cambio = (reg2.getString("TIPO_CAMBIO") == null) ? "" : reg2.getString("TIPO_CAMBIO");
				String monto_Valuado = (reg2.getString("MONTO_VALUADO") == null) ? "" : reg2.getString("MONTO_VALUADO");
				String modalidad_plazo = (reg2.getString("MODALIDAD_PLAZO") == null) ? "" : reg2.getString("MODALIDAD_PLAZO");
				String estatus = (reg2.getString("ESTATUS") == null) ? "" : reg2.getString("ESTATUS");
				String tipo_credito = (reg2.getString("TIPO_CREDITO") == null) ? "" : reg2.getString("TIPO_CREDITO");
				String num_docto_final = (reg2.getString("NUM_DOCTO_FINAL") == null) ? "" : reg2.getString("NUM_DOCTO_FINAL");				
				String plazo_final = (reg2.getString("PLAZO_FINAL") == null) ? "" : reg2.getString("PLAZO_FINAL");
				String fecha_venc_final = (reg2.getString("FECHA_VENC_FINAL") == null) ? "" : reg2.getString("FECHA_VENC_FINAL");
				String fecha_opera_final = (reg2.getString("FECHA_OPERA_FINAL") == null) ? "" : reg2.getString("FECHA_OPERA_FINAL");
				String nombre_if = (reg2.getString("NOMBRE_IF") == null) ? "" : reg2.getString("NOMBRE_IF");
				String referencia = (reg2.getString("REFERENCIA_TASA_INT") == null) ? "" : reg2.getString("REFERENCIA_TASA_INT");
				String valor_tasa_int = (reg2.getString("VALOR_TASA_INT") == null) ? "" : reg2.getString("VALOR_TASA_INT");
				String monto_int = (reg2.getString("MONTO_INTERES") == null) ? "" : reg2.getString("MONTO_INTERES");
				String tipo_cobro_int = (reg2.getString("TIPO_COBRO_INT") == null) ? "" : reg2.getString("TIPO_COBRO_INT");
				String ic_moneda_linea = (reg2.getString("IC_MONEDA_LINEA") == null) ? "" : reg2.getString("IC_MONEDA_LINEA");	
				String ic_moneda = (reg2.getString("IC_MONEDA") == null) ? "" : reg2.getString("IC_MONEDA");	
				String ventaCartera = (reg2.getString("CG_VENTACARTERA") == null) ? "" : reg2.getString("CG_VENTACARTERA");				
				//FODEA-013-2014 MOD(SE OBTIENEN LOS VALORES DE LAS NUEVAS COLUMNAS AL CSV)
				String comisionAplicable = (reg2.getString("COMISION_APLICABLE") == null) ? "" : reg2.getString("COMISION_APLICABLE");	
				String montoComision = (reg2.getString("MONTO_COMISION") == null) ? "" : reg2.getString("MONTO_COMISION");	
				String montoDepositar = (reg2.getString("MONTO_DEPOSITAR") == null) ? "" : reg2.getString("MONTO_DEPOSITAR");	
				String bins = (reg2.getString("BINS") == null) ? "" : reg2.getString("BINS");	 
				String operaTajeta = (reg2.getString("OPERA_TARJETA") == null) ? "" : reg2.getString("OPERA_TARJETA");	 
				String numTarjeta = (reg2.getString("NUM_TC") == null) ? "" : reg2.getString("NUM_TC");	 
				double monto2  = Double.parseDouble((String)monto)-Double.parseDouble((String)monto_desc);
				String monto2_1 = Double.toString (monto2) ;
			    String tipoPago = (reg2.getString("TIPOPAGO") == null) ? "" : reg2.getString("TIPOPAGO"); 
			                
				/////////////////////////
				//	Si opera con tarjeta de credito
				/*if(	operaTajeta.equals("S")	){
					plazo_final = "N/A";
					fecha_venc_final = "N/A";
					valor_tasa_int = "N/A";
					monto_int = "N/A";
					referencia = "N/A";
				}
				if(	operaTajeta.equals("N") ||	!estatus.equals("Operada TC")	){
					comisionAplicable = "N/A";
					montoComision = "N/A";
					montoDepositar = "N/A";
					numTarjeta = "N/A";
				}*/
				//	Si opera con tarjeta de credito
				/*if(	operaTajeta.equals("S")	){
					plazo_final = "N/A";
					fecha_venc_final = "N/A";
					valor_tasa_int = "N/A";
					monto_int = "N/A";
					referencia = "N/A";
				}*/
				//Validaciones 
					//Si no es Operada TC
					if(!estatus.equals("Operada TC")){
						bins		= "N/A";
						comisionAplicable = "N/A";
						montoComision = "N/A";
						montoDepositar = "N/A";
						numTarjeta	=	"N/A";
					}else{
						plazo_final = "N/A";
						fecha_venc_final = "N/A";
						valor_tasa_int = "N/A";
						monto_int = "N/A";
						referencia = "N/A";
					}
				
				contenidoArchivo.append(nombreEpo.replace(',',' ')+", "+
				 nombrePYME .replace(',',' ')+", "+
				 num_acuse.replace(',',' ')+", "+
				 num_docto_inicial.replace(',',' ')+", "+	
				 fecha_publicacion.replace(',',' ')+", "+
				 fecha_Emision.replace(',',' ')+", "+
				 fecha_vencimiento.replace(',',' ')+", "+
				 plazo_docto.replace(',',' ')+", "+
				 moneda.replace(',',' ')+", "+
				 monto.replace(',',' ')+", "+
				 plazo_desc.replace(',',' ')+", "+
				 porc_desc.replace(',',' ')+", "+
				 monto_desc.replace(',',' ')+", ");
				 
				 if(ic_moneda.equals(ic_moneda_linea)){
					contenidoArchivo.append(" , , , ");					 
				 }else  {
					 contenidoArchivo.append(tipo_convenio.replace(',',' ')+", "+
					 tipo_cambio.replace(',',' ')+", "+
					 monto_Valuado.replace(',',' ')+", ");
				 }
				 if(ventaCartera.equals("S")){
						contenidoArchivo.append(" , ");
					}else  {
						contenidoArchivo.append(modalidad_plazo.replace(',',' ')+", ");
					}
					
				contenidoArchivo.append(tipoPago.replace(',',' ')+", "+
				 estatus.replace(',',' ')+", "+
				 tipo_credito.replace(',',' ')+", "+
				 num_docto_final.replace(',',' ')+", "+
				 monto2_1.replace(',',' ')+", "+ 
				 //FODEA-013-2014 MOD(SE AGRAGAN LOS VALORES DE LAS NUEVAS COLUMNAS AL CSV)
				 comisionAplicable.replace(',',' ')+", "+ 
				 montoComision.replace(',',' ')+", "+ 
				 montoDepositar.replace(',',' ')+", "+ 
				 ////
				 plazo_final.replace(',',' ')+", "+ 
				 fecha_venc_final.replace(',',' ')+", "+
				 fecha_opera_final.replace(',',' ')+", "+
				 nombre_if.replace(',',' ')+", "+
				 bins.replace(',',' ')+", "+//nombre del producto
				 referencia.replace(',',' ')+", "+
				 valor_tasa_int.replace(',',' ')+", "+
				 monto_int.replace(',',' ')+", "+
				 tipo_cobro_int.replace(',',' ')+", "+
				 ((!"".equals(numTarjeta.trim()) && !"N/A".equals(numTarjeta.trim()))?"XXXX-XXXX-XXXX-"+numTarjeta:numTarjeta)+ "\n ");
				
				total++;
				if(total==1000){					
					total=0;	
					buffer.write(contenidoArchivo.toString());
					contenidoArchivo = new StringBuffer();//Limpio  
				}					
			 }
			
			buffer.write(contenidoArchivo.toString());
			
			buffer.close();	
			contenidoArchivo = new StringBuffer();//Limpio    
					
						
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
			
			} catch(Exception e) {}
				log.debug("crearCustomFile (S)");
			}
			log.info("crearCustomFile (S)");
			return nombreArchivo;
		}	
			
			
		public String getTipo_credito() {
			return tipo_credito;
		}
	
		public void setTipo_credito(String tipo_credito) {
			this.tipo_credito = tipo_credito;
		}

	}

