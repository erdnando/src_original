package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class AvNotifIfDist implements IQueryGenerator, IQueryGeneratorRegExtJS{
	public AvNotifIfDist(){}
	private static final Log log = ServiceLocator.getInstance().getLog(AvNotifIfDist.class);
	StringBuffer 	querySentencia;
	StringBuffer 	condicion;
	private List 	conditions;
	private String paginaOffset;
	private String paginaNo;
	private String	ses_ic_if;
	private String ic_epo;
	private String ic_pyme;
	private String fchinicial;
	private String fchfinal;
	private String tipoCredito;
	private String tipoConsulta;
	private String strPerfil;
	private String numOrden;


	public String getAggregateCalculationQuery(HttpServletRequest request) {
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer qryAux		  = new StringBuffer();
		StringBuffer condicion    = new StringBuffer();   

		String ses_ic_if			= request.getSession().getAttribute("iNoCliente").toString();
		String ic_pyme				= (request.getParameter("ic_pyme") == null)?"":request.getParameter("ic_pyme");
		String ic_epo				= (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");
	
		String fchinicial			= (request.getParameter("Txtfchini") == null)?"":request.getParameter("Txtfchini");
		String fchfinal			= (request.getParameter("Txtffin") == null)?"":request.getParameter("Txtffin");				
		String numOrden = (request.getParameter("numeroOrden") == null)?"":request.getParameter("numeroOrden");

		try {
			   
			condicion.append(" AND D.ic_estatus_docto in(4,11,32)  ");
			
			if(!ic_epo.equals(""))	 
				condicion.append("AND D.IC_EPO =  " + ic_epo);
			if(!ic_pyme.equals(""))	 
				condicion.append(" AND D.IC_PYME =  " +	ic_pyme) ;
			if (!fchinicial.equals("") && !fchfinal.equals(""))
				condicion.append(" AND trunc(C3.df_fecha_hora) BETWEEN TO_DATE('"+fchinicial+"','DD/MM/YYYY') and TO_DATE('"+fchfinal+"','DD/MM/YYYY') ");
			if(ic_epo.equals("") && ic_pyme.equals("") && fchinicial.equals("") && fchfinal.equals(""))
				condicion.append(" AND TO_CHAR(C3.df_fecha_hora,'DD/MM/YYYY')=TO_CHAR(SYSDATE,'DD/MM/YYYY') ");
		if(numOrden.equals("")){
			qryAux.append(
			"   SELECT /*+ index(d CP_DIS_DOCUMENTO_PK)"   +
			"     index(ds CP_DIS_DOCTO_SELECCIONADO_PK)"   +
			"     index(s CP_DIS_SOLICITUD_PK)    "   +
			"     index(m CP_COMCAT_MONEDA_PK)"   +
			"     index(tc CP_COM_TIPO_CAMBIO_PK)"   +
			"     index(pe CP_COMREL_PRODUCTO_EPO_PK)"   +
			"     index(lcd CP_DIS_LINEA_CREDITO_DM_PK)"   +
			"   */"   +
			"    d.ic_moneda as moneda, m.cd_nombre as nommoneda, d.fn_monto as monto"   +
			"   ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
			"   ,DS.fn_importe_recibir as MONTO_CREDITO"   +
			"   ,DS.fn_importe_interes as MONTO_INTERES"   +
			"   , pn.cg_tipo_conversion as cg_tipo_conversion "+
			"	  , p.ic_pyme as ic_pyme  "+
			"   , e.ic_epo  as ic_epo "+
			"   , DS.IC_TASA as IC_TASA"+
			"   FROM DIS_DOCUMENTO D  "   +
			"  ,DIS_DOCTO_SELECCIONADO DS"   +
			"  ,DIS_SOLICITUD S"   +
			"  ,COM_ACUSE3 C3"   +
			"  ,COMCAT_EPO E"   +
			"  ,COMREL_PRODUCTO_EPO PE"   +
			"  ,COMCAT_PRODUCTO_NAFIN PN"   +
			"  ,COMCAT_MONEDA M"   +
			"  ,COM_TIPO_CAMBIO TC"   +
			"  ,DIS_LINEA_CREDITO_DM LCD"   +
			"  ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
			"  ,COMCAT_TASA CT"   +
			"  WHERE D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
			"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
			"  AND D.IC_EPO = PE.IC_EPO"   +
			"  AND D.IC_EPO = E.IC_EPO"   +
			"  AND D.IC_MONEDA = M.IC_MONEDA"   +
			"  AND D.IC_EPO = PE.IC_EPO"   +
			"  AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM"   +
			"  AND DS.IC_TASA = CT.IC_TASA"   +
			"  AND S.CC_ACUSE = C3.CC_ACUSE"   +
			"  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"  AND M.IC_MONEDA = TC.IC_MONEDA"   +
			"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
			"  AND LCD.IC_IF = IEP.IC_IF"   +
			"  AND IEP.IC_EPO = D.IC_EPO"   +
			"  AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"  AND PN.IC_PRODUCTO_NAFIN = 4"  +
			"	AND LCD.IC_IF = "+ses_ic_if+
			"	AND E.CS_HABILITADO = 'S' " +
			condicion.toString());
			qryAux.append(" UNION ALL");
			qryAux.append(
	
			"  	SELECT /*+ index(d CP_DIS_DOCUMENTO_PK)"   +
			"     index(ds CP_DIS_DOCTO_SELECCIONADO_PK)"   +
			"     index(s CP_DIS_SOLICITUD_PK)    "   +
			"     index(m CP_COMCAT_MONEDA_PK)"   +
			"     index(tc CP_COM_TIPO_CAMBIO_PK)"   +
			"     index(pe CP_COMREL_PRODUCTO_EPO_PK)"   +
			"     index(lcd CP_COM_LINEA_CREDITO_PK)"   +
			"   */"   +
			"    d.ic_moneda as moneda, m.cd_nombre as nommoneda, d.fn_monto as monto"   +
			"   ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
			"   ,DS.fn_importe_recibir as MONTO_CREDITO"   +
			"   ,DS.fn_importe_interes as MONTO_INTERES"   +
			"   , pn.cg_tipo_conversion as cg_tipo_conversion "+
			"	  , p.ic_pyme as ic_pyme  "+
			"   , e.ic_epo  as ic_epo "+
			"   , DS.IC_TASA as IC_TASA"+
			"    FROM DIS_DOCUMENTO D"   +
			"   ,DIS_DOCTO_SELECCIONADO DS"   +
			"   ,DIS_SOLICITUD S"   +
			" 	,COM_BINS_IF cbi"+/////7777
			" 	,dis_doctos_pago_tc ddptc"+//////////77
			"   ,COM_ACUSE3 C3"   +
			"   ,COMCAT_EPO E "   +
			"   ,COMREL_PRODUCTO_EPO PE"   +
			"   ,COMCAT_PRODUCTO_NAFIN PN"   +
			"   ,COMCAT_MONEDA M"   +
			"   ,COM_TIPO_CAMBIO TC"   +
			"   ,COM_LINEA_CREDITO LCD"   +
			"   ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
			"   ,COMCAT_TASA CT"   +
			"   WHERE D.IC_EPO = PE.IC_EPO"   +
			" 	 AND D.IC_EPO = E.IC_EPO"   +
			"   AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"   AND PN.IC_PRODUCTO_NAFIN = 4"   +
			"  AND d.IC_BINS = cbi.IC_BINS(+)"  +///////////777
			"   AND (LCD.IC_IF = IEP.IC_IF OR cbi.IC_IF = IEP.IC_IF) "   +///////////
			"   AND (LCD.IC_IF ="+ses_ic_if+" OR cbi.IC_IF = "+ses_ic_if+") "   +//////////////
			" 	AND d.ic_orden_pago_tc = ddptc.ic_orden_pago_tc(+)" +///////////77
			"   AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
			"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
			"   AND D.IC_EPO = PE.IC_EPO"   +
			"   AND D.IC_MONEDA = M.IC_MONEDA"   +
			"   AND M.IC_MONEDA = TC.IC_MONEDA"   +
			"   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
			"   AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
			"   AND LCD.IC_IF = IEP.IC_IF"   +
			"   AND IEP.IC_EPO = D.IC_EPO"   +
			"   AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"   AND DS.IC_TASA = CT.IC_TASA"   +
			"   AND S.CC_ACUSE = C3.CC_ACUSE"  +
			//"	 AND LCD.IC_IF = "+ses_ic_if+
			"	 AND E.CS_HABILITADO = 'S' " +
	  		condicion.toString());
			if(!numOrden.equals("")){
				qryAux.append(" and ddptc.ic_orden_pago_tc = ? ");
				conditions.add(numOrden);
			}
		}else{
			qryAux.append(
	
			"  	SELECT /*+ index(d CP_DIS_DOCUMENTO_PK)"   +
			"     index(ds CP_DIS_DOCTO_SELECCIONADO_PK)"   +
			"     index(s CP_DIS_SOLICITUD_PK)    "   +
			"     index(m CP_COMCAT_MONEDA_PK)"   +
			"     index(tc CP_COM_TIPO_CAMBIO_PK)"   +
			"     index(pe CP_COMREL_PRODUCTO_EPO_PK)"   +
			"     index(lcd CP_COM_LINEA_CREDITO_PK)"   +
			"   */"   +
			"    d.ic_moneda as moneda, m.cd_nombre as nommoneda, d.fn_monto as monto"   +
			"   ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
			"   ,DS.fn_importe_recibir as MONTO_CREDITO"   +
			"   ,DS.fn_importe_interes as MONTO_INTERES"   +
			"   , pn.cg_tipo_conversion as cg_tipo_conversion "+
			"	  , p.ic_pyme as ic_pyme  "+
			"   , e.ic_epo  as ic_epo "+
			"   , DS.IC_TASA as IC_TASA"+
			"    FROM DIS_DOCUMENTO D"   +
			"   ,DIS_DOCTO_SELECCIONADO DS"   +
			"   ,DIS_SOLICITUD S"   +
			" 	,COM_BINS_IF cbi"+/////7777
			" 	,dis_doctos_pago_tc ddptc"+//////////77
			"   ,COM_ACUSE3 C3"   +
			"   ,COMCAT_EPO E "   +
			"   ,COMREL_PRODUCTO_EPO PE"   +
			"   ,COMCAT_PRODUCTO_NAFIN PN"   +
			"   ,COMCAT_MONEDA M"   +
			"   ,COM_TIPO_CAMBIO TC"   +
			"   ,COM_LINEA_CREDITO LCD"   +
			"   ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
			"   ,COMCAT_TASA CT"   +
			"   WHERE D.IC_EPO = PE.IC_EPO"   +
			" 	 AND D.IC_EPO = E.IC_EPO"   +
			"   AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"   AND PN.IC_PRODUCTO_NAFIN = 4"   +
			"  AND d.IC_BINS = cbi.IC_BINS(+)"  +///////////777
			"   AND (LCD.IC_IF = IEP.IC_IF OR cbi.IC_IF = IEP.IC_IF) "   +///////////
			"   AND (LCD.IC_IF ="+ses_ic_if+" OR cbi.IC_IF = "+ses_ic_if+") "   +//////////////
			" 	AND d.ic_orden_pago_tc = ddptc.ic_orden_pago_tc(+)" +///////////77
			"   AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
			"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
			"   AND D.IC_EPO = PE.IC_EPO"   +
			"   AND D.IC_MONEDA = M.IC_MONEDA"   +
			"   AND M.IC_MONEDA = TC.IC_MONEDA"   +
			"   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
			"   AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
			"   AND LCD.IC_IF = IEP.IC_IF"   +
			"   AND IEP.IC_EPO = D.IC_EPO"   +
			"   AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"   AND DS.IC_TASA = CT.IC_TASA"   +
			"   AND S.CC_ACUSE = C3.CC_ACUSE"  +
			//"	 AND LCD.IC_IF = "+ses_ic_if+
			"	 AND E.CS_HABILITADO = 'S' " +
	  		condicion.toString());
			if(!numOrden.equals("")){
				qryAux.append(" and ddptc.ic_orden_pago_tc = ? ");
				conditions.add(numOrden);
			}
			
		}

			qrySentencia.append(
				"SELECT moneda, nommoneda, COUNT (1), SUM (monto), SUM(monto_descuento),SUM(monto_credito),SUM(monto_interes), 'AvisNotifEpoDist::getAggregateCalculationQuery'"+
				"  FROM ("+qryAux.toString()+")"+
				"GROUP BY  moneda, nommoneda ORDER BY moneda ");

		}catch(Exception e){
			System.out.println("AvNotifPymeDist::getAggregateCalculationQuery " + e);
		}
		return qrySentencia.toString();
	}


	/**
	 *
	 */
	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids) {
		log.debug("getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids)");
		int i=0;
		StringBuffer qrySentencia	= new StringBuffer();
    	StringBuffer condicion		= new StringBuffer();
    	
		String ses_ic_if			= request.getSession().getAttribute("iNoCliente").toString();
		if(!tipoConsulta.equals("")&&tipoConsulta.equals("Operada TC"))	 
				condicion.append("AND ed.ic_estatus_docto = 32 ");
		
		condicion.append(" AND d.ic_documento in (");
		i=0;
		for (Iterator it = ids.iterator(); it.hasNext();i++){
        	if(i>0){
				condicion.append(",");
			}
			condicion.append(" "+it.next().toString()+" ");
		}
		condicion.append(") ");
		if(numOrden.equals("")){
			qrySentencia.append(
					"  SELECT /*+ use_nl(D,DS,S,C3,LCD,P,PE,PN,M,TC,TCI,IEP,CT,E,ED,CI,TF)"   +
					"   index(d cp_dis_documento_pk)"   +
					"   index(m cp_comcat_moneda_pk)*/"   +
					"  P.cg_razon_social AS DISTRIBUIDOR"   +
					"  ,D.ig_numero_docto"   +
					
					"  ,D.cc_acuse"   +
					"  ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') AS df_fecha_emision"   +
					"  ,TO_CHAR(D.df_carga,'dd/mm/yyyy') AS df_fecha_publicacion"   +
					"  ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') AS df_fecha_venc"   +
					"  ,D.IG_PLAZO_DOCTO"   +
					"  ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') AS df_fecha_seleccion"   +
					"  ,M.cd_nombre AS MONEDA"   +
					"  ,M.ic_moneda"   +
					"  ,D.fn_monto"   +
					"  ,0 as COMISION_APLICABLE"   +/////////////////7
					"  ,'' as MONTO_DEPOSITAR"   +/////////////////7
					"  ,'' as MONTO_COMISION"   +/////////////////7
					"  ,'' as IC_ORDEN_ENVIADO"   +/////////////////7
					"  ,'' as NUMERO_TC"   +/////////////////7
					"  ,'' as IC_OPERACION_CCACUSE"   +/////////////////7
					"  ,'' AS codigo_autorizado"   +/////////////////7
					"  ,'' as FECHA_REGISTRO"   +////////////////cg_descripcion_bins
					"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') AS TIPO_CONVERSION"   +
					"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
					"  ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) AS MONTO_VALUADO"   +
					"  ,D.IG_PLAZO_DESCUENTO"   +
					"  ,D.FN_MONTO"   +
					"  ,D.fn_porc_descuento"   +
					"  ,D.fn_monto*(NVL(D.fn_porc_descuento,0)/100) AS MONTO_DESCUENTO"   +
					"  ,ED.CD_DESCRIPCION AS ESTATUS"   +
					"  ,0 AS ic_estatus_docto"   +
					"	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) AS REFERENCIA_INT" +
					"  ,DS.fn_valor_tasa  AS VALOR_TASA"   +
					"  ,D.IG_PLAZO_CREDITO"   +
					"  ,DS.fn_importe_recibir AS MONTO_CREDITO"   +
					"  ,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') AS DF_FECHA_VENC_CREDITO"   +
					"  ,TCI.ic_tipo_cobro_interes"   +
					"  ,TCI.cd_descripcion AS tipo_cobro_interes"   +
					"  ,DS.fn_importe_interes AS MONTO_INTERES"   +
					"  ,D.ic_documento"   +
					"  ,E.cg_razon_social AS EPO"   +
					"  , D.ic_documento"   +
					"  ,CI.cg_razon_social AS DIF"   +
					"  ,TF.cd_descripcion AS MODO_PLAZO"   +
					"  ,TO_CHAR(C3.df_fecha_hora,'dd/mm/yyyy') AS df_fecha_hora"   +
					"  ,LCD.ic_moneda AS moneda_linea"   +
					" , pn.cg_tipo_conversion as cg_tipo_conversion "+
					"	  , p.ic_pyme as ic_pyme  "+
					"   , e.ic_epo  as ic_epo "+
					"   , DS.IC_TASA as IC_TASA"+	
					"  ,'' as cg_descripcion_bins"   +
					" , D.IG_TIPO_PAGO " +
					"   FROM DIS_DOCUMENTO D"   +
					"  ,DIS_DOCTO_SELECCIONADO DS"   +
					"  ,DIS_SOLICITUD S"   +
					"  ,COM_ACUSE3 C3"   +
					"  ,DIS_LINEA_CREDITO_DM LCD"   +
					"  ,COMCAT_PYME P"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"  ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"  ,COMCAT_TASA CT"   +
					"  ,COMCAT_EPO E"   +
					"  ,COMCAT_ESTATUS_DOCTO ED"   +
					"  ,COMCAT_IF CI"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  ,'' as cg_descripcion_bins"   +
					"  WHERE D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_PYME = P.IC_PYME"   +
					"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"  AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM"   +
					"  AND D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND D.IC_EPO = E.IC_EPO"   +
					"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
					"  AND D.IC_EPO = IEP.IC_EPO"   +
					"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"  AND DS.IC_TASA = CT.IC_TASA"   +
					"  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"  AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"  AND LCD.IC_IF = IEP.IC_IF"   +
					"  AND LCD.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES"   +
					"  AND S.CC_ACUSE = C3.CC_ACUSE"   +
					"  AND LCD.IC_IF = CI.IC_IF"   +
					"  AND M.IC_MONEDA = TC.IC_MONEDA"   +
					"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"  AND PN.IC_PRODUCTO_NAFIN = 4"   +
					" AND LCD.IC_IF = "+ses_ic_if+
					condicion.toString());
					qrySentencia.append(" UNION ALL");
					qrySentencia.append(
					"  SELECT /*+ use_nl(D,DS,S,C3,LCD,P,PE,PN,M,TC,TCI,IEP,CT,E,ED,CI,TF)"   +
					"   index(d cp_dis_documento_pk)"   +
					"   index(m cp_comcat_moneda_pk)*/"   +
					"  P.cg_razon_social AS DISTRIBUIDOR"   +
					"  ,D.ig_numero_docto"   +
					"  ,D.cc_acuse"   +
					"  ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') AS df_fecha_emision"   +
					"  ,TO_CHAR(D.df_carga,'dd/mm/yyyy') AS df_fecha_publicacion"   +
					"  ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') AS df_fecha_venc"   +
					"  ,D.IG_PLAZO_DOCTO"   +
					"  ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') AS df_fecha_seleccion"   +
					"  ,M.cd_nombre AS MONEDA"   +
					"  ,M.ic_moneda"   +
					"  ,D.fn_monto"   +
					"  ,DS.FN_PORC_COMISION_APLI as COMISION_APLICABLE"   +/////////////////7
					"  ,'' as MONTO_DEPOSITAR"   +
					"  ,'' as MONTO_COMISION"   +
					"  ,DDPTC.IC_ORDEN_PAGO_TC as IC_ORDEN_ENVIADO"   +
					"  ,DDPTC.CG_NUM_TARJETA as NUMERO_TC"   +
					"  ,DDPTC.CG_TRANSACCION_ID as IC_OPERACION_CCACUSE"   +
					"  ,decode(CONCAT(ddptc.CG_CODIGO_RESP ,ddptc.CG_CODIGO_DESC),'','',ddptc.CG_CODIGO_RESP||'-' ||ddptc.CG_CODIGO_DESC)AS codigo_autorizado"   +
					"  ,TO_CHAR(DDPTC.DF_FECHA_AUTORIZACION,'dd/mm/yyyy') as FECHA_REGISTRO"   +////////////////
					
					"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') AS TIPO_CONVERSION"   +
					"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
					"  ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) AS MONTO_VALUADO"   +
					"  ,D.IG_PLAZO_DESCUENTO"   +
					"  ,D.FN_MONTO"   +
					"  ,D.fn_porc_descuento"   +
					"  ,D.fn_monto*(NVL(D.fn_porc_descuento,0)/100) AS MONTO_DESCUENTO"   +
					"  ,ED.CD_DESCRIPCION AS ESTATUS"   +
					"	,ED.IC_ESTATUS_DOCTO AS IC_ESTATUS_DOCTO "+
					"  ,CT.CD_NOMBRE||' '||ds.cg_rel_mat||' '||ds.fn_puntos AS REFERENCIA_INT"   +
					"  ,DECODE(DS.CG_REL_MAT"   +
					"        ,'+',DS.fn_valor_tasa + DS.fn_puntos"   +
					"        ,'-',DS.fn_valor_tasa - DS.fn_puntos"   +
					"        ,'*',DS.fn_valor_tasa * DS.fn_puntos"   +
					"        ,'/',DS.fn_valor_tasa / DS.fn_puntos"   +
					"        ,0) AS VALOR_TASA"   +
					"  ,D.IG_PLAZO_CREDITO"   +
					"  ,DS.fn_importe_recibir AS MONTO_CREDITO"   +
					"  ,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') AS DF_FECHA_VENC_CREDITO"   +
					"  ,TCI.ic_tipo_cobro_interes"   +
					"  ,TCI.cd_descripcion AS tipo_cobro_interes"   +
					"  ,DS.fn_importe_interes AS MONTO_INTERES"   +
					"  ,D.ic_documento"   +
					"  ,E.cg_razon_social AS EPO"   +
					"  , D.ic_documento"   +
					"  ,decode(ed.ic_estatus_docto,32,ci2.cg_razon_social,ci.cg_razon_social) AS dif"   +
					"  ,TF.cd_descripcion AS MODO_PLAZO"   +
					"  ,TO_CHAR(C3.df_fecha_hora,'dd/mm/yyyy') AS df_fecha_hora"   +
					"  ,LCD.ic_moneda AS moneda_linea"   +
					" , pn.cg_tipo_conversion as cg_tipo_conversion "+
					"	  , p.ic_pyme as ic_pyme  "+
					"   , e.ic_epo  as ic_epo "+
					"   , DS.IC_TASA as IC_TASA,   cbi.CG_CODIGO_BIN||' ' ||cbi.cg_descripcion AS cg_descripcion_bins"+
					" , D.IG_TIPO_PAGO " +
					"   FROM DIS_DOCUMENTO D"   +
					"  ,DIS_DOCTOS_PAGO_TC DDPTC"   +
					"  ,COM_BINS_IF cbi "+
					"	,comcat_if ci2 "+
					"  ,DIS_DOCTO_SELECCIONADO DS"   +
					"  ,DIS_SOLICITUD S"   +
					"  ,COM_ACUSE3 C3"   +
					"  ,COM_LINEA_CREDITO LCD"   +
					"  ,COMCAT_PYME P"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"  ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"  ,COMCAT_TASA CT"   +
					"  ,COMCAT_EPO E"   +
					"  ,COMCAT_ESTATUS_DOCTO ED"   +
					"  ,COMCAT_IF CI"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  WHERE D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_ORDEN_PAGO_TC = DDPTC.IC_ORDEN_PAGO_TC(+)"   +
					"  AND D.IC_PYME = P.IC_PYME"   +
					"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"  AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
					"  AND D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND D.IC_EPO = E.IC_EPO"   +
					"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
					"  AND D.IC_EPO = IEP.IC_EPO"   +
					"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"  AND DS.IC_TASA = CT.IC_TASA"   +
					"  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"  AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"   AND (LCD.IC_IF = IEP.IC_IF OR cbi.IC_IF = IEP.IC_IF) "   +//////////////
					"   AND (LCD.IC_IF ="+ses_ic_if+" OR cbi.IC_IF = "+ses_ic_if+") "   +//////////////
					"  AND LCD.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES"   +
					"  AND S.CC_ACUSE = C3.CC_ACUSE"   +
					"  AND d.IC_BINS = cbi.IC_BINS(+)"  +////////////77
					"  AND cbi.IC_if = ci2.IC_if(+)"  +/////////////////7
					"  AND LCD.IC_IF = CI.IC_IF"   +
					"  AND M.IC_MONEDA = TC.IC_MONEDA"   +
					"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"  AND PN.IC_PRODUCTO_NAFIN = 4"  +
					//" AND LCD.IC_IF = "+ses_ic_if+
					condicion.toString());
					if(!numOrden.equals("")){
						condicion.append(" and ddptc.ic_orden_pago_tc ="+numOrden);
					}
		}else{
			qrySentencia.append(
					"  SELECT /*+ use_nl(D,DS,S,C3,LCD,P,PE,PN,M,TC,TCI,IEP,CT,E,ED,CI,TF)"   +
					"   index(d cp_dis_documento_pk)"   +
					"   index(m cp_comcat_moneda_pk)*/"   +
					"  P.cg_razon_social AS DISTRIBUIDOR"   +
					"  ,D.ig_numero_docto"   +
					"  ,D.cc_acuse"   +
					"  ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') AS df_fecha_emision"   +
					"  ,TO_CHAR(D.df_carga,'dd/mm/yyyy') AS df_fecha_publicacion"   +
					"  ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') AS df_fecha_venc"   +
					"  ,D.IG_PLAZO_DOCTO"   +
					"  ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') AS df_fecha_seleccion"   +
					"  ,M.cd_nombre AS MONEDA"   +
					"  ,M.ic_moneda"   +
					"  ,D.fn_monto"   +
					"  ,DS.FN_PORC_COMISION_APLI as COMISION_APLICABLE"   +/////////////////7
					"  ,'' as MONTO_DEPOSITAR"   +
					"  ,'' as MONTO_COMISION"   +
					"  ,DDPTC.IC_ORDEN_PAGO_TC as IC_ORDEN_ENVIADO"   +
					"  ,DDPTC.CG_NUM_TARJETA as NUMERO_TC"   +
					"  ,DDPTC.CG_TRANSACCION_ID as IC_OPERACION_CCACUSE"   +
					"  ,decode(CONCAT(ddptc.CG_CODIGO_RESP ,ddptc.CG_CODIGO_DESC),'','',ddptc.CG_CODIGO_RESP||'-' ||ddptc.CG_CODIGO_DESC)AS codigo_autorizado"   +
					"  ,TO_CHAR(DDPTC.DF_FECHA_AUTORIZACION,'dd/mm/yyyy') as FECHA_REGISTRO"   +////////////////
					
					"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') AS TIPO_CONVERSION"   +
					"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
					"  ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) AS MONTO_VALUADO"   +
					"  ,D.IG_PLAZO_DESCUENTO"   +
					"  ,D.FN_MONTO"   +
					"  ,D.fn_porc_descuento"   +
					"  ,D.fn_monto*(NVL(D.fn_porc_descuento,0)/100) AS MONTO_DESCUENTO"   +
					"  ,ED.CD_DESCRIPCION AS ESTATUS"   +
					"	,ED.IC_ESTATUS_DOCTO AS IC_ESTATUS_DOCTO "+
					"  ,CT.CD_NOMBRE||' '||ds.cg_rel_mat||' '||ds.fn_puntos AS REFERENCIA_INT"   +
					"  ,DECODE(DS.CG_REL_MAT"   +
					"        ,'+',DS.fn_valor_tasa + DS.fn_puntos"   +
					"        ,'-',DS.fn_valor_tasa - DS.fn_puntos"   +
					"        ,'*',DS.fn_valor_tasa * DS.fn_puntos"   +
					"        ,'/',DS.fn_valor_tasa / DS.fn_puntos"   +
					"        ,0) AS VALOR_TASA"   +
					"  ,D.IG_PLAZO_CREDITO"   +
					"  ,DS.fn_importe_recibir AS MONTO_CREDITO"   +
					"  ,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') AS DF_FECHA_VENC_CREDITO"   +
					"  ,TCI.ic_tipo_cobro_interes"   +
					"  ,TCI.cd_descripcion AS tipo_cobro_interes"   +
					"  ,DS.fn_importe_interes AS MONTO_INTERES"   +
					"  ,D.ic_documento"   +
					"  ,E.cg_razon_social AS EPO"   +
					"  , D.ic_documento"   +
					"  ,decode(ed.ic_estatus_docto,32,ci2.cg_razon_social,ci.cg_razon_social) AS dif"   +
					"  ,TF.cd_descripcion AS MODO_PLAZO"   +
					"  ,TO_CHAR(C3.df_fecha_hora,'dd/mm/yyyy') AS df_fecha_hora"   +
					"  ,LCD.ic_moneda AS moneda_linea"   +
					" , pn.cg_tipo_conversion as cg_tipo_conversion "+
					"	  , p.ic_pyme as ic_pyme  "+
					"   , e.ic_epo  as ic_epo "+
					"   , DS.IC_TASA as IC_TASA,   cbi.CG_CODIGO_BIN||' ' ||cbi.cg_descripcion AS cg_descripcion_bins"+
					" , D.IG_TIPO_PAGO " +
					"   FROM DIS_DOCUMENTO D"   +
					"  ,DIS_DOCTOS_PAGO_TC DDPTC"   +
					"  ,COM_BINS_IF cbi "+
					"	,comcat_if ci2 "+
					"  ,DIS_DOCTO_SELECCIONADO DS"   +
					"  ,DIS_SOLICITUD S"   +
					"  ,COM_ACUSE3 C3"   +
					"  ,COM_LINEA_CREDITO LCD"   +
					"  ,COMCAT_PYME P"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"  ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"  ,COMCAT_TASA CT"   +
					"  ,COMCAT_EPO E"   +
					"  ,COMCAT_ESTATUS_DOCTO ED"   +
					"  ,COMCAT_IF CI"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  WHERE D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_ORDEN_PAGO_TC = DDPTC.IC_ORDEN_PAGO_TC(+)"   +
					"  AND D.IC_PYME = P.IC_PYME"   +
					"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"  AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
					"  AND D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND D.IC_EPO = E.IC_EPO"   +
					"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
					"  AND D.IC_EPO = IEP.IC_EPO"   +
					"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"  AND DS.IC_TASA = CT.IC_TASA"   +
					"  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"  AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"   AND (LCD.IC_IF = IEP.IC_IF OR cbi.IC_IF = IEP.IC_IF) "   +//////////////
					"   AND (LCD.IC_IF ="+ses_ic_if+" OR cbi.IC_IF = "+ses_ic_if+") "   +//////////////
					"  AND LCD.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES"   +
					"  AND S.CC_ACUSE = C3.CC_ACUSE"   +
					"  AND d.IC_BINS = cbi.IC_BINS(+)"  +////////////77
					"  AND cbi.IC_if = ci2.IC_if(+)"  +/////////////////7
					"  AND LCD.IC_IF = CI.IC_IF"   +
					"  AND M.IC_MONEDA = TC.IC_MONEDA"   +
					"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"  AND PN.IC_PRODUCTO_NAFIN = 4"  +
					//" AND LCD.IC_IF = "+ses_ic_if+
					condicion.toString());
					if(!numOrden.equals("")){
						condicion.append(" and ddptc.ic_orden_pago_tc ="+numOrden);
					}
		
		}

		System.out.println("el query queda de la siguiente manera "+qrySentencia.toString());
		return qrySentencia.toString();
	}
	
	
	/**
	 *
	 */
	public String getDocumentQuery(HttpServletRequest request) {

      String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion    = new StringBuffer();
		
		String ses_ic_if			= request.getSession().getAttribute("iNoCliente").toString();
		String ic_pyme				= (request.getParameter("ic_pyme") == null)?"":request.getParameter("ic_pyme");
		String ic_epo				= (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");
	
		String fchinicial			= (request.getParameter("Txtfchini") == null)?"":request.getParameter("Txtfchini");
		String fchfinal			= (request.getParameter("Txtffin") == null)?"":request.getParameter("Txtffin");		
		String numOrden = (request.getParameter("numeroOrden") == null)?"":request.getParameter("numeroOrden");

		try {

			
				
			condicion.append(" AND D.ic_estatus_docto in(4,11,32)  ");
			if(!ic_epo.equals(""))	 
				condicion.append("AND D.IC_EPO =  " + ic_epo);
			if(!ic_pyme.equals(""))	 
				condicion.append(" AND D.IC_PYME =  " +	ic_pyme) ;
			if (!fchinicial.equals("") && !fchfinal.equals(""))
				condicion.append(" AND trunc(C3.df_fecha_hora) BETWEEN TO_DATE('"+fchinicial+"','DD/MM/YYYY') and TO_DATE('"+fchfinal+"','DD/MM/YYYY') ");
			if(ic_epo.equals("") && ic_pyme.equals("") && fchinicial.equals("") && fchfinal.equals(""))
				condicion.append(" AND TO_CHAR(C3.df_fecha_hora,'DD/MM/YYYY')=TO_CHAR(SYSDATE,'DD/MM/YYYY') ");
			
		if(numOrden.equals("")){	 
			qrySentencia.append(// preguntar la parte de 
					"   SELECT /*+index(d cp_dis_documento_pk)*/"   +
					"   D.ic_documento"   +
					"    FROM DIS_DOCUMENTO D"   +
					"   ,DIS_DOCTO_SELECCIONADO DS"   +
					"   ,DIS_SOLICITUD S"   +
					"   ,COM_ACUSE3 C3"   +
					"   ,COMCAT_EPO E "   +
					"   ,DIS_LINEA_CREDITO_DM LCD"   +
					"   ,COMREL_PRODUCTO_EPO PE"   +
					"   ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"   WHERE D.IC_EPO = PE.IC_EPO"   +
					" 	 AND D.IC_EPO = E.IC_EPO " +
					"   AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"   AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM"   +
					"   AND D.IC_EPO = PE.IC_EPO"   +
					"   AND D.IC_EPO = IEP.IC_EPO"   +
					"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"   AND D.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"   AND D.IC_PRODUCTO_NAFIN = IEP.IC_PRODUCTO_NAFIN"   +
					"   AND LCD.IC_IF = IEP.IC_IF"   +
					"   AND S.CC_ACUSE = C3.CC_ACUSE"  +
					" AND LCD.IC_IF = "+ses_ic_if+
					" 	 AND E.cs_habilitado = 'S' " +
					condicion.toString());
					qrySentencia.append(" UNION ALL");
					qrySentencia.append("   SELECT /*+ index(d cp_dis_documento_pk) */"   +
					"   D.ic_documento"   +
					"   FROM DIS_DOCUMENTO D"   +
					"   ,DIS_DOCTO_SELECCIONADO DS"   +
					" 	,COM_BINS_IF cbi"+
					"  ,DIS_DOCTOS_PAGO_TC DDPTC"   +
					"   ,DIS_SOLICITUD S"   +
					"   ,COM_ACUSE3 C3"   +
					"   , COMCAT_EPO E " +
					"   ,COM_LINEA_CREDITO LCD"   +
					"   ,COMREL_PRODUCTO_EPO PE"   +
					"   ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"   WHERE D.IC_EPO = PE.IC_EPO"   +
					"  AND d.IC_BINS = cbi.IC_BINS(+)"  +
					"  AND D.IC_ORDEN_PAGO_TC = DDPTC.IC_ORDEN_PAGO_TC(+)"   +
					" 	 AND D.IC_EPO = E.IC_EPO "   +
					"   AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"   AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
					"   AND D.IC_EPO = PE.IC_EPO"   +
					"   AND D.IC_EPO = IEP.IC_EPO"   +
					"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"   AND D.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"   AND D.IC_PRODUCTO_NAFIN = IEP.IC_PRODUCTO_NAFIN"   +
					"   AND (LCD.IC_IF = IEP.IC_IF OR cbi.IC_IF = IEP.IC_IF) "   +//////////////
					"   AND (LCD.IC_IF ="+ses_ic_if+" OR cbi.IC_IF = "+ses_ic_if+") "   +//////////////
					"   AND S.CC_ACUSE = C3.CC_ACUSE"   +
					//" AND LCD.IC_IF = "+ses_ic_if+
					" 	 AND E.cs_habilitado = 'S' " +
					condicion.toString());
				if(!numOrden.equals(""))	 
				condicion.append(" and ddptc.ic_orden_pago_tc = " + numOrden) ;
		}else{
			qrySentencia.append("   SELECT /*+ index(d cp_dis_documento_pk) */"   +
					"   D.ic_documento"   +
					"   FROM DIS_DOCUMENTO D"   +
					"   ,DIS_DOCTO_SELECCIONADO DS"   +
					" 	,COM_BINS_IF cbi"+
					"  ,DIS_DOCTOS_PAGO_TC DDPTC"   +
					"   ,DIS_SOLICITUD S"   +
					"   ,COM_ACUSE3 C3"   +
					"   , COMCAT_EPO E " +
					"   ,COM_LINEA_CREDITO LCD"   +
					"   ,COMREL_PRODUCTO_EPO PE"   +
					"   ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"   WHERE D.IC_EPO = PE.IC_EPO"   +
					"  AND d.IC_BINS = cbi.IC_BINS(+)"  +
					"  AND D.IC_ORDEN_PAGO_TC = DDPTC.IC_ORDEN_PAGO_TC(+)"   +
					" 	 AND D.IC_EPO = E.IC_EPO "   +
					"   AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"   AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
					"   AND D.IC_EPO = PE.IC_EPO"   +
					"   AND D.IC_EPO = IEP.IC_EPO"   +
					"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"   AND D.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"   AND D.IC_PRODUCTO_NAFIN = IEP.IC_PRODUCTO_NAFIN"   +
					"   AND (LCD.IC_IF = IEP.IC_IF OR cbi.IC_IF = IEP.IC_IF) "   +//////////////
					"   AND (LCD.IC_IF ="+ses_ic_if+" OR cbi.IC_IF = "+ses_ic_if+") "   +//////////////
					"   AND S.CC_ACUSE = C3.CC_ACUSE"   +
				//	" AND LCD.IC_IF = "+ses_ic_if+
					" 	 AND E.cs_habilitado = 'S' " +
					condicion.toString());
				if(!numOrden.equals(""))	 
				condicion.append(" and ddptc.ic_orden_pago_tc = " + numOrden) ;
		
		}

			System.out.println("El query de la llave primaria: "+qrySentencia.toString());
		}catch(Exception e){
			System.out.println("AvNotifPymeDist::getDocumentQuery Exception "+e);
		}
		return qrySentencia.toString();
	}
	
	
	/**
	 *
	 */
	public String getDocumentQueryFile(HttpServletRequest request) {
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion    = new StringBuffer();
		
		String ses_ic_if			= request.getSession().getAttribute("iNoCliente").toString();
		String ic_pyme				= (request.getParameter("ic_pyme") == null)?"":request.getParameter("ic_pyme");
		String ic_epo				= (request.getParameter("ic_epo") == null)?"":request.getParameter("ic_epo");
	
		String fchinicial			= (request.getParameter("Txtfchini") == null)?"":request.getParameter("Txtfchini");
		String fchfinal			= (request.getParameter("Txtffin") == null)?"":request.getParameter("Txtffin");		
		
		try {
			if(!tipoConsulta.equals("")&&tipoConsulta.equals("Operada TC"))	 
				condicion.append("AND ed.ic_estatus_docto = 32 " );
		
			condicion.append(" AND D.ic_estatus_docto in(4,11,32)  ");
			if(!ic_epo.equals(""))	 
				condicion.append("AND D.IC_EPO =  " + ic_epo);
			if(!ic_pyme.equals(""))	 
				condicion.append(" AND D.IC_PYME =  " +	ic_pyme) ;
			if (!fchinicial.equals("") && !fchfinal.equals(""))
				condicion.append(" AND trunc(C3.df_fecha_hora) BETWEEN TO_DATE('"+fchinicial+"','DD/MM/YYYY') and TO_DATE('"+fchfinal+"','DD/MM/YYYY') ");
			if(ic_epo.equals("") && ic_pyme.equals("") && fchinicial.equals("") && fchfinal.equals(""))
				condicion.append(" AND TO_CHAR(C3.df_fecha_hora,'DD/MM/YYYY')=TO_CHAR(SYSDATE,'DD/MM/YYYY') ");
			
		if(numOrden.equals("")){
			qrySentencia.append(
					"  SELECT /*+ use_nl(D,DS,S,C3,LCD,P,PE,PN,M,TC,TCI,IEP,CT,E,ED,CI,TF)"   +
					"   index(d cp_dis_documento_pk)"   +
					"   index(m cp_comcat_moneda_pk)*/"   +
					"  P.cg_razon_social AS DISTRIBUIDOR"   +
					
					"  ,D.ig_numero_docto"   +
					"  ,D.cc_acuse"   +
					"  ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') AS df_fecha_emision"   +
					"  ,TO_CHAR(D.df_carga,'dd/mm/yyyy') AS df_fecha_publicacion"   +
					"  ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') AS df_fecha_venc"   +
					"  ,D.IG_PLAZO_DOCTO"   +
					"  ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') AS df_fecha_seleccion"   +
					"  ,M.cd_nombre AS MONEDA"   +
					"  ,M.ic_moneda"   +
					"  ,D.fn_monto"   +
					"  ,0 as COMISION_APLICABLE"   +/////////////////7
					"  ,'' as MONTO_DEPOSITAR"   +/////////////////7
					"  ,'' as MONTO_COMISION"   +/////////////////7
					"  ,'' as IC_ORDEN_ENVIADO"   +/////////////////7
					"  ,'' as NUMERO_TC"   +/////////////////7
					"  ,'' as IC_OPERACION_CCACUSE"   +/////////////////7
					"  ,'' AS codigo_autorizado"   +/////////////////7
					"  ,'' as FECHA_REGISTRO"   +////////////////
					
					"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') AS TIPO_CONVERSION"   +
					"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
					"  ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) AS MONTO_VALUADO"   +
					"  ,D.IG_PLAZO_DESCUENTO"   +
					"  ,D.FN_MONTO"   +
					"  ,D.fn_porc_descuento"   +
					"  ,D.fn_monto*(NVL(D.fn_porc_descuento,0)/100) AS MONTO_DESCUENTO"   +
					"  ,ED.CD_DESCRIPCION AS ESTATUS"   +
					"  ,0 AS ic_estatus_docto"   +
					"	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) AS REFERENCIA_INT" +
					"  ,DS.fn_valor_tasa  AS VALOR_TASA"   +
					"  ,D.IG_PLAZO_CREDITO"   +
					"  ,DS.fn_importe_recibir AS MONTO_CREDITO"   +
					"  ,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') AS DF_FECHA_VENC_CREDITO"   +
					"  ,TCI.ic_tipo_cobro_interes"   +
					"  ,TCI.cd_descripcion AS tipo_cobro_interes"   +
					"  ,DS.fn_importe_interes AS MONTO_INTERES"   +
					"  ,D.ic_documento"   +
					"  ,E.cg_razon_social AS EPO"   +
					"  , D.ic_documento"   +
					"  ,CI.cg_razon_social AS DIF"   +
					"  ,TF.cd_descripcion AS MODO_PLAZO"   +
					"  ,TO_CHAR(C3.df_fecha_hora,'dd/mm/yyyy') AS df_fecha_hora"   +
					"  ,LCD.ic_moneda AS moneda_linea"   +
					" , pn.cg_tipo_conversion as cg_tipo_conversion "+
					"	  , p.ic_pyme as ic_pyme  "+
					"   , e.ic_epo  as ic_epo "+
					"   , DS.IC_TASA as IC_TASA"+
					"  ,'' as cg_descripcion_bins"   +
					" , D.IG_TIPO_PAGO " +
					" , D.IG_TIPO_PAGO " +
					"   FROM DIS_DOCUMENTO D"   +
					"  ,DIS_DOCTO_SELECCIONADO DS"   +
					"  ,DIS_SOLICITUD S"   +
					"  ,COM_ACUSE3 C3"   +
					"  ,DIS_LINEA_CREDITO_DM LCD"   +
					"  ,COMCAT_PYME P"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"  ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"  ,COMCAT_TASA CT"   +
					"  ,COMCAT_EPO E"   +
					"  ,COMCAT_ESTATUS_DOCTO ED"   +
					"  ,COMCAT_IF CI"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  WHERE D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_PYME = P.IC_PYME"   +
					"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"  AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM"   +
					"  AND D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND D.IC_EPO = E.IC_EPO"   +
					"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
					"  AND D.IC_EPO = IEP.IC_EPO"   +
					"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"  AND DS.IC_TASA = CT.IC_TASA"   +
					"  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"  AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"  AND LCD.IC_IF = IEP.IC_IF"   +
					"  AND LCD.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES"   +
					"  AND S.CC_ACUSE = C3.CC_ACUSE"   +
					"  AND LCD.IC_IF = CI.IC_IF"   +
					"  AND M.IC_MONEDA = TC.IC_MONEDA"   +
					"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"  AND PN.IC_PRODUCTO_NAFIN = 4"   +
					" AND LCD.IC_IF = "+ses_ic_if+
					" AND E.cs_habilitado = 'S' " +
					condicion.toString());
					qrySentencia.append(" UNION ALL");
					qrySentencia.append(
					"  SELECT /*+ use_nl(D,DS,S,C3,LCD,P,PE,PN,M,TC,TCI,IEP,CT,E,ED,CI,TF)"   +
					"   index(d cp_dis_documento_pk)"   +
					"   index(m cp_comcat_moneda_pk)*/"   +
					"  P.cg_razon_social AS DISTRIBUIDOR"   +
					"  ,D.ig_numero_docto"   +
					"  ,D.cc_acuse"   +
					"  ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') AS df_fecha_emision"   +
					"  ,TO_CHAR(D.df_carga,'dd/mm/yyyy') AS df_fecha_publicacion"   +
					"  ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') AS df_fecha_venc"   +
					"  ,D.IG_PLAZO_DOCTO"   +
					"  ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') AS df_fecha_seleccion"   +
					"  ,M.cd_nombre AS MONEDA"   +
					"  ,M.ic_moneda"   +
					"  ,D.fn_monto"   +
					"  ,DS.FN_PORC_COMISION_APLI as COMISION_APLICABLE"   +/////////////////7
					"  ,'' as MONTO_DEPOSITAR"   +
					"  ,'' as MONTO_COMISION"   +
					"  ,DDPTC.IC_ORDEN_PAGO_TC as IC_ORDEN_ENVIADO"   +
					"  ,DDPTC.CG_NUM_TARJETA as NUMERO_TC"   +
					"  ,DDPTC.CG_TRANSACCION_ID as IC_OPERACION_CCACUSE"   +
					"  ,decode(CONCAT(ddptc.CG_CODIGO_RESP ,ddptc.CG_CODIGO_DESC),'','',ddptc.CG_CODIGO_RESP||'-' ||ddptc.CG_CODIGO_DESC)AS codigo_autorizado"   +
					"  ,TO_CHAR(DDPTC.DF_FECHA_AUTORIZACION,'dd/mm/yyyy') as FECHA_REGISTRO"   +////////////////
					
					"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') AS TIPO_CONVERSION"   +
					"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
					"  ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) AS MONTO_VALUADO"   +
					"  ,D.IG_PLAZO_DESCUENTO"   +
					"  ,D.FN_MONTO"   +
					"  ,D.fn_porc_descuento"   +
					"  ,D.fn_monto*(NVL(D.fn_porc_descuento,0)/100) AS MONTO_DESCUENTO"   +
					"  ,ED.CD_DESCRIPCION AS ESTATUS"   +
					"	,ED.IC_ESTATUS_DOCTO AS IC_ESTATUS_DOCTO "+
					"  ,CT.CD_NOMBRE||' '||ds.cg_rel_mat||' '||ds.fn_puntos AS REFERENCIA_INT"   +
					"  ,DECODE(DS.CG_REL_MAT"   +
					"        ,'+',DS.fn_valor_tasa + DS.fn_puntos"   +
					"        ,'-',DS.fn_valor_tasa - DS.fn_puntos"   +
					"        ,'*',DS.fn_valor_tasa * DS.fn_puntos"   +
					"        ,'/',DS.fn_valor_tasa / DS.fn_puntos"   +
					"        ,0) AS VALOR_TASA"   +
					"  ,D.IG_PLAZO_CREDITO"   +
					"  ,DS.fn_importe_recibir AS MONTO_CREDITO"   +
					"  ,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') AS DF_FECHA_VENC_CREDITO"   +
					"  ,TCI.ic_tipo_cobro_interes"   +
					"  ,TCI.cd_descripcion AS tipo_cobro_interes"   +
					"  ,DS.fn_importe_interes AS MONTO_INTERES"   +
					"  ,D.ic_documento"   +
					"  ,E.cg_razon_social AS EPO"   +
					"  , D.ic_documento"   +
					"  ,decode(ed.ic_estatus_docto,32,ci2.cg_razon_social,ci.cg_razon_social) AS dif"   +
					"  ,TF.cd_descripcion AS MODO_PLAZO"   +
					"  ,TO_CHAR(C3.df_fecha_hora,'dd/mm/yyyy') AS df_fecha_hora"   +
					"  ,LCD.ic_moneda AS moneda_linea"   +
					" , pn.cg_tipo_conversion as cg_tipo_conversion "+
					"	  , p.ic_pyme as ic_pyme  "+
					"   , e.ic_epo  as ic_epo "+
					"   , DS.IC_TASA as IC_TASA,   cbi.CG_CODIGO_BIN||' ' ||cbi.cg_descripcion AS cg_descripcion_bins"+			
					" , D.IG_TIPO_PAGO " +
					" , D.IG_TIPO_PAGO " +
					"   FROM DIS_DOCUMENTO D"   +
					"  ,DIS_DOCTOS_PAGO_TC DDPTC"   +
					"  ,COM_BINS_IF cbi "+
					"	,comcat_if ci2 "+/////////////////agregados 
					"  ,DIS_DOCTO_SELECCIONADO DS"   +
					"  ,DIS_SOLICITUD S"   +
					"  ,COM_ACUSE3 C3"   +
					"  ,COM_LINEA_CREDITO LCD"   +
					"  ,COMCAT_PYME P"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"  ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"  ,COMCAT_TASA CT"   +
					"  ,COMCAT_EPO E"   +
					"  ,COMCAT_ESTATUS_DOCTO ED"   +
					"  ,COMCAT_IF CI"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  WHERE D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_ORDEN_PAGO_TC = DDPTC.IC_ORDEN_PAGO_TC(+)"   +
					"  AND D.IC_PYME = P.IC_PYME"   +
					"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"  AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
					"  AND D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND D.IC_EPO = E.IC_EPO"   +
					"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
					"  AND D.IC_EPO = IEP.IC_EPO"   +
					"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"  AND DS.IC_TASA = CT.IC_TASA"   +
					"  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"  AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"   AND (LCD.IC_IF = IEP.IC_IF OR cbi.IC_IF = IEP.IC_IF) "   +//////////////
					"   AND (LCD.IC_IF ="+ses_ic_if+" OR cbi.IC_IF = "+ses_ic_if+") "   +//////////////
					"  AND LCD.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES"   +
					"  AND S.CC_ACUSE = C3.CC_ACUSE"   +
					"  AND d.IC_BINS = cbi.IC_BINS(+)"  +////////////77
					"  AND cbi.IC_if = ci2.IC_if(+)"  +/////////////////7
					"  AND LCD.IC_IF = CI.IC_IF"   +
					"  AND M.IC_MONEDA = TC.IC_MONEDA"   +
					"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"  AND PN.IC_PRODUCTO_NAFIN = 4"  +
					//" AND LCD.IC_IF = "+ses_ic_if+
					" AND E.cs_habilitado = 'S' " +
					condicion.toString());
					if(!numOrden.equals(""))	 
				condicion.append(" and ddptc.ic_orden_pago_tc = " + numOrden) ;
		}else{
			qrySentencia.append(
					"  SELECT /*+ use_nl(D,DS,S,C3,LCD,P,PE,PN,M,TC,TCI,IEP,CT,E,ED,CI,TF)"   +
					"   index(d cp_dis_documento_pk)"   +
					"   index(m cp_comcat_moneda_pk)*/"   +
					"  P.cg_razon_social AS DISTRIBUIDOR"   +
					"  ,D.ig_numero_docto"   +
					"  ,D.cc_acuse"   +
					"  ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') AS df_fecha_emision"   +
					"  ,TO_CHAR(D.df_carga,'dd/mm/yyyy') AS df_fecha_publicacion"   +
					"  ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') AS df_fecha_venc"   +
					"  ,D.IG_PLAZO_DOCTO"   +
					"  ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') AS df_fecha_seleccion"   +
					"  ,M.cd_nombre AS MONEDA"   +
					"  ,M.ic_moneda"   +
					"  ,D.fn_monto"   +
					"  ,DS.FN_PORC_COMISION_APLI as COMISION_APLICABLE"   +/////////////////7
					"  ,'' as MONTO_DEPOSITAR"   +
					"  ,'' as MONTO_COMISION"   +
					"  ,DDPTC.IC_ORDEN_PAGO_TC as IC_ORDEN_ENVIADO"   +
					"  ,DDPTC.CG_NUM_TARJETA as NUMERO_TC"   +
					"  ,DDPTC.CG_TRANSACCION_ID as IC_OPERACION_CCACUSE"   +
					"  ,decode(CONCAT(ddptc.CG_CODIGO_RESP ,ddptc.CG_CODIGO_DESC),'','',ddptc.CG_CODIGO_RESP||'-' ||ddptc.CG_CODIGO_DESC)AS codigo_autorizado"   +
					"  ,TO_CHAR(DDPTC.DF_FECHA_AUTORIZACION,'dd/mm/yyyy') as FECHA_REGISTRO"   +////////////////
					
					"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') AS TIPO_CONVERSION"   +
					"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
					"  ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) AS MONTO_VALUADO"   +
					"  ,D.IG_PLAZO_DESCUENTO"   +
					"  ,D.FN_MONTO"   +
					"  ,D.fn_porc_descuento"   +
					"  ,D.fn_monto*(NVL(D.fn_porc_descuento,0)/100) AS MONTO_DESCUENTO"   +
					"  ,ED.CD_DESCRIPCION AS ESTATUS"   +
					"	,ED.IC_ESTATUS_DOCTO AS IC_ESTATUS_DOCTO "+
					"  ,CT.CD_NOMBRE||' '||ds.cg_rel_mat||' '||ds.fn_puntos AS REFERENCIA_INT"   +
					"  ,DECODE(DS.CG_REL_MAT"   +
					"        ,'+',DS.fn_valor_tasa + DS.fn_puntos"   +
					"        ,'-',DS.fn_valor_tasa - DS.fn_puntos"   +
					"        ,'*',DS.fn_valor_tasa * DS.fn_puntos"   +
					"        ,'/',DS.fn_valor_tasa / DS.fn_puntos"   +
					"        ,0) AS VALOR_TASA"   +
					"  ,D.IG_PLAZO_CREDITO"   +
					"  ,DS.fn_importe_recibir AS MONTO_CREDITO"   +
					"  ,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') AS DF_FECHA_VENC_CREDITO"   +
					"  ,TCI.ic_tipo_cobro_interes"   +
					"  ,TCI.cd_descripcion AS tipo_cobro_interes"   +
					"  ,DS.fn_importe_interes AS MONTO_INTERES"   +
					"  ,D.ic_documento"   +
					"  ,E.cg_razon_social AS EPO"   +
					"  , D.ic_documento"   +
					"  ,decode(ed.ic_estatus_docto,32,ci2.cg_razon_social,ci.cg_razon_social) AS dif"   +
					"  ,TF.cd_descripcion AS MODO_PLAZO"   +
					"  ,TO_CHAR(C3.df_fecha_hora,'dd/mm/yyyy') AS df_fecha_hora"   +
					"  ,LCD.ic_moneda AS moneda_linea"   +
					" , pn.cg_tipo_conversion as cg_tipo_conversion "+
					"	  , p.ic_pyme as ic_pyme  "+
					"   , e.ic_epo  as ic_epo "+
					"   , DS.IC_TASA as IC_TASA,   cbi.CG_CODIGO_BIN||' ' ||cbi.cg_descripcion AS cg_descripcion_bins"+			
					" , D.IG_TIPO_PAGO " +
					" , D.IG_TIPO_PAGO " +
					"   FROM DIS_DOCUMENTO D"   +
					"  ,DIS_DOCTOS_PAGO_TC DDPTC"   +
					"  ,COM_BINS_IF cbi "+
					"	,comcat_if ci2 "+/////////////////agregados 
					"  ,DIS_DOCTO_SELECCIONADO DS"   +
					"  ,DIS_SOLICITUD S"   +
					"  ,COM_ACUSE3 C3"   +
					"  ,COM_LINEA_CREDITO LCD"   +
					"  ,COMCAT_PYME P"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"  ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"  ,COMCAT_TASA CT"   +
					"  ,COMCAT_EPO E"   +
					"  ,COMCAT_ESTATUS_DOCTO ED"   +
					"  ,COMCAT_IF CI"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  WHERE D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_ORDEN_PAGO_TC = DDPTC.IC_ORDEN_PAGO_TC(+)"   +
					"  AND D.IC_PYME = P.IC_PYME"   +
					"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"  AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
					"  AND D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND D.IC_EPO = E.IC_EPO"   +
					"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
					"  AND D.IC_EPO = IEP.IC_EPO"   +
					"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"  AND DS.IC_TASA = CT.IC_TASA"   +
					"  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"  AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"   AND (LCD.IC_IF = IEP.IC_IF OR cbi.IC_IF = IEP.IC_IF) "   +//////////////
					"   AND (LCD.IC_IF ="+ses_ic_if+" OR cbi.IC_IF = "+ses_ic_if+") "   +//////////////
					"  AND LCD.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES"   +
					"  AND S.CC_ACUSE = C3.CC_ACUSE"   +
					"  AND d.IC_BINS = cbi.IC_BINS(+)"  +////////////77
					"  AND cbi.IC_if = ci2.IC_if(+)"  +/////////////////7
					"  AND LCD.IC_IF = CI.IC_IF"   +
					"  AND M.IC_MONEDA = TC.IC_MONEDA"   +
					"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"  AND PN.IC_PRODUCTO_NAFIN = 4"  +
					//" AND LCD.IC_IF = "+ses_ic_if+
					" AND E.cs_habilitado = 'S' " +
					condicion.toString());
					if(!numOrden.equals(""))	 
				condicion.append(" and ddptc.ic_orden_pago_tc = " + numOrden) ;
			
		}

		}catch(Exception e){
			System.out.println("AvNotifPymeDist::getDocumentQueryFile Exception "+e);
		}
		return qrySentencia.toString();
	}
	
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		querySentencia = new StringBuffer();
		conditions 		= new ArrayList();
		StringBuffer qryAux = new StringBuffer();

		if (this.tipoCredito == null || this.tipoCredito.equals("")){
		  if(numOrden.equals("")){
			qryAux.append(
			"   SELECT /*+ index(d CP_DIS_DOCUMENTO_PK)"   +
			"     index(ds CP_DIS_DOCTO_SELECCIONADO_PK)"   +
			"     index(s CP_DIS_SOLICITUD_PK)    "   +
			"     index(m CP_COMCAT_MONEDA_PK)"   +
			"     index(tc CP_COM_TIPO_CAMBIO_PK)"   +
			"     index(pe CP_COMREL_PRODUCTO_EPO_PK)"   +
			"     index(lcd CP_DIS_LINEA_CREDITO_DM_PK)"   +
			"   */"   +
			"    d.ic_moneda as moneda, m.cd_nombre as nommoneda, d.fn_monto as monto"   +
			"   ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
			"   ,DS.fn_importe_recibir as MONTO_CREDITO"   +
			"   ,DS.fn_importe_interes as MONTO_INTERES"   +
			"   , pn.cg_tipo_conversion as cg_tipo_conversion "+
			"	  , p.ic_pyme as ic_pyme  "+
			"   , e.ic_epo  as ic_epo "+
			"   , DS.IC_TASA as IC_TASA"+
			"   FROM DIS_DOCUMENTO D"   +
			"  ,DIS_DOCTO_SELECCIONADO DS"   +
			"  ,DIS_SOLICITUD S"   +
			"  ,COM_ACUSE3 C3"   +
			"  ,COMCAT_EPO E"   +
			"  ,COMCAT_PYME P"   +
			"  ,COMREL_PRODUCTO_EPO PE"   +
			"  ,COMCAT_PRODUCTO_NAFIN PN"   +
			"  ,COMCAT_MONEDA M"   +
			"  ,COM_TIPO_CAMBIO TC"   +
			"  ,DIS_LINEA_CREDITO_DM LCD"   +
			"  ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
			"  ,COMCAT_TASA CT"   +
			"  WHERE D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
			"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
			"  AND D.IC_EPO = PE.IC_EPO"   +
			"  AND D.IC_PYME = P.IC_PYME"   +
			"  AND D.IC_EPO = E.IC_EPO"   +
			"  AND D.IC_MONEDA = M.IC_MONEDA"   +
			"  AND D.IC_EPO = PE.IC_EPO"   +
			"  AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM"   +
			"  AND DS.IC_TASA = CT.IC_TASA"   +
			"  AND S.CC_ACUSE = C3.CC_ACUSE"   +
			"  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"  AND M.IC_MONEDA = TC.IC_MONEDA"   +
			"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
			"  AND LCD.IC_IF = IEP.IC_IF"   +
			"  AND IEP.IC_EPO = D.IC_EPO"   +
			"  AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"  AND PN.IC_PRODUCTO_NAFIN = ? "  +
			"	AND LCD.IC_IF = ? "+
			"	AND E.CS_HABILITADO = ? ");
			conditions.add(new Integer(4));
			conditions.add(new Integer(ses_ic_if));
			conditions.add("S");
			qryAux.append(" AND d.ic_estatus_docto in(?,?,?)  ");
			conditions.add(new Integer(4));
			conditions.add(new Integer(11));
			conditions.add(new Integer(32));
			
			if(!ic_epo.equals(""))	 {
				qryAux.append("AND d.ic_epo = ? ");
				conditions.add(ic_epo);
			}
			if(!ic_pyme.equals(""))	{
				qryAux.append(" AND d.ic_pyme = ? ");
				conditions.add(ic_pyme);
			}
			if (!fchinicial.equals("") && !fchfinal.equals("")){
				qryAux.append(" AND TRUNC(c3.df_fecha_hora) BETWEEN TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
				conditions.add(fchinicial);
				conditions.add(fchfinal);
			}
			if(ic_epo.equals("") && ic_pyme.equals("") && fchinicial.equals("") && fchfinal.equals("")){
				qryAux.append(" AND TO_CHAR(c3.df_fecha_hora,'DD/MM/YYYY')=TO_CHAR(SYSDATE,'DD/MM/YYYY') ");
			}
			qryAux.append(
			" UNION ALL");
			qryAux.append(
			"  	SELECT /*+ index(d CP_DIS_DOCUMENTO_PK)"   +
			"     index(ds CP_DIS_DOCTO_SELECCIONADO_PK)"   +
			"     index(s CP_DIS_SOLICITUD_PK)    "   +
			"     index(m CP_COMCAT_MONEDA_PK)"   +
			"     index(tc CP_COM_TIPO_CAMBIO_PK)"   +
			"     index(pe CP_COMREL_PRODUCTO_EPO_PK)"   +
			"     index(lcd CP_COM_LINEA_CREDITO_PK)"   +
			"   */"   +
			"    d.ic_moneda as moneda, m.cd_nombre as nommoneda, d.fn_monto as monto"   +
			"   ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
			"   ,DS.fn_importe_recibir as MONTO_CREDITO"   +
			"   ,DS.fn_importe_interes as MONTO_INTERES"   +
			"   , pn.cg_tipo_conversion as cg_tipo_conversion "+
			"	  , p.ic_pyme as ic_pyme  "+
			"   , e.ic_epo  as ic_epo "+
			"   , DS.IC_TASA as IC_TASA"+
			"    FROM DIS_DOCUMENTO D"   +
			"   ,DIS_DOCTO_SELECCIONADO DS"   +
			"   ,DIS_SOLICITUD S"   +
			" 	,COM_BINS_IF cbi"+
			" 	,dis_doctos_pago_tc ddptc"+
			"   ,COM_ACUSE3 C3"   +
			"   ,COMCAT_EPO E "   +
			"  ,COMCAT_PYME P"   +
			"   ,COMREL_PRODUCTO_EPO PE"   +
			"   ,COMCAT_PRODUCTO_NAFIN PN"   +
			"   ,COMCAT_MONEDA M"   +
			"   ,COM_TIPO_CAMBIO TC"   +
			"   ,COM_LINEA_CREDITO LCD"   +
			"   ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
			"   ,COMCAT_TASA CT"   +
			"   WHERE D.IC_EPO = PE.IC_EPO"   +
			" 	 AND D.IC_EPO = E.IC_EPO"   +
			"  AND D.IC_PYME = P.IC_PYME"   +
			"   AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"  AND d.IC_BINS = cbi.IC_BINS(+)"  +////////////////////
			" 	AND d.ic_orden_pago_tc = ddptc.ic_orden_pago_tc(+)" +
			"   AND PN.IC_PRODUCTO_NAFIN = ? "   +
			"   AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
			"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
			"   AND D.IC_EPO = PE.IC_EPO"   +
			"   AND D.IC_MONEDA = M.IC_MONEDA"   +
			"   AND M.IC_MONEDA = TC.IC_MONEDA"   +
			"   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
			"   AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
			"   AND (LCD.IC_IF = IEP.IC_IF OR cbi.IC_IF = IEP.IC_IF) "   +//////////////
			"   AND (LCD.IC_IF ="+ses_ic_if+" OR cbi.IC_IF = "+ses_ic_if+") "   +//////////////
			"   AND IEP.IC_EPO = D.IC_EPO"   +
			"   AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"   AND DS.IC_TASA = CT.IC_TASA"   +
			"   AND S.CC_ACUSE = C3.CC_ACUSE"  +
			//"	 AND LCD.IC_IF = ? "+
			"	 AND E.CS_HABILITADO = ? "+
			"	 AND S.CS_FACTORAJE_CON_REC = ? ");
			conditions.add(new Integer(4));
			//conditions.add(new Integer(ses_ic_if));
			conditions.add("S");
			conditions.add("N");
			qryAux.append(" AND d.ic_estatus_docto in(?,?,?)  ");
			conditions.add(new Integer(4));
			conditions.add(new Integer(11));
			conditions.add(new Integer(32));
			
			if(!ic_epo.equals(""))	 {
				qryAux.append("AND d.ic_epo = ? ");
				conditions.add(ic_epo);
			}
			if(!ic_pyme.equals(""))	{
				qryAux.append(" AND d.ic_pyme = ? ") ;
				conditions.add(ic_pyme);
			}
			if (!fchinicial.equals("") && !fchfinal.equals("")){
				qryAux.append(" AND TRUNC(c3.df_fecha_hora) BETWEEN TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
				conditions.add(fchinicial);
				conditions.add(fchfinal);
			}
			if(ic_epo.equals("") && ic_pyme.equals("") && fchinicial.equals("") && fchfinal.equals("")){
				qryAux.append(" AND TO_CHAR(c3.df_fecha_hora,'DD/MM/YYYY')=TO_CHAR(SYSDATE,'DD/MM/YYYY') ");
			}
			if(!numOrden.equals("")){
				qryAux.append(" and ddptc.ic_orden_pago_tc = ? ");
				conditions.add(numOrden);
			}
		  }else{
			qryAux.append(
			"  	SELECT /*+ index(d CP_DIS_DOCUMENTO_PK)"   +
			"     index(ds CP_DIS_DOCTO_SELECCIONADO_PK)"   +
			"     index(s CP_DIS_SOLICITUD_PK)    "   +
			"     index(m CP_COMCAT_MONEDA_PK)"   +
			"     index(tc CP_COM_TIPO_CAMBIO_PK)"   +
			"     index(pe CP_COMREL_PRODUCTO_EPO_PK)"   +
			"     index(lcd CP_COM_LINEA_CREDITO_PK)"   +
			"   */"   +
			"    d.ic_moneda as moneda, m.cd_nombre as nommoneda, d.fn_monto as monto"   +
			"   ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
			"   ,DS.fn_importe_recibir as MONTO_CREDITO"   +
			"   ,DS.fn_importe_interes as MONTO_INTERES"   +
			"   , pn.cg_tipo_conversion as cg_tipo_conversion "+
			"	  , p.ic_pyme as ic_pyme  "+
			"   , e.ic_epo  as ic_epo "+
			"   , DS.IC_TASA as IC_TASA"+
			"    FROM DIS_DOCUMENTO D"   +
			"   ,DIS_DOCTO_SELECCIONADO DS"   +
			"   ,DIS_SOLICITUD S"   +
			" 	,COM_BINS_IF cbi"+
			" 	,dis_doctos_pago_tc ddptc"+
			"   ,COM_ACUSE3 C3"   +
			"   ,COMCAT_EPO E "   +
			"  ,COMCAT_PYME P"   +
			"   ,COMREL_PRODUCTO_EPO PE"   +
			"   ,COMCAT_PRODUCTO_NAFIN PN"   +
			"   ,COMCAT_MONEDA M"   +
			"   ,COM_TIPO_CAMBIO TC"   +
			"   ,COM_LINEA_CREDITO LCD"   +
			"   ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
			"   ,COMCAT_TASA CT"   +
			"   WHERE D.IC_EPO = PE.IC_EPO"   +
			" 	 AND D.IC_EPO = E.IC_EPO"   +
			"  AND D.IC_PYME = P.IC_PYME"   +
			"   AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"  AND d.IC_BINS = cbi.IC_BINS(+)"  +////////////////////
			" 	AND d.ic_orden_pago_tc = ddptc.ic_orden_pago_tc(+)" +
			"   AND PN.IC_PRODUCTO_NAFIN = ? "   +
			"   AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
			"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
			"   AND D.IC_EPO = PE.IC_EPO"   +
			"   AND D.IC_MONEDA = M.IC_MONEDA"   +
			"   AND M.IC_MONEDA = TC.IC_MONEDA"   +
			"   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
			"   AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
			"   AND (LCD.IC_IF = IEP.IC_IF OR cbi.IC_IF = IEP.IC_IF) "   +//////////////
			"   AND (LCD.IC_IF ="+ses_ic_if+" OR cbi.IC_IF = "+ses_ic_if+") "   +//////////////
			"   AND IEP.IC_EPO = D.IC_EPO"   +
			"   AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"   AND DS.IC_TASA = CT.IC_TASA"   +
			"   AND S.CC_ACUSE = C3.CC_ACUSE"  +
		//	"	 AND LCD.IC_IF = ? "+
			"	 AND E.CS_HABILITADO = ? "+
			"	 AND S.CS_FACTORAJE_CON_REC = ? ");
			conditions.add(new Integer(4));
			//conditions.add(new Integer(ses_ic_if));
			conditions.add("S");
			conditions.add("N");
			qryAux.append(" AND d.ic_estatus_docto in(?,?,?)  ");
			conditions.add(new Integer(4));
			conditions.add(new Integer(11));
			conditions.add(new Integer(32));
			
			if(!ic_epo.equals(""))	 {
				qryAux.append("AND d.ic_epo = ? ");
				conditions.add(ic_epo);
			}
			if(!ic_pyme.equals(""))	{
				qryAux.append(" AND d.ic_pyme = ? ") ;
				conditions.add(ic_pyme);
			}
			if (!fchinicial.equals("") && !fchfinal.equals("")){
				qryAux.append(" AND TRUNC(c3.df_fecha_hora) BETWEEN TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
				conditions.add(fchinicial);
				conditions.add(fchfinal);
			}
			if(ic_epo.equals("") && ic_pyme.equals("") && fchinicial.equals("") && fchfinal.equals("")){
				qryAux.append(" AND TO_CHAR(c3.df_fecha_hora,'DD/MM/YYYY')=TO_CHAR(SYSDATE,'DD/MM/YYYY') ");
			}
			if(!numOrden.equals("")){
				qryAux.append(" and ddptc.ic_orden_pago_tc = ? ");
				conditions.add(numOrden);
			}
			  
		  }
		}else if (this.tipoCredito.equals("F")){
			qryAux.append(
			"  	SELECT /*+ index(d CP_DIS_DOCUMENTO_PK)"   +
			"     index(ds CP_DIS_DOCTO_SELECCIONADO_PK)"   +
			"     index(s CP_DIS_SOLICITUD_PK)    "   +
			"     index(m CP_COMCAT_MONEDA_PK)"   +
			"     index(tc CP_COM_TIPO_CAMBIO_PK)"   +
			"     index(pe CP_COMREL_PRODUCTO_EPO_PK)"   +
			"     index(lcd CP_COM_LINEA_CREDITO_PK)"   +
			"   */"   +
			"    d.ic_moneda as moneda, m.cd_nombre as nommoneda, d.fn_monto as monto"   +
			"   ,D.fn_monto*(nvl(D.fn_porc_descuento,0)/100) as MONTO_DESCUENTO"   +
			"   ,DS.fn_importe_recibir as MONTO_CREDITO"   +
			"   ,DS.fn_importe_interes as MONTO_INTERES"   +
			"   , pn.cg_tipo_conversion as cg_tipo_conversion "+
			"	  , p.ic_pyme as ic_pyme  "+
			"   , e.ic_epo  as ic_epo "+
			"   , DS.IC_TASA as IC_TASA"+
			"    FROM DIS_DOCUMENTO D"   +
			"   ,DIS_DOCTO_SELECCIONADO DS"   +
			"   ,DIS_SOLICITUD S"   +
			"   ,COM_ACUSE3 C3"   +
			"   ,COMCAT_EPO E "   +
			"  ,COMCAT_PYME P"   +
			"   ,COMREL_PRODUCTO_EPO PE"   +
			"   ,COMCAT_PRODUCTO_NAFIN PN"   +
			"   ,COMCAT_MONEDA M"   +
			"   ,COM_TIPO_CAMBIO TC"   +
			"   ,COM_LINEA_CREDITO LCD"   +
			"   ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
			"   ,COMCAT_TASA CT"   +
			"   WHERE D.IC_EPO = PE.IC_EPO"   +
			" 	 AND D.IC_EPO = E.IC_EPO"   +
			"  AND D.IC_PYME = P.IC_PYME"   +
			"   AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"   AND PN.IC_PRODUCTO_NAFIN = ? "   +
			"   AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
			"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
			"   AND D.IC_EPO = PE.IC_EPO"   +
			"   AND D.IC_MONEDA = M.IC_MONEDA"   +
			"   AND M.IC_MONEDA = TC.IC_MONEDA"   +
			"   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
			"   AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
			"   AND LCD.IC_IF = IEP.IC_IF"   +
			"   AND IEP.IC_EPO = D.IC_EPO"   +
			"   AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
			"   AND DS.IC_TASA = CT.IC_TASA"   +
			"   AND S.CC_ACUSE = C3.CC_ACUSE"  +
			"	 AND LCD.IC_IF = ? "+
			"	 AND E.CS_HABILITADO = ? "+
			"	 AND S.CS_FACTORAJE_CON_REC = ? ");
			conditions.add(new Integer(4));
			conditions.add(new Integer(ses_ic_if));
			conditions.add("S");
			conditions.add("S");
			qryAux.append(" AND d.ic_estatus_docto in(?,?)  ");
			conditions.add(new Integer(4));
			conditions.add(new Integer(11));

			if(!ic_epo.equals(""))	 {
				qryAux.append("AND d.ic_epo = ? ");
				conditions.add(ic_epo);
			}
			if(!ic_pyme.equals(""))	{
				qryAux.append(" AND d.ic_pyme = ? ") ;
				conditions.add(ic_pyme);
			}
			if (!fchinicial.equals("") && !fchfinal.equals("")){
				qryAux.append(" AND TRUNC(c3.df_fecha_hora) BETWEEN TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
				conditions.add(fchinicial);
				conditions.add(fchfinal);
			}
			if(ic_epo.equals("") && ic_pyme.equals("") && fchinicial.equals("") && fchfinal.equals("")){
				qryAux.append(" AND TO_CHAR(c3.df_fecha_hora,'DD/MM/YYYY')=TO_CHAR(SYSDATE,'DD/MM/YYYY') ");
			}
		}

		querySentencia.append(
			"SELECT moneda, nommoneda, COUNT (1), SUM (monto), SUM(monto_descuento),SUM(monto_credito),SUM(monto_interes), 'AvisNotifEpoDist::getAggregateCalculationQuery'"+
			"  FROM ("+qryAux.toString()+")"+
			"GROUP BY  moneda, nommoneda ORDER BY moneda ");

		log.debug("..:: qrySentencia "+querySentencia.toString());
		log.debug("..:: conditions: "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return querySentencia.toString();
	}

	public String getDocumentSummaryQueryForIds(List pageIds) {
		log.info("getDocumentSummaryQueryForIds(List pageIds)");
		querySentencia = new StringBuffer();
		conditions 		= new ArrayList();
		if (this.tipoCredito == null || this.tipoCredito.equals("")){
			if(numOrden.equals("")){
			querySentencia.append(
					"  SELECT /*+ use_nl(D,DS,S,C3,LCD,P,PE,PN,M,TC,TCI,IEP,CT,E,ED,CI,TF)"   +
					"   index(d cp_dis_documento_pk)"   +
					"   index(m cp_comcat_moneda_pk)*/"   +
					"  P.cg_razon_social AS DISTRIBUIDOR"   +
					
					"  ,D.ig_numero_docto"   +
					"  ,D.cc_acuse"   +
					"  ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') AS df_fecha_emision"   +
					"  ,TO_CHAR(D.df_carga,'dd/mm/yyyy') AS df_fecha_publicacion"   +
					"  ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') AS df_fecha_venc"   +
					"  ,D.IG_PLAZO_DOCTO"   +
					"  ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') AS df_fecha_seleccion"   +
					"  ,M.cd_nombre AS MONEDA"   +
					"  ,M.ic_moneda"   +
					"  ,D.fn_monto"   +
					"  ,0 as COMISION_APLICABLE"   +/////////////////7
					"  ,'' as MONTO_DEPOSITAR"   +/////////////////7
					"  ,'' as MONTO_COMISION"   +/////////////////7
					"  ,'' as IC_ORDEN_ENVIADO"   +/////////////////7
					"  ,'' as NUMERO_TC"   +/////////////////7
					"  ,'' as IC_OPERACION_CCACUSE"   +/////////////////7
					"  ,'' AS codigo_autorizado"   +/////////////////7
					"  ,'' as FECHA_REGISTRO"   +////////////////
					
					"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') AS TIPO_CONVERSION"   +
					"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
					"  ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) AS MONTO_VALUADO"   +
					"  ,D.IG_PLAZO_DESCUENTO"   +
					"  ,D.FN_MONTO"   +
					"  ,D.fn_porc_descuento"   +
					"  ,D.fn_monto*(NVL(D.fn_porc_descuento,0)/100) AS MONTO_DESCUENTO"   +
					"  ,ED.CD_DESCRIPCION AS ESTATUS"   +
					"  ,0 AS ic_estatus_docto"   +
					"	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) AS REFERENCIA_INT" +
					"  ,DS.fn_valor_tasa  AS VALOR_TASA"   +
					"  ,D.IG_PLAZO_CREDITO"   +
					"  ,DS.fn_importe_recibir AS MONTO_CREDITO"   +
					"  ,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') AS DF_FECHA_VENC_CREDITO"   +
					"  ,TCI.ic_tipo_cobro_interes"   +
					"  ,TCI.cd_descripcion AS tipo_cobro_interes"   +
					"  ,DS.fn_importe_interes AS MONTO_INTERES"   +
					"  ,D.ic_documento"   +
					"  ,E.cg_razon_social AS EPO"   +
					"  , D.ic_documento"   +
					"  ,CI.cg_razon_social AS DIF"   +
					"  ,TF.cd_descripcion AS MODO_PLAZO"   +
					"  ,TO_CHAR(C3.df_fecha_hora,'dd/mm/yyyy') AS df_fecha_hora"   +
					"  ,LCD.ic_moneda AS moneda_linea"   +
					" , pn.cg_tipo_conversion as cg_tipo_conversion "+
					"	  , p.ic_pyme as ic_pyme  "+
					"   , e.ic_epo  as ic_epo "+
					"   , DS.IC_TASA as IC_TASA"+		
					"  ,'' as cg_descripcion_bins"   +
					" , D.IG_TIPO_PAGO " +
					"   FROM DIS_DOCUMENTO D"   +
					"  ,DIS_DOCTO_SELECCIONADO DS"   +
					"  ,DIS_SOLICITUD S"   +
					"  ,COM_ACUSE3 C3"   +
					"  ,DIS_LINEA_CREDITO_DM LCD"   +
					"  ,COMCAT_PYME P"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"  ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"  ,COMCAT_TASA CT"   +
					"  ,COMCAT_EPO E"   +
					"  ,COMCAT_ESTATUS_DOCTO ED"   +
					"  ,COMCAT_IF CI"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  WHERE D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_PYME = P.IC_PYME"   +
					"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"  AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM"   +
					"  AND D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND D.IC_EPO = E.IC_EPO"   +
					"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
					"  AND D.IC_EPO = IEP.IC_EPO"   +
					"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"  AND DS.IC_TASA = CT.IC_TASA"   +
					"  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"  AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"  AND LCD.IC_IF = IEP.IC_IF"   +
					"  AND LCD.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES"   +
					"  AND S.CC_ACUSE = C3.CC_ACUSE"   +
					"  AND LCD.IC_IF = CI.IC_IF"   +
					"  AND M.IC_MONEDA = TC.IC_MONEDA"   +
					"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"  AND PN.IC_PRODUCTO_NAFIN = ?"   +
					" AND LCD.IC_IF = ? ");
			conditions.add(new Integer(4));
			conditions.add(new Integer(ses_ic_if));
			
		if(!tipoConsulta.equals("")&&tipoConsulta.equals("Operada TC")){	 
				querySentencia.append("  AND ed.ic_estatus_docto = ?");
				conditions.add(new Integer(32));
			}
			for(int i=0;i<pageIds.size();i++) {
				List lItem = (List)pageIds.get(i);
				if(i==0) {
					querySentencia.append(" AND d.ic_documento in (" );
				}
				querySentencia.append("?");
				conditions.add(new Long(lItem.get(0).toString()));
				if(i!=(pageIds.size()-1)) {
					querySentencia.append(",");
				} else {
					querySentencia.append(" ) ");
				}
			}
			querySentencia.append(
			" UNION ALL");
			querySentencia.append(
					
					"  SELECT /*+ use_nl(D,DS,S,C3,LCD,P,PE,PN,M,TC,TCI,IEP,CT,E,ED,CI,TF)"   +
					"   index(d cp_dis_documento_pk)"   +
					"   index(m cp_comcat_moneda_pk)*/"   +
					"  P.cg_razon_social AS DISTRIBUIDOR"   +
					"  ,D.ig_numero_docto"   +
					"  ,D.cc_acuse"   +
					"  ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') AS df_fecha_emision"   +
					"  ,TO_CHAR(D.df_carga,'dd/mm/yyyy') AS df_fecha_publicacion"   +
					"  ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') AS df_fecha_venc"   +
					"  ,D.IG_PLAZO_DOCTO"   +
					"  ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') AS df_fecha_seleccion"   +
					"  ,M.cd_nombre AS MONEDA"   +
					"  ,M.ic_moneda"   +
					"  ,D.fn_monto"   +
					"  ,DS.FN_PORC_COMISION_APLI as COMISION_APLICABLE"   +/////////////////7
					"  ,'' as MONTO_DEPOSITAR"   +
					"  ,'' as MONTO_COMISION"   +
					"  ,DDPTC.IC_ORDEN_PAGO_TC as IC_ORDEN_ENVIADO"   +
					"  ,DDPTC.CG_NUM_TARJETA as NUMERO_TC"   +
					"  ,DDPTC.CG_TRANSACCION_ID as IC_OPERACION_CCACUSE"   +
					"  ,decode(CONCAT(ddptc.CG_CODIGO_RESP ,ddptc.CG_CODIGO_DESC),'','',ddptc.CG_CODIGO_RESP||'-' ||ddptc.CG_CODIGO_DESC)AS codigo_autorizado"   +
					"  ,TO_CHAR(DDPTC.DF_FECHA_AUTORIZACION,'dd/mm/yyyy') as FECHA_REGISTRO"   +////////////////
					"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') AS TIPO_CONVERSION"   +
					"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
					"  ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) AS MONTO_VALUADO"   +
					"  ,D.IG_PLAZO_DESCUENTO"   +
					"  ,D.FN_MONTO"   +
					"  ,D.fn_porc_descuento"   +
					"  ,D.fn_monto*(NVL(D.fn_porc_descuento,0)/100) AS MONTO_DESCUENTO"   +
					"  ,ED.CD_DESCRIPCION AS ESTATUS"   +
					"	,ED.IC_ESTATUS_DOCTO AS IC_ESTATUS_DOCTO "+
					"  ,CT.CD_NOMBRE||' '||ds.cg_rel_mat||' '||ds.fn_puntos AS REFERENCIA_INT"   +
					"  ,DECODE(DS.CG_REL_MAT"   +
					"        ,'+',DS.fn_valor_tasa + DS.fn_puntos"   +
					"        ,'-',DS.fn_valor_tasa - DS.fn_puntos"   +
					"        ,'*',DS.fn_valor_tasa * DS.fn_puntos"   +
					"        ,'/',DS.fn_valor_tasa / DS.fn_puntos"   +
					"        ,0) AS VALOR_TASA"   +
					"  ,D.IG_PLAZO_CREDITO"   +
					"  ,DS.fn_importe_recibir AS MONTO_CREDITO"   +
					"  ,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') AS DF_FECHA_VENC_CREDITO"   +
					"  ,TCI.ic_tipo_cobro_interes"   +
					"  ,TCI.cd_descripcion AS tipo_cobro_interes"   +
					"  ,DS.fn_importe_interes AS MONTO_INTERES"   +
					"  ,D.ic_documento"   +
					"  ,E.cg_razon_social AS EPO"   +
					"  , D.ic_documento"   +
					"  ,decode(ed.ic_estatus_docto,32,ci2.cg_razon_social,ci.cg_razon_social) AS dif"   +
					"  ,TF.cd_descripcion AS MODO_PLAZO"   +
					"  ,TO_CHAR(C3.df_fecha_hora,'dd/mm/yyyy') AS df_fecha_hora"   +
					"  ,LCD.ic_moneda AS moneda_linea"   +
					" , pn.cg_tipo_conversion as cg_tipo_conversion "+
					"	  , p.ic_pyme as ic_pyme  "+
					"   , e.ic_epo  as ic_epo "+
					"   , DS.IC_TASA as IC_TASA,   cbi.CG_CODIGO_BIN||' ' ||cbi.cg_descripcion AS cg_descripcion_bins"+
					" , D.IG_TIPO_PAGO " +
					"   FROM DIS_DOCUMENTO D"   +
					"  ,DIS_DOCTOS_PAGO_TC DDPTC"   +
					"  ,COM_BINS_IF cbi "+
					"	,comcat_if ci2 "+/////////////////agregados 
					"  ,DIS_DOCTO_SELECCIONADO DS"   +
					"  ,DIS_SOLICITUD S"   +
					"  ,COM_ACUSE3 C3"   +
					"  ,COM_LINEA_CREDITO LCD"   +
					"  ,COMCAT_PYME P"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"  ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"  ,COMCAT_TASA CT"   +
					"  ,COMCAT_EPO E"   +
					"  ,COMCAT_ESTATUS_DOCTO ED"   +
					"  ,COMCAT_IF CI"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  WHERE D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_ORDEN_PAGO_TC = DDPTC.IC_ORDEN_PAGO_TC(+)"   +
					"  AND D.IC_PYME = P.IC_PYME"   +
					"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"  AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
					"  AND D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND D.IC_EPO = E.IC_EPO"   +
					"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
					"  AND D.IC_EPO = IEP.IC_EPO"   +
					"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"  AND DS.IC_TASA = CT.IC_TASA"   +
					"  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"  AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"   AND (LCD.IC_IF = IEP.IC_IF OR cbi.IC_IF = IEP.IC_IF) "   +//////////////
					"   AND (LCD.IC_IF ="+ses_ic_if+" OR cbi.IC_IF = "+ses_ic_if+") "   +//////////////
					"  AND LCD.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES"   +
					"  AND S.CC_ACUSE = C3.CC_ACUSE"   +
					"  AND d.IC_BINS = cbi.IC_BINS(+)"  +////////////77
					"  AND cbi.IC_if = ci2.IC_if(+)"  +/////////////////7
					"  AND LCD.IC_IF = CI.IC_IF"   +
					"  AND M.IC_MONEDA = TC.IC_MONEDA"   +
					"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"  AND PN.IC_PRODUCTO_NAFIN = ? " // +
				//	" AND LCD.IC_IF = ? "
					);
			conditions.add(new Integer(4));
			//conditions.add(new Integer(ses_ic_if));
			if(!tipoConsulta.equals("")&&tipoConsulta.equals("Operada TC")){	 
				querySentencia.append("  AND ed.ic_estatus_docto = ?");
				conditions.add(new Integer(32));
			}
		 
	
			for(int i=0;i<pageIds.size();i++) {
				List lItem = (List)pageIds.get(i);
				if(i==0) {
					querySentencia.append(" AND d.ic_documento in (" );
				}
				querySentencia.append("?");
				conditions.add(new Long(lItem.get(0).toString()));
				if(i!=(pageIds.size()-1)) {
					querySentencia.append(",");
				} else {
					querySentencia.append(" ) ");
				}
			}
		 }else{
		  querySentencia.append(
					
					"  SELECT /*+ use_nl(D,DS,S,C3,LCD,P,PE,PN,M,TC,TCI,IEP,CT,E,ED,CI,TF)"   +
					"   index(d cp_dis_documento_pk)"   +
					"   index(m cp_comcat_moneda_pk)*/"   +
					"  P.cg_razon_social AS DISTRIBUIDOR"   +
					"  ,D.ig_numero_docto"   +
					"  ,D.cc_acuse"   +
					"  ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') AS df_fecha_emision"   +
					"  ,TO_CHAR(D.df_carga,'dd/mm/yyyy') AS df_fecha_publicacion"   +
					"  ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') AS df_fecha_venc"   +
					"  ,D.IG_PLAZO_DOCTO"   +
					"  ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') AS df_fecha_seleccion"   +
					"  ,M.cd_nombre AS MONEDA"   +
					"  ,M.ic_moneda"   +
					"  ,D.fn_monto"   +
					"  ,DS.FN_PORC_COMISION_APLI as COMISION_APLICABLE"   +/////////////////7
					"  ,'' as MONTO_DEPOSITAR"   +
					"  ,'' as MONTO_COMISION"   +
					"  ,DDPTC.IC_ORDEN_PAGO_TC as IC_ORDEN_ENVIADO"   +
					"  ,DDPTC.CG_NUM_TARJETA as NUMERO_TC"   +
					"  ,DDPTC.CG_TRANSACCION_ID as IC_OPERACION_CCACUSE"   +
					"  ,decode(CONCAT(ddptc.CG_CODIGO_RESP ,ddptc.CG_CODIGO_DESC),'','',ddptc.CG_CODIGO_RESP||'-' ||ddptc.CG_CODIGO_DESC)AS codigo_autorizado"   +
					"  ,TO_CHAR(DDPTC.DF_FECHA_AUTORIZACION,'dd/mm/yyyy') as FECHA_REGISTRO"   +////////////////
					"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') AS TIPO_CONVERSION"   +
					"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
					"  ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) AS MONTO_VALUADO"   +
					"  ,D.IG_PLAZO_DESCUENTO"   +
					"  ,D.FN_MONTO"   +
					"  ,D.fn_porc_descuento"   +
					"  ,D.fn_monto*(NVL(D.fn_porc_descuento,0)/100) AS MONTO_DESCUENTO"   +
					"  ,ED.CD_DESCRIPCION AS ESTATUS"   +
					"	,ED.IC_ESTATUS_DOCTO AS IC_ESTATUS_DOCTO "+
					"  ,CT.CD_NOMBRE||' '||ds.cg_rel_mat||' '||ds.fn_puntos AS REFERENCIA_INT"   +
					"  ,DECODE(DS.CG_REL_MAT"   +
					"        ,'+',DS.fn_valor_tasa + DS.fn_puntos"   +
					"        ,'-',DS.fn_valor_tasa - DS.fn_puntos"   +
					"        ,'*',DS.fn_valor_tasa * DS.fn_puntos"   +
					"        ,'/',DS.fn_valor_tasa / DS.fn_puntos"   +
					"        ,0) AS VALOR_TASA"   +
					"  ,D.IG_PLAZO_CREDITO"   +
					"  ,DS.fn_importe_recibir AS MONTO_CREDITO"   +
					"  ,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') AS DF_FECHA_VENC_CREDITO"   +
					"  ,TCI.ic_tipo_cobro_interes"   +
					"  ,TCI.cd_descripcion AS tipo_cobro_interes"   +
					"  ,DS.fn_importe_interes AS MONTO_INTERES"   +
					"  ,D.ic_documento"   +
					"  ,E.cg_razon_social AS EPO"   +
					"  , D.ic_documento"   +
					"  ,decode(ed.ic_estatus_docto,32,ci2.cg_razon_social,ci.cg_razon_social) AS dif"   +
					"  ,TF.cd_descripcion AS MODO_PLAZO"   +
					"  ,TO_CHAR(C3.df_fecha_hora,'dd/mm/yyyy') AS df_fecha_hora"   +
					"  ,LCD.ic_moneda AS moneda_linea"   +
					" , pn.cg_tipo_conversion as cg_tipo_conversion "+
					"	  , p.ic_pyme as ic_pyme  "+
					"   , e.ic_epo  as ic_epo "+
					"   , DS.IC_TASA as IC_TASA,   cbi.CG_CODIGO_BIN||' ' ||cbi.cg_descripcion AS cg_descripcion_bins"+
					" , D.IG_TIPO_PAGO " +
					"   FROM DIS_DOCUMENTO D"   +
					"  ,DIS_DOCTOS_PAGO_TC DDPTC"   +
					"  ,COM_BINS_IF cbi "+
					"	,comcat_if ci2 "+/////////////////agregados 
					"  ,DIS_DOCTO_SELECCIONADO DS"   +
					"  ,DIS_SOLICITUD S"   +
					"  ,COM_ACUSE3 C3"   +
					"  ,COM_LINEA_CREDITO LCD"   +
					"  ,COMCAT_PYME P"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"  ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"  ,COMCAT_TASA CT"   +
					"  ,COMCAT_EPO E"   +
					"  ,COMCAT_ESTATUS_DOCTO ED"   +
					"  ,COMCAT_IF CI"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  WHERE D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_ORDEN_PAGO_TC = DDPTC.IC_ORDEN_PAGO_TC(+)"   +
					"  AND D.IC_PYME = P.IC_PYME"   +
					"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"  AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
					"  AND D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND D.IC_EPO = E.IC_EPO"   +
					"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
					"  AND D.IC_EPO = IEP.IC_EPO"   +
					"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"  AND DS.IC_TASA = CT.IC_TASA"   +
					"  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"  AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"   AND (LCD.IC_IF = IEP.IC_IF OR cbi.IC_IF = IEP.IC_IF) "   +//////////////
					"   AND (LCD.IC_IF ="+ses_ic_if+" OR cbi.IC_IF = "+ses_ic_if+") "   +//////////////
					"  AND LCD.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES"   +
					"  AND S.CC_ACUSE = C3.CC_ACUSE"   +
					"  AND d.IC_BINS = cbi.IC_BINS(+)"  +////////////77
					"  AND cbi.IC_if = ci2.IC_if(+)"  +/////////////////7
					"  AND LCD.IC_IF = CI.IC_IF"   +
					"  AND M.IC_MONEDA = TC.IC_MONEDA"   +
					"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"  AND PN.IC_PRODUCTO_NAFIN = ? "//  +
					//" AND LCD.IC_IF = ? "
					);
			conditions.add(new Integer(4));
		//	conditions.add(new Integer(ses_ic_if));
			if(!tipoConsulta.equals("")&&tipoConsulta.equals("Operada TC")){	 
				querySentencia.append("  AND ed.ic_estatus_docto = ?");
				conditions.add(new Integer(32));
			}
		 
	
			for(int i=0;i<pageIds.size();i++) {
				List lItem = (List)pageIds.get(i);
				if(i==0) {
					querySentencia.append(" AND d.ic_documento in (" );
				}
				querySentencia.append("?");
				conditions.add(new Long(lItem.get(0).toString()));
				if(i!=(pageIds.size()-1)) {
					querySentencia.append(",");
				} else {
					querySentencia.append(" ) ");
				}
			}
			
		 }
			
		}else if (this.tipoCredito.equals("F")){
			querySentencia.append(
					"  SELECT /*+ use_nl(D,DS,S,C3,LCD,P,PE,PN,M,TC,TCI,IEP,CT,E,ED,CI,TF)"   +
					"   index(d cp_dis_documento_pk)"   +
					"   index(m cp_comcat_moneda_pk)*/"   +
					"  P.cg_razon_social AS DISTRIBUIDOR"   +
					"  ,D.ig_numero_docto"   +
					"  ,D.cc_acuse"   +
					"  ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') AS df_fecha_emision"   +
					"  ,TO_CHAR(D.df_carga,'dd/mm/yyyy') AS df_fecha_publicacion"   +
					"  ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') AS df_fecha_venc"   +
					"  ,D.IG_PLAZO_DOCTO"   +
					"  ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') AS df_fecha_seleccion"   +
					"  ,M.cd_nombre AS MONEDA"   +
					"  ,M.ic_moneda"   +
					"  ,D.fn_monto"   +
					"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') AS TIPO_CONVERSION"   +
					"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
					"  ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) AS MONTO_VALUADO"   +
					"  ,D.IG_PLAZO_DESCUENTO"   +
					"  ,D.FN_MONTO"   +
					"  ,D.fn_porc_descuento"   +
					"  ,D.fn_monto*(NVL(D.fn_porc_descuento,0)/100) AS MONTO_DESCUENTO"   +
					"  ,ED.CD_DESCRIPCION AS ESTATUS"   +
					"  ,CT.CD_NOMBRE||' '||ds.cg_rel_mat||' '||ds.fn_puntos AS REFERENCIA_INT"   +
					"  ,DECODE(DS.CG_REL_MAT"   +
					"        ,'+',DS.fn_valor_tasa + DS.fn_puntos"   +
					"        ,'-',DS.fn_valor_tasa - DS.fn_puntos"   +
					"        ,'*',DS.fn_valor_tasa * DS.fn_puntos"   +
					"        ,'/',DS.fn_valor_tasa / DS.fn_puntos"   +
					"        ,0) AS VALOR_TASA"   +
					"  ,D.IG_PLAZO_CREDITO"   +
					"  ,DS.fn_importe_recibir AS MONTO_CREDITO"   +
					"  ,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') AS DF_FECHA_VENC_CREDITO"   +
					"  ,TCI.ic_tipo_cobro_interes"   +
					"  ,TCI.cd_descripcion AS tipo_cobro_interes"   +
					"  ,DS.fn_importe_interes AS MONTO_INTERES"   +
					"  ,D.ic_documento"   +
					"  ,E.cg_razon_social AS EPO"   +
					"  , D.ic_documento"   +
					"  ,CI.cg_razon_social AS DIF"   +
					//"  ,TF.cd_descripcion AS MODO_PLAZO"   +
					"  ,TO_CHAR(C3.df_fecha_hora,'dd/mm/yyyy') AS df_fecha_hora"   +
					"  ,LCD.ic_moneda AS moneda_linea"   +
					" , pn.cg_tipo_conversion as cg_tipo_conversion "+
					"	  , p.ic_pyme as ic_pyme  "+
					"   , e.ic_epo  as ic_epo "+
					"   , DS.IC_TASA as IC_TASA"+
					" , D.IG_TIPO_PAGO " +
					"   FROM DIS_DOCUMENTO D"   +
					"  ,DIS_DOCTO_SELECCIONADO DS"   +
					"  ,DIS_SOLICITUD S"   +
					"  ,COM_ACUSE3 C3"   +
					"  ,COM_LINEA_CREDITO LCD"   +
					"  ,COMCAT_PYME P"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"  ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"  ,COMCAT_TASA CT"   +
					"  ,COMCAT_EPO E"   +
					"  ,COMCAT_ESTATUS_DOCTO ED"   +
					"  ,COMCAT_IF CI"   +
					//"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  WHERE D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_PYME = P.IC_PYME"   +
					"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"  AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
					"  AND D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					//"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND D.IC_EPO = E.IC_EPO"   +
					"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
					"  AND D.IC_EPO = IEP.IC_EPO"   +
					"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"  AND DS.IC_TASA = CT.IC_TASA"   +
					"  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"  AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"  AND LCD.IC_IF = IEP.IC_IF"   +
					"  AND LCD.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES"   +
					"  AND S.CC_ACUSE = C3.CC_ACUSE"   +
					"  AND LCD.IC_IF = CI.IC_IF"   +
					"  AND M.IC_MONEDA = TC.IC_MONEDA"   +
					"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"  AND PN.IC_PRODUCTO_NAFIN = ? "  +
					" AND LCD.IC_IF = ? ");
			conditions.add(new Integer(4));
			conditions.add(new Integer(ses_ic_if));
	
			for(int i=0;i<pageIds.size();i++) {
				List lItem = (List)pageIds.get(i);
				if(i==0) {
					querySentencia.append(" AND d.ic_documento in (" );
				}
				querySentencia.append("?");
				conditions.add(new Long(lItem.get(0).toString()));
				if(i!=(pageIds.size()-1)) {
					querySentencia.append(",");
				} else {
					querySentencia.append(" ) ");
				}
			}
		}

		log.debug("..:: qrySentencia "+querySentencia.toString());
		log.debug("..:: conditions: "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return querySentencia.toString();
	}

	public String getDocumentQuery() {
		log.info("getDocumentQuery(E)");
		querySentencia = new StringBuffer();
		conditions 		= new ArrayList();
		System.out.println("tipoCredito tipoCredito "+tipoCredito);
		if (this.tipoCredito == null || this.tipoCredito.equals("")){
		 if(numOrden.equals("")){
			querySentencia.append(
					"   SELECT /*+index(d cp_dis_documento_pk)*/"   +
					"   D.ic_documento"   +
					"    FROM DIS_DOCUMENTO D"   +
					"   ,DIS_DOCTO_SELECCIONADO DS"   +
					"   ,DIS_SOLICITUD S"   +
					"   ,COM_ACUSE3 C3"   +
					"   ,COMCAT_EPO E "   +
					"   ,DIS_LINEA_CREDITO_DM LCD"   +
					"   ,COMREL_PRODUCTO_EPO PE"   +
					"   ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"   WHERE D.IC_EPO = PE.IC_EPO"   +
					" 	 AND D.IC_EPO = E.IC_EPO " +
					"   AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"   AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM"   +
					"   AND D.IC_EPO = PE.IC_EPO"   +
					"   AND D.IC_EPO = IEP.IC_EPO"   +
					"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"   AND D.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"   AND D.IC_PRODUCTO_NAFIN = IEP.IC_PRODUCTO_NAFIN"   +
					"   AND LCD.IC_IF = IEP.IC_IF"   +
					"   AND S.CC_ACUSE = C3.CC_ACUSE"  +
					" AND LCD.IC_IF = ? "+
					" 	 AND E.cs_habilitado = ? ");
			conditions.add(new Integer(ses_ic_if));	
			conditions.add("S");
			querySentencia.append(" AND d.ic_estatus_docto in(?,?,?)  ");
			conditions.add(new Integer(4));
			conditions.add(new Integer(11));
			conditions.add(new Integer(32));
			
			if(!ic_epo.equals(""))	 {
				querySentencia.append("AND d.ic_epo = ? ");
				conditions.add(ic_epo);
			}
			if(!ic_pyme.equals(""))	{
				querySentencia.append(" AND d.ic_pyme = ? ") ;
				conditions.add(ic_pyme);
			}
			if (!fchinicial.equals("") && !fchfinal.equals("")){
				querySentencia.append(" AND TRUNC(c3.df_fecha_hora) BETWEEN TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
				conditions.add(fchinicial);
				conditions.add(fchfinal);
			}
			if(ic_epo.equals("") && ic_pyme.equals("") && fchinicial.equals("") && fchfinal.equals("")){
				querySentencia.append(" AND TO_CHAR(c3.df_fecha_hora,'DD/MM/YYYY')=TO_CHAR(SYSDATE,'DD/MM/YYYY') ");
			}
			
			querySentencia.append(" UNION ALL");
			querySentencia.append(
					"   SELECT /*+ index(d cp_dis_documento_pk) */"   +
					"   D.ic_documento"   +
					"   FROM DIS_DOCUMENTO D"   +
					"   ,DIS_DOCTO_SELECCIONADO DS"   +
					" 	,COM_BINS_IF cbi"+
					"  ,DIS_DOCTOS_PAGO_TC DDPTC"   +////////7
					"   ,DIS_SOLICITUD S"   +
					"   ,COM_ACUSE3 C3"   +
					"   , COMCAT_EPO E " +
					"   ,COM_LINEA_CREDITO LCD"   +
					"   ,COMREL_PRODUCTO_EPO PE"   +
					"   ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"   WHERE D.IC_EPO = PE.IC_EPO"   +
					"  AND d.IC_BINS = cbi.IC_BINS(+)"  +
					"  AND D.IC_ORDEN_PAGO_TC = DDPTC.IC_ORDEN_PAGO_TC(+)"   +
					" 	 AND D.IC_EPO = E.IC_EPO "   +
					"   AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"   AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
					"   AND D.IC_EPO = PE.IC_EPO"   +
					"   AND D.IC_EPO = IEP.IC_EPO"   +
					"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"   AND D.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"   AND D.IC_PRODUCTO_NAFIN = IEP.IC_PRODUCTO_NAFIN"   +
					"   AND (LCD.IC_IF = IEP.IC_IF OR cbi.IC_IF = IEP.IC_IF) "   +//////////////
					"   AND (LCD.IC_IF ="+ses_ic_if+" OR cbi.IC_IF = "+ses_ic_if+") "   +//////////////
					"   AND S.CC_ACUSE = C3.CC_ACUSE"   +
					//"	 AND LCD.IC_IF = ?"+
					" 	 AND E.cs_habilitado = ? "+
					"	 AND S.CS_FACTORAJE_CON_REC = ? ");
			//conditions.add(new Integer(ses_ic_if));
			conditions.add("S");
			conditions.add("N");
			querySentencia.append(" AND d.ic_estatus_docto in(?,?,?)  ");
			conditions.add(new Integer(4));
			conditions.add(new Integer(11));
			conditions.add(new Integer(32));
			
	
			if(!ic_epo.equals(""))	 {
				querySentencia.append("AND d.ic_epo = ? ");
				conditions.add(ic_epo);
			}
			if(!ic_pyme.equals(""))	{
				querySentencia.append(" AND d.ic_pyme = ? ") ;
				conditions.add(ic_pyme);
			}
			if (!fchinicial.equals("") && !fchfinal.equals("")){
				querySentencia.append(" AND TRUNC(c3.df_fecha_hora) BETWEEN TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
				conditions.add(fchinicial);
				conditions.add(fchfinal);
			}
			if(ic_epo.equals("") && ic_pyme.equals("") && fchinicial.equals("") && fchfinal.equals("")){
				querySentencia.append(" AND TO_CHAR(c3.df_fecha_hora,'DD/MM/YYYY')=TO_CHAR(SYSDATE,'DD/MM/YYYY') ");
			}
			
			if(!numOrden.equals("")){
				querySentencia.append(" and ddptc.ic_orden_pago_tc = ? ");
				conditions.add(numOrden);
			}
		 }else{
			querySentencia.append(
					"   SELECT /*+ index(d cp_dis_documento_pk) */"   +
					"   D.ic_documento"   +
					"   FROM DIS_DOCUMENTO D"   +
					"   ,DIS_DOCTO_SELECCIONADO DS"   +
					" 	,COM_BINS_IF cbi"+
					"  ,DIS_DOCTOS_PAGO_TC DDPTC"   +////////7
					"   ,DIS_SOLICITUD S"   +
					"   ,COM_ACUSE3 C3"   +
					"   , COMCAT_EPO E " +
					"   ,COM_LINEA_CREDITO LCD"   +
					"   ,COMREL_PRODUCTO_EPO PE"   +
					"   ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"   WHERE D.IC_EPO = PE.IC_EPO"   +
					"  AND d.IC_BINS = cbi.IC_BINS(+)"  +
					"  AND D.IC_ORDEN_PAGO_TC = DDPTC.IC_ORDEN_PAGO_TC(+)"   +
					" 	 AND D.IC_EPO = E.IC_EPO "   +
					"   AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"   AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
					"   AND D.IC_EPO = PE.IC_EPO"   +
					"   AND D.IC_EPO = IEP.IC_EPO"   +
					"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"   AND D.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"   AND D.IC_PRODUCTO_NAFIN = IEP.IC_PRODUCTO_NAFIN"   +
					"   AND (LCD.IC_IF = IEP.IC_IF OR cbi.IC_IF = IEP.IC_IF) "   +//////////////
					"   AND (LCD.IC_IF ="+ses_ic_if+" OR cbi.IC_IF = "+ses_ic_if+") "   +//////////////
					"   AND S.CC_ACUSE = C3.CC_ACUSE"   +
					//"	 AND LCD.IC_IF = ?"+
					" 	 AND E.cs_habilitado = ? "+
					"	 AND S.CS_FACTORAJE_CON_REC = ? ");
			//conditions.add(new Integer(ses_ic_if));
			conditions.add("S");
			conditions.add("N");
			querySentencia.append(" AND d.ic_estatus_docto in(?,?,?)  ");
			conditions.add(new Integer(4));
			conditions.add(new Integer(11));
			conditions.add(new Integer(32));
			
	
			if(!ic_epo.equals(""))	 {
				querySentencia.append("AND d.ic_epo = ? ");
				conditions.add(ic_epo);
			}
			if(!ic_pyme.equals(""))	{
				querySentencia.append(" AND d.ic_pyme = ? ") ;
				conditions.add(ic_pyme);
			}
			if (!fchinicial.equals("") && !fchfinal.equals("")){
				querySentencia.append(" AND TRUNC(c3.df_fecha_hora) BETWEEN TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
				conditions.add(fchinicial);
				conditions.add(fchfinal);
			}
			if(ic_epo.equals("") && ic_pyme.equals("") && fchinicial.equals("") && fchfinal.equals("")){
				querySentencia.append(" AND TO_CHAR(c3.df_fecha_hora,'DD/MM/YYYY')=TO_CHAR(SYSDATE,'DD/MM/YYYY') ");
			}
			
			if(!numOrden.equals("")){
				querySentencia.append(" and ddptc.ic_orden_pago_tc = ? ");
				conditions.add(numOrden);
			}
		 
		 }
		}else if (this.tipoCredito.equals("F")){
			querySentencia.append(
					"   SELECT /*+ index(d cp_dis_documento_pk) */"   +
					"   D.ic_documento"   +
					"   FROM DIS_DOCUMENTO D"   +
					"   ,DIS_DOCTO_SELECCIONADO DS"   +
					"   ,DIS_SOLICITUD S"   +
					"   ,COM_ACUSE3 C3"   +
					"   , COMCAT_EPO E " +
					"   ,COM_LINEA_CREDITO LCD"   +
					"   ,COMREL_PRODUCTO_EPO PE"   +
					"   ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"   WHERE D.IC_EPO = PE.IC_EPO"   +
					" 	 AND D.IC_EPO = E.IC_EPO "   +
					"   AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"   AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
					"   AND D.IC_EPO = PE.IC_EPO"   +
					"   AND D.IC_EPO = IEP.IC_EPO"   +
					"   AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"   AND D.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"   AND D.IC_PRODUCTO_NAFIN = IEP.IC_PRODUCTO_NAFIN"   +
					"   AND LCD.IC_IF = IEP.IC_IF"   +
					"   AND S.CC_ACUSE = C3.CC_ACUSE"   +
					"	 AND LCD.IC_IF = ?"+
					" 	 AND E.cs_habilitado = ? "+
					"	 AND S.CS_FACTORAJE_CON_REC = ? ");
			conditions.add(new Integer(ses_ic_if));
			conditions.add("S");
			conditions.add("S");
			querySentencia.append(" AND d.ic_estatus_docto in(?,?)  ");
			conditions.add(new Integer(4));
			conditions.add(new Integer(11));
	
			if(!ic_epo.equals(""))	 {
				querySentencia.append("AND d.ic_epo = ? ");
				conditions.add(ic_epo);
			}
			if(!ic_pyme.equals(""))	{
				querySentencia.append(" AND d.ic_pyme = ? ") ;
				conditions.add(ic_pyme);
			}
			if (!fchinicial.equals("") && !fchfinal.equals("")){
				querySentencia.append(" AND TRUNC(c3.df_fecha_hora) BETWEEN TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
				conditions.add(fchinicial);
				conditions.add(fchfinal);
			}
			if(ic_epo.equals("") && ic_pyme.equals("") && fchinicial.equals("") && fchfinal.equals("")){
				querySentencia.append(" AND TO_CHAR(c3.df_fecha_hora,'DD/MM/YYYY')=TO_CHAR(SYSDATE,'DD/MM/YYYY') ");
			}
		}
		


		log.debug("..:: qrySentencia: "+querySentencia.toString());
		log.debug("..:: conditions: "+conditions);
		log.info("getDocumentQuery(S)");
		return querySentencia.toString();
	}
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		querySentencia = new StringBuffer();
		conditions 		= new ArrayList();

		if (this.tipoCredito == null || this.tipoCredito.equals("")){
		 if(numOrden.equals("")){
			querySentencia.append(
					"  SELECT /*+ use_nl(D,DS,S,C3,LCD,P,PE,PN,M,TC,TCI,IEP,CT,E,ED,CI,TF)"   +
					"   index(d cp_dis_documento_pk)"   +
					"   index(m cp_comcat_moneda_pk)*/"   +
					"  P.cg_razon_social AS DISTRIBUIDOR"   +
					"  ,D.ig_numero_docto"   +
					"  ,D.cc_acuse"   +
					"  ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') AS df_fecha_emision"   +
					"  ,TO_CHAR(D.df_carga,'dd/mm/yyyy') AS df_fecha_publicacion"   +
					"  ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') AS df_fecha_venc"   +
					"  ,D.IG_PLAZO_DOCTO"   +
					"  ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') AS df_fecha_seleccion"   +
					"  ,M.cd_nombre AS MONEDA"   +
					"  ,M.ic_moneda"   +
					"  ,D.fn_monto"   +
					"  ,0 as COMISION_APLICABLE"   +/////////////////7
					"  ,'' as MONTO_DEPOSITAR"   +/////////////////7
					"  ,'' as MONTO_COMISION"   +/////////////////7
					"  ,'' as IC_ORDEN_ENVIADO"   +/////////////////7
					"  ,'' as NUMERO_TC"   +/////////////////7
					"  ,'' as IC_OPERACION_CCACUSE"   +/////////////////7
					"  ,'' AS codigo_autorizado"   +/////////////////7
					"  ,'' as FECHA_REGISTRO"   +////////////////cg_descripcion_bins
					"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') AS TIPO_CONVERSION"   +
					"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
					"  ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) AS MONTO_VALUADO"   +
					"  ,D.IG_PLAZO_DESCUENTO"   +
					"  ,D.FN_MONTO"   +
					"  ,D.fn_porc_descuento"   +
					"  ,D.fn_monto*(NVL(D.fn_porc_descuento,0)/100) AS MONTO_DESCUENTO"   +
					"  ,ED.CD_DESCRIPCION AS ESTATUS"   +
					"	,0 AS ic_estatus_docto"+
					"	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) AS REFERENCIA_INT" +
					"  ,DS.fn_valor_tasa  AS VALOR_TASA"   +
					"  ,D.IG_PLAZO_CREDITO"   +
					"  ,DS.fn_importe_recibir AS MONTO_CREDITO"   +
					"  ,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') AS DF_FECHA_VENC_CREDITO"   +
					"  ,TCI.ic_tipo_cobro_interes"   +
					"  ,TCI.cd_descripcion AS tipo_cobro_interes"   +
					"  ,DS.fn_importe_interes AS MONTO_INTERES"   +
					"  ,D.ic_documento"   +
					"  ,E.cg_razon_social AS EPO"   +
					"  , D.ic_documento"   +
					"  ,CI.cg_razon_social AS DIF"   +
					"  ,TF.cd_descripcion AS MODO_PLAZO"   +
					"  ,TO_CHAR(C3.df_fecha_hora,'dd/mm/yyyy') AS df_fecha_hora"   +
					"  ,LCD.ic_moneda AS moneda_linea"   +
					" , pn.cg_tipo_conversion as cg_tipo_conversion "+
					"	  , p.ic_pyme as ic_pyme  "+
					"   , e.ic_epo  as ic_epo "+
					"   , DS.IC_TASA as IC_TASA"+
					"  ,'' as cg_descripcion_bins"   +
					" , D.IG_TIPO_PAGO " +
					"   FROM DIS_DOCUMENTO D"   +
					"  ,DIS_DOCTO_SELECCIONADO DS"   +
					"  ,DIS_SOLICITUD S"   +
					"  ,COM_ACUSE3 C3"   +
					"  ,DIS_LINEA_CREDITO_DM LCD"   +
					"  ,COMCAT_PYME P"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"  ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"  ,COMCAT_TASA CT"   +
					"  ,COMCAT_EPO E"   +
					"  ,COMCAT_ESTATUS_DOCTO ED"   +
					"  ,COMCAT_IF CI"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  WHERE D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_PYME = P.IC_PYME"   +
					"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"  AND D.IC_LINEA_CREDITO_DM = LCD.IC_LINEA_CREDITO_DM"   +
					"  AND D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND D.IC_EPO = E.IC_EPO"   +
					"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
					"  AND D.IC_EPO = IEP.IC_EPO"   +
					"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"  AND DS.IC_TASA = CT.IC_TASA"   +
					"  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"  AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"  AND LCD.IC_IF = IEP.IC_IF"   +
					"  AND LCD.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES"   +
					"  AND S.CC_ACUSE = C3.CC_ACUSE"   +
					"  AND LCD.IC_IF = CI.IC_IF"   +
					"  AND M.IC_MONEDA = TC.IC_MONEDA"   +
					"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"  AND PN.IC_PRODUCTO_NAFIN = ? "   +
					" AND LCD.IC_IF = ? "+
					" AND E.cs_habilitado = ? ");
			conditions.add(new Integer(4));
			conditions.add(new Integer(ses_ic_if));
			conditions.add("S");
			querySentencia.append(" AND d.ic_estatus_docto in(?,?,?)  ");
			conditions.add(new Integer(4));
			conditions.add(new Integer(11));
			conditions.add(new Integer(32));
	
			if(!ic_epo.equals(""))	 {
				querySentencia.append("AND d.ic_epo = ? ");
				conditions.add(ic_epo);
			}
			if(!ic_pyme.equals(""))	{
				querySentencia.append(" AND d.ic_pyme = ? ") ;
				conditions.add(ic_pyme);
			}
			if (!fchinicial.equals("") && !fchfinal.equals("")){
				querySentencia.append(" AND TRUNC(c3.df_fecha_hora) BETWEEN TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
				conditions.add(fchinicial);
				conditions.add(fchfinal);
			}
			if(ic_epo.equals("") && ic_pyme.equals("") && fchinicial.equals("") && fchfinal.equals("")){
				querySentencia.append(" AND TO_CHAR(c3.df_fecha_hora,'DD/MM/YYYY')=TO_CHAR(SYSDATE,'DD/MM/YYYY') ");
			}
			if(!tipoConsulta.equals("")&&tipoConsulta.equals("Operada TC")){	 
				querySentencia.append("  AND ed.ic_estatus_docto = ?");
				conditions.add(new Integer(32));
			}
			querySentencia.append(" UNION ALL");
			querySentencia.append(
					"  SELECT /*+ use_nl(D,DS,S,C3,LCD,P,PE,PN,M,TC,TCI,IEP,CT,E,ED,CI,TF)"   +
					"   index(d cp_dis_documento_pk)"   +
					"   index(m cp_comcat_moneda_pk)*/"   +
					"  P.cg_razon_social AS DISTRIBUIDOR"   +
					"  ,D.ig_numero_docto"   +
					"  ,D.cc_acuse"   +
					"  ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') AS df_fecha_emision"   +
					"  ,TO_CHAR(D.df_carga,'dd/mm/yyyy') AS df_fecha_publicacion"   +
					"  ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') AS df_fecha_venc"   +
					"  ,D.IG_PLAZO_DOCTO"   +
					"  ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') AS df_fecha_seleccion"   +
					"  ,M.cd_nombre AS MONEDA"   +
					"  ,M.ic_moneda"   +
					"  ,D.fn_monto"   +
					"  ,DS.FN_PORC_COMISION_APLI as COMISION_APLICABLE"   +/////////////////7
					"  ,'' as MONTO_DEPOSITAR"   +
					"  ,'' as MONTO_COMISION"   +
					"  ,DDPTC.IC_ORDEN_PAGO_TC as IC_ORDEN_ENVIADO"   +
					"  ,DDPTC.CG_NUM_TARJETA as NUMERO_TC"   +
					"  ,DDPTC.CG_TRANSACCION_ID as IC_OPERACION_CCACUSE"   +
					"  ,decode(CONCAT(ddptc.CG_CODIGO_RESP ,ddptc.CG_CODIGO_DESC),'','',ddptc.CG_CODIGO_RESP||'-' ||ddptc.CG_CODIGO_DESC)AS codigo_autorizado"   +
					"  ,TO_CHAR(DDPTC.DF_FECHA_AUTORIZACION,'dd/mm/yyyy') as FECHA_REGISTRO"   +////////////////
					"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') AS TIPO_CONVERSION"   +
					"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
					"  ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) AS MONTO_VALUADO"   +
					"  ,D.IG_PLAZO_DESCUENTO"   +
					"  ,D.FN_MONTO"   +
					"  ,D.fn_porc_descuento"   +
					"  ,D.fn_monto*(NVL(D.fn_porc_descuento,0)/100) AS MONTO_DESCUENTO"   +
					"  ,ED.CD_DESCRIPCION AS ESTATUS"   +
					"	,ED.IC_ESTATUS_DOCTO AS IC_ESTATUS_DOCTO "+
					"  ,CT.CD_NOMBRE||' '||ds.cg_rel_mat||' '||ds.fn_puntos AS REFERENCIA_INT"   +
					"  ,DECODE(DS.CG_REL_MAT"   +
					"        ,'+',DS.fn_valor_tasa + DS.fn_puntos"   +
					"        ,'-',DS.fn_valor_tasa - DS.fn_puntos"   +
					"        ,'*',DS.fn_valor_tasa * DS.fn_puntos"   +
					"        ,'/',DS.fn_valor_tasa / DS.fn_puntos"   +
					"        ,0) AS VALOR_TASA"   +
					"  ,D.IG_PLAZO_CREDITO"   +
					"  ,DS.fn_importe_recibir AS MONTO_CREDITO"   +
					"  ,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') AS DF_FECHA_VENC_CREDITO"   +
					"  ,TCI.ic_tipo_cobro_interes"   +
					"  ,TCI.cd_descripcion AS tipo_cobro_interes"   +
					"  ,DS.fn_importe_interes AS MONTO_INTERES"   +
					"  ,D.ic_documento"   +
					"  ,E.cg_razon_social AS EPO"   +
					"  , D.ic_documento"   +
					"  ,decode(ed.ic_estatus_docto,32,ci2.cg_razon_social,ci.cg_razon_social) AS dif"   +
					"  ,TF.cd_descripcion AS MODO_PLAZO"   +
					"  ,TO_CHAR(C3.df_fecha_hora,'dd/mm/yyyy') AS df_fecha_hora"   +
					"  ,LCD.ic_moneda AS moneda_linea"   +
					" , pn.cg_tipo_conversion as cg_tipo_conversion "+
					"	  , p.ic_pyme as ic_pyme  "+
					"   , e.ic_epo  as ic_epo "+
					"   , DS.IC_TASA as IC_TASA,   cbi.CG_CODIGO_BIN||' ' ||cbi.cg_descripcion AS cg_descripcion_bins"+			
					" , D.IG_TIPO_PAGO " +
					"   FROM DIS_DOCUMENTO D"   +
					"  ,DIS_DOCTOS_PAGO_TC DDPTC"   +
					"  ,COM_BINS_IF cbi "+
					"	,comcat_if ci2 "+/////////////////agregados 
					"  ,DIS_DOCTO_SELECCIONADO DS"   +
					"  ,DIS_SOLICITUD S"   +
					"  ,COM_ACUSE3 C3"   +
					"  ,COM_LINEA_CREDITO LCD"   +
					"  ,COMCAT_PYME P"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"  ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"  ,COMCAT_TASA CT"   +
					"  ,COMCAT_EPO E"   +
					"  ,COMCAT_ESTATUS_DOCTO ED"   +
					"  ,COMCAT_IF CI"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  WHERE D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_ORDEN_PAGO_TC = DDPTC.IC_ORDEN_PAGO_TC(+)"   +
					"  AND D.IC_PYME = P.IC_PYME"   +
					"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"  AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
					"  AND D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND D.IC_EPO = E.IC_EPO"   +
					"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
					"  AND D.IC_EPO = IEP.IC_EPO"   +
					"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"  AND DS.IC_TASA = CT.IC_TASA"   +
					"  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"  AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"   AND (LCD.IC_IF = IEP.IC_IF OR cbi.IC_IF = IEP.IC_IF) "   +//////////////
					"   AND (LCD.IC_IF ="+ses_ic_if+" OR cbi.IC_IF = "+ses_ic_if+") "   +//////////////
					"  AND LCD.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES"   +
					"  AND S.CC_ACUSE = C3.CC_ACUSE"   +
					"  AND d.IC_BINS = cbi.IC_BINS(+)"  +////////////77
					"  AND cbi.IC_if = ci2.IC_if(+)"  +/////////////////7
					"  AND LCD.IC_IF = CI.IC_IF"   +
					"  AND M.IC_MONEDA = TC.IC_MONEDA"   +
					"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"  AND PN.IC_PRODUCTO_NAFIN = ? "  +
					//" AND LCD.IC_IF = ? "+
					" AND E.cs_habilitado = ? "+
					" AND S.CS_FACTORAJE_CON_REC = ? ");
			conditions.add(new Integer(4));
			//conditions.add(new Integer(ses_ic_if));
			conditions.add("S");
			conditions.add("N");
			querySentencia.append(" AND d.ic_estatus_docto in(?,?,?)  ");
			conditions.add(new Integer(4));
			conditions.add(new Integer(11));
			conditions.add(new Integer(32));
			if(!tipoConsulta.equals("")&&tipoConsulta.equals("Operada TC")){	 
				querySentencia.append("  AND ed.ic_estatus_docto = ?");
				conditions.add(new Integer(32));
			}
				
			if(!ic_epo.equals(""))	 {
				querySentencia.append("AND d.ic_epo = ? ");
				conditions.add(ic_epo);
			}
			if(!ic_pyme.equals(""))	{
				querySentencia.append(" AND d.ic_pyme = ? ") ;
				conditions.add(ic_pyme);
			}
			if (!fchinicial.equals("") && !fchfinal.equals("")){
				querySentencia.append(" AND TRUNC(c3.df_fecha_hora) BETWEEN TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
				conditions.add(fchinicial);
				conditions.add(fchfinal);
			}
			if(ic_epo.equals("") && ic_pyme.equals("") && fchinicial.equals("") && fchfinal.equals("")){
				querySentencia.append(" AND TO_CHAR(c3.df_fecha_hora,'DD/MM/YYYY')=TO_CHAR(SYSDATE,'DD/MM/YYYY') ");
			}
			if(!numOrden.equals("")){
				querySentencia.append(" and ddptc.ic_orden_pago_tc = ? ");
				conditions.add(numOrden);
			}
		 }else{
			querySentencia.append(
					"  SELECT /*+ use_nl(D,DS,S,C3,LCD,P,PE,PN,M,TC,TCI,IEP,CT,E,ED,CI,TF)"   +
					"   index(d cp_dis_documento_pk)"   +
					"   index(m cp_comcat_moneda_pk)*/"   +
					"  P.cg_razon_social AS DISTRIBUIDOR"   +
					"  ,D.ig_numero_docto"   +
					"  ,D.cc_acuse"   +
					"  ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') AS df_fecha_emision"   +
					"  ,TO_CHAR(D.df_carga,'dd/mm/yyyy') AS df_fecha_publicacion"   +
					"  ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') AS df_fecha_venc"   +
					"  ,D.IG_PLAZO_DOCTO"   +
					"  ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') AS df_fecha_seleccion"   +
					"  ,M.cd_nombre AS MONEDA"   +
					"  ,M.ic_moneda"   +
					"  ,D.fn_monto"   +
					"  ,DS.FN_PORC_COMISION_APLI as COMISION_APLICABLE"   +/////////////////7
					"  ,'' as MONTO_DEPOSITAR"   +
					"  ,'' as MONTO_COMISION"   +
					"  ,DDPTC.IC_ORDEN_PAGO_TC as IC_ORDEN_ENVIADO"   +
					"  ,DDPTC.CG_NUM_TARJETA as NUMERO_TC"   +
					"  ,DDPTC.CG_TRANSACCION_ID as IC_OPERACION_CCACUSE"   +
					"  ,decode(CONCAT(ddptc.CG_CODIGO_RESP ,ddptc.CG_CODIGO_DESC),'','',ddptc.CG_CODIGO_RESP||'-' ||ddptc.CG_CODIGO_DESC)AS codigo_autorizado"   +
					"  ,TO_CHAR(DDPTC.DF_FECHA_AUTORIZACION,'dd/mm/yyyy') as FECHA_REGISTRO"   +////////////////
					"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') AS TIPO_CONVERSION"   +
					"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
					"  ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) AS MONTO_VALUADO"   +
					"  ,D.IG_PLAZO_DESCUENTO"   +
					"  ,D.FN_MONTO"   +
					"  ,D.fn_porc_descuento"   +
					"  ,D.fn_monto*(NVL(D.fn_porc_descuento,0)/100) AS MONTO_DESCUENTO"   +
					"  ,ED.CD_DESCRIPCION AS ESTATUS"   +
					"	,ED.IC_ESTATUS_DOCTO AS IC_ESTATUS_DOCTO "+
					"  ,CT.CD_NOMBRE||' '||ds.cg_rel_mat||' '||ds.fn_puntos AS REFERENCIA_INT"   +
					"  ,DECODE(DS.CG_REL_MAT"   +
					"        ,'+',DS.fn_valor_tasa + DS.fn_puntos"   +
					"        ,'-',DS.fn_valor_tasa - DS.fn_puntos"   +
					"        ,'*',DS.fn_valor_tasa * DS.fn_puntos"   +
					"        ,'/',DS.fn_valor_tasa / DS.fn_puntos"   +
					"        ,0) AS VALOR_TASA"   +
					"  ,D.IG_PLAZO_CREDITO"   +
					"  ,DS.fn_importe_recibir AS MONTO_CREDITO"   +
					"  ,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') AS DF_FECHA_VENC_CREDITO"   +
					"  ,TCI.ic_tipo_cobro_interes"   +
					"  ,TCI.cd_descripcion AS tipo_cobro_interes"   +
					"  ,DS.fn_importe_interes AS MONTO_INTERES"   +
					"  ,D.ic_documento"   +
					"  ,E.cg_razon_social AS EPO"   +
					"  , D.ic_documento"   +
					"  ,decode(ed.ic_estatus_docto,32,ci2.cg_razon_social,ci.cg_razon_social) AS dif"   +
					"  ,TF.cd_descripcion AS MODO_PLAZO"   +
					"  ,TO_CHAR(C3.df_fecha_hora,'dd/mm/yyyy') AS df_fecha_hora"   +
					"  ,LCD.ic_moneda AS moneda_linea"   +
					" , pn.cg_tipo_conversion as cg_tipo_conversion "+
					"	  , p.ic_pyme as ic_pyme  "+
					"   , e.ic_epo  as ic_epo "+
					"   , DS.IC_TASA as IC_TASA,   cbi.CG_CODIGO_BIN||' ' ||cbi.cg_descripcion AS cg_descripcion_bins"+			
					" , D.IG_TIPO_PAGO " +
					"   FROM DIS_DOCUMENTO D"   +
					"  ,DIS_DOCTOS_PAGO_TC DDPTC"   +
					"  ,COM_BINS_IF cbi "+
					"	,comcat_if ci2 "+/////////////////agregados 
					"  ,DIS_DOCTO_SELECCIONADO DS"   +
					"  ,DIS_SOLICITUD S"   +
					"  ,COM_ACUSE3 C3"   +
					"  ,COM_LINEA_CREDITO LCD"   +
					"  ,COMCAT_PYME P"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"  ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"  ,COMCAT_TASA CT"   +
					"  ,COMCAT_EPO E"   +
					"  ,COMCAT_ESTATUS_DOCTO ED"   +
					"  ,COMCAT_IF CI"   +
					"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  WHERE D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_ORDEN_PAGO_TC = DDPTC.IC_ORDEN_PAGO_TC(+)"   +
					"  AND D.IC_PYME = P.IC_PYME"   +
					"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"  AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
					"  AND D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND D.IC_EPO = E.IC_EPO"   +
					"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
					"  AND D.IC_EPO = IEP.IC_EPO"   +
					"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"  AND DS.IC_TASA = CT.IC_TASA"   +
					"  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"  AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"   AND (LCD.IC_IF = IEP.IC_IF OR cbi.IC_IF = IEP.IC_IF) "   +//////////////
					"   AND (LCD.IC_IF ="+ses_ic_if+" OR cbi.IC_IF = "+ses_ic_if+") "   +//////////////
					"  AND LCD.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES"   +
					"  AND S.CC_ACUSE = C3.CC_ACUSE"   +
					"  AND d.IC_BINS = cbi.IC_BINS(+)"  +////////////77
					"  AND cbi.IC_if = ci2.IC_if(+)"  +/////////////////7
					"  AND LCD.IC_IF = CI.IC_IF"   +
					"  AND M.IC_MONEDA = TC.IC_MONEDA"   +
					"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"  AND PN.IC_PRODUCTO_NAFIN = ? "  +
					//" AND LCD.IC_IF = ? "+
					" AND E.cs_habilitado = ? "+
					" AND S.CS_FACTORAJE_CON_REC = ? ");
			conditions.add(new Integer(4));
		//	conditions.add(new Integer(ses_ic_if));
			conditions.add("S");
			conditions.add("N");
			querySentencia.append(" AND d.ic_estatus_docto in(?,?,?)  ");
			conditions.add(new Integer(4));
			conditions.add(new Integer(11));
			conditions.add(new Integer(32));
			if(!tipoConsulta.equals("")&&tipoConsulta.equals("Operada TC")){	 
				querySentencia.append("  AND ed.ic_estatus_docto = ?");
				conditions.add(new Integer(32));
			}
				
			if(!ic_epo.equals(""))	 {
				querySentencia.append("AND d.ic_epo = ? ");
				conditions.add(ic_epo);
			}
			if(!ic_pyme.equals(""))	{
				querySentencia.append(" AND d.ic_pyme = ? ") ;
				conditions.add(ic_pyme);
			}
			if (!fchinicial.equals("") && !fchfinal.equals("")){
				querySentencia.append(" AND TRUNC(c3.df_fecha_hora) BETWEEN TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
				conditions.add(fchinicial);
				conditions.add(fchfinal);
			}
			if(ic_epo.equals("") && ic_pyme.equals("") && fchinicial.equals("") && fchfinal.equals("")){
				querySentencia.append(" AND TO_CHAR(c3.df_fecha_hora,'DD/MM/YYYY')=TO_CHAR(SYSDATE,'DD/MM/YYYY') ");
			}
			if(!numOrden.equals("")){
				querySentencia.append(" and ddptc.ic_orden_pago_tc = ? ");
				conditions.add(numOrden);
			}
			 
		 }
		}else if (this.tipoCredito.equals("F")){
			querySentencia.append(
					"  SELECT /*+ use_nl(D,DS,S,C3,LCD,P,PE,PN,M,TC,TCI,IEP,CT,E,ED,CI,TF)"   +
					"   index(d cp_dis_documento_pk)"   +
					"   index(m cp_comcat_moneda_pk)*/"   +
					"  P.cg_razon_social AS DISTRIBUIDOR"   +
					"  ,D.ig_numero_docto"   +
					"  ,D.cc_acuse"   +
					"  ,TO_CHAR(D.df_fecha_emision,'dd/mm/yyyy') AS df_fecha_emision"   +
					"  ,TO_CHAR(D.df_carga,'dd/mm/yyyy') AS df_fecha_publicacion"   +
					"  ,TO_CHAR(D.df_fecha_venc,'dd/mm/yyyy') AS df_fecha_venc"   +
					"  ,D.IG_PLAZO_DOCTO"   +
					"  ,TO_CHAR(DS.df_fecha_seleccion,'dd/mm/yyyy') AS df_fecha_seleccion"   +
					"  ,M.cd_nombre AS MONEDA"   +
					"  ,M.ic_moneda"   +
					"  ,D.fn_monto"   +
					"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') AS TIPO_CONVERSION"   +
					"  ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') AS TIPO_CAMBIO"   +
					"  ,(D.fn_monto * DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1')) AS MONTO_VALUADO"   +
					"  ,D.IG_PLAZO_DESCUENTO"   +
					"  ,D.FN_MONTO"   +
					"  ,D.fn_porc_descuento"   +
					"  ,D.fn_monto*(NVL(D.fn_porc_descuento,0)/100) AS MONTO_DESCUENTO"   +
					"  ,ED.CD_DESCRIPCION AS ESTATUS"   +
					"  ,CT.CD_NOMBRE||' '||ds.cg_rel_mat||' '||ds.fn_puntos AS REFERENCIA_INT"   +
					"  ,DECODE(DS.CG_REL_MAT"   +
					"        ,'+',DS.fn_valor_tasa + DS.fn_puntos"   +
					"        ,'-',DS.fn_valor_tasa - DS.fn_puntos"   +
					"        ,'*',DS.fn_valor_tasa * DS.fn_puntos"   +
					"        ,'/',DS.fn_valor_tasa / DS.fn_puntos"   +
					"        ,0) AS VALOR_TASA"   +
					"  ,D.IG_PLAZO_CREDITO"   +
					"  ,DS.fn_importe_recibir AS MONTO_CREDITO"   +
					"  ,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'dd/mm/yyyy') AS DF_FECHA_VENC_CREDITO"   +
					"  ,TCI.ic_tipo_cobro_interes"   +
					"  ,TCI.cd_descripcion AS tipo_cobro_interes"   +
					"  ,DS.fn_importe_interes AS MONTO_INTERES"   +
					"  ,D.ic_documento"   +
					"  ,E.cg_razon_social AS EPO"   +
					"  , D.ic_documento"   +
					"  ,CI.cg_razon_social AS DIF"   +
					//"  ,TF.cd_descripcion AS MODO_PLAZO"   +
					"  ,TO_CHAR(C3.df_fecha_hora,'dd/mm/yyyy') AS df_fecha_hora"   +
					"  ,LCD.ic_moneda AS moneda_linea"   +
					" , pn.cg_tipo_conversion as cg_tipo_conversion "+
					"	  , p.ic_pyme as ic_pyme  "+
					"   , e.ic_epo  as ic_epo "+
					"   , DS.IC_TASA as IC_TASA"+			
					" , D.IG_TIPO_PAGO " +
					"   FROM DIS_DOCUMENTO D"   +
					"  ,DIS_DOCTO_SELECCIONADO DS"   +
					"  ,DIS_SOLICITUD S"   +
					"  ,COM_ACUSE3 C3"   +
					"  ,COM_LINEA_CREDITO LCD"   +
					"  ,COMCAT_PYME P"   +
					"  ,COMREL_PRODUCTO_EPO PE"   +
					"  ,COMCAT_PRODUCTO_NAFIN PN"   +
					"  ,COMCAT_MONEDA M"   +
					"  ,COM_TIPO_CAMBIO TC"   +
					"  ,COMCAT_TIPO_COBRO_INTERES TCI"   +
					"  ,COMREL_IF_EPO_X_PRODUCTO IEP"   +
					"  ,COMCAT_TASA CT"   +
					"  ,COMCAT_EPO E"   +
					"  ,COMCAT_ESTATUS_DOCTO ED"   +
					"  ,COMCAT_IF CI"   +
					//"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
					"  WHERE D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_PYME = P.IC_PYME"   +
					"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO"   +
					"  AND D.IC_LINEA_CREDITO = LCD.IC_LINEA_CREDITO"   +
					"  AND D.IC_EPO = PE.IC_EPO"   +
					"  AND D.IC_MONEDA = M.IC_MONEDA"   +
					//"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
					"  AND D.IC_EPO = E.IC_EPO"   +
					"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
					"  AND D.IC_EPO = IEP.IC_EPO"   +
					"  AND DS.IC_DOCUMENTO = S.IC_DOCUMENTO"   +
					"  AND DS.IC_TASA = CT.IC_TASA"   +
					"  AND PE.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"  AND IEP.IC_PRODUCTO_NAFIN = PN.IC_PRODUCTO_NAFIN"   +
					"  AND LCD.IC_IF = IEP.IC_IF"   +
					"  AND LCD.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES"   +
					"  AND S.CC_ACUSE = C3.CC_ACUSE"   +
					"  AND LCD.IC_IF = CI.IC_IF"   +
					"  AND M.IC_MONEDA = TC.IC_MONEDA"   +
					"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"  AND PN.IC_PRODUCTO_NAFIN = ? "  +
					" AND LCD.IC_IF = ? "+
					" AND E.cs_habilitado = ? "+
					" AND S.CS_FACTORAJE_CON_REC = ? ");
			conditions.add(new Integer(4));
			conditions.add(new Integer(ses_ic_if));
			conditions.add("S");
			conditions.add("S");
			querySentencia.append(" AND d.ic_estatus_docto in(?,?)  ");
			conditions.add(new Integer(4));
			conditions.add(new Integer(11));
	
			if(!ic_epo.equals(""))	 {
				querySentencia.append("AND d.ic_epo = ? ");
				conditions.add(ic_epo);
			}
			if(!ic_pyme.equals(""))	{
				querySentencia.append(" AND d.ic_pyme = ? ") ;
				conditions.add(ic_pyme);
			}
			if (!fchinicial.equals("") && !fchfinal.equals("")){
				querySentencia.append(" AND TRUNC(c3.df_fecha_hora) BETWEEN TO_DATE(?,'DD/MM/YYYY') and TO_DATE(?,'DD/MM/YYYY') ");
				conditions.add(fchinicial);
				conditions.add(fchfinal);
			}
			if(ic_epo.equals("") && ic_pyme.equals("") && fchinicial.equals("") && fchfinal.equals("")){
				querySentencia.append(" AND TO_CHAR(c3.df_fecha_hora,'DD/MM/YYYY')=TO_CHAR(SYSDATE,'DD/MM/YYYY') ");
			}
		}

		log.debug("..:: qrySentencia: "+querySentencia.toString());
		log.debug("..:: conditions: "+conditions);
		log.info("getDocumentQueryFile(S)");
		return querySentencia.toString();
	}

  	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet reg, String path, String tipo) {
	
		
		System.out.println("crearCustomFile()");   
		HttpSession session = request.getSession();	
		CreaArchivo creaArchivo = new CreaArchivo();
		
		ComunesPDF pdfDoc = new ComunesPDF();
		String comAplicable= "", montoDepositar = "", montoComision= "",num_Orden= "", ordenEnvio = "", opAcuse = "", codAutorizado = "", fechRegistro="" ;
	
		String auxValortaza = "N/A", auxMontoint = "N/A", auxReferencia = "N/A";
		double auxMonComision = 0;
		double auxMonDepositar = 0;
	
		String epo				= "";
		String distribuidor	= "";
		String numdocto		= "";
		String acuse			= "";
		String fchemision		= "";
		String fchpublica		= "";
		String fchvence		= "";
		String plazodocto		= "";
		String ic_moneda		= "";
		String moneda			= "";
		double monto			= 0;
		String tipoconversion= "";
		String tipocambio		= "";
		double mntovaluado	= 0;
		String plzodescto		= "";
		String porcdescto		= "";
		double mntodescuento	= 0;
		String estatus			= "";
		String numcredito		= "";
		String descif			= "";
		String referencia		= "";
		String bins 			= "";
		String plazocred		= "";
		String fchvenccred	= "";
		String tipocobroint	= "";
		String modoPlazo		= "";
		double montoint		= 0;
		String valortaza		= "";
		double mntocredito	= 0;
		String fchopera		= "";
		String monedaLinea	= "";
		int numreg = 0;
		CreaArchivo archivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer(2000);  //2000 es la capacidad inicial reservada
		String nombreArchivo = null;
		try{	
			if (tipo.equals("CSV")) {
		
		/*******************
		Cabecera del archivo
		********************/
				contenidoArchivo.append(
					"EPO,Distribuidor,Numero de documento inicial,Num. acuse carga,Fecha de emisi�n,Fecha de publicaci�n"+
					",Fecha vencimiento,Fecha de operaci�n,Plazo docto.,Moneda,Monto,Plazo para descuento en dias,% de descuento"+
					",Monto % de descuento,Tipo conv.,Tipo cambio,Monto valuado,Modalidad de plazo,Estatus,Numero de documento final"+
					",Monto,% Comisi�n Aplicable de Terceros, Monto Comisi�n de Terceros (IVA Incluido), Monto a Depositar por Operaci�n,Plazo,Fecha de vencimiento,Referencia tasa de inter�s,Valor tasa de inter�s,Monto tasa de inter�s,Tipo de cobro inter�s,Nombre del Producto,Id Orden enviado, Respuesta de Operaci�n, C�digo Autorizaci�n,N�mero Tarjeta de Cr�dito "	);
		/***************************
		 Generacion del archivo
		/***************************/
		while(reg.next()){
			ic_moneda		= (reg.getString("ic_moneda")==null)?"":reg.getString("ic_moneda");	
			distribuidor	= (reg.getString("DISTRIBUIDOR")==null)?"":reg.getString("DISTRIBUIDOR");
			numdocto			= (reg.getString("ig_numero_docto")==null)?"":reg.getString("ig_numero_docto");
			acuse				= (reg.getString("cc_acuse")==null)?"":reg.getString("cc_acuse");
			fchemision		= (reg.getString("df_fecha_emision")==null)?"":reg.getString("df_fecha_emision");
			fchpublica		= (reg.getString("df_fecha_publicacion")==null)?"":reg.getString("df_fecha_publicacion");		
			fchvence			= (reg.getString("df_fecha_venc")==null)?"":reg.getString("df_fecha_venc");
			plazodocto		= (reg.getString("IG_PLAZO_DOCTO")==null)?"":reg.getString("IG_PLAZO_DOCTO");
			moneda			= (reg.getString("MONEDA")==null)?"":reg.getString("MONEDA");
			monto				= Double.parseDouble(reg.getString("FN_MONTO"));
			tipoconversion	= (reg.getString("TIPO_CONVERSION")==null)?"":reg.getString("TIPO_CONVERSION");
			tipocambio		= (reg.getString("TIPO_CAMBIO")==null)?"":reg.getString("TIPO_CAMBIO");
			plzodescto		= (reg.getString("IG_PLAZO_DESCUENTO")==null)?"":reg.getString("IG_PLAZO_DESCUENTO");
			porcdescto		= (reg.getString("fn_porc_descuento")==null)?"":reg.getString("fn_porc_descuento");
			mntodescuento	= Double.parseDouble(reg.getString("MONTO_DESCUENTO"));
			mntovaluado		= (monto-mntodescuento)*Double.parseDouble(tipocambio);
			estatus			= (reg.getString("ESTATUS")==null)?"":reg.getString("ESTATUS");
			comAplicable = (reg.getString("COMISION_APLICABLE")==null)?"0":reg.getString("COMISION_APLICABLE");	
			montoDepositar = (reg.getString("MONTO_DEPOSITAR")==null)?"":reg.getString("MONTO_DEPOSITAR");	
			montoComision = (reg.getString("MONTO_COMISION")==null)?"":reg.getString("MONTO_COMISION");	
			ordenEnvio = (reg.getString("IC_ORDEN_ENVIADO")==null)?"":reg.getString("IC_ORDEN_ENVIADO");
			opAcuse = (reg.getString("IC_OPERACION_CCACUSE")==null)?"":reg.getString("IC_OPERACION_CCACUSE");
			codAutorizado = (reg.getString("CODIGO_AUTORIZADO")==null)?"":reg.getString("CODIGO_AUTORIZADO");
			num_Orden = (reg.getString("NUMERO_TC")==null)?"":"XXXX-XXXX-XXXX-"+reg.getString("NUMERO_TC");
			fechRegistro = (reg.getString("FECHA_REGISTRO")==null)?"":reg.getString("FECHA_REGISTRO");
			mntocredito		= Double.parseDouble(reg.getString("MONTO_CREDITO"));   
			auxMonComision =mntocredito*(Double.parseDouble(comAplicable))/100;
			auxMonDepositar = mntocredito-auxMonComision;
			if(!strPerfil.equals("IF FACT RECURSO")){
            modoPlazo		= (reg.getString("MODO_PLAZO")==null)?"":reg.getString("MODO_PLAZO");
         }
			monedaLinea		= (reg.getString("MONEDA_LINEA")==null)?"":reg.getString("MONEDA_LINEA");
			epo				= (reg.getString("EPO")==null)?"":reg.getString("EPO");
			/**********PARA CREDITO**************/
			numcredito		=(reg.getString("ic_documento")==null)?"":reg.getString("ic_documento");
			numcredito		=(reg.getString("ic_documento")==null)?"":reg.getString("ic_documento");
			descif			= (reg.getString("DIF")==null)?"":reg.getString("DIF");
			referencia		= (reg.getString("REFERENCIA_INT")==null)?"":reg.getString("REFERENCIA_INT");
			plazocred		= (reg.getString("IG_PLAZO_CREDITO")==null)?"":reg.getString("IG_PLAZO_CREDITO");
			fchvenccred		= (reg.getString("DF_FECHA_VENC_CREDITO")==null)?"":reg.getString("DF_FECHA_VENC_CREDITO");
			tipocobroint	= (reg.getString("tipo_cobro_interes")==null)?"":reg.getString("tipo_cobro_interes");
			montoint			= Double.parseDouble((reg.getString("MONTO_INTERES")==null)?"0":reg.getString("MONTO_INTERES"));
			valortaza		= (reg.getString("VALOR_TASA")==null)?"":reg.getString("VALOR_TASA");
			bins 				= (reg.getString("cg_descripcion_bins").equals(""))?"":reg.getString("cg_descripcion_bins");
			fchopera			= (reg.getString("df_fecha_hora")==null)?"":reg.getString("df_fecha_hora");
			numreg++;
			if(estatus.equals("Operada TC")){
				contenidoArchivo.append("\n" + epo.replace(',',' ')+","+distribuidor.replace(',', ' ') + "," + numdocto + "," + acuse + ","+ fchemision+","+fchpublica+","+fchvence+","+fchopera+","+ plazodocto + "," + moneda +
						", "+ Comunes.formatoDecimal(monto,2,false) + ", " + plzodescto + ","+ porcdescto + "," + Comunes.formatoDecimal(mntodescuento,2,false) + "," + ((ic_moneda.equals(monedaLinea))?"":tipoconversion) + 
						", " + ((ic_moneda.equals(monedaLinea))?"":tipocambio) + ", " + ((ic_moneda.equals(monedaLinea))?"":Comunes.formatoDecimal(mntovaluado,2,false))+ ","+modoPlazo +"," + estatus + 
						", "+ numcredito + ", "	+ Comunes.formatoDecimal(mntocredito,2,false) + "," + comAplicable + ","+ Comunes.formatoDecimal(auxMonComision,2,false)  + ",  "+ Comunes.formatoDecimal(auxMonDepositar,2,false)  + ",  " + auxReferencia.replace(',', ' ') + "," + auxReferencia.replace(',', ' ') + ","	+ auxReferencia.replace(',', ' ') + 
						"," + auxValortaza + "," + auxMontoint + "," + tipocobroint+","+bins+","+ordenEnvio+"'"+","+opAcuse+","+codAutorizado+","+num_Orden);
			}else{
				contenidoArchivo.append("\n" + epo.replace(',',' ')+","+distribuidor.replace(',', ' ') + "," + numdocto + "," + acuse + ","+ fchemision+","+fchpublica+","+fchvence+","+fchopera+","+ plazodocto + "," + moneda +
						", "+ Comunes.formatoDecimal(monto,2,false) + ", " + plzodescto + ","+ porcdescto + "," + Comunes.formatoDecimal(mntodescuento,2,false) + "," + ((ic_moneda.equals(monedaLinea))?"":tipoconversion) + 
						", " + ((ic_moneda.equals(monedaLinea))?"":tipocambio) + ", " + ((ic_moneda.equals(monedaLinea))?"":Comunes.formatoDecimal(mntovaluado,2,false))+ ","+modoPlazo +"," + estatus + 
						", "+ numcredito + ", "	+ Comunes.formatoDecimal(mntocredito,2,false) + "," + comAplicable + ","+ Comunes.formatoDecimal(auxMonComision,2,false)  + ",  "+ Comunes.formatoDecimal(auxMonDepositar,2,false)  + ",  " + plazocred + "," + fchvenccred + ","	+ referencia.replace(',', ' ') + 
						", "+ Comunes.formatoDecimal(valortaza,2,false) + ", "+ Comunes.formatoDecimal(montoint,2,false)  + "," + tipocobroint+","+bins+","+ordenEnvio+"'"+","+opAcuse+","+codAutorizado+","+num_Orden);
		
			}
		}
				if (numreg == 0)	{
					contenidoArchivo.append("\nNo se Encontr� Ning�n Registro");
				}
			}
			if(tipo.equals("CSV") ) {
				creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
				nombreArchivo = creaArchivo.getNombre();
			}
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		System.out.println("crearCustomFile(S)"+nombreArchivo);
		return nombreArchivo;
	}

	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();    
		ComunesPDF pdfDoc = new ComunesPDF();
		String comAplicable= "", montoDepositar = "", montoComision= "", ordenEnvio = "", opAcuse = "", codAutorizado = "", fechRegistro="" ;	
		double  auxMonComision = 0;
		double auxMonDepositar = 0;
		String epo				= "";
		String distribuidor		= "";
		String numdocto			= "";
		String acuse			= "";
		String fchemision		= "";
		String fchpublica		= "";
		String fchvence			= "";
		String plazodocto		= "";
		String ic_moneda		= "";
		String moneda			= "";
		double monto			= 0;
		String tipoconversion	= "";
		String tipocambio		= "";
		double mntovaluado		= 0;
		String plzodescto		= "";
		String porcdescto		= "";
		double mntodescuento	= 0;
		String estatus			= "";
		String numcredito		= "";
		String descif			= "";
		String referencia		= "";
		String plazocred		= "";
		String fchvenccred		= "";
		String tipocobroint		= "";
		String modoPlazo		= "";
		String bins 			= "";
		double montoint			= 0;
		String valortaza		= "";
		double mntocredito		= 0;
		String fchopera			= "";
		String monedaLinea		= "";
		String num_Orden = "";
		int    totalDoctosMN	= 0;
		double imontoTot		= 0;
		double imontoValTot		= 0;
		double imontodescTot	= 0;
		double imontocredTot	= 0;
		double imontotasaTot	= 0;

		int totalDoctosUSD		= 0;
		double imontoTotDls		= 0;
		double imontoValTotDls	= 0;
		double imontodescTotDls	= 0;
		double imontocredTotDls	= 0;
		double imontotasaTotDls	= 0;
		int numreg = 0;
		int numCols = 19;
		//try {
			if(tipo.equals("PDF") ) {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(), 
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),  
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
				
		while(reg.next()) {
			ic_moneda		= (reg.getString("ic_moneda")==null)?"":reg.getString("ic_moneda");	
			distribuidor	= (reg.getString("DISTRIBUIDOR")==null)?"":reg.getString("DISTRIBUIDOR");
			numdocto			= (reg.getString("ig_numero_docto")==null)?"":reg.getString("ig_numero_docto");
			acuse				= (reg.getString("cc_acuse")==null)?"":reg.getString("cc_acuse");
			fchemision		= (reg.getString("df_fecha_emision")==null)?"":reg.getString("df_fecha_emision");
			fchpublica		= (reg.getString("df_fecha_publicacion")==null)?"":reg.getString("df_fecha_publicacion");		
			fchvence			= (reg.getString("df_fecha_venc")==null)?"":reg.getString("df_fecha_venc");
			plazodocto		= (reg.getString("IG_PLAZO_DOCTO")==null)?"":reg.getString("IG_PLAZO_DOCTO");
			moneda			= (reg.getString("MONEDA")==null)?"":reg.getString("MONEDA");
			monto				= Double.parseDouble(reg.getString("FN_MONTO"));
			tipoconversion	= (reg.getString("TIPO_CONVERSION")==null)?"":reg.getString("TIPO_CONVERSION");
			tipocambio		= (reg.getString("TIPO_CAMBIO")==null)?"":reg.getString("TIPO_CAMBIO");
			plzodescto		= (reg.getString("IG_PLAZO_DESCUENTO")==null)?"":reg.getString("IG_PLAZO_DESCUENTO");
			porcdescto		= (reg.getString("fn_porc_descuento")==null)?"":reg.getString("fn_porc_descuento");
			mntodescuento	= Double.parseDouble(reg.getString("MONTO_DESCUENTO"));
			mntovaluado		= (monto-mntodescuento)*Double.parseDouble(tipocambio);
			estatus			= (reg.getString("ESTATUS")==null)?"":reg.getString("ESTATUS");
         if(!strPerfil.equals("IF FACT RECURSO")){
            modoPlazo		= (reg.getString("MODO_PLAZO")==null)?"":reg.getString("MODO_PLAZO");
         }
			monedaLinea		= (reg.getString("MONEDA_LINEA")==null)?"":reg.getString("MONEDA_LINEA");
			epo				= (reg.getString("EPO")==null)?"":reg.getString("EPO");
			/**********PARA CREDITO**************/
			numcredito		=(reg.getString("ic_documento")==null)?"":reg.getString("ic_documento");
			numcredito		=(reg.getString("ic_documento")==null)?"":reg.getString("ic_documento");
			descif			= (reg.getString("DIF")==null)?"":reg.getString("DIF");
			referencia		= (reg.getString("REFERENCIA_INT")==null)?"":reg.getString("REFERENCIA_INT");
			plazocred		= (reg.getString("IG_PLAZO_CREDITO")==null)?"":reg.getString("IG_PLAZO_CREDITO");
			fchvenccred		= (reg.getString("DF_FECHA_VENC_CREDITO")==null)?"":reg.getString("DF_FECHA_VENC_CREDITO");
			tipocobroint	= (reg.getString("tipo_cobro_interes")==null)?"":reg.getString("tipo_cobro_interes");
			montoint			= Double.parseDouble((reg.getString("MONTO_INTERES")==null)?"0":reg.getString("MONTO_INTERES"));
			valortaza		= (reg.getString("VALOR_TASA")==null)?"":reg.getString("VALOR_TASA");
			mntocredito		= Double.parseDouble(reg.getString("MONTO_CREDITO"));
			fchopera			= (reg.getString("df_fecha_hora")==null)?"":reg.getString("df_fecha_hora");//cg_descripcion_bins
			bins 				= (reg.getString("cg_descripcion_bins").equals(""))?"":reg.getString("cg_descripcion_bins");
			comAplicable = (reg.getString("COMISION_APLICABLE").equals(""))?"0":reg.getString("COMISION_APLICABLE");	
			montoDepositar = (reg.getString("MONTO_DEPOSITAR").equals(""))?"":reg.getString("MONTO_DEPOSITAR");	
			montoComision = (reg.getString("MONTO_COMISION").equals(""))?"":reg.getString("MONTO_COMISION");	
			ordenEnvio = (reg.getString("IC_ORDEN_ENVIADO").equals(""))?"":reg.getString("IC_ORDEN_ENVIADO");
			opAcuse = (reg.getString("IC_OPERACION_CCACUSE").equals(""))?"":reg.getString("IC_OPERACION_CCACUSE");
			codAutorizado = (reg.getString("CODIGO_AUTORIZADO").equals(""))?"":reg.getString("CODIGO_AUTORIZADO");
			num_Orden = (reg.getString("NUMERO_TC").equals(""))?"":"XXXX-XXXX-XXXX-"+reg.getString("NUMERO_TC");
			fechRegistro = (reg.getString("FECHA_REGISTRO").equals(""))?"":reg.getString("FECHA_REGISTRO");
			auxMonComision = mntocredito*(Double.parseDouble(comAplicable))/100;
			auxMonDepositar = mntocredito-auxMonComision;
			if(ic_moneda.equals("1")){
				totalDoctosMN++;
				imontoTot+= monto; 
				imontoValTot+= mntovaluado;
				imontodescTot+= mntodescuento;
				imontocredTot+= mntocredito; 
				imontotasaTot+= montoint; 		
			}
			if(ic_moneda.equals("54")){
				totalDoctosUSD++;
				imontoTotDls+= monto;
				if(ic_moneda.equals(monedaLinea))
					imontoValTotDls+= mntovaluado;
				imontodescTotDls+= mntodescuento;
				imontocredTotDls+= mntocredito; 
				imontotasaTotDls+= montoint;
			}
			if(numreg == 0){
				float anchos[] = new float[numCols];
				for(int x=0;x<numCols;x++) {
					if(x==0)
						anchos[x] = .35f;
					else
						anchos[x] = 1f;
						
					if(x==11||x==3)
						anchos[x] = 1.45f;
					if(x==5||x==6||x==7||x==8)
						anchos[x] = 1.15f;
				}
				pdfDoc.setTable(numCols,100,anchos);
				pdfDoc.setCell("Datos del Documento Inicial","celda01rep",ComunesPDF.CENTER,numCols);
				pdfDoc.setCell("A","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("EPO","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Dist.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("No. Docto. Inicial","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("No. Acuse Carga","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Emisi�n","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Pub.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Venc.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Oper.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo docto.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo Dscto D�as","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("% Dscto","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto % Dscto","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo conv.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo cambio","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto valuado","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Modo Plazo","celda01rep",ComunesPDF.CENTER);
				
				pdfDoc.setCell("Datos Documento Final","celda01rep",ComunesPDF.CENTER,numCols);
				pdfDoc.setCell("B","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("No. Docto Final","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto","celda01rep",ComunesPDF.CENTER);
				
				pdfDoc.setCell("% Comisi�n Aplicable de Terceros","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Comisi�n de Terceros (IVA Incluido)","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto a Depositar por Operaci�n","celda01rep",ComunesPDF.CENTER);//7
				
				pdfDoc.setCell("Plazo","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha Venc.","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Ref. Tasa Inter�s","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Valor Tasa Inter�s","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Inter�s","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo Cobro Inter�s","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("Nombre del Producto","celda01rep",ComunesPDF.CENTER);	
				pdfDoc.setCell("Id Orden enviado","celda01rep",ComunesPDF.CENTER);	
				pdfDoc.setCell("Respuesta de Operaci�n","celda01rep",ComunesPDF.CENTER);	
				pdfDoc.setCell("C�digo Autorizaci�n","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("N�mero Tarjeta de Cr�dito","celda01rep",ComunesPDF.CENTER);
				pdfDoc.setCell("","celda01rep",ComunesPDF.CENTER,numCols-18);
			}
		/*************************/ /* Contenido del archivo */ /**************************/
			pdfDoc.setCell("A","formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(epo,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(distribuidor,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(numdocto,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(acuse,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(fchemision,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(fchpublica,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(fchvence,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(fchopera,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(plazodocto,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(moneda,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(monto,2),"formasrep",ComunesPDF.RIGHT);
			pdfDoc.setCell(plzodescto,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(porcdescto+"%","formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(mntodescuento,2),"formasrep",ComunesPDF.RIGHT);
			pdfDoc.setCell((ic_moneda.equals(monedaLinea))?"":tipoconversion,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell((ic_moneda.equals(monedaLinea))?"":tipocambio,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell((ic_moneda.equals(monedaLinea))?"":"$ "+Comunes.formatoDecimal(mntovaluado,2),"formasrep",ComunesPDF.CENTER);
         if(!strPerfil.equals("IF FACT RECURSO")){
            pdfDoc.setCell(modoPlazo,"formasrep",ComunesPDF.CENTER);
         }  

			pdfDoc.setCell("B","formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(estatus,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(numcredito,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(mntocredito,2),"formasrep",ComunesPDF.RIGHT);
			
			pdfDoc.setCell(comAplicable+"%","formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(auxMonComision,2),"formasrep",ComunesPDF.RIGHT);
			pdfDoc.setCell("$"+Comunes.formatoDecimal(auxMonDepositar,2),"formasrep",ComunesPDF.RIGHT);
					
			
			if(estatus.equals("Operada TC")){
				pdfDoc.setCell("N/A","formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell("N/A","formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell("N/A","formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell("N/A","formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell("N/A","formasrep",ComunesPDF.CENTER);
			}else{
				pdfDoc.setCell(plazocred,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(fchvenccred,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(referencia,"formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell(Comunes.formatoDecimal(valortaza,2)+" %","formasrep",ComunesPDF.CENTER);
				pdfDoc.setCell("$"+Comunes.formatoDecimal(montoint,2),"formasrep",ComunesPDF.CENTER);	
			}
			pdfDoc.setCell(tipocobroint,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(bins,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(ordenEnvio,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(opAcuse,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(codAutorizado,"formasrep",ComunesPDF.CENTER);
			pdfDoc.setCell(num_Orden,"formasrep",ComunesPDF.CENTER);
			
			pdfDoc.setCell("","formasrep",ComunesPDF.CENTER,numCols-18);
			numreg++;
		} 
		if(tipo.equals("PDF") && numreg!=0) {
			pdfDoc.addTable();
			
		}
		pdfDoc.endDocument();	
	}
		return nombreArchivo;
}
	/**
	  Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }  
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}

/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public void setIc_if(String ses_ic_if) {
		this.ses_ic_if = ses_ic_if;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}

	public void setFechaInicial(String fchinicial) {
		this.fchinicial = fchinicial;
	}

	public void setFechaFinal(String fchfinal) {
		this.fchfinal = fchfinal;
	}

	public void setTipoCredito(String tipoCredito){
		this.tipoCredito = tipoCredito;
		
	}
	public void setTipoConsulta(String tipoConsulta){
		this.tipoConsulta = tipoConsulta;
	}
	public void setStrPerfil(String strPerfil){
		this.strPerfil = strPerfil;
	}
	
	public String getNumOrden() {
		return numOrden;
	}

	public void setNumOrden(String numOrden) {
		this.numOrden = numOrden;
	}

}  