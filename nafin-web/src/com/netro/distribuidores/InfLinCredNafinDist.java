package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class InfLinCredNafinDist  implements IQueryGenerator, IQueryGeneratorRegExtJS {
  
  /*******************************************************************
	* Variables utilizadas en los metodos de IQueryGeneratorRegExtJS	*
	*******************************************************************/
	private String tipo_creditoM;
	private String tipo_solicM;
	private String ic_epoM;
	private String ic_ifM;
	private String ic_pymeM;
	private String ic_estatus_lineaM;
	private String fecha_auto_deM;
	private String fecha_auto_aM;
	private static final Log log = ServiceLocator.getInstance().getLog(InfLinCredNafinDist.class);
	private String paginaOffset;
	private String paginaNo;
	private List conditions;
	

	public InfLinCredNafinDist(){}
	
	public String getAggregateCalculationQuery(HttpServletRequest request) {
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		StringBuffer qrySentencia	= new StringBuffer();
		StringBuffer qryDM			= new StringBuffer();
		StringBuffer qryCCC			= new StringBuffer();
		StringBuffer condicion		= new StringBuffer();
		
		String tipo_credito 	= (request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");	
		String tipoSolic 		= (request.getParameter("tipoSolic")==null)?"":request.getParameter("tipoSolic");	
		String ic_epo 			= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String ic_if 			= (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String ic_pyme 			= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String ic_estatus_linea	= (request.getParameter("ic_estatus_linea")==null)?"":request.getParameter("ic_estatus_linea");	
		String fecha_auto_de	= (request.getParameter("fecha_auto_de")==null)?fechaHoy:request.getParameter("fecha_auto_de");	
		String fecha_auto_a		= (request.getParameter("fecha_auto_a")==null)?fechaHoy:request.getParameter("fecha_auto_a");	

		try {
			
			if (!"".equals(tipoSolic))
				condicion.append(" AND lc.cg_tipo_solicitud = '"+tipoSolic+"'");
			if (!"".equals(ic_epo))
				condicion.append(" AND lc.ic_epo = "+ic_epo);
			if (!"".equals(ic_if))
				condicion.append(" AND lc.ic_if = "+ic_if);
			if (!"".equals(ic_pyme))
				condicion.append(" AND lc.ic_pyme = "+ic_pyme);
			if (!"".equals(ic_estatus_linea))
				condicion.append(" AND lc.ic_estatus_linea = "+ic_estatus_linea);
			if(!"".equals(fecha_auto_de)&& fecha_auto_de!=null&&!"".equals(fecha_auto_a)&& fecha_auto_a!=null)
				condicion.append(" and trunc(ac.df_acuse) between trunc(to_date('"+fecha_auto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_auto_a+"','dd/mm/yyyy'))");

			qryDM.append(
				" SELECT /*+use_nl(lc pe ac cif2 epo cif pn mon)*/"   +
				"        lc.ic_moneda, mon.cd_nombre, COUNT (1), "   +
				"        SUM (lc.fn_monto_autorizado), SUM (lc.fn_saldo_total)"   +
				"   FROM dis_linea_credito_dm lc,"   +
				"        comrel_producto_epo pe,"   +
				"        dis_acuse4 ac,"   +
				"        comcat_financiera cif2,"   +
				"        comcat_epo epo,"   +
				"        comcat_if cif,"   +
				"        comcat_producto_nafin pn"   +
				"        ,comcat_moneda mon"   +
				"  WHERE lc.ic_epo = pe.ic_epo"   +
				"    AND pe.ic_producto_nafin = pn.ic_producto_nafin"   +
				"    AND lc.cc_acuse = ac.cc_acuse"   +
				"    AND lc.ic_financiera = cif2.ic_financiera (+)"   +
				"    AND lc.ic_epo = epo.ic_epo"   +
				"    AND lc.ic_if = cif.ic_if"   +
				"    AND lc.ic_moneda = mon.ic_moneda"   +
				"    AND pn.ic_producto_nafin = 4"   +
				"    AND epo.cs_habilitado = 'S' "   +
				"    AND NVL (pe.cg_tipos_credito, pn.cg_tipos_credito) IN ('A',  'D')"+condicion.toString()+" "+
				"  GROUP BY lc.ic_moneda, mon.cd_nombre ORDER BY lc.ic_moneda");
				
			qryCCC.append(
				" SELECT /*+use_nl(lc lc2 p ac cif2 cif pn mon)*/"   +
				"        lc.ic_moneda, mon.cd_nombre, COUNT (1), "   +
				"        SUM (lc.fn_monto_autorizado), SUM (lc.fn_saldo_linea_total)"   +
				"   FROM com_linea_credito lc,"   +
				"        com_linea_credito lc2,"   +
				"        comcat_pyme p,"   +
				"        dis_acuse4 ac,"   +
				"        comcat_financiera cif2,"   +
				"        comcat_if cif,"   +
				"        comcat_producto_nafin pn"   +
				"        ,comcat_moneda mon"   +
				"  WHERE lc.ic_pyme = p.ic_pyme"   +
				"    AND lc.ic_producto_nafin = pn.ic_producto_nafin"   +
				"    AND lc.cc_acuse = ac.cc_acuse (+)"   +
				"    AND lc.ic_linea_credito_padre = lc2.ic_linea_credito (+)"   +
				"    AND lc.ic_financiera = cif2.ic_financiera (+)"   +
				"    AND lc.ic_if = cif.ic_if"   +
				"    AND lc.ic_moneda = mon.ic_moneda"   +
				"    AND pn.ic_producto_nafin = 4"+condicion.toString()+" "+
				"  GROUP BY lc.ic_moneda, mon.cd_nombre ORDER BY lc.ic_moneda");
			
			if("D".equals(tipo_credito))
				qrySentencia = qryDM;
			else
				if("C".equals(tipo_credito))
					qrySentencia = qryCCC;

		}catch(Exception e){
			System.out.println("InfLinCredNafinDist::getAggregateCalculationQuery "+e);
		}
		return qrySentencia.toString();
	}

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids) {
		int i=0;
		String tipo_credito 	= (request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");	
		StringBuffer qrySentencia	= new StringBuffer();
		StringBuffer qryDM			= new StringBuffer();
		StringBuffer qryCCC			= new StringBuffer();
		StringBuffer condicion		= new StringBuffer();
		
		if("D".equals(tipo_credito))
			condicion.append(" AND lc.ic_linea_credito_dm in (");
		else
			if("C".equals(tipo_credito))
				condicion.append(" AND lc.ic_linea_credito in (");

		i=0;
		for (Iterator it = ids.iterator(); it.hasNext();i++){
        	if(i>0){
				condicion.append(",");
			}
			condicion.append(" "+it.next().toString()+" ");
		}
		condicion.append(") ");

		qryDM.append(
			" SELECT /*+index(lc IN_DIS_LINEA_CREDITO_DM_01_NUK) use_nl(pe epo cif cn cn2 el mon)*/"   +
			"        lc.ic_linea_credito_dm,"   +
			"        'Descuento y/o Factoraje' AS tipocre,"   +
			"        DECODE (lc.cg_tipo_solicitud,'I', 'Inicial','A', 'Ampliaci�n','R', 'Renovacion') AS tipo_solic,"   +
			"        lc.ic_linea_credito_dm_padre, cn.ic_nafin_electronico,"   +
			"        epo.cg_razon_social AS epo, cn2.ic_nafin_electronico,"   +
			"        cif.cg_razon_social AS if, 'N/A' AS nodist, 'N/A' AS dist,"   +
			"        'N/A' AS fechasolic,"   +
			"        TO_CHAR (ac.df_acuse, 'DD/MM/YYYY') AS fechaauto,"   +
			"        mon.cd_nombre, el.cd_descripcion, 'N/A' AS causas,"   +
			"        lc.ig_plazo,"   +
			"        TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') AS fechavenc,"   +
			"        cif2.cd_nombre AS financiera, lc.cg_num_cuenta_epo,"   +
			"        lc.cg_num_cuenta_if, lc.fn_monto_autorizado,"   +
			"        lc.fn_saldo_total, mon.ic_moneda"   +
			"   FROM dis_linea_credito_dm lc,"   +
			"        comrel_nafin cn,"   +
			"        comrel_nafin cn2,     "   +
			"        comrel_producto_epo pe,"   +
			"        dis_acuse4 ac,"   +
			"        comcat_financiera cif2,"   +
			"        comcat_epo epo,"   +
			"        comcat_if cif,"   +
			"        comcat_moneda mon,"   +
			"        comcat_estatus_linea el,"   +
			"        comcat_producto_nafin pn"   +
			"  WHERE lc.ic_epo = cn.ic_epo_pyme_if"   +
			"    AND lc.ic_if = cn2.ic_epo_pyme_if"   +
			"    AND lc.ic_epo = pe.ic_epo"   +
			"    AND pe.ic_producto_nafin = pn.ic_producto_nafin"   +
			"    AND lc.cc_acuse = ac.cc_acuse"   +
			"    AND lc.ic_financiera = cif2.ic_financiera (+)"   +
			"    AND lc.ic_epo = epo.ic_epo"   +
			"    AND lc.ic_if = cif.ic_if"   +
			"    AND lc.ic_moneda = mon.ic_moneda"   +
			"    AND lc.ic_estatus_linea = el.ic_estatus_linea"   +
			"    AND cn.cg_tipo = 'E'"   +
			"    AND cn2.cg_tipo = 'I'"   +
			"    AND pn.ic_producto_nafin = 4"   +
			"    AND NVL (pe.cg_tipos_credito, pn.cg_tipos_credito) IN ('A',  'D') "+condicion.toString()+" ORDER BY 1");
				
		qryCCC.append(
			" SELECT   /*+ordered index(lc IN_COM_LINEA_CREDITO_02_NUK) use_nl(p cn cn2 ac cif2 cif mon el pn lc2)*/"   +
			"        lc.cg_tipo_solicitud ||"   +
			"        NVL (p.in_numero_sirac,'') ||lc.ic_linea_credito,"   +
			"        'Cr&eacute;dito en Cuenta Corriente' AS tipocre,"   +
			"        DECODE (lc.cg_tipo_solicitud,'I', 'Inicial','A', 'Ampliaci�n','R', 'Renovacion')AS tipo_solic,"   +
			"        NVL (lc2.cg_tipo_solicitud,'') ||DECODE (lc.ic_linea_credito_padre,NULL,'',NVL (p.in_numero_sirac,'')) ||NVL (lc.ic_linea_credito_padre,'')AS ic_linea_credito_padre,"   +
			"        'N/A', 'N/A' AS epo, cn2.ic_nafin_electronico,"   +
			"        cif.cg_razon_social AS if, cn.ic_nafin_electronico AS nodist,"   +
			"        p.cg_razon_social AS dist, 'N/A' AS fechasolic,"   +
			"        TO_CHAR (ac.df_acuse, 'DD/MM/YYYY') AS fechaauto,"   +
			"        mon.cd_nombre, el.cd_descripcion, 'N/A' AS causas, '',"   +
			"        TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') AS fechavenc,"   +
			"        cif2.cd_nombre AS financiera, lc.cg_numero_cuenta,"   +
			"        lc.cg_numero_cuenta_if, lc.fn_monto_autorizado,"   +
			"        lc.fn_saldo_linea_total, mon.ic_moneda"   +
			"   FROM com_linea_credito lc,"   +
			"        com_linea_credito lc2,"   +
			"        comcat_pyme p,"   +
			"        comrel_nafin cn,"   +
			"        comrel_nafin cn2,"   +
			"        dis_acuse4 ac,"   +
			"        comcat_financiera cif2,"   +
			"        comcat_if cif,"   +
			"        comcat_moneda mon,"   +
			"        comcat_estatus_linea el,"   +
			"        comcat_producto_nafin pn"   +
			"  WHERE lc.ic_pyme = p.ic_pyme"   +
			"    AND lc.ic_producto_nafin = pn.ic_producto_nafin"   +
			"    AND lc.ic_moneda = mon.ic_moneda"   +
			"    AND lc.cc_acuse = ac.cc_acuse (+)"   +
			"    AND lc.ic_estatus_linea = el.ic_estatus_linea"   +
			"    AND lc.ic_linea_credito_padre = lc2.ic_linea_credito (+)"   +
			"    AND lc.ic_financiera = cif2.ic_financiera (+)"   +
			"    AND lc.ic_pyme = cn.ic_epo_pyme_if"   +
			"    AND lc.ic_if = cn2.ic_epo_pyme_if"   +
			"    AND lc.ic_if = cif.ic_if"   +
			"    AND pn.ic_producto_nafin = 4"   +
			"    AND cn.cg_tipo = 'P'"   +
			"    AND cn2.cg_tipo = 'I' "+condicion.toString()+" ORDER BY 1");
			
			if("D".equals(tipo_credito))
				qrySentencia = qryDM;
			else
				if("C".equals(tipo_credito))
					qrySentencia = qryCCC;
					
		System.out.println("el query queda de la siguiente manera "+qrySentencia.toString());
		return qrySentencia.toString();
	}
	
	public String getDocumentQuery(HttpServletRequest request) {
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		StringBuffer qrySentencia	= new StringBuffer();
		StringBuffer qryDM			= new StringBuffer();
		StringBuffer qryCCC			= new StringBuffer();
		StringBuffer condicion		= new StringBuffer();
		
		String tipo_credito 	= (request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");	
		String tipoSolic 		= (request.getParameter("tipoSolic")==null)?"":request.getParameter("tipoSolic");	
		String ic_epo 			= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String ic_if 			= (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String ic_pyme 			= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String ic_estatus_linea	= (request.getParameter("ic_estatus_linea")==null)?"":request.getParameter("ic_estatus_linea");	
		String fecha_auto_de	= (request.getParameter("fecha_auto_de")==null)?fechaHoy:request.getParameter("fecha_auto_de");	
		String fecha_auto_a		= (request.getParameter("fecha_auto_a")==null)?fechaHoy:request.getParameter("fecha_auto_a");	

		try {
			
			if (!"".equals(tipoSolic))
				condicion.append(" AND lc.cg_tipo_solicitud = '"+tipoSolic+"'");
			if (!"".equals(ic_epo))
				condicion.append(" AND lc.ic_epo = "+ic_epo);
			if (!"".equals(ic_if))
				condicion.append(" AND lc.ic_if = "+ic_if);
			if (!"".equals(ic_pyme))
				condicion.append(" AND lc.ic_pyme = "+ic_pyme);
			if (!"".equals(ic_estatus_linea))
				condicion.append(" AND lc.ic_estatus_linea = "+ic_estatus_linea);
			if(!"".equals(fecha_auto_de)&& fecha_auto_de!=null&&!"".equals(fecha_auto_a)&& fecha_auto_a!=null)
				condicion.append(" and trunc(ac.df_acuse) between trunc(to_date('"+fecha_auto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_auto_a+"','dd/mm/yyyy'))");
				
			qryDM.append(
				" SELECT /*+use_nl(lc pe ac cif2 epo cif pn)*/"   +
				"        lc.ic_linea_credito_dm, 'InfLinCredNafinDist::getDocumentQuery'"   +
				"   FROM dis_linea_credito_dm lc,"   +
				"        comrel_producto_epo pe,"   +
				"        dis_acuse4 ac,"   +
				"        comcat_financiera cif2,"   +
				"        comcat_epo epo,"   +
				"        comcat_if cif,"   +
				"        comcat_producto_nafin pn"   +
				"  WHERE lc.ic_epo = pe.ic_epo"   +
				"    AND pe.ic_producto_nafin = pn.ic_producto_nafin"   +
				"    AND lc.cc_acuse = ac.cc_acuse"   +
				"    AND lc.ic_financiera = cif2.ic_financiera (+)"   +
				"    AND lc.ic_epo = epo.ic_epo"   +
				"    AND lc.ic_if = cif.ic_if"   +
				"    AND pn.ic_producto_nafin = 4"   +
				"    AND epo.cs_habilitado = 'S'"   +
				"    AND NVL (pe.cg_tipos_credito, pn.cg_tipos_credito) IN ('A',  'D')"+condicion.toString()+" ");
				
			qryCCC.append(
				" SELECT /*+use_nl(lc lc2 p ac cif2 cif pn)*/"   +
				"        lc.ic_linea_credito, 'InfLinCredNafinDist::getDocumentQuery'"   +
				"   FROM com_linea_credito lc,"   +
				"        com_linea_credito lc2,"   +
				"        comcat_pyme p,"   +
				"        dis_acuse4 ac,"   +
				"        comcat_financiera cif2,"   +
				"        comcat_if cif,"   +
				"        comcat_producto_nafin pn"   +
				"  WHERE lc.ic_pyme = p.ic_pyme"   +
				"    AND lc.ic_producto_nafin = pn.ic_producto_nafin"   +
				"    AND lc.cc_acuse = ac.cc_acuse (+)"   +
				"    AND lc.ic_linea_credito_padre = lc2.ic_linea_credito (+)"   +
				"    AND lc.ic_financiera = cif2.ic_financiera (+)"   +
				"    AND lc.ic_if = cif.ic_if"   +
				"    AND pn.ic_producto_nafin = 4"+condicion.toString()+" ");
			
			if("D".equals(tipo_credito))
				qrySentencia = qryDM;
			else
				if("C".equals(tipo_credito))
					qrySentencia = qryCCC;

			System.out.println("EL query de la llave primaria: "+qrySentencia.toString());
		}catch(Exception e){
			System.out.println("InfLinCredNafinDist::getDocumentQueryException "+e);
		}
		return qrySentencia.toString();
	}
	
	public String getDocumentQueryFile(HttpServletRequest request) {
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		StringBuffer qrySentencia	= new StringBuffer();
		StringBuffer qryDM			= new StringBuffer();
		StringBuffer qryCCC			= new StringBuffer();
		StringBuffer condicion		= new StringBuffer();

		String tipo_credito 	= (request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");	
		String tipoSolic 		= (request.getParameter("tipoSolic")==null)?"":request.getParameter("tipoSolic");	
		String ic_epo 			= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String ic_if 			= (request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String ic_pyme 			= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String ic_estatus_linea	= (request.getParameter("ic_estatus_linea")==null)?"":request.getParameter("ic_estatus_linea");	
		String fecha_auto_de	= (request.getParameter("fecha_auto_de")==null)?fechaHoy:request.getParameter("fecha_auto_de");	
		String fecha_auto_a		= (request.getParameter("fecha_auto_a")==null)?fechaHoy:request.getParameter("fecha_auto_a");	

		try {		

			if (!"".equals(tipoSolic))
				condicion.append(" AND lc.cg_tipo_solicitud = '"+tipoSolic+"'");
			if (!"".equals(ic_epo))
				condicion.append(" AND lc.ic_epo = "+ic_epo);
			if (!"".equals(ic_if))
				condicion.append(" AND lc.ic_if = "+ic_if);
			if (!"".equals(ic_pyme))
				condicion.append(" AND lc.ic_pyme = "+ic_pyme);
			if (!"".equals(ic_estatus_linea))
				condicion.append(" AND lc.ic_estatus_linea = "+ic_estatus_linea);
			if(!"".equals(fecha_auto_de)&& fecha_auto_de!=null&&!"".equals(fecha_auto_a)&& fecha_auto_a!=null)
				condicion.append(" and trunc(ac.df_acuse) between trunc(to_date('"+fecha_auto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_auto_a+"','dd/mm/yyyy'))");
			qryDM.append(
				" SELECT /*+index(lc IN_DIS_LINEA_CREDITO_DM_01_NUK) use_nl(pe epo cif cn cn2 el mon)*/"   +
				"        lc.ic_linea_credito_dm,"   +
				"        'Descuento y/o Factoraje' AS tipocre,"   +
				"        DECODE (lc.cg_tipo_solicitud,'I', 'Inicial','A', 'Ampliaci�n','R', 'Renovacion') AS tipo_solic,"   +
				"        lc.ic_linea_credito_dm_padre, cn.ic_nafin_electronico,"   +
				"        epo.cg_razon_social AS epo, cn2.ic_nafin_electronico,"   +
				"        cif.cg_razon_social AS if, 'N/A' AS nodist, 'N/A' AS dist,"   +
				"        'N/A' AS fechasolic,"   +
				"        TO_CHAR (ac.df_acuse, 'DD/MM/YYYY') AS fechaauto,"   +
				"        mon.cd_nombre, el.cd_descripcion, 'N/A' AS causas,"   +
				"        lc.ig_plazo,"   +
				"        TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') AS fechavenc,"   +
				"        cif2.cd_nombre AS financiera, lc.cg_num_cuenta_epo,"   +
				"        lc.cg_num_cuenta_if, lc.fn_monto_autorizado,"   +
				"        lc.fn_saldo_total, mon.ic_moneda"   +
				"   FROM dis_linea_credito_dm lc,"   +
				"        comrel_nafin cn,"   +
				"        comrel_nafin cn2,     "   +
				"        comrel_producto_epo pe,"   +
				"        dis_acuse4 ac,"   +
				"        comcat_financiera cif2,"   +
				"        comcat_epo epo,"   +
				"        comcat_if cif,"   +
				"        comcat_moneda mon,"   +
				"        comcat_estatus_linea el,"   +
				"        comcat_producto_nafin pn"   +
				"  WHERE lc.ic_epo = cn.ic_epo_pyme_if"   +
				"    AND lc.ic_if = cn2.ic_epo_pyme_if"   +
				"    AND lc.ic_epo = pe.ic_epo"   +
				"    AND pe.ic_producto_nafin = pn.ic_producto_nafin"   +
				"    AND lc.cc_acuse = ac.cc_acuse"   +
				"    AND lc.ic_financiera = cif2.ic_financiera (+)"   +
				"    AND lc.ic_epo = epo.ic_epo"   +
				"    AND lc.ic_if = cif.ic_if"   +
				"    AND lc.ic_moneda = mon.ic_moneda"   +
				"    AND lc.ic_estatus_linea = el.ic_estatus_linea"   +
				"    AND cn.cg_tipo = 'E'"   +
				"    AND cn2.cg_tipo = 'I'"   +
				"    AND pn.ic_producto_nafin = 4"   +
				"    AND epo.cs_habilitado = 'S' "   +
				"    AND NVL (pe.cg_tipos_credito, pn.cg_tipos_credito) IN ('A',  'D') "+condicion.toString()+" ORDER BY 1");
					
			qryCCC.append(
				" SELECT   /*+ordered index(lc IN_COM_LINEA_CREDITO_02_NUK) use_nl(p cn cn2 ac cif2 cif mon el pn lc2)*/"   +
				"        lc.cg_tipo_solicitud ||"   +
				"        NVL (p.in_numero_sirac,'') ||lc.ic_linea_credito,"   +
				"        'Cr&eacute;dito en Cuenta Corriente' AS tipocre,"   +
				"        DECODE (lc.cg_tipo_solicitud,'I', 'Inicial','A', 'Ampliaci�n','R', 'Renovacion')AS tipo_solic,"   +
				"        NVL (lc2.cg_tipo_solicitud,'') ||DECODE (lc.ic_linea_credito_padre,NULL,'',NVL (p.in_numero_sirac,'')) ||NVL (lc.ic_linea_credito_padre,'')AS ic_linea_credito_padre,"   +
				"        'N/A', 'N/A' AS epo, cn2.ic_nafin_electronico,"   +
				"        cif.cg_razon_social AS if, cn.ic_nafin_electronico AS nodist,"   +
				"        p.cg_razon_social AS dist, 'N/A' AS fechasolic,"   +
				"        TO_CHAR (ac.df_acuse, 'DD/MM/YYYY') AS fechaauto,"   +
				"        mon.cd_nombre, el.cd_descripcion, 'N/A' AS causas, '',"   +
				"        TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') AS fechavenc,"   +
				"        cif2.cd_nombre AS financiera, lc.cg_numero_cuenta,"   +
				"        lc.cg_numero_cuenta_if, lc.fn_monto_autorizado,"   +
				"        lc.fn_saldo_linea_total, mon.ic_moneda"   +
				"   FROM com_linea_credito lc,"   +
				"        com_linea_credito lc2,"   +
				"        comcat_pyme p,"   +
				"        comrel_nafin cn,"   +
				"        comrel_nafin cn2,"   +
				"        dis_acuse4 ac,"   +
				"        comcat_financiera cif2,"   +
				"        comcat_if cif,"   +
				"        comcat_moneda mon,"   +
				"        comcat_estatus_linea el,"   +
				"        comcat_producto_nafin pn"   +
				"  WHERE lc.ic_pyme = p.ic_pyme"   +
				"    AND lc.ic_producto_nafin = pn.ic_producto_nafin"   +
				"    AND lc.ic_moneda = mon.ic_moneda"   +
				"    AND lc.cc_acuse = ac.cc_acuse (+)"   +
				"    AND lc.ic_estatus_linea = el.ic_estatus_linea"   +
				"    AND lc.ic_linea_credito_padre = lc2.ic_linea_credito (+)"   +
				"    AND lc.ic_financiera = cif2.ic_financiera (+)"   +
				"    AND lc.ic_pyme = cn.ic_epo_pyme_if"   +
				"    AND lc.ic_if = cn2.ic_epo_pyme_if"   +
				"    AND lc.ic_if = cif.ic_if"   +
				"    AND pn.ic_producto_nafin = 4"   +
				"    AND cn.cg_tipo = 'P'"   +
				"    AND cn2.cg_tipo = 'I' "+condicion.toString()+" ORDER BY 1");

				if("D".equals(tipo_credito))
					qrySentencia = qryDM;
				else
					if("C".equals(tipo_credito))
						qrySentencia = qryCCC;
            
			System.out.println("getDocumentQueryFile qrySentencia: "+qrySentencia.toString());
		}catch(Exception e){
			System.out.println("InfLinCredNafinDist::getDocumentQueryFileException "+e);
		}
		return qrySentencia.toString();
	}
/*******************************************************************************
 *          Migraci�n. Implementa IQueryGeneratorRegExtJS.java                 *
 *******************************************************************************/
	public String getDocumentQuery(){

		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();

		log.info("getDocumentQuery(E)");

		try {
			
			if (!"".equals(tipo_solicM)){
				condicion.append(" AND lc.cg_tipo_solicitud = ?");
				conditions.add(tipo_solicM);
			}
			if (!"".equals(ic_epoM)){
				condicion.append(" AND lc.ic_epo = ?");
				conditions.add(ic_epoM);
			}
			if (!"".equals(ic_ifM)){
				condicion.append(" AND lc.ic_if = ?");
				conditions.add(ic_ifM);
			}
			if (!"".equals(ic_pymeM)){
				condicion.append(" AND lc.ic_pyme = ?");
				conditions.add(ic_pymeM);
			}
			if (!"".equals(ic_estatus_lineaM)){
				condicion.append(" AND lc.ic_estatus_linea = ?");
				conditions.add(ic_estatus_lineaM);
			}
			if(!"".equals(fecha_auto_deM)&& fecha_auto_deM!=null&&!"".equals(fecha_auto_aM)&& fecha_auto_aM!=null){
				condicion.append(" and trunc(ac.df_acuse) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");
				conditions.add(fecha_auto_deM);
				conditions.add(fecha_auto_aM);
			}

			if("D".equals(tipo_creditoM)){
				qrySentencia.append(
					" SELECT /*+use_nl(lc pe ac cif2 epo cif pn)*/"                             +
					"        lc.ic_linea_credito_dm, 'InfLinCredNafinDist::getDocumentQuery'"   +
					"   FROM dis_linea_credito_dm lc,"                                          +
					"        comrel_producto_epo pe,"                                           +
					"        dis_acuse4 ac,"                                                    +
					"        comcat_financiera cif2,"                                           +
					"        comcat_epo epo,"                                                   +
					"        comcat_if cif,"                                                    +
					"        comcat_producto_nafin pn"                                          +
					"  WHERE lc.ic_epo = pe.ic_epo"                                             +
					"    AND pe.ic_producto_nafin = pn.ic_producto_nafin"                       +
					"    AND lc.cc_acuse = ac.cc_acuse"                                         +
					"    AND lc.ic_financiera = cif2.ic_financiera (+)"                         +
					"    AND lc.ic_epo = epo.ic_epo"                                            +
					"    AND lc.ic_if = cif.ic_if"                                              +
					"    AND pn.ic_producto_nafin = 4"                                          +
					"    AND epo.cs_habilitado = 'S'"                                           +
					"    AND NVL (pe.cg_tipos_credito, pn.cg_tipos_credito) "                   +
					"        IN ('A',  'D')"+condicion.toString());
					
			} else if("C".equals(tipo_creditoM)){
				qrySentencia.append(
					" SELECT /*+use_nl(lc lc2 p ac cif2 cif pn)*/"                         +
					"        lc.ic_linea_credito, 'InfLinCredNafinDist::getDocumentQuery'" +
					"   FROM com_linea_credito lc,"                                        +
					"        com_linea_credito lc2,"                                       +
					"        comcat_pyme p,"                                               +
					"        dis_acuse4 ac,"                                               +
					"        comcat_financiera cif2,"                                      +
					"        comcat_if cif,"                                               +
					"        comcat_producto_nafin pn"                                     +
					"  WHERE lc.ic_pyme = p.ic_pyme"                                       +
					"    AND lc.ic_producto_nafin = pn.ic_producto_nafin"                  +
					"    AND lc.cc_acuse = ac.cc_acuse (+)"                                +
					"    AND lc.ic_linea_credito_padre = lc2.ic_linea_credito (+)"         +
					"    AND lc.ic_financiera = cif2.ic_financiera (+)"                    +
					"    AND lc.ic_if = cif.ic_if"                                         +
					"    AND pn.ic_producto_nafin = 4"+condicion.toString()+" ");
					
			}
			log.debug(qrySentencia.toString());
		}catch(Exception e){
			log.warn("InfLinCredNafinDist::getDocumentQueryException "+e);
		}
		log.info("getDocumentQuery(S");
		return qrySentencia.toString();
	}

	public String getDocumentSummaryQueryForIds(List ids){

		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();

		log.info("getDocumentSummaryQueryForIds(E)");

		try{
			if("D".equals(tipo_creditoM))
				condicion.append(" AND lc.ic_linea_credito_dm in (");
			else if("C".equals(tipo_creditoM))
				condicion.append(" AND lc.ic_linea_credito in (");

			for (int i = 0; i < ids.size(); i++) { 
				List lItem = (ArrayList)ids.get(i);
				if(i > 0){
					condicion.append(",");
				}
				condicion.append(" ? " );
				conditions.add(new Long(lItem.get(0).toString()));
			}
			condicion.append(" ) ");

			if("D".equals(tipo_creditoM)){
				qrySentencia.append(
					" SELECT /*+index(lc IN_DIS_LINEA_CREDITO_DM_01_NUK) use_nl(pe epo cif cn cn2 el mon)*/"                       +
					"        lc.ic_linea_credito_dm AS FOLIO_SOLICITUD,"                                                           +
					"        'Descuento y/o Factoraje' AS TIPO_CREDITO,"                                                           +
					"        DECODE (lc.cg_tipo_solicitud,'I', 'Inicial','A', 'Ampliaci�n','R', 'Renovacion') AS TIPO_SOLICITUD,"  +
					"        lc.ic_linea_credito_dm_padre AS FOLIO_SULICITUD_RELACIONADA, cn.ic_nafin_electronico AS NUM_EPO,"     +
					"        epo.cg_razon_social AS EPO, cn2.ic_nafin_electronico AS NUM_IF,"                                      +
					"        cif.cg_razon_social AS IF, 'N/A' AS NUM_DISTRIBUIDOR, 'N/A' AS DISTRIBUIDOR,"                         +
					"        'N/A' AS FECHA_SOLICITUD,"                                                                            +
					"        TO_CHAR (ac.df_acuse, 'DD/MM/YYYY') AS FECHA_AUTORIZACION,"                                           +
					"        mon.cd_nombre AS MONEDA, el.cd_descripcion AS ESTATUS, 'N/A' AS CAUSA_RECHAZO,"                       +
					"        lc.ig_plazo AS PLAZO,"                                                                                +
					"        TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') AS FECHA_VENCIMIENTO,"                            +
					"        cif2.cd_nombre AS FINANCIERA, lc.cg_num_cuenta_epo AS EPO_CLIENTE_NUM_CUENTA,"                        +
					"        lc.cg_num_cuenta_if AS IF_NUM_CUENTA, lc.fn_monto_autorizado AS MONTO_AUTORIZADO,"                    +
					"        lc.fn_saldo_total AS SALDO_DISPONIBLE, mon.ic_moneda"                                                 +
					"   FROM dis_linea_credito_dm lc,"                                                                             +
					"        comrel_nafin cn,"                                                                                     +
					"        comrel_nafin cn2,"                                                                                    +
					"        comrel_producto_epo pe,"                                                                              +
					"        dis_acuse4 ac,"                                                                                       +
					"        comcat_financiera cif2,"                                                                              +
					"        comcat_epo epo,"                                                                                      +
					"        comcat_if cif,"                                                                                       +
					"        comcat_moneda mon,"                                                                                   +
					"        comcat_estatus_linea el,"                                                                             +
					"        comcat_producto_nafin pn"                                                                             +
					"  WHERE lc.ic_epo = cn.ic_epo_pyme_if"                                                                        +
					"    AND lc.ic_if = cn2.ic_epo_pyme_if"                                                                        +
					"    AND lc.ic_epo = pe.ic_epo"                                                                                +
					"    AND pe.ic_producto_nafin = pn.ic_producto_nafin"                                                          +
					"    AND lc.cc_acuse = ac.cc_acuse"                                                                            +
					"    AND lc.ic_financiera = cif2.ic_financiera (+)"                                                            +
					"    AND lc.ic_epo = epo.ic_epo"                                                                               +
					"    AND lc.ic_if = cif.ic_if"                                                                                 +
					"    AND lc.ic_moneda = mon.ic_moneda"                                                                         +
					"    AND lc.ic_estatus_linea = el.ic_estatus_linea"                                                            +
					"    AND cn.cg_tipo = 'E'"                                                                                     +
					"    AND cn2.cg_tipo = 'I'"                                                                                    +
					"    AND pn.ic_producto_nafin = 4"                                                                             +
					"    AND NVL (pe.cg_tipos_credito, pn.cg_tipos_credito) IN ('A',  'D') "+condicion.toString()+" ORDER BY 1");

			} else if("C".equals(tipo_creditoM)){
				qrySentencia.append(
					" SELECT   /*+ordered index(lc IN_COM_LINEA_CREDITO_02_NUK) use_nl(p cn cn2 ac cif2 cif mon el pn lc2)*/"        +
					"        lc.cg_tipo_solicitud ||"                                                                                +
					"        NVL (p.in_numero_sirac,'') ||lc.ic_linea_credito AS FOLIO_SOLICITUD,"                                   +
					"        'Cr�dito en Cuenta Corriente' AS TIPO_CREDITO,"                                                         +
					"        DECODE (lc.cg_tipo_solicitud,'I', 'Inicial','A', 'Ampliaci�n','R', 'Renovacion') AS TIPO_SOLICITUD,"    +
					"        NVL (lc2.cg_tipo_solicitud,'') ||DECODE (lc.ic_linea_credito_padre,NULL,'',NVL (p.in_numero_sirac,''))" +
					"        ||NVL (lc.ic_linea_credito_padre,'') AS FOLIO_SULICITUD_RELACIONADA,"                                   +
					"        'N/A' AS NUM_EPO, 'N/A' AS EPO, cn2.ic_nafin_electronico AS NUM_IF,"                                    +
					"        cif.cg_razon_social AS IF, cn.ic_nafin_electronico AS NUM_DISTRIBUIDOR,"                                +
					"        p.cg_razon_social AS DISTRIBUIDOR, 'N/A' AS FECHA_SOLICITUD,"                                           +
					"        TO_CHAR (ac.df_acuse, 'DD/MM/YYYY') AS FECHA_AUTORIZACION,"                                             +
					"        mon.cd_nombre AS MONEDA, el.cd_descripcion AS ESTATUS, 'N/A' AS CAUSA_RECHAZO, '' AS PLAZO,"            +
					"        TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') AS FECHA_VENCIMIENTO,"                              +
					"        cif2.cd_nombre AS FINANCIERA, lc.cg_numero_cuenta AS EPO_CLIENTE_NUM_CUENTA,"                           +
					"        lc.cg_numero_cuenta_if AS IF_NUM_CUENTA, lc.fn_monto_autorizado AS MONTO_AUTORIZADO,"                   +
					"        lc.fn_saldo_linea_total AS SALDO_DISPONIBLE, mon.ic_moneda"                                             +
					"   FROM com_linea_credito lc,"                                                                                  +
					"        com_linea_credito lc2,"                                                                                 +
					"        comcat_pyme p,"                                                                                         +
					"        comrel_nafin cn,"                                                                                       +
					"        comrel_nafin cn2,"                                                                                      +
					"        dis_acuse4 ac,"                                                                                         +
					"        comcat_financiera cif2,"                                                                                +
					"        comcat_if cif,"                                                                                         +
					"        comcat_moneda mon,"                                                                                     +
					"        comcat_estatus_linea el,"                                                                               +
					"        comcat_producto_nafin pn"                                                                               +
					"  WHERE lc.ic_pyme = p.ic_pyme"                                                                                 +
					"    AND lc.ic_producto_nafin = pn.ic_producto_nafin"                                                            +
					"    AND lc.ic_moneda = mon.ic_moneda"                                                                           +
					"    AND lc.cc_acuse = ac.cc_acuse (+)"                                                                          +
					"    AND lc.ic_estatus_linea = el.ic_estatus_linea"                                                              +
					"    AND lc.ic_linea_credito_padre = lc2.ic_linea_credito (+)"                                                   +
					"    AND lc.ic_financiera = cif2.ic_financiera (+)"                                                              +
					"    AND lc.ic_pyme = cn.ic_epo_pyme_if"                                                                         +
					"    AND lc.ic_if = cn2.ic_epo_pyme_if"                                                                          +
					"    AND lc.ic_if = cif.ic_if"                                                                                   +
					"    AND pn.ic_producto_nafin = 4"                                                                               +
					"    AND cn.cg_tipo = 'P'"                                                                                       +
					"    AND cn2.cg_tipo = 'I' "+condicion.toString()+" ORDER BY 1");

			}
			log.debug(qrySentencia.toString());
		} catch(Exception e){
			log.warn("InfLinCredNafinDist::getDocumentSummaryQueryForIds "+e);
		}
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();
	}

	public String getAggregateCalculationQuery(){

		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();
		log.info("getAggregateCalculationQuery(E)");

		try {
			
			if (!"".equals(tipo_solicM)){
				condicion.append(" AND lc.cg_tipo_solicitud = ?");
				conditions.add(tipo_solicM);
			}
			if (!"".equals(ic_epoM)){
				condicion.append(" AND lc.ic_epo = ?");
				conditions.add(ic_epoM);
			}
			if (!"".equals(ic_ifM)){
				condicion.append(" AND lc.ic_if = ?");
				conditions.add(ic_ifM);
			}
			if (!"".equals(ic_pymeM)){
				condicion.append(" AND lc.ic_pyme = ?");
				conditions.add(ic_pymeM);
			}
			if (!"".equals(ic_estatus_lineaM)){
				condicion.append(" AND lc.ic_estatus_linea = ?");
				conditions.add(ic_estatus_lineaM);
			}
			if(!"".equals(fecha_auto_deM)&& fecha_auto_deM!=null&&!"".equals(fecha_auto_aM)&& fecha_auto_aM!=null){
				condicion.append(" and trunc(ac.df_acuse) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");
				conditions.add(fecha_auto_deM);
				conditions.add(fecha_auto_aM);
			}

			if("D".equals(tipo_creditoM)){
				qrySentencia.append(
					" SELECT /*+use_nl(lc pe ac cif2 epo cif pn mon)*/"                                             +
					"        lc.ic_moneda, mon.cd_nombre, COUNT (1) AS TOTAL, "                                     +
					"        SUM (lc.fn_monto_autorizado) AS MONTO_TOTAL, SUM (lc.fn_saldo_total) AS SALDO_TOTAL"   +
					"   FROM dis_linea_credito_dm lc,"                                                              +
					"        comrel_producto_epo pe,"                                                               +
					"        dis_acuse4 ac,"                                                                        +
					"        comcat_financiera cif2,"                                                               +
					"        comcat_epo epo,"                                                                       +
					"        comcat_if cif,"                                                                        +
					"        comcat_producto_nafin pn,"                                                             +
					"        comcat_moneda mon"                                                                     +
					"  WHERE lc.ic_epo = pe.ic_epo"                                                                 +
					"    AND pe.ic_producto_nafin = pn.ic_producto_nafin"                                           +
					"    AND lc.cc_acuse = ac.cc_acuse"                                                             +
					"    AND lc.ic_financiera = cif2.ic_financiera (+)"                                             +
					"    AND lc.ic_epo = epo.ic_epo"                                                                +
					"    AND lc.ic_if = cif.ic_if"                                                                  +
					"    AND lc.ic_moneda = mon.ic_moneda"                                                          +
					"    AND pn.ic_producto_nafin = 4"                                                              +
					"    AND epo.cs_habilitado = 'S' "                                                              +
					"    AND NVL (pe.cg_tipos_credito, pn.cg_tipos_credito) IN ('A',  'D')"+condicion.toString()+" "+
					"  GROUP BY lc.ic_moneda, mon.cd_nombre ORDER BY lc.ic_moneda");
			} else if("C".equals(tipo_creditoM)){
				qrySentencia.append(
					" SELECT /*+use_nl(lc lc2 p ac cif2 cif pn mon)*/"                                                  +
					"        lc.ic_moneda, mon.cd_nombre, COUNT (1) AS TOTAL, "                                         +
					"        SUM (lc.fn_monto_autorizado) AS MONTO_TOTAL, SUM (lc.fn_saldo_linea_total) AS SALDO_TOTAL" +
					"   FROM com_linea_credito lc,"                                                                     +
					"        com_linea_credito lc2,"                                                                    +
					"        comcat_pyme p,"                                                                            +
					"        dis_acuse4 ac,"                                                                            +
					"        comcat_financiera cif2,"                                                                   +
					"        comcat_if cif,"                                                                            +
					"        comcat_producto_nafin pn,"                                                                 +
					"        comcat_moneda mon"                                                                         +
					"  WHERE lc.ic_pyme = p.ic_pyme"                                                                    +
					"    AND lc.ic_producto_nafin = pn.ic_producto_nafin"                                               +
					"    AND lc.cc_acuse = ac.cc_acuse (+)"                                                             +
					"    AND lc.ic_linea_credito_padre = lc2.ic_linea_credito (+)"                                      +
					"    AND lc.ic_financiera = cif2.ic_financiera (+)"                                                 +
					"    AND lc.ic_if = cif.ic_if"                                                                      +
					"    AND lc.ic_moneda = mon.ic_moneda"                                                              +
					"    AND pn.ic_producto_nafin = 4"+condicion.toString()+" "                                         +
					"  GROUP BY lc.ic_moneda, mon.cd_nombre ORDER BY lc.ic_moneda");
			}
			log.debug(qrySentencia.toString());
		}catch(Exception e){
			log.warn("InfLinCredNafinDist::getAggregateCalculationQuery "+e);
		}
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();
	}

	public String getDocumentQueryFile(){

		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();
		log.info("getDocumentQueryFile(E)");

		try {
			if (!"".equals(tipo_solicM)){
				condicion.append(" AND lc.cg_tipo_solicitud = ?");
				conditions.add(tipo_solicM);
			}
			if (!"".equals(ic_epoM)){
				condicion.append(" AND lc.ic_epo = ?");
				conditions.add(ic_epoM);
			}
			if (!"".equals(ic_ifM)){
				condicion.append(" AND lc.ic_if = ?");
				conditions.add(ic_ifM);
			}
			if (!"".equals(ic_pymeM)){
				condicion.append(" AND lc.ic_pyme = ?");
				conditions.add(ic_pymeM);
			}
			if (!"".equals(ic_estatus_lineaM)){
				condicion.append(" AND lc.ic_estatus_linea = ?");
				conditions.add(ic_estatus_lineaM);
			}
			if(!"".equals(fecha_auto_deM)&& fecha_auto_deM!=null&&!"".equals(fecha_auto_aM)&& fecha_auto_aM!=null){
				condicion.append(" and trunc(ac.df_acuse) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");
				conditions.add(fecha_auto_deM);
				conditions.add(fecha_auto_aM);
			}

			if("D".equals(tipo_creditoM)){
				qrySentencia.append(
				" SELECT /*+index(lc IN_DIS_LINEA_CREDITO_DM_01_NUK) use_nl(pe epo cif cn cn2 el mon)*/"                      +
				"        lc.ic_linea_credito_dm AS FOLIO_SOLICITUD,"                                                          +
				"        'Descuento y/o Factoraje' AS TIPO_CREDITO,"                                                          +
				"        DECODE (lc.cg_tipo_solicitud,'I', 'Inicial','A', 'Ampliaci�n','R', 'Renovacion') AS TIPO_SOLICITUD," +
				"        lc.ic_linea_credito_dm_padre AS FOLIO_SULICITUD_RELACIONADA, cn.ic_nafin_electronico AS NUM_EPO,"    +
				"        epo.cg_razon_social AS EPO, cn2.ic_nafin_electronico AS NUM_IF,"                                     +
				"        cif.cg_razon_social AS IF, 'N/A' AS NUM_DISTRIBUIDOR, 'N/A' AS DISTRIBUIDOR,"                        +
				"        'N/A' AS FECHA_SOLICITUD,"                                                                           +
				"        TO_CHAR (ac.df_acuse, 'DD/MM/YYYY') AS FECHA_AUTORIZACION,"                                          +
				"        mon.cd_nombre AS MONEDA, el.cd_descripcion AS ESTATUS, 'N/A' AS CAUSA_RECHAZO,"                      +
				"        lc.ig_plazo AS PLAZO,"                                                                               +
				"        TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') AS FECHA_VENCIMIENTO,"                           +
				"        cif2.cd_nombre AS FINANCIERA, lc.cg_num_cuenta_epo AS EPO_CLIENTE_NUM_CUENTA,"                       +
				"        lc.cg_num_cuenta_if AS IF_NUM_CUENTA, lc.fn_monto_autorizado AS MONTO_AUTORIZADO,"                   +
				"        lc.fn_saldo_total AS SALDO_DISPONIBLE, mon.ic_moneda"                                                +
				"   FROM dis_linea_credito_dm lc,"                                                                            +
				"        comrel_nafin cn,"                                                                                    +
				"        comrel_nafin cn2,"                                                                                   +
				"        comrel_producto_epo pe,"                                                                             +
				"        dis_acuse4 ac,"                                                                                      +
				"        comcat_financiera cif2,"                                                                             +
				"        comcat_epo epo,"                                                                                     +
				"        comcat_if cif,"                                                                                      +
				"        comcat_moneda mon,"                                                                                  +
				"        comcat_estatus_linea el,"                                                                            +
				"        comcat_producto_nafin pn"                                                                            +
				"  WHERE lc.ic_epo = cn.ic_epo_pyme_if"                                                                       +
				"    AND lc.ic_if = cn2.ic_epo_pyme_if"                                                                       +
				"    AND lc.ic_epo = pe.ic_epo"                                                                               +
				"    AND pe.ic_producto_nafin = pn.ic_producto_nafin"                                                         +
				"    AND lc.cc_acuse = ac.cc_acuse"                                                                           +
				"    AND lc.ic_financiera = cif2.ic_financiera (+)"                                                           +
				"    AND lc.ic_epo = epo.ic_epo"                                                                              +
				"    AND lc.ic_if = cif.ic_if"                                                                                +
				"    AND lc.ic_moneda = mon.ic_moneda"                                                                        +
				"    AND lc.ic_estatus_linea = el.ic_estatus_linea"                                                           +
				"    AND cn.cg_tipo = 'E'"                                                                                    +
				"    AND cn2.cg_tipo = 'I'"                                                                                   +
				"    AND pn.ic_producto_nafin = 4"                                                                            +
				"    AND epo.cs_habilitado = 'S' "                                                                            +
				"    AND NVL (pe.cg_tipos_credito, pn.cg_tipos_credito) IN ('A',  'D') "+condicion.toString()+" ORDER BY 1");
			} else if("C".equals(tipo_creditoM)){
				qrySentencia.append(
				" SELECT   /*+ordered index(lc IN_COM_LINEA_CREDITO_02_NUK) use_nl(p cn cn2 ac cif2 cif mon el pn lc2)*/"         +
				"        lc.cg_tipo_solicitud ||"                                                                                 +
				"        NVL (p.in_numero_sirac,'') ||lc.ic_linea_credito AS FOLIO_SOLICITUD,"                                    +
				"        'Cr�dito en Cuenta Corriente' AS TIPO_CREDITO,"                                                          +
				"        DECODE (lc.cg_tipo_solicitud,'I', 'Inicial','A', 'Ampliaci�n','R', 'Renovacion') AS TIPO_SOLICITUD,"     +
				"        NVL (lc2.cg_tipo_solicitud,'') ||DECODE (lc.ic_linea_credito_padre,NULL,'',NVL (p.in_numero_sirac,'')) " +
				"        ||NVL (lc.ic_linea_credito_padre,'') AS FOLIO_SULICITUD_RELACIONADA,"                                    +
				"        'N/A' AS NUM_EPO, 'N/A' AS EPO, cn2.ic_nafin_electronico AS NUM_IF,"                                     +
				"        cif.cg_razon_social AS IF, cn.ic_nafin_electronico AS NUM_DISTRIBUIDOR,"                                 +
				"        p.cg_razon_social AS DISTRIBUIDOR, 'N/A' AS FECHA_SOLICITUD,"                                            +
				"        TO_CHAR (ac.df_acuse, 'DD/MM/YYYY') AS FECHA_AUTORIZACION,"                                              +
				"        mon.cd_nombre AS MONEDA, el.cd_descripcion AS ESTATUS, 'N/A' AS CAUSA_RECHAZO, '' AS PLAZO,"             +
				"        TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') AS FECHA_VENCIMIENTO,"                               +
				"        cif2.cd_nombre AS FINANCIERA, lc.cg_numero_cuenta AS EPO_CLIENTE_NUM_CUENTA,"                            +
				"        lc.cg_numero_cuenta_if AS IF_NUM_CUENTA, lc.fn_monto_autorizado AS MONTO_AUTORIZADO,"                    +
				"        lc.fn_saldo_linea_total AS SALDO_DISPONIBLE, mon.ic_moneda"                                              +
				"   FROM com_linea_credito lc,"                                                                                   +
				"        com_linea_credito lc2,"                                                                                  +
				"        comcat_pyme p,"                                                                                          +
				"        comrel_nafin cn,"                                                                                        +
				"        comrel_nafin cn2,"                                                                                       +
				"        dis_acuse4 ac,"                                                                                          +
				"        comcat_financiera cif2,"                                                                                 +
				"        comcat_if cif,"                                                                                          +
				"        comcat_moneda mon,"                                                                                      +
				"        comcat_estatus_linea el,"                                                                                +
				"        comcat_producto_nafin pn"                                                                                +
				"  WHERE lc.ic_pyme = p.ic_pyme"                                                                                  +
				"    AND lc.ic_producto_nafin = pn.ic_producto_nafin"                                                             +
				"    AND lc.ic_moneda = mon.ic_moneda"                                                                            +
				"    AND lc.cc_acuse = ac.cc_acuse (+)"                                                                           +
				"    AND lc.ic_estatus_linea = el.ic_estatus_linea"                                                               +
				"    AND lc.ic_linea_credito_padre = lc2.ic_linea_credito (+)"                                                    +
				"    AND lc.ic_financiera = cif2.ic_financiera (+)"                                                               +
				"    AND lc.ic_pyme = cn.ic_epo_pyme_if"                                                                          +
				"    AND lc.ic_if = cn2.ic_epo_pyme_if"                                                                           +
				"    AND lc.ic_if = cif.ic_if"                                                                                    +
				"    AND pn.ic_producto_nafin = 4"                                                                                +
				"    AND cn.cg_tipo = 'P'"                                                                                        +
				"    AND cn2.cg_tipo = 'I' "+condicion.toString()+" ORDER BY 1");
			}

			log.info(qrySentencia.toString());
		}catch(Exception e){
			log.warn("InfLinCredNafinDist::getDocumentQueryFileException "+e);
		}
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();
	}

	/**
	 * Genera el archivo CSV o PDF seg�n el tipo que se le indique.
	 * En el caso del PDF, el archivo generado no comtempla paginaci�n, imprime todos los registros que trae la consulta.
	 * @param request
	 * @param rs
	 * @param path
	 * @param tipo: CSV o PDF
	 * @return nombre del archivo
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){

		log.info("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();
		StringBuffer contenidoArchivo = new StringBuffer();

		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		int total = 0;

		if(tipo.equals("CSV")){

			try {

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
				buffer = new BufferedWriter(writer);

				contenidoArchivo = new StringBuffer();
				contenidoArchivo.append("Folio de solicitud, Tipo de cr�dito, Tipo de solicitud, Folio solicitud relacionada, "+
				"Num EPO, EPO, Num IF, IF, Num distribuidor, Distribuidor, Fecha solicitud, Fecha autorizaci�n,"+
				"Moneda, Estatus,  Causas del rechazo, Plazo, Fecha de vencimiento, Cliente- Banc Servicio, "+
				"EPO/Cliente- Num cuenta, IF- Num cuenta, Monto autorizado, Saldo disponible \n");

				while (rs.next()) {

					String folioSolicitud    = (rs.getString("FOLIO_SOLICITUD")             == null) ? "" : rs.getString("FOLIO_SOLICITUD");
					String tipoCredito       = (rs.getString("TIPO_CREDITO")                == null) ? "" : rs.getString("TIPO_CREDITO");
					String tipoSolicitud     = (rs.getString("TIPO_SOLICITUD")              == null) ? "" : rs.getString("TIPO_SOLICITUD");
					String folioSolicitudRel = (rs.getString("FOLIO_SULICITUD_RELACIONADA") == null) ? "" : rs.getString("FOLIO_SULICITUD_RELACIONADA");
					String numEpo            = (rs.getString("NUM_EPO")                     == null) ? "" : rs.getString("NUM_EPO");
					String epo               = (rs.getString("EPO")                         == null) ? "" : rs.getString("EPO");
					String numIf             = (rs.getString("NUM_IF")                      == null) ? "" : rs.getString("NUM_IF");
					String icIf              = (rs.getString("IF")                          == null) ? "" : rs.getString("IF");
					String numDistribuidor   = (rs.getString("NUM_DISTRIBUIDOR")            == null) ? "" : rs.getString("NUM_DISTRIBUIDOR");
					String distribuidor      = (rs.getString("DISTRIBUIDOR")                == null) ? "" : rs.getString("DISTRIBUIDOR");
					String fechaSolicitud    = (rs.getString("FECHA_SOLICITUD")             == null) ? "" : rs.getString("FECHA_SOLICITUD");
					String fechaAutorizacion = (rs.getString("FECHA_AUTORIZACION")          == null) ? "" : rs.getString("FECHA_AUTORIZACION");
					String moneda            = (rs.getString("MONEDA")                      == null) ? "" : rs.getString("MONEDA");
					String estatus           = (rs.getString("ESTATUS")                     == null) ? "" : rs.getString("ESTATUS");
					String causaRechazo      = (rs.getString("CAUSA_RECHAZO")               == null) ? "" : rs.getString("CAUSA_RECHAZO");
					String plazo             = (rs.getString("PLAZO")                       == null) ? "" : rs.getString("PLAZO");
					String fechaVencimiento  = (rs.getString("FECHA_VENCIMIENTO")           == null) ? "" : rs.getString("FECHA_VENCIMIENTO");
					String financiera        = (rs.getString("FINANCIERA")                  == null) ? "" : rs.getString("FINANCIERA");
					String epoCuenta         = (rs.getString("EPO_CLIENTE_NUM_CUENTA")      == null) ? "" : rs.getString("EPO_CLIENTE_NUM_CUENTA");
					String ifCuenta          = (rs.getString("IF_NUM_CUENTA")               == null) ? "" : rs.getString("IF_NUM_CUENTA");
					String montoAutorizado   = (rs.getString("MONTO_AUTORIZADO")            == null) ? "" : rs.getString("MONTO_AUTORIZADO");
					String saldoDisponible   = (rs.getString("SALDO_DISPONIBLE")            == null) ? "" : rs.getString("SALDO_DISPONIBLE");

					contenidoArchivo.append(folioSolicitud.replace(',',' ')+", "+
					tipoCredito.replace(',',' ')                           +", "+
					tipoSolicitud.replace(',',' ')                         +", "+
					folioSolicitudRel.replace(',',' ')                     +", "+
					numEpo.replace(',',' ')                                +", "+
					epo.replace(',',' ')                                   +", "+
					numIf.replace(',',' ')                                 +", "+
					icIf.replace(',',' ')                                  +", "+
					numDistribuidor.replace(',',' ')                       +", "+
					distribuidor.replace(',',' ')                          +", "+
					fechaSolicitud.replace(',',' ')                        +", "+
					fechaAutorizacion.replace(',',' ')                     +", "+
					moneda.replace(',',' ')                                +", "+
					estatus.replace(',',' ')                               +", "+
					causaRechazo.replace(',',' ')                          +", "+
					plazo.replace(',',' ')                                 +", "+
					fechaVencimiento.replace(',',' ')                      +", "+
					financiera.replace(',',' ')                            +", "+
					epoCuenta.replace(',',' ')                             +", "+
					ifCuenta.replace(',',' ')                              +", "+
					montoAutorizado.replace(',',' ')                       +", "+
					saldoDisponible.replace(',',' ')                       +"\n");

					total++;
					if(total==1000){
						total=0;
						buffer.write(contenidoArchivo.toString());
						contenidoArchivo = new StringBuffer();//Limpio
					}
				}//while(rs.next()){

				buffer.write(contenidoArchivo.toString());
				buffer.close();
				contenidoArchivo = new StringBuffer();//Limpio

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo CSV. ", e);
			}

		} else if(tipo.equals("PDF")){

			try {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

				pdfDoc.setLTable(22,100);
				pdfDoc.setLCell("Folio de solicitud ",          "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de cr�dito ",             "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de solicitud ",           "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Folio solicitud relacionada ", "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Num EPO ",                     "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("EPO ",                         "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Num IF ",                      "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("IF ",                          "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Num distribuidor ",            "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Distribuidor ",                "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha solicitud ",             "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha autorizaci�n ",          "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda ",                      "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Estatus ",                     "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Causas del rechazo ",          "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo ",                       "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de vencimiento ",        "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("DCliente- Banc Servicio ",     "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("EPO/Cliente- Num cuenta ",     "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("IF- Num cuenta ",              "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto autorizado ",            "celda01", ComunesPDF.CENTER);
				pdfDoc.setLCell("Saldo disponible ",            "celda01", ComunesPDF.CENTER);
				pdfDoc.setLHeaders();
				while (rs.next()){
					String folioSolicitud =    (rs.getString("FOLIO_SOLICITUD")             == null) ? "" : rs.getString("FOLIO_SOLICITUD");
					String tipoCredito =       (rs.getString("TIPO_CREDITO")                == null) ? "" : rs.getString("TIPO_CREDITO");
					String tipoSolicitud =     (rs.getString("TIPO_SOLICITUD")              == null) ? "" : rs.getString("TIPO_SOLICITUD");
					String folioSolicitudRel = (rs.getString("FOLIO_SULICITUD_RELACIONADA") == null) ? "" : rs.getString("FOLIO_SULICITUD_RELACIONADA");
					String numEpo =            (rs.getString("NUM_EPO")                     == null) ? "" : rs.getString("NUM_EPO");
					String epo =               (rs.getString("EPO")                         == null) ? "" : rs.getString("EPO");
					String numIf =             (rs.getString("NUM_IF")                      == null) ? "" : rs.getString("NUM_IF");
					String icIf =              (rs.getString("IF")                          == null) ? "" : rs.getString("IF");
					String numDistribuidor =   (rs.getString("NUM_DISTRIBUIDOR")            == null) ? "" : rs.getString("NUM_DISTRIBUIDOR");
					String distribuidor =      (rs.getString("DISTRIBUIDOR")                == null) ? "" : rs.getString("DISTRIBUIDOR");
					String fechaSolicitud =    (rs.getString("FECHA_SOLICITUD")             == null) ? "" : rs.getString("FECHA_SOLICITUD");
					String fechaAutorizacion = (rs.getString("FECHA_AUTORIZACION")          == null) ? "" : rs.getString("FECHA_AUTORIZACION");
					String moneda =            (rs.getString("MONEDA")                      == null) ? "" : rs.getString("MONEDA");
					String estatus =           (rs.getString("ESTATUS")                     == null) ? "" : rs.getString("ESTATUS");
					String causaRechazo =      (rs.getString("CAUSA_RECHAZO")               == null) ? "" : rs.getString("CAUSA_RECHAZO");
					String plazo =             (rs.getString("PLAZO")                       == null) ? "" : rs.getString("PLAZO");
					String fechaVencimiento =  (rs.getString("FECHA_VENCIMIENTO")           == null) ? "" : rs.getString("FECHA_VENCIMIENTO");
					String financiera =        (rs.getString("FINANCIERA")                  == null) ? "" : rs.getString("FINANCIERA");
					String epoCuenta =         (rs.getString("EPO_CLIENTE_NUM_CUENTA")      == null) ? "" : rs.getString("EPO_CLIENTE_NUM_CUENTA");
					String ifCuenta =          (rs.getString("IF_NUM_CUENTA")               == null) ? "" : rs.getString("IF_NUM_CUENTA");
					String montoAutorizado =   (rs.getString("MONTO_AUTORIZADO")            == null) ? "" : rs.getString("MONTO_AUTORIZADO");
					String saldoDisponible =   (rs.getString("SALDO_DISPONIBLE")            == null) ? "" : rs.getString("SALDO_DISPONIBLE");

					pdfDoc.setLCell(folioSolicitud,    "formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipoCredito,       "formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipoSolicitud,     "formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(folioSolicitudRel, "formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(numEpo,            "formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(epo,               "formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(numIf,             "formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(icIf,              "formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(numDistribuidor,   "formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(distribuidor,      "formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaSolicitud,    "formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaAutorizacion, "formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(moneda,            "formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(estatus,           "formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(causaRechazo,      "formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(plazo,             "formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaVencimiento,  "formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(financiera,        "formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(epoCuenta,         "formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(ifCuenta,          "formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoAutorizado,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(saldoDisponible,2),"formas",ComunesPDF.RIGHT);
				}

				pdfDoc.addLTable();
				pdfDoc.endDocument();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF. ", e);
			}

		}

		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}

	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo){
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		log.debug("*********** crearPageCustomFile (E)");
		try {
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
			pdfDoc = new ComunesPDF(2, path + nombreArchivo);
			
			String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
			String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
			String diaActual    = fechaActual.substring(0,2);
			String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
			String anioActual   = fechaActual.substring(6,10);
			String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
						
			pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
			pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
			
			
			pdfDoc.setLTable(22,100);
			pdfDoc.setLCell("Folio de solicitud ","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Tipo de cr�dito ","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Tipo de solicitud ","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Folio solicitud relacionada ","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Num EPO ","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("EPO ","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Num IF ","celda01",ComunesPDF.CENTER); 
			pdfDoc.setLCell("IF ","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Num distribuidor ","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Distribuidor ","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Fecha solicitud ","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Fecha autorizaci�n ","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Moneda ","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Estatus ","celda01",ComunesPDF.CENTER); 
			pdfDoc.setLCell("Causas del rechazo ","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Plazo ","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("DCliente- Banc Servicio ","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("EPO/Cliente- Num cuenta ","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("IF- Num cuenta ","celda01",ComunesPDF.CENTER);
			pdfDoc.setLCell("Monto autorizado ","celda01",ComunesPDF.CENTER); 
			pdfDoc.setLCell("Saldo disponible ","celda01",ComunesPDF.CENTER);
			
			
			while (rs.next())	{		
				String folioSolicitud = (rs.getString("FOLIO_SOLICITUD") == null) ? "" : rs.getString("FOLIO_SOLICITUD");	
				String tipoCredito = (rs.getString("TIPO_CREDITO") == null) ? "" : rs.getString("TIPO_CREDITO");				
				String tipoSolicitud = (rs.getString("TIPO_SOLICITUD") == null) ? "" : rs.getString("TIPO_SOLICITUD");
				String folioSolicitudRel = (rs.getString("FOLIO_SULICITUD_RELACIONADA") == null) ? "" : rs.getString("FOLIO_SULICITUD_RELACIONADA");
				String numEpo = (rs.getString("NUM_EPO") == null) ? "" : rs.getString("NUM_EPO");
				String epo = (rs.getString("EPO") == null) ? "" : rs.getString("EPO");
				String numIf = (rs.getString("NUM_IF") == null) ? "" : rs.getString("NUM_IF");
				String icIf = (rs.getString("IF") == null) ? "" : rs.getString("IF");
				String numDistribuidor = (rs.getString("NUM_DISTRIBUIDOR") == null) ? "" : rs.getString("NUM_DISTRIBUIDOR");
				String distribuidor = (rs.getString("DISTRIBUIDOR") == null) ? "" : rs.getString("DISTRIBUIDOR");
				String fechaSolicitud = (rs.getString("FECHA_SOLICITUD") == null) ? "" : rs.getString("FECHA_SOLICITUD");
				String fechaAutorizacion = (rs.getString("FECHA_AUTORIZACION") == null) ? "" : rs.getString("FECHA_AUTORIZACION");
				String moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
				String estatus = (rs.getString("ESTATUS") == null) ? "" : rs.getString("ESTATUS");
				String causaRechazo = (rs.getString("CAUSA_RECHAZO") == null) ? "" : rs.getString("CAUSA_RECHAZO");
				String plazo = (rs.getString("PLAZO") == null) ? "" : rs.getString("PLAZO");
				String fechaVencimiento = (rs.getString("FECHA_VENCIMIENTO") == null) ? "" : rs.getString("FECHA_VENCIMIENTO");
				String financiera = (rs.getString("FINANCIERA") == null) ? "" : rs.getString("FINANCIERA");
				String epoCuenta = (rs.getString("EPO_CLIENTE_NUM_CUENTA") == null) ? "" : rs.getString("EPO_CLIENTE_NUM_CUENTA");
				String ifCuenta = (rs.getString("IF_NUM_CUENTA") == null) ? "" : rs.getString("IF_NUM_CUENTA");
				String montoAutorizado = (rs.getString("MONTO_AUTORIZADO") == null) ? "" : rs.getString("MONTO_AUTORIZADO");
				String saldoDisponible = (rs.getString("SALDO_DISPONIBLE") == null) ? "" : rs.getString("SALDO_DISPONIBLE");
				
				pdfDoc.setLCell(folioSolicitud,"formas",ComunesPDF.CENTER);			
				pdfDoc.setLCell(tipoCredito,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(tipoSolicitud,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(folioSolicitudRel,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(numEpo,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(epo,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(numIf,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(icIf,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(numDistribuidor,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(distribuidor,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(fechaSolicitud,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(fechaAutorizacion,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(moneda,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(estatus,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(causaRechazo,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(plazo,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(financiera,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell(epoCuenta,"formas",ComunesPDF.CENTER);
			   pdfDoc.setLCell(ifCuenta,"formas",ComunesPDF.CENTER);
				pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoAutorizado,2),"formas",ComunesPDF.RIGHT);
				pdfDoc.setLCell("$"+Comunes.formatoDecimal(saldoDisponible,2),"formas",ComunesPDF.RIGHT);
			}
		
			pdfDoc.addLTable();
			pdfDoc.endDocument();	
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		}
		log.debug("*********** crearPageCustomFile (S)");
		return nombreArchivo;
					
	}

	/************************************************************
	 *								Get() y Set()   							*
	 ************************************************************/
	public String getTipo_creditoM() {
		return tipo_creditoM;
	}

	public void setTipo_creditoM(String tipo_creditoM) {
		this.tipo_creditoM = tipo_creditoM;
	}

	public String getTipo_solicM() {
		return tipo_solicM;
	}

	public void setTipo_solicM(String tipo_solicM) {
		this.tipo_solicM = tipo_solicM;
	}

	public String getIc_epoM() {
		return ic_epoM;
	}

	public void setIc_epoM(String ic_epoM) {
		this.ic_epoM = ic_epoM;
	}

	public String getIc_ifM() {
		return ic_ifM;
	}

	public void setIc_ifM(String ic_ifM) {
		this.ic_ifM = ic_ifM;
	}

	public String getIc_pymeM() {
		return ic_pymeM;
	}

	public void setIc_pymeM(String ic_pymeM) {
		this.ic_pymeM = ic_pymeM;
	}

	public String getIc_estatus_lineaM() {
		return ic_estatus_lineaM;
	}

	public void setIc_estatus_lineaM(String ic_estatus_lineaM) {
		this.ic_estatus_lineaM = ic_estatus_lineaM;
	}

	public String getFecha_auto_deM() {
		return fecha_auto_deM;
	}

	public void setFecha_auto_deM(String fecha_auto_deM) {
		this.fecha_auto_deM = fecha_auto_deM;
	}

	public String getFecha_auto_aM() {
		return fecha_auto_aM;
	}

	public void setFecha_auto_aM(String fecha_auto_aM) {
		this.fecha_auto_aM = fecha_auto_aM;
	}

	public String getPaginaOffset() {
		return paginaOffset;
	}

	public void setPaginaOffset(String paginaOffset) {
		this.paginaOffset = paginaOffset;
	}

	public String getPaginaNo() {
		return paginaNo;
	}

	public void setPaginaNo(String paginaNo) {
		this.paginaNo = paginaNo;
	}
	
	 public List getConditions(){
		return conditions; 
	}

//	public void setConditions(String conditions) {
//		this.conditions = conditions;
//	}
}