package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorPS;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class InfSolicIfDist implements IQueryGeneratorPS, IQueryGeneratorRegExtJS  {
		private int numList = 1;
		private List conditions;
		private String ctCredito,icEpo,icDist,numCred,fechaOper1,fechaOper2,monto1,monto2,cbMoneda,fechaVenc1,fechaVenc2,tipoCoInt,estatus,sesIf;
		private String tipoPago;
		private static final Log log = ServiceLocator.getInstance().getLog(InfSolicIfDist.class);


	public int getNumList(HttpServletRequest request){
		return numList;
	}

	public InfSolicIfDist() {}

	public String getAggregateCalculationQuery() {
		log.info("InfSolicIfDist::getAggregateCalculationQuery(E)");
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer qryDM  = new StringBuffer();
		StringBuffer qryCCC = new StringBuffer();
		StringBuffer qryFF = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		try {
			conditions=new ArrayList();
			conditions.add(sesIf);

			if(!"".equals(icEpo)){
				condicion.append(" and D.ic_epo = ? ");
				conditions.add(icEpo);
			}
			if(!"".equals(icDist)){
				condicion.append(" and D.ic_pyme = ? ");
				conditions.add(icDist);
			}
			if(!"".equals(numCred)){
				condicion.append(" and D.ic_documento = ? ");
				conditions.add(numCred);
			}
			//Agregada ic_moneda
			if(!"".equals(cbMoneda)){
				condicion.append(" and D.ic_moneda = ? ");
				conditions.add(cbMoneda);
			}
			//Agregada ic_moneda
			if("D".equals(ctCredito)){
				if(!"".equals(tipoCoInt)){
					condicion.append(" and s.ic_tipo_cobro_interes = ? ");
					conditions.add(tipoCoInt);
				}
			} else {
				if(!"".equals(tipoCoInt)){
					condicion.append(" and PE.ic_tipo_cobro_interes = ? ");
					conditions.add(tipoCoInt);
				}
			}

			if(!"".equals(estatus)){
				condicion.append(" and S.ic_estatus_solic = ? ");
				conditions.add(estatus);
			}
			if(!"".equals(monto1)&&!"".equals(monto2)){
				condicion.append(" and (D.fn_monto between ? and ?)");
				conditions.add(monto1);conditions.add(monto2);
			}
			if(!"".equals(fechaOper1)&&!"".equals(fechaOper2)){
				condicion.append(" and trunc(DS.df_fecha_seleccion) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");
				conditions.add(fechaOper1);conditions.add(fechaOper2);
			}
			if(!"".equals(fechaVenc1)&&!"".equals(fechaVenc2)){
				condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");
				conditions.add(fechaVenc1);conditions.add(fechaVenc2);
			}

			if(!"".equals(ctCredito)){
				condicion.append(" and  PEP.cg_tipo_credito = ? ");
				conditions.add(ctCredito);
			}
			if(!tipoPago.equals("")){
				condicion.append(" and  D.IG_TIPO_PAGO = ? ");
				conditions.add(tipoPago);
			}
			qryDM.append(
				"   select /*+ use_nl(d ds s lc cpe pep pe epo m es)"   +
				"     index(d cp_dis_documento_pk)"   +
				"     index(ds)"   +
				"     index(s)"   +
				"     index(lc cp_dis_linea_credito_dm_pk)*/"   +
				"   	 M.ic_moneda "   +
				"  	 ,M.cd_nombre"   +
				"  	,DS.fn_importe_recibir as MONTO"   +
				"  	,0 as MONTO_DEPOSITAR"   +
				"   FROM dis_documento D"   +
				"  	,dis_docto_seleccionado DS"   +
				"  	,dis_solicitud S"   +
				"  	,dis_linea_credito_dm LC"   +
				"     ,comrel_pyme_epo cpe"   +
				"  	,comrel_pyme_epo_x_producto PEP"   +
				"  	,comrel_producto_epo PE"   +
				"  	,comcat_epo epo"   +
				"  	,comcat_moneda M"   +
				"  	,comcat_estatus_solic ES"   +
				" 	,comcat_tipo_cobro_interes TCI"   +//////////////////////////////////////
				"   WHERE D.ic_documento = DS.ic_documento"   +
				"  	and D.ic_linea_credito_dm = LC.ic_linea_credito_dm"   +
				"     AND D.ic_epo = cpe.ic_epo"   +
				"     AND D.ic_pyme = cpe.ic_pyme"   +
				"  	AND D.ic_producto_nafin = PEP.ic_producto_nafin"   +
				"  	AND D.ic_producto_nafin = PE.ic_producto_nafin"   +
				"  	and D.ic_epo = PEP.ic_epo"   +
				"  	and pep.ic_epo = epo.ic_epo"   +
				"  	and D.ic_pyme = PEP.ic_pyme"   +
				"  	and D.ic_epo = PE.ic_epo"   +
				"  	and DS.ic_documento = S.ic_documento"   +
				"  	and S.ic_estatus_solic = ES.ic_estatus_solic"   +
				"		AND s.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes	"+/////////////////////////////////
				"  	and LC.ic_moneda = M.ic_moneda"   +
				"  	and PEP.ic_producto_nafin = 4"   +
				"  	and epo.cs_habilitado = 'S' "   +
				"   and lc.ic_moneda=d.ic_moneda" +
				"	and LC.ic_if = ? " +
				condicion.toString()  );
				
		qryCCC.append(
				"  select /*+ use_nl(d,ds,s,lc,cpe,pep,pe,m,es) "   +
				"       index(d cp_dis_documento_pk)"   +
				"       index(ds)"   +
				"       index(lc cp_com_linea_credito_pk)*/"   +
				"       M.ic_moneda "   +
				"  	 ,M.cd_nombre"   +
				"  	,DS.fn_importe_recibir as MONTO"   +
				"		,decode(es.IC_ESTATUS_SOLIC,14,(  ds.fn_importe_recibir  - (  (  ds.fn_importe_recibir  * NVL (ds.fn_porc_comision_apli, 0) ) / 100 )  ),0) AS monto_depositar	"	+
				"  FROM dis_documento D"   +
				"  ,dis_docto_seleccionado DS"   +
				"  ,dis_solicitud S"   +
				"  ,com_linea_credito LC"   +
				"  ,comrel_pyme_epo cpe"   +
				"  ,comrel_pyme_epo_x_producto PEP"   +
				"  ,comrel_producto_epo PE"   +
				"  ,comcat_epo epo"   +
				"  ,comcat_moneda M"   +
				"  ,comcat_estatus_solic ES"   +
				
				"	,com_bins_if bins"	+
				"	,dis_doctos_pago_tc ptc"	+
				
				"  WHERE D.ic_documento = DS.ic_documento"   +
				
				"  AND d.ic_bins = bins.ic_bins(+)"   +
				"  AND d.ic_orden_pago_tc = ptc.ic_orden_pago_tc(+)"   +
				
				"  and D.ic_linea_credito = LC.ic_linea_credito"   +
				"  AND D.ic_epo = cpe.ic_epo"   +
				"  AND D.ic_pyme = cpe.ic_pyme"   +
				"  AND D.ic_producto_nafin = PEP.ic_producto_nafin"   +
				"  AND D.ic_producto_nafin = PE.ic_producto_nafin"   +
				"  and D.ic_epo = PEP.ic_epo"   +
				"  and pep.ic_epo = epo.ic_epo"   +
				"  and D.ic_pyme = PEP.ic_pyme"   +
				"  and D.ic_epo = PE.ic_epo"   +
				"  and DS.ic_documento = S.ic_documento"   +
				"  and S.ic_estatus_solic = ES.ic_estatus_solic"   +
				"  and LC.ic_moneda = M.ic_moneda"   +
				"  and PEP.ic_producto_nafin = 4"   +
				"  and epo.cs_habilitado = 'S' "   +
				"  and lc.cs_factoraje_con_rec = 'N' " +
				"  and lc.ic_moneda=d.ic_moneda" +
				"  and (LC.ic_if = ? or bins.ic_if = "+sesIf+") " +
				condicion.toString());

		qryFF.append(
				"  select /*+ use_nl(d,ds,s,lc,cpe,pep,pe,m,es) "   +
				"       index(d cp_dis_documento_pk)"   +
				"       index(ds)"   +
				"       index(lc cp_com_linea_credito_pk)*/"   +
				"       M.ic_moneda "   +
				"  	 ,M.cd_nombre"   +
				"  	,DS.fn_importe_recibir as MONTO"   +
				"	,(ds.fn_importe_recibir-(( ds.fn_importe_recibir * NVL(ds.fn_porc_comision_apli,0)) /100)) as MONTO_DEPOSITAR "+
				"  FROM dis_documento D"   +
				"  ,dis_docto_seleccionado DS"   +
				"  ,dis_solicitud S"   +
				"  ,com_linea_credito LC"   +
				"  ,comrel_pyme_epo cpe"   +
				"  ,comrel_pyme_epo_x_producto PEP"   +
				"  ,comrel_producto_epo PE"   +
				"  ,comcat_epo epo"   +
				"  ,comcat_moneda M"   +
				"  ,comcat_estatus_solic ES"   +
				"  WHERE D.ic_documento = DS.ic_documento"   +
				"  and D.ic_linea_credito = LC.ic_linea_credito"   +
				"  AND D.ic_epo = cpe.ic_epo"   +
				"  AND D.ic_pyme = cpe.ic_pyme"   +
				"  AND D.ic_producto_nafin = PEP.ic_producto_nafin"   +
				"  AND D.ic_producto_nafin = PE.ic_producto_nafin"   +
				"  and D.ic_epo = PEP.ic_epo"   +
				"  and pep.ic_epo = epo.ic_epo"   +
				"  and D.ic_pyme = PEP.ic_pyme"   +
				"  and D.ic_epo = PE.ic_epo"   +
				"  and DS.ic_documento = S.ic_documento"   +
				"  and S.ic_estatus_solic = ES.ic_estatus_solic"   +
				"  and LC.ic_moneda = M.ic_moneda"   +
				"  and PEP.ic_producto_nafin = 4"   +
				"  and epo.cs_habilitado = 'S' "   +
				"  and lc.cs_factoraje_con_rec = 'S' " +
				"  and lc.ic_moneda=d.ic_moneda" +
				"  and LC.ic_if = ? " +
				condicion.toString());
				
			String qryAux = "";	
			if("D".equals(ctCredito)){
				this.numList = 1;
				qryAux=qryDM.toString();
			} else if("C".equals(ctCredito)){
				this.numList = 1;
				qryAux=qryCCC.toString();
			} else if("F".equals(ctCredito)){
				this.numList = 1;
				qryAux=qryFF.toString();
			} else {
				this.numList = 1;
				qryAux=qryDM.toString()+ " UNION ALL "+qryCCC.toString();
				conditions.addAll(conditions);
			}
			qrySentencia.append(
				"SELECT ic_moneda, cd_nombre, COUNT (1), SUM (monto),SUM(MONTO_DEPOSITAR), 'InfSolicIfDist::getAggregateCalculationQuery'"+
				"  FROM ("+qryAux.toString()+")"+
				"GROUP BY  ic_moneda,cd_nombre ORDER BY ic_moneda ");
				
            log.debug("conditions:"+conditions.toString());
            log.debug("qrySentencia: "+qrySentencia.toString());
		}catch(Exception e){
			log.error("InfSolicIfDist::getAggregateCalculationQuery "+e);
		}
		log.info("InfSolicIfDist::getAggregateCalculationQuery(S)");
		return qrySentencia.toString();
	}

	public String getDocumentSummaryQueryForIds(List pageIds) {
		log.info("InfSolicIfDist::getDocumentSummaryQueryForIds(E)");
		int i=0;
		StringBuffer qrySentencia     = new StringBuffer();
		StringBuffer qryDM            = new StringBuffer();
		StringBuffer qryCCC           = new StringBuffer();
		StringBuffer qryFF            = new StringBuffer();
		StringBuffer clavesDocumentos = new StringBuffer();

		String ses_ic_if    = getSesIf();
		String tipo_credito = getCtCredito();

		conditions = new ArrayList();
		for(int j = 0; j < pageIds.size(); j++){
			List lItem = (ArrayList)pageIds.get(j);
				clavesDocumentos.append("?,");
				conditions.add(new Long(lItem.get(0).toString()));
			}
			clavesDocumentos.deleteCharAt(clavesDocumentos.length()-1);
		qryDM.append(
			"  select /*+ use_nl(d,ds,s,lc,cpe,pep,pn,pe,t,e,p,m,es,tci) "   +
			"     index(pe cp_comrel_producto_epo_pk)"   +
			"     index(tci cp_comcat_tipo_cobro_inter_pk)*/"   +
			"     D.ic_documento"   +
			" 	,E.cg_razon_social as NOMBRE_EPO"   +
			" 	,P.cg_razon_social as NOMBRE_PYME"   +
			" 	,decode(PEP.cg_tipo_credito, 'D', 'Modalidad 1 (Riesgo Empresa de Primer Orden )', 'C', 'Modalidad 2 (Riesgo Distribuidor)') as TIPO_CREDITO"   +
			" 	,decode(s.cg_responsable_interes, 'E', 'EPO', 'D', 'Distribuidor') as RESPONSABLE_INTERES"   +
			"  ,decode(s.cg_tipo_cobranza, 'D', 'Delegada', 'N', 'No Delegada') as TIPO_COBRANZA"   +
			" 	,to_char(DS.df_fecha_seleccion, 'dd/mm/yyyy') as FECHA_OPERACION"   +
			" 	,M.cd_nombre as MONEDA"   +
			" 	,DS.fn_importe_recibir as MONTO"   +
			" 	,decode(nvl(PE.cg_tipo_conversion, PN.cg_tipo_conversion), 'P', 'Dolar-Peso','') as TIPO_CONVERSION"   +
			" 	,D.fn_tipo_cambio as TIPO_CAMBIO"   +
			"	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',T.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) AS REFERENCIA_TASA" +
//			" 	,decode(DS.cg_rel_mat, '+', DS.fn_valor_tasa + DS.fn_puntos, '-', DS.fn_valor_tasa - DS.fn_puntos, '/', DS.fn_valor_tasa / DS.fn_puntos, '*', DS.fn_valor_tasa * DS.fn_puntos) as TASA_INTERES"   +
			" 	,DS.fn_valor_tasa as TASA_INTERES"   +
			" 	,D.ig_plazo_credito as PLAZO"   +
			" 	,to_char(D.df_fecha_venc_credito, 'dd/mm/yyyy') as FECHA_VENCIMIENTO"   +
			" 	,DS.fn_importe_interes as MONTO_INTERES"   +
			" 	,TCI.cd_descripcion as TIPO_COBRO_INTERES"   +
			" 	,ES.cd_descripcion as ESTATUS"   +
			" 	,M.ic_moneda"   +
			" 	,LC.ic_moneda as moneda_linea"   +
			"  ,cpe.cg_num_distribuidor as numdist, ds.ic_tasa as cve_tasa,"   +
			"        NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion) as tipo_conver,"   +
			"        d.fn_tipo_cambio as tipo_cambio,"   +
			"        NVL (pe.cg_responsable_interes, pn.cg_responsable_interes) as resp_interes,"   +
			"        NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza)as rs_tipo_cobranza,d.ic_epo"   +
			" 		,ct_referencia, cg_campo1, cg_campo2, cg_campo3, cg_campo4, cg_campo5"   +
			
			"	,0 AS comision_aplicable	"	+//FODEA-013-2014
			"	,0 AS monto_comision	"	+//FODEA-013-2014
			"	,0 AS monto_depositar	"	+//FODEA-013-2014
			"	,'' AS opera_tarjeta	"	+//FODEA-013-2014
			"	,'' AS id_orden_enviado	"	+//FODEA-013-2014
			"	,'' AS id_operacion	"	+//FODEA-013-2014
			"	,'' AS codigo_autorizacion	"	+//FODEA-013-2014
			"	,'' AS fecha_registro	"	+//FODEA-013-2014
			"	,'' AS bins	"	+//FODEA-013-2014
			"	,'' AS num_tc	"	+//FODEA-013-2014
			" , D.IG_TIPO_PAGO "+ 
			" , S.CC_ACUSE " +
			" , (SELECT CASE WHEN COUNT(*) > 0 THEN 'S' ELSE 'N' END FROM COM_ARCDOCTOS WHERE CC_ACUSE = S.CC_ACUSE) AS MUESTRA_VISOR " +
			"  FROM dis_documento D"   +
			" 	,dis_docto_seleccionado DS"   +
			" 	,dis_solicitud S"   +
			" 	,dis_linea_credito_dm LC"   +
			"  ,comrel_pyme_epo cpe"   +
			" 	,comrel_pyme_epo_x_producto PEP"   +
			" 	,comcat_producto_nafin PN"   +
			" 	,comrel_producto_epo PE"   +
			" 	,comcat_tasa T"   +
			" 	,comcat_epo E"   +
			" 	,comcat_pyme P"   +
			" 	,comcat_moneda M"   +
			" 	,comcat_estatus_solic ES"   +
			" 	,comcat_tipo_cobro_interes TCI"   +
			"  WHERE D.ic_documento = DS.ic_documento "   +
			" 	and D.ic_linea_credito_dm = LC.ic_linea_credito_dm"   +
			" 	and D.ic_epo = E.ic_epo"   +
			" 	and D.ic_pyme = P.ic_pyme"   +
			"  AND D.ic_epo = cpe.ic_epo"   +
			"  AND D.ic_pyme = cpe.ic_pyme"   +
			" 	AND D.ic_producto_nafin = PN.ic_producto_nafin"   +
			" 	AND D.ic_producto_nafin = PEP.ic_producto_nafin"   +
			" 	AND D.ic_producto_nafin = PE.ic_producto_nafin"   +
			" 	and D.ic_epo = PEP.ic_epo"   +
			" 	and D.ic_pyme = PEP.ic_pyme"   +
			" 	and D.ic_epo = PE.ic_epo"   +
			" 	and DS.ic_documento = S.ic_documento"   +
			" 	and DS.ic_tasa = T.ic_tasa"   +
			" 	and S.ic_estatus_solic = ES.ic_estatus_solic"   +
			" 	and LC.ic_moneda = M.ic_moneda"   +			
			" 	and s.ic_tipo_cobro_interes = TCI.ic_tipo_cobro_interes"   +
			" 	and PN.ic_producto_nafin = 4"   +
			"   and lc.ic_moneda=d.ic_moneda" +
//			"	and LC.ic_if = ? " + 
			"   and D.ic_documento in (" + clavesDocumentos + ")" );
				
		qryCCC.append(
			" select /*+ use_nl(d,ds,s,lc,cpe,pep,pn,pe,t,e,p,m,es,tci)"   +
			" index(pe cp_comrel_producto_epo_pk)"   +
			" index(tci cp_comcat_tipo_cobro_inter_pk)*/"   +
			" D.ic_documento"   +
			" ,E.cg_razon_social as NOMBRE_EPO"   +
			" ,P.cg_razon_social as NOMBRE_PYME"   +
			" ,decode(PEP.cg_tipo_credito, 'D', 'Modalidad 1 (Riesgo Empresa de Primer Orden )', 'C', 'Modalidad 2 (Riesgo Distribuidor)') as TIPO_CREDITO"   +
			" ,decode(nvl(PE.cg_responsable_interes, PN.cg_responsable_interes), 'E', 'EPO', 'D', 'Distribuidor') as RESPONSABLE_INTERES"   +
			" ,decode(nvl(PE.cg_tipo_cobranza, PN.cg_tipo_cobranza), 'D', 'Delegada', 'N', 'No Delegada') as TIPO_COBRANZA"   +
			" ,to_char(DS.df_fecha_seleccion, 'dd/mm/yyyy') as FECHA_OPERACION"   +
			" ,M.cd_nombre as MONEDA"   +
			" ,DS.fn_importe_recibir as MONTO"   +
			" ,decode(nvl(PE.cg_tipo_conversion, PN.cg_tipo_conversion), 'P', 'Dolar-Peso', '') as TIPO_CONVERSION"   +
			" ,D.fn_tipo_cambio as TIPO_CAMBIO"   +
			" ,T.cd_nombre || ' ' || DS.cg_rel_mat || ' ' || DS.fn_puntos as REFERENCIA_TASA"   +
//			" ,decode(DS.cg_rel_mat, '+', DS.fn_valor_tasa + DS.fn_puntos, '-', DS.fn_valor_tasa - DS.fn_puntos, '/', DS.fn_valor_tasa / DS.fn_puntos, '*', DS.fn_valor_tasa * DS.fn_puntos) as TASA_INTERES"   +
			" 	,DS.fn_valor_tasa as TASA_INTERES"   +
			" ,D.ig_plazo_credito as PLAZO"   +
			" ,to_char(D.df_fecha_venc_credito, 'dd/mm/yyyy') as FECHA_VENCIMIENTO"   +
			" ,DS.fn_importe_interes as MONTO_INTERES"   +
			" ,TCI.cd_descripcion as TIPO_COBRO_INTERES"   +
			" ,ES.cd_descripcion as ESTATUS"   +
			" ,D.ic_moneda"   +
			" ,LC.ic_moneda as moneda_linea"   +
			" ,cpe.cg_num_distribuidor as numdist, ds.ic_tasa as cve_tasa,"   +
			" NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion) as tipo_conver,"   +
			" d.fn_tipo_cambio as tipo_cambio,"   +
			" NVL (pe.cg_responsable_interes, pn.cg_responsable_interes) as resp_interes,"   +
			" NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza) as rs_tipo_cobranza,d.ic_epo"   +
			" ,ct_referencia, cg_campo1, cg_campo2, cg_campo3, cg_campo4, cg_campo5"   +
			
			"	,NVL (ds.fn_porc_comision_apli, 0) AS comision_aplicable	"	+//FODEA-013-2014
			"	,((ds.fn_importe_recibir * NVL (ds.fn_porc_comision_apli, 0)) / 100 ) AS monto_comision	"	+//FODEA-013-2014
			"	,ds.fn_importe_recibir - ((ds.fn_importe_recibir * NVL (ds.fn_porc_comision_apli, 0)) / 100) AS monto_depositar	"	+//FODEA-013-2014
			"	,'' AS opera_tarjeta	"	+//FODEA-013-2014
			"	,ptc.ic_orden_pago_tc AS id_orden_enviado	"	+//FODEA-013-2014
			"	,ptc.cg_transaccion_id AS id_operacion	"	+//FODEA-013-2014
			"	,DECODE (CONCAT (ptc.cg_codigo_resp, ptc.cg_codigo_desc),'', '',ptc.cg_codigo_resp || '-' || ptc.cg_codigo_desc ) AS codigo_autorizacion	"	+//FODEA-013-2014
			"	,TO_CHAR (ptc.df_fecha_autorizacion, 'dd/mm/yyyy') AS fecha_registro	"	+//FODEA-013-2014
			"	,bins.CG_CODIGO_BIN||' '||bins.CG_DESCRIPCION as bins "+//FODEA-013-2014
			"	,SUBSTR(ptc.CG_NUM_TARJETA,-4,4) as NUM_TC	"+
			" , D.IG_TIPO_PAGO "+ 
			" , S.CC_ACUSE " +
			" , (SELECT CASE WHEN COUNT(*) > 0 THEN 'S' ELSE 'N' END FROM COM_ARCDOCTOS WHERE CC_ACUSE = S.CC_ACUSE) AS MUESTRA_VISOR " +
			" FROM dis_documento D"   +
			" ,dis_docto_seleccionado DS"   +
			" ,dis_solicitud S"   +
			" ,com_linea_credito LC"   +
			" ,comrel_pyme_epo cpe"   +
			" ,comrel_pyme_epo_x_producto PEP"   +
			" ,comcat_producto_nafin PN"   +
			" ,comrel_producto_epo PE"   +
			" ,comcat_tasa T"   +
			" ,comcat_epo E"   +
			" ,comcat_pyme P"   +
			" ,comcat_moneda M"   +
			" ,comcat_estatus_solic ES"   +
			" ,comcat_tipo_cobro_interes TCI"   +
			
			"	,com_bins_if bins	"	+//FODEA-013-2014
			"	,dis_doctos_pago_tc ptc	"	+//FODEA-013-2014
			
			" WHERE D.ic_documento = DS.ic_documento"   +
			
			"	AND d.ic_bins = bins.ic_bins(+)	"	+//FODEA-013-2014
			"	AND d.ic_orden_pago_tc = ptc.ic_orden_pago_tc(+)	"	+//FODEA-013-2014
			
			" and D.ic_linea_credito = LC.ic_linea_credito"   +
			" and D.ic_epo = E.ic_epo"   +
			" and D.ic_pyme = P.ic_pyme"   +
			" AND D.ic_epo = cpe.ic_epo"   +
			" AND D.ic_pyme = cpe.ic_pyme"   +
			" AND D.ic_producto_nafin = PN.ic_producto_nafin"   +
			" AND D.ic_producto_nafin = PEP.ic_producto_nafin"   +
			" AND D.ic_producto_nafin = PE.ic_producto_nafin"   +
			" and D.ic_epo = PEP.ic_epo"   +
			" and D.ic_pyme = PEP.ic_pyme"   +
			" and D.ic_epo = PE.ic_epo"   +
			" and DS.ic_documento = S.ic_documento"   +
			" and DS.ic_tasa = T.ic_tasa"   +
			" and S.ic_estatus_solic = ES.ic_estatus_solic"   +
			" and LC.ic_moneda = M.ic_moneda"   +
			" and lc.ic_tipo_cobro_interes = TCI.ic_tipo_cobro_interes"   +
			" and PN.ic_producto_nafin = 4"   +
			" and lc.ic_moneda=d.ic_moneda" +
			"  and lc.cs_factoraje_con_rec = 'N' " +
//			" and LC.ic_if = ? " + 
			" and D.ic_documento in (" + clavesDocumentos + ")" );

		qryFF.append(
			" select /*+ use_nl(d,ds,s,lc,cpe,pep,pn,pe,t,e,p,m,es,tci)"   +
			" index(pe cp_comrel_producto_epo_pk)"   +
			" index(tci cp_comcat_tipo_cobro_inter_pk)*/"   +
			" D.ic_documento"   +
			" ,E.cg_razon_social as NOMBRE_EPO"   +
			" ,P.cg_razon_social as NOMBRE_PYME"   +
			" ,'Factoraje con Recurso' as TIPO_CREDITO"   +
			" ,decode(nvl(PE.cg_responsable_interes, PN.cg_responsable_interes), 'E', 'EPO', 'D', 'Distribuidor') as RESPONSABLE_INTERES"   +
			" ,decode(nvl(PE.cg_tipo_cobranza, PN.cg_tipo_cobranza), 'D', 'Delegada', 'N', 'No Delegada') as TIPO_COBRANZA"   +
			" ,to_char(DS.df_fecha_seleccion, 'dd/mm/yyyy') as FECHA_OPERACION"   +
			" ,M.cd_nombre as MONEDA"   +
			" ,DS.fn_importe_recibir as MONTO"   +
			" ,decode(nvl(PE.cg_tipo_conversion, PN.cg_tipo_conversion), 'P', 'Dolar-Peso', '') as TIPO_CONVERSION"   +
			" ,D.fn_tipo_cambio as TIPO_CAMBIO"   +
			" ,T.cd_nombre || ' ' || DS.cg_rel_mat || ' ' || DS.fn_puntos as REFERENCIA_TASA"   +
//			" ,decode(DS.cg_rel_mat, '+', DS.fn_valor_tasa + DS.fn_puntos, '-', DS.fn_valor_tasa - DS.fn_puntos, '/', DS.fn_valor_tasa / DS.fn_puntos, '*', DS.fn_valor_tasa * DS.fn_puntos) as TASA_INTERES"   +
			" 	,DS.fn_valor_tasa as TASA_INTERES"   +
			" ,D.ig_plazo_credito as PLAZO"   +
			" ,to_char(D.df_fecha_venc_credito, 'dd/mm/yyyy') as FECHA_VENCIMIENTO"   +
			" ,DS.fn_importe_interes as MONTO_INTERES"   +
			" ,TCI.cd_descripcion as TIPO_COBRO_INTERES"   +
			" ,ES.cd_descripcion as ESTATUS"   +
			" ,D.ic_moneda"   +
			" ,LC.ic_moneda as moneda_linea"   +
			" ,cpe.cg_num_distribuidor as numdist, ds.ic_tasa as cve_tasa,"   +
			" NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion) as tipo_conver,"   +
			" d.fn_tipo_cambio as tipo_cambio,"   +
			" NVL (pe.cg_responsable_interes, pn.cg_responsable_interes) as resp_interes,"   +
			" NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza) as rs_tipo_cobranza,d.ic_epo"   +
			" ,ct_referencia, cg_campo1, cg_campo2, cg_campo3, cg_campo4, cg_campo5"   +
			
			"	,NVL (ds.fn_porc_comision_apli, 0) AS comision_aplicable	"	+//FODEA-013-2014
			"	,((ds.fn_importe_recibir * NVL (ds.fn_porc_comision_apli, 0)) / 100 ) AS monto_comision	"	+//FODEA-013-2014
			"	,ds.fn_importe_recibir - ((ds.fn_importe_recibir * NVL (ds.fn_porc_comision_apli, 0)) / 100) AS monto_depositar	"	+//FODEA-013-2014
			"	,'' AS opera_tarjeta	"	+//FODEA-013-2014
			"	,ptc.ic_orden_pago_tc AS id_orden_enviado	"	+//FODEA-013-2014
			"	,ptc.cg_transaccion_id AS id_operacion	"	+//FODEA-013-2014
			"	,DECODE (CONCAT (ptc.cg_codigo_resp, ptc.cg_codigo_desc),'', '',ptc.cg_codigo_resp || '-' || ptc.cg_codigo_desc ) AS codigo_autorizacion	"	+//FODEA-013-2014
			"	,TO_CHAR (ptc.df_fecha_autorizacion, 'dd/mm/yyyy') AS fecha_registro	"	+//FODEA-013-2014
			"	,bins.CG_CODIGO_BIN||' '||bins.CG_DESCRIPCION as bins "+//FODEA-013-2014
			"	,SUBSTR(ptc.CG_NUM_TARJETA,-4,4) as NUM_TC	"+
			" , D.IG_TIPO_PAGO " +
			" , S.CC_ACUSE " +
			" , (SELECT CASE WHEN COUNT(*) > 0 THEN 'S' ELSE 'N' END FROM COM_ARCDOCTOS WHERE CC_ACUSE = S.CC_ACUSE) AS MUESTRA_VISOR " +
			" FROM dis_documento D"   +
			" ,dis_docto_seleccionado DS"   +
			" ,dis_solicitud S"   +
			" ,com_linea_credito LC"   +
			" ,comrel_pyme_epo cpe"   +
			" ,comrel_pyme_epo_x_producto PEP"   +
			" ,comcat_producto_nafin PN"   +
			" ,comrel_producto_epo PE"   +
			" ,comcat_tasa T"   +
			" ,comcat_epo E"   +
			" ,comcat_pyme P"   +
			" ,comcat_moneda M"   +
			" ,comcat_estatus_solic ES"   +
			" ,comcat_tipo_cobro_interes TCI"   +
			
			"	,com_bins_if bins	"	+//FODEA-013-2014
			"	,dis_doctos_pago_tc ptc	"	+//FODEA-013-2014
			
			" WHERE D.ic_documento = DS.ic_documento"   +
			
			"	AND d.ic_bins = bins.ic_bins(+)	"	+//FODEA-013-2014
			"	AND d.ic_orden_pago_tc = ptc.ic_orden_pago_tc(+)	"	+//FODEA-013-2014
			
			" and D.ic_linea_credito = LC.ic_linea_credito"   +
			" and D.ic_epo = E.ic_epo"   +
			" and D.ic_pyme = P.ic_pyme"   +
			" AND D.ic_epo = cpe.ic_epo"   +
			" AND D.ic_pyme = cpe.ic_pyme"   +
			" AND D.ic_producto_nafin = PN.ic_producto_nafin"   +
			" AND D.ic_producto_nafin = PEP.ic_producto_nafin"   +
			" AND D.ic_producto_nafin = PE.ic_producto_nafin"   +
			" and D.ic_epo = PEP.ic_epo"   +
			" and D.ic_pyme = PEP.ic_pyme"   +
			" and D.ic_epo = PE.ic_epo"   +
			" and DS.ic_documento = S.ic_documento"   +
			" and DS.ic_tasa = T.ic_tasa"   +
			" and S.ic_estatus_solic = ES.ic_estatus_solic"   +
			" and LC.ic_moneda = M.ic_moneda"   +
			" and PE.ic_tipo_cobro_interes = TCI.ic_tipo_cobro_interes"   +
			" and PN.ic_producto_nafin = 4"   +
			" and lc.cs_factoraje_con_rec = 'S' " +
			" and lc.ic_moneda=d.ic_moneda" +
//			" and LC.ic_if = ? " + 
			" and D.ic_documento in (" + clavesDocumentos + ")" );

			if("D".equals(tipo_credito)){
				this.numList = 1;
				qrySentencia.append(qryDM.toString());
			} else if("C".equals(tipo_credito)){
				this.numList = 1;
				qrySentencia.append(qryCCC.toString());
			} else if("F".equals(tipo_credito)){
				this.numList = 1;
				qrySentencia.append(qryFF.toString());
			} else {
				this.numList = 2;
				qrySentencia.append(qryDM.toString()+ " UNION ALL "+qryCCC.toString());
				conditions.addAll(conditions);
			}

		log.debug("qrySentencia:"+qrySentencia.toString());
		log.debug("conditions:"+conditions.toString());
		log.info("InfSolicIfDist::getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();
	}

  	public ArrayList getConditions(HttpServletRequest request){
    	ArrayList conditions = new ArrayList();
    	int cond = 1;    
		String	ses_ic_if           = request.getSession().getAttribute("iNoCliente").toString();
		String	tipo_credito 		= (request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String 	fecha_vto_de		= (request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String 	fecha_vto_a			= (request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String	ic_pyme				= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String	ic_epo				= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String	ic_estatus_docto	= (request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto");
		String	ic_documento		= (request.getParameter("ic_documento")==null)?"":request.getParameter("ic_documento");
		String 	fecha_seleccion_de	= (request.getParameter("fecha_seleccion_de")==null)?"":request.getParameter("fecha_seleccion_de");
		String 	fecha_seleccion_a	= (request.getParameter("fecha_seleccion_a")==null)?"":request.getParameter("fecha_seleccion_a");
		String 	monto_credito_de	= (request.getParameter("monto_credito_de")==null)?"":request.getParameter("monto_credito_de");
		String 	monto_credito_a		= (request.getParameter("monto_credito_a")==null)?"":request.getParameter("monto_credito_a");
		String 	ic_tipo_cobro_int	= (request.getParameter("ic_tipo_cobro_int")==null)?"":request.getParameter("ic_tipo_cobro_int");
		String 	fn_monto_de			= (request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String 	fn_monto_a			= (request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String 	ig_numero_docto		= (request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String 	cc_acuse			= (request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String 	fecha_emision_de	= (request.getParameter("fecha_emision_de")==null)?"":request.getParameter("fecha_emision_de");
		String 	fecha_emision_a		= (request.getParameter("fecha_emision_a")==null)?"":request.getParameter("fecha_emision_a");
		String	ic_moneda			= (request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String 	monto_con_descuento	= (request.getParameter("monto_con_descuento")==null)?"":"checked";
		String	solo_cambio			= (request.getParameter("solo_cambio")==null)?"":"checked";
		String	modo_plazo			= (request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");

		if(!tipo_credito.equals("D")&&!tipo_credito.equals("C"))
			cond = 2;

		for(int i = 0; i < cond ; i++){
			conditions.add(ses_ic_if);
			
			if(!"".equals(ic_epo))
				conditions.add(ic_epo);
			if(!"".equals(ic_pyme))
				conditions.add(ic_pyme);
			if(!"".equals(ic_documento))
				conditions.add(ic_documento);
//Agregada ic_moneda
			if(!"".equals(ic_moneda))
				conditions.add(ic_moneda);
//Agregada ic_moneda
			if(!"".equals(ic_tipo_cobro_int))
				conditions.add(ic_tipo_cobro_int);
			if(!"".equals(ic_estatus_docto))
				conditions.add(ic_estatus_docto);
			if(!"".equals(monto_credito_de)&&!"".equals(monto_credito_a)){
				conditions.add(monto_credito_de);
				conditions.add(monto_credito_a);
			}
			if(!"".equals(fecha_seleccion_de)&&!"".equals(fecha_seleccion_a)){
				conditions.add(fecha_seleccion_de);
				conditions.add(fecha_seleccion_a);
			}
			if(!"".equals(fecha_vto_de)&&!"".equals(fecha_vto_a)){
				conditions.add(fecha_vto_de);
				conditions.add(fecha_vto_a);
			}
		}

    	return conditions;
  	}

	public String getDocumentQueryFile() {
		log.info("InfSolicIfDist::getDocumentQueryFile(E)");
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer qryDM        = new StringBuffer();
		StringBuffer qryCCC       = new StringBuffer();
		StringBuffer qryFF        = new StringBuffer();
		StringBuffer condicion    = new StringBuffer();

		try {
			conditions=new ArrayList();
			conditions.add(sesIf);

			if(!"".equals(icEpo)){
				condicion.append(" and D.ic_epo = ? ");
				conditions.add(icEpo);
			}
			if(!"".equals(icDist)){
				condicion.append(" and D.ic_pyme = ? ");
				conditions.add(icDist);
			}
			if(!"".equals(numCred)){
				condicion.append(" and D.ic_documento = ? ");
				conditions.add(numCred);
			}
			//Agregada ic_moneda
			if(!"".equals(cbMoneda)){
				condicion.append(" and D.ic_moneda = ? ");
				conditions.add(cbMoneda);
			}
			//Agregada ic_moneda
			if("D".equals(ctCredito)){
				if(!"".equals(tipoCoInt)){
					condicion.append(" and s.ic_tipo_cobro_interes = ? ");
					conditions.add(tipoCoInt);
				}
			} else{
				if(!"".equals(tipoCoInt)){
					condicion.append(" and PE.ic_tipo_cobro_interes = ? ");
					conditions.add(tipoCoInt);
				}
			}
			if(!"".equals(estatus)){
				condicion.append(" and S.ic_estatus_solic = ? ");
				conditions.add(estatus);
			}
			if(!"".equals(monto1)&&!"".equals(monto2)){
				condicion.append(" and (D.fn_monto between ? and ?)");
				conditions.add(monto1);conditions.add(monto2);
			}
			if(!"".equals(fechaOper1)&&!"".equals(fechaOper2)){
				condicion.append(" and trunc(DS.df_fecha_seleccion) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");
				conditions.add(fechaOper1);conditions.add(fechaOper2);
			}
			if(!"".equals(fechaVenc1)&&!"".equals(fechaVenc2)){
				condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");
				conditions.add(fechaVenc1);conditions.add(fechaVenc2);
			}
			if(!"".equals(ctCredito)){
				condicion.append(" and  PEP.cg_tipo_credito = ? ");
				conditions.add(ctCredito);
			}
			if(!"".equals(tipoPago)){
				condicion.append(" and  D.IG_TIPO_PAGO = ? ");
				conditions.add(tipoPago);
			}
			qryDM.append(
			"  select /*+ use_nl(d,ds,s,lc,cpe,pep,pn,pe,t,e,p,m,es,tci) "   +
			"     index(pe cp_comrel_producto_epo_pk)"   +
			"     index(tci cp_comcat_tipo_cobro_inter_pk)*/"   +
			"     D.ic_documento"   +
			" 	,E.cg_razon_social as NOMBRE_EPO"   +
			" 	,P.cg_razon_social as NOMBRE_PYME"   +
			" 	,decode(PEP.cg_tipo_credito, 'D', 'Modalidad 1 (Riesgo Empresa de Primer Orden )', 'C', 'Modalidad 2 (Riesgo Distribuidor)') as TIPO_CREDITO"   +
			" 	,decode(s.cg_responsable_interes, 'E', 'EPO', 'D', 'Distribuidor') as RESPONSABLE_INTERES"   +
			"  ,decode(s.cg_tipo_cobranza, 'D', 'Delegada', 'N', 'No Delegada') as TIPO_COBRANZA"   +
			" 	,to_char(DS.df_fecha_seleccion, 'dd/mm/yyyy') as FECHA_OPERACION"   +
			" 	,M.cd_nombre as MONEDA"   +
			" 	,DS.fn_importe_recibir as MONTO"   +
			" 	,decode(nvl(PE.cg_tipo_conversion, PN.cg_tipo_conversion), 'P', 'Dolar-Peso','') as TIPO_CONVERSION"   +
			" 	,D.fn_tipo_cambio as TIPO_CAMBIO"   +
			"	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',T.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) AS REFERENCIA_TASA" +
//			" 	,decode(DS.cg_rel_mat, '+', DS.fn_valor_tasa + DS.fn_puntos, '-', DS.fn_valor_tasa - DS.fn_puntos, '/', DS.fn_valor_tasa / DS.fn_puntos, '*', DS.fn_valor_tasa * DS.fn_puntos) as TASA_INTERES"   +
			" 	,DS.fn_valor_tasa as TASA_INTERES"   +
			" 	,D.ig_plazo_credito as PLAZO"   +
			" 	,to_char(D.df_fecha_venc_credito, 'dd/mm/yyyy') as FECHA_VENCIMIENTO"   +
			" 	,DS.fn_importe_interes as MONTO_INTERES"   +
			" 	,TCI.cd_descripcion as TIPO_COBRO_INTERES"   +
			" 	,ES.cd_descripcion as ESTATUS"   +
			" 	,M.ic_moneda"   +
			" 	,LC.ic_moneda as moneda_linea"   +
			"  ,cpe.cg_num_distribuidor as numdist, ds.ic_tasa as cve_tasa,"   +
			"        NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion) as tipo_conver,"   +
			"        d.fn_tipo_cambio as tipo_cambio,"   +
			"        NVL (pe.cg_responsable_interes, pn.cg_responsable_interes) as resp_interes,"   +
			"        NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza)as rs_tipo_cobranza,d.ic_epo"   +
			" 		,ct_referencia, cg_campo1, cg_campo2, cg_campo3, cg_campo4, cg_campo5"   +
			"    ,0 AS comision_aplicable    "   + //FODEA-013-2014
			"    ,0 AS monto_comision    "   +//FODEA-013-2014
			"    ,0 AS monto_depositar    "   +//FODEA-013-2014
			"    ,'' AS opera_tarjeta    "   +//FODEA-013-2014
			"    ,'' AS id_orden_enviado    "   +//FODEA-013-2014
			"    ,'' AS id_operacion    "   +//FODEA-013-2014
			"    ,'' AS codigo_autorizacion    "   +//FODEA-013-2014
			"    ,'' AS fecha_registro    "   +//FODEA-013-2014
			"    ,'' AS bins    "   +//FODEA-013-2014
			"    ,'' AS NUM_TC    "   +//FODEA-013-2014
			" , D.IG_TIPO_PAGO "+
			"  FROM dis_documento D"   +
			" 	,dis_docto_seleccionado DS"   +
			" 	,dis_solicitud S"   +
			" 	,dis_linea_credito_dm LC"   +
			"  ,comrel_pyme_epo cpe"   +
			" 	,comrel_pyme_epo_x_producto PEP"   +
			" 	,comcat_producto_nafin PN"   +
			" 	,comrel_producto_epo PE"   +
			" 	,comcat_tasa T"   +
			" 	,comcat_epo E"   +
			" 	,comcat_pyme P"   +
			" 	,comcat_moneda M"   +
			" 	,comcat_estatus_solic ES"   +
			" 	,comcat_tipo_cobro_interes TCI"   +
			"  WHERE D.ic_documento = DS.ic_documento "   +
			" 	and D.ic_linea_credito_dm = LC.ic_linea_credito_dm"   +
			" 	and D.ic_epo = E.ic_epo"   +
			" 	and D.ic_pyme = P.ic_pyme"   +
			"  AND D.ic_epo = cpe.ic_epo"   +
			"  AND D.ic_pyme = cpe.ic_pyme"   +
			" 	AND D.ic_producto_nafin = PN.ic_producto_nafin"   +
			" 	AND D.ic_producto_nafin = PEP.ic_producto_nafin"   +
			" 	AND D.ic_producto_nafin = PE.ic_producto_nafin"   +
			" 	and D.ic_epo = PEP.ic_epo"   +
			" 	and D.ic_pyme = PEP.ic_pyme"   +
			" 	and D.ic_epo = PE.ic_epo"   +
			" 	and DS.ic_documento = S.ic_documento"   +
			" 	and DS.ic_tasa = T.ic_tasa"   +
			" 	and S.ic_estatus_solic = ES.ic_estatus_solic"   +
			" 	and LC.ic_moneda = M.ic_moneda"   +
			" 	and s.ic_tipo_cobro_interes = TCI.ic_tipo_cobro_interes"   +
			" 	and PN.ic_producto_nafin = 4"   +
			" 	and E.cs_habilitado = 'S' "   +
			"   and lc.ic_moneda=d.ic_moneda" +
			"	and LC.ic_if = ? " +
			condicion.toString()  );
				
		qryCCC.append(
			" select /*+ use_nl(d,ds,s,lc,cpe,pep,pn,pe,t,e,p,m,es,tci)"   +
			" index(pe cp_comrel_producto_epo_pk)"   +
			" index(tci cp_comcat_tipo_cobro_inter_pk)*/"   +
			" D.ic_documento"   +
			" ,E.cg_razon_social as NOMBRE_EPO"   +
			" ,P.cg_razon_social as NOMBRE_PYME"   +
			" ,decode(PEP.cg_tipo_credito, 'D', 'Modalidad 1 (Riesgo Empresa de Primer Orden )', 'C', 'Modalidad 2 (Riesgo Distribuidor)') as TIPO_CREDITO"   +
			" ,decode(nvl(PE.cg_responsable_interes, PN.cg_responsable_interes), 'E', 'EPO', 'D', 'Distribuidor') as RESPONSABLE_INTERES"   +
			" ,decode(nvl(PE.cg_tipo_cobranza, PN.cg_tipo_cobranza), 'D', 'Delegada', 'N', 'No Delegada') as TIPO_COBRANZA"   +
			" ,to_char(DS.df_fecha_seleccion, 'dd/mm/yyyy') as FECHA_OPERACION"   +
			" ,M.cd_nombre as MONEDA"   +
			" ,DS.fn_importe_recibir as MONTO"   +
			" ,decode(nvl(PE.cg_tipo_conversion, PN.cg_tipo_conversion), 'P', 'Dolar-Peso', '') as TIPO_CONVERSION"   +
			" ,D.fn_tipo_cambio as TIPO_CAMBIO"   +
			" ,T.cd_nombre || ' ' || DS.cg_rel_mat || ' ' || DS.fn_puntos as REFERENCIA_TASA"   +
//			" ,decode(DS.cg_rel_mat, '+', DS.fn_valor_tasa + DS.fn_puntos, '-', DS.fn_valor_tasa - DS.fn_puntos, '/', DS.fn_valor_tasa / DS.fn_puntos, '*', DS.fn_valor_tasa * DS.fn_puntos) as TASA_INTERES"   +
			" 	,DS.fn_valor_tasa as TASA_INTERES"   +
			" ,D.ig_plazo_credito as PLAZO"   +
			" ,to_char(D.df_fecha_venc_credito, 'dd/mm/yyyy') as FECHA_VENCIMIENTO"   +
			" ,DS.fn_importe_interes as MONTO_INTERES"   +
			" ,TCI.cd_descripcion as TIPO_COBRO_INTERES"   +
			" ,ES.cd_descripcion as ESTATUS"   +
			" ,D.ic_moneda"   +
			" ,LC.ic_moneda as moneda_linea"   +
			" ,cpe.cg_num_distribuidor as numdist, ds.ic_tasa as cve_tasa,"   +
			" NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion) as tipo_conver,"   +
			" d.fn_tipo_cambio as tipo_cambio,"   +
			" NVL (pe.cg_responsable_interes, pn.cg_responsable_interes) as resp_interes,"   +
			" NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza) as rs_tipo_cobranza,d.ic_epo"   +
			" ,ct_referencia, cg_campo1, cg_campo2, cg_campo3, cg_campo4, cg_campo5"   +
			
			"    ,NVL (ds.fn_porc_comision_apli, 0) AS comision_aplicable    "+//FODEA-013-2014    
			"    ,((ds.fn_importe_recibir * NVL (ds.fn_porc_comision_apli, 0)) / 100) AS monto_comision    "+//FODEA-013-2014
			"    ,  ds.fn_importe_recibir - ((ds.fn_importe_recibir * NVL (ds.fn_porc_comision_apli, 0)) / 100) AS monto_depositar    "+//FODEA-013-2014
			"    ,'' AS opera_tarjeta    "+//FODEA-013-2014
			"    ,ptc.ic_orden_pago_tc AS id_orden_enviado    "+//FODEA-013-2014
			"    ,ptc.cg_transaccion_id AS id_operacion    "+//FODEA-013-2014
			"    ,DECODE (CONCAT (ptc.cg_codigo_resp, ptc.cg_codigo_desc), '', '', ptc.cg_codigo_resp || '-' || ptc.cg_codigo_desc ) AS codigo_autorizacion    "+//FODEA-013-2014
		   "    ,TO_CHAR (ptc.df_fecha_autorizacion, 'dd/mm/yyyy') AS fecha_registro    "+//FODEA-013-2014
			"	,bins.CG_CODIGO_BIN||' '||bins.CG_DESCRIPCION as bins "+//FODEA-013-2014
			"	,SUBSTR(ptc.CG_NUM_TARJETA,-4,4) as NUM_TC	"+
			" , D.IG_TIPO_PAGO "+
			" FROM dis_documento D"   +
			" ,dis_docto_seleccionado DS"   +
			" ,dis_solicitud S"   +
			" ,com_linea_credito LC"   +
			" ,comrel_pyme_epo cpe"   +
			" ,comrel_pyme_epo_x_producto PEP"   +
			" ,comcat_producto_nafin PN"   +
			" ,comrel_producto_epo PE"   +
			" ,comcat_tasa T"   +
			" ,comcat_epo E"   +
			" ,comcat_pyme P"   +
			" ,comcat_moneda M"   +
			" ,comcat_estatus_solic ES"   +
			" ,comcat_tipo_cobro_interes TCI"   +
			
			"    ,com_bins_if bins   "+//FODEA-013-2014
			"    ,dis_doctos_pago_tc ptc   "+//FODEA-013-2014
			
			" WHERE D.ic_documento = DS.ic_documento"   +
			
			"    AND d.ic_bins = bins.ic_bins(+) "   +
			"    AND d.ic_orden_pago_tc = ptc.ic_orden_pago_tc(+) "   +
			
			
			" and D.ic_linea_credito = LC.ic_linea_credito"   +
			" and D.ic_epo = E.ic_epo"   +
			" and D.ic_pyme = P.ic_pyme"   +
			" AND D.ic_epo = cpe.ic_epo"   +
			" AND D.ic_pyme = cpe.ic_pyme"   +
			" AND D.ic_producto_nafin = PN.ic_producto_nafin"   +
			" AND D.ic_producto_nafin = PEP.ic_producto_nafin"   +
			" AND D.ic_producto_nafin = PE.ic_producto_nafin"   +
			" and D.ic_epo = PEP.ic_epo"   +
			" and D.ic_pyme = PEP.ic_pyme"   +
			" and D.ic_epo = PE.ic_epo"   +
			" and DS.ic_documento = S.ic_documento"   +
			" and DS.ic_tasa = T.ic_tasa"   +
			" and S.ic_estatus_solic = ES.ic_estatus_solic"   + 
			" and LC.ic_moneda = M.ic_moneda"   +
			" and lc.ic_tipo_cobro_interes = TCI.ic_tipo_cobro_interes"   + 
			" and PN.ic_producto_nafin = 4"   +
			" and E.cs_habilitado = 'S' "   +
			" and lc.cs_factoraje_con_rec = 'N' " +
			" and lc.ic_moneda=d.ic_moneda" +
			"	and (LC.ic_if = ? or bins.ic_if ="+sesIf+" )" +
	  		condicion.toString());

		qryFF.append(
			" select /*+ use_nl(d,ds,s,lc,cpe,pep,pn,pe,t,e,p,m,es,tci)"   +
			" index(pe cp_comrel_producto_epo_pk)"   +
			" index(tci cp_comcat_tipo_cobro_inter_pk)*/"   +
			" D.ic_documento"   +
			" ,E.cg_razon_social as NOMBRE_EPO"   +
			" ,P.cg_razon_social as NOMBRE_PYME"   +
			" ,'Factoraje con recurso' as TIPO_CREDITO"   +
			" ,decode(nvl(PE.cg_responsable_interes, PN.cg_responsable_interes), 'E', 'EPO', 'D', 'Distribuidor') as RESPONSABLE_INTERES"   +
			" ,decode(nvl(PE.cg_tipo_cobranza, PN.cg_tipo_cobranza), 'D', 'Delegada', 'N', 'No Delegada') as TIPO_COBRANZA"   +
			" ,to_char(DS.df_fecha_seleccion, 'dd/mm/yyyy') as FECHA_OPERACION"   +
			" ,M.cd_nombre as MONEDA"   +
			" ,DS.fn_importe_recibir as MONTO"   +
			" ,decode(nvl(PE.cg_tipo_conversion, PN.cg_tipo_conversion), 'P', 'Dolar-Peso', '') as TIPO_CONVERSION"   +
			" ,D.fn_tipo_cambio as TIPO_CAMBIO"   +
			" ,T.cd_nombre || ' ' || DS.cg_rel_mat || ' ' || DS.fn_puntos as REFERENCIA_TASA"   +
//			" ,decode(DS.cg_rel_mat, '+', DS.fn_valor_tasa + DS.fn_puntos, '-', DS.fn_valor_tasa - DS.fn_puntos, '/', DS.fn_valor_tasa / DS.fn_puntos, '*', DS.fn_valor_tasa * DS.fn_puntos) as TASA_INTERES"   +
			" 	,DS.fn_valor_tasa as TASA_INTERES"   +
			" ,D.ig_plazo_credito as PLAZO"   +
			" ,to_char(D.df_fecha_venc_credito, 'dd/mm/yyyy') as FECHA_VENCIMIENTO"   +
			" ,DS.fn_importe_interes as MONTO_INTERES"   +
			" ,TCI.cd_descripcion as TIPO_COBRO_INTERES"   +
			" ,ES.cd_descripcion as ESTATUS"   +
			" ,D.ic_moneda"   +
			" ,LC.ic_moneda as moneda_linea"   +
			" ,cpe.cg_num_distribuidor as numdist, ds.ic_tasa as cve_tasa,"   +
			" NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion) as tipo_conver,"   +
			" d.fn_tipo_cambio as tipo_cambio,"   +
			" NVL (pe.cg_responsable_interes, pn.cg_responsable_interes) as resp_interes,"   +
			" NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza) as rs_tipo_cobranza,d.ic_epo"   +
			" ,ct_referencia, cg_campo1, cg_campo2, cg_campo3, cg_campo4, cg_campo5"   +
			
			"    ,NVL (ds.fn_porc_comision_apli, 0) AS comision_aplicable    "+//FODEA-013-2014    
       "    ,((ds.fn_importe_recibir * NVL (ds.fn_porc_comision_apli, 0)) / 100) AS monto_comision    "+//FODEA-013-2014
       "    ,  ds.fn_importe_recibir - ((ds.fn_importe_recibir * NVL (ds.fn_porc_comision_apli, 0)) / 100) AS monto_depositar    "+//FODEA-013-2014
       "    ,'' AS opera_tarjeta    "+//FODEA-013-2014
       "    ,ptc.ic_orden_pago_tc AS id_orden_enviado    "+//FODEA-013-2014
       "    ,ptc.cg_transaccion_id AS id_operacion    "+//FODEA-013-2014
       "    ,DECODE (CONCAT (ptc.cg_codigo_resp, ptc.cg_codigo_desc), '', '', ptc.cg_codigo_resp || '-' || ptc.cg_codigo_desc ) AS codigo_autorizacion    "+//FODEA-013-2014
       "    ,TO_CHAR (ptc.df_fecha_autorizacion, 'dd/mm/yyyy') AS fecha_registro    "+//FODEA-013-2014
       "	,bins.CG_CODIGO_BIN||' '||bins.CG_DESCRIPCION as bins "+//FODEA-013-2014
		 "	,SUBSTR(ptc.CG_NUM_TARJETA,-4,4) as NUM_TC	"+
			" , D.IG_TIPO_PAGO "+
			" FROM dis_documento D"   +
			" ,dis_docto_seleccionado DS"   +
			" ,dis_solicitud S"   +
			" ,com_linea_credito LC"   +
			" ,comrel_pyme_epo cpe"   +
			" ,comrel_pyme_epo_x_producto PEP"   +
			" ,comcat_producto_nafin PN"   +
			" ,comrel_producto_epo PE"   +
			" ,comcat_tasa T"   +
			" ,comcat_epo E"   +
			" ,comcat_pyme P"   +
			" ,comcat_moneda M"   +
			" ,comcat_estatus_solic ES"   +
			" ,comcat_tipo_cobro_interes TCI"   +
			
			"    ,com_bins_if bins   "+//FODEA-013-2014
			"    ,dis_doctos_pago_tc ptc   "+//FODEA-013-2014
			
			" WHERE D.ic_documento = DS.ic_documento"   +
			
			"    AND d.ic_bins = bins.ic_bins(+) "   +//FODEA-013-2014
			"    AND d.ic_orden_pago_tc = ptc.ic_orden_pago_tc(+) "   +//FODEA-013-2014
			
			
			" and D.ic_linea_credito = LC.ic_linea_credito"   +
			" and D.ic_epo = E.ic_epo"   +
			" and D.ic_pyme = P.ic_pyme"   +
			" AND D.ic_epo = cpe.ic_epo"   +
			" AND D.ic_pyme = cpe.ic_pyme"   +
			" AND D.ic_producto_nafin = PN.ic_producto_nafin"   +
			" AND D.ic_producto_nafin = PEP.ic_producto_nafin"   +
			" AND D.ic_producto_nafin = PE.ic_producto_nafin"   +
			" and D.ic_epo = PEP.ic_epo"   +
			" and D.ic_pyme = PEP.ic_pyme"   +
			" and D.ic_epo = PE.ic_epo"   +
			" and DS.ic_documento = S.ic_documento"   +
			" and DS.ic_tasa = T.ic_tasa"   +
			" and S.ic_estatus_solic = ES.ic_estatus_solic"   +
			" and LC.ic_moneda = M.ic_moneda"   +
			" and PE.ic_tipo_cobro_interes = TCI.ic_tipo_cobro_interes"   + 
			" and PN.ic_producto_nafin = 4"   +
			" and E.cs_habilitado = 'S' "   +
			" and lc.cs_factoraje_con_rec = 'S' " +
			" and lc.ic_moneda=d.ic_moneda" +
			"	and (LC.ic_if = ? or bins.ic_if = "+sesIf+" )" +
	  		condicion.toString());

			if("D".equals(ctCredito)){
				this.numList = 1;
				qrySentencia.append(qryDM.toString());
			} else if("C".equals(ctCredito)){
				this.numList = 1;
				qrySentencia.append(qryCCC.toString());
			} else if("F".equals(ctCredito)){
				this.numList = 1;
				qrySentencia.append(qryFF.toString());
			} else {
				this.numList = 1;
				qrySentencia.append(qryDM.toString()+ " UNION ALL "+qryCCC.toString());
				conditions.addAll(conditions);
			}

			log.debug("qrySentencia " +qrySentencia);
			log.debug("conditions " +conditions);

		}catch(Exception e){
			log.error("InfSolicIfDist::getDocumentQueryFileException "+e);
		}
		log.info("InfSolicIfDist::getDocumentQueryFile(S)");
		return qrySentencia.toString();
	}

	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		log.info("InfSolicIfDist::crearPageCustomFile(E)");
		String nombreArchivo = "";
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		
		
		if("PDF".equals(tipo)){
			try{
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
				
				String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual		= fechaActual.substring(0,2);
				String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual		= fechaActual.substring(6,10);
				String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
				
				pdfDoc.setLTable(16, 100);
				pdfDoc.setLCell("No. Docto Final","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Dist.","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de Cr�dito","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Resp. Pago de Inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de Cobranza","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Operaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Ref. Tasa Inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tasa Inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo Cobro Inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Estatus","celda01",ComunesPDF.CENTER);
				
				while (reg.next()){
					String numeroDocumento		= (reg.getString(1) == null) ? "" : reg.getString(1);
					String epo						= (reg.getString("NOMBRE_EPO") == null) ? "" : reg.getString("NOMBRE_EPO");
					String dist						= (reg.getString("NOMBRE_PYME") == null) ? "" : reg.getString("NOMBRE_PYME");
					String tipoCredito			= (reg.getString("TIPO_CREDITO") == null) ? "" : reg.getString("TIPO_CREDITO");
					String respInt					= (reg.getString("RESPONSABLE_INTERES") == null) ? "" : reg.getString("RESPONSABLE_INTERES");
					String tipoCob					= (reg.getString("TIPO_COBRANZA") == null) ? "" : reg.getString("TIPO_COBRANZA");
					String fOpera					= (reg.getString("FECHA_OPERACION") == null) ? "" : reg.getString("FECHA_OPERACION");
					String moneda					= (reg.getString("MONEDA") == null) ? "" : reg.getString("MONEDA");
					String monto					= (reg.getString("MONTO") == 	null) ? "" : reg.getString("MONTO");
					String refTasa					= (reg.getString("REFERENCIA_TASA") == null) ? "" : reg.getString("REFERENCIA_TASA");
					String tasaInt					= (reg.getString("TASA_INTERES") == null) ? "" : reg.getString("TASA_INTERES");
					String plazo					= (reg.getString("PLAZO") == null) ? "" : reg.getString("PLAZO");
					String fVenc					= (reg.getString("FECHA_VENCIMIENTO") == 	null) ? "" : reg.getString("FECHA_VENCIMIENTO");
					String montoInteres			= (reg.getString("MONTO_INTERES") == null) ? "" : reg.getString("MONTO_INTERES");
					String cobroInt				= (reg.getString("TIPO_COBRO_INTERES") == null) ? "" : reg.getString("TIPO_COBRO_INTERES");
					String estatus					= (reg.getString("ESTATUS") == null) ? "" : reg.getString("ESTATUS");
					
					if(!estatus.equals("Operada TC")){//FODEA-013-2014
						pdfDoc.setLCell(numeroDocumento,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(epo,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(dist,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(tipoCredito,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(respInt,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(tipoCob,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(fOpera,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(monto,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(refTasa,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(tasaInt,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(plazo,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(fVenc,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(montoInteres,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(cobroInt,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(estatus,"formas",ComunesPDF.CENTER);
					}
					
				}
				//pdfDoc.addTable();
/*
				CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS();//toma el valor de sesi�n
				Registros totales = queryHelper.getResultCount(request);
				
			
				
		if (totales.getNumeroRegistros()>0) {
			int i = 0;
			List vecColumnas = null;
			for(i=0;i<totales.getNumeroRegistros();i++){
				vecColumnas = totales.getData(i);
				pdfDoc.setLCell("TOTAL "+vecColumnas.get(1).toString(),"celda01rep",ComunesPDF.LEFT,3);
				pdfDoc.setLCell(vecColumnas.get(2).toString(),"celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell(" ","celda01rep",ComunesPDF.CENTER,4);
				pdfDoc.setLCell(Comunes.formatoDecimal(vecColumnas.get(3).toString(),2),"celda01rep",ComunesPDF.LEFT,8);
		}
	}
*/
	pdfDoc.addLTable();
	pdfDoc.endDocument();
			}catch(Throwable e){
			e.printStackTrace();
				throw new AppException("Error al generar el archivo",e);
				
			}finally{
				try{
				}catch(Exception e){}
			}
		}
		log.info("InfSolicIfDist::crearPageCustomFile(S)");
		return nombreArchivo;
	}

	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
		log.info("InfSolicIfDist::crearCustomFile(E)");
		String linea = "";
		String nombreArchivo = "";
		String espacio = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;

		try {
			if(tipo.equals("CSV")) {
			linea = "No. Docto Final,EPO,Dist.,Tipo de Cr�dito,Resp. Pago de Inter�s,Tipo de Cobranza,Fecha de Operaci�n,Moneda,Monto,Ref. Tasa Inter�s"
			+",Tasa Inter�s,Plazo,Fecha de Vencimiento,Monto Inter�s,Tipo Cobro Inter�s,Estatus\n";
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);

			buffer.write(linea);

			while (rs.next()) {
				String numeroDocumento = (rs.getString(1)                     == null) ? "" : rs.getString(1);
				String epo             = (rs.getString("NOMBRE_EPO")          == null) ? "" : rs.getString("NOMBRE_EPO");
				String dist            = (rs.getString("NOMBRE_PYME")         == null) ? "" : rs.getString("NOMBRE_PYME");
				String tipoCredito     = (rs.getString("TIPO_CREDITO")        == null) ? "" : rs.getString("TIPO_CREDITO");
				String respInt         = (rs.getString("RESPONSABLE_INTERES") == null) ? "" : rs.getString("RESPONSABLE_INTERES");
				String tipoCob         = (rs.getString("TIPO_COBRANZA")       == null) ? "" : rs.getString("TIPO_COBRANZA");
				String fOpera          = (rs.getString("FECHA_OPERACION")     == null) ? "" : rs.getString("FECHA_OPERACION");
				String moneda          = (rs.getString("MONEDA")              == null) ? "" : rs.getString("MONEDA");
				String monto           = (rs.getString("MONTO")               == null) ? "" : rs.getString("MONTO");
				String refTasa         = (rs.getString("REFERENCIA_TASA")     == null) ? "" : rs.getString("REFERENCIA_TASA");
				String tasaInt         = (rs.getString("TASA_INTERES")        == null) ? "" : rs.getString("TASA_INTERES");
				String plazo           = (rs.getString("PLAZO")               == null) ? "" : rs.getString("PLAZO");
				String fVenc           = (rs.getString("FECHA_VENCIMIENTO")   == null) ? "" : rs.getString("FECHA_VENCIMIENTO");
				String montoInteres    = (rs.getString("MONTO_INTERES")       == null) ? "" : rs.getString("MONTO_INTERES");
				String cobroInt        = (rs.getString("TIPO_COBRO_INTERES")  == null) ? "" : rs.getString("TIPO_COBRO_INTERES");
				String estatus         = (rs.getString("ESTATUS")             == null) ? "" : rs.getString("ESTATUS");
				//INICIO: FODEA 09-2015
				String tipo_pago       = (rs.getString("IG_TIPO_PAGO")        == null) ? "" : rs.getString("IG_TIPO_PAGO");
				if(tipo_pago.equals("2")){
					plazo = plazo + " (M)";
				}
				//FIN: FODEA 09-2015
				if(!estatus.equals("Operada TC")){//FODEA-013-2014	
					linea = numeroDocumento.replace(',',' ') + ", " + 
									epo.replace(',',' ') + ", " +
									dist.replace(',',' ') + ", " +
									tipoCredito.replace(',',' ') + ", " +
									respInt.replace(',',' ') + ", " +
									tipoCob.replace(',',' ') + ", " +
									fOpera.replace(',',' ') + ", " +
									moneda.replace(',',' ') + ", " +
									monto.replace(',',' ') + ", " +
									refTasa.replace(',',' ') + ", " +
									tasaInt.replace(',',' ') + ", " +
									plazo.replace(',',' ') + ", " +
									fVenc.replace(',',' ') + ", " +
									montoInteres.replace(',',' ') + ", " +
									cobroInt.replace(',',' ') + ", " +
									estatus.replace(',',' ') + "\n";
						buffer.write(linea);
					}
		  }
/*
		  CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS();//toma el valor de sesi�n
				Registros totales = queryHelper.getResultCount(request);
				
			
		linea="";
		if (totales.getNumeroRegistros()>0) {
			int i = 0;
			List vecColumnas = null;
			for(i=0;i<totales.getNumeroRegistros();i++){
				vecColumnas = totales.getData(i);
				linea+=("TOTAL"+vecColumnas.get(1).toString()+",");
				linea+=(vecColumnas.get(2).toString()+",,,,,,,");
				linea+=(vecColumnas.get(3).toString()+"\n");
		}
	}
*/
            //buffer.write(linea);
            buffer.close();
		 }else if (tipo.equals("TC-CSV")) {
        linea = "No. Docto Final,EPO,Dist.,Tipo de Cr�dito,Resp. Pago de Inter�s,Tipo de Cobranza,Fecha de Operaci�n,Moneda,Monto,% Comisi�n Aplicable de Terceros ,Monto Comisi�n de Terceros (IVA Incluido),Monto a Depositar por Operaci�n,Ref. Tasa Inter�s,Tasa Inter�s,Plazo,Fecha de Vencimiento,Monto Inter�s,Tipo Cobro Inter�s,Estatus,Nombre del Producto,ID Orden Enviado,Respuesta de Operaci�n,C�digo Autorizaci�n,N�mero Tarjeta de Cr�dito\n";

        nombreArchivo = String.valueOf(Comunes.cadenaAleatoria(16)) + ".csv";
        writer = new OutputStreamWriter(new FileOutputStream(String.valueOf(path) + nombreArchivo, true), "ISO-8859-1");
        buffer = new BufferedWriter(writer);

        buffer.write(linea);

        while (rs.next()) {
          String numeroDocumento = rs.getString(1) == null ? "" : rs.getString(1);
          String epo = rs.getString("NOMBRE_EPO") == null ? "" : rs.getString("NOMBRE_EPO");
          String dist = rs.getString("NOMBRE_PYME") == null ? "" : rs.getString("NOMBRE_PYME");
          String tipoCredito = rs.getString("TIPO_CREDITO") == null ? "" : rs.getString("TIPO_CREDITO");
          String respInt = rs.getString("RESPONSABLE_INTERES") == null ? "" : rs.getString("RESPONSABLE_INTERES");
          String tipoCob = rs.getString("TIPO_COBRANZA") == null ? "" : rs.getString("TIPO_COBRANZA");
          String fOpera = rs.getString("FECHA_OPERACION") == null ? "" : rs.getString("FECHA_OPERACION");
          String moneda = rs.getString("MONEDA") == null ? "" : rs.getString("MONEDA");
          String monto = rs.getString("MONTO") == null ? "" : rs.getString("MONTO");

          String comisiponAplicable = rs.getString("COMISION_APLICABLE") == null ? "" : rs.getString("COMISION_APLICABLE");
          String montoComision = rs.getString("MONTO_COMISION") == null ? "" : rs.getString("MONTO_COMISION");
          String montoDepositar = rs.getString("MONTO_DEPOSITAR") == null ? "" : rs.getString("MONTO_DEPOSITAR");

          String refTasa = rs.getString("REFERENCIA_TASA") == null ? "" : rs.getString("REFERENCIA_TASA");
          String tasaInt = rs.getString("TASA_INTERES") == null ? "" : rs.getString("TASA_INTERES");
          String plazo = rs.getString("PLAZO") == null ? "" : rs.getString("PLAZO");
          String fVenc = rs.getString("FECHA_VENCIMIENTO") == null ? "" : rs.getString("FECHA_VENCIMIENTO");
          String montoInteres = rs.getString("MONTO_INTERES") == null ? "" : rs.getString("MONTO_INTERES");
          String cobroInt = rs.getString("TIPO_COBRO_INTERES") == null ? "" : rs.getString("TIPO_COBRO_INTERES");
          String estatus = rs.getString("ESTATUS") == null ? "" : rs.getString("ESTATUS");

          String bins = rs.getString("BINS") == null ? "" : rs.getString("BINS");
          String idOrdenEnviado = rs.getString("ID_ORDEN_ENVIADO") == null ? "" : rs.getString("ID_ORDEN_ENVIADO")+"'";
          String idOperacion = rs.getString("ID_OPERACION") == null ? "" : rs.getString("ID_OPERACION");
          String codigoAutorizacion = rs.getString("CODIGO_AUTORIZACION") == null ? "" : rs.getString("CODIGO_AUTORIZACION");
          String fechaRegistro = rs.getString("FECHA_REGISTRO") == null ? "" : rs.getString("FECHA_REGISTRO");
			 String numTarjeta = rs.getString("NUM_TC") == null ? "" : rs.getString("NUM_TC");

          String operaTC = rs.getString("OPERA_TARJETA") == null ? "" : rs.getString("OPERA_TARJETA");
          if ("Operada TC".equals(estatus)) {
            refTasa = "N/A";
            tasaInt = "N/A";
            plazo = "N/A";
            fVenc = "N/A";
            montoInteres = "N/A";
          } else {
            bins = "N/A";
            comisiponAplicable = "N/A"; montoComision = "N/A"; montoDepositar = "N/A";
            idOrdenEnviado = "N/A"; idOperacion = "N/A"; codigoAutorizacion = "N/A";
				numTarjeta = "N/A";
          }

          if ("Operada TC".equals(estatus)) {
					linea = String.valueOf(numeroDocumento.replace(',', ' ')) + ", " + 
						epo.replace(',', ' ') + ", " + dist.replace(',', ' ') + ", " + 
						tipoCredito.replace(',', ' ') + ", " + respInt.replace(',', ' ') + ", " + 
						tipoCob.replace(',', ' ') + ", " + fOpera.replace(',', ' ') + ", " +
						moneda.replace(',', ' ') + ", " + monto.replace(',', ' ') + ", " +
						comisiponAplicable.replace(',', ' ') + ", " + 
						montoComision.replace(',', ' ') + ", " +
						montoDepositar.replace(',', ' ') + ", " +
						refTasa.replace(',', ' ') + ", " + 
						tasaInt.replace(',', ' ') + ", " +
						plazo.replace(',', ' ') + ", " +
						fVenc.replace(',', ' ') + ", " +
						montoInteres.replace(',', ' ') + ", " + 
						cobroInt.replace(',', ' ') + ", " + 
						estatus.replace(',', ' ') + " , " +
						bins.replace(',', ' ') + " , " + 
						idOrdenEnviado.replace(',', ' ') + " , " + 
						idOperacion.replace(',', ' ') + " , " +
						codigoAutorizacion.replace(',', ' ') + " , "+
						((!"".equals(numTarjeta.replace(',', ' ').trim()))?"XXXX-XXXX-XXXX-"+numTarjeta:numTarjeta) +"\n";	
						
            buffer.write(linea);
          }
        }
/*
        CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS();
        Registros totales = queryHelper.getResultCount(request);

        linea = "";
        if (totales!=null && totales.getNumeroRegistros() > 0) {
          int i = 0;
          List vecColumnas = null;
          for (i = 0; i < totales.getNumeroRegistros(); i++) {
            vecColumnas = totales.getData(i);
            linea = String.valueOf(linea) + "TOTAL " + vecColumnas.get(1).toString() + ",";
            linea = String.valueOf(linea) + vecColumnas.get(2).toString() + ",,,,,,,";
            linea = String.valueOf(linea) + vecColumnas.get(3).toString() + ",,,";
				linea = String.valueOf(linea) + vecColumnas.get(4).toString() + "\n";
          }
        }
*/
        //buffer.write(linea);
        buffer.close();
      }
		 else if(tipo.equals("PDF")){
		  HttpSession session = request.getSession();
		  nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
		  ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
		  String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
		  String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
		  String diaActual = fechaActual.substring(0,2);
		  String mesActual = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
		  String anioActual = fechaActual.substring(6,10);
		  String horaActual = new SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
		  pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
		  session.getAttribute("iNoNafinElectronico").toString(),
		  (String)session.getAttribute("sesExterno"),
		  (String)session.getAttribute("strNombre"),
		  (String)session.getAttribute("strNombreUsuario"),
		  (String)session.getAttribute("strLogo"),
		  (String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
		  pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual, "formas",ComunesPDF.RIGHT);
		  //pdfDoc.setTable(7,100);
				pdfDoc.setLTable(16, 100);
				pdfDoc.setLCell("No. Docto Final","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Dist.","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de Cr�dito","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Resp. Pago de Inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de Cobranza","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Operaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Ref. Tasa Inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tasa Inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo Cobro Inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setLHeaders();
				while (rs.next()){
					String numeroDocumento = (rs.getString(1)                     == null) ? "" : rs.getString(1);
					String epo             = (rs.getString("NOMBRE_EPO")          == null) ? "" : rs.getString("NOMBRE_EPO");
					String dist            = (rs.getString("NOMBRE_PYME")         == null) ? "" : rs.getString("NOMBRE_PYME");
					String tipoCredito     = (rs.getString("TIPO_CREDITO")        == null) ? "" : rs.getString("TIPO_CREDITO");
					String respInt         = (rs.getString("RESPONSABLE_INTERES") == null) ? "" : rs.getString("RESPONSABLE_INTERES");
					String tipoCob         = (rs.getString("TIPO_COBRANZA")       == null) ? "" : rs.getString("TIPO_COBRANZA");
					String fOpera          = (rs.getString("FECHA_OPERACION")     == null) ? "" : rs.getString("FECHA_OPERACION");
					String moneda          = (rs.getString("MONEDA")              == null) ? "" : rs.getString("MONEDA");
					String monto           = (rs.getString("MONTO")               == null) ? "" : rs.getString("MONTO");
					String refTasa         = (rs.getString("REFERENCIA_TASA")     == null) ? "" : rs.getString("REFERENCIA_TASA");
					String tasaInt         = (rs.getString("TASA_INTERES")        == null) ? "" : rs.getString("TASA_INTERES");
					String plazo           = (rs.getString("PLAZO")               == null) ? "" : rs.getString("PLAZO");
					String fVenc           = (rs.getString("FECHA_VENCIMIENTO")   == null) ? "" : rs.getString("FECHA_VENCIMIENTO");
					String montoInteres    = (rs.getString("MONTO_INTERES")       == null) ? "" : rs.getString("MONTO_INTERES");
					String cobroInt        = (rs.getString("TIPO_COBRO_INTERES")  == null) ? "" : rs.getString("TIPO_COBRO_INTERES");
					String estatus         = (rs.getString("ESTATUS")             == null) ? "" : rs.getString("ESTATUS");
					//INICIO: FODEA 09-2015
					String tipo_pago       = (rs.getString("IG_TIPO_PAGO")        == null) ? "" : rs.getString("IG_TIPO_PAGO");
					if(tipo_pago.equals("2")){
						plazo = plazo + " (M)";
					}
					//FIN: FODEA 09-2015
					pdfDoc.setLCell(numeroDocumento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(epo,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(dist,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipoCredito,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(respInt,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipoCob,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fOpera,"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(monto,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(refTasa,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tasaInt,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(plazo,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fVenc,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(montoInteres,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(cobroInt,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(estatus,"formas",ComunesPDF.CENTER);
					
				}
				//pdfDoc.addTable();
				
				/*CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS();//toma el valor de sesi�n
				Registros regTotales =	queryHelper.getResultCount(request);
				session.setAttribute("_cqhregextjs_totales", regTotales);
				*/
/*
				CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS();//toma el valor de sesi�n
				Registros totales = queryHelper.getResultCount(request);
				
			
				
		if (totales.getNumeroRegistros()>0) {
			int i = 0;
			List vecColumnas = null;
			for(i=0;i<totales.getNumeroRegistros();i++){
				vecColumnas = totales.getData(i);
				pdfDoc.setLCell("TOTAL "+vecColumnas.get(1).toString(),"celda01rep",ComunesPDF.LEFT,3);
				pdfDoc.setLCell(vecColumnas.get(2).toString(),"celda01rep",ComunesPDF.CENTER);
				pdfDoc.setLCell(" ","celda01rep",ComunesPDF.CENTER,4);
				pdfDoc.setLCell(Comunes.formatoDecimal(vecColumnas.get(3).toString(),2),"celda01rep",ComunesPDF.LEFT,8);
		}
	}
*/
	pdfDoc.addLTable();
	pdfDoc.endDocument();
				
				/*
				regTotales.next();
				pdfDoc.setTable(6,100);
				pdfDoc.setCell("TOTALES","celda01",ComunesPDF.CENTER,2);
				pdfDoc.setCell("Total de Registros","celda01",ComunesPDF.CENTER,2);
				pdfDoc.setCell("Total Monto","celda01",ComunesPDF.CENTER,2);
				
				pdfDoc.setCell("TOTALES MONEDA NACIONAL","celda01",ComunesPDF.CENTER,2);
				pdfDoc.setCell(regTotales.getString("COUNT(1)"),"formasrep",ComunesPDF.CENTER,2);
				pdfDoc.setCell("$"+regTotales.getString("SUM(MONTO)"),"formasrep",ComunesPDF.CENTER,2);
			
				regTotales.next();
				
				pdfDoc.setCell("TOTALES DOLARES","celda01",ComunesPDF.CENTER,2);
				pdfDoc.setCell(regTotales.getString("COUNT(1)"),"formasrep",ComunesPDF.CENTER,2);
				pdfDoc.setCell("$"+regTotales.getString("SUM(MONTO)"),"formasrep",ComunesPDF.CENTER,2);
				pdfDoc.addTable();
				
				pdfDoc.endDocument();*/
		 
		 }else if (tipo.equals("TC-PDF")) {
        HttpSession session = request.getSession();
        nombreArchivo = String.valueOf(Comunes.cadenaAleatoria(16)) + ".pdf";
        ComunesPDF pdfDoc = new ComunesPDF(2, String.valueOf(path) + nombreArchivo);

        String[] meses = { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };
        String fechaActual = new SimpleDateFormat("dd/MM/yyyy").format(new Date());
        String diaActual = fechaActual.substring(0, 2);
        String mesActual = meses[(java.lang.Integer.parseInt(fechaActual.substring(3, 5)) - 1)];
        String anioActual = fechaActual.substring(6, 10);
        String horaActual = new SimpleDateFormat("HH:mm:ss").format(new Date());

        pdfDoc.encabezadoConImagenes(pdfDoc, (String)session.getAttribute("strPais"), session.getAttribute("iNoNafinElectronico").toString(), (String)session.getAttribute("sesExterno"), (String)session.getAttribute("strNombre"), (String)session.getAttribute("strNombreUsuario"), (String)session.getAttribute("strLogo"), (String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

        pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual, "formas", ComunesPDF.RIGHT);
        pdfDoc.setLTable(7, 100);
        pdfDoc.setLTable(24, 100);
        pdfDoc.setLCell("No. Docto Final", "celda01", ComunesPDF.CENTER);
        pdfDoc.setLCell("EPO", "celda01", ComunesPDF.CENTER);
        pdfDoc.setLCell("Dist.", "celda01", ComunesPDF.CENTER);
        pdfDoc.setLCell("Tipo de Cr�dito", "celda01", ComunesPDF.CENTER);
        pdfDoc.setLCell("Resp. Pago de Inter�s", "celda01", ComunesPDF.CENTER);
        pdfDoc.setLCell("Tipo de Cobranza", "celda01", ComunesPDF.CENTER);
        pdfDoc.setLCell("Fecha de Operaci�n", "celda01", ComunesPDF.CENTER);
        pdfDoc.setLCell("Moneda", "celda01", ComunesPDF.CENTER);
        pdfDoc.setLCell("Monto", "celda01", ComunesPDF.CENTER);

        pdfDoc.setLCell("% Comisi�n Aplicable\nde Terceros", "celda01", ComunesPDF.CENTER);
        pdfDoc.setLCell("Monto Comisi�n de Terceros (IVA Incluido)", "celda01", ComunesPDF.CENTER);
        pdfDoc.setLCell("Monto a Depositar\npor\nOperaci�n", "celda01", ComunesPDF.CENTER);

        pdfDoc.setLCell("Ref. Tasa Inter�s", "celda01", ComunesPDF.CENTER);
        pdfDoc.setLCell("Tasa Inter�s", "celda01", ComunesPDF.CENTER);
        pdfDoc.setLCell("Plazo", "celda01", ComunesPDF.CENTER);
        pdfDoc.setLCell("Fecha de Vencimiento", "celda01", ComunesPDF.CENTER);
        pdfDoc.setLCell("Monto Inter�s", "celda01", ComunesPDF.CENTER);
        pdfDoc.setLCell("Tipo Cobro Inter�s", "celda01", ComunesPDF.CENTER);
        pdfDoc.setLCell("Estatus", "celda01", ComunesPDF.CENTER);

        pdfDoc.setLCell("Nombre del Producto", "celda01", ComunesPDF.CENTER);
        pdfDoc.setLCell("ID Orden Enviado", "celda01", ComunesPDF.CENTER);
        pdfDoc.setLCell("Respuesta de Operaci�n", "celda01", ComunesPDF.CENTER);
        pdfDoc.setLCell("C�digo Autorizaci�n", "celda01", ComunesPDF.CENTER);
		  pdfDoc.setLCell("N�mero Tarjeta de Cr�dito", "celda01", ComunesPDF.CENTER);

        while (rs.next()) {
          String numeroDocumento = rs.getString(1) == null ? "" : rs.getString(1);
          String epo = rs.getString("NOMBRE_EPO") == null ? "" : rs.getString("NOMBRE_EPO");
          String dist = rs.getString("NOMBRE_PYME") == null ? "" : rs.getString("NOMBRE_PYME");
          String tipoCredito = rs.getString("TIPO_CREDITO") == null ? "" : rs.getString("TIPO_CREDITO");
          String respInt = rs.getString("RESPONSABLE_INTERES") == null ? "" : rs.getString("RESPONSABLE_INTERES");
          String tipoCob = rs.getString("TIPO_COBRANZA") == null ? "" : rs.getString("TIPO_COBRANZA");
          String fOpera = rs.getString("FECHA_OPERACION") == null ? "" : rs.getString("FECHA_OPERACION");
          String moneda = rs.getString("MONEDA") == null ? "" : rs.getString("MONEDA");
          String monto = rs.getString("MONTO") == null ? "" : rs.getString("MONTO");

          String comisiponAplicable = rs.getString("COMISION_APLICABLE") == null ? "" : rs.getString("COMISION_APLICABLE");
          String montoComision = rs.getString("MONTO_COMISION") == null ? "" : rs.getString("MONTO_COMISION");
          String montoDepositar = rs.getString("MONTO_DEPOSITAR") == null ? "" : rs.getString("MONTO_DEPOSITAR");

          String refTasa = rs.getString("REFERENCIA_TASA") == null ? "" : rs.getString("REFERENCIA_TASA");
          String tasaInt = rs.getString("TASA_INTERES") == null ? "" : rs.getString("TASA_INTERES");
          String plazo = rs.getString("PLAZO") == null ? "" : rs.getString("PLAZO");
          String fVenc = rs.getString("FECHA_VENCIMIENTO") == null ? "" : rs.getString("FECHA_VENCIMIENTO");
          String montoInteres = rs.getString("MONTO_INTERES") == null ? "" : rs.getString("MONTO_INTERES");
          String cobroInt = rs.getString("TIPO_COBRO_INTERES") == null ? "" : rs.getString("TIPO_COBRO_INTERES");
          String estatus = rs.getString("ESTATUS") == null ? "" : rs.getString("ESTATUS");

          String bins = rs.getString("BINS") == null ? "" : rs.getString("BINS");
          String idOrdenEnviado = rs.getString("ID_ORDEN_ENVIADO") == null ? "" : rs.getString("ID_ORDEN_ENVIADO");
          String idOperacion = rs.getString("ID_OPERACION") == null ? "" : rs.getString("ID_OPERACION");
          String codigoAutorizacion = rs.getString("CODIGO_AUTORIZACION") == null ? "" : rs.getString("CODIGO_AUTORIZACION");
          String fechaRegistro = rs.getString("FECHA_REGISTRO") == null ? "" : rs.getString("FECHA_REGISTRO");
			 String numTarjeta = rs.getString("NUM_TC") == null ? "" : rs.getString("NUM_TC");

          String operaTC = rs.getString("OPERA_TARJETA") == null ? "" : rs.getString("OPERA_TARJETA");

          if (!"N".equals(operaTC)) {
            refTasa = "N/A";
            tasaInt = "N/A";
            plazo = "N/A";
            fVenc = "N/A";
            montoInteres = "N/A";
          } else {
            bins = "N/A";
            comisiponAplicable = "N/A"; montoComision = "N/A"; montoDepositar = "N/A";
            idOrdenEnviado = "N/A"; idOperacion = "N/A"; codigoAutorizacion = "N/A";
				numTarjeta = "N/A";
          }

          if ("Operada TC".equals(estatus)) {
            pdfDoc.setLCell(numeroDocumento, "formas", ComunesPDF.CENTER);
            pdfDoc.setLCell(epo, "formas", ComunesPDF.CENTER);
            pdfDoc.setLCell(dist, "formas", ComunesPDF.CENTER);
            pdfDoc.setLCell(tipoCredito, "formas", ComunesPDF.CENTER);
            pdfDoc.setLCell(respInt, "formas", ComunesPDF.CENTER);
            pdfDoc.setLCell(tipoCob, "formas", ComunesPDF.CENTER);
            pdfDoc.setLCell(fOpera, "formas", ComunesPDF.RIGHT);
            pdfDoc.setLCell(moneda, "formas", ComunesPDF.CENTER);
            pdfDoc.setLCell("$"+Comunes.formatoDecimal( monto,2), "formas", ComunesPDF.RIGHT);

            pdfDoc.setLCell(Comunes.formatoDecimal(comisiponAplicable,2)+"%", "formas", ComunesPDF.CENTER);
            pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoComision,2), "formas", ComunesPDF.RIGHT);
            pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoDepositar,2), "formas", ComunesPDF.RIGHT);

            pdfDoc.setLCell(refTasa, "formas", ComunesPDF.CENTER);
            pdfDoc.setLCell(tasaInt, "formas", ComunesPDF.CENTER);
            pdfDoc.setLCell(plazo, "formas", ComunesPDF.CENTER);
            pdfDoc.setLCell(fVenc, "formas", ComunesPDF.CENTER);
            pdfDoc.setLCell(montoInteres, "formas", ComunesPDF.CENTER);
            pdfDoc.setLCell(cobroInt, "formas", ComunesPDF.CENTER);
            pdfDoc.setLCell(estatus, "formas", ComunesPDF.CENTER);

            pdfDoc.setLCell(bins, "formas", ComunesPDF.CENTER);
            pdfDoc.setLCell(idOrdenEnviado, "formas", ComunesPDF.CENTER);
            pdfDoc.setLCell(idOperacion, "formas", ComunesPDF.CENTER);
            pdfDoc.setLCell(codigoAutorizacion, "formas", ComunesPDF.CENTER);
				pdfDoc.setLCell((!"".equals(numTarjeta.trim()))?"XXXX-XXXX-XXXX-"+numTarjeta:numTarjeta, "formas", ComunesPDF.CENTER);
          }

        }
/*
        CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS();
        Registros totales = queryHelper.getResultCount(request);

        if (totales != null  && totales.getNumeroRegistros() > 0) {
          int i = 0;
          List vecColumnas = null;
          for (i = 0; i < totales.getNumeroRegistros(); i++) {
				 vecColumnas = totales.getData(i);
				pdfDoc.setLCell("TOTAL " + vecColumnas.get(1).toString(), "celda01rep", ComunesPDF.CENTER,4);
				pdfDoc.setLCell(vecColumnas.get(2).toString(), "celda01rep", ComunesPDF.RIGHT);
				pdfDoc.setLCell("", "celda01rep", ComunesPDF.CENTER,3);
				pdfDoc.setLCell("$"+Comunes.formatoDecimal(vecColumnas.get(3).toString(),2), "celda01rep", ComunesPDF.RIGHT);
				pdfDoc.setLCell("", "celda01rep", ComunesPDF.CENTER,2);
				pdfDoc.setLCell("$"+Comunes.formatoDecimal(vecColumnas.get(4).toString(),2), "celda01rep", ComunesPDF.RIGHT);
				pdfDoc.setLCell("", "celda01rep", ComunesPDF.CENTER,12);
          }

        }
*/
        pdfDoc.addLTable();
        pdfDoc.endDocument();
      }else if(tipo.equals("VAR")){
				String fechaHoy	= "";
				String fechaActual	= "";
				String HoraActual	= "";
				try{
					fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
					fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
					HoraActual	= new java.text.SimpleDateFormat("hh:mm:ss").format(new java.util.Date());
				}catch(Exception e){
					fechaHoy = "";
				}
				int numreg = 0;
				String	contenidoArchVar = "";	
				String	ArchivoVariable = "";
				String contenidoArchiv	= "";
				//String nombreArchivo	= "";	
				String numCredito 			= "";
				String EPO 					= "";
				String distribuidor 		= "";
				String tipoCredito 			= "";
				String respPagoInteres 		= "";
				String tipoCobranza 		= "";
				String fechaOperacion 		= "";
				String moneda 				= "";
				double monto 				= 0;
				String tipoConv 			= "";
				double tipoCambio 			= 0;
				double montoValuado 		= 0;
				String referenciaTasa 		= "";
				String tasaInteres 			= "";
				String plazo 				= "";
				String fechaVencimiento 	= "";
				double montoInteres 		= 0;
				String tipoCobroInteres 	= "";
				String estatus 				= "";
				String tipoMoneda			= "";
				String monedaLinea			= "";
				double montoTotalMN			= 0;
				double montoTotalDolares	= 0;
				double	totalMontoIntMN			= 0;
				double	totalMontoIntUSD		= 0;
				int		genArch = 0;
				int 	imn = 0;
				int		id = 0;
					contenidoArchVar += 
			"\nD,No. Documento final,"+
			"No. Distribuidor,"+
			"Nombre Distribuidor,"+
			"Clave Epo,"+
			"Nombre EPO,"+
			"Fecha de operaci�n,"+
			"Clave de moneda,"+
			"Moneda,"+
			"Monto del cr�dito,"+
			"Plazo cr�dito,"+
			"Fecha vencimiento cr�dito,"+
			"Monto Interes,"+
			"Clave Tasa interes,"+
			"Tasa interes,"+
			"Tipo de conversi�n,"+
			"Monto docto.inicial dls.,"+
			"Tipo de cambio,"+
			"Responsable pago de inter�s,"+
			"Tipo cobranza,"+
			"Referencia,"+
			"CG campo1,"+
			"CG campo2,"+
			"CG campo3,"+
			"CG campo4,"+
			"CG campo5";

	while(rs.next()){
			numreg++;
			genArch ++;
			numCredito 			= 	(rs.getString("ic_documento")==null)?"":(rs.getString("ic_documento"));
			EPO		 			= 	(rs.getString("NOMBRE_EPO")==null)?"":(rs.getString("NOMBRE_EPO"));
			distribuidor		= 	(rs.getString("NOMBRE_PYME")==null)?"":(rs.getString("NOMBRE_PYME"));
			tipoCredito			= 	(rs.getString("TIPO_CREDITO")==null)?"":(rs.getString("TIPO_CREDITO"));
			respPagoInteres		= 	(rs.getString("RESPONSABLE_INTERES")==null)?"":(rs.getString("RESPONSABLE_INTERES"));
			tipoCobranza		= 	(rs.getString("TIPO_COBRANZA")==null)?"":(rs.getString("TIPO_COBRANZA"));
			fechaOperacion		= 	(rs.getString("FECHA_OPERACION")==null)?"":(rs.getString("FECHA_OPERACION"));
			moneda				= 	(rs.getString("MONEDA")==null)?"":(rs.getString("MONEDA"));
			monto		 		= 	rs.getDouble("MONTO");
			tipoConv			= 	(rs.getString("TIPO_CONVERSION")==null)?"":(rs.getString("TIPO_CONVERSION"));
			tipoCambio 			= 	rs.getDouble("TIPO_CAMBIO");
			montoValuado		= 	monto * tipoCambio;
			referenciaTasa		= 	(rs.getString("REFERENCIA_TASA")==null)?"":(rs.getString("REFERENCIA_TASA"));
			tasaInteres			= 	(rs.getString("TASA_INTERES")==null)?"":(rs.getString("TASA_INTERES"));
			plazo	 			= 	(rs.getString("PLAZO")==null)?"":(rs.getString("PLAZO"));
			fechaVencimiento	= 	(rs.getString("FECHA_VENCIMIENTO")==null)?"":(rs.getString("FECHA_VENCIMIENTO"));
			montoInteres		= 	rs.getDouble("MONTO_INTERES");
			tipoCobroInteres	= 	(rs.getString("TIPO_COBRO_INTERES")==null)?"":(rs.getString("TIPO_COBRO_INTERES"));
			estatus	 			= 	(rs.getString("ESTATUS")==null)?"":(rs.getString("ESTATUS"));
			tipoMoneda 			= 	(rs.getString("ic_moneda")==null)?"":(rs.getString("ic_moneda"));
			monedaLinea 		= 	(rs.getString("moneda_linea")==null)?"":(rs.getString("moneda_linea"));
			String rs_numDist	=	rs.getString("numdist")==null?"":rs.getString("numdist");
			String rs_cveTasa	=	rs.getString("cve_tasa")==null?"":rs.getString("cve_tasa");
			String rs_tipoConver=	rs.getString("tipo_conver")==null?"":rs.getString("tipo_conver");
			String rs_tipoCamb	=	rs.getString("tipo_cambio")==null?"":rs.getString("tipo_cambio");
			String rs_respInt	=	rs.getString("resp_interes")==null?"":rs.getString("resp_interes");
			String rs_tipoCob	=	rs.getString("rs_tipo_cobranza")==null?"":rs.getString("rs_tipo_cobranza");
			String rs_ic_epo	=	rs.getString("ic_epo")==null?"":rs.getString("ic_epo");
			String 	rs_referencia 	=	rs.getString("ct_referencia")==null?"":rs.getString("ct_referencia");
			String 	rs_campo1		=	rs.getString("cg_campo1")==null?"":rs.getString("cg_campo1");
			String 	rs_campo2		=	rs.getString("cg_campo2")==null?"":rs.getString("cg_campo2");
			String 	rs_campo3		=	rs.getString("cg_campo3")==null?"":rs.getString("cg_campo3");
			String 	rs_campo4		=	rs.getString("cg_campo4")==null?"":rs.getString("cg_campo4");
			String 	rs_campo5 		=	rs.getString("cg_campo5")==null?"":rs.getString("cg_campo5");
			//INICIO: FODEA 09-2015
			String tipo_pago           = (rs.getString("IG_TIPO_PAGO") == null) ? "" : rs.getString("IG_TIPO_PAGO");
			if(tipo_pago.equals("2")){
				plazo = plazo + " (M)";
			}
	   //FIN: FODEA 09-2015
			contenidoArchVar +="\nD,"+
				numCredito.trim()+","+
				rs_numDist.trim()+","+
				distribuidor.replace(',',' ')+","+
				rs_ic_epo.trim()+","+
				EPO.replace(',',' ')+","+
				fechaOperacion.trim()+","+
				tipoMoneda.trim()+","+
				moneda.trim()+","+
				Comunes.formatoDecimal(monto,2,false)+","+
				plazo.trim()+","+
				fechaVencimiento.trim()+","+
				Comunes.formatoDecimal(montoInteres,2,false)+","+
				rs_cveTasa.trim()+","+
				Comunes.formatoDecimal(tasaInteres,2,false)+",";
			if(!"1".equals(rs_tipoCamb.trim())){
				contenidoArchVar +=
					rs_tipoConver.trim()+","+
					Comunes.formatoDecimal(montoValuado,2,false)+","+
					rs_tipoCamb.trim()+",";
			}
			else {
				contenidoArchVar +=
					"N,0.00,0.00,";
			}
			contenidoArchVar +=
				rs_respInt.trim()+","+
				rs_tipoCob.trim()+","+
				rs_referencia.trim()+","+
				rs_campo1.trim()+","+
				rs_campo2.trim()+","+
				rs_campo3.trim()+","+
				rs_campo4.trim()+","+
				rs_campo5.trim();
			if(tipoMoneda.equals("1")){
				montoTotalMN = montoTotalMN + monto;
				totalMontoIntMN		+= montoInteres;
				imn++;
			}else if(tipoMoneda.equals("54")){
				montoTotalDolares = montoTotalDolares + monto;
				totalMontoIntUSD		+= montoInteres;
				id++;
			}
	}//while

	ArchivoVariable = 
		"H," +fechaActual+","+HoraActual+",Consulta Solicitudes" +
		contenidoArchVar+
		"\nT,Total registros M.N.,"+imn+
		",Monto Total Cr�ditos M.N.,,,,,,"+Comunes.formatoDecimal(montoTotalMN,2,false)+
		",Total Monto Inter�s M.N.,,"+Comunes.formatoDecimal(totalMontoIntMN,2,false)+"\n"+
		",Total registros D.L.,"+id+
		",Monto Total Cr�ditos D.L.,,,,,,"+Comunes.formatoDecimal(montoTotalDolares,2,false)+
		",Total Monto Inter�s D.L.,,"+Comunes.formatoDecimal(totalMontoIntUSD,2,false);	

		CreaArchivo archivo = new CreaArchivo();		
		archivo.make(ArchivoVariable, path, ".csv");
      nombreArchivo = archivo.nombre;

		 }else if(tipo.equals("FIJO")){
			String fechaHoy	= "";
			String fechaActual	= "";
			String HoraActual	= "";
			try{
				fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				HoraActual	= new java.text.SimpleDateFormat("hh:mm:ss").format(new java.util.Date());
			}catch(Exception e){
				fechaHoy = "";
			}
			String contenidoArchiv	= "";
			String sinregistro ="";
			String		contenidoArchFijo = "";
			String		ArchivoFijo = "";
			String numCredito 			= "";
			String EPO 					= "";
			String distribuidor 		= "";
			String tipoCredito 			= "";
			String respPagoInteres 		= "";
			String tipoCobranza 		= "";
			String fechaOperacion 		= "";
			String moneda 				= "";
			double monto 				= 0;
			String tipoConv 			= "";
			double tipoCambio 			= 0;
			double montoValuado 		= 0;
			String referenciaTasa 		= "";
			String tasaInteres 			= "";
			String plazo 				= "";
			String fechaVencimiento 	= "";
			double montoInteres 		= 0;
			String tipoCobroInteres 	= "";
			String estatus 				= "";
			String tipoMoneda			= "";
			String monedaLinea			= "";
			double montoTotalMN			= 0;
			double montoTotalDolares	= 0;
			double	totalMontoIntMN			= 0;
			double	totalMontoIntUSD		= 0;
			int		genArch = 0;
			int 	imn = 0;
			int		id = 0;
			int numreg=0;
			
			while(rs.next()){
			numreg++;
			genArch ++;
			numCredito 			= 	(rs.getString("ic_documento")==null)?"":(rs.getString("ic_documento"));
			EPO		 			= 	(rs.getString("NOMBRE_EPO")==null)?"":(rs.getString("NOMBRE_EPO"));
			distribuidor		= 	(rs.getString("NOMBRE_PYME")==null)?"":(rs.getString("NOMBRE_PYME"));
			tipoCredito			= 	(rs.getString("TIPO_CREDITO")==null)?"":(rs.getString("TIPO_CREDITO"));
			respPagoInteres		= 	(rs.getString("RESPONSABLE_INTERES")==null)?"":(rs.getString("RESPONSABLE_INTERES"));
			tipoCobranza		= 	(rs.getString("TIPO_COBRANZA")==null)?"":(rs.getString("TIPO_COBRANZA"));
			fechaOperacion		= 	(rs.getString("FECHA_OPERACION")==null)?"":(rs.getString("FECHA_OPERACION"));
			moneda				= 	(rs.getString("MONEDA")==null)?"":(rs.getString("MONEDA"));
			monto		 		= 	rs.getDouble("MONTO");
			tipoConv			= 	(rs.getString("TIPO_CONVERSION")==null)?"":(rs.getString("TIPO_CONVERSION"));
			tipoCambio 			= 	rs.getDouble("TIPO_CAMBIO");
			montoValuado		= 	monto * tipoCambio;
			referenciaTasa		= 	(rs.getString("REFERENCIA_TASA")==null)?"":(rs.getString("REFERENCIA_TASA"));
			tasaInteres			= 	(rs.getString("TASA_INTERES")==null)?"":(rs.getString("TASA_INTERES"));
			plazo	 			= 	(rs.getString("PLAZO")==null)?"":(rs.getString("PLAZO"));
			fechaVencimiento	= 	(rs.getString("FECHA_VENCIMIENTO")==null)?"":(rs.getString("FECHA_VENCIMIENTO"));
			montoInteres		= 	rs.getDouble("MONTO_INTERES");
			tipoCobroInteres	= 	(rs.getString("TIPO_COBRO_INTERES")==null)?"":(rs.getString("TIPO_COBRO_INTERES"));
			estatus	 			= 	(rs.getString("ESTATUS")==null)?"":(rs.getString("ESTATUS"));
			tipoMoneda 			= 	(rs.getString("ic_moneda")==null)?"":(rs.getString("ic_moneda"));
			monedaLinea 		= 	(rs.getString("moneda_linea")==null)?"":(rs.getString("moneda_linea"));

			String rs_numDist    = rs.getString("numdist")==null?"":rs.getString("numdist");
			String rs_cveTasa    = rs.getString("cve_tasa")==null?"":rs.getString("cve_tasa");
			String rs_tipoConver = rs.getString("tipo_conver")==null?"":rs.getString("tipo_conver");
			String rs_tipoCamb   = rs.getString("tipo_cambio")==null?"":rs.getString("tipo_cambio");
			String rs_respInt    = rs.getString("resp_interes")==null?"":rs.getString("resp_interes");
			String rs_tipoCob    = rs.getString("rs_tipo_cobranza")==null?"":rs.getString("rs_tipo_cobranza");
			String rs_ic_epo     = rs.getString("ic_epo")          ==null?"":rs.getString("ic_epo");
			String rs_referencia = rs.getString("ct_referencia")   ==null?"":rs.getString("ct_referencia");
			String rs_campo1     = rs.getString("cg_campo1")==null?"":rs.getString("cg_campo1");
			String rs_campo2     = rs.getString("cg_campo2")==null?"":rs.getString("cg_campo2");
			String rs_campo3     = rs.getString("cg_campo3")==null?"":rs.getString("cg_campo3");
			String rs_campo4     = rs.getString("cg_campo4")==null?"":rs.getString("cg_campo4");
			String rs_campo5     = rs.getString("cg_campo5")==null?"":rs.getString("cg_campo5");			
			String tipo_pago     = rs.getString("IG_TIPO_PAGO") == null? "" : rs.getString("IG_TIPO_PAGO"); //FODEA 09-2015

			contenidoArchFijo+="\nD"+
				Comunes.formatoFijo(Comunes.formatoDecimal(numCredito,0,false),10,"0","") + 
				Comunes.formatoFijo(rs_numDist,25," ","A") +
				Comunes.formatoFijo(distribuidor.replace(',',' '),100," ","A") +
				Comunes.formatoFijo(Comunes.formatoDecimal(rs_ic_epo,0,false),3,"0","") + 
				Comunes.formatoFijo(EPO.replace(',',' '),100," ","A") +
				Comunes.formatoFijo(fechaOperacion,10," ","") + 
				Comunes.formatoFijo(Comunes.formatoDecimal(tipoMoneda,0,false),3,"0","") + 
				Comunes.formatoFijo(moneda,30," ","A") +
				Comunes.formatoFijo(Comunes.formatoDecimal(monto,2,false),15,"0","") + 
				Comunes.formatoFijo(Comunes.formatoDecimal(plazo,0,false),3,"0","") + 
				//FODEA 09-2015. Si es a meses sin intereses (tipo_pago = 2) se agrega la letra M, en otro caso, se queda como est�.
				Comunes.formatoFijo(tipo_pago.equals("2")?Comunes.formatoDecimal(plazo,0,false) + "(M)":Comunes.formatoDecimal(plazo,0,false),8,"0","") + //FODEA 09-2015
				Comunes.formatoFijo(fechaVencimiento,10," ","") + 
				Comunes.formatoFijo(Comunes.formatoDecimal(montoInteres,2,false),15,"0","") + 
				Comunes.formatoFijo(Comunes.formatoDecimal(rs_cveTasa,0,false),3,"0","") + 
				Comunes.formatoFijo(Comunes.formatoDecimal(tasaInteres,5,false),8,"0","");
			if(!"1".equals(rs_tipoCamb.trim())){
			contenidoArchFijo+=
				Comunes.formatoFijo(rs_tipoConver,1," ","A") +
				Comunes.formatoFijo(Comunes.formatoDecimal(montoValuado,2,false),15,"0","") + 
				Comunes.formatoFijo(Comunes.formatoDecimal(rs_tipoCamb,2,false),15,"0","");
			}
			else {
			contenidoArchFijo+=
				Comunes.formatoFijo("N",1," ","A") +
				Comunes.formatoFijo(Comunes.formatoDecimal("0",2,false),15,"0","") + 
				Comunes.formatoFijo(Comunes.formatoDecimal("0",2,false),15,"0","");
			}
			contenidoArchFijo+=
				Comunes.formatoFijo(rs_respInt,1," ","A") +
				Comunes.formatoFijo(rs_tipoCob,1," ","A")+
				Comunes.formatoFijo(rs_referencia,100," ","A") +
				Comunes.formatoFijo(rs_campo1,100," ","A") +
				Comunes.formatoFijo(rs_campo2,100," ","A") +
				Comunes.formatoFijo(rs_campo3,100," ","A") +
				Comunes.formatoFijo(rs_campo4,100," ","A") +
				Comunes.formatoFijo(rs_campo5,100," ","A")+".";
				
				
				
			if(tipoMoneda.equals("1")){
				montoTotalMN = montoTotalMN + monto;
				totalMontoIntMN		+= montoInteres;
				imn++;
			}else if(tipoMoneda.equals("54")){
				montoTotalDolares = montoTotalDolares + monto;
				totalMontoIntUSD		+= montoInteres;
				id++;
			}
	}//while

	ArchivoFijo = 
		"H" +fechaActual+HoraActual+"  Documentos por Autorizar" +
		contenidoArchFijo+
		"\nT"+
		Comunes.formatoFijo(Comunes.formatoDecimal(imn,0,false),14,"0","") + 
		Comunes.formatoFijo(Comunes.formatoDecimal(montoTotalMN,2,false),15,"0","") + 
		Comunes.formatoFijo(Comunes.formatoDecimal(totalMontoIntMN,2,false),15,"0","") + 
		Comunes.formatoFijo(Comunes.formatoDecimal(id,0,false),14,"0","") + 
		Comunes.formatoFijo(Comunes.formatoDecimal(montoTotalDolares,2,false),15,"0","") + 
		Comunes.formatoFijo(Comunes.formatoDecimal(totalMontoIntUSD,2,false),15,"0","");



       CreaArchivo archivo = new CreaArchivo();		
		archivo.make(ArchivoFijo, path, ".txt");
      nombreArchivo = archivo.nombre;
		 
		 }
		 
		}catch (Throwable e) {
			e.printStackTrace();
			throw new AppException("Error al generar el archivo",e);
		}
		finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
		log.info("InfSolicIfDist::crearCustomFile(S)");
		return nombreArchivo;
	}
	
	
	public String getDocumentQuery() {
		log.info("InfSolicIfDist::getDocumentQuery(E)");
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer qryDM			  = new StringBuffer();
		StringBuffer qryCCC		    = new StringBuffer();
		StringBuffer qryFF			  = new StringBuffer();
		StringBuffer condicion    = new StringBuffer();
		conditions 		= new ArrayList();
		try {
		
		conditions.add(sesIf);

		if(!"".equals(icEpo)){
			condicion.append(" and D.ic_epo = ? ");
			conditions.add(icEpo);
		}
		if(!"".equals(icDist)){
			condicion.append(" and D.ic_pyme = ? ");
			conditions.add(icDist);
		}
		if(!"".equals(numCred)){
			condicion.append(" and D.ic_documento = ? ");
			conditions.add(numCred);
		}
//Agregada ic_moneda
		if(!"".equals(cbMoneda)){
			condicion.append(" and D.ic_moneda = ? ");
			conditions.add(cbMoneda);
		}
		
//Agregada ic_moneda		
		if("D".equals(ctCredito)){
			if(!"".equals(tipoCoInt)){ 
				condicion.append(" and s.ic_tipo_cobro_interes = ? ");
				conditions.add(tipoCoInt);
			}
		}else  {
			if(!"".equals(tipoCoInt)){
				condicion.append(" and PE.ic_tipo_cobro_interes = ? ");
				conditions.add(tipoCoInt);
			}
		}
		if(!"".equals(estatus)){
			condicion.append(" and S.ic_estatus_solic = ? ");
			conditions.add(estatus);
		}
		if(!"".equals(monto1)&&!"".equals(monto2)){
			condicion.append(" and (D.fn_monto between ? and ?)");
			conditions.add(monto1);conditions.add(monto2);
		}
		if(!"".equals(fechaOper1)&&!"".equals(fechaOper2)){
			condicion.append(" and trunc(DS.df_fecha_seleccion) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");
			conditions.add(fechaOper1);conditions.add(fechaOper2);
		}
		if(!"".equals(fechaVenc1)&&!"".equals(fechaVenc2)){
			condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");
			conditions.add(fechaVenc1);conditions.add(fechaVenc2);
		}
		
		if(!"".equals(ctCredito)){
			condicion.append(" and  PEP.cg_tipo_credito = ? ");
			conditions.add(ctCredito);
		}
		if(!"".equals(tipoPago)){
			condicion.append(" and  D.IG_TIPO_PAGO = ? ");
			conditions.add(tipoPago);
		}
		qryDM.append(
			"   select /*+ use_nl(d,ds,s,lc,cpe,pep,pe,es)"   +
			"       index(d cp_dis_documento_pk)"   +
			"       index(ds)*/"   +
			"   D.ic_documento"   +
			"   FROM dis_documento D"   +
			"  	,dis_docto_seleccionado DS"   +
			"  	,dis_solicitud S"   +
			"  	,dis_linea_credito_dm LC"   +
			"  	,comrel_pyme_epo cpe"   +
			"  	,comrel_pyme_epo_x_producto PEP"   +
			"  	,comrel_producto_epo PE"   +
			"  	,comcat_epo epo"   +
			"  	,comcat_estatus_solic ES"   +
			"		,comcat_tipo_cobro_interes tci	"+//////////////////////////////////////////////////////
			"   WHERE D.ic_documento = DS.ic_documento"   +
			"  	and D.ic_linea_credito_dm = LC.ic_linea_credito_dm"   +
			" 	AND D.ic_epo = cpe.ic_epo"   +
			" 	AND cpe.ic_epo = epo.ic_epo"   +
			"   	AND D.ic_pyme = cpe.ic_pyme"   +
			"  	AND D.ic_producto_nafin = PEP.ic_producto_nafin"   +
			"  	AND D.ic_producto_nafin = PE.ic_producto_nafin"   +
			"  	and D.ic_epo = PEP.ic_epo"   +
			"  	and D.ic_pyme = PEP.ic_pyme"   +
			"  	and D.ic_epo = PE.ic_epo"   +
			"  	and DS.ic_documento = S.ic_documento"   +
			"  	and S.ic_estatus_solic = ES.ic_estatus_solic"   +
			"		AND s.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes	"+/////////////////////////////////
			"  	and PEP.ic_producto_nafin = 4"   +
			"  	and epo.cs_habilitado = 'S'"   +
			"   and lc.ic_moneda=d.ic_moneda" +
			"	and LC.ic_if = ? " +
			condicion.toString()  );
				
		qryCCC.append(
			"  select /*+ use_nl(d,ds,s,lc,cpe,pep,pe,es)"   +
			"      index(d cp_dis_documento_pk)"   +
			"      index(ds)"   +
			"      index(s)*/"   +
			"  d.ic_documento"   +
			"  FROM dis_documento D"   +
			"  ,dis_docto_seleccionado DS"   +
			"  ,dis_solicitud S"   +
			"  ,com_linea_credito LC"   +
			"  ,comrel_pyme_epo cpe"   +
			"  ,comrel_pyme_epo_x_producto PEP"   +
			"  ,comrel_producto_epo PE"   +
			"  ,comcat_epo epo"   +
			"  ,comcat_estatus_solic ES"   +
			"  ,comcat_tipo_cobro_interes TCI"   +
			"  ,com_bins_if BINS"   +//FODEA-013-2014
			"  WHERE D.ic_documento = DS.ic_documento"   +
			"	AND d.ic_bins = bins.ic_bins(+)	"+
			"  and D.ic_linea_credito = LC.ic_linea_credito"   +
			"  AND D.ic_epo = cpe.ic_epo"   +
			"  AND cpe.ic_epo = epo.ic_epo"   +
			"  AND D.ic_pyme = cpe.ic_pyme"   +
			"  AND D.ic_producto_nafin = PEP.ic_producto_nafin"   +
			"  AND D.ic_producto_nafin = PE.ic_producto_nafin"   +
			"  and D.ic_epo = PEP.ic_epo"   +
			"  and D.ic_pyme = PEP.ic_pyme"   +
			"  and D.ic_epo = PE.ic_epo"   +
			"  and DS.ic_documento = S.ic_documento"   +
			"  and S.ic_estatus_solic = ES.ic_estatus_solic"   +
			"  and PE.ic_tipo_cobro_interes = TCI.ic_tipo_cobro_interes"   +
			"  and PEP.ic_producto_nafin = 4"   +
			"  and epo.cs_habilitado = 'S' "   +
			"  and lc.cs_factoraje_con_rec = 'N' " +
			"  and lc.ic_moneda=d.ic_moneda" +
			"  and (LC.ic_if = ? or bins.ic_if ="+ sesIf+" )" +
			
	  		condicion.toString());

		qryFF.append(
			"  select /*+ use_nl(d,ds,s,lc,cpe,pep,pe,es)"   +
			"      index(d cp_dis_documento_pk)"   +
			"      index(ds)"   +
			"      index(s)*/"   +
			"  d.ic_documento"   +
			"  FROM dis_documento D"   +
			"  ,dis_docto_seleccionado DS"   +
			"  ,dis_solicitud S"   +
			"  ,com_linea_credito LC"   +
			"  ,comrel_pyme_epo cpe"   +
			"  ,comrel_pyme_epo_x_producto PEP"   +
			"  ,comrel_producto_epo PE"   +
			"  ,comcat_epo epo"   +
			"  ,comcat_estatus_solic ES"   +
			"  ,comcat_tipo_cobro_interes TCI"   +
			"  WHERE D.ic_documento = DS.ic_documento"   +
			"  and D.ic_linea_credito = LC.ic_linea_credito"   +
			"  AND D.ic_epo = cpe.ic_epo"   +
			"  AND cpe.ic_epo = epo.ic_epo"   +
			"  AND D.ic_pyme = cpe.ic_pyme"   +
			"  AND D.ic_producto_nafin = PEP.ic_producto_nafin"   +
			"  AND D.ic_producto_nafin = PE.ic_producto_nafin"   +
			"  and D.ic_epo = PEP.ic_epo"   +
			"  and D.ic_pyme = PEP.ic_pyme"   +
			"  and D.ic_epo = PE.ic_epo"   +
			"  and DS.ic_documento = S.ic_documento"   +
			"  and S.ic_estatus_solic = ES.ic_estatus_solic"   +
			"  and PE.ic_tipo_cobro_interes = TCI.ic_tipo_cobro_interes"   +
			"  and PEP.ic_producto_nafin = 4"   +
			"  and epo.cs_habilitado = 'S' "   +
			"  and lc.cs_factoraje_con_rec = 'S' " +
			"  and lc.ic_moneda=d.ic_moneda" +
			"  and LC.ic_if = ? " +
	  		condicion.toString());

			if("D".equals(ctCredito)){
				this.numList = 1;
				qrySentencia.append(qryDM.toString());
			} else if("C".equals(ctCredito)){
				this.numList = 1;
				qrySentencia.append(qryCCC.toString());
			} else if("F".equals(ctCredito)){
				this.numList = 1;
				qrySentencia.append(qryFF.toString());
			} else {
				this.numList = 1;
				qrySentencia.append(qryDM.toString()+ " UNION ALL "+qryCCC.toString());
				conditions.addAll(conditions);
			}


			log.debug("qrySentencia: "+qrySentencia.toString());
			log.debug("conditions: "+conditions);
		}catch(Exception e){
			log.error("InfSolicIfDist::getDocumentQueryException "+e);
		}
		log.info("InfSolicIfDist::getDocumentQuery(S)");
		return qrySentencia.toString();
	}
	public String getAggregateCalculationQuery(HttpServletRequest request) {
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer qryDM			  = new StringBuffer();
		StringBuffer qryCCC		    = new StringBuffer();
		StringBuffer condicion    = new StringBuffer();
		
		String	ses_ic_if           = request.getSession().getAttribute("iNoCliente").toString();
		String	tipo_credito 		= (request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String 	fecha_vto_de		= (request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String 	fecha_vto_a			= (request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String	ic_pyme				= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String	ic_epo				= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String	ic_estatus_docto	= (request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto");
		String	ic_documento		= (request.getParameter("ic_documento")==null)?"":request.getParameter("ic_documento");
		String 	fecha_seleccion_de	= (request.getParameter("fecha_seleccion_de")==null)?"":request.getParameter("fecha_seleccion_de");
		String 	fecha_seleccion_a	= (request.getParameter("fecha_seleccion_a")==null)?"":request.getParameter("fecha_seleccion_a");
		String 	monto_credito_de	= (request.getParameter("monto_credito_de")==null)?"":request.getParameter("monto_credito_de");
		String 	monto_credito_a		= (request.getParameter("monto_credito_a")==null)?"":request.getParameter("monto_credito_a");
		String 	ic_tipo_cobro_int	= (request.getParameter("ic_tipo_cobro_int")==null)?"":request.getParameter("ic_tipo_cobro_int");
		String 	fn_monto_de			= (request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String 	fn_monto_a			= (request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String 	ig_numero_docto		= (request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String 	cc_acuse			= (request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String 	fecha_emision_de	= (request.getParameter("fecha_emision_de")==null)?"":request.getParameter("fecha_emision_de");
		String 	fecha_emision_a		= (request.getParameter("fecha_emision_a")==null)?"":request.getParameter("fecha_emision_a");
		String	ic_moneda			= (request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String 	monto_con_descuento	= (request.getParameter("monto_con_descuento")==null)?"":"checked";
		String	solo_cambio			= (request.getParameter("solo_cambio")==null)?"":"checked";
		String	modo_plazo			= (request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
			
		try {

		if(!"".equals(ic_epo))
			condicion.append(" and D.ic_epo = ? ");
		if(!"".equals(ic_pyme))
			condicion.append(" and D.ic_pyme = ? ");
		if(!"".equals(ic_documento))
			condicion.append(" and D.ic_documento = ? ");
//Agregada ic_moneda
		if(!"".equals(ic_moneda))
			condicion.append(" and D.ic_moneda = ? ");
//Agregada ic_moneda						
		if(!"".equals(ic_tipo_cobro_int))
			condicion.append(" and PE.ic_tipo_cobro_interes = ? ");
		if(!"".equals(ic_estatus_docto))
			condicion.append(" and S.ic_estatus_solic = ? ");
		if(!"".equals(monto_credito_de)&&!"".equals(monto_credito_a))
			condicion.append(" and (D.fn_monto between ? and ?)");
		if(!"".equals(fecha_seleccion_de)&&!"".equals(fecha_seleccion_a))
			condicion.append(" and trunc(DS.df_fecha_seleccion) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");
		if(!"".equals(fecha_vto_de)&&!"".equals(fecha_vto_a))
			condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");

			qryDM.append(
				"   select /*+ use_nl(d ds s lc cpe pep pe epo m es)"   +
				"     index(d cp_dis_documento_pk)"   +
				"     index(ds)"   +
				"     index(s)"   +
				"     index(lc cp_dis_linea_credito_dm_pk)*/"   +
				"   	 M.ic_moneda "   +
				"  	 ,M.cd_nombre"   +
				"  	,DS.fn_importe_recibir as MONTO"   +
				"   FROM dis_documento D"   +
				"  	,dis_docto_seleccionado DS"   +
				"  	,dis_solicitud S"   +
				"  	,dis_linea_credito_dm LC"   +
				"     ,comrel_pyme_epo cpe"   +
				"  	,comrel_pyme_epo_x_producto PEP"   +
				"  	,comrel_producto_epo PE"   +
				"  	,comcat_epo epo"   +
				"  	,comcat_moneda M"   +
				"  	,comcat_estatus_solic ES"   +
				"   WHERE D.ic_documento = DS.ic_documento"   +
				"  	and D.ic_linea_credito_dm = LC.ic_linea_credito_dm"   +
				"     AND D.ic_epo = cpe.ic_epo"   +
				"     AND D.ic_pyme = cpe.ic_pyme"   +
				"  	AND D.ic_producto_nafin = PEP.ic_producto_nafin"   +
				"  	AND D.ic_producto_nafin = PE.ic_producto_nafin"   +
				"  	and D.ic_epo = PEP.ic_epo"   +
				"  	and pep.ic_epo = epo.ic_epo"   +
				"  	and D.ic_pyme = PEP.ic_pyme"   +
				"  	and D.ic_epo = PE.ic_epo"   +
				"  	and DS.ic_documento = S.ic_documento"   +
				"  	and S.ic_estatus_solic = ES.ic_estatus_solic"   +
				"  	and LC.ic_moneda = M.ic_moneda"   +
				"  	and PEP.ic_producto_nafin = 4"   +
				"  	and epo.cs_habilitado = 'S' "   +
				"   and lc.ic_moneda=d.ic_moneda" +
				"	and LC.ic_if = ? " +
				condicion.toString()  );
				
		qryCCC.append(
				"  select /*+ use_nl(d,ds,s,lc,cpe,pep,pe,m,es) "   +
				"       index(d cp_dis_documento_pk)"   +
				"       index(ds)"   +
				"       index(lc cp_com_linea_credito_pk)*/"   +
				"       M.ic_moneda "   +
				"  	 ,M.cd_nombre"   +
				"  	,DS.fn_importe_recibir as MONTO"   +
				"  FROM dis_documento D"   +
				"  ,dis_docto_seleccionado DS"   +
				"  ,dis_solicitud S"   +
				"  ,com_linea_credito LC"   +
				"  ,comrel_pyme_epo cpe"   +
				"  ,comrel_pyme_epo_x_producto PEP"   +
				"  ,comrel_producto_epo PE"   +
				"  ,comcat_epo epo"   +
				"  ,comcat_moneda M"   +
				"  ,comcat_estatus_solic ES"   +
				"  WHERE D.ic_documento = DS.ic_documento"   +
				"  and D.ic_linea_credito = LC.ic_linea_credito"   +
				"  AND D.ic_epo = cpe.ic_epo"   +
				"  AND D.ic_pyme = cpe.ic_pyme"   +
				"  AND D.ic_producto_nafin = PEP.ic_producto_nafin"   +
				"  AND D.ic_producto_nafin = PE.ic_producto_nafin"   +
				"  and D.ic_epo = PEP.ic_epo"   +
				"  and pep.ic_epo = epo.ic_epo"   +
				"  and D.ic_pyme = PEP.ic_pyme"   +
				"  and D.ic_epo = PE.ic_epo"   +
				"  and DS.ic_documento = S.ic_documento"   +
				"  and S.ic_estatus_solic = ES.ic_estatus_solic"   +
				"  and LC.ic_moneda = M.ic_moneda"   +
				"  and PEP.ic_producto_nafin = 4"   +
				"  and epo.cs_habilitado = 'S' "   +
				"  and lc.ic_moneda=d.ic_moneda" +
				"  and LC.ic_if = ? " +
				condicion.toString());

			String qryAux = "";	
			if("D".equals(tipo_credito)){
				this.numList = 1;
				qryAux = qryDM.toString();
			} else if("C".equals(tipo_credito)){
				this.numList = 1;
				qryAux = qryCCC.toString();
			} else {
				this.numList = 1;
				qryAux = qryDM.toString()+ " UNION ALL "+qryCCC.toString();
			}

			qrySentencia.append(
				"SELECT ic_moneda, cd_nombre, COUNT (1), SUM (monto), 'InfSolicIfDist::getAggregateCalculationQuery'"+
				"  FROM ("+qryAux.toString()+")"+
				"GROUP BY  ic_moneda,cd_nombre ORDER BY ic_moneda ");
				

		}catch(Exception e) {
			log.error("InfSolicIfDist::getAggregateCalculationQuery "+e);
		}
		return qrySentencia.toString();
	}

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids) {
		int i=0;
		StringBuffer qrySentencia	= new StringBuffer();
    	StringBuffer qryDM			= new StringBuffer();
    	StringBuffer qryCCC			= new StringBuffer();
    	StringBuffer clavesDocumentos = new StringBuffer();
    	
		String	ses_ic_if       = request.getSession().getAttribute("iNoCliente").toString();
		String	tipo_credito 	= (request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
    
		for(int x = 0;x<ids.size();x++ ) {
          clavesDocumentos.append("?,");
      	}
		clavesDocumentos = clavesDocumentos.delete(clavesDocumentos.length()-1, clavesDocumentos.length());

		qryDM.append(
			"  select /*+ use_nl(d,ds,s,lc,cpe,pep,pn,pe,t,e,p,m,es,tci) "   +
			"     index(pe cp_comrel_producto_epo_pk)"   +
			"     index(tci cp_comcat_tipo_cobro_inter_pk)*/"   +
			"     D.ic_documento"   +
			" 	,E.cg_razon_social as NOMBRE_EPO"   +
			" 	,P.cg_razon_social as NOMBRE_PYME"   +
			" 	,decode(PEP.cg_tipo_credito, 'D', 'Modalidad 1 (Riesgo Empresa de Primer Orden )', 'C', 'Modalidad 2 (Riesgo Distribuidor)') as TIPO_CREDITO"   +
			" 	,decode(nvl(PE.cg_responsable_interes, PN.cg_responsable_interes), 'E', 'EPO', 'D', 'Distribuidor') as RESPONSABLE_INTERES"   +
			"  ,decode(nvl(PE.cg_tipo_cobranza, PN.cg_tipo_cobranza), 'D', 'Delegada', 'N', 'No Delegada') as TIPO_COBRANZA"   +
			" 	,to_char(DS.df_fecha_seleccion, 'dd/mm/yyyy') as FECHA_OPERACION"   +
			" 	,M.cd_nombre as MONEDA"   +
			" 	,DS.fn_importe_recibir as MONTO"   +
			" 	,decode(nvl(PE.cg_tipo_conversion, PN.cg_tipo_conversion), 'P', 'Dolar-Peso','') as TIPO_CONVERSION"   +
			" 	,D.fn_tipo_cambio as TIPO_CAMBIO"   +
			"	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',T.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) AS REFERENCIA_TASA" +
//			" 	,decode(DS.cg_rel_mat, '+', DS.fn_valor_tasa + DS.fn_puntos, '-', DS.fn_valor_tasa - DS.fn_puntos, '/', DS.fn_valor_tasa / DS.fn_puntos, '*', DS.fn_valor_tasa * DS.fn_puntos) as TASA_INTERES"   +
			" 	,DS.fn_valor_tasa as TASA_INTERES"   +
			" 	,D.ig_plazo_credito as PLAZO"   +
			" 	,to_char(D.df_fecha_venc_credito, 'dd/mm/yyyy') as FECHA_VENCIMIENTO"   +
			" 	,DS.fn_importe_interes as MONTO_INTERES"   +
			" 	,TCI.cd_descripcion as TIPO_COBRO_INTERES"   +
			" 	,ES.cd_descripcion as ESTATUS"   +
			" 	,M.ic_moneda"   +
			" 	,LC.ic_moneda as moneda_linea"   +
			"  ,cpe.cg_num_distribuidor as numdist, ds.ic_tasa as cve_tasa,"   +
			"        NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion) as tipo_conver,"   +
			"        d.fn_tipo_cambio as tipo_cambio,"   +
			"        NVL (pe.cg_responsable_interes, pn.cg_responsable_interes) as resp_interes,"   +
			"        NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza)as rs_tipo_cobranza,d.ic_epo"   +
			" 		,ct_referencia, cg_campo1, cg_campo2, cg_campo3, cg_campo4, cg_campo5"   +
			"  FROM dis_documento D"   +
			" 	,dis_docto_seleccionado DS"   +
			" 	,dis_solicitud S"   +
			" 	,dis_linea_credito_dm LC"   +
			"  ,comrel_pyme_epo cpe"   +
			" 	,comrel_pyme_epo_x_producto PEP"   +
			" 	,comcat_producto_nafin PN"   +
			" 	,comrel_producto_epo PE"   +
			" 	,comcat_tasa T"   +
			" 	,comcat_epo E"   +
			" 	,comcat_pyme P"   +
			" 	,comcat_moneda M"   +
			" 	,comcat_estatus_solic ES"   +
			" 	,comcat_tipo_cobro_interes TCI"   +
			"  WHERE D.ic_documento = DS.ic_documento "   +
			" 	and D.ic_linea_credito_dm = LC.ic_linea_credito_dm"   +
			" 	and D.ic_epo = E.ic_epo"   +
			" 	and D.ic_pyme = P.ic_pyme"   +
			"  AND D.ic_epo = cpe.ic_epo"   +
			"  AND D.ic_pyme = cpe.ic_pyme"   +
			" 	AND D.ic_producto_nafin = PN.ic_producto_nafin"   +
			" 	AND D.ic_producto_nafin = PEP.ic_producto_nafin"   +
			" 	AND D.ic_producto_nafin = PE.ic_producto_nafin"   +
			" 	and D.ic_epo = PEP.ic_epo"   +
			" 	and D.ic_pyme = PEP.ic_pyme"   +
			" 	and D.ic_epo = PE.ic_epo"   +
			" 	and DS.ic_documento = S.ic_documento"   +
			" 	and DS.ic_tasa = T.ic_tasa"   +
			" 	and S.ic_estatus_solic = ES.ic_estatus_solic"   +
			" 	and LC.ic_moneda = M.ic_moneda"   +
			" 	and PE.ic_tipo_cobro_interes = TCI.ic_tipo_cobro_interes"   +
			" 	and PN.ic_producto_nafin = 4"   +
			"   and lc.ic_moneda=d.ic_moneda" +
//			"	and LC.ic_if = ? " + 
			"   and D.ic_documento in (" + clavesDocumentos + ")" );
				
		qryCCC.append(
			" select /*+ use_nl(d,ds,s,lc,cpe,pep,pn,pe,t,e,p,m,es,tci)"   +
			" index(pe cp_comrel_producto_epo_pk)"   +
			" index(tci cp_comcat_tipo_cobro_inter_pk)*/"   +
			" D.ic_documento"   +
			" ,E.cg_razon_social as NOMBRE_EPO"   +
			" ,P.cg_razon_social as NOMBRE_PYME"   +
			" ,decode(PEP.cg_tipo_credito, 'D', 'Modalidad 1 (Riesgo Empresa de Primer Orden )', 'C', 'Modalidad 2 (Riesgo Distribuidor)') as TIPO_CREDITO"   +
			" ,decode(nvl(PE.cg_responsable_interes, PN.cg_responsable_interes), 'E', 'EPO', 'D', 'Distribuidor') as RESPONSABLE_INTERES"   +
			" ,decode(nvl(PE.cg_tipo_cobranza, PN.cg_tipo_cobranza), 'D', 'Delegada', 'N', 'No Delegada') as TIPO_COBRANZA"   +
			" ,to_char(DS.df_fecha_seleccion, 'dd/mm/yyyy') as FECHA_OPERACION"   +
			" ,M.cd_nombre as MONEDA"   +
			" ,DS.fn_importe_recibir as MONTO"   +
			" ,decode(nvl(PE.cg_tipo_conversion, PN.cg_tipo_conversion), 'P', 'Dolar-Peso', '') as TIPO_CONVERSION"   +
			" ,D.fn_tipo_cambio as TIPO_CAMBIO"   +
			" ,T.cd_nombre || ' ' || DS.cg_rel_mat || ' ' || DS.fn_puntos as REFERENCIA_TASA"   +
//			" ,decode(DS.cg_rel_mat, '+', DS.fn_valor_tasa + DS.fn_puntos, '-', DS.fn_valor_tasa - DS.fn_puntos, '/', DS.fn_valor_tasa / DS.fn_puntos, '*', DS.fn_valor_tasa * DS.fn_puntos) as TASA_INTERES"   +
			" 	,DS.fn_valor_tasa as TASA_INTERES"   +
			" ,D.ig_plazo_credito as PLAZO"   +
			" ,to_char(D.df_fecha_venc_credito, 'dd/mm/yyyy') as FECHA_VENCIMIENTO"   +
			" ,DS.fn_importe_interes as MONTO_INTERES"   +
			" ,TCI.cd_descripcion as TIPO_COBRO_INTERES"   +
			" ,ES.cd_descripcion as ESTATUS"   +
			" ,D.ic_moneda"   +
			" ,LC.ic_moneda as moneda_linea"   +
			" ,cpe.cg_num_distribuidor as numdist, ds.ic_tasa as cve_tasa,"   +
			" NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion) as tipo_conver,"   +
			" d.fn_tipo_cambio as tipo_cambio,"   +
			" NVL (pe.cg_responsable_interes, pn.cg_responsable_interes) as resp_interes,"   +
			" NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza) as rs_tipo_cobranza,d.ic_epo"   +
			" ,ct_referencia, cg_campo1, cg_campo2, cg_campo3, cg_campo4, cg_campo5"   +
			" FROM dis_documento D"   +
			" ,dis_docto_seleccionado DS"   +
			" ,dis_solicitud S"   +
			" ,com_linea_credito LC"   +
			" ,comrel_pyme_epo cpe"   +
			" ,comrel_pyme_epo_x_producto PEP"   +
			" ,comcat_producto_nafin PN"   +
			" ,comrel_producto_epo PE"   +
			" ,comcat_tasa T"   +
			" ,comcat_epo E"   +
			" ,comcat_pyme P"   +
			" ,comcat_moneda M"   +
			" ,comcat_estatus_solic ES"   +
			" ,comcat_tipo_cobro_interes TCI"   +
			" WHERE D.ic_documento = DS.ic_documento"   +
			" and D.ic_linea_credito = LC.ic_linea_credito"   +
			" and D.ic_epo = E.ic_epo"   +
			" and D.ic_pyme = P.ic_pyme"   +
			" AND D.ic_epo = cpe.ic_epo"   +
			" AND D.ic_pyme = cpe.ic_pyme"   +
			" AND D.ic_producto_nafin = PN.ic_producto_nafin"   +
			" AND D.ic_producto_nafin = PEP.ic_producto_nafin"   +
			" AND D.ic_producto_nafin = PE.ic_producto_nafin"   +
			" and D.ic_epo = PEP.ic_epo"   +
			" and D.ic_pyme = PEP.ic_pyme"   +
			" and D.ic_epo = PE.ic_epo"   +
			" and DS.ic_documento = S.ic_documento"   +
			" and DS.ic_tasa = T.ic_tasa"   +
			" and S.ic_estatus_solic = ES.ic_estatus_solic"   +
			" and LC.ic_moneda = M.ic_moneda"   +
			" and PE.ic_tipo_cobro_interes = TCI.ic_tipo_cobro_interes"   +
			" and PN.ic_producto_nafin = 4"   +
			" and lc.ic_moneda=d.ic_moneda" +
//			" and LC.ic_if = ? " + 
			" and D.ic_documento in (" + clavesDocumentos + ")" );

			if("D".equals(tipo_credito)){
				this.numList = 1;
				qrySentencia.append(qryDM.toString());
			} else if("C".equals(tipo_credito)){
				this.numList = 1;
				qrySentencia.append(qryCCC.toString());
			} else {
				this.numList = 2;
				qrySentencia.append(qryDM.toString()+ " UNION ALL "+qryCCC.toString());
			}

		log.debug("el query queda de la siguiente manera "+qrySentencia.toString());
		return qrySentencia.toString();
	}
	
	public String getDocumentQuery(HttpServletRequest request) {

      String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
      StringBuffer qrySentencia = new StringBuffer();
      StringBuffer qryDM		= new StringBuffer();
      StringBuffer qryCCC		= new StringBuffer();
      StringBuffer condicion    = new StringBuffer();

		String	ses_ic_if           = request.getSession().getAttribute("iNoCliente").toString();
		String	tipo_credito 		= (request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String 	fecha_vto_de		= (request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String 	fecha_vto_a			= (request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String	ic_pyme				= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String	ic_epo				= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String	ic_estatus_docto	= (request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto");
		String	ic_documento		= (request.getParameter("ic_documento")==null)?"":request.getParameter("ic_documento");
		String 	fecha_seleccion_de	= (request.getParameter("fecha_seleccion_de")==null)?"":request.getParameter("fecha_seleccion_de");
		String 	fecha_seleccion_a	= (request.getParameter("fecha_seleccion_a")==null)?"":request.getParameter("fecha_seleccion_a");
		String 	monto_credito_de	= (request.getParameter("monto_credito_de")==null)?"":request.getParameter("monto_credito_de");
		String 	monto_credito_a		= (request.getParameter("monto_credito_a")==null)?"":request.getParameter("monto_credito_a");
		String 	ic_tipo_cobro_int	= (request.getParameter("ic_tipo_cobro_int")==null)?"":request.getParameter("ic_tipo_cobro_int");
		String 	fn_monto_de			= (request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String 	fn_monto_a			= (request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String 	ig_numero_docto		= (request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String 	cc_acuse			= (request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String 	fecha_emision_de	= (request.getParameter("fecha_emision_de")==null)?"":request.getParameter("fecha_emision_de");
		String 	fecha_emision_a		= (request.getParameter("fecha_emision_a")==null)?"":request.getParameter("fecha_emision_a");
		String	ic_moneda			= (request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String 	monto_con_descuento	= (request.getParameter("monto_con_descuento")==null)?"":"checked";
		String	solo_cambio			= (request.getParameter("solo_cambio")==null)?"":"checked";
		String	modo_plazo			= (request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
			
		try {

		if(!"".equals(ic_epo))
			condicion.append(" and D.ic_epo = ? ");
		if(!"".equals(ic_pyme))
			condicion.append(" and D.ic_pyme = ? ");
		if(!"".equals(ic_documento))
			condicion.append(" and D.ic_documento = ? ");
//Agregada ic_moneda
		if(!"".equals(ic_moneda))
			condicion.append(" and D.ic_moneda = ? ");
//Agregada ic_moneda						
		if(!"".equals(ic_tipo_cobro_int))
			condicion.append(" and PE.ic_tipo_cobro_interes = ? ");
		if(!"".equals(ic_estatus_docto))
			condicion.append(" and S.ic_estatus_solic = ? ");
		if(!"".equals(monto_credito_de)&&!"".equals(monto_credito_a))
			condicion.append(" and (D.fn_monto between ? and ?)");
		if(!"".equals(fecha_seleccion_de)&&!"".equals(fecha_seleccion_a))
			condicion.append(" and trunc(DS.df_fecha_seleccion) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");
		if(!"".equals(fecha_vto_de)&&!"".equals(fecha_vto_a))
			condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");

		qryDM.append(
			"   select /*+ use_nl(d,ds,s,lc,cpe,pep,pe,es)"   +
			"       index(d cp_dis_documento_pk)"   +
			"       index(ds)*/"   +
			"   D.ic_documento"   +
			"   FROM dis_documento D"   +
			"  	,dis_docto_seleccionado DS"   +
			"  	,dis_solicitud S"   +
			"  	,dis_linea_credito_dm LC"   +
			"  	,comrel_pyme_epo cpe"   +
			"  	,comrel_pyme_epo_x_producto PEP"   +
			"  	,comrel_producto_epo PE"   +
			"  	,comcat_epo epo"   +
			"  	,comcat_estatus_solic ES"   +
			"   WHERE D.ic_documento = DS.ic_documento"   +
			"  	and D.ic_linea_credito_dm = LC.ic_linea_credito_dm"   +
			" 	AND D.ic_epo = cpe.ic_epo"   +
			" 	AND cpe.ic_epo = epo.ic_epo"   +
			"   	AND D.ic_pyme = cpe.ic_pyme"   +
			"  	AND D.ic_producto_nafin = PEP.ic_producto_nafin"   +
			"  	AND D.ic_producto_nafin = PE.ic_producto_nafin"   +
			"  	and D.ic_epo = PEP.ic_epo"   +
			"  	and D.ic_pyme = PEP.ic_pyme"   +
			"  	and D.ic_epo = PE.ic_epo"   +
			"  	and DS.ic_documento = S.ic_documento"   +
			"  	and S.ic_estatus_solic = ES.ic_estatus_solic"   +
			"  	and PEP.ic_producto_nafin = 4"   +
			"  	and epo.cs_habilitado = 'S'"   +
			"   and lc.ic_moneda=d.ic_moneda" +
			"	and LC.ic_if = ? " +
			condicion.toString()  );
				
		qryCCC.append(
			"  select /*+ use_nl(d,ds,s,lc,cpe,pep,pe,es)"   +
			"      index(d cp_dis_documento_pk)"   +
			"      index(ds)"   +
			"      index(s)*/"   +
			"  d.ic_documento"   +
			"  FROM dis_documento D"   +
			"  ,dis_docto_seleccionado DS"   +
			"  ,dis_solicitud S"   +
			"  ,com_linea_credito LC"   +
			"  ,comrel_pyme_epo cpe"   +
			"  ,comrel_pyme_epo_x_producto PEP"   +
			"  ,comrel_producto_epo PE"   +
			"  ,comcat_epo epo"   +
			"  ,comcat_estatus_solic ES"   +
			"  ,comcat_tipo_cobro_interes TCI"   +
			"  WHERE D.ic_documento = DS.ic_documento"   +
			"  and D.ic_linea_credito = LC.ic_linea_credito"   +
			"  AND D.ic_epo = cpe.ic_epo"   +
			"  AND cpe.ic_epo = epo.ic_epo"   +
			"  AND D.ic_pyme = cpe.ic_pyme"   +
			"  AND D.ic_producto_nafin = PEP.ic_producto_nafin"   +
			"  AND D.ic_producto_nafin = PE.ic_producto_nafin"   +
			"  and D.ic_epo = PEP.ic_epo"   +
			"  and D.ic_pyme = PEP.ic_pyme"   +
			"  and D.ic_epo = PE.ic_epo"   +
			"  and DS.ic_documento = S.ic_documento"   +
			"  and S.ic_estatus_solic = ES.ic_estatus_solic"   +
			"  and PE.ic_tipo_cobro_interes = TCI.ic_tipo_cobro_interes"   +
			"  and PEP.ic_producto_nafin = 4"   +
			"  and epo.cs_habilitado = 'S' "   +
			"  and lc.ic_moneda=d.ic_moneda" +
			"  and LC.ic_if = ? " +
	  		condicion.toString());

			if("D".equals(tipo_credito)){
				this.numList = 1;
				qrySentencia.append(qryDM.toString());
			} else if("C".equals(tipo_credito)){
				this.numList = 1;
				qrySentencia.append(qryCCC.toString());
			} else {
				this.numList = 1;
				qrySentencia.append(qryDM.toString()+ " UNION ALL "+qryCCC.toString());
			}

			log.debug("EL query de la llave primaria: "+qrySentencia.toString());
		}catch(Exception e){
			log.error("InfSolicIfDist::getDocumentQueryException "+e);
		}
		return qrySentencia.toString();
	}
	
	public String getDocumentQueryFile(HttpServletRequest request) {
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer qryDM			  = new StringBuffer();
		StringBuffer qryCCC		    = new StringBuffer();
		StringBuffer condicion    = new StringBuffer();
		
		String	ses_ic_if           = request.getSession().getAttribute("iNoCliente").toString();
		String	tipo_credito 		= (request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String 	fecha_vto_de		= (request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String 	fecha_vto_a			= (request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String	ic_pyme				= (request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String	ic_epo				= (request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String	ic_estatus_docto	= (request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto");
		String	ic_documento		= (request.getParameter("ic_documento")==null)?"":request.getParameter("ic_documento");
		String 	fecha_seleccion_de	= (request.getParameter("fecha_seleccion_de")==null)?"":request.getParameter("fecha_seleccion_de");
		String 	fecha_seleccion_a	= (request.getParameter("fecha_seleccion_a")==null)?"":request.getParameter("fecha_seleccion_a");
		String 	monto_credito_de	= (request.getParameter("monto_credito_de")==null)?"":request.getParameter("monto_credito_de");
		String 	monto_credito_a		= (request.getParameter("monto_credito_a")==null)?"":request.getParameter("monto_credito_a");
		String 	ic_tipo_cobro_int	= (request.getParameter("ic_tipo_cobro_int")==null)?"":request.getParameter("ic_tipo_cobro_int");
		String 	fn_monto_de			= (request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String 	fn_monto_a			= (request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String 	ig_numero_docto		= (request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String 	cc_acuse			= (request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String 	fecha_emision_de	= (request.getParameter("fecha_emision_de")==null)?"":request.getParameter("fecha_emision_de");
		String 	fecha_emision_a		= (request.getParameter("fecha_emision_a")==null)?"":request.getParameter("fecha_emision_a");
		String	ic_moneda			= (request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String 	monto_con_descuento	= (request.getParameter("monto_con_descuento")==null)?"":"checked";
		String	solo_cambio			= (request.getParameter("solo_cambio")==null)?"":"checked";
		String	modo_plazo			= (request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
			
		try {

		if(!"".equals(ic_epo))
			condicion.append(" and D.ic_epo = ? ");
		if(!"".equals(ic_pyme))
			condicion.append(" and D.ic_pyme = ? ");
		if(!"".equals(ic_documento))
			condicion.append(" and D.ic_documento = ? ");
//Agregada ic_moneda
		if(!"".equals(ic_moneda))
			condicion.append(" and D.ic_moneda = ? ");
//Agregada ic_moneda
		if(!"".equals(ic_tipo_cobro_int))
			condicion.append(" and PE.ic_tipo_cobro_interes = ? ");
		if(!"".equals(ic_estatus_docto))
			condicion.append(" and S.ic_estatus_solic = ? ");
		if(!"".equals(monto_credito_de)&&!"".equals(monto_credito_a))
			condicion.append(" and (D.fn_monto between ? and ?)");
		if(!"".equals(fecha_seleccion_de)&&!"".equals(fecha_seleccion_a))
			condicion.append(" and trunc(DS.df_fecha_seleccion) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");
		if(!"".equals(fecha_vto_de)&&!"".equals(fecha_vto_a))
			condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");

		qryDM.append(
			"  select /*+ use_nl(d,ds,s,lc,cpe,pep,pn,pe,t,e,p,m,es,tci) "   +
			"     index(pe cp_comrel_producto_epo_pk)"   +
			"     index(tci cp_comcat_tipo_cobro_inter_pk)*/"   +
			"     D.ic_documento"   +
			" 	,E.cg_razon_social as NOMBRE_EPO"   +
			" 	,P.cg_razon_social as NOMBRE_PYME"   +
			" 	,decode(PEP.cg_tipo_credito, 'D', 'Modalidad 1 (Riesgo Empresa de Primer Orden )', 'C', 'Modalidad 2 (Riesgo Distribuidor)') as TIPO_CREDITO"   +
			" 	,decode(nvl(PE.cg_responsable_interes, PN.cg_responsable_interes), 'E', 'EPO', 'D', 'Distribuidor') as RESPONSABLE_INTERES"   +
			"  ,decode(nvl(PE.cg_tipo_cobranza, PN.cg_tipo_cobranza), 'D', 'Delegada', 'N', 'No Delegada') as TIPO_COBRANZA"   +
			" 	,to_char(DS.df_fecha_seleccion, 'dd/mm/yyyy') as FECHA_OPERACION"   +
			" 	,M.cd_nombre as MONEDA"   +
			" 	,DS.fn_importe_recibir as MONTO"   +
			" 	,decode(nvl(PE.cg_tipo_conversion, PN.cg_tipo_conversion), 'P', 'Dolar-Peso','') as TIPO_CONVERSION"   +
			" 	,D.fn_tipo_cambio as TIPO_CAMBIO"   +
			"	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',T.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) AS REFERENCIA_TASA" +
//			" 	,decode(DS.cg_rel_mat, '+', DS.fn_valor_tasa + DS.fn_puntos, '-', DS.fn_valor_tasa - DS.fn_puntos, '/', DS.fn_valor_tasa / DS.fn_puntos, '*', DS.fn_valor_tasa * DS.fn_puntos) as TASA_INTERES"   +
			" 	,DS.fn_valor_tasa as TASA_INTERES"   +
			" 	,D.ig_plazo_credito as PLAZO"   +
			" 	,to_char(D.df_fecha_venc_credito, 'dd/mm/yyyy') as FECHA_VENCIMIENTO"   +
			" 	,DS.fn_importe_interes as MONTO_INTERES"   +
			" 	,TCI.cd_descripcion as TIPO_COBRO_INTERES"   +
			" 	,ES.cd_descripcion as ESTATUS"   +
			" 	,M.ic_moneda"   +
			" 	,LC.ic_moneda as moneda_linea"   +
			"  ,cpe.cg_num_distribuidor as numdist, ds.ic_tasa as cve_tasa,"   +
			"        NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion) as tipo_conver,"   +
			"        d.fn_tipo_cambio as tipo_cambio,"   +
			"        NVL (pe.cg_responsable_interes, pn.cg_responsable_interes) as resp_interes,"   +
			"        NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza)as rs_tipo_cobranza,d.ic_epo"   +
			" 		,ct_referencia, cg_campo1, cg_campo2, cg_campo3, cg_campo4, cg_campo5"   +
			"  FROM dis_documento D"   +
			" 	,dis_docto_seleccionado DS"   +
			" 	,dis_solicitud S"   +
			" 	,dis_linea_credito_dm LC"   +
			"  ,comrel_pyme_epo cpe"   +
			" 	,comrel_pyme_epo_x_producto PEP"   +
			" 	,comcat_producto_nafin PN"   +
			" 	,comrel_producto_epo PE"   +
			" 	,comcat_tasa T"   +
			" 	,comcat_epo E"   +
			" 	,comcat_pyme P"   +
			" 	,comcat_moneda M"   +
			" 	,comcat_estatus_solic ES"   +
			" 	,comcat_tipo_cobro_interes TCI"   +
			"  WHERE D.ic_documento = DS.ic_documento "   +
			" 	and D.ic_linea_credito_dm = LC.ic_linea_credito_dm"   +
			" 	and D.ic_epo = E.ic_epo"   +
			" 	and D.ic_pyme = P.ic_pyme"   +
			"  AND D.ic_epo = cpe.ic_epo"   +
			"  AND D.ic_pyme = cpe.ic_pyme"   +
			" 	AND D.ic_producto_nafin = PN.ic_producto_nafin"   +
			" 	AND D.ic_producto_nafin = PEP.ic_producto_nafin"   +
			" 	AND D.ic_producto_nafin = PE.ic_producto_nafin"   +
			" 	and D.ic_epo = PEP.ic_epo"   +
			" 	and D.ic_pyme = PEP.ic_pyme"   +
			" 	and D.ic_epo = PE.ic_epo"   +
			" 	and DS.ic_documento = S.ic_documento"   +
			" 	and DS.ic_tasa = T.ic_tasa"   +
			" 	and S.ic_estatus_solic = ES.ic_estatus_solic"   +
			" 	and LC.ic_moneda = M.ic_moneda"   +
			" 	and PE.ic_tipo_cobro_interes = TCI.ic_tipo_cobro_interes"   +
			" 	and PN.ic_producto_nafin = 4"   +
			" 	and E.cs_habilitado = 'S' "   +
			"   and lc.ic_moneda=d.ic_moneda" +
			"	and LC.ic_if = ? " +
			condicion.toString()  );
				
		qryCCC.append(
			" select /*+ use_nl(d,ds,s,lc,cpe,pep,pn,pe,t,e,p,m,es,tci)"   +
			" index(pe cp_comrel_producto_epo_pk)"   +
			" index(tci cp_comcat_tipo_cobro_inter_pk)*/"   +
			" D.ic_documento"   +
			" ,E.cg_razon_social as NOMBRE_EPO"   +
			" ,P.cg_razon_social as NOMBRE_PYME"   +
			" ,decode(PEP.cg_tipo_credito, 'D', 'Modalidad 1 (Riesgo Empresa de Primer Orden )', 'C', 'Modalidad 2 (Riesgo Distribuidor)') as TIPO_CREDITO"   +
			" ,decode(nvl(PE.cg_responsable_interes, PN.cg_responsable_interes), 'E', 'EPO', 'D', 'Distribuidor') as RESPONSABLE_INTERES"   +
			" ,decode(nvl(PE.cg_tipo_cobranza, PN.cg_tipo_cobranza), 'D', 'Delegada', 'N', 'No Delegada') as TIPO_COBRANZA"   +
			" ,to_char(DS.df_fecha_seleccion, 'dd/mm/yyyy') as FECHA_OPERACION"   +
			" ,M.cd_nombre as MONEDA"   +
			" ,DS.fn_importe_recibir as MONTO"   +
			" ,decode(nvl(PE.cg_tipo_conversion, PN.cg_tipo_conversion), 'P', 'Dolar-Peso', '') as TIPO_CONVERSION"   +
			" ,D.fn_tipo_cambio as TIPO_CAMBIO"   +
			" ,T.cd_nombre || ' ' || DS.cg_rel_mat || ' ' || DS.fn_puntos as REFERENCIA_TASA"   +
//			" ,decode(DS.cg_rel_mat, '+', DS.fn_valor_tasa + DS.fn_puntos, '-', DS.fn_valor_tasa - DS.fn_puntos, '/', DS.fn_valor_tasa / DS.fn_puntos, '*', DS.fn_valor_tasa * DS.fn_puntos) as TASA_INTERES"   +
			" 	,DS.fn_valor_tasa as TASA_INTERES"   +
			" ,D.ig_plazo_credito as PLAZO"   +
			" ,to_char(D.df_fecha_venc_credito, 'dd/mm/yyyy') as FECHA_VENCIMIENTO"   +
			" ,DS.fn_importe_interes as MONTO_INTERES"   +
			" ,TCI.cd_descripcion as TIPO_COBRO_INTERES"   +
			" ,ES.cd_descripcion as ESTATUS"   +
			" ,D.ic_moneda"   +
			" ,LC.ic_moneda as moneda_linea"   +
			" ,cpe.cg_num_distribuidor as numdist, ds.ic_tasa as cve_tasa,"   +
			" NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion) as tipo_conver,"   +
			" d.fn_tipo_cambio as tipo_cambio,"   +
			" NVL (pe.cg_responsable_interes, pn.cg_responsable_interes) as resp_interes,"   +
			" NVL (pe.cg_tipo_cobranza, pn.cg_tipo_cobranza) as rs_tipo_cobranza,d.ic_epo"   +
			" ,ct_referencia, cg_campo1, cg_campo2, cg_campo3, cg_campo4, cg_campo5"   +
			" FROM dis_documento D"   +
			" ,dis_docto_seleccionado DS"   +
			" ,dis_solicitud S"   +
			" ,com_linea_credito LC"   +
			" ,comrel_pyme_epo cpe"   +
			" ,comrel_pyme_epo_x_producto PEP"   +
			" ,comcat_producto_nafin PN"   +
			" ,comrel_producto_epo PE"   +
			" ,comcat_tasa T"   +
			" ,comcat_epo E"   +
			" ,comcat_pyme P"   +
			" ,comcat_moneda M"   +
			" ,comcat_estatus_solic ES"   +
			" ,comcat_tipo_cobro_interes TCI"   +
			" WHERE D.ic_documento = DS.ic_documento"   +
			" and D.ic_linea_credito = LC.ic_linea_credito"   +
			" and D.ic_epo = E.ic_epo"   +
			" and D.ic_pyme = P.ic_pyme"   +
			" AND D.ic_epo = cpe.ic_epo"   +
			" AND D.ic_pyme = cpe.ic_pyme"   +
			" AND D.ic_producto_nafin = PN.ic_producto_nafin"   +
			" AND D.ic_producto_nafin = PEP.ic_producto_nafin"   +
			" AND D.ic_producto_nafin = PE.ic_producto_nafin"   +
			" and D.ic_epo = PEP.ic_epo"   +
			" and D.ic_pyme = PEP.ic_pyme"   +
			" and D.ic_epo = PE.ic_epo"   +
			" and DS.ic_documento = S.ic_documento"   +
			" and DS.ic_tasa = T.ic_tasa"   +
			" and S.ic_estatus_solic = ES.ic_estatus_solic"   +
			" and LC.ic_moneda = M.ic_moneda"   +
			" and PE.ic_tipo_cobro_interes = TCI.ic_tipo_cobro_interes"   +
			" and PN.ic_producto_nafin = 4"   +
			" and E.cs_habilitado = 'S' "   +
			" and lc.ic_moneda=d.ic_moneda" +
			"	and LC.ic_if = ? " +
	  		condicion.toString());

			if("D".equals(tipo_credito)){
				this.numList = 1;
				qrySentencia.append(qryDM.toString());
			} else if("C".equals(tipo_credito)){
				this.numList = 1;
				qrySentencia.append(qryCCC.toString());
			} else {
				this.numList = 1;
				qrySentencia.append(qryDM.toString()+ " UNION ALL "+qryCCC.toString());
			}

		}catch(Exception e){
			log.error("InfSolicIfDist::getDocumentQueryFileException "+e);
		}
		return qrySentencia.toString();
	}
	
		public List getConditions() {
		return conditions;
	}


	public void setNumList(int numList) {
		this.numList = numList;
	}


	public int get_numList() {
		return numList;
	}


	public void setConditions(List conditions) {
		this.conditions = conditions;
	}


	public List get_conditions() {
		return conditions;
	}


	public void setCtCredito(String ctCredito) {
		this.ctCredito = ctCredito;
	}


	public String getCtCredito() {
		return ctCredito;
	}


	public void setIcEpo(String icEpo) {
		this.icEpo = icEpo;
	}


	public String getIcEpo() {
		return icEpo;
	}


	public void setIcDist(String icDist) {
		this.icDist = icDist;
	}


	public String getIcDist() {
		return icDist;
	}


	public void setNumCred(String numCred) {
		this.numCred = numCred;
	}


	public String getNumCred() {
		return numCred;
	}


	public void setFechaOper1(String fechaOper1) {
		this.fechaOper1 = fechaOper1;
	}


	public String getFechaOper1() {
		return fechaOper1;
	}


	public void setFechaOper2(String fechaOper2) {
		this.fechaOper2 = fechaOper2;
	}


	public String getFechaOper2() {
		return fechaOper2;
	}


	public void setMonto1(String monto1) {
		this.monto1 = monto1;
	}


	public String getMonto1() {
		return monto1;
	}


	public void setMonto2(String monto2) {
		this.monto2 = monto2;
	}


	public String getMonto2() {
		return monto2;
	}


	public void setCbMoneda(String cbMoneda) {
		this.cbMoneda = cbMoneda;
	}


	public String getCbMoneda() {
		return cbMoneda;
	}


	public void setFechaVenc1(String fechaVenc1) {
		this.fechaVenc1 = fechaVenc1;
	}


	public String getFechaVenc1() {
		return fechaVenc1;
	}


	public void setFechaVenc2(String fechaVenc2) {
		this.fechaVenc2 = fechaVenc2;
	}


	public String getFechaVenc2() {
		return fechaVenc2;
	}


	public void setTipoCoInt(String tipoCoInt) {
		this.tipoCoInt = tipoCoInt;
	}


	public String getTipoCoInt() {
		return tipoCoInt;
	}


	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}


	public String getEstatus() {
		return estatus;
	}


	public void set_numList(int numList) {
		this.numList = numList;
	}


	public void set_conditions(List conditions) {
		this.conditions = conditions;
	}


	public void set_ctCredito(String ctCredito) {
		this.ctCredito = ctCredito;
	}


	public String get_ctCredito() {
		return ctCredito;
	}


	public void set_icEpo(String icEpo) {
		this.icEpo = icEpo;
	}


	public String get_icEpo() {
		return icEpo;
	}


	public void set_icDist(String icDist) {
		this.icDist = icDist;
	}


	public String get_icDist() {
		return icDist;
	}


	public void set_numCred(String numCred) {
		this.numCred = numCred;
	}


	public String get_numCred() {
		return numCred;
	}


	public void set_fechaOper1(String fechaOper1) {
		this.fechaOper1 = fechaOper1;
	}


	public String get_fechaOper1() {
		return fechaOper1;
	}


	public void set_fechaOper2(String fechaOper2) {
		this.fechaOper2 = fechaOper2;
	}


	public String get_fechaOper2() {
		return fechaOper2;
	}


	public void set_monto1(String monto1) {
		this.monto1 = monto1;
	}


	public String get_monto1() {
		return monto1;
	}


	public void set_monto2(String monto2) {
		this.monto2 = monto2;
	}


	public String get_monto2() {
		return monto2;
	}


	public void set_cbMoneda(String cbMoneda) {
		this.cbMoneda = cbMoneda;
	}


	public String get_cbMoneda() {
		return cbMoneda;
	}


	public void set_fechaVenc1(String fechaVenc1) {
		this.fechaVenc1 = fechaVenc1;
	}


	public String get_fechaVenc1() {
		return fechaVenc1;
	}


	public void set_fechaVenc2(String fechaVenc2) {
		this.fechaVenc2 = fechaVenc2;
	}


	public String get_fechaVenc2() {
		return fechaVenc2;
	}


	public void set_tipoCoInt(String tipoCoInt) {
		this.tipoCoInt = tipoCoInt;
	}


	public String get_tipoCoInt() {
		return tipoCoInt;
	}


	public void set_estatus(String estatus) {
		this.estatus = estatus;
	}


	public String get_estatus() {
		return estatus;
	}


	public void setSesIf(String sesIf) {
		this.sesIf = sesIf;
	}


	public String getSesIf() {
		return sesIf;
	}

	public void setTipoPago(String tipoPago) {
		this.tipoPago = tipoPago;
	}

	public String getTipoPago() {
		return tipoPago;
	}

}