package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.CQueryHelperRegExtJS;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsNoOperIfDist implements IQueryGeneratorRegExtJS {

	private static final Log LOG = ServiceLocator.getInstance().getLog(ConsNoOperIfDist.class);
	private List<Object> conditions;
	private String iNoCliente;
	private String ctCredito;
	private String ic_epo;
	private String icDist;
	private String numDoc;
	private String fechaOper1;
	private String fechaOper2;
	private String monto1;
	private String monto2;
	private String rechazo;
	private String fechaVenc1;
	private String fechaVenc2;
	private String tipoCoInt;
	private String fechaRe1;
	private String fechaRe2;
	
	public ConsNoOperIfDist(){}	

	@Override
	public String getAggregateCalculationQuery() {
		StringBuilder qrySentencia = new StringBuilder();
		StringBuilder condicion    = new StringBuilder();
		try {
			String COM_LINEA_CREDITO = "";
			String IC_LINEA_CREDITO = "";
			conditions=new ArrayList<>();
			if (!("".equals(fechaVenc1))&&fechaVenc1!=null&&fechaVenc2!=null && !("".equals(fechaVenc2))){
				condicion.append("    AND TRUNC(doc.df_fecha_venc_credito) BETWEEN TRUNC(TO_DATE(?,'dd/mm/yyyy')) AND TRUNC(TO_DATE(?,'dd/mm/yyyy')) ");
				conditions.add(fechaVenc1);
				conditions.add(fechaVenc2);
			}
			if (!("".equals(ic_epo))&&ic_epo!=null){
				condicion.append("   AND doc.ic_epo = ? ");
				conditions.add(ic_epo);
			}
			if (!("".equals(tipoCoInt))&&tipoCoInt!=null){
				condicion.append("   AND lc.ic_tipo_cobro_interes = ? ");
				conditions.add(tipoCoInt);
			}
			if (!("".equals(icDist))&&icDist!=null){
				condicion.append("   AND doc.ic_pyme = ? ");
				conditions.add(icDist);
			}
			if (!("".equals(numDoc))&&numDoc!=null){
				condicion.append("   AND doc.ic_documento = ? ");
				conditions.add(numDoc);
			}
			
			if (!("".equals(fechaOper1))&&fechaOper1!=null&&fechaOper2!=null && !("".equals(fechaOper2))){
				condicion.append("    AND TRUNC(ds.df_fecha_seleccion) BETWEEN TRUNC(TO_DATE(?,'dd/mm/yyyy')) AND TRUNC(TO_DATE(?,'dd/mm/yyyy')) ");
				conditions.add(fechaOper1);
				conditions.add(fechaOper2);
			}
			if (!("".equals(rechazo))&&rechazo!=null){
				condicion.append("   AND ce.ic_cambio_estatus = ? ");
				conditions.add(rechazo);
			} else {
				condicion.append("   AND ce.ic_cambio_estatus IN (23,34,2) ");
			}
			if (!("".equals(fechaRe1))&&fechaRe1!=null&&fechaRe2!=null && !("".equals(fechaRe2))){
				condicion.append("    AND TRUNC(ce.dc_fecha_cambio) BETWEEN TRUNC(TO_DATE(?,'dd/mm/yyyy')) AND TRUNC(TO_DATE(?,'dd/mm/yyyy')) ");
				conditions.add(fechaRe1);
				conditions.add(fechaRe2);
			}
			if("D".equals(ctCredito)&&ctCredito!=null){
				COM_LINEA_CREDITO = "dis_linea_credito_dm";
				IC_LINEA_CREDITO  = "ic_linea_credito_dm";
			}else if("C".equals(ctCredito)&&ctCredito!=null){
				COM_LINEA_CREDITO = "com_linea_credito";
				IC_LINEA_CREDITO  = "ic_linea_credito";
			}			

			qrySentencia.append(
					"  SELECT /*+ "   +
					"     index(doc cp_dis_documento_pk)"   +
					"     index(ce cp_dis_cambio_estatus_pk)"   +
					"     index(ds cp_dis_docto_seleccionado_pk)"   +
					"     index(lc cp_"+COM_LINEA_CREDITO+"_pk)"   +
					"     index(x cp_comrel_pyme_epo_x_prod_pk)"   +
					"     index(m cp_comcat_moneda_pk)*/"   +
					"    m.ic_moneda,m.cd_nombre, count(*),sum(ds.fn_importe_recibir) as suma_monto"   +
					"    FROM dis_documento doc"   +
					"         ,dis_cambio_estatus ce"   +
					"         ,dis_docto_seleccionado ds"   +
					"         ,"+COM_LINEA_CREDITO+" lc "+
					"         ,comrel_pyme_epo_x_producto x"   +
					"         ,comrel_producto_epo pe"   +
					"         ,comcat_moneda m"   +
					"         ,comcat_cambio_estatus cce"   +
					"   WHERE doc.ic_documento = ds.ic_documento"   +
					"     AND doc.ic_documento = ce.ic_documento"   +
					"     AND doc.ic_producto_nafin = x.ic_producto_nafin"   +
					"     AND doc.ic_epo = x.ic_epo"   +
					"     AND doc.ic_pyme = x.ic_pyme"   +
					"     AND doc."+IC_LINEA_CREDITO+" = lc."+IC_LINEA_CREDITO+" "+
					"     AND doc.ic_epo = pe.ic_epo"   +
					"     AND doc.ic_producto_nafin = pe.ic_producto_nafin"   +
					"     AND lc.ic_moneda = m.ic_moneda"   +
					"     AND ce.ic_cambio_estatus = cce.ic_cambio_estatus"   +
					"     AND pe.ic_producto_nafin = 4"   +
					"   AND x.cg_tipo_credito = '"+ctCredito+"' "+
					"   AND lc.ic_if = "+iNoCliente+
					condicion.toString()+
					" GROUP BY M.IC_MONEDA,M.CD_NOMBRE"   +
					" ORDER BY M.IC_MONEDA");
		}catch(Exception e){
			LOG.error("CambioEstatusEpoDE::getAggregateCalculationQuery ",e);
		}
		
		return qrySentencia.toString();	
	}

	/**
	 * Este m�todo regresa el query con el que se obtienen los 
	 * datos a mostrar en la consulta basandose en los identificadores unicos 
	 * especificados en las lista de ids.
	 * @param pageIds Lista de los identificadores unicos. El tama�o de la lista 
	 *    recibida ser� de acuerdo a la configuraci�n de registros x p�gina
	 * @return Cadena con el query armado de acuerdo a los parametros recibidos
	 */
	@Override
	public String getDocumentSummaryQueryForIds(List pageIds) {
		StringBuilder qrySentencia	= new StringBuilder();
		StringBuilder condicion		= new StringBuilder();
		this.conditions = new ArrayList<>();
		String	ses_ic_if         = this.getINoCliente();
		String	tipoLinCre		= this.getCtCredito();

		String COM_LINEA_CREDITO = "";
		String IC_LINEA_CREDITO = "";

		if("D".equals(tipoLinCre)){
			COM_LINEA_CREDITO = "dis_linea_credito_dm";
			IC_LINEA_CREDITO  = "ic_linea_credito_dm";
		}else if("C".equals(tipoLinCre)){
			COM_LINEA_CREDITO = "com_linea_credito";
			IC_LINEA_CREDITO  = "ic_linea_credito";
		}

		condicion.append(" AND x.cg_tipo_credito = ? ");
		this.conditions.add(tipoLinCre);
		
		condicion.append(" AND lc.ic_if = ? ");
		this.conditions.add(Integer.valueOf(ses_ic_if));

		condicion.append(" AND ( " );
		
		for(int j = 0; j < pageIds.size(); j++) {
			//lIds. Lista de dos elementos 0.- ic_documento, 1.- dc_fecha_cambio
			List lIds = (List)pageIds.get(j);
			condicion.append(
					( j>0?" OR ":"" ) +
					" ( ce.ic_documento = ? AND ce.dc_fecha_cambio = TO_DATE(?, 'dd/mm/yyyy hh24:mi:ss') ) ");
			this.conditions.add(lIds.get(0));
			this.conditions.add(lIds.get(1));
		}
		
		condicion.append(" ) " );
		
		qrySentencia.append(
				" SELECT /*+ use_nl(doc,ce,ds,lc,tc,x,pe,p,e,m,pn,ed,tc,cce)"   +
				"         index(doc cp_dis_documento_pk)"   +
				"         index(lc cp_"+COM_LINEA_CREDITO+"_pk)"   +
				"         index(ce cp_dis_cambio_estatus_pk)"   +
				"         index(tci cp_comcat_tipo_cobro_inter_pk)"   +
				" */"   +
				"        doc.ic_documento, doc.ig_numero_docto, e.cg_razon_social AS epo,"   +
				"        p.cg_razon_social AS pyme,"   +
				" 		DECODE	(x.cg_tipo_credito,'D','Modalidad 1 (Riesgo Empresa de Primer Orden )','C','Modalidad 2 (Riesgo Distribuidor)') as cg_tipo_credito,"   +
				"        DECODE (NVL (pe.cg_responsable_interes, pn.cg_responsable_interes),'E', 'EPO','D', 'Distribuidor','') AS responsable,"   +
				"        DECODE (NVL(pe.cg_tipo_cobranza, pn.cg_tipo_cobranza),'D', 'Delegada','N', 'No Delegada','') AS cobranza,"   +
				"        TO_CHAR (ds.df_fecha_seleccion, 'dd/mm/yyyy') AS fecha_oper,"   +
				"        m.cd_nombre, ds.fn_importe_recibir,"   +
				"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', 'Sin Conversion','P', 'Peso - Dolar','') AS conversion,"   +
				"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio,"   +
				"        DECODE (doc.ic_moneda,1, doc.fn_monto,54, doc.fn_monto * doc.fn_tipo_cambio) AS monto_val,"   +
				"        doc.ct_referencia,"   +
				"        DECODE (ds.cg_rel_mat,"   +
				"           '+', ds.fn_valor_tasa + ds.fn_puntos,"   +
				"           '-', ds.fn_valor_tasa - ds.fn_puntos,"   +
				"           '*', ds.fn_valor_tasa * ds.fn_puntos,"   +
				"           '/', ds.fn_valor_tasa / ds.fn_puntos,"   +
				"           0) AS tasa_int,"   +
				"        doc.ig_plazo_descuento,"   +
				"        TO_CHAR (doc.df_fecha_venc_credito, 'dd/mm/yyyy') AS fecha_venc,"   +
				"        ds.fn_importe_interes AS monto_int,"   +
				"        tci.cd_descripcion AS tipo_cob_int,"   +
				"        cce.cd_descripcion AS cambioestatus,"   +
				"        TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fechacambio,"   +
				"        ce.ct_cambio_motivo AS cambiomotivo, m.ic_moneda"   +
				"   FROM dis_documento doc "   +
				"        ,dis_cambio_estatus ce  "   +
				"        ,dis_docto_seleccionado ds"   +
				"        ,"+COM_LINEA_CREDITO+" lc "+
				"        ,com_tipo_cambio tc"   +
				"        ,comrel_pyme_epo_x_producto x"   +
				"        ,comrel_producto_epo pe"   +
				"        ,comcat_pyme p"   +
				"        ,comcat_epo e"   +
				"        ,comcat_moneda m"   +
				"        ,comcat_producto_nafin pn"   +
				"        ,comcat_estatus_docto ed"   +
				"        ,comcat_tipo_cobro_interes tci"   +
				"        ,comcat_cambio_estatus cce"   +
				"  WHERE doc.ic_documento = ds.ic_documento "   +
				"    AND doc.ic_documento = ce.ic_documento"   +
				"    AND doc.ic_epo = e.ic_epo"   +
				"    AND doc.ic_pyme = p.ic_pyme"   +
				"    AND doc.ic_producto_nafin = x.ic_producto_nafin"   +
				"    AND doc.ic_producto_nafin = pn.ic_producto_nafin"   +
				"    AND doc.ic_epo = x.ic_epo"   +
				"    AND doc.ic_pyme = x.ic_pyme"   +
				"    AND doc.ic_estatus_docto = ed.ic_estatus_docto"   +
				"   AND doc."+IC_LINEA_CREDITO+" = lc."+IC_LINEA_CREDITO+" "+
				"    AND doc.ic_epo = pe.ic_epo"   +
				"    AND doc.ic_producto_nafin = pe.ic_producto_nafin"   +
				"    AND lc.ic_moneda = m.ic_moneda"   +
				"    AND lc.ic_moneda = tc.ic_moneda"   +
				"    AND lc.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes"   +
				"    AND ce.ic_cambio_estatus = cce.ic_cambio_estatus"   +
				"    AND tc.dc_fecha IN (SELECT MAX (dc_fecha)  FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)"   +
				"    AND pn.ic_producto_nafin = 4"   +
				condicion.toString());

		LOG.debug("********************Query IDS"+qrySentencia.toString());
		LOG.debug("********************query IDs"+this.conditions.toString());
		
		return qrySentencia.toString();
	
	}		
	
	 /**
	  * En este m�todo se realiza la generaci�n de archivo
	  * con base en el objeto Registros que recibe como par�metro.
	  * @param request HttpRequest empleado principalmente para obtener el objeto session
	  * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	  * @param path Ruta donde se generar� el archivo
	  * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	  * @return Cadena con la ruta del archivo generado
	  */
	@Override
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		String nombreArchivo = "";
		
		if("PDF".equals(tipo)){
			try{
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
				
				String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual		= fechaActual.substring(0,2);
				String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual		= fechaActual.substring(6,10);
				String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
				
				pdfDoc.setLTable(18, 100);
				pdfDoc.setLCell("No. Docto Final","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Dist.","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de Cr�dito","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Resp. Pago de Inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de Cobranza","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Operaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Ref. Tasa Inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tasa Inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo Cobro Inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de rechazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Causa","celda01",ComunesPDF.CENTER);

				
				while (reg.next()){
					String numeroDocumento		= (reg.getString(1) == null) ? "" : reg.getString(1);
					String epo						= (reg.getString("EPO") == null) ? "" : reg.getString("EPO");
					String dist						= (reg.getString("PYME") == null) ? "" : reg.getString("PYME");
					String tipoCredito			= (reg.getString("CG_TIPO_CREDITO") == null) ? "" : reg.getString("CG_TIPO_CREDITO");
					String respInt					= (reg.getString("RESPONSABLE") == null) ? "" : reg.getString("RESPONSABLE");
					String tipoCob					= (reg.getString("COBRANZA") == null) ? "" : reg.getString("COBRANZA");
					String fOpera					= (reg.getString("FECHA_OPER") == null) ? "" : reg.getString("FECHA_OPER");
					String moneda					= (reg.getString("CD_NOMBRE") == null) ? "" : reg.getString("CD_NOMBRE");
					String monto					= (reg.getString("MONTO_VAL") == 	null) ? "" : reg.getString("MONTO_VAL");
					String refTasa					= (reg.getString("CT_REFERENCIA") == null) ? "" : reg.getString("CT_REFERENCIA");
					String tasaInt					= (reg.getString("TASA_INT") == null) ? "" : reg.getString("TASA_INT");
					String plazo					= (reg.getString("IG_PLAZO_DESCUENTO") == null) ? "" : reg.getString("IG_PLAZO_DESCUENTO");
					String fVenc					= (reg.getString("FECHA_VENC") == 	null) ? "" : reg.getString("FECHA_VENC");
					String montoInteres			= (reg.getString("MONTO_INT") == null) ? "" : reg.getString("MONTO_INT");
					String cobroInt				= (reg.getString("TIPO_COB_INT") == null) ? "" : reg.getString("TIPO_COB_INT");
					String estatus					= (reg.getString("CAMBIOESTATUS") == null) ? "" : reg.getString("CAMBIOESTATUS");
					String fechaRe					= (reg.getString("FECHACAMBIO") == null) ? "" : reg.getString("FECHACAMBIO");
					String causa					= (reg.getString("CAMBIOMOTIVO") == null) ? "" : reg.getString("CAMBIOMOTIVO");

					
					pdfDoc.setLCell(numeroDocumento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(epo,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(dist,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipoCredito,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(respInt,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipoCob,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fOpera,"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(refTasa,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(Comunes.formatoDecimal(tasaInt,2)+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(plazo,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fVenc,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoInteres,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(cobroInt,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(estatus,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaRe,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(causa,"formas",ComunesPDF.CENTER);
					
				}
				//pdfDoc.addTable();
				
				
				CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS();//toma el valor de sesi�n
				Registros regi = queryHelper.getResultCount(request);
				
			
				
					
				pdfDoc.setLCell("MONEDA ","celda01rep",ComunesPDF.LEFT,6);
				pdfDoc.setLCell("TOTAL REGISTROS","celda01rep",ComunesPDF.CENTER,6);
				pdfDoc.setLCell("MONTO","celda01rep",ComunesPDF.CENTER,6);

				while(regi.next()){
					pdfDoc.setLCell("TOTAL "+regi.getString(2),"celda01rep",ComunesPDF.LEFT,6);
					pdfDoc.setLCell(regi.getString(3),"celda01rep",ComunesPDF.CENTER,6);
					pdfDoc.setLCell("$"+regi.getString(4),"celda01rep",ComunesPDF.CENTER,6);

				}
			
				pdfDoc.addLTable();
				pdfDoc.endDocument();
			}catch(Throwable e){
				throw new AppException("Error al generar el archivo",e);
			}
		}	
		return nombreArchivo;
	}
	
	/**
	 * En este m�todo realiza la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	@Override
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
		LOG.info("crearCustomFile (E)");
		String linea = "";
		String nombreArchivo = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;

		try {
			if(tipo.equals("CSV")) {
				linea = "No. Docto Final,EPO,Dist.,Tipo de Cr�dito,Resp. Pago de Inter�s,Tipo de Cobranza,Fecha de Operaci�n,Moneda,Monto,Ref. Tasa Inter�s"
				+",Tasa Inter�s,Plazo,Fecha de Vencimiento,Monto Inter�s,Tipo Cobro Inter�s,Estatus,Fecha de Rechazo,Causa\n";
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
				buffer = new BufferedWriter(writer);

				buffer.write(linea);

				while (rs.next()) {
					String numeroDocumento		= (rs.getString(1) == null) ? "" : rs.getString(1);
					String epo						= (rs.getString("EPO") == null) ? "" : rs.getString("EPO");
					String dist						= (rs.getString("PYME") == null) ? "" : rs.getString("PYME");
					String tipoCredito			= (rs.getString("CG_TIPO_CREDITO") == null) ? "" : rs.getString("CG_TIPO_CREDITO");
					String respInt					= (rs.getString("RESPONSABLE") == null) ? "" : rs.getString("RESPONSABLE");
					String tipoCob					= (rs.getString("COBRANZA") == null) ? "" : rs.getString("COBRANZA");
					String fOpera					= (rs.getString("FECHA_OPER") == null) ? "" : rs.getString("FECHA_OPER");
					String moneda					= (rs.getString("CD_NOMBRE") == null) ? "" : rs.getString("CD_NOMBRE");
					String monto					= (rs.getString("MONTO_VAL") == 	null) ? "" : rs.getString("MONTO_VAL");
					String refTasa					= (rs.getString("CT_REFERENCIA") == null) ? "" : rs.getString("CT_REFERENCIA");
					String tasaInt					= (rs.getString("TASA_INT") == null) ? "" : rs.getString("TASA_INT");
					String plazo					= (rs.getString("IG_PLAZO_DESCUENTO") == null) ? "" : rs.getString("IG_PLAZO_DESCUENTO");
					String fVenc					= (rs.getString("FECHA_VENC") == 	null) ? "" : rs.getString("FECHA_VENC");
					String montoInteres			= (rs.getString("MONTO_INT") == null) ? "" : rs.getString("MONTO_INT");
					String cobroInt				= (rs.getString("TIPO_COB_INT") == null) ? "" : rs.getString("TIPO_COB_INT");
					String estatus					= (rs.getString("CAMBIOESTATUS") == null) ? "" : rs.getString("CAMBIOESTATUS");
					String fechaRe					= (rs.getString("FECHACAMBIO") == null) ? "" : rs.getString("FECHACAMBIO");
					String causa					= (rs.getString("CAMBIOMOTIVO") == null) ? "" : rs.getString("CAMBIOMOTIVO");

					linea = numeroDocumento.replace(',',' ') + ", " +
									epo.replace(',',' ') + ", " +
								dist.replace(',',' ') + ", " +
								tipoCredito.replace(',',' ') + ", " +
								respInt.replace(',',' ') + ", " +
								tipoCob.replace(',',' ') + ", " +
								fOpera.replace(',',' ') + ", " +
								moneda.replace(',',' ') + ", " +
								monto.replace(',',' ') + ", " +
								refTasa.replace(',',' ') + ", " +
								tasaInt.replace(',',' ') + ", " +
								plazo.replace(',',' ') + ", " +
								fVenc.replace(',',' ') + ", " +
								montoInteres.replace(',',' ') + ", " +
								cobroInt.replace(',',' ') + ", " +
								estatus.replace(',',' ') + ", " +
								fechaRe.replace(',',' ') + ", " +
								causa.replace(',',' ') + "\n";		

					buffer.write(linea);
				}
				CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS();//toma el valor de sesi�n
				Registros regi = queryHelper.getResultCount(request);

				while(regi.next()){
					linea="TOTAL "+regi.getString(2)+","+regi.getString(3)+","+"$"+regi.getString(4)+"\n";
					buffer.write(linea);
				}

				buffer.close();

			} else if("PDF".equals(tipo)){
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);

				String meses[]     = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));

				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);

				pdfDoc.setLTable(18, 100);
				pdfDoc.setLCell("No. Docto Final","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Dist.","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de Cr�dito","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Resp. Pago de Inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo de Cobranza","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Operaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Ref. Tasa Inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tasa Inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Plazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Monto Inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Tipo Cobro Inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Fecha de rechazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setLCell("Causa","celda01",ComunesPDF.CENTER);
				pdfDoc.setLHeaders();

				while (rs.next()){
					String numeroDocumento = (rs.getString(1)                    == null) ? "" : rs.getString(1);
					String epo             = (rs.getString("EPO")                == null) ? "" : rs.getString("EPO");
					String dist            = (rs.getString("PYME")               == null) ? "" : rs.getString("PYME");
					String tipoCredito     = (rs.getString("CG_TIPO_CREDITO")    == null) ? "" : rs.getString("CG_TIPO_CREDITO");
					String respInt         = (rs.getString("RESPONSABLE")        == null) ? "" : rs.getString("RESPONSABLE");
					String tipoCob         = (rs.getString("COBRANZA")           == null) ? "" : rs.getString("COBRANZA");
					String fOpera          = (rs.getString("FECHA_OPER")         == null) ? "" : rs.getString("FECHA_OPER");
					String moneda          = (rs.getString("CD_NOMBRE")          == null) ? "" : rs.getString("CD_NOMBRE");
					String monto           = (rs.getString("MONTO_VAL")          == null) ? "" : rs.getString("MONTO_VAL");
					String refTasa         = (rs.getString("CT_REFERENCIA")      == null) ? "" : rs.getString("CT_REFERENCIA");
					String tasaInt         = (rs.getString("TASA_INT")           == null) ? "" : rs.getString("TASA_INT");
					String plazo           = (rs.getString("IG_PLAZO_DESCUENTO") == null) ? "" : rs.getString("IG_PLAZO_DESCUENTO");
					String fVenc           = (rs.getString("FECHA_VENC")         == null) ? "" : rs.getString("FECHA_VENC");
					String montoInteres    = (rs.getString("MONTO_INT")          == null) ? "" : rs.getString("MONTO_INT");
					String cobroInt        = (rs.getString("TIPO_COB_INT")       == null) ? "" : rs.getString("TIPO_COB_INT");
					String estatus         = (rs.getString("CAMBIOESTATUS")      == null) ? "" : rs.getString("CAMBIOESTATUS");
					String fechaRe         = (rs.getString("FECHACAMBIO")        == null) ? "" : rs.getString("FECHACAMBIO");
					String causa           = (rs.getString("CAMBIOMOTIVO")       == null) ? "" : rs.getString("CAMBIOMOTIVO");

					pdfDoc.setLCell(numeroDocumento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(epo,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(dist,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipoCredito,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(respInt,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(tipoCob,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fOpera,"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(refTasa,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(Comunes.formatoDecimal(tasaInt,2)+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(plazo,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fVenc,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoInteres,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(cobroInt,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(estatus,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaRe,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(causa,"formas",ComunesPDF.CENTER);
	
				}
				//pdfDoc.addTable();

				CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS();//toma el valor de sesi�n
				Registros regi = queryHelper.getResultCount(request);

				pdfDoc.setLCell("MONEDA ","celda01",ComunesPDF.LEFT,6);
				pdfDoc.setLCell("TOTAL REGISTROS","celda01",ComunesPDF.CENTER,6);
				pdfDoc.setLCell("MONTO","celda01",ComunesPDF.CENTER,6);

				while(regi.next()){
					pdfDoc.setLCell("TOTAL "+regi.getString(2),"celda01",ComunesPDF.LEFT,6);
					pdfDoc.setLCell(regi.getString(3),"celda01",ComunesPDF.CENTER,6);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(regi.getString(4),2),"celda01",ComunesPDF.CENTER,6);
				}

				pdfDoc.addLTable();
				pdfDoc.endDocument();
			}
		}catch (Throwable e) {
			throw new AppException("Error al generar el archivo",e);
		}
		finally {
			try {
				rs.close();
			} catch(Exception e) {LOG.error("Error al cerrar recursos", e);}
		}
		LOG.info("crearCustomFile (S)");
		return nombreArchivo;
	}


	/**
	 * Este m�todo regresa el query con el que se obtienen los 
	 * identificadores unicos de los registros a mostrar en la consulta.
	 * @return Cadena con el query armado de acuerdo a los parametros establecidos
	 */
	@Override
	public String getDocumentQuery() {
		StringBuilder qrySentencia = new StringBuilder();
		StringBuilder condicion    = new StringBuilder();

		try {
			String COM_LINEA_CREDITO = "";
			String IC_LINEA_CREDITO = "";
			this.conditions=new ArrayList<>();
			if (!("".equals(fechaVenc1))&&fechaVenc1!=null&&fechaVenc2!=null && !("".equals(fechaVenc2))){
				condicion.append(
						" AND doc.df_fecha_venc_credito >= TO_DATE(?,'dd/mm/yyyy') " +
						" AND doc.df_fecha_venc_credito < TO_DATE(?,'dd/mm/yyyy') + 1 ");
				this.conditions.add(fechaVenc1);
				this.conditions.add(fechaVenc2);
			}
			if (!("".equals(ic_epo))&&ic_epo!=null){
				condicion.append(" AND doc.ic_epo = ? ");
				this.conditions.add(ic_epo);
			}
			if (!("".equals(tipoCoInt))&&tipoCoInt!=null){
				condicion.append(" AND lc.ic_tipo_cobro_interes = ? ");
				this.conditions.add(tipoCoInt);
			}
			if (!("".equals(icDist))&&icDist!=null){
				condicion.append(" AND doc.ic_pyme = ? ");
				this.conditions.add(icDist);
			}
			if (!("".equals(numDoc))&&numDoc!=null){
				condicion.append(" AND doc.ic_documento = ? ");
				this.conditions.add(numDoc);
			}
			
			if (!("".equals(fechaOper1))&&fechaOper1!=null&&fechaOper2!=null && !("".equals(fechaOper2))){
				condicion.append(
						" AND ds.df_fecha_seleccion >= TO_DATE(?,'dd/mm/yyyy') " +
						" AND ds.df_fecha_seleccion < TO_DATE(?,'dd/mm/yyyy') + 1 ");
				this.conditions.add(fechaOper1);
				this.conditions.add(fechaOper2);
			}
			if (!("".equals(rechazo))&&rechazo!=null){
				condicion.append(" AND ce.ic_cambio_estatus = ? ");
				this.conditions.add(rechazo);
			} else {
				condicion.append(" AND ce.ic_cambio_estatus IN (23,34,2) ");
			}
			if (!("".equals(fechaRe1))&&fechaRe1!=null&&fechaRe2!=null && !("".equals(fechaRe2))){
				condicion.append(
						" AND ce.dc_fecha_cambio >= TO_DATE(?,'dd/mm/yyyy') " +
						" AND ce.dc_fecha_cambio < TO_DATE(?,'dd/mm/yyyy') + 1 ");
				this.conditions.add(fechaRe1);
				this.conditions.add(fechaRe2);
			}
			
			condicion.append(" AND x.cg_tipo_credito = ? ");
			this.conditions.add(ctCredito);
			
			condicion.append(" AND lc.ic_if = ? ");
			conditions.add(Integer.valueOf(iNoCliente));

			if("D".equals(ctCredito)&&ctCredito!=null){
				COM_LINEA_CREDITO = "dis_linea_credito_dm";
				IC_LINEA_CREDITO  = "ic_linea_credito_dm";
			}else if("C".equals(ctCredito)&&ctCredito!=null){
				COM_LINEA_CREDITO = "com_linea_credito";
				IC_LINEA_CREDITO  = "ic_linea_credito";
			}

			qrySentencia.append(
					" SELECT /*+ use_nl(doc,ce,ds,lc,x,pe,cce)"   +
					"     index(doc cp_dis_documento_pk)"   +
					"     index(ce cp_dis_cambio_estatus_pk)"   +
					"     index(lc cp_"+COM_LINEA_CREDITO+"_pk)*/"   +
					"     ce.ic_documento, TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy hh24:mi:ss') AS dc_fecha_cambio "   +
					"   FROM dis_documento doc "   +
					"        ,dis_cambio_estatus ce  "   +
					"        ,dis_docto_seleccionado ds"   +
					"        ,"+COM_LINEA_CREDITO+" lc "+
					"        ,comrel_pyme_epo_x_producto x"   +
					"        ,comrel_producto_epo pe"   +
					"        ,comcat_cambio_estatus cce"   +
					"  WHERE doc.ic_documento = ds.ic_documento "   +
					"    AND doc.ic_documento = ce.ic_documento"   +
					"    AND doc.ic_producto_nafin = x.ic_producto_nafin"   +
					"    AND doc.ic_epo = x.ic_epo"   +
					"    AND doc.ic_pyme = x.ic_pyme"   +
					"    AND doc."+IC_LINEA_CREDITO+" = lc."+IC_LINEA_CREDITO+" "+
					"    AND doc.ic_epo = pe.ic_epo"   +
					"    AND doc.ic_producto_nafin = pe.ic_producto_nafin"   +
					"    AND ce.ic_cambio_estatus = cce.ic_cambio_estatus"   +
					"    AND pe.ic_producto_nafin = 4"   +
					condicion.toString());

			LOG.debug("EL query de la llave primaria:"+qrySentencia.toString()+"::conditions:::.."+this.conditions.toString());
		}catch(Exception e){
			LOG.error("ConsInfDocEpoDist::getDocumentQueryException ",e);
		}
		return qrySentencia.toString();
	}

	/**
	 * Este m�todo regresa el query que obtendr� todos los registros
	 * resultantes de la b�squeda, con la finalidad de generar un archivo.
	 * @return Cadena con el query armado de acuerdo a los parametros establecidos
	 */
	@Override
	public String getDocumentQueryFile() {
		StringBuilder qrySentencia = new StringBuilder();
		StringBuilder condicion    = new StringBuilder();
		try {
			String COM_LINEA_CREDITO = "";
			String IC_LINEA_CREDITO = "";

			this.conditions=new ArrayList<>();
			if (!("".equals(fechaVenc1))&&fechaVenc1!=null&&fechaVenc2!=null && !("".equals(fechaVenc2))){
				condicion.append(
						" AND doc.df_fecha_venc_credito >= TO_DATE(?,'dd/mm/yyyy') " +
						" AND doc.df_fecha_venc_credito < TO_DATE(?,'dd/mm/yyyy') + 1 ");
				this.conditions.add(fechaVenc1);
				this.conditions.add(fechaVenc2);
			}
			if (!("".equals(ic_epo))&&ic_epo!=null){
				condicion.append("   AND doc.ic_epo = ? ");
				this.conditions.add(ic_epo);
			}
			if (!("".equals(tipoCoInt))&&tipoCoInt!=null){
				condicion.append("   AND lc.ic_tipo_cobro_interes = ? ");
				this.conditions.add(tipoCoInt);
			}
			if (!("".equals(icDist))&&icDist!=null){
				condicion.append("   AND doc.ic_pyme = ? ");
				this.conditions.add(icDist);
			}
			if (!("".equals(numDoc))&&numDoc!=null){
				condicion.append("   AND doc.ic_documento = ? ");
				this.conditions.add(numDoc);
			}
			
			if (!("".equals(fechaOper1))&&fechaOper1!=null&&fechaOper2!=null && !("".equals(fechaOper2))){
				condicion.append("    AND TRUNC(ds.df_fecha_seleccion) BETWEEN TRUNC(TO_DATE(?,'dd/mm/yyyy')) AND TRUNC(TO_DATE(?,'dd/mm/yyyy')) ");
				this.conditions.add(fechaOper1);
				this.conditions.add(fechaOper2);
			}
			if (!("".equals(rechazo))&&rechazo!=null){
				condicion.append("   AND ce.ic_cambio_estatus = ? ");
				this.conditions.add(rechazo);
			} else {
				condicion.append("   AND ce.ic_cambio_estatus IN (23,34,2) ");
			}
			if (!("".equals(fechaRe1))&&fechaRe1!=null&&fechaRe2!=null && !("".equals(fechaRe2))){
				condicion.append(
						" AND ce.dc_fecha_cambio >= TO_DATE(?,'dd/mm/yyyy') " +
						" AND ce.dc_fecha_cambio < TO_DATE(?,'dd/mm/yyyy') + 1 ");
				this.conditions.add(fechaRe1);
				this.conditions.add(fechaRe2);
			}
			
			condicion.append(" AND x.cg_tipo_credito = ? ");
			this.conditions.add(ctCredito);
			condicion.append("   AND lc.ic_if = ? ");
			this.conditions.add(Integer.valueOf(iNoCliente));
			
			if("D".equals(ctCredito)&&ctCredito!=null){
				COM_LINEA_CREDITO = "dis_linea_credito_dm";
				IC_LINEA_CREDITO  = "ic_linea_credito_dm";
			}else if("C".equals(ctCredito)&&ctCredito!=null){
				COM_LINEA_CREDITO = "com_linea_credito";
				IC_LINEA_CREDITO  = "ic_linea_credito";
			}

			qrySentencia.append(
					" SELECT /*+ use_nl(doc,ce,ds,lc,tc,x,pe,p,e,m,pn,ed,tc,cce)"   +
					"         index(doc cp_dis_documento_pk)"   +
					"         index(lc cp_"+COM_LINEA_CREDITO+"_pk)"   +
					"         index(ce cp_dis_cambio_estatus_pk)"   +
					"         index(tci cp_comcat_tipo_cobro_inter_pk)"   +
					" */"   +
					"        doc.ic_documento, doc.ig_numero_docto, e.cg_razon_social AS epo,"   +
					"        p.cg_razon_social AS pyme,"   +
					" 		DECODE	(x.cg_tipo_credito,'D','Modalidad 1 (Riesgo Empresa de Primer Orden ','C','Modalidad 2 (Riesgo Distribuidor)') as cg_tipo_credito,"   +
					"        DECODE (NVL (pe.cg_responsable_interes, pn.cg_responsable_interes),'E', 'EPO','D', 'Distribuidor','') AS responsable,"   +
					"        DECODE (NVL(pe.cg_tipo_cobranza, pn.cg_tipo_cobranza),'D', 'Delegada','N', 'No Delegada','') AS cobranza,"   +
					"        TO_CHAR (ds.df_fecha_seleccion, 'dd/mm/yyyy') AS fecha_oper,"   +
					"        m.cd_nombre, ds.fn_importe_recibir,"   +
					"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', 'Sin Conversion','P', 'Peso - Dolar','') AS conversion,"   +
					"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio,"   +
					"        DECODE (doc.ic_moneda,1, doc.fn_monto,54, doc.fn_monto * doc.fn_tipo_cambio) AS monto_val,"   +
					"        doc.ct_referencia,"   +
					"        DECODE (ds.cg_rel_mat,"   +
					"           '+', ds.fn_valor_tasa + ds.fn_puntos,"   +
					"           '-', ds.fn_valor_tasa - ds.fn_puntos,"   +
					"           '*', ds.fn_valor_tasa * ds.fn_puntos,"   +
					"           '/', ds.fn_valor_tasa / ds.fn_puntos,"   +
					"           0) AS tasa_int,"   +
					"        doc.ig_plazo_descuento,"   +
					"        TO_CHAR (doc.df_fecha_venc_credito, 'dd/mm/yyyy') AS fecha_venc,"   +
					"        ds.fn_importe_interes AS monto_int,"   +
					"        tci.cd_descripcion AS tipo_cob_int,"   +
					"        cce.cd_descripcion AS cambioestatus,"   +
					"        TO_CHAR (ce.dc_fecha_cambio, 'dd/mm/yyyy') AS fechacambio,"   +
					"        ce.ct_cambio_motivo AS cambiomotivo, m.ic_moneda"   +
					"   FROM dis_documento doc "   +
					"        ,dis_cambio_estatus ce  "   +
					"        ,dis_docto_seleccionado ds"   +
					"        ,"+COM_LINEA_CREDITO+" lc "+
					"        ,com_tipo_cambio tc"   +
					"        ,comrel_pyme_epo_x_producto x"   +
					"        ,comrel_producto_epo pe"   +
					"        ,comcat_pyme p"   +
					"        ,comcat_epo e"   +
					"        ,comcat_moneda m"   +
					"        ,comcat_producto_nafin pn"   +
					"        ,comcat_estatus_docto ed"   +
					"        ,comcat_tipo_cobro_interes tci"   +
					"        ,comcat_cambio_estatus cce"   +
					"  WHERE doc.ic_documento = ds.ic_documento "   +
					"    AND doc.ic_documento = ce.ic_documento"   +
					"    AND doc.ic_epo = e.ic_epo"   +
					"    AND doc.ic_pyme = p.ic_pyme"   +
					"    AND doc.ic_producto_nafin = x.ic_producto_nafin"   +
					"    AND doc.ic_producto_nafin = pn.ic_producto_nafin"   +
					"    AND doc.ic_epo = x.ic_epo"   +
					"    AND doc.ic_pyme = x.ic_pyme"   +
					"    AND doc.ic_estatus_docto = ed.ic_estatus_docto"   +
					"   AND doc."+IC_LINEA_CREDITO+" = lc."+IC_LINEA_CREDITO+" "+
					"    AND doc.ic_epo = pe.ic_epo"   +
					"    AND doc.ic_producto_nafin = pe.ic_producto_nafin"   +
					"    AND lc.ic_moneda = m.ic_moneda"   +
					"    AND lc.ic_moneda = tc.ic_moneda"   +
					"    AND lc.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes"   +
					"    AND ce.ic_cambio_estatus = cce.ic_cambio_estatus"   +
					"    AND tc.dc_fecha IN (SELECT MAX (dc_fecha)  FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)"   +
					"    AND pn.ic_producto_nafin = 4 " +
					condicion.toString());
		}catch(Exception e){
			System.out.println("CambioEstatusEpoDE::getDocumentQueryFileException "+e);
		}
		return qrySentencia.toString();
	}
	
	/**
	 * Este m�todo regresa la Lista de parametros que ser�n usados
	 * como valor de las variables BIND de los queries generados.
	 * @return Lista con los valores a usar en las variables bind
	 */
	@Override
	public List getConditions() {
		return this.conditions;
	}
	
	public void setINoCliente(String iNoCliente) {
		this.iNoCliente = iNoCliente;
	}
	
	public String getINoCliente() {
		return iNoCliente;
	}

	public void setCtCredito(String ctCredito) {
		this.ctCredito = ctCredito;
	}

	public String getCtCredito() {
		return ctCredito;
	}

	public void setConditions(List<Object> conditions) {
		this.conditions = conditions;
	}

	public List get_conditions() {
		return this.conditions;
	}

	public void set_iNoCliente(String iNoCliente) {
		this.iNoCliente = iNoCliente;
	}

	public String get_iNoCliente() {
		return iNoCliente;
	}

	public void set_ctCredito(String ctCredito) {
		this.ctCredito = ctCredito;
	}

	public String get_ctCredito() {
		return ctCredito;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getIc_epo() {
		return ic_epo;
	}

	public void setIcDist(String icDist) {
		this.icDist = icDist;
	}

	public String getIcDist() {
		return icDist;
	}

	public void setNumDoc(String numDoc) {
		this.numDoc = numDoc;
	}

	public String getNumDoc() {
		return numDoc;
	}

	public void setFechaOper1(String fechaOper1) {
		this.fechaOper1 = fechaOper1;
	}

	public String getFechaOper1() {
		return fechaOper1;
	}

	public void setFechaOper2(String fechaOper2) {
		this.fechaOper2 = fechaOper2;
	}

	public String getFechaOper2() {
		return fechaOper2;
	}

	public void setMonto1(String monto1) {
		this.monto1 = monto1;
	}

	public String getMonto1() {
		return monto1;
	}

	public void setMonto2(String monto2) {
		this.monto2 = monto2;
	}

	public String getMonto2() {
		return monto2;
	}

	public void setRechazo(String rechazo) {
		this.rechazo = rechazo;
	}

	public String getRechazo() {
		return rechazo;
	}

	public void setFechaVenc1(String fechaVenc1) {
		this.fechaVenc1 = fechaVenc1;
	}

	public String getFechaVenc1() {
		return fechaVenc1;
	}

	public void setFechaVenc2(String fechaVenc2) {
		this.fechaVenc2 = fechaVenc2;
	}

	public String getFechaVenc2() {
		return fechaVenc2;
	}

	public void setTipoCoInt(String tipoCoInt) {
		this.tipoCoInt = tipoCoInt;
	}

	public String getTipoCoInt() {
		return tipoCoInt;
	}

	public void setFechaRe1(String fechaRe1) {
		this.fechaRe1 = fechaRe1;
	}

	public String getFechaRe1() {
		return fechaRe1;
	}

	public void setFechaRe2(String fechaRe2) {
		this.fechaRe2 = fechaRe2;
	}

	public String getFechaRe2() {
		return fechaRe2;
	}
}