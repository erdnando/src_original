package com.netro.distribuidores;


import com.netro.pdf.ComunesPDF;

import java.math.BigDecimal;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsDoctosSolicXEstatusDist  implements IQueryGeneratorRegExtJS {

	private final static Log log = ServiceLocator.getInstance().getLog(ConsDoctosSolicXEstatusDist.class);

	
	private String estatusDocto;
	private String estatusSolic;
	private String claveDocto;
	private List 		conditions ;

	RepDoctoSolicxEstatusDistBean qryBase=null;
	StringBuffer qrySentencia = new StringBuffer("");
	public ConsDoctosSolicXEstatusDist( RepDoctoSolicxEstatusDistBean rep) {
		qryBase=rep;
	}
	/**
	 * 
	 * @return 
	 */
	public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
	/**
	 * 
	 * @return 
	 */
	public String getDocumentQuery() {
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	/**
	 * 
	 * @return 
	 * @param pageIds
	 */
	public String getDocumentSummaryQueryForIds(List pageIds) {
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();

		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	/**
	 * 
	 * @return regresa la sentencia sql para generar la consulta 
	 */
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
			qryBase.setEstatusDocto(Integer.parseInt(estatusDocto));
			qryBase.setEstatusSolic(estatusSolic);
			qrySentencia.append(qryBase.getQueryPorEstatus());
			if(estatusDocto.equals("3") && estatusSolic.equals("D")||(estatusDocto.equals("24")&&estatusSolic.equals("D")) ||(estatusDocto.equals("32")&&estatusSolic.equals("D")) ){
				conditions.add(estatusDocto);
			}
		
		log.info("qrySentencia > "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
	/**
	 * 
	 * @return 
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
	 public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		log.info("crearPageCustomFile(E)");
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		try {
		
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
			} catch(Exception e) {}
		}
		log.info("crearPageCustomFile(S)");
		
		return nombreArchivo;
					
	}
	/**
	 * 
	 * @return	: retorna el nombre del archivo
	 * @param tipo : de archivo a generar ('PDF' o 'CSV' )
	 * @param path : ruta temporal dond se descargara el archivo
	 * @param rs	: 
	 * @param request : request
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		log.info("crearCustomFile (E)");
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer  contenidoArchivo = new StringBuffer();
		ComunesPDF pdfDoc = new ComunesPDF();
		//variables de despliegue en el pdf y/o csv
			String epo				= "";
			String distribuidor 	= "";
			String numDocto			= "";
			String numAcuseCarga	= "";
			String fechaEmision		= "";
			String fechaPublicacion	= "";
			String fechaVencimiento	= "";
			String plazoDocto		= "";
			String moneda 			= "";
			String icMoneda			= "";
			double monto 			= 0;
			String tipoConversion	= "";
			double tipoCambio		= 0;
			double montoValuado		= 0;
			String plazoDescuento	= "";
			String porcDescuento	= "";
			double montoDescontar	= 0;
			String modoPlazo		= "";
			String estatus 			= "";
			String icDocumento		= "";
			String intermediario	= "";
			String tipoCredito		= "";
			String fechaOperacion	= "";
			double montoCredito		= 0;
			String plazoCredito		= "";
			String fechaSeleccion	= "";
			String fechaVencCredito	= "";
			String fechaOperIF		= "";
			String referenciaTasaInt= "";
			double valorTasaInt		= 0;
			double montoTasaInt 	= 0;
			String tipoCobranza		= "";
			String tipoCobroInt		= ""; 
			String numeroPago	 	= "";
			String causa			= "";
			String fechaCambio		= "";
			String monedaLinea		= "";
			String codigoBin			= "";
      
      String COM_APLICABLE="";
      String MON_COMISION="";
      String MON_DEPOSITAR="";
      
      String ORDEN_ENVIADO="";
      String ID_OPERACION="";
      String CODIGO_AUTORIZA="";
      String tarjetaCredito="";
      
      
			//totales
			int		totalDoctosMN		= 0;
			int 	totalDoctosUSD		= 0;
			double	totalMontoMN		= 0;
			double	totalMontoUSD		= 0;
			double	totalMontoValuadoMN	= 0;
			double	totalMontoValuadoUSD= 0;
			double	totalMontoCreditoMN	= 0;
			double	totalMontoCreditoUSD= 0;
			String bandeVentaCartera ="";
			try {
				if(tipo.equals("PDF") ) {
					nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
					pdfDoc = new ComunesPDF(2, path + nombreArchivo);
					String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
					String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
					String diaActual    = fechaActual.substring(0,2);
					String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
					String anioActual   = fechaActual.substring(6,10);
					String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
							
					String pais=(String)session.getAttribute("strPais");
					String inonafin=(String)session.getAttribute("iNoNafinElectronico");
					String sesExterno=(String)session.getAttribute("sesExterno");
					String strNombre=(String) session.getAttribute("strNombre");
					String strNombreUsuario=(String) session.getAttribute("strNombreUsuario");
					String strLogo=(String)session.getAttribute("strLogo");
					String dirpub=(String)session.getServletContext().getAttribute("strDirectorioPublicacion");
					
					pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					(session.getAttribute("iNoNafinElectronico")==null?"":session.getAttribute("iNoNafinElectronico")).toString(),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
					
					pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
					pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);
					int columnas=0;
					int caso = Integer.parseInt(estatusDocto);
					switch (caso){
						case 1:
							if(estatusSolic.equals("C")){
								//columnas=27;
								columnas=15;
								pdfDoc.setTable(8);
								pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);
								pdfDoc.setCell("Seleccionada IF","formas",ComunesPDF.LEFT);
								pdfDoc.addTable();
								
								pdfDoc.setTable(columnas,100);
								/* A*/pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Distribuidor ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de acuse de carga ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de documento inicial ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de emisi�n ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de publicaci�n ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Plazo docto. ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Plazo para descuento en dias ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("% de descuento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto % de descuento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Tipo conv. ","celda01",ComunesPDF.CENTER);
								//pdfDoc.addTable();
								/*B*/pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Tipo cambio ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto valuado ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Modalidad de plazo ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de documento final ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Plazo ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de operaci�n distribuidor ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("IF ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Referencia tasa de inter�s ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Valor tasa de inter�s ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto de intereses ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto total de capital e inter�s ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
							}else if(estatusSolic.equals("D")){
								columnas=17;
								pdfDoc.setTable(8);
								pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);
								pdfDoc.setCell("No Negociable","formas",ComunesPDF.LEFT);
								pdfDoc.addTable();
								
								pdfDoc.setTable(columnas,100);
								pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Distribuidor ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de acuse de carga ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de documento inicial ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de emisi�n ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de publicaci�n ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Plazo docto. ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Plazo para descuento en dias ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("% de descuento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto % de descuento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Tipo conv. ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Tipo cambio ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto valuado ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Modalidad de plazo ","celda01",ComunesPDF.CENTER);
							}
							break;
						case 2:
							if(estatusSolic.equals("C")){
								columnas=15;
								pdfDoc.setTable(8);
								pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);
								pdfDoc.setCell("En Proceso","formas",ComunesPDF.LEFT);
								pdfDoc.addTable();
								
								pdfDoc.setTable(columnas,100);
								/* A*/pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Distribuidor ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de acuse de carga ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de documento inicial ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de emisi�n ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de publicaci�n ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Plazo docto. ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Plazo para descuento en dias ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("% de descuento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto % de descuento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Tipo conv. ","celda01",ComunesPDF.CENTER);
								
								/* B*/pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Tipo cambio ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto valuado ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Modalidad de plazo ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de documento final ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Plazo ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de operaci�n distribuidor ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("IF ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Referencia tasa de inter�s ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Valor tasa de inter�s ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto de intereses ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto total de capital e inter�s ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("","formas",ComunesPDF.CENTER);
							}else if(estatusSolic.equals("D")){
								columnas=17;
								pdfDoc.setTable(8);
								pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);
								pdfDoc.setCell("Negociable","formas",ComunesPDF.LEFT);
								pdfDoc.addTable();
								
								pdfDoc.setTable(columnas,100);
								pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Distribuidor ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de acuse de carga ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de documento inicial ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de emisi�n ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de publicaci�n ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Plazo docto. ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Plazo para descuento en dias ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("% de descuento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto % de descuento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Tipo conv. ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Tipo cambio ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto valuado ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Modalidad de plazo ","celda01",ComunesPDF.CENTER);
								
							}
							break;
						case 3:
						case 24:
							if(estatusSolic.equals("C")){
								columnas=15;
								pdfDoc.setTable(8);
								pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);
								pdfDoc.setCell("Operada","formas",ComunesPDF.LEFT);
								pdfDoc.addTable();
							}else if(estatusSolic.equals("D")){
								columnas=15;
								pdfDoc.setTable(8);
								pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);
								//MOD +(
								if(caso==3){
									pdfDoc.setCell("Seleccionada Pyme","formas",ComunesPDF.LEFT);
								}else{
									pdfDoc.setCell("En Proceso de Autorizacion IF","formas",ComunesPDF.LEFT);
								}
								//)
								pdfDoc.addTable();
							}
							
							pdfDoc.setTable(columnas,100);
							/* A*/pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Distribuidor ","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("N�mero de acuse de carga ","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("N�mero de documento inicial ","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Fecha de emisi�n ","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Fecha de publicaci�n ","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Plazo docto. ","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Monto ","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Plazo para descuento en dias ","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("% de descuento ","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Monto % de descuento ","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Tipo conv. ","celda01",ComunesPDF.CENTER);
							/* B*/pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Tipo cambio ","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Monto valuado ","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Modalidad de plazo ","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("N�mero de documento final ","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Monto ","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Plazo ","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Fecha de operaci�n distribuidor ","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("IF ","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Referencia tasa de inter�s ","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Valor tasa de inter�s ","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Monto de intereses ","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell("Monto total de capital e inter�s ","celda01",ComunesPDF.CENTER);
							/**/pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
							
							break;
						case 4://C
							if(estatusSolic.equals("C")){
								columnas=15;
								pdfDoc.setTable(8);
								pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);
								pdfDoc.setCell("Rechazada","formas",ComunesPDF.LEFT);
								pdfDoc.addTable();
								
								pdfDoc.setTable(columnas,100);
								/* A*/pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Distribuidor ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de acuse de carga ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de documento inicial ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de emisi�n ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de publicaci�n ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Plazo docto. ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Plazo para descuento en dias ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("% de descuento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto % de descuento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Tipo conv. ","celda01",ComunesPDF.CENTER);
								/* B*/pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Tipo cambio ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto valuado ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Modalidad de plazo ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de documento final ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Plazo ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de operaci�n distribuidor ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("IF ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Referencia tasa de inter�s ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Valor tasa de inter�s ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto de intereses ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto total de capital e inter�s ","celda01",ComunesPDF.CENTER);
								/* */pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
							}
							break;
						case 5://D
							if(estatusSolic.equals("D")){
								columnas=17;
								pdfDoc.setTable(8);
								pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);
								pdfDoc.setCell("Baja","formas",ComunesPDF.LEFT);
								pdfDoc.addTable();
								
								pdfDoc.setTable(columnas,100);
								pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Distribuidor ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de acuse de carga ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de documento inicial ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de emisi�n ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de publicaci�n ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Plazo docto. ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Plazo para descuento en dias ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("% de descuento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto % de descuento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Tipo conv. ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Tipo cambio ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto valuado ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Modalidad de plazo ","celda01",ComunesPDF.CENTER);
							}
							break;
						case 9://D
							if(estatusSolic.equals("D")){
								columnas=17;
								pdfDoc.setTable(8);
								pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);
								pdfDoc.setCell("Vencido sin Operar","formas",ComunesPDF.LEFT);
								pdfDoc.addTable();
								
								pdfDoc.setTable(columnas,100);
								pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Distribuidor ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de acuse de carga ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de documento inicial ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de emisi�n ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de publicaci�n ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Plazo docto. ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Plazo para descuento en dias ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("% de descuento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto % de descuento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Tipo conv. ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Tipo cambio ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto valuado ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Modalidad de plazo ","celda01",ComunesPDF.CENTER);
								
								
							}
							break;
						case 11: //D
							if(estatusSolic.equals("D")){
								columnas=15;
								pdfDoc.setTable(8);
								pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);
								pdfDoc.setCell("Operada Pagada","formas",ComunesPDF.LEFT);
								pdfDoc.addTable();
								
								pdfDoc.setTable(columnas,100);
								/* A*/pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Distribuidor ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de acuse de carga ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de documento inicial ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de emisi�n ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de publicaci�n ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Plazo docto. ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Plazo para descuento en dias ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("% de descuento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto % de descuento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Tipo conv. ","celda01",ComunesPDF.CENTER);
								/*B*/pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Tipo cambio ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto valuado ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Modalidad de plazo ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de documento final ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Plazo ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de operaci�n distribuidor ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("IF ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Referencia tasa de inter�s ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Valor tasa de inter�s ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto de intereses ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto total de capital e inter�s ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de pago ","celda01",ComunesPDF.CENTER);
							}
							break;
						case 20://D
							if(estatusSolic.equals("D")){
							columnas=16;
								pdfDoc.setTable(8);
								pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);
								pdfDoc.setCell("Rechazado IF","formas",ComunesPDF.LEFT);
								pdfDoc.addTable();
								
								pdfDoc.setTable(columnas,100);
								/* A*/pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Distribuidor ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de acuse de carga ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de documento inicial ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de emisi�n ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de publicaci�n ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Plazo docto. ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Plazo para descuento en dias ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("% de descuento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto % de descuento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Tipo conv. ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Tipo cambio ","celda01",ComunesPDF.CENTER);
								/* B*/pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto valuado ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Modalidad de plazo ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de documento final ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Plazo ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de operaci�n distribuidor ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("IF ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Referencia tasa de inter�s ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Valor tasa de inter�s ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto de intereses ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto total de capital e inter�s ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de Rechazo ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Causa ","celda01",ComunesPDF.CENTER);
								/* */pdfDoc.setCell("","celda01",ComunesPDF.CENTER);
							}
							break;
						case 22://D
							if(estatusSolic.equals("D")){
								columnas=15;
								pdfDoc.setTable(8);
								pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);
								pdfDoc.setCell("Pendiente de pago IF","formas",ComunesPDF.LEFT);
								pdfDoc.addTable();
								
								pdfDoc.setTable(columnas,100);
								/* A*/pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Distribuidor ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de acuse de carga ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de documento inicial ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de emisi�n ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de publicaci�n ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Plazo docto. ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Plazo para descuento en dias ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("% de descuento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto % de descuento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Tipo conv. ","celda01",ComunesPDF.CENTER);
								/* A*/pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Tipo cambio ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto valuado ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Modalidad de plazo ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de documento final ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Plazo ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de operaci�n distribuidor ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("IF ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Referencia tasa de inter�s ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Valor tasa de inter�s ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto de intereses ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto total de capital e inter�s ","celda01",ComunesPDF.CENTER); 
								pdfDoc.setCell("N�mero de pago de inter�s relacionado ","celda01",ComunesPDF.CENTER); 
							}
							break;
            case 32: //D
              if(estatusSolic.equals("D")){
								columnas=15;
								pdfDoc.setTable(8);
								pdfDoc.setCell("Estatus:","celda01",ComunesPDF.LEFT);
								pdfDoc.setCell("Operada TC","formas",ComunesPDF.LEFT);
								pdfDoc.addTable();
                
                pdfDoc.setTable(columnas,100);
                /* A*/pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Distribuidor ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de acuse de carga ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de documento inicial ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de emisi�n ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de vencimiento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de publicaci�n ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Plazo docto. ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Moneda ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Plazo para descuento en dias ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("% de descuento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto % de descuento ","celda01",ComunesPDF.CENTER);
/*----*/        pdfDoc.setCell("Modalidad de plazo ","celda01",ComunesPDF.CENTER);
                 /* B*/pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("N�mero de Documento Final","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("% Comision Aplicable de Terceros","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto Comisi�n de Terceros (IVA Incluido)","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Monto a Depositar por Operaci�n","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Plazo ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de Vencimiento ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Fecha de Operaci�n Distribuidor ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("If ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Nombre del Producto","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Id Orden Enviado ","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("Respuesta de Operaci�n","celda01",ComunesPDF.CENTER);
								pdfDoc.setCell("C�digo Autorizaci�n","celda01",ComunesPDF.CENTER);
/*----*/        			pdfDoc.setCell("N�mero Tarjeta de Cr�dito","celda01",ComunesPDF.CENTER);                           
              }
              break;
					}
				}// fin if tipo==PDF
				if(tipo.equals("CSV")){
					int caso = Integer.parseInt(estatusDocto);
					switch (caso){
						case 1:
							if(estatusSolic.equals("C")){
								contenidoArchivo.append(" ESTATUS: , Seleccionada IF ");
								contenidoArchivo.append(" \n "); 
								contenidoArchivo.append(" \n ");
								contenidoArchivo.append(" EPO,	Distribuidor,	N�m. Acuse de Carga,	N�m. docto.inicial,	Fecha de emisi�n,	Fecha de vencimiento,"+ 
																"	Fecha de Publicaci�n,	Plazo docto.,	Moneda,	Monto,	Plazo para descuento en dias,	% de descuento,"+
																"	Monto % de descuento,	Tipo conv.,	Tipo cambio,	Monto valuado,	Modalidad de plazo,	N�m.docto.final,"+
																"	Monto,	Plazo,	Fecha de Vencimiento,	Fecha de Operaci�n Distribuidor,	IF,	Referencia tasa de inter�s,"+
																"	Valor tasa de inter�s,	Monto de Intereses,	Monto Total de Capital e Inter�s");
								contenidoArchivo.append(" \n ");
							}else if(estatusSolic.equals("D")){
								contenidoArchivo.append(" ESTATUS: No Negociable ");
								contenidoArchivo.append(" \n "); 
								contenidoArchivo.append(" \n ");
								contenidoArchivo.append(" EPO,	Distribuidor,	N�m. Acuse de Carga,	N�m. docto.inicial,	Fecha de emisi�n,	Fecha de vencimiento,"+ 
																"	Fecha de Publicaci�n,	Plazo docto.,	Moneda,	Monto,	Plazo para descuento en dias,	% de descuento,"+
																"	Monto % de descuento,	Tipo conv.,	Tipo cambio,	Monto valuado,	Modalidad de plazo");
								contenidoArchivo.append(" \n ");
							}
							break;
						case 2:
							if(estatusSolic.equals("C")){
								contenidoArchivo.append(" ESTATUS: En proceso ");
								contenidoArchivo.append(" \n "); 
								contenidoArchivo.append(" \n ");
								contenidoArchivo.append(" EPO,	Distribuidor,	N�m. Acuse de Carga,	N�m. docto.inicial,	Fecha de emisi�n,	Fecha de vencimiento,"+ 
																"	Fecha de Publicaci�n,	Plazo docto.,	Moneda,	Monto,	Plazo para descuento en dias,	% de descuento,"+
																"	Monto % de descuento,	Tipo conv.,	Tipo cambio,	Monto valuado,	Modalidad de plazo,	N�m.docto.final,"+
																"	Monto,	Plazo,	Fecha de Vencimiento,	Fecha de Operaci�n Distribuidor,	IF,	Referencia tasa de inter�s,"+
																"	Valor tasa de inter�s,	Monto de Intereses,	Monto Total de Capital e Inter�s");
								contenidoArchivo.append(" \n ");
							}else if(estatusSolic.equals("D")){
								contenidoArchivo.append(" ESTATUS: Negociable ");
								contenidoArchivo.append(" \n "); 
								contenidoArchivo.append(" \n ");
								contenidoArchivo.append(" EPO,	Distribuidor,	N�m. Acuse de Carga,	N�m. docto.inicial,	Fecha de emisi�n,	Fecha de vencimiento,"+ 
																"	Fecha de Publicaci�n,	Plazo docto.,	Moneda,	Monto,	Plazo para descuento en dias,	% de descuento,"+
																"	Monto % de descuento,	Tipo conv.,	Tipo cambio,	Monto valuado,	Modalidad de plazo");
								contenidoArchivo.append(" \n ");
							}
							break;
						case 3:
		  /*MOD +(*/case 24: //)
							if(estatusSolic.equals("C")){
								contenidoArchivo.append(" ESTATUS: Operada ");
							}else if(estatusSolic.equals("D")){
								if(caso==3){
									contenidoArchivo.append(" ESTATUS: Seleccionada Pyme ");
								}else{
									contenidoArchivo.append(" ESTATUS: En Proceso de Autorizacion IF ");
								}
							}
								contenidoArchivo.append(" \n "); 
								contenidoArchivo.append(" \n ");
								contenidoArchivo.append(" EPO,	Distribuidor,	N�m. Acuse de Carga,	N�m. docto.inicial,	Fecha de emisi�n,	Fecha de vencimiento,"+ 
																"	Fecha de Publicaci�n,	Plazo docto.,	Moneda,	Monto,	Plazo para descuento en dias,	% de descuento,"+
																"	Monto % de descuento,	Tipo conv.,	Tipo cambio,	Monto valuado,	Modalidad de plazo,	N�m.docto.final,"+
																"	Monto,	Plazo,	Fecha de Vencimiento,	Fecha de Operaci�n Distribuidor,	IF,	Referencia tasa de inter�s,"+
																"	Valor tasa de inter�s,	Monto de Intereses,	Monto Total de Capital e Inter�s");
								contenidoArchivo.append(" \n ");
							break;
						case 4://C
							if(estatusSolic.equals("C")){
								contenidoArchivo.append(" ESTATUS: Rechazada ");
								contenidoArchivo.append(" \n "); 
								contenidoArchivo.append(" \n ");
								contenidoArchivo.append(" EPO,	Distribuidor,	N�m. Acuse de Carga,	N�m. docto.inicial,	Fecha de emisi�n,	Fecha de vencimiento,"+ 
																"	Fecha de Publicaci�n,	Plazo docto.,	Moneda,	Monto,	Plazo para descuento en dias,	% de descuento,"+
																"	Monto % de descuento,	Tipo conv.,	Tipo cambio,	Monto valuado,	Modalidad de plazo,	N�m.docto.final,"+
																"	Monto,	Plazo,	Fecha de Vencimiento,	Fecha de Operaci�n Distribuidor,	IF,	Referencia tasa de inter�s,"+
																"	Valor tasa de inter�s,	Monto de Intereses,	Monto Total de Capital e Inter�s");
								contenidoArchivo.append(" \n ");
							}
							break;
						case 5://D
							if(estatusSolic.equals("D")){
								contenidoArchivo.append(" ESTATUS: Baja ");
								contenidoArchivo.append(" \n "); 
								contenidoArchivo.append(" \n ");
								contenidoArchivo.append(" EPO,	Distribuidor,	N�m. Acuse de Carga,	N�m. docto.inicial,	Fecha de emisi�n,	Fecha de vencimiento,"+ 
																"	Fecha de Publicaci�n,	Plazo docto.,	Moneda,	Monto,	Plazo para descuento en dias,	% de descuento,"+
																"	Monto % de descuento,	Tipo conv.,	Tipo cambio,	Monto valuado,	Modalidad de plazo");
								contenidoArchivo.append(" \n ");
							}
							break;
						case 9://D
							if(estatusSolic.equals("D")){
								contenidoArchivo.append(" ESTATUS: Vencido sin Operar ");
								contenidoArchivo.append(" \n "); 
								contenidoArchivo.append(" \n ");
								contenidoArchivo.append(" EPO,	Distribuidor,	N�m. Acuse de Carga,	N�m. docto.inicial,	Fecha de emisi�n,	Fecha de vencimiento,"+ 
																"	Fecha de Publicaci�n,	Plazo docto.,	Moneda,	Monto,	Plazo para descuento en dias,	% de descuento,"+
																"	Monto % de descuento,	Tipo conv.,	Tipo cambio,	Monto valuado,	Modalidad de plazo");
								contenidoArchivo.append(" \n ");
							}
							break;
						case 11: //D
							if(estatusSolic.equals("D")){
								contenidoArchivo.append(" ESTATUS: Operada Pagada ");
								contenidoArchivo.append(" \n "); 
								contenidoArchivo.append(" \n ");
								contenidoArchivo.append(" EPO,	Distribuidor,	N�m. Acuse de Carga,	N�m. docto.inicial,	Fecha de emisi�n,	Fecha de vencimiento,"+ 
																"	Fecha de Publicaci�n,	Plazo docto.,	Moneda,	Monto,	Plazo para descuento en dias,	% de descuento,"+
																"	Monto % de descuento,	Tipo conv.,	Tipo cambio,	Monto valuado,	Modalidad de plazo,	N�m.docto.final,"+
																"	Monto,	Plazo,	Fecha de Vencimiento,	Fecha de Operaci�n Distribuidor,	IF,	Referencia tasa de inter�s,"+
																"	Valor tasa de inter�s,	Monto de Intereses,	Monto Total de Capital e Inter�s, N�mero de pago");
								contenidoArchivo.append(" \n ");
							}
							break;
						case 20://D
							if(estatusSolic.equals("D")){
								contenidoArchivo.append(" ESTATUS: Rechazada IF ");
								contenidoArchivo.append(" \n "); 
								contenidoArchivo.append(" \n ");
								contenidoArchivo.append(" EPO,	Distribuidor,	N�m. Acuse de Carga,	N�m. docto.inicial,	Fecha de emisi�n,	Fecha de vencimiento,"+ 
																"	Fecha de Publicaci�n,	Plazo docto.,	Moneda,	Monto,	Plazo para descuento en dias,	% de descuento,"+
																"	Monto % de descuento,	Tipo conv.,	Tipo cambio,	Monto valuado,	Modalidad de plazo,	N�m.docto.final,"+
																"	Monto,	Plazo,	Fecha de Vencimiento,	Fecha de Operaci�n Distribuidor,	IF,	Referencia tasa de inter�s,"+
																"	Valor tasa de inter�s,	Monto de Intereses,	Monto Total de Capital e Inter�s, Fecha de Rechazo, Causa");
								contenidoArchivo.append(" \n ");
							}
							break;
						case 22://D
							if(estatusSolic.equals("D")){
								contenidoArchivo.append(" ESTATUS: Pendiente de pago IF ");
								contenidoArchivo.append(" \n "); 
								contenidoArchivo.append(" \n ");
								contenidoArchivo.append(" EPO,	Distribuidor,	N�m. Acuse de Carga,	N�m. docto.inicial,	Fecha de emisi�n,	Fecha de vencimiento,"+ 
																"	Fecha de Publicaci�n,	Plazo docto.,	Moneda,	Monto,	Plazo para descuento en dias,	% de descuento,"+
																"	Monto % de descuento,	Tipo conv.,	Tipo cambio,	Monto valuado,	Modalidad de plazo,	N�m.docto.final,"+
																"	Monto,	Plazo,	Fecha de Vencimiento,	Fecha de Operaci�n Distribuidor,	IF,	Referencia tasa de inter�s,"+
																"	Valor tasa de inter�s,	Monto de Intereses,	Monto Total de Capital e Inter�s,  N�mero de pago de inter�s relacionado");
								contenidoArchivo.append(" \n ");
							}
							break;
            case 32://D
							if(estatusSolic.equals("D")){
                  contenidoArchivo.append(" ESTATUS: Operada TC");
								contenidoArchivo.append(" \n "); 
								contenidoArchivo.append(" \n ");
								contenidoArchivo.append(" EPO,	Distribuidor,	N�m. Acuse de Carga,	N�m. docto.inicial,	Fecha de emisi�n,	Fecha de vencimiento,"+ 
																"	Fecha de Publicaci�n,	Plazo docto.,	Moneda,	Monto,	Plazo para descuento en dias,	% de descuento,"+
																"	Monto % de descuento,	Modalidad Plazo,N�mero docto. Final,	Monto,	% Comision Aplicable de Terceros,Monto Comisi�n de Terceros (IVA Incluido),"+
																"	Monto a Depositar por Operaci�n,	Plazo,	Fecha de Vencimiento,	Fecha de Operaci�n Distribuidor,	IF,Nombre del Producto, " +
																"	Id Orden Enviado,Respuesta de Operaci�n,C�digo Autorizaci�n,N�mero Tarjeta de Cr�dito");
								contenidoArchivo.append(" \n ");
							}
							break;
					}
				}//fin if tipo==CVS
			if((tipo.equals("PDF") || tipo.equals("CSV")) && 
				(estatusDocto.equals("1")|| estatusDocto.equals("2")||estatusDocto.equals("3")||estatusDocto.equals("4"))&&
				estatusSolic.equals("C")){
				while(rs.next()){
					epo			 		= (rs.getString("NOMBRE_EPO")==null)?"":rs.getString("NOMBRE_EPO");
					distribuidor 		= (rs.getString("NOMBRE_DIST")==null)?"":rs.getString("NOMBRE_DIST");
					numDocto			= (rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO");
					numAcuseCarga		= (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
					fechaEmision		= (rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION");
					fechaPublicacion	= (rs.getString("DF_CARGA")==null)?"":rs.getString("DF_CARGA");
					fechaVencimiento	= (rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC");
					plazoDocto			= (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");
					moneda 				= (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
					monto 				= rs.getDouble("FN_MONTO");
					tipoConversion		= (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");
					tipoCambio			= rs.getDouble("TIPO_CAMBIO");
					plazoDescuento		= (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
					porcDescuento		= (rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO");
					montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
					montoValuado		= (monto-montoDescontar)*tipoCambio;
					monedaLinea			= (rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA");
					modoPlazo			= (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
					estatus 			= (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");
					icMoneda			= (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");
					icDocumento			= (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
					tipoCobranza		= (rs.getString("TIPO_COBRANZA")==null)?"":rs.getString("TIPO_COBRANZA");
					icDocumento			= (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
					intermediario		= (rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF");
					tipoCredito			= (rs.getString("TIPO_CREDITO")==null)?"":rs.getString("TIPO_CREDITO");
					montoCredito		= rs.getDouble("MONTO_CREDITO");
					plazoCredito		= (rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO");
					tipoCobroInt		= (rs.getString("TIPO_COBRO_INT")==null)?"":rs.getString("TIPO_COBRO_INT");
					referenciaTasaInt	= (rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT");
					valorTasaInt		= rs.getDouble("VALOR_TASA_INT");
					montoTasaInt		= rs.getDouble("MONTO_TASA_INT");
					fechaSeleccion		= (rs.getString("FECHA_SELECCION")==null)?"":rs.getString("FECHA_SELECCION");
					fechaVencCredito	= (rs.getString("FECHA_VENC_CREDITO")==null)?"":rs.getString("FECHA_VENC_CREDITO");
					fechaOperIF			= (rs.getString("FECHA_OPER_IF")==null)?"":rs.getString("FECHA_OPER_IF");
					bandeVentaCartera			= (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
				  if (bandeVentaCartera.equals("S") ) {
						modoPlazo ="";
				 }
					if("1".equals(icMoneda)){
						totalDoctosMN ++;
						totalMontoMN += monto;
						totalMontoValuadoMN += montoValuado;
						totalMontoCreditoMN += montoCredito;
					}else{
						totalDoctosUSD ++;
						totalMontoUSD += monto;
						if(!icMoneda.equals(monedaLinea))
							totalMontoValuadoUSD += montoValuado;
						totalMontoCreditoUSD += montoCredito;
					}
					if(tipo.equals("CSV")){
						contenidoArchivo.append( epo.trim().replaceAll(","," ") +","+distribuidor.trim().replaceAll(","," ")+","+
						numAcuseCarga+","+numDocto+","+fechaEmision+","+fechaVencimiento+","+
						fechaPublicacion+","+plazoDocto+","+moneda+","+monto+","+plazoDescuento+","+porcDescuento+","+
						montoDescontar+","+
						((icMoneda.equals(monedaLinea))?"":tipoConversion)+","+
						((icMoneda.equals(monedaLinea))?"":""+tipoCambio)+","+
						((icMoneda.equals(monedaLinea))?"":""+montoValuado)+","+
						modoPlazo+","+
						icDocumento+","+
						montoCredito+","+
						plazoCredito+","+
						fechaVencCredito+","+
						fechaSeleccion+","+
						intermediario.trim().replaceAll(","," ")+","+
						referenciaTasaInt.replace(',',' ')+","+
						valorTasaInt+"%,"+montoTasaInt+","+
						(montoCredito+montoTasaInt)+"\n");
					}if(tipo.equals("PDF")){
						/* A*/pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(epo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(distribuidor,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(numAcuseCarga,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(numDocto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaEmision,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaPublicacion,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(plazoDocto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+monto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(plazoDescuento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("% "+porcDescuento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+montoDescontar,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(((icMoneda.equals(monedaLinea))?"":tipoConversion),"formas",ComunesPDF.CENTER);
						
						/*B*/pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(((icMoneda.equals(monedaLinea))?"":""+tipoCambio),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(((icMoneda.equals(monedaLinea))?"":""+montoValuado),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(modoPlazo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(icDocumento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+montoCredito,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(plazoCredito,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaVencCredito,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaSeleccion,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(intermediario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(referenciaTasaInt.replace(',',' '),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(valorTasaInt+"%","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+montoTasaInt,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+(montoCredito+montoTasaInt),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					}
				}//fi while
			}//fin de if 	
			if((tipo.equals("PDF") || tipo.equals("CSV")) &&
				(estatusDocto.equals("3") && estatusSolic.equals("D")||(estatusDocto.equals("24")&&estatusSolic.equals("D")) )	){//MOD +(||(estatusDocto.equals("24")&&estatusSolic.equals("D")))
				while(rs.next()){
					epo			 		= (rs.getString("NOMBRE_EPO")==null)?"":rs.getString("NOMBRE_EPO");
					distribuidor 		= (rs.getString("NOMBRE_DIST")==null)?"":rs.getString("NOMBRE_DIST");
					numDocto			= (rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO");
					numAcuseCarga		= (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
					fechaEmision		= (rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION");
					fechaPublicacion	= (rs.getString("DF_CARGA")==null)?"":rs.getString("DF_CARGA");
					fechaVencimiento	= (rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC");
					plazoDocto			= (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");
					moneda 				= (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
					monto 				= rs.getDouble("FN_MONTO");
					tipoConversion		= (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");
					tipoCambio			= rs.getDouble("TIPO_CAMBIO");
					plazoDescuento		= (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
					porcDescuento		= (rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO");
					montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
					montoValuado		= (monto-montoDescontar)*tipoCambio;
					modoPlazo			= (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
					estatus 			= (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");
					icMoneda			= (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");
					icDocumento			= (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
					tipoCobranza		= (rs.getString("TIPO_COBRANZA")==null)?"":rs.getString("TIPO_COBRANZA");
					icDocumento			= (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
					intermediario		= (rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF");
					tipoCredito			= (rs.getString("TIPO_CREDITO")==null)?"":rs.getString("TIPO_CREDITO");
					montoCredito		= rs.getDouble("MONTO_CREDITO");
					plazoCredito		= (rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO");
					tipoCobroInt		= (rs.getString("TIPO_COBRO_INT")==null)?"":rs.getString("TIPO_COBRO_INT");
					referenciaTasaInt	= (rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT");
					valorTasaInt		= rs.getDouble("VALOR_TASA_INT");
					montoTasaInt		= rs.getDouble("MONTO_TASA_INT");
					fechaSeleccion		= (rs.getString("FECHA_SELECCION")==null)?"":rs.getString("FECHA_SELECCION");
					fechaVencCredito	= (rs.getString("FECHA_VENC_CREDITO")==null)?"":rs.getString("FECHA_VENC_CREDITO");
					monedaLinea			= (rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA");
					bandeVentaCartera			= (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
				  if (bandeVentaCartera.equals("S") ) {
						modoPlazo ="";
				 }
					if("1".equals(icMoneda)){
						totalDoctosMN ++;
						totalMontoMN += monto;
						totalMontoValuadoMN += montoValuado;
						totalMontoCreditoMN += montoCredito;
					}else{
						totalDoctosUSD ++;
						totalMontoUSD += monto;
						if(!icMoneda.equals(monedaLinea))
							totalMontoValuadoUSD += montoValuado;
						totalMontoCreditoUSD += montoCredito;
					}
					if(tipo.equals("CSV")){
						contenidoArchivo.append( epo.trim().replaceAll(","," ")+","+distribuidor.trim().replaceAll(","," ")+","+
						numAcuseCarga+","+numDocto+","+fechaEmision+","+fechaVencimiento+","+
						fechaPublicacion+","+plazoDocto+","+moneda+","+monto+","+plazoDescuento+","+porcDescuento+","+
						montoDescontar+","+
						((icMoneda.equals(monedaLinea))?"":tipoConversion)+","+
						((icMoneda.equals(monedaLinea))?"":""+tipoCambio)+","+
						((icMoneda.equals(monedaLinea))?"":""+montoValuado)+","+
						modoPlazo+","+
						icDocumento+","+
						montoCredito+","+
						plazoCredito+","+
						fechaVencCredito+","+
						fechaSeleccion+","+
						intermediario.trim().replaceAll(","," ")+","+
						referenciaTasaInt.replace(',',' ')+","+
						valorTasaInt+"%,"+montoTasaInt+","+
						(montoCredito+montoTasaInt)+"\n");
					}if(tipo.equals("PDF")){
					/* A*/pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(epo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(distribuidor,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(numAcuseCarga,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(numDocto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaEmision,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaPublicacion,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(plazoDocto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+monto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(plazoDescuento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("% "+porcDescuento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+montoDescontar,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(((icMoneda.equals(monedaLinea))?"":tipoConversion),"formas",ComunesPDF.CENTER);
						/* B*/pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(((icMoneda.equals(monedaLinea))?"":""+tipoCambio),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(((icMoneda.equals(monedaLinea))?"":""+montoValuado),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(modoPlazo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(icDocumento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+montoCredito,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(plazoCredito,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaVencCredito,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaSeleccion,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(intermediario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(referenciaTasaInt.replace(',',' '),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(valorTasaInt+"%","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+montoTasaInt,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+(montoCredito+montoTasaInt),"formas",ComunesPDF.CENTER);
						/* */pdfDoc.setCell("","formas",ComunesPDF.CENTER);
					}
				}//fin while
			}	//fin de if 3D
			if((tipo.equals("PDF")|| tipo.equals("CSV")) &&
				(estatusDocto.equals("1")||estatusDocto.equals("2")||estatusDocto.equals("5")||estatusDocto.equals("9"))&&
				estatusSolic.equals("D")	){
					while(rs.next()){
						epo			 		= (rs.getString("NOMBRE_EPO")==null)?"":rs.getString("NOMBRE_EPO");
						distribuidor 		= (rs.getString("NOMBRE_DIST")==null)?"":rs.getString("NOMBRE_DIST");
						numDocto			= (rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO");
						numAcuseCarga		= (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
						fechaEmision		= (rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION");
						fechaPublicacion	= (rs.getString("DF_CARGA")==null)?"":rs.getString("DF_CARGA");
						fechaVencimiento	= (rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC");
						plazoDocto			= (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");
						moneda 				= (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
						monto 				= rs.getDouble("FN_MONTO");
						tipoConversion		= (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");
						tipoCambio			= rs.getDouble("TIPO_CAMBIO");
						plazoDescuento		= (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
						porcDescuento		= (rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO");
						montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
						montoValuado		= (monto-montoDescontar)*tipoCambio;
						modoPlazo			= (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
						estatus 			= (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");
						icMoneda			= (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");
						icDocumento			= (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
						tipoCobranza		= (rs.getString("TIPO_COBRANZA")==null)?"":rs.getString("TIPO_COBRANZA");
						bandeVentaCartera			= (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
						if (bandeVentaCartera.equals("S") ) {
							modoPlazo ="";
						}
						if("1".equals(icMoneda)){
							totalDoctosMN ++;
							totalMontoMN += monto;
							totalMontoValuadoMN += montoValuado;
						}else{
							totalDoctosUSD ++;
							totalMontoUSD += monto;
							totalMontoValuadoUSD += montoValuado;
						}
						if(tipo.equals("CSV")){
							contenidoArchivo.append( epo.trim().replaceAll(","," ")+","+distribuidor.trim().replaceAll(","," ")+","+
							numAcuseCarga+","+numDocto+","+fechaEmision+","+
							fechaVencimiento+","+fechaPublicacion+","+plazoDocto+","+moneda+","+monto+","+
							plazoDescuento+","+porcDescuento+","+montoDescontar+","+((tipoCambio==1)?"":tipoConversion)+","+
							((tipoCambio==1)?"":""+tipoCambio)+","+((tipoCambio==1)?"":""+montoValuado)+","+	modoPlazo+"\n");
						}
						if(tipo.equals("PDF")){
							pdfDoc.setCell(epo,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(distribuidor,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(numAcuseCarga,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(numDocto,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(fechaEmision,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(fechaPublicacion,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(plazoDocto,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$ "+monto,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(plazoDescuento,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("% "+porcDescuento,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$ "+montoDescontar,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(((icMoneda.equals(monedaLinea))?"":tipoConversion),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(((icMoneda.equals(monedaLinea))?"":""+tipoCambio),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(((icMoneda.equals(monedaLinea))?"":""+montoValuado),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(modoPlazo,"formas",ComunesPDF.CENTER);
						}
					}//fin while de 1,2,5, 9 D
			}//fin de if 1,2,5, 9 D
			if((tipo.equals("PDF")||tipo.equals("CSV"))&&
					estatusDocto.equals("11")&& estatusSolic.equals("D")	){
					while(rs.next()){
							epo			 		= (rs.getString("NOMBRE_EPO")==null)?"":rs.getString("NOMBRE_EPO");
							distribuidor 		= (rs.getString("NOMBRE_DIST")==null)?"":rs.getString("NOMBRE_DIST");
							numDocto			= (rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO");
							numAcuseCarga		= (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
							fechaEmision		= (rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION");
							fechaPublicacion	= (rs.getString("DF_CARGA")==null)?"":rs.getString("DF_CARGA");
							fechaVencimiento	= (rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC");
							plazoDocto			= (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");
							moneda 				= (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
							monto 				= rs.getDouble("FN_MONTO");
							tipoConversion		= (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");
							tipoCambio			= rs.getDouble("TIPO_CAMBIO");
							monedaLinea			= (rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA");
							plazoDescuento		= (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
							porcDescuento		= (rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO");
							montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
							montoValuado		= (monto-montoDescontar)*tipoCambio;
							modoPlazo			= (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
							estatus 			= (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");
							icMoneda			= (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");
							icDocumento			= (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
							tipoCobranza		= (rs.getString("TIPO_COBRANZA")==null)?"":rs.getString("TIPO_COBRANZA");
							icDocumento			= (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
							intermediario		= (rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF");
							tipoCredito			= (rs.getString("TIPO_CREDITO")==null)?"":rs.getString("TIPO_CREDITO");
							montoCredito		= rs.getDouble("MONTO_CREDITO");
							plazoCredito		= (rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO");
							tipoCobroInt		= (rs.getString("TIPO_COBRO_INT")==null)?"":rs.getString("TIPO_COBRO_INT");
							referenciaTasaInt	= (rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT");
							valorTasaInt		= rs.getDouble("VALOR_TASA_INT");
							montoTasaInt		= rs.getDouble("MONTO_TASA_INT");
							numeroPago			= (rs.getString("CG_NUMERO_PAGO")==null)?"":rs.getString("CG_NUMERO_PAGO");
							fechaSeleccion		= (rs.getString("FECHA_SELECCION")==null)?"":rs.getString("FECHA_SELECCION");
							fechaVencCredito	= (rs.getString("FECHA_VENC_CREDITO")==null)?"":rs.getString("FECHA_VENC_CREDITO");
							fechaOperIF			= (rs.getString("FECHA_OPER_IF")==null)?"":rs.getString("FECHA_OPER_IF");
							bandeVentaCartera			= (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
						if (bandeVentaCartera.equals("S") ) {
								modoPlazo ="";
						 }
						if("1".equals(icMoneda)){
							totalDoctosMN ++;
							totalMontoMN += monto;
							totalMontoValuadoMN += montoValuado;
							totalMontoCreditoMN += montoCredito;
						}else{
							totalDoctosUSD ++;
							totalMontoUSD += monto;
							if(!icMoneda.equals(monedaLinea))
								totalMontoValuadoUSD += montoValuado;
							totalMontoCreditoUSD += montoCredito;
						}
						
					if(tipo.equals("CSV")){
						contenidoArchivo.append( epo.trim().replaceAll(","," ")+","+distribuidor.trim().replaceAll(","," ")+","+
						numAcuseCarga+","+numDocto+","+fechaEmision+","+fechaVencimiento+","+
						fechaPublicacion+","+plazoDocto+","+moneda+","+monto+","+plazoDescuento+","+porcDescuento+","+
						montoDescontar+","+
						((icMoneda.equals(monedaLinea))?"":tipoConversion)+","+
						((icMoneda.equals(monedaLinea))?"":""+tipoCambio)+","+
						((icMoneda.equals(monedaLinea))?"":""+montoValuado)+","+
						modoPlazo+","+
						icDocumento+","+
						montoCredito+","+
						plazoCredito+","+
						fechaVencCredito+","+
						fechaSeleccion+","+
						intermediario.trim().replaceAll(","," ")+","+
						referenciaTasaInt.replace(',',' ')+","+
						valorTasaInt+"%,"+montoTasaInt+","+
						(montoCredito+montoTasaInt)+","+
						numeroPago+"\n");
					}
					if(tipo.equals("PDF")){
					/* A*/pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(epo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(distribuidor,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(numAcuseCarga,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(numDocto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaEmision,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaPublicacion,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(plazoDocto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+monto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(plazoDescuento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("% "+porcDescuento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+montoDescontar,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(((icMoneda.equals(monedaLinea))?"":tipoConversion),"formas",ComunesPDF.CENTER);
						/* B*/pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(((icMoneda.equals(monedaLinea))?"":""+tipoCambio),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(((icMoneda.equals(monedaLinea))?"":""+montoValuado),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(modoPlazo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(icDocumento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+montoCredito,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(plazoCredito,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaVencCredito,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaSeleccion,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(intermediario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(referenciaTasaInt.replace(',',' '),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(valorTasaInt+"%","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+montoTasaInt,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+(montoCredito+montoTasaInt),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(numeroPago,"formas",ComunesPDF.CENTER);
					}	
				}//fin de While 11D
			}//finde if 11D
			if((tipo.equals("PDF")||tipo.equals("CSV"))&&
					estatusDocto.equals("20")&& estatusSolic.equals("D")	){
					while(rs.next()){
						epo			 		= (rs.getString("NOMBRE_EPO")==null)?"":rs.getString("NOMBRE_EPO");
						distribuidor 		= (rs.getString("NOMBRE_DIST")==null)?"":rs.getString("NOMBRE_DIST");
						numDocto			= (rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO");
						numAcuseCarga		= (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
						fechaEmision		= (rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION");
						fechaPublicacion	= (rs.getString("DF_CARGA")==null)?"":rs.getString("DF_CARGA");
						fechaVencimiento	= (rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC");
						plazoDocto			= (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");
						moneda 				= (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
						monto 				= rs.getDouble("FN_MONTO");
						tipoConversion		= (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");
						tipoCambio			= rs.getDouble("TIPO_CAMBIO");
						monedaLinea			= (rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA");
						plazoDescuento		= (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
						porcDescuento		= (rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO");
						montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
						montoValuado		= (monto-montoDescontar)*tipoCambio;
						modoPlazo			= (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
						estatus 			= (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");
						icMoneda			= (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");
						icDocumento			= (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
						tipoCobranza		= (rs.getString("TIPO_COBRANZA")==null)?"":rs.getString("TIPO_COBRANZA");
						icDocumento			= (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
						intermediario		= (rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF");
						tipoCredito			= (rs.getString("TIPO_CREDITO")==null)?"":rs.getString("TIPO_CREDITO");
						montoCredito		= rs.getDouble("MONTO_CREDITO");
						plazoCredito		= (rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO");
						tipoCobroInt		= (rs.getString("TIPO_COBRO_INT")==null)?"":rs.getString("TIPO_COBRO_INT");
						referenciaTasaInt	= (rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT");
						valorTasaInt		= rs.getDouble("VALOR_TASA_INT");
						montoTasaInt		= rs.getDouble("MONTO_TASA_INT");
						causa				= (rs.getString("CT_CAMBIO_MOTIVO")==null)?"":rs.getString("CT_CAMBIO_MOTIVO");
						fechaSeleccion		= (rs.getString("FECHA_SELECCION")==null)?"":rs.getString("FECHA_SELECCION");
						fechaVencCredito	= (rs.getString("FECHA_VENC_CREDITO")==null)?"":rs.getString("FECHA_VENC_CREDITO");
						fechaCambio			= (rs.getString("FECHA_CAMBIO")==null)?"":rs.getString("FECHA_CAMBIO");
						bandeVentaCartera			= (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
						if (bandeVentaCartera.equals("S") ) {
							modoPlazo ="";
						}
						if("1".equals(icMoneda)){
							totalDoctosMN ++;
							totalMontoMN += monto;
							totalMontoValuadoMN += montoValuado;
							totalMontoCreditoMN += montoCredito;
						}else{
							totalDoctosUSD ++;
							totalMontoUSD += monto;
							if(!icMoneda.equals(monedaLinea))
								totalMontoValuadoUSD += montoValuado;
							totalMontoCreditoUSD += montoCredito;
						}
						if(tipo.equals("CSV")){
							contenidoArchivo.append( epo.trim().replaceAll(","," ")+","+distribuidor.trim().replaceAll(","," ")+","+
							numAcuseCarga+","+numDocto+","+fechaEmision+","+fechaVencimiento+","+
							fechaPublicacion+","+plazoDocto+","+moneda+","+monto+","+plazoDescuento+","+porcDescuento+","+
							montoDescontar+","+
							((icMoneda.equals(monedaLinea))?"":tipoConversion)+","+
							((icMoneda.equals(monedaLinea))?"":""+tipoCambio)+","+
							((icMoneda.equals(monedaLinea))?"":""+montoValuado)+","+
							modoPlazo+","+
							icDocumento+","+
							montoCredito+","+
							plazoCredito+","+
							fechaVencCredito+","+
							fechaSeleccion+","+
							intermediario.trim().replaceAll(","," ")+","+
							referenciaTasaInt.replace(',',' ')+","+
							valorTasaInt+"%,"+montoTasaInt+","+
							(montoCredito+montoTasaInt)+","+
							fechaCambio+","+causa+"\n");
						}
						if(tipo.equals("PDF")){
						/* A*/pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell(epo,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(distribuidor,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(numAcuseCarga,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(numDocto,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(fechaEmision,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(fechaPublicacion,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(plazoDocto,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$ "+monto,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(plazoDescuento,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("% "+porcDescuento,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$ "+montoDescontar,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(((icMoneda.equals(monedaLinea))?"":tipoConversion),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(((icMoneda.equals(monedaLinea))?"":""+tipoCambio),"formas",ComunesPDF.CENTER);
							/* B*/pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
							pdfDoc.setCell(((icMoneda.equals(monedaLinea))?"":""+montoValuado),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(modoPlazo,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(icDocumento,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$ "+montoCredito,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(plazoCredito,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(fechaVencCredito,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(fechaSeleccion,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(intermediario,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(referenciaTasaInt.replace(',',' '),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(valorTasaInt+"%","formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$ "+montoTasaInt,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell("$ "+(montoCredito+montoTasaInt),"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(fechaCambio,"formas",ComunesPDF.CENTER);
							pdfDoc.setCell(causa,"formas",ComunesPDF.CENTER);
							/* */pdfDoc.setCell("","formas",ComunesPDF.CENTER);
						}	
				}//FIN de while 20D
			}//Fin de if 20D
			if((tipo.equals("PDF")||tipo.equals("CSV"))&&
			estatusDocto.equals("22")&& estatusSolic.equals("D")	){
				while(rs.next()){
					epo			 		= (rs.getString("NOMBRE_EPO")==null)?"":rs.getString("NOMBRE_EPO");
					distribuidor 		= (rs.getString("NOMBRE_DIST")==null)?"":rs.getString("NOMBRE_DIST");
					numDocto			= (rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO");
					numAcuseCarga		= (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
					fechaEmision		= (rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION");
					fechaPublicacion	= (rs.getString("DF_CARGA")==null)?"":rs.getString("DF_CARGA");
					fechaVencimiento	= (rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC");
					plazoDocto			= (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");
					moneda 				= (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
					monto 				= rs.getDouble("FN_MONTO");
					tipoConversion		= (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");
					tipoCambio			= rs.getDouble("TIPO_CAMBIO");
					monedaLinea			= (rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA");
					plazoDescuento		= (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
					porcDescuento		= (rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO");
					montoDescontar		= monto*Double.parseDouble(porcDescuento)/100;
					montoValuado		= (monto-montoDescontar)*tipoCambio;
					modoPlazo			= (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
					estatus 			= (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");
					icMoneda			= (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");
					icDocumento			= (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
					tipoCobranza		= (rs.getString("TIPO_COBRANZA")==null)?"":rs.getString("TIPO_COBRANZA");
					icDocumento			= (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
					intermediario		= (rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF");
					tipoCredito			= (rs.getString("TIPO_CREDITO")==null)?"":rs.getString("TIPO_CREDITO");
					montoCredito		= rs.getDouble("MONTO_CREDITO");
					plazoCredito		= (rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO");
					tipoCobroInt		= (rs.getString("TIPO_COBRO_INT")==null)?"":rs.getString("TIPO_COBRO_INT");
					referenciaTasaInt	= (rs.getString("REFERENCIA_INT")==null)?"":rs.getString("REFERENCIA_INT");
					valorTasaInt		= rs.getDouble("VALOR_TASA_INT");
					montoTasaInt		= rs.getDouble("MONTO_TASA_INT");
					numeroPago			= (rs.getString("CG_NUMERO_PAGO")==null)?"":rs.getString("CG_NUMERO_PAGO");
					fechaSeleccion		= (rs.getString("FECHA_SELECCION")==null)?"":rs.getString("FECHA_SELECCION");
					fechaVencCredito	= (rs.getString("FECHA_VENC_CREDITO")==null)?"":rs.getString("FECHA_VENC_CREDITO");
					fechaOperIF			= (rs.getString("FECHA_OPER_IF")==null)?"":rs.getString("FECHA_OPER_IF");
					bandeVentaCartera			= (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
					if (bandeVentaCartera.equals("S") ) {
						modoPlazo ="";
					}
					if("1".equals(icMoneda)){
						totalDoctosMN ++;
						totalMontoMN += monto;
						totalMontoValuadoMN += montoValuado;
						totalMontoCreditoMN += montoCredito;
					}else{
						totalDoctosUSD ++;
						totalMontoUSD += monto;
						if(!icMoneda.equals(monedaLinea))
							totalMontoValuadoUSD += montoValuado;
						totalMontoCreditoUSD += montoCredito;
					}
					if(tipo.equals("CSV")){
						contenidoArchivo.append( epo.trim().replaceAll(","," ")+","+distribuidor.trim().replaceAll(","," ")+","+
						numAcuseCarga+","+numDocto+","+fechaEmision+","+fechaVencimiento+","+
						fechaPublicacion+","+plazoDocto+","+moneda+","+monto+","+plazoDescuento+","+porcDescuento+","+
						montoDescontar+","+
						((icMoneda.equals(monedaLinea))?"":tipoConversion)+","+
						((icMoneda.equals(monedaLinea))?"":""+tipoCambio)+","+
						((icMoneda.equals(monedaLinea))?"":""+montoValuado)+","+
						modoPlazo+","+
						icDocumento+","+
						montoCredito+","+
						plazoCredito+","+
						fechaVencCredito+","+
						fechaSeleccion+","+
						intermediario.trim().replaceAll(","," ")+","+
						referenciaTasaInt.replace(',',' ')+","+
						valorTasaInt+"%,"+montoTasaInt+","+
						(montoCredito+montoTasaInt)+","+
						numeroPago+"\n");
					}
					if(tipo.equals("PDF")){
						/*A*/pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(epo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(distribuidor,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(numAcuseCarga,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(numDocto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaEmision,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaPublicacion,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(plazoDocto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+monto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(plazoDescuento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("% "+porcDescuento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+montoDescontar,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(((icMoneda.equals(monedaLinea))?"":tipoConversion),"formas",ComunesPDF.CENTER);
						/*A*/pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(((icMoneda.equals(monedaLinea))?"":""+tipoCambio),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(((icMoneda.equals(monedaLinea))?"":""+montoValuado),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(modoPlazo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(icDocumento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+montoCredito,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(plazoCredito,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaVencCredito,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaSeleccion,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(intermediario,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(referenciaTasaInt.replace(',',' '),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(valorTasaInt+"%","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+montoTasaInt,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+(montoCredito+montoTasaInt),"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(numeroPago,"formas",ComunesPDF.CENTER);
					}	
				}//FIN de while 22D
			}//Fin de if 22D 
      //**aki entra 32D
      if((tipo.equals("PDF")||tipo.equals("CSV"))&&
			estatusDocto.equals("32")&& estatusSolic.equals("D")	){
      String monCred ="";
		//System.err.println(estatusDocto);
				while(rs.next()){
          epo			 		= (rs.getString("NOMBRE_EPO")==null)?"":rs.getString("NOMBRE_EPO");
					distribuidor 		= (rs.getString("NOMBRE_DIST")==null)?"":rs.getString("NOMBRE_DIST");
					numDocto			= (rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO");
					numAcuseCarga		= (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
					fechaEmision		= (rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION");
					fechaPublicacion	= (rs.getString("DF_CARGA")==null)?"":rs.getString("DF_CARGA");
					fechaVencimiento	= (rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC");
					plazoDocto			= (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");
					moneda 				= (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
					double monto32	= rs.getDouble("FN_MONTO"); 
    			plazoDescuento	= (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
					porcDescuento		= (rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO");
					montoDescontar		= Double.parseDouble(porcDescuento);
					montoValuado		= (monto-montoDescontar)*tipoCambio;
					modoPlazo			= (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
					icDocumento			= (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
					monCred				= rs.getString("MONTO_CREDITO")==null ?"":rs.getString("MONTO_CREDITO");
					montoCredito		= rs.getDouble("MONTO_CREDITO");
			//	COM_APLICABLE			= (rs.getString("COMISION_APLICABLE")==null)?"":rs.getString("COMISION_APLICABLE");
          montoDescontar = ( monto32 * montoDescontar)/100;
        double comApl    = rs.getDouble("COMISION_APLICABLE");
        double monCom1= monto32 * ( comApl / 100);
        double monCom = Math.round(monCom1 *100)/100;
        double monDep = montoCredito;//monto32 - monCom1;
				double montodos=0.0;
          montodos = monto32 - montoDescontar;
          monCom = (montodos *comApl)/100;
          monDep = montodos - monCom;
           /* if (modoPlazo.equals("Pronto pago")){
					 montodos = ( monto32 - (monto32 * (comApl / 100)) );
                monDep =  (montodos - (montodos * (comApl / 100)) ); // montoCredito - (monCom1 -(monCom1 - comApl));// monDep - (monDep * comApl);//montoCredito
            } else {
					montodos= (monto32 * (comApl /100));
                monDep =  monto32 - montodos;// monto32 - monCom1;
				}*/
                
            plazoCredito		= (rs.getString("IG_PLAZO_CREDITO")==null)?"":rs.getString("IG_PLAZO_CREDITO");
 				fechaSeleccion		= (rs.getString("FECHA_SELECCION")==null)?"":rs.getString("FECHA_SELECCION");
					fechaVencCredito	= (rs.getString("FECHA_SELECCION")==null)?"":rs.getString("FECHA_SELECCION");
					fechaOperIF			= (rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF");
					codigoBin			= (rs.getString("CG_CODIGO_BIN")==null)?"":rs.getString("CG_CODIGO_BIN");   
					bandeVentaCartera	= (rs.getString("BIN")==null)?"":rs.getString("BIN");   
					ORDEN_ENVIADO		= (rs.getString("ORDEN_ENVIADO")==null)?"":rs.getString("ORDEN_ENVIADO");
					ID_OPERACION		= (rs.getString("ID_OPERACION")==null)?"":rs.getString("ID_OPERACION");
					CODIGO_AUTORIZA	= (rs.getString("CODIGO_AUTORIZA")==null)?"":rs.getString("CODIGO_AUTORIZA");
					tarjetaCredito		= (rs.getString("NUM_TARJETA")==null)?"":rs.getString("NUM_TARJETA");

         BigDecimal nm = new BigDecimal(""+monto32); 
            BigDecimal bm = new BigDecimal(""+monCom); 
            BigDecimal bmd = new BigDecimal(""+monDep); 
            Comunes c = new Comunes ();
            String nbd =Comunes.formatoDecimal(bm.toPlainString(),2);
            String nbd2=Comunes.formatoDecimal(bmd.toPlainString(),2);
            String nbdm=Comunes.formatoDecimal(bmd.toPlainString(),2);
            
        if(tipo.equals("CSV")){
						contenidoArchivo.append( epo.trim().replaceAll(","," ")+","+distribuidor.trim().replaceAll(","," ")+","+
						numAcuseCarga+","+numDocto+","+fechaEmision+","+"N/A"+","+
						fechaPublicacion+","+"N/A"+","+moneda+","+"$ "+monto32+","+plazoDescuento+","+porcDescuento+" %,"+
						"$ "+montoDescontar+","+
						modoPlazo+","+
						icDocumento+","+
						"$ "+montodos+","+            
            comApl+"%,"+
            "$ "+nbd.trim().replaceAll(","," ")+","+
            "$ "+nbd2.trim().replaceAll(","," ")+","+
						"N/A"+","+
						"N/A"+","+
            fechaVencCredito+","+
            fechaOperIF+","+
            codigoBin+"-"+bandeVentaCartera+","+
            "'"+ORDEN_ENVIADO+","+
            ID_OPERACION+","+
            CODIGO_AUTORIZA +","+
            "XXXX-XXXX-XXXX-"+tarjetaCredito + "\n");
					}//fin CSV
          if(tipo.equals("PDF")){
						/*A*/pdfDoc.setCell("A","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(epo,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(distribuidor,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(numAcuseCarga,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(numDocto,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaEmision,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(fechaPublicacion,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);
						pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+monto32,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(plazoDescuento,"formas",ComunesPDF.CENTER);
						pdfDoc.setCell(porcDescuento+" %","formas",ComunesPDF.CENTER);
						pdfDoc.setCell("$ "+montoDescontar,"formas",ComunesPDF.CENTER);           
						pdfDoc.setCell(modoPlazo,"formas",ComunesPDF.CENTER);            
             	/*B*/pdfDoc.setCell("B","celda01",ComunesPDF.CENTER);
						pdfDoc.setCell(icDocumento,"formas",ComunesPDF.CENTER);            
            pdfDoc.setCell("$ "+ montodos,"formas",ComunesPDF.CENTER);            
						pdfDoc.setCell(comApl+"%","formas",ComunesPDF.CENTER);//COM_APLICABLE            
            pdfDoc.setCell("$ "+monCom,"formas",ComunesPDF.CENTER);//MON_COMISION nbd
            pdfDoc.setCell("$ "+nbd2,"formas",ComunesPDF.CENTER);//MON_DEPOSITAR nbd2
                      
				pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER); 
            pdfDoc.setCell("N/A","formas",ComunesPDF.CENTER);
				pdfDoc.setCell(fechaVencCredito,"formas",ComunesPDF.CENTER);			
            pdfDoc.setCell(fechaOperIF,"formas",ComunesPDF.CENTER);
             
				pdfDoc.setCell(codigoBin+"-"+bandeVentaCartera,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(ORDEN_ENVIADO,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(ID_OPERACION,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell(CODIGO_AUTORIZA,"formas",ComunesPDF.CENTER);
            pdfDoc.setCell("XXXX-XXXX-XXXX-"+tarjetaCredito,"formas",ComunesPDF.CENTER);
					}	//fin pdf
        }//fin while}
      }
      
			if(tipo.equals("PDF") ) {
				pdfDoc.addTable();
				pdfDoc.endDocument();	
			}
			if(tipo.equals("CSV") ) {
				creaArchivo.make(contenidoArchivo.toString(), path, ".csv");
				nombreArchivo = creaArchivo.getNombre();
			}
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			log.debug("crearCustomFile (S)");
		}
		log.info("crearCustomFile (S)");
	return nombreArchivo;	
	}


		/**
	 * metodo para crear el catalgo estatus 
	 * @return lista con los elementos del catalogo estatus
	 */
	
	public List  getEstatusData( ){
		log.info("getEstatusData (E) ");
		AccesoDB con = new AccesoDB(); 
		ResultSet rs = null;	
		HashMap datos =  new HashMap();
		List registros  = new ArrayList();
		try{
			con.conexionDB();
			String 	strSQL =	" select 'D' || ic_estatus_docto as clave , "+
									" cd_descripcion as descripcion "+
									" FROM comcat_estatus_docto "+
									" where ic_estatus_docto  in(1, 2, 3, 5,9, 11, 22,24,32) " +							
									" union all "+
									" select 'C'|| ic_estatus_solic as clave , "+
									" cd_descripcion as descripcion "+
									" FROM comcat_estatus_solic "+
									" where ic_estatus_solic  in(1, 2, 3, 4 ) " +
									" order by 1 ";  
			log.debug(" strSQL  "+strSQL );
			rs = con.queryDB(strSQL);///////////////////////////////////////////////////////////////////////////////////////////				
			while(rs.next()) {			
				datos = new HashMap();			
				datos.put("clave", rs.getString("clave") );
				datos.put("descripcion", rs.getString("descripcion"));
				registros.add(datos);
			}
			rs.close();
			con.cierraStatement();
		
		} catch (Exception e) {
			log.error("getEstatusData  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("getEstatusData (S) ");
		return registros;
	}
	/**
	 * 
	 * @return elementos del catalogo estatus de tipo Registros
	 */
	public Registros  getEstatusDataRegistros( ){
		log.info("getEstatusDataRegistros (E) ");
		AccesoDB con = new AccesoDB(); 
		ResultSet rs = null;	
		HashMap datos =  new HashMap();
		Registros registros  = new Registros();
		try{
			con.conexionDB();
					
			String 	strSQL =	" select 'D' || ic_estatus_docto as clave , "+
									" cd_descripcion as descripcion "+
									" FROM comcat_estatus_docto "+
									" where ic_estatus_docto  in(1, 2, 3, 5,9, 11, 20, 22) " +							
									" union all "+
									" select 'C'|| ic_estatus_solic as clave , "+
									" cd_descripcion as descripcion "+
									" FROM comcat_estatus_solic "+
									" where ic_estatus_solic  in(1, 2, 3, 4 ) " +
									" order by 1 ";  
			registros=con.consultarDB(strSQL);
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("getEstatusDataRegistros  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("getEstatusDataRegistros (S) ");
		return registros;
	}
	
	/**
	 * 
	 * @return 
	 */
	public  Registros getTotalesPorEstatus(){
		log.info("getTotalesPorEstatus(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		Registros registros = new Registros();
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			qryBase.setEstatusDocto(Integer.parseInt(estatusDocto));
			qryBase.setEstatusSolic(estatusSolic);
			qrySentencia.append(qryBase.getTotalesPorEstatus());	
			if(estatusDocto.equals("3") && estatusSolic.equals("D")||(estatusDocto.equals("24")&&estatusSolic.equals("D")) ||(estatusDocto.equals("32")&&estatusSolic.equals("D")) ){
				conditions.add(estatusDocto);
			}
			registros = con.consultarDB(qrySentencia.toString(),conditions);
			con.cierraConexionDB();
		} catch(Exception e) {
			throw new AppException("getTotalesPorEstatus  ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(true); 
				con.cierraConexionDB();
			}
		}
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getTotalesPorEstatus (S) ");
		return registros;
	}
	/**
	 * 
	 * @return 
	 */
	public Registros getDetallesDocumentQuery() {
		log.info("getDetallesDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		Registros registros = new Registros();
		AccesoDB con = new AccesoDB();
		try{
			con.conexionDB();
			qryBase.setClaveDocto(claveDocto);
			qrySentencia.append(qryBase.getDetallesDocumento());		
			conditions.add(claveDocto);
			registros=con.consultarDB(qrySentencia.toString(),conditions);
			con.cierraConexionDB();
		} catch (Exception e) {
			log.error("getDetallesDocumentQuery  Error: " + e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		} 
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDetallesDocumentQuery(S)");
		return registros;	
	}
	/**
	 * 
	 * @return 
	 *
	public Registros getDocumentQueryFile_Con() {
		log.info("getDocumentQueryFile_Con(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		Registros registros = new Registros();
		
		AccesoDB con = new AccesoDB();
		try {
			con.conexionDB();
			qryBase.setEstatusDocto(Integer.parseInt(estatusDocto));
			qryBase.setEstatusSolic(estatusSolic);
			qrySentencia.append(qryBase.getTotalesPorEstatus());	
			registros = con.consultarDB(qrySentencia.toString());
			con.cierraConexionDB();
		} catch(Exception e) {
			throw new AppException("getDocumentQueryFile_Con  ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(true); 
				con.cierraConexionDB();
			}
		}
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile_Con(S)");
		return registros;	
	}
*/

	public void setConditions(List conditions) {
		this.conditions = conditions;
	}


	public List getConditions() {
		return conditions;
	}


	public void setEstatusDocto(String estatusDocto) {
		this.estatusDocto = estatusDocto;
	}


	public String getEstatusDocto() {
		return estatusDocto;
	}


	public void setEstatusSolic(String estatusSolic) {
		this.estatusSolic = estatusSolic;
	}


	public String getEstatusSolic() {
		return estatusSolic;
	}


	public void setClaveDocto(String claveDocto) {
		this.claveDocto = claveDocto;
	}


	public String getClaveDocto() {
		return claveDocto;
	}


	public void set_claveDocto(String claveDocto) {
		this.claveDocto = claveDocto;
	}


	public String get_claveDocto() {
		return claveDocto;
	}


}