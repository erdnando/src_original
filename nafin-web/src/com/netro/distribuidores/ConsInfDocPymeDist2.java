package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.math.BigDecimal;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsInfDocPymeDist2 implements IQueryGenerator, IQueryGeneratorRegExtJS{ 
  public ConsInfDocPymeDist2() {  }
	public String getAggregateCalculationQuery(HttpServletRequest request) {

		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
    	StringBuffer qrySentencia	= new StringBuffer();
    	StringBuffer qryDM			= new StringBuffer();
    	StringBuffer qryCCC			= new StringBuffer();
    	StringBuffer condicion		= new StringBuffer();
      StringBuffer qryFdR			= new StringBuffer();

		String	ic_pyme					= request.getSession().getAttribute("iNoCliente").toString();
		String	ic_epo					=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String	ic_if					=	(request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String	ic_estatus_docto		=	(request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto");
		String 	ig_numero_docto			=	(request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String 	fecha_seleccion_de		=	(request.getParameter("fecha_seleccion_de")==null)?"":request.getParameter("fecha_seleccion_de");
		String 	fecha_seleccion_a		=	(request.getParameter("fecha_seleccion_a")==null)?"":request.getParameter("fecha_seleccion_a");
		String 	cc_acuse				=	(request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String 	monto_credito_de		=	(request.getParameter("monto_credito_de")==null)?"":request.getParameter("monto_credito_de");
		String 	monto_credito_a			=	(request.getParameter("monto_credito_a")==null)?"":request.getParameter("monto_credito_a");
		String 	fecha_emision_de		=	(request.getParameter("fecha_emision_de")==null)?"":request.getParameter("fecha_emision_de");
		String 	fecha_emision_a			=	(request.getParameter("fecha_emision_a")==null)?"":request.getParameter("fecha_emision_a");
		String 	fecha_vto_de			=	(request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String 	fecha_vto_a				=	(request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String 	fecha_vto_credito_de	=	(request.getParameter("fecha_vto_credito_de")==null)?"":request.getParameter("fecha_vto_credito_de");
		String 	fecha_vto_credito_a 	=	(request.getParameter("fecha_vto_credito_a")==null)?"":request.getParameter("fecha_vto_credito_a");
		String 	ic_tipo_cobro_int		=	(request.getParameter("ic_tipo_cobro_int")==null)?"":request.getParameter("ic_tipo_cobro_int");
		String	ic_moneda				=	(request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String 	fn_monto_de				=	(request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String 	fn_monto_a				=	(request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String 	monto_con_descuento		=	(request.getParameter("monto_con_descuento")==null)?"":"checked";
		String	solo_cambio				=	(request.getParameter("solo_cambio")==null)?"":request.getParameter("solo_cambio");
		String	modo_plazo				=	(request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
		String	tipo_credito			=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String	cgTipoConversion		=	(request.getParameter("tipo_conversion")==null)?"":request.getParameter("tipo_conversion");
		String	ic_documento			=	(request.getParameter("ic_documento")==null)?"":request.getParameter("ic_documento");
		String 	fecha_publicacion_de	=	(request.getParameter("fecha_publicacion_de")==null)?fechaHoy:request.getParameter("fecha_publicacion_de");
		String 	fecha_publicacion_a		=	(request.getParameter("fecha_publicacion_a")==null)?fechaHoy:request.getParameter("fecha_publicacion_a");
		String tipoCreditoXepo		=	(request.getParameter("tipoCreditoXepo")==null)?"":request.getParameter("tipoCreditoXepo");
		String tipo_pago = (request.getParameter("tipo_pago")==null)?"":request.getParameter("tipo_pago"); // F009-2015

		try {
			if(!"".equals(ic_epo)&&ic_epo!=null)
				condicion.append(" AND D.IC_EPO = "+ic_epo);
			if("4".equals(ic_estatus_docto)||"20".equals(ic_estatus_docto)||"22".equals(ic_estatus_docto)||"3".equals(ic_estatus_docto)||"11".equals(ic_estatus_docto)||"1".equals(ic_estatus_docto)||"24".equals(ic_estatus_docto)||"32".equals(ic_estatus_docto))//Mod +(||"24".equals(ic_estatus_docto))
				condicion.append(" AND D.IC_ESTATUS_DOCTO ="+ic_estatus_docto);
			if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
				condicion.append(" and D.ig_numero_docto = '"+ig_numero_docto+"'");
			if(!"".equals(fecha_seleccion_de)&& fecha_seleccion_de!=null&&!"".equals(fecha_seleccion_a)&& fecha_seleccion_a!=null)
				condicion.append(" and trunc(DS.df_fecha_seleccion) between trunc(to_date('"+fecha_seleccion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_seleccion_a+"','dd/mm/yyyy'))");
			if(!"".equals(cc_acuse)&& cc_acuse!=null)
				condicion.append(" and D.cc_acuse = '"+cc_acuse+"'");
			if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null)
				condicion.append(" and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date('"+fecha_vto_credito_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_credito_a+"','dd/mm/yyyy'))");		
			if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null)
				condicion.append(" AND LC.ic_tipo_cobro_interes = "+ic_tipo_cobro_int);
			if(!"".equals(ic_moneda)&&ic_moneda!=null)
				condicion.append(" and M.ic_moneda = "+ic_moneda);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
				condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
				condicion.append(" and D.fn_monto between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(solo_cambio)&& solo_cambio!=null)
				condicion.append(" and D.ic_documento in (select ic_documento from dis_cambio_estatus)");
			if(!"".equals(modo_plazo)&& modo_plazo!=null)
				condicion.append(" and D.ic_tipo_financiamiento = "+modo_plazo);
			if(!"".equals(ic_documento)&&ic_documento!=null)
				condicion.append(" AND D.IC_DOCUMENTO = "+ic_documento);
			if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
				condicion.append(" and D.df_carga >= to_date('"+fecha_publicacion_de+"','dd/mm/yyyy') and D.df_carga < to_date('"+fecha_publicacion_a+"','dd/mm/yyyy') + 1");
			if(!"".equals(monto_credito_de)&& monto_credito_de!=null&&!"".equals(monto_credito_a)&& monto_credito_a!=null)
				condicion.append(" and (D.fn_monto-(fn_monto*(fn_porc_descuento/100))) between "+monto_credito_de+" and "+monto_credito_a);	
			if("".equals(ic_estatus_docto))
				condicion.append(" and D.ic_estatus_docto in (4,20,22,3,11,1,24,32)");//Mod +(24)
			if(!"".equals(ic_estatus_docto)&&!"4".equals(ic_estatus_docto)&&!"20".equals(ic_estatus_docto)&&!"22".equals(ic_estatus_docto)&&!"3".equals(ic_estatus_docto)&&!"11".equals(ic_estatus_docto)&&!"1".equals(ic_estatus_docto)&&!"24".equals(ic_estatus_docto)&&!"32".equals(ic_estatus_docto))//Mod +(&&!"24".equals(ic_estatus_docto))
				condicion.append(" and D.ic_estatus_docto is null");
			if(!"".equals(ic_if)&&ic_if!=null)
				condicion.append(" AND LC.IC_IF = "+ic_if);
			if(!tipo_pago.equals("")){
				condicion.append(" AND D.IG_TIPO_PAGO = " + tipo_pago);
			}

			qryDM.append("   SELECT /*+index(d) use_nl(d, m, pe, epo pn, tc, lc, pep, ds)*/"   +
					"   M.ic_moneda as moneda, m.cd_nombre as nommoneda,d.fn_monto as monto"   +
					"   FROM DIS_DOCUMENTO D"   +
					"   ,COMCAT_MONEDA M"   +
					"   ,COMREL_PRODUCTO_EPO PE"   +
					"   ,COMCAT_EPO EPO"   +
					"   ,COMCAT_PRODUCTO_NAFIN PN"   +
					"   ,COM_TIPO_CAMBIO TC"   +
					"   ,DIS_LINEA_CREDITO_DM LC"   +
					"   ,COMREL_PYME_EPO_X_PRODUCTO PEP"   +
					"   ,DIS_DOCTO_SELECCIONADO DS"   +
					"   WHERE D.IC_MONEDA = M.IC_MONEDA"   +
					"   AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"   AND PE.IC_EPO = D.IC_EPO"   +
					"   AND D.IC_EPO = EPO.IC_EPO"   +
					"   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"   AND M.ic_moneda = TC.ic_moneda"   +
					"   AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM (+)"   +
					"   AND PEP.IC_EPO = D.IC_EPO"   +
					"   AND PEP.IC_PYME = D.IC_PYME"   +
					"   AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO (+)"   +
//					"   AND PEP.CS_HABILITADO = 'S'"   +
					"   AND PN.IC_PRODUCTO_NAFIN = 4"   +
					"   AND PEP.IC_PRODUCTO_NAFIN = 4"   +
					"   AND EPO.CS_HABILITADO = 'S'"   +
					" 	and d.IC_TIPO_FINANCIAMIENTO is not  null "+
					" AND D.IC_pyme = "+ic_pyme+
					condicion.toString());

			qryCCC.append("   SELECT /*+index(d) use_nl(d, m, pe, epo, pn, tc, lc, pep, ds)*/"   +
					"   M.ic_moneda as moneda, m.cd_nombre as nommoneda,d.fn_monto as monto"   +
					"   FROM DIS_DOCUMENTO D"   +
					"   ,COMCAT_MONEDA M"   +
					"   ,COMREL_PRODUCTO_EPO PE"   +
					"   ,COMCAT_EPO EPO"   +
					"   ,COMCAT_PRODUCTO_NAFIN PN"   +
					"   ,COM_TIPO_CAMBIO TC"   +
					"   ,COM_LINEA_CREDITO LC"   +
					"   ,COMREL_PYME_EPO_X_PRODUCTO PEP"   +
					"   ,DIS_DOCTO_SELECCIONADO DS"   +
					"	,COM_BINS_IF BINS	"	+//FODEA-O13-2014
					"   WHERE D.IC_MONEDA = M.IC_MONEDA"   +
					"		AND D.IC_BINS = BINS.IC_BINS(+)	"	+
					"   AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO (+)"   +
					"   AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO"   +
					"   AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"   AND PE.IC_EPO = D.IC_EPO"   +
					"   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"   AND M.ic_moneda = TC.ic_moneda"   +
					"   AND PEP.IC_EPO = D.IC_EPO"   +
					"   AND D.IC_EPO = EPO.IC_EPO"   +
					"   AND PEP.IC_PYME = D.IC_PYME"   +
					"   AND PEP.IC_PRODUCTO_NAFIN = 4"   +
//					"   AND PEP.CS_HABILITADO = 'S'"   +
					"   AND PN.IC_PRODUCTO_NAFIN = 4"   +
					"   AND EPO.CS_HABILITADO = 'S' "   +
					" 	and d.IC_TIPO_FINANCIAMIENTO is not  null "+
					" AND D.IC_pyme = "+ic_pyme+
					condicion.toString());
					
		qryFdR.append("   SELECT /*+index(d) use_nl(d, m, pe, epo, pn, tc, lc, pep, ds)*/"   +
					"   M.ic_moneda as moneda, m.cd_nombre as nommoneda,d.fn_monto as monto"   +
					"   FROM DIS_DOCUMENTO D"   +
					"   ,COMCAT_MONEDA M"   +
					"   ,COMREL_PRODUCTO_EPO PE"   +
					"   ,COMCAT_EPO EPO"   +
					"   ,COMCAT_PRODUCTO_NAFIN PN"   +
					"   ,COM_TIPO_CAMBIO TC"   +
					"   ,COM_LINEA_CREDITO LC"   +
					"   ,COMREL_PYME_EPO_X_PRODUCTO PEP"   +
					"   ,DIS_DOCTO_SELECCIONADO DS"   +
					"   WHERE D.IC_MONEDA = M.IC_MONEDA"   +
					"   AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO (+)"   +
					"   AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO"   +
					"   AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"   AND PE.IC_EPO = D.IC_EPO"   +
					"   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"   AND M.ic_moneda = TC.ic_moneda"   +
					"   AND PEP.IC_EPO = D.IC_EPO"   +
					"   AND D.IC_EPO = EPO.IC_EPO"   +
					"   AND PEP.IC_PYME = D.IC_PYME"   +
					"   AND PEP.IC_PRODUCTO_NAFIN = 4"   +
					"   AND PN.IC_PRODUCTO_NAFIN = 4"   +
					"   AND EPO.CS_HABILITADO = 'S' "   +
					"   AND d.IC_TIPO_FINANCIAMIENTO is null "+
					"   AND D.IC_pyme = "+ic_pyme+
					condicion.toString());
			String qryAux = "";	
			if("D".equals(tipo_credito))
				qryAux = qryDM.toString();
			else if("C".equals(tipo_credito))
				qryAux = qryCCC.toString();
			else  if("".equals(tipo_credito)  && tipoCreditoXepo.equals("F"))   
				qryAux = qryFdR.toString();
			else
				qryAux = qryDM.toString()+ " UNION ALL "+qryCCC.toString();

			qrySentencia.append(
				"SELECT moneda, nommoneda, COUNT (1), SUM (monto), 'ConsInfDocPymeDis2t::getAggregateCalculationQuery'"+
				"  FROM ("+qryAux.toString()+")"+
				"GROUP BY  moneda, nommoneda ORDER BY moneda ");
				
		}catch(Exception e){
			System.out.println("ConsInfDocPymeDist2::getAggregateCalculationQuery "+e);
		}
		return qrySentencia.toString();
	}

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids) {
		int i=0;
		StringBuffer qrySentencia	= new StringBuffer();
    	StringBuffer qryDM			= new StringBuffer();
    	StringBuffer qryCCC			= new StringBuffer();
    	StringBuffer condicion		= new StringBuffer();
    	StringBuffer qryFdR			= new StringBuffer();
		
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date()); 
		String	ic_pyme					= request.getSession().getAttribute("iNoCliente").toString();
		String	ic_epo					=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String	ic_if					=	(request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");		
		String	ic_estatus_docto		=	(request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto");
		String 	ig_numero_docto			=	(request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String 	fecha_seleccion_de		=	(request.getParameter("fecha_seleccion_de")==null)?"":request.getParameter("fecha_seleccion_de");
		String 	fecha_seleccion_a		=	(request.getParameter("fecha_seleccion_a")==null)?"":request.getParameter("fecha_seleccion_a");
		String 	cc_acuse				=	(request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String 	monto_credito_de		=	(request.getParameter("monto_credito_de")==null)?"":request.getParameter("monto_credito_de");
		String 	monto_credito_a			=	(request.getParameter("monto_credito_a")==null)?"":request.getParameter("monto_credito_a");
		String 	fecha_emision_de		=	(request.getParameter("fecha_emision_de")==null)?"":request.getParameter("fecha_emision_de");
		String 	fecha_emision_a			=	(request.getParameter("fecha_emision_a")==null)?"":request.getParameter("fecha_emision_a");
		String 	fecha_vto_de			=	(request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String 	fecha_vto_a				=	(request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String 	fecha_vto_credito_de	=	(request.getParameter("fecha_vto_credito_de")==null)?"":request.getParameter("fecha_vto_credito_de");
		String 	fecha_vto_credito_a 	=	(request.getParameter("fecha_vto_credito_a")==null)?"":request.getParameter("fecha_vto_credito_a");
		String 	ic_tipo_cobro_int		=	(request.getParameter("ic_tipo_cobro_int")==null)?"":request.getParameter("ic_tipo_cobro_int");
		String	ic_moneda				=	(request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String 	fn_monto_de				=	(request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String 	fn_monto_a				=	(request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String 	monto_con_descuento		=	(request.getParameter("monto_con_descuento")==null)?"":"checked";
		String	solo_cambio				=	(request.getParameter("solo_cambio")==null)?"":request.getParameter("solo_cambio");
		String	modo_plazo				=	(request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
		String	tipo_credito			=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String	cgTipoConversion		=	(request.getParameter("tipo_conversion")==null)?"":request.getParameter("tipo_conversion");
		String	ic_documento			=	(request.getParameter("ic_documento")==null)?"":request.getParameter("ic_documento");
		String 	fecha_publicacion_de	=	(request.getParameter("fecha_publicacion_de")==null)?"":request.getParameter("fecha_publicacion_de");
		String 	fecha_publicacion_a		=	(request.getParameter("fecha_publicacion_a")==null)?"":request.getParameter("fecha_publicacion_a");
		String tipoCreditoXepo		=	(request.getParameter("tipoCreditoXepo")==null)?"":request.getParameter("tipoCreditoXepo");
		String tipo_pago = (request.getParameter("tipo_pago")==null)?"":request.getParameter("tipo_pago"); // F009-2015

		if(!"".equals(ic_epo)&&ic_epo!=null)
				condicion.append(" AND D.IC_EPO = "+ic_epo);
			if("4".equals(ic_estatus_docto)||"20".equals(ic_estatus_docto)||"22".equals(ic_estatus_docto)||"3".equals(ic_estatus_docto)||"11".equals(ic_estatus_docto)||"1".equals(ic_estatus_docto)||"24".equals(ic_estatus_docto)||"32".equals(ic_estatus_docto))//Mod +(||"24".equals(ic_estatus_docto))
				condicion.append(" AND D.IC_ESTATUS_DOCTO ="+ic_estatus_docto);
			if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
				condicion.append(" and D.ig_numero_docto = '"+ig_numero_docto+"'");
			if(!"".equals(fecha_seleccion_de)&& fecha_seleccion_de!=null&&!"".equals(fecha_seleccion_a)&& fecha_seleccion_a!=null)
				condicion.append(" and trunc(DS.df_fecha_seleccion) between trunc(to_date('"+fecha_seleccion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_seleccion_a+"','dd/mm/yyyy'))");
			if(!"".equals(cc_acuse)&& cc_acuse!=null)
				condicion.append(" and D.cc_acuse = '"+cc_acuse+"'");
			if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null)
				condicion.append(" and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date('"+fecha_vto_credito_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_credito_a+"','dd/mm/yyyy'))");		
			if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null)
				condicion.append(" AND LC.ic_tipo_cobro_interes = "+ic_tipo_cobro_int);
			if(!"".equals(ic_moneda)&&ic_moneda!=null)
				condicion.append(" and M.ic_moneda = "+ic_moneda);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
				condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
				condicion.append(" and D.fn_monto between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(solo_cambio)&& solo_cambio!=null)
				condicion.append(" and D.ic_documento in (select ic_documento from dis_cambio_estatus)");
			if(!"".equals(modo_plazo)&& modo_plazo!=null)
				condicion.append(" and D.ic_tipo_financiamiento = "+modo_plazo);
			if(!"".equals(ic_documento)&&ic_documento!=null)
				condicion.append(" AND D.IC_DOCUMENTO = "+ic_documento);
			if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
				condicion.append(" and D.df_carga >= to_date('"+fecha_publicacion_de+"','dd/mm/yyyy') and D.df_carga < to_date('"+fecha_publicacion_a+"','dd/mm/yyyy') + 1");
			if(!"".equals(monto_credito_de)&& monto_credito_de!=null&&!"".equals(monto_credito_a)&& monto_credito_a!=null)
				condicion.append(" and (D.fn_monto-(fn_monto*(fn_porc_descuento/100))) between "+monto_credito_de+" and "+monto_credito_a);	
			if("".equals(ic_estatus_docto))
				condicion.append(" and D.ic_estatus_docto in (4,20,22,3,11,1,24,32)");//Mod +(24)
			if(!"".equals(ic_estatus_docto)&&!"4".equals(ic_estatus_docto)&&!"20".equals(ic_estatus_docto)&&!"22".equals(ic_estatus_docto)&&!"3".equals(ic_estatus_docto)&&!"11".equals(ic_estatus_docto)&&!"1".equals(ic_estatus_docto)&&!"24".equals(ic_estatus_docto)&&!"32".equals(ic_estatus_docto))//Mod +(&&!"24".equals(ic_estatus_docto))
				condicion.append(" and D.ic_estatus_docto is null");
			if(!"".equals(ic_if)&&ic_if!=null)
				condicion.append(" AND LC.IC_IF = "+ic_if);
				
				
		condicion.append(" AND d.ic_documento in (");
		i=0;
		for (Iterator it = ids.iterator(); it.hasNext();i++){
        	if(i>0){
				condicion.append(",");
			}
			condicion.append(" "+it.next().toString()+" ");
		}
		condicion.append(") ");

			qryDM.append("  SELECT /*+ use_nl(d,ds,e,m,pe,pn,tc,tf,ed,lc,i,pep,ct,tci)"   +
				"     index(pep cp_comrel_pyme_epo_x_prod_pk)*/"   +
				"     E.CG_RAZON_SOCIAL AS NOMBRE_EPO"   +
				"  	,D.IG_NUMERO_DOCTO"   +
				"  	,DS.CC_ACUSE"   +
				" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"   +
				" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"   +
				" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"   +
				" 	,D.IG_PLAZO_DOCTO"   +
				" 	,M.CD_NOMBRE AS MONEDA"   +
				" 	,D.FN_MONTO"   +
				" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
				" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
				" 	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"   +
				" 	,DECODE(D.IC_TIPO_FINANCIAMIENTO,2,0,NVL(D.FN_PORC_DESCUENTO,0)) AS FN_PORC_DESCUENTO"   +
				" 	,TF.CD_DESCRIPCION AS MODO_PLAZO"   +
				" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
				" 	,M.ic_moneda"   +
				" 	,D.ic_documento"   +
				" 	,I.cg_razon_social as NOMBRE_IF"   +
				//"	,decode(ed.ic_estatus_docto,32,i2.cg_razon_social,i.cg_razon_social) AS nombre_if	"+
				" 	,decode(PEP.cg_tipo_credito,'D','Descuento','Credito en Cuenta Corriente') as TIPO_CREDITO"   +
				" 	,TO_CHAR(DS.DF_FECHA_SELECCION,'DD/MM/YYYY') AS DF_OPERACION_CREDITO"   +
				" 	,d.ig_plazo_credito AS PLAZO_CREDITO"   +
				" 	,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'DD/MM/YYYY') AS DF_VENC_CREDITO"   +
				"	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) AS REF_TASA" +
				" 	,TCI.CD_DESCRIPCION AS TIPO_COBRO_INT"   +
				"  ,DS.fn_valor_tasa"   +
				" 	,DS.FN_IMPORTE_INTERES"   +
				" 	,LC.ic_moneda as moneda_linea, D.IC_ESTATUS_DOCTO,clas.cg_descripcion as CATEGORIA "   +
				" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				" ,  (SELECT nvl(fn_valor_aforo, 0)     FROM dis_aforo_epo   WHERE ic_if = i.ic_if      AND ic_epo = d.ic_epo        AND ic_moneda = d.ic_moneda) AS POR_AFRO  "+
				/*
				"	,d.IC_ORDEN_PAGO_TC as id_orden_enviado	"+
				"	,ptc.CG_TRANSACCION_ID as id_operacion	"+
				//"	,ptc.CG_CODIGO_AUTORIZACION as codigo_autorizacion	"+
				"	 ,CONCAT(ptc.CG_CODIGO_RESP ,ptc.CG_CODIGO_DESC)AS codigo_autorizado	"+
				"	,DF_FECHA_AUTORIZACION as fecha_registro	"+
           "	,trim(cpi.cg_opera_tarjeta) as OPERA_TARJETA "+
			  
			  "	,bins.CG_DESCRIPCION as bins "+
				*/
				"	,'' as id_orden_enviado 	"	+
				"	,'' as id_operacion 	"	+
				"	,'' as codigo_autorizado 	"	+
				"	,'' as fecha_registro 	"	+
				"	,'' as OPERA_TARJETA 	"	+
				"	,'' as NUM_TC 	"	+
				" , D.IG_TIPO_PAGO"+
				"  FROM DIS_DOCUMENTO D"   +
				"  ,DIS_DOCTO_SELECCIONADO DS"   +
				"  ,COMCAT_EPO E"   +
				"  ,COMCAT_MONEDA M"   +
				"  ,COMREL_PRODUCTO_EPO PE"   +
				"  ,COMCAT_PRODUCTO_NAFIN PN"   +
				"  ,COM_TIPO_CAMBIO TC"   +
				"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
				"  ,COMCAT_ESTATUS_DOCTO ED"   +
				"  ,DIS_LINEA_CREDITO_DM LC"   +
				"  ,COMCAT_IF I"   +
				//"  ,COMCAT_IF I2"   +
				"  ,COMREL_PYME_EPO_X_PRODUCTO PEP"   +
				"  ,COMCAT_TASA CT"   +
				"  ,COMCAT_TIPO_COBRO_INTERES TCI,comrel_clasificacion clas "+
				/*
				"	,com_bins_if bins "+
				"	,comrel_producto_if cpi "+
				"	,DIS_DOCTOS_PAGO_TC ptc "+
				*/
				"  WHERE D.IC_EPO = E.IC_EPO"   +
				"  AND D.IC_MONEDA = M.IC_MONEDA"   +
				"  AND PN.IC_PRODUCTO_NAFIN = 4"   +
				"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
				"  AND PE.IC_EPO = D.IC_EPO"   +
				"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
				"  AND M.ic_moneda = TC.ic_moneda"   +
				"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
				"  AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"   +
				"  AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM (+)"   +
				"  AND LC.IC_IF = I.IC_IF(+)"   +
				/*
				//"	and lc.IC_IF = bins.IC_IF "+
				"    AND d.IC_BINS = bins.IC_BINS(+) "+
				"    AND bins.IC_if = i2.IC_if(+)    "+
				
				"	and lc.IC_IF = cpi.ic_if "   +
				"	and pep.IC_PRODUCTO_NAFIN = cpi.IC_PRODUCTO_NAFIN "   +
				"	and d.IC_ORDEN_PAGO_TC  =ptc.IC_ORDEN_PAGO_TC (+) "+
				*/
				"  AND PEP.IC_EPO = E.IC_EPO"   +
				"  AND PEP.IC_EPO = D.IC_EPO"   +
				"  AND PEP.IC_PYME = D.IC_PYME"   +
//				"  AND PEP.CS_HABILITADO = 'S'"   +
				"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO (+)"   +
				"  AND DS.IC_TASA = CT.IC_TASA(+)"   +
				"  AND LC.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES(+)"   +
				"  AND D.ic_clasificacion = clas.ic_clasificacion (+) "+
				"  AND PEP.IC_PRODUCTO_NAFIN = 4"   +
				" AND D.IC_pyme = "+ic_pyme+
				condicion.toString());

		qryCCC.append("  SELECT /*+ use_nl(D,DS,E,M,PE,PN,TC,TF,ED,LC,I,PEP,CT,TCI)"   +
				"  index(pep cp_comrel_pyme_epo_x_prod_pk)*/"   +
				"  E.CG_RAZON_SOCIAL AS NOMBRE_EPO"   +
				"  ,D.IG_NUMERO_DOCTO"   +
				"  ,D.CC_ACUSE"   +
				" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"   +
				" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"   +
				" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"   +
				" 	,D.IG_PLAZO_DOCTO"   +
				" 	,M.CD_NOMBRE AS MONEDA"   +
				" 	,D.FN_MONTO"   +
				" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
				" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
				" 	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"   +
				" 	,DECODE(D.IC_TIPO_FINANCIAMIENTO,2,0,NVL(D.FN_PORC_DESCUENTO,0)) AS FN_PORC_DESCUENTO"   +
				" 	,TF.CD_DESCRIPCION AS MODO_PLAZO"   +
				" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
				" 	,M.ic_moneda"   +
				" 	,D.ic_documento"   +
				//" 	,I.cg_razon_social as NOMBRE_IF"   +
				"	,decode(ed.ic_estatus_docto,32,i2.cg_razon_social,i.cg_razon_social) AS nombre_if	"+
				" 	,decode(PEP.cg_tipo_credito,'D','Descuento','Credito en Cuenta Corriente') as TIPO_CREDITO"   +
				" 	,TO_CHAR(DS.DF_FECHA_SELECCION,'DD/MM/YYYY') AS DF_OPERACION_CREDITO"   +
				" 	,d.ig_plazo_credito AS PLAZO_CREDITO"   +
				" 	,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'DD/MM/YYYY') AS DF_VENC_CREDITO"   +
				" 	,CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS AS REF_TASA"   +
				" 	,TCI.CD_DESCRIPCION AS TIPO_COBRO_INT"   +
				"  ,DS.fn_valor_tasa"   +
				" 	,DS.FN_IMPORTE_INTERES"   +
				" 	,LC.IC_MONEDA as moneda_linea, D.IC_ESTATUS_DOCTO,clas.cg_descripcion as CATEGORIA "   +
				" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				" ,  (SELECT nvl(fn_valor_aforo, 0)     FROM dis_aforo_epo   WHERE ic_if = i.ic_if      AND ic_epo = d.ic_epo        AND ic_moneda = d.ic_moneda) AS POR_AFRO " +
				
				"	,d.IC_ORDEN_PAGO_TC as id_orden_enviado	"+
				"	,ptc.CG_TRANSACCION_ID as id_operacion	"+
				"	 ,CONCAT(ptc.CG_CODIGO_RESP ,ptc.CG_CODIGO_DESC)AS codigo_autorizado	"+
				"	,DF_FECHA_AUTORIZACION as fecha_registro	"+
           "	,trim(cpi.cg_opera_tarjeta) as OPERA_TARJETA "+
			  "	,SUBSTR(ptc.CG_NUM_TARJETA,-4,4) as NUM_TC	"+
			  
			   "	,bins.CG_CODIGO_BIN||' '||bins.CG_DESCRIPCION as bins "+
				" , D.IG_TIPO_PAGO "+
				"  FROM DIS_DOCUMENTO D"   +
				"  ,DIS_DOCTO_SELECCIONADO DS"   +
				"  ,COMCAT_EPO E"   +
				"  ,COMCAT_MONEDA M"   +
				"  ,COMREL_PRODUCTO_EPO PE"   +
				"  ,COMCAT_PRODUCTO_NAFIN PN"   +
				"  ,COM_TIPO_CAMBIO TC"   +
				"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
				"  ,COMCAT_ESTATUS_DOCTO ED"   +
				"  ,COM_LINEA_CREDITO LC"   +
				"  ,COMCAT_IF I"   +
				"  ,COMCAT_IF I2"   +
				"  ,COMREL_PYME_EPO_X_PRODUCTO PEP"   +
				"  ,COMCAT_TASA CT"   +
				"  ,COMCAT_TIPO_COBRO_INTERES TCI,comrel_clasificacion clas "+
				"	,comrel_producto_if cpi "+
				"	,DIS_DOCTOS_PAGO_TC ptc "+
				
					"	,com_bins_if bins "+
				
				"  WHERE D.IC_EPO = E.IC_EPO"   +
				"  AND D.IC_MONEDA = M.IC_MONEDA"   +
				"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
				"  AND PE.IC_EPO = D.IC_EPO"   +
				"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
				"  AND M.ic_moneda = TC.ic_moneda"   +
				"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
				"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
				"  AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO"   +
				"  AND LC.IC_IF = I.IC_IF(+)"   +
				
				"	and lc.IC_IF = cpi.ic_if "   +
				"	and pep.IC_PRODUCTO_NAFIN = cpi.IC_PRODUCTO_NAFIN "   +
				"	and d.IC_ORDEN_PAGO_TC  =ptc.IC_ORDEN_PAGO_TC (+) "+
				
				//"	and lc.IC_IF = bins.IC_IF "+
				"    AND d.IC_BINS = bins.IC_BINS(+) "+
				"    AND bins.IC_if = i2.IC_if(+)    "+
				"	AND (lc.ic_if = cpi.ic_if or bins.ic_if = cpi.ic_if) "+
				
				"  AND PEP.IC_EPO = E.IC_EPO"   +
				"  AND PEP.IC_EPO = D.IC_EPO"   +
				"  AND PEP.IC_PYME = D.IC_PYME"   +
				"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO (+)"   +
				"  AND DS.IC_TASA = CT.IC_TASA(+)"   +
				"  AND LC.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES(+)"   +
				"  AND D.ic_clasificacion = clas.ic_clasificacion (+) "+
//				"  AND PEP.CS_HABILITADO = 'S'"   +
				"  AND PEP.IC_PRODUCTO_NAFIN = 4"   +
				"  AND PN.IC_PRODUCTO_NAFIN = 4"   +
				" AND D.IC_pyme = "+ic_pyme+
					condicion.toString());
					
					
				qryFdR.append("  SELECT /*+ use_nl(D,DS,E,M,PE,PN,TC,TF,ED,LC,I,PEP,CT,TCI)"   +
				"  index(pep cp_comrel_pyme_epo_x_prod_pk)*/"   +
				"  E.CG_RAZON_SOCIAL AS NOMBRE_EPO"   +
				"  ,D.IG_NUMERO_DOCTO"   +
				"  ,D.CC_ACUSE"   +
				" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"   +
				" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"   +
				" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"   +
				" 	,D.IG_PLAZO_DOCTO"   +
				" 	,M.CD_NOMBRE AS MONEDA"   +
				" 	,D.FN_MONTO"   +
				" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
				" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
				" 	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"   +
				" 	,DECODE(D.IC_TIPO_FINANCIAMIENTO,2,0,NVL(D.FN_PORC_DESCUENTO,0)) AS FN_PORC_DESCUENTO"   +
				" 	, 'NA' AS MODO_PLAZO"   +
				" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
				" 	,M.ic_moneda"   +
				" 	,D.ic_documento"   +
				//" 	,I.cg_razon_social as NOMBRE_IF"   +
				"	,decode(ed.ic_estatus_docto,32,i2.cg_razon_social,i.cg_razon_social) AS nombre_if	"+
				" 	,decode(PEP.cg_tipo_credito,'F','Factoraje con Recurso') as TIPO_CREDITO"   +
				" 	,TO_CHAR(DS.DF_FECHA_SELECCION,'DD/MM/YYYY') AS DF_OPERACION_CREDITO"   +
				" 	,d.ig_plazo_credito AS PLAZO_CREDITO"   +
				" 	,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'DD/MM/YYYY') AS DF_VENC_CREDITO"   +
				" 	,CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS AS REF_TASA"   +
				" 	,TCI.CD_DESCRIPCION AS TIPO_COBRO_INT"   +
				"  ,DS.fn_valor_tasa"   +
				" 	,DS.FN_IMPORTE_INTERES"   +
				" 	,LC.IC_MONEDA as moneda_linea, D.IC_ESTATUS_DOCTO,clas.cg_descripcion as CATEGORIA "   +
				" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				" ,  (SELECT nvl(fn_valor_aforo, 0)     FROM dis_aforo_epo   WHERE ic_if = i.ic_if      AND ic_epo = d.ic_epo        AND ic_moneda = d.ic_moneda) AS POR_AFRO " +
				
				"	,d.IC_ORDEN_PAGO_TC as id_orden_enviado	"+
				"	,ptc.CG_TRANSACCION_ID as id_operacion	"+
				//"	,ptc.CG_CODIGO_AUTORIZACION as codigo_autorizacion	"+
				"	 ,CONCAT(ptc.CG_CODIGO_RESP ,ptc.CG_CODIGO_DESC)AS codigo_autorizado	"+
				"	,DF_FECHA_AUTORIZACION as fecha_registro	"+
           "	,trim(cpi.cg_opera_tarjeta) as OPERA_TARJETA "+
			  "	,SUBSTR(ptc.CG_NUM_TARJETA,-4,4) as NUM_TC	"+
			  
			   "	,bins.CG_CODIGO_BIN||' '||bins.CG_DESCRIPCION as bins "+
				" , D.IG_TIPO_PAGO "+
				"  FROM DIS_DOCUMENTO D"   +
				"  ,DIS_DOCTO_SELECCIONADO DS"   +
				"  ,COMCAT_EPO E"   +
				"  ,COMCAT_MONEDA M"   +
				"  ,COMREL_PRODUCTO_EPO PE"   +
				"  ,COMCAT_PRODUCTO_NAFIN PN"   +
				"  ,COM_TIPO_CAMBIO TC"   +
				"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
				"  ,COMCAT_ESTATUS_DOCTO ED"   +
				"  ,COM_LINEA_CREDITO LC"   +
				"  ,COMCAT_IF I"   +
				"  ,COMCAT_IF I2"   +
				"  ,COMREL_PYME_EPO_X_PRODUCTO PEP"   +
				"  ,COMCAT_TASA CT"   +
				"  ,COMCAT_TIPO_COBRO_INTERES TCI,comrel_clasificacion clas "+
				"	,comrel_producto_if cpi "+
				"	,DIS_DOCTOS_PAGO_TC ptc "+
				
					"	,com_bins_if bins "+
				
				"  WHERE D.IC_EPO = E.IC_EPO"   +
				"  AND D.IC_MONEDA = M.IC_MONEDA"   +
				"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
				"  AND PE.IC_EPO = D.IC_EPO"   +
				"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
				"  AND M.ic_moneda = TC.ic_moneda"   +
				"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO(+)"   +
				"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
				"  AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO"   +
				"  AND LC.IC_IF = I.IC_IF(+)"   +
				
				"	and lc.IC_IF = cpi.ic_if "   +
				"	and pep.IC_PRODUCTO_NAFIN = cpi.IC_PRODUCTO_NAFIN "   +
				"	and d.IC_ORDEN_PAGO_TC  =ptc.IC_ORDEN_PAGO_TC (+) "+
				
				//"	and lc.IC_IF = bins.IC_IF "+
				"    AND d.IC_BINS = bins.IC_BINS(+) "+
				"    AND bins.IC_if = i2.IC_if(+)    "+
				"	AND (lc.ic_if = cpi.ic_if or bins.ic_if = cpi.ic_if) "+
				
				"  AND PEP.IC_EPO = E.IC_EPO"   +
				"  AND PEP.IC_EPO = D.IC_EPO"   +
				"  AND PEP.IC_PYME = D.IC_PYME"   +
				"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO (+)"   +
				"  AND DS.IC_TASA = CT.IC_TASA(+)"   +
				"  AND LC.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES(+)"   +
				"  AND D.ic_clasificacion = clas.ic_clasificacion (+) "+
				" and d.IC_TIPO_FINANCIAMIENTO is null "+
				"  AND PEP.IC_PRODUCTO_NAFIN = 4"   +
				"  AND PN.IC_PRODUCTO_NAFIN = 4"   +
				" AND D.IC_pyme = "+ic_pyme+
					condicion.toString());
					
					
			if("D".equals(tipo_credito))
				qrySentencia = qryDM;
			else if("C".equals(tipo_credito))
				qrySentencia = qryCCC;
			else  if("".equals(tipo_credito)  && tipoCreditoXepo.equals("F"))   
				qrySentencia = qryFdR;		
			else
				qrySentencia.append(qryDM.toString() + " UNION ALL "+ qryCCC.toString());

		System.out.println("el query queda de la siguiente manera  "+tipoCreditoXepo  +"--"+tipo_credito+"---"+qrySentencia.toString());
		return qrySentencia.toString();
	}
	
	public String getDocumentQuery(HttpServletRequest request) {

		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
    	StringBuffer qrySentencia	= new StringBuffer();
    	StringBuffer qryDM			= new StringBuffer();
    	StringBuffer qryCCC			= new StringBuffer();
    	StringBuffer condicion		= new StringBuffer();
      StringBuffer qryFdR			= new StringBuffer();

		String	ic_pyme					= request.getSession().getAttribute("iNoCliente").toString();
		String	ic_epo					=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String	ic_if					=	(request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");		
		String	ic_estatus_docto		=	(request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto");
		String 	ig_numero_docto			=	(request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String 	fecha_seleccion_de		=	(request.getParameter("fecha_seleccion_de")==null)?"":request.getParameter("fecha_seleccion_de");
		String 	fecha_seleccion_a		=	(request.getParameter("fecha_seleccion_a")==null)?"":request.getParameter("fecha_seleccion_a");
		String 	cc_acuse				=	(request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String 	monto_credito_de		=	(request.getParameter("monto_credito_de")==null)?"":request.getParameter("monto_credito_de");
		String 	monto_credito_a			=	(request.getParameter("monto_credito_a")==null)?"":request.getParameter("monto_credito_a");
		String 	fecha_emision_de		=	(request.getParameter("fecha_emision_de")==null)?"":request.getParameter("fecha_emision_de");
		String 	fecha_emision_a			=	(request.getParameter("fecha_emision_a")==null)?"":request.getParameter("fecha_emision_a");
		String 	fecha_vto_de			=	(request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String 	fecha_vto_a				=	(request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String 	fecha_vto_credito_de	=	(request.getParameter("fecha_vto_credito_de")==null)?"":request.getParameter("fecha_vto_credito_de");
		String 	fecha_vto_credito_a 	=	(request.getParameter("fecha_vto_credito_a")==null)?"":request.getParameter("fecha_vto_credito_a");
		String 	ic_tipo_cobro_int		=	(request.getParameter("ic_tipo_cobro_int")==null)?"":request.getParameter("ic_tipo_cobro_int");
		String	ic_moneda				=	(request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String 	fn_monto_de				=	(request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String 	fn_monto_a				=	(request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String 	monto_con_descuento		=	(request.getParameter("monto_con_descuento")==null)?"":"checked";
		String	solo_cambio				=	(request.getParameter("solo_cambio")==null)?"":request.getParameter("solo_cambio");
		String	modo_plazo				=	(request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
		String	tipo_credito			=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String	cgTipoConversion		=	(request.getParameter("tipo_conversion")==null)?"":request.getParameter("tipo_conversion");
		String	ic_documento			=	(request.getParameter("ic_documento")==null)?"":request.getParameter("ic_documento");
		String 	fecha_publicacion_de	=	(request.getParameter("fecha_publicacion_de")==null)?fechaHoy:request.getParameter("fecha_publicacion_de");
		String 	fecha_publicacion_a		=	(request.getParameter("fecha_publicacion_a")==null)?fechaHoy:request.getParameter("fecha_publicacion_a");
		String tipoCreditoXepo		=	(request.getParameter("tipoCreditoXepo")==null)?"":request.getParameter("tipoCreditoXepo");
		String tipo_pago = (request.getParameter("tipo_pago")==null)?"":request.getParameter("tipo_pago"); // F009-2015

		try {
			if(!"".equals(ic_epo)&&ic_epo!=null)
				condicion.append(" AND D.IC_EPO = "+ic_epo);
			if("4".equals(ic_estatus_docto)||"20".equals(ic_estatus_docto)||"22".equals(ic_estatus_docto)||"3".equals(ic_estatus_docto)||"11".equals(ic_estatus_docto)||"1".equals(ic_estatus_docto)||"24".equals(ic_estatus_docto)||"32".equals(ic_estatus_docto))//Mod +(||"24".equals(ic_estatus_docto))
				condicion.append(" AND D.IC_ESTATUS_DOCTO ="+ic_estatus_docto);
			if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
				condicion.append(" and D.ig_numero_docto = '"+ig_numero_docto+"'");
			if(!"".equals(fecha_seleccion_de)&& fecha_seleccion_de!=null&&!"".equals(fecha_seleccion_a)&& fecha_seleccion_a!=null)
				condicion.append(" and trunc(DS.df_fecha_seleccion) between trunc(to_date('"+fecha_seleccion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_seleccion_a+"','dd/mm/yyyy'))");
			if(!"".equals(cc_acuse)&& cc_acuse!=null)
				condicion.append(" and D.cc_acuse = '"+cc_acuse+"'");
			if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null)
				condicion.append(" and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date('"+fecha_vto_credito_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_credito_a+"','dd/mm/yyyy'))");		
			if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null)
				condicion.append(" AND LC.ic_tipo_cobro_interes = "+ic_tipo_cobro_int);
			if(!"".equals(ic_moneda)&&ic_moneda!=null)
				condicion.append(" and M.ic_moneda = "+ic_moneda);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
				condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
				condicion.append(" and D.fn_monto between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(solo_cambio)&& solo_cambio!=null)
				condicion.append(" and D.ic_documento in (select ic_documento from dis_cambio_estatus)");
			if(!"".equals(modo_plazo)&& modo_plazo!=null)
				condicion.append(" and D.ic_tipo_financiamiento = "+modo_plazo);
			if(!"".equals(ic_documento)&&ic_documento!=null)
				condicion.append(" AND D.IC_DOCUMENTO = "+ic_documento);
			if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
				condicion.append(" and D.df_carga >= to_date('"+fecha_publicacion_de+"','dd/mm/yyyy') and D.df_carga < to_date('"+fecha_publicacion_a+"','dd/mm/yyyy') + 1");
			if(!"".equals(monto_credito_de)&& monto_credito_de!=null&&!"".equals(monto_credito_a)&& monto_credito_a!=null)
				condicion.append(" and (D.fn_monto-(fn_monto*(fn_porc_descuento/100))) between "+monto_credito_de+" and "+monto_credito_a);	
			if("".equals(ic_estatus_docto))
				condicion.append(" and D.ic_estatus_docto in (4,20,22,3,11,1,24,32)");//Mod +(24)
			if(!"".equals(ic_estatus_docto)&&!"4".equals(ic_estatus_docto)&&!"20".equals(ic_estatus_docto)&&!"22".equals(ic_estatus_docto)&&!"3".equals(ic_estatus_docto)&&!"11".equals(ic_estatus_docto)&&!"1".equals(ic_estatus_docto)&&!"24".equals(ic_estatus_docto)&&!"32".equals(ic_estatus_docto))//Mod +(&&!"24".equals(ic_estatus_docto))
				condicion.append(" and D.ic_estatus_docto is null");
			if(!"".equals(ic_if)&&ic_if!=null)
				condicion.append(" AND LC.IC_IF = "+ic_if);
			if(!tipo_pago.equals("")){
				condicion.append(" AND D.IG_TIPO_PAGO = " + tipo_pago);
			}

			qryDM.append("    SELECT /*+index(d) use_nl(d, ds, m, pe, epo, pn, tc, lc, pep)*/ "   +
					"    D.ic_documento "   +
					"    FROM DIS_DOCUMENTO D"   +
					"    ,DIS_DOCTO_SELECCIONADO DS"   +
					"    ,COMCAT_MONEDA M"   +
					"    ,COMREL_PRODUCTO_EPO PE"   +
					"    ,COMCAT_EPO EPO"   +
					"    ,COMCAT_PRODUCTO_NAFIN PN"   +
					"    ,COM_TIPO_CAMBIO TC"   +
					"    ,DIS_LINEA_CREDITO_DM LC"   +
					"    ,COMREL_PYME_EPO_X_PRODUCTO PEP"   +
					"    WHERE D.IC_MONEDA = M.IC_MONEDA"   +
					"    AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO (+)"   +
					"    AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"    AND PE.IC_EPO = D.IC_EPO"   +
					"    AND D.IC_EPO = EPO.IC_EPO"   +
					"    AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"    AND M.ic_moneda = TC.ic_moneda"   +
					"    AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM (+)"   +
					"    AND PEP.IC_EPO = D.IC_EPO"   +
					"    AND PEP.IC_PYME = D.IC_PYME"   +
					"    AND PN.IC_PRODUCTO_NAFIN = 4"   +
//					"    AND PEP.CS_HABILITADO = 'S'"   +
					"    AND PEP.IC_PRODUCTO_NAFIN = 4"   +
					"    AND EPO.CS_HABILITADO = 'S' "   +
					" 	and d.IC_TIPO_FINANCIAMIENTO is not  null "+
					" AND D.IC_pyme = "+ic_pyme+
					condicion.toString());

		qryCCC.append("   SELECT /*+ index(d) index(pep cp_comrel_pyme_epo_x_prod_pk)*/"   +
					"   d.ic_documento"   +
					"   FROM DIS_DOCUMENTO D"   +
					"   ,DIS_DOCTO_SELECCIONADO DS"   +
					"   ,COMCAT_MONEDA M"   +
					"   ,COMREL_PRODUCTO_EPO PE"   +
					"   ,COMCAT_EPO EPO"   +
					"   ,COMCAT_PRODUCTO_NAFIN PN"   +
					"   ,COM_TIPO_CAMBIO TC"   +
					"   ,COM_LINEA_CREDITO LC"   +
					"   ,COMREL_PYME_EPO_X_PRODUCTO PEP"   +
					"	,com_bins_if bins	"+//FODEA-013-2014
					"   WHERE D.IC_MONEDA = M.IC_MONEDA"   +
					"   AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO (+)"   +
					"   AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"	and d.IC_BINS = bins.IC_BINS(+)	"	+//FODEA-013-2014
					"   AND PE.IC_EPO = D.IC_EPO"   +
					"   AND D.IC_EPO = EPO.IC_EPO"   +
					"   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"   AND M.ic_moneda = TC.ic_moneda"   +
					"   AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO"   +
					"   AND PEP.IC_EPO = D.IC_EPO"   +
					"   AND PEP.IC_PYME = D.IC_PYME"   +
					"   AND PEP.IC_PRODUCTO_NAFIN = 4"   +
//					"   AND PEP.CS_HABILITADO = 'S'"   +
					"   AND PN.IC_PRODUCTO_NAFIN = 4"   +
					"   AND EPO.CS_HABILITADO = 'S' "   +
					" 	and d.IC_TIPO_FINANCIAMIENTO is not  null "+
					" AND D.IC_pyme = "+ic_pyme+
					condicion.toString());
					
			qryFdR.append("   SELECT /*+ index(d) index(pep cp_comrel_pyme_epo_x_prod_pk)*/"   +
					"   d.ic_documento"   +
					"   FROM DIS_DOCUMENTO D"   +
					"   ,DIS_DOCTO_SELECCIONADO DS"   +
					"   ,COMCAT_MONEDA M"   +
					"   ,COMREL_PRODUCTO_EPO PE"   +
					"   ,COMCAT_EPO EPO"   +
					"   ,COMCAT_PRODUCTO_NAFIN PN"   +
					"   ,COM_TIPO_CAMBIO TC"   +
					"   ,COM_LINEA_CREDITO LC"   +
					"   ,COMREL_PYME_EPO_X_PRODUCTO PEP"   +
					"   WHERE D.IC_MONEDA = M.IC_MONEDA"   +
					"   AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO (+)"   +
					"   AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
					"   AND PE.IC_EPO = D.IC_EPO"   +
					"   AND D.IC_EPO = EPO.IC_EPO"   +
					"   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
					"   AND M.ic_moneda = TC.ic_moneda"   +
					"   AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO"   +
					"   AND PEP.IC_EPO = D.IC_EPO"   +
					"   AND PEP.IC_PYME = D.IC_PYME"   +
					"   AND PEP.IC_PRODUCTO_NAFIN = 4"   +
					"   AND PN.IC_PRODUCTO_NAFIN = 4"   +
					"   AND EPO.CS_HABILITADO = 'S' "   +
					" 	 AND d.IC_TIPO_FINANCIAMIENTO is null "+
					"   AND D.IC_pyme = "+ic_pyme+
					condicion.toString());
					
					
			if("D".equals(tipo_credito))
				qrySentencia = qryDM;
			else if("C".equals(tipo_credito))
				qrySentencia = qryCCC;
			else  if("".equals(tipo_credito)  && tipoCreditoXepo.equals("F"))   
				qrySentencia = qryFdR;
			else
				qrySentencia.append(qryDM.toString() + " UNION ALL "+ qryCCC.toString());

				System.out.println("EL query de la llave primaria: "+qrySentencia.toString());
		}catch(Exception e){
			System.out.println("ConsInfDocPymeDist2::getDocumentQueryException "+e);
		}
		return qrySentencia.toString();
	}
	
	public String getDocumentQueryFile(HttpServletRequest request) {

		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
    	StringBuffer qrySentencia	= new StringBuffer();
    	StringBuffer qryDM			= new StringBuffer();
    	StringBuffer qryCCC			= new StringBuffer();
    	StringBuffer condicion		= new StringBuffer();
      StringBuffer qryFdR			= new StringBuffer();

		String	ic_pyme					= request.getSession().getAttribute("iNoCliente").toString();
		String	ic_epo					=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String	ic_if					=	(request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");		
		String	ic_estatus_docto		=	(request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto");
		String 	ig_numero_docto			=	(request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String 	fecha_seleccion_de		=	(request.getParameter("fecha_seleccion_de")==null)?"":request.getParameter("fecha_seleccion_de");
		String 	fecha_seleccion_a		=	(request.getParameter("fecha_seleccion_a")==null)?"":request.getParameter("fecha_seleccion_a");
		String 	cc_acuse				=	(request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String 	monto_credito_de		=	(request.getParameter("monto_credito_de")==null)?"":request.getParameter("monto_credito_de");
		String 	monto_credito_a			=	(request.getParameter("monto_credito_a")==null)?"":request.getParameter("monto_credito_a");
		String 	fecha_emision_de		=	(request.getParameter("fecha_emision_de")==null)?"":request.getParameter("fecha_emision_de");
		String 	fecha_emision_a			=	(request.getParameter("fecha_emision_a")==null)?"":request.getParameter("fecha_emision_a");
		String 	fecha_vto_de			=	(request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String 	fecha_vto_a				=	(request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String 	fecha_vto_credito_de	=	(request.getParameter("fecha_vto_credito_de")==null)?"":request.getParameter("fecha_vto_credito_de");
		String 	fecha_vto_credito_a 	=	(request.getParameter("fecha_vto_credito_a")==null)?"":request.getParameter("fecha_vto_credito_a");
		String 	ic_tipo_cobro_int		=	(request.getParameter("ic_tipo_cobro_int")==null)?"":request.getParameter("ic_tipo_cobro_int");
		String	ic_moneda				=	(request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String 	fn_monto_de				=	(request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String 	fn_monto_a				=	(request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String 	monto_con_descuento		=	(request.getParameter("monto_con_descuento")==null)?"":"checked";
		String	solo_cambio				=	(request.getParameter("solo_cambio")==null)?"":request.getParameter("solo_cambio");
		String	modo_plazo				=	(request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
		String	tipo_credito			=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String	cgTipoConversion		=	(request.getParameter("tipo_conversion")==null)?"":request.getParameter("tipo_conversion");
		String	ic_documento			=	(request.getParameter("ic_documento")==null)?"":request.getParameter("ic_documento");
		String 	fecha_publicacion_de	=	(request.getParameter("fecha_publicacion_de")==null)?fechaHoy:request.getParameter("fecha_publicacion_de");
		String 	fecha_publicacion_a		=	(request.getParameter("fecha_publicacion_a")==null)?fechaHoy:request.getParameter("fecha_publicacion_a");
		String tipoCreditoXepo		=	(request.getParameter("tipoCreditoXepo")==null)?"":request.getParameter("tipoCreditoXepo");
		String tipo_pago = (request.getParameter("tipo_pago")==null)?"":request.getParameter("tipo_pago"); // F009-2015

		try {
			if(!"".equals(ic_epo)&&ic_epo!=null)
				condicion.append(" AND D.IC_EPO = "+ic_epo);
			if("4".equals(ic_estatus_docto)||"20".equals(ic_estatus_docto)||"22".equals(ic_estatus_docto)||"3".equals(ic_estatus_docto)||"11".equals(ic_estatus_docto)||"1".equals(ic_estatus_docto)||"24".equals(ic_estatus_docto)||"32".equals(ic_estatus_docto))//Mod +(||"24".equals(ic_estatus_docto))
				condicion.append(" AND D.IC_ESTATUS_DOCTO ="+ic_estatus_docto);
			if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
				condicion.append(" and D.ig_numero_docto = '"+ig_numero_docto+"'");
			if(!"".equals(fecha_seleccion_de)&& fecha_seleccion_de!=null&&!"".equals(fecha_seleccion_a)&& fecha_seleccion_a!=null)
				condicion.append(" and trunc(DS.df_fecha_seleccion) between trunc(to_date('"+fecha_seleccion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_seleccion_a+"','dd/mm/yyyy'))");
			if(!"".equals(cc_acuse)&& cc_acuse!=null)
				condicion.append(" and D.cc_acuse = '"+cc_acuse+"'");
			if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null)
				condicion.append(" and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null)
				condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date('"+fecha_vto_credito_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_credito_a+"','dd/mm/yyyy'))");		
			if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null)
				condicion.append(" AND LC.ic_tipo_cobro_interes = "+ic_tipo_cobro_int);
			if(!"".equals(ic_moneda)&&ic_moneda!=null)
				condicion.append(" and M.ic_moneda = "+ic_moneda);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
				condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
				condicion.append(" and D.fn_monto between "+fn_monto_de+" and "+fn_monto_a);
			if(!"".equals(solo_cambio)&& solo_cambio!=null)
				condicion.append(" and D.ic_documento in (select ic_documento from dis_cambio_estatus)");
			if(!"".equals(modo_plazo)&& modo_plazo!=null)
				condicion.append(" and D.ic_tipo_financiamiento = "+modo_plazo);
			if(!"".equals(ic_documento)&&ic_documento!=null)
				condicion.append(" AND D.IC_DOCUMENTO = "+ic_documento);
			if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
				condicion.append(" and D.df_carga >= to_date('"+fecha_publicacion_de+"','dd/mm/yyyy') and D.df_carga < to_date('"+fecha_publicacion_a+"','dd/mm/yyyy') + 1");
			if(!"".equals(monto_credito_de)&& monto_credito_de!=null&&!"".equals(monto_credito_a)&& monto_credito_a!=null)
				condicion.append(" and (D.fn_monto-(fn_monto*(fn_porc_descuento/100))) between "+monto_credito_de+" and "+monto_credito_a);	
			if("".equals(ic_estatus_docto))
				condicion.append(" and D.ic_estatus_docto in (4,20,22,3,11,1,24,32)");//Mod +(24)
			if(!"".equals(ic_estatus_docto)&&!"4".equals(ic_estatus_docto)&&!"20".equals(ic_estatus_docto)&&!"22".equals(ic_estatus_docto)&&!"3".equals(ic_estatus_docto)&&!"11".equals(ic_estatus_docto)&&!"1".equals(ic_estatus_docto)&&!"24".equals(ic_estatus_docto)&&!"32".equals(ic_estatus_docto))//Mod +(&&!"24".equals(ic_estatus_docto))
				condicion.append(" and D.ic_estatus_docto is null");
			if(!"".equals(ic_if)&&ic_if!=null)
				condicion.append(" AND LC.IC_IF = "+ic_if);
			if(!tipo_pago.equals("")){
				condicion.append(" AND D.IG_TIPO_PAGO = " + tipo_pago);
			}

			qryDM.append("  SELECT /*+ use_nl(d,ds,e,m,pe,pn,tc,tf,ed,lc,i,pep,ct,tci)"   +
				"     index(pep cp_comrel_pyme_epo_x_prod_pk)*/"   +
				"     E.CG_RAZON_SOCIAL AS NOMBRE_EPO"   +
				"  	,D.IG_NUMERO_DOCTO"   +
				"  	,DS.CC_ACUSE"   +
				" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"   +
				" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"   +
				" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"   +
				" 	,D.IG_PLAZO_DOCTO"   +
				" 	,M.CD_NOMBRE AS MONEDA"   +
				" 	,D.FN_MONTO"   +
				" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
				" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
				" 	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"   +
				" 	,DECODE(D.IC_TIPO_FINANCIAMIENTO,2,0,NVL(D.FN_PORC_DESCUENTO,0)) AS FN_PORC_DESCUENTO"   +
				" 	,TF.CD_DESCRIPCION AS MODO_PLAZO"   +
				" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
				" 	,M.ic_moneda"   +
				" 	,D.ic_documento"   +
				" 	,I.cg_razon_social as NOMBRE_IF"   +
				//"	,DECODE (ed.ic_estatus_docto,32, i2.cg_razon_social, i.cg_razon_social) AS nombre_if	"+
				" 	,decode(PEP.cg_tipo_credito,'D','Descuento','Credito en Cuenta Corriente') as TIPO_CREDITO"   +
				" 	,TO_CHAR(DS.DF_FECHA_SELECCION,'DD/MM/YYYY') AS DF_OPERACION_CREDITO"   +
				" 	,d.ig_plazo_credito AS PLAZO_CREDITO"   +
				" 	,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'DD/MM/YYYY') AS DF_VENC_CREDITO"   +
				"	,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) AS REF_TASA" +
				" 	,TCI.CD_DESCRIPCION AS TIPO_COBRO_INT"   +
				"  ,DS.fn_valor_tasa"   +
				" 	,DS.FN_IMPORTE_INTERES"   +
				" 	,LC.ic_moneda as moneda_linea, D.IC_ESTATUS_DOCTO,clas.cg_descripcion as CATEGORIA "   +
				" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				" ,  (SELECT nvl(fn_valor_aforo, 0)     FROM dis_aforo_epo   WHERE ic_if = i.ic_if      AND ic_epo = d.ic_epo        AND ic_moneda = d.ic_moneda) AS POR_AFRO  "+
				/*
				"	,d.IC_ORDEN_PAGO_TC as id_orden_enviado	"+
				"	,ptc.CG_TRANSACCION_ID as id_operacion	"+
				//"	,ptc.CG_CODIGO_AUTORIZACION as codigo_autorizacion	"+
				"	,CONCAT (ptc.cg_codigo_resp, ptc.cg_codigo_desc) AS codigo_autorizado	"+
				"	,DF_FECHA_AUTORIZACION as fecha_registro	"+
           "	,trim(cpi.cg_opera_tarjeta) as OPERA_TARJETA "+
			*/
			
				"	,'' as id_orden_enviado 	"	+
				"	,'' as id_operacion 	"	+
				"	,'' as codigo_autorizado 	"	+
				"	,'' as fecha_registro 	"	+
				"	,'' as OPERA_TARJETA 	"	+
				"	,'' as bins	"+
				"	,'' as NUM_TC 	"	+
				" , D.IG_TIPO_PAGO "+
				"  FROM DIS_DOCUMENTO D"   +
				"  ,DIS_DOCTO_SELECCIONADO DS"   +
				"  ,COMCAT_EPO E"   +
				"  ,COMCAT_MONEDA M"   +
				"  ,COMREL_PRODUCTO_EPO PE"   +
				"  ,COMCAT_PRODUCTO_NAFIN PN"   +
				"  ,COM_TIPO_CAMBIO TC"   +
				"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
				"  ,COMCAT_ESTATUS_DOCTO ED"   +
				"  ,DIS_LINEA_CREDITO_DM LC"   +
				"  ,COMCAT_IF I"   +
				//"  ,COMCAT_IF I2"   +
				"  ,COMREL_PYME_EPO_X_PRODUCTO PEP"   +
				"  ,COMCAT_TASA CT"   +
				"  ,COMCAT_TIPO_COBRO_INTERES TCI,comrel_clasificacion clas "+
				//"	,comrel_producto_if cpi "+
				
				//"	,DIS_DOCTOS_PAGO_TC ptc "+
				
				"  WHERE D.IC_EPO = E.IC_EPO"   +
				"  AND D.IC_MONEDA = M.IC_MONEDA"   +
				"  AND PN.IC_PRODUCTO_NAFIN = 4"   +
				"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
				"  AND PE.IC_EPO = D.IC_EPO"   +
				"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
				"  AND M.ic_moneda = TC.ic_moneda"   +
				"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
				"  AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO"   +
				"  AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM (+)"   +
				"  AND LC.IC_IF = I.IC_IF(+)"   +
				/*
				//"	and lc.IC_IF = cpi.ic_if "   +
				"	AND d.ic_bins = bins.ic_bins(+)	"+
				"	AND bins.ic_if = i2.ic_if(+)	"+
				
				"	and pep.IC_PRODUCTO_NAFIN = cpi.IC_PRODUCTO_NAFIN "   +
				"	and d.IC_ORDEN_PAGO_TC  =ptc.IC_ORDEN_PAGO_TC (+) "+
				*/
				"  AND PEP.IC_EPO = E.IC_EPO"   +
				"  AND PEP.IC_EPO = D.IC_EPO"   +
				"  AND PEP.IC_PYME = D.IC_PYME"   +
//				"  AND PEP.CS_HABILITADO = 'S'"   +
				"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO (+)"   +
				"  AND DS.IC_TASA = CT.IC_TASA(+)"   +
				"  AND LC.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES(+)"   +
				" AND D.ic_clasificacion = clas.ic_clasificacion (+) "+
				"  AND PEP.IC_PRODUCTO_NAFIN = 4"   +
				"  AND E.CS_HABILITADO = 'S' "   +
				" AND D.IC_pyme = "+ic_pyme+
				condicion.toString());

		qryCCC.append("  SELECT /*+ use_nl(D,DS,E,M,PE,PN,TC,TF,ED,LC,I,PEP,CT,TCI)"   +
				"  index(pep cp_comrel_pyme_epo_x_prod_pk)*/"   +
				"  E.CG_RAZON_SOCIAL AS NOMBRE_EPO"   +
				"  ,D.IG_NUMERO_DOCTO"   +
				"  ,D.CC_ACUSE"   +
				" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"   +
				" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"   +
				" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"   +
				" 	,D.IG_PLAZO_DOCTO"   +
				" 	,M.CD_NOMBRE AS MONEDA"   +
				" 	,D.FN_MONTO"   +
				" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
				" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
				" 	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"   +
				" 	,DECODE(D.IC_TIPO_FINANCIAMIENTO,2,0,NVL(D.FN_PORC_DESCUENTO,0)) AS FN_PORC_DESCUENTO"   +
				" 	,TF.CD_DESCRIPCION AS MODO_PLAZO"   +
				" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
				" 	,M.ic_moneda"   +
				" 	,D.ic_documento"   +
				//" 	,I.cg_razon_social as NOMBRE_IF"   +
				"	,DECODE (ed.ic_estatus_docto,32, i2.cg_razon_social, i.cg_razon_social) AS nombre_if	"+
				" 	,decode(PEP.cg_tipo_credito,'D','Descuento','Credito en Cuenta Corriente') as TIPO_CREDITO"   +
				" 	,TO_CHAR(DS.DF_FECHA_SELECCION,'DD/MM/YYYY') AS DF_OPERACION_CREDITO"   +
				" 	,d.ig_plazo_credito AS PLAZO_CREDITO"   +
				" 	,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'DD/MM/YYYY') AS DF_VENC_CREDITO"   +
				" 	,CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS AS REF_TASA"   +
				" 	,TCI.CD_DESCRIPCION AS TIPO_COBRO_INT"   +
				"  ,DS.fn_valor_tasa"   +
				" 	,DS.FN_IMPORTE_INTERES"   +
				" 	,LC.IC_MONEDA as moneda_linea, D.IC_ESTATUS_DOCTO,clas.cg_descripcion as CATEGORIA "   +
				" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				" ,  (SELECT nvl(fn_valor_aforo, 0)     FROM dis_aforo_epo   WHERE ic_if = i.ic_if      AND ic_epo = d.ic_epo        AND ic_moneda = d.ic_moneda) AS POR_AFRO  "+
			
			"	,d.IC_ORDEN_PAGO_TC as id_orden_enviado	"+
				"	,ptc.CG_TRANSACCION_ID as id_operacion	"+
				//"	,ptc.CG_CODIGO_AUTORIZACION as codigo_autorizacion	"+
				"	,CONCAT (ptc.cg_codigo_resp, ptc.cg_codigo_desc) AS codigo_autorizado	"+
				"	,DF_FECHA_AUTORIZACION as fecha_registro	"+
           "	,trim(cpi.cg_opera_tarjeta) as OPERA_TARJETA "+			
				"	,bins.CG_CODIGO_BIN||' '||bins.CG_DESCRIPCION as bins "+
				"	,SUBSTR(ptc.CG_NUM_TARJETA,-4,4) as NUM_TC	"+
				" , D.IG_TIPO_PAGO "+
				"  FROM DIS_DOCUMENTO D"   +
				"  ,DIS_DOCTO_SELECCIONADO DS"   +
				"  ,COMCAT_EPO E"   +
				"  ,COMCAT_MONEDA M"   +
				"  ,COMREL_PRODUCTO_EPO PE"   +
				"  ,COMCAT_PRODUCTO_NAFIN PN"   +
				"  ,COM_TIPO_CAMBIO TC"   +
				"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
				"  ,COMCAT_ESTATUS_DOCTO ED"   +
				"  ,COM_LINEA_CREDITO LC"   +
				"  ,COMCAT_IF I"   +
				"  ,COMCAT_IF I2"   +
				"  ,COMREL_PYME_EPO_X_PRODUCTO PEP"   +
				"  ,COMCAT_TASA CT"   +
				"  ,COMCAT_TIPO_COBRO_INTERES TCI,comrel_clasificacion clas "+
				"	,comrel_producto_if cpi "+
				"	,DIS_DOCTOS_PAGO_TC ptc "+
				
				"	,com_bins_if bins "+
				
				"  WHERE D.IC_EPO = E.IC_EPO"   +
				"  AND D.IC_MONEDA = M.IC_MONEDA"   +
				"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
				"  AND PE.IC_EPO = D.IC_EPO"   +
				"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
				"  AND M.ic_moneda = TC.ic_moneda"   +
				"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO"   +
				"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
				"  AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO"   +
				"  AND LC.IC_IF = I.IC_IF(+)"   +
				
				//"	and lc.IC_IF = cpi.ic_if "   +
				"	AND d.ic_bins = bins.ic_bins(+)	"+
				"	AND bins.ic_if = i2.ic_if(+)	"+
				"	and lc.IC_IF = cpi.IC_IF	"+
				
				"	and pep.IC_PRODUCTO_NAFIN = cpi.IC_PRODUCTO_NAFIN "   +
				"	and d.IC_ORDEN_PAGO_TC  =ptc.IC_ORDEN_PAGO_TC (+) "+
				
				"  AND PEP.IC_EPO = E.IC_EPO"   +
				"  AND PEP.IC_EPO = D.IC_EPO"   +
				"  AND PEP.IC_PYME = D.IC_PYME"   +
				"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO (+)"   +
				"  AND DS.IC_TASA = CT.IC_TASA(+)"   +
				"  AND LC.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES(+)"   +
				" AND D.ic_clasificacion = clas.ic_clasificacion (+) "+
//				"  AND PEP.CS_HABILITADO = 'S'"   +
				"  AND PEP.IC_PRODUCTO_NAFIN = 4"   +
				"  AND PN.IC_PRODUCTO_NAFIN = 4"   +
				"  AND E.CS_HABILITADO = 'S' "   +
				" AND D.IC_pyme = "+ic_pyme+
					condicion.toString());
					
			qryFdR.append("  SELECT /*+ use_nl(D,DS,E,M,PE,PN,TC,TF,ED,LC,I,PEP,CT,TCI)"   +
				"  index(pep cp_comrel_pyme_epo_x_prod_pk)*/"   +
				"  E.CG_RAZON_SOCIAL AS NOMBRE_EPO"   +
				"  ,D.IG_NUMERO_DOCTO"   +
				"  ,D.CC_ACUSE"   +
				" 	,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION"   +
				" 	,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA"   +
				" 	,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC"   +
				" 	,D.IG_PLAZO_DOCTO"   +
				" 	,M.CD_NOMBRE AS MONEDA"   +
				" 	,D.FN_MONTO"   +
				" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION"   +
				" 	,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO"   +
				" 	,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO"   +
				" 	,DECODE(D.IC_TIPO_FINANCIAMIENTO,2,0,NVL(D.FN_PORC_DESCUENTO,0)) AS FN_PORC_DESCUENTO"   +
				" 	,'NA' AS MODO_PLAZO"   +
				" 	,ED.CD_DESCRIPCION AS ESTATUS"   +
				" 	,M.ic_moneda"   +
				" 	,D.ic_documento"   +
				//" 	,I.cg_razon_social as NOMBRE_IF"   +
				"	,DECODE (ed.ic_estatus_docto,32, i2.cg_razon_social, i.cg_razon_social) AS nombre_if	"+
				" 	,decode(PEP.cg_tipo_credito,'F','Factoraje con Recurso') as TIPO_CREDITO"   +
				" 	,TO_CHAR(DS.DF_FECHA_SELECCION,'DD/MM/YYYY') AS DF_OPERACION_CREDITO"   +
				" 	,d.ig_plazo_credito AS PLAZO_CREDITO"   +
				" 	,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'DD/MM/YYYY') AS DF_VENC_CREDITO"   +
				" 	,CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS AS REF_TASA"   +
				" 	,TCI.CD_DESCRIPCION AS TIPO_COBRO_INT"   +
				"  ,DS.fn_valor_tasa"   +
				" 	,DS.FN_IMPORTE_INTERES"   +
				" 	,LC.IC_MONEDA as moneda_linea, D.IC_ESTATUS_DOCTO,clas.cg_descripcion as CATEGORIA "   +
				" ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				" ,  (SELECT nvl(fn_valor_aforo, 0)     FROM dis_aforo_epo   WHERE ic_if = i.ic_if      AND ic_epo = d.ic_epo        AND ic_moneda = d.ic_moneda) AS POR_AFRO  "+
				
				"	,d.IC_ORDEN_PAGO_TC as id_orden_enviado	"+
				"	,ptc.CG_TRANSACCION_ID as id_operacion	"+
				//"	,ptc.CG_CODIGO_AUTORIZACION as codigo_autorizacion	"+
				"	,CONCAT (ptc.cg_codigo_resp, ptc.cg_codigo_desc) AS codigo_autorizado	"+
				"	,DF_FECHA_AUTORIZACION as fecha_registro	"+
           "	,trim(cpi.cg_opera_tarjeta) as OPERA_TARJETA "+
				"	,bins.CG_CODIGO_BIN||' '||bins.CG_DESCRIPCION as bins "+
				"	,SUBSTR(ptc.CG_NUM_TARJETA,-4,4) as NUM_TC	"+
				" , D.IG_TIPO_PAGO "+
				"  FROM DIS_DOCUMENTO D"   +
				"  ,DIS_DOCTO_SELECCIONADO DS"   +
				"  ,COMCAT_EPO E"   +
				"  ,COMCAT_MONEDA M"   +
				"  ,COMREL_PRODUCTO_EPO PE"   +
				"  ,COMCAT_PRODUCTO_NAFIN PN"   +
				"  ,COM_TIPO_CAMBIO TC"   +
				"  ,COMCAT_TIPO_FINANCIAMIENTO TF"   +
				"  ,COMCAT_ESTATUS_DOCTO ED"   +
				"  ,COM_LINEA_CREDITO LC"   +
				"  ,COMCAT_IF I"   +
				"  ,COMCAT_IF I2"   +
				"  ,COMREL_PYME_EPO_X_PRODUCTO PEP"   +
				"  ,COMCAT_TASA CT"   +
				"  ,COMCAT_TIPO_COBRO_INTERES TCI,comrel_clasificacion clas "+
				"	,comrel_producto_if cpi "+
				"	,DIS_DOCTOS_PAGO_TC ptc "+
				
				"	,com_bins_if bins "+
				
				"  WHERE D.IC_EPO = E.IC_EPO"   +
				"  AND D.IC_MONEDA = M.IC_MONEDA"   +
				"  AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN"   +
				"  AND PE.IC_EPO = D.IC_EPO"   +
				"  AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)"   +
				"  AND M.ic_moneda = TC.ic_moneda"   +
				"  AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO(+)"   +
				"  AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO"   +
				"  AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO"   +
				"  AND LC.IC_IF = I.IC_IF(+)"   +
				
				//"	and lc.IC_IF = cpi.ic_if "   +
				"	AND d.ic_bins = bins.ic_bins(+)	"+
				"	AND bins.ic_if = i2.ic_if(+)	"+
				"	and lc.IC_IF = cpi.IC_IF	"+
				
				"	and pep.IC_PRODUCTO_NAFIN = cpi.IC_PRODUCTO_NAFIN "   +
				"	and d.IC_ORDEN_PAGO_TC  =ptc.IC_ORDEN_PAGO_TC (+) "+
				
				"  AND PEP.IC_EPO = E.IC_EPO"   +
				"  AND PEP.IC_EPO = D.IC_EPO"   +
				"  AND PEP.IC_PYME = D.IC_PYME"   +
				"  AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO (+)"   +
				"  AND DS.IC_TASA = CT.IC_TASA(+)"   +
				"  AND LC.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES(+)"   +
				" AND D.ic_clasificacion = clas.ic_clasificacion (+) "+
				"  AND PEP.IC_PRODUCTO_NAFIN = 4"   +
				"  AND PN.IC_PRODUCTO_NAFIN = 4"   +
				"  AND E.CS_HABILITADO = 'S' "   +
				" and d.IC_TIPO_FINANCIAMIENTO is null "+
				" AND D.IC_pyme = "+ic_pyme+
					condicion.toString());
										
			if("D".equals(tipo_credito))
				qrySentencia = qryDM;
			else if("C".equals(tipo_credito))
				qrySentencia = qryCCC;
			else  if("".equals(tipo_credito)  && tipoCreditoXepo.equals("F"))   
				qrySentencia = qryFdR;
			else
				qrySentencia.append(qryDM.toString() + " UNION ALL "+ qryCCC.toString());

			System.out.println("EL query queda as� : "+qrySentencia.toString());
		}catch(Exception e){
			System.out.println("ConsInfDocPymeDist2::getDocumentQueryFileException "+e);
		}
		return qrySentencia.toString();
	}

/*******************************************************************************
 *          Migraci�n. Implementa IQueryGeneratorRegExtJS.java                 *
 *******************************************************************************/
	private static final Log log = ServiceLocator.getInstance().getLog(ConsInfDocPymeDist2.class);
	private List conditions;
	private String ic_moneda;
	private String ic_pyme;
	private String ic_epo;
	private String ic_if;
	private String ic_estatus_docto;
	private String ig_numero_docto;
	private String fecha_seleccion_de;
	private String fecha_seleccion_a;
	private String cc_acuse;
	private String monto_credito_de;
	private String monto_credito_a;
	private String fecha_emision_de;
	private String fecha_emision_a;
	private String fecha_vto_de;
	private String fecha_vto_a;
	private String fecha_vto_credito_de;
	private String fecha_vto_credito_a;
	private String ic_tipo_cobro_int;
	private String c_moneda;
	private String fn_monto_de;
	private String fn_monto_a;
	private String monto_con_descuento;
	private String solo_cambio;
	private String modo_plazo;
	private String tipo_credito;
	private String cgTipoConversion;
	private String ic_documento;
	private String fecha_publicacion_de;
	private String fecha_publicacion_a;
	private String tipoCreditoXepo;
	private String tipoConversion;
	private String tipo_pago;
	private String publicaDoctosFinanciables;

	public String getDocumentQuery() {
		return null;
	}

	public String getDocumentSummaryQueryForIds(List ids) {
		return null;
	}

	public String getAggregateCalculationQuery() {
		return null;
	}

/**
 * Se forma la consulta para generar los archivos pdf y csv
 * @return qrySentencia
 */
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer qryDM = new StringBuffer();
		StringBuffer qryCCC = new StringBuffer();
		StringBuffer qryFdR = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();

		if(!"".equals(ic_epo) && ic_epo!=null){
			condicion.append(" AND D.IC_EPO = ?");
			conditions.add(ic_epo);
		}
		if("4".equals(ic_estatus_docto)||"20".equals(ic_estatus_docto)||"22".equals(ic_estatus_docto)||"3".equals(ic_estatus_docto)||"11".equals(ic_estatus_docto)||"1".equals(ic_estatus_docto)||"24".equals(ic_estatus_docto)||"32".equals(ic_estatus_docto)){
			condicion.append(" AND D.IC_ESTATUS_DOCTO = ?");
			conditions.add(ic_estatus_docto);
		}
		if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null){
			condicion.append(" AND D.ig_numero_docto = ?");
			conditions.add(ig_numero_docto);
		}
		if(!"".equals(fecha_seleccion_de)&& fecha_seleccion_de!=null&&!"".equals(fecha_seleccion_a)&& fecha_seleccion_a!=null){
			condicion.append(" and trunc(DS.df_fecha_seleccion) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");
			conditions.add(fecha_seleccion_de);
			conditions.add(fecha_seleccion_a);
		}
		if(!"".equals(cc_acuse)&& cc_acuse!=null){
			condicion.append(" AND D.cc_acuse = ?");
			conditions.add(cc_acuse);
		}
		if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null){
			condicion.append(" and trunc(D.df_fecha_emision) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");
			conditions.add(fecha_emision_de);
			conditions.add(fecha_emision_a);
		}
		if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null){
			condicion.append(" and trunc(D.df_fecha_venc) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");
			conditions.add(fecha_vto_de);
			conditions.add(fecha_vto_a);
		}
		if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null){
			condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");
			conditions.add(fecha_vto_credito_de);
			conditions.add(fecha_vto_credito_a);
		}
		if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null){
			condicion.append(" AND LC.ic_tipo_cobro_interes = ?");
			conditions.add(ic_tipo_cobro_int);
		}
		if(!"".equals(ic_moneda)&&ic_moneda!=null){
			condicion.append(" AND M.ic_moneda = ?");
			conditions.add(ic_moneda);
		}
		if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento)){
			condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between ? and ?\"");
			conditions.add(fn_monto_de);
			conditions.add(fn_monto_a);
		}
		if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento)){
			condicion.append(" and D.fn_monto between ? and ?");
			conditions.add(fn_monto_de);
			conditions.add(fn_monto_a);
		}
		if(!"".equals(solo_cambio)&& solo_cambio!=null){
			condicion.append(" and D.ic_documento in (select ic_documento from dis_cambio_estatus)");
		}
		if(!"".equals(modo_plazo)&& modo_plazo!=null){
			condicion.append(" AND D.ic_tipo_financiamiento = ?");
			conditions.add(modo_plazo);
		}
		if(!"".equals(ic_documento)&&ic_documento!=null){
			condicion.append(" AND D.IC_DOCUMENTO = ?");
			conditions.add(ic_documento);
			}
		if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null){
			condicion.append(" and D.df_carga >= to_date(?,'dd/mm/yyyy') and D.df_carga < to_date(?,'dd/mm/yyyy') + 1");
			conditions.add(fecha_publicacion_de);
			conditions.add(fecha_publicacion_a);
		}
		if(!"".equals(monto_credito_de)&& monto_credito_de!=null&&!"".equals(monto_credito_a)&& monto_credito_a!=null){
			condicion.append(" and (D.fn_monto-(fn_monto*(fn_porc_descuento/100))) between ? and ?");
			conditions.add(monto_credito_de);
			conditions.add(monto_credito_a);
		}
		if("".equals(ic_estatus_docto)){
			condicion.append(" and D.ic_estatus_docto in (4,20,22,3,11,1,24,32)");
		}
		if(!"".equals(ic_estatus_docto)&&!"4".equals(ic_estatus_docto)&&!"20".equals(ic_estatus_docto)&&!"22".equals(ic_estatus_docto)&&!"3".equals(ic_estatus_docto)&&!"11".equals(ic_estatus_docto)&&!"1".equals(ic_estatus_docto)&&!"24".equals(ic_estatus_docto)&&!"32".equals(ic_estatus_docto)){
			condicion.append(" and D.ic_estatus_docto is null");
		}
		if(!"".equals(ic_if)&&ic_if!=null){
			condicion.append(" AND LC.IC_IF = ?");
			conditions.add(ic_if);
		}
		if(!"".equals(tipo_pago)){
			condicion.append(" AND D.IG_TIPO_PAGO = ?");
			conditions.add(tipo_pago);
		}

		qryDM.append("  SELECT /*+ use_nl(d,ds,e,m,pe,pn,tc,tf,ed,lc,i,pep,ct,tci)");
		qryDM.append("         index(pep cp_comrel_pyme_epo_x_prod_pk)*/");
		qryDM.append("         E.CG_RAZON_SOCIAL AS NOMBRE_EPO");
		qryDM.append("         ,D.IG_NUMERO_DOCTO");
		qryDM.append("         ,DS.CC_ACUSE");
		qryDM.append("         ,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION");
		qryDM.append("         ,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA");
		qryDM.append("         ,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC");
		qryDM.append("         ,D.IG_PLAZO_DOCTO");
		qryDM.append("         ,M.CD_NOMBRE AS MONEDA");
		qryDM.append("         ,D.FN_MONTO");
		qryDM.append("         ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION");
		qryDM.append("         ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO");
		qryDM.append("         ,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO");
		qryDM.append("         ,DECODE(D.IC_TIPO_FINANCIAMIENTO,2,0,NVL(D.FN_PORC_DESCUENTO,0)) AS FN_PORC_DESCUENTO");
		qryDM.append("         ,TF.CD_DESCRIPCION AS MODO_PLAZO");
		qryDM.append("         ,ED.CD_DESCRIPCION AS ESTATUS");
		qryDM.append("         ,M.ic_moneda");
		qryDM.append("         ,D.ic_documento");
		qryDM.append("         ,I.cg_razon_social as NOMBRE_IF");
		qryDM.append("         ,decode(PEP.cg_tipo_credito,'D','Descuento','Credito en Cuenta Corriente') as TIPO_CREDITO");
		qryDM.append("         ,TO_CHAR(DS.DF_FECHA_SELECCION,'DD/MM/YYYY') AS DF_OPERACION_CREDITO");
		qryDM.append("         ,d.ig_plazo_credito AS PLAZO_CREDITO");
		qryDM.append("         ,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'DD/MM/YYYY') AS DF_VENC_CREDITO");
		qryDM.append("         ,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) AS REF_TASA");
		qryDM.append("         ,TCI.CD_DESCRIPCION AS TIPO_COBRO_INT");
		qryDM.append("         ,DS.fn_valor_tasa");
		qryDM.append("         ,DS.FN_IMPORTE_INTERES");
		qryDM.append("         ,LC.ic_moneda as moneda_linea, D.IC_ESTATUS_DOCTO,clas.cg_descripcion as CATEGORIA ");
		qryDM.append("         ,d.CG_VENTACARTERA as CG_VENTACARTERA ");
		qryDM.append("         , (SELECT nvl(fn_valor_aforo, 0) FROM dis_aforo_epo WHERE ic_if = i.ic_if AND ic_epo = d.ic_epo AND ic_moneda = d.ic_moneda) AS POR_AFRO");
		qryDM.append("         ,'' as id_orden_enviado ");
		qryDM.append("         ,'' as id_operacion ");
		qryDM.append("         ,'' as codigo_autorizado ");
		qryDM.append("         ,'' as fecha_registro ");
		qryDM.append("         ,'' as OPERA_TARJETA ");
		qryDM.append("         ,'' as bins");
		qryDM.append("         ,'' as NUM_TC, D.IG_TIPO_PAGO ");
		qryDM.append("   FROM DIS_DOCUMENTO D");
		qryDM.append("        ,DIS_DOCTO_SELECCIONADO DS");
		qryDM.append("        ,COMCAT_EPO E");
		qryDM.append("        ,COMCAT_MONEDA M");
		qryDM.append("        ,COMREL_PRODUCTO_EPO PE");
		qryDM.append("        ,COMCAT_PRODUCTO_NAFIN PN");
		qryDM.append("        ,COM_TIPO_CAMBIO TC");
		qryDM.append("        ,COMCAT_TIPO_FINANCIAMIENTO TF");
		qryDM.append("        ,COMCAT_ESTATUS_DOCTO ED");
		qryDM.append("        ,DIS_LINEA_CREDITO_DM LC");
		qryDM.append("        ,COMCAT_IF I");
		qryDM.append("        ,COMREL_PYME_EPO_X_PRODUCTO PEP");
		qryDM.append("        ,COMCAT_TASA CT");
		qryDM.append("        ,COMCAT_TIPO_COBRO_INTERES TCI,comrel_clasificacion clas ");
		qryDM.append("  WHERE D.IC_EPO = E.IC_EPO");
		qryDM.append("    AND D.IC_MONEDA = M.IC_MONEDA");
		qryDM.append("    AND PN.IC_PRODUCTO_NAFIN = 4");
		qryDM.append("    AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN");
		qryDM.append("    AND PE.IC_EPO = D.IC_EPO");
		qryDM.append("    AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)");
		qryDM.append("    AND M.ic_moneda = TC.ic_moneda");
		qryDM.append("    AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO");
		qryDM.append("    AND ED.IC_ESTATUS_DOCTO = D.IC_ESTATUS_DOCTO");
		qryDM.append("    AND D.IC_LINEA_CREDITO_DM = LC.IC_LINEA_CREDITO_DM (+)");
		qryDM.append("    AND LC.IC_IF = I.IC_IF(+)");
		qryDM.append("    AND PEP.IC_EPO = E.IC_EPO");
		qryDM.append("    AND PEP.IC_EPO = D.IC_EPO");
		qryDM.append("    AND PEP.IC_PYME = D.IC_PYME");
		//"  AND PEP.CS_HABILITADO = 'S'");
		qryDM.append("    AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO (+)");
		qryDM.append("    AND DS.IC_TASA = CT.IC_TASA(+)");
		qryDM.append("    AND LC.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES(+)");
		qryDM.append("    AND D.ic_clasificacion = clas.ic_clasificacion (+) ");
		qryDM.append("    AND PEP.IC_PRODUCTO_NAFIN = 4");
		qryDM.append("    AND E.CS_HABILITADO = 'S' ");
		qryDM.append("    AND D.IC_pyme = "+ic_pyme);
		qryDM.append(condicion.toString());

		qryCCC.append("SELECT /*+ use_nl(D,DS,E,M,PE,PN,TC,TF,ED,LC,I,PEP,CT,TCI)");
		qryCCC.append("       index(pep cp_comrel_pyme_epo_x_prod_pk)*/");
		qryCCC.append("       E.CG_RAZON_SOCIAL AS NOMBRE_EPO");
		qryCCC.append("       ,D.IG_NUMERO_DOCTO");
		qryCCC.append("       ,D.CC_ACUSE");
		qryCCC.append("       ,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION");
		qryCCC.append("       ,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA");
		qryCCC.append("       ,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC");
		qryCCC.append("       ,D.IG_PLAZO_DOCTO");
		qryCCC.append("       ,M.CD_NOMBRE AS MONEDA");
		qryCCC.append("       ,D.FN_MONTO");
		qryCCC.append("       ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION");
		qryCCC.append("       ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO");
		qryCCC.append("       ,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO");
		qryCCC.append("       ,DECODE(D.IC_TIPO_FINANCIAMIENTO,2,0,NVL(D.FN_PORC_DESCUENTO,0)) AS FN_PORC_DESCUENTO");
		qryCCC.append("       ,TF.CD_DESCRIPCION AS MODO_PLAZO");
		qryCCC.append("       ,ED.CD_DESCRIPCION AS ESTATUS");
		qryCCC.append("       ,M.ic_moneda");
		qryCCC.append("       ,D.ic_documento");
		//" ,I.cg_razon_social as NOMBRE_IF");
		qryCCC.append("       ,DECODE (ed.ic_estatus_docto,32, i2.cg_razon_social, i.cg_razon_social) AS nombre_if");
		qryCCC.append("       ,decode(PEP.cg_tipo_credito,'D','Descuento','Credito en Cuenta Corriente') as TIPO_CREDITO");
		qryCCC.append("       ,TO_CHAR(DS.DF_FECHA_SELECCION,'DD/MM/YYYY') AS DF_OPERACION_CREDITO");
		qryCCC.append("       ,d.ig_plazo_credito AS PLAZO_CREDITO");
		qryCCC.append("       ,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'DD/MM/YYYY') AS DF_VENC_CREDITO");
		qryCCC.append("       ,CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS AS REF_TASA");
		qryCCC.append("       ,TCI.CD_DESCRIPCION AS TIPO_COBRO_INT");
		qryCCC.append("       ,DS.fn_valor_tasa");
		qryCCC.append("       ,DS.FN_IMPORTE_INTERES");
		qryCCC.append("       ,LC.IC_MONEDA as moneda_linea, D.IC_ESTATUS_DOCTO,clas.cg_descripcion as CATEGORIA ");
		qryCCC.append("       ,d.CG_VENTACARTERA as CG_VENTACARTERA ");
		qryCCC.append("       ,(SELECT nvl(fn_valor_aforo, 0) FROM dis_aforo_epo WHERE ic_if = i.ic_if AND ic_epo = d.ic_epo AND ic_moneda = d.ic_moneda) AS POR_AFRO");
		qryCCC.append("       ,d.IC_ORDEN_PAGO_TC as id_orden_enviado");
		qryCCC.append("       ,ptc.CG_TRANSACCION_ID as id_operacion");
		//",ptc.CG_CODIGO_AUTORIZACION as codigo_autorizacion");
		qryCCC.append("       ,CONCAT (ptc.cg_codigo_resp, ptc.cg_codigo_desc) AS codigo_autorizado");
		qryCCC.append("       ,DF_FECHA_AUTORIZACION as fecha_registro");
		qryCCC.append("       ,trim(cpi.cg_opera_tarjeta) as OPERA_TARJETA ");
		qryCCC.append("       ,bins.CG_CODIGO_BIN||' '||bins.CG_DESCRIPCION as bins ");
		qryCCC.append("       ,SUBSTR(ptc.CG_NUM_TARJETA,-4,4) as NUM_TC, D.IG_TIPO_PAGO ");
		qryCCC.append("  FROM DIS_DOCUMENTO D");
		qryCCC.append("       ,DIS_DOCTO_SELECCIONADO DS");
		qryCCC.append("       ,COMCAT_EPO E");
		qryCCC.append("       ,COMCAT_MONEDA M");
		qryCCC.append("       ,COMREL_PRODUCTO_EPO PE");
		qryCCC.append("       ,COMCAT_PRODUCTO_NAFIN PN");
		qryCCC.append("       ,COM_TIPO_CAMBIO TC");
		qryCCC.append("       ,COMCAT_TIPO_FINANCIAMIENTO TF");
		qryCCC.append("       ,COMCAT_ESTATUS_DOCTO ED");
		qryCCC.append("       ,COM_LINEA_CREDITO LC");
		qryCCC.append("       ,COMCAT_IF I");
		qryCCC.append("       ,COMCAT_IF I2");
		qryCCC.append("       ,COMREL_PYME_EPO_X_PRODUCTO PEP");
		qryCCC.append("       ,COMCAT_TASA CT");
		qryCCC.append("       ,COMCAT_TIPO_COBRO_INTERES TCI,comrel_clasificacion clas ");
		qryCCC.append("       ,comrel_producto_if cpi ");
		qryCCC.append("       ,DIS_DOCTOS_PAGO_TC ptc ");
		qryCCC.append("       ,com_bins_if bins ");
		qryCCC.append(" WHERE D.IC_EPO = E.IC_EPO");
		qryCCC.append("   AND D.IC_MONEDA = M.IC_MONEDA");
		qryCCC.append("   AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN");
		qryCCC.append("   AND PE.IC_EPO = D.IC_EPO");
		qryCCC.append("   AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)");
		qryCCC.append("   AND M.ic_moneda = TC.ic_moneda");
		qryCCC.append("   AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO");
		qryCCC.append("   AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO");
		qryCCC.append("   AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO");
		qryCCC.append("   AND LC.IC_IF = I.IC_IF(+)");
		qryCCC.append("   AND d.ic_bins = bins.ic_bins(+)");
		qryCCC.append("   AND bins.ic_if = i2.ic_if(+)");
		qryCCC.append("   and lc.IC_IF = cpi.IC_IF");
		qryCCC.append("   and pep.IC_PRODUCTO_NAFIN = cpi.IC_PRODUCTO_NAFIN ");
		qryCCC.append("   and d.IC_ORDEN_PAGO_TC  =ptc.IC_ORDEN_PAGO_TC (+) ");
		qryCCC.append("   AND PEP.IC_EPO = E.IC_EPO");
		qryCCC.append("   AND PEP.IC_EPO = D.IC_EPO");
		qryCCC.append("   AND PEP.IC_PYME = D.IC_PYME");
		qryCCC.append("   AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO (+)");
		qryCCC.append("   AND DS.IC_TASA = CT.IC_TASA(+)");
		qryCCC.append("   AND LC.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES(+)");
		qryCCC.append("   AND D.ic_clasificacion = clas.ic_clasificacion (+) ");
		//"  AND PEP.CS_HABILITADO = 'S'");
		qryCCC.append("   AND PEP.IC_PRODUCTO_NAFIN = 4");
		qryCCC.append("   AND PN.IC_PRODUCTO_NAFIN = 4");
		qryCCC.append("   AND E.CS_HABILITADO = 'S' ");
		qryCCC.append("   AND D.IC_pyme = "+ic_pyme);
		qryCCC.append(condicion.toString());

		qryFdR.append("  SELECT /*+ use_nl(D,DS,E,M,PE,PN,TC,TF,ED,LC,I,PEP,CT,TCI)");
		qryFdR.append("         index(pep cp_comrel_pyme_epo_x_prod_pk)*/");
		qryFdR.append("         E.CG_RAZON_SOCIAL AS NOMBRE_EPO");
		qryFdR.append("         ,D.IG_NUMERO_DOCTO");
		qryFdR.append("         ,D.CC_ACUSE");
		qryFdR.append("         ,TO_CHAR(D.DF_FECHA_EMISION,'DD/MM/YYYY') AS DF_FECHA_EMISION");
		qryFdR.append("         ,TO_CHAR(D.DF_CARGA,'DD/MM/YYYY') AS DF_CARGA");
		qryFdR.append("         ,TO_CHAR(D.DF_FECHA_VENC,'DD/MM/YYYY') AS DF_FECHA_VENC");
		qryFdR.append("         ,D.IG_PLAZO_DOCTO");
		qryFdR.append("         ,M.CD_NOMBRE AS MONEDA");
		qryFdR.append("         ,D.FN_MONTO");
		qryFdR.append("         ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'N','','P','Dolar-Peso','') as TIPO_CONVERSION");
		qryFdR.append("         ,DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',decode(M.ic_moneda,54,TC.FN_VALOR_COMPRA,'1'),'1') as TIPO_CAMBIO");
		qryFdR.append("         ,NVL(D.IG_PLAZO_DESCUENTO,'') as IG_PLAZO_DESCUENTO");
		qryFdR.append("         ,DECODE(D.IC_TIPO_FINANCIAMIENTO,2,0,NVL(D.FN_PORC_DESCUENTO,0)) AS FN_PORC_DESCUENTO");
		qryFdR.append("         ,'NA' AS MODO_PLAZO");
		qryFdR.append("         ,ED.CD_DESCRIPCION AS ESTATUS");
		qryFdR.append("         ,M.ic_moneda");
		qryFdR.append("         ,D.ic_documento");
		//" ,I.cg_razon_social as NOMBRE_IF");
		qryFdR.append("         ,DECODE (ed.ic_estatus_docto,32, i2.cg_razon_social, i.cg_razon_social) AS nombre_if");
		qryFdR.append("         ,decode(PEP.cg_tipo_credito,'F','Factoraje con Recurso') as TIPO_CREDITO");
		qryFdR.append("         ,TO_CHAR(DS.DF_FECHA_SELECCION,'DD/MM/YYYY') AS DF_OPERACION_CREDITO");
		qryFdR.append("         ,d.ig_plazo_credito AS PLAZO_CREDITO");
		qryFdR.append("         ,TO_CHAR(D.DF_FECHA_VENC_CREDITO,'DD/MM/YYYY') AS DF_VENC_CREDITO");
		qryFdR.append("         ,CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS AS REF_TASA");
		qryFdR.append("         ,TCI.CD_DESCRIPCION AS TIPO_COBRO_INT");
		qryFdR.append("         ,DS.fn_valor_tasa");
		qryFdR.append("         ,DS.FN_IMPORTE_INTERES");
		qryFdR.append("         ,LC.IC_MONEDA as moneda_linea, D.IC_ESTATUS_DOCTO,clas.cg_descripcion as CATEGORIA ");
		qryFdR.append("         ,d.CG_VENTACARTERA as CG_VENTACARTERA ");
		qryFdR.append("         ,(SELECT nvl(fn_valor_aforo, 0) FROM dis_aforo_epo WHERE ic_if = i.ic_if AND ic_epo = d.ic_epo AND ic_moneda = d.ic_moneda) AS POR_AFRO");
		qryFdR.append("         ,d.IC_ORDEN_PAGO_TC as id_orden_enviado");
		qryFdR.append("         ,ptc.CG_TRANSACCION_ID as id_operacion");
		//",ptc.CG_CODIGO_AUTORIZACION as codigo_autorizacion");
		qryFdR.append("         ,CONCAT (ptc.cg_codigo_resp, ptc.cg_codigo_desc) AS codigo_autorizado");
		qryFdR.append("         ,DF_FECHA_AUTORIZACION as fecha_registro");
		qryFdR.append("         ,trim(cpi.cg_opera_tarjeta) as OPERA_TARJETA ");
		qryFdR.append("         ,bins.CG_CODIGO_BIN||' '||bins.CG_DESCRIPCION as bins ");
		qryFdR.append("         ,SUBSTR(ptc.CG_NUM_TARJETA,-4,4) as NUM_TC, D.IG_TIPO_PAGO ");
		qryFdR.append("    FROM DIS_DOCUMENTO D");
		qryFdR.append("         ,DIS_DOCTO_SELECCIONADO DS");
		qryFdR.append("         ,COMCAT_EPO E");
		qryFdR.append("         ,COMCAT_MONEDA M");
		qryFdR.append("         ,COMREL_PRODUCTO_EPO PE");
		qryFdR.append("         ,COMCAT_PRODUCTO_NAFIN PN");
		qryFdR.append("         ,COM_TIPO_CAMBIO TC");
		qryFdR.append("         ,COMCAT_TIPO_FINANCIAMIENTO TF");
		qryFdR.append("         ,COMCAT_ESTATUS_DOCTO ED");
		qryFdR.append("         ,COM_LINEA_CREDITO LC");
		qryFdR.append("         ,COMCAT_IF I");
		qryFdR.append("         ,COMCAT_IF I2");
		qryFdR.append("         ,COMREL_PYME_EPO_X_PRODUCTO PEP");
		qryFdR.append("         ,COMCAT_TASA CT");
		qryFdR.append("         ,COMCAT_TIPO_COBRO_INTERES TCI,comrel_clasificacion clas ");
		qryFdR.append("         ,comrel_producto_if cpi ");
		qryFdR.append("         ,DIS_DOCTOS_PAGO_TC ptc ");
		qryFdR.append("         ,com_bins_if bins ");
		qryFdR.append("   WHERE D.IC_EPO = E.IC_EPO");
		qryFdR.append("     AND D.IC_MONEDA = M.IC_MONEDA");
		qryFdR.append("     AND PN.IC_PRODUCTO_NAFIN = PE.IC_PRODUCTO_NAFIN");
		qryFdR.append("     AND PE.IC_EPO = D.IC_EPO");
		qryFdR.append("     AND TC.DC_FECHA IN(SELECT MAX(DC_FECHA) FROM COM_TIPO_CAMBIO WHERE IC_MONEDA = M.IC_MONEDA)");
		qryFdR.append("     AND M.ic_moneda = TC.ic_moneda");
		qryFdR.append("     AND D.IC_TIPO_FINANCIAMIENTO = TF.IC_TIPO_FINANCIAMIENTO(+)");
		qryFdR.append("     AND D.IC_ESTATUS_DOCTO = ED.IC_ESTATUS_DOCTO");
		qryFdR.append("     AND D.IC_LINEA_CREDITO = LC.IC_LINEA_CREDITO");
		qryFdR.append("     AND LC.IC_IF = I.IC_IF(+)");
		qryFdR.append("     AND d.ic_bins = bins.ic_bins(+)");
		qryFdR.append("     AND bins.ic_if = i2.ic_if(+)");
		qryFdR.append("     and lc.IC_IF = cpi.IC_IF");
		qryFdR.append("     and pep.IC_PRODUCTO_NAFIN = cpi.IC_PRODUCTO_NAFIN ");
		qryFdR.append("     and d.IC_ORDEN_PAGO_TC  =ptc.IC_ORDEN_PAGO_TC (+) ");
		qryFdR.append("     AND PEP.IC_EPO = E.IC_EPO");
		qryFdR.append("     AND PEP.IC_EPO = D.IC_EPO");
		qryFdR.append("     AND PEP.IC_PYME = D.IC_PYME");
		qryFdR.append("     AND D.IC_DOCUMENTO = DS.IC_DOCUMENTO (+)");
		qryFdR.append("     AND DS.IC_TASA = CT.IC_TASA(+)");
		qryFdR.append("     AND LC.IC_TIPO_COBRO_INTERES = TCI.IC_TIPO_COBRO_INTERES(+)");
		qryFdR.append("     AND D.ic_clasificacion = clas.ic_clasificacion (+) ");
		qryFdR.append("     AND PEP.IC_PRODUCTO_NAFIN = 4");
		qryFdR.append("     AND PN.IC_PRODUCTO_NAFIN = 4");
		qryFdR.append("     AND E.CS_HABILITADO = 'S' ");
		qryFdR.append("     and d.IC_TIPO_FINANCIAMIENTO is null ");
		qryFdR.append("     AND D.IC_pyme = "+ic_pyme);
		qryFdR.append(condicion.toString());

		if("D".equals(tipo_credito))
			qrySentencia = qryDM;
		else if("C".equals(tipo_credito))
			qrySentencia = qryCCC;
		else if("".equals(tipo_credito)  && tipoCreditoXepo.equals("F"))
			qrySentencia = qryFdR;
		else
			qrySentencia.append(qryDM.toString() + " UNION ALL "+ qryCCC.toString());

		log.info("qrySentencia: " + qrySentencia.toString());
		log.info("conditions: " + conditions.toString());
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();
	}

	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo) {

		log.info("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();

		String epo                = "";
		String numDocto           = "";
		String numAcuseCarga      = "";
		String fechaEmision       = "";
		String fechaPublicacion   = "";
		String fechaVencimiento   = "";
		String plazoDocto         = "";
		String moneda             = "";
		String icMoneda           = "";
		String tipoConversion     = "";
		String categoria          = "";
		String plazoDescuento     = "";
		String porcDescuento      = "";
		String modoPlazo          = "";
		String estatus            = "";
		String icDocumento        = "";
		String intermediario      = "";
		String tipoCredito        = "";
		String fechaOperacion     = "";
		String plazoCredito       = "";
		String fechaVencCredito   = "";
		String referenciaTasaInt  = "";
		String valorTasaInt       = "";
		String montoTasaInt       = "";
		String tipoCobroInt       = "";
		String monedaLinea        = "";
		String bandeVentaCartera  = "";
		String porc_Aforo         = "";
		String idOrdenEnviado     = "";
		String idOperacion        = "";
		String codigoAutorizacion = "";
		String fechaRegistro      = "";
		String bins               = "";
		String numTarjeta         = "";
		String operaTarjeta       = "";
		String tipoPago           = "";
		int nRow                  = 0;
		double monto              = 0;
		double tipoCambio         = 0;
		double montoValuado       = 0;
		double montoDescontar     = 0;
		double montoCredito       = 0;

		if(tipo.equals("PDF")){
			try {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText(" ","formas",ComunesPDF.RIGHT);

				while (rs.next()){
					if(nRow == 0){
						int numCols = 21;
						if(!"".equals(tipoConversion)){
							numCols = 21;
						}else{
							numCols = 18;
						}
						if(publicaDoctosFinanciables.equals("S")){
							numCols++;
						}
						pdfDoc.setLTable(numCols,100);
						pdfDoc.setLCell("Datos del Documento Inicial","celda01",ComunesPDF.CENTER,numCols);
						pdfDoc.setLCell("A","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("EPO","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("No. Docto Inicial","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("No. Acuse Carga","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha Emisi�n","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha Pub.","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha Venc.","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Plazo Docto","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Categor�a","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("% de Descuento Aforo","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto a Descontar","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Plazo Descuento D�as","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("% Descuento","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto % Descuento","celda01",ComunesPDF.CENTER);
						if(!"".equals(tipoConversion)){
							pdfDoc.setLCell("Tipo Conv.","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Tipo Cambio","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Monto Valuado","celda01",ComunesPDF.CENTER);
						}
						pdfDoc.setLCell("Modalidad de Plazo","celda01",ComunesPDF.CENTER);
						if(publicaDoctosFinanciables.equals("S")){
							pdfDoc.setLCell("Tipo de pago","celda01",ComunesPDF.CENTER);
						}
						pdfDoc.setLCell("Estatus","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Datos del Documento Final","celda01",ComunesPDF.CENTER,numCols);
						pdfDoc.setLCell("B","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Tipo Cred.","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("No. Docto Final","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Plazo","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha Venc.","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha Oper","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("IF","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Nombre del Producto","celda01",ComunesPDF.CENTER);//FODEA-013-2014
						pdfDoc.setLCell("Ref. Tasa Inter�s","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Valor Tasa Inter�s","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto Inter�s","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("ID Orden enviado","celda01",ComunesPDF.CENTER);//FODEA-013-2014
						pdfDoc.setLCell("Respuesta de Operaci�n","celda01",ComunesPDF.CENTER);//FODEA-013-2014
						pdfDoc.setLCell("C�dido Autorizaci�n","celda01",ComunesPDF.CENTER);//FODEA-013-2014
						pdfDoc.setLCell("N�mero Tarjeta de Cr�dito","celda01",ComunesPDF.CENTER);//FODEA-013-2014
						if(!"".equals(tipoConversion)){
							pdfDoc.setLCell(" ","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell(" ","celda01",ComunesPDF.CENTER);
						}
						pdfDoc.setLCell(" ","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","celda01",ComunesPDF.CENTER);
						if(publicaDoctosFinanciables.equals("S")){
							pdfDoc.setLCell(" ","celda01",ComunesPDF.CENTER);
						}
						pdfDoc.setLHeaders();
						nRow++;
					}
					epo               = (rs.getString("NOMBRE_EPO")==null)?"":rs.getString("NOMBRE_EPO");
					numDocto          = (rs.getString("IG_NUMERO_DOCTO")==null)?"":rs.getString("IG_NUMERO_DOCTO");
					numAcuseCarga     = (rs.getString("CC_ACUSE")==null)?"":rs.getString("CC_ACUSE");
					fechaEmision      = (rs.getString("DF_FECHA_EMISION")==null)?"":rs.getString("DF_FECHA_EMISION");
					fechaPublicacion  = (rs.getString("DF_CARGA")==null)?"":rs.getString("DF_CARGA");
					fechaVencimiento  = (rs.getString("DF_FECHA_VENC")==null)?"":rs.getString("DF_FECHA_VENC");
					plazoDocto        = (rs.getString("IG_PLAZO_DOCTO")==null)?"":rs.getString("IG_PLAZO_DOCTO");
					moneda            = (rs.getString("MONEDA")==null)?"":rs.getString("MONEDA");
					monto             = Double.parseDouble(rs.getString("FN_MONTO"));
					tipoConversion    = (rs.getString("TIPO_CONVERSION")==null)?"":rs.getString("TIPO_CONVERSION");
					tipoCambio        = Double.parseDouble(rs.getString("TIPO_CAMBIO"));
					plazoDescuento    = (rs.getString("IG_PLAZO_DESCUENTO")==null)?"":rs.getString("IG_PLAZO_DESCUENTO");
					porcDescuento     = (rs.getString("FN_PORC_DESCUENTO")==null)?"0":rs.getString("FN_PORC_DESCUENTO");
					montoDescontar    = monto*Double.parseDouble(porcDescuento)/100;
					montoValuado      = (monto-montoDescontar)*tipoCambio;
					modoPlazo         = (rs.getString("MODO_PLAZO")==null)?"":rs.getString("MODO_PLAZO");
					estatus           = (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");
					icMoneda          = (rs.getString("IC_MONEDA")==null)?"":rs.getString("IC_MONEDA");
					icDocumento       = (rs.getString("IC_DOCUMENTO")==null)?"":rs.getString("IC_DOCUMENTO");
					intermediario     = (rs.getString("NOMBRE_IF")==null)?"":rs.getString("NOMBRE_IF");
					tipoCredito       = (rs.getString("TIPO_CREDITO")==null)?"":rs.getString("TIPO_CREDITO");
					fechaOperacion    = (rs.getString("DF_OPERACION_CREDITO")==null)?"":rs.getString("DF_OPERACION_CREDITO");
					montoCredito      = monto-montoDescontar;
					plazoCredito      = (rs.getString("PLAZO_CREDITO")==null)?"":rs.getString("PLAZO_CREDITO");
					fechaVencCredito  = (rs.getString("DF_VENC_CREDITO")==null)?"":rs.getString("DF_VENC_CREDITO");
					referenciaTasaInt = (rs.getString("REF_TASA")==null)?"":rs.getString("REF_TASA");
					valorTasaInt      = ( (rs.getString("FN_VALOR_TASA")==null)?"0":rs.getString("FN_VALOR_TASA") );
					montoTasaInt      = ( (rs.getString("FN_IMPORTE_INTERES")==null)?"0":rs.getString("FN_IMPORTE_INTERES") );
					tipoCobroInt      = (rs.getString("TIPO_COBRO_INT")==null)?"":rs.getString("TIPO_COBRO_INT"); 
					monedaLinea       = (rs.getString("MONEDA_LINEA")==null)?"":rs.getString("MONEDA_LINEA");
					categoria         = (rs.getString("CATEGORIA")==null)?"":rs.getString("CATEGORIA");
					bandeVentaCartera = (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");
					porc_Aforo  = (rs.getString("POR_AFRO")==null)?"0":rs.getString("POR_AFRO"); //F05-2014
					BigDecimal  por_Aforo= new BigDecimal(porc_Aforo);  //F05-2014
					BigDecimal  montoD= new BigDecimal(monto);  //F05-2014
					BigDecimal   MontoDesconta = montoD.multiply(por_Aforo.divide(new BigDecimal("100"),10,BigDecimal.ROUND_HALF_UP)); //F05-2014

					if (bandeVentaCartera.equals("S") ) {
						modoPlazo ="";
					}
					operaTarjeta   = (rs.getString("OPERA_TARJETA")==null)?"":rs.getString("OPERA_TARJETA");
					idOrdenEnviado = (rs.getString("ID_ORDEN_ENVIADO")==null)?"":rs.getString("ID_ORDEN_ENVIADO");
					idOperacion = (rs.getString("ID_OPERACION")==null)?"":rs.getString("ID_OPERACION");
					codigoAutorizacion   = (rs.getString("CODIGO_AUTORIZADO")==null)?"":rs.getString("CODIGO_AUTORIZADO");
					//fechaRegistro   = (rs.getString("FECHA_REGISTRO")==null)?"":rs.getString("FECHA_REGISTRO");
					operaTarjeta   = (rs.getString("OPERA_TARJETA")==null)?"":rs.getString("OPERA_TARJETA");
					bins  = (rs.getString("BINS")==null)?"":rs.getString("BINS");
					numTarjeta  = (rs.getString("NUM_TC")==null)?"":rs.getString("NUM_TC");
					tipoPago = (rs.getString("IG_TIPO_PAGO")==null)?"":rs.getString("IG_TIPO_PAGO");
					if(tipoPago.equals("1")){
						tipoPago = "Financiamiento con intereses";
					} else if(tipo_pago.equals("2")){
						tipoPago = "Meses sin intereses";
					}
					if(!"Operada TC".equals(estatus)){//FODE-013-2014
						idOrdenEnviado = "N/A";
						idOperacion = "N/A";
						codigoAutorizacion = "N/A";
						operaTarjeta = "N/A";
						bins = "N/A";
						numTarjeta = "N/A";
					}else{//FODE-013-2014
						fechaVencimiento  = "N/A";
						plazoDocto        = "N/A";
						referenciaTasaInt = "N/A";
						valorTasaInt      = "N/A";
						montoTasaInt      = "N/A";
						plazoCredito      = "N/A";
						fechaVencCredito  = "N/A";
					}
					pdfDoc.setLCell("A","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell(epo.replace(',',' '),"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(numDocto,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(numAcuseCarga,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaEmision,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaPublicacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(plazoDocto,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(moneda,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(categoria,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(porc_Aforo+'%',"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(MontoDesconta,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(plazoDescuento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(porcDescuento+'%',"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoDescontar,2),"formas",ComunesPDF.RIGHT);

					if(!"".equals(tipoConversion)){
						pdfDoc.setLCell(((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?tipoConversion:""),"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell("$"+((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?Comunes.formatoDecimal(tipoCambio,2,false):""),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?Comunes.formatoDecimal(montoValuado,2,false):""),"formas",ComunesPDF.RIGHT);
					}
					pdfDoc.setLCell(modoPlazo,"formas",ComunesPDF.LEFT);
					if(publicaDoctosFinanciables.equals("S")){
						pdfDoc.setLCell(tipoPago,"formas",ComunesPDF.LEFT);
					}
					pdfDoc.setLCell(estatus,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell("B","celda01",ComunesPDF.CENTER);
					if(!estatus.equals("Negociable")){
						pdfDoc.setLCell(tipoCredito,"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell(icDocumento,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoCredito,2),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell(plazoCredito,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(fechaVencCredito,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(fechaOperacion,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(intermediario,"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell(bins,"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell(referenciaTasaInt.replace(',', '.'),"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell((!"N/A".equals(valorTasaInt))?Comunes.formatoDecimal(valorTasaInt,2)+"%":valorTasaInt,"formas",ComunesPDF.CENTER);
						pdfDoc.setLCell((!"N/A".equals(montoTasaInt))?"$"+Comunes.formatoDecimal(montoTasaInt,2):montoTasaInt,"formas",ComunesPDF.RIGHT);
					}else{
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
					}
					pdfDoc.setLCell(idOrdenEnviado,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(idOperacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(codigoAutorizacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell((!"N/A".equals(numTarjeta.trim()) && !"".equals(numTarjeta.trim()))?"XXXX-XXXX-XXXX-"+numTarjeta:numTarjeta,"formas",ComunesPDF.CENTER);
					if(!"".equals(tipoConversion)){
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
					}
					pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
					if(publicaDoctosFinanciables.equals("S")){
						pdfDoc.setLCell(tipo_pago,"formas",ComunesPDF.LEFT);
					}
				}

				pdfDoc.addLTable();
				pdfDoc.endDocument();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo PDF. ", e);
			}
		}
		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}

	public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo) {
		return null;
	}
	/*******************************************************************************
	*                       GETTERS AND SETTERS                                   *
	*******************************************************************************/
	public String getIc_pyme() {
		return ic_pyme;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}

	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getIc_estatus_docto() {
		return ic_estatus_docto;
	}

	public void setIc_estatus_docto(String ic_estatus_docto) {
		this.ic_estatus_docto = ic_estatus_docto;
	}

	public String getIg_numero_docto() {
		return ig_numero_docto;
	}

	public void setIg_numero_docto(String ig_numero_docto) {
		this.ig_numero_docto = ig_numero_docto;
	}

	public String getFecha_seleccion_de() {
		return fecha_seleccion_de;
	}

	public void setFecha_seleccion_de(String fecha_seleccion_de) {
		this.fecha_seleccion_de = fecha_seleccion_de;
	}

	public String getFecha_seleccion_a() {
		return fecha_seleccion_a;
	}

	public void setFecha_seleccion_a(String fecha_seleccion_a) {
		this.fecha_seleccion_a = fecha_seleccion_a;
	}

	public String getCc_acuse() {
		return cc_acuse;
	}

	public void setCc_acuse(String cc_acuse) {
		this.cc_acuse = cc_acuse;
	}

	public String getMonto_credito_de() {
		return monto_credito_de;
	}

	public void setMonto_credito_de(String monto_credito_de) {
		this.monto_credito_de = monto_credito_de;
	}

	public String getMonto_credito_a() {
		return monto_credito_a;
	}

	public void setMonto_credito_a(String monto_credito_a) {
		this.monto_credito_a = monto_credito_a;
	}

	public String getFecha_emision_de() {
		return fecha_emision_de;
	}

	public void setFecha_emision_de(String fecha_emision_de) {
		this.fecha_emision_de = fecha_emision_de;
	}

	public String getFecha_emision_a() {
		return fecha_emision_a;
	}

	public void setFecha_emision_a(String fecha_emision_a) {
		this.fecha_emision_a = fecha_emision_a;
	}

	public String getFecha_vto_de() {
		return fecha_vto_de;
	}

	public void setFecha_vto_de(String fecha_vto_de) {
		this.fecha_vto_de = fecha_vto_de;
	}

	public String getFecha_vto_a() {
		return fecha_vto_a;
	}

	public void setFecha_vto_a(String fecha_vto_a) {
		this.fecha_vto_a = fecha_vto_a;
	}

	public String getFecha_vto_credito_de() {
		return fecha_vto_credito_de;
	}

	public void setFecha_vto_credito_de(String fecha_vto_credito_de) {
		this.fecha_vto_credito_de = fecha_vto_credito_de;
	}

	public String getFecha_vto_credito_a() {
		return fecha_vto_credito_a;
	}

	public void setFecha_vto_credito_a(String fecha_vto_credito_a) {
		this.fecha_vto_credito_a = fecha_vto_credito_a;
	}

	public String getIc_tipo_cobro_int() {
		return ic_tipo_cobro_int;
	}

	public void setIc_tipo_cobro_int(String ic_tipo_cobro_int) {
		this.ic_tipo_cobro_int = ic_tipo_cobro_int;
	}

	public String getC_moneda() {
		return c_moneda;
	}

	public void setC_moneda(String c_moneda) {
		this.c_moneda = c_moneda;
	}

	public String getFn_monto_de() {
		return fn_monto_de;
	}

	public void setFn_monto_de(String fn_monto_de) {
		this.fn_monto_de = fn_monto_de;
	}

	public String getFn_monto_a() {
		return fn_monto_a;
	}

	public void setFn_monto_a(String fn_monto_a) {
		this.fn_monto_a = fn_monto_a;
	}

	public String getMonto_con_descuento() {
		return monto_con_descuento;
	}

	public void setMonto_con_descuento(String monto_con_descuento) {
		this.monto_con_descuento = monto_con_descuento;
	}

	public String getSolo_cambio() {
		return solo_cambio;
	}

	public void setSolo_cambio(String solo_cambio) {
		this.solo_cambio = solo_cambio;
	}

	public String getModo_plazo() {
		return modo_plazo;
	}

	public void setModo_plazo(String modo_plazo) {
		this.modo_plazo = modo_plazo;
	}

	public String getTipo_credito() {
		return tipo_credito;
	}

	public void setTipo_credito(String tipo_credito) {
		this.tipo_credito = tipo_credito;
	}

	public String getCgTipoConversion() {
		return cgTipoConversion;
	}

	public void setCgTipoConversion(String cgTipoConversion) {
		this.cgTipoConversion = cgTipoConversion;
	}

	public String getIc_documento() {
		return ic_documento;
	}

	public void setIc_documento(String ic_documento) {
		this.ic_documento = ic_documento;
	}

	public String getFecha_publicacion_de() {
		return fecha_publicacion_de;
	}

	public void setFecha_publicacion_de(String fecha_publicacion_de) {
		this.fecha_publicacion_de = fecha_publicacion_de;
	}

	public String getFecha_publicacion_a() {
		return fecha_publicacion_a;
	}

	public void setFecha_publicacion_a(String fecha_publicacion_a) {
		this.fecha_publicacion_a = fecha_publicacion_a;
	}

	public String getTipoCreditoXepo() {
		return tipoCreditoXepo;
	}

	public void setTipoCreditoXepo(String tipoCreditoXepo) {
		this.tipoCreditoXepo = tipoCreditoXepo;
	}

	public List getConditions(){
		return conditions; 
	}

	public String getIc_moneda() {
		return ic_moneda;
	}

	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}

	public String getTipoConversion() {
		return tipoConversion;
	}

	public void setTipoConversion(String tipoConversion) {
		this.tipoConversion = tipoConversion;
	}

	public String getTipo_pago() {
		return tipo_pago;
	}

	public void setTipo_pago(String tipo_pago) {
		this.tipo_pago = tipo_pago;
	}

	public String getPublicaDoctosFinanciables() {
		return publicaDoctosFinanciables;
	}

	public void setPublicaDoctosFinanciables(String publicaDoctosFinanciables) {
		this.publicaDoctosFinanciables = publicaDoctosFinanciables;
	}
}