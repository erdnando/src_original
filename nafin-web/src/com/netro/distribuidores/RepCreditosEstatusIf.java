package com.netro.distribuidores;

import java.util.ArrayList;
import java.util.List;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Registros;

public class RepCreditosEstatusIf{

	private StringBuffer qrysentencia;
	private String ic_estatus_docto="";
	private String tipoSolic = "";
	private String ic_if = "";
	
	private Registros registros;
	private List conditions;
	
	public void setClaveEstatus(String ic_estatus_docto){
		this.ic_estatus_docto=ic_estatus_docto;
	}

	public void setTipoSolic(String tipoSolic) {
		this.tipoSolic = tipoSolic;
	}

	public void setClaveIf(String ic_if) {
		this.ic_if = ic_if;
	}

	public Registros executeQuery() {
		AccesoDB con = new AccesoDB();
		Registros registros = new Registros();
		try{
			con.conexionDB();			
			registros = con.consultarDB(this.getQrysentencia(),conditions);
			return registros;
		} catch(Exception e) {
			throw new AppException("RepCreditosEstatusIf::ExecuteQuery(Exception) ", e);
		} finally {
			if (con.hayConexionAbierta()) {
				con.terminaTransaccion(true); //Para consultas que hagan uso de tablas remotas via dblink
				con.cierraConexionDB();
			}
		}
	}

	public void setQrysentencia(){
		StringBuffer campos, condiciones, tablas;
		conditions = new ArrayList();

		campos = new StringBuffer();
		campos.append("SELECT py.cg_razon_social AS nombre_dist,	d.ig_numero_docto,	d.cc_acuse"+
						"	,TO_CHAR(d.df_fecha_emision,'dd/mm/yyyy') AS df_fecha_emision"+
						"	,TO_CHAR(d.df_carga,'dd/mm/yyyy') AS df_carga"   +
						" 	,TO_CHAR(d.df_fecha_venc,'dd/mm/yyyy') AS df_fecha_venc"   +
						" 	,d.ig_plazo_docto"   +
						" 	,m.cd_nombre AS moneda"   +
						" 	,d.fn_monto"   +
						" 	,DECODE(NVL(pe.cg_tipo_conversion,pn.cg_tipo_conversion),'N','Sin conversion','P','Dolar-Peso','') AS tipo_conversion");

		if((("3".equals(ic_estatus_docto)||"24".equals(ic_estatus_docto))&&"D".equals(tipoSolic))){
			campos.append(" 	,DECODE(NVL(pe.cg_tipo_conversion,pn.cg_tipo_conversion),'P',DECODE(m.ic_moneda,54,d.fn_tipo_cambio,'1'),'1') AS tipo_cambio");
		}else{
			campos.append(" 	,DECODE(NVL(pe.cg_tipo_conversion,pn.cg_tipo_conversion),'P',DECODE(m.ic_moneda,54,tc.fn_valor_compra,'1'),'1') AS tipo_cambio");
		}
		campos.append("	,NVL(d.ig_plazo_descuento,'') AS ig_plazo_descuento"   +
						"	,DECODE(d.ic_tipo_financiamiento,1,NVL(d.fn_porc_descuento,0),0) AS fn_porc_descuento"+
						" 	,tf.cd_descripcion AS modo_plazo"   +
						" 	,ed.cd_descripcion AS estatus"   +
						" 	,m.ic_moneda"   +
						" 	,d.ic_documento"   +
						" 	,e.cg_razon_social AS nombre_epo"   +
						" 	,decode(NVL(PE.CG_TIPO_COBRANZA,PN.CG_TIPO_COBRANZA),'D','Delegada','N','No Delegada') AS tipo_cobranza"   +
						" 	,I.cg_razon_social as NOMBRE_IF"   +
						" 	,DECODE(PP.cg_tipo_credito,'D','Modalidad 1 (Riesgo Empresa de Primer Orden )','C','Modalidad 2 (Riesgo Distribuidor)') AS tipo_credito"   +
						" 	,ds.fn_importe_recibir AS monto_credito"   +
						" 	,d.ig_plazo_credito"   +
						" 	,tci.cd_descripcion AS tipo_cobro_int"   +
						"  ,DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',ct.cd_nombre ||' ' || ds.cg_rel_mat||' '||ds.fn_puntos) AS referencia_int"+
						" 	,ds.fn_valor_tasa AS valor_tasa_int"   +
						" 	,ds.fn_importe_interes AS monto_tasa_int");
		if(	("11".equals(ic_estatus_docto)&&"D".equals(tipoSolic))	|| ("22".equals(ic_estatus_docto)&&"D".equals(tipoSolic))){//MOD +(|| ("24".equals(ic_estatus_docto)&&"D".equals(tipoSolic)))
			campos.append("	,ce.cg_numero_pago");
		}
		if(("20".equals(ic_estatus_docto)&&"D".equals(tipoSolic))){
			campos.append(" 	,ce.ct_cambio_motivo");
		}
		campos.append("	,TO_CHAR(ds.df_fecha_seleccion,'dd/mm/yyyy') AS fecha_seleccion "+
						"	,TO_CHAR(d.df_fecha_venc_credito,'dd/mm/yyyy') AS fecha_venc_credito");

		if(("1".equals(ic_estatus_docto)&&"C".equals(tipoSolic)) || 
			("2".equals(ic_estatus_docto)&&"C".equals(tipoSolic)) || 
			(("3".equals(ic_estatus_docto)||"24".equals(ic_estatus_docto))&&"C".equals(tipoSolic)) ||
			("4".equals(ic_estatus_docto)&&"C".equals(tipoSolic))	||
			("11".equals(ic_estatus_docto)&&"D".equals(tipoSolic))||
			("22".equals(ic_estatus_docto)&&"D".equals(tipoSolic)) ){//MOD +((|| "24".equals(ic_estatus_docto)&&"D".equals(tipoSolic)))
			campos.append("	,TO_CHAR(ca.df_fecha_hora,'dd/mm/yyyy') AS fecha_oper_if");
		}
		if(("20".equals(ic_estatus_docto)&&"D".equals(tipoSolic))){
			campos.append("	,TO_CHAR(ce.dc_fecha_cambio,'dd/mm/yyyy') AS fecha_cambio");
		}
		campos.append("	,lc.ic_moneda AS moneda_linea, '24finandist/24if/24reporte01ext.jsp' AS pantalla ");

	//tablas
		tablas = new StringBuffer();
		tablas.append("	FROM dis_documento d"   +
							"  ,comcat_pyme py"   +
							"  ,comcat_moneda m"   +
							"  ,comrel_producto_epo pe"   +
							"  ,comcat_producto_nafin pn"   +
							"  ,comcat_tipo_financiamiento tf"   +
							"  ,comcat_estatus_docto ed"   +
							"  ,(SELECT ic_linea_credito,ic_if,ic_tipo_cobro_interes ,'C' AS cg_tipo_credito,ic_moneda FROM com_linea_credito WHERE ic_producto_nafin = ? "   +
							"    UNION ALL"   +
							"    SELECT ic_linea_credito_dm AS ic_linea_credito,ic_if,ic_tipo_cobro_interes,'D' AS cg_tipo_credito,ic_moneda FROM dis_linea_credito_dm WHERE ic_producto_nafin = ? ) lc"   +
							"  ,comcat_epo e"   +
							"  ,dis_docto_seleccionado ds"   +
							"  ,comcat_if i"   +
							"  ,comrel_pyme_epo_x_producto pp"   +
							"  ,comcat_tipo_cobro_interes tci"   +
							"  ,comcat_tasa ct");
		if(!(("3".equals(ic_estatus_docto)||"24".equals(ic_estatus_docto))&&"D".equals(tipoSolic))){
			tablas.append("  ,com_tipo_cambio tc");
		}
		conditions.add(new Integer(4));
		conditions.add(new Integer(4));
		if(("1".equals(ic_estatus_docto)&&"C".equals(tipoSolic))	||
			("2".equals(ic_estatus_docto)&&"C".equals(tipoSolic))	||
			(("3".equals(ic_estatus_docto)||"24".equals(ic_estatus_docto))&&"C".equals(tipoSolic))	||
			("4".equals(ic_estatus_docto)&&"C".equals(tipoSolic))	||
			("11".equals(ic_estatus_docto)&&"D".equals(tipoSolic))||
			("22".equals(ic_estatus_docto)&&"D".equals(tipoSolic)) 	){//MOD +((|| "24".equals(ic_estatus_docto)&&"D".equals(tipoSolic)))

			tablas.append("  ,dis_solicitud s,	com_acuse3 ca");

			if(("11".equals(ic_estatus_docto)&&"D".equals(tipoSolic)) || ("22".equals(ic_estatus_docto)&&"D".equals(tipoSolic)) 	){//MOD +( || ("24".equals(ic_estatus_docto)&&"D".equals(tipoSolic)))
				tablas.append("  ,dis_cambio_estatus ce");
			}
		}
		if(("20".equals(ic_estatus_docto)&&"D".equals(tipoSolic))){
			tablas.append("  ,dis_cambio_estatus ce");
		}
		
	//condiciones	
		condiciones = new StringBuffer();
		if((("3".equals(ic_estatus_docto)||"24".equals(ic_estatus_docto))&&"D".equals(tipoSolic))){
			condiciones.append("  WHERE d.ic_pyme = py.ic_pyme AND ds.ic_documento = d.ic_documento AND d.ic_moneda = m.ic_moneda AND pn.ic_producto_nafin = ? "+
									"  AND pn.ic_producto_nafin = pe.ic_producto_nafin AND pe.ic_epo = d.ic_epo AND i.ic_if = lc.ic_if  AND e.ic_epo = d.ic_epo"+
									"  AND pp.ic_epo = e.ic_epo AND pp.ic_pyme = py.ic_pyme AND pp.ic_producto_nafin = ? AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento"+
									"  AND ed.ic_estatus_docto = d.ic_estatus_docto AND tci.ic_tipo_cobro_interes = lc.ic_tipo_cobro_interes AND ct.ic_tasa = ds.ic_tasa"+
									"  AND d.ic_estatus_docto = ? "+
									"	AND lc.ic_if = ? "+
									"	AND pp.cg_tipo_credito = lc.cg_tipo_credito"+
									"  AND DECODE(PP.cg_tipo_credito,'D',d.ic_linea_credito_dm,'C',d.ic_linea_credito) = lc.ic_linea_credito"+
									"  AND ds.df_fecha_seleccion >= TRUNC(SYSDATE) AND ds.df_fecha_seleccion < TRUNC(SYSDATE+1)");
			conditions.add(new Integer(4));
			conditions.add(new Integer(4));
			conditions.add(new Integer(ic_estatus_docto));
			conditions.add(new Integer(ic_if));
		}
		if(("1".equals(ic_estatus_docto)&&"C".equals(tipoSolic))){
			condiciones.append(
					"  WHERE d.ic_pyme = py.ic_pyme"   +
					"  AND ds.ic_documento = d.ic_documento"   +
					"  AND s.ic_documento = ds.ic_documento"   +
					"  AND ca.cc_acuse = s.cc_acuse"   	+
					"  AND d.ic_moneda = m.ic_moneda"   +
					"  AND pn.ic_producto_nafin = ? "   +
					"  AND pn.ic_producto_nafin = pe.ic_producto_nafin"   +
					"  AND pe.ic_epo = d.ic_epo" +
					"  AND i.ic_if = lc.ic_if"   +
					"  AND e.ic_epo = d.ic_epo"  +
					"  AND pp.ic_epo = e.ic_epo" +
					"  AND pp.ic_pyme = py.ic_pyme"		+
					"  AND pp.ic_producto_nafin = ? "	+
					"  AND tc.dc_fecha IN(SELECT MAX(dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)"   +
					"  AND M.ic_moneda = TC.ic_moneda"   +
					"  AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento"   +
					"  AND ed.ic_estatus_docto = d.ic_estatus_docto"   +
					"  AND tci.ic_tipo_cobro_interes = lc.ic_tipo_cobro_interes"   +
					"  AND ct.ic_tasa = ds.ic_tasa"   +
					"  AND d.ic_estatus_docto IN(?,?)"   +
					"  AND s.ic_estatus_solic = ? "   +
					"  AND lc.ic_if = ? "   +
					"  AND pp.cg_tipo_credito = lc.cg_tipo_credito"   +
					"  AND DECODE(pp.cg_tipo_credito,'D',d.ic_linea_credito_dm,'C',d.ic_linea_credito) = lc.ic_linea_credito"   +
					"  AND TRUNC(ca.df_fecha_hora) = TRUNC(SYSDATE)"
			);
			conditions.add(new Integer(4));
			conditions.add(new Integer(4));
			conditions.add(new Integer(3));
			conditions.add(new Integer(4));
			conditions.add(new Integer(ic_estatus_docto));
			conditions.add(new Integer(ic_if));
		}
		if(("2".equals(ic_estatus_docto)&&"C".equals(tipoSolic))){
			condiciones.append("  WHERE d.ic_pyme = py.ic_pyme"   +
				"  AND ds.ic_documento = d.ic_documento"   +
				"  AND s.ic_documento = ds.ic_documento"   +
				"  AND d.ic_moneda = m.ic_moneda"   +
				"  AND pn.ic_producto_nafin = ? "   +
				"  AND pn.ic_producto_nafin = pe.ic_producto_nafin"   +
				"  AND pe.ic_epo = d.ic_epo"   +
				"  AND i.ic_if = lc.ic_if"   +
				"  AND e.ic_epo = d.ic_epo"   +
				"  AND ca.cc_acuse = s.cc_acuse"   +
				"  AND pp.ic_epo = e.ic_epo"   +
				"  AND pp.ic_pyme = py.ic_pyme"   +
				"  AND pp.ic_producto_nafin = ? "   +
				"  AND tc.dc_fecha IN(SELECT MAX(dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)"   +
				"  AND m.ic_moneda = tc.ic_moneda"   +
				"  AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento"   +
				"  AND ed.ic_estatus_docto = d.ic_estatus_docto"   +
				"  AND tci.ic_tipo_cobro_interes = lc.ic_tipo_cobro_interes"   +
				"  AND ct.ic_tasa = ds.ic_tasa"   +
				"  AND d.ic_estatus_docto IN(?,?)"   +
				"  AND s.ic_estatus_solic = ? "   +
				"  AND lc.ic_if = ? "   +
				"  AND pp.cg_tipo_credito = lc.cg_tipo_credito"   +
				"  AND DECODE(pp.cg_tipo_credito,'D',d.ic_linea_credito_dm,'C',d.ic_linea_credito) = lc.ic_linea_credito"   +
				"  AND TRUNC(ca.df_fecha_hora) = TRUNC(SYSDATE)");
			conditions.add(new Integer(4));
			conditions.add(new Integer(4));
			conditions.add(new Integer(3));
			conditions.add(new Integer(4));
			conditions.add(new Integer(ic_estatus_docto));
			conditions.add(new Integer(ic_if));
		}
		if((("3".equals(ic_estatus_docto)||"24".equals(ic_estatus_docto))&&"C".equals(tipoSolic))){
			condiciones.append("  WHERE d.ic_pyme = py.ic_pyme"   +
					"  AND ds.ic_documento = d.ic_documento"   +
					"  AND s.ic_documento = ds.ic_documento"   +
					"  AND d.ic_moneda = m.ic_moneda"   +
					"  AND pn.ic_producto_nafin = ? "   +
					"  AND pn.ic_producto_nafin = pe.ic_producto_nafin"   +
					"  AND pe.ic_epo = d.ic_epo"   +
					"  AND i.ic_if = lc.ic_if"   +
					"  AND s.cc_acuse=ca.cc_acuse"+
					"  AND e.ic_epo = d.ic_epo"   +
					"  AND pp.ic_epo = e.ic_epo"   +
					"  AND pp.ic_pyme = py.ic_pyme"   +
					"  AND pp.ic_producto_nafin = ? "   +
					"  AND tc.dc_fecha IN(SELECT MAX(dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)"   +
					"  AND m.ic_moneda = tc.ic_moneda"   +
					"  AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento"   +
					"  AND ed.ic_estatus_docto = d.ic_estatus_docto"   +
					"  AND tci.ic_tipo_cobro_interes = lc.ic_tipo_cobro_interes"   +
					"  AND ct.ic_tasa = ds.ic_tasa"   +
					"  AND d.ic_estatus_docto IN(?)"   +
					"  AND s.ic_estatus_solic = ? "   +
					"  AND lc.ic_if = ? "   +
					"  AND pp.cg_tipo_credito = lc.cg_tipo_credito"   +
					"  AND DECODE(pp.cg_tipo_credito,'D',d.ic_linea_credito_dm,'C',d.ic_linea_credito) = lc.ic_linea_credito"   +
					"  AND TRUNC(s.df_operacion) = TRUNC(SYSDATE)");
			conditions.add(new Integer(4));
			conditions.add(new Integer(4));
			conditions.add(new Integer(4));
			conditions.add(new Integer(ic_estatus_docto));
			conditions.add(new Integer(ic_if));
		}
		if(("4".equals(ic_estatus_docto)&&"C".equals(tipoSolic))){
			condiciones.append("  WHERE d.ic_pyme = py.ic_pyme"   +
					"  AND ds.ic_documento = d.ic_documento"   +
					"  AND s.ic_documento = ds.ic_documento"   +
					"  AND d.ic_moneda = m.ic_moneda"   +
					"  AND pn.ic_producto_nafin = ? "   +
					"  AND pn.ic_producto_nafin = pe.ic_producto_nafin"   +
					"  AND pe.ic_epo = d.ic_epo"   +
					"  AND i.ic_if = lc.ic_if"   +
					"  AND s.cc_acuse=ca.cc_acuse"+
					"  AND e.ic_epo = d.ic_epo"   +
					"  AND pp.ic_epo = e.ic_epo"   +
					"  AND pp.ic_pyme = py.ic_pyme"   +
					"  AND pp.ic_producto_nafin = ? "   +
					"  AND tc.dc_fecha IN(SELECT MAX(dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)"   +
					"  AND m.ic_moneda = tc.ic_moneda"   +
					"  AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento"   +
					"  AND ed.ic_estatus_docto = d.ic_estatus_docto"   +
					"  AND tci.ic_tipo_cobro_interes = lc.ic_tipo_cobro_interes"   +
					"  AND ct.ic_tasa = ds.ic_tasa"   +
					//ESL preguntar dis_documento que estatus sino quitar
					"  AND d.ic_estatus_docto IN(?)"   +
					//ESL "  AND S.IC_ESTATUS_SOLIC = 3"   +
					"  AND s.ic_estatus_solic = ? "   +
					"  AND pp.cg_tipo_credito = lc.cg_tipo_credito"   +
					"  AND DECODE(pp.cg_tipo_credito,'D',d.ic_linea_credito_dm,'C',d.ic_linea_credito) = lc.ic_linea_credito"   +
					"  AND TRUNC(s.df_operacion) = TRUNC(SYSDATE)");
			conditions.add(new Integer(4));
			conditions.add(new Integer(4));
			conditions.add(new Integer(4));
			conditions.add(new Integer(ic_estatus_docto));
		}
		if(("11".equals(ic_estatus_docto)&&"D".equals(tipoSolic))){
			condiciones.append("  WHERE d.ic_pyme = py.ic_pyme"   +
					"  AND ds.ic_documento = d.ic_documento"   +
					"  AND ds.ic_documento = s.ic_documento"+
					"  AND s.cc_acuse = ca.cc_acuse"+
					"  AND d.ic_moneda = m.ic_moneda"   +
					"  AND pn.ic_producto_nafin = ? "   +
					"  AND pn.ic_producto_nafin = pe.ic_producto_nafin"   +
					"  AND pe.ic_epo = d.ic_epo"   +
					"  AND i.ic_if = lc.ic_if"   +
					"  AND e.ic_epo = d.ic_epo"   +
					"  AND pp.ic_epo = e.ic_epo"   +
					"  AND pp.ic_pyme = py.ic_pyme"   +
					"  AND pp.ic_producto_nafin = ? "   +
					"  AND tc.dc_fecha IN(SELECT MAX(dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)"   +
					"  AND m.ic_moneda = tc.ic_moneda"   +
					"  AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento"   +
					"  AND ed.ic_estatus_docto = d.ic_estatus_docto"   +
					"  AND tci.ic_tipo_cobro_interes = lc.ic_tipo_cobro_interes"   +
					"  AND ct.ic_tasa = ds.ic_tasa"   +
					"  AND ce.ic_documento = d.ic_documento"   +
					"  AND ce.ic_cambio_estatus IN(?,?)"   +
					"  AND d.ic_estatus_docto = ? "   +
					"  AND lc.ic_if = ? "   +
					"  AND pp.cg_tipo_credito = lc.cg_tipo_credito"   +
					"  AND DECODE(pp.cg_tipo_credito,'D',d.ic_linea_credito_dm,'C',d.ic_linea_credito) = lc.ic_linea_credito"   +
					"  AND TRUNC(ce.dc_fecha_cambio) = TRUNC(SYSDATE)"   +
					"  AND ce.dc_fecha_cambio IN(SELECT MAX(dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_cambio_estatus IN(?,?) AND ic_documento = d.ic_documento)");
			conditions.add(new Integer(4));
			conditions.add(new Integer(4));
			conditions.add(new Integer(19));
			conditions.add(new Integer(20));
			conditions.add(new Integer(ic_estatus_docto));
			conditions.add(new Integer(ic_if));
			conditions.add(new Integer(19));
			conditions.add(new Integer(20));
		}
		if(("22".equals(ic_estatus_docto)&&"D".equals(tipoSolic))){
			condiciones.append("  WHERE d.ic_pyme = py.ic_pyme"   +
					"  AND ds.ic_documento = d.ic_documento"   +
					"  AND ds.ic_documento = s.ic_documento"+
					"  AND s.cc_acuse = ca.cc_acuse"+
					"  AND d.ic_moneda = m.ic_moneda"   +
					"  AND pn.ic_producto_nafin = ? "   +
					"  AND pn.ic_producto_nafin = pe.ic_producto_nafin"   +
					"  AND pe.ic_epo = d.ic_epo"   +
					"  AND i.ic_if = lc.ic_if"   +
					"  AND e.ic_epo = d.ic_epo"   +
					"  AND pp.ic_epo = e.ic_epo"   +
					"  AND pp.ic_pyme = py.ic_pyme"   +
					"  AND pp.ic_producto_nafin = ? "   +
					"  AND tc.dc_fecha IN(SELECT MAX(dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)"   +
					"  AND m.ic_moneda = tc.ic_moneda"   +
					"  AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento"   +
					"  AND ed.ic_estatus_docto = d.ic_estatus_docto"   +
					"  AND tci.ic_tipo_cobro_interes = lc.ic_tipo_cobro_interes"   +
					"  AND ct.ic_tasa = ds.ic_tasa"   +
					"  AND ce.ic_documento = d.ic_documento"   +
					"  AND ce.ic_cambio_estatus = ? "   +
					"  AND d.ic_estatus_docto = ? "   +
					"  AND lc.ic_if = ? "   +
					"  AND pp.cg_tipo_credito = lc.cg_tipo_credito"   +
					"  AND DECODE(pp.cg_tipo_credito,'D',d.ic_linea_credito_dm,'C',d.ic_linea_credito) = lc.ic_linea_credito "   +
					"  AND TRUNC(ce.dc_fecha_cambio) = TRUNC(SYSDATE)"   +
					"  AND ce.dc_fecha_cambio IN(SELECT MAX(dc_fecha_cambio) FROM dis_cambio_estatus WHERE ic_cambio_estatus = ? AND ic_documento = d.ic_documento)");
			conditions.add(new Integer(4));
			conditions.add(new Integer(4));
			conditions.add(new Integer(18));
			conditions.add(new Integer(ic_estatus_docto));
			conditions.add(new Integer(ic_if));
			conditions.add(new Integer(18));
		}
		if(("20".equals(ic_estatus_docto)&&"D".equals(tipoSolic))){
			condiciones.append("  WHERE d.ic_pyme = py.ic_pyme"   +
					"  AND ds.ic_documento = d.ic_documento"   +
					"  AND d.ic_moneda = m.ic_moneda"   +
					"  AND ce.ic_documento = d.ic_documento"   +
					"  AND pn.ic_producto_nafin = ? "   +
					"  AND pn.ic_producto_nafin = pe.ic_producto_nafin"   +
					"  AND pe.ic_epo = d.ic_epo"   +
					"  AND i.ic_if = lc.ic_if"   +
					"  AND e.ic_epo = d.ic_epo"   +
					"  AND pp.ic_epo = e.ic_epo"   +
					"  AND pp.ic_pyme = py.ic_pyme"   +
					"  AND pp.ic_producto_nafin = ? "   +
					"  AND tc.dc_fecha IN(SELECT MAX(dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)"   +
					"  AND m.ic_moneda = tc.ic_moneda"   +
					"  AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento"   +
					"  AND ed.ic_estatus_docto = d.ic_estatus_docto"   +
					"  AND tci.ic_tipo_cobro_interes = lc.ic_tipo_cobro_interes"   +
					"  AND ct.ic_tasa = ds.ic_tasa"   +
					"  AND d.ic_estatus_docto = ? "   +
					"  AND ce.ic_cambio_estatus = ? "   +
					"  AND lc.ic_if = ? "   +
					"  AND pp.cg_tipo_credito = lc.cg_tipo_credito"   +
					"  AND DECODE(pp.cg_tipo_credito,'D',d.ic_linea_credito_dm,'C',d.ic_linea_credito) = lc.ic_linea_credito"   +
					"  AND TRUNC(ce.dc_fecha_cambio) = TRUNC(SYSDATE)");
			conditions.add(new Integer(4));
			conditions.add(new Integer(4));
			conditions.add(new Integer(ic_estatus_docto));
			conditions.add(new Integer(14));
			conditions.add(new Integer(ic_if));
		}
		
		//MOD +(
		/*
		if(("24".equals(ic_estatus_docto)&&"D".equals(tipoSolic))){
			condiciones.append("  WHERE d.ic_pyme = py.ic_pyme AND ds.ic_documento = d.ic_documento AND d.ic_moneda = m.ic_moneda AND pn.ic_producto_nafin = ? "+
									"  AND pn.ic_producto_nafin = pe.ic_producto_nafin AND pe.ic_epo = d.ic_epo AND i.ic_if = lc.ic_if  AND e.ic_epo = d.ic_epo"+
									"  AND pp.ic_epo = e.ic_epo AND pp.ic_pyme = py.ic_pyme AND pp.ic_producto_nafin = ? AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento"+
									"  AND ed.ic_estatus_docto = d.ic_estatus_docto AND tci.ic_tipo_cobro_interes = lc.ic_tipo_cobro_interes AND ct.ic_tasa = ds.ic_tasa"+
									"  AND d.ic_estatus_docto = ? "+
									"	AND lc.ic_if = ? "+
									"	AND pp.cg_tipo_credito = lc.cg_tipo_credito"+
									"  AND DECODE(PP.cg_tipo_credito,'D',d.ic_linea_credito_dm,'C',d.ic_linea_credito) = lc.ic_linea_credito"+
									"  AND ds.df_fecha_seleccion >= TRUNC(SYSDATE) AND ds.df_fecha_seleccion < TRUNC(SYSDATE+1)");
			conditions.add(new Integer(4));
			conditions.add(new Integer(4));
			conditions.add(new Integer(ic_estatus_docto));
			conditions.add(new Integer(ic_if));
		}*/
		//)

		this.qrysentencia = new StringBuffer();
		this.qrysentencia.append(campos.toString());
		this.qrysentencia.append(tablas.toString());
		this.qrysentencia.append(condiciones.toString());
		System.out.println("El query es - - - "+qrysentencia.toString());
		System.out.println("Las condiciones son - - - "+conditions);
	}

	public String getQrysentencia(){
		return this.qrysentencia.toString();
	}

}//Class End.