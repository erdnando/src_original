package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class MonitorLineasIfDist_inhabil implements IQueryGeneratorRegExtJS {

	public MonitorLineasIfDist_inhabil(){}
	private static final Log log = ServiceLocator.getInstance().getLog(MonitorLineasIfDist_inhabil.class);

	StringBuffer 	querySentencia;
	private List 	conditions;
	private String paginaOffset;
	private String paginaNo;
	private String	ses_ic_if;
	private String tipoCredito;

	public String getAggregateCalculationQuery() {
		/*log.info("getAggregateCalculationQuery(E)");
		querySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		log.debug("..:: qrySentencia "+querySentencia.toString());
		log.debug("..:: conditions: "+conditions);
		log.info("getAggregateCalculationQuery(S)");*/
		return "";
	}

	public String getDocumentSummaryQueryForIds(List pageIds) {
		/*log.info("getDocumentSummaryQueryForIds(E)");
		querySentencia = new StringBuffer();
		conditions 		= new ArrayList();
		log.debug("..:: qrySentencia "+querySentencia.toString());
		log.debug("..:: conditions: "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");*/
		return "";
	}

	public String getDocumentQuery() {
		/*log.info("getDocumentQuery(E)");
		querySentencia = new StringBuffer();
		conditions 		= new ArrayList();
		log.debug("..:: qrySentencia: "+querySentencia.toString());
		log.debug("..:: conditions: "+conditions);
		log.info("getDocumentQuery(S)");*/
		return "";
	}

	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		querySentencia = new StringBuffer();
		conditions 		= new ArrayList();
		if (ses_ic_if != null && !ses_ic_if.equals("")) {
			if(tipoCredito != null && !tipoCredito.equals("") && tipoCredito.equals("F")){
				querySentencia.append(
						"SELECT lc.ic_linea_credito AS folio, 'Factoraje con recurso' as tipoCredito, "+
						"       DECODE (lc.cg_tipo_solicitud,'I', 'Inicial', 'A', 'Ampliacion', 'R', 'Renovacion') AS tipo_sol, "+
						"       'N/A' AS nepo, 'N/A' AS epo, TO_CHAR (cn.ic_nafin_electronico) AS npyme, cp.cg_razon_social AS pyme, ic_linea_credito_padre AS folio_rel, "+
						"       TO_CHAR (lc.df_captura, 'DD/MM/YYYY') AS fecha_sol, "+
						//"       TO_CHAR (lc.df_autorizacion_nafin, 'DD/MM/YYYY') AS fecha_auto, "+
						"       TO_CHAR (lc.df_captura, 'DD/MM/YYYY') AS fecha_auto, "+	//Cambio realizado por modificaciones solicitas
						"       cm.cd_nombre AS moneda, el.cd_descripcion AS estatus, lc.cg_causa_rechazo AS causas, cpl.in_plazo_dias AS plazo, "+
						"       TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') AS fecha_venc, "+
						"       cf.cd_nombre, lc.cg_numero_cuenta, lc.cg_numero_cuenta_if, lc.fn_monto_autorizado_total AS monto_total,lc.FN_SALDO_LINEA_TOTAL AS saldo_total, lc.ic_moneda "+
						"  FROM com_linea_credito lc, "+
						"       comrel_nafin cn, "+
						"       comcat_pyme cp, "+
						"       comcat_moneda cm, "+
						"       comcat_estatus_linea el, "+
						"       comcat_plazo cpl, "+
						"       comcat_financiera cf "+
						" WHERE lc.ic_pyme = cn.ic_epo_pyme_if "+
						"   AND cn.cg_tipo = ? "+
						"   AND lc.ic_pyme = cp.ic_pyme "+
						"   AND lc.ic_moneda = cm.ic_moneda "+
						"   AND lc.ic_estatus_linea = el.ic_estatus_linea "+
						"   AND lc.ic_plazo = cpl.ic_plazo "+
						"   AND lc.ic_financiera = cf.ic_financiera (+) "+
						"   AND (TRUNC (lc.df_vencimiento_adicional) - TRUNC (SYSDATE)) <= ? "+
						"	AND TRUNC (lc.df_vencimiento_adicional) > TRUNC(SYSDATE)"+
						"	AND lc.cs_factoraje_con_rec = ? "+
						"   AND lc.ic_if = ? ");
					conditions.add("P");
					conditions.add(new Integer(90));
					conditions.add("S");
					conditions.add(ses_ic_if);
			}else{
				querySentencia.append(
						"SELECT lc.ic_linea_credito_dm AS folio,  'Descuento y/o Factoraje' as tipoCredito, "+
						"       DECODE (lc.cg_tipo_solicitud,'I', 'Inicial','A', 'Ampliacion','R', 'Renovacion') AS tipo_sol, "+
						"       TO_CHAR (cn.ic_nafin_electronico) AS nepo, ce.cg_razon_social AS epo, 'N/A' AS npyme, 'N/A' AS pyme, ic_linea_credito_dm_padre AS folio_rel, "+
						"       TO_CHAR (ac4.df_acuse, 'DD/MM/YYYY') AS fecha_auto,TO_CHAR (ac4.df_acuse, 'DD/MM/YYYY') AS fecha_sol, cm.cd_nombre AS moneda,  "+
						"       el.cd_descripcion AS estatus, '' AS causas, lc.ig_plazo AS plazo, "+
						"       TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') AS fecha_venc, "+
						"       cf.cd_nombre, lc.cg_num_cuenta_epo, lc.cg_num_cuenta_if, lc.fn_monto_autorizado_total AS monto_total,lc.FN_SALDO_TOTAL AS saldo_total, lc.ic_moneda "+
						"  FROM dis_linea_credito_dm lc, "+
						"       comrel_nafin cn, "+
						"       comcat_epo ce, "+
						"       dis_acuse4 ac4, "+
						"       comcat_moneda cm, "+
						"       comcat_estatus_linea el, "+
						"       comcat_financiera cf "+
						" WHERE lc.ic_epo = cn.ic_epo_pyme_if "+
						"   AND cn.cg_tipo = ? "+
						"   AND lc.ic_epo = ce.ic_epo "+
						"   AND lc.cc_acuse = ac4.cc_acuse "+
						"   AND lc.ic_moneda = cm.ic_moneda "+
						"   AND lc.ic_estatus_linea = el.ic_estatus_linea "+
						"   AND lc.ic_financiera = cf.ic_financiera (+) "+
						"   AND (TRUNC (lc.df_vencimiento_adicional) - TRUNC (SYSDATE)) <= ? "+
						"	AND TRUNC (lc.df_vencimiento_adicional) > TRUNC(SYSDATE)"+						
						"  AND lc.ic_if = ? "+
						" UNION ALL "+
						"SELECT lc.ic_linea_credito AS folio, 'Credito en Cuenta Corriente' as tipoCredito, "+
						"       DECODE (lc.cg_tipo_solicitud,'I', 'Inicial', 'A', 'Ampliacion', 'R', 'Renovacion') AS tipo_sol, "+
						"       'N/A' AS nepo, 'N/A' AS epo, TO_CHAR (cn.ic_nafin_electronico) AS npyme, cp.cg_razon_social AS pyme, ic_linea_credito_padre AS folio_rel, "+
						"       TO_CHAR (lc.df_captura, 'DD/MM/YYYY') AS fecha_sol, "+
						"       TO_CHAR (lc.df_autorizacion_nafin, 'DD/MM/YYYY') AS fecha_auto, "+
						"       cm.cd_nombre AS moneda, el.cd_descripcion AS estatus, lc.cg_causa_rechazo AS causas, cpl.in_plazo_dias AS plazo, "+
						"       TO_CHAR (lc.df_vencimiento_adicional, 'DD/MM/YYYY') AS fecha_venc, "+
						"       cf.cd_nombre, lc.cg_numero_cuenta, lc.cg_numero_cuenta_if, lc.fn_monto_autorizado_total AS monto_total,lc.FN_SALDO_LINEA_TOTAL AS saldo_total, lc.ic_moneda "+
						"  FROM com_linea_credito lc, "+
						"       comrel_nafin cn, "+
						"       comcat_pyme cp, "+
						"       comcat_moneda cm, "+
						"       comcat_estatus_linea el, "+
						"       comcat_plazo cpl, "+
						"       comcat_financiera cf "+
						" WHERE lc.ic_pyme = cn.ic_epo_pyme_if "+
						"   AND cn.cg_tipo = ? "+
						"   AND lc.ic_pyme = cp.ic_pyme "+
						"   AND lc.ic_moneda = cm.ic_moneda "+
						"   AND lc.ic_estatus_linea = el.ic_estatus_linea "+
						"   AND lc.ic_plazo = cpl.ic_plazo "+
						"   AND lc.ic_financiera = cf.ic_financiera (+) "+
						"   AND (TRUNC (lc.df_vencimiento_adicional) - TRUNC (SYSDATE)) <= ? "+
						"	AND TRUNC (lc.df_vencimiento_adicional) > TRUNC(SYSDATE)"+
						"	AND lc.cs_factoraje_con_rec = ? "+
						"  AND lc.ic_if = ? ");
					conditions.add("E");
					conditions.add(new Integer(90));				
					conditions.add(ses_ic_if);
					conditions.add("P");
					conditions.add(new Integer(90));
					conditions.add("N");
					conditions.add(ses_ic_if);
			}
		}
		log.debug("..:: qrySentencia: "+querySentencia.toString());
		log.debug("..:: conditions: "+conditions);
		log.info("getDocumentQueryFile(S)");
		return querySentencia.toString();
	}

  	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String nombreArchivo	=	"";
		String linea 			= "";
		OutputStreamWriter writer	= null;
		BufferedWriter buffer 		= null;
		
		if ("CSV".equals(tipo)) {
			try {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
				buffer = new BufferedWriter(writer);
				boolean flag = true;
				while (rs.next()) {
					String rsFolioSolic		= rs.getString(1)==null?"":rs.getString(1);
					String rsTipoCredito 	= rs.getString(2)==null?"":rs.getString(2);
					String rsTipoSol 			= rs.getString(3)==null?"":rs.getString(3);
					String rsNumEpo 			= rs.getString(4)==null?"":rs.getString(4);
					String rsEpo 				= rs.getString(5)==null?"":rs.getString(5);
					String rsNumPyme 			= rs.getString(6)==null?"":rs.getString(6);
					String rsPyme 				= rs.getString(7)==null?"":rs.getString(7);
					String rsFolioSolicRel 	= rs.getString(8)==null?"":rs.getString(8);
					String rsFechaSol			= rs.getString(9)==null?"":rs.getString(9);
					String rsFechaAuto		= rs.getString(10)==null?"":rs.getString(10);
					String rsMoneda			= rs.getString(11)==null?"":rs.getString(11);
					String rsestatus			= rs.getString(12)==null?"":rs.getString(12);
					String rsPlazo				= rs.getString(14)==null?"":rs.getString(14);
					String rsFechaVenc		= rs.getString(15)==null?"":rs.getString(15);
					String rsMontoAuto		= rs.getString(19)==null?"":rs.getString(19);
					String rsMtoSol			= rs.getString(20)==null?"":rs.getString(20);
					if (flag) {
						linea = "\n Folio de solicitud,Esquema l�nea de cr�dito,Tipo de solicitud,Num. EPO,EPO,Num. Distribuidor,Distribuidor,Folio solicitud relacionada,Fecha solicitud,Fecha autorizaci�n,Moneda,Estatus,Plazo,Fecha de vencimiento,Monto autorizado,Saldo Disponible";
						buffer.write(linea);
					}
					flag= false;
					linea =	"\n"+rsFolioSolic+","+rsTipoCredito+","+rsTipoSol+","+rsNumEpo+","+rsEpo.replace(',',' ')+","+rsNumPyme+","+
					rsPyme.replace(',',' ')+","+rsFolioSolicRel+","+rsFechaSol+","+rsFechaAuto+","+rsMoneda.replace(',',' ')+","+rsestatus+","+
					rsPlazo+","+rsFechaVenc+","+rsMontoAuto+","+rsMtoSol;
					buffer.write(linea);
				}
				if (flag){
					linea = "\nNo se encontro ningun registro";
					buffer.write(linea);
				}
				buffer.close();
			}catch (Throwable e)	{
				throw new AppException("Error al generar el archivo", e);
			}finally{
				try {
					rs.close();
				}catch(Exception e) {}
			}
		}else if ("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				int nRow = 0;
				boolean flag = true;
				while (rs.next()) {
					String rsFolioSolic		= rs.getString(1)==null?"":rs.getString(1);
					String rsTipoCredito 	= rs.getString(2)==null?"":rs.getString(2);
					String rsTipoSol 			= rs.getString(3)==null?"":rs.getString(3);
					String rsNumEpo 			= rs.getString(4)==null?"":rs.getString(4);
					String rsEpo 				= rs.getString(5)==null?"":rs.getString(5);
					String rsNumPyme 			= rs.getString(6)==null?"":rs.getString(6);
					String rsPyme 				= rs.getString(7)==null?"":rs.getString(7);
					String rsFolioSolicRel 	= rs.getString(8)==null?"":rs.getString(8);
					String rsFechaSol			= rs.getString(9)==null?"":rs.getString(9);
					String rsFechaAuto		= rs.getString(10)==null?"":rs.getString(10);
					String rsMoneda			= rs.getString(11)==null?"":rs.getString(11);
					String rsestatus			= rs.getString(12)==null?"":rs.getString(12);
					String rsPlazo				= rs.getString(14)==null?"":rs.getString(14);
					String rsFechaVenc		= rs.getString(15)==null?"":rs.getString(15);
					String rsMontoAuto		= rs.getString(19)==null?"":rs.getString(19);
					String rsMtoSol			= rs.getString(20)==null?"":rs.getString(20);
					if (flag) {
						String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
						String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
						String diaActual    = fechaActual.substring(0,2);
						String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
						String anioActual   = fechaActual.substring(6,10);
						String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
							 
						  pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
							 session.getAttribute("iNoNafinElectronico").toString(),
							 (String)session.getAttribute("sesExterno"),
							 (String) session.getAttribute("strNombre"),
							 (String) session.getAttribute("strNombreUsuario"),
							 (String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
							 
						pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
						pdfDoc.addText(" ","formas", ComunesPDF.CENTER);

						pdfDoc.setTable(16, 100);
						pdfDoc.setCell("Folio de solicitud", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setCell("Esquema l�nea de cr�dito", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setCell("Tipo de solicitud", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setCell("Num. EPO", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setCell("EPO", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setCell("Num. Distribuidor", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setCell("Distribuidor", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setCell("Folio solicitud relacionada", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha solicitud", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha autorizaci�n", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setCell("Moneda", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setCell("Estatus", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setCell("Plazo", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setCell("Fecha de vencimiento", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setCell("Monto autorizado", "formasmenB", ComunesPDF.CENTER);
						pdfDoc.setCell("Saldo Disponible", "formasmenB", ComunesPDF.CENTER);
					}
					flag= false;
					pdfDoc.setCell(rsFolioSolic, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(rsTipoCredito, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(rsTipoSol, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(rsNumEpo, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(rsEpo, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(rsNumPyme, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(rsPyme, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(rsFolioSolicRel, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(rsFechaSol, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(rsFechaAuto, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(rsMoneda, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(rsestatus, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(rsPlazo, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell(rsFechaVenc, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell("$ "+rsMontoAuto, "formas", ComunesPDF.CENTER);
					pdfDoc.setCell("$ "+rsMtoSol, "formas", ComunesPDF.CENTER);
				}
				if (flag) {
					pdfDoc.addText("No se encontr� ning�n registro","formas", ComunesPDF.CENTER);
				}else{
					pdfDoc.addTable();
				}
				pdfDoc.endDocument();
			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
			}
		}
		return nombreArchivo;
	}

	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		String nombreArchivo	=	"";
		return nombreArchivo;
	}

	/**
	  Obtiene la lista de par�metros a usar como variables bind en las condiciones de la consulta
	  @return Lista con los parametros de las condiciones 
	 */
	public List getConditions() {  return conditions;  }
  /**
	 * Obtiene el numero de pagina.
	 * @return Cadena con el numero de pagina
	 */
	public String getPaginaNo() { return paginaNo; 	}
  /**
	 * Obtiene el offset de la pagina.
	 * @return Cadena con el offset de pagina
	 */
	public String getPaginaOffset() { return paginaOffset; 	}

/*****************************************************
					 SETTERS
*******************************************************/
	/**
	 * Establece el numero de pagina.
	 * @param  newPaginaNo Cadena con el numero de pagina
	 */
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
  
  /**
	 * Establece el offset de la pagina.
	 * @param newPaginaOffset Cadena con el offset de pagina
	 */
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public void setIc_if(String ses_ic_if) {
		this.ses_ic_if = ses_ic_if;
	}
	public void setTipoCredito(String tipoCredito) {
		this.tipoCredito = tipoCredito;
	}

}