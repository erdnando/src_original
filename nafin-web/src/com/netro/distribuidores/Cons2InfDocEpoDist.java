package com.netro.distribuidores;

import com.netro.pdf.ComunesPDF;

import java.math.BigDecimal;

import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGenerator;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class Cons2InfDocEpoDist implements IQueryGenerator, IQueryGeneratorRegExtJS{
  public Cons2InfDocEpoDist() {  }
	public String getAggregateCalculationQuery(HttpServletRequest request) {
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
    	StringBuffer qrySentencia	= new StringBuffer();
    	StringBuffer qryDM			= new StringBuffer();
    	StringBuffer qryCCC			= new StringBuffer();
    	StringBuffer condicion		= new StringBuffer();
      StringBuffer qryFdR			= new StringBuffer();

		String	ic_epo				=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String	ic_pyme				=	(request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String	ic_if				=	(request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String	ic_estatus_docto	=	(request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto");
		String 	ig_numero_docto		=	(request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String 	fecha_seleccion_de	=	(request.getParameter("fecha_seleccion_de")==null)?"":request.getParameter("fecha_seleccion_de");
		String 	fecha_seleccion_a	=	(request.getParameter("fecha_seleccion_a")==null)?"":request.getParameter("fecha_seleccion_a");
		String 	cc_acuse			=	(request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String 	monto_credito_de	=	(request.getParameter("monto_credito_de")==null)?"":request.getParameter("monto_credito_de");
		String 	monto_credito_a		=	(request.getParameter("monto_credito_a")==null)?"":request.getParameter("monto_credito_a");
		String 	fecha_emision_de	=	(request.getParameter("fecha_emision_de")==null)?"":request.getParameter("fecha_emision_de");
		String 	fecha_emision_a		=	(request.getParameter("fecha_emision_a")==null)?"":request.getParameter("fecha_emision_a");
		String 	fecha_vto_de		=	(request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String 	fecha_vto_a			=	(request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String 	fecha_vto_credito_de=	(request.getParameter("fecha_vto_credito_de")==null)?"":request.getParameter("fecha_vto_credito_de");
		String 	fecha_vto_credito_a =	(request.getParameter("fecha_vto_credito_a")==null)?"":request.getParameter("fecha_vto_credito_a");
		String 	ic_tipo_cobro_int	=	(request.getParameter("ic_tipo_cobro_int")==null)?"":request.getParameter("ic_tipo_cobro_int");
		String	ic_moneda			=	(request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String 	fn_monto_de			=	(request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String 	fn_monto_a			=	(request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String 	monto_con_descuento	=	(request.getParameter("monto_con_descuento")==null)?"":"checked";
		String	solo_cambio			=	(request.getParameter("solo_cambio")==null)?"":request.getParameter("solo_cambio");
		String	modo_plazo			=	(request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
		String	tipo_credito		=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String	ic_documento		=	(request.getParameter("ic_documento")==null)?"":request.getParameter("ic_documento");
		String 	fecha_publicacion_de=	(request.getParameter("fecha_publicacion_de")==null)?fechaHoy:request.getParameter("fecha_publicacion_de");
		String 	fecha_publicacion_a	=	(request.getParameter("fecha_publicacion_a")==null)?fechaHoy:request.getParameter("fecha_publicacion_a");
		String 	NOnegociable			=	(request.getParameter("NOnegociable")==null)?"":request.getParameter("NOnegociable");//Fodea 029-2010 Distribuidores Fase III
		String tipo_pago = (request.getParameter("tipo_pago")==null)?"":request.getParameter("tipo_pago"); // F009-2015

		try {
			if(!"".equals(ic_pyme)&&ic_pyme!=null)
				condicion.append(" AND D.IC_PYME = "+ic_pyme);
		 	if("4".equals(ic_estatus_docto)||"20".equals(ic_estatus_docto)||"22".equals(ic_estatus_docto)||"3".equals(ic_estatus_docto)||"11".equals(ic_estatus_docto) ||"1".equals(ic_estatus_docto)||"32".equals(ic_estatus_docto) ||"28".equals(ic_estatus_docto)||"30".equals(ic_estatus_docto))		
				condicion.append(" AND D.IC_ESTATUS_DOCTO ="+ic_estatus_docto);
			if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
				condicion.append(" AND D.ig_numero_docto = '"+ig_numero_docto+"'");
			if(!"".equals(fecha_seleccion_de)&& fecha_seleccion_de!=null&&!"".equals(fecha_seleccion_a)&& fecha_seleccion_a!=null)
				condicion.append(" AND trunc(DS.df_fecha_seleccion) between trunc(to_date('"+fecha_seleccion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_seleccion_a+"','dd/mm/yyyy'))");
			if(!"".equals(cc_acuse)&& cc_acuse!=null)
				condicion.append(" AND D.cc_acuse = '"+cc_acuse+"'");
			if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null)
				condicion.append(" AND TRUNC(D.df_fecha_emision) BETWEEN TRUNC(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) AND TRUNC(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
				condicion.append(" AND TRUNC(D.df_fecha_venc) BETWEEN TRUNC(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) AND TRUNC(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null)
				condicion.append(" AND TRUNC(D.df_fecha_venc_credito) BETWEEN TRUNC(to_date('"+fecha_vto_credito_de+"','dd/mm/yyyy')) AND TRUNC(to_date('"+fecha_vto_credito_a+"','dd/mm/yyyy'))");
			if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null)
				condicion.append(" AND LC.ic_tipo_cobro_interes = "+ic_tipo_cobro_int);
			if(!"".equals(ic_moneda)&&ic_moneda!=null)
				condicion.append(" AND D.ic_moneda = "+ic_moneda);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
				condicion.append(" AND (D.fn_monto*(fn_porc_descuento/100)) BETWEEN "+fn_monto_de+" AND "+fn_monto_a);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
				condicion.append(" AND D.fn_monto BETWEEN "+fn_monto_de+" AND "+fn_monto_a);
			if(!"".equals(solo_cambio)&& solo_cambio!=null)
				condicion.append(" AND D.ic_documento IN (SELECT ic_documento FROM dis_cambio_estatus WHERE ic_cambio_estatus = 8)");
			if(!"".equals(modo_plazo)&& modo_plazo!=null)
				condicion.append(" AND D.ic_tipo_financiamiento = "+modo_plazo);
			if(!"".equals(ic_documento)&&ic_documento!=null)
				condicion.append(" AND D.IC_DOCUMENTO = "+ic_documento);
			if(!"".equals(ic_if)&&ic_if!=null)
				condicion.append(" AND LC.IC_IF = "+ic_if);
			if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
				condicion.append(" AND TRUNC(D.df_carga) BETWEEN TRUNC(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) AND TRUNC(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))");
			if(!"".equals(monto_credito_de)&& monto_credito_de!=null&&!"".equals(monto_credito_a)&& monto_credito_a!=null)
				condicion.append(" AND (D.fn_monto-(fn_monto*(fn_porc_descuento/100))) BETWEEN "+monto_credito_de+" AND "+monto_credito_a);
			if("".equals(ic_estatus_docto) && NOnegociable.equals("N")){ //Fodea 029-2010 Distribuidores Fase III
				condicion.append(" AND D.ic_estatus_docto IN (2,3,4,5,9,11,20,22,24,32,28,30)");//MOD +(24)
			}else 	if("".equals(ic_estatus_docto) && NOnegociable.equals("S")){
				condicion.append(" AND D.ic_estatus_docto IN (2,1,3,4,5,9,11,20,22,24,32,28,30)");//MOD +(24)
			}
			if(!"F".equals(tipo_credito)) {
				if(!"".equals(ic_estatus_docto)&&!"4".equals(ic_estatus_docto)&&!"20".equals(ic_estatus_docto)&&!"22".equals(ic_estatus_docto)&&!"3".equals(ic_estatus_docto)&&!"11".equals(ic_estatus_docto)&&!"1".equals(ic_estatus_docto)&&!"24".equals(ic_estatus_docto)&&!"32".equals(ic_estatus_docto)&&!"28".equals(ic_estatus_docto)&&!"30".equals(ic_estatus_docto))//MOD +(&&!"24".equals(ic_estatus_docto))//if("".equals(condicion))
					condicion.append(" and D.ic_estatus_docto is null");
			}
			if("F".equals(tipo_credito)) {
				//condicion.append(" AND pep.CG_TIPO_CREDITO IS NULL");  
			} else {
				condicion.append(" AND pep.CG_TIPO_CREDITO IS NOT NULL");
			}
			if(!tipo_pago.equals("")){
				condicion.append(" AND D.IG_TIPO_PAGO = " + tipo_pago);
			}
			qryDM.append(
				" SELECT   /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */"   +
				"       distinct  d.ic_moneda AS moneda, m.cd_nombre AS nommoneda, d.fn_monto AS monto, "   +
				"        DECODE(d.ic_moneda,lc.ic_moneda,0,"   +
				"           (d.fn_monto-"   +
				"             (d.fn_monto*(NVL(D.FN_PORC_DESCUENTO,0)/100)))*"   +
				"             DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',"   +
				"             DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,1),1)) AS montovaluado"  +
				//"			,	(d.fn_monto - ((d.fn_monto * ds.FN_PORC_COMISION_APLI) / 100)   ) AS monto_depositar	"+
				"			,	0 AS monto_depositar	"+
				"   FROM dis_documento d,"   +
				"        dis_docto_seleccionado ds,"   +
				"        comcat_pyme py,"   +
				"        comcat_if i,"   +
			//	"        comcat_if i2,"   +
				"        comrel_producto_epo pe,"   +
				"        comrel_pyme_epo_x_producto pep,"   +
				"        dis_linea_credito_dm lc,"   +
				"        comcat_moneda m,"   +
				"        comcat_producto_nafin pn,"   +
				"        com_tipo_cambio tc,"   +
				"        comcat_tasa ct,"   +
				"        comcat_estatus_docto ed,"   +
				"        comcat_tipo_financiamiento tf,"   +
				"        comcat_tipo_cobro_interes tci"   +
				/*
				"	,com_bins_if bins, "+
				"	comrel_producto_if cpi	"+
				"	,DIS_DOCTOS_PAGO_TC ptc "+
				*/
				"  WHERE d.ic_epo = pep.ic_epo"   +
				"    AND d.ic_pyme = py.ic_pyme"   +
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento"   +
				"    AND d.ic_estatus_docto = ed.ic_estatus_docto"   +
				"    AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm (+)"   + 
				"    AND d.ic_documento = ds.ic_documento"   +
				"    AND ds.ic_tasa = ct.ic_tasa"   +
				"    AND pn.ic_producto_nafin = pe.ic_producto_nafin"   +
				"    AND d.ic_moneda = tc.ic_moneda"   +
				"    AND lc.ic_if = i.ic_if"   +
				/*
				" AND d.IC_BINS = bins.IC_BINS(+) "+
				" AND bins.IC_if = i2.IC_if(+)"+

				 "	and lc.IC_IF = cpi.ic_if "+
				 "	and pep.IC_PRODUCTO_NAFIN = cpi.IC_PRODUCTO_NAFIN "+
				 "	and d.IC_ORDEN_PAGO_TC  =ptc.IC_ORDEN_PAGO_TC (+) "+
				*/
				"    AND pep.ic_pyme = py.ic_pyme"   +
				"    AND lc.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes"   +
				"    AND tc.dc_fecha = (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = d.ic_moneda)"   +
//				"    AND pep.cs_habilitado = 'S'"   +
				"    AND d.ic_producto_nafin = 4"   +
				"    AND pep.ic_producto_nafin = 4"   +
				"    AND pn.ic_producto_nafin = 4"   +
				"    AND d.ic_epo = "+ic_epo+" "+condicion.toString()+" ");

			qryCCC.append(
				" SELECT   /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */"   +
				"       distinct d.ic_moneda AS moneda, m.cd_nombre AS nommoneda, d.fn_monto AS monto, "   +
				"        DECODE(d.ic_moneda,lc.ic_moneda,0,"   +
				"           (d.fn_monto-"   +
				"             (d.fn_monto*(NVL(D.FN_PORC_DESCUENTO,0)/100)))*"   +
				"             DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',"   +
				"             DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,1),1)) AS montovaluado"  +
				//"			,	(ds.fn_importe_recibir - ((ds.fn_importe_recibir * ds.FN_PORC_COMISION_APLI) / 100)   ) AS monto_depositar	"+
				"				,	(ds.fn_importe_recibir - ((((ds.fn_importe_recibir * ds.fn_porc_comision_apli) / 100)+((ds.fn_importe_recibir * ds.fn_porc_comision_apli) / 100)  * (SELECT iva.fn_porcentaje_iva FROM comcat_iva iva WHERE iva.ic_iva = (SELECT MAX (ic_iva) FROM comcat_iva)) / 100 )) ) AS monto_depositar	"+				
				"   FROM dis_documento d,"   +
				"        dis_docto_seleccionado ds,"   +
				"        comcat_pyme py,"   +
				"        comcat_if i,"   +
				"        comcat_if i2,"   +
				"        comrel_producto_epo pe,"   +
				"        comrel_pyme_epo_x_producto pep,"   +
				"        com_linea_credito lc,"   +
				"        comcat_moneda m,"   +
				"        comcat_producto_nafin pn,"   +
				"        com_tipo_cambio tc,"   +
				"        comcat_tasa ct,"   +
				"        comcat_estatus_docto ed,"   +
				"        comcat_tipo_financiamiento tf,"   +
				"        comcat_tipo_cobro_interes tci"   +
				
				"	,com_bins_if bins, "+
				"	comrel_producto_if cpi	"+
				"	,DIS_DOCTOS_PAGO_TC ptc "+
				
				"  WHERE d.ic_epo = pep.ic_epo"   +
				"    AND d.ic_pyme = py.ic_pyme"   +
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento"   +
				"    AND d.ic_estatus_docto = ed.ic_estatus_docto"   +
				"    AND d.ic_linea_credito = lc.ic_linea_credito"   +
				"    AND d.ic_documento = ds.ic_documento"   +
				"    AND ds.ic_tasa = ct.ic_tasa"   +
				"    AND pn.ic_producto_nafin = pe.ic_producto_nafin"   +
				"    AND d.ic_moneda = tc.ic_moneda"   +
				"    AND lc.ic_if = i.ic_if"   +
				
				" AND d.IC_BINS = bins.IC_BINS(+) "+
				" AND bins.IC_if = i2.IC_if(+)"+
	
				 "	and lc.IC_IF = cpi.ic_if "+
				 "	and pep.IC_PRODUCTO_NAFIN = cpi.IC_PRODUCTO_NAFIN "+
				 "	and d.IC_ORDEN_PAGO_TC  =ptc.IC_ORDEN_PAGO_TC (+) "+
				
				"    AND pep.ic_pyme = py.ic_pyme"   +
				"    AND lc.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes"   +
				"    AND tc.dc_fecha = (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = d.ic_moneda)"   +
//				"    AND pep.cs_habilitado = 'S'"   +
				"    AND d.ic_producto_nafin = 4"   +
				"    AND pep.ic_producto_nafin = 4"   +
				"    AND pn.ic_producto_nafin = 4"  +
				"    AND d.ic_epo = "+ic_epo+" "+condicion.toString()+" ");
				
			qryFdR.append(
				" SELECT   /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */"   +
				"      distinct  d.ic_moneda AS moneda, m.cd_nombre AS nommoneda, d.fn_monto AS monto, "   +
				"        DECODE(d.ic_moneda,lc.ic_moneda,0,"   +
				"           (d.fn_monto-"   +
				"             (d.fn_monto*(NVL(D.FN_PORC_DESCUENTO,0)/100)))*"   +
				"             DECODE(NVL(PE.CG_TIPO_CONVERSION,PN.CG_TIPO_CONVERSION),'P',"   +
				"             DECODE(M.ic_moneda,54,TC.FN_VALOR_COMPRA,1),1)) AS montovaluado"  +
				"				,0 AS monto_depositar	"+
				"   FROM dis_documento d,"   +
				"        dis_docto_seleccionado ds,"   +
				"        comcat_pyme py,"   +
				"        comcat_if i,"   +
				"        comcat_if i2,"   +
				"        comrel_producto_epo pe,"   +
				"        comrel_pyme_epo_x_producto pep,"   +
				"        com_linea_credito lc,"   +
				"        comcat_moneda m,"   +
				"        comcat_producto_nafin pn,"   +
				"        com_tipo_cambio tc,"   +
				"        comcat_tasa ct,"   +
				"        comcat_estatus_docto ed,"   +
				"        comcat_tipo_financiamiento tf,"   +
				"        comcat_tipo_cobro_interes tci"   +
				/*
				"	,com_bins_if bins, "+
				"	comrel_producto_if cpi	"+
				"	,DIS_DOCTOS_PAGO_TC ptc "+
				*/
				"  WHERE d.ic_epo = pep.ic_epo"   +
				"    AND d.ic_pyme = py.ic_pyme"   +
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento(+)"   +
				"    AND d.ic_estatus_docto = ed.ic_estatus_docto"   +
				"    AND d.ic_linea_credito = lc.ic_linea_credito"   +
				"    AND d.ic_documento = ds.ic_documento"   +
				"    AND ds.ic_tasa = ct.ic_tasa"   +
				"    AND pn.ic_producto_nafin = pe.ic_producto_nafin"   +
				"    AND d.ic_moneda = tc.ic_moneda"   +
				"    AND lc.ic_if = i.ic_if"   +
				/*				
				" AND d.IC_BINS = bins.IC_BINS(+) "+
				" AND bins.IC_if = i2.IC_if(+)"+
	
				 "	and lc.IC_IF = cpi.ic_if "+
				 "	and pep.IC_PRODUCTO_NAFIN = cpi.IC_PRODUCTO_NAFIN "+
				 "	and d.IC_ORDEN_PAGO_TC  =ptc.IC_ORDEN_PAGO_TC (+) "+
				*/
				"    AND pep.ic_pyme = py.ic_pyme"   +
				"    AND lc.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes"   +
				"    AND tc.dc_fecha = (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = d.ic_moneda)"   +
				"    AND d.ic_producto_nafin = 4"   +
				"    AND pep.ic_producto_nafin = 4"   +
				"    AND pn.ic_producto_nafin = 4"  +
				"   and d.IC_TIPO_FINANCIAMIENTO is null "+
				"	 AND lc.cg_tipo_solicitud = 'I' "+
				"	  AND lc.cs_factoraje_con_rec = 'S' "+
				"    AND d.ic_epo = "+ic_epo+" "+condicion.toString()+" ");
				
			String qryAux = "";	
			if("D".equals(tipo_credito))
				qryAux = qryDM.toString();
			else if("C".equals(tipo_credito))
				qryAux = qryCCC.toString();
			else if("F".equals(tipo_credito))
				qryAux = qryFdR.toString();
			else
				qryAux = qryDM.toString()+ " UNION ALL "+qryCCC.toString();

			qrySentencia.append(
				"SELECT moneda, nommoneda, COUNT (1), SUM (monto), SUM (montovaluado),sum(monto_depositar),'Cons2InfDocEpoDist::getAggregateCalculationQuery'"+
				"  FROM ("+qryAux.toString()+") "+
				" GROUP BY  moneda, nommoneda ORDER BY moneda ");
			System.out.println("Cons2InfDocEpoDist::getAggregateCalculationQuery :"+qrySentencia.toString());
		}catch(Exception e){
			System.out.println("Cons2InfDocEpoDist::getAggregateCalculationQuery "+e);
		}
		return qrySentencia.toString();
	}

	public String getDocumentSummaryQueryForIds(HttpServletRequest request,Collection ids) {
		int i=0;
		StringBuffer qrySentencia	= new StringBuffer();
    	StringBuffer qryDM			= new StringBuffer();
    	StringBuffer qryCCC			= new StringBuffer();
    	StringBuffer condicion		= new StringBuffer();
    	StringBuffer qryFdR			= new StringBuffer();
		
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());      
		String	ic_epo				=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String	ic_pyme				=	(request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String	ic_if				=	(request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String	ic_estatus_docto	=	(request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto");
		String 	ig_numero_docto		=	(request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String 	fecha_seleccion_de	=	(request.getParameter("fecha_seleccion_de")==null)?"":request.getParameter("fecha_seleccion_de");
		String 	fecha_seleccion_a	=	(request.getParameter("fecha_seleccion_a")==null)?"":request.getParameter("fecha_seleccion_a");
		String 	cc_acuse			=	(request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String 	monto_credito_de	=	(request.getParameter("monto_credito_de")==null)?"":request.getParameter("monto_credito_de");
		String 	monto_credito_a		=	(request.getParameter("monto_credito_a")==null)?"":request.getParameter("monto_credito_a");
		String 	fecha_emision_de	=	(request.getParameter("fecha_emision_de")==null)?"":request.getParameter("fecha_emision_de");
		String 	fecha_emision_a		=	(request.getParameter("fecha_emision_a")==null)?"":request.getParameter("fecha_emision_a");
		String 	fecha_vto_de		=	(request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String 	fecha_vto_a			=	(request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String 	fecha_vto_credito_de=	(request.getParameter("fecha_vto_credito_de")==null)?"":request.getParameter("fecha_vto_credito_de");
		String 	fecha_vto_credito_a =	(request.getParameter("fecha_vto_credito_a")==null)?"":request.getParameter("fecha_vto_credito_a");
		String 	ic_tipo_cobro_int	=	(request.getParameter("ic_tipo_cobro_int")==null)?"":request.getParameter("ic_tipo_cobro_int");
		String	ic_moneda			=	(request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String 	fn_monto_de			=	(request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String 	fn_monto_a			=	(request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String 	monto_con_descuento	=	(request.getParameter("monto_con_descuento")==null)?"":"checked";
		String	solo_cambio			=	(request.getParameter("solo_cambio")==null)?"":request.getParameter("solo_cambio");
		String	modo_plazo			=	(request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
		String	tipo_credito		=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String	ic_documento		=	(request.getParameter("ic_documento")==null)?"":request.getParameter("ic_documento");
		//String 	fecha_publicacion_de=	(request.getParameter("fecha_publicacion_de")==null)?fechaHoy:request.getParameter("fecha_publicacion_de");
		String 	fecha_publicacion_de=	(request.getParameter("fecha_publicacion_de")==null)?"":request.getParameter("fecha_publicacion_de");
		//String 	fecha_publicacion_a	=	(request.getParameter("fecha_publicacion_a")==null)?fechaHoy:request.getParameter("fecha_publicacion_a");
		String 	fecha_publicacion_a	=	(request.getParameter("fecha_publicacion_a")==null)?"":request.getParameter("fecha_publicacion_a");
		String 	NOnegociable			=	(request.getParameter("NOnegociable")==null)?"":request.getParameter("NOnegociable");//Fodea 029-2010 Distribuidores Fase III
		String tipo_pago = (request.getParameter("tipo_pago")==null)?"":request.getParameter("tipo_pago"); // F009-2015

		condicion.append(" AND d.ic_documento in (");
		i=0;
		for (Iterator it = ids.iterator(); it.hasNext();i++){
        	if(i>0){
				condicion.append(",");
			}
			condicion.append(" "+it.next().toString()+" ");
		}
		condicion.append(") ");

	
      	if(!"".equals(ic_pyme)&&ic_pyme!=null)
				condicion.append(" AND D.IC_PYME = "+ic_pyme);
		 	if("4".equals(ic_estatus_docto)||"20".equals(ic_estatus_docto)||"22".equals(ic_estatus_docto)||"3".equals(ic_estatus_docto)||"11".equals(ic_estatus_docto) ||"1".equals(ic_estatus_docto))//??????????
				condicion.append(" AND D.IC_ESTATUS_DOCTO ="+ic_estatus_docto);
			if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
				condicion.append(" AND D.ig_numero_docto = '"+ig_numero_docto+"'");
			if(!"".equals(fecha_seleccion_de)&& fecha_seleccion_de!=null&&!"".equals(fecha_seleccion_a)&& fecha_seleccion_a!=null)
				condicion.append(" AND trunc(DS.df_fecha_seleccion) between trunc(to_date('"+fecha_seleccion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_seleccion_a+"','dd/mm/yyyy'))");
			if(!"".equals(cc_acuse)&& cc_acuse!=null)
				condicion.append(" AND D.cc_acuse = '"+cc_acuse+"'");
			if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null)
				condicion.append(" AND TRUNC(D.df_fecha_emision) BETWEEN TRUNC(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) AND TRUNC(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
				condicion.append(" AND TRUNC(D.df_fecha_venc) BETWEEN TRUNC(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) AND TRUNC(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))");
			if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null)
				condicion.append(" AND TRUNC(D.df_fecha_venc_credito) BETWEEN TRUNC(to_date('"+fecha_vto_credito_de+"','dd/mm/yyyy')) AND TRUNC(to_date('"+fecha_vto_credito_a+"','dd/mm/yyyy'))");
			if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null)
				condicion.append(" AND LC.ic_tipo_cobro_interes = "+ic_tipo_cobro_int);
			if(!"".equals(ic_moneda)&&ic_moneda!=null)
				condicion.append(" AND D.ic_moneda = "+ic_moneda);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
				condicion.append(" AND (D.fn_monto*(fn_porc_descuento/100)) BETWEEN "+fn_monto_de+" AND "+fn_monto_a);
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
				condicion.append(" AND D.fn_monto BETWEEN "+fn_monto_de+" AND "+fn_monto_a);
			if(!"".equals(solo_cambio)&& solo_cambio!=null)
				condicion.append(" AND D.ic_documento IN (SELECT ic_documento FROM dis_cambio_estatus WHERE ic_cambio_estatus = 8)");
			if(!"".equals(modo_plazo)&& modo_plazo!=null)
				condicion.append(" AND D.ic_tipo_financiamiento = "+modo_plazo);
			if(!"".equals(ic_documento)&&ic_documento!=null)
				condicion.append(" AND D.IC_DOCUMENTO = "+ic_documento);
			if(!"".equals(ic_if)&&ic_if!=null)
				condicion.append(" AND LC.IC_IF = "+ic_if);
			if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
				condicion.append(" AND TRUNC(D.df_carga) BETWEEN TRUNC(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) AND TRUNC(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))");
			if(!"".equals(monto_credito_de)&& monto_credito_de!=null&&!"".equals(monto_credito_a)&& monto_credito_a!=null)
				condicion.append(" AND (D.fn_monto-(fn_monto*(fn_porc_descuento/100))) BETWEEN "+monto_credito_de+" AND "+monto_credito_a);
			if("".equals(ic_estatus_docto) && NOnegociable.equals("N")){ //Fodea 029-2010 Distribuidores Fase III
				condicion.append(" AND D.ic_estatus_docto IN (2,3,4,5,9,11,20,22,24,32,28,30)");//MOD +(24)
			}else 	if("".equals(ic_estatus_docto) && NOnegociable.equals("S")){
				condicion.append(" AND D.ic_estatus_docto IN (2,1,3,4,5,9,11,20,22,24,32,28,30)");//MOD +(24)
			}
			if(!"F".equals(tipo_credito)) {
				if(!"".equals(ic_estatus_docto)&&!"4".equals(ic_estatus_docto)&&!"20".equals(ic_estatus_docto)&&!"22".equals(ic_estatus_docto)&&!"3".equals(ic_estatus_docto)&&!"11".equals(ic_estatus_docto)&&!"1".equals(ic_estatus_docto)&&!"24".equals(ic_estatus_docto)&&!"32".equals(ic_estatus_docto)&&!"28".equals(ic_estatus_docto)&&!"30".equals(ic_estatus_docto))//MOD +(&&!"24".equals(ic_estatus_docto))//if("".equals(condicion))
					condicion.append(" and D.ic_estatus_docto is null"); 
			}
			if(!tipo_pago.equals("")){
				condicion.append(" AND D.IG_TIPO_PAGO = " + tipo_pago);
			}
			qryDM.append(
				" SELECT /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */"   +
				"       distinct py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse,"   +
				"        TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision,"   +
				"        TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga,"   +
				"        TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc,"   +
				"        d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto,"   +
				"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', '','P', 'Dolar-Peso','') AS tipo_conversion,"   +
				"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio,"   +
				"        NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento,"   +
				"        NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento,"   +
				"        tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus,"   +
				"        m.ic_moneda, d.ic_documento, i.cg_razon_social AS nombre_if,"   +
				"        DECODE (pep.cg_tipo_credito,'D', 'Modalidad 1 (Riesgo Empresa de Primer Orden)','Modalidad 2 (Riesgo Distribuidor)') AS tipo_credito,"   +
				"        TO_CHAR (ds.df_fecha_seleccion, 'DD/MM/YYYY') AS df_operacion_credito,"   +
				"        d.ig_plazo_credito AS plazo_credito,"   +
				"        TO_CHAR (d.df_fecha_venc_credito, 'DD/MM/YYYY') AS df_venc_credito,"   +
				"			DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) AS REF_TASA," +
				"        tci.cd_descripcion AS tipo_cobro_int,"   +
				"        ds.fn_valor_tasa,"   +
				"        ds.fn_importe_interes, lc.ic_moneda AS moneda_linea,clas.cg_descripcion as CATEGORIA,'Cons2InfDocEpoDist::getDocumentSummaryQueryForIds' "   +
				"        ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				
				"	,'' AS cg_ventacartera, "+
				"	0  as comision_aplicable,"+
				"	0 as monto_comision,"+
				"	0 as monto_depositar,"+
				"	'' as bins, "+
				"	'' as Opera_tarjeta	"+
			
				"	,'' as id_orden_enviado	"+
				"	,'' as id_operacion	"+
				"	,'' AS codigo_autorizacion"	+
				"	,'' as fecha_registro	"+
				"	,'' as NUM_TC	"+
				"  , D.IG_TIPO_PAGO " +
				"        ,(SELECT CASE WHEN COUNT(*) > 0 THEN 'S' ELSE 'N' END FROM COM_ARCDOCTOS WHERE CC_ACUSE = d.cc_acuse) AS MUESTRA_VISOR " +
				"   FROM dis_documento d,"   +
				"        dis_docto_seleccionado ds,"   +
				"        comcat_pyme py,"   +
				"        comcat_if i,"   +
				"        comrel_producto_epo pe,"   +
				"        comrel_pyme_epo_x_producto pep,"   +
				"        dis_linea_credito_dm lc,"   +
				"        comcat_tasa ct,"   +
				"        comcat_moneda m,"   +
				"        comcat_estatus_docto ed,"   +
				"        com_tipo_cambio tc,"   +
				"        comcat_producto_nafin pn,"   +
				"        comcat_tipo_financiamiento tf,"   +
				"        comcat_tipo_cobro_interes tci,comrel_clasificacion clas "+
				"  WHERE d.ic_epo = pep.ic_epo"   +
				"    AND d.ic_pyme = py.ic_pyme"   +
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento"   +
				"    AND d.ic_estatus_docto = ed.ic_estatus_docto"   +
				"    AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm (+)"   +
				"    AND d.ic_documento = ds.ic_documento"   +
				"    AND ds.ic_tasa = ct.ic_tasa"   +
				"    AND pn.ic_producto_nafin = pe.ic_producto_nafin"   +
				"    AND m.ic_moneda = tc.ic_moneda"   +
				"    AND lc.ic_if = i.ic_if"   +
				/* 
				"	AND d.IC_BINS = bins.IC_BINS(+)"+
				"  AND bins.IC_if = i2.IC_if(+) "+
				
				 "	and lc.IC_IF = cpi.ic_if "+
				 "	and pep.IC_PRODUCTO_NAFIN = cpi.IC_PRODUCTO_NAFIN "+
				 "	and d.IC_ORDEN_PAGO_TC  =ptc.IC_ORDEN_PAGO_TC (+) "+
				*/
				"    AND pep.ic_pyme = py.ic_pyme"   +
				"    AND lc.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes"   +
				"    AND tc.dc_fecha = (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)"   +
				" 	 AND d.ic_clasificacion = clas.ic_clasificacion (+) "+
//				"    AND pep.cs_habilitado = 'S'"   +
				"    AND pep.ic_producto_nafin = 4"   +
				"    AND pn.ic_producto_nafin = 4"   +
				"    AND d.ic_epo = "+ic_epo+" "+condicion.toString()+" ");
				
			qryCCC.append(
				" SELECT /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */"   +
				"       distinct py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse,"   +
				"        TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision,"   +
				"        TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga,"   +
				"        TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc,"   +
				"        d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto,"   +
				"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', '','P', 'Dolar-Peso','') AS tipo_conversion,"   +
				"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio,"   +
				"        NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento,"   +
				"        NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento,"   +
				"        tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus,"   +
				"        m.ic_moneda, d.ic_documento,	"+
				"			decode(ed.ic_estatus_docto,32,i2.cg_razon_social,i.cg_razon_social) AS NOMBRE_IF,"   +
				"        DECODE (pep.cg_tipo_credito,'D', 'Modalidad 1 (Riesgo Empresa de Primer Orden)','Modalidad 2 (Riesgo Distribuidor)') AS tipo_credito,"   +
				"        TO_CHAR (ds.df_fecha_seleccion, 'DD/MM/YYYY') AS df_operacion_credito,"   +
				"        d.ig_plazo_credito AS plazo_credito,"   +
				"        TO_CHAR (d.df_fecha_venc_credito, 'DD/MM/YYYY') AS df_venc_credito,"   +
				"        ct.cd_nombre || ' ' || ds.cg_rel_mat || ' ' || ds.fn_puntos AS ref_tasa,"   +
				"        tci.cd_descripcion AS tipo_cobro_int,"   +
				"        DECODE (ds.cg_rel_mat,'+', ds.fn_valor_tasa + ds.fn_puntos,'-', ds.fn_valor_tasa - ds.fn_puntos,'*', ds.fn_valor_tasa * ds.fn_puntos,"   +
				"           '/', ds.fn_valor_tasa / ds.fn_puntos,0) AS fn_valor_tasa,"   +
				"        ds.fn_importe_interes, lc.ic_moneda AS moneda_linea,clas.cg_descripcion as CATEGORIA,'Cons2InfDocEpoDist::getDocumentSummaryQueryForIds' "   +
				"      ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				
				
				"	,d.cg_ventacartera AS cg_ventacartera, "+
				//"	bins.FN_COMISION  as comision_aplicable,"+
				"	ds.FN_PORC_COMISION_APLI  as comision_aplicable,"+
				"	(((ds.fn_importe_recibir * ds.fn_porc_comision_apli) / 100)+((ds.fn_importe_recibir * ds.fn_porc_comision_apli) / 100)  * (SELECT iva.fn_porcentaje_iva FROM comcat_iva iva WHERE iva.ic_iva = (SELECT MAX (ic_iva) FROM comcat_iva)) / 100 ) AS monto_comision,"+
				"	(ds.fn_importe_recibir - ((((ds.fn_importe_recibir * ds.fn_porc_comision_apli) / 100)+((ds.fn_importe_recibir * ds.fn_porc_comision_apli) / 100)  * (SELECT iva.fn_porcentaje_iva FROM comcat_iva iva WHERE iva.ic_iva = (SELECT MAX (ic_iva) FROM comcat_iva)) / 100 )) ) AS monto_depositar,	"+
				"	bins.CG_CODIGO_BIN||' '||bins.CG_DESCRIPCION as bins, "+
				"	trim(cpi.CG_OPERA_TARJETA) as Opera_tarjeta	"+
			
				"	,d.IC_ORDEN_PAGO_TC as id_orden_enviado	"+
				"	,ptc.CG_TRANSACCION_ID as id_operacion	"+
				"	,CONCAT(ptc.CG_CODIGO_RESP ,ptc.CG_CODIGO_DESC)AS codigo_autorizacion"	+
				//"	,ptc.CG_CODIGO_AUTORIZACION as codigo_autorizacion	"+
				"	,'' as fecha_registro	"+
				"	,SUBSTR(ptc.CG_NUM_TARJETA,-4,4) as NUM_TC	"+
				"  , D.IG_TIPO_PAGO " +
				"        ,(SELECT CASE WHEN COUNT(*) > 0 THEN 'S' ELSE 'N' END FROM COM_ARCDOCTOS WHERE CC_ACUSE = d.cc_acuse) AS MUESTRA_VISOR " +
				"   FROM dis_documento d,"   +
				"        dis_docto_seleccionado ds,"   +
				"        comcat_pyme py,"   +
				"        comcat_if i,"   +
				"			comcat_if i2,"+
				"        comrel_producto_epo pe,"   +
				"        comrel_pyme_epo_x_producto pep,"   +
				"        com_linea_credito lc,"   +
				"        comcat_tasa ct,"   +
				"        comcat_moneda m,"   +
				"        comcat_estatus_docto ed,"   +
				"        com_tipo_cambio tc,"   +
				"        comcat_producto_nafin pn,"   +
				"        comcat_tipo_financiamiento tf,"   +
				"        comcat_tipo_cobro_interes tci,comrel_clasificacion clas "+
				
				"	,com_bins_if bins, "+
				"	comrel_producto_if cpi	"+
				"	,DIS_DOCTOS_PAGO_TC ptc "+
				
				"  WHERE d.ic_epo = pep.ic_epo"   +
				"    AND d.ic_pyme = py.ic_pyme"   +
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento"   +
				"    AND d.ic_estatus_docto = ed.ic_estatus_docto"   +
				"    AND d.ic_linea_credito = lc.ic_linea_credito"   +
				"    AND d.ic_documento = ds.ic_documento"   +
				"    AND ds.ic_tasa = ct.ic_tasa"   +
				"    AND pn.ic_producto_nafin = pe.ic_producto_nafin"   +
				"    AND m.ic_moneda = tc.ic_moneda"   +
				"    AND lc.ic_if = i.ic_if"   +
				
				"	AND d.IC_BINS = bins.IC_BINS(+)"+
				"  AND bins.IC_if = i2.IC_if(+) "+
				
				 "	and lc.IC_IF = cpi.ic_if "+
				 "	and pep.IC_PRODUCTO_NAFIN = cpi.IC_PRODUCTO_NAFIN "+
				 "	and d.IC_ORDEN_PAGO_TC  =ptc.IC_ORDEN_PAGO_TC (+) "+
				
				"    AND pep.ic_pyme = py.ic_pyme"   +
				"    AND lc.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes"   +
				"    AND tc.dc_fecha = (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)"   +
				" 	 AND D.ic_clasificacion = clas.ic_clasificacion (+) "+
//				"    AND pep.cs_habilitado = 'S'"   +
				"    AND pep.ic_producto_nafin = 4"   +
				"    AND pn.ic_producto_nafin = 4"  +
				"    AND d.ic_epo = "+ic_epo+" "+condicion.toString()+" ");
				
			qryFdR.append(
				" SELECT /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */"   +
				"       distinct py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse,"   +
				"        TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision,"   +
				"        TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga,"   +
				"        TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc,"   +
				"        d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto,"   +
				"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', '','P', 'Dolar-Peso','') AS tipo_conversion,"   +
				"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio,"   +
				"        NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento,"   +
				"        NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento,"   +
				"        'NA' AS modo_plazo, ed.cd_descripcion AS estatus,"   +
				"        m.ic_moneda, d.ic_documento, i.cg_razon_social AS nombre_if,"   +
				"        DECODE (pe.cg_tipos_credito,'F', 'Factoraje con Recurso') AS tipo_credito,"   +
				"        TO_CHAR (ds.df_fecha_seleccion, 'DD/MM/YYYY') AS df_operacion_credito,"   +
				"        d.ig_plazo_credito AS plazo_credito,"   +
				"        TO_CHAR (d.df_fecha_venc_credito, 'DD/MM/YYYY') AS df_venc_credito,"   +
				"        ct.cd_nombre || ' ' || ds.cg_rel_mat || ' ' || ds.fn_puntos AS ref_tasa,"   +
				"        tci.cd_descripcion AS tipo_cobro_int,"   +
				"        DECODE (ds.cg_rel_mat,'+', ds.fn_valor_tasa + ds.fn_puntos,'-', ds.fn_valor_tasa - ds.fn_puntos,'*', ds.fn_valor_tasa * ds.fn_puntos,"   +
				"           '/', ds.fn_valor_tasa / ds.fn_puntos,0) AS fn_valor_tasa,"   +
				"        ds.fn_importe_interes, lc.ic_moneda AS moneda_linea,clas.cg_descripcion as CATEGORIA,'Cons2InfDocEpoDist::getDocumentSummaryQueryForIds' "   +
				"      ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				" 		,lc.ig_cuenta_bancaria as CUENTA_BANCARIA "+ 
				
				/*
				"	,d.cg_ventacartera AS cg_ventacartera, "+
				"	bins.FN_COMISION  as comision_aplicable,"+
				"	((d.fn_monto*ds.FN_PORC_COMISION_APLI)/100) as monto_comision,"+
				"	(d.fn_monto-((d.fn_monto*ds.FN_PORC_COMISION_APLI)/100)) as monto_depositar,"+
				"	bins.CG_DESCRIPCION as bins, "+
				"	trim(cpi.CG_OPERA_TARJETA) as Opera_tarjeta	"+
			
				"	,d.IC_ORDEN_PAGO_TC as id_orden_enviado	"+
				"	,ptc.CG_TRANSACCION_ID as id_operacion	"+
				"	,CONCAT(ptc.CG_CODIGO_RESP ,ptc.CG_CODIGO_DESC)AS codigo_autorizacion"	+
				//"	,ptc.CG_CODIGO_AUTORIZACION as codigo_autorizacion	"+
				"	,DF_FECHA_AUTORIZACION as fecha_registro	"+
				*/
				"	,'' AS cg_ventacartera, "+
				"	0  as comision_aplicable,"+
				"	0 as monto_comision,"+
				"	0 as monto_depositar,"+
				"	'' as bins, "+
				"	'' as Opera_tarjeta	"+
			
				"	,'' as id_orden_enviado	"+
				"	,'' as id_operacion	"+
				"	,'' AS codigo_autorizacion"	+
				"	,'' as fecha_registro	"+
				"	,'' as NUM_TC	"+
				"  , D.IG_TIPO_PAGO " +
				"        ,(SELECT CASE WHEN COUNT(*) > 0 THEN 'S' ELSE 'N' END FROM COM_ARCDOCTOS WHERE CC_ACUSE = d.cc_acuse) AS MUESTRA_VISOR " +
				"   FROM dis_documento d,"   +
				"        dis_docto_seleccionado ds,"   +
				"        comcat_pyme py,"   +
				"        comcat_if i,"   +
				"			comcat_if i2,"+
				"        comrel_producto_epo pe,"   +
				"        comrel_pyme_epo_x_producto pep,"   +
				"        com_linea_credito lc,"   +
				"        comcat_tasa ct,"   +
				"        comcat_moneda m,"   +
				"        comcat_estatus_docto ed,"   +
				"        com_tipo_cambio tc,"   +
				"        comcat_producto_nafin pn,"   +
				"        comcat_tipo_financiamiento tf,"   +
				"        comcat_tipo_cobro_interes tci,comrel_clasificacion clas "+
				/*
				"	,com_bins_if bins, "+
				"	comrel_producto_if cpi	"+
				"	,DIS_DOCTOS_PAGO_TC ptc "+
				*/
				"  WHERE d.ic_epo = pep.ic_epo"   +
				"    AND d.ic_pyme = py.ic_pyme"   +
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento(+)"   +
				"    AND d.ic_estatus_docto = ed.ic_estatus_docto"   +
				"    AND d.ic_linea_credito = lc.ic_linea_credito"   +
				"    AND d.ic_documento = ds.ic_documento"   +
				"    AND ds.ic_tasa = ct.ic_tasa"   +
				"    AND pn.ic_producto_nafin = pe.ic_producto_nafin"   +
				"    AND m.ic_moneda = tc.ic_moneda"   +
				"    AND lc.ic_if = i.ic_if"   +
				/*
				"	AND d.IC_BINS = bins.IC_BINS(+)"+
				"  AND bins.IC_if = i2.IC_if(+) "+
				
				 "	and lc.IC_IF = cpi.ic_if "+
				 "	and pep.IC_PRODUCTO_NAFIN = cpi.IC_PRODUCTO_NAFIN "+
				 "	and d.IC_ORDEN_PAGO_TC  =ptc.IC_ORDEN_PAGO_TC (+) "+
				*/
				"    AND pep.ic_pyme = py.ic_pyme"   +
				"    AND lc.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes"   +
				"    AND tc.dc_fecha = (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)"   +
				" 	 AND D.ic_clasificacion = clas.ic_clasificacion (+) "+
				"   and d.IC_TIPO_FINANCIAMIENTO is null "+
				"   AND pep.ic_producto_nafin = 4"   +
				"   AND pn.ic_producto_nafin = 4"  +
				"   and d.IC_TIPO_FINANCIAMIENTO is null "+ 
				"	 AND lc.cg_tipo_solicitud = 'I' "+
				"	  AND lc.cs_factoraje_con_rec = 'S' "+
				"   AND d.ic_epo = "+ic_epo+" "+condicion.toString()+" ");
				

			if("D".equals(tipo_credito))
				qrySentencia.append(qryDM.toString());
			else if("C".equals(tipo_credito))
				qrySentencia.append(qryCCC.toString()); 
			else if("F".equals(tipo_credito))
				qrySentencia.append(qryFdR.toString());
			else
				qrySentencia.append(qryDM.toString()+ " UNION ALL "+qryCCC.toString());
	
			System.out.println("el query queda de la siguiente manera "+tipo_credito+"  "+qrySentencia.toString());
			return qrySentencia.toString();
	}
	
	public String getDocumentQuery(HttpServletRequest request) {

		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
    	StringBuffer qrySentencia	= new StringBuffer();
    	StringBuffer qryDM			= new StringBuffer();
    	StringBuffer qryCCC			= new StringBuffer();
		StringBuffer qryFdR			= new StringBuffer();

    	String condicion = "";
      
		String	ic_epo				=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String	ic_pyme				=	(request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String	ic_if				=	(request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String	ic_estatus_docto	=	(request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto");
		String 	ig_numero_docto		=	(request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String 	fecha_seleccion_de	=	(request.getParameter("fecha_seleccion_de")==null)?"":request.getParameter("fecha_seleccion_de");
		String 	fecha_seleccion_a	=	(request.getParameter("fecha_seleccion_a")==null)?"":request.getParameter("fecha_seleccion_a");
		String 	cc_acuse			=	(request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String 	monto_credito_de	=	(request.getParameter("monto_credito_de")==null)?"":request.getParameter("monto_credito_de");
		String 	monto_credito_a		=	(request.getParameter("monto_credito_a")==null)?"":request.getParameter("monto_credito_a");
		String 	fecha_emision_de	=	(request.getParameter("fecha_emision_de")==null)?"":request.getParameter("fecha_emision_de");
		String 	fecha_emision_a		=	(request.getParameter("fecha_emision_a")==null)?"":request.getParameter("fecha_emision_a");
		String 	fecha_vto_de		=	(request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String 	fecha_vto_a			=	(request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String 	fecha_vto_credito_de=	(request.getParameter("fecha_vto_credito_de")==null)?"":request.getParameter("fecha_vto_credito_de");
		String 	fecha_vto_credito_a =	(request.getParameter("fecha_vto_credito_a")==null)?"":request.getParameter("fecha_vto_credito_a");
		String 	ic_tipo_cobro_int	=	(request.getParameter("ic_tipo_cobro_int")==null)?"":request.getParameter("ic_tipo_cobro_int");
		String	ic_moneda			=	(request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String 	fn_monto_de			=	(request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String 	fn_monto_a			=	(request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String 	monto_con_descuento	=	(request.getParameter("monto_con_descuento")==null)?"":"checked";
		String	solo_cambio			=	(request.getParameter("solo_cambio")==null)?"":request.getParameter("solo_cambio");
		String	modo_plazo			=	(request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
		String	tipo_credito		=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String	ic_documento		=	(request.getParameter("ic_documento")==null)?"":request.getParameter("ic_documento");
		String 	fecha_publicacion_de=	(request.getParameter("fecha_publicacion_de")==null)?fechaHoy:request.getParameter("fecha_publicacion_de");
		String 	fecha_publicacion_a	=	(request.getParameter("fecha_publicacion_a")==null)?fechaHoy:request.getParameter("fecha_publicacion_a");
		String 	NOnegociable			=	(request.getParameter("NOnegociable")==null)?"":request.getParameter("NOnegociable");//Fodea 029-2010 Distribuidores Fase III
		String tipo_pago = (request.getParameter("tipo_pago")==null)?"":request.getParameter("tipo_pago"); // F009-2015

		try {
			if(!"".equals(ic_pyme)&&ic_pyme!=null)
				condicion += " AND D.IC_PYME = "+ic_pyme;
	   	if("4".equals(ic_estatus_docto)||"20".equals(ic_estatus_docto)||"22".equals(ic_estatus_docto)||"3".equals(ic_estatus_docto)||"11".equals(ic_estatus_docto) ||"1".equals(ic_estatus_docto) ||"24".equals(ic_estatus_docto)||"32".equals(ic_estatus_docto) ||"28".equals(ic_estatus_docto)||"30".equals(ic_estatus_docto))//MOD +( ||"24".equals(ic_estatus_docto))				
				condicion += " AND D.IC_ESTATUS_DOCTO ="+ic_estatus_docto;
			if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
				condicion += " AND D.ig_numero_docto = '"+ig_numero_docto+"'";
			if(!"".equals(fecha_seleccion_de)&& fecha_seleccion_de!=null&&!"".equals(fecha_seleccion_a)&& fecha_seleccion_a!=null)
				condicion += " AND TRUNC(DS.df_fecha_seleccion) BETWEEN TRUNC(to_date('"+fecha_seleccion_de+"','dd/mm/yyyy')) AND TRUNC(to_date('"+fecha_seleccion_a+"','dd/mm/yyyy'))";
			if(!"".equals(cc_acuse)&& cc_acuse!=null)
				condicion += " AND D.cc_acuse = '"+cc_acuse+"'";
			if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null)
				condicion += " AND TRUNC(D.df_fecha_emision) BETWEEN TRUNC(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) AND TRUNC(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))";
			if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
				condicion += " AND TRUNC(D.df_fecha_venc) BETWEEN TRUNC(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) AND TRUNC(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))";
			if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null)
				condicion += " AND TRUNC(D.df_fecha_venc_credito) BETWEEN TRUNC(to_date('"+fecha_vto_credito_de+"','dd/mm/yyyy')) AND TRUNC(to_date('"+fecha_vto_credito_a+"','dd/mm/yyyy'))";		
			if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null)
				condicion += " AND LC.ic_tipo_cobro_interes = "+ic_tipo_cobro_int;
			if(!"".equals(ic_moneda)&&ic_moneda!=null)
				condicion += " AND D.ic_moneda = "+ic_moneda;
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
				condicion += " AND (D.fn_monto*(fn_porc_descuento/100)) BETWEEN "+fn_monto_de+" AND "+fn_monto_a;
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
				condicion += " AND D.fn_monto between "+fn_monto_de+" AND "+fn_monto_a;
			if(!"".equals(solo_cambio)&& solo_cambio!=null)
				//condicion += " and D.ic_documento in (select ic_documento from dis_cambio_estatus)";
				condicion += " AND D.ic_documento IN (SELECT ic_documento FROM dis_cambio_estatus WHERE ic_cambio_estatus = 8)";
			if(!"".equals(modo_plazo)&& modo_plazo!=null)
				condicion += " AND D.ic_tipo_financiamiento = "+modo_plazo;
			if(!"".equals(ic_documento)&&ic_documento!=null)
				condicion += " AND D.IC_DOCUMENTO = "+ic_documento;
			if(!"".equals(ic_if)&&ic_if!=null)
				condicion += " AND LC.IC_IF = "+ic_if;
			if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
				condicion += " AND TRUNC(D.df_carga) BETWEEN TRUNC(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) AND TRUNC(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))";
			if(!"".equals(monto_credito_de)&& monto_credito_de!=null&&!"".equals(monto_credito_a)&& monto_credito_a!=null)
				condicion += " AND (D.fn_monto-(fn_monto*(fn_porc_descuento/100))) BETWEEN "+monto_credito_de+" AND "+monto_credito_a;	
			
			if("".equals(ic_estatus_docto) && NOnegociable.equals("N")){ //Fodea 029-2010 Distribuidores Fase III
					condicion += " AND D.ic_estatus_docto IN (2,3,4,5,9,11,20,22,24,32,28,30)";//MOD +(24)
			}else 	if("".equals(ic_estatus_docto) && NOnegociable.equals("S")){
				condicion += " AND D.ic_estatus_docto IN (2,1,3,4,5,9,11,20,22,24,32,28,30)";//MOD +(24)
			}
			if(!"F".equals(tipo_credito)) {
				if(!"".equals(ic_estatus_docto)&&!"4".equals(ic_estatus_docto)&&!"20".equals(ic_estatus_docto)&&!"22".equals(ic_estatus_docto)&&!"3".equals(ic_estatus_docto)&&!"11".equals(ic_estatus_docto)&&!"1".equals(ic_estatus_docto)&&!"24".equals(ic_estatus_docto)&&!"32".equals(ic_estatus_docto)&&!"28".equals(ic_estatus_docto)&&!"30".equals(ic_estatus_docto))//MOD +(&&!"24".equals(ic_estatus_docto))//if("".equals(condicion))
					condicion += " AND D.ic_estatus_docto IS NULL";
			}
				
			if("F".equals(tipo_credito)) {
				//condicion +=" AND pep.CG_TIPO_CREDITO IS NULL";
			} else {
				condicion +=" AND pep.CG_TIPO_CREDITO IS NOT NULL";
			}
			if(!tipo_pago.equals("")){
				condicion += " AND D.IG_TIPO_PAGO = " + tipo_pago;
			}
			qryDM.append(
				" SELECT /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */"   +
				"       distinct d.ic_documento, 'Cons2InfDocEpoDist::getDocumentQuery' "   +
				"   FROM dis_documento d,"   +
				"        dis_docto_seleccionado ds,"   +
				"        comcat_pyme py,"   +
				"        comcat_if i,"   +
				"        comrel_producto_epo pe,"   +
				"        comrel_pyme_epo_x_producto pep,"   +
				"        dis_linea_credito_dm lc,"   +
				"        comcat_tasa ct,"   +
				"        comcat_moneda m,"   +
				"        comcat_estatus_docto ed,"   +
				"        com_tipo_cambio tc,"   +
				"        comcat_producto_nafin pn,"   +
				"        comcat_tipo_financiamiento tf,"   +
				"        comcat_tipo_cobro_interes tci"   +
				
				/*"	,com_bins_if bins "+
				"	,comrel_producto_if cpi	"+
				"	,DIS_DOCTOS_PAGO_TC ptc "+
				*/
				"  WHERE d.ic_epo = pep.ic_epo"   +
				"    AND d.ic_pyme = py.ic_pyme"   +
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento"   +
				"    AND d.ic_estatus_docto = ed.ic_estatus_docto"   +
				"    AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm (+)"   +
				"    AND d.ic_documento = ds.ic_documento"   +
				"    AND ds.ic_tasa = ct.ic_tasa"   +
				"    AND pn.ic_producto_nafin = pe.ic_producto_nafin"   +
				"    AND m.ic_moneda = tc.ic_moneda"   +
				"    AND lc.ic_if = i.ic_if"   +

			/*	 "	and lc.IC_IF = bins.IC_IF "+
				 "	and lc.IC_IF = cpi.ic_if "+
				 "	and pep.IC_PRODUCTO_NAFIN = cpi.IC_PRODUCTO_NAFIN "+
				 "	and d.IC_ORDEN_PAGO_TC  =ptc.IC_ORDEN_PAGO_TC (+) "+
				*/
				
				"    AND pep.ic_pyme = py.ic_pyme"   +
				"    AND lc.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes"   +
				"    AND tc.dc_fecha = (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = d.ic_moneda)"   +
//				"    AND pep.cs_habilitado = 'S'"   +
				"    AND d.ic_producto_nafin = 4"   +
				"    AND pep.ic_producto_nafin = 4"   +
				"    AND pn.ic_producto_nafin = 4"   +
				"    AND d.ic_epo = "+ic_epo+" "+condicion+" ");
				
			qryCCC.append(
				" SELECT /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */"   +
				"      distinct  d.ic_documento, 'Cons2InfDocEpoDist::getDocumentQuery' "   +
				"   FROM dis_documento d,"   +
				"        dis_docto_seleccionado ds,"   +
				"        comcat_pyme py,"   +
				"        comcat_if i,"   +
				"        comrel_producto_epo pe,"   +
				"        comrel_pyme_epo_x_producto pep,"   +
				"        com_linea_credito lc,"   +
				"        comcat_tasa ct,"   +
				"        comcat_moneda m,"   +
				"        comcat_estatus_docto ed,"   +
				"        com_tipo_cambio tc,"   +
				"        comcat_producto_nafin pn,"   +
				"        comcat_tipo_financiamiento tf,"   +
				"        comcat_tipo_cobro_interes tci"   +
				
				"	,com_bins_if bins "+
				"	,comrel_producto_if cpi	"+
				"	,DIS_DOCTOS_PAGO_TC ptc "+
				
				"  WHERE d.ic_epo = pep.ic_epo"   +
				"    AND d.ic_pyme = py.ic_pyme"   +
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento"   +
				"    AND d.ic_estatus_docto = ed.ic_estatus_docto"   +
				"    AND d.ic_linea_credito = lc.ic_linea_credito"   +
				"    AND d.ic_documento = ds.ic_documento"   +
				"    AND ds.ic_tasa = ct.ic_tasa"   +
				"    AND pn.ic_producto_nafin = pe.ic_producto_nafin"   +
				"    AND m.ic_moneda = tc.ic_moneda"   +
				"    AND lc.ic_if = i.ic_if"   +
				
				"	and d.IC_BINS = bins.IC_BINS(+) "+
				 "	and lc.IC_IF = cpi.ic_if "+
				 "	and pep.IC_PRODUCTO_NAFIN = cpi.IC_PRODUCTO_NAFIN "+
				 "	and d.IC_ORDEN_PAGO_TC  =ptc.IC_ORDEN_PAGO_TC (+) "+
				
				"    AND pep.ic_pyme = py.ic_pyme"   +
				"    AND lc.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes"   +
				"    AND tc.dc_fecha = (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = d.ic_moneda)"   +
//				"    AND pep.cs_habilitado = 'S'"   +
				"    AND d.ic_producto_nafin = 4"   +
				"    AND pep.ic_producto_nafin = 4"   +
				"    AND pn.ic_producto_nafin = 4"  +
				"    AND d.ic_epo = "+ic_epo+" "+condicion+" ");
				
			qryFdR.append(
				" SELECT /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */"   +
				"      distinct  d.ic_documento, 'Cons2InfDocEpoDist::getDocumentQuery' "   +
				"   FROM dis_documento d,"   +
				"        dis_docto_seleccionado ds,"   +
				"        comcat_pyme py,"   +
				"        comcat_if i,"   +
				"        comrel_producto_epo pe,"   +
				"        comrel_pyme_epo_x_producto pep,"   +
				"        com_linea_credito lc,"   +
				"        comcat_tasa ct,"   +
				"        comcat_moneda m,"   +
				"        comcat_estatus_docto ed,"   +
				"        com_tipo_cambio tc,"   +
				"        comcat_producto_nafin pn,"   +
				"        comcat_tipo_financiamiento tf,"   +
				"        comcat_tipo_cobro_interes tci"   +
				
				/*"	,com_bins_if bins "+
				"	,comrel_producto_if cpi	"+
				"	,DIS_DOCTOS_PAGO_TC ptc "+
				*/
				"  WHERE d.ic_epo = pep.ic_epo"   +
				"    AND d.ic_pyme = py.ic_pyme"   +
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento (+)"   +
				"    AND d.ic_estatus_docto = ed.ic_estatus_docto"   +
				"    AND d.ic_linea_credito = lc.ic_linea_credito"   +
				"    AND d.ic_documento = ds.ic_documento"   +
				"    AND ds.ic_tasa = ct.ic_tasa"   +
				"    AND pn.ic_producto_nafin = pe.ic_producto_nafin"   +
				"    AND m.ic_moneda = tc.ic_moneda"   +
				"    AND lc.ic_if = i.ic_if"   +
				
				/* "	and lc.IC_IF = bins.IC_IF "+
				 "	and lc.IC_IF = cpi.ic_if "+
				 "	and pep.IC_PRODUCTO_NAFIN = cpi.IC_PRODUCTO_NAFIN "+
				 "	and d.IC_ORDEN_PAGO_TC  =ptc.IC_ORDEN_PAGO_TC (+) "+
				*/
				"    AND pep.ic_pyme = py.ic_pyme"   +
				"    AND lc.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes"   +
				"    AND tc.dc_fecha = (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = d.ic_moneda)"   +
				"    AND d.ic_producto_nafin = 4"   +
				"    AND pep.ic_producto_nafin = 4"   +
				"    AND pn.ic_producto_nafin = 4"  +
				" and d.IC_TIPO_FINANCIAMIENTO is null "+
				"	 AND lc.cg_tipo_solicitud = 'I' "+
				"	  AND lc.cs_factoraje_con_rec = 'S' "+
				"    AND d.ic_epo = "+ic_epo+" "+condicion.toString()+" ");


			if("D".equals(tipo_credito))
				qrySentencia.append(qryDM.toString());
			else if("C".equals(tipo_credito))
				qrySentencia.append(qryCCC.toString());
			else if("F".equals(tipo_credito))
				qrySentencia.append(qryFdR.toString()); 
			else
				qrySentencia.append(qryDM.toString()+ " UNION ALL "+qryCCC.toString());

			System.out.println("EL query de la llave primaria********: "+qrySentencia.toString());
		}catch(Exception e){
			System.out.println("Cons2InfDocEpoDist::getDocumentQueryException "+e);
		}
		return qrySentencia.toString();
	}

	public String getDocumentQueryFile(HttpServletRequest request) {

		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
    	StringBuffer qrySentencia	= new StringBuffer();
    	StringBuffer qryDM			= new StringBuffer();
    	StringBuffer qryCCC			= new StringBuffer();
		StringBuffer qryFdR			= new StringBuffer();

    	String condicion = "";
      
		String	ic_epo				=	(request.getParameter("ic_epo")==null)?"":request.getParameter("ic_epo");
		String	ic_pyme				=	(request.getParameter("ic_pyme")==null)?"":request.getParameter("ic_pyme");
		String	ic_if				=	(request.getParameter("ic_if")==null)?"":request.getParameter("ic_if");
		String	ic_estatus_docto	=	(request.getParameter("ic_estatus_docto")==null)?"":request.getParameter("ic_estatus_docto");
		String 	ig_numero_docto		=	(request.getParameter("ig_numero_docto")==null)?"":request.getParameter("ig_numero_docto");
		String 	fecha_seleccion_de	=	(request.getParameter("fecha_seleccion_de")==null)?"":request.getParameter("fecha_seleccion_de");
		String 	fecha_seleccion_a	=	(request.getParameter("fecha_seleccion_a")==null)?"":request.getParameter("fecha_seleccion_a");
		String 	cc_acuse			=	(request.getParameter("cc_acuse")==null)?"":request.getParameter("cc_acuse");
		String 	monto_credito_de	=	(request.getParameter("monto_credito_de")==null)?"":request.getParameter("monto_credito_de");
		String 	monto_credito_a		=	(request.getParameter("monto_credito_a")==null)?"":request.getParameter("monto_credito_a");
		String 	fecha_emision_de	=	(request.getParameter("fecha_emision_de")==null)?"":request.getParameter("fecha_emision_de");
		String 	fecha_emision_a		=	(request.getParameter("fecha_emision_a")==null)?"":request.getParameter("fecha_emision_a");
		String 	fecha_vto_de		=	(request.getParameter("fecha_vto_de")==null)?"":request.getParameter("fecha_vto_de");
		String 	fecha_vto_a			=	(request.getParameter("fecha_vto_a")==null)?"":request.getParameter("fecha_vto_a");
		String 	fecha_vto_credito_de=	(request.getParameter("fecha_vto_credito_de")==null)?"":request.getParameter("fecha_vto_credito_de");
		String 	fecha_vto_credito_a =	(request.getParameter("fecha_vto_credito_a")==null)?"":request.getParameter("fecha_vto_credito_a");
		String 	ic_tipo_cobro_int	=	(request.getParameter("ic_tipo_cobro_int")==null)?"":request.getParameter("ic_tipo_cobro_int");
		String	ic_moneda			=	(request.getParameter("ic_moneda")==null)?"":request.getParameter("ic_moneda");
		String 	fn_monto_de			=	(request.getParameter("fn_monto_de")==null)?"":request.getParameter("fn_monto_de");
		String 	fn_monto_a			=	(request.getParameter("fn_monto_a")==null)?"":request.getParameter("fn_monto_a");
		String 	monto_con_descuento	=	(request.getParameter("monto_con_descuento")==null)?"":"checked";
		String	solo_cambio			=	(request.getParameter("solo_cambio")==null)?"":request.getParameter("solo_cambio");
		String	modo_plazo			=	(request.getParameter("modo_plazo")==null)?"":request.getParameter("modo_plazo");
		String	tipo_credito		=	(request.getParameter("tipo_credito")==null)?"":request.getParameter("tipo_credito");
		String	ic_documento		=	(request.getParameter("ic_documento")==null)?"":request.getParameter("ic_documento");
		String 	fecha_publicacion_de=	(request.getParameter("fecha_publicacion_de")==null)?fechaHoy:request.getParameter("fecha_publicacion_de");
		String 	fecha_publicacion_a	=	(request.getParameter("fecha_publicacion_a")==null)?fechaHoy:request.getParameter("fecha_publicacion_a");
		String 	NOnegociable			=	(request.getParameter("NOnegociable")==null)?"":request.getParameter("NOnegociable");//Fodea 029-2010 Distribuidores Fase III
		String tipo_pago = (request.getParameter("tipo_pago")==null)?"":request.getParameter("tipo_pago"); // F009-2015

		try {
			if(!"".equals(ic_pyme)&&ic_pyme!=null)
				condicion += " AND D.IC_PYME = "+ic_pyme;
		 	if("4".equals(ic_estatus_docto)||"20".equals(ic_estatus_docto)||"22".equals(ic_estatus_docto)||"3".equals(ic_estatus_docto)||"11".equals(ic_estatus_docto) ||"1".equals(ic_estatus_docto) ||"24".equals(ic_estatus_docto) ||"32".equals(ic_estatus_docto) ||"28".equals(ic_estatus_docto) ||"30".equals(ic_estatus_docto))//MOD +( ||"24".equals(ic_estatus_docto))				
				condicion += " AND D.IC_ESTATUS_DOCTO ="+ic_estatus_docto;
			if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null)
				condicion += " and D.ig_numero_docto = '"+ig_numero_docto+"'";
			if(!"".equals(fecha_seleccion_de)&& fecha_seleccion_de!=null&&!"".equals(fecha_seleccion_a)&& fecha_seleccion_a!=null)
				condicion += " and trunc(DS.df_fecha_seleccion) between trunc(to_date('"+fecha_seleccion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_seleccion_a+"','dd/mm/yyyy'))";
			if(!"".equals(cc_acuse)&& cc_acuse!=null)
				condicion += " and D.cc_acuse = '"+cc_acuse+"'";
			if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null)
				condicion += " and trunc(D.df_fecha_emision) between trunc(to_date('"+fecha_emision_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_emision_a+"','dd/mm/yyyy'))";
			if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null)
				condicion += " and trunc(D.df_fecha_venc) between trunc(to_date('"+fecha_vto_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_a+"','dd/mm/yyyy'))";
			if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null)
				condicion += " and trunc(D.df_fecha_venc_credito) between trunc(to_date('"+fecha_vto_credito_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_vto_credito_a+"','dd/mm/yyyy'))";		
			if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null)
				condicion += " AND LC.ic_tipo_cobro_interes = "+ic_tipo_cobro_int;
			if(!"".equals(ic_moneda)&&ic_moneda!=null)
				condicion += " and M.ic_moneda = "+ic_moneda;
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento))
				condicion += " and (D.fn_monto*(fn_porc_descuento/100)) between "+fn_monto_de+" and "+fn_monto_a;
			if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento))
				condicion += " and D.fn_monto between "+fn_monto_de+" and "+fn_monto_a;
			if(!"".equals(solo_cambio)&& solo_cambio!=null)
				condicion += " and D.ic_documento in (select ic_documento from dis_cambio_estatus)";
			if(!"".equals(modo_plazo)&& modo_plazo!=null)
				condicion += " and TF.ic_tipo_financiamiento = "+modo_plazo;
			if(!"".equals(ic_documento)&&ic_documento!=null)
				condicion += " AND D.IC_DOCUMENTO = "+ic_documento;
			if(!"".equals(ic_if)&&ic_if!=null)
				condicion += " AND LC.IC_IF = "+ic_if;
			if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null)
				condicion += " and trunc(D.df_carga) between trunc(to_date('"+fecha_publicacion_de+"','dd/mm/yyyy')) and trunc(to_date('"+fecha_publicacion_a+"','dd/mm/yyyy'))";
			if(!"".equals(monto_credito_de)&& monto_credito_de!=null&&!"".equals(monto_credito_a)&& monto_credito_a!=null)
				condicion += " and (D.fn_monto-(fn_monto*(fn_porc_descuento/100))) between "+monto_credito_de+" and "+monto_credito_a;	
		 if("".equals(ic_estatus_docto) && NOnegociable.equals("N")){ //Fodea 029-2010 Distribuidores Fase III
				condicion += " and D.ic_estatus_docto in (2,3,4,5,9,11,20,22,32,28,30)";
      }else 	if("".equals(ic_estatus_docto) && NOnegociable.equals("S")){
      	condicion += " and D.ic_estatus_docto in (2,1,3,4,5,9,11,20,22,32,28,30)";
      }
		if(!"F".equals(tipo_credito)) {
			if(!"".equals(ic_estatus_docto)&&!"4".equals(ic_estatus_docto)&&!"20".equals(ic_estatus_docto)&&!"22".equals(ic_estatus_docto)&&!"3".equals(ic_estatus_docto)&&!"11".equals(ic_estatus_docto)&&!"1".equals(ic_estatus_docto)&&!"24".equals(ic_estatus_docto)&&!"32".equals(ic_estatus_docto)&&!"28".equals(ic_estatus_docto)&&!"30".equals(ic_estatus_docto))//MOD +(&&!"24".equals(ic_estatus_docto))//if("".equals(condicion))
				condicion += " and D.ic_estatus_docto is null";
		}
			if("F".equals(tipo_credito)) {
				//condicion +=" AND pep.CG_TIPO_CREDITO IS NULL";
			} else {
				condicion +=" AND pep.CG_TIPO_CREDITO IS NOT NULL";
			}
			if(!tipo_pago.equals("")){
				condicion += " AND D.IG_TIPO_PAGO = " + tipo_pago;
			}
	qryDM.append(
				" SELECT /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */"   +
				"        distinct py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse,"   +
				"        TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision,"   +
				"        TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga,"   +
				"        TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc,"   +
				"        d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto,"   +
				"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', '','P', 'Dolar-Peso','') AS tipo_conversion,"   +
				"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio,"   +
				"        NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento,"   +
				"        NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento,"   +
				"        tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus,"   +
				"        m.ic_moneda, d.ic_documento, i.cg_razon_social AS nombre_if,"   +
				"        DECODE (pep.cg_tipo_credito,'D', 'Modalidad 1 (Riesgo Empresa de Primer Orden)','Modalidad 2 (Riesgo Distribuidor) ') AS tipo_credito,"   +
				"        TO_CHAR (ds.df_fecha_seleccion, 'DD/MM/YYYY') AS df_operacion_credito,"   +
				"        d.ig_plazo_credito AS plazo_credito,"   +
				"        TO_CHAR (d.df_fecha_venc_credito, 'DD/MM/YYYY') AS df_venc_credito,"   +
				"			DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) AS REF_TASA," +
				"        tci.cd_descripcion AS tipo_cobro_int,"   +
				"        ds.fn_valor_tasa,"   +
				"        ds.fn_importe_interes, lc.ic_moneda AS moneda_linea,clas.cg_descripcion as CATEGORIA,'Cons2InfDocEpoDist::getDocumentQueryFile' "   +
				"      ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				
			
				"	,'' AS cg_ventacartera, "+
				"	0  as comision_aplicable,"+
				"	0 as monto_comision,"+
				"	0 as monto_depositar,"+
				"	'' as bins, "+
				"	'' as Opera_tarjeta	"+
			
				"	,'' as id_orden_enviado	"+
				"	,'' as id_operacion	"+
				"	,'' AS codigo_autorizacion"	+
				"	,'' as fecha_registro	"+
				"	,'' as NUM_TC	"+
				" , D.IG_TIPO_PAGO " + 
				"   FROM dis_documento d,"   +
				"        dis_docto_seleccionado ds,"   +
				"        comcat_pyme py,"   +
				"        comcat_if i,"   +
				"			comcat_if i2,"+
				"        comrel_producto_epo pe,"   +
				"        comrel_pyme_epo_x_producto pep,"   +
				"        dis_linea_credito_dm lc,"   +
				"        comcat_tasa ct,"   +
				"        comcat_moneda m,"   +
				"        comcat_estatus_docto ed,"   +
				"        com_tipo_cambio tc,"   +
				"        comcat_producto_nafin pn,"   +
				"        comcat_tipo_financiamiento tf,"   +
				"        comcat_tipo_cobro_interes tci,comrel_clasificacion clas "+
				/*
				"	,com_bins_if bins, "+
				"	comrel_producto_if cpi	"+
				"	,DIS_DOCTOS_PAGO_TC ptc "+
				*/
				"  WHERE d.ic_epo = pep.ic_epo"   +
				"    AND d.ic_pyme = py.ic_pyme"   +
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento"   +
				"    AND d.ic_estatus_docto = ed.ic_estatus_docto"   +
				"    AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm (+)"   +
				"    AND d.ic_documento = ds.ic_documento"   +
				"    AND ds.ic_tasa = ct.ic_tasa"   +
				"    AND pn.ic_producto_nafin = pe.ic_producto_nafin"   +
				"    AND m.ic_moneda = tc.ic_moneda"   +
				"    AND lc.ic_if = i.ic_if"   +
				/*
				"	AND d.IC_BINS = bins.IC_BINS(+)"+
				"  AND bins.IC_if = i2.IC_if(+) "+
				
				 "	and lc.IC_IF = cpi.ic_if "+
				 "	and pep.IC_PRODUCTO_NAFIN = cpi.IC_PRODUCTO_NAFIN "+
				 "	and d.IC_ORDEN_PAGO_TC  =ptc.IC_ORDEN_PAGO_TC (+) "+
				*/
				"    AND pep.ic_pyme = py.ic_pyme"   +
				"    AND lc.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes"   +
				"    AND tc.dc_fecha = (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)"   +
//				"    AND pep.cs_habilitado = 'S'"   +
				" 	 AND D.ic_clasificacion = clas.ic_clasificacion (+) "+
				"    AND pep.ic_producto_nafin = 4"   +
				"    AND pn.ic_producto_nafin = 4"   +
				"    AND d.ic_epo = "+ic_epo+" "+condicion+" ");
				
			qryCCC.append(
				" SELECT /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */"   +
				"        distinct py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse,"   +
				"        TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision,"   +
				"        TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga,"   +
				"        TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc,"   +
				"        d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto,"   +
				"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', '','P', 'Dolar-Peso','') AS tipo_conversion,"   +
				"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio,"   +
				"        NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento,"   +
				"        NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento,"   +
				"        tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus,"   +
				"        m.ic_moneda, d.ic_documento,	"+
				//"			, i.cg_razon_social AS nombre_if,"   +
				"			decode(ed.ic_estatus_docto,32,i2.cg_razon_social,i.cg_razon_social) AS NOMBRE_IF,"   +
				"        DECODE (pep.cg_tipo_credito,'D', 'Modalidad 1 (Riesgo Empresa de Primer Orden)','Modalidad 2 (Riesgo Distribuidor)') AS tipo_credito,"   +
				"        TO_CHAR (ds.df_fecha_seleccion, 'DD/MM/YYYY') AS df_operacion_credito,"   +
				"        d.ig_plazo_credito AS plazo_credito,"   +
				"        TO_CHAR (d.df_fecha_venc_credito, 'DD/MM/YYYY') AS df_venc_credito,"   +
				"        ct.cd_nombre || ' ' || ds.cg_rel_mat || ' ' || ds.fn_puntos AS ref_tasa,"   +
				"        tci.cd_descripcion AS tipo_cobro_int,"   +
				"        DECODE (ds.cg_rel_mat,'+', ds.fn_valor_tasa + ds.fn_puntos,'-', ds.fn_valor_tasa - ds.fn_puntos,'*', ds.fn_valor_tasa * ds.fn_puntos,"   +
				"           '/', ds.fn_valor_tasa / ds.fn_puntos,0) AS fn_valor_tasa,"   +
				"        ds.fn_importe_interes, lc.ic_moneda AS moneda_linea,clas.cg_descripcion as CATEGORIA,'Cons2InfDocEpoDist::getDocumentQueryFile' "   +
				"      ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				
				"	,d.cg_ventacartera AS cg_ventacartera, "+
				"	ds.FN_PORC_COMISION_APLI  as comision_aplicable,"+
				//"	((ds.fn_importe_recibir*ds.FN_PORC_COMISION_APLI)/100) as monto_comision,"+
				//"	(ds.fn_importe_recibir-((ds.fn_importe_recibir*ds.FN_PORC_COMISION_APLI)/100)) as monto_depositar,"+
				"	(((ds.fn_importe_recibir * ds.fn_porc_comision_apli) / 100)+((ds.fn_importe_recibir * ds.fn_porc_comision_apli) / 100)  * (SELECT iva.fn_porcentaje_iva FROM comcat_iva iva WHERE iva.ic_iva = (SELECT MAX (ic_iva) FROM comcat_iva)) / 100 ) AS monto_comision,"+
				"	(ds.fn_importe_recibir - ((((ds.fn_importe_recibir * ds.fn_porc_comision_apli) / 100)+((ds.fn_importe_recibir * ds.fn_porc_comision_apli) / 100)  * (SELECT iva.fn_porcentaje_iva FROM comcat_iva iva WHERE iva.ic_iva = (SELECT MAX (ic_iva) FROM comcat_iva)) / 100 )) ) AS monto_depositar,	"+
				"	bins.CG_CODIGO_BIN||' '||bins.CG_DESCRIPCION as bins, "+
				"	trim(cpi.CG_OPERA_TARJETA) as Opera_tarjeta	"+
			
				"	,d.IC_ORDEN_PAGO_TC as id_orden_enviado	"+
				"	,ptc.CG_TRANSACCION_ID as id_operacion	"+
				"	,CONCAT(ptc.CG_CODIGO_RESP ,ptc.CG_CODIGO_DESC)AS codigo_autorizacion"	+
				//"	,ptc.CG_CODIGO_AUTORIZACION as codigo_autorizacion	"+
				//"	,DF_FECHA_AUTORIZACION as fecha_registro	"+
				"	,'' as fecha_registro	"+
				"	,SUBSTR(dtc.CG_NUM_TARJETA,-4,4) as NUM_TC	"+
				" , D.IG_TIPO_PAGO " + 
				"   FROM dis_documento d,"   +
				"        dis_docto_seleccionado ds,"   +
				"        comcat_pyme py,"   +
				"        comcat_if i,"   +
				"			comcat_if i2,"+
				"        comrel_producto_epo pe,"   +
				"        comrel_pyme_epo_x_producto pep,"   +
				"        com_linea_credito lc,"   +
				"        comcat_tasa ct,"   +
				"        comcat_moneda m,"   +
				"        comcat_estatus_docto ed,"   +
				"        com_tipo_cambio tc,"   +
				"        comcat_producto_nafin pn,"   +
				"        comcat_tipo_financiamiento tf,"   +
				"        comcat_tipo_cobro_interes tci,comrel_clasificacion clas "+
				
				"	,com_bins_if bins, "+
				"	comrel_producto_if cpi	"+
				"	,DIS_DOCTOS_PAGO_TC ptc "+
				"	,DIS_DOCTOS_PAGO_TC dtc "+
				
				"  WHERE d.ic_epo = pep.ic_epo"   +
				"    AND d.ic_pyme = py.ic_pyme"   +
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento"   +
				"    AND d.ic_estatus_docto = ed.ic_estatus_docto"   +
				"    AND d.ic_linea_credito = lc.ic_linea_credito"   +
				"    AND d.ic_documento = ds.ic_documento"   +
				"    AND ds.ic_tasa = ct.ic_tasa"   +
				"    AND pn.ic_producto_nafin = pe.ic_producto_nafin"   +
				"    AND m.ic_moneda = tc.ic_moneda"   +
				"    AND lc.ic_if = i.ic_if"   +
				
				  "	AND d.IC_BINS = bins.IC_BINS(+)"+
				"  AND bins.IC_if = i2.IC_if(+) "+
				"	AND d.ic_orden_pago_tc = dtc.ic_orden_pago_tc(+)	"+
				
				 "	and lc.IC_IF = cpi.ic_if "+
				 "	and pep.IC_PRODUCTO_NAFIN = cpi.IC_PRODUCTO_NAFIN "+
				 "	and d.IC_ORDEN_PAGO_TC  =ptc.IC_ORDEN_PAGO_TC (+) "+
				
				"    AND pep.ic_pyme = py.ic_pyme"   +
				"    AND lc.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes"   +
				"    AND tc.dc_fecha = (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)"   +
				" 	 AND D.ic_clasificacion = clas.ic_clasificacion (+) "+
//				"    AND pep.cs_habilitado = 'S'"   +
				"    AND pep.ic_producto_nafin = 4"   +
				"    AND pn.ic_producto_nafin = 4"  +
				"    AND d.ic_epo = "+ic_epo+" "+condicion+" ");
				
		qryFdR.append(
				" SELECT /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */"   +
				"       distinct  py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse,"   +
				"        TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision,"   +
				"        TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga,"   +
				"        TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc,"   +
				"        d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto,"   +
				"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', '','P', 'Dolar-Peso','') AS tipo_conversion,"   +
				"        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio,"   +
				"        NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento,"   +
				"        NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento,"   +
				"        tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus,"   +
				"        m.ic_moneda, d.ic_documento, i.cg_razon_social AS nombre_if,"   +
				"        DECODE (pe.cg_tipos_credito,'F', 'Factoraje con Recurso') AS tipo_credito,"   +
				"        TO_CHAR (ds.df_fecha_seleccion, 'DD/MM/YYYY') AS df_operacion_credito,"   +
				"        d.ig_plazo_credito AS plazo_credito,"   +
				"        TO_CHAR (d.df_fecha_venc_credito, 'DD/MM/YYYY') AS df_venc_credito,"   +
				"        ct.cd_nombre || ' ' || ds.cg_rel_mat || ' ' || ds.fn_puntos AS ref_tasa,"   +
				"        tci.cd_descripcion AS tipo_cobro_int,"   +
				"        DECODE (ds.cg_rel_mat,'+', ds.fn_valor_tasa + ds.fn_puntos,'-', ds.fn_valor_tasa - ds.fn_puntos,'*', ds.fn_valor_tasa * ds.fn_puntos,"   +
				"           '/', ds.fn_valor_tasa / ds.fn_puntos,0) AS fn_valor_tasa,"   +
				"        ds.fn_importe_interes, lc.ic_moneda AS moneda_linea,clas.cg_descripcion as CATEGORIA,'Cons2InfDocEpoDist::getDocumentQueryFile' "   +
				"      ,d.CG_VENTACARTERA as CG_VENTACARTERA "+
				" 		,lc.ig_cuenta_bancaria as CUENTA_BANCARIA "+ 
				/*
				"	,d.cg_ventacartera AS cg_ventacartera, "+
				"	bins.FN_COMISION  as comision_aplicable,"+
				"	((d.fn_monto*ds.FN_PORC_COMISION_APLI)/100) as monto_comision,"+
				"	(d.fn_monto-((d.fn_monto*ds.FN_PORC_COMISION_APLI)/100)) as monto_depositar,"+
				"	bins.CG_DESCRIPCION as bins, "+
				"	trim(cpi.CG_OPERA_TARJETA) as Opera_tarjeta	"+
			
				"	,d.IC_ORDEN_PAGO_TC as id_orden_enviado	"+
				"	,ptc.CG_TRANSACCION_ID as id_operacion	"+
				"	,CONCAT(ptc.CG_CODIGO_RESP ,ptc.CG_CODIGO_DESC)AS codigo_autorizacion"	+
				//"	,ptc.CG_CODIGO_AUTORIZACION as codigo_autorizacion	"+
				"	,DF_FECHA_AUTORIZACION as fecha_registro	"+
				*/
				"	,'' AS cg_ventacartera, "+
				"	0  as comision_aplicable,"+
				"	0 as monto_comision,"+
				"	0 as monto_depositar,"+
				"	'' as bins, "+
				"	'' as Opera_tarjeta	"+
			
				"	,'' as id_orden_enviado	"+
				"	,'' as id_operacion	"+
				"	,'' AS codigo_autorizacion"	+
				"	,'' as fecha_registro	"+
				"	,'' as NUM_TC	"+
				" , D.IG_TIPO_PAGO " + 
				"   FROM dis_documento d,"   +
				"        dis_docto_seleccionado ds,"   +
				"        comcat_pyme py,"   +
				"        comcat_if i,"   +
				"			comcat_if i2,"+
				"        comrel_producto_epo pe,"   +
				"        comrel_pyme_epo_x_producto pep,"   +
				"        com_linea_credito lc,"   +
				"        comcat_tasa ct,"   +
				"        comcat_moneda m,"   +
				"        comcat_estatus_docto ed,"   +
				"        com_tipo_cambio tc,"   +
				"        comcat_producto_nafin pn,"   +
				"        comcat_tipo_financiamiento tf,"   +
				"        comcat_tipo_cobro_interes tci,comrel_clasificacion clas "+
				/*
				"	,com_bins_if bins, "+
				"	comrel_producto_if cpi	"+
				"	,DIS_DOCTOS_PAGO_TC ptc "+
				*/
				"  WHERE d.ic_epo = pep.ic_epo"   +
				"    AND d.ic_pyme = py.ic_pyme"   +
				"    AND d.ic_epo = pe.ic_epo"   +
				"    AND d.ic_moneda = m.ic_moneda"   +
				"    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento(+)"   +
				"    AND d.ic_estatus_docto = ed.ic_estatus_docto"   +
				"    AND d.ic_linea_credito = lc.ic_linea_credito"   +
				"    AND d.ic_documento = ds.ic_documento"   +
				"    AND ds.ic_tasa = ct.ic_tasa"   +
				"    AND pn.ic_producto_nafin = pe.ic_producto_nafin"   +
				"    AND m.ic_moneda = tc.ic_moneda"   +
				"    AND lc.ic_if = i.ic_if"   +
				/*
				 "	AND d.IC_BINS = bins.IC_BINS(+)"+
				"  AND bins.IC_if = i2.IC_if(+) "+
				
				 "	and lc.IC_IF = cpi.ic_if "+
				 "	and pep.IC_PRODUCTO_NAFIN = cpi.IC_PRODUCTO_NAFIN "+
				 "	and d.IC_ORDEN_PAGO_TC  =ptc.IC_ORDEN_PAGO_TC (+) "+
				*/
				"    AND pep.ic_pyme = py.ic_pyme"   +
				"    AND lc.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes"   +
				"    AND tc.dc_fecha = (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)"   +
				" 	 AND D.ic_clasificacion = clas.ic_clasificacion (+) "+
				"    AND pep.ic_producto_nafin = 4"   +
				"    AND pn.ic_producto_nafin = 4"  +
				" and d.IC_TIPO_FINANCIAMIENTO is null "+
				"	 AND lc.cg_tipo_solicitud = 'I' "+
				"	  AND lc.cs_factoraje_con_rec = 'S' "+
				"    AND d.ic_epo = "+ic_epo+" "+condicion+" ");
				

			if("D".equals(tipo_credito))
				qrySentencia.append(qryDM.toString());
			else if("C".equals(tipo_credito))
				qrySentencia.append(qryCCC.toString());
			else if("F".equals(tipo_credito))
				qrySentencia.append(qryFdR.toString());
			else
				qrySentencia.append(qryDM.toString()+ " UNION ALL "+qryCCC.toString());
				
			System.out.println("EL query queda as� : "+tipo_credito+"   "+qrySentencia.toString());
		}catch(Exception e){
			System.out.println("Cons2InfDocEpoDist::getDocumentQueryFileException "+e);
		}
		return qrySentencia.toString();
	}
/*******************************************************************************
 *          Migraci�n. Implementa IQueryGeneratorRegExtJS.java                 *
 *******************************************************************************/
	private List conditions;
	private String ic_epo;
	private String ic_pyme;
	private String ic_if;
	private String ic_estatus_docto;
	private String ig_numero_docto;
	private String fecha_seleccion_de;
	private String fecha_seleccion_a;
	private String cc_acuse;
	private String monto_credito_de;
	private String monto_credito_a;
	private String fecha_emision_de;
	private String fecha_emision_a;
	private String fecha_vto_de;
	private String fecha_vto_a;
	private String fecha_vto_credito_de;
	private String fecha_vto_credito_a;
	private String ic_tipo_cobro_int;
	private String ic_moneda;
	private String fn_monto_de;
	private String fn_monto_a;
	private String monto_con_descuento;
	private String solo_cambio;
	private String modo_plazo;
	private String tipo_credito;
	private String ic_documento;
	private String fecha_publicacion_de;
	private String fecha_publicacion_a;
	private String NOnegociable;
	private String tipo_pago;
	private String publicaDoctosFinanciables;
	private JSONArray gridTotales;
	private static final Log log = ServiceLocator.getInstance().getLog(Cons2InfDocEpoDist.class);

	public String getDocumentQuery() {
		return null;
	}

	public String getDocumentSummaryQueryForIds(List ids) {
		return null;
	}

	public String getAggregateCalculationQuery() {
		return null;
	}

/**
 * Se crea el query para generar el archivo CSV y PDF sin utilizar paginaci�n
 * @return cadena con la consulta.
 */
	public String getDocumentQueryFile(){

		log.info("getDocumentQueryFile(E)");
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer qryDM  = new StringBuffer();
		StringBuffer qryCCC = new StringBuffer();
		StringBuffer qryFdR = new StringBuffer();
		StringBuffer condicion = new StringBuffer();
		conditions = new ArrayList();

		if(!"".equals(ic_pyme)&&ic_pyme!=null){
			condicion.append(" AND D.IC_PYME = ?");
			conditions.add(ic_pyme);	
		}
		if("4".equals(ic_estatus_docto)||"20".equals(ic_estatus_docto)||"22".equals(ic_estatus_docto)||"3".equals(ic_estatus_docto)||"11".equals(ic_estatus_docto) ||"1".equals(ic_estatus_docto) ||"24".equals(ic_estatus_docto) ||"32".equals(ic_estatus_docto) ||"28".equals(ic_estatus_docto) ||"30".equals(ic_estatus_docto)){
			condicion.append(" AND D.IC_ESTATUS_DOCTO = ?");
			conditions.add(ic_estatus_docto);
		}
		if(!"".equals(ig_numero_docto)&& ig_numero_docto!=null){
			condicion.append(" AND D.ig_numero_docto = ?");
			conditions.add(ig_numero_docto);
		}
		if(!"".equals(fecha_seleccion_de)&& fecha_seleccion_de!=null&&!"".equals(fecha_seleccion_a)&& fecha_seleccion_a!=null){
			condicion.append(" and trunc(DS.df_fecha_seleccion) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");
			conditions.add(fecha_seleccion_de);
			conditions.add(fecha_seleccion_a);
		}
		if(!"".equals(cc_acuse)&& cc_acuse!=null){
			condicion.append(" AND D.cc_acuse = ?");
			conditions.add(cc_acuse);
		}
		if(!"".equals(fecha_emision_de)&& fecha_emision_de!=null&&!"".equals(fecha_emision_a)&& fecha_emision_a!=null){
			condicion.append(" and trunc(D.df_fecha_emision) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");
			conditions.add(fecha_emision_de);
			conditions.add(fecha_emision_a);
		}
		if(!"".equals(fecha_vto_de)&& fecha_vto_de!=null&&!"".equals(fecha_vto_a)&& fecha_vto_a!=null){
			condicion.append(" and trunc(D.df_fecha_venc) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");
			conditions.add(fecha_vto_de);
			conditions.add(fecha_vto_a);
		}
		if(!"".equals(fecha_vto_credito_de)&& fecha_vto_credito_de!=null&&!"".equals(fecha_vto_credito_a)&& fecha_vto_credito_a!=null){
			condicion.append(" and trunc(D.df_fecha_venc_credito) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");
			conditions.add(fecha_vto_credito_de);
			conditions.add(fecha_vto_credito_a);
		}
		if(!"".equals(ic_tipo_cobro_int)&&ic_tipo_cobro_int!=null){
			condicion.append(" AND LC.ic_tipo_cobro_interes = ?");
			conditions.add(ic_tipo_cobro_int);
		}
		if(!"".equals(ic_moneda)&&ic_moneda!=null){
			condicion.append(" AND M.ic_moneda = ?");
			conditions.add(ic_moneda);
		}
		if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&!"".equals(monto_con_descuento)){
			condicion.append(" and (D.fn_monto*(fn_porc_descuento/100)) between ? and ?");
			conditions.add(fn_monto_de);
			conditions.add(fn_monto_a);
		}
		if(!"".equals(fn_monto_de)&& fn_monto_de!=null&&!"".equals(fn_monto_a)&& fn_monto_a!=null&&"".equals(monto_con_descuento)){
			condicion.append(" and D.fn_monto between ? and ?");
			conditions.add(fn_monto_de);
			conditions.add(fn_monto_a);
		}
		if(!"".equals(solo_cambio)&& solo_cambio!=null){
			condicion.append(" AND D.ic_documento in (select ic_documento from dis_cambio_estatus)");
		}
		if(!"".equals(modo_plazo)&& modo_plazo!=null){
			condicion.append(" AND TF.ic_tipo_financiamiento = ?");
			conditions.add(modo_plazo);
		}
		if(!"".equals(ic_documento)&&ic_documento!=null){
			condicion.append(" AND D.IC_DOCUMENTO = ?");
			conditions.add(ic_documento);
		}
		if(!"".equals(ic_if)&&ic_if!=null){
			condicion.append(" AND AND LC.IC_IF = ?");
			conditions.add(ic_if);
		}
		if(!"".equals(fecha_publicacion_de)&& fecha_publicacion_de!=null&&!"".equals(fecha_publicacion_a)&& fecha_publicacion_a!=null){
			condicion.append(" and trunc(D.df_carga) between trunc(to_date(?,'dd/mm/yyyy')) and trunc(to_date(?,'dd/mm/yyyy'))");
			conditions.add(fecha_publicacion_de);
			conditions.add(fecha_publicacion_a);
		}
		if(!"".equals(monto_credito_de)&& monto_credito_de!=null&&!"".equals(monto_credito_a)&& monto_credito_a!=null){
			condicion.append(" and (D.fn_monto-(fn_monto*(fn_porc_descuento/100))) between ? and ?");
			conditions.add(monto_credito_de);
			conditions.add(monto_credito_a);
		}
		if("".equals(ic_estatus_docto) && NOnegociable.equals("N")){
			condicion.append(" and D.ic_estatus_docto in (2,3,4,5,9,11,20,22,32,28,30)");
		} else if("".equals(ic_estatus_docto) && NOnegociable.equals("S")){
			condicion.append(" and D.ic_estatus_docto in (2,1,3,4,5,9,11,20,22,32,28,30)");
		}
		if(!"F".equals(tipo_credito)){
			if(!"".equals(ic_estatus_docto)&&!"4".equals(ic_estatus_docto)&&!"20".equals(ic_estatus_docto)&&!"22".equals(ic_estatus_docto)&&!"3".equals(ic_estatus_docto)&&!"11".equals(ic_estatus_docto)&&!"1".equals(ic_estatus_docto)&&!"24".equals(ic_estatus_docto)&&!"32".equals(ic_estatus_docto)&&!"28".equals(ic_estatus_docto)&&!"30".equals(ic_estatus_docto))
				condicion.append(" and D.ic_estatus_docto is null");
		}
		if("F".equals(tipo_credito)) {
			//condicion.append(" AND pep.CG_TIPO_CREDITO IS NULL");
		} else {
			condicion.append(" AND pep.CG_TIPO_CREDITO IS NOT NULL");
		}
		if(!"".equals(tipo_pago)){
			condicion.append(" AND D.IG_TIPO_PAGO = ?");
			conditions.add(tipo_pago);
		}

		qryDM.append(" SELECT /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */");
		qryDM.append("        distinct py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse,");
		qryDM.append("        TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision,");
		qryDM.append("        TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga,");
		qryDM.append("        TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc,");
		qryDM.append("        d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto,");
		qryDM.append("        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', '','P', 'Dolar-Peso','') AS tipo_conversion,");
		qryDM.append("        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio," );
		qryDM.append("        NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento,");
		qryDM.append("        NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento,");
		qryDM.append("        tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus,");
		qryDM.append("        m.ic_moneda, d.ic_documento, i.cg_razon_social AS nombre_if,");
		qryDM.append("        DECODE (pep.cg_tipo_credito,'D', 'Modalidad 1 (Riesgo Empresa de Primer Orden)','Modalidad 2 (Riesgo Distribuidor) ') AS tipo_credito,");
		qryDM.append("        TO_CHAR (ds.df_fecha_seleccion, 'DD/MM/YYYY') AS df_operacion_credito,");
		qryDM.append("        d.ig_plazo_credito AS plazo_credito,");
		qryDM.append("        TO_CHAR (d.df_fecha_venc_credito, 'DD/MM/YYYY') AS df_venc_credito,");
		qryDM.append("        DECODE(ds.cg_tipo_tasa,'P','Preferencial','N','Negociada',CT.CD_NOMBRE ||' ' || DS.CG_REL_MAT||' '||DS.FN_PUNTOS) AS REF_TASA,");
		qryDM.append("        tci.cd_descripcion AS tipo_cobro_int,");
		qryDM.append("        ds.fn_valor_tasa,");
		qryDM.append("        ds.fn_importe_interes, lc.ic_moneda AS moneda_linea,clas.cg_descripcion as CATEGORIA,'Cons2InfDocEpoDist::getDocumentQueryFile' ");
		qryDM.append("        ,d.CG_VENTACARTERA as CG_VENTACARTERA ");
		qryDM.append("        ,'' AS cg_ventacartera, ");
		qryDM.append("        0  as comision_aplicable,");
		qryDM.append("        0 as monto_comision,");
		qryDM.append("        0 as monto_depositar,");
		qryDM.append("        '' as bins, ");
		qryDM.append("        '' as Opera_tarjeta ");
		qryDM.append("        ,'' as id_orden_enviado ");
		qryDM.append("        ,'' as id_operacion  ");
		qryDM.append("        ,'' AS codigo_autorizacion");
		qryDM.append("        ,'' as fecha_registro   ");
		qryDM.append("        ,'' as NUM_TC, D.IG_TIPO_PAGO ");
		qryDM.append("   FROM dis_documento d,");
		qryDM.append("        dis_docto_seleccionado ds,");
		qryDM.append("        comcat_pyme py,");
		qryDM.append("        comcat_if i,");
		qryDM.append("        comcat_if i2,");
		qryDM.append("        comrel_producto_epo pe,");
		qryDM.append("        comrel_pyme_epo_x_producto pep,");
		qryDM.append("        dis_linea_credito_dm lc,");
		qryDM.append("        comcat_tasa ct,");
		qryDM.append("        comcat_moneda m,");
		qryDM.append("        comcat_estatus_docto ed,");
		qryDM.append("        com_tipo_cambio tc,");
		qryDM.append("        comcat_producto_nafin pn,");
		qryDM.append("        comcat_tipo_financiamiento tf,");
		qryDM.append("        comcat_tipo_cobro_interes tci,comrel_clasificacion clas ");
		qryDM.append("  WHERE d.ic_epo = pep.ic_epo");
		qryDM.append("    AND d.ic_pyme = py.ic_pyme");
		qryDM.append("    AND d.ic_epo = pe.ic_epo");
		qryDM.append("    AND d.ic_moneda = m.ic_moneda");
		qryDM.append("    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento");
		qryDM.append("    AND d.ic_estatus_docto = ed.ic_estatus_docto");
		qryDM.append("    AND d.ic_linea_credito_dm = lc.ic_linea_credito_dm (+)");
		qryDM.append("    AND d.ic_documento = ds.ic_documento");
		qryDM.append("    AND ds.ic_tasa = ct.ic_tasa");
		qryDM.append("    AND pn.ic_producto_nafin = pe.ic_producto_nafin");
		qryDM.append("    AND m.ic_moneda = tc.ic_moneda");
		qryDM.append("    AND lc.ic_if = i.ic_if");
		qryDM.append("    AND pep.ic_pyme = py.ic_pyme");
		qryDM.append("    AND lc.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes");
		qryDM.append("    AND tc.dc_fecha = (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)");
		qryDM.append("    AND D.ic_clasificacion = clas.ic_clasificacion (+) ");
		qryDM.append("    AND pep.ic_producto_nafin = 4");
		qryDM.append("    AND pn.ic_producto_nafin = 4");
		qryDM.append("    AND d.ic_epo = "+ic_epo+" "+condicion+" ");

		qryCCC.append(" SELECT /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */");
		qryCCC.append("        distinct py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse,");
		qryCCC.append("        TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision,");
		qryCCC.append("        TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga,");
		qryCCC.append("        TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc,");
		qryCCC.append("        d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto,");
		qryCCC.append("        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', '','P', 'Dolar-Peso','') AS tipo_conversion,");
		qryCCC.append("        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio,");
		qryCCC.append("        NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento,");
		qryCCC.append("        NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento,");
		qryCCC.append("        tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus,");
		qryCCC.append("        m.ic_moneda, d.ic_documento,   ");
		qryCCC.append("        decode(ed.ic_estatus_docto,32,i2.cg_razon_social,i.cg_razon_social) AS NOMBRE_IF,");
		qryCCC.append("        DECODE (pep.cg_tipo_credito,'D', 'Modalidad 1 (Riesgo Empresa de Primer Orden)','Modalidad 2 (Riesgo Distribuidor)') AS tipo_credito,");
		qryCCC.append("        TO_CHAR (ds.df_fecha_seleccion, 'DD/MM/YYYY') AS df_operacion_credito,");
		qryCCC.append("        d.ig_plazo_credito AS plazo_credito,");
		qryCCC.append("        TO_CHAR (d.df_fecha_venc_credito, 'DD/MM/YYYY') AS df_venc_credito,");
		qryCCC.append("        ct.cd_nombre || ' ' || ds.cg_rel_mat || ' ' || ds.fn_puntos AS ref_tasa,");
		qryCCC.append("        tci.cd_descripcion AS tipo_cobro_int,");
		qryCCC.append("        DECODE (ds.cg_rel_mat,'+', ds.fn_valor_tasa + ds.fn_puntos,'-', ds.fn_valor_tasa - ds.fn_puntos,'*', ds.fn_valor_tasa * ds.fn_puntos,");
		qryCCC.append("        '/', ds.fn_valor_tasa / ds.fn_puntos,0) AS fn_valor_tasa,");
		qryCCC.append("        ds.fn_importe_interes, lc.ic_moneda AS moneda_linea,clas.cg_descripcion as CATEGORIA,'Cons2InfDocEpoDist::getDocumentQueryFile' ");
		qryCCC.append("        ,d.CG_VENTACARTERA as CG_VENTACARTERA ");
		qryCCC.append("        ,d.cg_ventacartera AS cg_ventacartera, ");
		qryCCC.append("        ds.FN_PORC_COMISION_APLI  as comision_aplicable,");
		qryCCC.append("        (((ds.fn_importe_recibir * ds.fn_porc_comision_apli) / 100)+((ds.fn_importe_recibir * ds.fn_porc_comision_apli) / 100)  * (SELECT iva.fn_porcentaje_iva FROM comcat_iva iva WHERE iva.ic_iva = (SELECT MAX (ic_iva) FROM comcat_iva)) / 100 ) AS monto_comision,");
		qryCCC.append("        (ds.fn_importe_recibir - ((((ds.fn_importe_recibir * ds.fn_porc_comision_apli) / 100)+((ds.fn_importe_recibir * ds.fn_porc_comision_apli) / 100)  * (SELECT iva.fn_porcentaje_iva FROM comcat_iva iva WHERE iva.ic_iva = (SELECT MAX (ic_iva) FROM comcat_iva)) / 100 )) ) AS monto_depositar,  ");
		qryCCC.append("        bins.CG_CODIGO_BIN||' '||bins.CG_DESCRIPCION as bins, ");
		qryCCC.append("        trim(cpi.CG_OPERA_TARJETA) as Opera_tarjeta  ");
		qryCCC.append("        ,d.IC_ORDEN_PAGO_TC as id_orden_enviado   ");
		qryCCC.append("        ,ptc.CG_TRANSACCION_ID as id_operacion ");
		qryCCC.append("        ,CONCAT(ptc.CG_CODIGO_RESP ,ptc.CG_CODIGO_DESC)AS codigo_autorizacion");
		qryCCC.append("        ,'' as fecha_registro   ");
		qryCCC.append("        ,SUBSTR(dtc.CG_NUM_TARJETA,-4,4) as NUM_TC, D.IG_TIPO_PAGO ");
		qryCCC.append("   FROM dis_documento d,");
		qryCCC.append("        dis_docto_seleccionado ds,");
		qryCCC.append("        comcat_pyme py,");
		qryCCC.append("        comcat_if i,");
		qryCCC.append("        comcat_if i2,");
		qryCCC.append("        comrel_producto_epo pe,");
		qryCCC.append("        comrel_pyme_epo_x_producto pep,");
		qryCCC.append("        com_linea_credito lc,");
		qryCCC.append("        comcat_tasa ct,");
		qryCCC.append("        comcat_moneda m,");
		qryCCC.append("        comcat_estatus_docto ed,");
		qryCCC.append("        com_tipo_cambio tc,");
		qryCCC.append("        comcat_producto_nafin pn,");
		qryCCC.append("        comcat_tipo_financiamiento tf,");
		qryCCC.append("        comcat_tipo_cobro_interes tci,comrel_clasificacion clas ");
		qryCCC.append("        ,com_bins_if bins, ");
		qryCCC.append("        comrel_producto_if cpi  ");
		qryCCC.append("        ,DIS_DOCTOS_PAGO_TC ptc ");
		qryCCC.append("        ,DIS_DOCTOS_PAGO_TC dtc ");
		qryCCC.append("  WHERE d.ic_epo = pep.ic_epo");
		qryCCC.append("    AND d.ic_pyme = py.ic_pyme");
		qryCCC.append("    AND d.ic_epo = pe.ic_epo");
		qryCCC.append("    AND d.ic_moneda = m.ic_moneda");
		qryCCC.append("    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento");
		qryCCC.append("    AND d.ic_estatus_docto = ed.ic_estatus_docto");
		qryCCC.append("    AND d.ic_linea_credito = lc.ic_linea_credito");
		qryCCC.append("    AND d.ic_documento = ds.ic_documento");
		qryCCC.append("    AND ds.ic_tasa = ct.ic_tasa");
		qryCCC.append("    AND pn.ic_producto_nafin = pe.ic_producto_nafin");
		qryCCC.append("    AND m.ic_moneda = tc.ic_moneda");
		qryCCC.append("    AND lc.ic_if = i.ic_if");
		qryCCC.append("    AND d.IC_BINS = bins.IC_BINS(+)");
		qryCCC.append("    AND bins.IC_if = i2.IC_if(+) ");
		qryCCC.append("    AND d.ic_orden_pago_tc = dtc.ic_orden_pago_tc(+)");
		qryCCC.append("    and lc.IC_IF = cpi.ic_if ");
		qryCCC.append("    and pep.IC_PRODUCTO_NAFIN = cpi.IC_PRODUCTO_NAFIN ");
		qryCCC.append("    and d.IC_ORDEN_PAGO_TC  =ptc.IC_ORDEN_PAGO_TC (+) ");
		qryCCC.append("    AND pep.ic_pyme = py.ic_pyme");
		qryCCC.append("    AND lc.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes");
		qryCCC.append("    AND tc.dc_fecha = (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)");
		qryCCC.append("    AND D.ic_clasificacion = clas.ic_clasificacion (+) ");
		qryCCC.append("    AND pep.ic_producto_nafin = 4");
		qryCCC.append("    AND pn.ic_producto_nafin = 4");
		qryCCC.append("    AND d.ic_epo = "+ic_epo+" "+condicion+" ");

		qryFdR.append(" SELECT /*+ index (d IN_DIS_DOCUMENTO_01_NUK, pep CP_COMREL_PYME_EPO_X_PROD_PK) */");
		qryFdR.append("        distinct  py.cg_razon_social AS nombre_dist, d.ig_numero_docto, d.cc_acuse,");
		qryFdR.append("        TO_CHAR (d.df_fecha_emision, 'DD/MM/YYYY') AS df_fecha_emision,");
		qryFdR.append("        TO_CHAR (d.df_carga, 'DD/MM/YYYY') AS df_carga,");
		qryFdR.append("        TO_CHAR (d.df_fecha_venc, 'DD/MM/YYYY') AS df_fecha_venc,");
		qryFdR.append("        d.ig_plazo_docto, m.cd_nombre AS moneda, d.fn_monto,");
		qryFdR.append("        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'N', '','P', 'Dolar-Peso','') AS tipo_conversion,");
		qryFdR.append("        DECODE (NVL (pe.cg_tipo_conversion, pn.cg_tipo_conversion),'P', DECODE (m.ic_moneda, 54, tc.fn_valor_compra, '1'),'1') AS tipo_cambio,");
		qryFdR.append("        NVL (d.ig_plazo_descuento, '') AS ig_plazo_descuento,");
		qryFdR.append("        NVL (d.fn_porc_descuento, 0) AS fn_porc_descuento,");
		qryFdR.append("        tf.cd_descripcion AS modo_plazo, ed.cd_descripcion AS estatus,");
		qryFdR.append("        m.ic_moneda, d.ic_documento, i.cg_razon_social AS nombre_if,");
		qryFdR.append("        DECODE (pe.cg_tipos_credito,'F', 'Factoraje con Recurso') AS tipo_credito,");
		qryFdR.append("        TO_CHAR (ds.df_fecha_seleccion, 'DD/MM/YYYY') AS df_operacion_credito,");
		qryFdR.append("        d.ig_plazo_credito AS plazo_credito,");
		qryFdR.append("        TO_CHAR (d.df_fecha_venc_credito, 'DD/MM/YYYY') AS df_venc_credito,");
		qryFdR.append("        ct.cd_nombre || ' ' || ds.cg_rel_mat || ' ' || ds.fn_puntos AS ref_tasa," );
		qryFdR.append("        tci.cd_descripcion AS tipo_cobro_int,");
		qryFdR.append("        DECODE (ds.cg_rel_mat,'+', ds.fn_valor_tasa + ds.fn_puntos,'-', ds.fn_valor_tasa - ds.fn_puntos,'*', ds.fn_valor_tasa * ds.fn_puntos,");
		qryFdR.append("        '/', ds.fn_valor_tasa / ds.fn_puntos,0) AS fn_valor_tasa,");
		qryFdR.append("        ds.fn_importe_interes, lc.ic_moneda AS moneda_linea,clas.cg_descripcion as CATEGORIA,'Cons2InfDocEpoDist::getDocumentQueryFile' ");
		qryFdR.append("        ,d.CG_VENTACARTERA as CG_VENTACARTERA ");
		qryFdR.append("        ,lc.ig_cuenta_bancaria as CUENTA_BANCARIA ");
		qryFdR.append("        ,'' AS cg_ventacartera, ");
		qryFdR.append("        0 as comision_aplicable,");
		qryFdR.append("        0 as monto_comision,");
		qryFdR.append("        0 as monto_depositar,");
		qryFdR.append("        '' as bins, ");
		qryFdR.append("        '' as Opera_tarjeta  ");
		qryFdR.append("        ,'' as id_orden_enviado ");
		qryFdR.append("        ,'' as id_operacion  ");
		qryFdR.append("        ,'' AS codigo_autorizacion");
		qryFdR.append("        ,'' as fecha_registro   ");
		qryFdR.append("        ,'' as NUM_TC, D.IG_TIPO_PAGO ");
		qryFdR.append("   FROM dis_documento d,");
		qryFdR.append("        dis_docto_seleccionado ds,");
		qryFdR.append("        comcat_pyme py,");
		qryFdR.append("        comcat_if i,");
		qryFdR.append("        comcat_if i2,");
		qryFdR.append("        comrel_producto_epo pe,");
		qryFdR.append("        comrel_pyme_epo_x_producto pep,");
		qryFdR.append("        com_linea_credito lc,");
		qryFdR.append("        comcat_tasa ct,");
		qryFdR.append("        comcat_moneda m,");
		qryFdR.append("        comcat_estatus_docto ed,");
		qryFdR.append("        com_tipo_cambio tc,");
		qryFdR.append("        comcat_producto_nafin pn,");
		qryFdR.append("        comcat_tipo_financiamiento tf,");
		qryFdR.append("        comcat_tipo_cobro_interes tci,comrel_clasificacion clas ");
		qryFdR.append("  WHERE d.ic_epo = pep.ic_epo");
		qryFdR.append("    AND d.ic_pyme = py.ic_pyme");
		qryFdR.append("    AND d.ic_epo = pe.ic_epo");
		qryFdR.append("    AND d.ic_moneda = m.ic_moneda");
		qryFdR.append("    AND d.ic_tipo_financiamiento = tf.ic_tipo_financiamiento(+)");
		qryFdR.append("    AND d.ic_estatus_docto = ed.ic_estatus_docto");
		qryFdR.append("    AND d.ic_linea_credito = lc.ic_linea_credito");
		qryFdR.append("    AND d.ic_documento = ds.ic_documento");
		qryFdR.append("    AND ds.ic_tasa = ct.ic_tasa");
		qryFdR.append("    AND pn.ic_producto_nafin = pe.ic_producto_nafin");
		qryFdR.append("    AND m.ic_moneda = tc.ic_moneda");
		qryFdR.append("    AND lc.ic_if = i.ic_if");
		qryFdR.append("    AND pep.ic_pyme = py.ic_pyme");
		qryFdR.append("    AND lc.ic_tipo_cobro_interes = tci.ic_tipo_cobro_interes");
		qryFdR.append("    AND tc.dc_fecha = (SELECT MAX (dc_fecha) FROM com_tipo_cambio WHERE ic_moneda = m.ic_moneda)");
		qryFdR.append("    AND D.ic_clasificacion = clas.ic_clasificacion (+) ");
		qryFdR.append("    AND pep.ic_producto_nafin = 4");
		qryFdR.append("    AND pn.ic_producto_nafin = 4");
		qryFdR.append("    and d.IC_TIPO_FINANCIAMIENTO is null ");
		qryFdR.append("    AND lc.cg_tipo_solicitud = 'I' ");
		qryFdR.append("    AND lc.cs_factoraje_con_rec = 'S' ");
		qryFdR.append("    AND d.ic_epo = "+ic_epo+" "+condicion+" ");

		if("D".equals(tipo_credito))
			qrySentencia.append(qryDM.toString());
		else if("C".equals(tipo_credito))
			qrySentencia.append(qryCCC.toString());
		else if("F".equals(tipo_credito))
			qrySentencia.append(qryFdR.toString());
		else
			qrySentencia.append(qryDM.toString()+ " UNION ALL "+qryCCC.toString());

		log.info("qrySentencia: " + qrySentencia.toString());
		log.info("conditions: " + conditions.toString());
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();
	}

/**
 * Genera el archivo CSV o PDF seg�n el tipo que se le indique.
 * En el caso del PDF, el archivo generado no comtempla paginaci�n, imprime todos los registros que trae la consulta.
 * @param request
 * @param rs
 * @param path
 * @param tipo: CSV o PDF
 * @return nombre del archivo
 */
	public String crearCustomFile(HttpServletRequest request, ResultSet rs, String path, String tipo) {

		log.info("crearCustomFile (E)");
		String nombreArchivo ="";
		HttpSession session = request.getSession();
		ComunesPDF pdfDoc = new ComunesPDF();

		String dist               = "";
		String numDocto           = "";
		String numAcuseCarga      = "";
		String fechaEmision       = "";
		String fechaPublicacion   = "";
		String fechaVencimiento   = "";
		String plazoDocto         = "";
		String moneda             = "";
		String icMoneda           = "";
		double monto              = 0;
		String tipoConversion     = "";
		double tipoCambio         = 0;
		double montoValuado       = 0;
		String categoria          = "";
		String plazoDescuento     = "";
		String porcDescuento      = "";
		double montoDescontar     = 0;
		String modoPlazo          = "";
		String estatus            = "";
		String icDocumento        = "";
		String intermediario      = "";
		String tipoCredito        = "";
		String fechaOperacion     = "";
		double montoCredito       = 0;
		String plazoCredito       = "";
		String fechaVencCredito   = "";
		String referenciaTasaInt  = "";
		String valorTasaInt       = "";
		String montoTasaInt       = "";
		String tipoCobroInt       = "";
		String monedaLinea        = "";
		String bandeVentaCartera  = "";
		String cuenta_Bancaria    = "";
		String comisionAplicable  = "";
		String montoComision      = "";
		String montoDepositar     = "";
		String bins               = "";
		String idOrdenEnviado     = "";
		String idOperacion        = "";
		String codigoAutorizacion = "";
		String fechaRegistro      = "";
		String numTarjeta         = "";
		String tipoPago           = "";
		int nRow                  = 0;

		if(tipo.equals("PDF")){

			try {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				pdfDoc = new ComunesPDF(2, path + nombreArchivo);

				String meses[]     = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual   = fechaActual.substring(0,2);
				String mesActual   = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual  = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());

				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
					((session.getAttribute("iNoNafinElectronico")==null)?"":session.getAttribute("iNoNafinElectronico").toString()),
					(String)session.getAttribute("sesExterno"),
					(String) session.getAttribute("strNombre"),
					(String) session.getAttribute("strNombreUsuario"),
					(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);

				while(rs.next()){
					dist              = (rs.getString("NOMBRE_DIST")          ==null)?"" :rs.getString("NOMBRE_DIST");
					numDocto          = (rs.getString("IG_NUMERO_DOCTO")      ==null)?"" :rs.getString("IG_NUMERO_DOCTO");
					numAcuseCarga     = (rs.getString("CC_ACUSE")             ==null)?"" :rs.getString("CC_ACUSE");
					fechaEmision      = (rs.getString("DF_FECHA_EMISION")     ==null)?"" :rs.getString("DF_FECHA_EMISION");
					fechaPublicacion  = (rs.getString("DF_CARGA")             ==null)?"" :rs.getString("DF_CARGA");
					fechaVencimiento  = (rs.getString("DF_FECHA_VENC")        ==null)?"" :rs.getString("DF_FECHA_VENC");
					plazoDocto        = (rs.getString("IG_PLAZO_DOCTO")       ==null)?"" :rs.getString("IG_PLAZO_DOCTO");
					moneda            = (rs.getString("MONEDA")               ==null)?"" :rs.getString("MONEDA");
					tipoConversion    = (rs.getString("TIPO_CONVERSION")      ==null)?"" :rs.getString("TIPO_CONVERSION");
					plazoDescuento    = (rs.getString("IG_PLAZO_DESCUENTO")   ==null)?"" :rs.getString("IG_PLAZO_DESCUENTO");
					porcDescuento     = (rs.getString("FN_PORC_DESCUENTO")    ==null)?"0":rs.getString("FN_PORC_DESCUENTO");
					modoPlazo         = (rs.getString("MODO_PLAZO")           ==null)?"" :rs.getString("MODO_PLAZO");
					estatus           = (rs.getString("ESTATUS")              ==null)?"" :rs.getString("ESTATUS");
					icMoneda          = (rs.getString("IC_MONEDA")            ==null)?"" :rs.getString("IC_MONEDA");
					icDocumento       = (rs.getString("IC_DOCUMENTO")         ==null)?"" :rs.getString("IC_DOCUMENTO");
					intermediario     = (rs.getString("NOMBRE_IF")            ==null)?"" :rs.getString("NOMBRE_IF");
					tipoCredito       = (rs.getString("TIPO_CREDITO")         ==null)?"" :rs.getString("TIPO_CREDITO");
					fechaOperacion    = (rs.getString("DF_OPERACION_CREDITO") ==null)?"" :rs.getString("DF_OPERACION_CREDITO");
					plazoCredito      = (rs.getString("PLAZO_CREDITO")        ==null)?"" :rs.getString("PLAZO_CREDITO");
					fechaVencCredito  = (rs.getString("DF_VENC_CREDITO")      ==null)?"" :rs.getString("DF_VENC_CREDITO");
					referenciaTasaInt = (rs.getString("REF_TASA")             ==null)?"" :rs.getString("REF_TASA");
					valorTasaInt      = (rs.getString("FN_VALOR_TASA")        ==null)?"0":rs.getString("FN_VALOR_TASA");
					montoTasaInt      = (rs.getString("FN_IMPORTE_INTERES")   ==null)?"0":rs.getString("FN_IMPORTE_INTERES");
					tipoCobroInt      = (rs.getString("TIPO_COBRO_INT")       ==null)?"0":rs.getString("TIPO_COBRO_INT"); 
					monedaLinea       = (rs.getString("MONEDA_LINEA")         ==null)?"" :rs.getString("MONEDA_LINEA");
					categoria         = (rs.getString("CATEGORIA")            ==null)?"" :rs.getString("CATEGORIA");
					comisionAplicable = (rs.getString("COMISION_APLICABLE")   ==null)?"" :rs.getString("COMISION_APLICABLE");
					montoComision     = (rs.getString("MONTO_COMISION")       ==null)?"" :rs.getString("MONTO_COMISION");
					montoDepositar    = (rs.getString("MONTO_DEPOSITAR")      ==null)?"" :rs.getString("MONTO_DEPOSITAR");
					bins              = (rs.getString("BINS")                 ==null)?"" :rs.getString("BINS");
					idOrdenEnviado    = (rs.getString("ID_ORDEN_ENVIADO")     ==null)?"" :rs.getString("ID_ORDEN_ENVIADO");
					idOperacion       = (rs.getString("ID_OPERACION")         ==null)?"" :rs.getString("ID_OPERACION");
					codigoAutorizacion= (rs.getString("CODIGO_AUTORIZACION")  ==null)?"" :rs.getString("CODIGO_AUTORIZACION");
					fechaRegistro     = (rs.getString("FECHA_REGISTRO")       ==null)?"" :rs.getString("FECHA_REGISTRO");
					String operaTajeta= (rs.getString("OPERA_TARJETA")        ==null)?"" :rs.getString("OPERA_TARJETA");
					numTarjeta        = (rs.getString("NUM_TC")               ==null)?"" :rs.getString("NUM_TC");
					tipoPago          = (rs.getString("IG_TIPO_PAGO")         ==null)?"" :rs.getString("IG_TIPO_PAGO");
					monto             = Double.parseDouble(rs.getString("FN_MONTO"));
					tipoCambio        = Double.parseDouble(rs.getString("TIPO_CAMBIO"));
					montoDescontar    = monto*Double.parseDouble(porcDescuento)/100;
					montoValuado      = (monto-montoDescontar)*tipoCambio;
					montoCredito      = monto-montoDescontar;
					if(tipo_credito.equals("C")){
						if(tipoPago.equals("1")){
							tipoPago = "Financiamiento con intereses";
						} else if(tipoPago.equals("2")){
							tipoPago = "Meses sin intereses";
						}
					} else{
						tipoPago = "N/A";
					}
					//Si no es Operada TC
					if(!estatus.equals("Operada TC")){
						bins = "N/A";
						comisionAplicable = "N/A";
						montoComision = "N/A";
						montoDepositar = "N/A";

						idOrdenEnviado = "N/A";
						idOperacion = "N/A";
						codigoAutorizacion = "N/A";
						fechaRegistro = "N/A";
						numTarjeta = "N/A";
					} else{
						//if( operaTajeta.equals("S") ){
						fechaVencimiento = "N/A";
						plazoDocto = "N/A";
						plazoCredito = "N/A";
						fechaVencCredito = "N/A";
						valorTasaInt = "N/A";
						montoTasaInt = "N/A";
						referenciaTasaInt = "N/A";
					}

					if("F".equals(tipo_credito)){
						cuenta_Bancaria = (rs.getString("CUENTA_BANCARIA")==null)?"":rs.getString("CUENTA_BANCARIA");
					}
					if(!icMoneda.equals(monedaLinea))
						montoCredito = montoValuado;
					else 
						montoCredito = monto-montoDescontar;
					bandeVentaCartera = (rs.getString("CG_VENTACARTERA")==null)?"":rs.getString("CG_VENTACARTERA");

					if (bandeVentaCartera.equals("S")){
						modoPlazo ="";
					}

					if(nRow == 0){
						int numCols = 17;//18
						float widths[];
						if(!"".equals(tipoConversion)){
							numCols = 19;//20
						}else{
							numCols = 17;//18
						}
						if("F".equals(tipo_credito)){
							numCols++;
						}
						if(publicaDoctosFinanciables.equals("S")){
							numCols++;
						}
						pdfDoc.setLTable(numCols,100);
						pdfDoc.setLCell("Datos del Documento Inicial","celda01",ComunesPDF.CENTER,numCols);
						pdfDoc.setLCell("A","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Distribuidor","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("No. Docto Inicial","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("No. Acuse Pyme","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha Emisi�n","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha Pub.","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Fecha Venc.","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Plazo Docto","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Moneda","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Categor�a","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Plazo Descuento D�as","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("%   Descuento","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto % Descuento","celda01",ComunesPDF.CENTER);
						if(!"".equals(tipoConversion)){
							pdfDoc.setLCell("Tipo Conv.","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Tipo Cambio","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Monto Valuado","celda01",ComunesPDF.CENTER);
						}
						pdfDoc.setLCell("Modalidad de Plazo","celda01",ComunesPDF.CENTER);
						if(publicaDoctosFinanciables.equals("S")){
							pdfDoc.setLCell("Tipo de pago","celda01",ComunesPDF.CENTER);
						}
						pdfDoc.setLCell("Estatus","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Tipo Cred.","celda01",ComunesPDF.CENTER);
						if("F".equals(tipo_credito)){
							pdfDoc.setLCell("Cuenta Bancaria Pyme","celda01",ComunesPDF.CENTER);
						}
						//pdfDoc.setLCell("","celda01",ComunesPDF.CENTER);

						//pdfDoc.setLCell("Datos del Documento Final","celda01",ComunesPDF.CENTER,numCols);
						pdfDoc.setLCell("B","celda01",ComunesPDF.CENTER);

						pdfDoc.setLCell("No. Docto Final","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto","celda01",ComunesPDF.CENTER);

						pdfDoc.setLCell("% Comisi�n Aplicable \n de Terceros ","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto Comisi�n de Terceros (IVA Incluido)","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto a Depositar \n por \n Operaci�n","celda01",ComunesPDF.CENTER);

						if(!"F".equals(tipo_credito)){
							pdfDoc.setLCell("Plazo","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Fecha Venc.","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Fecha Oper","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("IF","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Nombre del Producto","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Ref. Tasa Inter�s","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Valor Tasa Inter�s","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Monto Inter�s","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("ID Orden enviado","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("Respuesta de Operaci�n","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("C�digo Autorizaci�n","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell("N�mero Tarjeta de Cr�dito","celda01",ComunesPDF.CENTER);

						}else if("F".equals(tipo_credito)){
							pdfDoc.setLCell(" ","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell(" ","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell(" ","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell(" ","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell(" ","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell(" ","celda01",ComunesPDF.CENTER);
							pdfDoc.setLCell(" ","celda01",ComunesPDF.CENTER);
						}

						if(!"".equals(tipoConversion)){
							pdfDoc.setLCell(" ","celda01",ComunesPDF.CENTER);
						}
						if("F".equals(tipo_credito)){
							pdfDoc.setLCell(" ","celda01",ComunesPDF.CENTER);
						}
						if(publicaDoctosFinanciables.equals("S")){
							//pdfDoc.setLCell(" ","celda01",ComunesPDF.CENTER);
						}
						pdfDoc.setLHeaders();
						nRow++;
					}
					pdfDoc.setLCell("A","celda01",ComunesPDF.CENTER);
					pdfDoc.setLCell(dist.replace(',',' '),"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(numDocto,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(numAcuseCarga,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaEmision,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaPublicacion,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(fechaVencimiento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(plazoDocto,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(moneda,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.RIGHT);
					pdfDoc.setLCell(categoria,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(plazoDescuento,"formas",ComunesPDF.CENTER);
					pdfDoc.setLCell(porcDescuento+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoDescontar,2),"formas",ComunesPDF.RIGHT);

					if(!"".equals(tipoConversion)){
						pdfDoc.setLCell(((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?tipoConversion:""),"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell("$"+((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?Comunes.formatoDecimal(tipoCambio,2,false):""),"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell("$"+((tipoCambio!=1&&!icMoneda.equals(monedaLinea))?Comunes.formatoDecimal(montoValuado,2,false):""),"formas",ComunesPDF.RIGHT);
					}
					pdfDoc.setLCell(modoPlazo,"formas",ComunesPDF.LEFT);
					if(publicaDoctosFinanciables.equals("S")){
						pdfDoc.setLCell(tipoPago,"formas",ComunesPDF.LEFT);
					}
					pdfDoc.setLCell(estatus,"formas",ComunesPDF.LEFT);
					pdfDoc.setLCell(tipoCredito,"formas",ComunesPDF.LEFT);
					if("F".equals(tipo_credito)){
						pdfDoc.setLCell(cuenta_Bancaria,"formas",ComunesPDF.CENTER);
					}
					//pdfDoc.setLCell("","formas",ComunesPDF.LEFT);

					pdfDoc.setLCell("B ","celda01",ComunesPDF.CENTER);
					if(!estatus.equals("Negociable")){
						pdfDoc.setLCell(icDocumento,"formas",ComunesPDF.LEFT);
						pdfDoc.setLCell("$"+Comunes.formatoDecimal(montoCredito,2),"formasrep",ComunesPDF.RIGHT);

						pdfDoc.setLCell((!"N/A".equals(comisionAplicable))?Comunes.formatoDecimal(comisionAplicable,2)+"%":comisionAplicable,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell((!"N/A".equals(montoComision))?"$"+Comunes.formatoDecimal(montoComision,2):montoComision,"formas",ComunesPDF.RIGHT);
						pdfDoc.setLCell((!"N/A".equals(montoDepositar))?"$"+Comunes.formatoDecimal(montoDepositar,2):montoDepositar,"formas",ComunesPDF.RIGHT);

						if(!"F".equals(tipo_credito)){
							pdfDoc.setLCell(plazoCredito,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell(fechaVencCredito,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell(fechaOperacion,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell(intermediario,"formas",ComunesPDF.LEFT);
							pdfDoc.setLCell(bins,"formas",ComunesPDF.CENTER);

							pdfDoc.setLCell(referenciaTasaInt.replace(',', '.'),"formas",ComunesPDF.LEFT);
							pdfDoc.setLCell((!"N/A".equals(valorTasaInt))?"$"+Comunes.formatoDecimal(valorTasaInt,2):valorTasaInt,"formas",ComunesPDF.RIGHT);
							pdfDoc.setLCell((!"N/A".equals(montoTasaInt))?"$"+Comunes.formatoDecimal(montoTasaInt,2):montoTasaInt,"formas",ComunesPDF.RIGHT);

							pdfDoc.setLCell(idOrdenEnviado,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell(idOperacion,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell(codigoAutorizacion,"formas",ComunesPDF.CENTER);
							pdfDoc.setLCell((!"".equals(numTarjeta.trim()) && !"N/A".equals(numTarjeta.trim()))?"XXXX-XXXX-XXXX-"+numTarjeta:numTarjeta,"formas",ComunesPDF.CENTER);

						}else if("F".equals(tipo_credito)){
							pdfDoc.setLCell("","formas",ComunesPDF.LEFT);
							pdfDoc.setLCell("","formas",ComunesPDF.LEFT);
							pdfDoc.setLCell("","formas",ComunesPDF.LEFT);
							pdfDoc.setLCell("","formas",ComunesPDF.LEFT);
							pdfDoc.setLCell("","formas",ComunesPDF.LEFT);
							pdfDoc.setLCell("","formas",ComunesPDF.LEFT); 
							pdfDoc.setLCell("","formas",ComunesPDF.LEFT);
						}
					} else{
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);

						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
					}

					if(!"".equals(tipoConversion)){
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
					}
					if("F".equals(tipo_credito)){
						pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
					}
					if(publicaDoctosFinanciables.equals("S")){
						//pdfDoc.setLCell(" ","formas",ComunesPDF.CENTER);
					}
				}
				pdfDoc.addLTable();

				/***** Lleno la tabla de totales *****/
				try{
					if(gridTotales.size() > 0){
						BigDecimal cantidadTmp = new BigDecimal("0.00");
						pdfDoc.setLTable(4,100);
						pdfDoc.setLCell("TOTALES","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Registros","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto Documentos","celda01",ComunesPDF.CENTER);
						pdfDoc.setLCell("Monto a depositar por operaci�n","celda01",ComunesPDF.CENTER);
						pdfDoc.setLHeaders();
						for(int i=0; i<gridTotales.size(); i++){
							JSONObject auxiliar = gridTotales.getJSONObject(i);
							pdfDoc.setLCell(auxiliar.getString("NOMMONEDA"), "formas", ComunesPDF.LEFT);
							pdfDoc.setLCell(auxiliar.getString("TOTAL_REGISTROS"), "formas", ComunesPDF.CENTER);
							cantidadTmp = new BigDecimal(auxiliar.getString("TOTAL_MONTO_DOCUMENTOS"));
						   System.out.println(cantidadTmp);
							pdfDoc.setLCell("$"+Comunes.formatoDecimal(cantidadTmp,2), "formas", ComunesPDF.RIGHT);
							cantidadTmp = new BigDecimal(auxiliar.getString("MONTO_DEPOSITARB"));
							pdfDoc.setLCell("$"+Comunes.formatoDecimal(cantidadTmp,2), "formas", ComunesPDF.RIGHT);
						}
						pdfDoc.addLTable();
					}
				} catch(Exception e){
					log.warn("Error al obtener la tabla de totales. (El resto del documento se imprimir� de forma normal)." + e);
				}

				pdfDoc.endDocument();

			} catch(Throwable e){
				throw new AppException("Error al generar el archivo PDF. ", e);
			}
		}
		log.info("crearCustomFile (S)");
		return nombreArchivo;
	}

	public String crearPageCustomFile(HttpServletRequest request, Registros reg, String path, String tipo) {
		return null;
	}

/*******************************************************************************
 *                             GETTERS Y SETTERS                               *
 *******************************************************************************/
	public String getIc_epo() {
		return ic_epo;
	}

	public void setIc_epo(String ic_epo) {
		this.ic_epo = ic_epo;
	}

	public String getIc_pyme() {
		return ic_pyme;
	}

	public void setIc_pyme(String ic_pyme) {
		this.ic_pyme = ic_pyme;
	}

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getIc_estatus_docto() {
		return ic_estatus_docto;
	}

	public void setIc_estatus_docto(String ic_estatus_docto) {
		this.ic_estatus_docto = ic_estatus_docto;
	}

	public String getIg_numero_docto() {
		return ig_numero_docto;
	}

	public void setIg_numero_docto(String ig_numero_docto) {
		this.ig_numero_docto = ig_numero_docto;
	}

	public String getFecha_seleccion_de() {
		return fecha_seleccion_de;
	}

	public void setFecha_seleccion_de(String fecha_seleccion_de) {
		this.fecha_seleccion_de = fecha_seleccion_de;
	}

	public String getFecha_seleccion_a() {
		return fecha_seleccion_a;
	}

	public void setFecha_seleccion_a(String fecha_seleccion_a) {
		this.fecha_seleccion_a = fecha_seleccion_a;
	}

	public String getCc_acuse() {
		return cc_acuse;
	}

	public void setCc_acuse(String cc_acuse) {
		this.cc_acuse = cc_acuse;
	}

	public String getMonto_credito_de() {
		return monto_credito_de;
	}

	public void setMonto_credito_de(String monto_credito_de) {
		this.monto_credito_de = monto_credito_de;
	}

	public String getMonto_credito_a() {
		return monto_credito_a;
	}

	public void setMonto_credito_a(String monto_credito_a) {
		this.monto_credito_a = monto_credito_a;
	}

	public String getFecha_emision_de() {
		return fecha_emision_de;
	}

	public void setFecha_emision_de(String fecha_emision_de) {
		this.fecha_emision_de = fecha_emision_de;
	}

	public String getFecha_emision_a() {
		return fecha_emision_a;
	}

	public void setFecha_emision_a(String fecha_emision_a) {
		this.fecha_emision_a = fecha_emision_a;
	}

	public String getFecha_vto_de() {
		return fecha_vto_de;
	}

	public void setFecha_vto_de(String fecha_vto_de) {
		this.fecha_vto_de = fecha_vto_de;
	}

	public String getFecha_vto_a() {
		return fecha_vto_a;
	}

	public void setFecha_vto_a(String fecha_vto_a) {
		this.fecha_vto_a = fecha_vto_a;
	}

	public String getFecha_vto_credito_de() {
		return fecha_vto_credito_de;
	}

	public void setFecha_vto_credito_de(String fecha_vto_credito_de) {
		this.fecha_vto_credito_de = fecha_vto_credito_de;
	}

	public String getFecha_vto_credito_a() {
		return fecha_vto_credito_a;
	}

	public void setFecha_vto_credito_a(String fecha_vto_credito_a) {
		this.fecha_vto_credito_a = fecha_vto_credito_a;
	}

	public String getIc_tipo_cobro_int() {
		return ic_tipo_cobro_int;
	}

	public void setIc_tipo_cobro_int(String ic_tipo_cobro_int) {
		this.ic_tipo_cobro_int = ic_tipo_cobro_int;
	}

	public String getIc_moneda() {
		return ic_moneda;
	}

	public void setIc_moneda(String ic_moneda) {
		this.ic_moneda = ic_moneda;
	}

	public String getFn_monto_de() {
		return fn_monto_de;
	}

	public void setFn_monto_de(String fn_monto_de) {
		this.fn_monto_de = fn_monto_de;
	}

	public String getFn_monto_a() {
		return fn_monto_a;
	}

	public void setFn_monto_a(String fn_monto_a) {
		this.fn_monto_a = fn_monto_a;
	}

	public String getMonto_con_descuento() {
		return monto_con_descuento;
	}

	public void setMonto_con_descuento(String monto_con_descuento) {
		this.monto_con_descuento = monto_con_descuento;
	}

	public String getSolo_cambio() {
		return solo_cambio;
	}

	public void setSolo_cambio(String solo_cambio) {
		this.solo_cambio = solo_cambio;
	}

	public String getModo_plazo() {
		return modo_plazo;
	}

	public void setModo_plazo(String modo_plazo) {
		this.modo_plazo = modo_plazo;
	}

	public String getTipo_credito() {
		return tipo_credito;
	}

	public void setTipo_credito(String tipo_credito) {
		this.tipo_credito = tipo_credito;
	}

	public String getIc_documento() {
		return ic_documento;
	}

	public void setIc_documento(String ic_documento) {
		this.ic_documento = ic_documento;
	}

	public String getFecha_publicacion_de() {
		return fecha_publicacion_de;
	}

	public void setFecha_publicacion_de(String fecha_publicacion_de) {
		this.fecha_publicacion_de = fecha_publicacion_de;
	}

	public String getFecha_publicacion_a() {
		return fecha_publicacion_a;
	}

	public void setFecha_publicacion_a(String fecha_publicacion_a) {
		this.fecha_publicacion_a = fecha_publicacion_a;
	}

	public String getNOnegociable() {
		return NOnegociable;
	}

	public void setNOnegociable(String NOnegociable) {
		this.NOnegociable = NOnegociable;
	}

	public List getConditions(){
		return conditions; 
	}

	public String getTipo_pago() {
		return tipo_pago;
	}

	public void setTipo_pago(String tipo_pago) {
		this.tipo_pago = tipo_pago;
	}

	public String getPublicaDoctosFinanciables() {
		return publicaDoctosFinanciables;
	}

	public void setPublicaDoctosFinanciables(String publicaDoctosFinanciables) {
		this.publicaDoctosFinanciables = publicaDoctosFinanciables;
	}

	public JSONArray getGridTotales() {
		return gridTotales;
	}

	public void setGridTotales(JSONArray gridTotales) {
		this.gridTotales = gridTotales;
	}
}
