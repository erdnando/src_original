package com.netro.electronica;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class Cons_Operaciones implements  IQueryGeneratorRegExtJS {	

	
	public Cons_Operaciones() {}   
	
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(Cons_Operaciones.class);  
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");  
	private String 	paginaOffset;
	private String 	paginaNo;
	private String ic_if;
	private String ic_solicitud;	
	private String iTipoAfiliado;
	private String strPerfil;
	private String fechaSolicitudIni;
	private String fechaSolicitudFin;
	private String fechaOperacionIni;
	private String fechaOperacionFin;
	private String fechaRechazoIni;
	private String fechaRechazoFin;
	private String montoIni;
	private String montoFin;
	private String inicializa;
	private String iNoUsuario;
	private String rutaNombreArchivo;
	

  public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
		public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
	   log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
			
					
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
	
	
		qrySentencia.append( " 	 SELECT   "+
		   " i.ic_if as IC_IF, "+ 
			" i.CG_RAZON_SOCIAL as NOMBRE_IF, "+
			" to_char(S.DF_SOLICITUD , 'dd/mm/yyyy HH:MI:SS AM')   as FECHA_SOLICITUD , "+
			" s.IC_SOLICITUD as NO_SOLICITUD, "+
			" s.cc_acuse as CS_ACUSE, "+
			" c.CG_NOMBRE_CONTRATO as NOMBRE_CONTRATO, "+
			" TO_CHAR (di.DF_FIRMA_CONTRATO, 'dd/mm/yyyy')   as FECHA_FIRMA_CONTRATO ,  "+
			" di.CG_CONT_MODIFICATORIOS as NO_MODIFICATORIOS,"+
			" 'NA' as FECHA_FIRMA_MODIFICATORIOS,  "+
			" di.CG_CONTRATO_OE  as NOMBRE_CONTRATO_M,"+
			" to_char(di.DF_FIRMA_CONTRATO_OE , 'dd/mm/yyyy')   as  FECHA_CONTRATOS_OE,"+
			" s.FN_MONTO_SOLICITUD as  IMPORTE, "+
			" s.CG_DES_RECURSOS as DESTINO_RECURSOS, " +
			" to_char(s.DF_PAGO_CAPITAL , 'dd/mm/yyyy')  as FECHA_PAGO_CAPITAL, "+
			" to_char(s.DF_PAGO_INTERES , 'dd/mm/yyyy') as FECHA_PAGO_INTERES,"+
			" to_char(s.DF_VENCIMIENTO , 'dd/mm/yyyy') as FECHA_VENCIMIENTO,"+
			" t.CD_NOMBRE as NOMBRE_TASA,  "+
			" s.IN_TASA_ACEPTADA as TASA_INTERES, "+
			" s.CG_TIPO_TASA as TIPO_TASA,"+
			" e.CD_DESCRIPCION as ESTATUS, "+
			" to_char(s.DF_OPERADO , 'dd/mm/yyyy HH:MI:SS AM') as FECHA_OPERACION_EJECUTIVO, "+
		   " to_char(s.DF_OPERADO_CON, 'dd/mm/yyyy') as FECHA_OP_CONCENTRADOR, "+   
			" S.CG_CAUSAS_RECHAZO as CAUSA_RECHAZO,"+       
			" S.CG_OBSERVACIONES_IF as OBSER_INTERMEDIARIO,"+
			" S.CG_OBSERVACIONES_OP as OBSER_EJECUTIVO,  "+			
			" m.CD_NOMBRE as MONEDA  ,  "+
			" length(s.BI_DOC_COTIZACION)  as  DOC_COTIZACION ,  "+
			" length(s.BI_DOC_CARTERA)  as  DOC_CARTERA ,"+
			" length(s.BI_DOC_CARTERA_PDF)  as  DOC_CARTERA_PDF ,"+///
			" length(s.BI_DOC_BOLETARUG)  as  DOC_BOLETA ,"+
			" length(s.BI_DOC_PRENDA)  as  DOC_PRENDA,  "+  
			" length(s.BI_EMPRESAS)  as  DOC_EMPRESAS,  "+
			" s.ic_estatus_ope as  IC_ESTATUS , "+  			   
			" a.CG_NOMBRE_USUARIO as  CG_NOMBRE_USUARIO ,  "+  						
			" S.CG_NOM_USU_OP  AS  USUARIO_EJE_OP ,   "+
			" S.CG_NOM_USUARIO_CON2 AS  USUARIO_EJE_CON2 ,   "+ 
			" to_char(s.DF_RECHAZO , 'dd/mm/yyyy HH:MI:SS AM')   as DF_RECHAZO  , "+
			" to_char(s.DF_RECHAZADO_CON , 'dd/mm/yyyy HH:MI:SS AM') as DF_RECHAZADO_CON,  "+
			" '' AS TIPO_USUARIO , "+
			
			" '' AS MONTO_ASIGNADO,   "+
			" '' AS MONTO_UTILIZADO,  "+
			" '' AS MONTO_DISPONIBLE, "+
			" '' AS NUMERO_PRESTAMO,     "+
			" '' AS SALDO_FONDO_LIQUIDO , " +
			" decode(s.IG_NUMERO_PRESTAMO,'0', '', s.IG_NUMERO_PRESTAMO) AS NO_PRESTAMO ,s.CG_CAUSA_RECHAZO_AUTOR as CAUSA_RECHAZO_AUTOR, '' AS EXISTE_REVISION"+  
			
			" FROM OPE_REG_SOLICITUD S, comcat_if i, OPECAT_CONTRATO c , OPE_DATOS_IF  di, "+
			" OPECAT_ESTATUS e, comcat_moneda m,   "+
			" comcat_tasa t  , OPE_ACUSE a  "+  
			
			"  WHERE s.ic_if = i.ic_if  "+
			"  and di.ic_if = i.ic_if  "+
			"  and di.ic_if = s.ic_if  "+
			"  and s.IC_CONTRATO = c.IC_CONTRATO "+
			"  and s.ic_estatus_ope = e.ic_estatus_ope "+
			"  and  s.ic_if = di.ic_if  "+			
			"  and s.ic_moneda = m.ic_moneda  " +
			"  and s.ic_tasa = t.ic_tasa(+)   "+
			"  and s.cc_acuse = a.cc_acuse  ");  
			
			 if (!"".equals(ic_if) )  {
				 qrySentencia.append( " and s.ic_if  = ? ");    
				  conditions.add(ic_if);
			 }
			 
			 if (!"".equals(ic_solicitud) )  {
				 qrySentencia.append( " and s.cc_acuse  = ? ");
				  conditions.add(ic_solicitud);
			 }
			 
			 if ( !"".equals(fechaSolicitudIni ) &&   !"".equals(fechaSolicitudFin ) )   {
				qrySentencia.append(" and s.DF_SOLICITUD >= TO_DATE ( ?, 'dd/mm/yyyy') ");
				qrySentencia.append(" and  s.DF_SOLICITUD < TO_DATE (?, 'dd/mm/yyyy') + 1 " );
				conditions.add(fechaSolicitudIni);
				conditions.add(fechaSolicitudFin);
			}
			
			if ( !"".equals(fechaOperacionIni ) &&   !"".equals(fechaOperacionFin ) )   {
				qrySentencia.append(" and s.DF_OPERADO_CON  >= TO_DATE ( ?, 'dd/mm/yyyy') ");
				qrySentencia.append(" and  s.DF_OPERADO_CON  < TO_DATE (?, 'dd/mm/yyyy') + 1 " );
				conditions.add(fechaOperacionIni);
				conditions.add(fechaOperacionFin);
			}
			
			if ( !"".equals(fechaRechazoIni ) &&   !"".equals(fechaRechazoFin ) )   {
				qrySentencia.append(" and s.DF_RECHAZADO_CON  >= TO_DATE ( ?, 'dd/mm/yyyy') ");
				qrySentencia.append(" and  s.DF_RECHAZADO_CON  < TO_DATE (?, 'dd/mm/yyyy') + 1 " );
				conditions.add(fechaRechazoIni);
				conditions.add(fechaRechazoFin);
			}
			
			if ( !"".equals(montoIni ) &&   !"".equals(montoFin ) )   {
				qrySentencia.append(" and s.FN_MONTO_SOLICITUD  between ? and  ? ");
				conditions.add(montoIni);
				conditions.add(montoFin);
			}
				
			  
			if ("OP OP".equals(strPerfil)  )  {
			 qrySentencia.append( " and s.IC_EJECUTIVO_OP = ?  ");  
			 conditions.add(iNoUsuario);			
			}
			if ("SUP SEG".equals(strPerfil)  )  {
			 qrySentencia.append( " and di.CG_EJEC_SEG = ?  "); 
			 conditions.add(iNoUsuario);			
			}
			if ("EJE PROMO".equals(strPerfil)  )  {
			 qrySentencia.append( " and di.CG_EJEC_PROMO = ?  "); 
			 conditions.add(iNoUsuario);			
			}			



				//Sub. L�nea Inmediata y Con Prenda 		
			 if ("SUB LI".equals(strPerfil)  )  {
				 qrySentencia.append( " and c.IC_CONTRATO  in (  ?) "); 
				  conditions.add("1");
				  
			 }
			 
			  if ("SUB MICRO CP".equals(strPerfil) )  {
				 qrySentencia.append( " and c.IC_CONTRATO  in ( ?, ? ,?,?) ");
				  conditions.add("2");
				  conditions.add("3");
				  conditions.add("4");
				  conditions.add("5");
				  
			 }			 
			 
			 if ("S".equals(inicializa ) )   {
				qrySentencia.append(" and s.DF_OPERADO_CON  >= TRUNC(SYSDATE)  ");
				qrySentencia.append(" and  s.DF_OPERADO_CON  < TRUNC(SYSDATE) + 1 " );
				qrySentencia.append(" and s.ic_estatus_ope  = ?  ");			   	
				conditions.add("5");
			}else  {
				if ("IF 4CP".equals(strPerfil)|| "IF 5CP".equals(strPerfil) || "IF LI".equals(strPerfil)|| "IF 4MIC".equals(strPerfil)|| "IF 5MIC".equals(strPerfil) )  {
					qrySentencia.append("  and s.ic_estatus_ope in (1,2, 3,4,5,6,7,8)  ");
				}else if ("OP CON".equals(strPerfil)|| "OP OP".equals(strPerfil) || "ADMIN NAFIN".equals(strPerfil)){
					qrySentencia.append("  and s.ic_estatus_ope in (1,2, 3,4,5,6,7,8)  ");
				}else{    
					qrySentencia.append("  and s.ic_estatus_ope in (1,2, 3,4,5,6)  ");
				}
			}
			
			 
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		

		try {
				
			
		
				
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {			
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	/**
	 * 
	 * @return 
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
		
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		int total = 0;
		 String  saldoLiquido ="";
		 double SaldoCredito = 0;
		 HashMap registros  = new HashMap();
		try { 
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);
					
			contenidoArchivo = new StringBuffer();	
			
			if (!"".equals(ic_if) )  {
				registros =   this.getSaldos (ic_if);	
				contenidoArchivo.append(" Monto L�nea Credito,  $"+Comunes.formatoDecimal(registros.get("MONTO_ASIGNADO").toString(), 2, false)+"\n");
				contenidoArchivo.append(" Monto Utilizado,  $"+Comunes.formatoDecimal(registros.get("MONTO_UTILIZADO").toString(), 2, false)+"\n");
				contenidoArchivo.append(" Monto Disponible,  $"+Comunes.formatoDecimal(registros.get("MONTO_DISPONIBLE").toString(), 2, false)+"\n");
				contenidoArchivo.append(" N�mero de Prestamos,"+ registros.get("NUMERO_PRESTAMO").toString()+"\n");
			}
	
			if ("IF 5CP".equals(strPerfil)   || "IF 4MIC".equals(strPerfil)  || "IF 5MIC".equals(strPerfil) )  {
				contenidoArchivo.append(" Saldo Fondo L�quido,  $"+Comunes.formatoDecimal(registros.get("SALDO_FONDO_LIQUIDO").toString(), 2, false)+"\n");				
			}
			contenidoArchivo.append(" \n ");	
			
			if(iTipoAfiliado.equals("N"))  {
				contenidoArchivo.append(" Nombre IF ,");					
			}
			contenidoArchivo.append(" Fecha  Solicitud, No. solicitud, No. Pr�stamo, Nombre Contrato, Fecha Firma Contrato, "+
			" Contratos Modificatorios , Fecha Firma Modificatorios  ,  Nombre Contrato ,  " +
			" Fecha Firma Contrato Operaci�n Electr�nica  , Importe, Moneda, Destino de los recursos, Fecha de primer pago de Capital,  "+
			" Fecha de primer pago de Intereses,  Fecha de Vencimiento,  Tipo de Tasa, Tasa, Valor,  Estatus,  Usuario IF,  "+
			" Fecha Operaci�n Ejecutivo OP, Fecha Rechazo Ejecutivo OP, Usuario Ejecutivo OP,   Fecha Operaci�n, "+
			" Fecha Rechazo Concentrador, Usuario Concentrador, Causas de Rechazo, Observaciones Intermediario Financiero ,  " +
			" Observaciones del Ejecutivo de Operaci�n, Causas de Rechazo autorizador");
			
			if(iTipoAfiliado.equals("N"))  {
				contenidoArchivo.append(", Monto l�nea de cr�dito  ,  Monto utilizado  ,  Monto Disponible  ,  	N�mero de Pr�stamos  ");				
				contenidoArchivo.append(", Saldo de Fondo L�quido  ");						
			}
			
			contenidoArchivo.append(" \n ");
			
			while (rs.next())	{	  
			
				String fechaSolic = (rs.getString("FECHA_SOLICITUD") == null) ? "" : rs.getString("FECHA_SOLICITUD");
				String acuse = (rs.getString("CS_ACUSE") == null) ? "" : rs.getString("CS_ACUSE");
				String nombreContrato = (rs.getString("NOMBRE_CONTRATO") == null) ? "" : rs.getString("NOMBRE_CONTRATO");
				String fecFirmaContrato = (rs.getString("FECHA_FIRMA_CONTRATO") == null) ? "" : rs.getString("FECHA_FIRMA_CONTRATO");
				
				String no_modificatorios = (rs.getString("NO_MODIFICATORIOS") == null) ? "N/A" : rs.getString("NO_MODIFICATORIOS");
				String nombreContratoM = (rs.getString("NOMBRE_CONTRATO_M") == null) ? "" : rs.getString("NOMBRE_CONTRATO_M");
				String FechaContratoOE = (rs.getString("FECHA_CONTRATOS_OE") == null) ? "" : rs.getString("FECHA_CONTRATOS_OE");
				String importe = (rs.getString("IMPORTE") == null) ? "" : rs.getString("IMPORTE");
				String moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
				String destinoRecursos = (rs.getString("DESTINO_RECURSOS") == null) ? "" : rs.getString("DESTINO_RECURSOS");
				String fecPagoCapital = (rs.getString("FECHA_PAGO_CAPITAL") == null) ? "" : rs.getString("FECHA_PAGO_CAPITAL");
				String fecPagoInteres = (rs.getString("FECHA_PAGO_INTERES") == null) ? "" : rs.getString("FECHA_PAGO_INTERES");
				String fecVencimiento = (rs.getString("FECHA_VENCIMIENTO") == null) ? "" : rs.getString("FECHA_VENCIMIENTO");
				String tasaInteres = (rs.getString("TASA_INTERES") == null) ? "N/A" : rs.getString("TASA_INTERES");
				String tipoTasa = (rs.getString("TIPO_TASA") == null) ? "" : rs.getString("TIPO_TASA");
				String estatus = (rs.getString("ESTATUS") == null) ? "" : rs.getString("ESTATUS");
				String fecOperaEjecutivo = (rs.getString("FECHA_OPERACION_EJECUTIVO") == null) ? "" : rs.getString("FECHA_OPERACION_EJECUTIVO");
				String fecOperaConcentrador = (rs.getString("FECHA_OP_CONCENTRADOR") == null) ? "" : rs.getString("FECHA_OP_CONCENTRADOR");
				String causa_rechazo = (rs.getString("CAUSA_RECHAZO") == null) ? "" : rs.getString("CAUSA_RECHAZO");
				String obseIntermediario = (rs.getString("OBSER_INTERMEDIARIO") == null) ? "" : rs.getString("OBSER_INTERMEDIARIO");
				String obseEjecutivo = (rs.getString("OBSER_EJECUTIVO") == null) ? "" : rs.getString("OBSER_EJECUTIVO");
				String ic_if = (rs.getString("ic_if") == null) ? "" : rs.getString("ic_if");
				String nombreIF = (rs.getString("NOMBRE_IF") == null) ? "" : rs.getString("NOMBRE_IF");
				String nombreTasa = (rs.getString("NOMBRE_TASA") == null) ? "N/A" : rs.getString("NOMBRE_TASA");
				String strNombreUsuario = (rs.getString("CG_NOMBRE_USUARIO") == null) ? "" : rs.getString("CG_NOMBRE_USUARIO");
				String usuarioEje_PRO = (rs.getString("USUARIO_EJE_OP") == null) ? "" : rs.getString("USUARIO_EJE_OP");
				String usuarioCon2 = (rs.getString("USUARIO_EJE_CON2") == null) ? "" : rs.getString("USUARIO_EJE_CON2");
				String fechaRechazo = (rs.getString("DF_RECHAZO") == null) ? "" : rs.getString("DF_RECHAZO");
				String fechaRechazoConcentrador = (rs.getString("DF_RECHAZADO_CON") == null) ? "" : rs.getString("DF_RECHAZADO_CON");
			   String ic_if_1 = (rs.getString("IC_IF") == null) ? "" : rs.getString("IC_IF");	
				String no_Prestamo = (rs.getString("NO_PRESTAMO") == null) ? "" : rs.getString("NO_PRESTAMO");	//
				String CAUSA_RECHAZO_AUTOR = (rs.getString("CAUSA_RECHAZO_AUTOR") == null) ? "" : rs.getString("CAUSA_RECHAZO_AUTOR");	//
				
				String nombreUsuario = strNombreUsuario.substring(strNombreUsuario.lastIndexOf("-")+1, strNombreUsuario.length());
				String fechasModificatorias = this.getFirmasModificatorias (ic_if_1) ;
				
				 registros =   this.getSaldos (ic_if_1);			
				
				if(iTipoAfiliado.equals("N"))  {
					contenidoArchivo.append(nombreIF.replace(',',' ')+",");  
				}
				contenidoArchivo.append(fechaSolic.replace(',',' ')+","+
					acuse.replace(',',' ')+","+
					no_Prestamo.replace(',',' ')+","+					
					nombreContrato.replace(',',' ')+","+
					fecFirmaContrato.replace(',',' ')+","+
					no_modificatorios.replace(',',' ')+","+
					fechasModificatorias.replace(',',' ')+","+
					nombreContratoM.replace(',',' ')+","+
					FechaContratoOE.replace(',',' ')+","+
					importe.replace(',',' ')+","+
					moneda.replace(',',' ')+","+
					destinoRecursos.replace(',',' ')+","+
					fecPagoCapital.replace(',',' ')+","+
					fecPagoInteres.replace(',',' ')+","+
					fecVencimiento.replace(',',' ')+","+
					tipoTasa.replace(',',' ')+","+
					nombreTasa.replace(',',' ')+","+
					tasaInteres.replace(',',' ')+","+
					estatus.replace(',',' ')+","+
					nombreUsuario.replace(',',' ')+","+
					fecOperaEjecutivo.replace(',',' ')+","+
					fechaRechazo.replace(',',' ')+","+
					usuarioEje_PRO.replace(',',' ')+","+
					fecOperaConcentrador.replace(',',' ')+","+
					fechaRechazoConcentrador.replace(',',' ')+","+
					usuarioCon2.replace(',',' ')+","+
					causa_rechazo.replaceAll("[\n\r]", " ").replace(',',' ')+","+
					obseIntermediario.replaceAll("[\n\r]", " ").replace(',',' ')+","+	
					obseEjecutivo.replaceAll("[\n\r]", " ").replace(',',' ')+","+
					CAUSA_RECHAZO_AUTOR.replaceAll("[\n\r]", " ").replace(',',' ')+","
					);
					
					if(iTipoAfiliado.equals("N"))  {
						contenidoArchivo.append(Comunes.formatoDecimal(registros.get("MONTO_ASIGNADO").toString(), 2, false)+",");
						contenidoArchivo.append(Comunes.formatoDecimal(registros.get("MONTO_UTILIZADO").toString(), 2, false)+",");
						contenidoArchivo.append(Comunes.formatoDecimal(registros.get("MONTO_DISPONIBLE").toString(), 2, false)+",");
						contenidoArchivo.append(registros.get("NUMERO_PRESTAMO").toString()+",");
						contenidoArchivo.append(Comunes.formatoDecimal(registros.get("SALDO_FONDO_LIQUIDO").toString(), 2, false)+",");										
					}
					
					contenidoArchivo.append("\n");		
			 
				total++;
				if(total==1000){					
					total=0;	
					buffer.write(contenidoArchivo.toString());
					contenidoArchivo = new StringBuffer();//Limpio  
				}
				
				
			}//while(rs.next()){
				
			buffer.write(contenidoArchivo.toString());
			buffer.close();	
			contenidoArchivo = new StringBuffer();//Limpio    
			
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}

	
	
	/**
	 * Obtener Fechas Modificatorias para mostrarlas en el Grid 
	 * @throws java.lang.Exception
	 * @return 
	 * @param ic_if
	 */
	public String  getFirmasModificatorias (String ic_if)   throws Exception{ 
		log.info("getFirmasModificatorias (E)");
		
   	AccesoDB 	con = new AccesoDB();
      boolean		commit = true;		
      PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer 	SQL = new StringBuffer();  
		StringBuffer 	contenido = new StringBuffer();   
		List varBind = new ArrayList();
		int i = 0;
   	try{
			con.conexionDB();
         
			SQL.append( " SELECT  CG_DESCRIPCION AS  DESCRIPCION, to_char(DF_FIRMA_MODIFICATORIA,'dd/mm/yyyy') as  FECHA " +
			" from OPE_DET_FIR_MODIFI "+
			" where ic_if  = ? ");			
			varBind.add(ic_if);
			//log.info("SQL.toString() "+SQL.toString());
			//log.info("varBind "+varBind);
			
			ps = con.queryPrecompilado(SQL.toString(), varBind);
			rs = ps.executeQuery();
			int x =1;
		   while( rs.next() ){
				i++;
				String descripcion =  rs.getString("DESCRIPCION")==null?"":rs.getString("DESCRIPCION");
				String fecha =  rs.getString("FECHA")==null?"":rs.getString("FECHA");
				contenido.append(" Contrato Modificatorio "+x+" con fecha de firma " +fecha + ", ");
				x++;
			}
			rs.close();
			ps.close();		
					
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			log.error("Error getFirmasModificatorias "+e);
			throw new AppException("Error al getFirmasModificatorias",e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getFirmasModificatorias (S)");
		}	
		return contenido.toString();
   }  
	
	/**
	 * 
	 * @throws java.lang.Exception
	 * @return 
	 * @param ic_if
	 */
	public HashMap  getSaldos (String ic_if)   throws Exception{ 
		log.info("getSaldos (E)");
		
   	AccesoDB 	con = new AccesoDB();
      boolean		commit = true;		
     	StringBuffer 	SQL = new StringBuffer();  
		List varBind = new ArrayList();		
		PreparedStatement ps = null;
		ResultSet rs =null;				
		String linea = "", ic_financiera = "", codigo_cliente ="", monto_asignado ="0", 
				 monto_utilizado ="0", num_prestamos ="", saldoLiquido =  "0";
		double monto_disponible = 0;
		HashMap	registros = new HashMap();
		 
		
   	try{
			con.conexionDB();
			
     		SQL = new StringBuffer();  
			varBind = new ArrayList();	
			SQL.append( " SELECT IC_FINANCIERA  FROM COMCAT_IF  WHERE IC_IF  = ?  ");  				
			varBind.add(ic_if);
			log.info("SQL.toString() "+SQL.toString());
			log.info("varBind "+varBind);
			
			ps = con.queryPrecompilado(SQL.toString(), varBind);
			rs = ps.executeQuery();			
		   if( rs.next() ){
				ic_financiera=  rs.getString("IC_FINANCIERA")==null?"":rs.getString("IC_FINANCIERA");					
			}
			rs.close();
			ps.close();	
					
			//leer el archivo
			java.io.File lofArchivo = new java.io.File(rutaNombreArchivo);
			BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(lofArchivo),"ISO-8859-1"));
		  	int	iNumLinea=0;			
					
			while((linea=br.readLine())!=null) {
				
				SQL = new StringBuffer();  
				varBind = new ArrayList();	
				
				if(iNumLinea==0) {
				
					SQL.append(linea);					
					varBind.add(ic_financiera); 
					
					log.debug("iNumLinea== "+iNumLinea +"  SQL ::::"+SQL.toString() );
					log.debug("varBind  ::::"+varBind );
					
					ps = con.queryPrecompilado(SQL.toString(), varBind);
					rs = ps.executeQuery();			
					if( rs.next() ){
						codigo_cliente=  rs.getString("CODIGO_CLIENTE")==null?"":rs.getString("CODIGO_CLIENTE");		
						monto_asignado=  rs.getString("MONTO_ASIGNADO")==null?"0":rs.getString("MONTO_ASIGNADO");						
					}
					rs.close();
					ps.close();	
				}
				
				if(iNumLinea==1 &&  "".equals(codigo_cliente) )  {
				
					SQL.append(linea);					
					varBind.add(ic_financiera); 
					
					log.debug("iNumLinea== "+iNumLinea +"  SQL ::::"+SQL.toString() );
					log.debug("varBind  ::::"+varBind );
					
					ps = con.queryPrecompilado(SQL.toString(), varBind);
					rs = ps.executeQuery();			
					if( rs.next() ){
						codigo_cliente=  rs.getString("CODIGO_CLIENTE")==null?"":rs.getString("CODIGO_CLIENTE");		
						monto_asignado=  rs.getString("MONTO_ASIGNADO")==null?"0":rs.getString("MONTO_ASIGNADO");						
					}
					rs.close();
					ps.close();	
				}
				
				
				if(iNumLinea==2 &&  !"".equals(codigo_cliente) )  {
					
					SQL.append(linea);					
					varBind.add(codigo_cliente);
					
					log.debug("iNumLinea== "+iNumLinea +"  SQL ::::"+SQL.toString() );
					log.debug("varBind ::::"+varBind );
					
					ps = con.queryPrecompilado(SQL.toString(), varBind);
					rs = ps.executeQuery();			
					if( rs.next() ){
					
						monto_utilizado=  rs.getString("MONTO_UTILIZADO")==null?"0":rs.getString("MONTO_UTILIZADO");		
						num_prestamos =  rs.getString("NUMERO_PRESTAMO")==null?"0":rs.getString("NUMERO_PRESTAMO");							
											
					}
					rs.close();
					ps.close();	
				}
				
				iNumLinea ++;				
			}
			
		
			monto_disponible  =  Double.parseDouble((String)monto_asignado) -Double.parseDouble((String)monto_utilizado);
			
			
				//Fondo l�quido que el Intermediario Financiero dep�sito a NAFIN CU9	
			SQL = new StringBuffer();  
			varBind = new ArrayList();	
			SQL.append( " select  FG_SALDO_HOY as SALDOLIQUIDO  from   OPE_CARGA_SALDO_FLIQUIDO " +
							" where ic_carga =  (select max(ic_carga)  from OPE_CARGA_SALDO_FLIQUIDO  where ic_if  = ? ) ");				
			varBind.add(ic_if);
			log.debug("SQL.toString() "+SQL.toString());
			log.debug("varBind "+varBind);
			
			ps = con.queryPrecompilado(SQL.toString(), varBind);
			rs = ps.executeQuery();			
		   if( rs.next() ){
				saldoLiquido =  rs.getString("SALDOLIQUIDO")==null?"0":rs.getString("SALDOLIQUIDO");
			}
			rs.close();
			ps.close();
			
			
			registros.put("MONTO_ASIGNADO", monto_asignado);
			registros.put("MONTO_UTILIZADO", monto_utilizado);	
			registros.put("NUMERO_PRESTAMO", num_prestamos);	
			registros.put("MONTO_DISPONIBLE", Double.toString (monto_disponible));	
			registros.put("SALDO_FONDO_LIQUIDO", saldoLiquido);	
				
					
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			log.error("Error getSaldos "+e);
			throw new AppException("Error al getSaldos",e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getSaldos (S)");
		}	
		return registros;
   }  
	
	
	public List getConditions() {  return conditions;  }  
	public String getPaginaNo() { return paginaNo; 	}
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getIc_solicitud() {
		return ic_solicitud;
	}

	public void setIc_solicitud(String ic_solicitud) {
		this.ic_solicitud = ic_solicitud;
	}


	public String getITipoAfiliado() {
		return iTipoAfiliado;
	}

	public void setITipoAfiliado(String iTipoAfiliado) {
		this.iTipoAfiliado = iTipoAfiliado;
	}

	public String getStrPerfil() {
		return strPerfil;
	}

	public void setStrPerfil(String strPerfil) {
		this.strPerfil = strPerfil;
	}

	public String getFechaSolicitudIni() {
		return fechaSolicitudIni;
	}

	public void setFechaSolicitudIni(String fechaSolicitudIni) {
		this.fechaSolicitudIni = fechaSolicitudIni;
	}

	public String getFechaSolicitudFin() {
		return fechaSolicitudFin;
	}

	public void setFechaSolicitudFin(String fechaSolicitudFin) {
		this.fechaSolicitudFin = fechaSolicitudFin;
	}

	public String getFechaOperacionIni() {
		return fechaOperacionIni;
	}

	public void setFechaOperacionIni(String fechaOperacionIni) {
		this.fechaOperacionIni = fechaOperacionIni;
	}

	public String getFechaOperacionFin() {
		return fechaOperacionFin;
	}

	public void setFechaOperacionFin(String fechaOperacionFin) {
		this.fechaOperacionFin = fechaOperacionFin;
	}

	public String getFechaRechazoIni() {
		return fechaRechazoIni;
	}

	public void setFechaRechazoIni(String fechaRechazoIni) {
		this.fechaRechazoIni = fechaRechazoIni;
	}

	public String getFechaRechazoFin() {
		return fechaRechazoFin;
	}

	public void setFechaRechazoFin(String fechaRechazoFin) {
		this.fechaRechazoFin = fechaRechazoFin;
	}

	public String getMontoIni() {
		return montoIni;
	}

	public void setMontoIni(String montoIni) {
		this.montoIni = montoIni;
	}

	public String getMontoFin() {
		return montoFin;
	}

	public void setMontoFin(String montoFin) {
		this.montoFin = montoFin;
	}

	public String getInicializa() {
		return inicializa;
	}

	public void setInicializa(String inicializa) {
		this.inicializa = inicializa;
	}

	public String getINoUsuario() {
		return iNoUsuario;
	}

	public void setINoUsuario(String iNoUsuario) {
		this.iNoUsuario = iNoUsuario;
	}

	public String getRutaNombreArchivo() {
		return rutaNombreArchivo;
	}

	public void setRutaNombreArchivo(String rutaNombreArchivo) {
		this.rutaNombreArchivo = rutaNombreArchivo;
	}

	
}