package com.netro.electronica;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class CargaIFOE implements IQueryGeneratorRegExtJS {
	
	public CargaIFOE() {
		//Constructor sin Argumentos
	}
	
	private String cve_if="";
	private String valida="";

	private static final Log log = ServiceLocator.getInstance().getLog(CargaIFOE.class);//Variable para enviar mensajes al log.

	public String getAggregateCalculationQuery()	{
		log.debug("..:: CargaIFOE - getAggregateCalculationQuery(E)");
		StringBuffer 	qrySentencia 	= new StringBuffer();
		
		return qrySentencia.toString();
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds)  {
	
		StringBuffer qrySentencia = new StringBuffer();
		return qrySentencia.toString();
	}
	
	public String getDocumentQuery()	{ //genera todos los id's
	
		StringBuffer qrySentencia = new StringBuffer();		
		log.debug("getDocumentQuery)"+qrySentencia.toString());
		return qrySentencia.toString();
	}
	
	public String getDocumentQueryFile() {
		this.variablesBind = new ArrayList();
		StringBuffer 	qrySentencia 	= new StringBuffer();
    	StringBuffer	condicion 		= new StringBuffer();
		
		try{
		
			if(valida.equals("formulario")){
				qrySentencia.append(
						" SELECT ic_if as CVEIF, ic_num_cuenta as NUMCUENTA, cg_banco as BANCO, ic_cta_clabe as CLABE, TO_CHAR (df_firma_contrato, 'dd/mm/yyyy') as FIRMACONT, cg_contrato_oe as NOMCONTRATO, "+
						" TO_CHAR (DF_FIRMA_CONTRATO_OE, 'dd/mm/yyyy') as FIRMAOE, CG_CONT_MODIFICATORIOS as NUMMOD, CG_EJEC_PROMO as USRPROMO, CG_TEL_PROM0 as TELPROMO, CG_EJEC_SEG as USRSEG, "+
						" CG_TEL_SEG as TELSEG, CG_HORARIO_ENVIO as HR_ENVIO, CG_HORARIO_REENVIO as HR_REENVIO, cg_opera_firma_manc, cg_operador, cg_autorizador_uno, cg_autorizador_dos, CS_MUJERES_EMPRESARIAS "+
						" FROM OPE_DATOS_IF ");
			
			}else if(valida.equals("grid")){
				qrySentencia.append(
						" SELECT cg_descripcion as DESCRIPCION, TO_CHAR (df_firma_modificatoria, 'dd/mm/yyyy') as FECHAMOD "+
						" FROM OPE_DET_FIR_MODIFI ");
				
			}
			
			qrySentencia.append(" WHERE ic_if = ? ");
		
			this.variablesBind.add(cve_if);
			
			if(valida.equals("grid")){
				qrySentencia.append(" ORDER BY ic_firma_modifi ");
			}
			
		}catch (Exception e){
			log.debug("CargaIFOE::getDocumentQueryException "+e);
		}
	
		log.debug("getDocumentQueryFile)"+qrySentencia.toString());
		return qrySentencia.toString();
	}
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String nombreArchivo = "";
		
		return nombreArchivo;
	}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		log.debug("crearCustomFile (E)");
		
		return nombreArchivo;
	}
	
	public List getConditions() {
		log.debug("*************getConditions=" + variablesBind);
		return variablesBind;
	}
	private ArrayList variablesBind = null;

	public void setCve_if(String cve_if) {
		this.cve_if = cve_if;
	}

	public String getCve_if() {
		return cve_if;
	}


	public void setValida(String valida) {
		this.valida = valida;
	}


	public String getValida() {
		return valida;
	}

			
	
}