package com.netro.electronica;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class Rep_Prestamos implements  IQueryGeneratorRegExtJS {	

	
	public Rep_Prestamos() {}   
	
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(Rep_Prestamos.class);  
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");  
	private String 	paginaOffset;
	private String 	paginaNo;
	private String ic_if;
	private String fechaOperacionIni;
	private String fechaOperacionFin;
	private String no_Prestamo;
		

  public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
		public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
	   log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
			
					
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
	
	
		qrySentencia.append(" SELECT  "+
			 "  i.DESCRIPCION as NOMBRE_IF  ,  "+
			 "  s.IG_NUMERO_PRESTAMO  as NO_PRESTAMO ,  "+
			 "  TO_CHAR(lr.RGLO_FECHA_OPERACION,'DD/MM/YYYY')AS FECHA_OPERACION   , " +
			 "   mo.CD_NOMBRE as MONEDA,  "+
			 "   TO_CHAR(lr.fecha,'DD/MM/YYYY')AS FECHA, "+
			 "   lr.monto MONTO, "+
			 "   ac.nombre  as NOMBRE, "+
			 "   ac.nombre2 as NOMBRE2 , "+
			 "   ac.apellido_paterno as APE_PATERNO,   "+ 
			 "   ac.apellido_materno as APE_MATERNO, "+
			 "   ac.RFC  as RFC, "+
			 "   lr.MONTO as IMPORTE ,  "+
			 "   pi.DESCRIPCION as PRODUCTO,  "+
			 "   ae.DESCRIPCION as ACT_ECONOMICA,  "+
			 "   Est.DESCRIPCION as ESTADO,  "+
			 "   m.DESCRIPCION as MUNICIPIO, "+
			 "   er.DESCRIPCION as ESTRATO , "+
			 "   ac.VENTAS_NETAS  as VENTAS, "+
			 "   ac.NO_EMPLEADOS as NO_EMPLEADOS ,   "+
			 "   '' as GROUP_ID    "+
			 "   FROM linrev_lineas_revolventes lr, "+
			 "    linrev_producto_intermediarios pi,  "+
			 "   linrev_intermediarios i,  "+
			 "   linrev_municipios m,  "+
			 "   linrev_estrato er,  "+
			 "   linrev_actividades_economicas ae, "+
			 "   linrev_ramas r,  "+
			 "   linrev_subsectores sb,  "+
			 "   linrev_sectores sc,  "+
			 "   linrev_acreditados ac, "+
			 "   linrev_monedas mn, "+
			 "   linrev_fuentes fn, "+
			 "   linrev_rubros rb, "+
			 "   linrev_unidades un, "+
			 "   linrev_conceptos cn, "+
			 "   linrev_estatus es ,"+
			 "   linrev_estados est,"+
			 "   ope_reg_solicitud s,"+
			 "   comcat_moneda mo"+
			 "    WHERE pi.inte_clave = lr.pint_inte_clave "+
			 "    AND pi.prod_clave = lr.pint_prod_clave "+
			 "    AND i.clave = pi.inte_clave"+ 
			 "    AND m.clave = lr.muni_clave "+ 
			 "    AND er.clave = lr.estr_clave "+
			 "    AND ae.sect_clave = lr.aeco_sect_clave "+
			  "   AND ae.ssec_clave = lr.aeco_ssec_clave "+
			  "   AND ae.rama_clave = lr.aeco_rama_clave "+
			  "   AND ae.clas_clave = lr.aeco_clas_clave "+
			  "   AND r.rama_clave = ae.rama_clave "+
			  "   AND r.ssec_clave = ae.ssec_clave "+ 
			  "   AND r.sect_clave = ae.sect_clave "+
			  "   AND sb.ssec_clave = r.ssec_clave "+
			  "   AND sb.sect_clave = r.sect_clave "+
			  "   AND sc.sect_clave = sb.sect_clave "+
			  "   AND ac.clave = lr.acre_clave "+
			  "   AND mn.clave = lr.mone_clave "+
			  "   AND fn.clave = lr.fuen_clave "+
			  "   AND rb.clave = lr.rubr_clave "+
			  "   AND un.clave = lr.unid_clave "+
			  "   AND cn.clave = lr.conc_clave "+
			  "   AND es.clave = lr.estt_clave "+
			  "   and est.CLAVE  = m.ESTA_CLAVE "+
			  "   and lr.RGLO_NUMERO_CREDITO =  s.IG_NUMERO_PRESTAMO "+
			  "   and s.ic_moneda = mo.ic_moneda "+
			  "   and lr.CG_VALIDA_MONTO = 'S'  ");
			  
		  
         if (!"".equals(ic_if) )  {
				 qrySentencia.append( " and s.ic_if  = ? ");
				  conditions.add(ic_if);
			 }
			 
			  if (!"".equals(no_Prestamo) )  {
				 qrySentencia.append( " and s.IG_NUMERO_PRESTAMO  = ? ");
				  conditions.add(no_Prestamo);
			 }
			 
			  if ( !"".equals(fechaOperacionIni ) &&   !"".equals(fechaOperacionFin ) )   {
				qrySentencia.append(" and lr.RGLO_FECHA_OPERACION >= TO_DATE ( ?, 'dd/mm/yyyy') ");
				qrySentencia.append(" and  lr.RGLO_FECHA_OPERACION < TO_DATE (?, 'dd/mm/yyyy') + 1 " );
				conditions.add(fechaOperacionIni);
				conditions.add(fechaOperacionFin);
			}
					
       
			 
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		

		try {
				
			
		
				
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {			
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	/**
	 * 
	 * @return 
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
		
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		int total = 0;
		String SaldoCredito = "", SaldoFondo ="";
		
		try { 
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);					
		
			contenidoArchivo.append(" Intermediario Financiero,  N�mero de Pr�stamo , Fecha Operaci�n ,  	Moneda,  	Fecha,  " +
			" 	Nombre,  Nombre 2 ,  Apellido Paterno, Apellido Materno ,  	RFC,  Importe ,  Producto,  Actividad ,  Estados,  Municipio , "+
			" Estrato ,  Ventas, No. Empleados  \n ");			
			
			while (rs.next())	{	  
		
				String NOMBRE_IF = (rs.getString("NOMBRE_IF") == null) ? "" : rs.getString("NOMBRE_IF");
				String NO_PRESTAMO = (rs.getString("NO_PRESTAMO") == null) ? "" : rs.getString("NO_PRESTAMO");
				String FECHA_OPERACION = (rs.getString("FECHA_OPERACION") == null) ? "" : rs.getString("FECHA_OPERACION");
				String MONEDA = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
				String FECHA = (rs.getString("FECHA") == null) ? "" : rs.getString("FECHA");
				String NOMBRE = (rs.getString("NOMBRE") == null) ? "" : rs.getString("NOMBRE");
				String NOMBRE2 = (rs.getString("NOMBRE2") == null) ? "" : rs.getString("NOMBRE2");
				String APE_PATERNO = (rs.getString("APE_PATERNO") == null) ? "" : rs.getString("APE_PATERNO");
				String APE_MATERNO = (rs.getString("APE_MATERNO") == null) ? "" : rs.getString("APE_MATERNO");
				String RFC = (rs.getString("RFC") == null) ? "" : rs.getString("RFC");
				String IMPORTE = (rs.getString("IMPORTE") == null) ? "" : rs.getString("IMPORTE");
				String PRODUCTO = (rs.getString("PRODUCTO") == null) ? "" : rs.getString("PRODUCTO");
				String ACT_ECONOMICA = (rs.getString("ACT_ECONOMICA") == null) ? "" : rs.getString("ACT_ECONOMICA");
				String ESTADO = (rs.getString("ESTADO") == null) ? "" : rs.getString("ESTADO");
				String MUNICIPIO = (rs.getString("MUNICIPIO") == null) ? "" : rs.getString("MUNICIPIO");
				String ESTRATO = (rs.getString("ESTRATO") == null) ? "" : rs.getString("ESTRATO");
				String VENTAS = (rs.getString("VENTAS") == null) ? "" : rs.getString("VENTAS");
				String NO_EMPLEADOS = (rs.getString("NO_EMPLEADOS") == null) ? "" : rs.getString("NO_EMPLEADOS");
							
				
				contenidoArchivo.append(
					NOMBRE_IF.replace(',',' ')+","+
					NO_PRESTAMO.replace(',',' ')+","+ 
					FECHA_OPERACION.replace(',',' ')+","+ 
					MONEDA.replace(',',' ')+","+ 
					FECHA.replace(',',' ')+","+ 
					NOMBRE.replace(',',' ')+","+ 
					NOMBRE2.replace(',',' ')+","+ 
					APE_PATERNO.replace(',',' ')+","+ 
					APE_MATERNO.replace(',',' ')+","+ 
					RFC.replace(',',' ')+","+ 
					IMPORTE.replace(',',' ')+","+ 
					PRODUCTO.replace(',',' ')+","+ 
					ACT_ECONOMICA.replace(',',' ')+","+ 
					ESTADO.replace(',',' ')+","+ 
					MUNICIPIO.replace(',',' ')+","+ 
					ESTRATO.replace(',',' ')+","+ 
					VENTAS.replace(',',' ')+","+ 
					NO_EMPLEADOS.replace(',',' ')+","+ 
				"\n");		
			
				total++;
				if(total==1000){					
					total=0;	
					buffer.write(contenidoArchivo.toString());
					contenidoArchivo = new StringBuffer();//Limpio  
				}
				
			}//while(rs.next()){
				
			buffer.write(contenidoArchivo.toString());
			buffer.close();	
			contenidoArchivo = new StringBuffer();//Limpio    
			
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo;
	}

	
	
	
	
	
	public List getConditions() {  return conditions;  }  
	public String getPaginaNo() { return paginaNo; 	}
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getFechaOperacionIni() {
		return fechaOperacionIni;
	}

	public void setFechaOperacionIni(String fechaOperacionIni) {
		this.fechaOperacionIni = fechaOperacionIni;
	}

	public String getFechaOperacionFin() {
		return fechaOperacionFin;
	}

	public void setFechaOperacionFin(String fechaOperacionFin) {
		this.fechaOperacionFin = fechaOperacionFin;
	}

	public String getNo_Prestamo() {
		return no_Prestamo;
	}

	public void setNo_Prestamo(String no_Prestamo) {
		this.no_Prestamo = no_Prestamo;
	}


}