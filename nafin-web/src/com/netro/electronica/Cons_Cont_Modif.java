package com.netro.electronica;

import com.netro.pdf.ComunesPDF;
import com.netro.xls.ComunesXLS;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.CQueryHelperRegExtJS;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class Cons_Cont_Modif implements  IQueryGeneratorRegExtJS{
    private static final Log log = ServiceLocator.getInstance().getLog(Cons_Cont_Modif.class);
    private List conditions;
    private String iNoCliente,fechaCargaIni,fechaCargaFin,fechaCargaOpeIni,fechaCargaOpeFin , cveIF, tipoUsuario, strUsuario;

    public Cons_Cont_Modif(){}

    public String getDocumentSummaryQueryForIds(List pageIds) {
        log.info("getDocumentSummaryQueryForIds");
            //int i=0;
            StringBuffer qrySentencia       = new StringBuffer();
            StringBuffer condicion          = new StringBuffer();
            conditions              = new ArrayList();


            if (!("".equals(cveIF)) && cveIF!=null){
                    condicion.append("   AND ope_contrato_if.ic_if = ? ");
                    conditions.add(cveIF);
            }
    
            if (!"".equals(fechaCargaIni) && fechaCargaIni!=null ){
                    condicion.append("    AND DF_FECHA_FIRMA >= TO_DATE(?,'dd/mm/yyyy') ");
                    conditions.add(fechaCargaIni);
            }
            
            if (!"".equals(fechaCargaFin) && fechaCargaFin!=null ){
                    condicion.append("    AND DF_FECHA_FIRMA < TO_DATE(?,'dd/mm/yyyy')+1 ");
                    conditions.add(fechaCargaFin);
            }
    
            if (!"".equals(fechaCargaOpeIni) && fechaCargaOpeIni!=null ){
                condicion.append("    AND DF_FIRMA_OE >= TO_DATE(?,'dd/mm/yyyy') ");
                conditions.add(fechaCargaOpeIni);
            }
            
            if (!"".equals(fechaCargaOpeFin) && fechaCargaOpeFin!=null ){
                condicion.append("    AND DF_FIRMA_OE < TO_DATE(?,'dd/mm/yyyy')+1 ");
                conditions.add(fechaCargaOpeFin);
            }            
            
            /*qrySentencia.append(
                            " select IC_CONTRATO,CG_CONTRATO_OE,TIPO, to_char(DF_FECHA_FIRMA, 'dd/mm/yyyy')  as DF_FECHA_FIRMA, to_char(DF_FIRMA_OE, 'dd/mm/yyyy')  as DF_FIRMA_OE,ic_child,ic_if, CG_RAZON_SOCIAL, " +
                            " (CASE WHEN modifi IS NULL THEN to_char(IC_CONTRATO) ELSE to_char(cont)||'.'||to_char(modifi)end) num_cont from( " + 
                            "select IC_CONTRATO,CG_CONTRATO_OE,tipo,DF_FECHA_FIRMA, DF_FIRMA_OE,ic_child, ic_if,CG_RAZON_SOCIAL,cont, modifi from( " + 
                            "select IC_CONTRATO,CG_CONTRATO_OE,'Contrato' tipo     ,DF_FECHA_FIRMA, DF_FIRMA_OE, 0 ic_child, comcat_if.ic_if, comcat_if.CG_RAZON_SOCIAL,null cont, null modifi from ope_contrato_if, comcat_if " + 
                            "where ope_contrato_if.ic_if =comcat_if.ic_if " + 
                            "union all " + 
                            "select IC_MODIFICATORIO,CG_CONTRATO_OE,'Modificatorio' tipo,DF_FECHA_FIRMA, DF_FIRMA_OE, IC_CONTRATO ic_child, null ic_if, null CG_RAZON_SOCIAL, IC_CONTRATO cont, IC_MODIFICATORIO modifi from ope_modificatorio_if " + 
                            "where ic_contrato in(select IC_CONTRATO from ope_contrato_if)) " + 
                            "where ((level = 1 and tipo = 'Contrato') or (level = 2)) " + 
                            "connect by prior IC_CONTRATO = ic_child " + 
                            "order siblings by IC_CONTRATO desc) contratos " +
                            condicion.toString()+ ")");*/
            qrySentencia.append(
                " SELECT                                                                          "+ 
                "     ic_contrato,                                                                "+ 
                "     ic_modificatorio,                                                           "+ 
                "     cg_contrato_oe,                                                             "+ 
                "     tipo,                                                                       "+ 
                "     TO_CHAR(df_fecha_firma, 'dd/mm/yyyy') AS df_fecha_firma,                    "+ 
                "     TO_CHAR(df_firma_oe, 'dd/mm/yyyy') AS df_firma_oe,                          "+ 
                "     ic_if,                                                                      "+ 
                "     cg_razon_social,                                                            "+ 
                "     DECODE(ic_modificatorio, 0, ic_contrato, ic_contrato                        "+ 
                "                                              || '.'                             "+ 
                "                                              || ic_modificatorio) AS num_cont   "+ 
                " FROM                                                                            "+ 
                "     (                                                                           "+ 
                "         SELECT                                                                  "+ 
                "             ic_contrato,                                                        "+ 
                "             0 AS ic_modificatorio,                                              "+ 
                "             cg_contrato_oe,                                                     "+ 
                "             'Contrato' tipo,                                                    "+ 
                "             df_fecha_firma,                                                     "+ 
                "             df_firma_oe,                                                        "+ 
                "             comcat_if.ic_if,                                                    "+ 
                "             comcat_if.cg_razon_social                                           "+ 
                "         FROM                                                                    "+ 
                "             ope_contrato_if,                                                    "+ 
                "             comcat_if                                                           "+ 
                "         WHERE                                                                   "+ 
                "             ope_contrato_if.ic_if = comcat_if.ic_if                             ");
                qrySentencia.append(condicion.toString());
                qrySentencia.append(
                "         UNION ALL                                                               "+ 
                "         SELECT                                                                  "+ 
                "             c.ic_contrato,                                                      "+ 
                "             m.ic_modificatorio,                                                 "+ 
                "             m.cg_contrato_oe,                                                   "+ 
                "             'Modificatorio' tipo,                                               "+ 
                "             m.df_fecha_firma,                                                   "+ 
                "             m.df_firma_oe,                                                      "+ 
                "             comcat_if.ic_if,                                                    "+ 
                "             comcat_if.cg_razon_social                                           "+ 
                "         FROM                                                                    "+ 
                "             ope_modificatorio_if   m,                                           "+ 
                "             ope_contrato_if        c,                                           "+ 
                "             comcat_if                                                           "+ 
                "         WHERE                                                                   "+ 
                "             m.ic_contrato = c.ic_contrato                                       "+ 
                "             AND c.ic_if = comcat_if.ic_if                                       ");
                    condicion.delete(0, condicion.length());
                    if (!("".equals(cveIF)) && cveIF!=null){
                            condicion.append("   AND c.ic_if = ? ");
                            conditions.add(cveIF);
                    }
                    
                    if (!"".equals(fechaCargaIni) && fechaCargaIni!=null ){
                            condicion.append("    AND m.DF_FECHA_FIRMA >= TO_DATE(?,'dd/mm/yyyy') ");
                            conditions.add(fechaCargaIni);
                    }
                    
                    if (!"".equals(fechaCargaFin) && fechaCargaFin!=null ){
                            condicion.append("    AND m.DF_FECHA_FIRMA < TO_DATE(?,'dd/mm/yyyy')+1 ");
                            conditions.add(fechaCargaFin);
                    }
                    
                    if (!"".equals(fechaCargaOpeIni) && fechaCargaOpeIni!=null ){
                        condicion.append("    AND m.DF_FIRMA_OE >= TO_DATE(?,'dd/mm/yyyy') ");
                        conditions.add(fechaCargaOpeIni);
                    }
                    
                    if (!"".equals(fechaCargaOpeFin) && fechaCargaOpeFin!=null ){
                        condicion.append("    AND m.DF_FIRMA_OE < TO_DATE(?,'dd/mm/yyyy')+1 ");
                        conditions.add(fechaCargaOpeFin);
                    }             
                qrySentencia.append(condicion.toString());
                condicion.delete(0, condicion.length());
                qrySentencia.append("     ) contratos                                                                 ");
                condicion.append(" WHERE contratos.IC_CONTRATO in (");
                for(int j = 0; j < pageIds.size(); j++){
                    
                        List lItem = (ArrayList)pageIds.get(j);
                                condicion.append("?,");
                                conditions.add(new String(lItem.get(0).toString()));      
                        }
                condicion.deleteCharAt(condicion.length()-1);
                condicion.append(")");
                
                qrySentencia.append(condicion.toString()+
                " ORDER BY                                                                        "+ 
                "     ic_if, ic_contrato, ic_modificatorio                                        ");
            
            log.debug("Query IDS"+qrySentencia.toString());
            log.debug(" query IDs"+conditions.toString());
            System.out.println("Query IDS"+qrySentencia.toString());
            System.out.println(" query IDs"+conditions.toString());
            
            return qrySentencia.toString();
    
    }     
    
    public String getAggregateCalculationQuery() {
            log.info("getAggregateCalculationQuery");
            
            StringBuffer qrySentencia = new StringBuffer();
            StringBuffer condicion    = new StringBuffer();
            
                    
            try {
                    conditions              = new ArrayList();

                if (!("".equals(cveIF)) && cveIF!=null){
                        condicion.append("   AND comcat_if.ic_if = ? ");
                        conditions.add(cveIF);
                }

                if (!"".equals(fechaCargaIni) && fechaCargaIni!=null ){
                        condicion.append("    AND DF_FECHA_FIRMA >= TO_DATE(?,'dd/mm/yyyy') ");
                        conditions.add(fechaCargaIni);
                }
                
                if (!"".equals(fechaCargaFin) && fechaCargaFin!=null ){
                        condicion.append("    AND DF_FECHA_FIRMA < TO_DATE(?,'dd/mm/yyyy')+1 ");
                        conditions.add(fechaCargaFin);
                }

                if (!"".equals(fechaCargaOpeIni) && fechaCargaOpeIni!=null ){
                    condicion.append("    AND DF_FIRMA_OE >= TO_DATE(?,'dd/mm/yyyy') ");
                    conditions.add(fechaCargaOpeIni);
                }
                
                if (!"".equals(fechaCargaOpeFin) && fechaCargaOpeFin!=null ){
                    condicion.append("    AND DF_FIRMA_OE < TO_DATE(?,'dd/mm/yyyy')+1 ");
                    conditions.add(fechaCargaOpeFin);
                }

                
                /*qrySentencia.append(" select count(IC_CONTRATO) from( " + 
                " select IC_CONTRATO,CG_CONTRATO_OE,tipo,DF_FECHA_FIRMA, DF_FIRMA_OE,ic_child from( " + 
                " select IC_CONTRATO,CG_CONTRATO_OE,'Contrato' tipo,DF_FECHA_FIRMA, DF_FIRMA_OE, 0 ic_child from ope_contrato_if where 1=1 ");
                qrySentencia.append(condicion.toString());
                qrySentencia.append("union all " + 
                " select IC_MODIFICATORIO,CG_CONTRATO_OE,'Modificatorio' tipo,DF_FECHA_FIRMA, DF_FIRMA_OE, IC_CONTRATO ic_child from ope_modificatorio_if " + 
                " where ic_contrato in(select IC_CONTRATO from ope_contrato_if where 1=1 ");*/
                qrySentencia.append(
                    " SELECT                                                "+
                    "     count(ic_contrato)                                "+
                    " FROM                                                  "+
                    "     (                                                 "+
                    "         SELECT                                        "+
                    "             ic_contrato,                              "+
                    "             0 AS ic_modificatorio,                    "+
                    "             comcat_if.ic_if,                          "+
                    "             df_fecha_firma,                           "+
                    "             df_firma_oe                               "+
                    "         FROM                                          "+
                    "             ope_contrato_if,                          "+
                    "             comcat_if                                 "+
                    "         WHERE                                         "+
                    "             ope_contrato_if.ic_if = comcat_if.ic_if   ");
                    qrySentencia.append(condicion.toString());
                    qrySentencia.append(
                    "         UNION ALL                                     "+
                    "         SELECT                                        "+
                    "             c.ic_contrato,                            "+
                    "             m.ic_modificatorio,                       "+
                    "             comcat_if.ic_if,                          "+
                    "             m.df_fecha_firma,                         "+
                    "             m.df_firma_oe                             "+
                    "         FROM                                          "+
                    "             ope_modificatorio_if   m,                 "+
                    "             ope_contrato_if        c,                 "+
                    "             comcat_if                                 "+
                    "         WHERE                                         "+
                    "             m.ic_contrato = c.ic_contrato             "+
                    "             AND c.ic_if = comcat_if.ic_if             ");
                    //"     ) contratos                                       ");
                condicion.delete(0, condicion.length());
                if (!("".equals(cveIF)) && cveIF!=null){
                        condicion.append("   AND comcat_if.ic_if = ? ");
                        conditions.add(cveIF);
                }

                if (!"".equals(fechaCargaIni) && fechaCargaIni!=null ){
                        condicion.append("    AND m.DF_FECHA_FIRMA >= TO_DATE(?,'dd/mm/yyyy') ");
                        conditions.add(fechaCargaIni);
                }
                
                if (!"".equals(fechaCargaFin) && fechaCargaFin!=null ){
                        condicion.append("    AND m.DF_FECHA_FIRMA < TO_DATE(?,'dd/mm/yyyy')+1 ");
                        conditions.add(fechaCargaFin);
                }

                if (!"".equals(fechaCargaOpeIni) && fechaCargaOpeIni!=null ){
                    condicion.append("    AND m.DF_FIRMA_OE >= TO_DATE(?,'dd/mm/yyyy') ");
                    conditions.add(fechaCargaOpeIni);
                }
                
                if (!"".equals(fechaCargaOpeFin) && fechaCargaOpeFin!=null ){
                    condicion.append("    AND m.DF_FIRMA_OE < TO_DATE(?,'dd/mm/yyyy')+1 ");
                    conditions.add(fechaCargaOpeFin);
                }
                qrySentencia.append(condicion.toString());
                qrySentencia.append(" ) ");
                
                /*qrySentencia.append(")) where ((level = 1 and tipo = 'Contrato') or (level = 2))" + 
                " connect by prior IC_CONTRATO = ic_child " +
                " order siblings by IC_CONTRATO desc)");*/
                log.info("getAggregateCalculationQuery: "+qrySentencia);
                log.info("getAggregateCalculationQuery Condiciones: "+conditions.toString());
                
            }catch(Exception e){
                    System.out.println("Cons_Cont_Modif::getAggregateCalculationQuery "+e);
            }
            return qrySentencia.toString();
    }
    
    public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
            String nombreArchivo = "";
            String linea = "";
            OutputStreamWriter writer = null;
            BufferedWriter buffer = null;
            
            
            if("PDF".equals(tipo)){
                    try{
                            HttpSession session = request.getSession();
                            nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
                            ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
                            
                            String meses[]                  = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
                            String fechaActual      = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
                            String diaActual                = fechaActual.substring(0,2);
                            String mesActual                = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
                            String anioActual               = fechaActual.substring(6,10);
                            String horaActual               = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
                            
                            pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
                            session.getAttribute("iNoNafinElectronico").toString(),
                            (String)session.getAttribute("sesExterno"),
                            (String)session.getAttribute("strNombre"),
                            (String)session.getAttribute("strNombreUsuario"),
                            (String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
                            
                            pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
                            
                            pdfDoc.setTable(18, 100);
                            pdfDoc.setCell("No. Docto Final","celda01",ComunesPDF.CENTER);
                            pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
                            pdfDoc.setCell("Dist.","celda01",ComunesPDF.CENTER);
                            pdfDoc.setCell("Tipo de Cr�dito","celda01",ComunesPDF.CENTER);
                            pdfDoc.setCell("Resp. Pago de Inter�s","celda01",ComunesPDF.CENTER);
                            pdfDoc.setCell("Tipo de Cobranza","celda01",ComunesPDF.CENTER);
                            pdfDoc.setCell("Fecha de Operaci�n","celda01",ComunesPDF.CENTER);
                            pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
                            pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
                            pdfDoc.setCell("Ref. Tasa Inter�s","celda01",ComunesPDF.CENTER);
                            pdfDoc.setCell("Tasa Inter�s","celda01",ComunesPDF.CENTER);
                            pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);
                            pdfDoc.setCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
                            pdfDoc.setCell("Monto Inter�s","celda01",ComunesPDF.CENTER);
                            pdfDoc.setCell("Tipo Cobro Inter�s","celda01",ComunesPDF.CENTER);
                            pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
                            pdfDoc.setCell("Fecha de rechazo","celda01",ComunesPDF.CENTER);
                            pdfDoc.setCell("Causa","celda01",ComunesPDF.CENTER);

                            
                            while (reg.next()){
                                    String numeroDocumento          = (reg.getString(1) == null) ? "" : reg.getString(1);
                                    String epo                                              = (reg.getString("EPO") == null) ? "" : reg.getString("EPO");
                                    String dist                                             = (reg.getString("PYME") == null) ? "" : reg.getString("PYME");
                                    String tipoCredito                      = (reg.getString("CG_TIPO_CREDITO") == null) ? "" : reg.getString("CG_TIPO_CREDITO");
                                    String respInt                                  = (reg.getString("RESPONSABLE") == null) ? "" : reg.getString("RESPONSABLE");
                                    String tipoCob                                  = (reg.getString("COBRANZA") == null) ? "" : reg.getString("COBRANZA");
                                    String fOpera                                   = (reg.getString("FECHA_OPER") == null) ? "" : reg.getString("FECHA_OPER");
                                    String moneda                                   = (reg.getString("CD_NOMBRE") == null) ? "" : reg.getString("CD_NOMBRE");
                                    String monto                                    = (reg.getString("MONTO_VAL") ==        null) ? "" : reg.getString("MONTO_VAL");
                                    String refTasa                                  = (reg.getString("CT_REFERENCIA") == null) ? "" : reg.getString("CT_REFERENCIA");
                                    String tasaInt                                  = (reg.getString("TASA_INT") == null) ? "" : reg.getString("TASA_INT");
                                    String plazo                                    = (reg.getString("IG_PLAZO_DESCUENTO") == null) ? "" : reg.getString("IG_PLAZO_DESCUENTO");
                                    String fVenc                                    = (reg.getString("FECHA_VENC") ==       null) ? "" : reg.getString("FECHA_VENC");
                                    String montoInteres                     = (reg.getString("MONTO_INT") == null) ? "" : reg.getString("MONTO_INT");
                                    String cobroInt                         = (reg.getString("TIPO_COB_INT") == null) ? "" : reg.getString("TIPO_COB_INT");
                                    String estatus                                  = (reg.getString("CAMBIOESTATUS") == null) ? "" : reg.getString("CAMBIOESTATUS");
                                    String fechaRe                                  = (reg.getString("FECHACAMBIO") == null) ? "" : reg.getString("FECHACAMBIO");
                                    String causa                                    = (reg.getString("CAMBIOMOTIVO") == null) ? "" : reg.getString("CAMBIOMOTIVO");

                                    
                                    pdfDoc.setCell(numeroDocumento,"formas",ComunesPDF.CENTER);
                                    pdfDoc.setCell(epo,"formas",ComunesPDF.CENTER);
                                    pdfDoc.setCell(dist,"formas",ComunesPDF.CENTER);
                                    pdfDoc.setCell(tipoCredito,"formas",ComunesPDF.CENTER);
                                    pdfDoc.setCell(respInt,"formas",ComunesPDF.CENTER);
                                    pdfDoc.setCell(tipoCob,"formas",ComunesPDF.CENTER);
                                    pdfDoc.setCell(fOpera,"formas",ComunesPDF.RIGHT);
                                    pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
                                    pdfDoc.setCell("$"+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.CENTER);
                                    pdfDoc.setCell(refTasa,"formas",ComunesPDF.CENTER);
                                    pdfDoc.setCell(Comunes.formatoDecimal(tasaInt,2)+"%","formas",ComunesPDF.CENTER);
                                    pdfDoc.setCell(plazo,"formas",ComunesPDF.CENTER);
                                    pdfDoc.setCell(fVenc,"formas",ComunesPDF.CENTER);
                                    pdfDoc.setCell("$"+Comunes.formatoDecimal(montoInteres,2),"formas",ComunesPDF.CENTER);
                                    pdfDoc.setCell(cobroInt,"formas",ComunesPDF.CENTER);
                                    pdfDoc.setCell(estatus,"formas",ComunesPDF.CENTER);
                                    pdfDoc.setCell(fechaRe,"formas",ComunesPDF.CENTER);
                                    pdfDoc.setCell(causa,"formas",ComunesPDF.CENTER);
                                    
                            }
                            //pdfDoc.addTable();
                            
                            
                            CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS();//toma el valor de sesi�n
                            Registros regi = queryHelper.getResultCount(request);
                            
                    
                            
                                    
                                    pdfDoc.setCell("MONEDA ","celda01rep",ComunesPDF.LEFT,6);
                                    pdfDoc.setCell("TOTAL REGISTROS","celda01rep",ComunesPDF.CENTER,6);
                                    pdfDoc.setCell("MONTO","celda01rep",ComunesPDF.CENTER,6);

                                    while(regi.next()){
                                            pdfDoc.setCell("TOTAL "+regi.getString(2),"celda01rep",ComunesPDF.LEFT,6);
                                            pdfDoc.setCell(regi.getString(3),"celda01rep",ComunesPDF.CENTER,6);
                                            pdfDoc.setCell("$"+regi.getString(4),"celda01rep",ComunesPDF.CENTER,6);

                                    }
                            
                                    pdfDoc.addTable();
                                    pdfDoc.endDocument();
                                                    }catch(Throwable e){
                            throw new AppException("Error al generar el archivo",e);
                    }finally{
                            try{
                            }catch(Exception e){}
                    }
            }       
            return nombreArchivo;
    }
    
    public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
            CreaArchivo creaArchivo = new CreaArchivo();
            String nombreArchivo    = creaArchivo.nombreArchivo()+".xls";
            ComunesXLS xls = new ComunesXLS(path+nombreArchivo);
            try {
                    
                    xls.setTabla(6);
                    xls.setCelda("Nombre del Intermediario", "celda01", ComunesXLS.CENTER, 1);
                    xls.setCelda("Nombre Contrato", "celda01", ComunesXLS.CENTER, 1);
                    xls.setCelda("Tipo", "celda01", ComunesXLS.CENTER, 1);
                    xls.setCelda("N�mero", "celda01", ComunesXLS.CENTER, 1);
                    xls.setCelda("Fecha Firma", "celda01", ComunesXLS.CENTER, 1);
                    xls.setCelda("Fecha Operaci�n Electr�nica", "celda01", ComunesXLS.CENTER, 1);
                     
                    while (rs.next()) {
                            String nombreIF              = (rs.getString("CG_RAZON_SOCIAL") == null) ? "" : rs.getString("CG_RAZON_SOCIAL");
                            String nombreCont              = (rs.getString("CG_CONTRATO_OE") == null) ? "" : rs.getString("CG_CONTRATO_OE");
                            String tipoCont                 = (rs.getString("TIPO") == null) ? "" : rs.getString("TIPO");
                            String numCont                 = (rs.getString("NUM_CONT") == null) ? "" : rs.getString("NUM_CONT");
                            String fechaFirma             = (rs.getString("DF_FECHA_FIRMA") == null) ? "" : rs.getString("DF_FECHA_FIRMA");
                            String fechaFirmaOpe                       = (rs.getString("DF_FIRMA_OE") == null) ? "" : rs.getString("DF_FIRMA_OE");
                                    
                            xls.setCelda(nombreIF, "formas", ComunesXLS.CENTER, 1);
                            xls.setCelda(nombreCont, "formas", ComunesXLS.CENTER, 1);
                            xls.setCelda(tipoCont, "formas", ComunesXLS.CENTER, 1);
                            xls.setCelda(numCont, "formas", ComunesXLS.CENTER, 1);
                            xls.setCelda(fechaFirma, "formas", ComunesXLS.CENTER, 1);
                            xls.setCelda(fechaFirmaOpe, "formas", ComunesXLS.LEFT, 1);
                    }
                    
                    CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS();//toma el valor de sesi�n
                    Registros regi = queryHelper.getResultCount(request);
                            
                    while(regi.next()){
                            //linea="TOTAL "+regi.getString(2)+","+regi.getString(3)+","+"$"+regi.getString(4)+"\n";
                            //buffer.write(linea);
                    }
                                    
                    xls.cierraTabla();
                    xls.cierraXLS();
              
            }catch (Throwable e) {
                    throw new AppException("Error al generar el archivo",e);
            }
            finally {
                    try {
                            rs.close();
                    } catch(Exception e) {}
            }
            
            return nombreArchivo;
             }

    public String getDocumentQuery() {
            log.info("getDocumentQuery");
    StringBuffer qrySentencia = new StringBuffer();
    StringBuffer condicion    = new StringBuffer();                    
            try {
            
                    
            
                    conditions=new ArrayList();

                    if (!("".equals(cveIF)) && cveIF!=null){
                            condicion.append("   AND comcat_if.ic_if = ? ");
                            conditions.add(cveIF);
                    }

                    if (!"".equals(fechaCargaIni) && fechaCargaIni!=null ){
                            condicion.append("    AND DF_FECHA_FIRMA >= TO_DATE(?,'dd/mm/yyyy') ");
                            conditions.add(fechaCargaIni);
                    }
                    
                    if (!"".equals(fechaCargaFin) && fechaCargaFin!=null ){
                            condicion.append("    AND DF_FECHA_FIRMA < TO_DATE(?,'dd/mm/yyyy')+1 ");
                            conditions.add(fechaCargaFin);
                    }

                if (!"".equals(fechaCargaOpeIni) && fechaCargaOpeIni!=null ){
                        condicion.append("    AND DF_FIRMA_OE >= TO_DATE(?,'dd/mm/yyyy') ");
                        conditions.add(fechaCargaOpeIni);
                }
                
                if (!"".equals(fechaCargaOpeFin) && fechaCargaOpeFin!=null ){
                        condicion.append("    AND DF_FIRMA_OE < TO_DATE(?,'dd/mm/yyyy')+1 ");
                        conditions.add(fechaCargaOpeFin);
                }


            
                /*qrySentencia.append(" select IC_CONTRATO from( " + 
                " select IC_CONTRATO,CG_CONTRATO_OE,tipo,DF_FECHA_FIRMA, DF_FIRMA_OE,ic_child from( " + 
                " select IC_CONTRATO,CG_CONTRATO_OE,'Contrato' tipo,DF_FECHA_FIRMA, DF_FIRMA_OE, 0 ic_child from ope_contrato_if where 1=1 ");
                qrySentencia.append(condicion.toString());
                qrySentencia.append("union all " + 
                " select IC_MODIFICATORIO,CG_CONTRATO_OE,'Modificatorio' tipo,DF_FECHA_FIRMA, DF_FIRMA_OE, IC_CONTRATO ic_child from ope_modificatorio_if " + 
                " where ic_contrato in(select IC_CONTRATO from ope_contrato_if where 1=1 ");*/

                qrySentencia.append(
                    " SELECT                                                "+
                    "     ic_contrato                                       "+
                    " FROM                                                  "+
                    "     (                                                 "+
                    "         SELECT                                        "+
                    "             ic_contrato,                              "+
                    "             0 AS ic_modificatorio,                    "+
                    "             comcat_if.ic_if,                          "+
                    "             df_fecha_firma,                           "+
                    "             df_firma_oe                               "+
                    "         FROM                                          "+
                    "             ope_contrato_if,                          "+
                    "             comcat_if                                 "+
                    "         WHERE                                         "+
                    "             ope_contrato_if.ic_if = comcat_if.ic_if   ");
                    qrySentencia.append(condicion.toString());
                    qrySentencia.append(
                    "         UNION ALL                                     "+
                    "         SELECT                                        "+
                    "             c.ic_contrato,                            "+
                    "             m.ic_modificatorio,                       "+
                    "             comcat_if.ic_if,                          "+
                    "             m.df_fecha_firma,                         "+
                    "             m.df_firma_oe                             "+
                    "         FROM                                          "+
                    "             ope_modificatorio_if   m,                 "+
                    "             ope_contrato_if        c,                 "+
                    "             comcat_if                                 "+
                    "         WHERE                                         "+
                    "             m.ic_contrato = c.ic_contrato             "+
                    "             AND c.ic_if = comcat_if.ic_if             ");
                
                condicion.delete(0, condicion.length());

                if (!("".equals(cveIF)) && cveIF!=null){
                        condicion.append("   AND comcat_if.ic_if = ? ");
                        conditions.add(cveIF);
                }

                if (!"".equals(fechaCargaIni) && fechaCargaIni!=null ){
                        condicion.append("    AND m.DF_FECHA_FIRMA >= TO_DATE(?,'dd/mm/yyyy') ");
                        conditions.add(fechaCargaIni);
                }
                
                if (!"".equals(fechaCargaFin) && fechaCargaFin!=null ){
                        condicion.append("    AND m.DF_FECHA_FIRMA < TO_DATE(?,'dd/mm/yyyy')+1 ");
                        conditions.add(fechaCargaFin);
                }

                if (!"".equals(fechaCargaOpeIni) && fechaCargaOpeIni!=null ){
                    condicion.append("    AND m.DF_FIRMA_OE >= TO_DATE(?,'dd/mm/yyyy') ");
                    conditions.add(fechaCargaOpeIni);
                }
                
                if (!"".equals(fechaCargaOpeFin) && fechaCargaOpeFin!=null ){
                    condicion.append("    AND m.DF_FIRMA_OE < TO_DATE(?,'dd/mm/yyyy')+1 ");
                    conditions.add(fechaCargaOpeFin);
                }
                qrySentencia.append(condicion.toString());
                /*qrySentencia.append(")) where ((level = 1 and tipo = 'Contrato') or (level = 2))" + 
                " connect by prior IC_CONTRATO = ic_child " +
                " order siblings by IC_CONTRATO desc)");*/
                qrySentencia.append(") order by ic_if, ic_contrato, ic_modificatorio ");
                    log.debug("EL query de la llave primaria: "+qrySentencia.toString());
                    log.debug("condicion: "+conditions.toString());
            }catch(Exception e){
                    System.out.println("Cons_Cont_Modif::getDocumentQueryException "+e);
            }
            return qrySentencia.toString();
    }
    
     public String getDocumentQueryFile() {
            log.info("getDocumentQueryFile");
            String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
            StringBuffer qrySentencia = new StringBuffer();
            StringBuffer condicion    = new StringBuffer();
            boolean existeTE = false;
            
            try {


                    conditions=new ArrayList();
                if (!("".equals(cveIF)) && cveIF!=null){
                        condicion.append("   AND ope_contrato_if.ic_if = ? ");
                        conditions.add(cveIF);
                }

                if (!"".equals(fechaCargaIni) && fechaCargaIni!=null ){
                        condicion.append("    AND DF_FECHA_FIRMA >= TO_DATE(?,'dd/mm/yyyy') ");
                        conditions.add(fechaCargaIni);
                }
                
                if (!"".equals(fechaCargaFin) && fechaCargaFin!=null ){
                        condicion.append("    AND DF_FECHA_FIRMA < TO_DATE(?,'dd/mm/yyyy')+1 ");
                        conditions.add(fechaCargaFin);
                }

                if (!"".equals(fechaCargaOpeIni) && fechaCargaOpeIni!=null ){
                    condicion.append("    AND DF_FIRMA_OE >= TO_DATE(?,'dd/mm/yyyy') ");
                    conditions.add(fechaCargaOpeIni);
                }
                
                if (!"".equals(fechaCargaOpeFin) && fechaCargaOpeFin!=null ){
                    condicion.append("    AND DF_FIRMA_OE < TO_DATE(?,'dd/mm/yyyy')+1 ");
                    conditions.add(fechaCargaOpeFin);
                }

                /*qrySentencia.append(
                                " select IC_CONTRATO,CG_CONTRATO_OE,TIPO, to_char(DF_FECHA_FIRMA, 'dd/mm/yyyy')  as DF_FECHA_FIRMA, to_char(DF_FIRMA_OE, 'dd/mm/yyyy')  as DF_FIRMA_OE,ic_child,ic_if, CG_RAZON_SOCIAL, " +
                                " (CASE WHEN modifi IS NULL THEN to_char(IC_CONTRATO) ELSE to_char(cont)||'.'||to_char(modifi)end) num_cont from( " + 
                                "select IC_CONTRATO,CG_CONTRATO_OE,tipo,DF_FECHA_FIRMA, DF_FIRMA_OE,ic_child, ic_if,CG_RAZON_SOCIAL,cont, modifi from( " + 
                                "select IC_CONTRATO,CG_CONTRATO_OE,'Contrato' tipo     ,DF_FECHA_FIRMA, DF_FIRMA_OE, 0 ic_child, comcat_if.ic_if, comcat_if.CG_RAZON_SOCIAL,null cont, null modifi from ope_contrato_if, comcat_if " + 
                                "where ope_contrato_if.ic_if =comcat_if.ic_if ");
                                qrySentencia.append(condicion.toString());
                                qrySentencia.append("union all " + 
                                "select IC_MODIFICATORIO,CG_CONTRATO_OE,'Modificatorio' tipo,DF_FECHA_FIRMA, DF_FIRMA_OE, IC_CONTRATO ic_child, null ic_if, null CG_RAZON_SOCIAL, IC_CONTRATO cont, IC_MODIFICATORIO modifi from ope_modificatorio_if " + 
                                "where ic_contrato in(select IC_CONTRATO from ope_contrato_if where 1=1");*/
                qrySentencia.append(
                    " SELECT                                                                          "+ 
                    "     ic_contrato,                                                                "+ 
                    "     ic_modificatorio,                                                           "+ 
                    "     cg_contrato_oe,                                                             "+ 
                    "     tipo,                                                                       "+ 
                    "     TO_CHAR(df_fecha_firma, 'dd/mm/yyyy') AS df_fecha_firma,                    "+ 
                    "     TO_CHAR(df_firma_oe, 'dd/mm/yyyy') AS df_firma_oe,                          "+ 
                    "     ic_if,                                                                      "+ 
                    "     cg_razon_social,                                                            "+ 
                    "     DECODE(ic_modificatorio, 0, ic_contrato, ic_contrato                        "+ 
                    "                                              || '.'                             "+ 
                    "                                              || ic_modificatorio) AS num_cont   "+ 
                    " FROM                                                                            "+ 
                    "     (                                                                           "+ 
                    "         SELECT                                                                  "+ 
                    "             ic_contrato,                                                        "+ 
                    "             0 AS ic_modificatorio,                                              "+ 
                    "             cg_contrato_oe,                                                     "+ 
                    "             'Contrato' tipo,                                                    "+ 
                    "             df_fecha_firma,                                                     "+ 
                    "             df_firma_oe,                                                        "+ 
                    "             comcat_if.ic_if,                                                    "+ 
                    "             comcat_if.cg_razon_social                                           "+ 
                    "         FROM                                                                    "+ 
                    "             ope_contrato_if,                                                    "+ 
                    "             comcat_if                                                           "+ 
                    "         WHERE                                                                   "+
                    "           ope_contrato_if.ic_if = comcat_if.ic_if                               ");
                    qrySentencia.append(condicion.toString());
                    qrySentencia.append(
                    "         UNION ALL                                                               "+ 
                    "         SELECT                                                                  "+ 
                    "             c.ic_contrato,                                                      "+ 
                    "             m.ic_modificatorio,                                                 "+ 
                    "             m.cg_contrato_oe,                                                   "+ 
                    "             'Modificatorio' tipo,                                               "+ 
                    "             m.df_fecha_firma,                                                   "+ 
                    "             m.df_firma_oe,                                                      "+ 
                    "             comcat_if.ic_if,                                                    "+ 
                    "             comcat_if.cg_razon_social                                           "+ 
                    "         FROM                                                                    "+ 
                    "             ope_modificatorio_if   m,                                           "+ 
                    "             ope_contrato_if        c,                                           "+ 
                    "             comcat_if                                                           "+ 
                    "         WHERE                                                                   "+ 
                    "             m.ic_contrato = c.ic_contrato                                       "+ 
                    "             AND c.ic_if = comcat_if.ic_if                                       ");

                                
                                condicion.delete(0, condicion.length());
                                if (!("".equals(cveIF)) && cveIF!=null){
                                        condicion.append("   AND c.ic_if = ? ");
                                        conditions.add(cveIF);
                                }
            
                                if (!"".equals(fechaCargaIni) && fechaCargaIni!=null ){
                                        condicion.append("    AND m.DF_FECHA_FIRMA >= TO_DATE(?,'dd/mm/yyyy') ");
                                        conditions.add(fechaCargaIni);
                                }
                                
                                if (!"".equals(fechaCargaFin) && fechaCargaFin!=null ){
                                        condicion.append("    AND m.DF_FECHA_FIRMA < TO_DATE(?,'dd/mm/yyyy')+1 ");
                                        conditions.add(fechaCargaFin);
                                }
            
                                if (!"".equals(fechaCargaOpeIni) && fechaCargaOpeIni!=null ){
                                    condicion.append("    AND m.DF_FIRMA_OE >= TO_DATE(?,'dd/mm/yyyy') ");
                                    conditions.add(fechaCargaOpeIni);
                                }
                                
                                if (!"".equals(fechaCargaOpeFin) && fechaCargaOpeFin!=null ){
                                    condicion.append("    AND m.DF_FIRMA_OE < TO_DATE(?,'dd/mm/yyyy')+1 ");
                                    conditions.add(fechaCargaOpeFin);
                                }
                                
                                qrySentencia.append(condicion.toString());
                        qrySentencia.append(
                "     ) contratos                                                                 "+
                " ORDER BY                                                                        "+ 
                "    ic_if, ic_contrato, ic_modificatorio                                         ");
                                /*qrySentencia.append(")) where ((level = 1 and tipo = 'Contrato') or (level = 2)) " + 
                                "connect by prior IC_CONTRATO = ic_child " + 
                                "order siblings by IC_CONTRATO desc) contratos");*/
                log.info(qrySentencia.toString());                
                log.info(conditions.toString());
            }catch(Exception e){
                    log.error(e.getMessage());
                    e.printStackTrace();
            }
            return qrySentencia.toString();
     }
     
     public List getConditions() {
            return conditions;
    }

    public void setINoCliente(String iNoCliente) {
            this.iNoCliente = iNoCliente;
    }

    public String getINoCliente() {
            return iNoCliente;
    }

    public void setFechaCargaIni(String fechaCargaIni) {
            this.fechaCargaIni = fechaCargaIni;
    }

    public String getFechaCargaIni() {
            return fechaCargaIni;
    }

    public void setCveIF(String cveIF) {
            this.cveIF = cveIF;
    }

    public String getCveIF() {
            return cveIF;
    }

    public void setFechaCargaFin(String fechaCargaFin) {
            this.fechaCargaFin = fechaCargaFin;
    }

    public String getFechaCargaFin() {
            return fechaCargaFin;
    }

    public void setFechaCargaOpeIni(String fechaCargaOpeIni) {
        this.fechaCargaOpeIni = fechaCargaOpeIni;
    }

    public String getFechaCargaOpeIni() {
        return fechaCargaOpeIni;
    }

    public void setFechaCargaOpeFin(String fechaCargaOpeFin) {
        this.fechaCargaOpeFin = fechaCargaOpeFin;
    }

    public String getFechaCargaOpeFin() {
        return fechaCargaOpeFin;
    }

    public void setTipoUsuario(String tipoUsuario) {
            this.tipoUsuario = tipoUsuario;
    }

    public String getTipoUsuario() {
            return tipoUsuario;
    }

    public void setStrUsuario(String strUsuario) {
            this.strUsuario = strUsuario;
    }

    public String getStrUsuario() {
            return strUsuario;
    }    
}
