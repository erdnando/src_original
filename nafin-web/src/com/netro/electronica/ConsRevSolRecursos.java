package com.netro.electronica;

import com.netro.pdf.ComunesPDF;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class ConsRevSolRecursos implements  IQueryGeneratorRegExtJS {	

	
	public ConsRevSolRecursos() {}   
	
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(ConsRevSolRecursos.class);  
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");  
	private String 	paginaOffset;
	private String 	paginaNo;
	
	private String numSolic;
	private String fecRecpIni;
	private String fecRecpFin;
	private String ic_if;
	private String auto;
	

  public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
		public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
	   log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
			
					
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();    
		conditions 		= new ArrayList();
	
	
		qrySentencia.append( 
			"SELECT ors.CG_USUARIO_AUTOR2,ors.CG_USUARIO_AUTOR1,ors.ic_solicitud cvesolicitud, ors.ic_if cveif, ors.ic_contrato cvecontrato, " +
				"       TO_CHAR (ors.df_solicitud, 'dd/mm/yyyy HH24:mi:ss') fecsolicitud, " +
				"       oc.cg_nombre_contrato nombrecontrato, " +
				"		  TO_CHAR (odi.df_firma_contrato, 'dd/mm/yyyy') fecfirmacontrato, " +
				"       odi.cg_contrato_oe nombrecontratooe, " +
				"       TO_CHAR (odi.df_firma_contrato_oe, 'dd/mm/yyyy') fecfirmacontratooe, " +
				"       ors.fn_monto_solicitud importesolic, cm.cd_nombre nombremoneda, " +
				"       ors.cg_des_recursos destinorecurso, " +
				"		  TO_CHAR (ors.df_pago_capital, 'dd/mm/yyyy') fecpagocapital, " +
				"       TO_CHAR (ors.df_pago_interes, 'dd/mm/yyyy') fecpagointeres,  " +
				"       TO_CHAR (ors.df_vencimiento, 'dd/mm/yyyy')  fecvencimiento, " +
				"       TO_CHAR (ors.df_asig_ejeop, 'dd/mm/yyyy HH24:mi:ss')  fecasignaejeop, " +
				"       ors.cg_nom_usuario_con1  nombreusrcon1, " +
				"       ors.ic_estatus_ope estatussolic, oe.cd_descripcion nombreestatus, " +
				"       ors.cg_observaciones_if observif, ors.cg_observaciones_op observop, " +
				"       ors.cg_tipo_tasa tipotasa, " +
				"       ors.ic_tasa cvetasa, " +
				"       ct.cd_nombre nombretasa, " +
				"       ors.in_tasa_aceptada tasainteres,  " +
				"       ci.cg_razon_social nombreinter,  " +
				"		  ors.ic_moneda cvemoneda, " +
				"		  ors.ig_numero_prestamo numprestamo, " +
				"		  TO_CHAR(ors.df_operado,'dd/mm/yyyy') fecop, " +
				"       TO_CHAR(ors.df_rechazo,'dd/mm/yyyy HH24:mi:ss') fecrechazo, " +
				"		  odi.ic_num_cuenta numcuenta, " +
				"		  odi.cg_banco nombrebanco, " +
				"		  ors.no_folio folio, " +
				"		  ors.cg_usuario_if usuarioif, " +
				"		  ors.cg_causas_rechazo causasRech, " +
				"		  odi.cg_cont_modificatorios countmodif, " +
				"		  ors.cc_acuse ccacuse, " +
				"		  decode (nvl(dbms_lob.getlength(ors.bi_doc_cotizacion),0),0,'N','S' ) as EXISTEFILECOTIZA, " +
				"		   decode ( nvl(dbms_lob.getlength(ors.bi_doc_cartera),0),0,'N','S') as EXISTEFILECARTERA," +
				"		   decode (nvl(dbms_lob.getlength(ors.bi_doc_cartera_pdf),0),0,'N','S') as EXISTEFILECARTERAPDF, " +
				"		 decode( nvl(dbms_lob.getlength(ors.bi_doc_boletarug),0),0,'N','S') as EXISTEFILEBOLETARUG,  " +
				"		 decode (nvl(dbms_lob.getlength(ors.bi_doc_acuse_solic),0),0,'N','S') as EXISTEFILEACUSESOL, " +
				"		'' AS ALLFECMODCONT, " +
				"		'' as NOMBREUSERIF " +
				"  FROM ope_reg_solicitud ors, " +
				"       ope_datos_if odi, " +
				"       comcat_moneda cm, " +
				"       opecat_estatus oe, " +
				"		  opecat_contrato oc, " +
				"		  comcat_if ci, " +	
				"		  comcat_tasa ct " +	
				" WHERE ors.ic_if = odi.ic_if " +
				"   AND odi.ic_if = ci.ic_if " +
				"   AND ors.ic_moneda = cm.ic_moneda " +
				"   AND ors.ic_tasa = ct.ic_tasa(+) " +
				"   AND ors.ic_estatus_ope = oe.ic_estatus_ope " +
				"	 AND ors.ic_contrato = oc.ic_contrato " +
				
				"   AND ors.ic_if = "+ic_if
			); 
			if(auto!=null && "1".equals(auto)){
				qrySentencia.append(  " AND ors.ic_estatus_ope in(?)");
				 conditions.add("7");
			}else if("2".equals(auto)){
				qrySentencia.append(  " AND ors.ic_estatus_ope in(?,?)");
				 conditions.add("7");
				 conditions.add("8");
			}else{
				qrySentencia.append(  " AND ors.ic_estatus_ope in(?,?)");
				 conditions.add("7");
				 conditions.add("8");
			}
			if(numSolic!=null && !"".equals(numSolic)){
				qrySentencia.append(  "   AND ors.cc_acuse = ? ");
				 conditions.add(numSolic);
			}
			if(fecRecpIni!=null && !"".equals(fecRecpIni)){
				qrySentencia.append(  " AND ors.df_solicitud >= TO_DATE (?, 'dd/mm/yyyy')");
				 conditions.add(fecRecpIni);
			}
			if(fecRecpFin!=null && !"".equals(fecRecpFin)){
				qrySentencia.append(  " AND ors.df_solicitud < (TO_DATE (?, 'dd/mm/yyyy') + 1)");
				 conditions.add(fecRecpFin);
			}
			
			 
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		

		try {
				
			
		
				
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {			
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	/**
	 * 
	 * @return 
	 * @param tipo
	 * @param path
	 * @param rs
	 * @param request
	 */
		
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		
		return "";
	}

	
	
	/**
	 * Obtener Fechas Modificatorias para mostrarlas en el Grid 
	 * @throws java.lang.Exception
	 * @return 
	 * @param ic_if
	 */
	public String  getFirmasModificatorias (String ic_if)   throws Exception{ 
		log.info("getFirmasModificatorias (E)");
   	AccesoDB 	con = new AccesoDB();
      boolean		commit = true;		
      PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer 	SQL = new StringBuffer();  
		StringBuffer 	contenido = new StringBuffer();   
		List varBind = new ArrayList();
		int i = 0;
		String allFecModCont = "";
   	try{
			con.conexionDB();
         
			SQL.append( " SELECT  CG_DESCRIPCION AS  DESCRIPCION, to_char(DF_FIRMA_MODIFICATORIA,'dd/mm/yyyy') as  FECHA " +
			" from OPE_DET_FIR_MODIFI "+
			" where ic_if  = ? "+
			" ORDER BY FECHA "
			);			
			varBind.add(ic_if);
			ps = con.queryPrecompilado(SQL.toString(), varBind);
			rs = ps.executeQuery();
			int x =1;
			String fecModCont = "";
			String userIF = "";
		   while( rs.next() ){
				fecModCont = rs.getString("DESCRIPCION")+": "+rs.getString("FECHA");
				allFecModCont += ("".equals(allFecModCont))?fecModCont:(", "+fecModCont);
			}
			rs.close();
			ps.close();		
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			log.error("Error getFirmasModificatorias "+e);
			throw new AppException("Error al getFirmasModificatorias",e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getFirmasModificatorias (S)");
		}	
		return allFecModCont;
   }  
	
	  
	
	
	public List getConditions() {  return conditions;  }  
	public String getPaginaNo() { return paginaNo; 	}
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getNumSolic() {
		return numSolic;
	}

	public void setNumSolic(String numSolic) {
		this.numSolic = numSolic;
	}
	

	public String getFecRecpIni() {
		return fecRecpIni;
	}

	public void setFecRecpIni(String fecRecpIni) {
		this.fecRecpIni = fecRecpIni;
	}


	public String getFecRecpFin() {
		return fecRecpFin;
	}

	public void setFecRecpFin(String fecRecpFin) {
		this.fecRecpFin = fecRecpFin;
	}
	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}
	
	public String getNoAut() {
		return auto;
	}

	public void setNoAut(String auto) {
		this.auto = auto;
	}

	

	
}