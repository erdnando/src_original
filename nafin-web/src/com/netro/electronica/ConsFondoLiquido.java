package com.netro.electronica;

import com.netro.pdf.ComunesPDF;
import com.netro.xls.ComunesXLS;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.CQueryHelperRegExtJS;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.Registros;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsFondoLiquido implements IQueryGeneratorRegExtJS {
	private static final Log log = ServiceLocator.getInstance().getLog(ConsFondoLiquido.class);
	private List conditions;
	private String iNoCliente,fechaCargaIni,fechaCargaFin, cveIF, tipoUsuario, strPerfil, strUsuario;

	public ConsFondoLiquido(){}

	public String getDocumentSummaryQueryForIds(List pageIds) {
		int i=0;
		StringBuffer qrySentencia	= new StringBuffer();
    	StringBuffer condicion		= new StringBuffer();
    	
		String	ses_ic_if         = getINoCliente();
    
		condicion.append(" AND ocs.ic_carga in (");
		i=0;

			
		conditions 		= new ArrayList();
				for(int j = 0; j < pageIds.size(); j++){
					List lItem = (ArrayList)pageIds.get(j);
						condicion.append("?,");
						conditions.add(new String(lItem.get(0).toString()));      
					}
		condicion.deleteCharAt(condicion.length()-1);
		qrySentencia.append(
				"SELECT ocs.ic_num_contrato NUMCONTRATO, " +
				"       ci.cg_razon_social NOMBREINTER, " +
				"       ocs.ic_financiera CVESIRAC, " +
				"       TO_CHAR (ocs.df_carga, 'dd/mm/yyyy HH24:mi:ss') FECHACARGA,  " +
				"       ocs.fg_saldo_hoy SALDOHOY, " +
				"       cm.ic_moneda CVEMONEDA, " +
				"       cm.CD_NOMBRE NOMBREMONEDA, " +
				"       ocs.ic_usuario NUMUSUARIO,  " +
				"       ocs.cg_nom_usuario NOMBREUSUARIO " +
				"  FROM ope_carga_saldo_fliquido ocs, comcat_if ci, comcat_moneda cm " +
				" WHERE ocs.ic_if = ci.ic_if " +
				" AND ocs.ic_moneda = cm.ic_moneda " +
				condicion.toString()+ ")");

		log.debug("Query IDS"+qrySentencia.toString());
		log.debug(" query IDs"+conditions.toString());
		
		return qrySentencia.toString();
	
	}		
	public String getAggregateCalculationQuery() {
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion    = new StringBuffer();
		
			
		try {
			conditions 		= new ArrayList();

			if (!("".equals(cveIF)) && cveIF!=null){
				condicion.append("   AND ocs.ic_if = ? ");
				conditions.add(cveIF);
			}

			if (!"".equals(fechaCargaIni) && fechaCargaIni!=null ){
				condicion.append("    AND ocs.df_carga >= TO_DATE(?,'dd/mm/yyyy') ");
				conditions.add(fechaCargaIni);
			}
			
			if (!"".equals(fechaCargaFin) && fechaCargaFin!=null ){
				condicion.append("    AND ocs.df_carga < TO_DATE(?,'dd/mm/yyyy')+1 ");
				conditions.add(fechaCargaFin);
			}

		qrySentencia.append(
				"SELECT count(1) REGTOTAL, sum(FG_SALDO_HOY) SALDOTOTAL " +
				"  FROM ope_carga_saldo_fliquido ocs, comcat_if ci, comcat_moneda cm " +
				" WHERE ocs.ic_if = ci.ic_if " +
				" AND ocs.ic_moneda = cm.ic_moneda " +
				condicion.toString());
		}catch(Exception e){
			System.out.println("CambioEstatusEpoDE::getAggregateCalculationQuery "+e);
		}
		return qrySentencia.toString();
	}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		String nombreArchivo = "";
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		
		
		if("PDF".equals(tipo)){
			try{
				HttpSession session = request.getSession();
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				ComunesPDF pdfDoc = new ComunesPDF(2,path + nombreArchivo);
				
				String meses[]			= {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual	= new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual		= fechaActual.substring(0,2);
				String mesActual		= meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual		= fechaActual.substring(6,10);
				String horaActual		= new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
				
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				session.getAttribute("iNoNafinElectronico").toString(),
				(String)session.getAttribute("sesExterno"),
				(String)session.getAttribute("strNombre"),
				(String)session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));
				
				pdfDoc.addText("M�xico, D.F. a " + diaActual + " de " + mesActual + " del " + anioActual + " ----------------------------- " + horaActual,"formas",ComunesPDF.RIGHT);
				
				pdfDoc.setTable(18, 100);
				pdfDoc.setCell("No. Docto Final","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("EPO","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Dist.","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de Cr�dito","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Resp. Pago de Inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo de Cobranza","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Operaci�n","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Moneda","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Ref. Tasa Inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tasa Inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Plazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de Vencimiento","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Monto Inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Tipo Cobro Inter�s","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Fecha de rechazo","celda01",ComunesPDF.CENTER);
				pdfDoc.setCell("Causa","celda01",ComunesPDF.CENTER);

				
				while (reg.next()){
					String numeroDocumento		= (reg.getString(1) == null) ? "" : reg.getString(1);
					String epo						= (reg.getString("EPO") == null) ? "" : reg.getString("EPO");
					String dist						= (reg.getString("PYME") == null) ? "" : reg.getString("PYME");
					String tipoCredito			= (reg.getString("CG_TIPO_CREDITO") == null) ? "" : reg.getString("CG_TIPO_CREDITO");
					String respInt					= (reg.getString("RESPONSABLE") == null) ? "" : reg.getString("RESPONSABLE");
					String tipoCob					= (reg.getString("COBRANZA") == null) ? "" : reg.getString("COBRANZA");
					String fOpera					= (reg.getString("FECHA_OPER") == null) ? "" : reg.getString("FECHA_OPER");
					String moneda					= (reg.getString("CD_NOMBRE") == null) ? "" : reg.getString("CD_NOMBRE");
					String monto					= (reg.getString("MONTO_VAL") == 	null) ? "" : reg.getString("MONTO_VAL");
					String refTasa					= (reg.getString("CT_REFERENCIA") == null) ? "" : reg.getString("CT_REFERENCIA");
					String tasaInt					= (reg.getString("TASA_INT") == null) ? "" : reg.getString("TASA_INT");
					String plazo					= (reg.getString("IG_PLAZO_DESCUENTO") == null) ? "" : reg.getString("IG_PLAZO_DESCUENTO");
					String fVenc					= (reg.getString("FECHA_VENC") == 	null) ? "" : reg.getString("FECHA_VENC");
					String montoInteres			= (reg.getString("MONTO_INT") == null) ? "" : reg.getString("MONTO_INT");
					String cobroInt				= (reg.getString("TIPO_COB_INT") == null) ? "" : reg.getString("TIPO_COB_INT");
					String estatus					= (reg.getString("CAMBIOESTATUS") == null) ? "" : reg.getString("CAMBIOESTATUS");
					String fechaRe					= (reg.getString("FECHACAMBIO") == null) ? "" : reg.getString("FECHACAMBIO");
					String causa					= (reg.getString("CAMBIOMOTIVO") == null) ? "" : reg.getString("CAMBIOMOTIVO");

					
					pdfDoc.setCell(numeroDocumento,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(epo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(dist,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(tipoCredito,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(respInt,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(tipoCob,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fOpera,"formas",ComunesPDF.RIGHT);
					pdfDoc.setCell(moneda,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monto,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(refTasa,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(Comunes.formatoDecimal(tasaInt,2)+"%","formas",ComunesPDF.CENTER);
					pdfDoc.setCell(plazo,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fVenc,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(montoInteres,2),"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(cobroInt,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(estatus,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(fechaRe,"formas",ComunesPDF.CENTER);
					pdfDoc.setCell(causa,"formas",ComunesPDF.CENTER);
					
				}
				//pdfDoc.addTable();
				
				
				CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS();//toma el valor de sesi�n
				Registros regi = queryHelper.getResultCount(request);
				
			
				
					
					pdfDoc.setCell("MONEDA ","celda01rep",ComunesPDF.LEFT,6);
					pdfDoc.setCell("TOTAL REGISTROS","celda01rep",ComunesPDF.CENTER,6);
					pdfDoc.setCell("MONTO","celda01rep",ComunesPDF.CENTER,6);

					while(regi.next()){
						pdfDoc.setCell("TOTAL "+regi.getString(2),"celda01rep",ComunesPDF.LEFT,6);
						pdfDoc.setCell(regi.getString(3),"celda01rep",ComunesPDF.CENTER,6);
						pdfDoc.setCell("$"+regi.getString(4),"celda01rep",ComunesPDF.CENTER,6);

					}
				
					pdfDoc.addTable();
					pdfDoc.endDocument();
							}catch(Throwable e){
				throw new AppException("Error al generar el archivo",e);
			}finally{
				try{
				}catch(Exception e){}
			}
		}	
		return nombreArchivo;
	}
	
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo){
		CreaArchivo creaArchivo = new CreaArchivo();
		String nombreArchivo	= creaArchivo.nombreArchivo()+".xls";
		ComunesXLS xls = new ComunesXLS(path+nombreArchivo);
      boolean visible = false;
		try {
		  if("NAFIN".equals(tipoUsuario)) visible = true;
			
			xls.setTabla(visible?7:4);
			xls.setCelda("CONTRATO", "celda01", ComunesXLS.CENTER, 1);
			if(visible){
				xls.setCelda("NOMBRE", "celda01", ComunesXLS.CENTER, 1);
				xls.setCelda("CLAVE SIRAC", "celda01", ComunesXLS.CENTER, 1);
			}
			xls.setCelda("SALDO", "celda01", ComunesXLS.CENTER, 1);
			xls.setCelda("MONEDA", "celda01", ComunesXLS.CENTER, 1);
			xls.setCelda("FECHA Y HORA DE CARGA", "celda01", ComunesXLS.CENTER, 1);
			if(visible){
				xls.setCelda("USUARIO DE CARGA", "celda01", ComunesXLS.CENTER, 1);
			}
			 
			while (rs.next()) {
				String numContrato		= (rs.getString("NUMCONTRATO") == null) ? "" : rs.getString("NUMCONTRATO");
				String nombreInter		= (rs.getString("NOMBREINTER") == null) ? "" : rs.getString("NOMBREINTER");
				String cveSirac			= (rs.getString("CVESIRAC") == null) ? "" : rs.getString("CVESIRAC");
				String saldoHoy			= (rs.getString("SALDOHOY") == null) ? "" : rs.getString("SALDOHOY");
				String nombreMoneda		= (rs.getString("NOMBREMONEDA") == null) ? "" : rs.getString("NOMBREMONEDA");
				String fechaCarga			= (rs.getString("FECHACARGA") == null) ? "" : rs.getString("FECHACARGA");
				String numUsuario			= (rs.getString("NUMUSUARIO") == null) ? "" : rs.getString("NUMUSUARIO");
				String nombreUsuario		= (rs.getString("NOMBREUSUARIO") == null) ? "" : rs.getString("NOMBREUSUARIO");
					
				xls.setCelda(numContrato, "formas", ComunesXLS.CENTER, 1);
				if(visible){
					xls.setCelda(nombreInter, "formas", ComunesXLS.CENTER, 1);
					xls.setCelda(cveSirac, "formas", ComunesXLS.CENTER, 1);
				}
				xls.setCelda("$"+Comunes.formatoDecimal(saldoHoy,2) , "formas", ComunesXLS.RIGHT, 1);
				xls.setCelda(nombreMoneda, "formas", ComunesXLS.CENTER, 1);
				xls.setCelda(fechaCarga, "formas", ComunesXLS.CENTER, 1);
				if(visible){
					xls.setCelda(numUsuario+" - "+nombreUsuario, "formas", ComunesXLS.LEFT, 1);
				}
			}
			
			CQueryHelperRegExtJS queryHelper = new CQueryHelperRegExtJS();//toma el valor de sesi�n
			Registros regi = queryHelper.getResultCount(request);
				
			while(regi.next()){
				//linea="TOTAL "+regi.getString(2)+","+regi.getString(3)+","+"$"+regi.getString(4)+"\n";
				//buffer.write(linea);
			}
					
			xls.cierraTabla();
			xls.cierraXLS();
		  
		}catch (Throwable e) {
			throw new AppException("Error al generar el archivo",e);
		}
		finally {
			try {
				rs.close();
			} catch(Exception e) {}
		}
		
		return nombreArchivo;
		 }

	public String getDocumentQuery() {
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
      StringBuffer qrySentencia = new StringBuffer();
      StringBuffer condicion    = new StringBuffer();
		String tablaExtra = "";
		boolean existeTE = false;
			
		try {
		
			
		
			conditions=new ArrayList();

			if (!("".equals(cveIF)) && cveIF!=null){
				condicion.append("   AND ocs.ic_if = ? ");
				conditions.add(cveIF);
			}

			if (!"".equals(fechaCargaIni) && fechaCargaIni!=null ){
				condicion.append("    AND ocs.df_carga >= TO_DATE(?,'dd/mm/yyyy') ");
				conditions.add(fechaCargaIni);
			}
			
			if (!"".equals(fechaCargaFin) && fechaCargaFin!=null ){
				condicion.append("    AND ocs.df_carga < TO_DATE(?,'dd/mm/yyyy')+1 ");
				conditions.add(fechaCargaFin);
			}

			
			
			if("OP OP".equals(strPerfil)){
				tablaExtra = " ,ope_reg_solicitud op "; 
				condicion.append("    AND op.ic_ejecutivo_op = ? ");
				conditions.add(strUsuario);
				existeTE = true;
			}
			if("SUP SEG".equals(strPerfil)){
				tablaExtra = " ,ope_datos_if op "; 
				condicion.append("    AND op.cg_ejec_seg = ? ");
				conditions.add(strUsuario);
				existeTE = true;
			}
			if("EJE PROMO".equals(strPerfil)){
				tablaExtra = " ,ope_datos_if op "; 
				condicion.append("    AND op.cg_ejec_promo = ? ");
				conditions.add(strUsuario);
				existeTE = true;
			}
			if("SUB LI".equals(strPerfil)){
				tablaExtra = " ,ope_reg_solicitud op "; 
				condicion.append("    AND op.ic_contrato in (?) ");
				conditions.add(new Integer(1));
				
				existeTE = true;
			}
			if("SUB MICRO CP".equals(strPerfil)){
				tablaExtra = " ,ope_reg_solicitud op "; 
				condicion.append("    AND op.ic_contrato in (?,?,?,?) ");
				conditions.add(new Integer(2));
				conditions.add(new Integer(3));
				conditions.add(new Integer(4));
				conditions.add(new Integer(5));
				
				existeTE = true;
			}



		qrySentencia.append(
				"SELECT distinct ocs.ic_carga " +
				"  FROM ope_carga_saldo_fliquido ocs, comcat_if ci, comcat_moneda cm " +tablaExtra+
				" WHERE ocs.ic_if = ci.ic_if " +
				" AND ocs.ic_moneda = cm.ic_moneda " +
				((existeTE)?" AND ocs.ic_if =  op.ic_if":"")+
				condicion.toString());

			log.debug("EL query de la llave primaria: "+qrySentencia.toString());
			log.debug("condicion: "+conditions.toString());
		}catch(Exception e){
			System.out.println("ConsInfDocEpoDist::getDocumentQueryException "+e);
		}
		return qrySentencia.toString();
	}
	
	 public String getDocumentQueryFile() {
		String fechaHoy = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());  
		StringBuffer qrySentencia = new StringBuffer();
		StringBuffer condicion    = new StringBuffer();
		String tablaExtra = "";
		boolean existeTE = false;
		
		try {


			conditions=new ArrayList();
			if (!("".equals(cveIF)) && cveIF!=null){
				condicion.append("   AND ocs.ic_if = ? ");
				conditions.add(cveIF);
			}

			if (!"".equals(fechaCargaIni) && fechaCargaIni!=null ){
				condicion.append("    AND ocs.df_carga >= TO_DATE(?,'dd/mm/yyyy') ");
				conditions.add(fechaCargaIni);
			}
			
			if (!"".equals(fechaCargaFin) && fechaCargaFin!=null ){
				condicion.append("    AND ocs.df_carga < TO_DATE(?,'dd/mm/yyyy')+1 ");
				conditions.add(fechaCargaFin);
			}
			
			if("OP OP".equals(strPerfil)){
				tablaExtra = " ,ope_reg_solicitud op "; 
				condicion.append("    AND op.ic_ejecutivo_op = ? ");
				conditions.add(strUsuario);
				existeTE = true;
			}
			if("SUP SEG".equals(strPerfil)){
				tablaExtra = " ,ope_datos_if op "; 
				condicion.append("    AND op.cg_ejec_seg = ? ");
				conditions.add(strUsuario);
				existeTE = true;
			}
			if("EJE PROMO".equals(strPerfil)){
				tablaExtra = " ,ope_datos_if op "; 
				condicion.append("    AND op.cg_ejec_promo = ? ");
				conditions.add(strUsuario);
				existeTE = true;
			}
			if("SUB LI".equals(strPerfil)){
				tablaExtra = " ,ope_reg_solicitud op "; 
				condicion.append("    AND op.ic_contrato in (?) ");
				conditions.add(new Integer(1));
				
				existeTE = true;
			}
			if("SUB MICRO CP".equals(strPerfil)){
				tablaExtra = " ,ope_reg_solicitud op "; 
				condicion.append("    AND op.ic_contrato in (?,?,?,?) ");
				conditions.add(new Integer(2));
				conditions.add(new Integer(3));
				conditions.add(new Integer(4));
				conditions.add(new Integer(5));
				existeTE = true;
			}

			qrySentencia.append(
				"SELECT distinct ocs.ic_carga CVECARGA, ocs.ic_num_contrato NUMCONTRATO, " +
				"       ci.cg_razon_social NOMBREINTER, " +
				"       ocs.ic_financiera CVESIRAC, " +
				"       TO_CHAR (ocs.df_carga, 'dd/mm/yyyy HH24:mi:ss') FECHACARGA,  " +
				"       ocs.fg_saldo_hoy SALDOHOY, " +
				"       cm.ic_moneda CVEMONEDA, " +
				"       cm.CD_NOMBRE NOMBREMONEDA, " +
				"       ocs.ic_usuario NUMUSUARIO,  " +
				"       ocs.cg_nom_usuario NOMBREUSUARIO " +
				"  FROM ope_carga_saldo_fliquido ocs, comcat_if ci, comcat_moneda cm " +tablaExtra+
				" WHERE ocs.ic_if = ci.ic_if " +
				" AND ocs.ic_moneda = cm.ic_moneda " +
				((existeTE)?" AND ocs.ic_if =  op.ic_if":"")+
				condicion.toString());
				
		}catch(Exception e){
			System.out.println("CambioEstatusEpoDE::getDocumentQueryFileException "+e);
		}
		return qrySentencia.toString();
	 }
	 public List getConditions() {
		return conditions;
	}


	public void setINoCliente(String iNoCliente) {
		this.iNoCliente = iNoCliente;
	}


	public String getINoCliente() {
		return iNoCliente;
	}


	public void setFechaCargaIni(String fechaCargaIni) {
		this.fechaCargaIni = fechaCargaIni;
	}


	public String getFechaCargaIni() {
		return fechaCargaIni;
	}


	public void setCveIF(String cveIF) {
		this.cveIF = cveIF;
	}


	public String getCveIF() {
		return cveIF;
	}


	public void setFechaCargaFin(String fechaCargaFin) {
		this.fechaCargaFin = fechaCargaFin;
	}


	public String getFechaCargaFin() {
		return fechaCargaFin;
	}


	public void setTipoUsuario(String tipoUsuario) {
		this.tipoUsuario = tipoUsuario;
	}


	public String getTipoUsuario() {
		return tipoUsuario;
	}


	public void setStrPerfil(String strPerfil) {
		this.strPerfil = strPerfil;
	}


	public String getStrPerfil() {
		return strPerfil;
	}


	public void setStrUsuario(String strUsuario) {
		this.strUsuario = strUsuario;
	}


	public String getStrUsuario() {
		return strUsuario;
	}
		


	

}