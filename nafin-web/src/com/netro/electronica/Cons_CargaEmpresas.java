package com.netro.electronica;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;


public class Cons_CargaEmpresas implements  IQueryGeneratorRegExtJS {	   

	
	public Cons_CargaEmpresas() {}
	
	/**
	 * Variable que se emplea para enviar mensajes al log.
	 */
	private final static Log log = ServiceLocator.getInstance().getLog(Cons_CargaEmpresas.class);
	private List 		conditions;
	StringBuffer qrySentencia = new StringBuffer("");
	private String 	paginaOffset;
	private String 	paginaNo;
	private String ic_if;
	private String ic_solicitud;
	private String fechaRecepcionIni;
	private String fechaRecepcionFin;	
	private String strPerfil;
	private String no_Prestamo;
	private String inicializa;
	

  public String getAggregateCalculationQuery() {
		log.info("getAggregateCalculationQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
		
		
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getAggregateCalculationQuery(S)");
		return qrySentencia.toString();	
	}
	
		public String getDocumentQuery() {
		
		log.info("getDocumentQuery(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
	   log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQuery(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentSummaryQueryForIds(List pageIds) {
	
		log.info("getDocumentSummaryQueryForIds(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
			
			
					
		
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentSummaryQueryForIds(S)");
		return qrySentencia.toString();	
	}
	
	public String getDocumentQueryFile() {
		log.info("getDocumentQueryFile(E)");
		qrySentencia 	= new StringBuffer();
		conditions 		= new ArrayList();
	
	
		qrySentencia.append( " 	 SELECT   "+
		   " i.ic_if as IC_IF, "+
			" i.CG_RAZON_SOCIAL as NOMBRE_IF, "+
			" to_char(S.DF_SOLICITUD , 'dd/mm/yyyy HH:MI:SS AM')   as FECHA_SOLICITUD , "+
			" s.IC_SOLICITUD as NO_SOLICITUD, "+
			" s.cc_acuse as CS_ACUSE, "+
			" c.CG_NOMBRE_CONTRATO as NOMBRE_CONTRATO, "+
			" TO_CHAR (di.DF_FIRMA_CONTRATO, 'dd/mm/yyyy')   as FECHA_FIRMA_CONTRATO ,  "+
			" di.CG_CONT_MODIFICATORIOS as NO_MODIFICATORIOS,"+
			" 'NA' as FECHA_FIRMA_MODIFICATORIOS,  "+
			" di.CG_CONTRATO_OE  as NOMBRE_CONTRATO_M,"+
			" to_char(di.DF_FIRMA_CONTRATO_OE , 'dd/mm/yyyy')   as  FECHA_CONTRATOS_OE,"+
			" s.FN_MONTO_SOLICITUD as  IMPORTE, "+
			" s.CG_DES_RECURSOS as DESTINO_RECURSOS, " +
			" to_char(s.DF_PAGO_CAPITAL , 'dd/mm/yyyy')  as FECHA_PAGO_CAPITAL, "+
			" to_char(s.DF_PAGO_INTERES , 'dd/mm/yyyy') as FECHA_PAGO_INTERES,"+
			" to_char(s.DF_VENCIMIENTO , 'dd/mm/yyyy') as FECHA_VENCIMIENTO,"+
			" s.CG_TIPO_TASA as TIPO_TASA,"+
			" t.CD_NOMBRE as TASA,  "+
			" s.IN_TASA_ACEPTADA as VALOR, "+			
			" e.CD_DESCRIPCION as ESTATUS, "+
			" to_char(s.DF_OPERADO , 'dd/mm/yyyy HH:MI:SS AM') as FECHA_OPERACION_EJECUTIVO, "+
			" to_char(s.DF_OPERADO_CON , 'dd/mm/yyyy HH:MI:SS AM') as FECHA_OP_CONCENTRADOR,"+			
			" S.CG_OBSERVACIONES_IF as OBSER_INTERMEDIARIO,"+
			" S.CG_OBSERVACIONES_OP as OBSER_EJECUTIVO,  "+			
			" m.CD_NOMBRE as MONEDA  ,  "+
			" m.ic_moneda as IC_MONEDA , "+
			" length(s.BI_DOC_COTIZACION)  as  DOC_COTIZACION ,  "+ 
			" length(s.BI_DOC_CARTERA)  as  DOC_CARTERA ,"+
			" length(s.BI_DOC_BOLETARUG)  as  DOC_BOLETA ,"+
			" length(s.BI_DOC_PRENDA)  as  DOC_PRENDA,  "+  			
			" s.ic_estatus_ope as  IC_ESTATUS , "+  
			" '' AS PERFIL,  "+
			" s.IG_NUMERO_PRESTAMO AS NO_PRESTAMO ,"+  			
			" s.CC_ACUSE as ACUSE,   "+
			" s.CG_EMP_APOYADAS AS CARGA_APOYADAS,  " +			
			" a.CG_NOMBRE_USUARIO as  CG_NOMBRE_USUARIO ,  "+  						
			" S.CG_NOM_USU_OP  AS  USUARIO_EJE_OP ,   "+
			" S.CG_NOM_USUARIO_CON2 AS  USUARIO_EJE_CON2  "+
			
			"  FROM OPE_REG_SOLICITUD S, comcat_if i, OPECAT_CONTRATO c , OPE_DATOS_IF  di, "+
			"  OPECAT_ESTATUS e, comcat_moneda m, "+
			"  comcat_tasa t   , OPE_ACUSE a   " +		
			
			"  WHERE s.ic_if = i.ic_if  "+
			"  and s.IC_CONTRATO = c.IC_CONTRATO "+
			"  and s.ic_estatus_ope = e.ic_estatus_ope "+
			"  and  s.ic_if = di.ic_if  "+
			"  and s.ic_estatus_ope = 5  "+
			" and s.ic_tasa = t.ic_tasa(+)   "+
			" and s.ic_moneda = m.ic_moneda  " + 
			"  and s.cc_acuse = a.cc_acuse  " );
			
		
			 if (!"".equals(ic_if) )  {
				 qrySentencia.append( " and s.ic_if  = ? ");
				  conditions.add(ic_if);
			 }
			 
			  if (!"".equals(no_Prestamo) )  {
				 qrySentencia.append( " and s.IG_NUMERO_PRESTAMO  = ? ");
				  conditions.add(no_Prestamo);
			 }
			 
			 
			 if (!"".equals(ic_solicitud) )  {
				 qrySentencia.append( " and s.cc_acuse  = ? ");
				  conditions.add(ic_solicitud);
			 }
			 
			 if ( !"".equals(fechaRecepcionIni ) &&   !"".equals(fechaRecepcionFin ) )   {
				qrySentencia.append(" and s.DF_SOLICITUD >= TO_DATE ( ?, 'dd/mm/yyyy') ");
				qrySentencia.append(" and  s.DF_SOLICITUD < TO_DATE (?, 'dd/mm/yyyy') + 1 " );
				conditions.add(fechaRecepcionIni);
				conditions.add(fechaRecepcionFin);
			}
			
			if ("S".equals(inicializa ) ) {
				qrySentencia.append( " AND TO_CHAR (s.df_solicitud, 'DD/MM/YYYY') = TO_CHAR (SYSDATE, 'DD/MM/YYYY') ");				
			}					
			
			 
		log.info("qrySentencia  "+qrySentencia);
		log.info("conditions "+conditions);
		log.info("getDocumentQueryFile(S)");
		return qrySentencia.toString();	
	}
	
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros rs, String path, String tipo) {
		String nombreArchivo = "";
		HttpSession session = request.getSession();	
		ComunesPDF pdfDoc = new ComunesPDF();
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		

		try {
				
			
		
				
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {			
			} catch(Exception e) {}
		  
		}
		return nombreArchivo;
					
	}
	
	
		
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		
		log.debug("crearCustomFile (E)");
		String nombreArchivo ="";
		CreaArchivo creaArchivo = new CreaArchivo();
		StringBuffer contenidoArchivo = new StringBuffer();
		
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		int total = 0;
		String SaldoCredito = "", SaldoFondo ="";
		
		try { 
			
			nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
			writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true),"ISO-8859-1");
			buffer = new BufferedWriter(writer);
		
			
			contenidoArchivo = new StringBuffer();		
			
			contenidoArchivo.append(" Fecha  Solicitud,  No. Pr�stamo, No. solicitud, Nombre Contrato, Fecha Firma Contrato, "+
			" Contratos Modificatorios , Fecha Firma Modificatorios  , Nombre Contrato Operaci�n Electr�nica ,  " +
			" Fecha Firma Contrato Operaci�n Electr�nica  , Importe, Moneda, Destino de los recursos, Fecha de primer pago de Capital,  "+
			" Fecha de primer pago de Intereses,  Fecha de Vencimiento,  Tipo Tasa, Tasa, Valor ,  Estatus, Usuario IF,  "+
			" Fecha Operaci�n Ejecutivo OP, Usuario Ejecutivo OP,   Fecha Operaci�n Concentrador, Usuario Concentrador, Observaciones Intermediario Financiero ,  Observaciones del Ejecutivo de Operaci�n \n ");			
			
			while (rs.next())	{	
			
				String fechaSolic = (rs.getString("FECHA_SOLICITUD") == null) ? "" : rs.getString("FECHA_SOLICITUD");
				String acuse = (rs.getString("CS_ACUSE") == null) ? "" : rs.getString("CS_ACUSE");
				String nombreContrato = (rs.getString("NOMBRE_CONTRATO") == null) ? "" : rs.getString("NOMBRE_CONTRATO");
				String fecFirmaContrato = (rs.getString("FECHA_FIRMA_CONTRATO") == null) ? "" : rs.getString("FECHA_FIRMA_CONTRATO");
				
				String no_modificatorios = (rs.getString("NO_MODIFICATORIOS") == null) ? "N/A" : rs.getString("NO_MODIFICATORIOS");
				String nombreContratoM = (rs.getString("NOMBRE_CONTRATO_M") == null) ? "" : rs.getString("NOMBRE_CONTRATO_M");
				String FechaContratoOE = (rs.getString("FECHA_CONTRATOS_OE") == null) ? "" : rs.getString("FECHA_CONTRATOS_OE");
				String importe = (rs.getString("IMPORTE") == null) ? "" : rs.getString("IMPORTE");
				String moneda = (rs.getString("MONEDA") == null) ? "" : rs.getString("MONEDA");
				String destinoRecursos = (rs.getString("DESTINO_RECURSOS") == null) ? "" : rs.getString("DESTINO_RECURSOS");
				String fecPagoCapital = (rs.getString("FECHA_PAGO_CAPITAL") == null) ? "" : rs.getString("FECHA_PAGO_CAPITAL");
				String fecPagoInteres = (rs.getString("FECHA_PAGO_INTERES") == null) ? "" : rs.getString("FECHA_PAGO_INTERES");
				String fecVencimiento = (rs.getString("FECHA_VENCIMIENTO") == null) ? "" : rs.getString("FECHA_VENCIMIENTO");
				String tipoTasa = (rs.getString("TIPO_TASA") == null) ? "" : rs.getString("TIPO_TASA");
				String tasa = (rs.getString("TASA") == null) ? "" : rs.getString("TASA");
				String valor = (rs.getString("VALOR") == null) ? "" : rs.getString("VALOR");
				String estatus = (rs.getString("ESTATUS") == null) ? "" : rs.getString("ESTATUS");
				String fecOperaEjecutivo = (rs.getString("FECHA_OPERACION_EJECUTIVO") == null) ? "" : rs.getString("FECHA_OPERACION_EJECUTIVO");
				String fecOperaConcentrador = (rs.getString("FECHA_OP_CONCENTRADOR") == null) ? "" : rs.getString("FECHA_OP_CONCENTRADOR");
				String obseIntermediario = (rs.getString("OBSER_INTERMEDIARIO") == null) ? "" : rs.getString("OBSER_INTERMEDIARIO");
				String obseEjecutivo = (rs.getString("OBSER_EJECUTIVO") == null) ? "" : rs.getString("OBSER_EJECUTIVO");
				String ic_if = (rs.getString("ic_if") == null) ? "" : rs.getString("ic_if");
				String prestamo = (rs.getString("NO_PRESTAMO") == null) ? "" : rs.getString("NO_PRESTAMO");
				String strNombreUsuario = (rs.getString("CG_NOMBRE_USUARIO") == null) ? "" : rs.getString("CG_NOMBRE_USUARIO");
				String usuarioEje_PRO = (rs.getString("USUARIO_EJE_OP") == null) ? "" : rs.getString("USUARIO_EJE_OP");
				String usuarioCon2 = (rs.getString("USUARIO_EJE_CON2") == null) ? "" : rs.getString("USUARIO_EJE_CON2");
				
				String nombreUsuario = strNombreUsuario.substring(strNombreUsuario.lastIndexOf("-")+1, strNombreUsuario.length());
			
										
				String fechasModificatorias = this.getFirmasModificatorias (ic_if) ;
				
			
				contenidoArchivo.append(fechaSolic.replace(',',' ')+","+								
					prestamo.replace(',',' ')+","+
					acuse.replace(',',' ')+","+
					nombreContrato.replace(',',' ')+","+
					fecFirmaContrato.replace(',',' ')+","+
					no_modificatorios.replace(',',' ')+","+
					fechasModificatorias.replace(',',' ')+","+
					nombreContratoM.replace(',',' ')+","+
					FechaContratoOE.replace(',',' ')+","+
					importe.replace(',',' ')+","+
					moneda.replace(',',' ')+","+
					destinoRecursos.replace(',',' ')+","+ 
					fecPagoCapital.replace(',',' ')+","+
					fecPagoInteres.replace(',',' ')+","+
					fecVencimiento.replace(',',' ')+","+
					tipoTasa.replace(',',' ')+","+					
					tasa.replace(',',' ')+","+					
					valor.replace(',',' ')+","+										
					estatus.replace(',',' ')+","+
					nombreUsuario.replace(',',' ')+","+
					fecOperaEjecutivo.replace(',',' ')+","+
					usuarioEje_PRO.replace(',',' ')+","+
					fecOperaConcentrador.replace(',',' ')+","+
					usuarioCon2.replace(',',' ')+","+					
					obseIntermediario.replaceAll("[\n\r]", " ").replace(',',' ')+","+	
					obseEjecutivo.replaceAll("[\n\r]", " ").replace(',',' ')+",");
					contenidoArchivo.append("\n");		
					
				total++;
				if(total==1000){					
					total=0;	
					buffer.write(contenidoArchivo.toString());
					contenidoArchivo = new StringBuffer();//Limpio  
				}
				
			}//while(rs.next()){
				
			buffer.write(contenidoArchivo.toString());
			buffer.close();	
			contenidoArchivo = new StringBuffer();//Limpio     
			
			
		} catch(Throwable e) {
			throw new AppException("Error al generar el archivo ", e);
		} finally {
			try {
				//rs.close();
			} catch(Exception e) {}
		  log.debug("crearCustomFile (S)");
		}
		return nombreArchivo; 
	}

	/**  
	 * Obtener Fechas Modificatorias para mostrarlas en el Grid 
	 * @throws java.lang.Exception
	 * @return 
	 * @param ic_if
	 */
	public String  getFirmasModificatorias (String ic_if)   throws Exception{
		log.info("getFirmasModificatorias (E)");
		
   	AccesoDB 	con = new AccesoDB();
      boolean		commit = true;		
      PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuffer 	SQL = new StringBuffer();  
		StringBuffer 	contenido = new StringBuffer();   
		List varBind = new ArrayList();
		int i = 0;
   	try{
			con.conexionDB();
         
			SQL.append( " SELECT  CG_DESCRIPCION AS  DESCRIPCION, to_char(DF_FIRMA_MODIFICATORIA,'dd/mm/yyyy') as  FECHA " +
			" from OPE_DET_FIR_MODIFI "+
			" where ic_if  = ? ");			
			varBind.add(ic_if);
			log.info("SQL.toString() "+SQL.toString());
			log.info("varBind "+varBind);
			
			ps = con.queryPrecompilado(SQL.toString(), varBind);
			rs = ps.executeQuery();
			int x= 1;
		   while( rs.next() ){
				i++;
				String descripcion =  rs.getString("DESCRIPCION")==null?"":rs.getString("DESCRIPCION");
				String fecha =  rs.getString("FECHA")==null?"":rs.getString("FECHA");
				contenido.append(" Contrato Modificatorio "+x+" con fecha de firma " +fecha + ", ");	
				x++;
			}
			rs.close();
			ps.close();		
					
		}catch(Exception e){
			commit = false;
			e.printStackTrace();
			log.error("Error getFirmasModificatorias "+e);
			throw new AppException("Error al getFirmasModificatorias",e);
		}finally{
			con.terminaTransaccion(commit);
			if(con.hayConexionAbierta())
				con.cierraConexionDB();
			log.info("getFirmasModificatorias (S)");
		}	
		return contenido.toString();
   }
	

	
	
	public List getConditions() {  return conditions;  }  
	public String getPaginaNo() { return paginaNo; 	}
	public String getPaginaOffset() { return paginaOffset; 	}
	 
	public void setPaginaNo(String newPaginaNo) {  paginaNo = newPaginaNo;  }
	public void setPaginaOffset(String paginaOffset)			{this.paginaOffset=paginaOffset;}

	public String getIc_if() {
		return ic_if;
	}

	public void setIc_if(String ic_if) {
		this.ic_if = ic_if;
	}

	public String getIc_solicitud() {
		return ic_solicitud;
	}

	public void setIc_solicitud(String ic_solicitud) {
		this.ic_solicitud = ic_solicitud;
	}

	public String getFechaRecepcionIni() {
		return fechaRecepcionIni;
	}

	public void setFechaRecepcionIni(String fechaRecepcionIni) {
		this.fechaRecepcionIni = fechaRecepcionIni;
	}

	public String getFechaRecepcionFin() {
		return fechaRecepcionFin;
	}

	public void setFechaRecepcionFin(String fechaRecepcionFin) {
		this.fechaRecepcionFin = fechaRecepcionFin;
	}

	
	public String getStrPerfil() {
		return strPerfil;
	}

	public void setStrPerfil(String strPerfil) {
		this.strPerfil = strPerfil;
	}

	public String getNo_Prestamo() {
		return no_Prestamo;
	}

	public void setNo_Prestamo(String no_Prestamo) {
		this.no_Prestamo = no_Prestamo;
	}  

	public String getInicializa() {
		return inicializa;
	}

	public void setInicializa(String inicializa) {
		this.inicializa = inicializa;
	}


	  
}