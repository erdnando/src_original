package com.netro.filter;

import java.io.IOException;

import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import netropology.utilerias.Comunes;

/**
 * Filtro para compresion del response
 *
 * @author Amy Roh
 * @author Dmitri Valdin
 */
public class CompressionFilter implements Filter{
	
	//private final static Log log = ServiceLocator.getInstance().getLog(CompressionFilter.class);
	
	/**
	 * The filter configuration object we are associated with.  If this value
	 * is null, this filter instance is not currently configured.
	 */
	private FilterConfig config = null;
	
	/**
	 * Tama�o minimo de la respuesta a comprimir
	 */
	private int minThreshold = 1024;
	
	
	/**
	 * Tama�o de la respuesta a partir de la cual se lleva a cabo la compresion
	 */
	protected int compressionThreshold;
	
	/**
	 * Inicializa el filtro|
	 *
	 * @param filterConfig The filter configuration object
	 */
	public void init(FilterConfig filterConfig) {
		config = filterConfig;
		if (filterConfig != null) {
			String str = filterConfig.getInitParameter("compressionThreshold");
			if (str!=null) {
				compressionThreshold = Integer.parseInt(str);
				if (compressionThreshold != 0 && compressionThreshold < minThreshold) {
					//log.debug("compressionThreshold should be either 0 - no compression or >= " + minThreshold);
					//log.debug("compressionThreshold set to " + minThreshold);
					compressionThreshold = minThreshold;
				}
			} else {
				compressionThreshold = 0;
			}
		} else {
			compressionThreshold = 0;
		}
	}
	
	/**
	 * Termina el filtro
	 */
	public void destroy() {
		this.config = null;
	}
	
	/**
	* The <code>doFilter</code> method of the Filter is called by the container
	* each time a request/response pair is passed through the chain due
	* to a client request for a resource at the end of the chain.
	* The FilterChain passed into this method allows the Filter to pass on the
	* request and response to the next entity in the chain.<p>
	* This method first examines the request to check whether the client support
	* compression. <br>
	* It simply just pass the request and response if there is no support for
	* compression.<br>
	* If the compression support is available, it creates a
	* CompressionServletResponseWrapper object which compresses the content and
	* modifies the header if the content length is big enough.
	* It then invokes the next entity in the chain using the FilterChain object
	* (<code>chain.doFilter()</code>), <br>
	**/
	
	public void doFilter ( ServletRequest request, ServletResponse response,
			FilterChain chain ) throws IOException, ServletException {
		//System.out.println("doFilter: " + ((HttpServletRequest)request).getRequestURI());
		//log.debug("@doFilter");
		String extension = Comunes.getExtensionNombreArchivo(  ((HttpServletRequest)request).getRequestURI()) ;
		if ( "csv".equals(extension) || "txt".equals(extension) ) {
			((HttpServletResponse)response).setHeader( "Content-Disposition", "attachment;" );
		}
		if (compressionThreshold == 0) {
			//log.debug("doFilter gets called, but compressionTreshold is set to 0 - no compression");
			chain.doFilter(request, response);
			return;
		}
		
		boolean supportCompression = false;
		if (request instanceof HttpServletRequest) {
			//log.debug("requestURI = " + ((HttpServletRequest)request).getRequestURI());
			
			// Are we allowed to compress ?
			String s = (String) ((HttpServletRequest)request).getParameter("gzip");
			if ("false".equals(s)) {
			//log.debug("got parameter gzip=false --> don't compress, just chain filter");
				chain.doFilter(request, response);
				return;
			}
	
			Enumeration e =
			((HttpServletRequest)request).getHeaders("Accept-Encoding");
			while (e.hasMoreElements()) {
				String name = (String)e.nextElement();
				if (name.indexOf("gzip") != -1) {
					supportCompression = true;
				} 
			}
		}
	
		if (!supportCompression) {
			chain.doFilter(request, response);
			return;
		} else {
			if (response instanceof HttpServletResponse) {
				CompressionServletResponseWrapper wrappedResponse =
						new CompressionServletResponseWrapper((HttpServletResponse)response);
						wrappedResponse.setCompressionThreshold(compressionThreshold);
				try {
					chain.doFilter(request, wrappedResponse);
				} finally {
					wrappedResponse.finishResponse();
				}
				return;
			}
		}
	}
	
}
