package com.netro.filter;

import java.io.IOException;

import java.util.zip.GZIPOutputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

/**
 * Implementacion del ServletOutputStream para el filtro de compresion
 * @author Amy Roh
 * @author Dmitri Valdin
 */

public class CompressionResponseStream extends ServletOutputStream {
	
	private final static Log log = ServiceLocator.getInstance().getLog(CompressionFilter.class);

	/**
	 * Construye un stream de salida para el servlet asociado con el 
	 * response especificado.
	 *
	 * @param response El response asociado
	 */
	public CompressionResponseStream(HttpServletResponse response) throws IOException{
		super();
		closed = false;
		this.response = response;
		this.output = response.getOutputStream();
	}

	/**
	 * threshold que decide si se comprime o no la salida
	 * Para configurar en web.xml
	 */
	protected int compressionThreshold = 0;

	/**
	 * El buffer a traves del cual toda la salida pasa.
	 */
	protected byte[] buffer = null;

	/**
	 * La cantidad de bytes de  informaci�n actualmente en el buffer.
	 */
	protected int bufferCount = 0;

	/**
	 * El stream de salida en el cual se escribe la informacion.
	 */
	protected GZIPOutputStream gzipstream = null;

	/**
	 * Especifica si el stream ha sido cerrado
	 */
	protected boolean closed = false;

	/**
	 * The content length past which we will not write, or -1 if there is
	 * no defined content length.
	 */
	protected int length = -1;

	/**
	 * El response con el cual el stream de salida esta asociado
	 */
	protected HttpServletResponse response = null;

	/**
	 * El stream de salida al que se debe escribir.
	 */
	protected ServletOutputStream output = null;


	/**
	 * Establece compressionThreshold y crea el buffer para este tama�o
	 */
	protected void setBuffer(int threshold) {
		compressionThreshold = threshold;
		buffer = new byte[compressionThreshold];
		//log.debug("buffer is set to "+compressionThreshold);
	}

	/**
	 * Cierra el stream de salida, causando que cualquier informaci�n en el buffer 
	 * se le aplique un flush. Cualquier intento de escribir lanzar� IOException.
	 */
	public void close() throws IOException {

		if (closed)
			throw new IOException("This output stream has already been closed");
	
		if (gzipstream != null) {
			flushToGZip();
			gzipstream.close();
			gzipstream = null;
		} else {
			if (bufferCount > 0) {
				output.write(buffer, 0, bufferCount);
				bufferCount = 0;
			}
		}
		
		output.close();
		closed = true;
	}


	/**
	 * Flush la informaci�n en el buffer para el stream de salida
	 */
	public void flush() throws IOException {
	
		//log.debug("flush() @ CompressionResponseStream");
		if (closed) {
			throw new IOException("No se puede realizar un flush sobre un stream cerrado");
		}
		
		if (gzipstream != null) {
			gzipstream.flush();
		}
	
	}

	public void flushToGZip() throws IOException {
		//log.debug("flushToGZip() @ CompressionResponseStream");

		if (bufferCount > 0) {
			writeToGZip(buffer, 0, bufferCount);
			bufferCount = 0;
		}
	}

	/**
	 * Escribe los bytes especificados al stream de salida.
	 *
	 * @param b Bytes a ser escritos
	 *
	 * @exception IOException si un error de entrada/salida ocurre
	 */
	public void write(int b) throws IOException {
	
		if (closed)
			throw new IOException("No se puede escribir sobre un stream cerrado");
		
		if (bufferCount >= buffer.length) {
			flushToGZip();
		}
		
		buffer[bufferCount++] = (byte) b;
	}


	/**
	 * Escribe los bytes especificados como parametro al stream de sallida
	 *
	 * @param b Arreglo de bytes a escribir
	 *
	 * @exception IOException si un error de entrada/salida ocurre
	 */
	public void write(byte b[]) throws IOException {
		write(b, 0, b.length);
	}


	/**
	 * Escribe los bytes del arreglo, comenzando
	 * en el offset especificado.
	 *
	 * @param b Arreglo de bytes a ser escritos
	 * @param off Zero-relative offset de inicio de los bytes a ser escritos
	 * @param len cantidad de bytes a ser escritos
	 *
	 * @exception IOException si un error de entrada/salida ocurre
	 */
	public void write(byte b[], int off, int len) throws IOException {

		if (closed)
			throw new IOException("No se puede escribir a un sctream que ha sido cerrado");

		if (len == 0)
			return;

		// Can we write into buffer ?
		if (len <= (buffer.length - bufferCount)) {
			System.arraycopy(b, off, buffer, bufferCount, len);
			bufferCount += len;
			return;
		}

		// No hay suficiente espacio en el buffer. Flush...
		flushToGZip();

		// ... and try again. Note, that bufferCount = 0 here !
		if (len <= (buffer.length - bufferCount)) {
			System.arraycopy(b, off, buffer, bufferCount, len);
			bufferCount += len;
			return;
		}

		// write direct to gzip
		writeToGZip(b, off, len);
	}

	public void writeToGZip(byte b[], int off, int len) throws IOException {

		if (gzipstream == null) {
			response.addHeader("Content-Encoding", "gzip");
			gzipstream = new GZIPOutputStream(output);
		}
		gzipstream.write(b, off, len);
	}

	/**
	 * Determina si el stream ha sido cerrado o no
	 */
	public boolean closed() {
		return (this.closed);
	}
}
