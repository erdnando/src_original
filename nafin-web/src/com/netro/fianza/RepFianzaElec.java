package com.netro.fianza;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class RepFianzaElec implements IQueryGeneratorRegExtJS {
  
	public RepFianzaElec() {  }

	private List conditions;
	StringBuffer query;

	// PARAMETROS PARA CONSULTAS 
	private String icEpo = "";
	private String icAfianzadora = "";
	private String icFiado = "";
	private String icEstatus = "";
	private String dfEmisionIni = "";
	private String dfEmisionFin = "";
	private String dfVencIni =""; 
	private String dfVencFin =""; 
	private String usuario = "";
	private String fechaHoy;
	private String autorizador;
	
	
	private static final Log log = ServiceLocator.getInstance().getLog(RepFianzaElec.class);//Variable para enviar mensajes al log.
	private String perfil;



	public String getAggregateCalculationQuery() {
		
		return "";
 	}
		

	public String getDocumentQuery(){
		conditions = new ArrayList();	
		query 		= new StringBuffer();
			
		query.append(
				" SELECT  distinct  ff.ic_fianza cvefianza " +
				" FROM fe_fianza ff, "+
				"      fe_afianzadora fa,  " +
				"      fe_fiado fd , " +
				"			 BIT_FIANZA_ELECTRONICA bi "+
				" WHERE ff.ic_afianzadora = fa.ic_afianzadora  " +
				" AND ff.ic_fiado = fd.ic_fiado  "+
				" AND bi.ic_fianza = ff.ic_fianza "+
				" AND bi.ic_fiado = ff.ic_fiado "+
				" and bi.ic_afianzadora  = ff.ic_afianzadora"		 
				);
		
		if(!"".equals(icEpo)){
			query.append(" AND ff.ic_epo = ? ");
			conditions.add(new Integer(icEpo));
		}
	
		if(!"".equals(icAfianzadora)){
			query.append(" AND ff.ic_afianzadora = ? ");
			conditions.add( new Integer(icAfianzadora));
		}
		if(!"".equals(icFiado)){
			query.append(" AND ff.ic_fiado = ? ");
			conditions.add( new Integer(icFiado) );
		}
		if(!"".equals(usuario)){
			query.append(" AND ff.cg_usuario = ? ");
			conditions.add(usuario);
		}
		
		if(!"".equals(icEstatus)){
			query.append(" AND ff.cc_estatus = ? ");
			conditions.add(icEstatus);
		}
		
		if(!"".equals(dfEmisionIni)){
			query.append(" AND ff.df_publicacion >= to_date(?,'dd/mm/yyyy') ");
			conditions.add(dfEmisionIni);
		}
		if(!"".equals(dfEmisionFin)){
			query.append(" AND ff.df_publicacion < to_date(?,'dd/mm/yyyy')+1 ");
			conditions.add(dfEmisionFin);
		}
		if(!"".equals(dfVencIni)){
			query.append(" AND ff.df_vencimiento >= to_date(?,'dd/mm/yyyy') ");
			conditions.add(dfVencIni);
		}
		if(!"".equals(dfVencFin)){
			query.append(" AND ff.df_vencimiento < to_date(?,'dd/mm/yyyy')+1 ");
			conditions.add(dfVencFin);
		}
		
		if(!"".equals(fechaHoy)){		
		query.append(" AND bi.DF_FECHA >= TO_DATE (?, 'dd/mm/yyyy')  ");
    query.append("  AND bi.DF_FECHA < TO_DATE (?, 'dd/mm/yyyy') + 1 ");
		conditions.add(fechaHoy);
		conditions.add(fechaHoy);		 
		}
			
		if(!"".equals(autorizador)){
			if(!"ADMIN AUTO EPO".equals(perfil)){
			query.append(" AND ff.cg_autorizador = ? ");
			conditions.add(autorizador);
		 }else{
			query.append(" AND ff.cg_autorizador is not null ");			
		 }
		}
				
				
		query.append( " ORDER BY ff.ic_fianza " );
		
		log.debug("getDocumentQuery)"+query.toString());
		log.debug("getDocumentQuery)"+conditions);
		return query.toString();
 	}
	
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
		query 		= new StringBuffer();
		
		 
		
		query.append(
				"SELECT  distinct " +
				" fe.cg_descripcion as ESTATUS,    "+
        " fa.cg_razon_social as AFIANZADORA, "+
        " ff.cg_numero_fianza as NOFIANZA,"+
        " ff.cg_numero_contrato as NOCONTRATO, "+
        " ff.fg_monto as MONTO,"+
        " fr.cg_descripcion as RAMO, "+
        " fs.cg_descripcion as SUB_RAMO,"+
        " ff.cg_tipo_fianza as TIPO_FIANZA,"+
        " TO_CHAR (ff.df_publicacion, 'dd/mm/yyyy HH:mi:ss') as FECHA_EMISION,"+
        " TO_CHAR (ff.df_fin_vig_fianza, 'dd/mm/yyyy HH:mi:ss') as VIGENCIA,"+
        " fd.cg_nombre as FIADO, "+
        " ce.cg_razon_social as DESTINATARIO ,"+
        " ff.cg_benf_nombre as BENEFICIARIO,   "+             
				" REPLACE(REPLACE(REPLACE(ff.cg_observaciones,CHR(10),' ') ,CHR(13),' ') ,'  ',' ')  as OBSERVACIONES "+
				
				"  FROM fe_fianza ff,  " +
				"       fe_afianzadora fa,  " +
				"       fe_fiado fd,  " +
				"       comcat_epo ce,  " +
				"       fecat_estatus fe,  " +
				"       fecat_ramo fr,  " +
				"       fecat_subramo fs,  " +
				"			 BIT_FIANZA_ELECTRONICA bi "+
				" WHERE ff.ic_afianzadora = fa.ic_afianzadora  " +
				"   AND ff.ic_fiado = fd.ic_fiado  " +
				"   AND ff.ic_epo = ce.ic_epo  " +
				"   AND ff.cc_estatus = fe.cc_estatus  " +
				"   AND ff.cc_benf_idramo = fr.cc_ramo  " +
				"   AND ff.cc_benf_idsubramo = fs.cc_subramo  " +
				"   AND fr.cc_ramo = fs.cc_ramo  "+
				" AND bi.ic_fianza = ff.ic_fianza "+
				" AND bi.ic_fiado = ff.ic_fiado "+
				" and bi.ic_afianzadora  = ff.ic_afianzadora"		 
			 	);

						
		for(int i=0;i<pageIds.size();i++) {
			List lItem = (List)pageIds.get(i);
			if(i==0) {
				query.append(" AND ff.ic_fianza  IN ( ");
			}
			query.append("?");
			conditions.add(new Long(lItem.get(0).toString()));					
			if(i!=(pageIds.size()-1)) {
				query.append(",");
			} else {
				query.append(" ) ");
			}			
		}
		
		query.append( " ORDER BY  fecha_emision asc " );
	
	
		log.debug("getDocumentSummaryQueryForIds "+query.toString());
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
		
						
		return query.toString();
 	}
		
			
	public String getDocumentQueryFile(){
		conditions = new ArrayList();
		query = new StringBuffer();
		
		query.append(
				"SELECT   distinct "+	
				" fe.cc_estatus as noEstatus, "+
				" fe.cg_descripcion as ESTATUS,    "+
        " fa.cg_razon_social as AFIANZADORA, "+
        " ff.cg_numero_fianza as NOFIANZA,"+
        " ff.cg_numero_contrato as NOCONTRATO, "+
        " ff.fg_monto as MONTO,"+
        " fr.cg_descripcion as RAMO, "+
        " fs.cg_descripcion as SUB_RAMO,"+
        " ff.cg_tipo_fianza as TIPO_FIANZA,"+
        " TO_CHAR (ff.df_publicacion, 'dd/mm/yyyy HH:mi:ss') as FECHA_EMISION,"+
        " TO_CHAR (ff.df_fin_vig_fianza, 'dd/mm/yyyy HH:mi:ss') as VIGENCIA,"+
        " fd.cg_nombre as FIADO, "+
        " ce.cg_razon_social as DESTINATARIO ,"+
        " ff.cg_benf_nombre as BENEFICIARIO,   "+              
				" REPLACE(REPLACE(REPLACE(ff.cg_observaciones,CHR(10),' ') ,CHR(13),' ') ,'  ',' ')  as OBSERVACIONES "+
				
				"  FROM fe_fianza ff,  " +
				"       fe_afianzadora fa,  " +
				"       fe_fiado fd,  " +
				"       comcat_epo ce,  " +
				"       fecat_estatus fe,  " +
				"       fecat_ramo fr,  " +
				"       fecat_subramo fs,  " +
				"			 BIT_FIANZA_ELECTRONICA bi "+
				" WHERE ff.ic_afianzadora = fa.ic_afianzadora  " +
				"   AND ff.ic_fiado = fd.ic_fiado  " +
				"   AND ff.ic_epo = ce.ic_epo  " +
				"   AND ff.cc_estatus = fe.cc_estatus  " +
				"   AND ff.cc_benf_idramo = fr.cc_ramo  " +
				"   AND ff.cc_benf_idsubramo = fs.cc_subramo  " +
				"   AND fr.cc_ramo = fs.cc_ramo    "+
				" AND bi.ic_fianza = ff.ic_fianza "+
				" AND bi.ic_fiado = ff.ic_fiado "+
				" and bi.ic_afianzadora  = ff.ic_afianzadora"		 
				);
		
	
		if(!"".equals(icEpo)){
			query.append(" AND ff.ic_epo = ? ");
			conditions.add(new Integer(icEpo));
		}	
		if(!"".equals(icAfianzadora)){
			query.append(" AND ff.ic_afianzadora = ? ");
			conditions.add( new Integer(icAfianzadora));
		}
		if(!"".equals(icFiado)){
			query.append(" AND fd.ic_fiado = ? ");
			conditions.add( new Integer(icFiado) );
		}
		
		if(!"".equals(usuario)){
			query.append(" AND ff.cg_usuario = ? ");
			conditions.add(usuario);
		}	
		
		if(!"".equals(icEstatus)){
			query.append(" AND fe.cc_estatus = ? ");
			conditions.add(icEstatus);
		}
			if(!"".equals(dfEmisionIni)){
			query.append(" AND ff.df_publicacion >= to_date(?,'dd/mm/yyyy') ");
			conditions.add(dfEmisionIni);
		}
		if(!"".equals(dfEmisionFin)){
			query.append(" AND ff.df_publicacion < to_date(?,'dd/mm/yyyy')+1 ");
			conditions.add(dfEmisionFin);
		}
		
		if(!"".equals(dfVencIni)){
			query.append(" AND ff.df_vencimiento >= to_date(?,'dd/mm/yyyy') ");
			conditions.add(dfVencIni);
		}
		if(!"".equals(dfVencFin)){
			query.append(" AND ff.df_vencimiento < to_date(?,'dd/mm/yyyy')+1 ");
			conditions.add(dfVencFin);
		}
		
		if(!"".equals(fechaHoy)){		
		query.append(" AND bi.DF_FECHA >= TO_DATE (?, 'dd/mm/yyyy')  ");
    query.append("  AND bi.DF_FECHA < TO_DATE (?, 'dd/mm/yyyy') + 1 ");
		conditions.add(fechaHoy);
		conditions.add(fechaHoy);		 
		}
		
		if(!"".equals(autorizador)){
			if(!"ADMIN AUTO EPO".equals(perfil)){
			query.append(" AND ff.cg_autorizador = ? ");
			conditions.add(autorizador);
		 }else{
			query.append(" AND ff.cg_autorizador is not null ");			
		 }
		}
		
		query.append( " ORDER BY  ESTATUS, fecha_emision asc " );
		
		log.debug("getDocumentQueryFile "+query.toString());
		log.debug("getDocumentQueryFile)"+conditions);
	
		return query.toString();
 	} 		
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";
		String lineaT = "";
		String  estatusAnterior = "";		
		int totales =0;
		double montosTotales=0; 
		String  lineaTotales ="";
		String Totalmontos ="";
		
		if ("CSV".equals(tipo)) {
			lineaT = "Afianzadora, No. Fianza,No. Contrato,Monto,Ramo,Sub Ramo,Tipo Fianza,Fecha de Emisi�n,Vigencia,Fiado,Destinatario,Beneficiario,Observaciones\n";
			try {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
				buffer = new BufferedWriter(writer);
				
			
				while (rs.next()) {
					String nomAfinza = (rs.getString("AFIANZADORA")==null)?"":rs.getString("AFIANZADORA");
					String numFianza = (rs.getString("NOFIANZA")==null)?"":rs.getString("NOFIANZA");
					String numContrato = (rs.getString("NOCONTRATO")==null)?"":rs.getString("NOCONTRATO");
					String monTototal = (rs.getString("MONTO")==null)?"":rs.getString("MONTO");
					String nomRamo = (rs.getString("RAMO")==null)?"":rs.getString("RAMO");
					String nomSubramo = (rs.getString("SUB_RAMO")==null)?"":rs.getString("SUB_RAMO");
					String tipoFianza = (rs.getString("TIPO_FIANZA")==null)?"":rs.getString("TIPO_FIANZA");
					String fecAutoriza = (rs.getString("FECHA_EMISION")==null)?"":rs.getString("FECHA_EMISION");
					String fecVigencia = (rs.getString("VIGENCIA")==null)?"":rs.getString("VIGENCIA");
					String nomFiado = (rs.getString("FIADO")==null)?"":rs.getString("FIADO");
					String nomDest = (rs.getString("DESTINATARIO")==null)?"":rs.getString("DESTINATARIO");
					String nomBenef = (rs.getString("BENEFICIARIO")==null)?"":rs.getString("BENEFICIARIO");
					String observaciones = (rs.getString("OBSERVACIONES")==null)?"":rs.getString("OBSERVACIONES");
					String nomEstatus = (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");
					String noEstatus  = (rs.getString("noEstatus")==null)?"":rs.getString("noEstatus"); 
					 
					
					if(!estatusAnterior.equals(noEstatus)) {
						buffer.write(lineaTotales);
						totales = 1;
						montosTotales = Double.parseDouble((String)monTototal);
						lineaTotales =  "Totales, "+totales+", , " + montosTotales + "\n";

						String lineaEs = nomEstatus.replace(',',' ')+"\n";
						buffer.write("\n");
						buffer.write(lineaEs); //linea Descripcion Estatus
						buffer.write(lineaT); //linea Descripcion Titulos 						
					} else {
						totales++;						
						montosTotales +=Double.parseDouble(monTototal);
						lineaTotales =  "Totales, "+totales+", , "+ montosTotales +"\n";						
					}		
					 
					linea = nomAfinza.replace(',',' ')+", " +
							numFianza.replace(',',' ')+", " +
							numContrato.replace(',',' ')+", " +
							monTototal.replace(',',' ')+", " +												
							nomRamo.replace(',',' ')+", " +
							nomSubramo.replace(',',' ')+", " +
							tipoFianza.replace(',',' ')+", " +
							fecAutoriza.replace(',',' ')+", " +
							fecVigencia.replace(',',' ')+", " +
							nomFiado.replace(',',' ')+", " +
							nomDest.replace(',',' ')+", " +
							nomBenef.replace(',',' ')+", " +							
							observaciones+", "+"\n";
					
					buffer.write(linea); //linea registos 
							
						
				estatusAnterior = noEstatus; 
						
				}//while
				buffer.write(lineaTotales); //esta es para escribir el ultimo registro
					 
				buffer.close();
			} catch (Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		} else if ("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				(session.getAttribute("iNoNafinElectronico")==null?"":session.getAttribute("iNoNafinElectronico")).toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
					
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				pdfDoc.addText("","formas",ComunesPDF.CENTER);			
				pdfDoc.addText("REPORTE FIANZAS POR ESTATUS ","formas",ComunesPDF.CENTER);
				pdfDoc.addText("","formas",ComunesPDF.CENTER);
				
				pdfDoc.setTable(13,100);	
				
					while (rs.next()) {
					String nomAfinza = (rs.getString("AFIANZADORA")==null)?"":rs.getString("AFIANZADORA");
					String numFianza = (rs.getString("NOFIANZA")==null)?"":rs.getString("NOFIANZA");
					String numContrato = (rs.getString("NOCONTRATO")==null)?"":rs.getString("NOCONTRATO");
					String monTototal = (rs.getString("MONTO")==null)?"":rs.getString("MONTO");
					String nomRamo = (rs.getString("RAMO")==null)?"":rs.getString("RAMO");
					String nomSubramo = (rs.getString("SUB_RAMO")==null)?"":rs.getString("SUB_RAMO");
					String tipoFianza = (rs.getString("TIPO_FIANZA")==null)?"":rs.getString("TIPO_FIANZA");
					String fecAutoriza = (rs.getString("FECHA_EMISION")==null)?"":rs.getString("FECHA_EMISION");
					String fecVigencia = (rs.getString("VIGENCIA")==null)?"":rs.getString("VIGENCIA");
					String nomFiado = (rs.getString("FIADO")==null)?"":rs.getString("FIADO");
					String nomDest = (rs.getString("DESTINATARIO")==null)?"":rs.getString("DESTINATARIO");
					String nomBenef = (rs.getString("BENEFICIARIO")==null)?"":rs.getString("BENEFICIARIO");
					String observaciones = (rs.getString("OBSERVACIONES")==null)?"":rs.getString("OBSERVACIONES");
					String nomEstatus = (rs.getString("ESTATUS")==null)?"":rs.getString("ESTATUS");
					String noEstatus  = (rs.getString("noEstatus")==null)?"":rs.getString("noEstatus");
								
				if(!estatusAnterior.equals(noEstatus)) {	 //para poner los encabezados 						
					
					if(!lineaTotales.equals("")) {
					 	pdfDoc.setCell(lineaTotales,"formas",ComunesPDF.LEFT,3,2);	
						pdfDoc.setCell(Totalmontos,"formas",ComunesPDF.RIGHT,1,2);	
						pdfDoc.setCell(" ","formas",ComunesPDF.LEFT,9,2);
						pdfDoc.setCell("   ","formas",ComunesPDF.LEFT,13,2);
					}
						pdfDoc.setCell(nomEstatus,"celda01",ComunesPDF.LEFT,13,2);
						pdfDoc.setCell("Afianzadora","celda01",ComunesPDF.CENTER,1,2);
						pdfDoc.setCell("No. Fianza","celda01",ComunesPDF.CENTER,1,2);
						pdfDoc.setCell("No. Contrato","celda01",ComunesPDF.CENTER,1,2);
						pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER,1,2);
						pdfDoc.setCell("Ramo","celda01",ComunesPDF.CENTER,1,2);
						pdfDoc.setCell("Sub Ramo","celda01",ComunesPDF.CENTER,1,2);
						pdfDoc.setCell("Tipo Fianza","celda01",ComunesPDF.CENTER,1,2);
						pdfDoc.setCell("Fecha de Emisi�n","celda01",ComunesPDF.CENTER,1,2);
						pdfDoc.setCell("Vigencia","celda01",ComunesPDF.CENTER,1,2);
						pdfDoc.setCell("Fiado","celda01",ComunesPDF.CENTER,1,2);
						pdfDoc.setCell("Destinatario","celda01",ComunesPDF.CENTER,1,2);
						pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER,1,2);				
						pdfDoc.setCell("Observaciones","celda01",ComunesPDF.CENTER,1,2);
						
						totales = 1;
						montosTotales = Double.parseDouble((String)monTototal);
						lineaTotales =  "Totales    "+totales;    
						Totalmontos  =  "$"+Comunes.formatoDecimal(montosTotales,2);	
						
					}	else {
						totales++;						
						montosTotales +=Double.parseDouble(monTototal);
						lineaTotales =  "Totales "+totales;		
						Totalmontos  =  "$"+Comunes.formatoDecimal(montosTotales,2);	
					}
				
					pdfDoc.setCell(nomAfinza,"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(numFianza,"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(numContrato,"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monTototal,2),"formas",ComunesPDF.RIGHT,1,2);
					pdfDoc.setCell(nomRamo,"formas",ComunesPDF.CENTER,1,2);					
					pdfDoc.setCell(nomSubramo,"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(tipoFianza,"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(fecAutoriza,"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(fecVigencia,"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(nomFiado,"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(nomDest,"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(nomBenef,"formas",ComunesPDF.CENTER,1,2);					
					pdfDoc.setCell(observaciones,"formas",ComunesPDF.CENTER,1,2);
					
					estatusAnterior = noEstatus; 
					
				}
				if(!lineaTotales.equals("")) {
					pdfDoc.setCell(lineaTotales,"formas",ComunesPDF.LEFT,3,2);	
					pdfDoc.setCell(Totalmontos,"formas",ComunesPDF.RIGHT,1,2);	
					pdfDoc.setCell(" ","formas",ComunesPDF.LEFT,9,2);
				}
				
				pdfDoc.addTable();
				
				pdfDoc.endDocument();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		}

		return nombreArchivo;
	}
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}

	
	public List getConditions() {
		return conditions;
	}


	public void setIcEpo(String icEpo) {
		this.icEpo = icEpo;
	}


	public String getIcEpo() {
		return icEpo;
	}


	public void setIcAfianzadora(String icAfianzadora) {
		this.icAfianzadora = icAfianzadora;
	}


	public String getIcAfianzadora() {
		return icAfianzadora;
	}


	public void setIcFiado(String icFiado) {
		this.icFiado = icFiado;
	}


	public String getIcFiado() {
		return icFiado;
	}


	public void setIcEstatus(String icEstatus) {
		this.icEstatus = icEstatus;
	}


	public String getIcEstatus() {
		return icEstatus;
	}

	public void setDfVencIni(String dfVencIni) {
		this.dfVencIni = dfVencIni;
	}


	public String getDfVencIni() {
		return dfVencIni;
	}


	public void setDfVencFin(String dfVencFin) {
		this.dfVencFin = dfVencFin;
	}


	public String getDfVencFin() {
		return dfVencFin;
	}


	public void setDfEmisionIni(String dfEmisionIni) {
		this.dfEmisionIni = dfEmisionIni;
	}


	public String getDfEmisionIni() {
		return dfEmisionIni;
	}


public void setDfEmisionFin(String dfEmisionFin) {
		this.dfEmisionFin = dfEmisionFin;
	}


	public String getDfEmisionFin() {
		return dfEmisionFin;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}


	public String getUsuario() {
		return usuario;
	}

	public String getFechaHoy() {
		return fechaHoy;
	}

	public void setFechaHoy(String fechaHoy) {
		this.fechaHoy = fechaHoy;
	}

	public String getAutorizador() {
		return autorizador;
	} 

	public void setAutorizador(String autorizador) {
		this.autorizador = autorizador;
	}

	public String getPerfil() {
		return perfil;
	}

	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}
  
}