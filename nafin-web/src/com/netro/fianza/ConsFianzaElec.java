package com.netro.fianza;

import com.netro.pdf.ComunesPDF;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.IQueryGeneratorRegExtJS;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class ConsFianzaElec implements IQueryGeneratorRegExtJS {

	public ConsFianzaElec() {  }

	private List conditions;
	StringBuffer query;

	// PARAMETROS PARA CONSULTAS 
	private String icEpo = "";
	private String icAfianzadora = "";
	private String icFiado = "";
	private String icEstatus = "";
	private String numFianza = "";
	private String numContrato = "";
	private String dfSolicIni = "";
	private String dfSolicFin = "";
	private String dfVencIni = "";
	private String dfVencFin = "";
	private String dfPublicIni = "";
	private String dfAutorizaIni = "";
	private String dfAutorizaFin = "";
	private String dfRechazaIni = "";
	private String dfRechazaFin = "";
	private String usuario = "";
	private String autorizador  = "";
	private String ramo  = "";
	private String subramo  = "";
	private String estrato  = "";
	private String perfil  = "";
	
	private static final Log log = ServiceLocator.getInstance().getLog(ConsFianzaElec.class);//Variable para enviar mensajes al log.

	public String getAggregateCalculationQuery() {
		return "";
 	}
		

	public String getDocumentQuery(){
		conditions = new ArrayList();	
		query 		= new StringBuffer();
			
		query.append(
				" SELECT ff.ic_fianza cvefianza " +
				" FROM fe_fianza ff, "+
				"      fe_afianzadora fa,  " +
				"      fe_fiado fd  " +
				" WHERE ff.ic_afianzadora = fa.ic_afianzadora  " +
				" AND ff.ic_fiado = fd.ic_fiado  "
				);
		
		if(!"".equals(icEpo)){
			query.append(" AND ff.ic_epo = ? ");
			conditions.add(new Integer(icEpo));
		}
		if(!"".equals(usuario)){
			query.append(" AND ff.cg_usuario = ? ");
			conditions.add(usuario);
		}
		if(!"".equals(icAfianzadora)){
			query.append(" AND ff.ic_afianzadora = ? ");
			conditions.add( new Integer(icAfianzadora));
		}
		if(!"".equals(icFiado)){
			query.append(" AND ff.ic_fiado = ? ");
			conditions.add( new Integer(icFiado) );
		}
		if(!"".equals(icEstatus)){
			query.append(" AND ff.cc_estatus = ? ");
			conditions.add(icEstatus);
		}
		if(!"".equals(numFianza)){
			query.append(" AND ff.cg_numero_fianza = ? ");
			conditions.add(numFianza);
		}
		if(!"".equals(numContrato)){
			query.append(" AND ff.cg_numero_contrato = ? ");
			conditions.add(numContrato);
		}
		if(!"".equals(ramo)){
			query.append(" AND ff.cc_benf_idramo = ? ");
			conditions.add(ramo);
		}
		if(!"".equals(subramo)){
			query.append(" AND ff.cc_benf_idsubramo = ? ");
			conditions.add(subramo);
		}
		if(!"".equals(estrato)){
			query.append(" AND fd.ic_estrato = ? ");
			conditions.add(estrato);
		}
		if(!"".equals(dfSolicIni)){
			query.append(" AND ff.df_publicacion >= to_date(?,'dd/mm/yyyy') ");
			conditions.add(dfSolicIni);
		}
		if(!"".equals(dfSolicFin)){
			query.append(" AND ff.df_publicacion < to_date(?,'dd/mm/yyyy')+1 ");
			conditions.add(dfSolicFin);
		}
		if(!"".equals(dfVencIni)){
			query.append(" AND ff.df_vencimiento >= to_date(?,'dd/mm/yyyy') ");
			conditions.add(dfVencIni);
		}
		if(!"".equals(dfVencFin)){
			query.append(" AND ff.df_vencimiento < to_date(?,'dd/mm/yyyy')+1 ");
			conditions.add(dfVencFin);
		}
		if(!"".equals(dfAutorizaIni)){
			query.append(" AND ff.df_autorizada >= to_date(?,'dd/mm/yyyy') ");
			conditions.add(dfAutorizaIni);
		}
		if(!"".equals(dfAutorizaFin)){
			query.append(" AND ff.df_autorizada < to_date(?,'dd/mm/yyyy')+1 ");
			conditions.add(dfAutorizaFin);
		}
		if(!"".equals(dfRechazaIni)){
			query.append(" AND ff.df_rechazada >= to_date(?,'dd/mm/yyyy') ");
			conditions.add(dfRechazaIni);
		}
		if(!"".equals(dfRechazaFin)){
			query.append(" AND ff.df_rechazada < to_date(?,'dd/mm/yyyy')+1 ");
			conditions.add(dfRechazaFin);
		}
		if(!"".equals(autorizador)){
			if(!"ADMIN AUTO EPO".equals(perfil)){
				query.append(" AND (ff.cg_autorizador = ? OR ff.cg_autorizador = 'T') ");
				conditions.add(autorizador);
			}else{
				query.append(" AND ff.cg_autorizador is not null ");
			}
			
		}
		/*
		if(!"".equals(dfPublicIni)){
			query.append(" AND ff.df_publicacion >= to_date(?,'dd/mm/yyyy') ");
			conditions.add(dfPublicIni);
		}
		if(!"".equals(dfPublicFin)){
			query.append(" AND ff.df_publicacion < to_date(?,'dd/mm/yyyy')+1 ");
			conditions.add(dfPublicFin);
		}
		*/
		query.append( " ORDER BY ff.ic_fianza " );
		
		log.debug("getDocumentQuery)"+query.toString());
		log.debug("getDocumentQuery)"+conditions);
		return query.toString();
 	}
	
	
	public String getDocumentSummaryQueryForIds(List pageIds){
		conditions = new ArrayList();
		query 		= new StringBuffer();
		
		log.debug("pageIds)"+pageIds);
			
		query.append(
				"SELECT ff.ic_fianza cvefianza, fa.ic_afianzadora cveafianza,  " +
				"       fe.cc_estatus estatus, fr.cc_ramo ramos, fs.cc_subramo subramo,  " +
				"       fa.cg_razon_social nomafinza, ff.cg_numero_fianza numfianza,  " +
				"       ff.cg_numero_contrato numcontrato, ff.fg_monto montototal,  " +
				"       fr.cg_descripcion nomramo, fs.cg_descripcion nomsubramo,  " +
				"       ff.cg_tipo_fianza tipofianza, to_char(ff.df_publicacion,'dd/mm/yyyy HH:mi:ss') fecautoriza,  " +
				"       to_char(ff.df_fin_vig_fianza,'dd/mm/yyyy HH:mi:ss') fecvigencia, fd.cg_nombre nomfiado,  " +
				"       ce.cg_razon_social nomdest, ff.cg_benf_nombre nombenef,  " +
				"       fe.cg_descripcion nomestatus, fa.cg_url_validacion_fianza urlafianza,  " +
				//"       ff.bi_pdf pdf, ff.cg_observaciones observaciones  " +
				"       ff.cg_observaciones observaciones  " +
				"  FROM fe_fianza ff,  " +
				"       fe_afianzadora fa,  " +
				"       fe_fiado fd,  " +
				"       comcat_epo ce,  " +
				"       fecat_estatus fe,  " +
				"       fecat_ramo fr,  " +
				"       fecat_subramo fs  " +
				" WHERE ff.ic_afianzadora = fa.ic_afianzadora  " +
				"   AND ff.ic_fiado = fd.ic_fiado  " +
				"   AND ff.ic_epo = ce.ic_epo  " +
				"   AND ff.cc_estatus = fe.cc_estatus  " +
				"   AND ff.cc_benf_idramo = fr.cc_ramo  " +
				"   AND ff.cc_benf_idsubramo = fs.cc_subramo  " +
				"   AND fr.cc_ramo = fs.cc_ramo  "
			 	);


		/*
		query.append( " AND ff.ic_fianza = ? " );
		conditions.add(claveIf );
		*/
						
		for(int i=0;i<pageIds.size();i++) {
			List lItem = (List)pageIds.get(i);
			if(i==0) {
				query.append(" AND ff.ic_fianza  IN ( ");
			}
			query.append("?");
			conditions.add(new Long(lItem.get(0).toString()));					
			if(i!=(pageIds.size()-1)) {
				query.append(",");
			} else {
				query.append(" ) ");
			}			
		}
		
		query.append( " ORDER BY ff.ic_fianza" );
	
	
		log.debug("getDocumentSummaryQueryForIds "+query.toString());
		log.debug("getDocumentSummaryQueryForIds)"+conditions);
						
		return query.toString();
 	}
		
			
	public String getDocumentQueryFile(){
		conditions = new ArrayList();
		query = new StringBuffer();
		
		query.append(
				"SELECT ff.ic_fianza cvefianza, fa.ic_afianzadora cveafianza,  " +
				"       fe.cc_estatus estatus, fr.cc_ramo ramos, fs.cc_subramo subramo,  " +
				"       fa.cg_razon_social nomafinza, ff.cg_numero_fianza numfianza,  " +
				"       ff.cg_numero_contrato numcontrato, ff.fg_monto montototal,  " +
				"       fr.cg_descripcion nomramo, fs.cg_descripcion nomsubramo,  " +
				"       ff.cg_tipo_fianza tipofianza, to_char(ff.df_publicacion,'dd/mm/yyyy HH:mi:ss') fecautoriza,  " +
				"       to_char(ff.df_fin_vig_fianza,'dd/mm/yyyy HH:mi:ss') fecvigencia, fd.cg_nombre nomfiado,  " +
				"       ce.cg_razon_social nomdest, ff.cg_benf_nombre nombenef,  " +
				"       fe.cg_descripcion nomestatus, fa.cg_url_validacion_fianza urlafianza,  " +
				"       ff.bi_pdf pdf, ff.cg_observaciones observaciones  " +
				"  FROM fe_fianza ff,  " +
				"       fe_afianzadora fa,  " +
				"       fe_fiado fd,  " +
				"       comcat_epo ce,  " +
				"       fecat_estatus fe,  " +
				"       fecat_ramo fr,  " +
				"       fecat_subramo fs  " +
				" WHERE ff.ic_afianzadora = fa.ic_afianzadora  " +
				"   AND ff.ic_fiado = fd.ic_fiado  " +
				"   AND ff.ic_epo = ce.ic_epo  " +
				"   AND ff.cc_estatus = fe.cc_estatus  " +
				"   AND ff.cc_benf_idramo = fr.cc_ramo  " +
				"   AND ff.cc_benf_idsubramo = fs.cc_subramo  " +
				"   AND fr.cc_ramo = fs.cc_ramo  "
				);
		
		if(!"".equals(icEpo)){
			query.append(" AND ff.ic_epo = ? ");
			conditions.add(new Integer(icEpo));
		}
		if(!"".equals(usuario)){
			query.append(" AND ff.cg_usuario = ? ");
			conditions.add(usuario);
		}
		if(!"".equals(icAfianzadora)){
			query.append(" AND ff.ic_afianzadora = ? ");
			conditions.add( new Integer(icAfianzadora));
		}
		if(!"".equals(icFiado)){
			query.append(" AND fd.ic_fiado = ? ");
			conditions.add( new Integer(icFiado) );
		}
		if(!"".equals(icEstatus)){
			query.append(" AND fe.cc_estatus = ? ");
			conditions.add(icEstatus);
		}
		if(!"".equals(numFianza)){
			query.append(" AND ff.cg_numero_fianza = ? ");
			conditions.add(numFianza);
		}
		if(!"".equals(numContrato)){
			query.append(" AND ff.cg_numero_contrato = ? ");
			conditions.add(numContrato);
		}
		if(!"".equals(ramo)){
			query.append(" AND ff.cc_benf_idramo = ? ");
			conditions.add(ramo);
		}
		if(!"".equals(subramo)){
			query.append(" AND ff.cc_benf_idsubramo = ? ");
			conditions.add(subramo);
		}
		if(!"".equals(estrato)){
			query.append(" AND fd.ic_estrato = ? ");
			conditions.add(estrato);
		}
		if(!"".equals(dfSolicIni)){
			query.append(" AND ff.df_publicacion >= to_date(?,'dd/mm/yyyy') ");
			conditions.add(dfSolicIni);
		}
		if(!"".equals(dfSolicFin)){
			query.append(" AND ff.df_publicacion < to_date(?,'dd/mm/yyyy')+1 ");
			conditions.add(dfSolicFin);
		}
		if(!"".equals(dfVencIni)){
			query.append(" AND ff.df_vencimiento >= to_date(?,'dd/mm/yyyy') ");
			conditions.add(dfVencIni);
		}
		if(!"".equals(dfVencFin)){
			query.append(" AND ff.df_vencimiento < to_date(?,'dd/mm/yyyy')+1 ");
			conditions.add(dfVencFin);
		}
		if(!"".equals(dfAutorizaIni)){
			query.append(" AND ff.df_autorizada >= to_date(?,'dd/mm/yyyy') ");
			conditions.add(dfAutorizaIni);
		}
		if(!"".equals(dfAutorizaFin)){
			query.append(" AND ff.df_autorizada < to_date(?,'dd/mm/yyyy')+1 ");
			conditions.add(dfAutorizaFin);
		}
		if(!"".equals(dfRechazaIni)){
			query.append(" AND ff.df_rechazada >= to_date(?,'dd/mm/yyyy') ");
			conditions.add(dfRechazaIni);
		}
		if(!"".equals(dfRechazaFin)){
			query.append(" AND ff.df_rechazada < to_date(?,'dd/mm/yyyy')+1 ");
			conditions.add(dfRechazaFin);
		}
		if(!"".equals(autorizador)){
			if(!"ADMIN AUTO EPO".equals(perfil)){
				query.append(" AND ff.cg_autorizador = ? ");
				conditions.add(autorizador);
			}else{
				query.append(" AND ff.cg_autorizador is not null ");
			}
			
		}
		
		query.append( " ORDER BY ff.ic_fianza" );
		
		log.debug("getDocumentQueryFile "+query.toString());
		log.debug("getDocumentQueryFile)"+conditions);
	
		return query.toString();
 	} 		
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el resultset que se recibe como par�metro. El ResulSet es el
	 * resultado de ejecutar la consulta obtenida al invocar getDocumentQueryFile()
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param rs ResultSet obtenido al ejecutar la consulta especificada en getDocumentQueryFile()
	 * @param path Ruta fisica donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearCustomFile(HttpServletRequest request, java.sql.ResultSet rs, String path, String tipo) {
		String linea = "";
		OutputStreamWriter writer = null;
		BufferedWriter buffer = null;
		String nombreArchivo = "";

		if ("CSV".equals(tipo)) {
			linea = "Afianzadora,No. Fianza,No. Contrato,Monto,Ramo,Sub Ramo,Tipo Fianza,Fecha de Emisi�n,Vigencia,Fiado,Destinatario,Beneficiario,Estatus,Observaciones\n";
			try {
				nombreArchivo = Comunes.cadenaAleatoria(16) + ".csv";
				writer = new OutputStreamWriter(new FileOutputStream(path + nombreArchivo, true), "ISO-8859-1");
				buffer = new BufferedWriter(writer);
				buffer.write(linea);

				while (rs.next()) {
					String nomAfinza = (rs.getString("NOMAFINZA")==null)?"":rs.getString("NOMAFINZA");
					String numFianza = (rs.getString("NUMFIANZA")==null)?"":rs.getString("NUMFIANZA");
					String numContrato = (rs.getString("NUMCONTRATO")==null)?"":rs.getString("NUMCONTRATO");
					String monTototal = (rs.getString("MONTOTOTAL")==null)?"":rs.getString("MONTOTOTAL");
					String nomRamo = (rs.getString("NOMRAMO")==null)?"":rs.getString("NOMRAMO");
					String nomSubramo = (rs.getString("NOMSUBRAMO")==null)?"":rs.getString("NOMSUBRAMO");
					String tipoFianza = (rs.getString("TIPOFIANZA")==null)?"":rs.getString("TIPOFIANZA");
					String fecAutoriza = (rs.getString("FECAUTORIZA")==null)?"":rs.getString("FECAUTORIZA");
					String fecVigencia = (rs.getString("FECVIGENCIA")==null)?"":rs.getString("FECVIGENCIA");
					String nomFiado = (rs.getString("NOMFIADO")==null)?"":rs.getString("NOMFIADO");
					String nomDest = (rs.getString("NOMDEST")==null)?"":rs.getString("NOMDEST");
					String nomBenef = (rs.getString("NOMBENEF")==null)?"":rs.getString("NOMBENEF");
					String nomEstatus = (rs.getString("NOMESTATUS")==null)?"":rs.getString("NOMESTATUS");
					String observaciones = (rs.getString("OBSERVACIONES")==null)?"":rs.getString("OBSERVACIONES");
				
					linea = nomAfinza.replace(',',' ')+", " +
							numFianza.replace(',',' ')+", " +
							numContrato.replace(',',' ')+", " +
							monTototal.replace(',',' ')+", " +
							nomRamo.replace(',',' ')+", " +
							nomSubramo.replace(',',' ')+", " +
							tipoFianza.replace(',',' ')+", " +
							fecAutoriza.replace(',',' ')+", " +
							fecVigencia.replace(',',' ')+", " +
							nomFiado.replace(',',' ')+", " +
							nomDest.replace(',',' ')+", " +
							nomBenef.replace(',',' ')+", " +
							nomEstatus.replace(',',' ')+", " +
							observaciones.replace(',',' ')+", "+"\n";
					
					buffer.write(linea);
				}
				buffer.close();
			} catch (Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		} else if ("PDF".equals(tipo)) {
			try {
				HttpSession session = request.getSession();

				nombreArchivo = Comunes.cadenaAleatoria(16) + ".pdf";
				
				ComunesPDF pdfDoc = new ComunesPDF(2, path + nombreArchivo);
				
				String meses[] = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
				String fechaActual  = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new java.util.Date());
				String diaActual    = fechaActual.substring(0,2);
				String mesActual    = meses[Integer.parseInt(fechaActual.substring(3,5))-1];
				String anioActual   = fechaActual.substring(6,10);
				String horaActual  = new java.text.SimpleDateFormat("HH:mm:ss").format(new java.util.Date());
					
				pdfDoc.encabezadoConImagenes(pdfDoc,(String)session.getAttribute("strPais"),
				(session.getAttribute("iNoNafinElectronico")==null?"":session.getAttribute("iNoNafinElectronico")).toString(),
				(String)session.getAttribute("sesExterno"),
				(String) session.getAttribute("strNombre"),
				(String) session.getAttribute("strNombreUsuario"),
				(String)session.getAttribute("strLogo"),(String)session.getServletContext().getAttribute("strDirectorioPublicacion"));	
					
				pdfDoc.addText("M�xico, D.F. a "+diaActual+" de "+mesActual+" del "+anioActual+" ----------------------------- "+horaActual,"formas",ComunesPDF.RIGHT);
				
				pdfDoc.setTable(14,100);
				pdfDoc.setCell("Afianzadora","celda01",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell("No. Fianza","celda01",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell("No. Contrato","celda01",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell("Monto","celda01",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell("Ramo","celda01",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell("Sub Ramo","celda01",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell("Tipo Fianza","celda01",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell("Fecha de Emisi�n","celda01",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell("Vigencia","celda01",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell("Fiado","celda01",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell("Destinatario","celda01",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell("Beneficiario","celda01",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell("Estatus","celda01",ComunesPDF.CENTER,1,2);
				pdfDoc.setCell("Observaciones","celda01",ComunesPDF.CENTER,1,2);
			
				while (rs.next()) {
					String nomAfinza = (rs.getString("NOMAFINZA")==null)?"":rs.getString("NOMAFINZA");
					String numFianza = (rs.getString("NUMFIANZA")==null)?"":rs.getString("NUMFIANZA");
					String numContrato = (rs.getString("NUMCONTRATO")==null)?"":rs.getString("NUMCONTRATO");
					String monTototal = (rs.getString("MONTOTOTAL")==null)?"":rs.getString("MONTOTOTAL");
					String nomRamo = (rs.getString("NOMRAMO")==null)?"":rs.getString("NOMRAMO");
					String nomSubramo = (rs.getString("NOMSUBRAMO")==null)?"":rs.getString("NOMSUBRAMO");
					String tipoFianza = (rs.getString("TIPOFIANZA")==null)?"":rs.getString("TIPOFIANZA");
					String fecAutoriza = (rs.getString("FECAUTORIZA")==null)?"":rs.getString("FECAUTORIZA");
					String fecVigencia = (rs.getString("FECVIGENCIA")==null)?"":rs.getString("FECVIGENCIA");
					String nomFiado = (rs.getString("NOMFIADO")==null)?"":rs.getString("NOMFIADO");
					String nomDest = (rs.getString("NOMDEST")==null)?"":rs.getString("NOMDEST");
					String nomBenef = (rs.getString("NOMBENEF")==null)?"":rs.getString("NOMBENEF");
					String nomEstatus = (rs.getString("NOMESTATUS")==null)?"":rs.getString("NOMESTATUS");
					String observaciones = (rs.getString("OBSERVACIONES")==null)?"":rs.getString("OBSERVACIONES");
		
					pdfDoc.setCell(nomAfinza,"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(numFianza,"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(numContrato,"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell("$"+Comunes.formatoDecimal(monTototal,2),"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(nomRamo,"formas",ComunesPDF.CENTER,1,2);
					
					pdfDoc.setCell(nomSubramo,"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(tipoFianza,"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(fecAutoriza,"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(fecVigencia,"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(nomFiado,"formas",ComunesPDF.CENTER,1,2);
					
					
					pdfDoc.setCell(nomDest,"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(nomBenef,"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(nomEstatus,"formas",ComunesPDF.CENTER,1,2);
					pdfDoc.setCell(observaciones,"formas",ComunesPDF.CENTER,1,2);
				}
				pdfDoc.addTable();
				pdfDoc.endDocument();

			} catch(Throwable e) {
				throw new AppException("Error al generar el archivo", e);
			} finally {
				try {
					rs.close();
				} catch(Exception e) {}
			}
		}

		return nombreArchivo;
	}
	
	/**
	 * En este m�todo se debe realizar la implementaci�n de la generaci�n de archivo
	 * con base en el objeto Registros que recibe como par�metro.
	 * @param request HttpRequest empleado principalmente para obtener el objeto session
	 * @param reg Objeto Registros obtenido al ejecutar la consulta especificada en getDocumentSummaryQueryForIds()
	 * @param path Ruta donde se generar� el archivo
	 * @param tipo Cadena que identifica el tipo o variante de archivo que va generar.
	 * @return Cadena con la ruta del archivo generado
	 */
	public String crearPageCustomFile(HttpServletRequest request, netropology.utilerias.Registros reg, String path, String tipo) {
		return "";
	}

	
	public List getConditions() {
		return conditions;
	}


	public void setIcEpo(String icEpo) {
		this.icEpo = icEpo;
	}


	public String getIcEpo() {
		return icEpo;
	}


	public void setIcAfianzadora(String icAfianzadora) {
		this.icAfianzadora = icAfianzadora;
	}


	public String getIcAfianzadora() {
		return icAfianzadora;
	}


	public void setIcFiado(String icFiado) {
		this.icFiado = icFiado;
	}


	public String getIcFiado() {
		return icFiado;
	}


	public void setIcEstatus(String icEstatus) {
		this.icEstatus = icEstatus;
	}


	public String getIcEstatus() {
		return icEstatus;
	}


	public void setNumFianza(String numFianza) {
		this.numFianza = numFianza;
	}


	public String getNumFianza() {
		return numFianza;
	}


	public void setNumContrato(String numContrato) {
		this.numContrato = numContrato;
	}


	public String getNumContrato() {
		return numContrato;
	}


	public void setDfSolicIni(String dfSolicIni) {
		this.dfSolicIni = dfSolicIni;
	}


	public String getDfSolicIni() {
		return dfSolicIni;
	}


	public void setDfSolicFin(String dfSolicFin) {
		this.dfSolicFin = dfSolicFin;
	}


	public String getDfSolicFin() {
		return dfSolicFin;
	}


	public void setDfVencIni(String dfVencIni) {
		this.dfVencIni = dfVencIni;
	}


	public String getDfVencIni() {
		return dfVencIni;
	}


	public void setDfVencFin(String dfVencFin) {
		this.dfVencFin = dfVencFin;
	}


	public String getDfVencFin() {
		return dfVencFin;
	}


	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}


	public String getUsuario() {
		return usuario;
	}


	public void setAutorizador(String autorizador) {
		this.autorizador = autorizador;
	}


	public String getAutorizador() {
		return autorizador;
	}


	public void setDfAutorizaIni(String dfAutorizaIni) {
		this.dfAutorizaIni = dfAutorizaIni;
	}


	public String getDfAutorizaIni() {
		return dfAutorizaIni;
	}


	public void setDfAutorizaFin(String dfAutorizaFin) {
		this.dfAutorizaFin = dfAutorizaFin;
	}


	public String getDfAutorizaFin() {
		return dfAutorizaFin;
	}


	public void setDfRechazaIni(String dfRechazaIni) {
		this.dfRechazaIni = dfRechazaIni;
	}


	public String getDfRechazaIni() {
		return dfRechazaIni;
	}


	public void setDfRechazaFin(String dfRechazaFin) {
		this.dfRechazaFin = dfRechazaFin;
	}


	public String getDfRechazaFin() {
		return dfRechazaFin;
	}


	public void setRamo(String ramo) {
		this.ramo = ramo;
	}


	public String getRamo() {
		return ramo;
	}


	public void setSubramo(String subramo) {
		this.subramo = subramo;
	}


	public String getSubramo() {
		return subramo;
	}


	public void setEstrato(String estrato) {
		this.estrato = estrato;
	}


	public String getEstrato() {
		return estrato;
	}


	public void setPerfil(String perfil) {
		this.perfil = perfil;
	}


	public String getPerfil() {
		return perfil;
	}


	

}