package com.netro.web.seguridad;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.Comunes;


public class SeguridadWeb  {
//Variable para enviar mensajes al log.
//	private static final Log log = ServiceLocator.getInstance().getLog(SeguridadWeb.class);

	public SeguridadWeb() {
	}
	
	/**
	 * Metodo para obtener el numero de serie del certificado que seleccion� el
	 * usuario en el momento en el quie le fue requerido por el servidor.
	 * El certificado est� disponible s�lo bajo HTTPS.
	 * @throws netropology.utilerias.AppException Si no hay certificado para
	 * 		el usuario.
	 * @return Cadena con el numero de serial del certificado del cliente
	 * @param request Objeto request de donde se obtiene el 
	 * 		certificado del cliente
	 */
/*	public static String getSerialCertificado(HttpServletRequest request) 
			throws AppException{
		log.info("getSerialCertificado(E)");
		String serial = "";
		try {
			java.security.cert.X509Certificate certs[] = 
					(java.security.cert.X509Certificate [])request.getAttribute("javax.servlet.request.X509Certificate");
			//Si no hay Certificados lanza una excepci�n
			if (certs == null || certs.length == 0) {
				throw new AppException("No se encontr� ningun certificado. " +
						" Asegurese que est� empleando HTTPS y que seleccion� un certificado v�lido");
			}
			for(int i = 0; i<certs.length; i++) {
				byte x [] = certs[i].getSerialNumber().toByteArray();
				serial = new String(x);
			}
			return serial;
		} catch(AppException e) {
			throw e;
		} catch(Exception e) {
			throw new AppException(
					"Error Inesperado al obtener el serial del certificado del cliente.", e);
		} finally {
			log.info("getSerialCertificado(S)");
		}
	}
*/
	/**
	 * Metodo para determinar si la ip del usuario es valida para accesar una
	 * pantalla.
	 * La validacion de Ip se realiza contra la expresion regular definida en 
	 * comcat_param_gral.cg_regexp_ips
	 * @param request Objeto request de donde se obtiene el 
	 * 		certificado del cliente
	 * @return true si tiene acceso o false de lo contrario
	 */
	public static boolean esIPValida(HttpServletRequest request) {
//			throws AppException{
		//log.info("getSerialCertificado(E)");
		AccesoDB con = new AccesoDB();
		boolean valida = false;
		try {
			con.conexionDB();
			String strSQL = 
					" SELECT cg_regexp_ips " +
					" FROM com_param_gral " +
					" WHERE ic_param_gral = ? ";
			PreparedStatement ps = con.queryPrecompilado(strSQL);
			ps.setInt(1,1);
			ResultSet rs = ps.executeQuery();
			String regExpIP = null;
			if (rs.next()) {
				regExpIP = rs.getString("cg_regexp_ips");
			} else {
				System.out.println("ERROR. NO esta parametrizado com_param_gral.cg_regexp_ips");
			}
			rs.close();
			ps.close();
			
			if (regExpIP != null && !regExpIP.equals("")) { //Valida
				
				String ipRemota = request.getRemoteAddr();
				
				List lRegexps = Comunes.explode(",", regExpIP);
				Pattern p = null;
				Matcher m = null;
				for (int i =0; i < lRegexps.size(); i++ ) {
					String expReg = ((String) lRegexps.get(i)).trim();
					
					p = Pattern.compile(expReg);
					m = p.matcher(ipRemota);
					if (m.matches()) {
						valida = true;
						break;
					}
				}
			}
		} catch(Exception e) {
			//throw new Exception(
			//		"Error Inesperado al obtener el serial del certificado del cliente.", e);
		} finally {
//			log.info("getSerialCertificado(S)");
			if (con.hayConexionAbierta()) {
				con.cierraConexionDB();
			}
		}
		return valida;
	}

}