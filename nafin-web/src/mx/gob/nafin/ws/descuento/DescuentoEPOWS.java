package mx.gob.nafin.ws.descuento;

import com.nafin.ws.CargaDocDistribuidoresWSInfo;
import com.nafin.ws.CargaDocumentoWSInfo;

import com.netro.afiliacion.Afiliacion;
import com.netro.descuento.CargaDocumento;
import com.netro.distribuidores.CargaDocDist;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import netropology.utilerias.ServiceLocator;


/**
 * En esta clase se especifican los servicios que brindara el Web Service de Descuento para EPOs.
 * @author Gilberto Aparicio
 */
@SOAPBinding(style = SOAPBinding.Style.RPC)
@WebService(name = "DescuentoEPOWS", serviceName = "DescuentoEPOWS", portName = "DescuentoEPOWS")
public class DescuentoEPOWS {
	public DescuentoEPOWS() {
		super();
	}

	/**
	 * Obtiene los avisos de notificacion
	 * @param claveEPO Clave de la epo
	 * @param usuario Usuario
	 * @param password Contrase�a del usuario
	 * @param tipoAviso Tipo de aviso de notificaci�n
	 *       "1".- Formato simple.
	 *       "2".- Formato de exportaci�n de interfases.
	 * @param neIF Numero de Nafin Electronico del IF
	 * @param fechaNotificacionIni Fecha de Notificacion Inicial dd/mm/aaaa
	 * @param fechaNotificacionFin Fecha de Notificacion Final dd/mm/aaaa
	 * @param fechaVencimientoIni Fecha de Vencimiento Inicial dd/mm/aaaa
	 * @param fechaVencimientoFin Fecha de Vencimiento Final dd/mm/aaaa
	 * @param acuseNotificacion Acuse de notificaci�n
	 *
	 * @return Cadena con la informaci�n separadas por comas o cadena vacias
	 *  si no hay informaci�n que concuerde con los par�metros epecificados
	 *  o "ERROR" cuando los par�metros no son validos u ocurrio un error.
	 *
	 */
	@WebMethod
	public String getAvisosNotificacionWS(@WebParam(name = "claveEPO") String claveEPO,
													  @WebParam(name = "usuario") String usuario, @WebParam(name = "password") String password,
													  @WebParam(name = "tipoAviso") String tipoAviso, @WebParam(name = "neIF") String neIF,
													  @WebParam(name = "fechaNotificacionIni") String fechaNotificacionIni,
													  @WebParam(name = "fechaNotificacionFin") String fechaNotificacionFin,
													  @WebParam(name = "fechaVencimientoIni") String fechaVencimientoIni, @WebParam(name = "fechaVencimientoFin") String fechaVencimientoFin,
													  @WebParam(name = "acuseNotificacion") String acuseNotificacion) {
	
		String notificaciones = "";
		try {
			CargaDocumento bean = ServiceLocator.getInstance().lookup("CargaDocumentoEJB", CargaDocumento.class);
			notificaciones =  bean.getAvisosNotificacionWS(
					claveEPO, usuario, password,
					tipoAviso, neIF,
					fechaNotificacionIni, fechaNotificacionFin,
					fechaVencimientoIni, fechaVencimientoFin,
					acuseNotificacion);
		} catch (Throwable e) {
			e.printStackTrace();
			notificaciones = "ERROR. Al invocar el metodo";
		} finally {
		}
		return notificaciones;
	}
	
	
	/**
	 *
	 * @param claveEPO Clave de la EPO que envia los documentos
	 * @param usuario Clave del usuario en N@E
	 * @param password Password del usuario en N@E
	 * @param cadenaArchivo Cadena con el contenido del archivo
	 * @param pkcs7 Cadena con el contenido del documento firmado
	 * @param String serial Serial del certificado.
	 *
	 * @return CargaDocumentoWSInfo que contiene informaci�n acerca de la
	 *  		ejecuci�n del proceso
	 */
	@WebMethod
	public CargaDocumentoWSInfo procesarDocumentosWS(@WebParam(name = "claveEPO") String claveEPO,
																	 @WebParam(name = "usuario") String usuario, @WebParam(name = "password") String password,
																	 @WebParam(name = "cadenaArchivo") String cadenaArchivo, @WebParam(name = "pkcs7") String pkcs7,
																	 @WebParam(name = "serial") String serial) {
		CargaDocumentoWSInfo info = null;
		try {
			CargaDocumento bean = ServiceLocator.getInstance().lookup("CargaDocumentoEJB", CargaDocumento.class);
			info =  bean.procesarDocumentosWS(
					claveEPO, usuario, password,
					cadenaArchivo, pkcs7, serial);

		} catch (Throwable e) {
			info = new CargaDocumentoWSInfo();
			info.setCodigoEjecucion(new Integer(1));	//1 Fallo la ejecuci�n del proceso
			info.setEstatusCarga(new Integer(3));	//3 Fallo la ejecuci�n del proceso
			info.setResumenEjecucion("Fallo la ejecucion del proceso: " + 
					e.getMessage());
		} finally {
		}
		return info;
	}
	
	/**
	 * Obtiene un listado de los documentos sin operar
	 * @param claveEPO Clave de la epo
	 * @param usuario Usuario
	 * @param password Contrase�a del usuario
	 *
	 * @return Cadena con la informaci�n separadas por comas
	 *  o "ERROR" cuando los par�metros no son validos u ocurrio un error.
	 *
	 */
	@WebMethod
	public String getDocumentosSinOperarWS(@WebParam(name = "claveEPO") String claveEPO,
														@WebParam(name = "usuario") String usuario,
														@WebParam(name = "password") String password) {
		
		String cadena = "";
		try {
			CargaDocumento bean = ServiceLocator.getInstance().lookup("CargaDocumentoEJB", CargaDocumento.class);
			cadena =  bean.getDocumentosSinOperarWS(
					claveEPO, usuario, password);
		} catch (Throwable e) {
			e.printStackTrace();
			cadena = "ERROR. Al invocar el metodo";
		} finally {
		}
		return cadena;

	}
	
	/**
	 * Obtiene los documentos que fueron dados de baja en la fecha especificada. Si esta no es especificada
	 * obtiene los de d�a actual.
	 * @param claveEPO Clave de la EPO
	 * @param usuario Login del usuario
	 * @param password Password del usuario
	 * @param fechaInicio Fecha inicial del rango
	 * @param fechaFin Fecha final del rango
	 * @return Cadena con la informaci�n separadas por comas o cadena vacias
	 *  si no hay informaci�n que concuerde con los par�metros epecificados
	 *  o "ERROR" cuando los par�metros no son v�lidos u ocurrio un error.
	 */
	@WebMethod
	public String getDocumentosBajaWS(@WebParam(name = "claveEPO") String claveEPO, @WebParam(name = "usuario") String usuario,
												 @WebParam(name = "password") String password, @WebParam(name = "fechaInicio") String fechaInicio,
												 @WebParam(name = "fechaFin") String fechaFin) {
		String documentosBaja = "";
		try {
			CargaDocumento bean = ServiceLocator.getInstance().lookup("CargaDocumentoEJB", CargaDocumento.class);
			documentosBaja =  bean.getDocumentosBajaWS(
					claveEPO, usuario, password,fechaInicio,fechaFin);
		} catch (Throwable e) {
			e.printStackTrace();
			documentosBaja = "ERROR. Al invocar el metodo";
		} finally {
		}

		return documentosBaja;
	}

	/**
	 * Obtiene las pymes afiliadas en N@E con el estatus especificado. Si este
	 * no se especifica de manera predetermianda obtendra unicamente los Habilitados (H)
	 * @param claveEPO Clave de la EPO
	 * @param usuario Login del usuario
	 * @param password Password del usuario
	 * @param estatus Estatus de la Pyme: H Habilitada, R Registrada en Descuento,
	 *  		S Suceptible, N No susceptible
	 * @return Cadena con la informaci�n separadas por comas o cadena vacias
	 *  si no hay informaci�n que concuerde con los par�metros epecificados
	 *  o "ERROR" cuando los par�metros no son v�lidos u ocurrio un error.
	 *
	 */
	@WebMethod
	public String getPymesAfiliadasWS(@WebParam(name = "claveEPO") String claveEPO, @WebParam(name = "usuario") String usuario,
												 @WebParam(name = "password") String password,
												 @WebParam(name = "estatus") String estatus) {
		String pymesAfiliadas = "";
		try {
			Afiliacion bean = ServiceLocator.getInstance().lookup("AfiliacionEJB", Afiliacion.class);
			pymesAfiliadas =  bean.getPymesAfiliadasWS(claveEPO, usuario, password, estatus);
		} catch (Throwable e) {
			e.printStackTrace();
			pymesAfiliadas = "ERROR. Al invocar el metodo";
		} finally {
		}
		return pymesAfiliadas;
	}


	/**
	 *
	 * @param claveEPO Clave de la EPO que envia los documentos
	 * @param usuario Clave del usuario en N@E
	 * @param password Password del usuario en N@E
	 * @param cadenaArchivo Cadena con el contenido del archivo
	 * @param pkcs7 Cadena con el contenido del documento firmado
	 * @param String serial Serial del certificado.
	 *
	 * @return CargaDocDistribuidoresWSInfo que contiene informaci�n acerca de la
	 *  		ejecuci�n del proceso
	 */
	@WebMethod
	public CargaDocDistribuidoresWSInfo procesarDocumentosDisWS(@WebParam(name = "claveEPO") String claveEPO,
																					@WebParam(name = "usuario") String usuario, @WebParam(name = "password") String password,
																					@WebParam(name = "cadenaArchivo") String cadenaArchivo, @WebParam(name = "pkcs7") String pkcs7,
																					@WebParam(name = "serial") String serial) {
		CargaDocDistribuidoresWSInfo info = null;
		try {
			CargaDocDist bean = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);
			info =  bean.procesarDocDistribuidoresWS(
					claveEPO, usuario, password,
					cadenaArchivo, pkcs7, serial);

		} catch (Throwable e) {
			info = new CargaDocDistribuidoresWSInfo();
			info.setCodigoEjecucion(new Integer(1));	//1 Fallo la ejecuci�n del proceso
			info.setEstatusCarga(new Integer(3));	//3 Fallo la ejecuci�n del proceso
			info.setResumenEjecucion("Fallo la ejecucion del proceso: " + 
					e.getMessage());
		} finally {
		}
		return info;
	}
	
	
	/**
	 * Obtiene los avisos de notificacion de distribuidores
	 * @return Cadena con la informaci�n separadas por comas o cadena vacias
	 *   si no hay informaci�n que concuerde con los par�metros epecificados
	 *   o "ERROR" cuando los par�metros no son validos u ocurrio un error.
	 * @param claveEPO Clave de la epo
	 * @param usuario Usuario
	 * @param password Contrase�a del usuario
	 * @param fechaOperacionIni Fecha de Vencimiento Inicial dd/mm/aaaa
	 * @param fehcaOperacionFin Fecha de Vencimiento Final dd/mm/aaaa
	 */
	@WebMethod
	public String getAvisosNotificacionDisWS(@WebParam(name = "claveEPO") String claveEPO,
														  @WebParam(name = "usuario") String usuario, @WebParam(name = "password") String password,
														  @WebParam(name = "fechaOperacionIni") String fechaOperacionIni,
														  @WebParam(name = "fehcaOperacionFin") String fehcaOperacionFin) {
		
		String notificaciones = "";
		try {
			CargaDocDist bean = ServiceLocator.getInstance().lookup("CargaDocDistEJB", CargaDocDist.class);
			notificaciones =  bean.getAvisosNotificacionDisWS(claveEPO, usuario, password, fechaOperacionIni, fehcaOperacionFin);
			System.out.println("notificaciones = "+notificaciones);
			
		} catch (Throwable e) {
			notificaciones = "ERROR. Al invocar el metodo";
		} finally {
		}
		return notificaciones;

	}
	
	/**
	 * Obtiene los documentos Dispersados registrados con estatus Conciliado
	 * @return Cadena con la informaci�n separadas por comas o cadena vacias
	 *   si no hay informaci�n que concuerde con los par�metros epecificados
	 *   o "ERROR" cuando los par�metros no son validos u ocurrio un error.
	 * @param claveEPO Clave de la epo
	 * @param usuario Usuario
	 * @param password Contrase�a del usuario
	 * @param fechaInicio Fecha de Inicio dd/mm/aaaa
	 * @param fehcaFinal Fecha Final dd/mm/aaaa  <-- se deja el nombre por motivos de compatibilidad. corregir cuando sea posible
	 */
	@WebMethod
	public String getDispersiones(@WebParam(name = "usuario") String usuario, @WebParam(name = "password") String password,
											@WebParam(name = "claveEpo") String claveEpo, @WebParam(name = "fechaInicio") String fechaInicio,
											@WebParam(name = "fehcaFinal") String fechaFinal) {
		
		String docDispersados = "";
		try {
			CargaDocumento bean = ServiceLocator.getInstance().lookup("CargaDocumentoEJB", CargaDocumento.class);
			System.out.println("Home obtenido");
			docDispersados =  bean.getDispersiones(usuario, password, claveEpo, fechaInicio, fechaFinal);
			System.out.println("Documentos Dispersados = "+docDispersados);
			
		} catch (Throwable e) {
			docDispersados = "ERROR. Al invocar el metodo getDispersiones()";
		} finally {
		}
		return docDispersados;

	}
}
