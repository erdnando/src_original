package mx.gob.nafin.ws.descuento;

import com.nafin.descuento.ws.DocumentoIFWS;
import com.nafin.descuento.ws.ProcesoIFWSInfo;

import com.netro.descuento.AutorizacionDescuento;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import netropology.utilerias.ServiceLocator;

@SOAPBinding(style = SOAPBinding.Style.RPC)
@WebService(name = "DescuentoIFWS", serviceName = "DescuentoIFWS", portName = "DescuentoIFWS")
public class DescuentoIFWS {
	/**
	 * Obtiene la cadena a firmar por parte del IF, usando la autorizacion via
	 * WebService.
	 * La cadena est� confirmado unicamente por los atributos que tengan valor.
	 * @param documentos Arreglo de documentos que ser�n utilizados en el proceso
	 *  de cambio de estatus v�a WebService.
	 * @return Cadena a firmar
	 */
	@WebMethod
	public String getCadenaFirmarWS(@WebParam(name = "documentos") DocumentoIFWS[] documentos) {
		
		try {
			AutorizacionDescuento bean = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB", AutorizacionDescuento.class);
			return bean.getCadenaFirmarWS(documentos);
		} catch(Throwable t) {
			t.printStackTrace();
			return "ERROR";
		}
	}


	/**
	 * Realiza el cambio de estatus de los documentos seleccionados Pyme,
	 * segun corresponda.
	 * @param claveUsuario Clave del usuario
	 * @param password Contrase�a del usuario
	 * @param claveIF Clave del Intermediario (ic_if)
	 * @param documentos Arreglo de DocumentoIFWS que contiene la informaci�n
	 *       de los documentos a procesar unicamente.
	 * @param pkcs7 Firma Digital de la cadena de documentos a procesar
	 *       en formato PKCS7
	 * @param serial Numero de Serie del Certificado
	 * @return Objeto ProcesoIFWSInfo que contiene la informaci�n de la ejecucion
	 *       del proceso mas los objetos DocumentoIFWS con los datos de los
	 *     documentos que tuvieron error (en caso de que exista error)
	 */
	@WebMethod
	public ProcesoIFWSInfo confirmacionDoctosIFWS (@WebParam(name = "usuario") String usuario,
																 @WebParam(name = "password") String password, @WebParam(name = "claveIF") String claveIF,
																 @WebParam(name = "documentos") DocumentoIFWS[] documentos, @WebParam(name = "pkcs7") String pkcs7,
																 @WebParam(name = "serial") String serial) {
		ProcesoIFWSInfo resultadoProceso = null;
		try {
			AutorizacionDescuento bean = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB", AutorizacionDescuento.class);
			resultadoProceso = bean.confirmacionDoctosIFWS (usuario, 
					password,   claveIF, 
					documentos, pkcs7, serial);
			return resultadoProceso;
		} catch(Throwable t) {
			t.printStackTrace();
			if (resultadoProceso == null) {
				resultadoProceso =  new ProcesoIFWSInfo();
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
				resultadoProceso.setResumenEjecucion("ERROR INESPERADO|Error en Webservice. " + t.getMessage());
			} else if (resultadoProceso.getCodigoEjecucion() == null) {
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
			}
			return resultadoProceso;
		}
	}


	/**
	 * Obtiene Los documentos Seleccionados Pyme, para su operaci�n de
	 * Descuento.
	 * @param claveUsuario Clave del usuario
	 * @param password Contrase�a del usuario
	 * @param claveIF Clave del Intermediario (ic_if)
	 * @return Objeto ProcesoIFWSInfo que contiene la informaci�n de la ejecucion
	 *       del proceso mas los objetos DocumentoIFWS con los datos de los
	 *     documentos seleccionados
	 */
	@WebMethod
	public ProcesoIFWSInfo getDoctosSelecPymeWS(@WebParam(name = "claveUsuario") String claveUsuario,
															  @WebParam(name = "password") String password,
															  @WebParam(name = "claveIF") String claveIF) {
		ProcesoIFWSInfo resultadoProceso = null;
		try {
			AutorizacionDescuento bean = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB", AutorizacionDescuento.class);
			resultadoProceso = bean.getDoctosSelecPymeWS(claveUsuario, 
					password, claveIF);
			return resultadoProceso;
		} catch(Throwable t) {
			t.printStackTrace();
			if (resultadoProceso == null) {
				resultadoProceso =  new ProcesoIFWSInfo();
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
				resultadoProceso.setResumenEjecucion("ERROR INESPERADO|Error en Webservice. " + t.getMessage());
			} else if (resultadoProceso.getCodigoEjecucion() == null) {
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
			}
			return resultadoProceso;
		}
	}

	/**
	 * Permitir� obtener el aviso de notificaci�n de los documentos autorizados por el intermediario
	 * @param claveUsuario Clave del usuario
	 * @param password Contrase�a del usuario
	 * @param claveIF Clave del IF
	 * @param fecNotificacionIni Fecha de notificacion inicial
	 * @param fecNotificacionFin Fecha de notificacion final
	 * @param fecVencIni Fecha de vencimiento inicial
	 * @param fecVencFin Fecha de vencimiento final
	 * @param claveEpo Clave de la Epo
	 * @param acuseIf Acuse del IF
	 * @return  ProcesoIFWSInfo que contiene la informaci�n de la ejecucion
	 *  del proceso mas los objetos DocumentoIFWS con los datos de los
	 *  documentos
	 */
	@WebMethod
	public ProcesoIFWSInfo getAvisosNotificacionWS(@WebParam(name = "claveUsuario") String claveUsuario,
																  @WebParam(name = "password") String password, @WebParam(name = "claveIF") String claveIF,
																  @WebParam(name = "fecNotificacionIni") String fecNotificacionIni,
																  @WebParam(name = "fecNotificacionFin") String fecNotificacionFin, @WebParam(name = "fecVencIni") String fecVencIni,
																  @WebParam(name = "fecVencFin") String fecVencFin, @WebParam(name = "claveEpo") String claveEpo,
																  @WebParam(name = "acuseIf") String acuseIf) {
		ProcesoIFWSInfo resultadoProceso = null;
		try {
			AutorizacionDescuento bean = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB", AutorizacionDescuento.class);
			resultadoProceso = bean.getAvisosNotificacionWS(claveUsuario, 
					password,   claveIF, 
					fecNotificacionIni, fecNotificacionFin, 
					fecVencIni, fecVencFin,
					claveEpo, acuseIf);
			return resultadoProceso;
		} catch(Throwable t) {
			t.printStackTrace();
			if (resultadoProceso == null) {
				resultadoProceso =  new ProcesoIFWSInfo();
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
				resultadoProceso.setResumenEjecucion("ERROR INESPERADO|Error en Webservice. " + t.getMessage());
			} else if (resultadoProceso.getCodigoEjecucion() == null) {
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
			}
			return resultadoProceso;
		}
	}

}
