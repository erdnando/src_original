package mx.gob.nafin.ws.descuento;

import com.nafin.descuento.ws.CuentasPymeIFWS;
import com.nafin.descuento.ws.DocumentoIFWS;
import com.nafin.descuento.ws.DocumentoIFWSV2;
import com.nafin.descuento.ws.ProcesoIFWSInfoV2;

import com.netro.descuento.AutorizacionDescuento;
import com.netro.distribuidores.AutorizacionSolic;
import com.netro.distribuidores.ws.DocumentoDistIFWS;
import com.netro.distribuidores.ws.ProcesoIFDistWSInfo;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import netropology.utilerias.ServiceLocator;

@SOAPBinding(style = SOAPBinding.Style.RPC)
@WebService(serviceName = "DescuentoIFWSV2", portName = "DescuentoIFWSV2")
public class DescuentoIFWSV2 {

	/**
	 * Obtiene la cadena a firmar por parte del IF, usando la autorizacion via
	 * WebService.
	 * La cadena est� confirmado unicamente por los atributos que tengan valor.
	 * @param documentos Arreglo de documentos que ser�n utilizados en el proceso
	 *  de cambio de estatus v�a WebService.
	 * @return Cadena a firmar
	 */
	@WebMethod
	public String getCadenaFirmarWS(@WebParam(name = "documentos") DocumentoIFWSV2[] documentos) {
		
		try {
			DocumentoIFWS[] arrDocumentos=null;
			 if(documentos!=null){
				 arrDocumentos=new DocumentoIFWS[documentos.length];
				 for(int i=0;i<documentos.length;i++){
					arrDocumentos[i]=(DocumentoIFWS)documentos[i];
				 }
			 }
			AutorizacionDescuento bean = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB", AutorizacionDescuento.class);
			return bean.getCadenaFirmarWS(arrDocumentos);
		} catch(Throwable t) {
			t.printStackTrace();
			return "ERROR";
		}
	}


	/**
	 * Realiza el cambio de estatus de los documentos seleccionados Pyme,
	 * segun corresponda.
	 * @param claveUsuario Clave del usuario
	 * @param password Contrase�a del usuario
	 * @param claveIF Clave del Intermediario (ic_if)
	 * @param documentos Arreglo de DocumentoIFWS que contiene la informaci�n
	 *       de los documentos a procesar unicamente.
	 * @param pkcs7 Firma Digital de la cadena de documentos a procesar
	 *       en formato PKCS7
	 * @param serial Numero de Serie del Certificado
	 * @return Objeto ProcesoIFWSInfo que contiene la informaci�n de la ejecucion
	 *       del proceso mas los objetos DocumentoIFWS con los datos de los
	 *     documentos que tuvieron error (en caso de que exista error)
	 */
	@WebMethod
	public ProcesoIFWSInfoV2 confirmacionDoctosIFWSV2 (@WebParam(name = "usuario") String usuario,
																	  @WebParam(name = "password") String password, @WebParam(name = "claveIF") String claveIF,
																	  @WebParam(name = "documentos") DocumentoIFWSV2[] documentos, @WebParam(name = "pkcs7") String pkcs7,
																	  @WebParam(name = "serial") String serial,
																	  @WebParam(name = "detalle_doctos_error") String detalle_doctos_error) {
		ProcesoIFWSInfoV2 resultadoProceso = null;
		try {
			AutorizacionDescuento bean = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB", AutorizacionDescuento.class);
			resultadoProceso = bean.confirmacionDoctosIFWSV2 (usuario, 
					password,   claveIF, 
					documentos, pkcs7, serial,detalle_doctos_error);
			return resultadoProceso;
		} catch(Throwable t) {
			t.printStackTrace();
			if (resultadoProceso == null) {
				resultadoProceso =  new ProcesoIFWSInfoV2();
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
				resultadoProceso.setResumenEjecucion("ERROR INESPERADO|Error en Webservice. " + t.getMessage());
			} else if (resultadoProceso.getCodigoEjecucion() == null) {
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
			}
			return resultadoProceso;
		}
	}


	/**
	 * Obtiene Los documentos Seleccionados Pyme, para su operaci�n de
	 * Descuento.
	 * @param claveUsuario Clave del usuario
	 * @param password Contrase�a del usuario
	 * @param claveIF Clave del Intermediario (ic_if)
	 * @return Objeto ProcesoIFWSInfo que contiene la informaci�n de la ejecucion
	 *       del proceso mas los objetos DocumentoIFWS con los datos de los
	 *     documentos seleccionados
	 */
	@WebMethod
	public ProcesoIFWSInfoV2 getDoctosSelecPymeWSV2(@WebParam(name = "claveUsuario") String claveUsuario,
																	@WebParam(name = "password") String password,
																	@WebParam(name = "claveIF") String claveIF) {
		ProcesoIFWSInfoV2 resultadoProceso = null;
		try {
			AutorizacionDescuento bean = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB", AutorizacionDescuento.class);
			resultadoProceso = bean.getDoctosSelecPymeWSV2(claveUsuario, 
					password, claveIF);
			return resultadoProceso;
		} catch(Throwable t) {
			t.printStackTrace();
			if (resultadoProceso == null) {
				resultadoProceso =  new ProcesoIFWSInfoV2();
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
				resultadoProceso.setResumenEjecucion("ERROR INESPERADO|Error en Webservice. " + t.getMessage());
			} else if (resultadoProceso.getCodigoEjecucion() == null) {
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
			}
			return resultadoProceso;
		}
	}

	/**
	 * Obtiene Las Cuentas Seleccionados Pyme, para su operaci�n de Autorizacion
	 *
	 * El m�todo est� pensado para ser invocado via Web Service DescuentoIFWS.
	 *
	 * @param claveUsuario Clave del usuario
	 * @param password Contrase�a del usuario
	 * @param claveIF Clave del Intermediario (ic_if)
	 * @return Objeto ProcesoIFWSInfo que contiene la informaci�n de la ejecucion
	 *       del proceso mas los objetos CuentasPymeIFWS con los datos de los
	 *     documentos seleccionados
	 */
	@WebMethod
	public ProcesoIFWSInfoV2 getCtasBancPymePend(@WebParam(name = "claveUsuario") String claveUsuario,
																@WebParam(name = "password") String password, @WebParam(name = "claveIF") String claveIF,
																@WebParam(name = "fechaParamInic") String fechaParamInic, @WebParam(name = "fechaParamFin") String fechaParamFin,
																@WebParam(name = "claveEpo") String claveEpo) {
		ProcesoIFWSInfoV2 resultadoProceso = null;
		try {
			AutorizacionDescuento bean = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB", AutorizacionDescuento.class);
			resultadoProceso = bean.getCtasBancPymePend(claveUsuario,password,claveIF,fechaParamInic,fechaParamFin,claveEpo);
			return resultadoProceso;
		} catch(Throwable t) {
			t.printStackTrace();
			if (resultadoProceso == null) {
				resultadoProceso =  new ProcesoIFWSInfoV2();
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
				resultadoProceso.setResumenEjecucion("ERROR INESPERADO|Error en Webservice. " + t.getMessage());
			} else if (resultadoProceso.getCodigoEjecucion() == null) {
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
			}
			return resultadoProceso;
		}
	}
	
	
	/**
	 * Obtiene Las Cuentas Seleccionados Pyme autorizadas
	 *
	 * El m�todo est� pensado para ser invocado via Web Service DescuentoIFWS.
	 *
	 * @param claveUsuario Clave del usuario
	 * @param password Contrase�a del usuario
	 * @param claveIF Clave del Intermediario (ic_if)
	 * @return Objeto ProcesoIFWSInfo que contiene la informaci�n de la ejecucion
	 *       del proceso mas los CuentasPymeIFWS con los datos de los
	 *     documentos seleccionados
	 */
	@WebMethod

	public ProcesoIFWSInfoV2 getCtasBancPymeAutorizadas(@WebParam(name = "claveUsuario") String claveUsuario,
																		 @WebParam(name = "password") String password, @WebParam(name = "claveIF") String claveIF,
																		 @WebParam(name = "fechaParamInic") String fechaParamInic, @WebParam(name = "fechaParamFin") String fechaParamFin,
																		 @WebParam(name = "claveEpo") String claveEpo) {
		ProcesoIFWSInfoV2 resultadoProceso = null;
		try {
			AutorizacionDescuento bean = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB", AutorizacionDescuento.class);
			resultadoProceso = bean.getCtasBancPymeAutorizadas(claveUsuario,password,claveIF,fechaParamInic,fechaParamFin,claveEpo);
			return resultadoProceso;
		} catch(Throwable t) {
			t.printStackTrace();
			if (resultadoProceso == null) {
				resultadoProceso =  new ProcesoIFWSInfoV2();
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
				resultadoProceso.setResumenEjecucion("ERROR INESPERADO|Error en Webservice. " + t.getMessage());
			} else if (resultadoProceso.getCodigoEjecucion() == null) {
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
			}
			return resultadoProceso;
		}
	}
	
	
		/**
	 * Realiza el cambio de estatus de las Cuentas seleccionadas Pyme,
	 * segun corresponda.
	 * @param claveUsuario Clave del usuario
	 * @param password Contrase�a del usuario
	 * @param claveIF Clave del Intermediario (ic_if)
	 * @param Cuentas Arreglo de CuentasPymeIFWS que contiene la informaci�n
	 *       de las Cuentas a procesar unicamente.
	 * @return Objeto ProcesoIFWSInfo que contiene la informaci�n de la ejecucion
	 *       del proceso mas los objetos CuentasPymeIFWS con los datos de los
	 *     documentos que tuvieron error (en caso de que exista error)
	 */
	@WebMethod
	public ProcesoIFWSInfoV2 confirmacionCuentasIFWS (@WebParam(name = "usuario") String usuario,
																	 @WebParam(name = "password") String password, @WebParam(name = "claveIF") String claveIF,
																	 @WebParam(name = "cuentas") CuentasPymeIFWS[] cuentas) {
		ProcesoIFWSInfoV2 resultadoProceso = null;
		try {
			AutorizacionDescuento bean = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB", AutorizacionDescuento.class);
			resultadoProceso = bean.confirmacionCuentasIFWS (usuario, 
					password,   claveIF, 
					cuentas);
			return resultadoProceso;
		} catch(Throwable t) {
			t.printStackTrace();
			if (resultadoProceso == null) {
				resultadoProceso =  new ProcesoIFWSInfoV2();
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
				resultadoProceso.setResumenEjecucion("ERROR INESPERADO|Error en Webservice. " + t.getMessage());
			} else if (resultadoProceso.getCodigoEjecucion() == null) {
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
			}
			return resultadoProceso;
		}
	}
		/**
	 * Permitir� obtener el aviso de notificaci�n de los documentos autorizados por el intermediario
	 * @param claveUsuario Clave del usuario
	 * @param password Contrase�a del usuario
	 * @param claveIF Clave del IF
	 * @param fecNotificacionIni Fecha de notificacion inicial
	 * @param fecNotificacionFin Fecha de notificacion final
	 * @param fecVencIni Fecha de vencimiento inicial
	 * @param fecVencFin Fecha de vencimiento final
	 * @param claveEpo Clave de la Epo
	 * @param acuseIf Acuse del IF
	 * @return  ProcesoIFWSInfo que contiene la informaci�n de la ejecucion
	 *  del proceso mas los objetos DocumentoIFWS con los datos de los
	 *  documentos
	 */
	@WebMethod
	public ProcesoIFWSInfoV2 getAvisosNotificacionWSV2(@WebParam(name = "claveUsuario") String claveUsuario,
																		@WebParam(name = "password") String password, @WebParam(name = "claveIF") String claveIF,
																		@WebParam(name = "fecNotificacionIni") String fecNotificacionIni,
																		@WebParam(name = "fecNotificacionFin") String fecNotificacionFin, @WebParam(name = "fecVencIni") String fecVencIni,
																		@WebParam(name = "fecVencFin") String fecVencFin, @WebParam(name = "claveEpo") String claveEpo,
																		@WebParam(name = "acuseIf") String acuseIf) {
		ProcesoIFWSInfoV2 resultadoProceso = null;
		try {
			AutorizacionDescuento bean = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB", AutorizacionDescuento.class);
			resultadoProceso = bean.getAvisosNotificacionWSV2(claveUsuario, 
					password,   claveIF, 
					fecNotificacionIni, fecNotificacionFin, 
					fecVencIni, fecVencFin,
					claveEpo, acuseIf);
			return resultadoProceso;
		} catch(Throwable t) {
			t.printStackTrace();
			if (resultadoProceso == null) {
				resultadoProceso =  new ProcesoIFWSInfoV2();
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
				resultadoProceso.setResumenEjecucion("ERROR INESPERADO|Error en Webservice. " + t.getMessage());
			} else if (resultadoProceso.getCodigoEjecucion() == null) {
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
			}
			return resultadoProceso;
		}
	}
	
	
	/**
	 * Metodo que obtiene los limites por EPO, por cada IF
	 *
	 *
	 */
	@WebMethod
	public ProcesoIFWSInfoV2 getLimitesPorEpo(@WebParam(name = "claveUsuario") String claveUsuario,
															@WebParam(name = "password") String password,
															@WebParam(name = "claveIF") String claveIF) {
		ProcesoIFWSInfoV2 resultadoProceso = null;
		try {
			AutorizacionDescuento bean = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB", AutorizacionDescuento.class);
			resultadoProceso = bean.getLimitesPorEpo(claveUsuario, 
					password,   claveIF);
			return resultadoProceso;
		} catch(Throwable t) {
			t.printStackTrace();
			return resultadoProceso;
		}
	}
	
	/**
	 * Metodo que obtiene los limites por EPO, por cada IF
	 *
	 *
	 */
	@WebMethod
	public ProcesoIFWSInfoV2 getProveedoresCFDI(@WebParam(name = "claveUsuario") String claveUsuario,
															  @WebParam(name = "password") String password, @WebParam(name = "claveIF") String claveIF,
															  @WebParam(name = "fechaOperaIni") String fechaOperaIni,
															  @WebParam(name = "fechaOperaFin") String fechaOperaFin) {
		ProcesoIFWSInfoV2 resultadoProceso = null;
		try {
			AutorizacionDescuento bean = ServiceLocator.getInstance().lookup("AutorizacionDescuentoEJB", AutorizacionDescuento.class);
			resultadoProceso = bean.getProveedoresCFDI(claveUsuario, 
					password,   claveIF,fechaOperaIni,fechaOperaFin);
			return resultadoProceso;
		} catch(Throwable t) {
			t.printStackTrace();
			return resultadoProceso;
		}
	}
	
	
	/**
	 * Obtiene la cadena a firmar por parte del IF, usando la autorizacion via
	 * WebService.
	 * La cadena est� confirmado unicamente por los atributos que tengan valor.
	 * @param documentos Arreglo de documentos que ser�n utilizados en el proceso
	 *  de cambio de estatus v�a WebService.
	 * @return Cadena a firmar
	 */
	@WebMethod
	public String getCadenaFirmarDistWS(@WebParam(name = "documentos") DocumentoDistIFWS[] documentos) {
		
		try {
			DocumentoDistIFWS[] arrDocumentos=null;
			 if(documentos!=null){
				 arrDocumentos=new DocumentoDistIFWS[documentos.length];
				 for(int i=0;i<documentos.length;i++){
					arrDocumentos[i]=(DocumentoDistIFWS)documentos[i];
				 }
			 }
			AutorizacionSolic bean = ServiceLocator.getInstance().lookup("AutorizacionSolicEJB", AutorizacionSolic.class);
			return bean.getCadenaFirmarDistWS(arrDocumentos);
		} catch(Throwable t) {
			t.printStackTrace();
			return "ERROR";
		}
	}
	
	
	/**
	 * Obtiene Los documentos Seleccionados Pyme, para su operaci�n de
	 * Distribuidores.
	 * @param claveUsuario Clave del usuario
	 * @param password Contrase�a del usuario
	 * @param claveIF Clave del Intermediario (ic_if)
	 * @return Objeto ProcesoIFDistWSInfo que contiene la informaci�n de la ejecucion
	 *       del proceso mas los objetos DocumentoDistIFWS con los datos de los
	 *     documentos seleccionados
	 */
	@WebMethod
	public ProcesoIFDistWSInfo getDoctosSelecPymeDisWS(@WebParam(name = "claveUsuario") String claveUsuario,
																		@WebParam(name = "password") String password,
																		@WebParam(name = "claveIF") String claveIF) {
		ProcesoIFDistWSInfo resultadoProceso = null;
		try {
			AutorizacionSolic bean = ServiceLocator.getInstance().lookup("AutorizacionSolicEJB", AutorizacionSolic.class);
			resultadoProceso = bean.getDoctosSelecPymeDisWS(claveUsuario, 
					password, claveIF);
			return resultadoProceso;
		} catch(Throwable t) {
			t.printStackTrace();
			if (resultadoProceso == null) {
				resultadoProceso =  new ProcesoIFDistWSInfo();
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
				resultadoProceso.setResumenEjecucion("ERROR INESPERADO|Error en Webservice. " + t.getMessage());
			} else if (resultadoProceso.getCodigoEjecucion() == null) {
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
			}
			return resultadoProceso;
		}
	}
	
		/**
	 * Permitir� obtener el aviso de notificaci�n de los documentos autorizados por el intermediario
	 * @param claveUsuario Clave del usuario
	 * @param password Contrase�a del usuario
	 * @param claveIF Clave del IF
	 * @param fecNotificacionIni Fecha de notificacion inicial
	 * @param fecNotificacionFin Fecha de notificacion final
	 * @param fecVencIni Fecha de vencimiento inicial
	 * @param fecVencFin Fecha de vencimiento final
	 * @param claveEpo Clave de la Epo
	 * @param acuseIf Acuse del IF
	 * @return  ProcesoIFDistWSInfo que contiene la informaci�n de la ejecucion
	 *  del proceso mas los objetos DocumentoDistIFWS con los datos de los
	 *  documentos
	 */
	@WebMethod
	public ProcesoIFDistWSInfo getAvisosNotificacionDisWS(@WebParam(name = "claveUsuario") String claveUsuario,
																			@WebParam(name = "password") String password, @WebParam(name = "claveEpo") String claveEpo,
																			@WebParam(name = "clavePyme") String clavePyme, @WebParam(name = "claveIF") String claveIF,
																			@WebParam(name = "fecOperacionIni") String fecOperacionIni,
																			@WebParam(name = "fecOperacionFin") String fecOperacionFin) {
		ProcesoIFDistWSInfo resultadoProceso = null;
		try {
			AutorizacionSolic bean = ServiceLocator.getInstance().lookup("AutorizacionSolicEJB", AutorizacionSolic.class);
			resultadoProceso = bean.getAvisosNotificacionDisWS(claveUsuario, password, claveEpo, clavePyme, claveIF, fecOperacionIni, fecOperacionFin);
			return resultadoProceso;
		} catch(Throwable t) {
			t.printStackTrace();
			if (resultadoProceso == null) {
				resultadoProceso =  new ProcesoIFDistWSInfo();
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
				resultadoProceso.setResumenEjecucion("ERROR INESPERADO|Error en Webservice. " + t.getMessage());
			} else if (resultadoProceso.getCodigoEjecucion() == null) {
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
			}
			return resultadoProceso;
		}
	}
	
	/**
	 * Realiza el cambio de estatus de los documentos En proceso de Autorizacion IF a negociable u operada,
	 * segun corresponda.
	 * @param claveUsuario Clave del usuario
	 * @param password Contrase�a del usuario
	 * @param claveIF Clave del Intermediario (ic_if)
	 * @param documentos Arreglo de DocumentoIFDistWS que contiene la informaci�n
	 *       de los documentos a procesar unicamente.
	 * @param pkcs7 Firma Digital de la cadena de documentos a procesar
	 *       en formato PKCS7
	 * @param serial Numero de Serie del Certificado
	 * @return Objeto ProcesoIFWSInfo que contiene la informaci�n de la ejecucion
	 *       del proceso mas los objetos DocumentoIFWS con los datos de los
	 *     documentos que tuvieron error (en caso de que exista error)
	 */
	@WebMethod
	public ProcesoIFDistWSInfo confirmacionDoctosDistIF (@WebParam(name = "usuario") String usuario,
																		 @WebParam(name = "password") String password, @WebParam(name = "claveIF") String claveIF,
																		 @WebParam(name = "documentos") DocumentoDistIFWS[] documentos, @WebParam(name = "pkcs7") String pkcs7,
																		 @WebParam(name = "serial") String serial) {
		ProcesoIFDistWSInfo resultadoProceso = null;
		try {
			AutorizacionSolic bean = ServiceLocator.getInstance().lookup("AutorizacionSolicEJB", AutorizacionSolic.class);
			resultadoProceso = bean.confirmacionDoctosDistIFWS (usuario, 
					password,   claveIF, 
					documentos, pkcs7, serial);
			return resultadoProceso;
		} catch(Throwable t) {
			t.printStackTrace();
			if (resultadoProceso == null) {
				resultadoProceso =  new ProcesoIFDistWSInfo();
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
				resultadoProceso.setResumenEjecucion("ERROR INESPERADO|Error en Webservice. " + t.getMessage());
			} else if (resultadoProceso.getCodigoEjecucion() == null) {
				resultadoProceso.setCodigoEjecucion(new Integer(2)); //2=Error
			}
			return resultadoProceso;
		}
	}

}
