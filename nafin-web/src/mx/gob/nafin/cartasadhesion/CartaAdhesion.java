package mx.gob.nafin.cartasadhesion;

public class CartaAdhesion {
    
    private Integer icCartaAdhesion;
    private Integer icEPO;
    private Integer icIF;
    private String folio;
    private String estatus;
    private String nombreCadena;
    private String nombreIF;
    private String nombreMoneda;
    private String tipoFactoraje;
    private String fechaFirma;
    private Float sobretasa;
    private String numeroFirmantesIF;
    private String numeroFirmantesEPO;
    private String nombreFirmantesIF;
    private String nombreFirmantesEPO;
    private String permitirFirma;
    
    public CartaAdhesion() {
        super();
    }

    public Integer getIcCartaAdhesion() {
        return icCartaAdhesion;
    }

    public void setIcCartaAdhesion(Integer icCartaAdhesion) {
        this.icCartaAdhesion = icCartaAdhesion;
    }

    public Integer getIcEPO() {
        return icEPO;
    }

    public void setIcEPO(Integer icEPO) {
        this.icEPO = icEPO;
    }

    public Integer getIcIF() {
        return icIF;
    }

    public void setIcIF(Integer icIF) {
        this.icIF = icIF;
    }

    public String getFolio() {
        return folio;
    }

    public void setFolio(String folio) {
        this.folio = folio;
    }

    public String getNombreCadena() {
        return nombreCadena;
    }

    public void setNombreCadena(String nombreCadena) {
        this.nombreCadena = nombreCadena;
    }

    public String getNombreMoneda() {
        return nombreMoneda;
    }

    public void setNombreMoneda(String nombreMoneda) {
        this.nombreMoneda = nombreMoneda;
    }

    public String getTipoFactoraje() {
        return tipoFactoraje;
    }

    public void setTipoFactoraje(String tipoFactoraje) {
        this.tipoFactoraje = tipoFactoraje;
    }

    public String getFechaFirma() {
        return fechaFirma;
    }

    public void setFechaFirma(String fechaFirma) {
        this.fechaFirma = fechaFirma;
    }

    public Float getSobretasa() {
        return sobretasa;
    }

    public void setSobretasa(Float sobretasa) {
        this.sobretasa = sobretasa;
    }

    public String getNumeroFirmantesIF() {
        return numeroFirmantesIF;
    }

    public void setNumeroFirmantesIF(String numeroFirmantesIF) {
        this.numeroFirmantesIF = numeroFirmantesIF;
    }

    public String getNumeroFirmantesEPO() {
        return numeroFirmantesEPO;
    }

    public void setNumeroFirmantesEPO(String numeroFirmantesEPO) {
        this.numeroFirmantesEPO = numeroFirmantesEPO;
    }

    public String getNombreFirmantesIF() {
        return nombreFirmantesIF;
    }

    public void setNombreFirmantesIF(String nombreFirmantesIF) {
        this.nombreFirmantesIF = nombreFirmantesIF;
    }

    public String getNombreFirmantesEPO() {
        return nombreFirmantesEPO;
    }

    public void setNombreFirmantesEPO(String nombreFirmantesEPO) {
        this.nombreFirmantesEPO = nombreFirmantesEPO;
    }

    public String getPermitirFirma() {
        return permitirFirma;
    }

    public void setPermitirFirma(String permitirFirma) {
        this.permitirFirma = permitirFirma;
    }

    public String getEstatus() {
        return estatus;
    }

    public void setEstatus(String estatus) {
        this.estatus = estatus;
    }

    public String getNombreIF() {
        return nombreIF;
    }

    public void setNombreIF(String nombreIF) {
        this.nombreIF = nombreIF;
    }
}
