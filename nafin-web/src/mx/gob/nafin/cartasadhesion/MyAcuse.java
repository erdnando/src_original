package mx.gob.nafin.cartasadhesion;

public class MyAcuse {
    
    private String folioPKI         ;
    private Integer icCartaAdhesion ;
    private Integer icIF ;
    private Integer icEPO ;
    private String icUsuario        ;
    private String nombreFirmante   ;
    private String fechaFirma       ;
    private String icTipoFirma      ;
    private String cadenaFirma      ;
    
    public MyAcuse() {
        super();
    }

    public String getFolioPKI() {
        return folioPKI;
    }

    public void setFolioPKI(String folioPKI) {
        this.folioPKI = folioPKI;
    }

    public Integer getIcCartaAdhesion() {
        return icCartaAdhesion;
    }

    public void setIcCartaAdhesion(Integer icCartaAdhesion) {
        this.icCartaAdhesion = icCartaAdhesion;
    }

    public String getIcUsuario() {
        return icUsuario;
    }

    public void setIcUsuario(String icUsuario) {
        this.icUsuario = icUsuario;
    }

    public String getNombreFirmante() {
        return nombreFirmante;
    }

    public void setNombreFirmante(String nombreFirmante) {
        this.nombreFirmante = nombreFirmante;
    }

    public String getFechaFirma() {
        return fechaFirma;
    }

    public void setFechaFirma(String fechaFirma) {
        this.fechaFirma = fechaFirma;
    }

    public String getIcTipoFirma() {
        return icTipoFirma;
    }

    public void setIcTipoFirma(String icTipoFirma) {
        this.icTipoFirma = icTipoFirma;
    }

    public String getCadenaFirma() {
        return cadenaFirma;
    }

    public void setCadenaFirma(String cadenaFirma) {
        this.cadenaFirma = cadenaFirma;
    }

    public Integer getIcIF() {
        return icIF;
    }

    public void setIcIF(Integer icIF) {
        this.icIF = icIF;
    }

    public Integer getIcEPO() {
        return icEPO;
    }

    public void setIcEPO(Integer icEPO) {
        this.icEPO = icEPO;
    }
}
