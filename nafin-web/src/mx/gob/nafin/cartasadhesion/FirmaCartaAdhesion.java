package mx.gob.nafin.cartasadhesion;


import com.nafin.docgarantias.ConstantesMonitor;
import com.nafin.docgarantias.MovimientoLinea;

import com.netro.exception.NafinException;
import com.netro.pdf.ComunesPDF;

import com.netro.xls.ComunesXLS;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.naming.NamingException;

import javax.servlet.http.HttpServletRequest;

import netropology.utilerias.AccesoDB;
import netropology.utilerias.AppException;
import netropology.utilerias.Comunes;
import netropology.utilerias.CreaArchivo;
import netropology.utilerias.ElementoCatalogo;
import netropology.utilerias.ServiceLocator;

import org.apache.commons.logging.Log;

public class FirmaCartaAdhesion {
    
    private static final Log log = ServiceLocator.getInstance().getLog(FirmaCartaAdhesion.class);
    private static final String FIRMA_EPO = "E";
    private static final String FIRMA_IF = "I";


    public FirmaCartaAdhesion() {
        super();
    }
        
    public String getVistaPrevia(Map<String,String> params){     
        String raw =  getTemplateCartaAdhesion();   
        if(raw != null){
            raw = raw.replaceAll("\n", "<br/>");
        }
        else{
            log.error("No se encontr� Template de Carta de Adhesi�n en la BD");
        }
        Date hoy = new Date();
        SimpleDateFormat sdfDia = new SimpleDateFormat("dd");
        SimpleDateFormat sdfMes = new SimpleDateFormat("MMMM");
        SimpleDateFormat sdfAnio = new SimpleDateFormat("YYYY");                                

        String[] plazosMNX = {"De 1 a 60 d�as","De 61 a 120 d�as","De 121 a 180 d�as","De 181 a 366 d�as"};
        String[] plazosUSD = {"De 1 a 30 d�as" ,"De 31 a 90 d�as" ,"De 91 a 180 d�as" ,"De 181 a 366 d�as"};
        String[] plazos = ("Moneda Nacional".equals(params.get("moneda"))) ? plazosMNX: plazosUSD;

        String tipoFactoraje = params.get("factoraje");
        Integer icPlazo = Integer.parseInt((params.get("icPlazo")).replace("p",""));
        boolean condicionUno = tipoFactoraje.equals("Factoraje Normal");
        boolean pintarTasa1 = condicionUno;
        boolean pintarTasa2 = condicionUno && icPlazo > 1;
        boolean pintarTasa3 = condicionUno && icPlazo > 2;
        boolean pintarTasa4 = condicionUno && icPlazo > 3;

        raw = raw.replaceAll("\\[EPO]", params.get("nombreEpo") );
        raw = raw.replaceAll("\\[IF]", params.get("nombreIF"));
        raw = raw.replaceAll("\\[MONEDA]", params.get("moneda"));
        raw = raw.replaceAll("\\[TIPO_FACTORAJE]", params.get("factoraje"));
        raw = raw.replaceAll("\\[FECHA_EPO-NAFIN]", params.get("dtFechaEpoNafin"));
        raw = raw.replaceAll("\\[FECHA_IF-NAFIN]", params.get("dtFechaIFNafin"));
        raw = raw.replaceAll("\\[TASA_MORATORIA]", params.get("txtTasaMoratoria"));
        raw = raw.replaceAll("\\[TITULAR_CUENTA]", params.get("txtTitularCuenta"));
        raw = raw.replaceAll("\\[BANCO]", params.get("txtBanco"));
        raw = raw.replaceAll("\\[NUMERO_CUENTA]", params.get("txtNumeroCuenta"));
        raw = raw.replaceAll("\\[CLABE]", params.get("txtCuentaClabe"));
        raw = raw.replaceAll("\\[SUCURSAL]", params.get("txtSucursal"));
        raw = raw.replaceAll("\\[PLAZA]", params.get("txtPlazaBanco"));
        raw = raw.replaceAll("\\[DIA]", sdfDia.format(hoy));
        raw = raw.replaceAll("\\[MES]", sdfMes.format(hoy));
        raw = raw.replaceAll("\\[A�O]", sdfAnio.format(hoy));
        raw = raw.replaceAll("\\[PLAZO1]", pintarTasa1 ? plazos[0]: "");
        raw = raw.replaceAll("\\[PLAZO2]", pintarTasa2 ? plazos[1]: "");
        raw = raw.replaceAll("\\[PLAZO3]", pintarTasa3 ? plazos[2]: "");
        raw = raw.replaceAll("\\[PLAZO4]", pintarTasa4 ? plazos[3]: "");
        raw = raw.replaceAll("\\[SOBRETASA1]", pintarTasa1 ? params.get("sobretasa1"): "");
        raw = raw.replaceAll("\\[SOBRETASA2]", pintarTasa2 ? params.get("sobretasa2"): "");
        raw = raw.replaceAll("\\[SOBRETASA3]", pintarTasa3 ? params.get("sobretasa3"): "");
        raw = raw.replaceAll("\\[SOBRETASA4]", pintarTasa4 ? params.get("sobretasa4"): "");

        raw = raw.replaceAll("\\[Plazos]",condicionUno ? "Plazos": "");
        raw = raw.replaceAll("\\[Puntos Adicionales]",condicionUno ? "Puntos Adicionales" : "");
        raw = raw.replaceAll("\\[PLAZO]", condicionUno ? "PLAZO" : "PLAZO: "+ params.get("plazo"));
        raw = raw.replaceAll("\n", "<br/>");
        log.info(raw);
        return raw;
    }
    
    public Integer guardaCartaAdhesion(Map<String, String> params){
        Integer icCarta = null;
        String tipoFactoraje = params.get("cbTipoFactoraje");
        Integer icPlazo = Integer.parseInt((params.get("cbPlazo")).replace("p",""));
        boolean condicionUno = !tipoFactoraje.equals("V");
        boolean insertarTasa1 = condicionUno;
        boolean insertarTasa2 = condicionUno && icPlazo > 1;
        boolean insertarTasa3 = condicionUno && icPlazo > 2;
        boolean insertarTasa4 = condicionUno && icPlazo > 3;
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean exito = false;
        try {
            con.conexionDB();
            String sqlSeq = " SELECT SEQ_CAD_CARTA_ADHESION.NEXTVAL AS IC_CARTA FROM DUAL ";
            ps = con.queryPrecompilado(sqlSeq); 
            rs = ps.executeQuery();
            while (rs.next()){
                icCarta = rs.getInt("IC_CARTA");
            }
            
            String sql = 
            " INSERT INTO cad_carta_adhesion (  "+
            "    ic_carta_adhesion,             "+
            "    ic_if,                         "+
            "    ic_epo,                        "+
            "    cc_tipo_factoraje,             "+
            "    ic_moneda,                     "+
            "    ic_plazo,                      "+
            "    dt_fecha_convenio_epo,         "+
            "    dt_fecha_convenio_if,          "+
            "    ic_estatus_carta,              "+
            "    fn_tasa_p1,                    "+
            "    fn_tasa_p2,                    "+
            "    fn_tasa_p3,                    "+
            "    fn_tasa_p4,                    "+
            "    cg_titular_cuenta,             "+
            "    cg_nombre_banco,               "+
            "    cg_numero_cuenta,              "+
            "    cg_clabe,                      "+
            "    cg_sucursal,                   "+
            "    cg_plaza,                      "+
            "    cg_tasa_moratoria,             "+
            "    in_numero_firmas_if,           "+
            "    in_numero_firmas_epo,          "+
            "    dt_fecha_creacion              "+
            ") VALUES (                         "+
            "    ?,                             "+
            "    ?,                             "+
            "    ?,                             "+
            "    ?,                             "+
            "    ?,                             "+
            "    ?,                             "+
            "    TO_DATE(?, 'dd/mm/yyyy'),      "+
            "    TO_DATE(?, 'dd/mm/yyyy'),      "+
            "    'I',                           "+
            "    ?,                             "+
            "    ?,                             "+
            "    ?,                             "+
            "    ?,                             "+
            "    ?,                             "+
            "    ?,                             "+
            "    ?,                             "+
            "    ?,                             "+
            "    ?,                             "+
            "    ?,                             "+
            "    ?,                             "+
            "    ?,                             "+
            "    ?,                             "+
            "    SYSDATE)                       ";
            ps = con.queryPrecompilado(sql); 
            
            ps.setInt(1, icCarta);
            ps.setInt(2, Integer.parseInt(params.get("icIF")));
            ps.setInt(3, Integer.parseInt(params.get("cbEPO")));
            ps.setString(4, params.get("cbTipoFactoraje"));
            ps.setInt(5, Integer.parseInt(params.get("cbMoneda")));
            ps.setInt(6, icPlazo);
            ps.setString(7, params.get("dtFechaEpoNafin"));
            ps.setString(8, params.get("dtFechaIFNafin"));
            if (insertarTasa1){
                ps.setFloat(9,  Float.parseFloat(params.get("sobretasa1")));    
            }else{
                ps.setNull(9,  Types.FLOAT);
            }
            if(insertarTasa2){
                ps.setFloat(10,  Float.parseFloat(params.get("sobretasa2")));    
            }else{
                ps.setNull(10,  Types.FLOAT);
            }
            if(insertarTasa3){
                ps.setFloat(11, Float.parseFloat(params.get("sobretasa3")));
            }else{
                ps.setNull(11, Types.FLOAT);
            }
            if (insertarTasa4){
                ps.setFloat(12, Float.parseFloat(params.get("sobretasa4")));
            }else{
                ps.setNull(12, Types.FLOAT);
            }
            ps.setString(13, params.get("txtTitularCuenta").trim());
            ps.setString(14, params.get("txtBanco").trim());
            ps.setString(15, params.get("txtNumeroCuenta").trim());
            ps.setString(16, params.get("txtCuentaClabe").trim());
            ps.setString(17, params.get("txtSucursal").trim());
            ps.setString(18, params.get("txtPlazaBanco").trim());
            ps.setString(19, params.get("txtTasaMoratoria").trim());
            ps.setInt(20, Integer.parseInt(params.get("txtNumeroFirmasIF")));
            ps.setInt(21, Integer.parseInt(params.get("txtNumeroFirmasEPO")));
            if(ps.executeUpdate()==1)    
                exito = true;
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al guardar la informaci�n", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }  
        if(exito)
            return icCarta;
        else
            return null;
    }

    public Map<String,String> getDatosCartaAdhesion(Integer icCartaAdhesion, Integer icEPO, Integer icIF){
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Map<String,String> data = new HashMap<>();
        try {
            con.conexionDB();
            String sql = 
            " SELECT                                                                                                    "+
            "     ic_carta_adhesion,                                                                                    "+
            "     comcat_if.cg_razon_social    AS nombre_if,                                                            "+
            "     comcat_epo.cg_razon_social   AS nombre_epo,                                                           "+
            "     DECODE(cc_tipo_factoraje, 'N', 'Factoraje Normal', 'V', 'Factoraje al Vencimiento') AS tipo_factoraje,"+
            "     DECODE(ic_moneda, 1, 'Moneda Nacional', 54, 'D�lar Americano') AS moneda,                             "+
            "     DECODE(ic_moneda, 1, 'De 1 a 60 d�as', 54, 'De 1 a 30 d�as') AS NombrePlazo_1,                        "+
            "     DECODE(ic_moneda, 1, 'De 61 a 120 d�a', 54, 'De 31 a 90 d�as') AS NombrePlazo_2,                      "+
            "     DECODE(ic_moneda, 1, 'De 121 a 180 d�as', 54, 'De 91 a 180 d�as') AS NombrePlazo_3,                   "+
            "     DECODE(ic_moneda, 1, 'De 181 a 366 d�as', 54, 'De 181 a 366 d�as') AS NombrePlazo_4,                  "+
            "     ic_plazo,                                                                                             "+
            "     TO_CHAR(dt_fecha_convenio_epo, 'dd/mm/yyyy') AS fecha_epo_nafin,                                      "+
            "     TO_CHAR(dt_fecha_convenio_if, 'dd/mm/yyyy') AS fecha_if_nafin,                                        "+
            "     EXTRACT(DAY FROM dt_fecha_creacion) AS dia_creacion,                                                  "+
            "     EXTRACT(MONTH FROM dt_fecha_creacion) AS mes_creacion,                                                "+
            "     EXTRACT(YEAR FROM dt_fecha_creacion) AS anio_creacion,                                                "+
            "     fn_tasa_p1,                                                                                           "+
            "     fn_tasa_p2,                                                                                           "+
            "     fn_tasa_p3,                                                                                           "+
            "     fn_tasa_p4,                                                                                           "+
            "     cg_titular_cuenta,                                                                                    "+
            "     cg_nombre_banco,                                                                                      "+
            "     cg_numero_cuenta,                                                                                     "+
            "     cg_clabe,                                                                                             "+
            "     cg_sucursal,                                                                                          "+
            "     cg_plaza,                                                                                             "+
            "     cg_tasa_moratoria,                                                                                    "+
            "     in_numero_firmas_if,                                                                                  "+
            "     in_numero_firmas_epo                                                                                  "+
            " FROM                                                                                                      "+
            "     cad_carta_adhesion c,                                                                                 "+
            "     comcat_if,                                                                                            "+
            "     comcat_epo                                                                                            "+
            " WHERE                                                                                                     "+
            "     c.ic_if = comcat_if.ic_if                                                                             "+
            "     AND c.ic_epo = comcat_epo.ic_epo                                                                      "+
            "     AND c.ic_epo = ?                                                                                      "+
            "     AND c.ic_if = ?                                                                                       "+
            "     AND ic_carta_adhesion = ?                                                                             ";
            ps = con.queryPrecompilado(sql); 
            ps.setInt(1, icEPO);
            ps.setInt(2, icIF);
            ps.setInt(3, icCartaAdhesion);
            rs = ps.executeQuery();
            while (rs.next()){
                data.put("IC_CARTA_ADHESION", rs.getString("ic_carta_adhesion"));
                data.put("NOMBRE_EPO", rs.getString("nombre_epo"));
                data.put("NOMBRE_IF", rs.getString("nombre_if"));
                data.put("TIPO_FACTORAJE", rs.getString("tipo_factoraje"));
                
                data.put("MONEDA", rs.getString("moneda"));
                data.put("IC_PLAZO", rs.getString("ic_plazo"));
                
                data.put("DIA_CREACION", rs.getString("dia_creacion"));
                data.put("MES_CREACION", rs.getString("mes_creacion"));
                data.put("ANIO_CREACION", rs.getString("anio_creacion"));
                
                data.put("FECHA_EPO_NAFIN", rs.getString("fecha_epo_nafin"));
                data.put("FECHA_IF_NAFIN", rs.getString("fecha_if_nafin"));
                
                data.put("NOMBRE_PLAZO1", rs.getString("NombrePlazo_1"));
                data.put("NOMBRE_PLAZO2", rs.getString("NombrePlazo_2"));
                data.put("NOMBRE_PLAZO3", rs.getString("NombrePlazo_3"));
                data.put("NOMBRE_PLAZO4", rs.getString("NombrePlazo_4"));
                
                data.put("TASA_1", rs.getString("fn_tasa_p1"));
                data.put("TASA_2", rs.getString("fn_tasa_p2"));
                data.put("TASA_3", rs.getString("fn_tasa_p3"));
                data.put("TASA_4", rs.getString("fn_tasa_p4"));
                
                data.put("TITULAR_CUENTA", rs.getString("cg_titular_cuenta"));
                data.put("NOMBRE_BANCO", rs.getString("cg_nombre_banco"));
                data.put("NUMERO_CUENTA", rs.getString("cg_numero_cuenta"));
                data.put("CLABE", rs.getString("cg_clabe"));
                data.put("SUCURSAL", rs.getString("cg_sucursal"));
                data.put("PLAZA", rs.getString("cg_plaza"));
                data.put("TASA_MORATORIA", rs.getString("cg_tasa_moratoria"));

                data.put("NUMERO_FIRMAS_IF", rs.getString("in_numero_firmas_if"));
                data.put("NUMERO_FIRMAS_EPO", rs.getString("in_numero_firmas_epo"));
            }        
        } catch (SQLException | NamingException e) {
            e.printStackTrace();
            log.error(e.getMessage());
            throw new AppException("Error inesperado al cargar la informaci�n", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }    
    return data;    
    }
    
    public String generarCartaPDF(String path, Map<String,String> param) {
        log.info("generarCartaPDF(E)");
        String nombreArchivo = "";
        String meses[] = {"","enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre"};
        String[] plazoSelMXN = {"","Hasta 60 d�as","Hasta 120 d�as","Hasta 180 d�as","Hasta 366 d�as"};
        String[] plazoSelUSD = {"","Hasta 1 mes","Hasta 3 meses","Hasta 6 meses","Hasta 12 meses"};
        Integer icIF = Integer.parseInt(param.get("ic_if"));
        Integer icCartaAdhesion = Integer.parseInt(param.get("ic_cartaAdhesion"));
        Integer icEPO = Integer.parseInt(param.get("ic_epo"));
        String pathFile = path +"00tmp/";
            try {
                String template = getTemplateCartaAdhesion();
                Map<String,String> data = this.getDatosCartaAdhesion(icCartaAdhesion, icEPO, icIF);
                String[] plazoMostrar = ("Moneda Nacional".equals(data.get("MONEDA"))) ? plazoSelMXN: plazoSelUSD;
                boolean mostrarSobretasa = data.get("TIPO_FACTORAJE").equals("Factoraje Normal");
                Integer icPlazo = Integer.parseInt(data.get("IC_PLAZO"));
                String p1,p2,p3,p4,st1,st2,st3,st4;
                p1 = p2 = p3 = p4 = st1 = st2 = st3 = st4 = "";
                if(mostrarSobretasa){
                    st1 = data.get("TASA_1");
                    st2 = icPlazo >1 ? data.get("TASA_2") : " ";
                    st3 = icPlazo >2 ? data.get("TASA_3") : " ";
                    st4 = icPlazo >3 ? data.get("TASA_4") : " ";
                    p1 = data.get("NOMBRE_PLAZO1");
                    p2 = icPlazo > 1 ? data.get("NOMBRE_PLAZO2") : " ";
                    p3 = icPlazo > 2 ? data.get("NOMBRE_PLAZO3") : " ";
                    p4 = icPlazo > 3 ? data.get("NOMBRE_PLAZO4") : " ";
                    template = template.replaceAll("\\[Plazos]", "Plazos");
                    template = template.replaceAll("\\[Puntos Adicionales]", "Puntos Adicionales");                    
                }else{
                    
                    template = template.replaceAll("\\[Plazos]", " ");
                    template = template.replaceAll("\\[Puntos Adicionales]", " ");
                }
                template = template.replaceAll("\\[PLAZO]", plazoMostrar[icPlazo]);
                template = template.replaceAll("\\[EPO]", data.get("NOMBRE_EPO") );
                template = template.replaceAll("\\[IF]", data.get("NOMBRE_IF"));
                template = template.replaceAll("\\[MONEDA]", data.get("MONEDA"));
                template = template.replaceAll("\\[TIPO_FACTORAJE]", data.get("TIPO_FACTORAJE"));
                template = template.replaceAll("\\[FECHA_EPO-NAFIN]", data.get("FECHA_EPO_NAFIN"));
                template = template.replaceAll("\\[FECHA_IF-NAFIN]", data.get("FECHA_IF_NAFIN"));
                template = template.replaceAll("\\[TASA_MORATORIA]", data.get("TASA_MORATORIA"));
                template = template.replaceAll("\\[TITULAR_CUENTA]", data.get("TITULAR_CUENTA"));
                template = template.replaceAll("\\[BANCO]", data.get("NOMBRE_BANCO"));
                template = template.replaceAll("\\[NUMERO_CUENTA]", data.get("NUMERO_CUENTA"));
                template = template.replaceAll("\\[CLABE]", data.get("CLABE"));
                template = template.replaceAll("\\[SUCURSAL]", data.get("SUCURSAL"));
                template = template.replaceAll("\\[PLAZA]", data.get("PLAZA"));
                template = template.replaceAll("\\[DIA]", data.get("DIA_CREACION"));
                template = template.replaceAll("\\[MES]", meses[Integer.parseInt(data.get("MES_CREACION"))]);
                template = template.replaceAll("\\[A�O]", data.get("ANIO_CREACION"));
                template = template.replaceAll("\\[PLAZO1]", p1);
                template = template.replaceAll("\\[PLAZO2]", p2);
                template = template.replaceAll("\\[PLAZO3]", p3);
                template = template.replaceAll("\\[PLAZO4]", p4);
                template = template.replaceAll("\\[SOBRETASA1]", st1);
                template = template.replaceAll("\\[SOBRETASA2]", st2);
                template = template.replaceAll("\\[SOBRETASA3]", st3);
                template = template.replaceAll("\\[SOBRETASA4]", st4);

                
                ComunesPDF pdfDoc = new ComunesPDF();
                nombreArchivo = Comunes.cadenaAleatoria(18) + ".pdf";
                pdfDoc = new ComunesPDF();
                pdfDoc.creaPDF(1,  pathFile+ nombreArchivo,  "", false, false, false);
                pdfDoc.setPadding(5f);               
                String[] templateSP = getFirstNLines(template, 4);
                pdfDoc.addText(templateSP[0], "contrato" ,ComunesPDF.CENTER);
                String meddleTemplate = templateSP[1].substring(0, templateSP[1].indexOf("[FIRMAS_EPO]"));
                pdfDoc.addTextWTabs(meddleTemplate);
                String templateFirmas = template.substring(template.indexOf("[FIRMAS_EPO]"), template.length());
                templateFirmas = templateFirmas.replaceAll("\\[FIRMAS_EPO]", getFirmasEPO(icCartaAdhesion));
                templateFirmas = templateFirmas.replaceAll("\\[FIRMAS_IF]", "\n"+getFirmasIF(icCartaAdhesion));
                pdfDoc.addText(templateFirmas, "contrato", ComunesPDF.LEFT);
                pdfDoc.endDocument();
            } catch(Throwable e) {
                    e.printStackTrace();
                    throw new AppException("Error al generar el archivo ", e);
            } 
        log.info("generarCartaPDF(S)");
        return  nombreArchivo;
    }
    
    private String getFirmasIF(Integer icCarta){
        return getFirmasCarta(icCarta, this.FIRMA_IF);
    }
    
    private String getFirmasEPO(Integer icCarta){
        return getFirmasCarta(icCarta, this.FIRMA_EPO);
    }    
    
    private String getFirmasCarta(Integer icCarta, String tipoFirma){
        StringBuffer firmas = new StringBuffer();
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con.conexionDB();
            String sql = 
                " SELECT                                                "+
                "     '|'||ic_usuario                                   "+
                "     || '|'                                            "+
                "     || REGEXP_REPLACE(cg_nombre_firmante,'( )+','.')  "+
                "     || '|'                                            "+
                "     || substr(tg_cadena_firma, - 50, 50)              "+
                "     AS NOMBRE_FIRMA                                   "+
                " FROM                                                  "+
                "     cad_acuse_firma                                   "+
                " WHERE                                                 "+
                "     ic_tipo_firma = ?                                 "+
                "     AND ic_carta_adhesion = ?                         "+
                " ORDER BY                                              "+
                "     dt_fecha_firma ASC                                ";
            ps = con.queryPrecompilado(sql); 
            ps.setString(1, tipoFirma);
            ps.setInt(2, icCarta);
            rs = ps.executeQuery();
            boolean agregaFinal = false;
            while (rs.next()){
                firmas.append(rs.getString("NOMBRE_FIRMA")+"\n");
                agregaFinal = true;
            }  
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al obtener las firmas de la carta", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }        
        return firmas.toString();
    }
    
    public String getTemplateCartaAdhesion() {
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String template = null;
        try {
            con.conexionDB();
            String sql = " SELECT CG_TEMPLATE_CARTA_ADHESION FROM CAD_TEMPLATE_CARTA_ADHESION WHERE  IC_TEMPLATE_CARTA_ADHESION = 1 ";
            ps = con.queryPrecompilado(sql); 
            rs = ps.executeQuery();
            while (rs.next()){
                template = rs.getString("CG_TEMPLATE_CARTA_ADHESION");
            }      
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al cargar la informaci�n", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }
        return template;
    }   

    public ArrayList<CartaAdhesion> getListaCartasAdhesion(String icUsuario, Map<String,String> params){
            ArrayList<CartaAdhesion> lista = new ArrayList<>();
            AccesoDB con = new AccesoDB();

        for (Map.Entry<String, String> p : params.entrySet()) {
            log.info("[" + p.getKey() + ", " + p.getValue()+"]");
        }
            PreparedStatement ps = null;
            ResultSet rs = null;
            String sqlFiltros = "";
            Integer icFolio = null;            
            if (params.get("icEstatus") != null) sqlFiltros += " AND c.IC_ESTATUS_CARTA = ? ";
            if (params.get("icFolio") != null){
                String [] folio = params.get("icFolio").split("/");
                if(folio.length == 2){
                    try{
                        Integer aaaa = Integer.parseInt(folio[0]);
                        icFolio = Integer.parseInt(folio[1]);
                    }
                    catch(NumberFormatException e){
                        return lista;                            
                    }
                }
                else{
                    return lista;
                }
                icFolio = Integer.parseInt(folio[1]);
                sqlFiltros += " AND C.IC_CARTA_ADHESION = ? ";
            }
            if (params.get("icMoneda") != null) sqlFiltros += " AND c.IC_MONEDA = ? ";
            if (params.get("icFactoraje") != null) sqlFiltros += " AND c.CC_TIPO_FACTORAJE = ? ";
            if (params.get("fechaDos") != null) sqlFiltros += " AND C.DT_FECHA_CREACION between TO_DATE(?, 'dd/mm/yyyy')  and TO_DATE(?, 'dd/mm/yyyy') ";
            if (params.get("iceEPO") != null) sqlFiltros += " AND c.ic_epo  = ? ";
            if (params.get("icIF") != null) sqlFiltros += " AND c.ic_if = ? ";
            
            try {
                con.conexionDB();    
        String sql = 
            " SELECT                                                                         "+
            "     c.ic_if, c.ic_epo,                                                         "+
            "     c.ic_carta_adhesion   AS iccartaadhesion,                                  "+
            "     EXTRACT(YEAR FROM c.dt_fecha_creacion)                                     "+
            "     || '/'                                                                     "+
            "     || c.ic_carta_adhesion AS folio,                                           "+
            "     e.cg_razon_social     AS cadena,                                           "+
            "     i.cg_razon_social     AS intermediario,                                    "+
            "     DECODE(c.ic_moneda,1, 'Moneda Nacional',54,'D�lar Americano') AS moneda,   "+
            "     DECODE(c.cc_tipo_factoraje,'N','Normal','V','Al Vencimiento') AS factoraje,"+
            "     TO_CHAR(c.DT_FECHA_CREACION, 'dd/mm/yyyy') AS fechafirma,                  "+
            "     c.fn_tasa_p1          AS sobretasa,                                        "+
            "     (                                                                          "+
            "         SELECT                                                                 "+
            "             COUNT(*)                                                           "+
            "         FROM                                                                   "+
            "             cad_acuse_firma                                                    "+
            "         WHERE                                                                  "+
            "             cad_acuse_firma.ic_tipo_firma = 'I'                                "+
            "             AND cad_acuse_firma.ic_carta_adhesion = c.ic_carta_adhesion        "+
            "     )                                                                          "+
            "     || ' de '                                                                  "+
            "     || c.in_numero_firmas_if AS firmantesif,                                   "+
            "     (                                                                          "+
            "         SELECT                                                                 "+
            "             COUNT(*)                                                           "+
            "         FROM                                                                   "+
            "             cad_acuse_firma                                                    "+
            "         WHERE                                                                  "+
            "             cad_acuse_firma.ic_tipo_firma = 'E'                                "+
            "             AND cad_acuse_firma.ic_carta_adhesion = c.ic_carta_adhesion        "+
            "     )                                                                          "+
            "     || ' de '                                                                  "+
            "     || c.in_numero_firmas_epo AS firmantesepo,                                 "+
            "     (                                                                          "+
            "         SELECT                                                                 "+
            "             LISTAGG(cg_nombre_firmante, '<br/>') WITHIN GROUP(                 "+
            "                 ORDER BY                                                       "+
            "                     cg_nombre_firmante                                         "+
            "             ) lista_nombres                                                    "+
            "         FROM                                                                   "+
            "             cad_acuse_firma                                                    "+
            "         WHERE                                                                  "+
            "             cad_acuse_firma.ic_carta_adhesion = c.ic_carta_adhesion            "+
            "             AND ic_tipo_firma = 'I'                                            "+
            "     ) AS firmaron_if,                                                          "+
            "     (                                                                          "+
            "         SELECT                                                                 "+
            "             LISTAGG(cg_nombre_firmante, '<br/>') WITHIN GROUP(                 "+
            "                 ORDER BY                                                       "+
            "                     cg_nombre_firmante                                         "+
            "             ) lista_nombres                                                    "+
            "         FROM                                                                   "+
            "             cad_acuse_firma                                                    "+
            "         WHERE                                                                  "+
            "             cad_acuse_firma.ic_carta_adhesion = c.ic_carta_adhesion            "+
            "             AND ic_tipo_firma = 'E'                                            "+
            "     ) AS firmaron_epo,                                                         "+
            "     DECODE((                                                                   "+
            "         SELECT                                                                 "+
            "             COUNT(*)                                                           "+
            "         FROM                                                                   "+
            "             cad_acuse_firma                                                    "+
            "         WHERE                                                                  "+
            "             ic_usuario = ?                                                     "+
            "             AND cad_acuse_firma.ic_carta_adhesion = c.ic_carta_adhesion        "+
            "     ), 0, 'S', 'N') AS permitefirmar,                                           "+
            " DECODE(c.IC_ESTATUS_CARTA, 'I', 'En Firma IF', 'E', 'En Firma EPO',            "+
            " 'F','Formalizado', 'A', 'Autorizado Nafin') AS estatus                         "+
            " FROM                                                                           "+
            "     cad_carta_adhesion   c,                                                    "+
            "     comcat_epo           e,                                                    "+
            "     comcat_if            i                                                     "+
            " WHERE                                                                          "+
            "     c.ic_epo = e.ic_epo                                                        "+
            "     AND c.ic_if = i.ic_if                                                      "+
            sqlFiltros+
            " ORDER BY                                                                       "+
            "     c.ic_carta_adhesion DESC                                                   ";
        ps = con.queryPrecompilado(sql); 
        ps.setString(1, icUsuario);
        int indice = 2;                
        
        if (params.get("icEstatus") != null) {ps.setString(indice, params.get("icEstatus")); indice++;}
        if (params.get("icFolio") != null){ ps.setInt(indice, icFolio); indice++;}
        if (params.get("icMoneda") != null){ps.setInt(indice,Integer.parseInt(params.get("icMoneda"))); indice++;}
        if (params.get("icFactoraje") != null) {ps.setString(indice, params.get("icFactoraje")); indice++;}
        if (params.get("fechaDos") != null){
            ps.setString(indice,params.get("fechaUno"));
            indice++;
            ps.setString(indice,params.get("fechaDos"));
            indice++;
        }
        if (params.get("iceEPO") != null) { ps.setInt(indice, Integer.parseInt(params.get("iceEPO"))); indice++;}
        if (params.get("icIF") != null)  ps.setInt(indice, Integer.parseInt(params.get("icIF"))); 
            
        log.info(sql);            
        log.info(params.toString());
        rs = ps.executeQuery();
        while (rs.next()){
            CartaAdhesion c = new CartaAdhesion();
            c.setEstatus(rs.getString("estatus"));
            c.setFechaFirma(rs.getString("fechafirma"));
            c.setFolio(rs.getString("folio"));
            c.setIcCartaAdhesion(rs.getInt("iccartaadhesion"));
            c.setIcEPO(rs.getInt("ic_epo"));
            c.setIcIF(rs.getInt("ic_if"));
            c.setNombreCadena(rs.getString("cadena"));
            c.setNombreIF(rs.getString("intermediario"));
            c.setNombreFirmantesEPO(rs.getString("firmaron_epo"));
            c.setNombreFirmantesIF(rs.getString("firmaron_if"));
            c.setNombreMoneda(rs.getString("moneda"));
            c.setNumeroFirmantesEPO(rs.getString("firmantesepo"));
            c.setNumeroFirmantesIF(rs.getString("firmantesif"));
            c.setPermitirFirma(rs.getString("permitefirmar"));
            if (rs.getString("factoraje").equals("Normal"))
                c.setSobretasa(rs.getFloat("sobretasa"));
            c.setTipoFactoraje(rs.getString("factoraje"));
            lista.add(c);
        }        
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al cargar la informaci�n de las cartas de adhesi�n", e);
        } finally {
        AccesoDB.cerrarResultSet(rs);
        AccesoDB.cerrarStatement(ps);
        if (con.hayConexionAbierta()) {
            con.cierraConexionDB();
        }             
        }
        return lista;    
    }  
    
    public ArrayList<ElementoCatalogo> getCatalogoCartasEPOS(){
        ArrayList<ElementoCatalogo> catalogo = new ArrayList<>();
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con.conexionDB();
            String sql = 
                " SELECT DISTINCT                                   "+
                "     ( comcat_epo.ic_epo ) AS IC_EPO,              "+
                "     comcat_epo.CG_RAZON_SOCIAL                    "+
                " FROM                                              "+
                "     cad_carta_adhesion,                           "+
                "     comcat_epo                                    "+
                " WHERE                                             "+
                "     cad_carta_adhesion.ic_epo = comcat_epo.ic_epo "+
                " ORDER BY                                          "+
                "     ( 2 )                                         ";
            ps = con.queryPrecompilado(sql); 
            rs = ps.executeQuery();
            while (rs.next()){
                String valor  = rs.getString("CG_RAZON_SOCIAL");
                Integer clave = rs.getInt("IC_EPO");
                ElementoCatalogo e = new ElementoCatalogo(clave+"", valor);
                catalogo.add(e);
            }        
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al cargar la informaci�n", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }            
        return catalogo;
    }

    public ArrayList<ElementoCatalogo> getCatIntermediarios(){
        ArrayList<ElementoCatalogo> catalogo = new ArrayList<>();
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con.conexionDB();
            String sql = 
                " SELECT DISTINCT                                 "+
                "     ( comcat_if.ic_if ) AS ic_if,               "+
                "     comcat_if.cg_razon_social                   "+
                " FROM                                            "+
                "     cad_carta_adhesion,                         "+
                "     comcat_if                                   "+
                " WHERE                                           "+
                "     cad_carta_adhesion.ic_if = comcat_if.ic_if  "+
                " ORDER BY                                        "+
                "     ( 2 )                                       ";
            ps = con.queryPrecompilado(sql); 
            rs = ps.executeQuery();
            while (rs.next()){
                String valor  = rs.getString("cg_razon_social");
                Integer clave = rs.getInt("ic_if");
                ElementoCatalogo e = new ElementoCatalogo(clave+"", valor);
                catalogo.add(e);
            }        
        } catch (SQLException | NamingException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al cargar la informaci�n", e);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }            
        return catalogo;
    }    
    
    public String[] getFirstNLines(String text, Integer n){
        text = text.replaceAll("\r","");
        String[] splitted = text.split("(\n)+");
        String lines= "";
        String otherLines= "";
        int index = 0;
        for (String l : splitted){
            if (index < n) {
                lines += l + "\n";
            }
            else{
                otherLines += l + "\n";
            }
            index++;
        }
        String[] returnSplit = {lines, otherLines};   
        return  returnSplit;
    }
    
    public boolean firmaCartaAdhesion(MyAcuse acuse){
        log.info("firmaCartaAdhesion(E)");
        boolean exito = false;
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;     
        try{
            con.conexionDB();
            // validacion que el usuario no haya firmado la carta 
            String sqlValidaFirmaUsuario = 
                        " SELECT                            "+
                        "     Count(TG_FOLIO_PKI) AS FRIMAS "+
                        " FROM                              "+
                        "     cad_acuse_firma               "+
                        " WHERE                             "+
                        "     ic_usuario = ?                "+
                        "     AND ic_carta_adhesion = ?     ";
            ps = con.queryPrecompilado(sqlValidaFirmaUsuario); 
            ps.setString(1, acuse.getIcUsuario());
            ps.setInt(2, acuse.getIcCartaAdhesion());
            log.info("sqlValidaFirmaUsuario: "+sqlValidaFirmaUsuario);
            rs = ps.executeQuery();
            int firmasRegistradasDelUsuario = 0;
            while (rs.next()){    
                    firmasRegistradasDelUsuario = rs.getInt("FRIMAS");
            }
            if (firmasRegistradasDelUsuario == 0){
                // validacion si se puede firmar la carta            
                    String sqlValidaCartaParaFirma = 
                            " SELECT                                                     "+
                            "     DECODE(c.ic_estatus_carta, 'I', c.in_numero_firmas_if, "+
                            "                                'E', c.in_numero_firmas_epo,"+
                            "                                 0) AS firmas_requeridas,   "+
                            "     (SELECT                                                "+
                            "         COUNT(*)                                           "+
                            "     FROM cad_acuse_firma                                   "+
                            "     WHERE ic_tipo_firma = c.ic_estatus_carta               "+
                            "     AND ic_carta_adhesion = c.ic_carta_adhesion)           "+
                            "     AS firmas_registradas                                  "+
                            " FROM cad_carta_adhesion c                                  "+
                            " WHERE                                                      "+
                            "     c.ic_epo = ?                                           "+
                            "     AND c.ic_if = ?                                        "+
                            "     AND c.ic_estatus_carta = ?                             "+
                            "     AND c.ic_carta_adhesion = ?                            ";
                    log.info("sqlValidaCartaParaFirma: "+sqlValidaCartaParaFirma);
                    ps = con.queryPrecompilado(sqlValidaCartaParaFirma); 
                    ps.setInt(1, acuse.getIcEPO());
                    ps.setInt(2, acuse.getIcIF());
                    ps.setString(3, acuse.getIcTipoFirma());
                    ps.setInt(4, acuse.getIcCartaAdhesion());
                    rs = ps.executeQuery(); 
                    int firmasRequeridas = 0;
                    int firmasRegistradas = 0;
                    while (rs.next()){    
                            firmasRequeridas = rs.getInt("firmas_requeridas");
                            firmasRegistradas = rs.getInt("firmas_registradas");
                    }                    
                    if (firmasRequeridas > firmasRegistradas){
                        // firma la carta
                        String sqlInsertarAcuse = 
                            " INSERT INTO cad_acuse_firma (             "+
                            "     tg_folio_pki,                         "+
                            "     ic_carta_adhesion,                    "+
                            "     ic_usuario,                           "+
                            "     cg_nombre_firmante,                   "+
                            "     dt_fecha_firma,                       "+
                            "     ic_tipo_firma,                        "+
                            "     tg_cadena_firma                       "+
                            " )                                         "+
                            "     SELECT                                "+
                            "         ?,                                "+
                            "         ?,                                "+
                            "         ?,                                "+
                            "         ?,                                "+
                            "        TO_DATE(?,'dd/mm/yyyy HH24:MI:SS'),"+
                            "         ?,                                "+
                            "         ?                                 "+
                            "     FROM                                  "+
                            "         dual                              "+
                            "     WHERE                                 "+
                            "         NOT EXISTS (                      "+
                            "             SELECT                        "+
                            "                 tg_folio_pki,             "+
                            "                 ic_carta_adhesion,        "+
                            "                 ic_usuario,               "+
                            "                 cg_nombre_firmante,       "+
                            "                 dt_fecha_firma,           "+
                            "                 ic_tipo_firma,            "+
                            "                 tg_cadena_firma           "+
                            "             FROM                          "+
                            "                 cad_acuse_firma           "+
                            "             WHERE                         "+
                            "                 ic_usuario = ?            "+
                            "                 AND ic_carta_adhesion = ? "+
                            "         )                                 ";
                        log.info("sqlInsertarAcuse: "+sqlInsertarAcuse);
                        ps = con.queryPrecompilado(sqlInsertarAcuse); 
                        ps.setString(1, acuse.getFolioPKI());
                        ps.setInt(2, acuse.getIcCartaAdhesion());
                        ps.setString(3, acuse.getIcUsuario());
                        ps.setString(4, acuse.getNombreFirmante());
                        ps.setString(5, acuse.getFechaFirma());
                        ps.setString(6, acuse.getIcTipoFirma());
                        ps.setString(7, acuse.getCadenaFirma());
                        ps.setString(8, acuse.getIcUsuario());
                        ps.setInt(9, acuse.getIcCartaAdhesion());
                        int resultadoInsercion = ps.executeUpdate(); 
                        // actualiza el estatus
                        firmasRegistradas +=1;
                        if (resultadoInsercion == 1){
                            if (firmasRequeridas == firmasRegistradas){
                                // Avanza al siguiente estatus la carta
                                String updateEstatusCarta = 
                                    " UPDATE cad_carta_adhesion                                                "+
                                    " SET                                                                      "+
                                    "     dt_fecha_formalizacion = DECODE(ic_estatus_carta,'E', SYSDATE, null),"+
                                    "     ic_estatus_carta = DECODE(ic_estatus_carta, 'I', 'E', 'E', 'F', 404) "+
                                    " WHERE                                                                    "+
                                    "     ic_carta_adhesion = ?                                                ";
                                log.info("updateEstatusCarta: "+updateEstatusCarta);
                                ps = con.queryPrecompilado(updateEstatusCarta); 
                                ps.setInt(1, acuse.getIcCartaAdhesion());
                                //ps.setInt(2, acuse.getIcCartaAdhesion());
                                ps.executeUpdate();
                            }
                            exito = true;                               
                        }
                    }
            }
        }
        catch(Exception e){
            log.error(e.getMessage());    
            e.printStackTrace();
        }
        finally {
            con.terminaTransaccion(exito);
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        } 
        log.info("firmaCartaAdhesion(S)");
        return exito;
    }

    public boolean autorizarCartaAdhesion(Integer icCarta, Integer icIF, Integer icEPO){
        log.info("autorizarCartaAdhesion(E)");
        boolean exito = false;
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;     
        try{
            con.conexionDB();
            // validacion que la carta este formalizada por la EPO y por el IF
            String sqlValidaEstatusCarta = 
                        " SELECT  ic_carta_adhesion FROM cad_carta_adhesion  "+
                        " WHERE                                              "+
                        "     ic_epo = ?                                     "+
                        "     AND ic_if = ?                                  "+
                        "     AND ic_estatus_carta = 'F'                     "+
                        "     AND ic_carta_adhesion = ?                      ";
            
            ps = con.queryPrecompilado(sqlValidaEstatusCarta); 
            ps.setInt(1, icEPO);
            ps.setInt(2, icIF);
            ps.setInt(3, icCarta);
            log.info("sqlValidaEstatusCarta: "+sqlValidaEstatusCarta);
            rs = ps.executeQuery();
            Integer icCartaBD = null;
            while (rs.next()){    
                    icCartaBD = rs.getInt("ic_carta_adhesion");
            }
            if (icCartaBD != null && icCartaBD == icCarta){
                // Actualizar estatus de la carta            
                String updateEstatusCarta = 
                    " UPDATE cad_carta_adhesion         "+
                    " SET                               "+
                    "     ic_estatus_carta = 'A'        "+
                    " WHERE                             "+
                    "     ic_epo = ?                    "+
                    "     AND ic_if = ?                 "+
                    "     AND ic_carta_adhesion = ?     ";
                log.info("updateEstatusCarta: "+updateEstatusCarta);
                ps = con.queryPrecompilado(updateEstatusCarta); 
                ps.setInt(1, icEPO);
                ps.setInt(2, icIF);
                ps.setInt(3, icCarta);
                ps.executeUpdate(); 
                exito = true;
            }
            else{
                log.error("No se encontr� la carta de ahesion con los parametros enviados: icCarta"+icCarta+" icIF:"+icIF+" icEPO:"+icEPO);
            }
        }
        catch(Exception e){
            log.error(e.getMessage());    
            e.printStackTrace();
        }
        finally {
            con.terminaTransaccion(exito);
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        } 
        log.info("autorizarCartaAdhesion(S)");
        return exito;
    }    
    
    public boolean eliminarCartaAdhesion(Integer icCartaAdhesion, Integer icIF, Integer icEPO){
        boolean exito= false;
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con.conexionDB();
            String sqlBuscaCartaAdhesion = " SELECT IC_CARTA_ADHESION FROM CAD_CARTA_ADHESION WHERE " +
                " IC_CARTA_ADHESION = ? AND IC_IF = ? AND IC_EPO = ? ";
            ps = con.queryPrecompilado(sqlBuscaCartaAdhesion); 
            ps.setInt(1, icCartaAdhesion);
            ps.setInt(2, icIF);
            ps.setInt(3, icEPO);
            log.info(sqlBuscaCartaAdhesion);
            log.info("Params["+icCartaAdhesion +", "+icIF+", "+icEPO+"]");
            rs = ps.executeQuery();               
            Integer icCartaBD = null;
            while(rs.next()){
                    icCartaBD = rs.getInt("IC_CARTA_ADHESION");
            }
            if (icCartaBD != null && icCartaBD == icCartaAdhesion){
                log.info("OK, parametros correctos, eliminando acuses de firma de la carta: icCarta"+icCartaAdhesion+" icIF:"+icIF+" icEPO:"+icEPO);
                String sqlAcuse = " DELETE FROM CAD_ACUSE_FIRMA WHERE IC_CARTA_ADHESION = ? ";
                ps = con.queryPrecompilado(sqlAcuse); 
                ps.setInt(1, icCartaAdhesion);
                ps.executeUpdate();
                log.info("OK, eliminando la carta: icCarta"+icCartaAdhesion+" icIF:"+icIF+" icEPO:"+icEPO);
                String sqlCartaAdhesion = " DELETE FROM CAD_CARTA_ADHESION WHERE IC_CARTA_ADHESION = ? ";
                ps = con.queryPrecompilado(sqlCartaAdhesion); 
                ps.setInt(1, icCartaAdhesion);
                ps.executeUpdate();            
                exito = true;                
            }
            else{
                String mensaje = "Error en los parametro o la carta no existe";
                log.error(mensaje);
                throw new NafinException(mensaje);
            }
            log.info("OK, Se elimin� la carta: icCarta"+icCartaAdhesion+" icIF:"+icIF+" icEPO:"+icEPO);
        } catch (SQLException | NamingException |NafinException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al eliminar la informaci�n", e);
        } finally {
            con.terminaTransaccion(exito);
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }
        }            
        return exito;
    }    
    
    public boolean rechazarCartaAdhesion(Integer icCartaAdhesion, Integer icIF, Integer icEPO){
        boolean exito= false;
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con.conexionDB();
            String sqlBuscaCartaAdhesion = " SELECT IC_CARTA_ADHESION FROM CAD_CARTA_ADHESION WHERE " +
                " IC_CARTA_ADHESION = ? AND IC_IF = ? AND IC_EPO = ? ";
            ps = con.queryPrecompilado(sqlBuscaCartaAdhesion); 
            ps.setInt(1, icCartaAdhesion);
            ps.setInt(2, icIF);
            ps.setInt(3, icEPO);
            log.info(sqlBuscaCartaAdhesion);
            log.info("Params["+icCartaAdhesion +", "+icIF+", "+icEPO+"]");
            rs = ps.executeQuery();               
            Integer icCartaBD = null;
            while(rs.next()){
                    icCartaBD = rs.getInt("IC_CARTA_ADHESION");
            }
            if (icCartaBD != null && icCartaAdhesion == icCartaBD ){
                log.info("OK, parametros correctos, eliminando acuses de firma carta: icCarta"+icCartaAdhesion+" icIF:"+icIF+" icEPO:"+icEPO);
                String sqlAcuse = " DELETE FROM CAD_ACUSE_FIRMA WHERE IC_CARTA_ADHESION = ? ";
                ps = con.queryPrecompilado(sqlAcuse); 
                ps.setInt(1, icCartaAdhesion);
                ps.executeUpdate();
                log.info("OK, actualizando estatus de carta: icCarta"+icCartaAdhesion);
                String sqlCartaAdhesion = " UPDATE CAD_CARTA_ADHESION SET ic_estatus_carta = 'I' WHERE IC_CARTA_ADHESION = ? ";
                ps = con.queryPrecompilado(sqlCartaAdhesion); 
                ps.setInt(1, icCartaAdhesion);
                ps.executeUpdate();            
                exito = true;                
            }
            else{
                String mensaje = "Error en los parametro o la carta no existe";
                log.error(mensaje);
                throw new NafinException(mensaje);
            }
            log.info("OK, se actualizo el estatus de la carta: icCarta"+icCartaAdhesion+" icIF:"+icIF+" icEPO:"+icEPO);
        } catch (SQLException | NamingException |NafinException e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new AppException("Error inesperado al eliminar o actualizar la informaci�n", e);
        } finally {
            con.terminaTransaccion(exito);
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }            
        return exito;
    }       
    
    public String getReporteXLS(String path, Map<String, String> param, String icUsuario ) {
            log.info("getReporteXLS(E)");
            String nombreArchivo = "archivo";
            ArrayList<CartaAdhesion>  resultado =  getListaCartasAdhesion(icUsuario, param);
            String pathFile = path +"00tmp/";
            String paramIF =  param.get("icIF") != null  ?  getNombreIF(Integer.parseInt(param.get("icIF"))) : "Todos";
            String paramEPO =  param.get("iceEPO") != null ? getNombreEPO(Integer.parseInt(param.get("iceEPO"))) : "Todas";
            String paramMoneda  = param.get("icMoneda") != null ? ("54".equals(param.get("icMoneda")) ? "D�lar Americano" : "Moneda Nacional") : "Todas";
            String paramFactoraje = param.get("icFactoraje") != null ? ("N".equals(param.get("icFactoraje")) ? "Normal": "Al Vencimiento") : "Todos";
            String paramFechaSolicitud =  param.get("fechaUno") != null ? param.get("fechaUno")+ " al " +param.get("fechaDos")  : "Todas";
            String paramEstatus =  "Todos";
            if (param.get("icEstatus") != null){
                String e = param.get("icEstatus");
                if(e.equals("I")) paramEstatus = "En Firma IF";
                else if(e.equals("E")) paramEstatus = "En Firma EPO";
                else if(e.equals("F")) paramEstatus = "Formalizado";
                else paramEstatus = "Autorizado NAFIN";
            }
            String paramFolio =  param.get("icFolio") != null ? param.get("icFolio") : "Todos";
                try {
                    CreaArchivo archivo  = new CreaArchivo();
                    nombreArchivo   = archivo.nombreArchivo()+".xls";
                    ComunesXLS xlsDoc  = new ComunesXLS(pathFile+nombreArchivo);
                    xlsDoc.agregaTitulo("Cartas de Adhesi�n",9);                
                    // Tabla de parametros enviados
                    xlsDoc.setTabla(2);
                    xlsDoc.setCelda("Cadena productiva","formasb",ComunesXLS.LEFT);
                    xlsDoc.setCelda(paramEPO,"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda("Intermediario Financiero","formasb",ComunesXLS.LEFT);
                    xlsDoc.setCelda(paramIF,"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda("Moneda","formasb",ComunesXLS.LEFT);
                    xlsDoc.setCelda(paramMoneda,"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda("Tipo de Factoraje","formasb",ComunesXLS.LEFT);                
                    xlsDoc.setCelda(paramFactoraje,"forma",ComunesXLS.LEFT);                
                    xlsDoc.setCelda("Fecha de Soliciud","formasb",ComunesXLS.LEFT);
                    xlsDoc.setCelda(paramFechaSolicitud,"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda("Estatus","formasb",ComunesXLS.LEFT);
                    xlsDoc.setCelda(paramEstatus,"forma",ComunesXLS.LEFT);
                    xlsDoc.setCelda("Folio","formasb",ComunesXLS.LEFT);
                    xlsDoc.setCelda(paramFolio,"formas",ComunesXLS.LEFT);
                    xlsDoc.setCelda("","formas",ComunesXLS.LEFT);                
                    xlsDoc.cierraTabla();
                    
                    xlsDoc.setTabla(12);
                    xlsDoc.setCelda("Folio","formasb",ComunesXLS.CENTER);
                    xlsDoc.setCelda("Cadena Productiva","formasb",ComunesXLS.CENTER);
                    xlsDoc.setCelda("Intermediario Financiero","formasb",ComunesXLS.CENTER);
                    xlsDoc.setCelda("Moneda","formasb",ComunesXLS.CENTER);
                    xlsDoc.setCelda("Tipo de Factoraje","formasb",ComunesXLS.CENTER);    
                    xlsDoc.setCelda("Fecha de Firma","formasb",ComunesXLS.CENTER);  
                    xlsDoc.setCelda("Sobretasa 1er plazo","formasb",ComunesXLS.CENTER);
                    xlsDoc.setCelda("Estatus","formasb",ComunesXLS.CENTER);
                    xlsDoc.setCelda("Firmates IF","formasb",ComunesXLS.CENTER);
                    xlsDoc.setCelda("Nombre Firmates IF","formasb",ComunesXLS.CENTER);                  
                    xlsDoc.setCelda("Firmates EPO","formasb",ComunesXLS.CENTER);
                    xlsDoc.setCelda("Nombre Firmates EPO","formasb",ComunesXLS.CENTER);      
                    for (CartaAdhesion c : resultado){
                        String sobretasa = c.getSobretasa() != null ? c.getSobretasa()+"" : "N/A";
                        String nombresIF = c.getNombreFirmantesIF() != null ? c.getNombreFirmantesIF().replaceAll("<br\\/>", ", \n") : "";
                        String nombresEPO= c.getNombreFirmantesEPO() != null ? c.getNombreFirmantesEPO().replaceAll("<br\\/>", ", \n") :"";                            
                        xlsDoc.setCelda(c.getFolio(),"formas",ComunesXLS.LEFT);
                        xlsDoc.setCelda(c.getNombreCadena(),"formas",ComunesXLS.LEFT);
                        xlsDoc.setCelda(c.getNombreIF(),"formas",ComunesXLS.LEFT);
                        xlsDoc.setCelda(c.getNombreMoneda(),"formas",ComunesXLS.LEFT);
                        xlsDoc.setCelda(c.getTipoFactoraje(),"formas",ComunesXLS.LEFT);
                        xlsDoc.setCelda(c.getFechaFirma(),"formas",ComunesXLS.CENTER);
                        xlsDoc.setCelda(sobretasa,"formas",ComunesXLS.RIGHT);
                        xlsDoc.setCelda(c.getEstatus(),"formas",ComunesXLS.LEFT);
                        xlsDoc.setCelda(c.getNumeroFirmantesIF(),"formas",ComunesXLS.CENTER);
                        xlsDoc.setCelda(nombresIF,"formas",ComunesXLS.LEFT);
                        xlsDoc.setCelda(c.getNumeroFirmantesEPO(),"formas",ComunesXLS.CENTER);
                        xlsDoc.setCelda(nombresEPO,"formas",ComunesXLS.LEFT);                        
                    }                
                    xlsDoc.cierraTabla();
                    xlsDoc.cierraXLS();                
                } catch (Throwable e) 
                {   
                    log.error(e.getMessage());
                    throw new AppException("Error al generar el archivo ", e);
                }      
            log.info("getReporteXLS(S)");                
            return  nombreArchivo;
        }    
    
    
    private String getNombreEPO(Integer icCatalogo){
        return getNombreEnte(FIRMA_EPO, icCatalogo);
    }
    
    private String getNombreIF(Integer icCatalogo){
        return getNombreEnte(FIRMA_IF, icCatalogo);
    }
    
    private String getNombreEnte(String tipo, Integer icCatalogo){
        String nombre = ""    ;
        AccesoDB con = new AccesoDB();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String c = " COMCAT_EPO ";
        String e = " IC_EPO ";
        if (tipo.equals(FIRMA_IF)){
            c = " COMCAT_IF ";
            e = " IC_IF ";
        }
        try {
            con.conexionDB();
            String sql = "SELECT CG_RAZON_SOCIAL FROM "+ c +" WHERE "+ e +" = ? ";
            ps = con.queryPrecompilado(sql); 
            ps.setInt(1, icCatalogo);
            rs = ps.executeQuery();
            while (rs.next()){
                nombre  = rs.getString("CG_RAZON_SOCIAL");
            }        
        } catch (SQLException | NamingException ex) {
            log.error(ex.getMessage());
            ex.printStackTrace();
            throw new AppException("Error inesperado al cargar la informaci�n", ex);
        } finally {
            AccesoDB.cerrarResultSet(rs);
            AccesoDB.cerrarStatement(ps);
            if (con.hayConexionAbierta()) {
                con.cierraConexionDB();
            }             
        }     
        return nombre;
    }
    
    
}
